﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.Interfaces;
using System.Timers;
using System.Net.Mail;
using Core.Utils;

namespace EnvioCorreoCumpleaños
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Timer timerBusqueda;
        private int contadorBusqueda;

        private Timer timerEjecutarTareas;
        private int contadorEjecutarTareas;
        public MainWindow()
        {
            timerBusqueda = new Timer
            {
                Interval = 1000
            };
            contadorBusqueda = 10;

            timerEjecutarTareas = new Timer
            {
                Interval = 1000
            };
            contadorEjecutarTareas = 27;
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            timerBusqueda.Elapsed += TimerBusqueda_Elapsed;
            timerBusqueda.Start();

            timerEjecutarTareas.Elapsed += TimerEjecutarTareas_Elapsed;
        }

        private async void TimerEjecutarTareas_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (this.contadorEjecutarTareas <= 0)
            {
                await Dispatcher.Invoke(async () =>
                {
                    timerEjecutarTareas.Stop();
                    this.contadorEjecutarTareas = 27;
                    await Task.Run(async () => await programarTareas());
                    //Task.Run( await BuscarEventosTablero());
                    //await BuscarEventosTablero();
                    this.timerEjecutarTareas.Start();
                });
            }
            else
            {
                this.contadorEjecutarTareas--;
            }
            Dispatcher.Invoke(() =>
             this.lblContadorTareas.Content = $"{this.contadorEjecutarTareas.ToString()}"
            );
        }

        private async void TimerBusqueda_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (this.contadorBusqueda <= 0)
            {
                await Dispatcher.Invoke(async () =>
                {
                    timerBusqueda.Stop();
                    this.contadorBusqueda = 10;
                    await Task.Run(async () => await buscarTarearAsync());
                    this.timerBusqueda.Start();
                });
            }
            else
            {
                this.contadorBusqueda--;
            }
            Dispatcher.Invoke(() =>
             this.lblContadorBusqueda.Content = $"{this.contadorBusqueda.ToString()}"
            );
        }

        private async void Button_ClickAsync(object sender, RoutedEventArgs e)
        {
            await Task.Run(async () => buscarTarearAsync());
        }
        bool primero = true;
        private async Task buscarTarearAsync()
        {
            var resp = new TareasEnvioCorreoSvc().getTareasEnviosCorreo();
            if (resp.typeResult == ResultTypes.success)
            {
                Dispatcher.Invoke(() =>
                {
                    List<TareasEnvioCorreo> resplistaTareas = resp.result as List<TareasEnvioCorreo>;
                    List<TareasEnvioCorreo> listaTareas = lvlTareas.Items.Cast<TareasEnvioCorreo>().ToList();
                    foreach (var item in resplistaTareas)
                    {
                        if (!listaTareas.Exists(s => s.idTareasEnvioCorreo == item.idTareasEnvioCorreo))
                        {
                            lvlTareas.Items.Add(item);
                        }
                        else
                        {
                            if (item.diario)
                            {
                                DateTime hoy = Convert.ToDateTime(string.Format("{0}/{1}/{2} 00:00", DateTime.Now.Day.ToString(), DateTime.Now.Month.ToString(), DateTime.Now.Year.ToString()));
                                DateTime fechaEjecucion = Convert.ToDateTime(string.Format("{0}/{1}/{2} 00:00", item.fechaEjecucion.Day.ToString(), item.fechaEjecucion.Month.ToString(), item.fechaEjecucion.Year.ToString()));
                                if (hoy != fechaEjecucion)
                                {
                                    lvlTareas.Items.Add(item);
                                }
                            }
                        }
                    }
                    if (primero)
                    {
                        timerEjecutarTareas.Start();
                        primero = false;
                    }
                });
            }
        }

        private async Task programarTareas()
        {
            try
            {
                await Dispatcher.Invoke(async () =>
                 {
                     foreach (TareasEnvioCorreo tarea in lvlTareas.Items.Cast<TareasEnvioCorreo>().ToList().FindAll(s => s.estatusActividad == EstatusActividad.ACTIVO))
                     {
                         if (DateTime.Now > tarea.fechaEjecucion)
                         {
                             tarea.estatusActividad = EstatusActividad.EN_CURSO;
                             if (tarea.diario)
                             {
                                 var respVal = new TareasEnvioCorreoSvc().validarTareaDiaria(tarea);
                                 if (respVal.typeResult == ResultTypes.success)
                                 {
                                     tarea.estatusActividad = EstatusActividad.TERMINADO;
                                     continue;
                                 }
                             }
                             if (await Task.Run<bool>(async () => await iniciarTarea(tarea)))
                             {
                                 tarea.estatusActividad = EstatusActividad.TERMINADO;
                             }
                             else
                             {
                                 tarea.estatusActividad = EstatusActividad.ACTIVO;
                             }
                         }
                     }
                 });
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private async Task<bool> iniciarTarea(TareasEnvioCorreo tarea)
        {
            try
            {
                //System.Threading.Thread.Sleep(5000);
                return enviarCorreo(tarea);
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool enviarCorreo(TareasEnvioCorreo tarea)
        {
            MailMessage mensajeCorreo = new MailMessage();
            SmtpClient smtp = new Util().getSmtpClientEnvioCorreos();
            try
            {
                if (tarea.listaDetalles.Count <= 0) return false;

                var path = System.Windows.Forms.Application.StartupPath;
                string archivo = path + @"\config.ini";

                mensajeCorreo.From = new MailAddress("notificaciones@ciltra.com");// new MailAddress(new Util().Read("CONFIGURACION_CORREO", "USUARIO", archivo));
                mensajeCorreo.Subject = tarea.titulo;
                mensajeCorreo.Body = tarea.Texto;
                mensajeCorreo.IsBodyHtml = false;
                mensajeCorreo.Priority = MailPriority.Normal;
                foreach (var item in tarea.listaDetalles)
                {
                    mensajeCorreo.To.Add(item.correoElectronico.direccionCorreo);
                }

                if (tarea.idTareasEnvioCorreo == 1)
                {
                    var resp = new TareasEnvioCorreoSvc().getCumpleAñosPersonal();
                    if (resp.typeResult == ResultTypes.success)
                    {
                        List<CumpleañosPersonal> listaCumpleaños = resp.result as List<CumpleañosPersonal>;
                        mensajeCorreo.Body += Environment.NewLine;

                        string body = "<!DOCTYPE html><html lang=\"es\"><head><meta charset=\"utf - 8\" /> ";
                        body += "<style>table, td, th, tr {border: 1px solid black;}";
                        body += "table {border-collapse: collapse;width: 100%;}";
                        body += "th {height: 50px;background-color: SteelBlue;}";
                        body += "td{width: 50px}</style></head><body>";
                        body += string.Format("<table ><colgroup><col /><col /><col /><col /><col /></colgroup><caption>{0}</caption>", tarea.Texto);
                        body += "<thead><tr bgcolor=\"SteelBlue\"><th>NOMBRE</th><th>DEPARTAMENTO</th></tr></thead>";
                        body += "<tbody>";

                        foreach (var item in listaCumpleaños)
                        {
                            body += string.Format("<tr><td>{0}</td><td>{1}</td></tr>"
                                , item.nombre
                                , item.departamento);
                        }

                        body += "</tbody>";
                        body += "</table></body></html>";
                        mensajeCorreo.Body = body;
                        mensajeCorreo.IsBodyHtml = true;
                    }
                    else if (resp.typeResult == ResultTypes.recordNotFound)
                    {
                        //var respSave = new TareasEnvioCorreoSvc().saveTareaCorreosEnviados(tarea);
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }

                try
                {
                    smtp.Send(mensajeCorreo);
                    var respSave = new TareasEnvioCorreoSvc().saveTareaCorreosEnviados(tarea);
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                mensajeCorreo.Dispose();
                smtp.Dispose();
            }
        }
    }
}
