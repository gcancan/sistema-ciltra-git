﻿using Entidades;
using Negocio;
using PtoVenta.UI.Clases;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static PtoVenta.UI.Clases.Inicio;

namespace PtoVenta.UI.Catalogos
{
    public partial class fBancos : Form
    {
        //VARIABLES
        List<Bancos> _listOriginal;
        bool esIdentiti = true;
        string CampoLlave = "idBanco";
        public fBancos()
        {
            InitializeComponent();
        }

        private void fProductos_Load(object sender, EventArgs e)
        {
            Cancelar();
            CargaGrid();
            FormatoGrid();
            txtFiltro.Focus();
        }

        private void CargaGrid(int idProducto = 0)
        {
            OperationResult resp = new BancosSvc().getBancoxId();
            if (resp.typeResult == ResultTypes.success)
            {
                _listOriginal = (List<Bancos>)resp.result;
                ActualizaStatusControl(_listOriginal.Count);

                grdDatos.DataSource = _listOriginal;

                CargaForma(grdDatos.Rows[0].Cells[CampoLlave].Value.ToString(), opcBusqueda.Bancos);
            }
            else if (resp.typeResult == ResultTypes.recordNotFound)
            {
                _listOriginal = (List<Bancos>)resp.result;
                ActualizaStatusControl(_listOriginal.Count);

                grdDatos.DataSource = _listOriginal;

            }
        }

        private void ActualizaStatusControl(int NumRegistros)
        {
            statusStrip.Items[0].Text = "Registros Encontrados: " + NumRegistros;
        }

        private void FormatoGrid()
        {
            try
            {
                this.grdDatos.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                //this.grdDatos.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;

                this.grdDatos.RowsDefaultCellStyle.BackColor = Color.White;
                this.grdDatos.AlternatingRowsDefaultCellStyle.BackColor = Color.DarkSeaGreen;

                this.grdDatos.RowsDefaultCellStyle.SelectionForeColor = Color.Yellow;
                this.grdDatos.RowsDefaultCellStyle.SelectionBackColor = Color.Gray;
                this.grdDatos.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

                this.grdDatos.DefaultCellStyle.Font = new Font("Berlin Sans FB", 12);
                this.grdDatos.ColumnHeadersDefaultCellStyle.Font = new Font("Berlin Sans FB", 12);
                this.grdDatos.ColumnHeadersDefaultCellStyle.BackColor = Color.Yellow;
                this.grdDatos.ColumnHeadersDefaultCellStyle.ForeColor = Color.Red;
                this.grdDatos.EnableHeadersVisualStyles = false;

                //
                //grdDatos.Columns["idClasificacion"].Visible = false;
                //grdDatos.Columns["Foto"].Visible = false;

                grdDatos.Columns["idBanco"].HeaderText = "Banco";
                grdDatos.Columns["NombreBanco"].HeaderText = "Nombre";

                //grdDatos.Columns["Precio"].DefaultCellStyle.Format = "C2";
                //grdDatos.Columns["Precio"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight;

                grdDatos.Columns["idBanco"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                grdDatos.Columns["NombreBanco"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                //grdDatos.Columns["Precio"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
            catch (Exception e)
            {
                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + e.Message,
                  this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            }


        }

        private void CargaForma(string Valor, opcBusqueda opc)
        {
            if (opc == opcBusqueda.Bancos)
            {
                OperationResult resp = new BancosSvc().getBancoxId(Convert.ToInt32(Valor));
                if (resp.typeResult == ResultTypes.success)
                {
                    Bancos banco = ((List<Bancos>)resp.result)[0];
                    txtidBanco.Tag = banco;
                    txtidBanco.Text = banco.idBanco.ToString();
                    txtNombreBanco.Text = banco.NombreBanco;
                    chkActivo.Checked = banco.Activo;
                }
                else
                {

                }
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ActivaCampos(opcHabilita opc)
        {
            switch (opc)
            {
                case opcHabilita.DesHabilitaTodos:
                    gpoDatos.Enabled = false;
                    break;
                case opcHabilita.Nuevo:
                    break;
                case opcHabilita.Editando:
                    gpoDatos.Enabled = true;
                    txtidBanco.Enabled = false;
                    break;
                default:
                    break;
            }
        }

        private void grdDatos_Enter(object sender, EventArgs e)
        {
            if (grdDatos.Rows.Count > 0)
            {
                CargaForma(grdDatos.Rows[grdDatos.CurrentCell.RowIndex].Cells[CampoLlave].Value.ToString(), opcBusqueda.Bancos);
            }

        }

        private void grdDatos_Click(object sender, EventArgs e)
        {
            CargaForma(grdDatos.Rows[grdDatos.CurrentCell.RowIndex].Cells[CampoLlave].Value.ToString(), opcBusqueda.Bancos);
        }

        private void txtFiltro_TextChanged(object sender, EventArgs e)
        {
            if (txtFiltro.Text.Length > 0)
            {
                var mySearch = (_listOriginal).FindAll(S => S.idBanco.ToString().IndexOf(txtFiltro.Text.Trim(), StringComparison.OrdinalIgnoreCase) >= 0 ||
                S.NombreBanco.IndexOf(txtFiltro.Text.Trim(), StringComparison.OrdinalIgnoreCase) >= 0
                );
                ActualizaStatusControl(mySearch.Count);
                grdDatos.DataSource = mySearch;
            }
            else
            {
                ActualizaStatusControl(_listOriginal.Count);
                grdDatos.DataSource = _listOriginal;
            }
            CargaForma(grdDatos.Rows[0].Cells[CampoLlave].Value.ToString(), opcBusqueda.Bancos);
        }

        private void txtFiltro_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                grdDatos.Focus();
            }
        }

        private void grdDatos_SelectionChanged(object sender, EventArgs e)
        {
            CargaForma(grdDatos.Rows[grdDatos.CurrentCell.RowIndex].Cells[CampoLlave].Value.ToString(), opcBusqueda.Bancos);
        }

        private void fProductos_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F3)
            {
                txtFiltro.Focus();
            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            txtidBanco.Tag = null;
            ActivaCampos(opcHabilita.Editando);
            ActivaBotones(opcHabilita.Editando);
            grdDatos.Enabled = false;
            LimpiaCampos();
            if (esIdentiti)
            {
                txtidBanco.Enabled = false;
                txtNombreBanco.Focus();
            }
            else
            {
                txtidBanco.Focus();
            }
        }

        private void LimpiaCampos()
        {
            txtidBanco.Clear();
            txtNombreBanco.Clear();
            chkActivo.Checked = false;

        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            ActivaCampos(opcHabilita.Editando);
            ActivaBotones(opcHabilita.Editando);
            grdDatos.Enabled = false;
            txtidBanco.Focus();
        }

        private void ActivaBotones(opcHabilita opc)
        {
            switch (opc)
            {
                case opcHabilita.DesHabilitaTodos:
                    break;
                case opcHabilita.Iniciando:
                    btnNuevo.Enabled = true;
                    btnGrabar.Enabled = false;
                    btnEditar.Enabled = true;
                    btnReporte.Enabled = true;
                    btnCancelar.Enabled = false;
                    btnSalir.Enabled = true;
                    break;
                case opcHabilita.Editando:
                    btnNuevo.Enabled = false;
                    btnGrabar.Enabled = true;
                    btnEditar.Enabled = false;
                    btnReporte.Enabled = false;
                    btnCancelar.Enabled = true;
                    btnSalir.Enabled = false;
                    break;
                case opcHabilita.Nuevo:
                    break;
                default:
                    break;
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Cancelar();
        }

        private void Cancelar()
        {
            LimpiaCampos();
            ActivaCampos(opcHabilita.DesHabilitaTodos);
            ActivaBotones(opcHabilita.Iniciando);
            CargaGrid();
            grdDatos.Enabled = true;
            grdDatos.Focus();
        }

        private void btnGrabar_Click(object sender, EventArgs e)
        {
            guardar();
        }

        public void guardar()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (!validar())
                {
                    return;
                }
                Bancos banco = mapForm();
                if (banco != null)
                {
                    var resp = new BancosSvc().GuardaBanco(ref banco);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        mapForm((Bancos)resp.result);
                        Cancelar();
                        //buscarEstados();
                    }
                    Tools.imprimirMensaje(resp);
                }
            }
            catch (Exception ex)
            {
                Tools.imprimirMensaje(new OperationResult { valor = 1, mensaje = ex.Message });
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private bool validar()
        {
            if (!esIdentiti)
            {
                if (string.IsNullOrEmpty(txtidBanco.Text.Trim()))
                {
                    MessageBox.Show("El ID del Banco no puede ser Vacio", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtidBanco.Focus();
                    return false;
                }
            }
            else
            {

            }

            //if (string.IsNullOrEmpty(txtidBanco.Text.Trim()))
            //{
            //    MessageBox.Show("El Codigo del Banco no puede ser Vacio", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //    txtidBanco.Focus();
            //    return false;
            //}
            if (string.IsNullOrEmpty(txtNombreBanco.Text.Trim()))
            {
                MessageBox.Show("El Nombre del Banco no puede ser Vacio", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtNombreBanco.Focus();
                return false;
            }

            return true;
        }

        private Bancos mapForm()
        {
            try
            {
                return new Bancos
                {
                //    if (opc == opcForma.Nuevo)
                //{

                //}
                //else if (opc == opcForma.Editar)
                //{

                //}

                    idBanco = txtidBanco.Tag == null ? 0 : ((Bancos)txtidBanco.Tag).idBanco,
                    NombreBanco = txtNombreBanco.Text.Trim(),
                    Activo = chkActivo.Checked
                };
        }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrio un error al querer obtener información del catálogo" +
                    Environment.NewLine + ex.Message, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
}

private void mapForm(Bancos banco)
{
    if (banco != null)
    {
        txtidBanco.Tag = banco;
        txtNombreBanco.Text = banco.NombreBanco;
        chkActivo.Checked = banco.Activo;
    }
    else
    {
        txtidBanco.Tag = null;
        txtNombreBanco.Clear();
        chkActivo.Checked = false;
    }
}

        private void txtidBanco_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtidBanco_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtNombreBanco.Focus();
                txtNombreBanco.Select();
            }
        }

        private void txtNombreBanco_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                chkActivo.Focus();
            }
        }

        private void chkActivo_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void chkActivo_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnGrabar.PerformClick();
            }
        }
    }//
}
