﻿using Core.Models;
using Fletera2.Clases;
using Fletera2Entidades;
using Fletera2Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Fletera2.Catalogos
{
    public partial class fCatGastos : Form
    {
        //VARIABLES
        List<CatGastos> _listOriginal;
        bool esIdentiti = true;
        string CampoLlave = "idGasto";
        public fCatGastos()
        {
            InitializeComponent();
        }

        private void fProductos_Load(object sender, EventArgs e)
        {
            Cancelar();
            CargaGrid();
            FormatoGrid();
            txtFiltro.Focus();
        }

        private void CargaGrid(int idProducto = 0)
        {
            OperationResult resp = new CatGastosSvc().getCatGastosxFiltro();
            if (resp.typeResult == ResultTypes.success)
            {
                _listOriginal = (List<CatGastos>)resp.result;
                ActualizaStatusControl(_listOriginal.Count);

                grdDatos.DataSource = _listOriginal;

                CargaForma(grdDatos.Rows[0].Cells[CampoLlave].Value.ToString(), opcBusqueda.CatGastos);
            }
            else if (resp.typeResult == ResultTypes.recordNotFound)
            {
                _listOriginal = (List<CatGastos>)resp.result;
                ActualizaStatusControl(_listOriginal.Count);

                grdDatos.DataSource = _listOriginal;

            }
        }

        private void ActualizaStatusControl(int NumRegistros)
        {
            statusStrip.Items[0].Text = "Registros Encontrados: " + NumRegistros;
        }

        private void FormatoGrid()
        {
            try
            {
                this.grdDatos.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                //this.grdDatos.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;

                this.grdDatos.RowsDefaultCellStyle.BackColor = Color.White;
                this.grdDatos.AlternatingRowsDefaultCellStyle.BackColor = Color.AliceBlue;

                this.grdDatos.RowsDefaultCellStyle.SelectionForeColor = Color.Yellow;
                this.grdDatos.RowsDefaultCellStyle.SelectionBackColor = Color.Gray;
                this.grdDatos.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

                this.grdDatos.DefaultCellStyle.Font = new Font("Berlin Sans FB", 12);
                this.grdDatos.ColumnHeadersDefaultCellStyle.Font = new Font("Berlin Sans FB", 12);
                this.grdDatos.ColumnHeadersDefaultCellStyle.BackColor = Color.Yellow;
                this.grdDatos.ColumnHeadersDefaultCellStyle.ForeColor = Color.Red;
                this.grdDatos.EnableHeadersVisualStyles = false;

                //
                //grdDatos.Columns["idClasificacion"].Visible = false;
                //grdDatos.Columns["Foto"].Visible = false;

                grdDatos.Columns["idGasto"].HeaderText = "Id";
                grdDatos.Columns["NomGasto"].HeaderText = "NomGasto";

                //grdDatos.Columns["Precio"].DefaultCellStyle.Format = "C2";
                //grdDatos.Columns["Precio"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight;

                grdDatos.Columns["idGasto"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                grdDatos.Columns["NomGasto"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                //grdDatos.Columns["Precio"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
            catch (Exception e)
            {
                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + e.Message,
                  this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            }


        }

        private void CargaForma(string Valor, opcBusqueda opc)
        {
            if (opc == opcBusqueda.CatGastos)
            {
                OperationResult resp = new CatGastosSvc().getCatGastosxFiltro(Convert.ToInt32(Valor));
                if (resp.typeResult == ResultTypes.success)
                {
                    CatGastos catalogo = ((List<CatGastos>)resp.result)[0];
                    txtidGasto.Tag = catalogo;
                    txtidGasto.Text = catalogo.idGasto.ToString();
                    txtNomGasto.Text = catalogo.NomGasto;
                    chkDeducible.Checked = catalogo.Deducible;
                    chkReqAutorizacion.Checked = catalogo.ReqAutorizacion;
                    chkReqComprobante.Checked = catalogo.ReqComprobante;
                    chkActivo.Checked = catalogo.Activo;
                }
                else
                {

                }
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ActivaCampos(opcHabilita opc)
        {
            switch (opc)
            {
                case opcHabilita.DesHabilitaTodos:
                    gpoDatos.Enabled = false;
                    break;
                case opcHabilita.Nuevo:
                    break;
                case opcHabilita.Editando:
                    gpoDatos.Enabled = true;
                    txtidGasto.Enabled = false;
                    break;
                default:
                    break;
            }
        }

        private void grdDatos_Enter(object sender, EventArgs e)
        {
            if (grdDatos.Rows.Count > 0)
            {
                CargaForma(grdDatos.Rows[grdDatos.CurrentCell.RowIndex].Cells[CampoLlave].Value.ToString(), opcBusqueda.CatGastos);
            }

        }

        private void grdDatos_Click(object sender, EventArgs e)
        {
            CargaForma(grdDatos.Rows[grdDatos.CurrentCell.RowIndex].Cells[CampoLlave].Value.ToString(), opcBusqueda.CatGastos);
        }

        private void txtFiltro_TextChanged(object sender, EventArgs e)
        {
            if (txtFiltro.Text.Length > 0)
            {
                var mySearch = (_listOriginal).FindAll(S => S.idGasto.ToString().IndexOf(txtFiltro.Text.Trim(), StringComparison.OrdinalIgnoreCase) >= 0 ||
                S.NomGasto.IndexOf(txtFiltro.Text.Trim(), StringComparison.OrdinalIgnoreCase) >= 0
                );
                ActualizaStatusControl(mySearch.Count);
                grdDatos.DataSource = mySearch;
            }
            else
            {
                ActualizaStatusControl(_listOriginal.Count);
                grdDatos.DataSource = _listOriginal;
            }
            CargaForma(grdDatos.Rows[0].Cells[CampoLlave].Value.ToString(), opcBusqueda.CatGastos);
        }

        private void txtFiltro_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                grdDatos.Focus();
            }
        }

        private void grdDatos_SelectionChanged(object sender, EventArgs e)
        {
            CargaForma(grdDatos.Rows[grdDatos.CurrentCell.RowIndex].Cells[CampoLlave].Value.ToString(), opcBusqueda.CatGastos);
        }

        private void fProductos_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F3)
            {
                txtFiltro.Focus();
            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            txtidGasto.Tag = null;
            ActivaCampos(opcHabilita.Editando);
            ActivaBotones(opcHabilita.Editando);
            grdDatos.Enabled = false;
            LimpiaCampos();
            if (esIdentiti)
            {
                txtidGasto.Enabled = false;
                txtNomGasto.Focus();
            }
            else
            {
                txtidGasto.Focus();
            }
        }

        private void LimpiaCampos()
        {
            txtidGasto.Clear();
            txtNomGasto.Clear();
            chkActivo.Checked = false;

        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            ActivaCampos(opcHabilita.Editando);
            ActivaBotones(opcHabilita.Editando);
            grdDatos.Enabled = false;
            txtidGasto.Focus();
        }

        private void ActivaBotones(opcHabilita opc)
        {
            switch (opc)
            {
                case opcHabilita.DesHabilitaTodos:
                    break;
                case opcHabilita.Iniciando:
                    btnNuevo.Enabled = true;
                    btnGrabar.Enabled = false;
                    btnEditar.Enabled = true;
                    btnReporte.Enabled = true;
                    btnCancelar.Enabled = false;
                    btnSalir.Enabled = true;
                    break;
                case opcHabilita.Editando:
                    btnNuevo.Enabled = false;
                    btnGrabar.Enabled = true;
                    btnEditar.Enabled = false;
                    btnReporte.Enabled = false;
                    btnCancelar.Enabled = true;
                    btnSalir.Enabled = false;
                    break;
                case opcHabilita.Nuevo:
                    break;
                default:
                    break;
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Cancelar();
        }

        private void Cancelar()
        {
            LimpiaCampos();
            ActivaCampos(opcHabilita.DesHabilitaTodos);
            ActivaBotones(opcHabilita.Iniciando);
            CargaGrid();
            grdDatos.Enabled = true;
            grdDatos.Focus();
        }

        private void btnGrabar_Click(object sender, EventArgs e)
        {
            guardar();
        }

        public void guardar()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (!validar())
                {
                    return;
                }
                CatGastos cat = mapForm();
                if (cat != null)
                {
                    var resp = new CatGastosSvc().GuardaCatGastos(ref cat);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        mapForm((CatGastos)resp.result);
                        Cancelar();
                        //buscarEstados();
                    }
                    Tools.imprimirMensaje(resp);
                }
            }
            catch (Exception ex)
            {
                Tools.imprimirMensaje(new OperationResult { valor = 1, mensaje = ex.Message });
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private bool validar()
        {
            if (!esIdentiti)
            {
                if (string.IsNullOrEmpty(txtidGasto.Text.Trim()))
                {
                    MessageBox.Show("El ID del Banco no puede ser Vacio", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtidGasto.Focus();
                    return false;
                }
            }
            else
            {

            }

            //if (string.IsNullOrEmpty(txtidBanco.Text.Trim()))
            //{
            //    MessageBox.Show("El Codigo del Banco no puede ser Vacio", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //    txtidBanco.Focus();
            //    return false;
            //}
            if (string.IsNullOrEmpty(txtNomGasto.Text.Trim()))
            {
                MessageBox.Show("El Nombre del Banco no puede ser Vacio", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtNomGasto.Focus();
                return false;
            }

            return true;
        }

        private CatGastos mapForm()
        {
            try
            {
                return new CatGastos
                {
                    //    if (opc == opcForma.Nuevo)
                    //{

                    //}
                    //else if (opc == opcForma.Editar)
                    //{

                    //}

                    idGasto = txtidGasto.Tag == null ? 0 : ((CatGastos)txtidGasto.Tag).idGasto,
                    NomGasto = txtNomGasto.Text.Trim(),
                    Deducible = chkDeducible.Checked,
                    ReqAutorizacion = chkReqAutorizacion.Checked,
                    ReqComprobante = chkReqComprobante.Checked,
                    Activo = chkActivo.Checked
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrio un error al querer obtener información del catálogo" +
                    Environment.NewLine + ex.Message, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }

        private void mapForm(CatGastos cat)
        {
            if (cat != null)
            {
                txtidGasto.Tag = cat;
                txtNomGasto.Text = cat.NomGasto;
                chkDeducible.Checked = cat.Deducible;
                chkReqAutorizacion.Checked = cat.ReqAutorizacion;
                chkReqComprobante.Checked = cat.ReqComprobante;
                chkActivo.Checked = cat.Activo;
            }
            else
            {
                txtidGasto.Tag = null;
                txtNomGasto.Clear();
                chkDeducible.Checked = false;
                chkReqAutorizacion.Checked = false;
                chkReqComprobante.Checked = false;
                chkActivo.Checked = false;
            }
        }

        private void txtidBanco_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtidBanco_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtNomGasto.Focus();
                txtNomGasto.Select();
            }
        }

        private void txtNombreBanco_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                chkActivo.Focus();
            }
        }

        private void chkActivo_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void chkActivo_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnGrabar.PerformClick();
            }
        }
    }//
}
