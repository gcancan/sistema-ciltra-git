﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2.Clases
{
    public enum opcBusqueda
    {
        CatTipoFolDin = 1,
        FoliosDinero = 2,
        FoliosDineroPadre = 3,
        CatConceptosLiq = 4,
        CatGastos = 5,
        CompGastosFoliosDin = 6,
        CabViajes = 7,
        PreSalidas = 8,
        PreOC = 9,
        OrdenTrabajo = 10,
        CatTipoUniTrans = 11,
        CatUnidadTrans = 12,
        CabOrdenServicio = 13,
        MasterOrdServ = 14,
        CatActividades = 15,
        CatServicios = 16,
        CatLLantas = 17,
        CatLugares = 18,
        opServLlantas = 19,
        Insumos = 20
    }
}
