﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2.Clases
{
    public enum  opcHabilita
    {
        DesHabilitaTodos = 1,
        Iniciando = 2,
        Editando = 3,
        Nuevo = 4,
        Reporte = 5
    }
}
