﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fletera2.Clases
{
    public class Tools
    {
        public static void imprimirMensaje(OperationResult oResult)
        {
            switch (oResult.typeResult)
            {
                case ResultTypes.success:
                    MessageBox.Show(oResult.mensaje, "Operación Exitosa", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case ResultTypes.error:
                    MessageBox.Show(oResult.mensaje, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case ResultTypes.warning:
                    MessageBox.Show(oResult.mensaje, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    break;
                case ResultTypes.recordNotFound:
                    MessageBox.Show(oResult.mensaje, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    break;
                default:
                    break;
            }
        }

        public static string FormatFecHora(DateTime vFecha, bool FechaUnificada, bool IncluyeHora = true, bool FechaFox = false)
        {
            //Dim strFecha As String = ""
            //YYYYMMDD HH:mm:ss
            if (FechaFox)
            {
                if (IncluyeHora)
                {
                    return vFecha.ToString("MM/dd/yyyy HH:mm:ss");
                }
                else
                {
                    //ANTES ojo
                    return vFecha.ToString("MM/dd/yyyy");
                    //'Return vFecha.ToString("dd/MM/yyyy")
                    //Return vFecha.ToString("yyyy/MM/dd")
                }
            }
            else
            {
                if (IncluyeHora)
                {
                    //strFecha = vFecha.ToString("dd/MM/yyyy HH:mm:ss")
                    if (FechaUnificada)
                    {
                        return vFecha.ToString("yyyyMMdd HH:mm:ss");
                    }
                    else
                    {
                        return vFecha.ToString("dd/MM/yyyy HH:mm:ss");
                    }

                    //Return
                }
                else
                {
                    if (FechaUnificada)
                    {
                        return vFecha.ToString("yyyyMMdd");
                    }
                    else
                    {
                        return vFecha.ToString("dd/MM/yyyy");
                    }

                }
            }

        }

        public static System.Drawing.Bitmap getImage(byte[] imageBytes)
        {
            try
            {
                //System.IO.MemoryStream ms = new System.IO.MemoryStream(imageBytes, 0,
                //  imageBytes.Length, true, false);

                MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length);
                ms.Write(imageBytes, 0, imageBytes.Length);
                ms.Position = 0; // this is important

                // Convert byte[] to Image
                //ms.Write(imageBytes, 0, imageBytes.Length);
                System.Drawing.Image image =
                    System.Drawing.Image.FromStream(ms, true);

                //Image image = (Bitmap)((new ImageConverter()).ConvertFrom(imageBytes));


                //return Image = Image.FromStream(ms, true);

                ms.Flush();
                ms.Close();
                ms.Dispose();
                return (System.Drawing.Bitmap)image;
            }
            catch (Exception ex)
            {
                return null;
                throw new Exception(ex.Message);

            }
        }

        public DataTable EnumToDataTable2(Type enumType)
        {
            DataTable table = new DataTable();

            //cmbActividades.DisplayMember = "VALUE";
            //cmbActividades.ValueMember = "KEY";
            //Column that contains the Captions/Keys of Enum        
            table.Columns.Add("VALUE", typeof(string));
            //Get the type of ENUM for DataColumn
            table.Columns.Add("KEY", Enum.GetUnderlyingType(enumType));
            //Add the items from the enum:
            foreach (string name in Enum.GetNames(enumType))
            {
                if (name != "NO_APLICA")
                {
                    //Replace underscores with space from caption/key and add item to collection:
                    table.Rows.Add(name.Replace('_', ' '), Enum.Parse(enumType, name));
                }
            }

            return table;
        }

       
    }//
}
