﻿using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;

namespace Fletera2.Clases
{
    public class ImprimeOrdenServicio
    {
        //Variables Globales
        DataColumn myDataColumn;
        DataRow myDataRow;
        DataTable MyTablaImprime = new DataTable("IMPRIME");
        int LugarNumericos = 12;

        //PROPIEDADES
        #region Propiedades
        public Font FuenteImprimeCampos { get; set; }
        public Font FuenteImprimeCampos2 { get; set; }
        public Font FuenteImprimeCampos3 { get; set; }
        public Font FuenteImprimeCampos4 { get; set; }
        public Font FuenteImprimeCampos5 { get; set; }

        public Font FuenteEtiquetas1 { get; set; }
        public Font FuenteEtiquetas2 { get; set; }

        public string NomTabla { get; set; }
        public string NomReporte { get; set; }
        public DataTable TablaImprime { get; set; }

        public Image imgLogo1 { get; set; }
        public Size TamanioimgLogo1 { get; set; }
        public Point PosicionimgLogo1 { get; set; }

        public Image imgLogo2 { get; set; }
        public Size TamanioimgLogo2 { get; set; }
        public Point PosicionimgLogo2 { get; set; }

        #endregion

        enum TipoDato
        {
            Texto = 0,
            Entero = 1,
            Numerico = 2,
            Moneda = 3,
            Fecha = 4,
            Hora = 5

        }
        private void CreaTablaImprime()
        {

            myDataColumn = new DataColumn();
            myDataColumn.DataType = System.Type.GetType("System.Int32");
            myDataColumn.ColumnName = "Campo";
            myDataColumn.ReadOnly = false;
            myDataColumn.Unique = false;
            MyTablaImprime.Columns.Add(myDataColumn);

            myDataColumn = new DataColumn();
            myDataColumn.DataType = System.Type.GetType("System.String");
            myDataColumn.ColumnName = "Descripcion";
            myDataColumn.ReadOnly = false;
            myDataColumn.Unique = false;
            MyTablaImprime.Columns.Add(myDataColumn);

            myDataColumn = new DataColumn();
            myDataColumn.DataType = System.Type.GetType("System.String");
            myDataColumn.ColumnName = "Valor";
            myDataColumn.ReadOnly = false;
            myDataColumn.Unique = false;
            MyTablaImprime.Columns.Add(myDataColumn);

            myDataColumn = new DataColumn();
            myDataColumn.DataType = System.Type.GetType("System.Single");
            myDataColumn.ColumnName = "PosDescX";
            myDataColumn.ReadOnly = false;
            myDataColumn.Unique = false;
            MyTablaImprime.Columns.Add(myDataColumn);

            myDataColumn = new DataColumn();
            myDataColumn.DataType = System.Type.GetType("System.Single");
            myDataColumn.ColumnName = "PosDescY";
            myDataColumn.ReadOnly = false;
            myDataColumn.Unique = false;
            MyTablaImprime.Columns.Add(myDataColumn);

            myDataColumn = new DataColumn();
            myDataColumn.DataType = System.Type.GetType("System.Single");
            myDataColumn.ColumnName = "PosValX";
            myDataColumn.ReadOnly = false;
            myDataColumn.Unique = false;
            MyTablaImprime.Columns.Add(myDataColumn);

            myDataColumn = new DataColumn();
            myDataColumn.DataType = System.Type.GetType("System.Single");
            myDataColumn.ColumnName = "PosValY";
            myDataColumn.ReadOnly = false;
            myDataColumn.Unique = false;
            MyTablaImprime.Columns.Add(myDataColumn);

            myDataColumn = new DataColumn();
            myDataColumn.DataType = System.Type.GetType("System.Single");
            myDataColumn.ColumnName = "Fuente";
            myDataColumn.ReadOnly = false;
            myDataColumn.Unique = false;
            MyTablaImprime.Columns.Add(myDataColumn);

            myDataColumn = new DataColumn();
            myDataColumn.DataType = System.Type.GetType("System.Int32");
            myDataColumn.ColumnName = "Lado";
            myDataColumn.ReadOnly = false;
            myDataColumn.Unique = false;
            MyTablaImprime.Columns.Add(myDataColumn);

            myDataColumn = new DataColumn();
            myDataColumn.DataType = System.Type.GetType("System.Int32");
            myDataColumn.ColumnName = "TipoDatoValor";
            myDataColumn.ReadOnly = false;
            myDataColumn.Unique = false;
            MyTablaImprime.Columns.Add(myDataColumn);

        }

        private void InsertFila(int Campo, string Descripcion, string Valor, Single PosDescX, Single PosDescY,
           Single PosValX, Single PosValY, Single vFuente, Int32 ladoImprimir, TipoDato TipoDatoValor)
        {

            myDataRow = MyTablaImprime.NewRow();
            myDataRow["Campo"] = Campo;
            myDataRow["Descripcion"] = Descripcion;
            myDataRow["Valor"] = Valor;
            myDataRow["PosDescX"] = PosDescX;
            myDataRow["PosDescY"] = PosDescY;
            myDataRow["PosValX"] = PosValX;
            myDataRow["PosValY"] = PosValY;
            myDataRow["Fuente"] = vFuente;
            myDataRow["Lado"] = ladoImprimir;
            myDataRow["TipoDatoValor"] = TipoDatoValor;
            MyTablaImprime.Rows.Add(myDataRow);

        }

        public DataTable ArmaTablaTicket(CabOrdenServicio folio, List<DetOrdenServicio> listaDetOS)
        {
            //Dim Y1 As Single = 200
            //Dim BandImpServ = False
            //Dim XSERV As Single = 0
            //Dim XACT As Single = 80
            //Dim XCODBAR As Single = 100



            //Dim ValPad As Integer = 12

            //DsRep = New DataSet
            //If MyTablaImprime.Rows.Count > 0 Then
            //    MyTablaImprime.Rows.Clear()
            //End If


            //DsRep = ImpRep.LlenaDsImprime(ImpRep.NomTabla)
            //If DsRep.Tables(ImpRep.NomTabla).DefaultView.Count > 0 Then

            //    For i As Integer = 0 To DsRep.Tables(ImpRep.NomTabla).DefaultView.Count - 1
















            //        'LINEA 6
            //        campo += 1
            //        PosDescY = PosDescY + 30 : PosValorY = PosDescY
            //        Descripcion = "TRACTOR: "
            //        Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idTractor"))
            //        PosDescX = X1 : PosValorX = PosDescX + 70
            //        If DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idTractor") = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idUnidadTrans") Then
            //            InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
            //        Else
            //            InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)
            //        End If


            //        campo += 1
            //        Descripcion = "REMOLQUE 1:"
            //        Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idTolva1"))
            //        PosDescY = PosDescY + 15 : PosValorY = PosDescY
            //        PosDescX = X1 : PosValorX = PosDescX + 95
            //        If DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idTolva1") = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idUnidadTrans") Then
            //            InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
            //        Else
            //            InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)
            //        End If



            //        'LINEA 7
            //        campo += 1
            //        PosDescY = PosDescY + 15 : PosValorY = PosDescY
            //        Descripcion = "DOLLY: "
            //        Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idDolly"))
            //        PosDescX = X1 : PosValorX = PosDescX + 50
            //        If DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idDolly") = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idUnidadTrans") Then
            //            InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
            //        Else
            //            InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)
            //        End If


            //        campo += 1
            //        Descripcion = "REMOLQUE 2:"
            //        Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idTolva2"))
            //        PosDescX = X1 : PosValorX = PosDescX + 95
            //        PosDescY = PosDescY + 15 : PosValorY = PosDescY
            //        If DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idTolva2") = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idUnidadTrans") Then
            //            InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
            //        Else
            //            InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)
            //        End If


            //        'LINEA 8
            //        'campo += 1
            //        'PosDescY = PosDescY + 15 : PosValorY = PosDescY
            //        'Descripcion = "KILOMETRAJE: "
            //        'Valor = Format(CInt(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("Kilometraje")), "##,##0")
            //        'PosDescX = X1 : PosValorX = PosDescX + 110
            //        'InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)


            //        'LINEA 9
            //        campo += 1
            //        PosDescY = PosDescY + 30 : PosValorY = PosDescY
            //        Descripcion = ""
            //        'If bOrdenTrab Then
            //        '    Valor = "REPARACIONES SOLICITADAS"
            //        'Else
            //        '    Valor = "DESCRIPCION DEL SERVICIO"
            //        'End If
            //        Valor = "DESCRIPCION DEL SERVICIO"
            //        PosDescX = X1 : PosValorX = PosDescX + 25
            //        InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 5)

            //        PosDescY = PosDescY + 30 : PosValorY = PosDescY

            //        'LINEA 10
            //        'DETALLE
            //        For j As Integer = 0 To DsRep.Tables(ImpRep.NomTabla).DefaultView.Count - 1
            //            If bOrdenTrab Then
            //                If DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NomServicio") <> "" Then
            //                    If Not BandImpServ Then
            //                        campo += 1
            //                        Descripcion = ""
            //                        Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NomServicio")
            //                        PosDescX = XSERV : PosValorX = PosDescX + 25
            //                        InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
            //                        BandImpServ = True

            //                    End If
            //                Else
            //                    BandImpServ = False
            //                    '12/JULIO/2017
            //                    'If DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NombreAct") <> "" Then
            //                    '    campo += 1
            //                    '    PosDescY = PosDescY + 15 : PosValorY = PosDescY
            //                    '    Descripcion = ""
            //                    '    Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NombreAct")
            //                    '    PosDescX = X1 : PosValorX = PosDescX + 25
            //                    '    InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
            //                    'Else
            //                    '    'salto de linea
            //                    '    PosDescY = PosDescY + 15 : PosValorY = PosDescY
            //                    'End If
            //                End If

            //                campo += 1
            //                'PosDescY = PosDescY + 30 : PosValorY = PosDescY
            //                Descripcion = ""
            //                Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NotaRecepcion")
            //                PosDescY = PosDescY + 15 : PosValorY = PosDescY

            //                If BandImpServ Then
            //                    PosDescX = XACT : PosValorX = PosDescX + 25
            //                    InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)
            //                Else
            //                    PosDescX = XSERV : PosValorX = PosDescX + 25
            //                    InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
            //                End If



            //            Else
            //                campo += 1

            //                PosDescY = PosDescY + 15 : PosValorY = PosDescY
            //                Descripcion = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("idUnidadTrans")
            //                Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NomServicio")
            //                PosDescX = X1 - 35 : PosValorX = X2 + 25
            //                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
            //                'PosDescY = PosDescY + 15

            //            End If
            //        Next




            //        'LINEA 11
            //        campo += 1
            //        Descripcion = ""
            //        Valor = "___________________"
            //        PosDescY = PosDescY + 100 : PosValorY = PosDescY
            //        PosDescX = X1 : PosValorX = PosDescX + 25
            //        InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
            //        'PosDescY = PosDescY + 15

            //        campo += 1
            //        Descripcion = ""
            //        Valor = " ( OPERADOR ) "
            //        PosDescY = PosDescY + 15 : PosValorY = PosDescY
            //        PosDescX = X1 : PosValorX = PosDescX + 35
            //        InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)

            //        campo += 1
            //        PosDescY = PosDescY + 15 : PosValorY = PosDescY
            //        Descripcion = ""
            //        Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("NomOperador"))
            //        PosDescX = X1 : PosValorX = PosDescX + 25
            //        InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 4)



            //        campo += 1
            //        Descripcion = ""
            //        Valor = "___________________"
            //        PosDescY = PosDescY + 100 : PosValorY = PosDescY
            //        PosDescX = X1 : PosValorX = PosDescX + 25
            //        InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)

            //        campo += 1
            //        Descripcion = ""
            //        Valor = " ( AUTORIZÓ ) "
            //        PosDescY = PosDescY + 15 : PosValorY = PosDescY
            //        PosDescX = X1 : PosValorX = PosDescX + 35
            //        InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
            //        'LINEA 13

            //        campo += 1
            //        Descripcion = ""
            //        Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("NomAutoriza"))
            //        PosDescY = PosDescY + 15 : PosValorY = PosDescY
            //        PosDescX = X1 : PosValorX = PosDescX + 25
            //        InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 4)
            //        'PosDescY = PosDescY + 15

            //        'LINEA 14
            //        'CODIGO DE BARRAS ORDEN SERVICIO
            //        campo += 1
            //        Descripcion = ""
            //        'PosDescY = PosDescY + 15 : PosValorY = PosDescY
            //        PosDescY = PosDescY + 100 : PosValorY = PosDescY
            //        PosDescX = XCODBAR : PosValorX = XCODBAR
            //        Valor = FormarBarCode(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idPadreOrdSer"))
            //        InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 2)

            //        'LINEA 15
            //        campo += 1
            //        Descripcion = "FOLIO: "
            //        PosDescY = PosDescY + 45 : PosValorY = PosDescY
            //        PosDescX = X2 : PosValorX = X2 + 60
            //        Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idPadreOrdSer")
            //        InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)

            //        'LINEA 14
            //        'If idContrato = 0 Then
            //        '    '4 LINEAS
            //        '    PosDescY = PosDescY + 60
            //        'End If

            //        'campo += 1
            //        'Descripcion = "RECIBO NO."
            //        'PosDescX = X1 : PosDescY = PosDescY + 15 : PosValorX = PosDescX + 100 : PosValorY = PosDescY
            //        'Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("NUMRECIBO")
            //        'InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY)
            //    Next
            //End If
            //int Residuo = 0;
            //int maximo = 0;
            //int contCaracteres = 0;
            int campo = 0;
            string descripcion;
            string valor;
            Single PosValorX = 0;
            Single PosDescX = 0;
            Single PosValorY = 0;
            Single PosDescY = 0;

            Single XCODBAR = 100;

            Single Y1 = 200;
            Single X1 = 20;
            Single XINI = X1;
            Single X1A = 80;
            Single X2 = 150;
            Single X1B = X1 + 25;

            const int ValPad = 12;
            int SaltoLinea = 15;


            //string CadenaLarga;

            if (MyTablaImprime.Columns.Count == 0)
            {
                CreaTablaImprime();
            }
            if (MyTablaImprime.Rows.Count > 0)
            {
                MyTablaImprime.Rows.Clear();
            }

            #region Encabezado
            //1.- RAZON SOCIAL
            campo++;
            PosDescX = X1A;
            PosDescY = Y1;
            PosValorX = PosDescX;
            PosValorY = PosDescY;
            descripcion = folio.RazonSocial.ToUpper();
            valor = "";
            InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

            //2
            campo++;
            PosDescX = X1A;
            PosDescY = PosDescX;
            PosValorX = PosDescY + SaltoLinea;
            PosValorY = PosDescY;
            descripcion = "";
            valor = "ORDEN DE SERVICIO";
            InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

            //3
            campo++;
            PosDescX = X2;
            PosDescY = X1A;
            PosValorX = PosDescX;
            PosValorY = PosDescY + SaltoLinea;
            descripcion = "";
            valor = folio.NomTipOrdServ;
            InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 2, 1, TipoDato.Texto);


            //4 CODIGO DE BARRAS ORDEN SERVICIO 
            campo++;
            PosDescX = XCODBAR;
            PosDescY = PosDescY + 30;
            PosValorX = PosDescX;
            PosValorY = PosDescY;
            descripcion = "";
            valor = Inicio.FormarBarCode(folio.idOrdenSer.ToString());
            InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

            //LINEA 5
            campo++;
            PosDescX = X2;
            PosDescY = PosDescY + 45;
            PosValorX = PosDescX + 80;
            PosValorY = PosDescY;
            descripcion = "ORD.SER.";
            valor = folio.idOrdenSer.ToString();
            InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

            //linea 6
            campo++;
            PosDescY = PosDescY + SaltoLinea; PosValorY = PosDescY;
            PosDescX = PosDescX; PosValorX = PosDescX + 50;
            descripcion = "FECHA:";
            valor = folio.fechaCaptura.ToString("dd/MMM/yyyy") + " " + folio.fechaCaptura.ToShortTimeString();
            InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

            //LINEA 5.1
            campo++;
            PosDescY = PosDescY + 15; PosValorY = PosDescY;
            PosDescX = PosDescX; PosValorX = PosDescX + 40;
            descripcion = "HORA:";
            valor = folio.fechaCaptura.ToShortTimeString();
            InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);


            //PUNTO DE VENTA
            campo++;
            PosDescX = XINI;
            PosDescY = PosDescY + 15;
            PosValorX = PosDescX + 50;
            PosValorY = PosDescY;
            descripcion = "En Letras ";
            NumeroALetras numlet = new NumeroALetras();
            //valor = "SON : " + numlet.Convertir(Math.Abs(Math.Round((folio.Importe), 2)).ToString(), true);
            InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

            //NOTA DE VENTA
            campo++;
            PosDescX = XINI;
            PosDescY = PosDescY + 15;
            PosValorX = PosDescX + 40;
            PosValorY = PosDescY;
            descripcion = "Entrega:";
            valor = folio.NomEmpleadoEntrega;
            InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

            campo++;
            PosDescX = XINI;
            PosDescY = PosDescY + 45;
            PosValorX = PosDescX + 120;
            PosValorY = PosDescY;
            descripcion = "Recibe";
            valor = "__________________";
            InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

            campo++;
            //PosDescX = 0;
            PosDescY = PosDescY + 15;
            PosValorX = 150;
            PosValorY = PosDescY;
            //descripcion = "Nombre";
            descripcion = "";
            valor = folio.NomEmpleadoEntrega;
            InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

            #endregion







            //FIN
            /////////////////////////////////////////////////////////////////////////////////////
            return MyTablaImprime;
        }

        public Single ImprimeLinea(System.Drawing.Printing.PrintPageEventArgs e, int Indice, int LadoImprime)
        {
            Single leftMargin = e.MarginBounds.Left;

            Single PosValorX = 0;
            Single PosDescX = 0;
            Single PosValorY = 0;
            Single PosDescY = 0;
            Single FuenteCodBar = 0;

            string Descripcion;
            string Valor;
            int Lado;
            TipoDato TipoDatoValor;
            String Cadena = "";
            /////////////////////////////////////////////////////////////////////////////////////



            //IMAGENES
            /////////////////////////////////////////////////////////////////////////////////////
            for (int i = 0; i < TablaImprime.Rows.Count; i++)
            {
                Descripcion = TablaImprime.Rows[i]["Descripcion"].ToString();
                PosValorX = Convert.ToSingle(TablaImprime.Rows[i]["PosValX"]);
                PosDescX = Convert.ToSingle(TablaImprime.Rows[i]["PosDescX"]);
                PosValorY = Convert.ToSingle(TablaImprime.Rows[i]["PosValY"]);
                PosDescY = Convert.ToSingle(TablaImprime.Rows[i]["PosDescY"]);
                FuenteCodBar = Convert.ToSingle(TablaImprime.Rows[i]["Fuente"]);
                Lado = Convert.ToInt32(TablaImprime.Rows[i]["Lado"]);

                // Creates a pen that draws in red.
                //Pen myPen;
                // Creates a brush that fills in solid blue.
                SolidBrush myBrush;
                myBrush = new SolidBrush(Color.Black);

                if (LadoImprime == Lado)
                {
                    if (FuenteCodBar == 1)
                    {
                        e.Graphics.DrawString(Descripcion, FuenteImprimeCampos, myBrush, PosDescX, PosDescY);
                    }
                    else if (FuenteCodBar == 2)
                    {
                        e.Graphics.DrawString(Descripcion, FuenteImprimeCampos2, myBrush, PosDescX, PosDescY);
                    }
                    else if (FuenteCodBar == 3)
                    {
                        e.Graphics.DrawString(Descripcion, FuenteImprimeCampos3, myBrush, PosDescX, PosDescY);
                    }
                    else if (FuenteCodBar == 4)
                    {
                        e.Graphics.DrawString(Descripcion, FuenteImprimeCampos4, myBrush, PosDescX, PosDescY);
                    }
                    else if (FuenteCodBar == 5)
                    {
                        e.Graphics.DrawString(Descripcion, FuenteImprimeCampos5, myBrush, PosDescX, PosDescY);
                    }


                    Valor = TablaImprime.Rows[i]["Valor"].ToString();
                    PosValorX = Convert.ToSingle(TablaImprime.Rows[i]["PosValX"]);
                    PosValorY = Convert.ToSingle(TablaImprime.Rows[i]["PosValY"]);
                    TipoDatoValor = (TipoDato)TablaImprime.Rows[i]["TipoDatoValor"];

                    if (FuenteCodBar == 1)
                    {

                        switch (TipoDatoValor)
                        {
                            case TipoDato.Texto:
                                e.Graphics.DrawString(Valor, FuenteImprimeCampos, Brushes.Black, PosValorX, PosValorY);
                                break;
                            case TipoDato.Entero:
                                e.Graphics.DrawString(Valor, FuenteImprimeCampos, Brushes.Black, PosValorX, PosValorY);
                                break;
                            case TipoDato.Numerico:
                                decimal vNumero = Convert.ToDecimal(TablaImprime.Rows[i]["Valor"].ToString());
                                Cadena = string.Format("{0:###,###0.00}", vNumero); //1.234,35 €
                                e.Graphics.DrawString((Cadena.PadLeft(LugarNumericos)), FuenteImprimeCampos, Brushes.Black, PosValorX, PosValorY);
                                break;
                            case TipoDato.Moneda:
                                decimal vMoney = Convert.ToDecimal(TablaImprime.Rows[i]["Valor"].ToString());
                                Cadena = string.Format("{0:c2}", vMoney); //1.234,35 €
                                e.Graphics.DrawString((Cadena.PadLeft(LugarNumericos)), FuenteImprimeCampos, Brushes.Black, PosValorX, PosValorY);
                                break;
                            case TipoDato.Fecha:
                                break;
                            case TipoDato.Hora:
                                break;
                            default:
                                break;
                        }
                    }
                    else if (FuenteCodBar == 2)
                    {
                        //CODIGO DE BARRAS
                        e.Graphics.DrawString(Valor, FuenteImprimeCampos2, Brushes.Black, PosValorX, PosValorY);

                    }
                    else if (FuenteCodBar == 3)
                    {

                        switch (TipoDatoValor)
                        {
                            case TipoDato.Texto:
                                e.Graphics.DrawString(Valor, FuenteImprimeCampos3, Brushes.Black, PosValorX, PosValorY);
                                break;
                            case TipoDato.Entero:
                                e.Graphics.DrawString(Valor, FuenteImprimeCampos3, Brushes.Black, PosValorX, PosValorY);
                                break;
                            case TipoDato.Numerico:
                                decimal vNumero = Convert.ToDecimal(TablaImprime.Rows[i]["Valor"].ToString());
                                Cadena = string.Format("{0:###,###0.00}", vNumero); //1.234,35 €
                                e.Graphics.DrawString((Cadena.PadLeft(LugarNumericos)), FuenteImprimeCampos3, Brushes.Black, PosValorX, PosValorY);

                                break;
                            case TipoDato.Moneda:
                                decimal vMoney = Convert.ToDecimal(TablaImprime.Rows[i]["Valor"].ToString());
                                Cadena = string.Format("{0:c2}", vMoney); //1.234,35 €
                                e.Graphics.DrawString((Cadena.PadLeft(LugarNumericos)), FuenteImprimeCampos3, Brushes.Black, PosValorX, PosValorY);

                                break;
                            case TipoDato.Fecha:
                                break;
                            case TipoDato.Hora:
                                break;
                            default:
                                break;
                        }
                    }
                    else if (FuenteCodBar == 4)
                    {

                        switch (TipoDatoValor)
                        {
                            case TipoDato.Texto:
                                e.Graphics.DrawString(Valor, FuenteImprimeCampos4, Brushes.Black, PosValorX, PosValorY);
                                break;
                            case TipoDato.Entero:
                                e.Graphics.DrawString(Valor, FuenteImprimeCampos4, Brushes.Black, PosValorX, PosValorY);
                                break;
                            case TipoDato.Numerico:
                                decimal vNumero = Convert.ToDecimal(TablaImprime.Rows[i]["Valor"].ToString());
                                Cadena = string.Format("{0:###,###0.00}", vNumero); //1.234,35 €
                                e.Graphics.DrawString((Cadena.PadLeft(LugarNumericos)), FuenteImprimeCampos4, Brushes.Black, PosValorX, PosValorY);

                                break;
                            case TipoDato.Moneda:
                                decimal vMoney = Convert.ToDecimal(TablaImprime.Rows[i]["Valor"].ToString());
                                Cadena = string.Format("{0:c2}", vMoney); //1.234,35 €
                                e.Graphics.DrawString((Cadena.PadLeft(LugarNumericos)), FuenteImprimeCampos4, Brushes.Black, PosValorX, PosValorY);

                                break;
                            case TipoDato.Fecha:
                                break;
                            case TipoDato.Hora:
                                break;
                            default:
                                break;
                        }
                    }
                    else if (FuenteCodBar == 5)
                    {

                        switch (TipoDatoValor)
                        {
                            case TipoDato.Texto:
                                e.Graphics.DrawString(Valor, FuenteImprimeCampos5, Brushes.Black, PosValorX, PosValorY);
                                break;
                            case TipoDato.Entero:
                                e.Graphics.DrawString(Valor, FuenteImprimeCampos5, Brushes.Black, PosValorX, PosValorY);
                                break;
                            case TipoDato.Numerico:
                                decimal vNumero = Convert.ToDecimal(TablaImprime.Rows[i]["Valor"].ToString());
                                Cadena = string.Format("{0:###,###0.00}", vNumero); //1.234,35 €
                                e.Graphics.DrawString((Cadena.PadLeft(LugarNumericos)), FuenteImprimeCampos5, Brushes.Black, PosValorX, PosValorY);

                                break;
                            case TipoDato.Moneda:
                                decimal vMoney = Convert.ToDecimal(TablaImprime.Rows[i]["Valor"].ToString());
                                Cadena = string.Format("{0:c2}", vMoney); //1.234,35 €
                                e.Graphics.DrawString((Cadena.PadLeft(LugarNumericos)), FuenteImprimeCampos5, Brushes.Black, PosValorX, PosValorY);

                                break;
                            case TipoDato.Fecha:
                                break;
                            case TipoDato.Hora:
                                break;
                            default:
                                break;
                        }
                    }

                }


            }
            return PosDescX;

            /////////////////////////////////////////////////////////////////////////////////////
        }

        public Single ImprimeLogo(System.Drawing.Printing.PrintPageEventArgs e, Single yPos)
        {
            //IMAGENES
            e.Graphics.DrawImage(imgLogo1, PosicionimgLogo1.X, PosicionimgLogo1.Y, TamanioimgLogo1.Width, TamanioimgLogo1.Height);
            if (imgLogo2 != null)
            {
                e.Graphics.DrawImage(imgLogo2, PosicionimgLogo2.X, PosicionimgLogo2.Y, TamanioimgLogo2.Width, TamanioimgLogo2.Height);
            }

            yPos += FuenteImprimeCampos.GetHeight(e.Graphics);
            //e.Graphics.DrawImage(vImagen1, vPosicionImagen1)
            //yPos += vFuenteEncabezado.GetHeight(e.Graphics)
            return yPos;
        }
        //28/FEB/2018
        public DataTable ArmaTablaCarta(CabOrdenServicio folio, List<DetOrdenServicio> listaDetOS, bool bOrdenTrab)
        {
            //Variables
            int campo = 0;
            string Descripcion;
            string Valor;
            Single PosValorX = 0;
            Single PosDescX = 0;
            Single PosValorY = 0;
            Single PosDescY = 0;

            Single Y1 = 150;
            Single X1 = 20;
            Single X2A = 80;
            Single X2 = 160;
            //Single X3A = 260;
            Single X3A = 320;
            Single X4 = 400;


            //Variables

            if (MyTablaImprime.Columns.Count == 0)
            {
                CreaTablaImprime();
            }
            if (MyTablaImprime.Rows.Count > 0)
            {
                MyTablaImprime.Rows.Clear();
            }

            //LINEA 0
            campo += 1;
            Descripcion = "";
            Valor = "REMOLQUES ATLAS";
            PosDescX = X2; PosDescY = Y1; PosValorX = X2; PosValorY = PosDescY;
            InsertFila(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);



            //LINEA 1
            campo += 1;
            Descripcion = "Cliente:";
            Valor = folio.RazonSocial;
            PosDescY = PosDescY + 45;
            PosValorY = PosDescY;
            PosDescX = X1; PosValorX = X1 + 65; 
            InsertFila(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

            //LINEA 2
            campo += 1;
            Descripcion = "";
            PosDescX = X4; PosValorX = X4;
            Valor = Inicio.FormarBarCode(folio.idOrdenSer.ToString()) ;
            InsertFila(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 2, 1, TipoDato.Texto);

            //LINEA 3
            campo += 1;
            PosDescX = X2;
            PosDescY = PosDescY + 15;
            PosValorX = X2 + 35;
            PosValorY = PosDescY;
            Descripcion = "";
            Valor = "ORDEN DE SERVICIO";
            InsertFila(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

            //LINEA 4
            campo += 1;
            Descripcion = "";
            PosDescY = PosDescY + 15; PosValorY = PosDescY;
            PosDescX = X2; PosValorX = X2 + 40;
            //Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("NomTipOrdServ"))
            Valor = folio.NomTipOrdServ;
            InsertFila(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

            //LINEA 5
            campo += 1;
            Descripcion = "ORD.SER.";
            PosDescY = PosDescY + 15; PosValorY = PosDescY;
            PosDescX = X4; PosValorX = X4 + 80;
            //Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idOrdenSer")
            Valor = folio.idOrdenSer.ToString();
            InsertFila(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

            //LINEA 6
            campo += 1;
            Descripcion = "FECHA:";
            PosDescY = PosDescY + 15; PosValorY = PosDescY;
            PosDescX = X4; PosValorX = PosDescX + 55;
            Valor = folio.fechaCaptura.ToString("dd/MMM/yyyy") + " " + folio.fechaCaptura.ToShortTimeString();
            InsertFila(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

            //LINEA 7
            //campo += 1;
            //Descripcion = "HORA:";
            ////Valor = CDate(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("fechaCaptura")).ToShortTimeString
            //PosDescY = PosDescY + 15; PosValorY = PosDescY;
            //PosDescX = X4; PosValorX = PosDescX + 40;
            //Valor = folio.fechaCaptura.ToShortTimeString();
            //InsertFila(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

            //LINEA 8
            campo += 1;
            PosDescY = PosDescY + 30; PosValorY = PosDescY;
            //Descripcion = "TRACTOR: ";
            Descripcion = folio.NomTipoUnidad.ToUpper() + " : ";
            //Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idTractor"))
            PosDescX = X1; PosValorX = PosDescX + 90;
            Valor = folio.idUnidadTrans;
            InsertFila(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

            //LINEA 9
            campo += 1;
            PosDescY = PosDescY + 30; PosValorY = PosDescY;
            Descripcion = "";
            if (bOrdenTrab)
            {
                Valor = "REPARACIONES SOLICITADAS";
            }
            else
            {
                Valor = "DESCRIPCION DEL SERVICIO";
            }
            PosDescX = X2; PosValorX = PosDescX + 25;
            InsertFila(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

            PosDescY = PosDescY + 15; PosValorY = PosDescY;

            //LINEA 10
            //DETALLE
            foreach (var item in listaDetOS)
            {
                if (bOrdenTrab)
                {
                    if (item.NomServicio != "")
                    {
                        campo += 1;
                        PosDescY = PosDescY + 15; PosValorY = PosDescY;
                        //Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NomServicio")
                        PosDescX = X1; PosValorX =X2;
                        Descripcion = item.idOrdenTrabajo.ToString();
                        Valor = item.NomServicio;
                        InsertFila(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);
                    }
                    else
                    {
                        if (item.NotaRecepcionA != "")
                        {
                            campo += 1;
                            PosDescY = PosDescY + 15; PosValorY = PosDescY;
                            //PosDescX = X1; PosValorX = PosDescX + 25;
                            PosDescX = X1; PosValorX = X2A;
                            Descripcion = item.idOrdenTrabajo.ToString();
                            //Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NombreAct")
                            Valor = item.NotaRecepcionF;
                            InsertFila(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);
                        }
                        else
                        {
                            PosDescY = PosDescY + 15; PosValorY = PosDescY;
                        }
                    }
                    //campo += 1;
                    //Descripcion = "";
                    ////Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NotaRecepcion")
                    //PosDescX = X2; PosValorX = PosDescX + 25;
                    //Valor = item.NotaRecepcionF;
                    //InsertFila(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);
                }
                else
                {
                    campo += 1;
                    PosDescY = PosDescY + 15; PosValorY = PosDescY;
                    PosDescX = X2 - 35; PosValorX = X2 + 25;
                    //Descripcion = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("idUnidadTrans")
                    Descripcion = folio.idUnidadTrans;
                    //Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NomServicio")
                    Valor = item.NomServicio;
                    InsertFila(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);
                }
            }
            //DETALLE

            //LINEA 11
            campo += 1;
            PosDescY = PosDescY + 290; PosValorY = PosDescY;
            PosDescX = X1; PosValorX = PosDescX + 25;
            Descripcion = "";
            Valor = "___________________";
            InsertFila(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

            //LINEA 12
            campo += 1;
            Descripcion = "";
            Valor = "___________________";
            PosDescX = X3A; PosValorX = PosDescX + 25;
            InsertFila(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

            //LINEA 13
            campo += 1;
            PosDescY = PosDescY + 15; PosValorY = PosDescY;
            PosDescX = X1; PosValorX = PosDescX + 35;
            Descripcion = "";
            Valor = " ( OPERADOR ) ";
            InsertFila(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

            //LINEA 14
            campo += 1;
            PosDescX = X3A; PosValorX = PosDescX + 35;
            Descripcion = "";
            Valor = " ( AUTORIZÓ ) ";
            InsertFila(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

            //LINEA 15
            campo += 1;
            PosDescY = PosDescY + 15; PosValorY = PosDescY;
            PosDescX = X1; PosValorX = PosDescX + 25;
            Descripcion = "";
            //Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("NomOperador"))
            Valor = folio.NomOperador.ToUpper();
            InsertFila(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

            //LINEA 16
            campo += 1;
            PosDescX = X3A; PosValorX = PosDescX + 25;
            Descripcion = "";
            //Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("NomAutoriza"))
            Valor = folio.NomEmpleadoAutoriza.ToUpper();
            InsertFila(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

            //LINEA 17
            campo += 1;
            Descripcion = "";
            PosDescY = PosDescY + 75; PosValorY = PosDescY;
            PosDescX = X4; PosValorX = X4;
            //Valor = FormarBarCode(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idPadreOrdSer"))
            Valor = Inicio.FormarBarCode(folio.idOrdenSer.ToString());
            InsertFila(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 2, 1, TipoDato.Texto);

            //LINEA 18
            campo += 1;
            Descripcion = "FOLIO: ";
            PosDescY = PosDescY + 45; PosValorY = PosDescY;
            PosDescX = X4; PosValorX = X4 + 60;
            //Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idPadreOrdSer")
            Valor = folio.idPadreOrdSer.ToString(); ;
            InsertFila(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

            return MyTablaImprime;

        }
    }//
}
