﻿using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;

namespace Fletera2.Clases
{
    public class ImpremeFolioDinero
    {
        //Variables Globales
        DataColumn myDataColumn;
        DataRow myDataRow;
        DataTable MyTablaImprime = new DataTable("IMPRIME");
        int LugarNumericos = 12;

        //PROPIEDADES
        #region Propiedades
        public Font FuenteImprimeCampos { get; set; }
        public Font FuenteImprimeCampos2 { get; set; }
        public Font FuenteImprimeCampos3 { get; set; }
        public Font FuenteImprimeCampos4 { get; set; }
        public Font FuenteImprimeCampos5 { get; set; }

        public Font FuenteEtiquetas1 { get; set; }
        public Font FuenteEtiquetas2 { get; set; }

        public string NomTabla { get; set; }
        public string NomReporte { get; set; }
        public DataTable TablaImprime { get; set; }

        public Image imgLogo1 { get; set; }
        public Size TamanioimgLogo1 { get; set; }
        public Point PosicionimgLogo1 { get; set; }

        public Image imgLogo2 { get; set; }
        public Size TamanioimgLogo2 { get; set; }
        public Point PosicionimgLogo2 { get; set; }

        #endregion

        enum TipoDato
        {
            Texto = 0,
            Entero = 1,
            Numerico = 2,
            Moneda = 3,
            Fecha = 4,
            Hora = 5

        }
        private void CreaTablaImprime()
        {

            myDataColumn = new DataColumn();
            myDataColumn.DataType = System.Type.GetType("System.Int32");
            myDataColumn.ColumnName = "Campo";
            myDataColumn.ReadOnly = false;
            myDataColumn.Unique = false;
            MyTablaImprime.Columns.Add(myDataColumn);

            myDataColumn = new DataColumn();
            myDataColumn.DataType = System.Type.GetType("System.String");
            myDataColumn.ColumnName = "Descripcion";
            myDataColumn.ReadOnly = false;
            myDataColumn.Unique = false;
            MyTablaImprime.Columns.Add(myDataColumn);

            myDataColumn = new DataColumn();
            myDataColumn.DataType = System.Type.GetType("System.String");
            myDataColumn.ColumnName = "Valor";
            myDataColumn.ReadOnly = false;
            myDataColumn.Unique = false;
            MyTablaImprime.Columns.Add(myDataColumn);

            myDataColumn = new DataColumn();
            myDataColumn.DataType = System.Type.GetType("System.Single");
            myDataColumn.ColumnName = "PosDescX";
            myDataColumn.ReadOnly = false;
            myDataColumn.Unique = false;
            MyTablaImprime.Columns.Add(myDataColumn);

            myDataColumn = new DataColumn();
            myDataColumn.DataType = System.Type.GetType("System.Single");
            myDataColumn.ColumnName = "PosDescY";
            myDataColumn.ReadOnly = false;
            myDataColumn.Unique = false;
            MyTablaImprime.Columns.Add(myDataColumn);

            myDataColumn = new DataColumn();
            myDataColumn.DataType = System.Type.GetType("System.Single");
            myDataColumn.ColumnName = "PosValX";
            myDataColumn.ReadOnly = false;
            myDataColumn.Unique = false;
            MyTablaImprime.Columns.Add(myDataColumn);

            myDataColumn = new DataColumn();
            myDataColumn.DataType = System.Type.GetType("System.Single");
            myDataColumn.ColumnName = "PosValY";
            myDataColumn.ReadOnly = false;
            myDataColumn.Unique = false;
            MyTablaImprime.Columns.Add(myDataColumn);

            myDataColumn = new DataColumn();
            myDataColumn.DataType = System.Type.GetType("System.Single");
            myDataColumn.ColumnName = "Fuente";
            myDataColumn.ReadOnly = false;
            myDataColumn.Unique = false;
            MyTablaImprime.Columns.Add(myDataColumn);

            myDataColumn = new DataColumn();
            myDataColumn.DataType = System.Type.GetType("System.Int32");
            myDataColumn.ColumnName = "Lado";
            myDataColumn.ReadOnly = false;
            myDataColumn.Unique = false;
            MyTablaImprime.Columns.Add(myDataColumn);

            myDataColumn = new DataColumn();
            myDataColumn.DataType = System.Type.GetType("System.Int32");
            myDataColumn.ColumnName = "TipoDatoValor";
            myDataColumn.ReadOnly = false;
            myDataColumn.Unique = false;
            MyTablaImprime.Columns.Add(myDataColumn);

        }

        private void InsertFila(int Campo, string Descripcion, string Valor, Single PosDescX, Single PosDescY,
           Single PosValX, Single PosValY, Single vFuente, Int32 ladoImprimir, TipoDato TipoDatoValor)
        {

            myDataRow = MyTablaImprime.NewRow();
            myDataRow["Campo"] = Campo;
            myDataRow["Descripcion"] = Descripcion;
            myDataRow["Valor"] = Valor;
            myDataRow["PosDescX"] = PosDescX;
            myDataRow["PosDescY"] = PosDescY;
            myDataRow["PosValX"] = PosValX;
            myDataRow["PosValY"] = PosValY;
            myDataRow["Fuente"] = vFuente;
            myDataRow["Lado"] = ladoImprimir;
            myDataRow["TipoDatoValor"] = TipoDatoValor;
            MyTablaImprime.Rows.Add(myDataRow);

        }

        public DataTable ArmaTablaTicket(FoliosDinero folio, Personal persona )
        {
            //int Residuo = 0;
            //int maximo = 0;
            //int contCaracteres = 0;
            int campo = 0;
            string descripcion;
            string valor;
            Single PosValorX = 0;
            Single PosDescX = 0;
            Single PosValorY = 0;
            Single PosDescY = 0;

            //Single XINI = 50;
            Single XINI = 30;
            Single X1 = 50;
            Single X1B = 85;
            Single X1C = 150;
            Single X2 = 180;
            Single X2B = 170;

            Single XD1 = XINI;
            Single XD2 = XD1 + 120;
            Single XD3 = XD2 + 40;
            Single XD4 = XD3 + 40;

            //string CadenaLarga;

            if (MyTablaImprime.Columns.Count == 0)
            {
                CreaTablaImprime();
            }
            if (MyTablaImprime.Rows.Count > 0)
            {
                MyTablaImprime.Rows.Clear();
            }

            #region Encabezado
            //RAZON SOCIAL
            campo++;
            PosDescX = X1B;
            PosDescY = 50;
            PosValorX = 0;
            PosValorY = 0;
            descripcion = folio.RazonSocial.ToUpper();
            valor = "";
            InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

            //TITULO DE DOCUMENTO
            campo++;
            PosDescX = X1B;
            PosDescY = PosDescY + 30;
            PosValorX = 0;
            PosValorY = PosDescY;
            descripcion = "RECIBO DINERO";
            valor = "";
            InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

            //FOLIO EN CODIGO DE BARRAS
            campo++;
            PosDescX = X2;
            PosDescY = PosDescY + 30;
            PosValorX = PosDescX;
            PosValorY = PosDescY;
            descripcion = "";
            valor = folio.FolioDin.ToString();
            InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 2, 1, TipoDato.Texto);


            //FOLIO 
            campo++;
            PosDescX = X2;
            PosDescY = PosDescY + 30;
            PosValorX = PosDescX + 30;
            PosValorY = PosDescY;
            descripcion = "Folio";
            valor = folio.FolioDin.ToString();
            InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

            //FECHA
            campo++;
            PosDescX = 130;
            PosDescY = PosDescY + 15;
            PosValorX = PosDescX + 40;
            PosValorY = PosDescY;
            descripcion = "Fecha: ";
            valor = folio.Fecha.ToString("dd/MMM/yyyy") + " " + folio.Fecha.ToShortTimeString();
            InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

            //DIRECCION2
            campo++;
            PosDescX = X1B;
            PosDescY = PosDescY + 30;
            PosValorX = PosDescX;
            PosValorY = PosDescY;
            descripcion = "";
            valor = folio.DescripcionFolDin;
            InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

            //DIRECCION3
            campo++;
            PosDescX = X1C;
            PosDescY = PosDescY + 30;
            PosValorX = PosDescX+70;
            PosValorY = PosDescY;
            descripcion = "BUENO POR:";
            valor = folio.Importe.ToString("C2");
            InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);


            //PUNTO DE VENTA
            campo++;
            PosDescX = XINI;
            PosDescY = PosDescY + 15;
            PosValorX = PosDescX+50;
            PosValorY = PosDescY;
            descripcion = "En Letras ";
            NumeroALetras numlet = new NumeroALetras();
            valor = "SON : " + numlet.Convertir(Math.Abs(Math.Round((folio.Importe), 2)).ToString(), true);
            InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

            //NOTA DE VENTA
            campo++;
            PosDescX = XINI;
            PosDescY = PosDescY + 15;
            PosValorX = PosDescX + 40;
            PosValorY = PosDescY;
            descripcion = "Entrega:";
            valor = folio.NombreEnt;
            InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

            campo++;
            PosDescX = XINI;
            PosDescY = PosDescY + 45;
            PosValorX = PosDescX+ 120;
            PosValorY = PosDescY;
            descripcion = "Recibe";
            valor = "__________________";
            InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

            campo++;
            //PosDescX = 0;
            PosDescY = PosDescY + 15;
            PosValorX = 150;
            PosValorY = PosDescY;
            //descripcion = "Nombre";
            descripcion = "";
            valor = folio.NombreRec;
            InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

            #endregion





            

            //FIN
            /////////////////////////////////////////////////////////////////////////////////////
            return MyTablaImprime;
        }

        public Single ImprimeLinea(System.Drawing.Printing.PrintPageEventArgs e, int Indice, int LadoImprime)
        {
            Single leftMargin = e.MarginBounds.Left;

            Single PosValorX = 0;
            Single PosDescX = 0;
            Single PosValorY = 0;
            Single PosDescY = 0;
            Single FuenteCodBar = 0;

            string Descripcion;
            string Valor;
            int Lado;
            TipoDato TipoDatoValor;
            String Cadena = "";
            /////////////////////////////////////////////////////////////////////////////////////



            //IMAGENES
            /////////////////////////////////////////////////////////////////////////////////////
            for (int i = 0; i < TablaImprime.Rows.Count; i++)
            {
                Descripcion = TablaImprime.Rows[i]["Descripcion"].ToString();
                PosValorX = Convert.ToSingle(TablaImprime.Rows[i]["PosValX"]);
                PosDescX = Convert.ToSingle(TablaImprime.Rows[i]["PosDescX"]);
                PosValorY = Convert.ToSingle(TablaImprime.Rows[i]["PosValY"]);
                PosDescY = Convert.ToSingle(TablaImprime.Rows[i]["PosDescY"]);
                FuenteCodBar = Convert.ToSingle(TablaImprime.Rows[i]["Fuente"]);
                Lado = Convert.ToInt32(TablaImprime.Rows[i]["Lado"]);

                // Creates a pen that draws in red.
                //Pen myPen;
                // Creates a brush that fills in solid blue.
                SolidBrush myBrush;
                myBrush = new SolidBrush(Color.Black);

                if (LadoImprime == Lado)
                {
                    if (FuenteCodBar == 1)
                    {
                        e.Graphics.DrawString(Descripcion, FuenteImprimeCampos, myBrush, PosDescX, PosDescY);
                    }
                    else if (FuenteCodBar == 2)
                    {
                        e.Graphics.DrawString(Descripcion, FuenteImprimeCampos2, myBrush, PosDescX, PosDescY);
                    }
                    else if (FuenteCodBar == 3)
                    {
                        e.Graphics.DrawString(Descripcion, FuenteImprimeCampos3, myBrush, PosDescX, PosDescY);
                    }
                    else if (FuenteCodBar == 4)
                    {
                        e.Graphics.DrawString(Descripcion, FuenteImprimeCampos4, myBrush, PosDescX, PosDescY);
                    }
                    else if (FuenteCodBar == 5)
                    {
                        e.Graphics.DrawString(Descripcion, FuenteImprimeCampos5, myBrush, PosDescX, PosDescY);
                    }


                    Valor = TablaImprime.Rows[i]["Valor"].ToString();
                    PosValorX = Convert.ToSingle(TablaImprime.Rows[i]["PosValX"]);
                    PosValorY = Convert.ToSingle(TablaImprime.Rows[i]["PosValY"]);
                    TipoDatoValor = (TipoDato)TablaImprime.Rows[i]["TipoDatoValor"];

                    if (FuenteCodBar == 1)
                    {

                        switch (TipoDatoValor)
                        {
                            case TipoDato.Texto:
                                e.Graphics.DrawString(Valor, FuenteImprimeCampos, Brushes.Black, PosValorX, PosValorY);
                                break;
                            case TipoDato.Entero:
                                e.Graphics.DrawString(Valor, FuenteImprimeCampos, Brushes.Black, PosValorX, PosValorY);
                                break;
                            case TipoDato.Numerico:
                                decimal vNumero = Convert.ToDecimal(TablaImprime.Rows[i]["Valor"].ToString());
                                Cadena = string.Format("{0:###,###0.00}", vNumero); //1.234,35 €
                                e.Graphics.DrawString((Cadena.PadLeft(LugarNumericos)), FuenteImprimeCampos, Brushes.Black, PosValorX, PosValorY);
                                break;
                            case TipoDato.Moneda:
                                decimal vMoney = Convert.ToDecimal(TablaImprime.Rows[i]["Valor"].ToString());
                                Cadena = string.Format("{0:c2}", vMoney); //1.234,35 €
                                e.Graphics.DrawString((Cadena.PadLeft(LugarNumericos)), FuenteImprimeCampos, Brushes.Black, PosValorX, PosValorY);
                                break;
                            case TipoDato.Fecha:
                                break;
                            case TipoDato.Hora:
                                break;
                            default:
                                break;
                        }
                    }
                    else if (FuenteCodBar == 2)
                    {
                        //CODIGO DE BARRAS
                        e.Graphics.DrawString(Valor, FuenteImprimeCampos2, Brushes.Black, PosValorX, PosValorY);

                    }
                    else if (FuenteCodBar == 3)
                    {

                        switch (TipoDatoValor)
                        {
                            case TipoDato.Texto:
                                e.Graphics.DrawString(Valor, FuenteImprimeCampos3, Brushes.Black, PosValorX, PosValorY);
                                break;
                            case TipoDato.Entero:
                                e.Graphics.DrawString(Valor, FuenteImprimeCampos3, Brushes.Black, PosValorX, PosValorY);
                                break;
                            case TipoDato.Numerico:
                                decimal vNumero = Convert.ToDecimal(TablaImprime.Rows[i]["Valor"].ToString());
                                Cadena = string.Format("{0:###,###0.00}", vNumero); //1.234,35 €
                                e.Graphics.DrawString((Cadena.PadLeft(LugarNumericos)), FuenteImprimeCampos3, Brushes.Black, PosValorX, PosValorY);

                                break;
                            case TipoDato.Moneda:
                                decimal vMoney = Convert.ToDecimal(TablaImprime.Rows[i]["Valor"].ToString());
                                Cadena = string.Format("{0:c2}", vMoney); //1.234,35 €
                                e.Graphics.DrawString((Cadena.PadLeft(LugarNumericos)), FuenteImprimeCampos3, Brushes.Black, PosValorX, PosValorY);

                                break;
                            case TipoDato.Fecha:
                                break;
                            case TipoDato.Hora:
                                break;
                            default:
                                break;
                        }
                    }
                    else if (FuenteCodBar == 4)
                    {

                        switch (TipoDatoValor)
                        {
                            case TipoDato.Texto:
                                e.Graphics.DrawString(Valor, FuenteImprimeCampos4, Brushes.Black, PosValorX, PosValorY);
                                break;
                            case TipoDato.Entero:
                                e.Graphics.DrawString(Valor, FuenteImprimeCampos4, Brushes.Black, PosValorX, PosValorY);
                                break;
                            case TipoDato.Numerico:
                                decimal vNumero = Convert.ToDecimal(TablaImprime.Rows[i]["Valor"].ToString());
                                Cadena = string.Format("{0:###,###0.00}", vNumero); //1.234,35 €
                                e.Graphics.DrawString((Cadena.PadLeft(LugarNumericos)), FuenteImprimeCampos4, Brushes.Black, PosValorX, PosValorY);

                                break;
                            case TipoDato.Moneda:
                                decimal vMoney = Convert.ToDecimal(TablaImprime.Rows[i]["Valor"].ToString());
                                Cadena = string.Format("{0:c2}", vMoney); //1.234,35 €
                                e.Graphics.DrawString((Cadena.PadLeft(LugarNumericos)), FuenteImprimeCampos4, Brushes.Black, PosValorX, PosValorY);

                                break;
                            case TipoDato.Fecha:
                                break;
                            case TipoDato.Hora:
                                break;
                            default:
                                break;
                        }
                    }
                    else if (FuenteCodBar == 5)
                    {

                        switch (TipoDatoValor)
                        {
                            case TipoDato.Texto:
                                e.Graphics.DrawString(Valor, FuenteImprimeCampos5, Brushes.Black, PosValorX, PosValorY);
                                break;
                            case TipoDato.Entero:
                                e.Graphics.DrawString(Valor, FuenteImprimeCampos5, Brushes.Black, PosValorX, PosValorY);
                                break;
                            case TipoDato.Numerico:
                                decimal vNumero = Convert.ToDecimal(TablaImprime.Rows[i]["Valor"].ToString());
                                Cadena = string.Format("{0:###,###0.00}", vNumero); //1.234,35 €
                                e.Graphics.DrawString((Cadena.PadLeft(LugarNumericos)), FuenteImprimeCampos5, Brushes.Black, PosValorX, PosValorY);

                                break;
                            case TipoDato.Moneda:
                                decimal vMoney = Convert.ToDecimal(TablaImprime.Rows[i]["Valor"].ToString());
                                Cadena = string.Format("{0:c2}", vMoney); //1.234,35 €
                                e.Graphics.DrawString((Cadena.PadLeft(LugarNumericos)), FuenteImprimeCampos5, Brushes.Black, PosValorX, PosValorY);

                                break;
                            case TipoDato.Fecha:
                                break;
                            case TipoDato.Hora:
                                break;
                            default:
                                break;
                        }
                    }

                }


            }
            return PosDescX;

            /////////////////////////////////////////////////////////////////////////////////////
        }

        public Single ImprimeLogo(System.Drawing.Printing.PrintPageEventArgs e, Single yPos)
        {
            //IMAGENES
            e.Graphics.DrawImage(imgLogo1, PosicionimgLogo1.X, PosicionimgLogo1.Y, TamanioimgLogo1.Width, TamanioimgLogo1.Height);
            if (imgLogo2 != null)
            {
                e.Graphics.DrawImage(imgLogo2, PosicionimgLogo2.X, PosicionimgLogo2.Y, TamanioimgLogo2.Width, TamanioimgLogo2.Height);
            }

            yPos += FuenteImprimeCampos.GetHeight(e.Graphics);
            //e.Graphics.DrawImage(vImagen1, vPosicionImagen1)
            //yPos += vFuenteEncabezado.GetHeight(e.Graphics)
            return yPos;
        }
    }//
}
