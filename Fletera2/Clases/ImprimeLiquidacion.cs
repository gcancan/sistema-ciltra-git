﻿using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2.Clases
{
    public class ImprimeLiquidacion
    {
        //Variables Globales
        DataColumn myDataColumn;
        DataRow myDataRow;
        DataTable MyTablaImprime = new DataTable("IMPRIME");
        int LugarNumericos = 12;

        //PROPIEDADES
        #region Propiedades
        public Font FuenteImprimeCampos { get; set; }
        public Font FuenteImprimeCampos2 { get; set; }
        public Font FuenteImprimeCampos3 { get; set; }
        public Font FuenteImprimeCampos4 { get; set; }
        public Font FuenteImprimeCampos5 { get; set; }

        public Font FuenteEtiquetas1 { get; set; }
        public Font FuenteEtiquetas2 { get; set; }

        public string NomTabla { get; set; }
        public string NomReporte { get; set; }
        public DataTable TablaImprime { get; set; }

        public Image imgLogo1 { get; set; }
        public Size TamanioimgLogo1 { get; set; }
        public Point PosicionimgLogo1 { get; set; }

        public Image imgLogo2 { get; set; }
        public Size TamanioimgLogo2 { get; set; }
        public Point PosicionimgLogo2 { get; set; }

        #endregion

        enum TipoDato
        {
            Texto = 0,
            Entero = 1,
            Numerico = 2,
            Moneda = 3,
            Fecha = 4,
            Hora = 5

        }
        private void CreaTablaImprime()
        {

            myDataColumn = new DataColumn();
            myDataColumn.DataType = System.Type.GetType("System.Int32");
            myDataColumn.ColumnName = "Campo";
            myDataColumn.ReadOnly = false;
            myDataColumn.Unique = false;
            MyTablaImprime.Columns.Add(myDataColumn);

            myDataColumn = new DataColumn();
            myDataColumn.DataType = System.Type.GetType("System.String");
            myDataColumn.ColumnName = "Descripcion";
            myDataColumn.ReadOnly = false;
            myDataColumn.Unique = false;
            MyTablaImprime.Columns.Add(myDataColumn);

            myDataColumn = new DataColumn();
            myDataColumn.DataType = System.Type.GetType("System.String");
            myDataColumn.ColumnName = "Valor";
            myDataColumn.ReadOnly = false;
            myDataColumn.Unique = false;
            MyTablaImprime.Columns.Add(myDataColumn);

            myDataColumn = new DataColumn();
            myDataColumn.DataType = System.Type.GetType("System.Single");
            myDataColumn.ColumnName = "PosDescX";
            myDataColumn.ReadOnly = false;
            myDataColumn.Unique = false;
            MyTablaImprime.Columns.Add(myDataColumn);

            myDataColumn = new DataColumn();
            myDataColumn.DataType = System.Type.GetType("System.Single");
            myDataColumn.ColumnName = "PosDescY";
            myDataColumn.ReadOnly = false;
            myDataColumn.Unique = false;
            MyTablaImprime.Columns.Add(myDataColumn);

            myDataColumn = new DataColumn();
            myDataColumn.DataType = System.Type.GetType("System.Single");
            myDataColumn.ColumnName = "PosValX";
            myDataColumn.ReadOnly = false;
            myDataColumn.Unique = false;
            MyTablaImprime.Columns.Add(myDataColumn);

            myDataColumn = new DataColumn();
            myDataColumn.DataType = System.Type.GetType("System.Single");
            myDataColumn.ColumnName = "PosValY";
            myDataColumn.ReadOnly = false;
            myDataColumn.Unique = false;
            MyTablaImprime.Columns.Add(myDataColumn);

            myDataColumn = new DataColumn();
            myDataColumn.DataType = System.Type.GetType("System.Single");
            myDataColumn.ColumnName = "Fuente";
            myDataColumn.ReadOnly = false;
            myDataColumn.Unique = false;
            MyTablaImprime.Columns.Add(myDataColumn);

            myDataColumn = new DataColumn();
            myDataColumn.DataType = System.Type.GetType("System.Int32");
            myDataColumn.ColumnName = "Lado";
            myDataColumn.ReadOnly = false;
            myDataColumn.Unique = false;
            MyTablaImprime.Columns.Add(myDataColumn);

            myDataColumn = new DataColumn();
            myDataColumn.DataType = System.Type.GetType("System.Int32");
            myDataColumn.ColumnName = "TipoDatoValor";
            myDataColumn.ReadOnly = false;
            myDataColumn.Unique = false;
            MyTablaImprime.Columns.Add(myDataColumn);

        }

        private void InsertFila(int Campo, string Descripcion, string Valor, Single PosDescX, Single PosDescY,
           Single PosValX, Single PosValY, Single vFuente, Int32 ladoImprimir, TipoDato TipoDatoValor)
        {

            myDataRow = MyTablaImprime.NewRow();
            myDataRow["Campo"] = Campo;
            myDataRow["Descripcion"] = Descripcion;
            myDataRow["Valor"] = Valor;
            myDataRow["PosDescX"] = PosDescX;
            myDataRow["PosDescY"] = PosDescY;
            myDataRow["PosValX"] = PosValX;
            myDataRow["PosValY"] = PosValY;
            myDataRow["Fuente"] = vFuente;
            myDataRow["Lado"] = ladoImprimir;
            myDataRow["TipoDatoValor"] = TipoDatoValor;
            MyTablaImprime.Rows.Add(myDataRow);

        }
        public DataTable ArmaTablaImprime(CabLiquidaciones cabLiq, List<DetLiqViaRep> detliqvia,
            List<DetLiqFolDinRep> detliqfd, List<DetLiqComGasRep> detliqcg,
            List<DetLiqOtrConRep> detliqoc)
        {
            //Variables
            int campo = 0;
            Single PosDescX = 0;
            Single PosDescY = 0;

            Single PosValorX = 0;
            Single PosValorY = 0;

            Single X1 = 30;
            Single X2 = X1+50 ;
            Single X3 = X2 + 50;
            Single X4 = X3 + 50;
            Single X5 = X4 + 50;
            Single X6 = X5 + 50;
            Single X7 = X6 + 50;
            Single X8 = X7 + 50;
            Single X9 = X8 + 50;
            Single X10 = X9 + 50;
            Single X11 = X10 + 50;
            Single X12 = X11 + 50;

            Single XD1 = 0;
            Single XD2 = 0;
            Single XD3 = 0;
            Single XD4 = 0;
            Single XD5 = 0;
            Single XD6 = 0;

            string descripcion;
            string valor;

            decimal TotalVia;
            decimal TotalCompgas;
            decimal Total;


            if (MyTablaImprime.Columns.Count == 0)
            {
                CreaTablaImprime();
            }
            if (MyTablaImprime.Rows.Count > 0)
            {
                MyTablaImprime.Rows.Clear();
            }

            #region Encabezado
            //Empresa
            campo++;
            PosDescX = X7;
            PosDescY = 100;
            PosValorX = PosDescX;
            PosValorY = PosDescY;
            descripcion = "";
            valor = cabLiq.RazonSocial.ToUpper();
            InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

            //Liquidacion No.
            //codigo de barras
            campo++;
            PosDescX = X10;
            PosDescY = PosDescY+30;
            PosValorX = PosDescX + 80;
            PosValorY = PosDescY;
            descripcion = "";
            valor = cabLiq.idLiquidacion.ToString();
            InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 2, 1, TipoDato.Texto);

            campo++;
            PosDescX = X10;
            PosDescY = PosDescY + 30;
            PosValorX = PosDescX + 80;
            PosValorY = PosDescY;
            descripcion = "Liquidacion No.";
            valor = cabLiq.idLiquidacion.ToString();
            InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);


            //Perido
            campo++;
            PosDescX = X1;
            PosDescY = PosDescY+ 30;
            PosValorX = PosDescX + 63;
            PosValorY = PosDescY;
            descripcion = "Periodo: DE ";
            valor = cabLiq.FechaIni.ToString("dd/MMMM/yyyy").ToUpper() + " A " + cabLiq.FechaFin.ToString("dd/MMMM/yyyy").ToUpper();
            InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

            //FECHA
            campo++;
            PosDescX = X10;
            //PosDescY = 100;
            PosValorX = PosDescX + 100;
            PosValorY = PosDescY;
            descripcion = "Fecha Elaboración: ";
            valor = cabLiq.Fecha.ToString("dd/MMMM/yyyy HH:mm:ss").ToUpper();
            InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

            //Operador
            campo++;
            PosDescX = X1;
            PosDescY = PosDescY + 15;
            PosValorX = PosDescX + 50;
            PosValorY = PosDescY;
            descripcion = "Operador ";
            valor = cabLiq.idOperador.ToString() + " - " + cabLiq.NomOperador;  
            InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

            //FECHA
            campo++;
            PosDescX = X10;
            //PosDescY = 100;
            PosValorX = PosDescX + 100;
            PosValorY = PosDescY;
            descripcion = "Fecha Impresión: ";
            valor = DateTime.Now.ToString("dd/MMMM/yyyy HH:mm:ss").ToUpper();
            InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);


            //Estatus
            campo++;
            PosDescX = 10;
            PosDescY = PosDescY + 15;
            PosValorX = PosDescX + 30;
            PosValorY = PosDescY;
            descripcion = "Estatus ";
            valor = cabLiq.Estatus;
            InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);
            #endregion
            
            #region Detalles
            //Detalle de Viajes
            if (detliqvia.Count > 0)
            {
                XD1 = X1; //NoViaje
                XD2 = XD1+ 50; //Ruta
                XD3 = XD2 + 250;//FEcha Ini
                XD4 = XD3 + 70;//Fecha Fin
                XD5 = XD4 + 70;//Unidades
                XD6 = XD5 + 100;//Maniobras

                //Encabezado Viajes
                //NoViaje
                campo++;
                PosDescX = XD1;
                PosDescY = PosDescY + 45;
                PosValorX = PosDescX;
                PosValorY = PosDescY;
                descripcion = "No.Viaje";
                valor = "";
                InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

                //Ruta
                campo++;
                PosDescX = XD2;
                //PosDescY = PosDescY + 45;
                PosValorX = PosDescX;
                PosValorY = PosDescY;
                descripcion = "Ruta";
                valor = "";
                InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

                //Fecha Ini
                campo++;
                PosDescX = XD3;
                //PosDescY = PosDescY + 45;
                PosValorX = PosDescX;
                PosValorY = PosDescY;
                descripcion = "Fecha Ini.";
                valor = "";
                InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

                //Fecha Fin
                campo++;
                PosDescX = XD4;
                //PosDescY = PosDescY + 45;
                PosValorX = PosDescX;
                PosValorY = PosDescY;
                descripcion = "Fecha Fin";
                valor = "";
                InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

                //Unidades
                campo++;
                PosDescX = XD5;
                //PosDescY = PosDescY + 45;
                PosValorX = PosDescX;
                PosValorY = PosDescY;
                descripcion = "Unidades";
                valor = "";
                InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

                //Maniobras
                campo++;
                PosDescX = XD6;
                //PosDescY = PosDescY + 45;
                PosValorX = PosDescX;
                PosValorY = PosDescY;
                descripcion = "Maniobras";
                valor = "";
                InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

                TotalVia = 0;
                //DATOS DEL DETALLE DE VENTAS
                foreach (var item in detliqvia)
                {
                    //NoViaje
                    campo++;
                    PosDescX = XD1;
                    PosDescY = PosDescY + 15;
                    PosValorX = PosDescX;
                    PosValorY = PosDescY;
                    descripcion = "";
                    valor = item.NumViaje.ToString();
                    InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

                    //Ruta
                    campo++;
                    PosDescX = XD2;
                    //PosDescY = PosDescY + 45;
                    PosValorX = PosDescX;
                    PosValorY = PosDescY;
                    //descripcion = "Ruta";
                    descripcion = "";
                    valor = item.Ruta;
                    InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

                    //Fecha Ini
                    campo++;
                    PosDescX = XD3;
                    //PosDescY = PosDescY + 45;
                    PosValorX = PosDescX;
                    PosValorY = PosDescY;
                    //descripcion = "Fecha Ini.";
                    descripcion = "";
                    valor = item.FecSal.ToShortDateString();
                    InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

                    //Fecha Fin
                    campo++;
                    PosDescX = XD4;
                    //PosDescY = PosDescY + 45;
                    PosValorX = PosDescX;
                    PosValorY = PosDescY;
                    //descripcion = "Fecha Fin";
                    descripcion = "";
                    valor = item.FecFin.ToShortDateString();
                    InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

                    //Unidades
                    campo++;
                    PosDescX = XD5;
                    //PosDescY = PosDescY + 45;
                    PosValorX = PosDescX;
                    PosValorY = PosDescY;
                    descripcion = "";
                    valor = item.idTractor + " , " + item.idRemolque1;
                    InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

                    //Maniobras
                    campo++;
                    PosDescX = XD6;
                    //PosDescY = PosDescY + 45;
                    PosValorX = PosDescX;
                    PosValorY = PosDescY;
                    descripcion = "";
                    valor = item.Importe.ToString("C2");
                    TotalVia = TotalVia + item.Importe;
                    InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);
                }
                //TOTAL DE VENTAS
                //Maniobras
                campo++;
                PosDescX = XD5;
                PosDescY = PosDescY + 30;
                PosValorX = XD6;
                PosValorY = PosDescY;
                descripcion = "TOTAL";
                valor = TotalVia.ToString("C2");
                InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

                //Detalle de Viajes
                //GASTOS
                if (detliqfd.Count > 0)
                {
                    //Encabezados
                    XD1 = X1; //Gastos
                    XD2 = XD1 + 330; //Cantidad
                    XD3 = XD2 + 70;//Importe
                    XD4 = XD3 + 70;//IVA
                    XD5 = XD4 + 70;//Total

                    campo++;
                    PosDescX = XD1;
                    PosDescY = PosDescY + 30;
                    PosValorX = PosDescX;
                    PosValorY = PosDescY;
                    descripcion = "Concepto";
                    valor = "";
                    InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

                    //Ruta
                    campo++;
                    PosDescX = XD2;
                    //PosDescY = PosDescY + 45;
                    PosValorX = PosDescX;
                    PosValorY = PosDescY;
                    descripcion = "Cantidad";
                    valor = "";
                    InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

                    //Fecha Ini
                    campo++;
                    PosDescX = XD3;
                    //PosDescY = PosDescY + 45;
                    PosValorX = PosDescX;
                    PosValorY = PosDescY;
                    descripcion = "Importe";
                    valor = "";
                    InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

                    //Fecha Fin
                    campo++;
                    PosDescX = XD4;
                    //PosDescY = PosDescY + 45;
                    PosValorX = PosDescX;
                    PosValorY = PosDescY;
                    descripcion = "I.V.A.";
                    valor = "";
                    InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

                    //Unidades
                    campo++;
                    PosDescX = XD5;
                    //PosDescY = PosDescY + 45;
                    PosValorX = PosDescX;
                    PosValorY = PosDescY;
                    descripcion = "Total";
                    valor = "";
                    InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

                    TotalCompgas = 0;
                    //DEtalles
                    foreach (var item in detliqfd)
                    {
                        campo++;
                        PosDescX = XD1;
                        PosDescY = PosDescY + 15;
                        PosValorX = PosDescX;
                        PosValorY = PosDescY;
                        //descripcion = "Concepto";
                        descripcion = "";
                        valor = item.Descripcion;
                        InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

                        //Ruta
                        campo++;
                        PosDescX = XD2;
                        //PosDescY = PosDescY + 45;
                        PosValorX = PosDescX;
                        PosValorY = PosDescY;
                        //descripcion = "Cantidad";
                        valor = "1";
                        InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

                        //Fecha Ini
                        campo++;
                        PosDescX = XD3;
                        //PosDescY = PosDescY + 45;
                        PosValorX = PosDescX;
                        PosValorY = PosDescY;
                        //descripcion = "Importe";
                        valor = item.Importe.ToString("C2");
                        InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

                        //Fecha Fin
                        campo++;
                        PosDescX = XD4;
                        //PosDescY = PosDescY + 45;
                        PosValorX = PosDescX;
                        PosValorY = PosDescY;
                        //descripcion = "I.V.A.";
                        valor = "0";
                        InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

                        //Unidades
                        campo++;
                        PosDescX = XD5;
                        //PosDescY = PosDescY + 45;
                        PosValorX = PosDescX;
                        PosValorY = PosDescY;
                        //descripcion = "Total";
                        valor = item.Importe.ToString("C2");
                        TotalCompgas = TotalCompgas + item.Importe;
                        InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);
                    }
                    foreach (var item in detliqcg)
                    {
                        campo++;
                        PosDescX = XD1;
                        PosDescY = PosDescY + 15;
                        PosValorX = PosDescX;
                        PosValorY = PosDescY;
                        //descripcion = "Concepto";
                        descripcion = "";
                        valor = item.NomGasto;
                        InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

                        //Ruta
                        campo++;
                        PosDescX = XD2;
                        //PosDescY = PosDescY + 45;
                        PosValorX = PosDescX;
                        PosValorY = PosDescY;
                        //descripcion = "Cantidad";
                        valor = "1";
                        InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

                        //Fecha Ini
                        campo++;
                        PosDescX = XD3;
                        //PosDescY = PosDescY + 45;
                        PosValorX = PosDescX;
                        PosValorY = PosDescY;
                        //descripcion = "Importe";
                        valor = item.Importe.ToString("C2");
                        InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

                        //Fecha Fin
                        campo++;
                        PosDescX = XD4;
                        //PosDescY = PosDescY + 45;
                        PosValorX = PosDescX;
                        PosValorY = PosDescY;
                        //descripcion = "I.V.A.";
                        valor = item.iva.ToString("C2");
                        InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

                        //Unidades
                        campo++;
                        PosDescX = XD5;
                        //PosDescY = PosDescY + 45;
                        PosValorX = PosDescX;
                        PosValorY = PosDescY;
                        //descripcion = "Total";
                        valor = (item.Importe + item.iva).ToString("C2");
                        TotalCompgas = TotalCompgas - (item.Importe + item.iva);
                        InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);
                    }
                    foreach (var item in detliqoc)
                    {
                        campo++;
                        PosDescX = XD1;
                        PosDescY = PosDescY + 15;
                        PosValorX = PosDescX;
                        PosValorY = PosDescY;
                        //descripcion = "Concepto";
                        descripcion = "";
                        valor = item.Descripcion;
                        InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

                        //Ruta
                        campo++;
                        PosDescX = XD2;
                        //PosDescY = PosDescY + 45;
                        PosValorX = PosDescX;
                        PosValorY = PosDescY;
                        //descripcion = "Cantidad";
                        valor = "1";
                        InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

                        //Fecha Ini
                        campo++;
                        PosDescX = XD3;
                        //PosDescY = PosDescY + 45;
                        PosValorX = PosDescX;
                        PosValorY = PosDescY;
                        //descripcion = "Importe";
                        valor = item.Importe.ToString("C2");
                        InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

                        //Fecha Fin
                        campo++;
                        PosDescX = XD4;
                        //PosDescY = PosDescY + 45;
                        PosValorX = PosDescX;
                        PosValorY = PosDescY;
                        //descripcion = "I.V.A.";
                        valor = item.iva.ToString("C2");
                        InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

                        //Unidades
                        campo++;
                        PosDescX = XD5;
                        //PosDescY = PosDescY + 45;
                        PosValorX = PosDescX;
                        PosValorY = PosDescY;
                        //descripcion = "Total";
                        valor = (item.Importe + item.iva).ToString("C2");
                        if (item.EsCargo)
                        {
                            TotalCompgas = TotalCompgas - ((item.Importe + item.iva) * -1);
                        }
                        else
                        {
                            TotalCompgas = TotalCompgas - ((item.Importe + item.iva));
                        }
                        
                        InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);
                    }

                    //TOTAL DE VENTAS
                    //Maniobras
                    campo++;
                    PosDescX = XD4;
                    PosDescY = PosDescY + 30;
                    PosValorX = XD5;
                    PosValorY = PosDescY;
                    descripcion = "TOTAL";
                    valor = TotalCompgas.ToString("C2");
                    InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

                    campo++;
                    PosDescX = XD3;
                    PosDescY = PosDescY + 15;
                    PosValorX = XD5;
                    PosValorY = PosDescY;
                    descripcion = "TOTAL A PAGAR";
                    valor = (TotalVia-  TotalCompgas).ToString("C2");
                    InsertFila(campo, descripcion, valor, PosDescX, PosDescY, PosValorX, PosValorY, 1, 1, TipoDato.Texto);

                }
            }
            #endregion
            #region Resumen

            #endregion
            return MyTablaImprime;
        }
        public Single ImprimeLinea(System.Drawing.Printing.PrintPageEventArgs e, int Indice, int LadoImprime)
        {
            Single leftMargin = e.MarginBounds.Left;

            Single PosValorX = 0;
            Single PosDescX = 0;
            Single PosValorY = 0;
            Single PosDescY = 0;
            Single FuenteCodBar = 0;

            string Descripcion;
            string Valor;
            int Lado;
            TipoDato TipoDatoValor;
            String Cadena = "";
            /////////////////////////////////////////////////////////////////////////////////////



            //IMAGENES
            /////////////////////////////////////////////////////////////////////////////////////
            for (int i = 0; i < TablaImprime.Rows.Count; i++)
            {
                Descripcion = TablaImprime.Rows[i]["Descripcion"].ToString();
                PosValorX = Convert.ToSingle(TablaImprime.Rows[i]["PosValX"]);
                PosDescX = Convert.ToSingle(TablaImprime.Rows[i]["PosDescX"]);
                PosValorY = Convert.ToSingle(TablaImprime.Rows[i]["PosValY"]);
                PosDescY = Convert.ToSingle(TablaImprime.Rows[i]["PosDescY"]);
                FuenteCodBar = Convert.ToSingle(TablaImprime.Rows[i]["Fuente"]);
                Lado = Convert.ToInt32(TablaImprime.Rows[i]["Lado"]);

                // Creates a pen that draws in red.
                //Pen myPen;
                // Creates a brush that fills in solid blue.
                SolidBrush myBrush;
                myBrush = new SolidBrush(Color.Black);

                if (LadoImprime == Lado)
                {
                    if (FuenteCodBar == 1)
                    {
                        e.Graphics.DrawString(Descripcion, FuenteImprimeCampos, myBrush, PosDescX, PosDescY);
                    }
                    else if (FuenteCodBar == 2)
                    {
                        e.Graphics.DrawString(Descripcion, FuenteImprimeCampos2, myBrush, PosDescX, PosDescY);
                    }
                    else if (FuenteCodBar == 3)
                    {
                        e.Graphics.DrawString(Descripcion, FuenteImprimeCampos3, myBrush, PosDescX, PosDescY);
                    }
                    else if (FuenteCodBar == 4)
                    {
                        e.Graphics.DrawString(Descripcion, FuenteImprimeCampos4, myBrush, PosDescX, PosDescY);
                    }
                    else if (FuenteCodBar == 5)
                    {
                        e.Graphics.DrawString(Descripcion, FuenteImprimeCampos5, myBrush, PosDescX, PosDescY);
                    }


                    Valor = TablaImprime.Rows[i]["Valor"].ToString();
                    PosValorX = Convert.ToSingle(TablaImprime.Rows[i]["PosValX"]);
                    PosValorY = Convert.ToSingle(TablaImprime.Rows[i]["PosValY"]);
                    TipoDatoValor = (TipoDato)TablaImprime.Rows[i]["TipoDatoValor"];

                    if (FuenteCodBar == 1)
                    {

                        switch (TipoDatoValor)
                        {
                            case TipoDato.Texto:
                                e.Graphics.DrawString(Valor, FuenteImprimeCampos, Brushes.Black, PosValorX, PosValorY);
                                break;
                            case TipoDato.Entero:
                                e.Graphics.DrawString(Valor, FuenteImprimeCampos, Brushes.Black, PosValorX, PosValorY);
                                break;
                            case TipoDato.Numerico:
                                decimal vNumero = Convert.ToDecimal(TablaImprime.Rows[i]["Valor"].ToString());
                                Cadena = string.Format("{0:###,###0.00}", vNumero); //1.234,35 €
                                e.Graphics.DrawString((Cadena.PadLeft(LugarNumericos)), FuenteImprimeCampos, Brushes.Black, PosValorX, PosValorY);
                                break;
                            case TipoDato.Moneda:
                                decimal vMoney = Convert.ToDecimal(TablaImprime.Rows[i]["Valor"].ToString());
                                Cadena = string.Format("{0:c2}", vMoney); //1.234,35 €
                                e.Graphics.DrawString((Cadena.PadLeft(LugarNumericos)), FuenteImprimeCampos, Brushes.Black, PosValorX, PosValorY);
                                break;
                            case TipoDato.Fecha:
                                break;
                            case TipoDato.Hora:
                                break;
                            default:
                                break;
                        }
                    }
                    else if (FuenteCodBar == 2)
                    {
                        //CODIGO DE BARRAS
                        e.Graphics.DrawString(Valor, FuenteImprimeCampos2, Brushes.Black, PosValorX, PosValorY);

                    }
                    else if (FuenteCodBar == 3)
                    {

                        switch (TipoDatoValor)
                        {
                            case TipoDato.Texto:
                                e.Graphics.DrawString(Valor, FuenteImprimeCampos3, Brushes.Black, PosValorX, PosValorY);
                                break;
                            case TipoDato.Entero:
                                e.Graphics.DrawString(Valor, FuenteImprimeCampos3, Brushes.Black, PosValorX, PosValorY);
                                break;
                            case TipoDato.Numerico:
                                decimal vNumero = Convert.ToDecimal(TablaImprime.Rows[i]["Valor"].ToString());
                                Cadena = string.Format("{0:###,###0.00}", vNumero); //1.234,35 €
                                e.Graphics.DrawString((Cadena.PadLeft(LugarNumericos)), FuenteImprimeCampos3, Brushes.Black, PosValorX, PosValorY);

                                break;
                            case TipoDato.Moneda:
                                decimal vMoney = Convert.ToDecimal(TablaImprime.Rows[i]["Valor"].ToString());
                                Cadena = string.Format("{0:c2}", vMoney); //1.234,35 €
                                e.Graphics.DrawString((Cadena.PadLeft(LugarNumericos)), FuenteImprimeCampos3, Brushes.Black, PosValorX, PosValorY);

                                break;
                            case TipoDato.Fecha:
                                break;
                            case TipoDato.Hora:
                                break;
                            default:
                                break;
                        }
                    }
                    else if (FuenteCodBar == 4)
                    {

                        switch (TipoDatoValor)
                        {
                            case TipoDato.Texto:
                                e.Graphics.DrawString(Valor, FuenteImprimeCampos4, Brushes.Black, PosValorX, PosValorY);
                                break;
                            case TipoDato.Entero:
                                e.Graphics.DrawString(Valor, FuenteImprimeCampos4, Brushes.Black, PosValorX, PosValorY);
                                break;
                            case TipoDato.Numerico:
                                decimal vNumero = Convert.ToDecimal(TablaImprime.Rows[i]["Valor"].ToString());
                                Cadena = string.Format("{0:###,###0.00}", vNumero); //1.234,35 €
                                e.Graphics.DrawString((Cadena.PadLeft(LugarNumericos)), FuenteImprimeCampos4, Brushes.Black, PosValorX, PosValorY);

                                break;
                            case TipoDato.Moneda:
                                decimal vMoney = Convert.ToDecimal(TablaImprime.Rows[i]["Valor"].ToString());
                                Cadena = string.Format("{0:c2}", vMoney); //1.234,35 €
                                e.Graphics.DrawString((Cadena.PadLeft(LugarNumericos)), FuenteImprimeCampos4, Brushes.Black, PosValorX, PosValorY);

                                break;
                            case TipoDato.Fecha:
                                break;
                            case TipoDato.Hora:
                                break;
                            default:
                                break;
                        }
                    }
                    else if (FuenteCodBar == 5)
                    {

                        switch (TipoDatoValor)
                        {
                            case TipoDato.Texto:
                                e.Graphics.DrawString(Valor, FuenteImprimeCampos5, Brushes.Black, PosValorX, PosValorY);
                                break;
                            case TipoDato.Entero:
                                e.Graphics.DrawString(Valor, FuenteImprimeCampos5, Brushes.Black, PosValorX, PosValorY);
                                break;
                            case TipoDato.Numerico:
                                decimal vNumero = Convert.ToDecimal(TablaImprime.Rows[i]["Valor"].ToString());
                                Cadena = string.Format("{0:###,###0.00}", vNumero); //1.234,35 €
                                e.Graphics.DrawString((Cadena.PadLeft(LugarNumericos)), FuenteImprimeCampos5, Brushes.Black, PosValorX, PosValorY);

                                break;
                            case TipoDato.Moneda:
                                decimal vMoney = Convert.ToDecimal(TablaImprime.Rows[i]["Valor"].ToString());
                                Cadena = string.Format("{0:c2}", vMoney); //1.234,35 €
                                e.Graphics.DrawString((Cadena.PadLeft(LugarNumericos)), FuenteImprimeCampos5, Brushes.Black, PosValorX, PosValorY);

                                break;
                            case TipoDato.Fecha:
                                break;
                            case TipoDato.Hora:
                                break;
                            default:
                                break;
                        }
                    }

                }


            }
            return PosDescX;

            /////////////////////////////////////////////////////////////////////////////////////
        }

        public Single ImprimeLogo(System.Drawing.Printing.PrintPageEventArgs e, Single yPos)
        {
            //IMAGENES
            e.Graphics.DrawImage(imgLogo1, PosicionimgLogo1.X, PosicionimgLogo1.Y, TamanioimgLogo1.Width, TamanioimgLogo1.Height);
            if (imgLogo2 != null)
            {
                e.Graphics.DrawImage(imgLogo2, PosicionimgLogo2.X, PosicionimgLogo2.Y, TamanioimgLogo2.Width, TamanioimgLogo2.Height);
            }

            yPos += FuenteImprimeCampos.GetHeight(e.Graphics);
            //e.Graphics.DrawImage(vImagen1, vPosicionImagen1)
            //yPos += vFuenteEncabezado.GetHeight(e.Graphics)
            return yPos;
        }
    }//
}
