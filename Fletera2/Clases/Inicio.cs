﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fletera2Entidades;
using Core.Models;
using Fletera2Negocio;
using System.Windows.Forms;
using Core.Interfaces;
using System.Diagnostics;
using Microsoft.VisualBasic;

namespace Fletera2.Clases
{
    public static class Inicio
    {
        public static MiStopWath TiempoUsado;
        public static String szRegKeySistema = "SOFTWARE\\Computación en Acción, SA CV\\CONTPAQ I COMERCIAL";
        //public static int EmpresaComercial = 3;
        //public static int EmpresaComercial = 12;
        public static int lError = 0;
        public static bool bActivoSDK;
        public static bool bEmpresaSDKAbierta;
        public static string sRutaEmpresaAdmPAQ;
        public static string Parametro_PassFacturacion;
        public static string lPlantilla;


        //Para MsgBox
        public static string caption;
        public static MessageBoxButtons buttons = MessageBoxButtons.YesNo;
        public static DialogResult result;
        public static string mensaje = "";

        //para facturacion COMERCIAL
        public static int CIDDOCUMENTO = 0;
        public static StringBuilder vSerie = new StringBuilder(12);
        public static double vFolio = 0;

        public static OperationResult resp;

        public enum etipoAcompletar
        {
            Producto = 1,
            Servicio = 2,
            Clientes = 3,
            UnidadesTrans = 4
        }
        public enum eTipoDocumento
        {
            Orden_Compra = 1,
            Orden_Servicio = 2,
            Orden_Revitalizado = 3,
            Orden_Baja = 4,
            Rescate = 5,
            Inventario = 6,
            ServicioInterno = 7,
            Orden_Trabajo = 8
        }
        public enum eEstatusOS
        {
            PRE = 1,
            REC = 2,
            ASIG = 3,
            //DIAG = 4,
            TER = 5,
            ENT = 6,
            CAN = 7


        }
        public enum eTipoElementoViaje
        {
            Tractor = 1,
            Remolque1 = 2,
            Dolly = 3,
            Remolque2 = 4,
            Operador = 5
        }
        public enum eTipoDato
        {
            TdNumerico = 0,
            TdCadena = 1,
            TdFecha = 2,
            TdBoolean = 3,
            TdFechaHora = 4,
            TdHora = 5
        }
        public enum etipodoctoCOM
        {
            Factura = 4
        }
        public enum opcForma
        {
            Nuevo = 1,
            Editar = 2,
            Borrar = 3,
            Reporte = 4,
            Utilizar = 5,
            Cancelar = 6,
            Todos = 10

        }

        public enum eTipoOrdenServicio
        {
            COMBUSTIBLE = 1,
            TALLER = 2,
            LLANTAS = 3,
            LAVADERO = 4,
            RESGUARDO = 5,
            TODAS = 6,
            LLANTAS_ESP = 7
        }
        public enum eTipoSalida
        {
            PRESALIDA = 1,
            PREORDENCOMPRA = 2
        }
        public enum eTipoUnidad
        {
            Tractor = 1,
            Remolque = 2,
            Dolly = 3
        }
        public enum eTipoServicio
        {
            PREVENTIVO = 1,
            CORRECTIVO = 2,
            RECARGA = 3,
            RESGUARDO = 4
        }

        public enum eRelTipOrdServCat
        {
            CatTipoServicio = 1,
            CatActividades = 2,
            CatServicios = 3
        }

        public enum eTipoElementoOrdenTrab
        {
            Servicio = 1,
            Actividad = 2,
            FallaReportada = 3
        }

        public enum ePermisoEspecial
        {
            MultiEmpresa = 1
        }

        public enum opServLlantas
        {
            NO_APLICA = 0,
            DESMONTAR = 1,
            MONTAR_UNIDAD = 2,
            ROTAR_MISMA_UNIDAD = 3,
            CAMBIAR_OTRA_UNIDAD = 4,
            CAMBIO_ALMANCEN = 5,
            RECHAZADA_PROVEEDOR = 6
        }

        public enum opLugarLlantas
        {
            ALMACEN = 1,
            PROVEEDOR = 2,
            UNIDADES_TRANS = 3,
            PILA_DESHECHOS = 4,
            SIN_UBICACION = 5
        }

        public enum eOpcionLlantas
        {
            Origen = 1,
            Destino = 2
        }


        //public enum eTipoClasCOM
        //{
        //    TALLER = 1,
        //    LLANTAS = 2,
        //    LAVADERO = 3
        //}

        public static bool IniciarSesionSDK(int vEmpresaComercial)
        {
            try
            {
                RegistryKey KeySistema = Registry.LocalMachine.OpenSubKey(szRegKeySistema);
                object lEntrada = KeySistema.GetValue("DirectorioBase");
                Directory.SetCurrentDirectory(lEntrada.ToString());
                string RutaDatosCOM = "";
                OperationResult Resp = new EmpresasCOMSvc().getEmpresasxId(vEmpresaComercial);
                if (Resp.typeResult == ResultTypes.success)
                {
                    EmpresasCOM empresa = ((List<EmpresasCOM>)Resp.result)[0];
                    RutaDatosCOM = empresa.CRUTADATOS;
                }
                else
                {
                    //mensaje
                }
                lError = SDK_Comercial.fSetNombrePAQ("CONTPAQ I COMERCIAL");
                if (lError != 0)
                {
                    if (lError == 999999)
                    {
                        bActivoSDK = true;
                    }
                    else
                    {
                        SDK_Comercial.MuestraError(lError);
                        bActivoSDK = false;
                    }
                }
                else
                {
                    lError = SDK_Comercial.fAbreEmpresa(RutaDatosCOM);
                    if (lError != 0)
                    {
                        bEmpresaSDKAbierta = false;
                        SDK_Comercial.MuestraError(lError);
                    }
                    else
                    {
                        bEmpresaSDKAbierta = true;
                        sRutaEmpresaAdmPAQ = RutaDatosCOM;
                    }
                }
                return bEmpresaSDKAbierta;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
                return false;
            }

        }

        public static string DeterminaClasificacion(eTipoOrdenServicio opc)
        {
            string Resp = "";
            switch (opc)
            {
                case eTipoOrdenServicio.TALLER:
                    Resp = "'MEC','HOJ','HER','ELE','PIN'";
                    break;
                case eTipoOrdenServicio.LLANTAS:
                    Resp = "'LLA'";
                    break;
                case eTipoOrdenServicio.LAVADERO:
                    Resp = "'LAV'";
                    break;
                default:
                    break;
            }
            return Resp;
        }

        public static string NomEstatus(string opc)
        {
            //eEstatusOS opc;
            switch (opc)
            {
                case "PRE":
                    return "CAPTURA";
                //break;
                case "REC":
                    return "RECEPCIONADO";
                //break;
                case "ASIG":
                    return "ASIGNADO";
                //break;
                case "TER":
                    return "TERMINADO";
                //break;
                case "ENT":
                    return "ENTREGADO";
                //break;
                case "CAN":
                    return "CANCELADO";
                //break;
                default:
                    return "SIN ESTATUS";
                    //break;
            }
        }

        public static string FormarBarCode(string Codigo)
        {
            string Barcode = "";
            if (Codigo.Length == 1)
            {
                Barcode = "*00" + Codigo + "*";
            }
            else if (Codigo.Length == 2)
            {
                Barcode = "*0" + Codigo + "*";
            }
            else if (Codigo.Length >= 3)
            {
                Barcode = "*" + Codigo + "*";
            }
            return Barcode;
        }

        public static bool ConsultarPrivilegioEspecial(ePermisoEspecial NomPrivilegio, int idUsuario)
        {
            resp = new PrivilegioSvc().consultarPrivilegio(NomPrivilegio.ToString(), idUsuario);
            if (resp.typeResult == ResultTypes.success)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        public static bool IsNumeric(this string s)
        {
            float output;
            return float.TryParse(s, out output);
        }
        public static void ImprimeFactura(string nombreDoc, string Ruta)
        {
            //ProcessStartInfo loPSI = new ProcessStartInfo();
            //Process loProceso = new Process();
            try
            {

                string pdfPath = Path.Combine(Application.StartupPath, Ruta + nombreDoc + ".pdf");

                Process.Start(pdfPath);


            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + ex.Message,
                "Inicio", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        public static bool ValidarCampo(string Valor, eTipoDato tipo)
        {
            bool Retorna;
            switch (tipo)
            {
                case eTipoDato.TdNumerico:
                    if (Inicio.IsNumeric(Valor))
                    {
                        Retorna = true;
                    }
                    else
                    {
                        Retorna = false;
                    }
                    break;
                case eTipoDato.TdCadena:
                    Retorna = true;
                    break;
                case eTipoDato.TdFecha:
                    Retorna = true;
                    break;
                case eTipoDato.TdBoolean:
                    Retorna = true;
                    break;
                case eTipoDato.TdFechaHora:
                    Retorna = true;
                    break;
                case eTipoDato.TdHora:
                    Retorna = true;
                    break;
                default:
                    Retorna = true;
                    break;
            }
            return Retorna;

        }
        public struct MiStopWath
        {
            private Stopwatch _StW;
            private double totalSec;
            private double totalMin;

            public void StartNew()
            {
                _StW = Stopwatch.StartNew();
            }
            public void Stopp()
            {
                if (_StW == null)
                {
                    StartNew();
                }
                _StW.Reset();
            }
            public void Pause()
            {
                if (_StW == null)
                {
                    StartNew();
                }
                _StW.Stop();
            }
            public void Reanuda()
            {
                if (_StW == null)
                {
                    StartNew();
                }
                _StW.Start();
            }
            public string GetTiempo(bool VDetener = true)
            {
                try
                {
                    if (_StW == null)
                    {
                        if (_StW.IsRunning)
                        {
                            _StW.Stop();
                        }
                        totalSec = _StW.ElapsedMilliseconds / 1000;
                        totalMin = Math.Truncate(totalSec / 60);
                        totalSec = totalSec - totalMin * 60;
                        if (totalMin > 0)
                        {
                            return string.Format("<En: {0:#,##0.##} min {1:#,##0.0000} seg>", totalMin, totalSec);
                        }
                        else
                        {
                            return string.Format("<En: {1:#,##0.0000} seg>", totalMin, totalSec);
                        }
                    }
                    else
                    {
                        return "";
                    }
                }
                catch (Exception ex)
                {
                    return ex.Message;
                }
            }
        }

        public static string IntervaloHoras(DateTime vFecha1, DateTime vFecha2)
        {
            string rsInt = "";
            double SegTot;
            double Min;
            double Hor;
            double Dias;

            SegTot = (vFecha1 - vFecha2).TotalSeconds;
            //Dias = (vFecha1 - vFecha2).TotalDays;
            Dias = Math.Truncate(SegTot / 86400);
            SegTot -= Dias * 86400;
            Hor = Math.Truncate(SegTot / 3600);
            SegTot -= Hor * 3600;
            Min = Math.Truncate(SegTot / 60);
            SegTot -= Min * 60;
            if (Dias == 1)
            {
                rsInt += Dias + " Dia ";
            }
            else if (Dias != 0)
            {
                rsInt += Dias + " Dias ";
            }

            if (Hor > 0)
            {
                rsInt += Hor + " H. ";
            }

            if (Min > 0 || rsInt == "")
            {
                rsInt += Min + " Min. ";
            }

            if (SegTot > 0 || rsInt == "")
            {
                rsInt += SegTot + " Seg.";
            }
            return rsInt;
        }
        public static string IntervaloTiempoCad(DateTime vFecha)
        {
            string CadFin = "";
            long NAnio = 0;
            long NMes = 0;
            long NSem = 0;
            long NDias = 0;
            DateTime FechaTemp1; //Tiene que ser menor ya que este ira incrementando para igualar a la fecha2
            DateTime FechaTemp2;
            if (vFecha.ToString("yyyy/MM/dd") == "0001/01/01")
            {
                return vFecha.ToString("HH:mm:ss tt");
            }
            if (vFecha.Date < DateTime.Now.Date)
            {
                if (vFecha.Date == (DateTime.Now.AddDays(-1)).Date)
                {
                    return " AYER";
                }
                CadFin = " <-Hace ";
                FechaTemp1 = vFecha.Date;
                FechaTemp2 = DateTime.Now.Date;
            }
            else if (vFecha.Date == DateTime.Now.Date)
            {
                return " Es HOY";
            }
            else
            {
                if (vFecha.Date == (DateTime.Now.AddDays(-1)).Date)
                {
                    return " MAÑANA";
                }
                CadFin = " ->Faltan ";
                //FechaTemp1 = FormatDateTime(Now, DateFormat.ShortDate);
                //FechaTemp1 = Strings.FormatDateTime(DateTime.Now, DateFormat.ShortDate);
                FechaTemp1 = DateTime.Now;
                FechaTemp2 = vFecha;
            }
            NAnio = FechaTemp1.Year - FechaTemp2.Year;
            if (FechaTemp1.AddYears(Convert.ToInt32(NAnio)) > FechaTemp2)
            {
                NAnio -= 1;
            }
            FechaTemp1 = FechaTemp1.AddYears(Convert.ToInt32(NAnio));
            NMes = (FechaTemp1.Month - FechaTemp1.Month);
            if (FechaTemp1.AddMonths(Convert.ToInt32(NMes)) > FechaTemp2)
            {
                NMes -= 1;
            }
            FechaTemp1 = FechaTemp1.AddMonths(Convert.ToInt32(NMes));
            NSem = (FechaTemp1.DayOfWeek - FechaTemp2.DayOfWeek);
            FechaTemp1 = FechaTemp1.AddDays(Convert.ToInt32(NSem * 7));
            NDias = Convert.ToInt64((FechaTemp1 - FechaTemp2).TotalDays);
            if (NAnio > 0)
            {
                if (NAnio == 1)
                {
                    CadFin += NAnio + " Año, ";
                }
                else
                {
                    CadFin += NAnio + " Años, ";
                }
            }
            if (NMes > 0)
            {
                if (NMes == 1)
                {
                    CadFin += NMes + " Mes, ";
                }
                else
                {
                    CadFin += NMes + " Meses, ";
                }
            }
            if (NSem > 0)
            {
                if (NSem == 1)
                {
                    CadFin += NSem + " Semana, ";
                }
                else
                {
                    CadFin += NSem + " Semanas, ";
                }
            }
            if (NDias > 0)
            {
                if (NDias == 1)
                {
                    CadFin += NDias + " Dia,";
                }
                else
                {
                    CadFin += NDias + " Dias,";
                }
            }
            return CadFin.Substring(0, CadFin.Length - 1);
        }







    }//
}
