﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Fletera2.Clases.Tools;

namespace Fletera2.Clases
{
    public class ConsultasSQL
    {
        string strSql;
        public string LiqViaxOperador(string FechaIni, string FechaFin, string FiltroSinWhere)
        {
            strSql = "SELECT CONVERT(BIT,0) AS Seleccionado, " +
            "v.idOperador, " +
            "p.NombreCompleto as NomOperador, " +
            "COUNT(v.NumViaje) AS NumViaje " +
            "FROM dbo.CabGuia v " +
            "INNER JOIN dbo.CatPersonal P ON v.idOperador = p.idPersonal " +
            "WHERE v.FechaHoraViaje >= '" + FechaIni + " 00:00:00' " +
            "AND v.FechaHoraViaje <= '" + FechaFin + " 23:59:59' " + FiltroSinWhere +
            "GROUP BY v.idOperador,p.NombreCompleto " +
            "ORDER BY p.NombreCompleto";

            //CAST(0 AS BIT)
            //            "WHERE v.FechaHoraViaje >= '20171001 00:00:00' " +
            //"AND v.FechaHoraViaje <= '20171130 23:59:59' " + FiltroSinWhere +


            return strSql;

        }

        public string DiagTerDetOrdenServicio(DateTime FechaTerminado, int idOrdenSer, int idOrdenTrabajo)
        {
            strSql = "UPDATE dbo.DetOrdenServicio SET FechaDiag = '" + FormatFecHora(FechaTerminado, true, true) +
            "', FechaTerminado = '" + FormatFecHora(FechaTerminado, true, true) +
             "', Estatus = 'TER' WHERE idOrdenSer = " + idOrdenSer + " AND idOrdenTrabajo = " + idOrdenTrabajo;

            return strSql;
        }

        public string TerDetOrdenServicio(DateTime FechaTerminado, int idOrdenSer, int idOrdenTrabajo)
        {

            strSql = "UPDATE dbo.DetOrdenServicio SET FechaTerminado = '" + FormatFecHora(FechaTerminado, true, true) +
                "', Estatus = 'TER' WHERE idOrdenSer = " + idOrdenSer + " AND idOrdenTrabajo = " + idOrdenTrabajo;

            return strSql;
        }
        public string DiagTerCabOrdenServicio(DateTime FechaTerminado, int idOrdenSer)
        {
            strSql = "UPDATE dbo.CabOrdenServicio SET FechaDiagnostico = '" + FormatFecHora(FechaTerminado, true, true) +
            "', Estatus = 'TER', FechaTerminado = '" + FormatFecHora(FechaTerminado, true, true) + "' WHERE idOrdenSer = " + idOrdenSer;

            return strSql;
        }
        public string TerCabOrdenServicio(DateTime FechaTerminado, int idOrdenSer)
        {
            strSql = "UPDATE dbo.CabOrdenServicio SET Estatus = 'TER', FechaTerminado = '" + FormatFecHora(FechaTerminado, true, true) + "' WHERE idOrdenSer = " + idOrdenSer;

            return strSql;
        }

        public string ActualizaCampoTabla(string Tabla, string NomCampo, Inicio.eTipoDato TipoCampo, 
            string valorCampo,
            string NomCampoWhere, Inicio.eTipoDato TipoCampoWhere, string ValorCampoWhere, string FiltroAdicional = "")
        {
            string filtro = "";
            string filtroWhere = "";
            switch (TipoCampo)
            {
                //Filtro de Aplicacion
                case Inicio.eTipoDato.TdNumerico:
                    filtro = NomCampo + " = " + Convert.ToDouble(valorCampo);
                    break;
                case Inicio.eTipoDato.TdCadena:
                    filtro = NomCampo + " = '" + valorCampo + "'";
                    break;
                case Inicio.eTipoDato.TdFecha:
                    break;
                case Inicio.eTipoDato.TdBoolean:
                    break;
                case Inicio.eTipoDato.TdFechaHora:
                    break;
                case Inicio.eTipoDato.TdHora:
                    break;
                default:
                    break;
            }
            //Filtro de Condicion
            switch (TipoCampoWhere)
            {
                case Inicio.eTipoDato.TdNumerico:
                    filtroWhere = NomCampoWhere + " = " + ValorCampoWhere;
                    break;
                case Inicio.eTipoDato.TdCadena:
                    filtroWhere = NomCampoWhere + " = '" + ValorCampoWhere + "'";
                    break;
                case Inicio.eTipoDato.TdFecha:
                    filtroWhere = NomCampoWhere + " = '" + ValorCampoWhere + "'";
                    break;
                case Inicio.eTipoDato.TdBoolean:
                    filtroWhere = NomCampoWhere + " = " + ValorCampoWhere;
                    break;
                case Inicio.eTipoDato.TdFechaHora:
                    filtroWhere = NomCampoWhere + " = '" + ValorCampoWhere + "'";
                    break;
                case Inicio.eTipoDato.TdHora:
                    break;
                default:
                    break;
            }

            if (FiltroAdicional != "")
            {
                strSql = "UPDATE " + Tabla + " SET " + filtro + " WHERE " + filtroWhere + " AND " + FiltroAdicional;
            }
            else
            {
                strSql = "UPDATE " + Tabla + " SET " + filtro + " WHERE " + filtroWhere;
            }

            

            return strSql;
        }


       

        //    If TipoCampoWhere = TipoDato.TdCadena Then
        //        
        //    ElseIf TipoCampoWhere = TipoDato.TdNumerico Then
        //        
        //    ElseIf TipoCampoWhere = TipoDato.TdFecha Then
        //       
        //    ElseIf TipoCampoWhere = TipoDato.TdFechaHora Then
        //        
        //    ElseIf TipoCampoWhere = TipoDato.TdBoolean Then
        //        'filtroWhere = NomCampoWhere & " = " & IIf(ValorCampoWhere = "true", 1, 0)
        //        filtroWhere = NomCampoWhere & " = " & ValorCampoWhere
        //    End If



        //    


        //    Return StrSQL
        //End Function

    }//
}
