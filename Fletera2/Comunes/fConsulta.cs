﻿using Core.Models;
using Fletera2.Clases;
using Fletera2Entidades;
using Fletera2Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fletera2.Comunes
{
    public partial class fConsulta : Form
    {
        private opcBusqueda _opcion;
        OperationResult resp;
        List<CabOrdenServicio> listaCabOS;
        List<MasterOrdServ_f2> listaMaster;
        List<CatActividades> listaActividades;
        List<CatServicios> listaServicios;


        String _titulo;
        string _CampoDevuelve;
        string _ValorDevuelve;

        //para Filtro incluido
        string _FiltroSinWhere;
        string _OrderBy;
        //string _NomCampoFiltro;
        //string _ValorFiltro;
        //Parametros _param;
        bool _VisibleNuevo;
        Usuario _usuario;
        string _TextoInicial;


        public fConsulta(opcBusqueda opcion, Usuario usuario,
            string Titulo,
            string CampoDevuelve,
            bool VisibleNuevo,
            string FiltroSinWhere = "",
            string OrderBy = "",
            string TextoInicial = ""
            
            )
        {
            //Parametros param,
            //string NomCampoFiltro = "", string ValorFiltro = ""
            _opcion = opcion;
            _titulo = Titulo;
            _CampoDevuelve = CampoDevuelve;

            _FiltroSinWhere = FiltroSinWhere;
            _OrderBy = OrderBy;
            //_param = param;
            _usuario = usuario;

            _TextoInicial = TextoInicial;
            InitializeComponent();
        }

        private void grdDatos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void fConsulta_Load(object sender, EventArgs e)
        {
            this.Text = "Consultando . . ." + _titulo;
            btnNuevo.Visible = _VisibleNuevo;
            CargaDatos();
            txtBusqueda.Text = _TextoInicial;
            txtBusqueda.Focus();
        }
        private void CargaDatos()
        {
            try
            {
                switch (_opcion)
                {
                    case opcBusqueda.CabOrdenServicio:

                        resp = new CabOrdenServicioSvc().getCabOrdenServicioxFilter(0, _FiltroSinWhere, _OrderBy);
                        if (resp.typeResult == ResultTypes.success)
                        {
                            listaCabOS = (List<CabOrdenServicio>)resp.result;
                            ActualizaStatusControl(listaCabOS.Count);

                            grdDatos.DataSource = null;
                            grdDatos.DataSource = (List<CabOrdenServicio>)resp.result;
                        }
                        else
                        {
                            grdDatos.DataSource = null;
                            ActualizaStatusControl(0);
                        }
                        break;
                    case opcBusqueda.MasterOrdServ:
                        resp = new MasterOrdServSvc_f2().getMasterOrdServxFilter(0, _FiltroSinWhere, _OrderBy);
                        if (resp.typeResult == ResultTypes.success)
                        {
                            //if (listaMaster == null)
                            //{
                            //    listaMaster = new List<MasterOrdServ_f2>();
                            //}
                            listaMaster = (List<MasterOrdServ_f2>)resp.result;
                            ActualizaStatusControl(listaMaster.Count);

                            grdDatos.DataSource = null;
                            grdDatos.DataSource = listaMaster;

                        }
                        else
                        {
                            ActualizaStatusControl(0);
                            grdDatos.DataSource = null;
                            //no se encontraron registros
                        }
                        break;
                    case opcBusqueda.CatActividades:
                        resp = new CatActividadesSvc().getCatActividadesxFilter(0, _FiltroSinWhere, _OrderBy);
                        if (resp.typeResult == ResultTypes.success)
                        {
                            listaActividades = (List<CatActividades>)resp.result;
                            ActualizaStatusControl(listaActividades.Count);

                            grdDatos.DataSource = null;
                            grdDatos.DataSource = listaActividades;
                        }
                        else
                        {
                            grdDatos.DataSource = null;
                            ActualizaStatusControl(0);
                        }
                        break;
                    case opcBusqueda.CatServicios:
                        resp = new CatServiciosSvc().getCatServiciosxFilter(0, "", " c.NomServicio ASC");
                        if (resp.typeResult == ResultTypes.success)
                        {
                            listaServicios = (List<CatServicios>)resp.result;
                            ActualizaStatusControl(listaServicios.Count);

                            grdDatos.DataSource = null;
                            grdDatos.DataSource = listaServicios;
                        }
                        else
                        {
                            grdDatos.DataSource = null;
                            ActualizaStatusControl(0);
                        }

                        break;
                    default:
                        break;
                }

                FormatoGrid();
                PersonalizaCampos();
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + ex.Message,
                 this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

        }
        private void FormatoGrid()
        {
            try
            {
                grdDatos.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                grdDatos.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;

                grdDatos.RowsDefaultCellStyle.BackColor = Color.White;
                grdDatos.AlternatingRowsDefaultCellStyle.BackColor = Color.DarkSeaGreen;

                grdDatos.RowsDefaultCellStyle.SelectionForeColor = Color.Yellow;
                grdDatos.RowsDefaultCellStyle.SelectionBackColor = Color.Gray;
                grdDatos.SelectionMode = DataGridViewSelectionMode.FullRowSelect;


                grdDatos.DefaultCellStyle.Font = new Font("Berlin Sans FB", 12);
                grdDatos.ColumnHeadersDefaultCellStyle.Font = new Font("Berlin Sans FB", 12);
                grdDatos.ColumnHeadersDefaultCellStyle.BackColor = Color.Yellow;
                grdDatos.ColumnHeadersDefaultCellStyle.ForeColor = Color.Red;
                grdDatos.EnableHeadersVisualStyles = false;
            }
            catch (Exception e)
            {

                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + e.Message,
                   this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

        }

        private void PersonalizaCampos()
        {
            try
            {
                switch (_opcion)
                {
                    case opcBusqueda.CabOrdenServicio:
                        //OCULTAR COLUMNAS
                        grdDatos.Columns["IdEmpresa"].Visible = false;
                        grdDatos.Columns["RazonSocial"].Visible = false;
                        grdDatos.Columns["idTipoOrden"].Visible = false;
                        grdDatos.Columns["FechaRecepcion"].Visible = false;
                        grdDatos.Columns["idEmpleadoEntrega"].Visible = false;
                        grdDatos.Columns["NomEmpleadoEntrega"].Visible = false;
                        grdDatos.Columns["UsuarioRecepcion"].Visible = false;
                        grdDatos.Columns["Kilometraje"].Visible = false;
                        grdDatos.Columns["Estatus"].Visible = false;
                        grdDatos.Columns["FechaDiagnostico"].Visible = false;
                        grdDatos.Columns["FechaAsignado"].Visible = false;
                        grdDatos.Columns["FechaTerminado"].Visible = false;
                        grdDatos.Columns["FechaEntregado"].Visible = false;
                        grdDatos.Columns["UsuarioEntrega"].Visible = false;
                        grdDatos.Columns["idEmpleadoRecibe"].Visible = false;
                        grdDatos.Columns["NotaFinal"].Visible = false;
                        grdDatos.Columns["usuarioCan"].Visible = false;
                        grdDatos.Columns["FechaCancela"].Visible = false;
                        grdDatos.Columns["idEmpleadoAutoriza"].Visible = false;
                        grdDatos.Columns["fechaCaptura"].Visible = false;
                        grdDatos.Columns["idTipOrdServ"].Visible = false;
                        grdDatos.Columns["idTipoServicio"].Visible = false;
                        //grdDatos.Columns["idPadreOrdSer"].Visible = false;
                        grdDatos.Columns["ClaveFicticia"].Visible = false;
                        grdDatos.Columns["NomEmpleadoAutoriza"].Visible = false;
                        grdDatos.Columns["idOperarador"].Visible = false;
                        //grdDatos.Columns["NomOperador"].Visible = false;
                        grdDatos.Columns["NomTipoServicio"].Visible = false;
                        //grdDatos.Columns["xx"].Visible = false;

                        //HACER READONLY COLUMNAS
                        //grdDatos.Columns["Seleccionado"].ReadOnly = true;

                        //ENCABEZADO DE COLUMNAS
                        grdDatos.Columns["idOrdenSer"].HeaderText = "No.";
                        grdDatos.Columns["idPadreOrdSer"].HeaderText = "No. Padre";
                        grdDatos.Columns["idUnidadTrans"].HeaderText = "Unidad";
                        grdDatos.Columns["NomTipoUnidad"].HeaderText = "Tipo Unidad";
                        grdDatos.Columns["NomOperador"].HeaderText = "Operador";
                        grdDatos.Columns["NomTipOrdServ"].HeaderText = "Tipo de Orden";

                        //FORMATO DE COLUMNAS
                        //grdDatos.Columns["Precio"].DefaultCellStyle.Format = "C2";

                        //ALINEACION DE COLUMNAS
                        //grdDatos.Columns["Precio"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight;

                        //TAMAÑO AUTOMATICO DE COLUMNAS
                        grdDatos.Columns["idOrdenSer"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                        grdDatos.Columns["idPadreOrdSer"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                        grdDatos.Columns["idUnidadTrans"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                        grdDatos.Columns["NomTipoUnidad"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                        grdDatos.Columns["NomOperador"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                        grdDatos.Columns["NomTipOrdServ"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                        //grdDatos.Columns["xx"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                        //grdDatos.Columns["xx"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

                        //ORDEN DE APARICION
                        grdDatos.Columns["idOrdenSer"].DisplayIndex = 0;
                        grdDatos.Columns["idPadreOrdSer"].DisplayIndex = 1;
                        grdDatos.Columns["idUnidadTrans"].DisplayIndex = 2;
                        grdDatos.Columns["NomTipoUnidad"].DisplayIndex = 3;
                        grdDatos.Columns["NomOperador"].DisplayIndex = 4;
                        grdDatos.Columns["NomTipOrdServ"].DisplayIndex = 5;


                        break;
                    case opcBusqueda.MasterOrdServ:
                       
                        grdDatos.Columns["UserModifica"].Visible = false;
                        grdDatos.Columns["FechaModifica"].Visible = false;
                        grdDatos.Columns["UserCancela"].Visible = false;
                        grdDatos.Columns["FechaCancela"].Visible = false;
                        grdDatos.Columns["MotivoCancela"].Visible = false;
                        grdDatos.Columns["Estatus"].Visible = false;

                        //ENCABEZADO DE COLUMNAS
                        grdDatos.Columns["idPadreOrdSer"].HeaderText = "Folio Padre";
                        grdDatos.Columns["idTractor"].HeaderText = "Tractor";
                        grdDatos.Columns["idTolva1"].HeaderText = "Remolque";
                        grdDatos.Columns["idDolly"].HeaderText = "Dolly";
                        grdDatos.Columns["idTolva2"].HeaderText = "Remolque 2";
                        grdDatos.Columns["idChofer"].HeaderText = "Chofer";
                        grdDatos.Columns["FechaCreacion"].HeaderText = "Fecha";
                        grdDatos.Columns["UserCrea"].HeaderText = "Usuario";
                        grdDatos.Columns["NomChofer"].HeaderText = "Nombre Chofer";

                        //TAMAÑO AUTOMATICO DE COLUMNAS
                        grdDatos.Columns["idPadreOrdSer"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                        grdDatos.Columns["idTractor"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                        grdDatos.Columns["idTolva1"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                        grdDatos.Columns["idDolly"].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
                        grdDatos.Columns["idTolva2"].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
                        grdDatos.Columns["idChofer"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                        grdDatos.Columns["FechaCreacion"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                        grdDatos.Columns["UserCrea"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                        grdDatos.Columns["NomChofer"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                        break;
                    case opcBusqueda.CatActividades:
                        grdDatos.Columns["CostoManoObra"].Visible = false;
                        grdDatos.Columns["idFamilia"].Visible = false;
                        grdDatos.Columns["DuracionHoras"].Visible = false;
                        grdDatos.Columns["idEnum"].Visible = false;
                        grdDatos.Columns["CIDPRODUCTO"].Visible = false;
                        grdDatos.Columns["CCODIGOPRODUCTO"].Visible = false;

                        grdDatos.Columns["idActividad"].HeaderText = "Id";
                        grdDatos.Columns["NombreAct"].HeaderText = "Actividad";

                        grdDatos.Columns["idActividad"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                        grdDatos.Columns["NombreAct"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                        break;

                    case opcBusqueda.CatServicios:
                        grdDatos.Columns["idTipoServicio"].Visible = false;
                        grdDatos.Columns["Descripcion"].Visible = false;
                        grdDatos.Columns["CadaKms"].Visible = false;
                        grdDatos.Columns["CadaTiempoDias"].Visible = false;
                        grdDatos.Columns["Estatus"].Visible = false;
                        grdDatos.Columns["Costo"].Visible = false;
                        grdDatos.Columns["idDivision"].Visible = false;

                        grdDatos.Columns["idServicio"].HeaderText = "Id";
                        grdDatos.Columns["NomServicio"].HeaderText = "Actividad";

                        grdDatos.Columns["idServicio"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                        grdDatos.Columns["NomServicio"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                        break;

                    
                    default:
                        break;
                }
            }
            catch (Exception e)
            {

                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + e.Message,
                     this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

        }

        private void txtBusqueda_TextChanged(object sender, EventArgs e)
        {
            switch (_opcion)
            {
                case opcBusqueda.CabOrdenServicio:
                    if (txtBusqueda.Text.Length > 0)
                    {
                        var mySearch = (listaCabOS).FindAll(S => S.idOrdenSer.ToString().IndexOf(txtBusqueda.Text.Trim(), StringComparison.OrdinalIgnoreCase) >= 0 ||
                        S.idPadreOrdSer.ToString().IndexOf(txtBusqueda.Text.Trim(), StringComparison.OrdinalIgnoreCase) >= 0 ||
                        S.idUnidadTrans.IndexOf(txtBusqueda.Text.Trim(), StringComparison.OrdinalIgnoreCase) >= 0 ||
                        S.NomTipoUnidad.IndexOf(txtBusqueda.Text.Trim(), StringComparison.OrdinalIgnoreCase) >= 0 ||
                        S.NomOperador.IndexOf(txtBusqueda.Text.Trim(), StringComparison.OrdinalIgnoreCase) >= 0 ||
                        S.NomTipOrdServ.IndexOf(txtBusqueda.Text.Trim(), StringComparison.OrdinalIgnoreCase) >= 0);

                        ActualizaStatusControl(mySearch.Count);
                        grdDatos.DataSource = mySearch;
                    }
                    else
                    {
                        ActualizaStatusControl(listaCabOS.Count);
                        grdDatos.DataSource = listaCabOS;
                    }
                    break;
                case opcBusqueda.CatActividades:
                    if (txtBusqueda.Text.Length > 0)
                    {
                        var mySearch = (listaActividades).FindAll(S => S.idActividad.ToString().IndexOf(txtBusqueda.Text.Trim(), StringComparison.OrdinalIgnoreCase) >= 0 ||
                        S.NombreAct.IndexOf(txtBusqueda.Text.Trim(), StringComparison.OrdinalIgnoreCase) >= 0);

                        ActualizaStatusControl(mySearch.Count);
                        grdDatos.DataSource = mySearch;
                    }
                    else
                    {
                        ActualizaStatusControl(listaActividades.Count);
                        grdDatos.DataSource = listaActividades;
                    }
                    break;
                //case opcBusqueda.Clientes:
                //    break;
                //case opcBusqueda.Productos:
                //    if (txtBusqueda.Text.Length > 0)
                //    {
                //        var mySearch = (_listProdOrig).FindAll(S => S.CCODIGOPRODUCTO.IndexOf(txtBusqueda.Text.Trim(), StringComparison.OrdinalIgnoreCase) >= 0 ||
                //        S.CNOMBREPRODUCTO.IndexOf(txtBusqueda.Text.Trim(), StringComparison.OrdinalIgnoreCase) >= 0
                //        );

                //        ActualizaStatusControl(mySearch.Count);
                //        //grdDatos.DataSource = null
                //        grdDatos.DataSource = mySearch;
                //    }
                //    else
                //    {
                //        ActualizaStatusControl(_listProdOrig.Count);
                //        grdDatos.DataSource = _listProdOrig;
                //    }
                //    break;
                //case opcBusqueda.NotasVentas:
                //    if (txtBusqueda.Text.Length > 0)
                //    {
                //        var mySearch = (_listNotasOrig).FindAll(S => S.CNOMBREAGENTE.IndexOf(txtBusqueda.Text.Trim(), StringComparison.OrdinalIgnoreCase) >= 0 ||
                //        S.TipoDoc.IndexOf(txtBusqueda.Text.Trim(), StringComparison.OrdinalIgnoreCase) >= 0
                //        );

                //        ActualizaStatusControl(mySearch.Count);
                //        //grdDatos.DataSource = null
                //        grdDatos.DataSource = mySearch;
                //    }
                //    else
                //    {
                //        ActualizaStatusControl(_listProdOrig.Count);
                //        grdDatos.DataSource = _listProdOrig;
                //    }
                //    break;
                default:
                    break;
            }
        }
        private void ActualizaStatusControl(int NumRegistros)
        {
            statusStrip.Items[0].Text = "Registros Encontrados: " + NumRegistros;
        }

        private void txtBusqueda_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                //BandNoEnter = true;
                grdDatos.Focus();
                //BandNoEnter = false;
            }
        }

        private void grdDatos_Click(object sender, EventArgs e)
        {
            _ValorDevuelve = grdDatos.Rows[grdDatos.CurrentCell.RowIndex].Cells[_CampoDevuelve].Value.ToString();
            if (_ValorDevuelve != "")
            {
                DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void grdDatos_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)Convert.ToChar(e.KeyChar) == 13)
            {
                _ValorDevuelve = grdDatos.Rows[grdDatos.CurrentCell.RowIndex - 1].Cells[_CampoDevuelve].Value.ToString();
                if (_ValorDevuelve != "")
                {
                    DialogResult = DialogResult.OK;
                    this.Close();
                }
            }
        }
        public string DevuelveValor()
        {
            try
            {
                DialogResult dResult = ShowDialog();
                if (dResult == DialogResult.OK && _ValorDevuelve != null)
                {
                    return _ValorDevuelve;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {

                return null;
            }
        }
    }//
}
