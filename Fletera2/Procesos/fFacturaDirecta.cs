﻿using Core.Models;
using Fletera2.Clases;
using Fletera2Entidades;
using Fletera2Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Fletera2.Clases.Inicio;

namespace Fletera2.Procesos
{
    public partial class fFacturaDirecta : Form
    {
        Usuario _user;
        //Variables
        opcForma opcionForma;
        List<admConceptos> listConceptos;
        List<CatSAT> listFormaPago;
        List<CatSAT> listCFDI;
        List<CatSAT> listMetodoPago;
        OperationResult resp;

        List<admProductos> listRefac;
        List<admProductos> listServ;
        List<admClientes> listClientes;
        List<CatEmpresas> listEmpresas;
        List<CatUnidadTrans> listUnidadesTrans;
        CatUnidadTrans unidadtrans;
        admProductos producto;

        CabMovimientos CabMov;
        List<DetMovimientos> listDetMov;

        admClientes cliente;

        bool bandUnixCli = false;
        int vIdEmpresa;
        int Consecutivo = 0;
        decimal porcIva = 0;

        //Para Ejecutar
        string StrSql;
        int indice;
        string[] ArraySql;
        ConsultasSQL objsql = new ConsultasSQL();

        CatEmpresas empresa;
        Parametros param;

        decimal totRef = 0;
        decimal totServ = 0;
        decimal Subtot = 0;
        decimal totiva = 0;
        decimal total = 0;
        decimal ContRef = 0;
        decimal ContServ = 0;

        public fFacturaDirecta(Usuario user)
        {
            _user = user;
            InitializeComponent();
        }

        private void fFacturaDirecta_Load(object sender, EventArgs e)
        {
            OperationResult resp = new ParametrosSvc().getParametrosxFiltro(1);
            if (resp.typeResult == ResultTypes.success)
            {
                param = ((List<Parametros>)resp.result)[0];
            }
            Cancelar();

        }
        private void Cancelar()
        {
            HabilitaBotones(opcHabilita.DesHabilitaTodos);
            HabilitaCampos(opcHabilita.DesHabilitaTodos);
            CargaCombos();
           
            FormatoGeneral(grdDetalle);
            CargaProductos();
            CargaEmpresas();
            CargaClientes();
            LimpiaCampos();
            LimpiaCamposDetalle();

        }

        private void CargaEmpresas()
        {
            resp = new CatEmpresasSvc().getCatEmpresasxFiltro();
            if (resp.typeResult == ResultTypes.success)
            {
                listEmpresas = (List<CatEmpresas>)resp.result;
            }
        }


        private void CargaClientes()
        {
            resp = new ComercialSvc().getadmClientesxFilter(0, "CTIPOCLIENTE in (1,2)");
            if (resp.typeResult == ResultTypes.success)
            {
                listClientes = (List<admClientes>)resp.result;
                cmbCCODIGOC01_CLI.DataSource = null;
                cmbCCODIGOC01_CLI.DataSource = listClientes;

                cmbCCODIGOC01_CLI.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                cmbCCODIGOC01_CLI.AutoCompleteSource = AutoCompleteSource.ListItems;
                cmbCCODIGOC01_CLI.DisplayMember = "CRAZONSOCIAL";
                cmbCCODIGOC01_CLI.SelectedIndex = 0;

            }
        }

        private void CargaProductos()
        {
            //productos
            resp = new ComercialSvc().getadmProductosxFilter(0, "CTIPOPRODUCTO = 1");
            if (resp.typeResult == ResultTypes.success)
            {
                listRefac = (List<admProductos>)resp.result;
            }
            //servicios
            resp = new ComercialSvc().getadmProductosxFilter(0, "CTIPOPRODUCTO = 3");
            if (resp.typeResult == ResultTypes.success)
            {
                listServ = (List<admProductos>)resp.result;
            }
        }


        private void LimpiaCampos()
        {
            cmbCCODIGOC01_CLI.SelectedIndex = 0;
            dtpFechaMovimiento.Value = DateTime.Now;
            dtpFechaMovimientoHora.Value = DateTime.Now;
            txtIdUnidadTrans.Clear();
            txtTipoUnidadTrans.Clear();
            cmbConcepto.SelectedIndex = 0;
            cmbMetodoPago.SelectedIndex = 0;
            cmbUsoCFDI.SelectedIndex = 0;
            cmbFormaPago.SelectedIndex = 0;
            txtReferencia.Clear();

            txtTotRefacciones.Clear();
            txtTotManoObra.Clear();
            txtTotManoObra.Clear();
            txtTotIVA.Clear();
            txtTotTOTAL.Clear();

            Consecutivo = 0;
            grdDetalle.DataSource = null;
            FormatoGeneral(grdDetalle);
        }
        private void LimpiaCamposDetalle()
        {
            rdbServ.Checked = true;
            txtCCODIGOPRODUCTO.Clear();
            txtidunidad.Clear();
            txtCantidad.Clear();
            txtPrecio.Clear();
            txtTotal.Clear();
            lblCNOMBREPRODUCTO.Text = "";
            porcIva = 0;
            txtCCODIGOP01_PRO.Clear();
            txtCCODIGOP01_PRO.Enabled = false;

        }
        private void HabilitaBotones(opcHabilita opc)
        {
            switch (opc)
            {
                case opcHabilita.DesHabilitaTodos:
                    btnNuevo.Enabled = true;
                    btnGrabar.Enabled = false;
                    btnCancelar.Enabled = false;
                    btnSalir.Enabled = true;
                    break;
                case opcHabilita.Iniciando:
                    break;
                case opcHabilita.Editando:
                    break;
                case opcHabilita.Nuevo:
                    btnNuevo.Enabled = false;
                    btnGrabar.Enabled = true;
                    btnCancelar.Enabled = true;
                    btnSalir.Enabled = false;
                    break;
                default:
                    break;
            }
        }
        private void HabilitaCampos(opcHabilita opc)
        {
            dtpFechaMovimiento.Enabled = false;
            dtpFechaMovimientoHora.Enabled = false;

            switch (opc)
            {
                case opcHabilita.DesHabilitaTodos:
                    gpoAnexo20.Enabled = false;
                    gpoCLiente.Enabled = false;
                    gpoDetalle.Enabled = false;

                    //txtidOrdenSer.Enabled = false;
                    //grdDetOs.Enabled = false;
                    //grdRefacciones.Enabled = false;

                    //grdCabOC.Enabled = false;
                    //grdDetOC.Enabled = false;
                    //grdCabPreSalida.Enabled = false;
                    //grdDetPreSalida.Enabled = false;

                    break;
                case opcHabilita.Iniciando:
                    break;
                case opcHabilita.Editando:
                    break;
                case opcHabilita.Nuevo:
                    gpoCLiente.Enabled = true;
                    txtIdUnidadTrans.Enabled = false;
                    txtTipoUnidadTrans.Enabled = false;
                    cmbConcepto.Enabled = false;
                    gpoDetalle.Enabled = false;
                    gpoAnexo20.Enabled = false;
                    //txtCCODIGOC01_CLI.Enabled = true;
                    //txtIdUnidadTrans.Enabled = false;
                    //txtTipoUnidadTrans.Enabled = false;
                    //cmbConcepto.Enabled = false;

                    //txtidOrdenSer.Enabled = true;
                    //grdDetOs.Enabled = true;
                    //grdRefacciones.Enabled = true;
                    //grdCabOC.Enabled = true;
                    //grdDetOC.Enabled = true;
                    //grdCabPreSalida.Enabled = true;
                    //grdDetPreSalida.Enabled = true;


                    break;
                default:
                    break;
            }
        }
        private void CargaCombos()
        {
            try
            {
                OperationResult resp = new ComercialSvc().getadmConceptosxFilter(0, " c.CIDDOCUMENTODE = 4");
                if (resp.typeResult == ResultTypes.success)
                {
                    listConceptos = (List<admConceptos>)resp.result;
                    cmbConcepto.DataSource = null;
                    cmbConcepto.DataSource = listConceptos;
                    cmbConcepto.DisplayMember = "CNOMBRECONCEPTO";
                    cmbConcepto.SelectedIndex = 0;
                }
                CargaAnexo20();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }
        private void CargaAnexo20()
        {
            try
            {
                //FORMA DE PAGO
                resp = new CatSATSvc().getCatSATxFilter(0, "c.Tipo = 'FORPAG'", " c.idclave");
                if (resp.typeResult == ResultTypes.success)
                {
                    listFormaPago = (List<CatSAT>)resp.result;
                    cmbFormaPago.DataSource = null;
                    cmbFormaPago.DataSource = listFormaPago;
                    cmbFormaPago.DisplayMember = "Descripcion";
                    cmbFormaPago.SelectedIndex = 0;
                }
                //USO DE CFDI
                resp = new CatSATSvc().getCatSATxFilter(0, "c.Tipo = 'CFDI'", " c.idclave");
                if (resp.typeResult == ResultTypes.success)
                {
                    listCFDI = (List<CatSAT>)resp.result;
                    cmbUsoCFDI.DataSource = null;
                    cmbUsoCFDI.DataSource = listCFDI;
                    cmbUsoCFDI.DisplayMember = "Descripcion";
                    cmbUsoCFDI.SelectedIndex = 0;

                }

                //METODO DE PAGO
                resp = new CatSATSvc().getCatSATxFilter(0, "c.Tipo = 'METPAG'", " c.idclave");
                if (resp.typeResult == ResultTypes.success)
                {
                    listMetodoPago = (List<CatSAT>)resp.result;
                    cmbMetodoPago.DataSource = null;
                    cmbMetodoPago.DataSource = listMetodoPago;
                    cmbMetodoPago.DisplayMember = "Descripcion";
                    cmbMetodoPago.SelectedIndex = 0;

                }

            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + ex.Message,
this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            }
        }
        private void SeleccionacmbFormaPago(string clave)
        {
            try
            {
                int i = 0;
                foreach (CatSAT item in cmbFormaPago.Items)
                {
                    if (item.Clave == clave)
                    {
                        cmbFormaPago.SelectedIndex = i;
                        return;
                    }
                    else
                    {
                        i++;
                    }
                }
            }
            catch (Exception e)
            {

                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + e.Message,
                  this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void SeleccionacmbUsoCFDI(string clave)
        {
            try
            {
                int i = 0;
                foreach (CatSAT item in cmbUsoCFDI.Items)
                {
                    if (item.Clave == clave)
                    {
                        cmbUsoCFDI.SelectedIndex = i;
                        return;
                    }
                    else
                    {
                        i++;
                    }
                }
            }
            catch (Exception e)
            {

                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + e.Message,
                  this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void FormatoGeneral(DataGridView grid)
        {
            //ESTILO GENERAL
            grid.RowsDefaultCellStyle.BackColor = Color.White;
            grid.AlternatingRowsDefaultCellStyle.BackColor = Color.AliceBlue;

            grid.RowsDefaultCellStyle.SelectionForeColor = Color.Yellow;
            grid.RowsDefaultCellStyle.SelectionBackColor = Color.Gray;
            grid.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            grid.DefaultCellStyle.Font = new Font("Berlin Sans FB", 12);
            grid.ColumnHeadersDefaultCellStyle.Font = new Font("Berlin Sans FB", 12);
            grid.ColumnHeadersDefaultCellStyle.BackColor = Color.Yellow;
            grid.ColumnHeadersDefaultCellStyle.ForeColor = Color.Red;
            grid.EnableHeadersVisualStyles = false;
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            opcionForma = opcForma.Nuevo;
            HabilitaBotones(opcHabilita.Nuevo);
            HabilitaCampos(opcHabilita.Nuevo);
            AutoAcompletaProductos();
            CabMov = new CabMovimientos();
            Consecutivo = 0;
            //cmbIdEmpresa.Focus();
            cmbCCODIGOC01_CLI.Focus();
        }

        private void AutoAcompletaProductos()
        {
            if (rdbProd.Checked)
            {
                AutoAcompletarTexto(txtCCODIGOPRODUCTO, etipoAcompletar.Producto);
            }
            else
            {
                AutoAcompletarTexto(txtCCODIGOPRODUCTO, etipoAcompletar.Servicio);
            }
        }

        private void AutoAcompletarTexto(TextBox control, Inicio.etipoAcompletar tipo)
        {
            AutoCompleteStringCollection coleccion = new AutoCompleteStringCollection();

            CargaProductos();
            switch (tipo)
            {
                case etipoAcompletar.Producto:
                    foreach (var item in listRefac)
                    {
                        coleccion.Add(Convert.ToString(item.CNOMBREPRODUCTO));
                    }
                    break;
                case etipoAcompletar.Servicio:
                    foreach (var item in listServ)
                    {
                        coleccion.Add(Convert.ToString(item.CNOMBREPRODUCTO));
                    }

                    break;
                case etipoAcompletar.UnidadesTrans:
                    foreach (var item in listUnidadesTrans)
                    {
                        coleccion.Add(Convert.ToString(item.idUnidadTrans));
                    }

                    break;
                default:
                    break;
            }


            control.AutoCompleteCustomSource = coleccion;
            control.AutoCompleteMode = AutoCompleteMode.Suggest;
            control.AutoCompleteSource = AutoCompleteSource.CustomSource;


        }

        private void rdbProd_CheckedChanged(object sender, EventArgs e)
        {
            AutoAcompletaProductos();

            
        }

        private void rdbServ_CheckedChanged(object sender, EventArgs e)
        {
            AutoAcompletaProductos();
        }

        private void txtCCODIGOC01_CLI_TextChanged(object sender, EventArgs e)
        {

        }

        private void cmbCCODIGOC01_CLI_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cmbCCODIGOC01_CLI_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                cliente = (admClientes)cmbCCODIGOC01_CLI.SelectedItem;
                bandUnixCli = VerificaUnixCli(((admClientes)cmbCCODIGOC01_CLI.SelectedItem).CIDCLIENTEPROVEEDOR);
                if (bandUnixCli)
                {
                    txtIdUnidadTrans.Enabled = true;
                    txtReferencia.Enabled = true;
                    cmbConcepto.Enabled = true;
                    CargaUnidadesTrans(vIdEmpresa);
                    txtIdUnidadTrans.Focus();
                }
                else
                {
                    txtIdUnidadTrans.Enabled = false;
                    txtReferencia.Enabled = true;
                    cmbConcepto.Enabled = true;
                    cmbConcepto.Focus();
                }
                gpoDetalle.Enabled = true;
                gpoAnexo20.Enabled = true;
                cmbCCODIGOC01_CLI.Enabled = false;
            }
        }



        private bool VerificaUnixCli(int CIDCLIENTEPROVEEDOR)
        {
            foreach (var item in listEmpresas)
            {
                if (item.CIDCLIENTEPROVEEDOR == CIDCLIENTEPROVEEDOR)
                {
                    vIdEmpresa = item.idEmpresa;
                    return true;
                    //break;
                }
            }
            return false;
        }
        private void CargaUnidadesTrans(int IdEmpresa)
        {
            resp = new CatUnidadTransSvc().geCatUnidadTransxFilter("", "c.idEmpresa = " + IdEmpresa, "c.idUnidadTrans");
            if (resp.typeResult == ResultTypes.success)
            {
                listUnidadesTrans  = (List<CatUnidadTrans>)resp.result;
                AutoAcompletarTexto(txtIdUnidadTrans, etipoAcompletar.UnidadesTrans);
            }
        }

        private void txtIdUnidadTrans_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtIdUnidadTrans_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (ValidaUnidad(txtIdUnidadTrans.Text))
                {
                    gpoDetalle.Enabled = true;
                    gpoAnexo20.Enabled = true;
                    txtReferencia.Enabled = true;
                    cmbConcepto.Enabled = true;
                    cmbConcepto.Focus();
                }
                else
                {
                    txtIdUnidadTrans.Focus();
                }

            }
        }
        private bool ValidaUnidad(string idUnidadTrans)
        {
            try
            {
                resp = new CatUnidadTransSvc().geCatUnidadTransxFilter(idUnidadTrans);
                if (resp.typeResult == ResultTypes.success)
                {
                    unidadtrans = ((List<CatUnidadTrans>)resp.result)[0];
                    txtTipoUnidadTrans.Text = unidadtrans.nomTipoUniTras;
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + ex.Message,
                  this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
        }

        

        private void cmbConcepto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                txtReferencia.Focus();
                
                
            }
        }

        private void cmbFormaPago_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                cmbUsoCFDI.Focus();
            }

        }

        private void cmbUsoCFDI_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                cmbMetodoPago.Focus();
            }

        }

        private void cmbMetodoPago_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                txtCCODIGOPRODUCTO.Focus();
                //rdbServ.Focus();
            }

        }

        private void txtCCODIGOPRODUCTO_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (txtCCODIGOPRODUCTO.Text != "")
                {
                    ValidaProducto(txtCCODIGOPRODUCTO.Text);
                }
                else
                {
                    txtCCODIGOPRODUCTO.Focus();
                }
                
            }
        }
        private void ValidaProducto(string CCODIGOPRODUCTO)
        {
            try
            {
                resp = new ComercialSvc().getadmProductosxFilter(0, "CNOMBREPRODUCTO = '" + CCODIGOPRODUCTO  + "'");
                if (resp.typeResult == ResultTypes.success)
                {
                    producto = ((List<admProductos>)resp.result)[0];
                    txtPrecio.Text = producto.CPRECIO1.ToString();
                    txtCCODIGOP01_PRO.Text = producto.CCODIGOPRODUCTO;
                    txtidunidad.Text = producto.CIDUNIDADBASE.ToString();
                    porcIva = Convert.ToDecimal(producto.CIMPUESTO1.ToString()) ;
                    ActualizaTotal();
                    txtCantidad.Focus();
                    txtCantidad.SelectAll();
                }
                else
                {
                    txtCCODIGOPRODUCTO.Focus();
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + ex.Message,
                  this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                //return false;
            }
        }

        private void txtCCODIGOPRODUCTO_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtCantidad_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                ActualizaTotal();
                txtPrecio.Focus();
                txtPrecio.SelectAll();

            }
        }
        private void ActualizaTotal()
        {
            double Cantidad;
            double Precio;

            if (txtCantidad.Text == "")
            {
                Cantidad = 0;
            }
            else
            {
                Cantidad = Convert.ToDouble(txtCantidad.Text);
            }
            if (txtPrecio.Text == "")
            {
                Precio = 0;
            }
            else
            {
                Precio = Convert.ToDouble(txtPrecio.Text);
            }

            txtTotal.Text = (Cantidad * Precio).ToString();
        }

        private void txtPrecio_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                //grabar bien
                Consecutivo++;
                AgregaListaDet(0,Convert.ToInt32(txtidunidad.Text), txtCCODIGOP01_PRO.Text, Consecutivo,0,"", 
                    "FACDI",Convert.ToDecimal(txtCantidad.Text),Convert.ToDecimal(txtPrecio.Text),porcIva,0,
                    0,0,1,0,0,0,0,false,false,0,0,"",(rdbServ.Checked?true:false), txtCCODIGOPRODUCTO.Text);
                ActualizaTotal();
                ActualizaTotales();
                LimpiaCamposDetalle();
                txtCCODIGOPRODUCTO.Focus();
            }
        }
        private CabMovimientos MapeaCabMov()
        {
            try
            {
                return new CabMovimientos
                {
                    NoMovimiento = 0,
                    IdMovimiento = 0,
                    NoSerie = "",
                    TipoMovimiento = "FACDI",
                    CCODIGOC01_CLI = cliente.CCODIGOCLIENTE,
                    FechaMovimiento = dtpFechaMovimiento.Value,
                    //SubTotalGrav = Convert.ToDecimal(Subtot),
                    SubTotalGrav = Subtot,
                    SubTotalExento = 0,
                    Retencion = 0,
                    //Iva = Convert.ToDecimal(txtTotIVA.Text),
                    Iva = totiva,
                    Estatus = "TER",
                    Descuento = 0,
                    CCODIGOA01 = "",
                    Usuario = _user.nombreUsuario,
                    IdEmpresa = 3,
                    UserAutoriza = "",
                    CIDDOCUM02 = 4,
                    FechaVencimiento = dtpFechaMovimiento.Value,
                    FechaEntrega = dtpFechaMovimiento.Value,
                    UsuarioCrea = _user.nombreUsuario,
                    FechaCrea = dtpFechaMovimiento.Value,
                    UsuarioModifica = _user.nombreUsuario,
                    CIDMONEDA = 1,
                    CTIPOCAM01 = 1,
                    Vehiculo = txtIdUnidadTrans.Text,
                    Chofer = "",
                    CMETODOPAG = "",
                    CNUMCTAPAG = "",
                    CREFEREN01 = txtReferencia.Text,
                    COBSERVA01 = "",
                    TimbraFactura = true,
                    EsExterno = false,
                    ReqAutDcto = false,
                    Kilometraje = 0,
                    CantTotal = 0,
                    FechaCancela = dtpFechaMovimiento.Value,
                    FechaModifica = dtpFechaMovimiento.Value,
                    UsuarioCancela = ""
                   
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + ex.Message,
                                 this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return null;
            }
        }

        private void txtTotal_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void AgregaListaDet(int NoMovimiento,int CIDUNIDA01,string CCODIGOP01_PRO,int Consecutivo,
        int IdMovimiento,string NoSerie,string TipoMovimiento,decimal Cantidad,decimal Precio,decimal PorcIVA,
        decimal Costo,decimal PorcDescto,decimal ImpDescto,int CIDALMACEN,decimal Existencia,int CIDUNIDA01BASE,
        decimal CantidadBASE,decimal tFactorUnidad,bool tNoEquivalente,bool EsListaPrecios,int CIDUNIDA02,
        decimal CantidadEQUI,string COBSERVA01,bool EsServicio, string CNOMBREPRODUCTO)
        {
            try
            {
                DetMovimientos detmov = new DetMovimientos
                {
                    NoMovimiento = NoMovimiento,
                    CIDUNIDA01 = CIDUNIDA01,
                    CCODIGOP01_PRO = CCODIGOP01_PRO,
                    Consecutivo = Consecutivo,
                    IdMovimiento = IdMovimiento,
                    NoSerie = NoSerie,
                    TipoMovimiento = TipoMovimiento,
                    Cantidad = Cantidad,
                    Precio = Precio,
                    PorcIVA = PorcIVA,
                    Costo = Costo,
                    PorcDescto = PorcDescto,
                    ImpDescto = ImpDescto,
                    CIDALMACEN = CIDALMACEN,
                    Existencia = Existencia,
                    CIDUNIDA01BASE = CIDUNIDA01BASE,
                    CantidadBASE = CantidadBASE,
                    tFactorUnidad = tFactorUnidad,
                    tNoEquivalente = tNoEquivalente,
                    EsListaPrecios = EsListaPrecios,
                    CIDUNIDA02 = CIDUNIDA02,
                    CantidadEQUI = CantidadEQUI,
                    COBSERVA01 = COBSERVA01,
                    EsServicio = EsServicio,
                    CNOMBREPRODUCTO = CNOMBREPRODUCTO,
                    Total = (Cantidad * Precio)
                };
                if (listDetMov == null)
                {
                    listDetMov = new List<DetMovimientos>();
                }
                listDetMov.Add(detmov);
                grdDetalle.DataSource = null;
                grdDetalle.DataSource = listDetMov;
                FormatoGridEncabezados();
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + ex.Message,
                  this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void FormatoGridEncabezados()
        {

        grdDetalle.Columns["NoMovimiento"].Visible = false;
            grdDetalle.Columns["CIDUNIDA01"].Visible = false;
            grdDetalle.Columns["IdMovimiento"].Visible = false;
            grdDetalle.Columns["NoSerie"].Visible = false;
            grdDetalle.Columns["TipoMovimiento"].Visible = false;
            grdDetalle.Columns["Costo"].Visible = false;
            grdDetalle.Columns["PorcDescto"].Visible = false;
            grdDetalle.Columns["ImpDescto"].Visible = false;
            grdDetalle.Columns["CIDALMACEN"].Visible = false;
            grdDetalle.Columns["Existencia"].Visible = false;
            grdDetalle.Columns["CantidadBASE"].Visible = false;
            grdDetalle.Columns["CIDUNIDA01BASE"].Visible = false;
            grdDetalle.Columns["tFactorUnidad"].Visible = false;
            grdDetalle.Columns["tNoEquivalente"].Visible = false;
            grdDetalle.Columns["EsListaPrecios"].Visible = false;
            grdDetalle.Columns["CIDUNIDA02"].Visible = false;
            grdDetalle.Columns["CantidadEQUI"].Visible = false;
            grdDetalle.Columns["COBSERVA01"].Visible = false;


            grdDetalle.Columns["Consecutivo"].HeaderText = "#";
            grdDetalle.Columns["CCODIGOP01_PRO"].HeaderText = "Codigo";
            grdDetalle.Columns["CNOMBREPRODUCTO"].HeaderText = "Producto";
            grdDetalle.Columns["Cantidad"].HeaderText = "Cantidad";
            grdDetalle.Columns["Precio"].HeaderText = "Precio";
            grdDetalle.Columns["PorcIVA"].HeaderText = "IVA";
            grdDetalle.Columns["EsServicio"].HeaderText = "Servicio";


            //gridVenta.Columns["PorcIVA"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;
            //gridVenta.Columns["PorcIVA"].DefaultCellStyle.Format = "P2";

            grdDetalle.Columns["Precio"].DefaultCellStyle.Format = "C2";
            grdDetalle.Columns["Precio"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight;
            grdDetalle.Columns["Total"].DefaultCellStyle.Format = "C2";
            grdDetalle.Columns["Total"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight;
            //grdDetalle.Columns["ImpDescto"].DefaultCellStyle.Format = "C2";
            //grdDetalle.Columns["ImpDescto"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight;

            //grdDetalle.Columns["PorcDescto"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;
            //gridVenta.Columns["PorcDescto"].DefaultCellStyle.Format = "P";
            grdDetalle.Columns["Cantidad"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;


            grdDetalle.Columns["Consecutivo"].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
            grdDetalle.Columns["CCODIGOP01_PRO"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdDetalle.Columns["CNOMBREPRODUCTO"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdDetalle.Columns["Cantidad"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            grdDetalle.Columns["Precio"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            grdDetalle.Columns["PorcIVA"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            grdDetalle.Columns["Total"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            grdDetalle.Columns["Consecutivo"].DisplayIndex = 1;
            grdDetalle.Columns["CCODIGOP01_PRO"].DisplayIndex = 2;
            grdDetalle.Columns["CNOMBREPRODUCTO"].DisplayIndex = 3;
            grdDetalle.Columns["Cantidad"].DisplayIndex = 4;
            grdDetalle.Columns["Precio"].DisplayIndex = 5;
            grdDetalle.Columns["PorcIVA"].DisplayIndex = 6;
            grdDetalle.Columns["Total"].DisplayIndex = 7;
            grdDetalle.Columns["EsServicio"].DisplayIndex = 8;

        }

        private void txtPrecio_TextChanged(object sender, EventArgs e)
        {

        }

        private void ActualizaTotales()
        {
           
            if (listDetMov != null)
            {
                if (listDetMov.Count > 0)
                {
                    foreach (var item in listDetMov)
                    {
                        if (item.EsServicio)
                        {
                            totServ = totServ + (item.Cantidad * item.Precio);
                            ContServ = ContServ + item.Cantidad;
                        }
                        else
                        {
                            totRef = totRef + (item.Cantidad * item.Precio);
                            ContRef = ContRef + item.Cantidad;
                        }
                        Subtot = Subtot + (item.Cantidad * item.Precio);
                        totiva = totiva + ((item.Cantidad * item.Precio) * (item.PorcIVA) / 100);
                    }
                    total = Subtot + totiva;

                    //desplegar
                    txtTotRefacciones.Text = totRef.ToString("C2");
                    txtTotManoObra.Text = totServ.ToString("C2");
                    txtTotSubTotal.Text = Subtot.ToString("C2");
                    txtTotIVA.Text = totiva.ToString("C2");
                    txtTotTOTAL.Text = total.ToString("C2");
                    lblTotRef.Text = "Total de Refacciones : " + ContRef;
                    lblTotServ.Text = "Total de Servicios : " + ContServ;
                }
            }
        }

        private void btnGrabar_Click(object sender, EventArgs e)
        {
            try
            {
                bool BandGraba = false;
                indice = 0;
                if (!Validar())
                {
                    return;
                }

                CabMov = MapeaCabMov();
                //FACTURAR A CLIENTE
                //if (EnviaComercial(Convert.ToInt32(txtidOrdenSer.Text), Convert.ToDecimal(txtTotalhr.Text)))
                if (EnviaComercial())
                {
                    //MessageBox.Show("EnviaComercial Exitoso", "Operación Exitosa", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //return;
                    //GRABAR LOCAL
                    
                    resp = new MovimientosSvc().GuardaMovimientos(ref CabMov, ref listDetMov);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        BandGraba = true;
                        
                        Inicio.ImprimeFactura(vSerie + "" + vFolio.ToString(), @"C:\Compac\Empresas\adprueba\XML_SDK\");
                        //MessageBox.Show("Se Facturo Satisfactoriamente", "Operación Exitosa", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Cancelar();
                    }
                    else
                    {
                        MessageBox.Show("NO Se grabo datos locales", "Operación Exitosa", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        BandGraba = false;
                    }


                    //indice = 0;
                    //foreach (var item in listDetOS)
                    //{

                    //    //update a CIDPRODUCTO_SERV,Cantidad_CIDPRODUCTO_SERV,Precio_CIDPRODUCTO_SERV
                    //    StrSql = objsql.ActualizaCampoTabla("DetOrdenServicio",
                    //    "CIDPRODUCTO_SERV", eTipoDato.TdNumerico, item.CIDPRODUCTO_SERV.ToString(),
                    //    "idOrdenSer", eTipoDato.TdNumerico, txtidOrdenSer.Text, " idOrdenTrabajo = " + item.idOrdenTrabajo.ToString());
                    //    System.Array.Resize(ref ArraySql, indice + 1);
                    //    ArraySql[indice] = StrSql;
                    //    indice += 1;

                    //    StrSql = objsql.ActualizaCampoTabla("DetOrdenServicio",
                    //    "Cantidad_CIDPRODUCTO_SERV", eTipoDato.TdNumerico, item.Cantidad_CIDPRODUCTO_SERV.ToString(),
                    //    "idOrdenSer", eTipoDato.TdNumerico, txtidOrdenSer.Text, " idOrdenTrabajo = " + item.idOrdenTrabajo.ToString());
                    //    System.Array.Resize(ref ArraySql, indice + 1);
                    //    ArraySql[indice] = StrSql;
                    //    indice += 1;

                    //    StrSql = objsql.ActualizaCampoTabla("DetOrdenServicio",
                    //   "Precio_CIDPRODUCTO_SERV", eTipoDato.TdNumerico, item.Precio_CIDPRODUCTO_SERV.ToString(),
                    //   "idOrdenSer", eTipoDato.TdNumerico, txtidOrdenSer.Text, " idOrdenTrabajo = " + item.idOrdenTrabajo.ToString());
                    //    System.Array.Resize(ref ArraySql, indice + 1);
                    //    ArraySql[indice] = StrSql;
                    //    indice += 1;

                    //    if ((Inicio.eTipoOrdenServicio)Convert.ToInt32(cabos.idTipOrdServ) == Inicio.eTipoOrdenServicio.LLANTAS)
                    //    {
                    //        StrSql = objsql.DiagTerDetOrdenServicio(DateTime.Now, item.idOrdenSer, item.idOrdenTrabajo);
                    //        System.Array.Resize(ref ArraySql, indice + 1);
                    //        ArraySql[indice] = StrSql;
                    //        indice += 1;

                    //    }
                    //    else if ((Inicio.eTipoOrdenServicio)Convert.ToInt32(cabos.idTipOrdServ) == Inicio.eTipoOrdenServicio.TALLER)
                    //    {
                    //        StrSql = objsql.TerDetOrdenServicio(DateTime.Now, item.idOrdenSer, item.idOrdenTrabajo);
                    //        System.Array.Resize(ref ArraySql, indice + 1);
                    //        ArraySql[indice] = StrSql;
                    //        indice += 1;

                    //    }
                    //    else if ((Inicio.eTipoOrdenServicio)Convert.ToInt32(cabos.idTipOrdServ) == Inicio.eTipoOrdenServicio.LAVADERO)
                    //    {
                    //        StrSql = objsql.TerDetOrdenServicio(DateTime.Now, item.idOrdenSer, item.idOrdenTrabajo);
                    //        System.Array.Resize(ref ArraySql, indice + 1);
                    //        ArraySql[indice] = StrSql;
                    //        indice += 1;
                    //    }
                    //}


                   // if ((Inicio.eTipoOrdenServicio)Convert.ToInt32(cabos.idTipOrdServ) == Inicio.eTipoOrdenServicio.LLANTAS)
                   // {
                   //     StrSql = objsql.DiagTerCabOrdenServicio(DateTime.Now, cabos.idOrdenSer);
                   //     System.Array.Resize(ref ArraySql, indice + 1);
                   //     ArraySql[indice] = StrSql;
                   //     indice += 1;
                   // }
                   // else if ((Inicio.eTipoOrdenServicio)Convert.ToInt32(cabos.idTipOrdServ) == Inicio.eTipoOrdenServicio.TALLER)
                   // {
                   //     StrSql = objsql.TerCabOrdenServicio(DateTime.Now, cabos.idOrdenSer);
                   //     System.Array.Resize(ref ArraySql, indice + 1);
                   //     ArraySql[indice] = StrSql;
                   //     indice += 1;
                   // }
                   // else if ((Inicio.eTipoOrdenServicio)Convert.ToInt32(cabos.idTipOrdServ) == Inicio.eTipoOrdenServicio.LAVADERO)
                   // {
                   //     StrSql = objsql.TerCabOrdenServicio(DateTime.Now, cabos.idOrdenSer);
                   //     System.Array.Resize(ref ArraySql, indice + 1);
                   //     ArraySql[indice] = StrSql;
                   //     indice += 1;
                   // }

                   // //Actualiza CabOrdenServicio CIDDOCUMENTO, CSERIEDOCUMENTO, CFOLIO de Comercial
                   // //StrSql = objsql.ActualizaCampoTabla("CabOrdenServicio", 
                   // //"CIDDOCUMENTO", TipoDato.TdNumerico, CIDDOCUMENTO.ToString(), 
                   // //"idOrdenSer", TipoDato.TdNumerico, txtidOrdenSer.Text);
                   // //System.Array.Resize(ref ArraySql, indice + 1);
                   // //ArraySql[indice] = StrSql;
                   // //indice += 1;

                   // StrSql = objsql.ActualizaCampoTabla("CabOrdenServicio",
                   //"CIDDOCUMENTO", eTipoDato.TdNumerico, "1",
                   //"idOrdenSer", eTipoDato.TdNumerico, txtidOrdenSer.Text);
                   // System.Array.Resize(ref ArraySql, indice + 1);
                   // ArraySql[indice] = StrSql;
                   // indice += 1;

                   // StrSql = objsql.ActualizaCampoTabla("CabOrdenServicio",
                   // "CSERIEDOCUMENTO", eTipoDato.TdCadena, vSerie.ToString(),
                   // "idOrdenSer", eTipoDato.TdNumerico, txtidOrdenSer.Text);
                   // System.Array.Resize(ref ArraySql, indice + 1);
                   // ArraySql[indice] = StrSql;
                   // indice += 1;

                   // StrSql = objsql.ActualizaCampoTabla("CabOrdenServicio",
                   // "CFOLIO", eTipoDato.TdNumerico, vFolio.ToString(),
                   // "idOrdenSer", eTipoDato.TdNumerico, txtidOrdenSer.Text);
                   // System.Array.Resize(ref ArraySql, indice + 1);
                   // ArraySql[indice] = StrSql;
                   // indice += 1;




                   // if (indice > 0)
                   // {
                   //     //MessageBox.Show("Sentencias por Ejecutar local " + indice.ToString(), "Operación Exitosa", MessageBoxButtons.OK, MessageBoxIcon.Information);
                   //     string Respuesta;
                   //     Respuesta = new UtilSvc().EjecutaSentenciaSql("", ref ArraySql, indice);
                   //     if (Respuesta == "EFECTUADO")
                   //     {
                   //         //MessageBox.Show("Se Facturo Satisfactoriamente la Order de Servicio: " + txtidOrdenSer.Text, "Operación Exitosa", MessageBoxButtons.OK, MessageBoxIcon.Information);
                   //         btnCancelar.PerformClick();
                   //     }
                   // }
                    //GRABAR LOCAL

                }
                else
                {
                    //Mensaje
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);

            }
        }
        private bool Validar()
        {
            ////checar si todo lo referente a insumos ya esta entregado.
            //foreach (var item in listDetOS)
            //{
            //    if (item.Estatus != "TER")
            //    {
            //        //break;
            //        //Mensaje
            //        return false;
            //    }
            //}
            return true;
        }
        private bool EnviaComercial()
        {
            try
            {
                bool Exito = false;
                string TipDocumento = "";
                string TipDocumento2 = "";

                OperationResult respC = new ComercialSvc().getadmConceptosxFilter(((admConceptos)cmbConcepto.SelectedValue).CIDCONCEPTODOCUMENTO);
                if (respC.typeResult == ResultTypes.success)
                {
                    admConceptos conceptoCom = ((List<admConceptos>)respC.result)[0];
                    TipDocumento = conceptoCom.CCODIGOCONCEPTO;
                    TipDocumento2 = conceptoCom.CIDDOCUMENTODE.ToString();
                }
                //Determina Cliente
                OperationResult respEmp = new CatEmpresasSvc().getCatEmpresasxFiltro(vIdEmpresa);
                if (respEmp.typeResult == ResultTypes.success)
                {
                    empresa = ((List<CatEmpresas>)respEmp.result)[0];

                    OperationResult respcli = new ComercialSvc().getadmClientesxFilter(empresa.CIDCLIENTEPROVEEDOR);
                    if (respcli.typeResult == ResultTypes.success)
                    {
                        cliente = ((List<admClientes>)respcli.result)[0];
                    }
                }
                //Determina Cliente
                //RecalculaInsumosConc(idOrdenSer, param.PorcUtilidad);
                //CalculaResumen();

                if (IniciarSesionSDK(param.idEmpresaCOMTaller))
                {
                    lError = SDK_Comercial.fSiguienteFolio(TipDocumento, vSerie, ref vFolio);
                    if (lError == 0)
                    {
                        //If MsgBox("!! Esta seguro que desea Aplicar el Documento: " & TextoDocumento & " : " & vSerie & " " & vFolio & " ? !!", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                        SDK_Comercial.tDocumento ltDocto = new SDK_Comercial.tDocumento();
                        SDK_Comercial.tMovimiento ltMovto = new SDK_Comercial.tMovimiento();

                        int lIdDocto = 0;
                        int LIdMovto = 0;

                        int nCCANTPARCI = 0;

                        ltDocto.aCodConcepto = TipDocumento;
                        ltDocto.aFecha = DateTime.Now.ToString("MM/dd/yyyy");
                        ltDocto.aCodigoCteProv = cliente.CCODIGOCLIENTE;
                        ltDocto.aCodigoAgente = "";
                        ltDocto.aSistemaOrigen = 0;
                        ltDocto.aNumMoneda = 1;
                        ltDocto.aTipoCambio = 1;
                        ltDocto.aAfecta = 0;
                        ltDocto.aReferencia = txtReferencia.Text;


                        lError = SDK_Comercial.fAltaDocumento(ref lIdDocto, ref ltDocto);
                        if (lError == 0)
                        {
                            CIDDOCUMENTO = lIdDocto;
                            int cont = 0;
                            //MessageBox.Show("Lista de Productos" + listInsumosconc.Count, "Checando", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            foreach (var item in listDetMov)
                            {
                                cont += 1;
                                //ltMovto.aCodAlmacen = almacen.CCODIGOALMACEN;
                                ltMovto.aCodAlmacen = param.idAlmacenEnt;
                                ltMovto.aConsecutivo = cont;
                                //MessageBox.Show("Producto" + item.CCODIGOPRODUCTO, "Checando", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                ltMovto.aCodProdSer = item.CCODIGOP01_PRO;
                                ltMovto.aUnidades = (double)item.Cantidad;
                                ltMovto.aPrecio = (double)item.Precio;
                                ltMovto.aCosto = 0;
                                //ltMovto.aReferencia = item.NoMov.ToString();
                                //ltMovto.aReferencia = "Orden Servicio = " + idOrdenSer.ToString();
                                ltMovto.aReferencia = "Orden Servicio = 0000"; 
                                lError = SDK_Comercial.fAltaMovimiento(lIdDocto, ref LIdMovto, ref ltMovto);
                                if (lError == 0)
                                {
                                    Exito = true;
                                }
                                else
                                {
                                    SDK_Comercial.MuestraError(lError);
                                    Exito = false;
                                    break;
                                }
                            }

                            //Actualizar datos
                            CabMov.IdMovimiento = Convert.ToInt32(vFolio.ToString()) ;
                            CabMov.NoSerie = vSerie.ToString();
                            CabMov.CIDDOCUM02 = Convert.ToInt32(TipDocumento.ToString());
                            foreach (var item in listDetMov)
                            {
                                item.IdMovimiento = Convert.ToInt32(vFolio.ToString());
                                item.NoSerie = vSerie.ToString();
                            }

                            //Actualizar datos
                            if ((etipodoctoCOM)(Convert.ToInt32(TipDocumento2)) == etipodoctoCOM.Factura)
                            {
                                //OPCIONES DE TIMBRADO
                                indice = 0;
                                //tipo pago
                                StrSql = objsql.ActualizaCampoTabla("admDocumentos",
                                "CMETODOPAG", eTipoDato.TdCadena, ((CatSAT)cmbFormaPago.SelectedItem).Clave,
                                "CIDDOCUMENTO", eTipoDato.TdNumerico, CIDDOCUMENTO.ToString());
                                System.Array.Resize(ref ArraySql, indice + 1);
                                ArraySql[indice] = StrSql;
                                indice += 1;

                                nCCANTPARCI = Convert.ToInt32(((CatSAT)cmbMetodoPago.SelectedItem).Clave);
                                StrSql = objsql.ActualizaCampoTabla("admDocumentos",
                               "CCANTPARCI", eTipoDato.TdNumerico, nCCANTPARCI.ToString(),
                               "CIDDOCUMENTO", eTipoDato.TdNumerico, CIDDOCUMENTO.ToString());
                                System.Array.Resize(ref ArraySql, indice + 1);
                                ArraySql[indice] = StrSql;
                                indice += 1;

                                StrSql = objsql.ActualizaCampoTabla("admFoliosDigitales",
                               "CCODCONCBA", eTipoDato.TdCadena, ((CatSAT)cmbUsoCFDI.SelectedItem).Clave,
                               "CIDDOCTO", eTipoDato.TdNumerico, CIDDOCUMENTO.ToString(), " CIDDOCTODE = " + TipDocumento);
                                System.Array.Resize(ref ArraySql, indice + 1);
                                ArraySql[indice] = StrSql;
                                indice += 1;

                                if (indice > 0)
                                {
                                    //MessageBox.Show("Sentencias por Ejecutar local " + indice.ToString(), "Operación Exitosa", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    string Respuesta;
                                    Respuesta = new UtilSvc().EjecutaSentenciaSqlCOM("", ref ArraySql, indice);
                                    if (Respuesta == "EFECTUADO")
                                    {
                                        //MessageBox.Show("Se Facturo Satisfactoriamente la Order de Servicio: " + txtidOrdenSer.Text, "Operación Exitosa", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        //btnCancelar.PerformClick();
                                    }
                                }

                                //OPCIONES DE TIMBRADO
                                //Timbrar Factura
                                if (TimbraFactura(TipDocumento, vSerie.ToString(), vFolio))
                                {
                                    Exito = true;
                                    //MessageBox.Show("Se Timbro satisfactoriamente", "Checando", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                                else
                                {
                                    SDK_Comercial.MuestraError(lError);
                                    //SDK_Comercial.fBorraDocumento();
                                    Exito = false;
                                }
                            }
                        }
                        else
                        {
                            SDK_Comercial.MuestraError(lError);
                            //SDK_Comercial.fBorraDocumento();
                            Exito = false;
                        }
                    }
                    else
                    {
                        SDK_Comercial.MuestraError(lError);
                        Exito = false;
                    }
                }
                if (Exito)
                {
                    //MessageBox.Show("Se realizo la Factura:" + vSerie + vFolio.ToString() + " para la Orden de Servicio " + txtidOrdenSer.Text, "Operación Exitosa", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    MessageBox.Show("Se realizo la Factura:" + vSerie + vFolio.ToString() , "Operación Exitosa", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                return Exito;
            }
            catch (Exception e)
            {
                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + e.Message,
                   this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                return false;
            }
            finally
            {
                SDK_Comercial.fCierraEmpresa();
                SDK_Comercial.fTerminaSDK();
            }
        }
        private bool TimbraFactura(string TipoDocumento, string vSerie, double vFolio)
        {

            bool Exito = false;
            try
            {
                byte lLicencia = 0;
                lError = SDK_Comercial.fInicializaLicenseInfo(lLicencia);
                if (lError == 0)
                {

                    Parametro_PassFacturacion = "12345678a";
                    lError = SDK_Comercial.fEmitirDocumento(TipoDocumento, vSerie, vFolio, Parametro_PassFacturacion, "");
                    if (lError == 0)
                    {
                        lPlantilla = "facturaROA.rdl";
                        lError = SDK_Comercial.fEntregEnDiscoXML(TipoDocumento, vSerie, vFolio, 1, lPlantilla);
                        if (lError == 0)
                        {
                            //MessageBox.Show("Se Ejecuto SDK_Comercial.fEntregEnDiscoXML", "Checando", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            lError = SDK_Comercial.fEntregEnDiscoXML(TipoDocumento, vSerie, vFolio, 0, lPlantilla);
                            if (lError == 0)
                            {
                                //MessageBox.Show("Se Ejecuto SDK_Comercial.fEntregEnDiscoXML", "Checando", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                Exito = true;
                            }
                            else
                            {
                                SDK_Comercial.MuestraError(lError);
                                Exito = false;

                            }
                        }
                        else
                        {
                            SDK_Comercial.MuestraError(lError);
                            Exito = false;

                        }
                    }
                    else
                    {
                        SDK_Comercial.MuestraError(lError);
                        Exito = false;
                    }
                }
                else
                {
                    SDK_Comercial.MuestraError(lError);
                    Exito = false;
                    //Error
                }
                return Exito;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
                return Exito;
            }
        }

        private void cmbConcepto_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void txtReferencia_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                cmbFormaPago.Focus();
            }
        }

        private void rdbProd_Click(object sender, EventArgs e)
        {
            txtCCODIGOPRODUCTO.Focus();
        }

        private void rdbServ_Click(object sender, EventArgs e)
        {
            txtCCODIGOPRODUCTO.Focus();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Cancelar();
        }
    }//
}
