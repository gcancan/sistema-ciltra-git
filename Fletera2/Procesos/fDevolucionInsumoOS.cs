﻿using Core.Models;
using Fletera2.Clases;
using Fletera2Entidades;
using Fletera2Negocio;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Fletera2.Clases.Inicio;

namespace Fletera2.Procesos
{
    public partial class fDevolucionInsumoOS : Form
    {
        Usuario _user;
        //VARIABLES
        CabOrdenServicio cabos;
        Inicio.opcForma opcforma;
        OperationResult resp;
        List<DetPreSalida> listInsumos;
        //CabPreSalida cabsalida;
        Parametros param;
        admExistencia procom;
        //int CIDALMACEN_ENT;
        admAlmacenes almacen;
        List<admConceptos> listConceptos;
        CabPreSalida cabpresal;
        List<DetPreSalida> listdetpresal;
        DetPreSalida detpresal;
        List<DetOrdenServicio> listOrdenTrabajo;
        //List<CabPreSalida> listOrdenTrabajo;
        List<DetOrdenActividadProductos> listDetOrdenActividadProductos;
        DetOrdenActividadProductos detActProd;
        public fDevolucionInsumoOS(Usuario user)
        {
            _user = user;
            InitializeComponent();
        }

        private void txtidOrdenSer_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                if (txtidOrdenSer.Text != "")
                {
                    //Cargar Orden de Servicios
                    CargaForma(txtidOrdenSer.Text, opcBusqueda.CabViajes);
                }
                else
                {
                    txtidOrdenSer.Focus();
                }
            }
        }
        private void CargaForma(string Valor, opcBusqueda opc, int Valor2 = 0)
        {
            if (opc == opcBusqueda.CabViajes)
            {
                OperationResult resp = new CabOrdenServicioSvc().getCabOrdenServicioxFilter(Convert.ToInt32(Valor), " c.Estatus = 'ASIG'");
                if (resp.typeResult == ResultTypes.success)
                {
                    
                    cabos = ((List<CabOrdenServicio>)resp.result)[0];
                    if (cabos.Estatus != "ASIG")
                    {
                        MessageBox.Show("La Orden de Servicio debe estar en estatus Asignado", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }

                        txtIdEmpresa.Text = cabos.RazonSocial;
                    txtTipoOrdenServ.Text = cabos.NomTipOrdServ;
                    dtpFecha.Value = (DateTime)cabos.FechaDiagnostico;
                    dtpFechaHora.Value = (DateTime)cabos.FechaDiagnostico;
                    txtTipoUnidad.Text = cabos.NomTipoUnidad;
                    txtidUnidadTrans.Text = cabos.idUnidadTrans;
                    txtidEmpleadoEntrega.Text = cabos.NomEmpleadoEntrega;
                    txtEstatus.Text = cabos.Estatus;

                    //
                    
                    resp = new DetOrdenServicioSvc().getDetOrdenServicioxFilter(cabos.idOrdenSer, "d.Estatus = 'ASIG'");
                    if (resp.typeResult == ResultTypes.success)
                    {
                        listOrdenTrabajo = (List<DetOrdenServicio>)resp.result;
                        cmbIdOrdenTrabajo.DataSource = null;
                        cmbIdOrdenTrabajo.DataSource = listOrdenTrabajo;
                        cmbIdOrdenTrabajo.DisplayMember = "idOrdenTrabajo";
                        cmbIdOrdenTrabajo.SelectedIndex = 0;

                        grdRefacciones.DataSource = null;
                        Valor2 = ((DetOrdenServicio)cmbIdOrdenTrabajo.SelectedItem).idOrdenTrabajo;
                        resp = new PreSalidasSvc().getDetSalidasInsumoxFilter(0, "CABSAL.IDORDENTRABAJO IN (" + Valor2 + ") AND CABSAL.Estatus in ('ENT')");
                        if (resp.typeResult == ResultTypes.success)
                        {

                            listInsumos = (List<DetPreSalida>)resp.result;
                            foreach (var item in listInsumos)
                            {
                                resp = new ComercialSvc().getadmExistenciaxProd(item.idProducto,
                                        Tools.FormatFecHora(DateTime.Now, true, false, false),
                                        almacen.CIDALMACEN);
                                if (resp.typeResult == ResultTypes.success)
                                {
                                    procom = ((List<admExistencia>)resp.result)[0];
                                    if (procom.Existencia < 0)
                                    {
                                        item.Existencia = 0;
                                    }
                                    else
                                    {
                                        item.Existencia = (decimal)procom.Existencia;
                                    }

                                }
                                else
                                {
                                    item.Existencia = 0;
                                }

                            }

                            grdRefacciones.DataSource = listInsumos;
                            FormatogrdRefacciones();
                        }
                    }
                    //
                    //resp = new DetOrdenActividadProductosSvc().getDetOrdenActividadProductosxFilter(0, " d.idOrdenSer = " + Convert.ToInt32(Valor) );
                    //resp = new PreSalidasSvc().getDetSalidasInsumoxFilter(0, "CABSAL.IDORDENTRABAJO IN (SELECT IDORDENTRABAJO FROM DBO.DETORDENSERVICIO WHERE IDORDENSER = " + Convert.ToInt32(Valor) + " ) AND CABSAL.Estatus in ('ENT')");


                }

            }
            else if (opc == opcBusqueda.Insumos)
            {
                grdRefacciones.DataSource = null;
                //resp = new PreSalidasSvc().getDetSalidasInsumoxFilter(0, "CABSAL.IDORDENTRABAJO IN (SELECT IDORDENTRABAJO FROM DBO.DETORDENSERVICIO WHERE IDORDENSER = " + Convert.ToInt32(Valor) + " ) AND CABSAL.Estatus in ('ENT')");
                resp = new PreSalidasSvc().getDetSalidasInsumoxFilter(0, "CABSAL.IDORDENTRABAJO IN (" + Valor2 + ") AND CABSAL.Estatus in ('ENT')");
                //resp = new DetOrdenActividadProductosSvc().getDetOrdenActividadProductosxFilter(0, " d.idOrdenSer = " + Convert.ToInt32(Valor) );
                if (resp.typeResult == ResultTypes.success)
                {

                    listInsumos = (List<DetPreSalida>)resp.result;
                    foreach (var item in listInsumos)
                    {
                        resp = new ComercialSvc().getadmExistenciaxProd(item.idProducto,
                                Tools.FormatFecHora(DateTime.Now, true, false, false),
                                almacen.CIDALMACEN);
                        if (resp.typeResult == ResultTypes.success)
                        {
                            procom = ((List<admExistencia>)resp.result)[0];
                            if (procom.Existencia < 0)
                            {
                                item.Existencia = 0;
                            }
                            else
                            {
                                item.Existencia = (decimal)procom.Existencia;
                            }

                        }
                        else
                        {
                            item.Existencia = 0;
                        }

                    }

                    grdRefacciones.DataSource = listInsumos;
                    FormatogrdRefacciones();
                }
            }
        }

        private void fDevolucionInsumoOS_Load(object sender, EventArgs e)
        {
            OperationResult resp = new ParametrosSvc().getParametrosxFiltro(1);
            if (resp.typeResult == ResultTypes.success)
            {
                param = ((List<Parametros>)resp.result)[0];
                resp = new ComercialSvc().getadmAlmacenesxFilter(0, "c.CCODIGOALMACEN = '" + param.idAlmacenEnt + "'");
                if (resp.typeResult == ResultTypes.success)
                {
                    almacen = ((List<admAlmacenes>)resp.result)[0];
                }

            }

            Cancelar();
        }
        private void Cancelar()
        {
            HabilitaBotones(opcHabilita.DesHabilitaTodos);
            HabilitaCampos(opcHabilita.DesHabilitaTodos);
            LimpiaCampos();
            //CargaCombos();
            //FormatoGeneral(grdInsumosConc);
            //FormatoGeneral(grdDetOs);
            FormatoGeneral(grdRefacciones);
            //FormatoGeneral(grdCabOC);
            //FormatoGeneral(grdDetOC);
            //FormatoGeneral(grdCabPreSalida);
            //FormatoGeneral(grdDetPreSalida);
        }
        private void HabilitaBotones(opcHabilita opc)
        {
            switch (opc)
            {
                case opcHabilita.DesHabilitaTodos:
                    btnNuevo.Enabled = true;
                    btnGrabar.Enabled = false;
                    btnCancelar.Enabled = false;
                    btnSalir.Enabled = true;
                    break;
                case opcHabilita.Iniciando:
                    break;
                case opcHabilita.Editando:
                    break;
                case opcHabilita.Nuevo:
                    btnNuevo.Enabled = false;
                    btnGrabar.Enabled = true;
                    btnCancelar.Enabled = true;
                    btnSalir.Enabled = false;
                    break;
                default:
                    break;
            }
        }
        private void HabilitaCampos(opcHabilita opc)
        {
            txtIdEmpresa.Enabled = false;
            txtTipoOrdenServ.Enabled = false;
            dtpFecha.Enabled = false;
            dtpFechaHora.Enabled = false;
            txtTipoUnidad.Enabled = false;
            txtidUnidadTrans.Enabled = false;
            txtidEmpleadoEntrega.Enabled = false;
            txtEstatus.Enabled = false;

            //txtTotRefacciones.Enabled = false;
            //txtTotManoObra.Enabled = false;
            //txtTotSubTotal.Enabled = false;
            //txtTotIVA.Enabled = false;
            //txtTotTOTAL.Enabled = false;
            switch (opc)
            {
                case opcHabilita.DesHabilitaTodos:
                    txtidOrdenSer.Enabled = false;
                    cmbIdOrdenTrabajo.Enabled = false;
                    //grdDetOs.Enabled = false;
                    grdRefacciones.Enabled = false;

                    //grdCabOC.Enabled = false;
                    //grdDetOC.Enabled = false;
                    //grdCabPreSalida.Enabled = false;
                    //grdDetPreSalida.Enabled = false;

                    break;
                case opcHabilita.Iniciando:
                    break;
                case opcHabilita.Editando:
                    break;
                case opcHabilita.Nuevo:
                    txtidOrdenSer.Enabled = true;
                    cmbIdOrdenTrabajo.Enabled = true;
                    //grdDetOs.Enabled = true;
                    grdRefacciones.Enabled = true;
                    //grdCabOC.Enabled = true;
                    //grdDetOC.Enabled = true;
                    //grdCabPreSalida.Enabled = true;
                    //grdDetPreSalida.Enabled = true;


                    break;
                default:
                    break;
            }
        }
        private void LimpiaCampos()
        {
            txtidOrdenSer.Clear();
            txtIdEmpresa.Clear();
            txtTipoOrdenServ.Clear();
            dtpFecha.Value = DateTime.Now;
            dtpFechaHora.Value = DateTime.Now;
            txtTipoUnidad.Clear();
            txtidUnidadTrans.Clear();
            txtidEmpleadoEntrega.Clear();
            txtEstatus.Clear();
            grdRefacciones.DataSource = null;
            listOrdenTrabajo = null;
            cmbIdOrdenTrabajo.DataSource = null;
            //grdCabOC.DataSource = null;
            //grdDetOC.DataSource = null;
            //grdCabPreSalida.DataSource = null;
            //grdDetPreSalida.DataSource = null;


        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            opcforma = Inicio.opcForma.Nuevo;
            HabilitaBotones(opcHabilita.Nuevo);
            HabilitaCampos(opcHabilita.Nuevo);
            txtidOrdenSer.Focus();
        }
        private void FormatoGeneral(DataGridView grid)
        {
            //ESTILO GENERAL
            grid.RowsDefaultCellStyle.BackColor = Color.White;
            grid.AlternatingRowsDefaultCellStyle.BackColor = Color.AliceBlue;

            grid.RowsDefaultCellStyle.SelectionForeColor = Color.Yellow;
            grid.RowsDefaultCellStyle.SelectionBackColor = Color.Gray;
            grid.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            grid.DefaultCellStyle.Font = new Font("Berlin Sans FB", 12);
            grid.ColumnHeadersDefaultCellStyle.Font = new Font("Berlin Sans FB", 12);
            grid.ColumnHeadersDefaultCellStyle.BackColor = Color.Yellow;
            grid.ColumnHeadersDefaultCellStyle.ForeColor = Color.Red;
            grid.EnableHeadersVisualStyles = false;
        }
        private void FormatogrdRefacciones()
        {
            //     public bool Seleccionado { get; set; }
            //public int IdPreSalida { get; set; }
            //public int idProducto { get; set; }
            //public string CCODIGOPRODUCTO { get; set; }
            //public string CNOMBREPRODUCTO { get; set; }
            //public int idUnidadProd { get; set; }
            //public decimal Cantidad { get; set; }
            //public decimal CantidadEnt { get; set; }
            //public decimal Costo { get; set; }
            //public decimal Existencia { get; set; }
            //public bool Inserta { get; set; }
            //public decimal CantEntregaCambio { get; set; }

            grdRefacciones.Columns["Seleccionado"].Visible = false;
            grdRefacciones.Columns["IdPreSalida"].Visible = false;
            grdRefacciones.Columns["CantEntregaCambio"].Visible = false;
            //grdRefacciones.Columns["CantidadEnt"].Visible = false;

            grdRefacciones.Columns["Costo"].Visible = false;
            grdRefacciones.Columns["idUnidadProd"].Visible = false;
            //grdRefacciones.Columns["Existencia"].Visible = false;
            grdRefacciones.Columns["Inserta"].Visible = false;

            grdRefacciones.Columns["Cantidad"].HeaderText = "Cantidad";
            grdRefacciones.Columns["idProducto"].HeaderText = "Producto";
            grdRefacciones.Columns["CCODIGOPRODUCTO"].HeaderText = "Código";
            grdRefacciones.Columns["CNOMBREPRODUCTO"].HeaderText = "Descripción";
            grdRefacciones.Columns["CantidadEnt"].HeaderText = "Cant. Devuelve";

            //grdDetPreSalida.Columns["IdPreSalida"].HeaderText = "Folio";
            //grdDetPreSalida.Columns["IdPreSalida"].HeaderText = "Folio";

            grdRefacciones.Columns["Cantidad"].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
            grdRefacciones.Columns["idProducto"].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
            grdRefacciones.Columns["CCODIGOPRODUCTO"].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
            grdRefacciones.Columns["CNOMBREPRODUCTO"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            grdRefacciones.Columns["CantidadEnt"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

        }

        private void btnGrabar_Click(object sender, EventArgs e)
        {
            bool BandExito = false;
            try
            {
                //Mapea Cabpresalida

                cabpresal = MapeaCabPresalida();
                //Mapea DetPresalida
                MapeaDetPresalida();
                //Realizar el traspaso de la bodega de facturacion  a la bodega Principal
                if (EnviaComercial())
                {
                    //Crear la Presalida con Bit de Entrada para la Orden de Servicio
                    resp = new PreSalidasSvc().GuardaPresalidas(ref cabpresal, ref listdetpresal);
                    if (resp.typeResult == ResultTypes.success)
                    {

                        foreach (var item in listDetOrdenActividadProductos)
                        {
                            detActProd = item;
                            resp = new DetOrdenActividadProductosSvc().GuardaDetOrdenActividadProductos(ref detActProd);
                            if (resp.typeResult == ResultTypes.success)
                            {
                                BandExito = true;
                            }
                            else
                            {
                                BandExito = false;
                            }
                        }
                        if (BandExito)
                        {
                            Cancelar();
                        }

                        
                    }
                }




            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + ex.Message,
                    this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private bool EnviaComercial()
        {
            try
            {
                string DatoSalidaCOM = "";
                string DatoEntradaCOM = "";

                //SDK_Comercial.tDocumento ltDocto = new SDK_Comercial.tDocumento();
                //SDK_Comercial.tMovimiento ltMovto = new SDK_Comercial.tMovimiento();
                //int lIdDocto = 0;
                //int LIdMovto = 0;
                int cont = 0;

                bool Exito = false;
                //Checa Parametros
                OperationResult resp = new ParametrosSvc().getParametrosxFiltro(1);
                if (resp.typeResult == ResultTypes.success)
                {
                    param = ((List<Parametros>)resp.result)[0];
                    if (IniciarSesionSDK(param.idEmpresaCOMTaller))
                    {
                        //SALIDA
                        lError = SDK_Comercial.fSiguienteFolio(param.idConceptoSalAlm, vSerie, ref vFolio);
                        if (lError == 0)
                        {
                            SDK_Comercial.tDocumento ltDocto_Sal = new SDK_Comercial.tDocumento();
                            SDK_Comercial.tMovimiento ltMovto_Sal = new SDK_Comercial.tMovimiento();

                            int lIdDocto_Sal = 0;
                            int LIdMovto_Sal = 0;

                            ltDocto_Sal.aCodConcepto = param.idConceptoSalAlm;
                            ltDocto_Sal.aFecha = DateTime.Now.ToString("MM/dd/yyyy");
                            ltDocto_Sal.aCodigoCteProv = "";
                            ltDocto_Sal.aCodigoAgente = "";
                            ltDocto_Sal.aSistemaOrigen = 0;
                            ltDocto_Sal.aNumMoneda = 1;
                            ltDocto_Sal.aTipoCambio = 1;
                            ltDocto_Sal.aAfecta = 0;

                            lError = SDK_Comercial.fAltaDocumento(ref lIdDocto_Sal, ref ltDocto_Sal);
                            if (lError == 0)
                            {
                                //DETALLE DE SALIDA
                                CIDDOCUMENTO = lIdDocto_Sal;
                                cont = 0;
                                //foreach (var item in listInsumos)
                                foreach (var item in listdetpresal)
                                {
                                    cont += 1;
                                    //ltMovto.aCodAlmacen = almacen.CCODIGOALMACEN;
                                    ltMovto_Sal.aCodAlmacen = param.idAlmacenEnt;
                                    ltMovto_Sal.aConsecutivo = cont;
                                    //MessageBox.Show(item.CCODIGOPRODUCTO.ToString() + " Productos", "Checando", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    ltMovto_Sal.aCodProdSer = item.CCODIGOPRODUCTO;
                                    ltMovto_Sal.aUnidades = Math.Abs((double)item.Cantidad);
                                    ltMovto_Sal.aCosto = (double)item.Costo;
                                    //ltMovto.aCosto = 0;
                                    //ltMovto.aReferencia = item.NoMov.ToString();
                                    ltMovto_Sal.aReferencia = "Orden Servicio = " + txtidOrdenSer.Text;
                                    lError = SDK_Comercial.fAltaMovimiento(lIdDocto_Sal, ref LIdMovto_Sal, ref ltMovto_Sal);
                                    if (lError == 0)
                                    {
                                        Exito = true;
                                    }
                                    else
                                    {
                                        SDK_Comercial.MuestraError(lError);
                                        SDK_Comercial.fBorraDocumento();
                                        Exito = false;
                                        break;
                                    }
                                }

                                if (Exito)
                                {
                                    DatoSalidaCOM = " Salida: " + vSerie + vFolio.ToString() + " del Almacen " + param.idAlmacenSal;
                                    if (cabpresal == null)
                                    {
                                        cabpresal = new CabPreSalida();
                                    }
                                    //cabpresal.CSERIEDOCUMENTO_SAL = vSerie.ToString();
                                    //cabpresal.CFOLIO_SAL = vFolio;
                                    //cabpresal.CIDDOCUMENTO_SAL = CIDDOCUMENTO;

                                    cabpresal.CSERIEDOCUMENTO_ENT = vSerie.ToString();
                                    cabpresal.CFOLIO_ENT = vFolio;
                                    cabpresal.CIDDOCUMENTO_ENT = CIDDOCUMENTO;
                                    //TERMINA SALIDA
                                    //ENTRADA
                                    lError = SDK_Comercial.fSiguienteFolio(param.idConceptoEntAlm, vSerie, ref vFolio);
                                    if (lError == 0)
                                    {
                                        SDK_Comercial.tDocumento ltDocto_Ent = new SDK_Comercial.tDocumento();
                                        SDK_Comercial.tMovimiento ltMovto_Ent = new SDK_Comercial.tMovimiento();

                                        int lIdDocto_Ent = 0;
                                        int LIdMovto_Ent = 0;

                                        ltDocto_Ent.aCodConcepto = param.idConceptoEntAlm;
                                        ltDocto_Ent.aFecha = DateTime.Now.ToString("MM/dd/yyyy");
                                        ltDocto_Ent.aCodigoCteProv = "";
                                        ltDocto_Ent.aCodigoAgente = "";
                                        ltDocto_Ent.aSistemaOrigen = 0;
                                        ltDocto_Ent.aNumMoneda = 1;
                                        ltDocto_Ent.aTipoCambio = 1;
                                        ltDocto_Ent.aAfecta = 0;

                                        lError = SDK_Comercial.fAltaDocumento(ref lIdDocto_Ent, ref ltDocto_Ent);
                                        if (lError == 0)
                                        {
                                            //DETALLE DE ENTRADA

                                            CIDDOCUMENTO = lIdDocto_Ent;
                                            cont = 0;
                                            //foreach (var item in listInsumos)
                                            foreach (var item in listdetpresal)
                                            {
                                                cont += 1;
                                                //ltMovto.aCodAlmacen = almacen.CCODIGOALMACEN;
                                                ltMovto_Ent.aCodAlmacen = param.idAlmacenSal;
                                                ltMovto_Ent.aConsecutivo = cont;
                                                ltMovto_Ent.aCodProdSer = item.CCODIGOPRODUCTO;
                                                ltMovto_Ent.aUnidades = Math.Abs((double)item.Cantidad);
                                                ltMovto_Ent.aCosto = (double)item.Costo;
                                                //ltMovto.aCosto = 0;
                                                //ltMovto.aReferencia = item.NoMov.ToString();
                                                ltMovto_Ent.aReferencia = "Orden Servicio = " + txtidOrdenSer.Text;
                                                lError = SDK_Comercial.fAltaMovimiento(lIdDocto_Ent, ref LIdMovto_Ent, ref ltMovto_Ent);
                                                if (lError == 0)
                                                {
                                                    Exito = true;
                                                }
                                                else
                                                {
                                                    SDK_Comercial.MuestraError(lError);
                                                    SDK_Comercial.fBorraDocumento();
                                                    Exito = false;
                                                    break;
                                                }
                                            }

                                            if (Exito)
                                            {
                                                DatoEntradaCOM = " Entrada: " + vSerie + vFolio.ToString() + " del Almacen " + param.idAlmacenEnt;
                                                if (cabpresal == null)
                                                {
                                                    cabpresal = new CabPreSalida();
                                                }

                                                //cabpresal.CSERIEDOCUMENTO_ENT = vSerie.ToString();
                                                //cabpresal.CFOLIO_ENT = vFolio;
                                                //cabpresal.CIDDOCUMENTO_ENT = CIDDOCUMENTO;
                                                cabpresal.CSERIEDOCUMENTO_SAL = vSerie.ToString();
                                                cabpresal.CFOLIO_SAL = vFolio;
                                                cabpresal.CIDDOCUMENTO_SAL = CIDDOCUMENTO;

                                                //TERMINA SALIDA
                                            }


                                        }
                                        else
                                        {
                                            SDK_Comercial.MuestraError(lError);
                                            Exito = false;
                                        }
                                    }
                                    else
                                    {
                                        SDK_Comercial.MuestraError(lError);
                                        SDK_Comercial.fBorraDocumento();
                                        Exito = false;
                                    }
                                }
                            }
                            else
                            {
                                SDK_Comercial.MuestraError(lError);
                                SDK_Comercial.fBorraDocumento();
                                Exito = false;
                            }
                        }
                        else
                        {
                            SDK_Comercial.MuestraError(lError);
                            Exito = false;
                        }
                    }

                }
                else
                {
                    //si no se encuentra cancelar
                }


                if (Exito)
                {
                    MessageBox.Show("Se realizo los siguientes Movimientos:" + DatoSalidaCOM +
                              System.Environment.NewLine + DatoEntradaCOM +
                              System.Environment.NewLine +
                              " para la Orden de Servicio:" + txtidOrdenSer.Text, "Operación Exitosa", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                return Exito;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + ex.Message,
                 this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;

            }
            finally
            {
                SDK_Comercial.fCierraEmpresa();
                SDK_Comercial.fTerminaSDK();
            }

        }
        public static bool IniciarSesionSDK(int vEmpresaComercial)
        {
            try
            {
                RegistryKey KeySistema = Registry.LocalMachine.OpenSubKey(szRegKeySistema);
                object lEntrada = KeySistema.GetValue("DirectorioBase");
                Directory.SetCurrentDirectory(lEntrada.ToString());
                string RutaDatosCOM = "";
                OperationResult Resp = new EmpresasCOMSvc().getEmpresasxId(vEmpresaComercial);
                if (Resp.typeResult == ResultTypes.success)
                {
                    EmpresasCOM empresa = ((List<EmpresasCOM>)Resp.result)[0];
                    RutaDatosCOM = empresa.CRUTADATOS;
                }
                else
                {
                    //mensaje
                }
                lError = SDK_Comercial.fSetNombrePAQ("CONTPAQ I COMERCIAL");
                if (lError != 0)
                {
                    if (lError == 999999)
                    {
                        bActivoSDK = true;
                    }
                    else
                    {
                        SDK_Comercial.MuestraError(lError);
                        bActivoSDK = false;
                    }
                }
                else
                {
                    lError = SDK_Comercial.fAbreEmpresa(RutaDatosCOM);
                    if (lError != 0)
                    {
                        bEmpresaSDKAbierta = false;
                        SDK_Comercial.MuestraError(lError);
                    }
                    else
                    {
                        bEmpresaSDKAbierta = true;
                        sRutaEmpresaAdmPAQ = RutaDatosCOM;
                    }
                }
                return bEmpresaSDKAbierta;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
                return false;
            }

        }
        //private void CargaCombos()
        //{
        //    try
        //    {
        //        OperationResult resp = new ComercialSvc().getadmConceptosxFilter(0, " c.CIDDOCUMENTODE = 32");
        //        if (resp.typeResult == ResultTypes.success)
        //        {
        //            listConceptos = (List<admConceptos>)resp.result;
        //            cmbConcepto.DataSource = null;
        //            cmbConcepto.DataSource = listConceptos;
        //            cmbConcepto.DisplayMember = "CNOMBRECONCEPTO";
        //            cmbConcepto.SelectedIndex = 0;
        //        }
        //        //CargaAnexo20();
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show("Error: " + ex.Message);
        //    }
        //}
        private CabPreSalida MapeaCabPresalida()
        {
            try
            {
                return new CabPreSalida
                {
                    IdPreSalida = 0,
                    idOrdenTrabajo = ((DetOrdenServicio)cmbIdOrdenTrabajo.SelectedItem).idOrdenTrabajo,
                    FecSolicitud = DateTime.Now,
                    UserSolicitud = _user.nombreUsuario,
                    IdEmpleadoEnc = 0,
                    idAlmacen = almacen.CIDALMACEN,
                    CIDALMACEN_COM = almacen.CIDALMACEN,
                    Estatus = "ENT",
                    SubTotal = 0,
                    FecEntrega = DateTime.Now,
                    UserEntrega = "",
                    idOrdenSer = Convert.ToInt32(txtidOrdenSer.Text),
                    EsDevolucion = true
                };
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + ex.Message,
this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return null;
            }
        }

        private void MapeaDetPresalida()
        {
            try
            {
                grdRefacciones.Refresh();
                listdetpresal = new List<DetPreSalida>();
                listDetOrdenActividadProductos = new List<DetOrdenActividadProductos>();
                foreach (var item in listInsumos)
                {
                    if (item.CantidadEnt > 0)
                    {
                        detpresal = new DetPreSalida
                        {
                            IdPreSalida = 0,
                            idProducto = item.idProducto,
                            idUnidadProd = item.idUnidadProd,
                            Cantidad = item.CantidadEnt * -1,
                            CantidadEnt = item.CantidadEnt * -1,
                            Costo = item.Costo,
                            Existencia = item.Existencia,
                            CantEntregaCambio = item.CantidadEnt * -1,
                            CCODIGOPRODUCTO = item.CCODIGOPRODUCTO,
                            CNOMBREPRODUCTO = item.CNOMBREPRODUCTO,
                            Inserta = true,

                        };
                        listdetpresal.Add(detpresal);

                        detActProd = new DetOrdenActividadProductos
                        {
                            idDetOrdenActividadProductos = 0,
                            idOrdenSer = Convert.ToInt32(txtidOrdenSer.Text),
                            idOrdenActividad = ((DetOrdenServicio)cmbIdOrdenTrabajo.SelectedItem).idOrdenActividad,
                            idProducto = item.idProducto,
                            idUnidadProd = item.idUnidadProd,
                            Cantidad = item.CantidadEnt * -1,
                            Costo = item.Costo,
                            Existencia = item.Existencia,
                            CantCompra = 0,
                            CantSalida = item.CantidadEnt * -1,
                            Precio = 0,
                            Inserta = true
                        };
                        listDetOrdenActividadProductos.Add(detActProd);

                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + ex.Message,
                   this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Cancelar();
        }

        private void txtidOrdenSer_TextChanged(object sender, EventArgs e)
        {

        }

        private void cmbIdOrdenTrabajo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                CargaForma(txtidOrdenSer.Text, opcBusqueda.Insumos, ((DetOrdenServicio)cmbIdOrdenTrabajo.SelectedItem).idOrdenTrabajo);
            }
        }

        private void cmbIdOrdenTrabajo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbIdOrdenTrabajo.DataSource != null)
            {
                CargaForma(txtidOrdenSer.Text, opcBusqueda.Insumos, ((DetOrdenServicio)cmbIdOrdenTrabajo.SelectedItem).idOrdenTrabajo);
            }

        }
    }//
}
