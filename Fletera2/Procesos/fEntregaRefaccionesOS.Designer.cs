﻿namespace Fletera2.Procesos
{
    partial class fEntregaRefaccionesOS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fEntregaRefaccionesOS));
            this.ToolStripMenu = new System.Windows.Forms.ToolStrip();
            this.btnNuevo = new System.Windows.Forms.ToolStripButton();
            this.btnGrabar = new System.Windows.Forms.ToolStripButton();
            this.btnEditar = new System.Windows.Forms.ToolStripButton();
            this.btnReporte = new System.Windows.Forms.ToolStripButton();
            this.btnCancelar = new System.Windows.Forms.ToolStripButton();
            this.btnSalir = new System.Windows.Forms.ToolStripButton();
            this.gpoFaC = new System.Windows.Forms.GroupBox();
            this.txtActividad = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtIdEmpleadoEnc = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtidUnidadTrans = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtTipoUnidad = new System.Windows.Forms.TextBox();
            this.txtidOrdenSer = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtNomEmpresa = new System.Windows.Forms.TextBox();
            this.txtidOrdenTrabajo = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dtpFecSolicitudHora = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpFecSolicitud = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.txtIdFOLIO = new System.Windows.Forms.TextBox();
            this.grdRef = new System.Windows.Forms.DataGridView();
            this.cmbIdPersonalRec = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtidPersonalAyu1 = new System.Windows.Forms.TextBox();
            this.txtidPersonalAyu2 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.ToolStripMenu.SuspendLayout();
            this.gpoFaC.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdRef)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolStripMenu
            // 
            this.ToolStripMenu.AutoSize = false;
            this.ToolStripMenu.BackColor = System.Drawing.SystemColors.Control;
            this.ToolStripMenu.Dock = System.Windows.Forms.DockStyle.None;
            this.ToolStripMenu.Font = new System.Drawing.Font("Berlin Sans FB", 11.25F);
            this.ToolStripMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnNuevo,
            this.btnGrabar,
            this.btnEditar,
            this.btnReporte,
            this.btnCancelar,
            this.btnSalir});
            this.ToolStripMenu.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.ToolStripMenu.Location = new System.Drawing.Point(0, 0);
            this.ToolStripMenu.Name = "ToolStripMenu";
            this.ToolStripMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.ToolStripMenu.Size = new System.Drawing.Size(409, 65);
            this.ToolStripMenu.TabIndex = 10;
            this.ToolStripMenu.Text = "ToolStrip1";
            // 
            // btnNuevo
            // 
            this.btnNuevo.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnNuevo.Image = ((System.Drawing.Image)(resources.GetObject("btnNuevo.Image")));
            this.btnNuevo.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnNuevo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(54, 62);
            this.btnNuevo.Text = "&Nuevo";
            this.btnNuevo.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnNuevo.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnNuevo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // btnGrabar
            // 
            this.btnGrabar.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnGrabar.Image = ((System.Drawing.Image)(resources.GetObject("btnGrabar.Image")));
            this.btnGrabar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnGrabar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnGrabar.Name = "btnGrabar";
            this.btnGrabar.Size = new System.Drawing.Size(60, 62);
            this.btnGrabar.Text = "&Grabar";
            this.btnGrabar.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnGrabar.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnGrabar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnGrabar.Click += new System.EventHandler(this.btnGrabar_Click);
            // 
            // btnEditar
            // 
            this.btnEditar.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnEditar.Image = ((System.Drawing.Image)(resources.GetObject("btnEditar.Image")));
            this.btnEditar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnEditar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(52, 62);
            this.btnEditar.Text = "&Editar";
            this.btnEditar.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnEditar.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnEditar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnEditar.Visible = false;
            // 
            // btnReporte
            // 
            this.btnReporte.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnReporte.Image = ((System.Drawing.Image)(resources.GetObject("btnReporte.Image")));
            this.btnReporte.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnReporte.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnReporte.Name = "btnReporte";
            this.btnReporte.Size = new System.Drawing.Size(65, 62);
            this.btnReporte.Text = "&Reporte";
            this.btnReporte.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnReporte.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnReporte.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // btnCancelar
            // 
            this.btnCancelar.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnCancelar.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar.Image")));
            this.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnCancelar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(69, 62);
            this.btnCancelar.Text = "&Cancelar";
            this.btnCancelar.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnCancelar.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnSalir.Image = ((System.Drawing.Image)(resources.GetObject("btnSalir.Image")));
            this.btnSalir.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnSalir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(55, 62);
            this.btnSalir.Text = "&S a l i r";
            this.btnSalir.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnSalir.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // gpoFaC
            // 
            this.gpoFaC.Controls.Add(this.label11);
            this.gpoFaC.Controls.Add(this.label9);
            this.gpoFaC.Controls.Add(this.txtidPersonalAyu2);
            this.gpoFaC.Controls.Add(this.txtidPersonalAyu1);
            this.gpoFaC.Controls.Add(this.txtActividad);
            this.gpoFaC.Controls.Add(this.label12);
            this.gpoFaC.Controls.Add(this.txtIdEmpleadoEnc);
            this.gpoFaC.Controls.Add(this.label10);
            this.gpoFaC.Controls.Add(this.txtidUnidadTrans);
            this.gpoFaC.Controls.Add(this.label7);
            this.gpoFaC.Controls.Add(this.txtTipoUnidad);
            this.gpoFaC.Controls.Add(this.txtidOrdenSer);
            this.gpoFaC.Controls.Add(this.label6);
            this.gpoFaC.Controls.Add(this.txtNomEmpresa);
            this.gpoFaC.Controls.Add(this.txtidOrdenTrabajo);
            this.gpoFaC.Controls.Add(this.label8);
            this.gpoFaC.Controls.Add(this.label4);
            this.gpoFaC.Controls.Add(this.label3);
            this.gpoFaC.Controls.Add(this.dtpFecSolicitudHora);
            this.gpoFaC.Controls.Add(this.label2);
            this.gpoFaC.Controls.Add(this.dtpFecSolicitud);
            this.gpoFaC.Controls.Add(this.label1);
            this.gpoFaC.Controls.Add(this.txtIdFOLIO);
            this.gpoFaC.Location = new System.Drawing.Point(0, 68);
            this.gpoFaC.Name = "gpoFaC";
            this.gpoFaC.Size = new System.Drawing.Size(929, 188);
            this.gpoFaC.TabIndex = 11;
            this.gpoFaC.TabStop = false;
            this.gpoFaC.Text = "Datos de Entrega";
            // 
            // txtActividad
            // 
            this.txtActividad.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtActividad.Location = new System.Drawing.Point(523, 48);
            this.txtActividad.Name = "txtActividad";
            this.txtActividad.Size = new System.Drawing.Size(400, 27);
            this.txtActividad.TabIndex = 73;
            this.txtActividad.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Blue;
            this.label12.Location = new System.Drawing.Point(443, 51);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(74, 18);
            this.label12.TabIndex = 74;
            this.label12.Text = "Actividad:";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtIdEmpleadoEnc
            // 
            this.txtIdEmpleadoEnc.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIdEmpleadoEnc.Location = new System.Drawing.Point(149, 116);
            this.txtIdEmpleadoEnc.Name = "txtIdEmpleadoEnc";
            this.txtIdEmpleadoEnc.Size = new System.Drawing.Size(375, 27);
            this.txtIdEmpleadoEnc.TabIndex = 71;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Blue;
            this.label10.Location = new System.Drawing.Point(9, 123);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(134, 18);
            this.label10.TabIndex = 72;
            this.label10.Text = "Tecnico Encargado:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtidUnidadTrans
            // 
            this.txtidUnidadTrans.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtidUnidadTrans.Location = new System.Drawing.Point(834, 84);
            this.txtidUnidadTrans.Name = "txtidUnidadTrans";
            this.txtidUnidadTrans.Size = new System.Drawing.Size(75, 25);
            this.txtidUnidadTrans.TabIndex = 70;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Blue;
            this.label7.Location = new System.Drawing.Point(552, 87);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(135, 18);
            this.label7.TabIndex = 69;
            this.label7.Text = "Unidad Transporte:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtTipoUnidad
            // 
            this.txtTipoUnidad.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtTipoUnidad.Location = new System.Drawing.Point(692, 84);
            this.txtTipoUnidad.Name = "txtTipoUnidad";
            this.txtTipoUnidad.Size = new System.Drawing.Size(136, 25);
            this.txtTipoUnidad.TabIndex = 68;
            // 
            // txtidOrdenSer
            // 
            this.txtidOrdenSer.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtidOrdenSer.Location = new System.Drawing.Point(127, 48);
            this.txtidOrdenSer.Name = "txtidOrdenSer";
            this.txtidOrdenSer.Size = new System.Drawing.Size(86, 25);
            this.txtidOrdenSer.TabIndex = 37;
            this.txtidOrdenSer.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Blue;
            this.label6.Location = new System.Drawing.Point(12, 51);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(105, 18);
            this.label6.TabIndex = 38;
            this.label6.Text = "Orden Servicio:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtNomEmpresa
            // 
            this.txtNomEmpresa.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtNomEmpresa.Location = new System.Drawing.Point(149, 82);
            this.txtNomEmpresa.Name = "txtNomEmpresa";
            this.txtNomEmpresa.Size = new System.Drawing.Size(218, 25);
            this.txtNomEmpresa.TabIndex = 10;
            this.txtNomEmpresa.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtidOrdenTrabajo
            // 
            this.txtidOrdenTrabajo.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtidOrdenTrabajo.Location = new System.Drawing.Point(342, 48);
            this.txtidOrdenTrabajo.Name = "txtidOrdenTrabajo";
            this.txtidOrdenTrabajo.Size = new System.Drawing.Size(86, 25);
            this.txtidOrdenTrabajo.TabIndex = 9;
            this.txtidOrdenTrabajo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Blue;
            this.label8.Location = new System.Drawing.Point(227, 51);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(109, 18);
            this.label8.TabIndex = 25;
            this.label8.Text = "Orden Trabajo:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Blue;
            this.label3.Location = new System.Drawing.Point(11, 85);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 18);
            this.label3.TabIndex = 21;
            this.label3.Text = "Cliente:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dtpFecSolicitudHora
            // 
            this.dtpFecSolicitudHora.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.dtpFecSolicitudHora.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpFecSolicitudHora.Location = new System.Drawing.Point(583, 17);
            this.dtpFecSolicitudHora.Name = "dtpFecSolicitudHora";
            this.dtpFecSolicitudHora.Size = new System.Drawing.Size(143, 25);
            this.dtpFecSolicitudHora.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Blue;
            this.label2.Location = new System.Drawing.Point(236, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 18);
            this.label2.TabIndex = 10;
            this.label2.Text = "Fecha:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dtpFecSolicitud
            // 
            this.dtpFecSolicitud.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.dtpFecSolicitud.Location = new System.Drawing.Point(314, 17);
            this.dtpFecSolicitud.Name = "dtpFecSolicitud";
            this.dtpFecSolicitud.Size = new System.Drawing.Size(263, 25);
            this.dtpFecSolicitud.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(11, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 18);
            this.label1.TabIndex = 7;
            this.label1.Text = "Pre Salida:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtIdFOLIO
            // 
            this.txtIdFOLIO.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtIdFOLIO.Location = new System.Drawing.Point(96, 19);
            this.txtIdFOLIO.Name = "txtIdFOLIO";
            this.txtIdFOLIO.Size = new System.Drawing.Size(117, 25);
            this.txtIdFOLIO.TabIndex = 6;
            this.txtIdFOLIO.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // grdRef
            // 
            this.grdRef.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdRef.Location = new System.Drawing.Point(5, 304);
            this.grdRef.Name = "grdRef";
            this.grdRef.Size = new System.Drawing.Size(918, 191);
            this.grdRef.TabIndex = 14;
            this.grdRef.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdRef_CellContentClick);
            this.grdRef.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdRef_CellValueChanged);
            // 
            // cmbIdPersonalRec
            // 
            this.cmbIdPersonalRec.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.cmbIdPersonalRec.FormattingEnabled = true;
            this.cmbIdPersonalRec.Location = new System.Drawing.Point(149, 262);
            this.cmbIdPersonalRec.Name = "cmbIdPersonalRec";
            this.cmbIdPersonalRec.Size = new System.Drawing.Size(375, 26);
            this.cmbIdPersonalRec.TabIndex = 79;
            this.cmbIdPersonalRec.SelectedIndexChanged += new System.EventHandler(this.cmbIdPersonalRec_SelectedIndexChanged);
            this.cmbIdPersonalRec.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbIdPersonalRec_KeyPress);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Blue;
            this.label13.Location = new System.Drawing.Point(9, 265);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(56, 18);
            this.label13.TabIndex = 78;
            this.label13.Text = "Recibe:";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Blue;
            this.label4.Location = new System.Drawing.Point(11, 155);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 18);
            this.label4.TabIndex = 23;
            this.label4.Text = "Ayudantes:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtidPersonalAyu1
            // 
            this.txtidPersonalAyu1.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtidPersonalAyu1.Location = new System.Drawing.Point(149, 149);
            this.txtidPersonalAyu1.Name = "txtidPersonalAyu1";
            this.txtidPersonalAyu1.Size = new System.Drawing.Size(375, 27);
            this.txtidPersonalAyu1.TabIndex = 75;
            // 
            // txtidPersonalAyu2
            // 
            this.txtidPersonalAyu2.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtidPersonalAyu2.Location = new System.Drawing.Point(548, 149);
            this.txtidPersonalAyu2.Name = "txtidPersonalAyu2";
            this.txtidPersonalAyu2.Size = new System.Drawing.Size(375, 27);
            this.txtidPersonalAyu2.TabIndex = 76;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Blue;
            this.label9.Location = new System.Drawing.Point(131, 152);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(12, 18);
            this.label9.TabIndex = 77;
            this.label9.Text = "1";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Blue;
            this.label11.Location = new System.Drawing.Point(530, 152);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(16, 18);
            this.label11.TabIndex = 78;
            this.label11.Text = "2";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // fEntregaRefaccionesOS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(941, 521);
            this.Controls.Add(this.cmbIdPersonalRec);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.grdRef);
            this.Controls.Add(this.gpoFaC);
            this.Controls.Add(this.ToolStripMenu);
            this.Name = "fEntregaRefaccionesOS";
            this.Text = "Entregar Refacciones e Insumos";
            this.Load += new System.EventHandler(this.fEntregaRefaccionesOS_Load);
            this.ToolStripMenu.ResumeLayout(false);
            this.ToolStripMenu.PerformLayout();
            this.gpoFaC.ResumeLayout(false);
            this.gpoFaC.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdRef)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.ToolStrip ToolStripMenu;
        private System.Windows.Forms.ToolStripButton btnNuevo;
        private System.Windows.Forms.ToolStripButton btnGrabar;
        private System.Windows.Forms.ToolStripButton btnEditar;
        private System.Windows.Forms.ToolStripButton btnReporte;
        private System.Windows.Forms.ToolStripButton btnCancelar;
        private System.Windows.Forms.ToolStripButton btnSalir;
        private System.Windows.Forms.GroupBox gpoFaC;
        private System.Windows.Forms.TextBox txtNomEmpresa;
        private System.Windows.Forms.TextBox txtidOrdenTrabajo;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dtpFecSolicitudHora;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpFecSolicitud;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtIdFOLIO;
        private System.Windows.Forms.TextBox txtidOrdenSer;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtidUnidadTrans;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtTipoUnidad;
        private System.Windows.Forms.TextBox txtIdEmpleadoEnc;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtActividad;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.DataGridView grdRef;
        private System.Windows.Forms.ComboBox cmbIdPersonalRec;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtidPersonalAyu2;
        private System.Windows.Forms.TextBox txtidPersonalAyu1;
        private System.Windows.Forms.Label label4;
    }
}