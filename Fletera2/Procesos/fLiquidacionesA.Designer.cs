﻿namespace Fletera2.Procesos
{
    partial class fLiquidacionesA
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fLiquidacionesA));
            this.ToolStripMenu = new System.Windows.Forms.ToolStrip();
            this.btnNuevo = new System.Windows.Forms.ToolStripButton();
            this.btnGrabar = new System.Windows.Forms.ToolStripButton();
            this.btnEditar = new System.Windows.Forms.ToolStripButton();
            this.btnReporte = new System.Windows.Forms.ToolStripButton();
            this.btnCancelar = new System.Windows.Forms.ToolStripButton();
            this.btnSalir = new System.Windows.Forms.ToolStripButton();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tbRes = new System.Windows.Forms.TabPage();
            this.label21 = new System.Windows.Forms.Label();
            this.txtObservaciones = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtNoViajesRes = new System.Windows.Forms.TextBox();
            this.dtpFechaHora = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpFecha = new System.Windows.Forms.DateTimePicker();
            this.cmbidEmpresa = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtNomOperador = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtidOperador = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtNoLiquidacion = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtCombustibleRes = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtGastosComprobados = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtTotalaPagar = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtAnticiposTot = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtImporteComp = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtGastosxCompTot = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtViajesTot = new System.Windows.Forms.TextBox();
            this.tbViajes = new System.Windows.Forms.TabPage();
            this.label18 = new System.Windows.Forms.Label();
            this.txtNoViajes = new System.Windows.Forms.TextBox();
            this.grdViajes = new System.Windows.Forms.DataGridView();
            this.tbViajesDet = new System.Windows.Forms.TabPage();
            this.label5 = new System.Windows.Forms.Label();
            this.txtTotalViajeOp = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.grdViajesDet = new System.Windows.Forms.DataGridView();
            this.tbGxC = new System.Windows.Forms.TabPage();
            this.button4 = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.txtAnticposManTot = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtGasxCompTot = new System.Windows.Forms.TextBox();
            this.grdFolioDinero = new System.Windows.Forms.DataGridView();
            this.tbCompro = new System.Windows.Forms.TabPage();
            this.label8 = new System.Windows.Forms.Label();
            this.txtComprobacionTot = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.grdCompGastos = new System.Windows.Forms.DataGridView();
            this.tbOtrCon = new System.Windows.Forms.TabPage();
            this.button3 = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.txtOtrosConcTot = new System.Windows.Forms.TextBox();
            this.grdOtrosConc = new System.Windows.Forms.DataGridView();
            this.label3 = new System.Windows.Forms.Label();
            this.dtpFechaIni = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.dtpFechaFin = new System.Windows.Forms.DateTimePicker();
            this.btnConsultar = new System.Windows.Forms.Button();
            this.grdViajexOpe = new System.Windows.Forms.DataGridView();
            this.mnuLiq = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuOpcLiquidar = new System.Windows.Forms.ToolStripMenuItem();
            this.txtFiltro = new System.Windows.Forms.TextBox();
            this.ToolStripMenu.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tbRes.SuspendLayout();
            this.tbViajes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdViajes)).BeginInit();
            this.tbViajesDet.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdViajesDet)).BeginInit();
            this.tbGxC.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdFolioDinero)).BeginInit();
            this.tbCompro.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdCompGastos)).BeginInit();
            this.tbOtrCon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdOtrosConc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViajexOpe)).BeginInit();
            this.mnuLiq.SuspendLayout();
            this.SuspendLayout();
            // 
            // ToolStripMenu
            // 
            this.ToolStripMenu.AutoSize = false;
            this.ToolStripMenu.BackColor = System.Drawing.SystemColors.Control;
            this.ToolStripMenu.Font = new System.Drawing.Font("Berlin Sans FB", 11.25F);
            this.ToolStripMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnNuevo,
            this.btnGrabar,
            this.btnEditar,
            this.btnReporte,
            this.btnCancelar,
            this.btnSalir});
            this.ToolStripMenu.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.ToolStripMenu.Location = new System.Drawing.Point(0, 0);
            this.ToolStripMenu.Name = "ToolStripMenu";
            this.ToolStripMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.ToolStripMenu.Size = new System.Drawing.Size(812, 45);
            this.ToolStripMenu.TabIndex = 9;
            this.ToolStripMenu.Text = "ToolStrip1";
            // 
            // btnNuevo
            // 
            this.btnNuevo.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnNuevo.Image = ((System.Drawing.Image)(resources.GetObject("btnNuevo.Image")));
            this.btnNuevo.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnNuevo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(54, 42);
            this.btnNuevo.Text = "&Nuevo";
            this.btnNuevo.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnNuevo.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnNuevo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // btnGrabar
            // 
            this.btnGrabar.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnGrabar.Image = ((System.Drawing.Image)(resources.GetObject("btnGrabar.Image")));
            this.btnGrabar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnGrabar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnGrabar.Name = "btnGrabar";
            this.btnGrabar.Size = new System.Drawing.Size(60, 42);
            this.btnGrabar.Text = "&Grabar";
            this.btnGrabar.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnGrabar.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnGrabar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnGrabar.Click += new System.EventHandler(this.btnGrabar_Click);
            // 
            // btnEditar
            // 
            this.btnEditar.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnEditar.Image = ((System.Drawing.Image)(resources.GetObject("btnEditar.Image")));
            this.btnEditar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnEditar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(52, 42);
            this.btnEditar.Text = "&Editar";
            this.btnEditar.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnEditar.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnEditar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnEditar.Visible = false;
            // 
            // btnReporte
            // 
            this.btnReporte.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnReporte.Image = ((System.Drawing.Image)(resources.GetObject("btnReporte.Image")));
            this.btnReporte.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnReporte.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnReporte.Name = "btnReporte";
            this.btnReporte.Size = new System.Drawing.Size(65, 42);
            this.btnReporte.Text = "&Reporte";
            this.btnReporte.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnReporte.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnReporte.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnReporte.Click += new System.EventHandler(this.btnReporte_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnCancelar.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar.Image")));
            this.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnCancelar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(69, 42);
            this.btnCancelar.Text = "&Cancelar";
            this.btnCancelar.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnCancelar.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnSalir.Image = ((System.Drawing.Image)(resources.GetObject("btnSalir.Image")));
            this.btnSalir.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnSalir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(55, 42);
            this.btnSalir.Text = "&S a l i r";
            this.btnSalir.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnSalir.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tbRes);
            this.tabControl1.Controls.Add(this.tbViajes);
            this.tabControl1.Controls.Add(this.tbViajesDet);
            this.tabControl1.Controls.Add(this.tbGxC);
            this.tabControl1.Controls.Add(this.tbCompro);
            this.tabControl1.Controls.Add(this.tbOtrCon);
            this.tabControl1.Location = new System.Drawing.Point(4, 230);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(796, 256);
            this.tabControl1.TabIndex = 17;
            // 
            // tbRes
            // 
            this.tbRes.Controls.Add(this.label21);
            this.tbRes.Controls.Add(this.txtObservaciones);
            this.tbRes.Controls.Add(this.label20);
            this.tbRes.Controls.Add(this.txtNoViajesRes);
            this.tbRes.Controls.Add(this.dtpFechaHora);
            this.tbRes.Controls.Add(this.label2);
            this.tbRes.Controls.Add(this.dtpFecha);
            this.tbRes.Controls.Add(this.cmbidEmpresa);
            this.tbRes.Controls.Add(this.label7);
            this.tbRes.Controls.Add(this.txtNomOperador);
            this.tbRes.Controls.Add(this.label1);
            this.tbRes.Controls.Add(this.txtidOperador);
            this.tbRes.Controls.Add(this.label16);
            this.tbRes.Controls.Add(this.txtNoLiquidacion);
            this.tbRes.Controls.Add(this.label17);
            this.tbRes.Controls.Add(this.txtCombustibleRes);
            this.tbRes.Controls.Add(this.label14);
            this.tbRes.Controls.Add(this.txtGastosComprobados);
            this.tbRes.Controls.Add(this.label13);
            this.tbRes.Controls.Add(this.txtTotalaPagar);
            this.tbRes.Controls.Add(this.label12);
            this.tbRes.Controls.Add(this.txtAnticiposTot);
            this.tbRes.Controls.Add(this.label11);
            this.tbRes.Controls.Add(this.txtImporteComp);
            this.tbRes.Controls.Add(this.label10);
            this.tbRes.Controls.Add(this.txtGastosxCompTot);
            this.tbRes.Controls.Add(this.label9);
            this.tbRes.Controls.Add(this.txtViajesTot);
            this.tbRes.Location = new System.Drawing.Point(4, 22);
            this.tbRes.Name = "tbRes";
            this.tbRes.Padding = new System.Windows.Forms.Padding(3);
            this.tbRes.Size = new System.Drawing.Size(788, 230);
            this.tbRes.TabIndex = 0;
            this.tbRes.Text = "Resumen";
            this.tbRes.UseVisualStyleBackColor = true;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Blue;
            this.label21.Location = new System.Drawing.Point(320, 170);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(105, 18);
            this.label21.TabIndex = 64;
            this.label21.Text = "Observaciones:";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtObservaciones
            // 
            this.txtObservaciones.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtObservaciones.Location = new System.Drawing.Point(320, 194);
            this.txtObservaciones.MaxLength = 100;
            this.txtObservaciones.Name = "txtObservaciones";
            this.txtObservaciones.Size = new System.Drawing.Size(462, 25);
            this.txtObservaciones.TabIndex = 63;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Blue;
            this.label20.Location = new System.Drawing.Point(317, 144);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(72, 18);
            this.label20.TabIndex = 62;
            this.label20.Text = "No. Viajes";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtNoViajesRes
            // 
            this.txtNoViajesRes.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtNoViajesRes.Location = new System.Drawing.Point(408, 141);
            this.txtNoViajesRes.Name = "txtNoViajesRes";
            this.txtNoViajesRes.Size = new System.Drawing.Size(123, 25);
            this.txtNoViajesRes.TabIndex = 61;
            this.txtNoViajesRes.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // dtpFechaHora
            // 
            this.dtpFechaHora.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.dtpFechaHora.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpFechaHora.Location = new System.Drawing.Point(677, 110);
            this.dtpFechaHora.Name = "dtpFechaHora";
            this.dtpFechaHora.Size = new System.Drawing.Size(106, 25);
            this.dtpFechaHora.TabIndex = 60;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Blue;
            this.label2.Location = new System.Drawing.Point(325, 115);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 18);
            this.label2.TabIndex = 59;
            this.label2.Text = "Fecha:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dtpFecha
            // 
            this.dtpFecha.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.dtpFecha.Location = new System.Drawing.Point(408, 110);
            this.dtpFecha.Name = "dtpFecha";
            this.dtpFecha.Size = new System.Drawing.Size(263, 25);
            this.dtpFecha.TabIndex = 58;
            // 
            // cmbidEmpresa
            // 
            this.cmbidEmpresa.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.cmbidEmpresa.FormattingEnabled = true;
            this.cmbidEmpresa.Location = new System.Drawing.Point(408, 73);
            this.cmbidEmpresa.Name = "cmbidEmpresa";
            this.cmbidEmpresa.Size = new System.Drawing.Size(208, 26);
            this.cmbidEmpresa.TabIndex = 57;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Blue;
            this.label7.Location = new System.Drawing.Point(325, 76);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(70, 18);
            this.label7.TabIndex = 56;
            this.label7.Text = "Empresa:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // txtNomOperador
            // 
            this.txtNomOperador.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtNomOperador.Location = new System.Drawing.Point(493, 42);
            this.txtNomOperador.MaxLength = 100;
            this.txtNomOperador.Name = "txtNomOperador";
            this.txtNomOperador.Size = new System.Drawing.Size(289, 25);
            this.txtNomOperador.TabIndex = 55;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(325, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 18);
            this.label1.TabIndex = 54;
            this.label1.Text = "Operador:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtidOperador
            // 
            this.txtidOperador.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtidOperador.Location = new System.Drawing.Point(408, 42);
            this.txtidOperador.Name = "txtidOperador";
            this.txtidOperador.Size = new System.Drawing.Size(79, 25);
            this.txtidOperador.TabIndex = 53;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Blue;
            this.label16.Location = new System.Drawing.Point(317, 14);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(97, 18);
            this.label16.TabIndex = 52;
            this.label16.Text = "Liquidacion #";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtNoLiquidacion
            // 
            this.txtNoLiquidacion.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtNoLiquidacion.Location = new System.Drawing.Point(419, 11);
            this.txtNoLiquidacion.Name = "txtNoLiquidacion";
            this.txtNoLiquidacion.Size = new System.Drawing.Size(68, 25);
            this.txtNoLiquidacion.TabIndex = 51;
            this.txtNoLiquidacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Blue;
            this.label17.Location = new System.Drawing.Point(75, 45);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(107, 18);
            this.label17.TabIndex = 50;
            this.label17.Text = "Combustible : $";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtCombustibleRes
            // 
            this.txtCombustibleRes.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtCombustibleRes.Location = new System.Drawing.Point(188, 42);
            this.txtCombustibleRes.Name = "txtCombustibleRes";
            this.txtCombustibleRes.Size = new System.Drawing.Size(123, 25);
            this.txtCombustibleRes.TabIndex = 49;
            this.txtCombustibleRes.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Blue;
            this.label14.Location = new System.Drawing.Point(16, 141);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(162, 18);
            this.label14.TabIndex = 48;
            this.label14.Text = "Gastos Comprobados: $";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtGastosComprobados
            // 
            this.txtGastosComprobados.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtGastosComprobados.Location = new System.Drawing.Point(188, 138);
            this.txtGastosComprobados.Name = "txtGastosComprobados";
            this.txtGastosComprobados.Size = new System.Drawing.Size(123, 25);
            this.txtGastosComprobados.TabIndex = 47;
            this.txtGastosComprobados.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Blue;
            this.label13.Location = new System.Drawing.Point(64, 201);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(114, 18);
            this.label13.TabIndex = 46;
            this.label13.Text = "Total a Pagar: $";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtTotalaPagar
            // 
            this.txtTotalaPagar.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtTotalaPagar.Location = new System.Drawing.Point(188, 198);
            this.txtTotalaPagar.Name = "txtTotalaPagar";
            this.txtTotalaPagar.Size = new System.Drawing.Size(123, 25);
            this.txtTotalaPagar.TabIndex = 45;
            this.txtTotalaPagar.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Blue;
            this.label12.Location = new System.Drawing.Point(3, 110);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(177, 18);
            this.label12.TabIndex = 44;
            this.label12.Text = "Anticipos de Maniobras : $";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAnticiposTot
            // 
            this.txtAnticiposTot.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtAnticiposTot.Location = new System.Drawing.Point(188, 107);
            this.txtAnticiposTot.Name = "txtAnticiposTot";
            this.txtAnticiposTot.Size = new System.Drawing.Size(123, 25);
            this.txtAnticiposTot.TabIndex = 43;
            this.txtAnticiposTot.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Blue;
            this.label11.Location = new System.Drawing.Point(50, 172);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(128, 18);
            this.label11.TabIndex = 42;
            this.label11.Text = "Otros Conceptos: $";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label11.Click += new System.EventHandler(this.label11_Click);
            // 
            // txtImporteComp
            // 
            this.txtImporteComp.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtImporteComp.Location = new System.Drawing.Point(188, 169);
            this.txtImporteComp.Name = "txtImporteComp";
            this.txtImporteComp.Size = new System.Drawing.Size(123, 25);
            this.txtImporteComp.TabIndex = 41;
            this.txtImporteComp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Blue;
            this.label10.Location = new System.Drawing.Point(18, 79);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(160, 18);
            this.label10.TabIndex = 40;
            this.label10.Text = "Gastos x Comprobar: $ ";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtGastosxCompTot
            // 
            this.txtGastosxCompTot.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtGastosxCompTot.Location = new System.Drawing.Point(188, 76);
            this.txtGastosxCompTot.Name = "txtGastosxCompTot";
            this.txtGastosxCompTot.Size = new System.Drawing.Size(123, 25);
            this.txtGastosxCompTot.TabIndex = 39;
            this.txtGastosxCompTot.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Blue;
            this.label9.Location = new System.Drawing.Point(117, 14);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(61, 18);
            this.label9.TabIndex = 38;
            this.label9.Text = "Viajes: $";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtViajesTot
            // 
            this.txtViajesTot.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtViajesTot.Location = new System.Drawing.Point(188, 11);
            this.txtViajesTot.Name = "txtViajesTot";
            this.txtViajesTot.Size = new System.Drawing.Size(123, 25);
            this.txtViajesTot.TabIndex = 37;
            this.txtViajesTot.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbViajes
            // 
            this.tbViajes.Controls.Add(this.label18);
            this.tbViajes.Controls.Add(this.txtNoViajes);
            this.tbViajes.Controls.Add(this.grdViajes);
            this.tbViajes.Location = new System.Drawing.Point(4, 22);
            this.tbViajes.Name = "tbViajes";
            this.tbViajes.Size = new System.Drawing.Size(788, 230);
            this.tbViajes.TabIndex = 5;
            this.tbViajes.Text = "Viajes";
            this.tbViajes.UseVisualStyleBackColor = true;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Blue;
            this.label18.Location = new System.Drawing.Point(567, 199);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(72, 18);
            this.label18.TabIndex = 49;
            this.label18.Text = "No. Viajes";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtNoViajes
            // 
            this.txtNoViajes.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtNoViajes.Location = new System.Drawing.Point(658, 196);
            this.txtNoViajes.Name = "txtNoViajes";
            this.txtNoViajes.Size = new System.Drawing.Size(123, 25);
            this.txtNoViajes.TabIndex = 48;
            // 
            // grdViajes
            // 
            this.grdViajes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdViajes.Location = new System.Drawing.Point(7, 9);
            this.grdViajes.Name = "grdViajes";
            this.grdViajes.Size = new System.Drawing.Size(774, 181);
            this.grdViajes.TabIndex = 47;
            // 
            // tbViajesDet
            // 
            this.tbViajesDet.Controls.Add(this.label5);
            this.tbViajesDet.Controls.Add(this.txtTotalViajeOp);
            this.tbViajesDet.Controls.Add(this.button1);
            this.tbViajesDet.Controls.Add(this.grdViajesDet);
            this.tbViajesDet.Location = new System.Drawing.Point(4, 22);
            this.tbViajesDet.Name = "tbViajesDet";
            this.tbViajesDet.Padding = new System.Windows.Forms.Padding(3);
            this.tbViajesDet.Size = new System.Drawing.Size(788, 230);
            this.tbViajesDet.TabIndex = 1;
            this.tbViajesDet.Text = "Viajes Detalle";
            this.tbViajesDet.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Blue;
            this.label5.Location = new System.Drawing.Point(593, 196);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 18);
            this.label5.TabIndex = 46;
            this.label5.Text = "Total : $";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtTotalViajeOp
            // 
            this.txtTotalViajeOp.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtTotalViajeOp.Location = new System.Drawing.Point(659, 193);
            this.txtTotalViajeOp.Name = "txtTotalViajeOp";
            this.txtTotalViajeOp.Size = new System.Drawing.Size(123, 25);
            this.txtTotalViajeOp.TabIndex = 45;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(8, 201);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 41;
            this.button1.Text = "Actualizar";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // grdViajesDet
            // 
            this.grdViajesDet.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdViajesDet.Location = new System.Drawing.Point(8, 6);
            this.grdViajesDet.Name = "grdViajesDet";
            this.grdViajesDet.Size = new System.Drawing.Size(774, 181);
            this.grdViajesDet.TabIndex = 0;
            // 
            // tbGxC
            // 
            this.tbGxC.Controls.Add(this.button4);
            this.tbGxC.Controls.Add(this.label19);
            this.tbGxC.Controls.Add(this.txtAnticposManTot);
            this.tbGxC.Controls.Add(this.label6);
            this.tbGxC.Controls.Add(this.txtGasxCompTot);
            this.tbGxC.Controls.Add(this.grdFolioDinero);
            this.tbGxC.Location = new System.Drawing.Point(4, 22);
            this.tbGxC.Name = "tbGxC";
            this.tbGxC.Size = new System.Drawing.Size(788, 230);
            this.tbGxC.TabIndex = 2;
            this.tbGxC.Text = "Gastos x Combrobar";
            this.tbGxC.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.AutoSize = true;
            this.button4.Location = new System.Drawing.Point(8, 202);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(104, 23);
            this.button4.TabIndex = 49;
            this.button4.Text = "Entrega Dinero";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Blue;
            this.label19.Location = new System.Drawing.Point(188, 203);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(173, 18);
            this.label19.TabIndex = 48;
            this.label19.Text = "Anticipos de Maniobras: $";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAnticposManTot
            // 
            this.txtAnticposManTot.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtAnticposManTot.Location = new System.Drawing.Point(367, 200);
            this.txtAnticposManTot.Name = "txtAnticposManTot";
            this.txtAnticposManTot.Size = new System.Drawing.Size(123, 25);
            this.txtAnticposManTot.TabIndex = 47;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Blue;
            this.label6.Location = new System.Drawing.Point(497, 203);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(156, 18);
            this.label6.TabIndex = 46;
            this.label6.Text = "Gastos x Comprobar: $";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtGasxCompTot
            // 
            this.txtGasxCompTot.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtGasxCompTot.Location = new System.Drawing.Point(659, 200);
            this.txtGasxCompTot.Name = "txtGasxCompTot";
            this.txtGasxCompTot.Size = new System.Drawing.Size(123, 25);
            this.txtGasxCompTot.TabIndex = 45;
            // 
            // grdFolioDinero
            // 
            this.grdFolioDinero.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdFolioDinero.Location = new System.Drawing.Point(8, 13);
            this.grdFolioDinero.Name = "grdFolioDinero";
            this.grdFolioDinero.Size = new System.Drawing.Size(774, 181);
            this.grdFolioDinero.TabIndex = 1;
            // 
            // tbCompro
            // 
            this.tbCompro.Controls.Add(this.label8);
            this.tbCompro.Controls.Add(this.txtComprobacionTot);
            this.tbCompro.Controls.Add(this.button2);
            this.tbCompro.Controls.Add(this.grdCompGastos);
            this.tbCompro.Location = new System.Drawing.Point(4, 22);
            this.tbCompro.Name = "tbCompro";
            this.tbCompro.Size = new System.Drawing.Size(788, 230);
            this.tbCompro.TabIndex = 4;
            this.tbCompro.Text = "Comprobacion Gastos";
            this.tbCompro.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Blue;
            this.label8.Location = new System.Drawing.Point(593, 205);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(60, 18);
            this.label8.TabIndex = 46;
            this.label8.Text = "Total : $";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtComprobacionTot
            // 
            this.txtComprobacionTot.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtComprobacionTot.Location = new System.Drawing.Point(659, 202);
            this.txtComprobacionTot.Name = "txtComprobacionTot";
            this.txtComprobacionTot.Size = new System.Drawing.Size(123, 25);
            this.txtComprobacionTot.TabIndex = 45;
            // 
            // button2
            // 
            this.button2.AutoSize = true;
            this.button2.Location = new System.Drawing.Point(8, 200);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(104, 23);
            this.button2.TabIndex = 44;
            this.button2.Text = "Comprobar Gastos";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // grdCompGastos
            // 
            this.grdCompGastos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdCompGastos.Location = new System.Drawing.Point(8, 16);
            this.grdCompGastos.Name = "grdCompGastos";
            this.grdCompGastos.Size = new System.Drawing.Size(774, 181);
            this.grdCompGastos.TabIndex = 2;
            // 
            // tbOtrCon
            // 
            this.tbOtrCon.Controls.Add(this.button3);
            this.tbOtrCon.Controls.Add(this.label15);
            this.tbOtrCon.Controls.Add(this.txtOtrosConcTot);
            this.tbOtrCon.Controls.Add(this.grdOtrosConc);
            this.tbOtrCon.Location = new System.Drawing.Point(4, 22);
            this.tbOtrCon.Name = "tbOtrCon";
            this.tbOtrCon.Size = new System.Drawing.Size(788, 230);
            this.tbOtrCon.TabIndex = 3;
            this.tbOtrCon.Text = "Otros Conceptos";
            this.tbOtrCon.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.AutoSize = true;
            this.button3.Location = new System.Drawing.Point(8, 204);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(136, 23);
            this.button3.TabIndex = 45;
            this.button3.Text = "Agregar Otros Conceptos";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Blue;
            this.label15.Location = new System.Drawing.Point(585, 204);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(60, 18);
            this.label15.TabIndex = 44;
            this.label15.Text = "Total : $";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtOtrosConcTot
            // 
            this.txtOtrosConcTot.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtOtrosConcTot.Location = new System.Drawing.Point(651, 201);
            this.txtOtrosConcTot.Name = "txtOtrosConcTot";
            this.txtOtrosConcTot.Size = new System.Drawing.Size(123, 25);
            this.txtOtrosConcTot.TabIndex = 43;
            // 
            // grdOtrosConc
            // 
            this.grdOtrosConc.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdOtrosConc.Location = new System.Drawing.Point(8, 14);
            this.grdOtrosConc.Name = "grdOtrosConc";
            this.grdOtrosConc.Size = new System.Drawing.Size(766, 181);
            this.grdOtrosConc.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Blue;
            this.label3.Location = new System.Drawing.Point(4, 55);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 18);
            this.label3.TabIndex = 25;
            this.label3.Text = "Rango:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dtpFechaIni
            // 
            this.dtpFechaIni.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.dtpFechaIni.Location = new System.Drawing.Point(106, 48);
            this.dtpFechaIni.Name = "dtpFechaIni";
            this.dtpFechaIni.Size = new System.Drawing.Size(290, 25);
            this.dtpFechaIni.TabIndex = 24;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Blue;
            this.label4.Location = new System.Drawing.Point(424, 53);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 18);
            this.label4.TabIndex = 26;
            this.label4.Text = "AL ";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dtpFechaFin
            // 
            this.dtpFechaFin.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.dtpFechaFin.Location = new System.Drawing.Point(470, 48);
            this.dtpFechaFin.Name = "dtpFechaFin";
            this.dtpFechaFin.Size = new System.Drawing.Size(290, 25);
            this.dtpFechaFin.TabIndex = 27;
            // 
            // btnConsultar
            // 
            this.btnConsultar.Location = new System.Drawing.Point(685, 185);
            this.btnConsultar.Name = "btnConsultar";
            this.btnConsultar.Size = new System.Drawing.Size(75, 36);
            this.btnConsultar.TabIndex = 30;
            this.btnConsultar.Text = "Consultar";
            this.btnConsultar.UseVisualStyleBackColor = true;
            this.btnConsultar.Click += new System.EventHandler(this.btnConsultar_Click);
            // 
            // grdViajexOpe
            // 
            this.grdViajexOpe.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdViajexOpe.Location = new System.Drawing.Point(4, 110);
            this.grdViajexOpe.Name = "grdViajexOpe";
            this.grdViajexOpe.Size = new System.Drawing.Size(671, 111);
            this.grdViajexOpe.TabIndex = 31;
            // 
            // mnuLiq
            // 
            this.mnuLiq.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuOpcLiquidar});
            this.mnuLiq.Name = "mnuLiq";
            this.mnuLiq.Size = new System.Drawing.Size(118, 26);
            // 
            // mnuOpcLiquidar
            // 
            this.mnuOpcLiquidar.Name = "mnuOpcLiquidar";
            this.mnuOpcLiquidar.Size = new System.Drawing.Size(117, 22);
            this.mnuOpcLiquidar.Text = "Liquidar";
            this.mnuOpcLiquidar.Click += new System.EventHandler(this.mnuOpcLiquidar_Click);
            // 
            // txtFiltro
            // 
            this.txtFiltro.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtFiltro.Location = new System.Drawing.Point(106, 79);
            this.txtFiltro.MaxLength = 100;
            this.txtFiltro.Name = "txtFiltro";
            this.txtFiltro.Size = new System.Drawing.Size(263, 25);
            this.txtFiltro.TabIndex = 56;
            this.txtFiltro.TextChanged += new System.EventHandler(this.txtFiltro_TextChanged);
            // 
            // fLiquidacionesA
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(812, 498);
            this.ContextMenuStrip = this.mnuLiq;
            this.Controls.Add(this.txtFiltro);
            this.Controls.Add(this.grdViajexOpe);
            this.Controls.Add(this.btnConsultar);
            this.Controls.Add(this.dtpFechaFin);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dtpFechaIni);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.ToolStripMenu);
            this.Name = "fLiquidacionesA";
            this.Text = "fLiquidacionesA";
            this.Load += new System.EventHandler(this.fLiquidacionesA_Load);
            this.ToolStripMenu.ResumeLayout(false);
            this.ToolStripMenu.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tbRes.ResumeLayout(false);
            this.tbRes.PerformLayout();
            this.tbViajes.ResumeLayout(false);
            this.tbViajes.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdViajes)).EndInit();
            this.tbViajesDet.ResumeLayout(false);
            this.tbViajesDet.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdViajesDet)).EndInit();
            this.tbGxC.ResumeLayout(false);
            this.tbGxC.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdFolioDinero)).EndInit();
            this.tbCompro.ResumeLayout(false);
            this.tbCompro.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdCompGastos)).EndInit();
            this.tbOtrCon.ResumeLayout(false);
            this.tbOtrCon.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdOtrosConc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViajexOpe)).EndInit();
            this.mnuLiq.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.ToolStrip ToolStripMenu;
        private System.Windows.Forms.ToolStripButton btnNuevo;
        private System.Windows.Forms.ToolStripButton btnGrabar;
        private System.Windows.Forms.ToolStripButton btnEditar;
        private System.Windows.Forms.ToolStripButton btnReporte;
        private System.Windows.Forms.ToolStripButton btnCancelar;
        private System.Windows.Forms.ToolStripButton btnSalir;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tbRes;
        private System.Windows.Forms.TabPage tbViajesDet;
        private System.Windows.Forms.TabPage tbGxC;
        private System.Windows.Forms.TabPage tbOtrCon;
        private System.Windows.Forms.TabPage tbCompro;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dtpFechaIni;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dtpFechaFin;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtTotalaPagar;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtAnticiposTot;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtImporteComp;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtGastosxCompTot;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtViajesTot;
        private System.Windows.Forms.DataGridView grdViajesDet;
        private System.Windows.Forms.DataGridView grdFolioDinero;
        private System.Windows.Forms.DataGridView grdCompGastos;
        private System.Windows.Forms.DataGridView grdOtrosConc;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtGastosComprobados;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtCombustibleRes;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtTotalViajeOp;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtGasxCompTot;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtComprobacionTot;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtOtrosConcTot;
        private System.Windows.Forms.Button btnConsultar;
        private System.Windows.Forms.DateTimePicker dtpFechaHora;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpFecha;
        private System.Windows.Forms.ComboBox cmbidEmpresa;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtNomOperador;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtidOperador;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtNoLiquidacion;
        private System.Windows.Forms.DataGridView grdViajexOpe;
        private System.Windows.Forms.ContextMenuStrip mnuLiq;
        private System.Windows.Forms.ToolStripMenuItem mnuOpcLiquidar;
        private System.Windows.Forms.TextBox txtFiltro;
        private System.Windows.Forms.TabPage tbViajes;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtNoViajes;
        private System.Windows.Forms.DataGridView grdViajes;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtAnticposManTot;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtNoViajesRes;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtObservaciones;
        private System.Windows.Forms.Button button4;
    }
}