﻿namespace Fletera2.Procesos
{
    using Core.Models;
    using Fletera2.Clases;
    using Fletera2.Comunes;
    using Fletera2.Properties;
    using Fletera2Entidades;
    using Fletera2Negocio;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Drawing;
    using System.Drawing.Printing;
    using System.Runtime.InteropServices;
    using System.Windows.Forms;

    public partial class fSolicitudOS : Form
    {
        private Usuario _user;
        private int IdEmpresa;
        private List<CatPersonal> listaOperadores;
        private List<CatPersonal> listaAutorizaPer;
        private List<CatEmpresas> listaClientes;
        private List<CatUnidadTrans> listaTractor;
        private List<CatUnidadTrans> listaRemolques;
        private List<CatUnidadTrans> listaDollys;
        private List<CatTipoOrdServ> listaTipoOS;
        private CabGuia UltViaje;
        private List<CabOrdenServicio> listaCabOrdenesServ;
        private MasterOrdServ_f2 folioPadre;
        private List<DetOrdenServicio> listaDetOrdenesServ;
        private List<DetRecepcionOS> listaDetRecepcionOS;
        private List<CatUnidadTrans> listaUnidadesaEnviar;
        private int NoOS = 0;
        private OperationResult resp;
        private bool BandTicket = false;
        private CabOrdenServicio cabosImp;
        private List<DetOrdenServicio> listaDetOsImp;
        private Inicio.opcForma opcionForma;
        private bool bMultEmpresa = false;
        private PrinterSettings prtSettings;
        private PrintDocument prtDoc;
        private Fletera2.Clases.ImprimeOrdenServicio ImpRep = new Fletera2.Clases.ImprimeOrdenServicio();
        private Font printFont = new Font("Courier New", 7f);
        private int lineaActual = 0;
        private int ContPaginas = 0;
        private int NumRegistros = 1;
        

        public fSolicitudOS(Usuario user)
        {
            this._user = user;
            this.InitializeComponent();
        }

        private void ActivaCampos(opcHabilita opc)
        {
            switch (opc)
            {
                case opcHabilita.DesHabilitaTodos:
                    this.gpoDatos.Enabled = false;
                    this.gpoOs.Enabled = false;
                    break;

                case opcHabilita.Iniciando:
                    this.gpoDatos.Enabled = true;
                    this.txtidPadreOrdSer.Enabled = false;
                    this.dtpFecha.Enabled = false;
                    this.dtpFechaHora.Enabled = false;
                    this.cmbIdEmpresa.Enabled = false;
                    this.txtEstatus.Enabled = false;
                    this.txtTractor.Enabled = false;
                    this.txtRemolque1.Enabled = false;
                    this.txtDolly.Enabled = false;
                    this.txtRemolque2.Enabled = false;
                    this.cmbidOperador.Enabled = false;
                    this.cmbidEmpleadoAutoriza.Enabled = false;
                    this.chkBoxTipoOrdServ.Enabled = false;
                    this.gpoOs.Enabled = true;
                    break;

                case opcHabilita.Nuevo:
                    this.gpoDatos.Enabled = true;
                    this.txtidPadreOrdSer.Enabled = false;
                    this.dtpFecha.Enabled = false;
                    this.dtpFechaHora.Enabled = false;
                    this.cmbIdEmpresa.Enabled = false;
                    this.txtEstatus.Enabled = false;
                    this.txtTractor.Enabled = true;
                    this.txtRemolque1.Enabled = true;
                    this.txtDolly.Enabled = true;
                    this.txtRemolque2.Enabled = true;
                    this.cmbidOperador.Enabled = true;
                    this.cmbidEmpleadoAutoriza.Enabled = true;
                    this.chkBoxTipoOrdServ.Enabled = true;
                    this.gpoOs.Enabled = true;
                    break;

                case opcHabilita.Reporte:
                    this.gpoDatos.Enabled = true;
                    this.txtidPadreOrdSer.Enabled = true;
                    this.dtpFecha.Enabled = false;
                    this.dtpFechaHora.Enabled = false;
                    this.cmbIdEmpresa.Enabled = false;
                    this.txtEstatus.Enabled = false;
                    this.txtTractor.Enabled = false;
                    this.txtRemolque1.Enabled = false;
                    this.txtDolly.Enabled = false;
                    this.txtRemolque2.Enabled = false;
                    this.cmbidOperador.Enabled = false;
                    this.cmbidEmpleadoAutoriza.Enabled = false;
                    this.chkBoxTipoOrdServ.Enabled = false;
                    this.gpoOs.Enabled = true;
                    break;
            }
        }

        private void AgregarListas()
        {
            foreach (CabOrdenServicio servicio in this.listaCabOrdenesServ)
            {
                foreach (DetOrdenServicio det in this.listaDetOrdenesServ)
                {
                    det.DetRec = new DetRecepcionOS();
                    det.DetRec = this.listaDetRecepcionOS.Find(s => (s.idOrdenTrabajo == det.idOrdenTrabajo) && (s.idOrdenSer == det.idOrdenSer));
                }
            }
            foreach (CabOrdenServicio cab in this.listaCabOrdenesServ)
            {
                cab.idEmpleadoAutoriza = ((CatPersonal)this.cmbidEmpleadoAutoriza.SelectedItem).idPersonal;
                cab.FechaRecepcion = DateTime.Now;
                cab.idEmpleadoEntrega = 0;
                cab.UsuarioRecepcion = "";
                cab.NotaFinal = "";
                cab.UsuarioEntrega = "";
                cab.listaDet = new List<DetOrdenServicio>();
                cab.listaDet = this.listaDetOrdenesServ.FindAll(s => s.idOrdenSer == cab.idOrdenSer);
            }
        }

        private void AgregaUnidadListaEnvio(string idUnidadTrans)
        {
            OperationResult result = new CatUnidadTransSvc().geCatUnidadTransxFilter(idUnidadTrans, "", "");
            if (result.typeResult == ResultTypes.success)
            {
                CatUnidadTrans item = ((List<CatUnidadTrans>)result.result)[0];
                this.listaUnidadesaEnviar.Add(item);
            }
        }

        private void AutoAcompletarTexto(TextBox control, int idEmpresa, Inicio.eTipoUnidad tipo)
        {
            OperationResult result;
            AutoCompleteStringCollection strings = new AutoCompleteStringCollection();
            switch (tipo)
            {
                case Inicio.eTipoUnidad.Tractor:
                    result = new CatUnidadTransSvc().geCatUnidadTransxFilter("", " t.TipoUso = 'T' and c.idEmpresa = " + this.IdEmpresa, " c.idUnidadTrans ASC");
                    break;

                case Inicio.eTipoUnidad.Remolque:
                    result = new CatUnidadTransSvc().geCatUnidadTransxFilter("", " t.TipoUso = 'R' and c.idEmpresa = " + this.IdEmpresa, " c.idUnidadTrans ASC");
                    break;

                case Inicio.eTipoUnidad.Dolly:
                    result = new CatUnidadTransSvc().geCatUnidadTransxFilter("", " t.TipoUso = 'D' and c.idEmpresa = " + this.IdEmpresa, " c.idUnidadTrans ASC");
                    break;

                default:
                    result = new CatUnidadTransSvc().geCatUnidadTransxFilter("", " c.idEmpresa = " + this.IdEmpresa, " c.idUnidadTrans ASC");
                    break;
            }
            if (result.typeResult == ResultTypes.success)
            {
                switch (tipo)
                {
                    case Inicio.eTipoUnidad.Tractor:
                        this.listaTractor = (List<CatUnidadTrans>)result.result;
                        foreach (CatUnidadTrans trans in this.listaTractor)
                        {
                            strings.Add(Convert.ToString(trans.idUnidadTrans));
                        }
                        break;

                    case Inicio.eTipoUnidad.Remolque:
                        this.listaRemolques = (List<CatUnidadTrans>)result.result;
                        foreach (CatUnidadTrans trans2 in this.listaRemolques)
                        {
                            strings.Add(Convert.ToString(trans2.idUnidadTrans));
                        }
                        break;

                    case Inicio.eTipoUnidad.Dolly:
                        this.listaDollys = (List<CatUnidadTrans>)result.result;
                        foreach (CatUnidadTrans trans3 in this.listaDollys)
                        {
                            strings.Add(Convert.ToString(trans3.idUnidadTrans));
                        }
                        break;

                    default:
                        this.listaTractor = (List<CatUnidadTrans>)result.result;
                        foreach (CatUnidadTrans trans4 in this.listaDollys)
                        {
                            strings.Add(Convert.ToString(trans4.idUnidadTrans));
                        }
                        break;
                }
                control.AutoCompleteCustomSource = strings;
                control.AutoCompleteMode = AutoCompleteMode.Suggest;
                control.AutoCompleteSource = AutoCompleteSource.CustomSource;
            }
        }

        private void btnAgregaOT_Click(object sender, EventArgs e)
        {
            try
            {
                if ((this.chkBoxTipoOrdServ.CheckedItems.Count != 0) && ((((this.txtTractor.Text != "") || (this.txtDolly.Text != "")) || (this.txtRemolque1.Text != "")) || (this.txtRemolque2.Text != "")))
                {
                    this.listaUnidadesaEnviar = new List<CatUnidadTrans>();
                    if (this.txtTractor.Text != "")
                    {
                        this.AgregaUnidadListaEnvio(this.txtTractor.Text);
                    }
                    if (this.txtRemolque1.Text != "")
                    {
                        this.AgregaUnidadListaEnvio(this.txtRemolque1.Text);
                    }
                    if (this.txtDolly.Text != "")
                    {
                        this.AgregaUnidadListaEnvio(this.txtDolly.Text);
                    }
                    if (this.txtRemolque2.Text != "")
                    {
                        this.AgregaUnidadListaEnvio(this.txtRemolque2.Text);
                    }
                    foreach (object obj2 in this.chkBoxTipoOrdServ.CheckedItems)
                    {
                        CatTipoOrdServ tipoOrdServ = (CatTipoOrdServ)obj2;
                        fOrdenTrabajo trabajo = new fOrdenTrabajo(this._user, this.listaUnidadesaEnviar, tipoOrdServ, false, this.NoOS, 0, 0, 0, 0, 0)
                        {
                            StartPosition = FormStartPosition.CenterParent
                        };
                        trabajo.DevuelveValor(((CatEmpresas)this.cmbIdEmpresa.SelectedItem).idEmpresa);
                        if (this.listaCabOrdenesServ == null)
                        {
                            if (trabajo.listaCabOS != null)
                            {
                                this.listaCabOrdenesServ = new List<CabOrdenServicio>();
                                this.listaCabOrdenesServ = trabajo.listaCabOS;
                                foreach (CabOrdenServicio servicio in trabajo.listaCabOS)
                                {
                                    this.NoOS = servicio.idOrdenSer;
                                }
                            }
                        }
                        else if (trabajo.listaCabOS != null)
                        {
                            foreach (CabOrdenServicio servicio3 in trabajo.listaCabOS)
                            {
                                CabOrdenServicio item = servicio3;
                                this.listaCabOrdenesServ.Add(item);
                                this.NoOS = servicio3.idOrdenSer;
                            }
                        }
                        if (this.listaCabOrdenesServ != null)
                        {
                            this.grdCabOs.DataSource = null;
                            this.grdCabOs.DataSource = this.listaCabOrdenesServ;
                            this.FormatogrdCab();
                        }
                        if (this.listaDetOrdenesServ == null)
                        {
                            if (trabajo.listaDetOS != null)
                            {
                                this.listaDetOrdenesServ = new List<DetOrdenServicio>();
                                this.listaDetOrdenesServ = trabajo.listaDetOS;
                            }
                        }
                        else if (trabajo.listaDetOS != null)
                        {
                            foreach (DetOrdenServicio servicio5 in trabajo.listaDetOS)
                            {
                                DetOrdenServicio item = servicio5;
                                this.listaDetOrdenesServ.Add(item);
                            }
                        }
                        if (this.listaDetOrdenesServ != null)
                        {
                            this.grdDetOs.DataSource = null;
                            this.grdDetOs.DataSource = this.listaDetOrdenesServ;
                            this.FormatogrdDet();
                            this.FiltraDetalleOrdenServList(this.listaCabOrdenesServ[0].idOrdenSer);
                        }
                        if (this.listaDetRecepcionOS == null)
                        {
                            if (trabajo.listaDetRecOS != null)
                            {
                                this.listaDetRecepcionOS = new List<DetRecepcionOS>();
                                this.listaDetRecepcionOS = trabajo.listaDetRecOS;
                            }
                        }
                        else if (trabajo.listaDetRecOS != null)
                        {
                            foreach (DetRecepcionOS nos2 in trabajo.listaDetRecOS)
                            {
                                DetRecepcionOS item = nos2;
                                this.listaDetRecepcionOS.Add(item);
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show("Ocurrio un Error !!! " + Environment.NewLine + "Error: " + exception.Message, base.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Cancelar();
        }

        private void btnGrabar_Click(object sender, EventArgs e)
        {
            try
            {
                bool flag = false;
                string str = "";
                if (this.Validar())
                {
                    this.AgregarListas();
                    MasterOrdServ_f2 master = this.MapeaPadre();
                    this.resp = new CabOrdenServicioSvc().GuardaOrdenServicio(ref master);
                    if (this.resp.typeResult == ResultTypes.success)
                    {
                        flag = true;
                    }
                    else
                    {
                        flag = false;
                    }
                    if (flag)
                    {
                        foreach (CabOrdenServicio servicio in master.listaCab)
                        {
                            str = str + servicio.idOrdenSer.ToString() + ",";
                        }
                        if (str != "")
                        {
                            str = str.Substring(0, str.Length - 1);
                        }
                        MessageBox.Show(string.Concat(new object[] { " Se Guardo Satisfactoriamente la(s) Orden(es): ", str, " para el FOLIO: ", master.idPadreOrdSer }), "Operaci\x00f3n Exitosa", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                        if (this.SetupThePrinting())
                        {
                            foreach (CabOrdenServicio servicio2 in master.listaCab)
                            {
                                this.resp = new CabOrdenServicioSvc().getCabOrdenServicioxFilter(servicio2.idOrdenSer, "", "");
                                if (this.resp.typeResult == ResultTypes.success)
                                {
                                    this.cabosImp = ((List<CabOrdenServicio>)this.resp.result)[0];
                                    this.resp = new DetOrdenServicioSvc().getDetOrdenServicioxFilter(servicio2.idOrdenSer, "");
                                    if (this.resp.typeResult == ResultTypes.success)
                                    {
                                        this.listaDetOsImp = (List<DetOrdenServicio>)this.resp.result;
                                        this.ImprimeOrdenServicio(true, true, false, this.cabosImp, this.listaDetOsImp, true, this.BandTicket, "");
                                    }
                                }
                            }
                        }
                        this.btnCancelar.PerformClick();
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show("Ocurrio un Error !!! " + Environment.NewLine + "Error: " + exception.Message, base.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            this.opcionForma = Inicio.opcForma.Nuevo;
            if (this.bMultEmpresa)
            {
                this.ActivaCampos(opcHabilita.Iniciando);
                this.HabilitaBotones(opcHabilita.DesHabilitaTodos);
                this.LimpiCampos();
                this.FormatoGeneral(this.grdCabOs);
                this.FormatoGeneral(this.grdDetOs);
                this.cmbIdEmpresa.Enabled = true;
                this.cmbIdEmpresa.Focus();
            }
            else
            {
                this.HabilitaBotones(opcHabilita.Nuevo);
                this.ActivaCampos(opcHabilita.Nuevo);
                this.txtTractor.Focus();
            }
        }

        private void btnReporte_Click(object sender, EventArgs e)
        {
            this.opcionForma = Inicio.opcForma.Reporte;
            this.HabilitaBotones(opcHabilita.Reporte);
            this.ActivaCampos(opcHabilita.Reporte);
            this.txtidPadreOrdSer.Focus();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void Cancelar()
        {
            this.ActivaCampos(opcHabilita.Iniciando);
            this.HabilitaBotones(opcHabilita.DesHabilitaTodos);
            this.LimpiCampos();
            this.txtidPadreOrdSer.Focus();
            this.FormatoGeneral(this.grdCabOs);
            this.FormatoGeneral(this.grdDetOs);
        }

        private void CargaCombos(int idEmpresa)
        {
            this.resp = new CatPersonalSvc().getCatPersonalxFiltro(" per.idpuesto in (7,36,37,16) and per.estatus = 'ACT'", "");
            if (this.resp.typeResult == ResultTypes.success)
            {
                this.listaOperadores = (List<CatPersonal>)this.resp.result;
                this.cmbidOperador.DataSource = null;
                this.cmbidOperador.DataSource = this.listaOperadores;
                this.cmbidOperador.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                this.cmbidOperador.AutoCompleteSource = AutoCompleteSource.ListItems;
                this.cmbidOperador.DisplayMember = "NombreCompleto";
                this.cmbidOperador.SelectedIndex = 0;
            }
            if (idEmpresa >= 1)
            {
                this.resp = new CatPersonalSvc().getCatPersonalxFiltro(" per.idPuesto in (2,15) and per.estatus = 'ACT' and  per.idEmpresa = " + idEmpresa, "");
            }
            else
            {
                this.resp = new CatPersonalSvc().getCatPersonalxFiltro(" per.idPuesto in (2,15) and per.estatus = 'ACT'", "");
            }
            if (this.resp.typeResult == ResultTypes.success)
            {
                this.listaAutorizaPer = (List<CatPersonal>)this.resp.result;
                this.cmbidEmpleadoAutoriza.DataSource = null;
                this.cmbidEmpleadoAutoriza.DataSource = this.listaAutorizaPer;
                this.cmbidEmpleadoAutoriza.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                this.cmbidEmpleadoAutoriza.AutoCompleteSource = AutoCompleteSource.ListItems;
                this.cmbidEmpleadoAutoriza.DisplayMember = "NombreCompleto";
                this.cmbidEmpleadoAutoriza.SelectedIndex = 0;
            }
        }

        private void CargaEmpresas()
        {
            this.resp = new CatEmpresasSvc().getCatEmpresasxFiltro(0, "");
            if (this.resp.typeResult == ResultTypes.success)
            {
                this.listaClientes = (List<CatEmpresas>)this.resp.result;
                this.cmbIdEmpresa.DataSource = null;
                this.cmbIdEmpresa.DataSource = this.listaClientes;
                this.cmbIdEmpresa.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                this.cmbIdEmpresa.AutoCompleteSource = AutoCompleteSource.ListItems;
                this.cmbIdEmpresa.DisplayMember = "RazonSocial";
                this.cmbIdEmpresa.SelectedIndex = 0;
            }
        }

        private void CargaForma(string valor, opcBusqueda opc)
        {
            try
            {
                OperationResult result;
                if (opc == opcBusqueda.CabViajes)
                {
                    result = new CabGuiaSvc().getCabGuiaxFilter(0, " idTractor = '" + valor + "'", " FechaHoraViaje DESC", " TOP 1 ");
                    if (result.typeResult == ResultTypes.success)
                    {
                        this.UltViaje = ((List<CabGuia>)result.result)[0];
                        this.txtRemolque1.Text = this.UltViaje.idRemolque1;
                        this.txtDolly.Text = this.UltViaje.idDolly;
                        this.txtRemolque2.Text = this.UltViaje.idRemolque2;
                        this.SelComboOperador(this.UltViaje.idOperador);
                    }
                }
                else if (opc == opcBusqueda.MasterOrdServ)
                {
                    result = new MasterOrdServSvc_f2().getMasterOrdServxFilter(Convert.ToInt32(valor), "", "");
                    if (result.typeResult == ResultTypes.success)
                    {
                        this.folioPadre = ((List<MasterOrdServ_f2>)result.result)[0];
                        this.dtpFecha.Value = this.folioPadre.FechaCreacion;
                        this.dtpFechaHora.Value = this.folioPadre.FechaCreacion;
                        this.txtEstatus.Text = this.folioPadre.Estatus;
                        this.txtTractor.Text = this.folioPadre.idTractor;
                        this.txtRemolque1.Text = this.folioPadre.idTolva1;
                        this.txtDolly.Text = this.folioPadre.idDolly;
                        this.txtRemolque2.Text = this.folioPadre.idTolva2;
                        this.SelComboOperador(this.folioPadre.idChofer);
                        result = new CabOrdenServicioSvc().getCabOrdenServicioxFilter(0, " c.idPadreOrdSer = " + this.folioPadre.idPadreOrdSer, "");
                        if (result.typeResult == ResultTypes.success)
                        {
                            this.listaCabOrdenesServ = (List<CabOrdenServicio>)result.result;
                            this.grdCabOs.DataSource = null;
                            this.grdCabOs.DataSource = this.listaCabOrdenesServ;
                            this.FiltraDetalleOrdenServ(this.listaCabOrdenesServ[0].idOrdenSer);
                            int index = 0;
                            foreach (CabOrdenServicio servicio in this.listaCabOrdenesServ)
                            {
                                foreach (CatTipoOrdServ serv in this.listaTipoOS)
                                {
                                    if (serv.idTipOrdServ == servicio.idTipOrdServ)
                                    {
                                        this.chkBoxTipoOrdServ.SetItemChecked(index, true);
                                        index = 0;
                                        break;
                                    }
                                    index++;
                                }
                            }
                            this.FormatogrdCab();
                            this.FormatogrdDet();
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show("Ocurrio un Error !!! " + Environment.NewLine + "Error: " + exception.Message, base.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void cmbidEmpleadoAutoriza_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.chkBoxTipoOrdServ.Focus();
            }
        }

        private void cmbIdEmpresa_KeyPress(object sender, KeyPressEventArgs e)
        {
        }

        private void cmbIdEmpresa_KeyUp(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.Enter) && (((CatEmpresas)this.cmbIdEmpresa.SelectedItem).idEmpresa > 0))
            {
                this.IdEmpresa = ((CatEmpresas)this.cmbIdEmpresa.SelectedItem).idEmpresa;
                this.LlenaCheckBox();
                this.SelComboEmpresa(this.IdEmpresa);
                this.cmbIdEmpresa.Enabled = false;
                this.AutoAcompletarTexto(this.txtTractor, this.IdEmpresa, Inicio.eTipoUnidad.Tractor);
                this.AutoAcompletarTexto(this.txtRemolque1, this.IdEmpresa, Inicio.eTipoUnidad.Remolque);
                this.AutoAcompletarTexto(this.txtDolly, this.IdEmpresa, Inicio.eTipoUnidad.Dolly);
                this.AutoAcompletarTexto(this.txtRemolque2, this.IdEmpresa, Inicio.eTipoUnidad.Remolque);
                this.CargaCombos(this.IdEmpresa);
                this.HabilitaBotones(opcHabilita.Nuevo);
                this.ActivaCampos(opcHabilita.Nuevo);
                this.txtTractor.Focus();
            }
        }

        private void cmbIdEmpresa_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void cmbidOperador_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.cmbidEmpleadoAutoriza.Focus();
            }
        }

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing && (this.components != null))
        //    {
        //        this.components.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}

        private void FiltraDetalleOrdenServ(int idOrdenSer)
        {
            OperationResult result = new DetOrdenServicioSvc().getDetOrdenServicioxFilter(idOrdenSer, "");
            if (result.typeResult == ResultTypes.success)
            {
                this.listaDetOrdenesServ = (List<DetOrdenServicio>)result.result;
                this.grdDetOs.DataSource = null;
                this.grdDetOs.DataSource = this.listaDetOrdenesServ;
                this.FormatogrdCab();
                this.FormatogrdDet();
            }
        }

        private void FiltraDetalleOrdenServList(int idOrdenSer)
        {
            List<DetOrdenServicio> list = this.listaDetOrdenesServ.FindAll(S => S.idOrdenSer.ToString().IndexOf(idOrdenSer.ToString(), StringComparison.OrdinalIgnoreCase) >= 0);
            this.grdDetOs.DataSource = null;
            this.grdDetOs.DataSource = list;
            this.FormatogrdCab();
            this.FormatogrdDet();
        }

        private void FormatoGeneral(DataGridView grid)
        {
            grid.RowsDefaultCellStyle.BackColor = Color.White;
            grid.AlternatingRowsDefaultCellStyle.BackColor = Color.AliceBlue;
            grid.RowsDefaultCellStyle.SelectionForeColor = Color.Yellow;
            grid.RowsDefaultCellStyle.SelectionBackColor = Color.Gray;
            grid.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            grid.DefaultCellStyle.Font = new Font("Berlin Sans FB", 12f);
            grid.ColumnHeadersDefaultCellStyle.Font = new Font("Berlin Sans FB", 12f);
            grid.ColumnHeadersDefaultCellStyle.BackColor = Color.Yellow;
            grid.ColumnHeadersDefaultCellStyle.ForeColor = Color.Red;
            grid.EnableHeadersVisualStyles = false;
        }

        private void FormatogrdCab()
        {
            this.grdCabOs.Columns["IdEmpresa"].Visible = false;
            this.grdCabOs.Columns["RazonSocial"].Visible = false;
            this.grdCabOs.Columns["idTipoOrden"].Visible = false;
            this.grdCabOs.Columns["FechaRecepcion"].Visible = false;
            this.grdCabOs.Columns["idEmpleadoEntrega"].Visible = false;
            this.grdCabOs.Columns["NomEmpleadoEntrega"].Visible = false;
            this.grdCabOs.Columns["UsuarioRecepcion"].Visible = false;
            this.grdCabOs.Columns["Kilometraje"].Visible = false;
            this.grdCabOs.Columns["Estatus"].Visible = false;
            this.grdCabOs.Columns["FechaDiagnostico"].Visible = false;
            this.grdCabOs.Columns["FechaAsignado"].Visible = false;
            this.grdCabOs.Columns["FechaTerminado"].Visible = false;
            this.grdCabOs.Columns["FechaEntregado"].Visible = false;
            this.grdCabOs.Columns["UsuarioEntrega"].Visible = false;
            this.grdCabOs.Columns["idEmpleadoRecibe"].Visible = false;
            this.grdCabOs.Columns["NotaFinal"].Visible = false;
            this.grdCabOs.Columns["usuarioCan"].Visible = false;
            this.grdCabOs.Columns["FechaCancela"].Visible = false;
            this.grdCabOs.Columns["idEmpleadoAutoriza"].Visible = false;
            this.grdCabOs.Columns["fechaCaptura"].Visible = false;
            this.grdCabOs.Columns["idTipOrdServ"].Visible = false;
            this.grdCabOs.Columns["idTipoServicio"].Visible = false;
            this.grdCabOs.Columns["idPadreOrdSer"].Visible = false;
            this.grdCabOs.Columns["ClaveFicticia"].Visible = false;
            this.grdCabOs.Columns["NomEmpleadoAutoriza"].Visible = false;
            this.grdCabOs.Columns["idOperarador"].Visible = false;
            this.grdCabOs.Columns["NomOperador"].Visible = false;
            this.grdCabOs.Columns["idOrdenSer"].HeaderText = "No.";
            this.grdCabOs.Columns["idUnidadTrans"].HeaderText = "Unidad";
            this.grdCabOs.Columns["NomTipoUnidad"].HeaderText = "Tipo Unidad";
            this.grdCabOs.Columns["NomTipoServicio"].HeaderText = "Tipo Servicio";
            this.grdCabOs.Columns["NomTipOrdServ"].HeaderText = "Tipo de Orden";
            this.grdCabOs.Columns["idOrdenSer"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.grdCabOs.Columns["idUnidadTrans"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.grdCabOs.Columns["NomTipoUnidad"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.grdCabOs.Columns["NomTipoServicio"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.grdCabOs.Columns["NomTipOrdServ"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
        }

        private void FormatogrdDet()
        {
            this.grdDetOs.Columns["Seleccionado"].Visible = false;
            this.grdDetOs.Columns["idOrdenSer"].Visible = false;
            this.grdDetOs.Columns["idOrdenActividad"].Visible = false;
            this.grdDetOs.Columns["idServicio"].Visible = false;
            this.grdDetOs.Columns["idActividad"].Visible = false;
            this.grdDetOs.Columns["DuracionActHr"].Visible = false;
            this.grdDetOs.Columns["NoPersonal"].Visible = false;
            this.grdDetOs.Columns["id_proveedor"].Visible = false;
            this.grdDetOs.Columns["Estatus"].Visible = false;
            this.grdDetOs.Columns["CostoManoObra"].Visible = false;
            this.grdDetOs.Columns["CostoProductos"].Visible = false;
            this.grdDetOs.Columns["FechaDiag"].Visible = false;
            this.grdDetOs.Columns["UsuarioDiag"].Visible = false;
            this.grdDetOs.Columns["FechaAsig"].Visible = false;
            this.grdDetOs.Columns["UsuarioAsig"].Visible = false;
            this.grdDetOs.Columns["FechaPausado"].Visible = false;
            this.grdDetOs.Columns["NotaPausado"].Visible = false;
            this.grdDetOs.Columns["FechaTerminado"].Visible = false;
            this.grdDetOs.Columns["UsuarioTerminado"].Visible = false;
            this.grdDetOs.Columns["idDivision"].Visible = false;
            this.grdDetOs.Columns["idLlanta"].Visible = false;
            this.grdDetOs.Columns["PosLlanta"].Visible = false;
            this.grdDetOs.Columns["NotaDiagnostico"].Visible = false;
            this.grdDetOs.Columns["FaltInsumo"].Visible = false;
            this.grdDetOs.Columns["Pendiente"].Visible = false;
            this.grdDetOs.Columns["CIDPRODUCTO_SERV"].Visible = false;
            this.grdDetOs.Columns["Cantidad_CIDPRODUCTO_SERV"].Visible = false;
            this.grdDetOs.Columns["Precio_CIDPRODUCTO_SERV"].Visible = false;
            this.grdDetOs.Columns["idFamilia"].Visible = false;
            this.grdDetOs.Columns["NotaRecepcion"].Visible = false;
            this.grdDetOs.Columns["isCancelado"].Visible = false;
            this.grdDetOs.Columns["motivoCancelacion"].Visible = false;
            this.grdDetOs.Columns["idPersonalResp"].Visible = false;
            this.grdDetOs.Columns["idPersonalAyu1"].Visible = false;
            this.grdDetOs.Columns["idPersonalAyu2"].Visible = false;
            this.grdDetOs.Columns["NotaRecepcionA"].Visible = false;
            this.grdDetOs.Columns["FallaReportada"].Visible = false;
            this.grdDetOs.Columns["NomServicio"].Visible = false;
            this.grdDetOs.Columns["NomPersonalResp"].Visible = false;
            this.grdDetOs.Columns["NomPersonalAyu1"].Visible = false;
            this.grdDetOs.Columns["NomPersonalAyu2"].Visible = false;
            this.grdDetOs.Columns["CIDPRODUCTO_ACT"].Visible = false;
            this.grdDetOs.Columns["DetRec"].Visible = false;
            this.grdDetOs.Columns["idOrdenTrabajo"].HeaderText = "No.";
            this.grdDetOs.Columns["NotaRecepcionF"].HeaderText = "Descripcion";
            this.grdDetOs.Columns["TipoNotaRecepcionF"].HeaderText = "Tipo";
            this.grdDetOs.Columns["idOrdenTrabajo"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.grdDetOs.Columns["NotaRecepcionF"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.grdDetOs.Columns["TipoNotaRecepcionF"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.grdDetOs.Columns["NotaRecepcionA"].DisplayIndex = 0;
        }

        private void fSolicitudOS_Load(object sender, EventArgs e)
        {
            this.bMultEmpresa = Inicio.ConsultarPrivilegioEspecial(Inicio.ePermisoEspecial.MultiEmpresa, this._user.idUsuario);
            this.CargaEmpresas();
            this.LlenaCheckBox();
            if (!this.bMultEmpresa)
            {
                this.IdEmpresa = this._user.personal.clave_empresa.Value;
                if (this.IdEmpresa > 0)
                {
                    this.SelComboEmpresa(this.IdEmpresa);
                    this.cmbIdEmpresa.Enabled = false;
                    this.AutoAcompletarTexto(this.txtTractor, this.IdEmpresa, Inicio.eTipoUnidad.Tractor);
                    this.AutoAcompletarTexto(this.txtRemolque1, this.IdEmpresa, Inicio.eTipoUnidad.Remolque);
                    this.AutoAcompletarTexto(this.txtDolly, this.IdEmpresa, Inicio.eTipoUnidad.Dolly);
                    this.AutoAcompletarTexto(this.txtRemolque2, this.IdEmpresa, Inicio.eTipoUnidad.Remolque);
                    this.CargaCombos(this.IdEmpresa);
                }
            }
            this.Cancelar();
        }

        private List<CabOrdenServicio> getlistaCab()
        {
            List<CabOrdenServicio> list = new List<CabOrdenServicio>();
            foreach (CabOrdenServicio servicio in list)
            {
            }
            return list;
        }

        private void grdCabOs_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void grdCabOs_Click(object sender, EventArgs e)
        {
            if (this.listaCabOrdenesServ.Count > 0)
            {
                if (this.txtidPadreOrdSer.Text != "")
                {
                    this.FiltraDetalleOrdenServ(this.listaCabOrdenesServ[this.grdCabOs.CurrentCell.RowIndex].idOrdenSer);
                }
                else
                {
                    this.FiltraDetalleOrdenServList(this.listaCabOrdenesServ[this.grdCabOs.CurrentCell.RowIndex].idOrdenSer);
                }
            }
        }

        private void HabilitaBotones(opcHabilita opc)
        {
            switch (opc)
            {
                case opcHabilita.DesHabilitaTodos:
                    this.btnNuevo.Enabled = true;
                    this.btnGrabar.Enabled = false;
                    this.btnEliminar.Enabled = false;
                    this.btnReporte.Enabled = true;
                    this.btnCancelar.Enabled = false;
                    this.btnSalir.Enabled = true;
                    this.btnAgregaOT.Enabled = false;
                    break;

                case opcHabilita.Nuevo:
                    this.btnNuevo.Enabled = false;
                    this.btnGrabar.Enabled = true;
                    this.btnEliminar.Enabled = false;
                    this.btnReporte.Enabled = false;
                    this.btnCancelar.Enabled = true;
                    this.btnSalir.Enabled = false;
                    this.btnAgregaOT.Enabled = true;
                    break;

                case opcHabilita.Reporte:
                    this.btnNuevo.Enabled = false;
                    this.btnGrabar.Enabled = false;
                    this.btnEliminar.Enabled = false;
                    this.btnReporte.Enabled = false;
                    this.btnCancelar.Enabled = true;
                    this.btnSalir.Enabled = false;
                    this.btnAgregaOT.Enabled = false;
                    break;
            }
        }

        public void ImprimeOrdenServicio(bool bOrdenTrab, bool BandPreview, bool vBandHorizontal, CabOrdenServicio cabOs, List<DetOrdenServicio> listaDetOS, bool ActivaImprime, bool TipoTicket, string nombreImpresora = "")
        {
            try
            {
                ConsultasSQL ssql = new ConsultasSQL();
                this.ImpRep.FuenteImprimeCampos = new Font("Courrier New", 10f, FontStyle.Bold);
                this.ImpRep.FuenteImprimeCampos2 = new Font("Free 3 of 9 Extended", 40f);
                this.ImpRep.FuenteImprimeCampos3 = new Font("Courrier New", 10f, FontStyle.Bold);
                this.ImpRep.FuenteImprimeCampos4 = new Font("Courrier New", 7f, FontStyle.Bold);
                this.ImpRep.FuenteImprimeCampos5 = new Font("Courrier New", 10f, FontStyle.Bold);
                if (TipoTicket)
                {
                    this.ImpRep.TablaImprime = this.ImpRep.ArmaTablaTicket(cabOs, listaDetOS);
                }
                else
                {
                    this.ImpRep.TablaImprime = this.ImpRep.ArmaTablaCarta(cabOs, listaDetOS, bOrdenTrab);
                }
                this.prtSettings = new PrinterSettings();
                this.prtDoc = new PrintDocument();
                this.prtDoc.PrintPage += new PrintPageEventHandler(this.prt_PrintPage);
                this.prtDoc.PrinterSettings = this.prtSettings;
                this.prtDoc.DefaultPageSettings.Landscape = vBandHorizontal;
                this.prtDoc.DefaultPageSettings.PaperSize = this.prtSettings.DefaultPageSettings.PaperSize;
                if (BandPreview)
                {
                    PrintPreviewDialog dialog = new PrintPreviewDialog
                    {
                        Document = this.prtDoc,
                        WindowState = FormWindowState.Maximized
                    };
                    dialog.Document.DefaultPageSettings.Landscape = vBandHorizontal;
                    if (vBandHorizontal)
                    {
                        dialog.PrintPreviewControl.Zoom = 1.0;
                    }
                    else
                    {
                        dialog.PrintPreviewControl.Zoom = 1.0;
                    }
                    if (!ActivaImprime)
                    {
                        ((ToolStrip)dialog.Controls[1]).Items[0].Enabled = false;
                    }
                    dialog.ShowDialog();
                }
                else
                {
                    this.prtDoc.Print();
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show("Ocurrio un error al querer Imprimir la Orden de Servicio" + Environment.NewLine + exception.Message, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                throw;
            }
        }

        private void LimpiCampos()
        {
            this.txtidPadreOrdSer.Clear();
            this.dtpFecha.Value = DateTime.Now;
            this.dtpFechaHora.Value = DateTime.Now;
            this.txtEstatus.Clear();
            this.txtTractor.Clear();
            this.txtRemolque1.Clear();
            this.txtDolly.Clear();
            this.txtRemolque2.Clear();
            if (this.cmbidOperador.Items.Count > 0)
            {
                this.cmbidOperador.SelectedIndex = 0;
            }
            if (this.cmbidEmpleadoAutoriza.Items.Count > 0)
            {
                this.cmbidEmpleadoAutoriza.SelectedIndex = 0;
            }
            for (int i = 0; i < this.chkBoxTipoOrdServ.Items.Count; i++)
            {
                this.chkBoxTipoOrdServ.SetItemChecked(i, false);
            }
            this.listaCabOrdenesServ = null;
            this.listaDetOrdenesServ = null;
            this.listaDetRecepcionOS = null;
            this.grdCabOs.DataSource = null;
            this.grdDetOs.DataSource = null;
            this.NoOS = 0;
        }

        private void LlenaCheckBox()
        {
            try
            {
                OperationResult result = new CatTipoOrdServSvc().getCatTipoOrdServxFilter(0, "", " c.NumOrden");
                if (result.typeResult == ResultTypes.success)
                {
                    this.listaTipoOS = (List<CatTipoOrdServ>)result.result;
                    this.chkBoxTipoOrdServ.Items.Clear();
                    this.chkBoxTipoOrdServ.DisplayMember = "nomTipOrdServ";
                    CatTipoOrdServ item = new CatTipoOrdServ();
                    foreach (CatTipoOrdServ serv2 in this.listaTipoOS)
                    {
                        item = serv2;
                        this.chkBoxTipoOrdServ.Items.Add(item);
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show("Ocurrio un Error !!! " + Environment.NewLine + "Error: " + exception.Message, base.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private MasterOrdServ_f2 MapeaPadre()
        {
            try
            {
                return new MasterOrdServ_f2
                {
                    idPadreOrdSer = 0,
                    idTractor = this.txtTractor.Text,
                    idTolva1 = this.txtRemolque1.Text,
                    idDolly = this.txtDolly.Text,
                    idTolva2 = this.txtRemolque2.Text,
                    idChofer = ((CatPersonal)this.cmbidOperador.SelectedItem).idPersonal,
                    FechaCreacion = DateTime.Now,
                    UserCrea = this._user.nombreUsuario,
                    UserModifica = "",
                    FechaModifica = DateTime.Now,
                    UserCancela = "",
                    FechaCancela = DateTime.Now,
                    MotivoCancela = "",
                    Estatus = "CAP",
                    listaCab = this.listaCabOrdenesServ
                };
            }
            catch (Exception exception)
            {
                MessageBox.Show("Ocurrio un Error !!! " + Environment.NewLine + "Error: " + exception.Message, base.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return null;
            }
        }

        private void prt_PrintPage(object sender, PrintPageEventArgs e)
        {
            try
            {
                float top = e.MarginBounds.Top;
                float left = e.MarginBounds.Left;
                float height = this.printFont.GetHeight(e.Graphics);
                this.ImpRep.imgLogo1 = Resources.ROA;
                this.ImpRep.TamanioimgLogo1 = new Size(this.ImpRep.imgLogo1.Width / 0x18, this.ImpRep.imgLogo1.Height / 0x18);
                if (this.BandTicket)
                {
                    this.ImpRep.PosicionimgLogo1 = new Point(180, 3);
                }
                else
                {
                    this.ImpRep.PosicionimgLogo1 = new Point(230, 3);
                }
                if (this.ContPaginas == 0)
                {
                    top = this.ImpRep.ImprimeLogo(e, (float)this.lineaActual);
                }
                top += height;
                do
                {
                    top += height;
                    top = this.ImpRep.ImprimeLinea(e, this.lineaActual, this.ContPaginas + 1);
                    this.lineaActual++;
                }
                while (((top < (e.MarginBounds.Bottom - 20)) && (this.lineaActual < this.NumRegistros)) && (this.ContPaginas != 2));
                this.ContPaginas++;
                if (this.lineaActual < this.NumRegistros)
                {
                    e.HasMorePages = true;
                }
                else
                {
                    this.lineaActual = 0;
                    this.ContPaginas = 0;
                    e.HasMorePages = false;
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show("Ocurrio un Error !!! " + Environment.NewLine + "Error: " + exception.Message, base.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void SelComboEmpresa(int idEmpresa)
        {
            try
            {
                int num = 0;
                foreach (CatEmpresas empresas in this.cmbIdEmpresa.Items)
                {
                    if (empresas.idEmpresa == idEmpresa)
                    {
                        this.cmbIdEmpresa.SelectedIndex = num;
                        return;
                    }
                    num++;
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show("Ocurrio un Error !!! " + Environment.NewLine + "Error: " + exception.Message, base.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void SelComboOperador(int idOperador)
        {
            try
            {
                int num = 0;
                foreach (CatPersonal personal in this.cmbidOperador.Items)
                {
                    if (personal.idPersonal == idOperador)
                    {
                        this.cmbidOperador.SelectedIndex = num;
                        return;
                    }
                    num++;
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show("Ocurrio un Error !!! " + Environment.NewLine + "Error: " + exception.Message, base.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private bool SetupThePrinting()
        {
            PrintDialog dialog = new PrintDialog
            {
                AllowCurrentPage = false,
                AllowPrintToFile = false,
                AllowSelection = false,
                AllowSomePages = false,
                PrintToFile = false,
                ShowHelp = false,
                ShowNetwork = false
            };
            if (dialog.ShowDialog() != DialogResult.OK)
            {
                return false;
            }
            if (this.prtSettings == null)
            {
                this.prtSettings = new PrinterSettings();
            }
            this.prtSettings = dialog.PrinterSettings;
            return true;
        }

        private void txtDolly_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (this.txtDolly.Text == "")
                {
                    this.txtRemolque2.Focus();
                }
                else if (this.ValidaUnidad(this.txtDolly.Text))
                {
                    this.txtRemolque2.Focus();
                }
                else
                {
                    this.txtDolly.Clear();
                    this.txtDolly.Focus();
                }
            }
        }

        private void txtDolly_TextChanged(object sender, EventArgs e)
        {
        }

        private void txtidPadreOrdSer_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F3)
            {
                string str = new fConsulta(opcBusqueda.MasterOrdServ, this._user, "Folios Padre", "idPadreOrdSer", false, " c.Estatus = 'CAP'", " c.idPadreOrdSer DESC", "") { StartPosition = FormStartPosition.CenterScreen }.DevuelveValor();
                if ((str != null) && (Convert.ToInt32(str) > 0))
                {
                    this.txtidPadreOrdSer.Text = str;
                    this.CargaForma(this.txtidPadreOrdSer.Text, opcBusqueda.MasterOrdServ);
                }
            }
        }

        private void txtidPadreOrdSer_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                if (this.txtidPadreOrdSer.Text == "")
                {
                    this.txtidPadreOrdSer.Focus();
                }
                this.CargaForma(this.txtidPadreOrdSer.Text, opcBusqueda.MasterOrdServ);
                if ((this.opcionForma == Inicio.opcForma.Reporte) && this.SetupThePrinting())
                {
                    foreach (CabOrdenServicio servicio in this.listaCabOrdenesServ)
                    {
                        this.resp = new CabOrdenServicioSvc().getCabOrdenServicioxFilter(servicio.idOrdenSer, "", "");
                        if (this.resp.typeResult == ResultTypes.success)
                        {
                            this.cabosImp = ((List<CabOrdenServicio>)this.resp.result)[0];
                            this.resp = new DetOrdenServicioSvc().getDetOrdenServicioxFilter(servicio.idOrdenSer, "");
                            if (this.resp.typeResult == ResultTypes.success)
                            {
                                this.listaDetOsImp = (List<DetOrdenServicio>)this.resp.result;
                                this.ImprimeOrdenServicio(true, true, false, this.cabosImp, this.listaDetOsImp, true, this.BandTicket, "");
                            }
                        }
                    }
                    this.Cancelar();
                }
            }
        }

        private void txtidPadreOrdSer_TextChanged(object sender, EventArgs e)
        {
        }

        private void txtRemolque1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (this.ValidaUnidad(this.txtRemolque1.Text))
                {
                    this.txtDolly.Focus();
                }
                else
                {
                    this.txtRemolque1.Clear();
                    this.txtRemolque1.Focus();
                }
            }
        }

        private void txtRemolque1_TextChanged(object sender, EventArgs e)
        {
        }

        private void txtRemolque2_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (this.txtRemolque2.Text == "")
                {
                    this.cmbidOperador.Focus();
                }
                else if (this.ValidaUnidad(this.txtRemolque2.Text))
                {
                    this.cmbidOperador.Focus();
                }
                else
                {
                    this.txtRemolque2.Clear();
                    this.txtRemolque2.Focus();
                }
            }
        }

        private void txtRemolque2_TextChanged(object sender, EventArgs e)
        {
        }

        private void txtTractor_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (this.ValidaUnidad(this.txtTractor.Text))
                {
                    if (this.txtRemolque1.Text == "")
                    {
                        this.CargaForma(this.txtTractor.Text, opcBusqueda.CabViajes);
                    }
                    this.txtRemolque1.Focus();
                }
                else
                {
                    this.txtTractor.Clear();
                    this.txtTractor.Focus();
                }
            }
        }

        private void txtTractor_TextChanged(object sender, EventArgs e)
        {
        }

        private bool Validar()
        {
            if (this.txtTractor.Text == "")
            {
                MessageBox.Show("El tractor no puede ser Vacio", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                this.txtTractor.SelectAll();
                this.txtTractor.Focus();
                return false;
            }
            if (!this.ValidaUnidad(this.txtTractor.Text))
            {
                MessageBox.Show("El tractor no es V\x00e1lido", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                this.txtTractor.SelectAll();
                this.txtTractor.Focus();
                return false;
            }
            if ((this.txtRemolque1.Text != "") && !this.ValidaUnidad(this.txtRemolque1.Text))
            {
                MessageBox.Show("El Remolque1 no es V\x00e1lido", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                this.txtRemolque1.SelectAll();
                this.txtRemolque1.Focus();
                return false;
            }
            if (this.txtDolly.Text != "")
            {
                if (!this.ValidaUnidad(this.txtDolly.Text))
                {
                    MessageBox.Show("El Dolly no es V\x00e1lido", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    this.txtDolly.SelectAll();
                    this.txtDolly.Focus();
                    return false;
                }
                if ((this.txtRemolque2.Text != "") && (this.txtRemolque1.Text == ""))
                {
                    MessageBox.Show("El Remolque1 no puede ser vacio", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    this.txtRemolque1.SelectAll();
                    this.txtRemolque1.Focus();
                    return false;
                }
            }
            if (this.txtRemolque2.Text != "")
            {
                if (!this.ValidaUnidad(this.txtRemolque2.Text))
                {
                    MessageBox.Show("El Remolque2 no es V\x00e1lido", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    this.txtRemolque2.SelectAll();
                    this.txtRemolque2.Focus();
                    return false;
                }
                if ((this.txtRemolque1.Text == "") || (this.txtDolly.Text == ""))
                {
                    if (this.txtRemolque1.Text == "")
                    {
                        MessageBox.Show("El Remolque1 no puede ser vacio", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                        this.txtRemolque1.SelectAll();
                        this.txtRemolque1.Focus();
                        return false;
                    }
                    if (this.txtDolly.Text == "")
                    {
                        MessageBox.Show("El Dolly no puede ser vacio", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                        this.txtDolly.SelectAll();
                        this.txtDolly.Focus();
                        return false;
                    }
                }
            }
            if (((this.txtRemolque1.Text != "") && (this.txtRemolque2.Text != "")) && (this.txtRemolque1.Text == this.txtRemolque2.Text))
            {
                MessageBox.Show("El Remolque1 no puede ser igual al Remolque2", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                this.txtRemolque2.SelectAll();
                this.txtRemolque2.Focus();
                return false;
            }
            if (this.cmbidOperador.Text == "")
            {
                MessageBox.Show("El Operador no puede ser vacio", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                this.cmbidOperador.Focus();
                return false;
            }
            if (((CatPersonal)this.cmbidOperador.SelectedItem).idPersonal == 0)
            {
                MessageBox.Show("El Operador no es Valido", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                this.cmbidOperador.Focus();
                return false;
            }
            if (this.cmbidEmpleadoAutoriza.Text == "")
            {
                MessageBox.Show("El Empleado Autoriza no puede ser vacio", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                this.cmbidEmpleadoAutoriza.Focus();
                return false;
            }
            if (((CatPersonal)this.cmbidEmpleadoAutoriza.SelectedItem).idPersonal == 0)
            {
                MessageBox.Show("El Empleado Autoriza no es Valido", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                this.cmbidEmpleadoAutoriza.Focus();
                return false;
            }
            return true;
        }

        private bool ValidaUnidad(string idUnidadTrans)
        {
            try
            {
                this.resp = new CatUnidadTransSvc().geCatUnidadTransxFilter(idUnidadTrans, $"idEmpresa = {((CatEmpresas)this.cmbIdEmpresa.SelectedItem).idEmpresa}", "");
                if (this.resp.typeResult == ResultTypes.success)
                {
                    return true;
                }
                MessageBox.Show($"{this.resp.mensaje}");
                return false;
            }
            catch (Exception exception)
            {
                MessageBox.Show("Ocurrio un Error !!! " + Environment.NewLine + "Error: " + exception.Message, base.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
        }
    }
}
