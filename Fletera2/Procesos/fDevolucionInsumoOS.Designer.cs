﻿namespace Fletera2.Procesos
{
    partial class fDevolucionInsumoOS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fDevolucionInsumoOS));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtIdEmpresa = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtEstatus = new System.Windows.Forms.TextBox();
            this.txtidUnidadTrans = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtidEmpleadoEntrega = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtTipoOrdenServ = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtTipoUnidad = new System.Windows.Forms.TextBox();
            this.dtpFechaHora = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpFecha = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtidOrdenSer = new System.Windows.Forms.TextBox();
            this.ToolStripMenu = new System.Windows.Forms.ToolStrip();
            this.btnNuevo = new System.Windows.Forms.ToolStripButton();
            this.btnGrabar = new System.Windows.Forms.ToolStripButton();
            this.btnEditar = new System.Windows.Forms.ToolStripButton();
            this.btnReporte = new System.Windows.Forms.ToolStripButton();
            this.btnCancelar = new System.Windows.Forms.ToolStripButton();
            this.btnSalir = new System.Windows.Forms.ToolStripButton();
            this.grdRefacciones = new System.Windows.Forms.DataGridView();
            this.cmbIdOrdenTrabajo = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.ToolStripMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdRefacciones)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cmbIdOrdenTrabajo);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtIdEmpresa);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtEstatus);
            this.groupBox1.Controls.Add(this.txtidUnidadTrans);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.txtidEmpleadoEntrega);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.txtTipoOrdenServ);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtTipoUnidad);
            this.groupBox1.Controls.Add(this.dtpFechaHora);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.dtpFecha);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtidOrdenSer);
            this.groupBox1.Location = new System.Drawing.Point(12, 68);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(943, 157);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // txtIdEmpresa
            // 
            this.txtIdEmpresa.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtIdEmpresa.Location = new System.Drawing.Point(100, 50);
            this.txtIdEmpresa.Name = "txtIdEmpresa";
            this.txtIdEmpresa.Size = new System.Drawing.Size(208, 25);
            this.txtIdEmpresa.TabIndex = 88;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Blue;
            this.label5.Location = new System.Drawing.Point(617, 118);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 18);
            this.label5.TabIndex = 87;
            this.label5.Text = "Estatus:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtEstatus
            // 
            this.txtEstatus.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtEstatus.Location = new System.Drawing.Point(681, 114);
            this.txtEstatus.Name = "txtEstatus";
            this.txtEstatus.Size = new System.Drawing.Size(156, 25);
            this.txtEstatus.TabIndex = 86;
            // 
            // txtidUnidadTrans
            // 
            this.txtidUnidadTrans.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtidUnidadTrans.Location = new System.Drawing.Point(762, 83);
            this.txtidUnidadTrans.Name = "txtidUnidadTrans";
            this.txtidUnidadTrans.Size = new System.Drawing.Size(75, 25);
            this.txtidUnidadTrans.TabIndex = 85;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Blue;
            this.label15.Location = new System.Drawing.Point(7, 114);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(119, 18);
            this.label15.TabIndex = 84;
            this.label15.Text = "Personal Entrego";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtidEmpleadoEntrega
            // 
            this.txtidEmpleadoEntrega.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtidEmpleadoEntrega.Location = new System.Drawing.Point(143, 111);
            this.txtidEmpleadoEntrega.Name = "txtidEmpleadoEntrega";
            this.txtidEmpleadoEntrega.Size = new System.Drawing.Size(331, 25);
            this.txtidEmpleadoEntrega.TabIndex = 83;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Blue;
            this.label8.Location = new System.Drawing.Point(314, 57);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(154, 18);
            this.label8.TabIndex = 82;
            this.label8.Text = "Tipo de Orden Servicio";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtTipoOrdenServ
            // 
            this.txtTipoOrdenServ.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtTipoOrdenServ.Location = new System.Drawing.Point(474, 50);
            this.txtTipoOrdenServ.Name = "txtTipoOrdenServ";
            this.txtTipoOrdenServ.Size = new System.Drawing.Size(123, 25);
            this.txtTipoOrdenServ.TabIndex = 81;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Blue;
            this.label3.Location = new System.Drawing.Point(480, 86);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(135, 18);
            this.label3.TabIndex = 80;
            this.label3.Text = "Unidad Transporte:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtTipoUnidad
            // 
            this.txtTipoUnidad.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtTipoUnidad.Location = new System.Drawing.Point(620, 83);
            this.txtTipoUnidad.Name = "txtTipoUnidad";
            this.txtTipoUnidad.Size = new System.Drawing.Size(136, 25);
            this.txtTipoUnidad.TabIndex = 79;
            // 
            // dtpFechaHora
            // 
            this.dtpFechaHora.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.dtpFechaHora.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpFechaHora.Location = new System.Drawing.Point(368, 81);
            this.dtpFechaHora.Name = "dtpFechaHora";
            this.dtpFechaHora.Size = new System.Drawing.Size(106, 25);
            this.dtpFechaHora.TabIndex = 78;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Blue;
            this.label2.Location = new System.Drawing.Point(7, 86);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 18);
            this.label2.TabIndex = 77;
            this.label2.Text = "Fecha:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dtpFecha
            // 
            this.dtpFecha.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.dtpFecha.Location = new System.Drawing.Point(99, 81);
            this.dtpFecha.Name = "dtpFecha";
            this.dtpFecha.Size = new System.Drawing.Size(263, 25);
            this.dtpFecha.TabIndex = 76;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Blue;
            this.label4.Location = new System.Drawing.Point(7, 53);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 18);
            this.label4.TabIndex = 75;
            this.label4.Text = "Empresa";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(7, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 18);
            this.label1.TabIndex = 74;
            this.label1.Text = "Orden. Servicio";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtidOrdenSer
            // 
            this.txtidOrdenSer.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtidOrdenSer.Location = new System.Drawing.Point(114, 19);
            this.txtidOrdenSer.Name = "txtidOrdenSer";
            this.txtidOrdenSer.Size = new System.Drawing.Size(71, 25);
            this.txtidOrdenSer.TabIndex = 73;
            this.txtidOrdenSer.TextChanged += new System.EventHandler(this.txtidOrdenSer_TextChanged);
            this.txtidOrdenSer.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtidOrdenSer_KeyPress);
            // 
            // ToolStripMenu
            // 
            this.ToolStripMenu.AutoSize = false;
            this.ToolStripMenu.BackColor = System.Drawing.SystemColors.Control;
            this.ToolStripMenu.Font = new System.Drawing.Font("Berlin Sans FB", 11.25F);
            this.ToolStripMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnNuevo,
            this.btnGrabar,
            this.btnEditar,
            this.btnReporte,
            this.btnCancelar,
            this.btnSalir});
            this.ToolStripMenu.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.ToolStripMenu.Location = new System.Drawing.Point(0, 0);
            this.ToolStripMenu.Name = "ToolStripMenu";
            this.ToolStripMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.ToolStripMenu.Size = new System.Drawing.Size(1043, 65);
            this.ToolStripMenu.TabIndex = 10;
            this.ToolStripMenu.Text = "ToolStrip1";
            // 
            // btnNuevo
            // 
            this.btnNuevo.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnNuevo.Image = ((System.Drawing.Image)(resources.GetObject("btnNuevo.Image")));
            this.btnNuevo.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnNuevo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(54, 62);
            this.btnNuevo.Text = "&Nuevo";
            this.btnNuevo.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnNuevo.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnNuevo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnNuevo.Click += new System.EventHandler(this.btnNuevo_Click);
            // 
            // btnGrabar
            // 
            this.btnGrabar.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnGrabar.Image = ((System.Drawing.Image)(resources.GetObject("btnGrabar.Image")));
            this.btnGrabar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnGrabar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnGrabar.Name = "btnGrabar";
            this.btnGrabar.Size = new System.Drawing.Size(69, 62);
            this.btnGrabar.Text = "&Devolver";
            this.btnGrabar.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnGrabar.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnGrabar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnGrabar.Click += new System.EventHandler(this.btnGrabar_Click);
            // 
            // btnEditar
            // 
            this.btnEditar.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnEditar.Image = ((System.Drawing.Image)(resources.GetObject("btnEditar.Image")));
            this.btnEditar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnEditar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(52, 62);
            this.btnEditar.Text = "&Editar";
            this.btnEditar.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnEditar.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnEditar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnEditar.Visible = false;
            // 
            // btnReporte
            // 
            this.btnReporte.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnReporte.Image = ((System.Drawing.Image)(resources.GetObject("btnReporte.Image")));
            this.btnReporte.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnReporte.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnReporte.Name = "btnReporte";
            this.btnReporte.Size = new System.Drawing.Size(65, 62);
            this.btnReporte.Text = "&Reporte";
            this.btnReporte.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnReporte.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnReporte.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnReporte.Visible = false;
            // 
            // btnCancelar
            // 
            this.btnCancelar.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnCancelar.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar.Image")));
            this.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnCancelar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(69, 62);
            this.btnCancelar.Text = "&Cancelar";
            this.btnCancelar.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnCancelar.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnSalir.Image = ((System.Drawing.Image)(resources.GetObject("btnSalir.Image")));
            this.btnSalir.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnSalir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(55, 62);
            this.btnSalir.Text = "&S a l i r";
            this.btnSalir.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnSalir.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // grdRefacciones
            // 
            this.grdRefacciones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdRefacciones.Location = new System.Drawing.Point(12, 231);
            this.grdRefacciones.Name = "grdRefacciones";
            this.grdRefacciones.Size = new System.Drawing.Size(943, 204);
            this.grdRefacciones.TabIndex = 11;
            // 
            // cmbIdOrdenTrabajo
            // 
            this.cmbIdOrdenTrabajo.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.cmbIdOrdenTrabajo.FormattingEnabled = true;
            this.cmbIdOrdenTrabajo.Location = new System.Drawing.Point(300, 14);
            this.cmbIdOrdenTrabajo.Name = "cmbIdOrdenTrabajo";
            this.cmbIdOrdenTrabajo.Size = new System.Drawing.Size(103, 26);
            this.cmbIdOrdenTrabajo.TabIndex = 92;
            this.cmbIdOrdenTrabajo.SelectedIndexChanged += new System.EventHandler(this.cmbIdOrdenTrabajo_SelectedIndexChanged);
            this.cmbIdOrdenTrabajo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbIdOrdenTrabajo_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Blue;
            this.label6.Location = new System.Drawing.Point(191, 22);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(103, 18);
            this.label6.TabIndex = 91;
            this.label6.Text = "O. de Trabajo:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // fDevolucionInsumoOS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1043, 447);
            this.Controls.Add(this.grdRefacciones);
            this.Controls.Add(this.ToolStripMenu);
            this.Controls.Add(this.groupBox1);
            this.Name = "fDevolucionInsumoOS";
            this.Text = "fDevolucionInsumoOS";
            this.Load += new System.EventHandler(this.fDevolucionInsumoOS_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ToolStripMenu.ResumeLayout(false);
            this.ToolStripMenu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdRefacciones)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        internal System.Windows.Forms.ToolStrip ToolStripMenu;
        private System.Windows.Forms.ToolStripButton btnNuevo;
        private System.Windows.Forms.ToolStripButton btnGrabar;
        private System.Windows.Forms.ToolStripButton btnEditar;
        private System.Windows.Forms.ToolStripButton btnReporte;
        private System.Windows.Forms.ToolStripButton btnCancelar;
        private System.Windows.Forms.ToolStripButton btnSalir;
        private System.Windows.Forms.TextBox txtIdEmpresa;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtEstatus;
        private System.Windows.Forms.TextBox txtidUnidadTrans;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtidEmpleadoEntrega;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtTipoOrdenServ;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtTipoUnidad;
        private System.Windows.Forms.DateTimePicker dtpFechaHora;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpFecha;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtidOrdenSer;
        private System.Windows.Forms.DataGridView grdRefacciones;
        private System.Windows.Forms.ComboBox cmbIdOrdenTrabajo;
        private System.Windows.Forms.Label label6;
    }
}