﻿namespace Fletera2.Procesos
{
    partial class fOrdenServicioTaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fOrdenServicioTaller));
            this.ToolStripMenu = new System.Windows.Forms.ToolStrip();
            this.btnNuevo = new System.Windows.Forms.ToolStripButton();
            this.btnGrabar = new System.Windows.Forms.ToolStripButton();
            this.btnCancelar = new System.Windows.Forms.ToolStripButton();
            this.btnSalir = new System.Windows.Forms.ToolStripButton();
            this.gpoDatos = new System.Windows.Forms.GroupBox();
            this.txtIdEmpresa = new System.Windows.Forms.TextBox();
            this.txtidEmpleadoAutoriza = new System.Windows.Forms.TextBox();
            this.txtidOperador = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtOrdenDe = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtidTipOrdServ = new System.Windows.Forms.TextBox();
            this.dtpFechaEstatusHora = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.dtpFechaEstatus = new System.Windows.Forms.DateTimePicker();
            this.txtidUnidadTrans = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtTipoUnidad = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtidPadreOrdSer = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtEstatus = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.fechaCapturaHora = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.fechaCaptura = new System.Windows.Forms.DateTimePicker();
            this.label8 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtidOrdenSer = new System.Windows.Forms.TextBox();
            this.tbsDet = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.chkMarcaTodos = new System.Windows.Forms.CheckBox();
            this.grdOT = new System.Windows.Forms.DataGridView();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.asignarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eliminarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cambiarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nuevoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label12 = new System.Windows.Forms.Label();
            this.grdDetPreSalida = new System.Windows.Forms.DataGridView();
            this.grdCabPreSalida = new System.Windows.Forms.DataGridView();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.label13 = new System.Windows.Forms.Label();
            this.grdDetOC = new System.Windows.Forms.DataGridView();
            this.grdCabOC = new System.Windows.Forms.DataGridView();
            this.gpoOpera = new System.Windows.Forms.GroupBox();
            this.btnAsigna = new System.Windows.Forms.Button();
            this.cmbDetOpera3 = new System.Windows.Forms.ComboBox();
            this.cmbDetOpera2 = new System.Windows.Forms.ComboBox();
            this.cmbDetOpera1 = new System.Windows.Forms.ComboBox();
            this.chkOpera3 = new System.Windows.Forms.CheckBox();
            this.chkOpera2 = new System.Windows.Forms.CheckBox();
            this.lblopera3 = new System.Windows.Forms.Label();
            this.lblopera2 = new System.Windows.Forms.Label();
            this.cmbOpera3 = new System.Windows.Forms.ComboBox();
            this.cmbOpera2 = new System.Windows.Forms.ComboBox();
            this.cmbOpera1 = new System.Windows.Forms.ComboBox();
            this.lblopera1 = new System.Windows.Forms.Label();
            this.ToolStripMenu.SuspendLayout();
            this.gpoDatos.SuspendLayout();
            this.tbsDet.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdOT)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdDetPreSalida)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdCabPreSalida)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdDetOC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdCabOC)).BeginInit();
            this.gpoOpera.SuspendLayout();
            this.SuspendLayout();
            // 
            // ToolStripMenu
            // 
            this.ToolStripMenu.AutoSize = false;
            this.ToolStripMenu.BackColor = System.Drawing.SystemColors.Control;
            this.ToolStripMenu.Font = new System.Drawing.Font("Berlin Sans FB", 11.25F);
            this.ToolStripMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnNuevo,
            this.btnGrabar,
            this.btnCancelar,
            this.btnSalir});
            this.ToolStripMenu.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.ToolStripMenu.Location = new System.Drawing.Point(0, 0);
            this.ToolStripMenu.Name = "ToolStripMenu";
            this.ToolStripMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.ToolStripMenu.Size = new System.Drawing.Size(939, 54);
            this.ToolStripMenu.TabIndex = 19;
            this.ToolStripMenu.Text = "ToolStrip1";
            // 
            // btnNuevo
            // 
            this.btnNuevo.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnNuevo.Image = ((System.Drawing.Image)(resources.GetObject("btnNuevo.Image")));
            this.btnNuevo.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnNuevo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(54, 51);
            this.btnNuevo.Text = "&Nuevo";
            this.btnNuevo.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnNuevo.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnNuevo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnNuevo.Click += new System.EventHandler(this.btnNuevo_Click);
            // 
            // btnGrabar
            // 
            this.btnGrabar.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnGrabar.Image = ((System.Drawing.Image)(resources.GetObject("btnGrabar.Image")));
            this.btnGrabar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnGrabar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnGrabar.Name = "btnGrabar";
            this.btnGrabar.Size = new System.Drawing.Size(60, 51);
            this.btnGrabar.Text = "&Grabar";
            this.btnGrabar.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnGrabar.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnGrabar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnGrabar.Click += new System.EventHandler(this.btnGrabar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnCancelar.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar.Image")));
            this.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnCancelar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(69, 51);
            this.btnCancelar.Text = "&Cancelar";
            this.btnCancelar.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnCancelar.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnSalir.Image = ((System.Drawing.Image)(resources.GetObject("btnSalir.Image")));
            this.btnSalir.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnSalir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(55, 51);
            this.btnSalir.Text = "&S a l i r";
            this.btnSalir.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnSalir.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // gpoDatos
            // 
            this.gpoDatos.Controls.Add(this.txtIdEmpresa);
            this.gpoDatos.Controls.Add(this.txtidEmpleadoAutoriza);
            this.gpoDatos.Controls.Add(this.txtidOperador);
            this.gpoDatos.Controls.Add(this.label11);
            this.gpoDatos.Controls.Add(this.txtOrdenDe);
            this.gpoDatos.Controls.Add(this.label10);
            this.gpoDatos.Controls.Add(this.txtidTipOrdServ);
            this.gpoDatos.Controls.Add(this.dtpFechaEstatusHora);
            this.gpoDatos.Controls.Add(this.label6);
            this.gpoDatos.Controls.Add(this.dtpFechaEstatus);
            this.gpoDatos.Controls.Add(this.txtidUnidadTrans);
            this.gpoDatos.Controls.Add(this.label9);
            this.gpoDatos.Controls.Add(this.txtTipoUnidad);
            this.gpoDatos.Controls.Add(this.label7);
            this.gpoDatos.Controls.Add(this.txtidPadreOrdSer);
            this.gpoDatos.Controls.Add(this.label5);
            this.gpoDatos.Controls.Add(this.txtEstatus);
            this.gpoDatos.Controls.Add(this.label4);
            this.gpoDatos.Controls.Add(this.label3);
            this.gpoDatos.Controls.Add(this.fechaCapturaHora);
            this.gpoDatos.Controls.Add(this.label2);
            this.gpoDatos.Controls.Add(this.fechaCaptura);
            this.gpoDatos.Controls.Add(this.label8);
            this.gpoDatos.Controls.Add(this.label1);
            this.gpoDatos.Controls.Add(this.txtidOrdenSer);
            this.gpoDatos.Location = new System.Drawing.Point(12, 57);
            this.gpoDatos.Name = "gpoDatos";
            this.gpoDatos.Size = new System.Drawing.Size(804, 172);
            this.gpoDatos.TabIndex = 20;
            this.gpoDatos.TabStop = false;
            this.gpoDatos.Text = "Orden de Servicio";
            // 
            // txtIdEmpresa
            // 
            this.txtIdEmpresa.Enabled = false;
            this.txtIdEmpresa.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtIdEmpresa.Location = new System.Drawing.Point(122, 48);
            this.txtIdEmpresa.Name = "txtIdEmpresa";
            this.txtIdEmpresa.Size = new System.Drawing.Size(228, 25);
            this.txtIdEmpresa.TabIndex = 6;
            // 
            // txtidEmpleadoAutoriza
            // 
            this.txtidEmpleadoAutoriza.Enabled = false;
            this.txtidEmpleadoAutoriza.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtidEmpleadoAutoriza.Location = new System.Drawing.Point(6, 137);
            this.txtidEmpleadoAutoriza.Name = "txtidEmpleadoAutoriza";
            this.txtidEmpleadoAutoriza.Size = new System.Drawing.Size(248, 25);
            this.txtidEmpleadoAutoriza.TabIndex = 12;
            // 
            // txtidOperador
            // 
            this.txtidOperador.Enabled = false;
            this.txtidOperador.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtidOperador.Location = new System.Drawing.Point(501, 82);
            this.txtidOperador.Name = "txtidOperador";
            this.txtidOperador.Size = new System.Drawing.Size(291, 25);
            this.txtidOperador.TabIndex = 11;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Blue;
            this.label11.Location = new System.Drawing.Point(356, 21);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(75, 18);
            this.label11.TabIndex = 101;
            this.label11.Text = "Orden de:";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtOrdenDe
            // 
            this.txtOrdenDe.Enabled = false;
            this.txtOrdenDe.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtOrdenDe.Location = new System.Drawing.Point(437, 17);
            this.txtOrdenDe.Name = "txtOrdenDe";
            this.txtOrdenDe.Size = new System.Drawing.Size(152, 25);
            this.txtOrdenDe.TabIndex = 4;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Blue;
            this.label10.Location = new System.Drawing.Point(595, 16);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(40, 18);
            this.label10.TabIndex = 99;
            this.label10.Text = "Tipo:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtidTipOrdServ
            // 
            this.txtidTipOrdServ.Enabled = false;
            this.txtidTipOrdServ.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtidTipOrdServ.Location = new System.Drawing.Point(641, 13);
            this.txtidTipOrdServ.Name = "txtidTipOrdServ";
            this.txtidTipOrdServ.Size = new System.Drawing.Size(151, 25);
            this.txtidTipOrdServ.TabIndex = 5;
            // 
            // dtpFechaEstatusHora
            // 
            this.dtpFechaEstatusHora.Enabled = false;
            this.dtpFechaEstatusHora.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.dtpFechaEstatusHora.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpFechaEstatusHora.Location = new System.Drawing.Point(686, 137);
            this.dtpFechaEstatusHora.Name = "dtpFechaEstatusHora";
            this.dtpFechaEstatusHora.Size = new System.Drawing.Size(106, 25);
            this.dtpFechaEstatusHora.TabIndex = 15;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Blue;
            this.label6.Location = new System.Drawing.Point(414, 115);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 18);
            this.label6.TabIndex = 97;
            this.label6.Text = "Fecha:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dtpFechaEstatus
            // 
            this.dtpFechaEstatus.Enabled = false;
            this.dtpFechaEstatus.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.dtpFechaEstatus.Location = new System.Drawing.Point(417, 137);
            this.dtpFechaEstatus.Name = "dtpFechaEstatus";
            this.dtpFechaEstatus.Size = new System.Drawing.Size(263, 25);
            this.dtpFechaEstatus.TabIndex = 14;
            // 
            // txtidUnidadTrans
            // 
            this.txtidUnidadTrans.Enabled = false;
            this.txtidUnidadTrans.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtidUnidadTrans.Location = new System.Drawing.Point(298, 85);
            this.txtidUnidadTrans.Name = "txtidUnidadTrans";
            this.txtidUnidadTrans.Size = new System.Drawing.Size(75, 25);
            this.txtidUnidadTrans.TabIndex = 10;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Blue;
            this.label9.Location = new System.Drawing.Point(8, 85);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(135, 18);
            this.label9.TabIndex = 93;
            this.label9.Text = "Unidad Transporte:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtTipoUnidad
            // 
            this.txtTipoUnidad.Enabled = false;
            this.txtTipoUnidad.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtTipoUnidad.Location = new System.Drawing.Point(156, 85);
            this.txtTipoUnidad.Name = "txtTipoUnidad";
            this.txtTipoUnidad.Size = new System.Drawing.Size(136, 25);
            this.txtTipoUnidad.TabIndex = 9;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Blue;
            this.label7.Location = new System.Drawing.Point(213, 20);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 18);
            this.label7.TabIndex = 80;
            this.label7.Text = "FOLIO:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtidPadreOrdSer
            // 
            this.txtidPadreOrdSer.Enabled = false;
            this.txtidPadreOrdSer.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtidPadreOrdSer.Location = new System.Drawing.Point(275, 18);
            this.txtidPadreOrdSer.Name = "txtidPadreOrdSer";
            this.txtidPadreOrdSer.Size = new System.Drawing.Size(75, 25);
            this.txtidPadreOrdSer.TabIndex = 3;
            this.txtidPadreOrdSer.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Blue;
            this.label5.Location = new System.Drawing.Point(260, 140);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 18);
            this.label5.TabIndex = 76;
            this.label5.Text = "Estatus:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtEstatus
            // 
            this.txtEstatus.Enabled = false;
            this.txtEstatus.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtEstatus.Location = new System.Drawing.Point(330, 137);
            this.txtEstatus.Name = "txtEstatus";
            this.txtEstatus.Size = new System.Drawing.Size(68, 25);
            this.txtEstatus.TabIndex = 13;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Blue;
            this.label4.Location = new System.Drawing.Point(12, 115);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 18);
            this.label4.TabIndex = 73;
            this.label4.Text = "Autoriza";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Blue;
            this.label3.Location = new System.Drawing.Point(418, 84);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 18);
            this.label3.TabIndex = 71;
            this.label3.Text = "Operador:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // fechaCapturaHora
            // 
            this.fechaCapturaHora.Enabled = false;
            this.fechaCapturaHora.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.fechaCapturaHora.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.fechaCapturaHora.Location = new System.Drawing.Point(686, 48);
            this.fechaCapturaHora.Name = "fechaCapturaHora";
            this.fechaCapturaHora.Size = new System.Drawing.Size(106, 25);
            this.fechaCapturaHora.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Blue;
            this.label2.Location = new System.Drawing.Point(359, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 18);
            this.label2.TabIndex = 24;
            this.label2.Text = "Fecha:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // fechaCaptura
            // 
            this.fechaCaptura.Enabled = false;
            this.fechaCaptura.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.fechaCaptura.Location = new System.Drawing.Point(417, 48);
            this.fechaCaptura.Name = "fechaCaptura";
            this.fechaCaptura.Size = new System.Drawing.Size(263, 25);
            this.fechaCaptura.TabIndex = 7;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Blue;
            this.label8.Location = new System.Drawing.Point(12, 56);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(56, 18);
            this.label8.TabIndex = 21;
            this.label8.Text = "Cliente:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(11, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 18);
            this.label1.TabIndex = 7;
            this.label1.Text = "Orden Servicio:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtidOrdenSer
            // 
            this.txtidOrdenSer.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtidOrdenSer.Location = new System.Drawing.Point(122, 17);
            this.txtidOrdenSer.Name = "txtidOrdenSer";
            this.txtidOrdenSer.Size = new System.Drawing.Size(75, 25);
            this.txtidOrdenSer.TabIndex = 2;
            this.txtidOrdenSer.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtidOrdenSer.TextChanged += new System.EventHandler(this.txtidOrdenSer_TextChanged);
            this.txtidOrdenSer.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtidOrdenSer_KeyDown);
            this.txtidOrdenSer.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtidOrdenSer_KeyPress);
            // 
            // tbsDet
            // 
            this.tbsDet.Controls.Add(this.tabPage1);
            this.tbsDet.Controls.Add(this.tabPage2);
            this.tbsDet.Controls.Add(this.tabPage3);
            this.tbsDet.Location = new System.Drawing.Point(12, 356);
            this.tbsDet.Name = "tbsDet";
            this.tbsDet.SelectedIndex = 0;
            this.tbsDet.Size = new System.Drawing.Size(920, 351);
            this.tbsDet.TabIndex = 21;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.chkMarcaTodos);
            this.tabPage1.Controls.Add(this.grdOT);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(912, 325);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "ORDENES DE TRABAJO";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // chkMarcaTodos
            // 
            this.chkMarcaTodos.AutoSize = true;
            this.chkMarcaTodos.Location = new System.Drawing.Point(11, 216);
            this.chkMarcaTodos.Name = "chkMarcaTodos";
            this.chkMarcaTodos.Size = new System.Drawing.Size(56, 17);
            this.chkMarcaTodos.TabIndex = 85;
            this.chkMarcaTodos.Text = "Todos";
            this.chkMarcaTodos.UseVisualStyleBackColor = true;
            this.chkMarcaTodos.CheckedChanged += new System.EventHandler(this.chkMarcaTodos_CheckedChanged);
            // 
            // grdOT
            // 
            this.grdOT.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdOT.ContextMenuStrip = this.contextMenuStrip1;
            this.grdOT.Location = new System.Drawing.Point(11, 6);
            this.grdOT.Name = "grdOT";
            this.grdOT.Size = new System.Drawing.Size(889, 204);
            this.grdOT.TabIndex = 49;
            this.grdOT.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdOT_CellContentClick);
            this.grdOT.Click += new System.EventHandler(this.grdOT_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.asignarToolStripMenuItem,
            this.eliminarToolStripMenuItem,
            this.cambiarToolStripMenuItem,
            this.nuevoToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(170, 92);
            this.contextMenuStrip1.Text = "Asignar";
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // asignarToolStripMenuItem
            // 
            this.asignarToolStripMenuItem.Name = "asignarToolStripMenuItem";
            this.asignarToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.asignarToolStripMenuItem.Text = "Asignar Marcados";
            this.asignarToolStripMenuItem.Click += new System.EventHandler(this.asignarToolStripMenuItem_Click);
            // 
            // eliminarToolStripMenuItem
            // 
            this.eliminarToolStripMenuItem.Name = "eliminarToolStripMenuItem";
            this.eliminarToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.eliminarToolStripMenuItem.Text = "Cancelar";
            this.eliminarToolStripMenuItem.Click += new System.EventHandler(this.eliminarToolStripMenuItem_Click);
            // 
            // cambiarToolStripMenuItem
            // 
            this.cambiarToolStripMenuItem.Name = "cambiarToolStripMenuItem";
            this.cambiarToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.cambiarToolStripMenuItem.Text = "Cambiar";
            this.cambiarToolStripMenuItem.Click += new System.EventHandler(this.cambiarToolStripMenuItem_Click);
            // 
            // nuevoToolStripMenuItem
            // 
            this.nuevoToolStripMenuItem.Name = "nuevoToolStripMenuItem";
            this.nuevoToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.nuevoToolStripMenuItem.Text = "Nuevo";
            this.nuevoToolStripMenuItem.Click += new System.EventHandler(this.nuevoToolStripMenuItem_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.label12);
            this.tabPage2.Controls.Add(this.grdDetPreSalida);
            this.tabPage2.Controls.Add(this.grdCabPreSalida);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(912, 325);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "PRE SALIDAS";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Blue;
            this.label12.Location = new System.Drawing.Point(10, 126);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(72, 18);
            this.label12.TabIndex = 80;
            this.label12.Text = "DETALLE";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // grdDetPreSalida
            // 
            this.grdDetPreSalida.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdDetPreSalida.Location = new System.Drawing.Point(10, 147);
            this.grdDetPreSalida.Name = "grdDetPreSalida";
            this.grdDetPreSalida.Size = new System.Drawing.Size(892, 142);
            this.grdDetPreSalida.TabIndex = 8;
            // 
            // grdCabPreSalida
            // 
            this.grdCabPreSalida.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdCabPreSalida.Location = new System.Drawing.Point(11, 6);
            this.grdCabPreSalida.Name = "grdCabPreSalida";
            this.grdCabPreSalida.Size = new System.Drawing.Size(895, 114);
            this.grdCabPreSalida.TabIndex = 7;
            this.grdCabPreSalida.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdCabPreSalida_CellContentClick);
            this.grdCabPreSalida.Click += new System.EventHandler(this.grdCabPreSalida_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.label13);
            this.tabPage3.Controls.Add(this.grdDetOC);
            this.tabPage3.Controls.Add(this.grdCabOC);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(912, 325);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "PRE COMPRAS";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Blue;
            this.label13.Location = new System.Drawing.Point(8, 123);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(72, 18);
            this.label13.TabIndex = 81;
            this.label13.Text = "DETALLE";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // grdDetOC
            // 
            this.grdDetOC.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdDetOC.Location = new System.Drawing.Point(10, 147);
            this.grdDetOC.Name = "grdDetOC";
            this.grdDetOC.Size = new System.Drawing.Size(892, 142);
            this.grdDetOC.TabIndex = 6;
            // 
            // grdCabOC
            // 
            this.grdCabOC.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdCabOC.Location = new System.Drawing.Point(11, 6);
            this.grdCabOC.Name = "grdCabOC";
            this.grdCabOC.Size = new System.Drawing.Size(895, 114);
            this.grdCabOC.TabIndex = 5;
            this.grdCabOC.Click += new System.EventHandler(this.grdCabOC_Click);
            // 
            // gpoOpera
            // 
            this.gpoOpera.Controls.Add(this.btnAsigna);
            this.gpoOpera.Controls.Add(this.cmbDetOpera3);
            this.gpoOpera.Controls.Add(this.cmbDetOpera2);
            this.gpoOpera.Controls.Add(this.cmbDetOpera1);
            this.gpoOpera.Controls.Add(this.chkOpera3);
            this.gpoOpera.Controls.Add(this.chkOpera2);
            this.gpoOpera.Controls.Add(this.lblopera3);
            this.gpoOpera.Controls.Add(this.lblopera2);
            this.gpoOpera.Controls.Add(this.cmbOpera3);
            this.gpoOpera.Controls.Add(this.cmbOpera2);
            this.gpoOpera.Controls.Add(this.cmbOpera1);
            this.gpoOpera.Controls.Add(this.lblopera1);
            this.gpoOpera.Location = new System.Drawing.Point(12, 235);
            this.gpoOpera.Name = "gpoOpera";
            this.gpoOpera.Size = new System.Drawing.Size(680, 115);
            this.gpoOpera.TabIndex = 22;
            this.gpoOpera.TabStop = false;
            // 
            // btnAsigna
            // 
            this.btnAsigna.Enabled = false;
            this.btnAsigna.Image = global::Fletera2.Properties.Resources.AddMecanico;
            this.btnAsigna.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnAsigna.Location = new System.Drawing.Point(631, 17);
            this.btnAsigna.Name = "btnAsigna";
            this.btnAsigna.Size = new System.Drawing.Size(41, 39);
            this.btnAsigna.TabIndex = 88;
            this.btnAsigna.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnAsigna.UseVisualStyleBackColor = true;
            this.btnAsigna.Click += new System.EventHandler(this.btnAsigna_Click);
            // 
            // cmbDetOpera3
            // 
            this.cmbDetOpera3.Enabled = false;
            this.cmbDetOpera3.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.cmbDetOpera3.FormattingEnabled = true;
            this.cmbDetOpera3.Location = new System.Drawing.Point(435, 82);
            this.cmbDetOpera3.Name = "cmbDetOpera3";
            this.cmbDetOpera3.Size = new System.Drawing.Size(180, 26);
            this.cmbDetOpera3.TabIndex = 87;
            // 
            // cmbDetOpera2
            // 
            this.cmbDetOpera2.Enabled = false;
            this.cmbDetOpera2.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.cmbDetOpera2.FormattingEnabled = true;
            this.cmbDetOpera2.Location = new System.Drawing.Point(435, 51);
            this.cmbDetOpera2.Name = "cmbDetOpera2";
            this.cmbDetOpera2.Size = new System.Drawing.Size(180, 26);
            this.cmbDetOpera2.TabIndex = 86;
            // 
            // cmbDetOpera1
            // 
            this.cmbDetOpera1.Enabled = false;
            this.cmbDetOpera1.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.cmbDetOpera1.FormattingEnabled = true;
            this.cmbDetOpera1.Location = new System.Drawing.Point(435, 18);
            this.cmbDetOpera1.Name = "cmbDetOpera1";
            this.cmbDetOpera1.Size = new System.Drawing.Size(180, 26);
            this.cmbDetOpera1.TabIndex = 85;
            // 
            // chkOpera3
            // 
            this.chkOpera3.AutoSize = true;
            this.chkOpera3.Location = new System.Drawing.Point(631, 89);
            this.chkOpera3.Name = "chkOpera3";
            this.chkOpera3.Size = new System.Drawing.Size(15, 14);
            this.chkOpera3.TabIndex = 84;
            this.chkOpera3.UseVisualStyleBackColor = true;
            this.chkOpera3.CheckedChanged += new System.EventHandler(this.chkOpera3_CheckedChanged);
            // 
            // chkOpera2
            // 
            this.chkOpera2.AutoSize = true;
            this.chkOpera2.Location = new System.Drawing.Point(631, 62);
            this.chkOpera2.Name = "chkOpera2";
            this.chkOpera2.Size = new System.Drawing.Size(15, 14);
            this.chkOpera2.TabIndex = 83;
            this.chkOpera2.UseVisualStyleBackColor = true;
            this.chkOpera2.CheckedChanged += new System.EventHandler(this.chkOpera2_CheckedChanged);
            // 
            // lblopera3
            // 
            this.lblopera3.AutoSize = true;
            this.lblopera3.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblopera3.ForeColor = System.Drawing.Color.Blue;
            this.lblopera3.Location = new System.Drawing.Point(11, 86);
            this.lblopera3.Name = "lblopera3";
            this.lblopera3.Size = new System.Drawing.Size(70, 18);
            this.lblopera3.TabIndex = 79;
            this.lblopera3.Text = "lblopera3";
            this.lblopera3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblopera2
            // 
            this.lblopera2.AutoSize = true;
            this.lblopera2.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblopera2.ForeColor = System.Drawing.Color.Blue;
            this.lblopera2.Location = new System.Drawing.Point(11, 54);
            this.lblopera2.Name = "lblopera2";
            this.lblopera2.Size = new System.Drawing.Size(70, 18);
            this.lblopera2.TabIndex = 78;
            this.lblopera2.Text = "lblopera2";
            this.lblopera2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cmbOpera3
            // 
            this.cmbOpera3.Enabled = false;
            this.cmbOpera3.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.cmbOpera3.FormattingEnabled = true;
            this.cmbOpera3.Location = new System.Drawing.Point(122, 82);
            this.cmbOpera3.Name = "cmbOpera3";
            this.cmbOpera3.Size = new System.Drawing.Size(307, 26);
            this.cmbOpera3.TabIndex = 77;
            // 
            // cmbOpera2
            // 
            this.cmbOpera2.Enabled = false;
            this.cmbOpera2.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.cmbOpera2.FormattingEnabled = true;
            this.cmbOpera2.Location = new System.Drawing.Point(122, 50);
            this.cmbOpera2.Name = "cmbOpera2";
            this.cmbOpera2.Size = new System.Drawing.Size(307, 26);
            this.cmbOpera2.TabIndex = 76;
            // 
            // cmbOpera1
            // 
            this.cmbOpera1.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.cmbOpera1.FormattingEnabled = true;
            this.cmbOpera1.Location = new System.Drawing.Point(122, 18);
            this.cmbOpera1.Name = "cmbOpera1";
            this.cmbOpera1.Size = new System.Drawing.Size(307, 26);
            this.cmbOpera1.TabIndex = 74;
            this.cmbOpera1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.cmbOpera1_KeyUp);
            // 
            // lblopera1
            // 
            this.lblopera1.AutoSize = true;
            this.lblopera1.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblopera1.ForeColor = System.Drawing.Color.Blue;
            this.lblopera1.Location = new System.Drawing.Point(12, 21);
            this.lblopera1.Name = "lblopera1";
            this.lblopera1.Size = new System.Drawing.Size(66, 18);
            this.lblopera1.TabIndex = 75;
            this.lblopera1.Text = "lblopera1";
            this.lblopera1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // fOrdenServicioTaller
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(939, 749);
            this.Controls.Add(this.gpoOpera);
            this.Controls.Add(this.tbsDet);
            this.Controls.Add(this.gpoDatos);
            this.Controls.Add(this.ToolStripMenu);
            this.Name = "fOrdenServicioTaller";
            this.Text = "Ordenes de Servicio";
            this.Load += new System.EventHandler(this.fOrdenServicioTaller_Load);
            this.ToolStripMenu.ResumeLayout(false);
            this.ToolStripMenu.PerformLayout();
            this.gpoDatos.ResumeLayout(false);
            this.gpoDatos.PerformLayout();
            this.tbsDet.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdOT)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdDetPreSalida)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdCabPreSalida)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdDetOC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdCabOC)).EndInit();
            this.gpoOpera.ResumeLayout(false);
            this.gpoOpera.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.ToolStrip ToolStripMenu;
        private System.Windows.Forms.ToolStripButton btnNuevo;
        private System.Windows.Forms.ToolStripButton btnGrabar;
        private System.Windows.Forms.ToolStripButton btnCancelar;
        private System.Windows.Forms.ToolStripButton btnSalir;
        private System.Windows.Forms.GroupBox gpoDatos;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtEstatus;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker fechaCapturaHora;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker fechaCaptura;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtidOrdenSer;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtidPadreOrdSer;
        private System.Windows.Forms.TextBox txtidUnidadTrans;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtTipoUnidad;
        private System.Windows.Forms.TabControl tbsDet;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtOrdenDe;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtidTipOrdServ;
        private System.Windows.Forms.DateTimePicker dtpFechaEstatusHora;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker dtpFechaEstatus;
        private System.Windows.Forms.DataGridView grdOT;
        private System.Windows.Forms.TextBox txtidEmpleadoAutoriza;
        private System.Windows.Forms.TextBox txtidOperador;
        private System.Windows.Forms.TextBox txtIdEmpresa;
        private System.Windows.Forms.GroupBox gpoOpera;
        private System.Windows.Forms.ComboBox cmbOpera1;
        private System.Windows.Forms.Label lblopera1;
        private System.Windows.Forms.CheckBox chkOpera3;
        private System.Windows.Forms.CheckBox chkOpera2;
        private System.Windows.Forms.Label lblopera3;
        private System.Windows.Forms.Label lblopera2;
        private System.Windows.Forms.ComboBox cmbOpera3;
        private System.Windows.Forms.ComboBox cmbOpera2;
        private System.Windows.Forms.ComboBox cmbDetOpera3;
        private System.Windows.Forms.ComboBox cmbDetOpera2;
        private System.Windows.Forms.ComboBox cmbDetOpera1;
        private System.Windows.Forms.Button btnAsigna;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem asignarToolStripMenuItem;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView grdDetPreSalida;
        private System.Windows.Forms.DataGridView grdCabPreSalida;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.DataGridView grdDetOC;
        private System.Windows.Forms.DataGridView grdCabOC;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.CheckBox chkMarcaTodos;
        private System.Windows.Forms.ToolStripMenuItem eliminarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cambiarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nuevoToolStripMenuItem;
    }
}