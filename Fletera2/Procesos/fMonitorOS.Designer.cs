﻿namespace Fletera2.Procesos
{
    partial class fMonitorOS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fMonitorOS));
            this.grdCabOS = new System.Windows.Forms.DataGridView();
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            this.chkOstesFin = new System.Windows.Forms.CheckBox();
            this.chkOstesIni = new System.Windows.Forms.CheckBox();
            this.ChkEnt = new System.Windows.Forms.CheckBox();
            this.ChkCan = new System.Windows.Forms.CheckBox();
            this.ChkTer = new System.Windows.Forms.CheckBox();
            this.ChkAsig = new System.Windows.Forms.CheckBox();
            this.ChkCap = new System.Windows.Forms.CheckBox();
            this.ChkRecep = new System.Windows.Forms.CheckBox();
            this.gpoTOS = new System.Windows.Forms.GroupBox();
            this.chkTosTaller = new System.Windows.Forms.CheckBox();
            this.chkTosLlan = new System.Windows.Forms.CheckBox();
            this.chkTosComb = new System.Windows.Forms.CheckBox();
            this.chkTosLav = new System.Windows.Forms.CheckBox();
            this.GroupBox4 = new System.Windows.Forms.GroupBox();
            this.rdbOrden = new System.Windows.Forms.RadioButton();
            this.rdbFolio = new System.Windows.Forms.RadioButton();
            this.rdbUnidad = new System.Windows.Forms.RadioButton();
            this.rdbEmpresa = new System.Windows.Forms.RadioButton();
            this.chkOtroFil = new System.Windows.Forms.CheckBox();
            this.cmbIdEmpresa = new System.Windows.Forms.ComboBox();
            this.txtOrden = new System.Windows.Forms.TextBox();
            this.txtPadre = new System.Windows.Forms.TextBox();
            this.txtUnidad = new System.Windows.Forms.TextBox();
            this.LTiempo = new System.Windows.Forms.Label();
            this.GroupBox2 = new System.Windows.Forms.GroupBox();
            this.chkRango = new System.Windows.Forms.RadioButton();
            this.ChkTmpReal = new System.Windows.Forms.RadioButton();
            this.dtpFecFin = new System.Windows.Forms.DateTimePicker();
            this.dtpFecIni = new System.Windows.Forms.DateTimePicker();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.ToolStripMenu = new System.Windows.Forms.ToolStrip();
            this.btnSalir = new System.Windows.Forms.ToolStripButton();
            this.btnCancelar = new System.Windows.Forms.ToolStripButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.chkVerUsers = new System.Windows.Forms.CheckBox();
            this.txtFiltro = new System.Windows.Forms.TextBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.grdDetOS = new System.Windows.Forms.DataGridView();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            ((System.ComponentModel.ISupportInitialize)(this.grdCabOS)).BeginInit();
            this.GroupBox1.SuspendLayout();
            this.gpoTOS.SuspendLayout();
            this.GroupBox4.SuspendLayout();
            this.GroupBox2.SuspendLayout();
            this.ToolStripMenu.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdDetOS)).BeginInit();
            this.statusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // grdCabOS
            // 
            this.grdCabOS.AllowUserToAddRows = false;
            this.grdCabOS.AllowUserToDeleteRows = false;
            this.grdCabOS.AllowUserToOrderColumns = true;
            this.grdCabOS.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdCabOS.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdCabOS.Location = new System.Drawing.Point(12, 237);
            this.grdCabOS.Name = "grdCabOS";
            this.grdCabOS.Size = new System.Drawing.Size(1034, 217);
            this.grdCabOS.TabIndex = 0;
            this.grdCabOS.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdCabOS_CellContentClick);
            this.grdCabOS.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.grdCabOS_RowPostPaint);
            this.grdCabOS.Click += new System.EventHandler(this.grdCabOS_Click);
            // 
            // GroupBox1
            // 
            this.GroupBox1.Controls.Add(this.chkOstesFin);
            this.GroupBox1.Controls.Add(this.chkOstesIni);
            this.GroupBox1.Controls.Add(this.ChkEnt);
            this.GroupBox1.Controls.Add(this.ChkCan);
            this.GroupBox1.Controls.Add(this.ChkTer);
            this.GroupBox1.Controls.Add(this.ChkAsig);
            this.GroupBox1.Controls.Add(this.ChkCap);
            this.GroupBox1.Controls.Add(this.ChkRecep);
            this.GroupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GroupBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.GroupBox1.Location = new System.Drawing.Point(12, 64);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Size = new System.Drawing.Size(888, 48);
            this.GroupBox1.TabIndex = 75;
            this.GroupBox1.TabStop = false;
            this.GroupBox1.Text = "Estatus de las Ordenes de Servicio";
            // 
            // chkOstesFin
            // 
            this.chkOstesFin.AutoSize = true;
            this.chkOstesFin.BackColor = System.Drawing.SystemColors.Control;
            this.chkOstesFin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkOstesFin.ForeColor = System.Drawing.Color.Black;
            this.chkOstesFin.Location = new System.Drawing.Point(781, 21);
            this.chkOstesFin.Name = "chkOstesFin";
            this.chkOstesFin.Size = new System.Drawing.Size(101, 20);
            this.chkOstesFin.TabIndex = 8;
            this.chkOstesFin.Text = "Cierre Ostes";
            this.chkOstesFin.UseVisualStyleBackColor = false;
            this.chkOstesFin.CheckedChanged += new System.EventHandler(this.chkOstesFin_CheckedChanged);
            // 
            // chkOstesIni
            // 
            this.chkOstesIni.AutoSize = true;
            this.chkOstesIni.BackColor = System.Drawing.SystemColors.Control;
            this.chkOstesIni.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkOstesIni.ForeColor = System.Drawing.Color.Black;
            this.chkOstesIni.Location = new System.Drawing.Point(659, 21);
            this.chkOstesIni.Name = "chkOstesIni";
            this.chkOstesIni.Size = new System.Drawing.Size(116, 20);
            this.chkOstesIni.TabIndex = 7;
            this.chkOstesIni.Text = "Apertura Ostes";
            this.chkOstesIni.UseVisualStyleBackColor = false;
            this.chkOstesIni.CheckedChanged += new System.EventHandler(this.chkOstesIni_CheckedChanged);
            // 
            // ChkEnt
            // 
            this.ChkEnt.AutoSize = true;
            this.ChkEnt.BackColor = System.Drawing.SystemColors.Control;
            this.ChkEnt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkEnt.ForeColor = System.Drawing.Color.Black;
            this.ChkEnt.Location = new System.Drawing.Point(450, 21);
            this.ChkEnt.Name = "ChkEnt";
            this.ChkEnt.Size = new System.Drawing.Size(97, 20);
            this.ChkEnt.TabIndex = 6;
            this.ChkEnt.Text = "Entregados";
            this.ChkEnt.UseVisualStyleBackColor = false;
            this.ChkEnt.Visible = false;
            // 
            // ChkCan
            // 
            this.ChkCan.AutoSize = true;
            this.ChkCan.BackColor = System.Drawing.SystemColors.Control;
            this.ChkCan.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCan.ForeColor = System.Drawing.Color.Black;
            this.ChkCan.Location = new System.Drawing.Point(553, 21);
            this.ChkCan.Name = "ChkCan";
            this.ChkCan.Size = new System.Drawing.Size(100, 20);
            this.ChkCan.TabIndex = 3;
            this.ChkCan.Text = "Cancelados";
            this.ChkCan.UseVisualStyleBackColor = false;
            this.ChkCan.CheckedChanged += new System.EventHandler(this.ChkCan_CheckedChanged);
            // 
            // ChkTer
            // 
            this.ChkTer.AutoSize = true;
            this.ChkTer.BackColor = System.Drawing.SystemColors.Control;
            this.ChkTer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkTer.ForeColor = System.Drawing.Color.Black;
            this.ChkTer.Location = new System.Drawing.Point(340, 21);
            this.ChkTer.Name = "ChkTer";
            this.ChkTer.Size = new System.Drawing.Size(100, 20);
            this.ChkTer.TabIndex = 2;
            this.ChkTer.Text = "Terminados";
            this.ChkTer.UseVisualStyleBackColor = false;
            this.ChkTer.CheckedChanged += new System.EventHandler(this.ChkTer_CheckedChanged);
            // 
            // ChkAsig
            // 
            this.ChkAsig.AutoSize = true;
            this.ChkAsig.BackColor = System.Drawing.SystemColors.Control;
            this.ChkAsig.Checked = true;
            this.ChkAsig.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ChkAsig.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkAsig.ForeColor = System.Drawing.Color.Black;
            this.ChkAsig.Location = new System.Drawing.Point(246, 21);
            this.ChkAsig.Name = "ChkAsig";
            this.ChkAsig.Size = new System.Drawing.Size(92, 20);
            this.ChkAsig.TabIndex = 1;
            this.ChkAsig.Text = "Asignados";
            this.ChkAsig.UseVisualStyleBackColor = false;
            this.ChkAsig.CheckedChanged += new System.EventHandler(this.ChkAsig_CheckedChanged);
            // 
            // ChkCap
            // 
            this.ChkCap.AutoSize = true;
            this.ChkCap.BackColor = System.Drawing.SystemColors.Control;
            this.ChkCap.Checked = true;
            this.ChkCap.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ChkCap.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCap.ForeColor = System.Drawing.Color.Black;
            this.ChkCap.Location = new System.Drawing.Point(13, 21);
            this.ChkCap.Name = "ChkCap";
            this.ChkCap.Size = new System.Drawing.Size(97, 20);
            this.ChkCap.TabIndex = 0;
            this.ChkCap.Text = "Capturados";
            this.ChkCap.UseVisualStyleBackColor = false;
            this.ChkCap.CheckedChanged += new System.EventHandler(this.ChkCap_CheckedChanged);
            // 
            // ChkRecep
            // 
            this.ChkRecep.AutoSize = true;
            this.ChkRecep.BackColor = System.Drawing.SystemColors.Control;
            this.ChkRecep.Checked = true;
            this.ChkRecep.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ChkRecep.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkRecep.ForeColor = System.Drawing.Color.Black;
            this.ChkRecep.Location = new System.Drawing.Point(116, 21);
            this.ChkRecep.Name = "ChkRecep";
            this.ChkRecep.Size = new System.Drawing.Size(124, 20);
            this.ChkRecep.TabIndex = 4;
            this.ChkRecep.Text = "Recepcionados";
            this.ChkRecep.UseVisualStyleBackColor = false;
            this.ChkRecep.CheckedChanged += new System.EventHandler(this.ChkRecep_CheckedChanged);
            // 
            // gpoTOS
            // 
            this.gpoTOS.Controls.Add(this.chkTosTaller);
            this.gpoTOS.Controls.Add(this.chkTosLlan);
            this.gpoTOS.Controls.Add(this.chkTosComb);
            this.gpoTOS.Controls.Add(this.chkTosLav);
            this.gpoTOS.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpoTOS.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.gpoTOS.Location = new System.Drawing.Point(12, 118);
            this.gpoTOS.Name = "gpoTOS";
            this.gpoTOS.Size = new System.Drawing.Size(444, 57);
            this.gpoTOS.TabIndex = 81;
            this.gpoTOS.TabStop = false;
            this.gpoTOS.Text = "Tipos de Ordenes de Servicio";
            // 
            // chkTosTaller
            // 
            this.chkTosTaller.AutoSize = true;
            this.chkTosTaller.BackColor = System.Drawing.SystemColors.Control;
            this.chkTosTaller.Checked = true;
            this.chkTosTaller.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkTosTaller.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkTosTaller.ForeColor = System.Drawing.Color.Black;
            this.chkTosTaller.Location = new System.Drawing.Point(370, 21);
            this.chkTosTaller.Name = "chkTosTaller";
            this.chkTosTaller.Size = new System.Drawing.Size(62, 20);
            this.chkTosTaller.TabIndex = 5;
            this.chkTosTaller.Text = "Taller";
            this.chkTosTaller.UseVisualStyleBackColor = false;
            this.chkTosTaller.CheckedChanged += new System.EventHandler(this.chkTosTaller_CheckedChanged);
            // 
            // chkTosLlan
            // 
            this.chkTosLlan.AutoSize = true;
            this.chkTosLlan.BackColor = System.Drawing.SystemColors.Control;
            this.chkTosLlan.Checked = true;
            this.chkTosLlan.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkTosLlan.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkTosLlan.ForeColor = System.Drawing.Color.Black;
            this.chkTosLlan.Location = new System.Drawing.Point(268, 21);
            this.chkTosLlan.Name = "chkTosLlan";
            this.chkTosLlan.Size = new System.Drawing.Size(70, 20);
            this.chkTosLlan.TabIndex = 1;
            this.chkTosLlan.Text = "Llantas";
            this.chkTosLlan.UseVisualStyleBackColor = false;
            this.chkTosLlan.CheckedChanged += new System.EventHandler(this.chkTosLlan_CheckedChanged);
            // 
            // chkTosComb
            // 
            this.chkTosComb.AutoSize = true;
            this.chkTosComb.BackColor = System.Drawing.SystemColors.Control;
            this.chkTosComb.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkTosComb.ForeColor = System.Drawing.Color.Black;
            this.chkTosComb.Location = new System.Drawing.Point(13, 21);
            this.chkTosComb.Name = "chkTosComb";
            this.chkTosComb.Size = new System.Drawing.Size(102, 20);
            this.chkTosComb.TabIndex = 0;
            this.chkTosComb.Text = "Combustible";
            this.chkTosComb.UseVisualStyleBackColor = false;
            this.chkTosComb.CheckedChanged += new System.EventHandler(this.chkTosComb_CheckedChanged);
            // 
            // chkTosLav
            // 
            this.chkTosLav.AutoSize = true;
            this.chkTosLav.BackColor = System.Drawing.SystemColors.Control;
            this.chkTosLav.Checked = true;
            this.chkTosLav.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkTosLav.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkTosLav.ForeColor = System.Drawing.Color.Black;
            this.chkTosLav.Location = new System.Drawing.Point(155, 21);
            this.chkTosLav.Name = "chkTosLav";
            this.chkTosLav.Size = new System.Drawing.Size(85, 20);
            this.chkTosLav.TabIndex = 4;
            this.chkTosLav.Text = "Lavadero";
            this.chkTosLav.UseVisualStyleBackColor = false;
            this.chkTosLav.CheckedChanged += new System.EventHandler(this.chkTosLav_CheckedChanged);
            // 
            // GroupBox4
            // 
            this.GroupBox4.Controls.Add(this.rdbOrden);
            this.GroupBox4.Controls.Add(this.rdbFolio);
            this.GroupBox4.Controls.Add(this.rdbUnidad);
            this.GroupBox4.Controls.Add(this.rdbEmpresa);
            this.GroupBox4.Controls.Add(this.chkOtroFil);
            this.GroupBox4.Controls.Add(this.cmbIdEmpresa);
            this.GroupBox4.Controls.Add(this.txtOrden);
            this.GroupBox4.Controls.Add(this.txtPadre);
            this.GroupBox4.Controls.Add(this.txtUnidad);
            this.GroupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GroupBox4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.GroupBox4.Location = new System.Drawing.Point(462, 118);
            this.GroupBox4.Name = "GroupBox4";
            this.GroupBox4.Size = new System.Drawing.Size(701, 57);
            this.GroupBox4.TabIndex = 82;
            this.GroupBox4.TabStop = false;
            this.GroupBox4.Text = "Otros Filtros";
            // 
            // rdbOrden
            // 
            this.rdbOrden.AutoSize = true;
            this.rdbOrden.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.rdbOrden.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.rdbOrden.Location = new System.Drawing.Point(555, 27);
            this.rdbOrden.Name = "rdbOrden";
            this.rdbOrden.Size = new System.Drawing.Size(64, 17);
            this.rdbOrden.TabIndex = 120;
            this.rdbOrden.Text = "ORDEN";
            this.rdbOrden.UseVisualStyleBackColor = true;
            // 
            // rdbFolio
            // 
            this.rdbFolio.AutoSize = true;
            this.rdbFolio.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.rdbFolio.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.rdbFolio.Location = new System.Drawing.Point(411, 27);
            this.rdbFolio.Name = "rdbFolio";
            this.rdbFolio.Size = new System.Drawing.Size(56, 17);
            this.rdbFolio.TabIndex = 119;
            this.rdbFolio.Text = "FOLIO";
            this.rdbFolio.UseVisualStyleBackColor = true;
            // 
            // rdbUnidad
            // 
            this.rdbUnidad.AutoSize = true;
            this.rdbUnidad.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.rdbUnidad.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.rdbUnidad.Location = new System.Drawing.Point(244, 26);
            this.rdbUnidad.Name = "rdbUnidad";
            this.rdbUnidad.Size = new System.Drawing.Size(67, 17);
            this.rdbUnidad.TabIndex = 118;
            this.rdbUnidad.Text = "UNIDAD";
            this.rdbUnidad.UseVisualStyleBackColor = true;
            // 
            // rdbEmpresa
            // 
            this.rdbEmpresa.AutoSize = true;
            this.rdbEmpresa.Checked = true;
            this.rdbEmpresa.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.rdbEmpresa.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.rdbEmpresa.Location = new System.Drawing.Point(31, 26);
            this.rdbEmpresa.Name = "rdbEmpresa";
            this.rdbEmpresa.Size = new System.Drawing.Size(77, 17);
            this.rdbEmpresa.TabIndex = 117;
            this.rdbEmpresa.TabStop = true;
            this.rdbEmpresa.Text = "EMPRESA";
            this.rdbEmpresa.UseVisualStyleBackColor = true;
            // 
            // chkOtroFil
            // 
            this.chkOtroFil.AutoSize = true;
            this.chkOtroFil.BackColor = System.Drawing.SystemColors.Control;
            this.chkOtroFil.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkOtroFil.ForeColor = System.Drawing.Color.Black;
            this.chkOtroFil.Location = new System.Drawing.Point(6, 28);
            this.chkOtroFil.Name = "chkOtroFil";
            this.chkOtroFil.Size = new System.Drawing.Size(15, 14);
            this.chkOtroFil.TabIndex = 116;
            this.chkOtroFil.UseVisualStyleBackColor = false;
            this.chkOtroFil.CheckedChanged += new System.EventHandler(this.chkOtroFil_CheckedChanged);
            // 
            // cmbIdEmpresa
            // 
            this.cmbIdEmpresa.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbIdEmpresa.FormattingEnabled = true;
            this.cmbIdEmpresa.Location = new System.Drawing.Point(114, 25);
            this.cmbIdEmpresa.Name = "cmbIdEmpresa";
            this.cmbIdEmpresa.Size = new System.Drawing.Size(124, 21);
            this.cmbIdEmpresa.TabIndex = 115;
            // 
            // txtOrden
            // 
            this.txtOrden.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtOrden.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOrden.Location = new System.Drawing.Point(625, 25);
            this.txtOrden.MaxLength = 10;
            this.txtOrden.Name = "txtOrden";
            this.txtOrden.Size = new System.Drawing.Size(70, 20);
            this.txtOrden.TabIndex = 113;
            this.txtOrden.Tag = "1";
            // 
            // txtPadre
            // 
            this.txtPadre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPadre.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPadre.Location = new System.Drawing.Point(473, 25);
            this.txtPadre.MaxLength = 10;
            this.txtPadre.Name = "txtPadre";
            this.txtPadre.Size = new System.Drawing.Size(76, 20);
            this.txtPadre.TabIndex = 111;
            this.txtPadre.Tag = "1";
            // 
            // txtUnidad
            // 
            this.txtUnidad.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUnidad.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUnidad.Location = new System.Drawing.Point(317, 25);
            this.txtUnidad.MaxLength = 10;
            this.txtUnidad.Name = "txtUnidad";
            this.txtUnidad.Size = new System.Drawing.Size(88, 20);
            this.txtUnidad.TabIndex = 108;
            this.txtUnidad.Tag = "1";
            // 
            // LTiempo
            // 
            this.LTiempo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LTiempo.AutoSize = true;
            this.LTiempo.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LTiempo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.LTiempo.Location = new System.Drawing.Point(1110, 64);
            this.LTiempo.Name = "LTiempo";
            this.LTiempo.Size = new System.Drawing.Size(53, 38);
            this.LTiempo.TabIndex = 84;
            this.LTiempo.Text = "10";
            // 
            // GroupBox2
            // 
            this.GroupBox2.Controls.Add(this.chkRango);
            this.GroupBox2.Controls.Add(this.ChkTmpReal);
            this.GroupBox2.Controls.Add(this.dtpFecFin);
            this.GroupBox2.Controls.Add(this.dtpFecIni);
            this.GroupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GroupBox2.ForeColor = System.Drawing.Color.Black;
            this.GroupBox2.Location = new System.Drawing.Point(12, 181);
            this.GroupBox2.Name = "GroupBox2";
            this.GroupBox2.Size = new System.Drawing.Size(547, 49);
            this.GroupBox2.TabIndex = 85;
            this.GroupBox2.TabStop = false;
            this.GroupBox2.Text = "Opciones de Busqueda";
            // 
            // chkRango
            // 
            this.chkRango.AutoSize = true;
            this.chkRango.Checked = true;
            this.chkRango.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.chkRango.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.chkRango.Location = new System.Drawing.Point(207, 23);
            this.chkRango.Name = "chkRango";
            this.chkRango.Size = new System.Drawing.Size(64, 17);
            this.chkRango.TabIndex = 119;
            this.chkRango.TabStop = true;
            this.chkRango.Text = "RANGO";
            this.chkRango.UseVisualStyleBackColor = true;
            this.chkRango.CheckedChanged += new System.EventHandler(this.chkRango_CheckedChanged);
            // 
            // ChkTmpReal
            // 
            this.ChkTmpReal.AutoSize = true;
            this.ChkTmpReal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.ChkTmpReal.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.ChkTmpReal.Location = new System.Drawing.Point(13, 23);
            this.ChkTmpReal.Name = "ChkTmpReal";
            this.ChkTmpReal.Size = new System.Drawing.Size(63, 17);
            this.ChkTmpReal.TabIndex = 118;
            this.ChkTmpReal.Text = "TODOS";
            this.ChkTmpReal.UseVisualStyleBackColor = true;
            this.ChkTmpReal.CheckedChanged += new System.EventHandler(this.ChkTmpReal_CheckedChanged);
            // 
            // dtpFecFin
            // 
            this.dtpFecFin.Enabled = false;
            this.dtpFecFin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpFecFin.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFecFin.Location = new System.Drawing.Point(388, 18);
            this.dtpFecFin.Name = "dtpFecFin";
            this.dtpFecFin.Size = new System.Drawing.Size(105, 22);
            this.dtpFecFin.TabIndex = 10;
            // 
            // dtpFecIni
            // 
            this.dtpFecIni.Enabled = false;
            this.dtpFecIni.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpFecIni.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFecIni.Location = new System.Drawing.Point(277, 18);
            this.dtpFecIni.Name = "dtpFecIni";
            this.dtpFecIni.Size = new System.Drawing.Size(105, 22);
            this.dtpFecIni.TabIndex = 7;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // ToolStripMenu
            // 
            this.ToolStripMenu.AutoSize = false;
            this.ToolStripMenu.BackColor = System.Drawing.SystemColors.Control;
            this.ToolStripMenu.Font = new System.Drawing.Font("Berlin Sans FB", 11.25F);
            this.ToolStripMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnSalir,
            this.btnCancelar});
            this.ToolStripMenu.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.ToolStripMenu.Location = new System.Drawing.Point(0, 0);
            this.ToolStripMenu.Name = "ToolStripMenu";
            this.ToolStripMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.ToolStripMenu.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ToolStripMenu.Size = new System.Drawing.Size(1175, 65);
            this.ToolStripMenu.TabIndex = 86;
            this.ToolStripMenu.Text = "ToolStrip1";
            // 
            // btnSalir
            // 
            this.btnSalir.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnSalir.Image = ((System.Drawing.Image)(resources.GetObject("btnSalir.Image")));
            this.btnSalir.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnSalir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(55, 62);
            this.btnSalir.Text = "&S a l i r";
            this.btnSalir.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnSalir.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnCancelar.Image = global::Fletera2.Properties.Resources.Refrescar;
            this.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnCancelar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(73, 62);
            this.btnCancelar.Text = "&Refrescar";
            this.btnCancelar.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnCancelar.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.chkVerUsers);
            this.groupBox3.Controls.Add(this.txtFiltro);
            this.groupBox3.Controls.Add(this.checkBox1);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.ForeColor = System.Drawing.Color.Black;
            this.groupBox3.Location = new System.Drawing.Point(565, 182);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(598, 49);
            this.groupBox3.TabIndex = 87;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Tiempo Ejecución";
            // 
            // chkVerUsers
            // 
            this.chkVerUsers.AutoSize = true;
            this.chkVerUsers.BackColor = System.Drawing.SystemColors.Control;
            this.chkVerUsers.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkVerUsers.ForeColor = System.Drawing.Color.Black;
            this.chkVerUsers.Location = new System.Drawing.Point(487, 19);
            this.chkVerUsers.Name = "chkVerUsers";
            this.chkVerUsers.Size = new System.Drawing.Size(112, 20);
            this.chkVerUsers.TabIndex = 121;
            this.chkVerUsers.Text = "Ver Ususarios";
            this.chkVerUsers.UseVisualStyleBackColor = false;
            this.chkVerUsers.CheckedChanged += new System.EventHandler(this.chkVerUsers_CheckedChanged);
            // 
            // txtFiltro
            // 
            this.txtFiltro.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFiltro.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFiltro.Location = new System.Drawing.Point(141, 19);
            this.txtFiltro.MaxLength = 10;
            this.txtFiltro.Name = "txtFiltro";
            this.txtFiltro.Size = new System.Drawing.Size(340, 20);
            this.txtFiltro.TabIndex = 120;
            this.txtFiltro.Tag = "1";
            this.txtFiltro.TextChanged += new System.EventHandler(this.txtFiltro_TextChanged);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Checked = true;
            this.checkBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox1.Location = new System.Drawing.Point(16, 22);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(119, 20);
            this.checkBox1.TabIndex = 119;
            this.checkBox1.Text = "En tiempo Real";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // grdDetOS
            // 
            this.grdDetOS.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdDetOS.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdDetOS.Location = new System.Drawing.Point(12, 469);
            this.grdDetOS.Name = "grdDetOS";
            this.grdDetOS.Size = new System.Drawing.Size(1034, 158);
            this.grdDetOS.TabIndex = 88;
            this.grdDetOS.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.grdDetOS_RowPostPaint);
            // 
            // statusStrip
            // 
            this.statusStrip.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip.Location = new System.Drawing.Point(0, 673);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(1175, 25);
            this.statusStrip.TabIndex = 103;
            this.statusStrip.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(93, 20);
            this.toolStripStatusLabel1.Text = "No.Registros";
            // 
            // fMonitorOS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1175, 698);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.grdDetOS);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.ToolStripMenu);
            this.Controls.Add(this.GroupBox2);
            this.Controls.Add(this.LTiempo);
            this.Controls.Add(this.GroupBox4);
            this.Controls.Add(this.gpoTOS);
            this.Controls.Add(this.GroupBox1);
            this.Controls.Add(this.grdCabOS);
            this.Name = "fMonitorOS";
            this.Text = "Monitor de Ordenes de Servicio";
            this.Load += new System.EventHandler(this.fMonitorOS_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grdCabOS)).EndInit();
            this.GroupBox1.ResumeLayout(false);
            this.GroupBox1.PerformLayout();
            this.gpoTOS.ResumeLayout(false);
            this.gpoTOS.PerformLayout();
            this.GroupBox4.ResumeLayout(false);
            this.GroupBox4.PerformLayout();
            this.GroupBox2.ResumeLayout(false);
            this.GroupBox2.PerformLayout();
            this.ToolStripMenu.ResumeLayout(false);
            this.ToolStripMenu.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdDetOS)).EndInit();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView grdCabOS;
        internal System.Windows.Forms.GroupBox GroupBox1;
        internal System.Windows.Forms.CheckBox chkOstesFin;
        internal System.Windows.Forms.CheckBox chkOstesIni;
        internal System.Windows.Forms.CheckBox ChkEnt;
        internal System.Windows.Forms.CheckBox ChkCan;
        internal System.Windows.Forms.CheckBox ChkTer;
        internal System.Windows.Forms.CheckBox ChkAsig;
        internal System.Windows.Forms.CheckBox ChkCap;
        internal System.Windows.Forms.CheckBox ChkRecep;
        internal System.Windows.Forms.GroupBox gpoTOS;
        internal System.Windows.Forms.CheckBox chkTosTaller;
        internal System.Windows.Forms.CheckBox chkTosLlan;
        internal System.Windows.Forms.CheckBox chkTosComb;
        internal System.Windows.Forms.CheckBox chkTosLav;
        internal System.Windows.Forms.GroupBox GroupBox4;
        internal System.Windows.Forms.TextBox txtOrden;
        internal System.Windows.Forms.Label LTiempo;
        internal System.Windows.Forms.GroupBox GroupBox2;
        internal System.Windows.Forms.DateTimePicker dtpFecFin;
        internal System.Windows.Forms.DateTimePicker dtpFecIni;
        internal System.Windows.Forms.ComboBox cmbIdEmpresa;
        internal System.Windows.Forms.TextBox txtPadre;
        internal System.Windows.Forms.TextBox txtUnidad;
        private System.Windows.Forms.Timer timer1;
        internal System.Windows.Forms.CheckBox chkOtroFil;
        private System.Windows.Forms.RadioButton rdbOrden;
        private System.Windows.Forms.RadioButton rdbFolio;
        private System.Windows.Forms.RadioButton rdbUnidad;
        private System.Windows.Forms.RadioButton rdbEmpresa;
        internal System.Windows.Forms.ToolStrip ToolStripMenu;
        private System.Windows.Forms.ToolStripButton btnCancelar;
        private System.Windows.Forms.ToolStripButton btnSalir;
        private System.Windows.Forms.RadioButton chkRango;
        private System.Windows.Forms.RadioButton ChkTmpReal;
        internal System.Windows.Forms.GroupBox groupBox3;
        internal System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.DataGridView grdDetOS;
        internal System.Windows.Forms.TextBox txtFiltro;
        internal System.Windows.Forms.CheckBox chkVerUsers;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
    }
}