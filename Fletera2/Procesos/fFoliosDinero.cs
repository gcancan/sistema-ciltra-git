﻿using Core.Interfaces;
using Core.Models;
using Fletera2.Clases;
using Fletera2Entidades;
using Fletera2Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Fletera2.Clases.Inicio;

namespace Fletera2.Procesos
{
    public partial class fFoliosDinero : Form
    {
        List<Empresa> listEmpresas;
        List<CatPersonal> listEntrega;
        List<CatPersonal> listRecibe;
        List<CatTipoFolDin> listTipos;
        Usuario _usuario;

        FoliosDinero folioPadre;
        FoliosDinero folio;
        CabGuia viaje;

        //Para MsgBox
        string caption;
        MessageBoxButtons buttons = MessageBoxButtons.YesNo;
        DialogResult result;
        string mensaje = "";

        ImpremeFolioDinero Imprime = new ImpremeFolioDinero();
        PrinterSettings prtSettings;
        PrintDocument prtDoc;

        Font printFont = new System.Drawing.Font("Courier New", 7);
        int lineaActual = 0;
        int ContPaginas = 0;
        int NumRegistros = 1;

        opcForma opcionForma;
        List<CompGastosFoliosDin> listCompGas;

        public fFoliosDinero(Usuario user)
        {
            _usuario = user;
            InitializeComponent();
        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void fFoliosDinero_Load(object sender, EventArgs e)
        {
            HabilitaCampos(opcHabilita.DesHabilitaTodos);
            CargaCombos();
        }

        private void CargaCombos()
        {
            //Empresas
            OperationResult resp = new EmpresaSvc().getEmpresasALLorById(Convert.ToInt32(_usuario.personal.clave_empresa));
            if (resp.typeResult == ResultTypes.success)
            {
                listEmpresas = (List<Empresa>)resp.result;

                //cmbidEmpresa.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                //cmbidEmpresa.AutoCompleteSource = AutoCompleteSource.ListItems;
                cmbidEmpresa.DataSource = listEmpresas;
                cmbidEmpresa.DisplayMember = "Nombre";
                cmbidEmpresa.SelectedIndex = 0;


            }

            //Personal Entrega
            //OperationResult respPE = new CatPersonalSvc().getCatPersonalxFiltro(" WHERE per.idDepto = 8", " ORDER BY per.NombreCompleto");
            OperationResult respPE = new CatPersonalSvc().getCatPersonalxFiltro("  per.idPersonal = " + _usuario.personal.clave, "  per.NombreCompleto");
            if (respPE.typeResult == ResultTypes.success)
            {
                listEntrega = (List<CatPersonal>)respPE.result;
                cmbidEmpleadoEnt.DataSource = listEntrega;
                cmbidEmpleadoEnt.DisplayMember = "NombreCompleto";
                cmbidEmpleadoEnt.SelectedIndex = 0;
            }
            //Personal Recibe
            OperationResult respPR = new CatPersonalSvc().getCatPersonalxFiltro("  per.idDepto IN (13,9,1)", "  per.NombreCompleto");
            if (respPR.typeResult == ResultTypes.success)
            {
                listRecibe = (List<CatPersonal>)respPR.result;
                cmbidEmpleadoRec.DataSource = listRecibe;
                cmbidEmpleadoRec.DisplayMember = "NombreCompleto";
                cmbidEmpleadoRec.SelectedIndex = 0;
            }

            //Tipos
            OperationResult respT = new CatTipoFolDinSvc().getCatTipoFolDinxId();
            if (respT.typeResult == ResultTypes.success)
            {
                listTipos = (List<CatTipoFolDin>)respT.result;
                cmbidTipoFolDin.DataSource = listTipos;
                cmbidTipoFolDin.DisplayMember = "Descripcion";
                cmbidTipoFolDin.SelectedIndex = 0;
            }

            //AutoCompletar Conceptos

        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnGrabar_Click(object sender, EventArgs e)
        {
            caption = this.Name;
            buttons = MessageBoxButtons.OKCancel;
            mensaje = "Desea Crear Folio de Dinero para :" + cmbidEmpleadoRec.Text;
            result = MessageBox.Show(mensaje, caption, buttons);
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                if (!Validar())
                {
                    return;
                }
                FoliosDinero folio = mapForm();
                if (folio != null)
                {
                    OperationResult resp = new FoliosDineroSvc().GuardaFoliosDinero(ref folio);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        if (folioPadre != null)
                        {
                            if (folioPadre.Importe - (folioPadre.ImporteEnt + (txtImporte.Text == "" ? 0 : Convert.ToDecimal(txtImporte.Text))) == 0)
                            {
                                folioPadre.Estatus = "ENT";
                            }
                            folioPadre.ImporteEnt = (txtImporte.Text == "" ? 0 : Convert.ToDecimal(txtImporte.Text));

                            OperationResult respP = new FoliosDineroSvc().GuardaFoliosDinero(ref folioPadre);
                            if (respP.typeResult == ResultTypes.success)
                            {
                                ImprimirTicketFolioDinero(folio, _usuario.personal, true, true);
                                Cancelar();
                                return;
                            }
                        }

                        ImprimirTicketFolioDinero(folio, _usuario.personal, true, true);
                        Cancelar();
                        //mapForm((Agente)resp.result);
                        //Se imprime Ticket
                    }
                }

            }
        }
        private FoliosDinero mapForm()
        {
            try
            {
                return new FoliosDinero
                {
                    FolioDin = txtFolioDin.Tag == null ? 0 : ((FoliosDinero)txtFolioDin.Tag).FolioDin,
                    Fecha = DateTime.Now,
                    idEmpleadoEnt = ((CatPersonal)cmbidEmpleadoEnt.SelectedValue).idPersonal,
                    NombreEnt = ((CatPersonal)cmbidEmpleadoEnt.SelectedValue).NombreCompleto,
                    idEmpleadoRec = ((CatPersonal)cmbidEmpleadoRec.SelectedValue).idPersonal,
                    NombreRec = ((CatPersonal)cmbidEmpleadoRec.SelectedValue).NombreCompleto,
                    Estatus = "ACT",
                    Concepto = txtConcepto.Text,
                    idEmpresa = ((Empresa)cmbidEmpresa.SelectedValue).clave,
                    RazonSocial = ((Empresa)cmbidEmpresa.SelectedValue).nombre,
                    idTipoFolDin = ((CatTipoFolDin)cmbidTipoFolDin.SelectedValue).idTipoFolDin,
                    DescripcionFolDin = ((CatTipoFolDin)cmbidTipoFolDin.SelectedValue).Descripcion,
                    Importe = (txtImporte.Text == "" ? 0 : Convert.ToDecimal(txtImporte.Text)),
                    ImporteComp = (txtImporteComp.Text == "" ? 0 : Convert.ToDecimal(txtImporteComp.Text)),
                    ImporteEnt = (txtImporteEnt.Text == "" ? 0 : Convert.ToDecimal(txtImporteEnt.Text)),
                    FolioDinPadre = (txtFolioDinPadre.Text == "" ? 0 : Convert.ToInt32(txtFolioDinPadre.Text)),
                    NumGuiaId = (txtNumGuiaId.Text == "" ? 0 : Convert.ToInt32(txtNumGuiaId.Text))
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrio un error al querer obtener información del catálogo" +
                    Environment.NewLine + ex.Message, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }



        private void ImprimirTicketFolioDinero(FoliosDinero folio, Personal personal, bool BandPreview, 
            bool ActivaImprime)
        {
            try
            {
                Boolean vBandHorizontal = false;
                Imprime.FuenteImprimeCampos = new System.Drawing.Font("Courrier New", 7, FontStyle.Bold);
                //CODIGO DE BARRAS
                //Imprime.FuenteImprimeCampos2 = new Font("Free 3 of 9 Extended", 40);
                Imprime.FuenteImprimeCampos2 = new Font("Free 3 of 9 Extended", 20);
                Imprime.FuenteImprimeCampos3 = new System.Drawing.Font("Courrier New", 3, FontStyle.Bold);
                Imprime.FuenteImprimeCampos4 = new System.Drawing.Font("Courrier New", 7, FontStyle.Bold);
                Imprime.FuenteImprimeCampos5 = new System.Drawing.Font("Courrier New", 4, FontStyle.Bold);

                Imprime.TablaImprime = Imprime.ArmaTablaTicket(folio, personal);

                prtSettings = new PrinterSettings();
                prtDoc = new PrintDocument();
                prtDoc.PrintPage += prt_PrintPage;
                prtDoc.PrinterSettings = prtSettings;
                prtDoc.DefaultPageSettings.Landscape = vBandHorizontal;
                prtDoc.DefaultPageSettings.PaperSize = prtSettings.DefaultPageSettings.PaperSize;

                if (BandPreview)
                {
                    PrintPreviewDialog prtPrev = new PrintPreviewDialog();
                    prtPrev.Document = prtDoc;
                    prtPrev.WindowState = FormWindowState.Maximized;
                    prtPrev.Document.DefaultPageSettings.Landscape = vBandHorizontal;
                    if (vBandHorizontal)
                    {
                        prtPrev.PrintPreviewControl.Zoom = 2;
                    }
                    else
                    {
                        prtPrev.PrintPreviewControl.Zoom = 2;
                    }

                    if (!ActivaImprime)
                    {
                        //DirectCast((prtPrev.Controls[1]), ToolStrip).Items.RemoveAt(0);
                        ((ToolStrip)(prtPrev.Controls[1])).Items[0].Enabled = false;
                    }

                    prtPrev.ShowDialog();
                }
                else
                {
                    prtDoc.Print();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrio un error al querer Imprimir la Licencia" +
                   Environment.NewLine + ex.Message, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }
        void prt_PrintPage(object sender, PrintPageEventArgs e)
        {
            try
            {
                Single yPos = e.MarginBounds.Top;
                Single leftMargin = e.MarginBounds.Left;
                Single LineHeight = printFont.GetHeight(e.Graphics);

                //////////////////////////////////////////////////////////////////////////////////////////
                //Imprime.imgLogo1 = Tools.getImage(Fletera2.Properties.Resources.Salida2);
                Imprime.imgLogo1 = Fletera2.Properties.Resources.Atlante;
                Imprime.TamanioimgLogo1 = new System.Drawing.Size(Imprime.imgLogo1.Width / 24, Imprime.imgLogo1.Height / 24);
                Imprime.PosicionimgLogo1 = new System.Drawing.Point(180, 3);

                if (ContPaginas == 0)
                {
                    yPos = Imprime.ImprimeLogo(e, lineaActual);
                }

                //Imprime.imgLogo2 = PtoVenta.UI.Properties.Resources.Report;
                //Imprime.TamanioimgLogo2 = new System.Drawing.Size(Imprime.imgLogo2.Width / 22, Imprime.imgLogo2.Height / 20);
                //Imprime.PosicionimgLogo2 = new System.Drawing.Point(210, 150);
                //////////////////////////////////////////////////////////////////////////////////////////
                yPos += LineHeight;
                do
                {
                    yPos += LineHeight;


                    yPos = Imprime.ImprimeLinea(e, lineaActual, ContPaginas + 1);


                    lineaActual += 1;

                } while (!(yPos >= e.MarginBounds.Bottom - 20 || lineaActual >= NumRegistros || ContPaginas == 2));

                ContPaginas += 1;
                if (ContPaginas > 2)
                {
                    ContPaginas = 0;
                    e.HasMorePages = false;
                }
                else
                {
                    e.HasMorePages = true;
                }
                //////////////////////////////////////////////////////////////////////////////////////////




            }

            catch (Exception ex)
            {
                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + ex.Message,
    this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private bool Validar()
        {

            if (((CatPersonal)cmbidEmpleadoEnt.SelectedValue).idPersonal == 0)
            {
                MessageBox.Show("Seleccione el Empleado que Entrega", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cmbidEmpleadoEnt.Focus();
                return false;

            }
            if (((CatPersonal)cmbidEmpleadoRec.SelectedValue).idPersonal == 0)
            {
                MessageBox.Show("Seleccione el Empleado que Recibe", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cmbidEmpleadoEnt.Focus();
                return false;

            }
            if (string.IsNullOrEmpty(txtImporte.Text.Trim()))
            {
                MessageBox.Show("El Importe no puede ser Vacio", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtImporte.Focus();
                return false;
            }
            if (folioPadre != null)
            {
                if (Convert.ToDecimal(txtImporte.Text) > (folioPadre.Importe - folioPadre.ImporteEnt))
                {
                    MessageBox.Show("El Importe no puede ser mayor a $: " + txtImporteFP.Text, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtImporte.Focus();
                    return false;
                }
            }

            if (((CatPersonal)cmbidEmpleadoEnt.SelectedValue).idPersonal == ((CatPersonal)cmbidEmpleadoRec.SelectedValue).idPersonal)
            {
                MessageBox.Show("No pueden ser iguales Personal que Entrega y Recibe", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cmbidEmpleadoRec.Focus();
                return false;
            }

            return true;
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            opcionForma = opcForma.Nuevo;
            //Habilitar lo que se va capturar
            HabilitaCampos(opcHabilita.Nuevo);
            txtFolioDinPadre.Focus();
        }

        private void HabilitaCampos(opcHabilita opc)
        {
            switch (opc)
            {
                case opcHabilita.DesHabilitaTodos:
                    gpoDatos.Enabled = false;
                    gpoImportes.Enabled = false;
                    gpoComprobantes.Enabled = false;
                    
                    break;
                case opcHabilita.Iniciando:
                    gpoDatos.Enabled = true;
                    dtpFecha.Enabled = false;
                    dtpFechaHora.Enabled = false;
                    txtEstatus.Enabled = false;
                    cmbidTipoFolDin.Enabled = false;
                    cmbidEmpleadoEnt.Enabled = false;
                    cmbidEmpleadoRec.Enabled = false;
                    txtConcepto.Enabled = false;
                    txtNumGuiaId.Enabled = false;
                    gpoImportes.Enabled = false;
                    gpoComprobantes.Enabled = false;

                    break;
                case opcHabilita.Editando:
                    gpoDatos.Enabled = true;
                    cmbidEmpresa.Enabled = false;
                    txtFolioDin.Enabled = false;
                    txtFolioDinPadre.Enabled = false;
                    txtImporteFP.Enabled = false;
                    dtpFecha.Enabled = false;
                    dtpFechaHora.Enabled = false;
                    txtEstatus.Enabled = false;
                    cmbidEmpleadoEnt.Enabled = true;
                    cmbidEmpleadoRec.Enabled = true;
                    txtConcepto.Enabled = true;
                    cmbidTipoFolDin.Enabled = true;
                    txtNumGuiaId.Enabled = true;

                    gpoImportes.Enabled = true;
                    txtidLiquidacion.Enabled = false;
                    txtImporte.Enabled = true;
                    txtImporteComp.Enabled = false;
                    txtImporteEnt.Enabled = false;
                    txtxComprobar.Enabled = false;
                    txtxEntregar.Enabled = false;
                    

                    gpoComprobantes.Enabled = true;
                    break;
                case opcHabilita.Nuevo:
                    gpoDatos.Enabled = true;
                    cmbidEmpresa.Enabled = false;
                    txtFolioDin.Enabled = false;
                    txtFolioDinPadre.Enabled = true;
                    txtImporteFP.Enabled = false;
                    dtpFecha.Enabled = false;
                    dtpFechaHora.Enabled = false;
                    txtEstatus.Enabled = false;
                    cmbidEmpleadoEnt.Enabled = false;
                    cmbidEmpleadoRec.Enabled = false;
                    txtConcepto.Enabled = false;
                    cmbidTipoFolDin.Enabled = false;

                    gpoImportes.Enabled = true;
                    txtidLiquidacion.Enabled = false;
                    txtImporte.Enabled = false;
                    txtImporteComp.Enabled = false;
                    txtImporteEnt.Enabled = false;
                    txtxComprobar.Enabled = false;
                    txtxEntregar.Enabled = false;
                    txtNumGuiaId.Enabled = false;

                    gpoComprobantes.Enabled = false;
                    break;
                case opcHabilita.Reporte:
                    gpoDatos.Enabled = true;
                    cmbidEmpresa.Enabled = false;
                    txtFolioDin.Enabled = true;
                    txtFolioDinPadre.Enabled = false;
                    txtImporteFP.Enabled = false;
                    dtpFecha.Enabled = false;
                    dtpFechaHora.Enabled = false;
                    txtEstatus.Enabled = false;
                    cmbidTipoFolDin.Enabled = false;
                    cmbidEmpleadoEnt.Enabled = false;
                    cmbidEmpleadoRec.Enabled = false;
                    txtConcepto.Enabled = false;
                    txtNumGuiaId.Enabled = false;
                    txtidLiquidacion.Enabled = false;

                    gpoImportes.Enabled = false;
                    //txtImporte.Enabled = false;
                    //txtImporteComp.Enabled = false;
                    //txtImporteEnt.Enabled = false;
                    //txtxComprobar.Enabled = false;
                    //txtxEntregar.Enabled = false;

                    gpoComprobantes.Enabled = true;
                    break;
                default://
                    break;
            }
        }

        private void cmbidEmpresa_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                if (txtFolioDin.Enabled)
                {
                    txtFolioDin.Focus();
                }
                else if (txtFolioDinPadre.Enabled)
                {
                    txtFolioDinPadre.Focus();
                }
                else
                {
                    cmbidTipoFolDin.Focus();
                }

            }

        }

        private void txtFolioDin_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                if (txtFolioDin.Text != "")
                {
                    CargaForma(txtFolioDin.Text, Clases.opcBusqueda.FoliosDinero);
                }
                if (cmbidTipoFolDin.Enabled)
                {
                    cmbidTipoFolDin.Focus();
                }
                else
                {
                    cmbidEmpleadoEnt.Focus();
                }
            }
        }

        private void cmbidEmpleadoEnt_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                if (cmbidEmpleadoRec.Enabled)
                {
                    cmbidEmpleadoRec.Focus();
                }
                else
                {
                    txtConcepto.Focus();
                    txtConcepto.SelectAll();
                }
            }
        }

        private void cmbidEmpleadoRec_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                if (txtConcepto.Enabled)
                {
                    txtConcepto.Focus();
                    txtConcepto.SelectAll();
                }
                else
                {
                    txtImporte.Focus();
                    txtImporte.SelectAll();
                }
            }
        }

        private void txtConcepto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                if (txtImporte.Enabled)
                {
                    txtImporte.Focus();
                }
                else if (grdComprobantes.Enabled)
                {
                    grdComprobantes.Focus();
                }
                else
                {
                    txtConcepto.Focus();
                }
            }
        }

        private void txtImporte_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                if (btnGrabar.Enabled)
                {
                    btnGrabar.PerformClick();
                }
            }
        }

        private void cmbidTipoFolDin_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                HabilitaxTipo(((CatTipoFolDin)cmbidTipoFolDin.SelectedItem).ConViaje);
                if (txtNumGuiaId.Enabled)
                {
                    txtNumGuiaId.Focus();
                }
                    else if (cmbidEmpleadoEnt.Enabled)
                {
                    cmbidEmpleadoEnt.Focus();
                }
                else if (cmbidEmpleadoRec.Enabled)
                {
                    cmbidEmpleadoRec.Focus();
                }
                else
                {
                    txtConcepto.Focus();
                    txtConcepto.SelectAll();
                }
            }
        }

        private void MandarImprimir(int FolioDin)
        {
            OperationResult resp = new FoliosDineroSvc().getFoliosDineroxId(FolioDin, "");
            if (resp.typeResult == ResultTypes.success)
            {
                FoliosDinero folio = ((List<FoliosDinero>)resp.result)[0];
                ImprimirTicketFolioDinero(folio, _usuario.personal, true, true);
            }
            else if (resp.typeResult == ResultTypes.recordNotFound)
            {
                //No Se encontro
            }
        }

        private void btnReporte_Click(object sender, EventArgs e)
        {
            //MandarImprimir(1);

            opcionForma = opcForma.Reporte;
            HabilitaCampos(opcHabilita.Reporte);
            txtFolioDin.Focus();

        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            opcionForma = opcForma.Editar;
            //habilitar Campos
            HabilitaCampos(opcHabilita.Iniciando);
            //Cargar los Folios de la empresa
            CargaFolios();

            txtFolioDin.Focus();

        }

        private void CargaFolios()
        {
            AutoCompleteStringCollection ColeccionProductos = new AutoCompleteStringCollection();

            OperationResult resp = new FoliosDineroSvc().getFoliosDineroxId(0,
            " fd.idEmpresa = " + ((Empresa)cmbidEmpresa.SelectedValue).clave.ToString());
            if (resp.typeResult == ResultTypes.success)
            {
                List<FoliosDinero> list = (List<FoliosDinero>)resp.result;
                foreach (var item in list)
                {
                    ColeccionProductos.Add(Convert.ToString(item.FolioDin));
                }
                txtFolioDin.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                txtFolioDin.AutoCompleteSource = AutoCompleteSource.CustomSource;
                txtFolioDin.AutoCompleteCustomSource = ColeccionProductos;
            }
        }

        private void CargaForma(string Valor, Clases.opcBusqueda opc)
        {
            OperationResult resp = null;
            if (opc == Clases.opcBusqueda.FoliosDinero)
            {
                resp = new FoliosDineroSvc().getFoliosDineroxId(Convert.ToInt32(Valor));
                if (resp.typeResult == ResultTypes.success)
                {
                    //Existe y se llena depende su Estatus
                    folio = ((List<FoliosDinero>)resp.result)[0];
                    SeleccionaEmpresa(folio.idEmpresa);
                    txtFolioDinPadre.Text = folio.FolioDinPadre.ToString();
                    dtpFecha.Value = folio.Fecha;
                    dtpFechaHora.Value = folio.Fecha;
                    SeleccionaTipoDin(folio.idTipoFolDin);
                    SeleccionaEntrega(folio.idEmpleadoEnt);
                    SeleccionaRecibe(folio.idEmpleadoRec);
                    txtConcepto.Text = folio.Concepto;
                    txtEstatus.Text = folio.Estatus;
                    txtImporteFP.Text = folio.Importe.ToString("C2");
                    txtidLiquidacion.Text = folio.idLiquidacion.ToString();

                    txtImporte.Text = folio.Importe.ToString("C2");
                    txtImporteEnt.Text = folio.ImporteEnt.ToString("C2");
                    txtxEntregar.Text = (folio.Importe - folio.ImporteEnt).ToString("C2");
                    txtImporteComp.Text = folio.ImporteComp.ToString("C2");
                    txtxComprobar.Text = (folio.Importe - folio.ImporteComp).ToString("C2");


                    resp = new CompGastosFoliosDinSvc().getCompGastosFoliosDinxId(0, " c.FolioDin = " + folio.FolioDin);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        listCompGas = (List<CompGastosFoliosDin>)resp.result;
                        grdComprobantes.DataSource = null;
                        grdComprobantes.DataSource = listCompGas;
                    }

                    if (opcionForma == opcForma.Reporte)
                    {
                        MandarImprimir(folio.FolioDin);
                    }
                    
                }
                
            }
            else if (opc == Clases.opcBusqueda.FoliosDineroPadre)
            {
                resp = new FoliosDineroSvc().getFoliosDineroxId(Convert.ToInt32(Valor));
                if (resp.typeResult == ResultTypes.success)
                {
                    folioPadre = ((List<FoliosDinero>)resp.result)[0];
                    if (folioPadre.idLiquidacion > 0)
                    {
                        MessageBox.Show("El Folio Padre: " + txtFolioDinPadre.Text + " ya esta Liquidado ", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        txtFolioDinPadre.Focus();
                        txtFolioDinPadre.SelectAll();
                        return;
                    }

                    txtImporteFP.Text = (folioPadre.Importe - folioPadre.ImporteEnt).ToString();
                    if (Convert.ToDecimal(txtImporteFP.Text) == 0)
                    {
                        MessageBox.Show("El Folio Padre: " + txtFolioDinPadre.Text + " No tiene Importe para Entregar ", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        txtFolioDinPadre.Focus();
                        txtFolioDinPadre.SelectAll();
                        return;
                    }
                    else
                    {
                        //carga quien Entrega
                        OperationResult respPE = new CatPersonalSvc().getCatPersonalxFiltro(" per.idPersonal = " + folioPadre.idEmpleadoRec, " per.NombreCompleto");
                        if (respPE.typeResult == ResultTypes.success)
                        {
                            listEntrega = (List<CatPersonal>)respPE.result;
                            cmbidEmpleadoEnt.DataSource = listEntrega;
                            cmbidEmpleadoEnt.DisplayMember = "NombreCompleto";
                            cmbidEmpleadoEnt.SelectedIndex = 0;
                        }
                        HabilitaCampos(opcHabilita.Editando);
                        cmbidTipoFolDin.Focus();
                        //cmbidEmpleadoEnt.Focus();
                    }


                }
                else
                {
                    //No se encontro Folio Padre
                }
            }
            else if (opc == Clases.opcBusqueda.CabViajes)
            {
                
                resp = new CabGuiaSvc().getCabGuiaxFilter(Convert.ToInt32(Valor), " c.IdEmpresa = " + folioPadre.idEmpresa);
                if (resp.typeResult == ResultTypes.success)
                {
                    viaje = ((List<CabGuia>)resp.result)[0];
                    txtTractor.Text = viaje.idTractor;
                    txtRemolque1.Text = viaje.idRemolque1;
                    txtDolly.Text = viaje.idDolly;
                    txtRemolque2.Text = viaje.idRemolque2;
                    SeleccionaRecibe(viaje.idOperador);
                    cmbidEmpleadoEnt.Focus();
                }
                else if (resp.typeResult == ResultTypes.recordNotFound)
                {
                    MessageBox.Show("El Viaje: " + Valor + " No Existe o no Pertenece a la empresa " + folioPadre.RazonSocial, 
                    "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtNumGuiaId.Clear();
                    txtNumGuiaId.Focus();
                }
            }
        }

        private void cmbidEmpresa_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void txtFolioDinPadre_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                if (txtFolioDinPadre.Text != "")
                {
                    if (Convert.ToInt32(txtFolioDinPadre.Text)  > 0)
                    {
                        CargaForma(txtFolioDinPadre.Text, Clases.opcBusqueda.FoliosDineroPadre);
                    }
                    else
                    {
                        HabilitaCampos(opcHabilita.Editando);
                        cmbidTipoFolDin.Focus();
                    }
                            
                }
                else if (cmbidTipoFolDin.Enabled)
                {
                    cmbidTipoFolDin.Focus();
                }
                else
                {
                    cmbidEmpleadoEnt.Focus();
                }
            }

        }

        private void Cancelar()
        {
            LimpiaCampos();
            HabilitaCampos(opcHabilita.DesHabilitaTodos);
        }

        private void LimpiaCampos()
        {
            cmbidEmpresa.SelectedIndex = 0;
            txtFolioDin.Clear();
            txtFolioDinPadre.Clear();
            txtImporteFP.Clear();
            cmbidEmpleadoEnt.SelectedIndex = 0;
            cmbidEmpleadoRec.SelectedIndex = 0;
            cmbidTipoFolDin.SelectedIndex = 0;
            txtConcepto.Clear();
            txtEstatus.Clear();
            txtNumGuiaId.Clear();
            txtTractor.Clear();
            txtRemolque1.Clear();
            txtRemolque2.Clear();
            txtDolly.Clear();

            txtImporte.Clear();
            txtImporteEnt.Clear();
            txtxEntregar.Clear();
            txtImporteComp.Clear();
            txtxComprobar.Clear();
            txtidLiquidacion.Clear();

            grdComprobantes.DataSource = null;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Cancelar();
        }

        private void txtFolioDinPadre_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtFolioDin_TextChanged(object sender, EventArgs e)
        {

        }

        private void SeleccionaEmpresa(int idEmpresa)
        {
            int i = 0;
            foreach (Empresa item in cmbidEmpresa.Items)
            {
                if (item.clave == idEmpresa)
                {
                    cmbidEmpresa.SelectedIndex = i;
                    return;
                }
                else
                {
                    i++;
                }
            }
        }

        private void SeleccionaTipoDin(int idTipoFolDin)
        {
            int i = 0;
            foreach (CatTipoFolDin item in cmbidTipoFolDin.Items)
            {
                if (item.idTipoFolDin == idTipoFolDin)
                {
                    cmbidTipoFolDin.SelectedIndex = i;
                    return;
                }
                else
                {
                    i++;
                }
            }
        }
        private void SeleccionaEntrega(int idpersonal)
        {
            int i = 0;
            foreach (CatPersonal item in cmbidEmpleadoEnt.Items)
            {
                if (item.idPersonal == idpersonal)
                {
                    cmbidEmpleadoEnt.SelectedIndex = i;
                    return;
                }
                else
                {
                    i++;
                }
            }
        }
        private void SeleccionaRecibe(int idpersonal)
        {
            int i = 0;
            foreach (CatPersonal item in cmbidEmpleadoRec.Items)
            {
                if (item.idPersonal == idpersonal)
                {
                    cmbidEmpleadoRec.SelectedIndex = i;
                    return;
                }
                else
                {
                    i++;
                }
            }
        }

        private void txtNumGuiaId_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                CargaForma(txtNumGuiaId.Text, opcBusqueda.CabViajes);
            }
        }

        private void txtNumGuiaId_TextChanged(object sender, EventArgs e)
        {

        }

        private void cmbidTipoFolDin_SelectedIndexChanged(object sender, EventArgs e)
        {
            HabilitaxTipo(((CatTipoFolDin)cmbidTipoFolDin.SelectedItem).ConViaje);
        }
        private void HabilitaxTipo(bool ConViaje)
        {
            if (ConViaje)
            {
                txtNumGuiaId.Enabled = true;
            }
            else
            {
                txtNumGuiaId.Enabled = false;
            }
        }

    }//
}
