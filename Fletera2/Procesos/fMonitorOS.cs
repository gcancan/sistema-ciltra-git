﻿using Core.Models;
using Fletera2.Clases;
using Fletera2Datos;
using Fletera2Entidades;
using Fletera2Negocio;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using static Fletera2.Clases.Inicio;
using static Fletera2.Clases.Tools;

namespace Fletera2.Procesos
{
    public partial class fMonitorOS : Form
    {
        int _idEmpresa;
        bool _bTodasEmpresas;
        eTipoOrdenServicio _OpcTipoServ;
        //Variables
        OperationResult resp;
        List<MonitorOS> listaCabOS;
        List<DetOrdenServicio> listaDetOS;
        //Colores
        Color _ColorCap = Color.LightGray;
        Color _ColorRec = Color.LightSalmon;
        Color _ColorAsig = Color.LightBlue;
        Color _ColorTer = Color.LightGreen;
        Color _ColorEnt = Color.Orange;
        Color _ColorCan = Color.OrangeRed;
        Color _ColorOstIni = Color.Yellow;
        Color _ColorOstFin = Color.Violet;

        int _TiempoActu = 20; //'Cada 10 segundos
        //static int TmpActu = 0;
        //int TmpActu = 0;
        //static int TmpActu = 0;
        private static int TmpActu = 0;

        List<CatEmpresas> listaEmpresas;
        List<CatUnidadTrans> listUnidadesTrans;
        List<MasterOrdServ_f2> listFolios;
        List<CabOrdenServicio> listOrdenServicio;

        public fMonitorOS(int idEmpresa = 0, bool bTodasEmpresas = true, eTipoOrdenServicio OpcTipoServ = eTipoOrdenServicio.TODAS)
        {
            _idEmpresa = idEmpresa;
            _bTodasEmpresas = bTodasEmpresas;
            _OpcTipoServ = OpcTipoServ;
            InitializeComponent();
        }


        private void fMonitorOS_Load(object sender, EventArgs e)
        {
            LTiempo.Text = _TiempoActu.ToString();

            ChkCap.BackColor = _ColorCap;
            ChkRecep.BackColor = _ColorRec;
            ChkAsig.BackColor = _ColorAsig;
            ChkTer.BackColor = _ColorTer;
            ChkCan.BackColor = _ColorCan;
            ChkEnt.BackColor = _ColorEnt;
            chkOstesIni.BackColor = _ColorOstIni;
            chkOstesFin.BackColor = _ColorOstFin;
            chkOtroFil.Checked = false;
            HabilitaOtrosFiltros(chkOtroFil.Checked);
            chkRango.Checked = true;
            HabilitaFechas(!ChkTmpReal.Checked);


            AutoaCompletar(_idEmpresa);
            PermisosFiltros(_bTodasEmpresas);
            PermisosTiposOS(_OpcTipoServ);

            this.KeyPreview = true;
            CargaDatos();
            //grdCabOS.DefaultCellStyle.ForeColor = Color.Black;
            timer1.Enabled = true;
            //timer1.Start();
            timer1.Interval = 1000;
            txtFiltro.Enabled = false;
            //FormatoGeneral(grdCabOS);
            //FormatoGeneral(grdDetOS);

            //Status("Monitor de Servicios Iniciado", Me)
        }



        private void AutoaCompletar(int cveEmpresa)
        {
            AutoCompletarEmpresas(cveEmpresa);
            AutoCompletarUnidadTransp(txtUnidad, cveEmpresa);
            //AutoCompletarFolios(txtPadre, cveEmpresa);
            //AutoCompletarOrdenes(txtOrden, cveEmpresa);

        }



        private void AutoCompletarUnidadTransp(TextBox Control, int IdEmpresa)
        {
            AutoCompleteStringCollection coleccion = new AutoCompleteStringCollection();
            try
            {
                resp = new CatUnidadTransSvc().geCatUnidadTransxFilter("", IdEmpresa == 0 ? "" : "c.idEmpresa = " + IdEmpresa, "c.idUnidadTrans");
                if (resp.typeResult == ResultTypes.success)
                {
                    listUnidadesTrans = (List<CatUnidadTrans>)resp.result;
                    //AutoAcompletarTexto(txtIdUnidadTrans, etipoAcompletar.UnidadesTrans);
                    foreach (var item in listUnidadesTrans)
                    {
                        coleccion.Add(Convert.ToString(item.idUnidadTrans));
                    }
                    Control.AutoCompleteCustomSource = coleccion;
                    Control.AutoCompleteMode = AutoCompleteMode.Suggest;
                    Control.AutoCompleteSource = AutoCompleteSource.CustomSource;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void AutoCompletarEmpresas(int idEmpresa)
        {
            //AutoCompleteStringCollection coleccion = new AutoCompleteStringCollection();
            resp = new CatEmpresasServicios().getCatEmpresasxFiltro(idEmpresa);
            if (resp.typeResult == ResultTypes.success)
            {
                listaEmpresas = (List<CatEmpresas>)resp.result;
                cmbIdEmpresa.DataSource = null;
                cmbIdEmpresa.DataSource = listaEmpresas;
                cmbIdEmpresa.DisplayMember = "RazonSocial";
                cmbIdEmpresa.SelectedIndex = 0;

                //foreach (var item in listaEmpresas)
                //{
                //    coleccion.Add(Convert.ToString(item.RazonSocial));
                //}
            }
        }

        private void CargaDatos()
        {
            string Filtro = "";
            try
            {
                Filtro = DeterminaFiltros();
                //TiempoUsado.StartNew();
                grdCabOS.DataSource = null;
                grdDetOS.DataSource = null;
                listaCabOS = new List<MonitorOS>();
                listaDetOS = new List<DetOrdenServicio>();

                //resp = new MonitorOSServicios().getMonitorOSxFilter(0, "c.fechaCaptura = '" + FormatFecHora(DateTime.Now, true, true) + "'", "");
                resp = new MonitorOSServicios().getMonitorOSxFilter(0, Filtro);
                if (resp.typeResult == ResultTypes.success)
                {
                    listaCabOS = ((List<MonitorOS>)resp.result);
                    grdCabOS.DataSource = listaCabOS;
                    FormatogrdCabOS();
                    ActualizaStatusControl(listaCabOS.Count);
                    resp = new DetOrdenServicioSvc().getDetOrdenServicioxFilter(listaCabOS[0].idOrdenSer);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        listaDetOS = (List<DetOrdenServicio>)resp.result;
                        grdDetOS.DataSource = null;
                        grdDetOS.DataSource = listaDetOS;
                        FormatogrdDet();
                    }



                    //TiempoUsado.GetTiempo();
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + ex.Message,
                   this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private string DeterminaFiltros()
        {
            string SWhere = "";
            string sStatusFiltro = "";
            string filtroOstes = "";
            string STOS = "";
            string filtroOtros = "";

            //string filtroEmpresa = "";
            //string filtroUnidad = "";
            //string filtroFolio = "";
            //string filtroOrdenServ = "";


            //string filtro= "";

            //ESTATUS DE ORDENES DE SERVICIO
            if (chkOstesIni.Checked)
            {
                if (chkOstesFin.Checked)
                {
                    //filtroOstes = " and c.externo = 1 and c.FechaTerminado <> null and c.estatus <> 'CAN'";
                    if (ChkCan.Checked)
                    {
                        //filtroOstes = " and c.externo = 1";
                        filtroOstes = " and c.externo = 1 and c.estatus in ('REC','TER','CAN')";
                    }
                    else
                    {
                        //filtroOstes = " and c.externo = 1 and c.estatus <> 'CAN'";
                        filtroOstes = " and c.externo = 1 and c.estatus in ('REC','TER')";
                    }

                }
                else
                {
                    if (ChkCan.Checked)
                    {
                        //filtroOstes = " and c.externo = 1  and not c.estatus in ('TER')";
                        filtroOstes = " and c.externo = 1  and c.estatus in ('REC','CAN')";
                    }
                    else
                    {
                        //filtroOstes = " and c.externo = 1  and not c.estatus in ('TER','CAN')";
                        filtroOstes = " and c.externo = 1  and c.estatus in ('REC')";
                    }

                    //filtroOstes = " and c.externo = 1 and c.FechaRecepcion <> null and c.estatus <> 'CAN'";
                }
            }
            else if (chkOstesFin.Checked)
            {
                //filtroOstes = " and c.externo = 1 and c.FechaTerminado <> null  and c.estatus <> 'CAN'";
                if (ChkCan.Checked)
                {
                    filtroOstes = " and c.externo = 1 and c.estatus in ('TER','CAN')";
                }
                else
                {
                    filtroOstes = " and c.externo = 1 and c.estatus = 'TER'";
                }

            }
            else
            {
                if (ChkCap.Checked)
                {
                    SWhere += "'PRE',";
                }
                if (ChkRecep.Checked)
                {
                    SWhere += "'REC',";
                }
                if (ChkAsig.Checked)
                {
                    SWhere += "'ASIG',";
                }
                if (ChkTer.Checked)
                {
                    SWhere += "'TER',";
                }
                if (ChkCan.Checked)
                {
                    SWhere += "'CAN',";
                }
                if (ChkEnt.Checked)
                {
                    SWhere += "'ENT',";
                }
            }


            //if (chkOstesIni.Checked)
            //{
            //    filtroOstes = " and c.externo = 1";
            //}
            //if (chkOstesFin.Checked)
            //{
            //    filtroOstes = " and c.externo = 1 and c.FechaTerminado <> null";
            //}
            //TIPOS de ORDENES DE SERVICIO
            if (chkTosComb.Checked)
            {
                STOS += "1,";
            }
            if (chkTosTaller.Checked)
            {
                STOS += "2,";
            }
            if (chkTosLlan.Checked)
            {
                STOS += "3,";
            }
            if (chkTosLav.Checked)
            {
                STOS += "4,";
            }
            //OTROS FILTROS 
            if (chkOtroFil.Checked)
            {

                if (rdbEmpresa.Checked)
                {
                    if (((CatEmpresas)cmbIdEmpresa.SelectedItem).idEmpresa > 0)
                    {
                        filtroOtros = " and c.IdEmpresa = " + ((CatEmpresas)cmbIdEmpresa.SelectedItem).idEmpresa;
                    }
                }
                if (rdbUnidad.Checked)
                {
                    if (txtUnidad.Text != "")
                    {
                        filtroOtros = " and c.idUnidadTrans = '" + txtUnidad.Text + "'";
                    }
                }
                if (rdbFolio.Checked)
                {
                    if (txtPadre.Text != "")
                    {
                        if (ValidarCampo(txtPadre.Text, eTipoDato.TdNumerico))
                        {
                            filtroOtros = " and c.idPadreOrdSer = '" + txtPadre.Text + "'";
                        }
                        else
                        {
                            txtPadre.Focus();
                            txtPadre.SelectAll();
                        }
                    }
                }
                if (rdbOrden.Checked)
                {
                    if (txtOrden.Text != "")
                    {
                        if (ValidarCampo(txtOrden.Text, eTipoDato.TdNumerico))
                        {
                            filtroOtros = " and c.idOrdenSer = '" + txtOrden.Text + "'";
                        }
                        else
                        {
                            txtOrden.Focus();
                            txtOrden.SelectAll();
                        }
                    }
                }
            }
            //OTROS FILTROS 
            sStatusFiltro = SWhere;

            if (SWhere != "")
            {
                SWhere = " c.Estatus IN (" + SWhere.Substring(0, SWhere.Length - 1) + ") ";
                SWhere = SWhere + " and c.idTipoOrden in (" + STOS.Substring(0, STOS.Length - 1) + ")";
            }
            else
            {
                SWhere = SWhere + " c.idTipoOrden in (" + STOS.Substring(0, STOS.Length - 1) + ")";
            }
            SWhere = SWhere + filtroOstes;
            SWhere = SWhere + filtroOtros;
            if (chkRango.Checked)
            {
                SWhere = SWhere + " and CAST(c.fechaCaptura AS DATE)  >= '" + Tools.FormatFecHora(dtpFecIni.Value, true, false) + "' AND "
                    + "CAST(c.fechaCaptura AS DATE) <= '" + Tools.FormatFecHora(dtpFecFin.Value, true, false) + "'";
            }
            else if (ChkTmpReal.Checked)
            {

            }

            return SWhere;
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }
        private void AutoCompletarFolios(TextBox Control, int idEmpresa)
        {
            AutoCompleteStringCollection coleccion = new AutoCompleteStringCollection();
            try
            {
                //resp = new CabOrdenServicioSvc().getCabOrdenServicioxFilter(0, idEmpresa == 0 ? "" : "c.idempresa = " + idEmpresa);
                resp = new MasterOrdServSvc_f2().getMasterOrdServxFilter(0, idEmpresa == 0 ? "" : "c.idempresa = " + idEmpresa);
                if (resp.typeResult == ResultTypes.success)
                {
                    listFolios = (List<MasterOrdServ_f2>)resp.result;
                    //AutoAcompletarTexto(txtIdUnidadTrans, etipoAcompletar.UnidadesTrans);
                    foreach (var item in listFolios)
                    {
                        coleccion.Add(Convert.ToString(item.idPadreOrdSer));
                    }
                    Control.AutoCompleteCustomSource = coleccion;
                    Control.AutoCompleteMode = AutoCompleteMode.Suggest;
                    Control.AutoCompleteSource = AutoCompleteSource.CustomSource;
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + ex.Message,
                  this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void AutoCompletarOrdenes(TextBox Control, int idEmpresa)
        {
            AutoCompleteStringCollection coleccion = new AutoCompleteStringCollection();
            try
            {
                resp = new CabOrdenServicioSvc().getCabOrdenServicioxFilter(0, idEmpresa == 0 ? "" : "c.idempresa = " + idEmpresa);
                if (resp.typeResult == ResultTypes.success)
                {
                    listOrdenServicio = (List<CabOrdenServicio>)resp.result;
                    //AutoAcompletarTexto(txtIdUnidadTrans, etipoAcompletar.UnidadesTrans);
                    foreach (var item in listOrdenServicio)
                    {
                        coleccion.Add(Convert.ToString(item.idOrdenSer));
                    }
                    Control.AutoCompleteCustomSource = coleccion;
                    Control.AutoCompleteMode = AutoCompleteMode.Suggest;
                    Control.AutoCompleteSource = AutoCompleteSource.CustomSource;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + ex.Message,
                  this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            }
        }
        private void PermisosFiltros(bool bTodasEmpresas)
        {
            if (!bTodasEmpresas)
            {
                Selecciona(_idEmpresa);
                cmbIdEmpresa.Enabled = false;
                AutoaCompletar(_idEmpresa);
            }
        }
        //    Private Sub PermisosFiltros(ByVal bTodasEmpresas As Boolean)
        //    If Not bTodasEmpresas Then
        //        Empresa = New EmpresaClass(_cveEmpresa)
        //        If Empresa.Existe Then
        //            cmbIdEmpresa.SelectedValue = _cveEmpresa
        //            cmbIdEmpresa.Enabled = False
        //        End If
        //        AutoCompletarFolios(txtPadre, " INNER JOIN dbo.CatUnidadTrans T ON mos.idTractor = t.idUnidadTrans WHERE t.idEmpresa = " & _cveEmpresa)
        //        AutoCompletarOrdenes(txtOrden, " where mos.IdEmpresa = " & _cveEmpresa)
        //        AutoCompletarUnidadTransp(txtUnidad, _cveEmpresa)
        //    End If
        //End Sub
        private void Selecciona(int IdEmpresa)
        {
            try
            {
                int i = 0;
                foreach (CatEmpresas item in cmbIdEmpresa.Items)
                {
                    if (item.idEmpresa == IdEmpresa)
                    {
                        cmbIdEmpresa.SelectedIndex = i;
                        return;
                    }
                    else
                    {
                        i++;
                    }
                }
            }
            catch (Exception e)
            {

                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + e.Message,
                  this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void PermisosTiposOS(eTipoOrdenServicio opcTipoServ)
        {
            switch (opcTipoServ)
            {
                case eTipoOrdenServicio.COMBUSTIBLE:
                    chkTosComb.Enabled = false;
                    chkTosComb.Checked = true;

                    chkTosLav.Enabled = false;
                    chkTosLav.Checked = false;

                    chkTosLlan.Enabled = false;
                    chkTosLlan.Checked = false;

                    chkTosTaller.Enabled = false;
                    chkTosTaller.Checked = false;
                    break;
                case eTipoOrdenServicio.TALLER:
                    chkTosComb.Enabled = false;
                    chkTosComb.Checked = false;

                    chkTosLav.Enabled = false;
                    chkTosLav.Checked = false;

                    chkTosLlan.Enabled = false;
                    chkTosLlan.Checked = false;

                    chkTosTaller.Enabled = false;
                    chkTosTaller.Checked = true;
                    break;
                case eTipoOrdenServicio.LLANTAS:
                    chkTosComb.Enabled = false;
                    chkTosComb.Checked = false;

                    chkTosLav.Enabled = false;
                    chkTosLav.Checked = false;

                    chkTosLlan.Enabled = false;
                    chkTosLlan.Checked = true;

                    chkTosTaller.Enabled = false;
                    chkTosTaller.Checked = false;
                    break;
                case eTipoOrdenServicio.LAVADERO:
                    chkTosComb.Enabled = false;
                    chkTosComb.Checked = false;

                    chkTosLav.Enabled = false;
                    chkTosLav.Checked = true;

                    chkTosLlan.Enabled = false;
                    chkTosLlan.Checked = false;

                    chkTosTaller.Enabled = false;
                    chkTosTaller.Checked = false;
                    break;
                case eTipoOrdenServicio.RESGUARDO:
                    break;
                case eTipoOrdenServicio.TODAS:
                    chkTosComb.Enabled = true;
                    chkTosComb.Checked = false;

                    chkTosLav.Enabled = true;
                    chkTosLav.Checked = true;

                    chkTosLlan.Enabled = true;
                    chkTosLlan.Checked = true;

                    chkTosTaller.Enabled = true;
                    chkTosTaller.Checked = true;
                    break;
                default:
                    break;
            }
        }

        private void chkOtroFil_CheckedChanged(object sender, EventArgs e)
        {
            HabilitaOtrosFiltros(chkOtroFil.Checked);
        }
        private void HabilitaOtrosFiltros(bool Valor)
        {
            cmbIdEmpresa.Enabled = Valor;
            
            //cmbIdEmpresa.SelectedIndex = 0;
            rdbEmpresa.Enabled = Valor;
            rdbEmpresa.Checked = true;
            txtUnidad.Clear();
            txtUnidad.Enabled = Valor;
            rdbUnidad.Enabled = Valor;
            txtPadre.Enabled = Valor;
            txtPadre.Clear();
            rdbFolio.Enabled = Valor;
            txtOrden.Enabled = Valor;
            txtOrden.Clear();
            rdbOrden.Enabled = Valor;
        }
        private void HabilitaFechas(bool Valor)
        {
            dtpFecIni.Enabled = Valor;
            dtpFecFin.Enabled = Valor;
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ChkTmpReal_CheckedChanged(object sender, EventArgs e)
        {
            HabilitaFechas(!ChkTmpReal.Checked);
        }

        private void chkRango_CheckedChanged(object sender, EventArgs e)
        {
            HabilitaFechas(!ChkTmpReal.Checked);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            //private static int TmpActu = 0;
            //private int TmpActu = 0;
            LTiempo.Text = (_TiempoActu - TmpActu).ToString();
            if (TmpActu >= _TiempoActu)
            {

                timer1.Enabled = false;
                //timer1.Stop();
                TmpActu = 0;
                int RIndx;
                int CIndx;
                try
                {
                    RIndx = grdCabOS.CurrentCell.RowIndex;
                    CIndx = grdCabOS.CurrentCell.ColumnIndex;
                }
                catch (Exception)
                {
                    RIndx = -1;
                    CIndx = -1;
                }
                CargaDatos();
                if (RIndx > 0 && CIndx > 0 && RIndx < grdCabOS.RowCount)
                {
                    grdCabOS.CurrentCell = grdCabOS.Rows[RIndx].Cells[CIndx];
                }
                timer1.Enabled = true;
                timer1.Interval = 1000;
                //                TmpActu = 0;
                //timer1.Start();
                //TmpActu = 0;
                //timer1.Start();
            }
            else
            {
                TmpActu += 1;
            }

            if (listaCabOS != null)
            {
                if (listaCabOS.Count > 0)
                {
                    foreach (var item in listaCabOS)
                    {

                        if (item.Estatus == "PRE")
                        {
                            item.TiempoTrasncurrido = IntervaloHoras(DateTime.Now, item.Captura);
                        }
                        else if (item.Estatus == "REC")
                        {
                            item.TiempoTrasncurrido = IntervaloHoras(DateTime.Now, Convert.ToDateTime(item.Recepcion.ToString()));

                        }
                        else if (item.Estatus == "ASIG")
                        {
                            item.TiempoTrasncurrido = IntervaloHoras(DateTime.Now, Convert.ToDateTime(item.Asignado.ToString()));
                        }
                        else if (item.Estatus == "TER")
                        {
                            item.TiempoTrasncurrido = IntervaloHoras(DateTime.Now, Convert.ToDateTime(item.Terminado.ToString()));
                        }
                        if (!item.externo)
                        {
                            item.OstesIni = null;
                            item.OstesFin = null;
                        }
                    }
                    //grdCabOS.DataSource = null;
                    //grdCabOS.DataSource = listaCabOS;

                    grdCabOS.Refresh();
                    grdCabOS.PerformLayout();
                    //FormatogrdCabOS();
                    //grdCabOS.Refresh();
                }
            }

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            txtFiltro.Clear();
            if (checkBox1.Checked)
            {
                timer1.Enabled = true;
                LTiempo.ForeColor = Color.Blue;
                txtFiltro.Enabled = false;

            }
            else
            {
                timer1.Enabled = false;
                LTiempo.ForeColor = Color.Red;
                txtFiltro.Enabled = true;
                txtFiltro.Focus();
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            //timer1.Enabled = false;
            CargaDatos();
            //timer1.Enabled = true;
            //TmpActu = 0;
        }

        private void grdCabOS_Click(object sender, EventArgs e)
        {
            CargaDetalleOS(listaCabOS[grdCabOS.CurrentCell.RowIndex].idOrdenSer);

        }
        private void CargaDetalleOS(int idOrdenSer)
        {
            OperationResult respd = new DetOrdenServicioSvc().getDetOrdenServicioxFilter(idOrdenSer);
            if (respd.typeResult == ResultTypes.success)
            {
                listaDetOS = (List<DetOrdenServicio>)respd.result;
                grdDetOS.DataSource = null;
                grdDetOS.DataSource = listaDetOS;
                //CargaRefaciones(idOrdenSer, listaDetOS[0].idOrdenActividad);
                FormatogrdDet();
            }
            else
            {
                grdDetOS.DataSource = null;
            }
        }
        private void FormatoGeneral(DataGridView grid)
        {
            //ESTILO GENERAL
            grid.RowsDefaultCellStyle.BackColor = Color.White;
            grid.AlternatingRowsDefaultCellStyle.BackColor = Color.AliceBlue;

            grid.RowsDefaultCellStyle.SelectionForeColor = Color.Yellow;
            grid.RowsDefaultCellStyle.SelectionBackColor = Color.Gray;
            grid.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            grid.DefaultCellStyle.Font = new Font("Berlin Sans FB", 12);
            grid.ColumnHeadersDefaultCellStyle.Font = new Font("Berlin Sans FB", 12);
            grid.ColumnHeadersDefaultCellStyle.BackColor = Color.Yellow;
            grid.ColumnHeadersDefaultCellStyle.ForeColor = Color.Red;
            grid.EnableHeadersVisualStyles = false;
        }
        private void FormatogrdDet()
        {
            //OCULTAR COLUMNAS
            grdDetOS.Columns["NomServicio"].Visible = false;
            grdDetOS.Columns["Seleccionado"].Visible = false;
            grdDetOS.Columns["idOrdenSer"].Visible = false;
            grdDetOS.Columns["idOrdenActividad"].Visible = false;
            grdDetOS.Columns["idServicio"].Visible = false;
            grdDetOS.Columns["idActividad"].Visible = false;
            grdDetOS.Columns["DuracionActHr"].Visible = false;
            grdDetOS.Columns["NoPersonal"].Visible = false;
            grdDetOS.Columns["id_proveedor"].Visible = false;
            grdDetOS.Columns["Estatus"].Visible = false;
            grdDetOS.Columns["CostoManoObra"].Visible = false;
            grdDetOS.Columns["CostoProductos"].Visible = false;
            grdDetOS.Columns["FechaDiag"].Visible = false;
            grdDetOS.Columns["UsuarioDiag"].Visible = false;
            grdDetOS.Columns["FechaAsig"].Visible = false;
            grdDetOS.Columns["UsuarioAsig"].Visible = false;
            grdDetOS.Columns["FechaPausado"].Visible = false;
            grdDetOS.Columns["NotaPausado"].Visible = false;
            grdDetOS.Columns["FechaTerminado"].Visible = false;
            grdDetOS.Columns["UsuarioTerminado"].Visible = false;
            grdDetOS.Columns["idDivision"].Visible = false;
            grdDetOS.Columns["idLlanta"].Visible = false;
            grdDetOS.Columns["PosLlanta"].Visible = false;
            grdDetOS.Columns["NotaDiagnostico"].Visible = false;
            grdDetOS.Columns["FaltInsumo"].Visible = false;
            grdDetOS.Columns["Pendiente"].Visible = false;
            grdDetOS.Columns["CIDPRODUCTO_SERV"].Visible = false;
            grdDetOS.Columns["Cantidad_CIDPRODUCTO_SERV"].Visible = false;
            grdDetOS.Columns["Precio_CIDPRODUCTO_SERV"].Visible = false;
            grdDetOS.Columns["idFamilia"].Visible = false;
            grdDetOS.Columns["NotaRecepcion"].Visible = false;
            grdDetOS.Columns["isCancelado"].Visible = false;
            grdDetOS.Columns["motivoCancelacion"].Visible = false;
            grdDetOS.Columns["idPersonalResp"].Visible = false;
            grdDetOS.Columns["idPersonalAyu1"].Visible = false;
            grdDetOS.Columns["idPersonalAyu2"].Visible = false;
            grdDetOS.Columns["NotaRecepcionA"].Visible = false;
            grdDetOS.Columns["CIDPRODUCTO_ACT"].Visible = false;




            //HACER READONLY COLUMNAS
            //grdViajes.Columns["Seleccionado"].ReadOnly = true;

            //ENCABEZADO DE COLUMNAS
            grdDetOS.Columns["idOrdenTrabajo"].HeaderText = "No.";
            grdDetOS.Columns["NotaRecepcionF"].HeaderText = "Descripción";
            grdDetOS.Columns["TipoNotaRecepcionF"].HeaderText = "Tipo";
            grdDetOS.Columns["FallaReportada"].HeaderText = "Falla Reportada";
            grdDetOS.Columns["NomPersonalResp"].HeaderText = "Responsable";
            grdDetOS.Columns["NomPersonalAyu1"].HeaderText = "Ayudante 1";
            grdDetOS.Columns["NomPersonalAyu2"].HeaderText = "Ayudante 2";
            //grdDet.Columns["x"].HeaderText = "Nota";
            //grdDet.Columns["FechaHoraFin"].HeaderText = "Fecha Fin";
            //grdDet.Columns["EstatusGuia"].HeaderText = "Estatus";
            //grdDet.Columns["TipoViaje"].HeaderText = "Tipo Viaje";
            //grdDet.Columns["idTipoOrden"].HeaderText = "idTipoOrden";


            //FORMATO DE COLUMNAS
            //grdDatos.Columns["Precio"].DefaultCellStyle.Format = "C2";

            //ALINEACION DE COLUMNAS
            //grdDatos.Columns["Precio"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight;

            //TAMAÑO AUTOMATICO DE COLUMNAS
            grdDetOS.Columns["idOrdenTrabajo"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdDetOS.Columns["NotaRecepcionF"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdDetOS.Columns["TipoNotaRecepcionF"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdDetOS.Columns["FallaReportada"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

            grdDetOS.Columns["NomPersonalResp"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdDetOS.Columns["NomPersonalAyu1"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdDetOS.Columns["NomPersonalAyu2"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            //grdDet.Columns["x"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            //grdDet.Columns["x"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            //grdDet.Columns["x"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

            //grdDet.Columns["EstatusGuia"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            //grdDet.Columns["TipoViaje"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
        }
        private void FormatogrdCabOS()
        {
            grdCabOS.Columns["Estatus"].Visible = false;
            grdCabOS.Columns["Entregado"].Visible = false;
            grdCabOS.Columns["externo"].Visible = false;
            grdCabOS.Columns["Cancelado"].Visible = false;

            //usuarios
            grdCabOS.Columns["Autorizo"].Visible = false;
            grdCabOS.Columns["Entrego"].Visible = false;
            grdCabOS.Columns["Recibio"].Visible = false;
            grdCabOS.Columns["Cancelo"].Visible = false;
            grdCabOS.Columns["Chofer"].Visible = false;
            grdCabOS.Columns["Asigno"].Visible = false;
            grdCabOS.Columns["Termino"].Visible = false;
            grdCabOS.Columns["CasetaEntradaUser"].Visible = false;
            grdCabOS.Columns["CasetaSalidaUser"].Visible = false;
            grdCabOS.Columns["OstesIniUser"].Visible = false;
            grdCabOS.Columns["OstesFinUser"].Visible = false;
            //Asigno
            if (chkOstesIni.Checked || chkOstesFin.Checked)
            {
                grdCabOS.Columns["Recepcion"].Visible = false;
                grdCabOS.Columns["Asignado"].Visible = false;
                grdCabOS.Columns["Terminado"].Visible = false;
            }
            else
            {
                grdCabOS.Columns["OstesIni"].Visible = false;
                grdCabOS.Columns["OstesFin"].Visible = false;
            }
            if (chkVerUsers.Checked)
            {
                grdCabOS.Columns["Chofer"].Visible = true;
                grdCabOS.Columns["Autorizo"].Visible = true;
                grdCabOS.Columns["CasetaEntradaUser"].Visible = true;
                grdCabOS.Columns["CasetaSalidaUser"].Visible = true;

                if (chkOstesIni.Checked || chkOstesFin.Checked)
                {
                    grdCabOS.Columns["OstesIniUser"].Visible = true;
                    grdCabOS.Columns["OstesFinUser"].Visible = true;
                }
                else
                {
                    grdCabOS.Columns["Asigno"].Visible = true;
                    grdCabOS.Columns["Termino"].Visible = true;
                    grdCabOS.Columns["Entrego"].Visible = true;
                    grdCabOS.Columns["Recibio"].Visible = true;

                }
                if (ChkCan.Checked)
                {
                    grdCabOS.Columns["Cancelo"].Visible = true;
                }
            }
            if (ChkCan.Checked)
            {
                grdCabOS.Columns["Cancelado"].Visible = true;
            }

            grdCabOS.Columns["idOrdenSer"].HeaderText = "No. Servicio";
            grdCabOS.Columns["CasetaEntradaUser"].HeaderText = "Usuario Entrada";
            grdCabOS.Columns["CasetaSalidaUser"].HeaderText = "Usuario Salida";
            grdCabOS.Columns["OstesIniUser"].HeaderText = "Usuario Ostes Ini";
            grdCabOS.Columns["OstesFinUser"].HeaderText = "Usuario Ostes Fin";

            grdCabOS.Columns["FOLIO"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdCabOS.Columns["idOrdenSer"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdCabOS.Columns["Tipo"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdCabOS.Columns["Unidad"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdCabOS.Columns["Empresa"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdCabOS.Columns["Entrego"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdCabOS.Columns["Recibio"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdCabOS.Columns["Autorizo"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdCabOS.Columns["TiempoTrasncurrido"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdCabOS.Columns["Captura"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdCabOS.Columns["Recepcion"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdCabOS.Columns["Asignado"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdCabOS.Columns["Terminado"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdCabOS.Columns["Entregado"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdCabOS.Columns["Cancelado"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdCabOS.Columns["Cancelo"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            
            grdCabOS.Columns["CasetaEntrada"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdCabOS.Columns["CasetaSalida"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdCabOS.Columns["OstesIni"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdCabOS.Columns["OstesFin"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

            grdCabOS.Columns["externo"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdCabOS.Columns["Entregado"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdCabOS.Columns["Estatus"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

            grdCabOS.Columns["Chofer"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdCabOS.Columns["Asigno"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdCabOS.Columns["Termino"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdCabOS.Columns["CasetaEntradaUser"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdCabOS.Columns["CasetaSalidaUser"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdCabOS.Columns["OstesIniUser"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdCabOS.Columns["OstesFinUser"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;


            grdCabOS.Columns["FOLIO"].DisplayIndex = 0;
            grdCabOS.Columns["idOrdenSer"].DisplayIndex = 1;
            grdCabOS.Columns["Tipo"].DisplayIndex = 2;
            grdCabOS.Columns["Unidad"].DisplayIndex = 3;
            grdCabOS.Columns["Chofer"].DisplayIndex = 4;
            grdCabOS.Columns["Empresa"].DisplayIndex = 5;
            grdCabOS.Columns["Autorizo"].DisplayIndex = 6;
            grdCabOS.Columns["TiempoTrasncurrido"].DisplayIndex = 7;
            grdCabOS.Columns["Captura"].DisplayIndex = 8;
            grdCabOS.Columns["CasetaEntrada"].DisplayIndex = 9;
            grdCabOS.Columns["CasetaEntradaUser"].DisplayIndex = 10;
            grdCabOS.Columns["Recepcion"].DisplayIndex = 11;
            grdCabOS.Columns["Entrego"].DisplayIndex = 12;
            grdCabOS.Columns["Recibio"].DisplayIndex = 13;
            grdCabOS.Columns["Asignado"].DisplayIndex = 14;
            grdCabOS.Columns["Asigno"].DisplayIndex = 15;
            grdCabOS.Columns["Terminado"].DisplayIndex = 16;
            grdCabOS.Columns["Termino"].DisplayIndex = 17;
            grdCabOS.Columns["OstesIni"].DisplayIndex = 18;
            grdCabOS.Columns["OstesIniUser"].DisplayIndex = 19;
            grdCabOS.Columns["OstesFin"].DisplayIndex = 20;
            grdCabOS.Columns["OstesFinUser"].DisplayIndex = 21;
            grdCabOS.Columns["CasetaSalida"].DisplayIndex = 22;
            grdCabOS.Columns["CasetaSalidaUser"].DisplayIndex = 23;
            grdCabOS.Columns["Cancelado"].DisplayIndex = 24;
            grdCabOS.Columns["Cancelo"].DisplayIndex = 25;



        }

        private void grdCabOS_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            try
            {
                grdCabOS.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Black;
                if (Convert.ToString(grdCabOS.Rows[e.RowIndex].Cells["Estatus"].Value) == "PRE")
                {
                    grdCabOS.Rows[e.RowIndex].DefaultCellStyle.BackColor = _ColorCap;
                }
                else if (Convert.ToString(grdCabOS.Rows[e.RowIndex].Cells["Estatus"].Value) == "REC")
                {
                    if (Convert.ToBoolean(grdCabOS.Rows[e.RowIndex].Cells["externo"].Value))
                    {
                        grdCabOS.Rows[e.RowIndex].DefaultCellStyle.BackColor = _ColorOstIni;
                    }
                    else
                    {
                        grdCabOS.Rows[e.RowIndex].DefaultCellStyle.BackColor = _ColorRec;
                    }

                }
                else if (Convert.ToString(grdCabOS.Rows[e.RowIndex].Cells["Estatus"].Value) == "ASIG")
                {
                    if (Convert.ToBoolean(grdCabOS.Rows[e.RowIndex].Cells["externo"].Value))
                    {
                        grdCabOS.Rows[e.RowIndex].DefaultCellStyle.BackColor = _ColorOstIni;
                    }
                    else
                    {
                        grdCabOS.Rows[e.RowIndex].DefaultCellStyle.BackColor = _ColorAsig;
                    }

                }
                else if (Convert.ToString(grdCabOS.Rows[e.RowIndex].Cells["Estatus"].Value) == "TER")
                {
                    if (Convert.ToBoolean(grdCabOS.Rows[e.RowIndex].Cells["externo"].Value))
                    {
                        grdCabOS.Rows[e.RowIndex].DefaultCellStyle.BackColor = _ColorOstFin;
                    }
                    else
                    {
                        grdCabOS.Rows[e.RowIndex].DefaultCellStyle.BackColor = _ColorTer;
                    }

                }
                else if (Convert.ToString(grdCabOS.Rows[e.RowIndex].Cells["Estatus"].Value) == "CAN")
                {
                    grdCabOS.Rows[e.RowIndex].DefaultCellStyle.BackColor = _ColorCan;
                }
                else if (Convert.ToString(grdCabOS.Rows[e.RowIndex].Cells["Estatus"].Value) == "ENT")
                {

                }
                //externo
                //if (Convert.ToBoolean(grdCabOS.Rows[e.RowIndex].Cells["externo"].Value) &&
                //    Convert.ToString(grdCabOS.Rows[e.RowIndex].Cells["Estatus"].Value) == "TER")
                //{
                //    grdCabOS.Rows[e.RowIndex].DefaultCellStyle.BackColor = _ColorOstFin;
                //}
                //if (Convert.ToBoolean(grdCabOS.Rows[e.RowIndex].Cells["externo"].Value) &&
                //    Convert.ToString(grdCabOS.Rows[e.RowIndex].Cells["Estatus"].Value) == "CAN")
                //{
                //    grdCabOS.Rows[e.RowIndex].DefaultCellStyle.BackColor = _ColorCan;
                //}
                //else if (Convert.ToBoolean(grdCabOS.Rows[e.RowIndex].Cells["externo"].Value))
                //{
                //    grdCabOS.Rows[e.RowIndex].DefaultCellStyle.BackColor = _ColorOstIni;
                //}
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + ex.Message,
                 this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }


        }

        private void chkOstesIni_CheckedChanged(object sender, EventArgs e)
        {
            if (!VerificaEstatusChk())
            {
                MessageBox.Show("Se requiere que al menos un Elemento este seleccionado", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                chkOstesIni.Checked = true;
            }

        }

        private void grdDetOS_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            grdDetOS.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Black;
            if (Convert.ToString(grdDetOS.Rows[e.RowIndex].Cells["Estatus"].Value) == "PRE")
            {
                grdDetOS.Rows[e.RowIndex].DefaultCellStyle.BackColor = _ColorCap;
            }
            else if (Convert.ToString(grdDetOS.Rows[e.RowIndex].Cells["Estatus"].Value) == "REC")
            {
                grdDetOS.Rows[e.RowIndex].DefaultCellStyle.BackColor = _ColorRec;
            }
            else if (Convert.ToString(grdDetOS.Rows[e.RowIndex].Cells["Estatus"].Value) == "ASIG")
            {
                grdDetOS.Rows[e.RowIndex].DefaultCellStyle.BackColor = _ColorAsig;
            }
            else if (Convert.ToString(grdDetOS.Rows[e.RowIndex].Cells["Estatus"].Value) == "TER")
            {
                grdDetOS.Rows[e.RowIndex].DefaultCellStyle.BackColor = _ColorTer;
            }
            else if (Convert.ToString(grdDetOS.Rows[e.RowIndex].Cells["Estatus"].Value) == "CAN")
            {
                grdDetOS.Rows[e.RowIndex].DefaultCellStyle.BackColor = _ColorCan;
            }
            else if (Convert.ToString(grdDetOS.Rows[e.RowIndex].Cells["Estatus"].Value) == "ENT")
            {

            }
        }

        private void grdCabOS_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void ChkCap_CheckedChanged(object sender, EventArgs e)
        {
            if (!VerificaEstatusChk())
            {
                MessageBox.Show("Se requiere que al menos un Elemento este seleccionado", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                ChkCap.Checked = true;
            }
        }
        private bool VerificaEstatusChk()
        {
            //bool bMinimoUno = true;

            if (!ChkCap.Checked)
            {
                if (!ChkRecep.Checked)
                {
                    if (!ChkAsig.Checked)
                    {
                        if (!ChkTer.Checked)
                        {
                            if (!ChkCan.Checked)
                            {
                                if (!chkOstesIni.Checked)
                                {
                                    if (!chkOstesFin.Checked)
                                    {
                                        return false;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return true;
        }

        private void ChkRecep_CheckedChanged(object sender, EventArgs e)
        {
            if (!VerificaEstatusChk())
            {
                MessageBox.Show("Se requiere que al menos un Elemento este seleccionado", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                ChkRecep.Checked = true;
            }
        }

        private void ChkAsig_CheckedChanged(object sender, EventArgs e)
        {
            if (!VerificaEstatusChk())
            {
                MessageBox.Show("Se requiere que al menos un Elemento este seleccionado", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                ChkAsig.Checked = true;
            }
        }

        private void ChkTer_CheckedChanged(object sender, EventArgs e)
        {
            if (!VerificaEstatusChk())
            {
                MessageBox.Show("Se requiere que al menos un Elemento este seleccionado", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                ChkTer.Checked = true;
            }
        }

        private void ChkCan_CheckedChanged(object sender, EventArgs e)
        {
            if (!VerificaEstatusChk())
            {
                MessageBox.Show("Se requiere que al menos un Elemento este seleccionado", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                ChkCan.Checked = true;
            }
        }

        private void chkOstesFin_CheckedChanged(object sender, EventArgs e)
        {
            if (!VerificaEstatusChk())
            {
                MessageBox.Show("Se requiere que al menos un Elemento este seleccionado", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                chkOstesFin.Checked = true;
            }

        }
        private bool VerificaTiposOSChk()
        {
            if (!chkTosComb.Checked)
            {
                if (!chkTosLav.Checked)
                {
                    if (!chkTosLlan.Checked)
                    {
                        if (!chkTosTaller.Checked)
                        {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        private void chkTosComb_CheckedChanged(object sender, EventArgs e)
        {
            if (!VerificaTiposOSChk())
            {
                MessageBox.Show("Se requiere que al menos un Elemento este seleccionado", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                chkTosComb.Checked = true;
            }
        }

        private void chkTosLav_CheckedChanged(object sender, EventArgs e)
        {
            if (!VerificaTiposOSChk())
            {
                MessageBox.Show("Se requiere que al menos un Elemento este seleccionado", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                chkTosLav.Checked = true;
            }
        }

        private void chkTosLlan_CheckedChanged(object sender, EventArgs e)
        {
            if (!VerificaTiposOSChk())
            {
                MessageBox.Show("Se requiere que al menos un Elemento este seleccionado", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                chkTosLlan.Checked = true;
            }
        }

        private void chkTosTaller_CheckedChanged(object sender, EventArgs e)
        {
            if (!VerificaTiposOSChk())
            {
                MessageBox.Show("Se requiere que al menos un Elemento este seleccionado", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                chkTosTaller.Checked = true;
            }
        }

        private void txtFiltro_TextChanged(object sender, EventArgs e)
        {
            grdCabOS.DataSource = null;
            if (txtFiltro.Text.Length > 0)
            {
                var mySearch = (listaCabOS).FindAll(S => S.idOrdenSer.ToString().IndexOf(txtFiltro.Text.Trim(), StringComparison.OrdinalIgnoreCase) >= 0 ||
                S.FOLIO.ToString().IndexOf(txtFiltro.Text.Trim(), StringComparison.OrdinalIgnoreCase) >= 0 ||
                S.Unidad.IndexOf(txtFiltro.Text.Trim(), StringComparison.OrdinalIgnoreCase) >= 0 ||
                S.Tipo.IndexOf(txtFiltro.Text.Trim(), StringComparison.OrdinalIgnoreCase) >= 0 ||
                S.Empresa.IndexOf(txtFiltro.Text.Trim(), StringComparison.OrdinalIgnoreCase) >= 0 ||
                S.Autorizo.IndexOf(txtFiltro.Text.Trim(), StringComparison.OrdinalIgnoreCase) >= 0);

                ActualizaStatusControl(mySearch.Count);

                grdCabOS.DataSource = mySearch;
            }
            else
            {
                //ActualizaStatusControl(listaCabOS.Count);
                grdCabOS.DataSource = listaCabOS;
            }
        }

        private void chkVerUsers_CheckedChanged(object sender, EventArgs e)
        {
            if (listaCabOS != null)
            {
                if (listaCabOS.Count > 0)
                {
                    FormatogrdCabOS();
                }
            }
            
        }
        private void ActualizaStatusControl(int NumRegistros)
        {
            statusStrip.Items[0].Text = "Ordenes  Encontrados: " + NumRegistros;
        }
    }//
}
