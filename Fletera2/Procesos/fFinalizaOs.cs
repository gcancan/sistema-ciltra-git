﻿using Core.Models;
using Fletera2.Clases;
using Fletera2Entidades;
using Fletera2Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Fletera2.Clases.Inicio;

namespace Fletera2.Procesos
{
    public partial class fFinalizaOs : Form
    {
        Usuario _user;


        //Variables
        CabOrdenServicio cabos;
        Inicio.opcForma opcforma;
        List<DetalleOSVista> listDetOS;
        //List<DOSInsumosVista> listInsumos;
        List<DetOrdenActividadProductos> listInsumos;
        List<DOSInsumosFAC> listInsumosconc;


        //CabPreOC cabpreoc;
        List<CabPreOC> listCabPreOC;
        List<DetPreOC> listDetPreOC;

        //CabPreSalida cabpresal;
        List<CabPreSalida> listCabPresal;
        List<DetPreSalida> listDetPresal;

        List<admConceptos> listConceptos;

        //Para MsgBox
        string caption;
        MessageBoxButtons buttons = MessageBoxButtons.YesNo;
        DialogResult result;
        string mensaje = "";

        //Para Ejecutar
        string StrSql;
        int indice;
        string[] ArraySql;
        ConsultasSQL objsql = new ConsultasSQL();

        int _IdEmpresa;
        CatEmpresas empresa;
        admClientes clienteCom;
        admProductos servicio;

        int _idOrdenSer;

        Parametros param;

        ////para facturacion COMERCIAL
        //int CIDDOCUMENTO;
        //StringBuilder vSerie = new StringBuilder(12);
        //double vFolio = 0;



        CatActividades actividad;

        List<CatSAT> listFormaPago;
        List<CatSAT> listCFDI;
        List<CatSAT> listMetodoPago;
        OperationResult resp;


        CabMovimientos CabMov;
        List<DetMovimientos> listDetMov;

        decimal PrecioTaller = 500;
        decimal PorcIva = 16;
        decimal TotManoObra = 0;
        decimal TotInsumos = 0;
        decimal subtotal = 0;
        decimal impIva = 0;
        decimal tothrs = 0;
        public fFinalizaOs(Usuario user, int IdEmpresa, int idOrdenSer = 0)
        {
            _IdEmpresa = IdEmpresa;
            _idOrdenSer = idOrdenSer;
            _user = user;
            InitializeComponent();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void fFinalizaOs_Load(object sender, EventArgs e)
        {
            Cancelar();
            OperationResult resp = new ParametrosSvc().getParametrosxFiltro(1);
            if (resp.typeResult == ResultTypes.success)
            {
                param = ((List<Parametros>)resp.result)[0];
            }
            if (_idOrdenSer > 0)
            {
                btnNuevo.PerformClick();
                txtidOrdenSer.Text = _idOrdenSer.ToString();
                 CargaForma(_idOrdenSer.ToString(), opcBusqueda.CabViajes);
            }
        }

        private void Cancelar()
        {
            HabilitaBotones(opcHabilita.DesHabilitaTodos);
            HabilitaCampos(opcHabilita.DesHabilitaTodos);
            LimpiaCampos();
            CargaCombos();
            FormatoGeneral(grdInsumosConc);
            FormatoGeneral(grdDetOs);
            FormatoGeneral(grdRefacciones);
            FormatoGeneral(grdCabOC);
            FormatoGeneral(grdDetOC);
            FormatoGeneral(grdCabPreSalida);
            FormatoGeneral(grdDetPreSalida);


        }
        private void LimpiaCampos()
        {
            txtidOrdenSer.Clear();
            txtIdEmpresa.Clear();
            txtTipoOrdenServ.Clear();
            dtpFecha.Value = DateTime.Now;
            dtpFechaHora.Value = DateTime.Now;
            txtTipoUnidad.Clear();
            txtidUnidadTrans.Clear();
            txtidEmpleadoEntrega.Clear();
            txtEstatus.Clear();

            txtTotRefacciones.Clear();
            txtTotManoObra.Clear();
            txtTotSubTotal.Clear();
            txtTotIVA.Clear();
            txtTotTOTAL.Clear();

            grdDetOs.DataSource = null;
            grdRefacciones.DataSource = null;
            grdCabOC.DataSource = null;
            grdDetOC.DataSource = null;
            grdCabPreSalida.DataSource = null;
            grdDetPreSalida.DataSource = null;


        }

        private void HabilitaBotones(opcHabilita opc)
        {
            switch (opc)
            {
                case opcHabilita.DesHabilitaTodos:
                    btnNuevo.Enabled = true;
                    btnGrabar.Enabled = false;
                    btnCancelar.Enabled = false;
                    btnSalir.Enabled = true;
                    break;
                case opcHabilita.Iniciando:
                    break;
                case opcHabilita.Editando:
                    break;
                case opcHabilita.Nuevo:
                    btnNuevo.Enabled = false;
                    btnGrabar.Enabled = true;
                    btnCancelar.Enabled = true;
                    btnSalir.Enabled = false;
                    break;
                default:
                    break;
            }
        }

        private void HabilitaCampos(opcHabilita opc)
        {
            txtIdEmpresa.Enabled = false;
            txtTipoOrdenServ.Enabled = false;
            dtpFecha.Enabled = false;
            dtpFechaHora.Enabled = false;
            txtTipoUnidad.Enabled = false;
            txtidUnidadTrans.Enabled = false;
            txtidEmpleadoEntrega.Enabled = false;
            txtEstatus.Enabled = false;

            txtTotRefacciones.Enabled = false;
            txtTotManoObra.Enabled = false;
            txtTotSubTotal.Enabled = false;
            txtTotIVA.Enabled = false;
            txtTotTOTAL.Enabled = false;
            switch (opc)
            {
                case opcHabilita.DesHabilitaTodos:
                    txtidOrdenSer.Enabled = false;
                    grdDetOs.Enabled = false;
                    grdRefacciones.Enabled = false;

                    grdCabOC.Enabled = false;
                    grdDetOC.Enabled = false;
                    grdCabPreSalida.Enabled = false;
                    grdDetPreSalida.Enabled = false;

                    break;
                case opcHabilita.Iniciando:
                    break;
                case opcHabilita.Editando:
                    break;
                case opcHabilita.Nuevo:
                    txtidOrdenSer.Enabled = true;
                    grdDetOs.Enabled = true;
                    grdRefacciones.Enabled = true;
                    grdCabOC.Enabled = true;
                    grdDetOC.Enabled = true;
                    grdCabPreSalida.Enabled = true;
                    grdDetPreSalida.Enabled = true;


                    break;
                default:
                    break;
            }





        }

        private void txtidOrdenSer_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                if (txtidOrdenSer.Text != "")
                {
                    //Cargar Orden de Servicios
                    CargaForma(txtidOrdenSer.Text, opcBusqueda.CabViajes);
                }
                else
                {
                    txtidOrdenSer.Focus();
                }
            }
        }

        private void CargaForma(string Valor, opcBusqueda opc)
        {
            if (opc == opcBusqueda.CabViajes)
            {
                //OperationResult resp = new CabOrdenServicioSvc().getCabOrdenServicioxFilter(Convert.ToInt32(Valor), " c.Estatus = 'DIAG' ");
                //OperationResult resp = new CabOrdenServicioSvc().getCabOrdenServicioxFilter(Convert.ToInt32(Valor), " c.Estatus = 'TER' ");
                OperationResult resp = new CabOrdenServicioSvc().getCabOrdenServicioxFilter(Convert.ToInt32(Valor));
                if (resp.typeResult == ResultTypes.success)
                {
                    cabos = ((List<CabOrdenServicio>)resp.result)[0];
                    //if (cabos.Estatus == "DIAG")
                    if (cabos.Estatus == "TER")
                    {
                        //Continua
                        txtIdEmpresa.Text = cabos.RazonSocial;
                        txtTipoOrdenServ.Text = cabos.NomTipOrdServ;
                        dtpFecha.Value = (DateTime)cabos.FechaDiagnostico;
                        dtpFechaHora.Value = (DateTime)cabos.FechaDiagnostico;
                        txtTipoUnidad.Text = cabos.NomTipoUnidad;
                        txtidUnidadTrans.Text = cabos.idUnidadTrans;
                        txtidEmpleadoEntrega.Text = cabos.NomEmpleadoEntrega;
                        txtEstatus.Text = cabos.Estatus;
                        //Determina Cliente
                        OperationResult respEmp = new CatEmpresasSvc().getCatEmpresasxFiltro(_IdEmpresa);
                        if (respEmp.typeResult == ResultTypes.success)
                        {
                            empresa = ((List<CatEmpresas>)respEmp.result)[0];

                            OperationResult respcli = new ComercialSvc().getadmClientesxFilter(empresa.CIDCLIENTEPROVEEDOR);
                            if (respcli.typeResult == ResultTypes.success)
                            {
                                clienteCom = ((List<admClientes>)respcli.result)[0];
                                SeleccionacmbFormaPago(clienteCom.CMETODOPAG);
                                SeleccionacmbUsoCFDI(clienteCom.CUSOCFDI);
                                //OperationResult resp = new ParametrosSvc().getParametrosxFiltro(1);
                                //if (resp.typeResult == ResultTypes.success)
                                //{
                                //    param = ((List<Parametros>)resp.result)[0];
                                //}
                            }
                        }
                        //Determina Cliente

                        //ORDENES DE TRABAJO NO CANCELADAS
                        OperationResult respot = new DetOrdenServicioSvc().getDetalleOSVistaxFilter(Convert.ToInt32(Valor), " dos.estatus in ('TER')");
                        if (respot.typeResult == ResultTypes.success)
                        {
                            listDetOS = (List<DetalleOSVista>)respot.result;
                            grdDetOs.DataSource = null;
                            grdDetOs.DataSource = listDetOS;
                            //FormatoGrid
                            //INSUMOS
                            //OperationResult resin = new DetOrdenServicioSvc().getDOSInsumosVistaxFilter(Convert.ToInt32(Valor));
                            //OperationResult resin = new DetOrdenActividadProductosSvc().getDetOrdenActividadProductosxFilter(0, " d.idOrdenSer = " + Convert.ToInt32(Valor) + " AND cabpresal.Estatus = 'ENT'");
                            OperationResult resin = new DetOrdenActividadProductosSvc().getDetOrdenActividadProductosxFilter(0, " d.idOrdenSer = " + Convert.ToInt32(Valor));
                            if (resin.typeResult == ResultTypes.success)
                            {
                                OperationResult resact;
                                admProductos procom;
                                listInsumos = (List<DetOrdenActividadProductos>)resin.result;
                                //Actualizar Precios de Comercial
                                foreach (var item in listInsumos)
                                {
                                    resact = new ComercialSvc().getadmProductosxFilter(item.idProducto);
                                    if (resact.typeResult == ResultTypes.success)
                                    {
                                        procom = ((List<admProductos>)resact.result)[0];
                                        item.Precio = (decimal)procom.CPRECIO1;
                                    }

                                }
                                //Actualizar Precios de Comercial
                                grdRefacciones.DataSource = null;
                                grdRefacciones.DataSource = listInsumos;

                                //FormatoGrid

                                //Ordenes de compra
                                OperationResult rescoc = new OrdenCompraSvc().getCabPreOCxFilter(0, " idOrdenTrabajo IN (SELECT idOrdenTrabajo FROM dbo.DetOrdenServicio WHERE idOrdenSer = " + Convert.ToInt32(Valor) + ")");
                                if (rescoc.typeResult == ResultTypes.success)
                                {
                                    listCabPreOC = (List<CabPreOC>)rescoc.result;
                                    // List<> ;
                                    grdCabOC.DataSource = null;
                                    grdCabOC.DataSource = listCabPreOC;
                                    //FormatoGrid
                                    OperationResult resdoc = new OrdenCompraSvc().getDetPreOCxFilter(0, " IdPreOC IN (SELECT IdPreOC FROM dbo.CabPreOC WHERE idOrdenTrabajo IN (SELECT idOrdenTrabajo FROM dbo.DetOrdenServicio WHERE idOrdenSer = " + Convert.ToInt32(Valor) + "))");
                                    if (resdoc.typeResult == ResultTypes.success)
                                    {
                                        listDetPreOC = (List<DetPreOC>)resdoc.result;
                                        grdDetOC.DataSource = null;
                                        grdDetOC.DataSource = listDetPreOC;
                                        FiltraOrdenesCompra(0);
                                        //FormatoGrid
                                    }
                                }
                                //Presalidas de Almacen
                                OperationResult rescps = new PreSalidasSvc().getCabPreSalidaxFilter(0, " c.Estatus = 'ENT' and c.idOrdenTrabajo IN (SELECT idOrdenTrabajo FROM dbo.DetOrdenServicio WHERE idOrdenSer = " + Convert.ToInt32(Valor) + ")");
                                if (rescps.typeResult == ResultTypes.success)
                                {
                                    listCabPresal = (List<CabPreSalida>)rescps.result;
                                    grdCabPreSalida.DataSource = null;
                                    grdCabPreSalida.DataSource = listCabPresal;
                                    OperationResult resdps = new PreSalidasSvc().getDetPreSalidaxFilter(0, " d.IdPreSalida IN (SELECT IdPreSalida FROM dbo.CabPreSalida WHERE Estatus = 'ENT' and idOrdenTrabajo IN (SELECT idOrdenTrabajo FROM dbo.DetOrdenServicio WHERE Estatus = 'TER'  AND idOrdenSer = " + Convert.ToInt32(Valor) + "))");
                                    if (resdps.typeResult == ResultTypes.success)
                                    {
                                        listDetPresal = (List<DetPreSalida>)resdps.result;
                                        grdDetPreSalida.DataSource = null;
                                        grdDetPreSalida.DataSource = listDetPresal;
                                        FiltraSalidasAlmacen(0);

                                    }
                                    // ;
                                    //List<> ;

                                }

                            }
                            else
                            {
                                //que pasa
                            }


                        }

                        RecalculaInsumosConc(Convert.ToInt32(txtidOrdenSer.Text), param.PorcUtilidad);
                        CalculaResumen();
                        FormatogrdInsumosConc();
                        FormatogrdDetPreSal();
                        FormatogrdCabPreSal();
                        //FormatogrdDetOs();

                    }
                    else
                    {
                        //Mensaje
                    }

                }

            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            opcforma = Inicio.opcForma.Nuevo;
            HabilitaBotones(opcHabilita.Nuevo);
            HabilitaCampos(opcHabilita.Nuevo);
            txtidOrdenSer.Focus();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            if (_idOrdenSer > 0)
            {
                Close();
            }
            else
            {
                Cancelar();
            }


        }

        private void btnGrabar_Click(object sender, EventArgs e)
        {

            try
            {
                indice = 0;
                if (!Validar())
                {
                    return;
                }

                //FACTURAR A CLIENTE
                CabMov = MapeaCabMov();
                int Consecutivo = 1;
                foreach (var item in listInsumosconc)
                {
                    AgregaListaDet(0, Convert.ToInt32(1), item.CCODIGOPRODUCTO, Consecutivo, 0, "",
       "FACOS", Convert.ToDecimal(item.Cantidad), Convert.ToDecimal(item.Precio), 16, 0,
       0, 0, 1, 0, 0, 0, 0, false, false, 0, 0, "", true, item.CNOMBREPRODUCTO);

                    Consecutivo++;
                }
                //if (EnviaComercial(Convert.ToInt32(txtidOrdenSer.Text), Convert.ToDecimal(txtTotalhr.Text)))
                if (EnviaComercial(Convert.ToInt32(txtidOrdenSer.Text)))
                {

                    //MessageBox.Show("EnviaComercial Exitoso", "Operación Exitosa", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //return;
                    //GRABAR LOCAL
                    indice = 0;
                    foreach (var item in listDetOS)
                    {

                        //update a CIDPRODUCTO_SERV,Cantidad_CIDPRODUCTO_SERV,Precio_CIDPRODUCTO_SERV
                        StrSql = objsql.ActualizaCampoTabla("DetOrdenServicio",
                        "CIDPRODUCTO_SERV", eTipoDato.TdNumerico, item.CIDPRODUCTO_SERV.ToString(),
                        "idOrdenSer", eTipoDato.TdNumerico, txtidOrdenSer.Text, " idOrdenTrabajo = " + item.idOrdenTrabajo.ToString());
                        System.Array.Resize(ref ArraySql, indice + 1);
                        ArraySql[indice] = StrSql;
                        indice += 1;

                        StrSql = objsql.ActualizaCampoTabla("DetOrdenServicio",
                        "Cantidad_CIDPRODUCTO_SERV", eTipoDato.TdNumerico, item.Cantidad_CIDPRODUCTO_SERV.ToString(),
                        "idOrdenSer", eTipoDato.TdNumerico, txtidOrdenSer.Text, " idOrdenTrabajo = " + item.idOrdenTrabajo.ToString());
                        System.Array.Resize(ref ArraySql, indice + 1);
                        ArraySql[indice] = StrSql;
                        indice += 1;

                        StrSql = objsql.ActualizaCampoTabla("DetOrdenServicio",
                       "Precio_CIDPRODUCTO_SERV", eTipoDato.TdNumerico, item.Precio_CIDPRODUCTO_SERV.ToString(),
                       "idOrdenSer", eTipoDato.TdNumerico, txtidOrdenSer.Text, " idOrdenTrabajo = " + item.idOrdenTrabajo.ToString());
                        System.Array.Resize(ref ArraySql, indice + 1);
                        ArraySql[indice] = StrSql;
                        indice += 1;

                        if ((Inicio.eTipoOrdenServicio)Convert.ToInt32(cabos.idTipOrdServ) == Inicio.eTipoOrdenServicio.LLANTAS)
                        {
                            StrSql = objsql.DiagTerDetOrdenServicio(DateTime.Now, item.idOrdenSer, item.idOrdenTrabajo);
                            System.Array.Resize(ref ArraySql, indice + 1);
                            ArraySql[indice] = StrSql;
                            indice += 1;

                        }
                        else if ((Inicio.eTipoOrdenServicio)Convert.ToInt32(cabos.idTipOrdServ) == Inicio.eTipoOrdenServicio.TALLER)
                        {
                            StrSql = objsql.TerDetOrdenServicio(DateTime.Now, item.idOrdenSer, item.idOrdenTrabajo);
                            System.Array.Resize(ref ArraySql, indice + 1);
                            ArraySql[indice] = StrSql;
                            indice += 1;

                        }
                        else if ((Inicio.eTipoOrdenServicio)Convert.ToInt32(cabos.idTipOrdServ) == Inicio.eTipoOrdenServicio.LAVADERO)
                        {
                            StrSql = objsql.TerDetOrdenServicio(DateTime.Now, item.idOrdenSer, item.idOrdenTrabajo);
                            System.Array.Resize(ref ArraySql, indice + 1);
                            ArraySql[indice] = StrSql;
                            indice += 1;
                        }
                    }


                    if ((Inicio.eTipoOrdenServicio)Convert.ToInt32(cabos.idTipOrdServ) == Inicio.eTipoOrdenServicio.LLANTAS)
                    {
                        StrSql = objsql.DiagTerCabOrdenServicio(DateTime.Now, cabos.idOrdenSer);
                        System.Array.Resize(ref ArraySql, indice + 1);
                        ArraySql[indice] = StrSql;
                        indice += 1;
                    }
                    else if ((Inicio.eTipoOrdenServicio)Convert.ToInt32(cabos.idTipOrdServ) == Inicio.eTipoOrdenServicio.TALLER)
                    {
                        StrSql = objsql.TerCabOrdenServicio(DateTime.Now, cabos.idOrdenSer);
                        System.Array.Resize(ref ArraySql, indice + 1);
                        ArraySql[indice] = StrSql;
                        indice += 1;
                    }
                    else if ((Inicio.eTipoOrdenServicio)Convert.ToInt32(cabos.idTipOrdServ) == Inicio.eTipoOrdenServicio.LAVADERO)
                    {
                        StrSql = objsql.TerCabOrdenServicio(DateTime.Now, cabos.idOrdenSer);
                        System.Array.Resize(ref ArraySql, indice + 1);
                        ArraySql[indice] = StrSql;
                        indice += 1;
                    }

                    //Actualiza CabOrdenServicio CIDDOCUMENTO, CSERIEDOCUMENTO, CFOLIO de Comercial
                    //StrSql = objsql.ActualizaCampoTabla("CabOrdenServicio", 
                    //"CIDDOCUMENTO", TipoDato.TdNumerico, CIDDOCUMENTO.ToString(), 
                    //"idOrdenSer", TipoDato.TdNumerico, txtidOrdenSer.Text);
                    //System.Array.Resize(ref ArraySql, indice + 1);
                    //ArraySql[indice] = StrSql;
                    //indice += 1;

                    StrSql = objsql.ActualizaCampoTabla("CabOrdenServicio",
                   "CIDDOCUMENTO", eTipoDato.TdNumerico, "1",
                   "idOrdenSer", eTipoDato.TdNumerico, txtidOrdenSer.Text);
                    System.Array.Resize(ref ArraySql, indice + 1);
                    ArraySql[indice] = StrSql;
                    indice += 1;

                    StrSql = objsql.ActualizaCampoTabla("CabOrdenServicio",
                    "CSERIEDOCUMENTO", eTipoDato.TdCadena, vSerie.ToString(),
                    "idOrdenSer", eTipoDato.TdNumerico, txtidOrdenSer.Text);
                    System.Array.Resize(ref ArraySql, indice + 1);
                    ArraySql[indice] = StrSql;
                    indice += 1;

                    StrSql = objsql.ActualizaCampoTabla("CabOrdenServicio",
                    "CFOLIO", eTipoDato.TdNumerico, vFolio.ToString(),
                    "idOrdenSer", eTipoDato.TdNumerico, txtidOrdenSer.Text);
                    System.Array.Resize(ref ArraySql, indice + 1);
                    ArraySql[indice] = StrSql;
                    indice += 1;




                    if (indice > 0)
                    {
                        //MessageBox.Show("Sentencias por Ejecutar local " + indice.ToString(), "Operación Exitosa", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        string Respuesta;
                        Respuesta = new UtilSvc().EjecutaSentenciaSql("", ref ArraySql, indice);
                        if (Respuesta == "EFECTUADO")
                        {
                            //btnCancelar.PerformClick();


                            resp = new MovimientosSvc().GuardaMovimientos(ref CabMov, ref listDetMov);
                            if (resp.typeResult == ResultTypes.success)
                            {
                                Inicio.ImprimeFactura(vSerie + "" + vFolio.ToString(), @"C:\Compac\Empresas\adprueba\XML_SDK\");
                                btnCancelar.PerformClick();
                            }
                            //MessageBox.Show("Se Facturo Satisfactoriamente la Order de Servicio: " + txtidOrdenSer.Text, "Operación Exitosa", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        }
                    }
                    //GRABAR LOCAL

                }
                else
                {
                    //Mensaje
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);

            }
        }

        private bool Validar()
        {
            //checar si todo lo referente a insumos ya esta entregado.
            foreach (var item in listDetOS)
            {
                if (item.Estatus != "TER")
                {
                    //break;
                    //Mensaje
                    return false;
                }
            }
            return true;
        }



        private void grdCabOC_Click(object sender, EventArgs e)
        {
            FiltraOrdenesCompra(grdCabOC.CurrentCell.RowIndex);
        }

        private void FiltraOrdenesCompra(int IndiceList)
        {
            int idPreOc = listCabPreOC[IndiceList].IdPreOC;

            var mySearch = (listDetPreOC).FindAll(S => S.IdPreOC.ToString().IndexOf(idPreOc.ToString().Trim(), StringComparison.OrdinalIgnoreCase) >= 0);

            grdDetOC.DataSource = null;
            grdDetOC.DataSource = mySearch;
        }

        private void grdCabPreSalida_Click(object sender, EventArgs e)
        {
            FiltraSalidasAlmacen(grdCabPreSalida.CurrentCell.RowIndex);
        }

        private void FiltraSalidasAlmacen(int IndiceList)
        {
            int idPreSal = listCabPresal[IndiceList].IdPreSalida;

            var mySearch = (listDetPresal).FindAll(S => S.IdPreSalida.ToString().IndexOf(idPreSal.ToString().Trim(), StringComparison.OrdinalIgnoreCase) >= 0);

            grdDetPreSalida.DataSource = null;
            grdDetPreSalida.DataSource = mySearch;
        }

        private void CalculaResumen()
        {
            //decimal PrecioTaller = 500;
            //decimal PorcIva = 16;
            //decimal TotManoObra = 0;
            //decimal TotInsumos = 0;
            //decimal subtotal = 0;
            //decimal impIva = 0;
            //decimal tothrs = 0;


            //Mano de Obra
            //if (listDetOS.Count > 0)
            //{
            //    foreach (var item in listDetOS)
            //    {
            //        //tothrs += item.DuracionActHr;
            //        //TotManoObra += (item.DuracionActHr * PrecioTaller);
            //        if (item.Precio_CIDPRODUCTO_SERV > 0)
            //        {
            //            TotManoObra += (item.Precio_CIDPRODUCTO_SERV * item.Cantidad_CIDPRODUCTO_SERV);
            //        }

            //        //TotManoObra += (item.DuracionActHr * PrecioTaller);

            //    }
            //}
            //Insumos
            if (listInsumosconc != null)
            {
                if (listInsumosconc.Count > 0)
                {
                    foreach (var item in listInsumosconc)
                    {
                        if (item.Precio == 0)
                        {
                            MessageBox.Show("Se Encontro con un Precio Cero para " + item.CNOMBREPRODUCTO, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            //break;

                        }
                        if (item.Tipo == "INSUMO")
                        {
                            TotInsumos += (item.Cantidad * item.Precio);
                        }
                        else
                        {
                            TotManoObra += (item.Precio * item.Cantidad);
                        }

                    }
                }
            }
            else
            {
                if (listInsumos != null)
                {
                    foreach (var item in listInsumos)
                    {

                    }
                }
            }

            txtTotalhr.Text = tothrs.ToString();
            txtTotManoObra.Text = TotManoObra.ToString("C2");
            txtTotRefacciones.Text = TotInsumos.ToString("C2");
            subtotal = TotInsumos + TotManoObra;
            impIva = (TotInsumos + TotManoObra) * (PorcIva / 100);
            txtTotSubTotal.Text = subtotal.ToString("C2");
            txtTotIVA.Text = impIva.ToString("C2");
            txtTotTOTAL.Text = (subtotal + impIva).ToString("C2");
        }

        private bool TimbraFactura(string TipoDocumento, string vSerie, double vFolio)
        {

            bool Exito = false;
            try
            {
                byte lLicencia = 0;
                lError = SDK_Comercial.fInicializaLicenseInfo(lLicencia);
                if (lError == 0)
                {

                    Parametro_PassFacturacion = "12345678a";
                    lError = SDK_Comercial.fEmitirDocumento(TipoDocumento, vSerie, vFolio, Parametro_PassFacturacion, "");
                    if (lError == 0)
                    {
                        lPlantilla = "facturaROA.rdl";
                        lError = SDK_Comercial.fEntregEnDiscoXML(TipoDocumento, vSerie, vFolio, 1, lPlantilla);
                        if (lError == 0)
                        {
                            //MessageBox.Show("Se Ejecuto SDK_Comercial.fEntregEnDiscoXML", "Checando", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            lError = SDK_Comercial.fEntregEnDiscoXML(TipoDocumento, vSerie, vFolio, 0, lPlantilla);
                            if (lError == 0)
                            {
                                //MessageBox.Show("Se Ejecuto SDK_Comercial.fEntregEnDiscoXML", "Checando", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                Exito = true;
                            }
                            else
                            {
                                SDK_Comercial.MuestraError(lError);
                                Exito = false;

                            }
                        }
                        else
                        {
                            SDK_Comercial.MuestraError(lError);
                            Exito = false;

                        }
                    }
                    else
                    {
                        SDK_Comercial.MuestraError(lError);
                        Exito = false;
                    }
                }
                else
                {
                    SDK_Comercial.MuestraError(lError);
                    Exito = false;
                    //Error
                }
                return Exito;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
                return Exito;
            }
        }
        //private bool EnviaComercial(int idOrdenSer, decimal Numhoras)
        private bool EnviaComercial(int idOrdenSer)
        {
            try
            {


                bool Exito = false;
                string TipDocumento = "";

                OperationResult respC = new ComercialSvc().getadmConceptosxFilter(((admConceptos)cmbConcepto.SelectedValue).CIDCONCEPTODOCUMENTO);
                if (respC.typeResult == ResultTypes.success)
                {
                    admConceptos conceptoCom = ((List<admConceptos>)respC.result)[0];
                    TipDocumento = conceptoCom.CCODIGOCONCEPTO;
                }
                //Determina Cliente
                OperationResult respEmp = new CatEmpresasSvc().getCatEmpresasxFiltro(_IdEmpresa);
                if (respEmp.typeResult == ResultTypes.success)
                {
                    empresa = ((List<CatEmpresas>)respEmp.result)[0];

                    OperationResult respcli = new ComercialSvc().getadmClientesxFilter(empresa.CIDCLIENTEPROVEEDOR);
                    if (respcli.typeResult == ResultTypes.success)
                    {
                        clienteCom = ((List<admClientes>)respcli.result)[0];
                        //OperationResult resp = new ParametrosSvc().getParametrosxFiltro(1);
                        //if (resp.typeResult == ResultTypes.success)
                        //{
                        //    param = ((List<Parametros>)resp.result)[0];
                        //}
                    }
                }
                //Determina Cliente
                //RecalculaInsumosConc(idOrdenSer, param.PorcUtilidad);
                //CalculaResumen();

                if (IniciarSesionSDK(param.idEmpresaCOMTaller))
                {
                    lError = SDK_Comercial.fSiguienteFolio(TipDocumento, vSerie, ref vFolio);
                    if (lError == 0)
                    {
                        //If MsgBox("!! Esta seguro que desea Aplicar el Documento: " & TextoDocumento & " : " & vSerie & " " & vFolio & " ? !!", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                        SDK_Comercial.tDocumento ltDocto = new SDK_Comercial.tDocumento();
                        SDK_Comercial.tMovimiento ltMovto = new SDK_Comercial.tMovimiento();

                        int lIdDocto = 0;
                        int LIdMovto = 0;

                        int nCCANTPARCI = 0;

                        ltDocto.aCodConcepto = TipDocumento;
                        ltDocto.aFecha = DateTime.Now.ToString("MM/dd/yyyy");
                        ltDocto.aCodigoCteProv = clienteCom.CCODIGOCLIENTE;
                        ltDocto.aCodigoAgente = "";
                        ltDocto.aSistemaOrigen = 0;
                        ltDocto.aNumMoneda = 1;
                        ltDocto.aTipoCambio = 1;
                        ltDocto.aAfecta = 0;


                        lError = SDK_Comercial.fAltaDocumento(ref lIdDocto, ref ltDocto);
                        if (lError == 0)
                        {
                            CIDDOCUMENTO = lIdDocto;
                            int cont = 0;
                            //MessageBox.Show("Lista de Productos" + listInsumosconc.Count, "Checando", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            foreach (var item in listInsumosconc)
                            {
                                cont += 1;
                                //ltMovto.aCodAlmacen = almacen.CCODIGOALMACEN;
                                ltMovto.aCodAlmacen = param.idAlmacenEnt;
                                ltMovto.aConsecutivo = cont;
                                //MessageBox.Show("Producto" + item.CCODIGOPRODUCTO, "Checando", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                ltMovto.aCodProdSer = item.CCODIGOPRODUCTO;
                                ltMovto.aUnidades = (double)item.Cantidad;
                                ltMovto.aPrecio = (double)item.Precio;
                                ltMovto.aCosto = 0;
                                //ltMovto.aReferencia = item.NoMov.ToString();
                                ltMovto.aReferencia = "Orden Servicio = " + idOrdenSer.ToString();
                                lError = SDK_Comercial.fAltaMovimiento(lIdDocto, ref LIdMovto, ref ltMovto);
                                if (lError == 0)
                                {
                                    Exito = true;
                                }
                                else
                                {
                                    SDK_Comercial.MuestraError(lError);
                                    Exito = false;
                                    break;
                                }
                            }

                            //Actualizar datos
                            CabMov.IdMovimiento = Convert.ToInt32(vFolio.ToString());
                            CabMov.NoSerie = vSerie.ToString();
                            CabMov.CIDDOCUM02 = Convert.ToInt32(TipDocumento.ToString());
                            foreach (var item in listDetMov)
                            {
                                item.IdMovimiento = Convert.ToInt32(vFolio.ToString());
                                item.NoSerie = vSerie.ToString();
                            }

                            //Actualizar datos
                            if ((etipodoctoCOM)(Convert.ToInt32(TipDocumento)) == etipodoctoCOM.Factura)
                            {
                                //OPCIONES DE TIMBRADO
                                indice = 0;
                                //tipo pago
                                StrSql = objsql.ActualizaCampoTabla("admDocumentos",
                                "CMETODOPAG", eTipoDato.TdCadena, ((CatSAT)cmbFormaPago.SelectedItem).Clave,
                                "CIDDOCUMENTO", eTipoDato.TdNumerico, CIDDOCUMENTO.ToString());
                                System.Array.Resize(ref ArraySql, indice + 1);
                                ArraySql[indice] = StrSql;
                                indice += 1;

                                nCCANTPARCI = Convert.ToInt32(((CatSAT)cmbMetodoPago.SelectedItem).Clave);
                                StrSql = objsql.ActualizaCampoTabla("admDocumentos",
                               "CCANTPARCI", eTipoDato.TdNumerico, nCCANTPARCI.ToString(),
                               "CIDDOCUMENTO", eTipoDato.TdNumerico, CIDDOCUMENTO.ToString());
                                System.Array.Resize(ref ArraySql, indice + 1);
                                ArraySql[indice] = StrSql;
                                indice += 1;

                                StrSql = objsql.ActualizaCampoTabla("admFoliosDigitales",
                               "CCODCONCBA", eTipoDato.TdCadena, ((CatSAT)cmbUsoCFDI.SelectedItem).Clave,
                               "CIDDOCTO", eTipoDato.TdNumerico, CIDDOCUMENTO.ToString(), " CIDDOCTODE = " + TipDocumento);
                                System.Array.Resize(ref ArraySql, indice + 1);
                                ArraySql[indice] = StrSql;
                                indice += 1;

                                if (indice > 0)
                                {
                                    //MessageBox.Show("Sentencias por Ejecutar local " + indice.ToString(), "Operación Exitosa", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    string Respuesta;
                                    Respuesta = new UtilSvc().EjecutaSentenciaSqlCOM("", ref ArraySql, indice);
                                    if (Respuesta == "EFECTUADO")
                                    {
                                        //MessageBox.Show("Se Facturo Satisfactoriamente la Order de Servicio: " + txtidOrdenSer.Text, "Operación Exitosa", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        //btnCancelar.PerformClick();
                                    }
                                }

                                //OPCIONES DE TIMBRADO
                                //Timbrar Factura
                                if (TimbraFactura(TipDocumento, vSerie.ToString(), vFolio))
                                {
                                    Exito = true;
                                    //MessageBox.Show("Se Timbro satisfactoriamente", "Checando", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                                else
                                {
                                    SDK_Comercial.MuestraError(lError);
                                    //SDK_Comercial.fBorraDocumento();
                                    Exito = false;
                                }
                            }
                        }
                        else
                        {
                            SDK_Comercial.MuestraError(lError);
                            //SDK_Comercial.fBorraDocumento();
                            Exito = false;
                        }
                    }
                    else
                    {

                        SDK_Comercial.MuestraError(lError);
                        Exito = false;
                    }
                }

                if (Exito)
                {
                    MessageBox.Show("Se realizo la Factura:" + vSerie + vFolio.ToString() + " para la Orden de Servicio " + txtidOrdenSer.Text, "Operación Exitosa", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                return Exito;

            }
            catch (Exception e)
            {
                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + e.Message,
                   this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                return false;
            }
            finally
            {
                SDK_Comercial.fCierraEmpresa();
                SDK_Comercial.fTerminaSDK();
            }

        }

        private void CargaCombos()
        {
            try
            {
                OperationResult resp = new ComercialSvc().getadmConceptosxFilter(0, " c.CIDDOCUMENTODE = 4");
                if (resp.typeResult == ResultTypes.success)
                {
                    listConceptos = (List<admConceptos>)resp.result;
                    cmbConcepto.DataSource = null;
                    cmbConcepto.DataSource = listConceptos;
                    cmbConcepto.DisplayMember = "CNOMBRECONCEPTO";
                    cmbConcepto.SelectedIndex = 0;
                }
                CargaAnexo20();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }
        //private void RecalculaInsumosConc(int idOrdenSer, decimal numhoras, decimal PorcUtilidad)
        private void RecalculaInsumosConc(int idOrdenSer, decimal PorcUtilidad)
        {
            listInsumosconc = null;
            admProductos prodcheca;
            admCostosHistoricos procom;
            //Insumos Concentrado


            //OperationResult resin = new DetOrdenActividadProductosSvc().getDetOrdenActividadProductosxFilter(0, " d.idOrdenSer = " + Convert.ToInt32(Valor) + " AND cabpresal.Estatus = 'ENT'");
            //OperationResult respins = new DetOrdenActividadProductosSvc().getDOSInsumosFACxFilter(idOrdenSer, " cabpresal.Estatus = 'ENT'");
            OperationResult respins = new DetOrdenActividadProductosSvc().getDOSInsumosFACxFilter(idOrdenSer);
            if (respins.typeResult == ResultTypes.success)
            {
                listInsumosconc = (List<DOSInsumosFAC>)respins.result;
                OperationResult resact;
                OperationResult resact2;


                //Actualizar Precio
                foreach (var item in listInsumosconc)
                {
                    //resact = new ComercialSvc().getadmProductosxFilter(item.idProducto);
                    resact = new ComercialSvc().getadmCostosHistoricosxFilter(item.idProducto, "", " c.CIDCOSTOH DESC");
                    if (resact.typeResult == ResultTypes.success)
                    {
                        procom = ((List<admCostosHistoricos>)resact.result)[0];
                        item.Precio = (decimal)procom.CULTIMOCOSTOH + ((decimal)procom.CULTIMOCOSTOH * (PorcUtilidad / 100));
                        item.Costo = (decimal)procom.CULTIMOCOSTOH;
                    }
                    else
                    {
                        if (item.Precio == 0)
                        {
                            resact2 = new ComercialSvc().getadmProductosxFilter(item.idProducto);
                            if (resact2.typeResult == ResultTypes.success)
                            {
                                prodcheca = ((List<admProductos>)resact2.result)[0];
                                if (prodcheca.CPRECIO1 > 0)
                                {
                                    item.Precio = Convert.ToDecimal(prodcheca.CPRECIO1);
                                }
                                else
                                {
                                    item.Precio = (item.Costo * (PorcUtilidad / 100)) + item.Costo;
                                }
                            }
                        }

                    }
                    item.Tipo = "INSUMO";
                }

                //grdInsumosConc.DataSource = null;
                //grdInsumosConc.DataSource = listInsumosconc;

                //Agregar Servicios UNICO
                //OperationResult respse = new ComercialSvc().getadmProductosxFilter(0, " CCODIGOPRODUCTO = 'SE00001'");
                //if (respse.typeResult == ResultTypes.success)
                //{
                //    servicio = ((List<admProductos>)respse.result)[0];
                //    DOSInsumosFAC serv = new DOSInsumosFAC
                //    {
                //        idProducto = servicio.CIDPRODUCTO,
                //        CCODIGOPRODUCTO = servicio.CCODIGOPRODUCTO,
                //        CNOMBREPRODUCTO = servicio.CNOMBREPRODUCTO,
                //        Cantidad = 1,
                //        Precio = 500
                //    };

                //    listInsumosconc.Add(serv);
                //}


            }

            //Agregar Serviciosif 
            if (listInsumosconc == null)
            {
                listInsumosconc = new List<DOSInsumosFAC>();
            }
            OperationResult respse;
            foreach (var item in listDetOS)
            {
                resp = new CatActividadesSvc().getCatActividadesxFilter(item.idActividad);
                if (resp.typeResult == ResultTypes.success)
                {
                    actividad = ((List<CatActividades>)resp.result)[0];

                    //item.CNOMBREPRODUCTO_SERV = actividad.
                    //item.CCODIGOPRODUCTO_SERV
                }
                //if (item.CIDPRODUCTO_SERV == 0)
                //{

                //}
                //else
                //{

                //}
                if (actividad.CIDPRODUCTO > 0)
                {
                    respse = new ComercialSvc().getadmProductosxFilter(0, "p.CCODIGOPRODUCTO = '" + actividad.CCODIGOPRODUCTO + "'");
                    if (respse.typeResult == ResultTypes.success)
                    {
                        prodcheca = ((List<admProductos>)respse.result)[0];
                        ////checar costo
                        //resp = new ComercialSvc().getadmCostosHistoricosxFilter(prodcheca.CIDPRODUCTO, "", " c.CIDMOVIMIENTO DESC");
                        //if (resp.typeResult == ResultTypes.success)
                        //{
                        //    tipopago = ((List<TipoPago>)resp.result)[0];
                        //}
                        item.CIDPRODUCTO_SERV = prodcheca.CIDPRODUCTO;
                        item.CCODIGOPRODUCTO_SERV = prodcheca.CCODIGOPRODUCTO;
                        item.CNOMBREPRODUCTO_SERV = prodcheca.CNOMBREPRODUCTO;
                        item.Cantidad_CIDPRODUCTO_SERV = 1;
                        DOSInsumosFAC serv = new DOSInsumosFAC
                        {
                            idProducto = prodcheca.CIDPRODUCTO,
                            CCODIGOPRODUCTO = prodcheca.CCODIGOPRODUCTO,
                            CNOMBREPRODUCTO = prodcheca.CNOMBREPRODUCTO,
                            Cantidad = item.Cantidad_CIDPRODUCTO_SERV,
                            Precio = (decimal)prodcheca.CPRECIO1,
                            Tipo = "MANO OBRA"
                        };

                        listInsumosconc.Add(serv);

                        foreach (var itemD in listDetOS)
                        {
                            if (itemD.CCODIGOPRODUCTO_SERV == prodcheca.CCODIGOPRODUCTO)
                            {
                                itemD.CIDPRODUCTO_SERV = prodcheca.CIDPRODUCTO;
                                itemD.CCODIGOPRODUCTO_SERV = prodcheca.CCODIGOPRODUCTO;
                                itemD.CNOMBREPRODUCTO_SERV = prodcheca.CNOMBREPRODUCTO;
                                itemD.Cantidad_CIDPRODUCTO_SERV = item.Cantidad_CIDPRODUCTO_SERV;
                                itemD.Precio_CIDPRODUCTO_SERV = (decimal)prodcheca.CPRECIO1;

                                break;
                            }
                        }
                    }
                }
            }
            //Agregar Servicios
            grdInsumosConc.DataSource = null;
            grdInsumosConc.DataSource = listInsumosconc;
        }

        private void grdCabOC_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txtidOrdenSer_TextChanged(object sender, EventArgs e)
        {

        }

        private void grdDetOs_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void grdDetOs_DoubleClick(object sender, EventArgs e)
        {
            fCapturaServCOM frm = new fCapturaServCOM(Convert.ToInt32(txtidOrdenSer.Text),
                listDetOS[grdDetOs.CurrentCell.RowIndex].idOrdenTrabajo);
            frm.StartPosition = FormStartPosition.CenterParent;
            //frm.ShowDialog();
            ServicioxOrdenTrabajo valor = frm.DevuelveValor();
            if (valor != null)
            {
                listDetOS[grdDetOs.CurrentCell.RowIndex].CIDPRODUCTO_SERV = valor.CIDPRODUCTO_SERV;
                listDetOS[grdDetOs.CurrentCell.RowIndex].CCODIGOPRODUCTO_SERV = valor.CCODIGOPRODUCTO_SERV;
                listDetOS[grdDetOs.CurrentCell.RowIndex].CNOMBREPRODUCTO_SERV = valor.CNOMBREPRODUCTO_SERV;
                listDetOS[grdDetOs.CurrentCell.RowIndex].Cantidad_CIDPRODUCTO_SERV = valor.Cantidad_CIDPRODUCTO_SERV;
                listDetOS[grdDetOs.CurrentCell.RowIndex].Precio_CIDPRODUCTO_SERV = valor.Precio_CIDPRODUCTO_SERV;
            }
            RecalculaInsumosConc(Convert.ToInt32(txtidOrdenSer.Text), param.PorcUtilidad);
            CalculaResumen();
        }

        private void grdInsumosConc_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void FormatoGeneral(DataGridView grid)
        {
            //ESTILO GENERAL
            grid.RowsDefaultCellStyle.BackColor = Color.White;
            grid.AlternatingRowsDefaultCellStyle.BackColor = Color.AliceBlue;

            grid.RowsDefaultCellStyle.SelectionForeColor = Color.Yellow;
            grid.RowsDefaultCellStyle.SelectionBackColor = Color.Gray;
            grid.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            grid.DefaultCellStyle.Font = new Font("Berlin Sans FB", 12);
            grid.ColumnHeadersDefaultCellStyle.Font = new Font("Berlin Sans FB", 12);
            grid.ColumnHeadersDefaultCellStyle.BackColor = Color.Yellow;
            grid.ColumnHeadersDefaultCellStyle.ForeColor = Color.Red;
            grid.EnableHeadersVisualStyles = false;
        }

        private void FormatogrdInsumosConc()
        {
            //public int idProducto { get; set; }
            //public string CNOMBREPRODUCTO { get; set; }
            //public string CCODIGOPRODUCTO { get; set; }
            //public decimal Cantidad { get; set; }
            //public decimal Precio { get; set; }
            //public string Tipo { get; set; }

            //public decimal Costo { get; set; }


            //OCULTAR COLUMNAS
            //grdDetOs.Columns["NomServicio"].Visible = false;
            grdInsumosConc.Columns["Costo"].Visible = false;
            //HACER READONLY COLUMNAS
            //grdViajes.Columns["Seleccionado"].ReadOnly = true;

            //ENCABEZADO DE COLUMNAS
            grdInsumosConc.Columns["idProducto"].HeaderText = "Producto";
            grdInsumosConc.Columns["CCODIGOPRODUCTO"].HeaderText = "Codigo";
            grdInsumosConc.Columns["CNOMBREPRODUCTO"].HeaderText = "Descripción";
            grdInsumosConc.Columns["Cantidad"].HeaderText = "Cantidad";
            grdInsumosConc.Columns["Precio"].HeaderText = "Precio";
            grdInsumosConc.Columns["Tipo"].HeaderText = "Tipo";


            //FORMATO DE COLUMNAS
            //grdDatos.Columns["Precio"].DefaultCellStyle.Format = "C2";

            //ALINEACION DE COLUMNAS
            //grdDatos.Columns["Precio"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight;

            //TAMAÑO AUTOMATICO DE COLUMNAS
            grdInsumosConc.Columns["idProducto"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdInsumosConc.Columns["CCODIGOPRODUCTO"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdInsumosConc.Columns["CNOMBREPRODUCTO"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdInsumosConc.Columns["Cantidad"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

            grdInsumosConc.Columns["Precio"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdInsumosConc.Columns["Tipo"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

            grdInsumosConc.Columns["idProducto"].DisplayIndex = 0;
            grdInsumosConc.Columns["CCODIGOPRODUCTO"].DisplayIndex = 1;
            grdInsumosConc.Columns["CNOMBREPRODUCTO"].DisplayIndex = 2;
            grdInsumosConc.Columns["Cantidad"].DisplayIndex = 3;
            grdInsumosConc.Columns["Precio"].DisplayIndex = 4;
            grdInsumosConc.Columns["Tipo"].DisplayIndex = 5;
        }
        private void FormatogrdDetOs()
        {
            //public int idOrdenTrabajo { get; set; }

            //public bool Seleccionado { get; set; }
            //public int idOrdenSer { get; set; }

            //public string Actividad { get; set; }
            //public string Estatus { get; set; }
            //public int idPersonalResp { get; set; }
            //public string NomResponsable { get; set; }
            //public int idPersonalAyu1 { get; set; }
            //public string NomAyudante1 { get; set; }
            //public int idPersonalAyu2 { get; set; }
            //public string NomAyudante2 { get; set; }
            //public decimal DuracionActHr { get; set; }
            //public int CIDPRODUCTO_SERV { get; set; }
            //public string CCODIGOPRODUCTO_SERV { get; set; }
            //public string CNOMBREPRODUCTO_SERV { get; set; }
            //public decimal Cantidad_CIDPRODUCTO_SERV { get; set; }
            //public decimal Precio_CIDPRODUCTO_SERV { get; set; }


            //OCULTAR COLUMNAS
            //grdDetOs.Columns["NomServicio"].Visible = false;
            grdDetOs.Columns["Seleccionado"].Visible = false;
            grdDetOs.Columns["idOrdenSer"].Visible = false;
            grdDetOs.Columns["idOrdenActividad"].Visible = false;
            grdDetOs.Columns["idServicio"].Visible = false;
            grdDetOs.Columns["idActividad"].Visible = false;
            grdDetOs.Columns["DuracionActHr"].Visible = false;
            grdDetOs.Columns["NoPersonal"].Visible = false;
            grdDetOs.Columns["id_proveedor"].Visible = false;
            grdDetOs.Columns["Estatus"].Visible = false;
            grdDetOs.Columns["CostoManoObra"].Visible = false;
            grdDetOs.Columns["CostoProductos"].Visible = false;
            grdDetOs.Columns["FechaDiag"].Visible = false;
            grdDetOs.Columns["UsuarioDiag"].Visible = false;
            grdDetOs.Columns["FechaAsig"].Visible = false;
            grdDetOs.Columns["UsuarioAsig"].Visible = false;
            grdDetOs.Columns["FechaPausado"].Visible = false;
            grdDetOs.Columns["NotaPausado"].Visible = false;
            grdDetOs.Columns["FechaTerminado"].Visible = false;
            grdDetOs.Columns["UsuarioTerminado"].Visible = false;
            grdDetOs.Columns["idDivision"].Visible = false;
            grdDetOs.Columns["idLlanta"].Visible = false;
            grdDetOs.Columns["PosLlanta"].Visible = false;
            grdDetOs.Columns["NotaDiagnostico"].Visible = false;
            grdDetOs.Columns["FaltInsumo"].Visible = false;
            grdDetOs.Columns["Pendiente"].Visible = false;
            grdDetOs.Columns["CIDPRODUCTO_SERV"].Visible = false;
            grdDetOs.Columns["Cantidad_CIDPRODUCTO_SERV"].Visible = false;
            grdDetOs.Columns["Precio_CIDPRODUCTO_SERV"].Visible = false;
            grdDetOs.Columns["idFamilia"].Visible = false;
            grdDetOs.Columns["NotaRecepcion"].Visible = false;
            grdDetOs.Columns["isCancelado"].Visible = false;
            grdDetOs.Columns["motivoCancelacion"].Visible = false;
            grdDetOs.Columns["idPersonalResp"].Visible = false;
            grdDetOs.Columns["idPersonalAyu1"].Visible = false;
            grdDetOs.Columns["idPersonalAyu2"].Visible = false;
            grdDetOs.Columns["NotaRecepcionA"].Visible = false;
            grdDetOs.Columns["CIDPRODUCTO_ACT"].Visible = false;




            //HACER READONLY COLUMNAS
            //grdViajes.Columns["Seleccionado"].ReadOnly = true;

            //ENCABEZADO DE COLUMNAS
            grdDetOs.Columns["idOrdenTrabajo"].HeaderText = "No.";
            grdDetOs.Columns["NotaRecepcionF"].HeaderText = "Descripción";
            grdDetOs.Columns["TipoNotaRecepcionF"].HeaderText = "Tipo";
            grdDetOs.Columns["FallaReportada"].HeaderText = "Falla Reportada";
            grdDetOs.Columns["NomPersonalResp"].HeaderText = "Responsable";
            grdDetOs.Columns["NomPersonalAyu1"].HeaderText = "Ayudante 1";
            grdDetOs.Columns["NomPersonalAyu2"].HeaderText = "Ayudante 2";
            //grdDetOs.Columns["x"].HeaderText = "Nota";
            //grdDetOs.Columns["FechaHoraFin"].HeaderText = "Fecha Fin";
            //grdDetOs.Columns["EstatusGuia"].HeaderText = "Estatus";
            //grdDetOs.Columns["TipoViaje"].HeaderText = "Tipo Viaje";
            //grdDetOs.Columns["idTipoOrden"].HeaderText = "idTipoOrden";


            //FORMATO DE COLUMNAS
            //grdDatos.Columns["Precio"].DefaultCellStyle.Format = "C2";

            //ALINEACION DE COLUMNAS
            //grdDatos.Columns["Precio"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight;

            //TAMAÑO AUTOMATICO DE COLUMNAS
            grdDetOs.Columns["idOrdenTrabajo"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdDetOs.Columns["NotaRecepcionF"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdDetOs.Columns["TipoNotaRecepcionF"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdDetOs.Columns["FallaReportada"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

            grdDetOs.Columns["NomPersonalResp"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdDetOs.Columns["NomPersonalAyu1"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdDetOs.Columns["NomPersonalAyu2"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            //grdDetOs.Columns["x"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            //grdDetOs.Columns["x"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            //grdDetOs.Columns["x"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

            //grdDetOs.Columns["EstatusGuia"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            //grdDetOs.Columns["TipoViaje"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

        }

        private void FormatogrdCabPreSal()
        {
            if (listCabPresal == null)
            {
                return;
            }

            if (listCabPresal.Count > 0)
            {
                grdCabPreSalida.Columns["IdEmpleadoEnc"].Visible = false;
                grdCabPreSalida.Columns["UserSolicitud"].Visible = false;

                grdCabPreSalida.Columns["idAlmacen"].Visible = false;
                grdCabPreSalida.Columns["NomAlmacen"].Visible = false;
                grdCabPreSalida.Columns["CIDALMACEN_COM"].Visible = false;
                grdCabPreSalida.Columns["Estatus"].Visible = false;
                grdCabPreSalida.Columns["SubTotal"].Visible = false;
                grdCabPreSalida.Columns["FecCancela"].Visible = false;
                grdCabPreSalida.Columns["UserCancela"].Visible = false;

                grdCabPreSalida.Columns["MotivoCancela"].Visible = false;
                grdCabPreSalida.Columns["FecEntrega"].Visible = false;
                grdCabPreSalida.Columns["UserEntrega"].Visible = false;
                grdCabPreSalida.Columns["IdPersonalRec"].Visible = false;
                grdCabPreSalida.Columns["EntregaCambio"].Visible = false;
                grdCabPreSalida.Columns["idPreOC"].Visible = false;
                grdCabPreSalida.Columns["CSERIEDOCUMENTO_ENT"].Visible = false;
                grdCabPreSalida.Columns["CIDDOCUMENTO_ENT"].Visible = false;
                grdCabPreSalida.Columns["CFOLIO_ENT"].Visible = false;
                grdCabPreSalida.Columns["CSERIEDOCUMENTO_SAL"].Visible = false;
                grdCabPreSalida.Columns["CIDDOCUMENTO_SAL"].Visible = false;
                grdCabPreSalida.Columns["CFOLIO_SAL"].Visible = false;
                grdCabPreSalida.Columns["NomEmpleadoEnc"].Visible = false;
                grdCabPreSalida.Columns["NomUserSolicitud"].Visible = false;



                grdCabPreSalida.Columns["IdPreSalida"].HeaderText = "Pre Salida";
                grdCabPreSalida.Columns["idOrdenTrabajo"].HeaderText = "O. Trabajo";
                grdCabPreSalida.Columns["idOrdenSer"].HeaderText = "O. Servicio";
                grdCabPreSalida.Columns["idUnidadTrans"].HeaderText = "Unidad";
                grdCabPreSalida.Columns["FecSolicitud"].HeaderText = "Fecha";
                //grdCabPreSalida.Columns["NomEmpleadoEnc"].HeaderText = "Encargado";
                //grdCabPreSalida.Columns["NotaRecepcion"].HeaderText = "Actividad";
                //grdCabPreSalida.Columns["NomSolicita"].HeaderText = "Solicita";

                grdCabPreSalida.Columns["IdPreSalida"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                grdCabPreSalida.Columns["idOrdenTrabajo"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                grdCabPreSalida.Columns["FecSolicitud"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                grdCabPreSalida.Columns["idOrdenSer"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                grdCabPreSalida.Columns["idUnidadTrans"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                //grdCabPreSalida.Columns["NomSolicita"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

                grdCabPreSalida.Columns["IdPreSalida"].DisplayIndex = 0;
                grdCabPreSalida.Columns["idOrdenTrabajo"].DisplayIndex = 1;
                grdCabPreSalida.Columns["idOrdenSer"].DisplayIndex = 2;
                grdCabPreSalida.Columns["idUnidadTrans"].DisplayIndex = 3;
                grdCabPreSalida.Columns["FecSolicitud"].DisplayIndex = 4;
            }
            //grdCabPreSal


        }
        private void FormatogrdDetPreSal()
        {
            if (listDetPresal == null)
            {
                return;
            }
            if (listDetPresal.Count > 0)
            {
                grdDetPreSalida.Columns["Seleccionado"].Visible = false;
                grdDetPreSalida.Columns["IdPreSalida"].Visible = false;
                //grdDetPreSalida.Columns["Entregar"].Visible = false;
                grdDetPreSalida.Columns["CantidadEnt"].Visible = false;
                grdDetPreSalida.Columns["Costo"].Visible = false;
                grdDetPreSalida.Columns["idUnidadProd"].Visible = false;
                grdDetPreSalida.Columns["Existencia"].Visible = false;
                grdDetPreSalida.Columns["Inserta"].Visible = false;

                grdDetPreSalida.Columns["Cantidad"].HeaderText = "Cantidad";
                grdDetPreSalida.Columns["idProducto"].HeaderText = "Producto";
                grdDetPreSalida.Columns["CCODIGOPRODUCTO"].HeaderText = "Código";
                grdDetPreSalida.Columns["CNOMBREPRODUCTO"].HeaderText = "Descripción";
                //grdDetPreSalida.Columns["IdPreSalida"].HeaderText = "Folio";
                //grdDetPreSalida.Columns["IdPreSalida"].HeaderText = "Folio";

                grdDetPreSalida.Columns["Cantidad"].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
                grdDetPreSalida.Columns["idProducto"].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
                grdDetPreSalida.Columns["CCODIGOPRODUCTO"].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
                grdDetPreSalida.Columns["CNOMBREPRODUCTO"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            }


        }
        private void FormatogrdRefacciones()
        {
            //public int idDetOrdenActividadProductos { get; set; }
            //public int idOrdenSer { get; set; }
            //public int idOrdenActividad { get; set; }
            //public int idProducto { get; set; }
            //public string CCODIGOPRODUCTO { get; set; }
            //public string CNOMBREPRODUCTO { get; set; }
            //public int idUnidadProd { get; set; }
            //public decimal Cantidad { get; set; }
            //public decimal Costo { get; set; }
            //public decimal Existencia { get; set; }
            //public decimal CantCompra { get; set; }
            //public decimal CantSalida { get; set; }
            //public decimal Precio { get; set; }
            grdRefacciones.Columns["Seleccionado"].Visible = false;
            grdRefacciones.Columns["IdPreSalida"].Visible = false;
            //grdDetPreSalida.Columns["Entregar"].Visible = false;
            grdDetPreSalida.Columns["CantidadEnt"].Visible = false;
            grdDetPreSalida.Columns["Costo"].Visible = false;
            grdDetPreSalida.Columns["idUnidadProd"].Visible = false;
            grdDetPreSalida.Columns["Existencia"].Visible = false;
            grdDetPreSalida.Columns["Inserta"].Visible = false;

            grdDetPreSalida.Columns["Cantidad"].HeaderText = "Cantidad";
            grdDetPreSalida.Columns["idProducto"].HeaderText = "Producto";
            grdDetPreSalida.Columns["CCODIGOPRODUCTO"].HeaderText = "Código";
            grdDetPreSalida.Columns["CNOMBREPRODUCTO"].HeaderText = "Descripción";
            //grdDetPreSalida.Columns["IdPreSalida"].HeaderText = "Folio";
            //grdDetPreSalida.Columns["IdPreSalida"].HeaderText = "Folio";

            grdDetPreSalida.Columns["Cantidad"].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
            grdDetPreSalida.Columns["idProducto"].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
            grdDetPreSalida.Columns["CCODIGOPRODUCTO"].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
            grdDetPreSalida.Columns["CNOMBREPRODUCTO"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

        }
        private void CargaAnexo20()
        {
            try
            {
                //FORMA DE PAGO
                resp = new CatSATSvc().getCatSATxFilter(0, "c.Tipo = 'FORPAG'", " c.idclave");
                if (resp.typeResult == ResultTypes.success)
                {
                    listFormaPago = (List<CatSAT>)resp.result;
                    cmbFormaPago.DataSource = null;
                    cmbFormaPago.DataSource = listFormaPago;
                    cmbFormaPago.DisplayMember = "Descripcion";
                    cmbFormaPago.SelectedIndex = 0;
                }
                //USO DE CFDI
                resp = new CatSATSvc().getCatSATxFilter(0, "c.Tipo = 'CFDI'", " c.idclave");
                if (resp.typeResult == ResultTypes.success)
                {
                    listCFDI = (List<CatSAT>)resp.result;
                    cmbUsoCFDI.DataSource = null;
                    cmbUsoCFDI.DataSource = listCFDI;
                    cmbUsoCFDI.DisplayMember = "Descripcion";
                    cmbUsoCFDI.SelectedIndex = 0;

                }

                //METODO DE PAGO
                resp = new CatSATSvc().getCatSATxFilter(0, "c.Tipo = 'METPAG'", " c.idclave");
                if (resp.typeResult == ResultTypes.success)
                {
                    listMetodoPago = (List<CatSAT>)resp.result;
                    cmbMetodoPago.DataSource = null;
                    cmbMetodoPago.DataSource = listMetodoPago;
                    cmbMetodoPago.DisplayMember = "Descripcion";
                    cmbMetodoPago.SelectedIndex = 0;

                }

            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + ex.Message,
this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            }
        }
        private void SeleccionacmbFormaPago(string clave)
        {
            try
            {
                int i = 0;
                foreach (CatSAT item in cmbFormaPago.Items)
                {
                    if (item.Clave == clave)
                    {
                        cmbFormaPago.SelectedIndex = i;
                        return;
                    }
                    else
                    {
                        i++;
                    }
                }
            }
            catch (Exception e)
            {

                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + e.Message,
                  this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void SeleccionacmbUsoCFDI(string clave)
        {
            try
            {
                int i = 0;
                foreach (CatSAT item in cmbUsoCFDI.Items)
                {
                    if (item.Clave == clave)
                    {
                        cmbUsoCFDI.SelectedIndex = i;
                        return;
                    }
                    else
                    {
                        i++;
                    }
                }
            }
            catch (Exception e)
            {

                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + e.Message,
                  this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private CabMovimientos MapeaCabMov()
        {
            try
            {
                return new CabMovimientos
                {
                    NoMovimiento = 0,
                    IdMovimiento = 0,
                    NoSerie = "",
                    TipoMovimiento = "FACOS",
                    CCODIGOC01_CLI = clienteCom.CCODIGOCLIENTE,
                    FechaMovimiento = DateTime.Now,
                    //SubTotalGrav = Convert.ToDecimal(Subtot),
                    SubTotalGrav = subtotal,
                    SubTotalExento = 0,
                    Retencion = 0,
                    //Iva = Convert.ToDecimal(txtTotIVA.Text),
                    Iva = impIva,
                    Estatus = "TER",
                    Descuento = 0,
                    CCODIGOA01 = "",
                    Usuario = _user.nombreUsuario,
                    IdEmpresa = 3,
                    UserAutoriza = "",
                    CIDDOCUM02 = 4,
                    FechaVencimiento = DateTime.Now,
                    FechaEntrega = DateTime.Now,
                    UsuarioCrea = _user.nombreUsuario,
                    FechaCrea = DateTime.Now,
                    UsuarioModifica = _user.nombreUsuario,
                    CIDMONEDA = 1,
                    CTIPOCAM01 = 1,
                    Vehiculo = txtTipoUnidad.Text,
                    Chofer = "",
                    CMETODOPAG = "",
                    CNUMCTAPAG = "",
                    CREFEREN01 = txtidOrdenSer.Text,
                    COBSERVA01 = "",
                    TimbraFactura = true,
                    EsExterno = false,
                    ReqAutDcto = false,
                    Kilometraje = 0,
                    CantTotal = 0,
                    FechaCancela = DateTime.Now,
                    FechaModifica = DateTime.Now,
                    UsuarioCancela = ""

                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + ex.Message,
                                 this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return null;
            }
        }


        private void AgregaListaDet(int NoMovimiento, int CIDUNIDA01, string CCODIGOP01_PRO, int Consecutivo,
        int IdMovimiento, string NoSerie, string TipoMovimiento, decimal Cantidad, decimal Precio, decimal PorcIVA,
        decimal Costo, decimal PorcDescto, decimal ImpDescto, int CIDALMACEN, decimal Existencia, int CIDUNIDA01BASE,
        decimal CantidadBASE, decimal tFactorUnidad, bool tNoEquivalente, bool EsListaPrecios, int CIDUNIDA02,
        decimal CantidadEQUI, string COBSERVA01, bool EsServicio, string CNOMBREPRODUCTO)
        {
            try
            {
                DetMovimientos detmov = new DetMovimientos
                {
                    NoMovimiento = NoMovimiento,
                    CIDUNIDA01 = CIDUNIDA01,
                    CCODIGOP01_PRO = CCODIGOP01_PRO,
                    Consecutivo = Consecutivo,
                    IdMovimiento = IdMovimiento,
                    NoSerie = NoSerie,
                    TipoMovimiento = TipoMovimiento,
                    Cantidad = Cantidad,
                    Precio = Precio,
                    PorcIVA = PorcIVA,
                    Costo = Costo,
                    PorcDescto = PorcDescto,
                    ImpDescto = ImpDescto,
                    CIDALMACEN = CIDALMACEN,
                    Existencia = Existencia,
                    CIDUNIDA01BASE = CIDUNIDA01BASE,
                    CantidadBASE = CantidadBASE,
                    tFactorUnidad = tFactorUnidad,
                    tNoEquivalente = tNoEquivalente,
                    EsListaPrecios = EsListaPrecios,
                    CIDUNIDA02 = CIDUNIDA02,
                    CantidadEQUI = CantidadEQUI,
                    COBSERVA01 = COBSERVA01,
                    EsServicio = EsServicio,
                    CNOMBREPRODUCTO = CNOMBREPRODUCTO,
                    Total = (Cantidad * Precio)
                };
                if (listDetMov == null)
                {
                    listDetMov = new List<DetMovimientos>();
                }
                listDetMov.Add(detmov);
                //grdDetalle.DataSource = null;
                //grdDetalle.DataSource = listDetMov;
                //FormatoGridEncabezados();
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + ex.Message,
                  this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
    }//
}
