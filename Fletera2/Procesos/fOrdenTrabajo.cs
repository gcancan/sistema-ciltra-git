﻿using Core.Models;
using Fletera2.Clases;
using Fletera2.Comunes;
using Fletera2Entidades;
using Fletera2Negocio;
//using Microsoft.VisualBasic.PowerPacks;
//using Microsoft.VisualBasic.PowerPacks;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Fletera2.Clases.Inicio;

namespace Fletera2.Procesos
{
    public partial class fOrdenTrabajo : Form
    {
        //Variables Referenciadas
        int _idOrdenSer;
        int _idOrdenTrabajo;
        Usuario _user;
        List<CatUnidadTrans> _listaUnidades;
        CatTipoOrdServ _TipoOrdServ;

        //Variables
        OperationResult resp;

        CatServicios servicio;
        CatActividades actividad;

        List<RelTipOrdServ> listaRelacion;
        List<CatTipoServicio> listaTipoServicios;
        List<CatServicios> listaServicios;
        List<CatActividades> listaActividades;
        List<CatLlantas> listaLlantas;
        List<CatLlantas> listaLlantasF;
        List<CatDivision> listaDivisiones;
        List<RelServicioActividad> listaRelServAct;

        int NoCabOS = 0;
        int ContOS = 0;
        int ContOT = 0;
        //CabOrdenServicio CabOS;
        //DetOrdenServicio DetOS;
        //DetRecepcionOS DetRecOS;
        public List<CabOrdenServicio> listaCabOS;
        public List<DetOrdenServicio> listaDetOS;
        public List<DetRecepcionOS> listaDetRecOS;

        List<CabOrdenServicio> _ValorDevuelve;

        string idUnidadTrans_Act = "";
        int idTipoServicio_Act = 0;
        CabOrdenServicio cabos;
        DetOrdenServicio detos;
        DetRecepcionOS detros;
        int ContModificando = 0;
        bool bExito;
        public List<DetOrdenServicio> listaDetOSCheck;

        //modificar o nuevo
        DetOrdenServicio detosG = new DetOrdenServicio();
        DetRecepcionOS detrosG = new DetRecepcionOS();

        int _idPersonalResp = 0;
        int _idPersonalAyu1 = 0;
        int _idPersonalAyu2 = 0;

        bool _ActivaAsigna;

        List<CatPersonal> listaResponsable;
        List<CatPersonal> listaAyudante1;
        List<CatPersonal> listaAyudante2;

        List<DetRecepcionOS> listaDetROS_Asigna;
        List<DetRecepcionOS> listaDetROS;

        AutoCompleteStringCollection coleccion = new AutoCompleteStringCollection();

        //LLantas
        CatTipoUniTrans tipouni;
        RadioButton[] rdbPosicion;
        RelEjeLlanta ejellanta;
        List<RelEjeLlanta> listaejellanta;
        CatLlantas llanta;
        List<CatLlantas> listallanta;
        int pos;
        CatEmpresas empresa;
        string NomEmpresa;
        string NomMarca;
        string NomDisenio;
        string NomTipLlanta;
        string TextoToolTip;
        CatLLantasProducto llantaProd;
        CatTipoLlanta tipollanta;
        ToolTip tt = new ToolTip();
        CatClasLlan clasllan;
        int entero = 0;

        public fOrdenTrabajo(Usuario user, List<CatUnidadTrans> listaUnidades, CatTipoOrdServ TipoOrdServ, bool ActivaAsigna,
        int NoOsfic = 0, int idOrdenSer = 0, int idOrdenTrabajo = 0, int idPersonalResp = 0, int idPersonalAyu1 = 0,
        int idPersonalAyu2 = 0)
        {
            _user = user;
            _listaUnidades = listaUnidades;
            _TipoOrdServ = TipoOrdServ;
            NoCabOS = NoOsfic;
            _idOrdenSer = idOrdenSer;
            _idOrdenTrabajo = idOrdenTrabajo;
            _idPersonalResp = idPersonalResp;
            _idPersonalAyu1 = idPersonalAyu1;
            _idPersonalAyu2 = idPersonalAyu2;

            _ActivaAsigna = ActivaAsigna;
            InitializeComponent();
        }

        private void fOrdenTrabajo_Load(object sender, EventArgs e)
        {
            CargaDatos();
            CargaTipoOrdenServicio();
            HabilitaBotones(opcHabilita.DesHabilitaTodos);
            HabilitaCampos(opcHabilita.DesHabilitaTodos);
            LimpiaCampos();
            FormatoGeneral(grdCab);
            FormatoGeneral(grdDet);
            if (_idOrdenSer > 0)
            {
                CargaForma(_idOrdenSer.ToString(), opcBusqueda.CabOrdenServicio);
                HabilitaBotones(opcHabilita.Editando);

            }
            else
            {
                grdCab.Visible = true;
                grdCab.Height = 92;
                grdCab.Width = 690;
                grdCab.Location = new Point(6, 19);


                lblopera1.Visible = false;
                lblopera2.Visible = false;
                lblopera3.Visible = false;

                cmbOpera1.Visible = false;
                cmbOpera2.Visible = false;
                cmbOpera3.Visible = false;

                cmbDetOpera1.Visible = false;
                cmbDetOpera2.Visible = false;
                cmbDetOpera3.Visible = false;

                btnAsigna.Visible = false;

                chkOpera2.Visible = false;
                chkOpera3.Visible = false;
            }

            gpoOs.Visible = _ActivaAsigna;
            if (_ActivaAsigna)
            {
                CargaASigna();
                btnAsigna.Enabled = true;
                //Asigna();
            }

            //PARA LLANTAS
            if (((eTipoOrdenServicio)_TipoOrdServ.idTipOrdServ) == eTipoOrdenServicio.LLANTAS)
            {
                GpoPosiciones.Visible = true;
                cmbPosicion.Visible = true;
                cmbidLlanta.Visible = true;
                //DibujaLinea(185, 25, 183, 300, GpoPosiciones);
                //
                this.Size = new Size(1169, 566);
                //LlenaComboLlantas(((CatUnidadTrans)cmbidUnidadTrans.SelectedItem).idUnidadTrans);
                LlenaComboLlantasF(((CatUnidadTrans)cmbidUnidadTrans.SelectedItem).idUnidadTrans);
                ParaLLantas();

            }
            else
            {
                this.Size = new Size(736, 566);
                GpoPosiciones.Visible = false;
                cmbPosicion.Visible = false;
                cmbidLlanta.Visible = false;

            }


            //PARA LLANTAS
        }


        private void HabilitaBotones(opcHabilita opc)
        {
            switch (opc)
            {
                case opcHabilita.DesHabilitaTodos:
                    btnNuevo.Enabled = true;
                    if (listaCabOS != null)
                    {
                        if (listaCabOS.Count > 0)
                        {
                            btnGrabar.Enabled = true;
                            btnCancelar.Enabled = true;
                            btnSalir.Enabled = false;
                        }
                        else
                        {
                            btnGrabar.Enabled = false;
                            btnCancelar.Enabled = false;
                            btnSalir.Enabled = true;
                        }
                    }
                    else
                    {
                        btnGrabar.Enabled = false;
                        btnCancelar.Enabled = false;
                        btnSalir.Enabled = true;
                    }


                    //btnCancelar.Enabled = false;

                    break;
                case opcHabilita.Iniciando:
                    break;
                case opcHabilita.Editando:
                    btnNuevo.Enabled = false;
                    btnGrabar.Enabled = true;
                    btnCancelar.Enabled = false;
                    btnSalir.Enabled = true;
                    break;
                case opcHabilita.Nuevo:
                    btnNuevo.Enabled = false;
                    btnGrabar.Enabled = true;
                    btnCancelar.Enabled = true;
                    btnSalir.Enabled = false;
                    break;
                case opcHabilita.Reporte:
                    break;
                default:
                    break;
            }
        }
        private void HabilitaCampos(opcHabilita opc)
        {
            switch (opc)
            {
                case opcHabilita.DesHabilitaTodos:
                    cmbidUnidadTrans.Enabled = false;
                    cmbOpciones.Enabled = false;
                    cmbPosicion.Enabled = false;
                    cmbidLlanta.Enabled = false;
                    //cmbidServicio.Enabled = false;
                    txtTodo.Enabled = false;
                    txtFallaReportada.Enabled = false;
                    cmbDivsiones.Enabled = false;

                    grdCab.Enabled = false;
                    grdDet.Enabled = false;
                    break;
                case opcHabilita.Iniciando:
                    cmbidUnidadTrans.Enabled = false;
                    cmbOpciones.Enabled = true;
                    //cmbPosicion.Enabled = false;
                    //cmbidLlanta.Enabled = false;
                    //cmbidServicio.Enabled = false;
                    if (((eTipoOrdenServicio)_TipoOrdServ.idTipOrdServ) == eTipoOrdenServicio.LLANTAS)
                    {
                        cmbPosicion.Enabled = true;
                        cmbidLlanta.Enabled = true;
                    }
                    else
                    {
                        cmbPosicion.Enabled = false;
                        cmbidLlanta.Enabled = false;

                    }
                    txtTodo.Enabled = false;
                    txtFallaReportada.Enabled = false;
                    cmbDivsiones.Enabled = false;


                    grdCab.Enabled = true;
                    grdDet.Enabled = true;


                    break;
                case opcHabilita.Editando:
                    cmbidUnidadTrans.Enabled = false;
                    cmbOpciones.Enabled = false;
                    if (((eTipoOrdenServicio)_TipoOrdServ.idTipOrdServ) == eTipoOrdenServicio.LLANTAS)
                    {
                        cmbPosicion.Enabled = true;
                        cmbidLlanta.Enabled = true;
                    }
                    else
                    {
                        cmbPosicion.Enabled = false;
                        cmbidLlanta.Enabled = false;

                    }
                    if ((eTipoServicio)((CatTipoServicio)cmbOpciones.SelectedItem).idTipoServicio == eTipoServicio.PREVENTIVO ||
                        (eTipoServicio)((CatTipoServicio)cmbOpciones.SelectedItem).idTipoServicio == eTipoServicio.RECARGA)
                    {
                        //cmbidServicio.Enabled = true;
                        txtTodo.Enabled = true;
                        txtFallaReportada.Enabled = false;
                    }
                    else
                    {
                        //cmbidServicio.Enabled = false;

                        if (((eTipoOrdenServicio)_TipoOrdServ.idTipOrdServ) == eTipoOrdenServicio.LLANTAS)
                        {
                            txtTodo.Enabled = true;
                            txtFallaReportada.Enabled = false;
                        }
                        else
                        {
                            txtTodo.Enabled = false;
                            txtFallaReportada.Enabled = true;
                        }

                    }
                    cmbDivsiones.Enabled = false;

                    grdCab.Enabled = true;
                    grdDet.Enabled = true;
                    break;
                case opcHabilita.Nuevo:
                    cmbidUnidadTrans.Enabled = true;
                    cmbOpciones.Enabled = false;
                    cmbPosicion.Enabled = false;
                    cmbidLlanta.Enabled = false;
                    //cmbidServicio.Enabled = false;
                    txtTodo.Enabled = false;
                    txtFallaReportada.Enabled = false;
                    cmbDivsiones.Enabled = false;

                    grdCab.Enabled = true;
                    grdDet.Enabled = true;
                    break;
                case opcHabilita.Reporte:
                    break;
                default:
                    break;
            }
        }

        private void CargaTipoOrdenServicio()
        {
            lbldivision.Visible = false;
            cmbDivsiones.Visible = false;
            if (((eTipoOrdenServicio)_TipoOrdServ.idTipOrdServ) == eTipoOrdenServicio.LLANTAS)
            {
                lblnollanta.Visible = true;
                lblposllanta.Visible = true;
                cmbidLlanta.Visible = true;
                cmbPosicion.Visible = true;
            }
            else

            {
                lblnollanta.Visible = false;
                lblposllanta.Visible = false;
                cmbidLlanta.Visible = false;
                cmbPosicion.Visible = false;
            }
        }
        private void CargaDatos()
        {
            try
            {
                if (_listaUnidades.Count > 0)
                {
                    lblTipoOS.Text = _TipoOrdServ.NomTipOrdServ;
                    CargaUnidades(_listaUnidades);
                    CargaOpciones(_TipoOrdServ.idTipOrdServ);
                    CargaDivisiones();

                    //"rel.idTipOrdServ = " & _idTipOrdServ
                }
                else
                {
                    //no se encontraron datos se mensaje y se cierra
                }
            }
            catch (Exception e)
            {

                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + e.Message,
this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            }

        }

        private void CargaUnidades(List<CatUnidadTrans> listaUni)
        {
            //cmbidUnidadTrans.DataSource = listaUni;
            cmbidUnidadTrans.DataSource = null;
            cmbidUnidadTrans.DataSource = listaUni;

            //cmbIdEmpresa.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            //cmbIdEmpresa.AutoCompleteSource = AutoCompleteSource.ListItems;
            cmbidUnidadTrans.DisplayMember = "idUnidadTrans";
            cmbidUnidadTrans.SelectedIndex = 0;

            //CargallantasxUnidad(listaUni[0].idUnidadTrans);
            LlenaComboLlantasF(listaUni[0].idUnidadTrans);


        }

        private void CargaOpciones(int idTipOrdServ)
        {

            //CatActividades actividad;
            CatTipoServicio serv;
            listaTipoServicios = new List<CatTipoServicio>();

            resp = new RelTipOrdServSvc().getRelTipOrdServxFilter(0, "c.idTipOrdServ = " + idTipOrdServ);
            if (resp.typeResult == ResultTypes.success)
            {
                listaRelacion = (List<RelTipOrdServ>)resp.result;

                foreach (var item in listaRelacion)
                {
                    if (item.NomCatalogo == eRelTipOrdServCat.CatTipoServicio.ToString())
                    {
                        resp = new CatTipoServicioSvc().getCatTipoServicioxFilter(item.IdRelacion);
                        if (resp.typeResult == ResultTypes.success)
                        {
                            serv = ((List<CatTipoServicio>)resp.result)[0];

                            listaTipoServicios.Add(serv);
                            //List<CatTipoServicio> listaServicios;
                        }
                        else
                        {
                            //que pasas
                        }
                    }
                    //else if (item.NomCatalogo == eRelTipOrdServCat.CatActividades.ToString())
                    //{
                    //    resp = new CatActividadesSvc().getCatActividadesxFilter(item.IdRelacion);
                    //    if (resp.typeResult == ResultTypes.success)
                    //    {
                    //        actividad = ((List<CatActividades>)resp.result)[0];

                    //        listaActividades.Add(actividad);
                    //        //List<CatTipoServicio> listaServicios;
                    //    }
                    //    else
                    //    {
                    //        //que pasas
                    //    }
                    //}
                }
                //Si hubieron Listas Agregar a commbo
                if (listaTipoServicios.Count > 0)
                {
                    cmbOpciones.DataSource = null;
                    cmbOpciones.DataSource = listaTipoServicios;
                    cmbOpciones.DisplayMember = "NomTipoServicio";
                    cmbOpciones.SelectedIndex = 0;
                }
                else
                {
                    cmbOpciones.DataSource = null;
                }


            }
            else
            {

            }
        }

        private void CargaServicios()
        {

            CatActividades actividad;
            CatServicios serv;

            listaServicios = new List<CatServicios>();
            listaActividades = new List<CatActividades>();

            foreach (var item in listaRelacion)
            {
                if (item.NomCatalogo == eRelTipOrdServCat.CatServicios.ToString())
                {
                    resp = new CatServiciosSvc().getCatServiciosxFilter(item.IdRelacion);
                    if (resp.typeResult == ResultTypes.success)
                    {

                        serv = ((List<CatServicios>)resp.result)[0];
                        listaServicios.Add(serv);
                        lblServicios.Text = "Servicios";
                    }
                    else
                    {

                    }
                    //List<CatServicios> listaServicios;
                }
                else if (item.NomCatalogo == eRelTipOrdServCat.CatActividades.ToString())
                {
                    resp = new CatActividadesSvc().getCatActividadesxFilter(item.IdRelacion);
                    if (resp.typeResult == ResultTypes.success)
                    {

                        lblServicios.Text = "Actividades";
                        actividad = ((List<CatActividades>)resp.result)[0];
                        listaActividades.Add(actividad);
                    }
                    else
                    {
                    }
                }
            }

            if (listaServicios.Count > 0)
            {
                foreach (var item in listaServicios)
                {
                    coleccion.Add(Convert.ToString(item.NomServicio));
                }

                txtTodo.AutoCompleteCustomSource = coleccion;
                txtTodo.AutoCompleteMode = AutoCompleteMode.Suggest;
                txtTodo.AutoCompleteSource = AutoCompleteSource.CustomSource;

                txtTodo.Text = listaServicios[0].NomServicio;
                //ConsultaElemento(txtTodo.Text);

                //cmbidServicio.DataSource = null;
                //cmbidServicio.DataSource = listaServicios;
                //cmbidServicio.DisplayMember = "NomServicio";
                //cmbidServicio.SelectedIndex = 0;
            }
            else if (listaActividades.Count > 0)
            {
                foreach (var item in listaActividades)
                {
                    coleccion.Add(Convert.ToString(item.NombreAct));
                }

                txtTodo.AutoCompleteCustomSource = coleccion;
                txtTodo.AutoCompleteMode = AutoCompleteMode.Suggest;
                txtTodo.AutoCompleteSource = AutoCompleteSource.CustomSource;

                txtTodo.Text = listaActividades[0].NombreAct;
                //cmbidServicio.DataSource = null;
                //cmbidServicio.DataSource = listaActividades;
                //cmbidServicio.DisplayMember = "NombreAct";
                //cmbidServicio.SelectedIndex = 0;

            }
            else
            {
                //cmbidServicio.DataSource = null;
            }
        }

        private void cmbOpciones_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cmbOpciones_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                idTipoServicio_Act = ((CatTipoServicio)cmbOpciones.SelectedItem).idTipoServicio;

                if ((eTipoServicio)((CatTipoServicio)cmbOpciones.SelectedItem).idTipoServicio == eTipoServicio.PREVENTIVO ||
                    (eTipoServicio)((CatTipoServicio)cmbOpciones.SelectedItem).idTipoServicio == eTipoServicio.RECARGA)
                {
                    //Se Carga Servicios y se aCtiva
                    CargaServicios();
                    txtTodo.Enabled = true;
                    txtFallaReportada.Enabled = false;
                    if (((eTipoOrdenServicio)_TipoOrdServ.idTipOrdServ) == eTipoOrdenServicio.LLANTAS)
                    {
                        cmbPosicion.Focus();
                    }
                    else
                    {
                        txtTodo.Focus();
                    }


                }
                else
                {
                    //cmbidServicio.Enabled = false;
                    txtTodo.Enabled = false;
                    txtFallaReportada.Enabled = true;
                    if (((eTipoOrdenServicio)_TipoOrdServ.idTipOrdServ) == eTipoOrdenServicio.LLANTAS)
                    {
                        CargaActividades();
                        txtTodo.Enabled = true;
                        txtFallaReportada.Enabled = false;
                        cmbPosicion.Focus();

                    }
                    else
                    {
                        txtFallaReportada.Focus();
                        txtFallaReportada.SelectAll();

                    }


                }
            }
        }

        //private void CargallantasxUnidad(string idLlanta)
        //{


        //    resp = new CatLlantasSvc().getCatLlantasxFilter(0, " c.idUbicacion = '" + idLlanta + "'");
        //    if (resp.typeResult == ResultTypes.success)
        //    {
        //        listaLlantas = (List<CatLlantas>)resp.result;
        //        cmbPosicion.DataSource = null;
        //        cmbPosicion.DataSource = listaLlantas;
        //        cmbPosicion.DisplayMember = "Posicion";
        //        cmbPosicion.SelectedIndex = 0;

        //        cmbidLlanta.DataSource = null;
        //        cmbidLlanta.DataSource = listaLlantas;
        //        cmbidLlanta.DisplayMember = "idLlanta";
        //        cmbidLlanta.SelectedIndex = 0;
        //    }
        //    else
        //    {
        //        cmbidLlanta.DataSource = null;
        //        cmbPosicion.DataSource = null;
        //    }
        //    //CargaLlantas();

        //}

        private void cmbidUnidadTrans_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                //CargallantasxUnidad(((CatUnidadTrans)cmbidUnidadTrans.SelectedItem).idUnidadTrans);
                LlenaComboLlantasF(((CatUnidadTrans)cmbidUnidadTrans.SelectedItem).idUnidadTrans);
                idUnidadTrans_Act = ((CatUnidadTrans)cmbidUnidadTrans.SelectedItem).idUnidadTrans;
                HabilitaCampos(opcHabilita.Iniciando);
                cmbOpciones.Focus();
            }
        }

        private void CargaDivisiones()
        {
            resp = new CatDivisionSvc().getCatDivisionxFilter(0, " c.idDivision > 0", " c.NomDivision ASC");
            if (resp.typeResult == ResultTypes.success)
            {
                listaDivisiones = (List<CatDivision>)resp.result;

                cmbDivsiones.DataSource = null;
                cmbDivsiones.DataSource = listaDivisiones;
                cmbDivsiones.DisplayMember = "NomDivision";
                cmbDivsiones.SelectedIndex = 0;

            }
            else
            {

            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            HabilitaBotones(opcHabilita.Nuevo);
            HabilitaCampos(opcHabilita.Nuevo);
            cmbidUnidadTrans.Focus();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void cmbidUnidadTrans_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (((eTipoOrdenServicio)_TipoOrdServ.idTipOrdServ) == eTipoOrdenServicio.LLANTAS)
            {
                //LlenaComboLlantas(((CatUnidadTrans)cmbidUnidadTrans.SelectedItem).idUnidadTrans);
                LlenaComboLlantasF(((CatUnidadTrans)cmbidUnidadTrans.SelectedItem).idUnidadTrans);
                ParaLLantas();

            }
        }

        private void LimpiaCampos()
        {
            if (cmbidUnidadTrans.Items.Count > 0)
            {
                cmbidUnidadTrans.SelectedIndex = 0;
            }
            if (cmbOpciones.Items.Count > 0)
            {
                cmbOpciones.SelectedIndex = 0;
            }
            if (cmbPosicion.Items.Count > 0)
            {
                cmbPosicion.SelectedIndex = 0;
            }
            if (cmbidLlanta.Items.Count > 0)
            {
                cmbidLlanta.SelectedIndex = 0;
            }
            //if (cmbidServicio.Items.Count > 0)
            //{
            //    cmbidServicio.SelectedIndex = 0;
            //}
            if (cmbDivsiones.Items.Count > 0)
            {
                cmbDivsiones.SelectedIndex = 0;
            }

            txtFallaReportada.Clear();



            //grdOT.Enabled = false;
            grdCab.DataSource = null;
            grdDet.DataSource = null;
            //grdDetRec.DataSource = null;

            listaCabOS = null;
            listaDetOS = null;
            listaDetRecOS = null;

        }





        private CabOrdenServicio MapeaCabOS(int ContOS)
        {
            try
            {
                return new CabOrdenServicio
                {
                    idOrdenSer = ContOS,
                    IdEmpresa = this.idEmpresa,
                    idUnidadTrans = ((CatUnidadTrans)cmbidUnidadTrans.SelectedItem).idUnidadTrans,
                    idTipoOrden = _TipoOrdServ.idTipOrdServ,
                    Kilometraje = 0,
                    Estatus = "PRE",
                    idEmpleadoAutoriza = 0,
                    fechaCaptura = DateTime.Now,
                    idTipOrdServ = _TipoOrdServ.idTipOrdServ,
                    idTipoServicio = ((CatTipoServicio)cmbOpciones.SelectedItem).idTipoServicio,
                    idPadreOrdSer = 0,
                    ClaveFicticia = ContOS,
                    NomTipoUnidad = ((CatUnidadTrans)cmbidUnidadTrans.SelectedItem).nomTipoUniTras,
                    NomTipOrdServ = _TipoOrdServ.NomTipOrdServ,
                    NomTipoServicio = ((CatTipoServicio)cmbOpciones.SelectedItem).NomTipoServicio
                };
            }
            catch (Exception e)
            {
                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + e.Message,
                 this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return null;
            }

        }
        private DateTime fechaDefault = DateTime.Now;
        private DetOrdenServicio MapeaDetOS(int ContOS, int ContOT, int vidServicio, int vidActividad,
        string vNotaRecepcion, string vFallaReportada, int vidPersonalResp, int vidPersonalAyu1,
        int vidPersonalAyu2, string vNomPersonalResp, string vNomPersonalAyu1, string vNomPersonalAyu2,
        string vEstatus, DateTime? vFechaAsig = null, string vUsuarioAsig = "", DateTime? vFechaTerminado = null,
        string vUsuarioTerminado = "", int vCIDPRODUCTO_SERV = 0, string idllanta = "", int Posicion = 0)
        {
            try
            {
                return new DetOrdenServicio
                {
                    idOrdenSer = ContOS,
                    idOrdenActividad = 0,
                    idServicio = vidServicio,
                    idActividad = vidActividad,
                    DuracionActHr = 0,
                    NoPersonal = 0,
                    id_proveedor = 0,
                    Estatus = vEstatus,
                    CostoManoObra = 0,
                    CostoProductos = 0,
                    idOrdenTrabajo = ContOT,
                    idDivision = 0,
                    idLlanta = idllanta,
                    PosLlanta = Posicion,
                    NotaDiagnostico = "",
                    FaltInsumo = false,
                    Pendiente = false,
                    idFamilia = 0,
                    NotaRecepcion = vNotaRecepcion,
                    isCancelado = false,
                    motivoCancelacion = "",
                    idPersonalResp = vidPersonalResp,
                    idPersonalAyu1 = vidPersonalAyu1,
                    idPersonalAyu2 = vidPersonalAyu2,
                    NotaRecepcionA = vFallaReportada,
                    FallaReportada = vFallaReportada,
                    TipoNotaRecepcionF = (vidActividad > 0 ? "ACTIVIDAD" : "FALLA REPORTADA"),
                    NotaRecepcionF = vNotaRecepcion,
                    NomPersonalResp = vNomPersonalResp,
                    NomPersonalAyu1 = vNomPersonalAyu1,
                    NomPersonalAyu2 = vNomPersonalAyu2,
                    FechaAsig = (vFechaAsig == null ? DateTime.Now : vFechaAsig.Value),
                    UsuarioAsig = vUsuarioAsig,
                    FechaTerminado = (vFechaTerminado == null ? DateTime.Now : (DateTime)vFechaAsig),
                    UsuarioTerminado = vUsuarioTerminado,
                    CIDPRODUCTO_SERV = vCIDPRODUCTO_SERV,
                    Cantidad_CIDPRODUCTO_SERV = 1,
                    Precio_CIDPRODUCTO_SERV = 0
                };
            }
            catch (Exception e)
            {

                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + e.Message,
                  this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return null;
            }

        }

        private DetRecepcionOS MapeaDetRecOS(int ContOS, int ContOT, string vNotaRecepcion)
        {
            try
            {
                return new DetRecepcionOS
                {
                    idOrdenTrabajo = ContOT,
                    idOrdenSer = ContOS,
                    idFamilia = 0,
                    NotaRecepcion = vNotaRecepcion,
                    isCancelado = false,
                    motivoCancelacion = "",
                    idDivision = 0,
                    idPersonalResp = 0,
                    idPersonalAyu1 = 0,
                    idPersonalAyu2 = 0,
                    NotaRecepcionA = vNotaRecepcion
                };
            }
            catch (Exception e)
            {

                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + e.Message,
                   this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return null;
            }

        }

        //private void cmbidServicio_KeyPress(object sender, KeyPressEventArgs e)
        //{
        //    if (e.KeyChar == Convert.ToChar(Keys.Enter))
        //    {
        //        if (_idOrdenSer > 0 && _idOrdenTrabajo > 0)
        //        //if (_idOrdenSer > 0 )
        //        {
        //            //**********************************
        //            listaDetOSCheck = new List<DetOrdenServicio>();
        //            //actividad = (CatActividades)cmbidServicio.SelectedItem;
        //            resp = new CatActividadesSvc().getCatActividadesxFilter(0, " c.NombreAct = '" + txtTodo.Text + "'");
        //            if (resp.typeResult == ResultTypes.success)
        //            {
        //                actividad = ((List<CatActividades>)resp.result)[0];
        //            }
        //            //verificar en la lista nuevas
        //            if (listaDetOS != null)
        //            {
        //                if (listaDetOS.Count > 0)
        //                {
        //                    foreach (var item in listaDetOS)
        //                    {
        //                        if (item.idActividad == actividad.idActividad && item.Estatus != "CAN")
        //                        {
        //                            MessageBox.Show("No se Puede agregar la actividad; " + actividad.NombreAct + " ya esta en la Orden de Trabajo: " + item.idOrdenTrabajo, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
        //                            //cmbidServicio.Focus();
        //                            txtTodo.Focus();
        //                            return;
        //                        }
        //                    }
        //                }
        //            }




        //            //Verificar si el servicio ya esta dado de alta en cualquiera de las ordenes de trabajo
        //            //aqui

        //            resp = new DetOrdenServicioSvc().getDetOrdenServicioxFilter(_idOrdenSer);
        //            if (resp.typeResult == ResultTypes.success)
        //            {
        //                listaDetOSCheck = (List<DetOrdenServicio>)resp.result;
        //                foreach (var item in listaDetOSCheck)
        //                {
        //                    if (item.idActividad == actividad.idActividad && item.Estatus != "CAN")
        //                    {
        //                        //No se puede Agregar por que ya esta esa actividad 
        //                        MessageBox.Show("No se Puede agregar la actividad; " + actividad.NombreAct + " ya esta en la Orden de Trabajo: " + item.idOrdenTrabajo, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
        //                        //cmbidServicio.Focus();
        //                        txtTodo.Focus();
        //                        return;
        //                    }
        //                }
        //            }
        //            //**********************************

        //            if (_idOrdenTrabajo == 0)
        //            {
        //                ContModificando++;
        //            }
        //            //AgregaOrdenTrabajoEspF(((CatActividades)cmbidServicio.SelectedItem).idActividad, eTipoElementoOrdenTrab.Actividad);
        //            AgregaOrdenTrabajoEspF(((CatActividades)txtTodo.Tag).idActividad, eTipoElementoOrdenTrab.Actividad);
        //            HabilitaModifica();
        //            HabilitaBotones(opcHabilita.Editando);
        //        }
        //        //else if (_idOrdenSer > 0 && _idOrdenTrabajo == 0)
        //        //{

        //        //}
        //        else
        //        {
        //            if (lblServicios.Text == "Actividades")
        //            {
        //                //AgregaOrdenTrabajo(((CatActividades)cmbidServicio.SelectedItem).idActividad.ToString(), eTipoElementoOrdenTrab.Actividad);
        //                AgregaOrdenTrabajo(((CatActividades)txtTodo.Tag).idActividad.ToString(), eTipoElementoOrdenTrab.Actividad);
        //            }
        //            else if (lblServicios.Text == "Servicios")
        //            {
        //                //AgregaOrdenTrabajo(((CatServicios)cmbidServicio.SelectedItem).idServicio.ToString(), eTipoElementoOrdenTrab.Servicio);
        //                AgregaOrdenTrabajo(((CatServicios)txtTodo.Tag).idServicio.ToString(), eTipoElementoOrdenTrab.Servicio);
        //            }
        //            HabilitaBotones(opcHabilita.DesHabilitaTodos);
        //            HabilitaCampos(opcHabilita.Editando);
        //        }
        //        //cmbidServicio.SelectedIndex = 0;
        //        //cmbidServicio.Focus();
        //        txtTodo.Focus();

        //    }
        //}

        private void cmbidServicio_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void txtFallaReportada_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtFallaReportada_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                AgregaOrdenTrabajo(txtFallaReportada.Text, eTipoElementoOrdenTrab.FallaReportada);
                HabilitaBotones(opcHabilita.DesHabilitaTodos);
                HabilitaCampos(opcHabilita.Editando);
                txtFallaReportada.Clear();
                txtFallaReportada.Focus();
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Cancelar();
        }

        private void Cancelar()
        {
            LimpiaCampos();
            HabilitaBotones(opcHabilita.DesHabilitaTodos);
            HabilitaCampos(opcHabilita.DesHabilitaTodos);
        }

        private void btnGrabar_Click(object sender, EventArgs e)
        {
            try
            {
                if (_idOrdenSer > 0 && _idOrdenTrabajo > 0)
                {

                    foreach (var item in listaDetOS)
                    {
                        detosG = item;
                        resp = new DetOrdenServicioSvc().GuardaDetOrdenServicio(ref detosG);
                        if (resp.typeResult == ResultTypes.success)
                        {
                            bExito = true;
                        }
                        else
                        {
                            bExito = false;
                            break;
                        }
                    }

                    foreach (var item in listaDetRecOS)
                    {
                        detrosG = item;
                        resp = new DetRecepcionOSSvc().GuardaDetRecepcionOS(ref detrosG);
                        if (resp.typeResult == ResultTypes.success)
                        {
                            bExito = true;
                        }
                        else
                        {
                            bExito = false;
                            break;
                        }
                    }

                    if (bExito)
                    {
                        MessageBox.Show("Se agregaron actividade(s) para la Orden de servicio: " + _idOrdenSer.ToString() + " Falla Reportada " + detos.FallaReportada, "Operación Exitosa", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        DialogResult = DialogResult.OK;
                        //this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Ocurrio un Error", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
                else if (_idOrdenSer > 0 && _idOrdenTrabajo == 0)
                {

                    //Mapear de nuevo
                    //Nuevo desde Terminar
                    foreach (var item in listaDetOS)
                    {
                        item.idOrdenSer = _idOrdenSer;
                        item.idOrdenTrabajo = 0;
                        item.Estatus = "ASIG";
                        item.FechaAsig = DateTime.Now;
                        item.UsuarioAsig = _user.nombreUsuario;
                        item.FechaTerminado = DateTime.Now;
                        item.UsuarioTerminado = "";
                        item.NotaDiagnostico = "";
                        item.CIDPRODUCTO_ACT = 0;

                        detosG = item;
                        resp = new DetOrdenServicioSvc().GuardaDetOrdenServicio(ref detosG);
                        if (resp.typeResult == ResultTypes.success)
                        {
                            bExito = true;
                            resp = new DetOrdenServicioSvc().GuardaDetOrdenServicio(ref detosG);
                            if (resp.typeResult == ResultTypes.success)
                            {
                                bExito = true;
                            }
                            else
                            {
                                bExito = false;
                            }


                        }
                        else
                        {
                            bExito = false;
                            break;
                        }
                    }
                    foreach (var item in listaDetRecOS)
                    {
                        item.idOrdenTrabajo = detosG.idOrdenTrabajo;
                        item.NotaRecepcion = detosG.FallaReportada;
                        item.idPersonalResp = _idPersonalResp;
                        item.idPersonalAyu1 = _idPersonalAyu1;
                        item.idOrdenSer = _idOrdenSer;
                        item.idPersonalAyu2 = _idPersonalAyu2;
                        item.isCancelado = false;
                        item.motivoCancelacion = "";
                        item.Inserta = true;

                        detrosG = item;
                        resp = new DetRecepcionOSSvc().GuardaDetRecepcionOS(ref detrosG);
                        if (resp.typeResult == ResultTypes.success)
                        {
                            bExito = true;
                        }
                        else
                        {
                            bExito = false;
                            break;
                        }
                    }

                    if (bExito)
                    {
                        if (_idOrdenTrabajo == 0)
                        {
                            MessageBox.Show("Se agregaron actividade(s) para la Orden de servicio: " + _idOrdenSer.ToString(), "Operación Exitosa", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            MessageBox.Show("Se agregaron actividade(s) para la Orden de servicio: " + _idOrdenSer.ToString() + " Falla Reportada " + detos.FallaReportada, "Operación Exitosa", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }


                        DialogResult = DialogResult.OK;
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Ocurrio un Error", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }

                }
                else
                {
                    //Enviar Listas a
                    _ValorDevuelve = listaCabOS;

                    if (_ValorDevuelve != null)
                    {
                        DialogResult = DialogResult.OK;
                        this.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + ex.Message,
                 this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        int idEmpresa = 1; 
        public List<CabOrdenServicio> DevuelveValor(int idEmpresa)
        {
            try
            {
                this.idEmpresa = idEmpresa;
                DialogResult dResult = ShowDialog();
                if (dResult == DialogResult.OK && _ValorDevuelve != null)
                {
                    return _ValorDevuelve;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + ex.Message,
                 this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return null;
            }
        }
        private void AgregaOrdenTrabajo(string Valor, eTipoElementoOrdenTrab opc)
        {
            try
            {
                if (listaCabOS == null)
                {
                    listaCabOS = new List<CabOrdenServicio>();
                }
                if (listaDetOS == null)
                {
                    listaDetOS = new List<DetOrdenServicio>();
                }
                if (listaDetRecOS == null)
                {
                    listaDetRecOS = new List<DetRecepcionOS>();
                }

                bool bNuevo = false;
                int contIndex = 0;
                //int contIndexDet = 0;
                //int contIndexDetRec = 0;

                if (listaCabOS.Count > 0)
                {
                    foreach (var item in listaCabOS)
                    {
                        if (item.idUnidadTrans == ((CatUnidadTrans)cmbidUnidadTrans.SelectedItem).idUnidadTrans)
                        {
                            if (item.idTipoServicio == ((CatTipoServicio)cmbOpciones.SelectedItem).idTipoServicio)
                            {
                                bNuevo = false;
                                NoCabOS = item.idOrdenSer;
                                if (opc == eTipoElementoOrdenTrab.Servicio)
                                {
                                    //Borrar 
                                    listaCabOS.RemoveAt(contIndex);
                                    grdCab.DataSource = null;
                                    grdCab.DataSource = listaCabOS;
                                    for (int i = 0; i < listaDetOS.Count;)
                                    {
                                        if (listaDetOS[i].idOrdenSer == NoCabOS)
                                        {
                                            listaDetOS.Remove(listaDetOS[i]);
                                            i = 0;
                                        }
                                        else
                                        {
                                            i++;
                                        }
                                    }

                                    grdDet.DataSource = null;
                                    grdDet.DataSource = listaDetOS;

                                    for (int i = 0; i < listaDetRecOS.Count;)
                                    {
                                        if (listaDetRecOS[i].idOrdenSer == NoCabOS)
                                        {
                                            listaDetRecOS.Remove(listaDetRecOS[i]);
                                            i = 0;
                                        }
                                        else
                                        {
                                            i++;
                                        }
                                    }

                                    //grdDetRec.DataSource = null;
                                    //grdDetRec.DataSource = listaDetRecOS;


                                }
                                contIndex = 0;
                                break;
                            }
                            else
                            {
                                bNuevo = true;

                            }

                        }
                        else
                        {
                            bNuevo = true;
                        }
                        contIndex++;
                    }
                    if (bNuevo)
                    {
                        ContOS++;
                        ContOT = 0;
                        listaCabOS.Add(MapeaCabOS(ContOS));
                    }
                    //if (idUnidadTrans_Act != ((CatUnidadTrans)cmbidUnidadTrans.SelectedItem).idUnidadTrans)
                    //{

                    //}
                }
                else
                {
                    if (NoCabOS > 0)
                    {
                        ContOS = NoCabOS;
                    }
                    bNuevo = true;
                    ContOS++;
                    ContOT = 0;
                    listaCabOS.Add(MapeaCabOS(ContOS));

                }



                switch (opc)
                {
                    case eTipoElementoOrdenTrab.Servicio:
                        resp = new CatServiciosSvc().getCatServiciosxFilter(Convert.ToInt32(Valor));
                        if (resp.typeResult == ResultTypes.success)
                        {
                            if (!bNuevo)
                            {
                                listaCabOS.Add(MapeaCabOS(NoCabOS));
                                ContOT = 0;
                            }

                            servicio = ((List<CatServicios>)resp.result)[0];

                            resp = new RelServicioActividadSvc().getRelServicioActividadxFilter(servicio.idServicio);
                            if (resp.typeResult == ResultTypes.success)
                            {
                                listaRelServAct = (List<RelServicioActividad>)resp.result;
                                foreach (var item in listaRelServAct)
                                {
                                    resp = new CatActividadesSvc().getCatActividadesxFilter(item.idActividad);
                                    if (resp.typeResult == ResultTypes.success)
                                    {
                                        ContOT++;
                                        actividad = ((List<CatActividades>)resp.result)[0];
                                        if (((eTipoOrdenServicio)_TipoOrdServ.idTipOrdServ) == eTipoOrdenServicio.LLANTAS)
                                        {
                                            listaDetOS.Add(MapeaDetOS(ContOS, ContOT, servicio.idServicio,
                                            actividad.idActividad, actividad.NombreAct, actividad.NombreAct, 0, 0, 0, "", "", "", "PRE", null, "", null, "", 0,
                                            ((CatLlantas)cmbidLlanta.SelectedItem).idLlanta,
                                             ((CatLlantas)cmbPosicion.SelectedItem).Posicion));
                                            listaDetRecOS.Add(MapeaDetRecOS(ContOS, ContOT, actividad.NombreAct));

                                        }
                                        else
                                        {
                                            listaDetOS.Add(MapeaDetOS(ContOS, ContOT, servicio.idServicio,
                                            actividad.idActividad, actividad.NombreAct, actividad.NombreAct, 0, 0, 0, "", "", "", "PRE", null, "", null, "", 0,
                                            "",
                                             0));
                                            listaDetRecOS.Add(MapeaDetRecOS(ContOS, ContOT, actividad.NombreAct));

                                        }

                                    }
                                    else
                                    {
                                        //no se encontro
                                    }
                                }
                            }
                            else
                            {
                                //solo servicios sin Actividades
                                ContOT++;
                                if (((eTipoOrdenServicio)_TipoOrdServ.idTipOrdServ) == eTipoOrdenServicio.LLANTAS)
                                {
                                    listaDetOS.Add(MapeaDetOS(ContOS, ContOT, servicio.idServicio, 0, servicio.Descripcion,
                                servicio.Descripcion, 0, 0, 0, "", "", "", "PRE", null, "", null, "", 0,
                                 ((CatLlantas)cmbidLlanta.SelectedItem).idLlanta,
                                ((CatLlantas)cmbPosicion.SelectedItem).Posicion));
                                    listaDetRecOS.Add(MapeaDetRecOS(ContOS, ContOT, servicio.Descripcion));

                                }
                                else
                                {
                                    listaDetOS.Add(MapeaDetOS(ContOS, ContOT, servicio.idServicio, 0, servicio.Descripcion,
                                    servicio.Descripcion, 0, 0, 0, "", "", "", "PRE", null, "", null, "", 0,
                                    "",
                                    0));
                                    listaDetRecOS.Add(MapeaDetRecOS(ContOS, ContOT, servicio.Descripcion));


                                }

                            }
                        }
                        else
                        {
                            //no se encontro el servicio
                        }
                        break;
                    case eTipoElementoOrdenTrab.Actividad:
                        resp = new CatActividadesSvc().getCatActividadesxFilter(Convert.ToInt32(Valor));
                        if (resp.typeResult == ResultTypes.success)
                        {
                            actividad = ((List<CatActividades>)resp.result)[0];
                            ContOT++;
                            if (((eTipoOrdenServicio)_TipoOrdServ.idTipOrdServ) == eTipoOrdenServicio.LLANTAS)
                            {
                                listaDetOS.Add(MapeaDetOS(ContOS, ContOT, 0, actividad.idActividad, actividad.NombreAct,
                            actividad.NombreAct, 0, 0, 0, "", "", "", "PRE", null, "", null, "", 0,
                             ((CatLlantas)cmbidLlanta.SelectedItem).idLlanta,
                             ((CatLlantas)cmbPosicion.SelectedItem).Posicion));
                                listaDetRecOS.Add(MapeaDetRecOS(ContOS, ContOT, actividad.NombreAct));

                            }
                            else
                            {
                                listaDetOS.Add(MapeaDetOS(ContOS, ContOT, 0, actividad.idActividad, actividad.NombreAct,
                            actividad.NombreAct, 0, 0, 0, "", "", "", "PRE", null, "", null, "", 0,
                             "", 0));
                                listaDetRecOS.Add(MapeaDetRecOS(ContOS, ContOT, actividad.NombreAct));

                            }
                        }


                        break;
                    case eTipoElementoOrdenTrab.FallaReportada:
                        ContOT++;
                        if (((eTipoOrdenServicio)_TipoOrdServ.idTipOrdServ) == eTipoOrdenServicio.LLANTAS)
                        {
                            listaDetOS.Add(MapeaDetOS(ContOS, ContOT, 0, 0, Valor, Valor, 0, 0, 0, "", "", "", "PRE", null, "", null, "", 0,
                             ((CatLlantas)cmbidLlanta.SelectedItem).idLlanta,
                             ((CatLlantas)cmbPosicion.SelectedItem).Posicion));
                            listaDetRecOS.Add(MapeaDetRecOS(ContOS, ContOT, Valor));

                        }
                        else
                        {
                            listaDetOS.Add(MapeaDetOS(ContOS, ContOT, 0, 0, Valor, Valor, 0, 0, 0, "", "", "", "PRE", null, "", null, "", 0, "", 0));

                            listaDetRecOS.Add(MapeaDetRecOS(ContOS, ContOT, Valor));

                        }

                        break;
                    default:
                        break;
                }

                grdCab.DataSource = null;
                grdCab.DataSource = listaCabOS;
                grdDet.DataSource = null;
                grdDet.DataSource = listaDetOS;
                //grdDetRec.DataSource = null;
                //grdDetRec.DataSource = listaDetRecOS;
                FormatogrdCab();
                FormatogrdDet();


            }
            catch (Exception e)
            {

                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + e.Message,
                   this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void FormatoGeneral(DataGridView grid)
        {
            //ESTILO GENERAL
            grid.RowsDefaultCellStyle.BackColor = Color.White;
            grid.AlternatingRowsDefaultCellStyle.BackColor = Color.AliceBlue;

            grid.RowsDefaultCellStyle.SelectionForeColor = Color.Yellow;
            grid.RowsDefaultCellStyle.SelectionBackColor = Color.Gray;
            grid.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            grid.DefaultCellStyle.Font = new Font("Berlin Sans FB", 12);
            grid.ColumnHeadersDefaultCellStyle.Font = new Font("Berlin Sans FB", 12);
            grid.ColumnHeadersDefaultCellStyle.BackColor = Color.Yellow;
            grid.ColumnHeadersDefaultCellStyle.ForeColor = Color.Red;
            grid.EnableHeadersVisualStyles = false;

            grid.ShowEditingIcon = false;
            //grid.EditMode = DataGridViewEditMode.EditOnF2;
            //grid.EditingControlShowing = 

        }

        private void FormatogrdCab()
        {
            //public int idOrdenSer { get; set; }
            //public string idUnidadTrans { get; set; }
            //public string NomTipoUnidad { get; set; }
            //public string NomTipOrdServ { get; set; }
            //public string NomTipoServicio { get; set; }

            //public int IdEmpresa { get; set; }
            //public string RazonSocial { get; set; }
            //public int idTipoOrden { get; set; }
            //public DateTime FechaRecepcion { get; set; }
            //public int idEmpleadoEntrega { get; set; }
            //public string NomEmpleadoEntrega { get; set; }
            //public string UsuarioRecepcion { get; set; }
            //public decimal Kilometraje { get; set; }
            //public string Estatus { get; set; }
            //public DateTime FechaDiagnostico { get; set; }
            //public DateTime FechaAsignado { get; set; }
            //public DateTime FechaTerminado { get; set; }
            //public DateTime FechaEntregado { get; set; }
            //public string UsuarioEntrega { get; set; }
            //public int idEmpleadoRecibe { get; set; }
            //public string NotaFinal { get; set; }
            //public string usuarioCan { get; set; }
            //public DateTime FechaCancela { get; set; }
            //public int idEmpleadoAutoriza { get; set; }
            //public DateTime fechaCaptura { get; set; }
            //public int idTipOrdServ { get; set; }
            //public int idTipoServicio { get; set; }
            //public int idPadreOrdSer { get; set; }
            //public int ClaveFicticia { get; set; }
            //public string NomEmpleadoAutoriza { get; set; }
            //public int idOperarador { get; set; }
            //public string NomOperador { get; set; }


            //OCULTAR COLUMNAS
            grdCab.Columns["IdEmpresa"].Visible = false;
            grdCab.Columns["RazonSocial"].Visible = false;
            grdCab.Columns["idTipoOrden"].Visible = false;

            grdCab.Columns["FechaRecepcion"].Visible = false;
            grdCab.Columns["idEmpleadoEntrega"].Visible = false;
            grdCab.Columns["NomEmpleadoEntrega"].Visible = false;
            grdCab.Columns["UsuarioRecepcion"].Visible = false;
            grdCab.Columns["Kilometraje"].Visible = false;
            grdCab.Columns["Estatus"].Visible = false;
            grdCab.Columns["FechaDiagnostico"].Visible = false;
            grdCab.Columns["FechaAsignado"].Visible = false;
            grdCab.Columns["FechaTerminado"].Visible = false;
            grdCab.Columns["FechaEntregado"].Visible = false;
            grdCab.Columns["UsuarioEntrega"].Visible = false;
            grdCab.Columns["idEmpleadoRecibe"].Visible = false;
            grdCab.Columns["NotaFinal"].Visible = false;
            grdCab.Columns["usuarioCan"].Visible = false;
            grdCab.Columns["FechaCancela"].Visible = false;
            grdCab.Columns["idEmpleadoAutoriza"].Visible = false;
            grdCab.Columns["fechaCaptura"].Visible = false;
            grdCab.Columns["idTipOrdServ"].Visible = false;
            grdCab.Columns["idTipoServicio"].Visible = false;
            grdCab.Columns["idPadreOrdSer"].Visible = false;
            grdCab.Columns["ClaveFicticia"].Visible = false;
            grdCab.Columns["NomEmpleadoAutoriza"].Visible = false;
            grdCab.Columns["idOperarador"].Visible = false;
            grdCab.Columns["NomOperador"].Visible = false;
            //grdCab.Columns["xx"].Visible = false;
            //grdCab.Columns["xx"].Visible = false;

            //grdCab.Columns["usuarioCan"].Visible = false;
            //HACER READONLY COLUMNAS
            //grdCab.Columns["Seleccionado"].ReadOnly = true;

            //ENCABEZADO DE COLUMNAS
            grdCab.Columns["idOrdenSer"].HeaderText = "No.";
            grdCab.Columns["idUnidadTrans"].HeaderText = "Unidad";
            grdCab.Columns["NomTipoUnidad"].HeaderText = "Tipo Unidad";
            grdCab.Columns["NomTipoServicio"].HeaderText = "Tipo Servicio";
            grdCab.Columns["NomTipOrdServ"].HeaderText = "Tipo de Orden";






            //grdCab.Columns["idRemolque2"].HeaderText = "Remolque2";
            //grdCab.Columns["FechaHoraFin"].HeaderText = "Fecha Fin";
            //grdCab.Columns["EstatusGuia"].HeaderText = "Estatus";
            //grdCab.Columns["TipoViaje"].HeaderText = "Tipo Viaje";

            //FORMATO DE COLUMNAS
            //grdDatos.Columns["Precio"].DefaultCellStyle.Format = "C2";

            //ALINEACION DE COLUMNAS
            //grdDatos.Columns["Precio"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight;

            //TAMAÑO AUTOMATICO DE COLUMNAS
            grdCab.Columns["idOrdenSer"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdCab.Columns["idUnidadTrans"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdCab.Columns["NomTipoUnidad"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdCab.Columns["NomTipoServicio"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdCab.Columns["NomTipOrdServ"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            //grdCab.Columns["idTipOrdServ"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            //grdCab.Columns["idTipoServicio"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

            //grdCab.Columns["idRemolque2"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            //grdCab.Columns["FechaHoraFin"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            //grdCab.Columns["EstatusGuia"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            //grdCab.Columns["TipoViaje"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

        }
        private void FormatogrdDet()
        {
            //public int idOrdenTrabajo { get; set; }
            //public string NotaRecepcionF { get; set; }
            //public string TipoNotaRecepcionF { get; set; }

            //public bool Seleccionado { get; set; }
            //public int idOrdenSer { get; set; }
            //public int idOrdenActividad { get; set; }
            //public int idServicio { get; set; }
            //public int idActividad { get; set; }
            //public decimal DuracionActHr { get; set; }
            //public int NoPersonal { get; set; }
            //public int id_proveedor { get; set; }
            //public string Estatus { get; set; }
            //public decimal CostoManoObra { get; set; }
            //public decimal CostoProductos { get; set; }
            //public DateTime FechaDiag { get; set; }
            //public string UsuarioDiag { get; set; }
            //public DateTime FechaAsig { get; set; }
            //public string UsuarioAsig { get; set; }
            //public DateTime FechaPausado { get; set; }
            //public string NotaPausado { get; set; }
            //public DateTime FechaTerminado { get; set; }
            //public string UsuarioTerminado { get; set; }
            //public int idDivision { get; set; }
            //public string idLlanta { get; set; }
            //public int PosLlanta { get; set; }
            //public string NotaDiagnostico { get; set; }
            //public bool FaltInsumo { get; set; }
            //public bool Pendiente { get; set; }
            //public int CIDPRODUCTO_SERV { get; set; }
            //public decimal Cantidad_CIDPRODUCTO_SERV { get; set; }
            //public decimal Precio_CIDPRODUCTO_SERV { get; set; }
            //public int idFamilia { get; set; }
            //public string NotaRecepcion { get; set; }
            //public bool isCancelado { get; set; }
            //public string motivoCancelacion { get; set; }
            //public int idPersonalResp { get; set; }
            //public int idPersonalAyu1 { get; set; }
            //public int idPersonalAyu2 { get; set; }
            //public string NotaRecepcionA { get; set; }
            //public string FallaReportada { get; set; }
            //public string NomServicio { get; set; }
            //public string NomPersonalResp { get; set; }
            //public string NomPersonalAyu1 { get; set; }
            //public string NomPersonalAyu2 { get; set; }

            //OCULTAR COLUMNAS
            grdDet.Columns["Seleccionado"].Visible = false;
            grdDet.Columns["idOrdenSer"].Visible = false;
            grdDet.Columns["idOrdenActividad"].Visible = false;
            grdDet.Columns["idServicio"].Visible = false;
            grdDet.Columns["idActividad"].Visible = false;
            grdDet.Columns["DuracionActHr"].Visible = false;
            grdDet.Columns["NoPersonal"].Visible = false;
            grdDet.Columns["id_proveedor"].Visible = false;
            grdDet.Columns["Estatus"].Visible = false;
            grdDet.Columns["CostoManoObra"].Visible = false;
            grdDet.Columns["CostoProductos"].Visible = false;
            grdDet.Columns["FechaDiag"].Visible = false;
            grdDet.Columns["UsuarioDiag"].Visible = false;
            grdDet.Columns["FechaAsig"].Visible = false;
            grdDet.Columns["UsuarioAsig"].Visible = false;
            grdDet.Columns["FechaPausado"].Visible = false;
            grdDet.Columns["NotaPausado"].Visible = false;
            grdDet.Columns["FechaTerminado"].Visible = false;
            grdDet.Columns["UsuarioTerminado"].Visible = false;
            grdDet.Columns["idDivision"].Visible = false;
            if (((eTipoOrdenServicio)_TipoOrdServ.idTipOrdServ) != eTipoOrdenServicio.LLANTAS)
            {
                grdDet.Columns["idLlanta"].Visible = false;
                grdDet.Columns["PosLlanta"].Visible = false;
            }
            grdDet.Columns["NotaDiagnostico"].Visible = false;
            grdDet.Columns["FaltInsumo"].Visible = false;
            grdDet.Columns["Pendiente"].Visible = false;
            grdDet.Columns["CIDPRODUCTO_SERV"].Visible = false;
            grdDet.Columns["Cantidad_CIDPRODUCTO_SERV"].Visible = false;
            grdDet.Columns["Precio_CIDPRODUCTO_SERV"].Visible = false;
            grdDet.Columns["idFamilia"].Visible = false;
            grdDet.Columns["NotaRecepcion"].Visible = false;
            grdDet.Columns["isCancelado"].Visible = false;
            grdDet.Columns["motivoCancelacion"].Visible = false;
            grdDet.Columns["idPersonalResp"].Visible = false;
            grdDet.Columns["idPersonalAyu1"].Visible = false;
            grdDet.Columns["idPersonalAyu2"].Visible = false;
            grdDet.Columns["NotaRecepcionA"].Visible = false;
            grdDet.Columns["FallaReportada"].Visible = false;
            grdDet.Columns["NomServicio"].Visible = false;
            grdDet.Columns["NomPersonalResp"].Visible = false;
            grdDet.Columns["NomPersonalAyu1"].Visible = false;
            grdDet.Columns["NomPersonalAyu2"].Visible = false;
            grdDet.Columns["CIDPRODUCTO_ACT"].Visible = false;


            grdDet.Columns["DetRec"].Visible = false;
            //grdDet.Columns["XX"].Visible = false;
            //grdDet.Columns["XX"].Visible = false;


            //HACER READONLY COLUMNAS
            //grdDet.Columns["Seleccionado"].ReadOnly = true;

            //ENCABEZADO DE COLUMNAS
            grdDet.Columns["idOrdenTrabajo"].HeaderText = "No.";
            grdDet.Columns["NotaRecepcionF"].HeaderText = "Descripcion";
            grdDet.Columns["TipoNotaRecepcionF"].HeaderText = "Tipo";
            if (((eTipoOrdenServicio)_TipoOrdServ.idTipOrdServ) == eTipoOrdenServicio.LLANTAS)
            {
                grdDet.Columns["idLlanta"].HeaderText = "Llanta";
                grdDet.Columns["PosLlanta"].HeaderText = "Posición";

            }

            //FORMATO DE COLUMNAS
            //grdDatos.Columns["Precio"].DefaultCellStyle.Format = "C2";

            //ALINEACION DE COLUMNAS
            //grdDatos.Columns["Precio"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight;

            //TAMAÑO AUTOMATICO DE COLUMNAS
            grdDet.Columns["idOrdenTrabajo"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdDet.Columns["NotaRecepcionF"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdDet.Columns["TipoNotaRecepcionF"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdDet.Columns["idLlanta"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdDet.Columns["PosLlanta"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;


            grdDet.Columns["idOrdenTrabajo"].DisplayIndex = 0;
            grdDet.Columns["NotaRecepcionF"].DisplayIndex = 1;
            grdDet.Columns["TipoNotaRecepcionF"].DisplayIndex = 2;
            grdDet.Columns["idLlanta"].DisplayIndex = 3;
            grdDet.Columns["PosLlanta"].DisplayIndex = 4;

            //grdDet.Columns["FechaHoraFin"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            //grdDet.Columns["TipoViaje"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            //grdDet.Columns["VolDescarga"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            //grdDet.Columns["idCargaGasolina"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            //grdDet.Columns["precioOperador"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            //grdDet.Columns["nombreOrigen"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            //grdDet.Columns["Destino"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            //grdDet.Columns["Producto"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
        }

        private void CargaForma(string Valor, opcBusqueda opc)
        {
            if (opc == opcBusqueda.CabOrdenServicio)
            {
                resp = new CabOrdenServicioSvc().getCabOrdenServicioxFilter(_idOrdenSer);
                if (resp.typeResult == ResultTypes.success)
                {
                    cabos = ((List<CabOrdenServicio>)resp.result)[0];

                    listaCabOS = new List<CabOrdenServicio>();
                    listaCabOS.Add(cabos);

                    resp = new DetOrdenServicioSvc().getDetOrdenServicioxFilter(_idOrdenSer, " d.idOrdenTrabajo = " + _idOrdenTrabajo);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        detos = ((List<DetOrdenServicio>)resp.result)[0];
                        listaDetOS = new List<DetOrdenServicio>();
                        listaDetOS.Add(detos);

                        grdDet.DataSource = null;
                        grdDet.DataSource = listaDetOS;
                        FormatoGridDetMod();

                        resp = new DetRecepcionOSSvc().getDetOrdenServicioxFilter(_idOrdenSer, " d.idOrdenTrabajo = " + _idOrdenTrabajo);
                        if (resp.typeResult == ResultTypes.success)
                        {
                            listaDetRecOS = new List<DetRecepcionOS>();
                            detros = ((List<DetRecepcionOS>)resp.result)[0];
                            listaDetRecOS.Add(detros);
                        }

                        this.Text = this.Text + " Modificando: Orden Servicio: " + detos.idOrdenSer.ToString() + " - Orden de Trabajo: " + detos.idOrdenTrabajo.ToString();
                        SeleccionacmbOpciones(cabos.idTipoServicio);
                        //**************************************************
                        listaActividades = new List<CatActividades>();

                        //"c.idTipOrdServ = " + idTipOrdServ
                        //idActividad IN (SELECT IdRelacion FROM dbo.RelTipOrdServ WHERE idTipOrdServ = 2 AND NomCatalogo = 'CatActividades')
                        resp = new CatActividadesSvc().getCatActividadesxFilter(0,
                        " idActividad IN (SELECT IdRelacion FROM dbo.RelTipOrdServ WHERE idTipOrdServ = " + _TipoOrdServ.idTipOrdServ + " AND NomCatalogo = 'CatActividades')");
                        if (resp.typeResult == ResultTypes.success)
                        {
                            listaActividades = (List<CatActividades>)resp.result;

                            if (listaActividades != null)
                            {
                                if (listaActividades.Count > 0)
                                {
                                    foreach (var item in listaActividades)
                                    {
                                        coleccion.Add(Convert.ToString(item.NombreAct));
                                    }


                                    lblServicios.Text = "Actividades";
                                    //cmbidServicio.DataSource = null;
                                    //cmbidServicio.DataSource = listaActividades;
                                    //cmbidServicio.DisplayMember = "NombreAct";
                                    //cmbidServicio.SelectedIndex = 0;
                                    txtTodo.AutoCompleteCustomSource = coleccion;
                                    txtTodo.AutoCompleteMode = AutoCompleteMode.Suggest;
                                    txtTodo.AutoCompleteSource = AutoCompleteSource.CustomSource;
                                }

                            }


                            //**************************************************

                            //idTipoServicio_Act = ((CatTipoServicio)cmbOpciones.SelectedItem).idTipoServicio;


                            HabilitaModifica();
                            txtFallaReportada.Text = detos.FallaReportada;

                            //cmbidServicio.Focus();
                            txtTodo.Focus();
                        }

                        //foreach (var item in listaRelacion)
                        //{
                        //    if (item.NomCatalogo == "CatActividades")
                        //    {
                        //        resp = new CatActividadesSvc().getCatActividadesxFilter(item.IdRelacion, "", " c.NombreAct");
                        //        if (resp.typeResult == ResultTypes.success)
                        //        {
                        //            actividad = ((List<CatActividades>)resp.result)[0];
                        //            listaActividades.Add(actividad);
                        //        }
                        //    }
                        //}

                    }
                    else
                    {
                        if (_idOrdenTrabajo == 0)
                        {
                            //**************************************************
                            listaActividades = new List<CatActividades>();
                            foreach (var item in listaRelacion)
                            {
                                if (item.NomCatalogo == "CatActividades")
                                {
                                    resp = new CatActividadesSvc().getCatActividadesxFilter(item.IdRelacion);
                                    if (resp.typeResult == ResultTypes.success)
                                    {
                                        actividad = ((List<CatActividades>)resp.result)[0];
                                        listaActividades.Add(actividad);
                                    }
                                }
                            }
                            if (listaActividades != null)
                            {
                                if (listaActividades.Count > 0)
                                {
                                    foreach (var item in listaActividades)
                                    {
                                        coleccion.Add(Convert.ToString(item.NombreAct));
                                    }
                                    lblServicios.Text = "Actividades";
                                    //cmbidServicio.DataSource = null;
                                    //cmbidServicio.DataSource = listaActividades;
                                    //cmbidServicio.DisplayMember = "NombreAct";
                                    //cmbidServicio.SelectedIndex = 0;

                                    txtTodo.AutoCompleteCustomSource = coleccion;
                                    txtTodo.AutoCompleteMode = AutoCompleteMode.Suggest;
                                    txtTodo.AutoCompleteSource = AutoCompleteSource.CustomSource;
                                }

                            }
                            //**************************************************
                            HabilitaModifica();
                            //cmbidServicio.Focus();
                            txtTodo.Focus();
                        }
                        //No existe Orden de Trabajo
                    }
                }
                else
                {
                    //No existe Orden de Servicio
                }
            }
        }

        private void SeleccionacmbOpciones(int idTipoServicio)
        {
            try
            {
                int i = 0;
                foreach (CatTipoServicio item in cmbOpciones.Items)
                {
                    if (item.idTipoServicio == idTipoServicio)
                    {
                        cmbOpciones.SelectedIndex = i;
                        return;
                    }
                    else
                    {
                        i++;
                    }
                }
            }
            catch (Exception e)
            {

                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + e.Message,
                  this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void HabilitaModifica()
        {
            cmbOpciones.Enabled = false;
            //cmbidServicio.Enabled = true;
            txtTodo.Enabled = true;
            txtFallaReportada.Enabled = false;
            grdCab.Visible = false;
            grdDet.Enabled = true;


            lblopera1.Visible = true;
            lblopera2.Visible = true;
            lblopera3.Visible = true;

            cmbOpera1.Visible = true;
            cmbOpera2.Visible = true;
            cmbOpera3.Visible = true;

            cmbDetOpera1.Visible = true;
            cmbDetOpera2.Visible = true;
            cmbDetOpera3.Visible = true;

            btnAsigna.Visible = true;

            chkOpera2.Visible = true;
            chkOpera3.Visible = true;
        }




        private void AgregaOrdenTrabajoEspF(int Valor, eTipoElementoOrdenTrab opc)
        {

            resp = new CatActividadesSvc().getCatActividadesxFilter(Valor);
            if (resp.typeResult == ResultTypes.success)
            {
                actividad = ((List<CatActividades>)resp.result)[0];

                if (ContModificando > 0)
                {
                    //AGREGA NUEVO
                    if (listaDetOS == null)
                    {
                        listaDetOS = new List<DetOrdenServicio>();
                    }



                    listaDetOS.Add(MapeaDetOS(cabos.idOrdenSer, 0, 0, actividad.idActividad, actividad.NombreAct,
                        txtFallaReportada.Text,
                        (listaDetRecOS[0] != null ? listaDetRecOS[0].idPersonalResp : 0),
                        (listaDetRecOS[0] != null ? listaDetRecOS[0].idPersonalAyu1 : 0),
                        (listaDetRecOS[0] != null ? listaDetRecOS[0].idPersonalAyu2 : 0),
                        (listaDetRecOS[0] != null ? listaDetRecOS[0].NomPersonalResp : ""),
                        (listaDetRecOS[0] != null ? listaDetRecOS[0].NomPersonalAyu1 : ""),
                        (listaDetRecOS[0] != null ? listaDetRecOS[0].NomPersonalAyu2 : ""),
                        "ASIG",
                        (listaDetOS[0] != null ? listaDetOS[0].FechaAsig : DateTime.Now),
                        (listaDetOS[0] != null ? listaDetOS[0].UsuarioAsig : ""),
                        (listaDetOS[0] != null ? listaDetOS[0].FechaTerminado : DateTime.Now),
                        (listaDetOS[0] != null ? listaDetOS[0].UsuarioTerminado : ""),
                        actividad.CIDPRODUCTO,
                         ((CatLlantas)cmbidLlanta.SelectedItem).idLlanta,
                         ((CatLlantas)cmbPosicion.SelectedItem).Posicion));


                    if (listaDetRecOS == null)
                    {
                        listaDetRecOS = new List<DetRecepcionOS>();
                    }
                    listaDetRecOS.Add(MapeaDetRecOS(ContOS, ContOT, actividad.NombreAct));

                }
                else
                {
                    //MODIFICA
                    resp = new CatActividadesSvc().getCatActividadesxFilter(Valor);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        actividad = ((List<CatActividades>)resp.result)[0];
                        ContModificando++;
                        listaDetOS[0].idActividad = actividad.idActividad;
                        listaDetOS[0].NotaRecepcion = actividad.NombreAct;
                        listaDetOS[0].NotaRecepcionF = actividad.NombreAct;
                        listaDetOS[0].TipoNotaRecepcionF = "ACTIVIDAD";

                        listaDetRecOS[0].NotaRecepcion = actividad.NombreAct;


                    }

                }
                grdDet.DataSource = null;
                grdDet.DataSource = listaDetOS;
                FormatoGridDetMod();
            }

        }

        private void FormatoGridDetMod()
        {
            //OCULTAR COLUMNAS
            grdDet.Columns["Seleccionado"].Visible = false;
            grdDet.Columns["idOrdenActividad"].Visible = false;
            grdDet.Columns["idServicio"].Visible = false;
            grdDet.Columns["idActividad"].Visible = false;
            grdDet.Columns["DuracionActHr"].Visible = false;
            grdDet.Columns["NoPersonal"].Visible = false;
            grdDet.Columns["id_proveedor"].Visible = false;
            grdDet.Columns["Estatus"].Visible = false;
            grdDet.Columns["CostoManoObra"].Visible = false;
            grdDet.Columns["CostoProductos"].Visible = false;
            grdDet.Columns["FechaDiag"].Visible = false;
            grdDet.Columns["UsuarioDiag"].Visible = false;
            grdDet.Columns["FechaAsig"].Visible = false;
            grdDet.Columns["UsuarioAsig"].Visible = false;
            grdDet.Columns["FechaPausado"].Visible = false;
            grdDet.Columns["NotaPausado"].Visible = false;
            grdDet.Columns["FechaTerminado"].Visible = false;
            grdDet.Columns["UsuarioTerminado"].Visible = false;
            grdDet.Columns["idDivision"].Visible = false;
            grdDet.Columns["idLlanta"].Visible = false;
            grdDet.Columns["PosLlanta"].Visible = false;
            grdDet.Columns["NotaDiagnostico"].Visible = false;
            grdDet.Columns["FaltInsumo"].Visible = false;
            grdDet.Columns["Pendiente"].Visible = false;
            grdDet.Columns["CIDPRODUCTO_SERV"].Visible = false;
            grdDet.Columns["Cantidad_CIDPRODUCTO_SERV"].Visible = false;
            grdDet.Columns["Precio_CIDPRODUCTO_SERV"].Visible = false;
            grdDet.Columns["idFamilia"].Visible = false;
            grdDet.Columns["NotaRecepcion"].Visible = false;
            grdDet.Columns["isCancelado"].Visible = false;
            grdDet.Columns["motivoCancelacion"].Visible = false;
            grdDet.Columns["idPersonalResp"].Visible = false;
            grdDet.Columns["idPersonalAyu1"].Visible = false;
            grdDet.Columns["idPersonalAyu2"].Visible = false;
            grdDet.Columns["NotaRecepcionA"].Visible = false;
            grdDet.Columns["NomServicio"].Visible = false;
            grdDet.Columns["CIDPRODUCTO_ACT"].Visible = false;




            //ENCABEZADOS
            grdDet.Columns["idOrdenSer"].HeaderText = "Orden Servicio";
            grdDet.Columns["idOrdenTrabajo"].HeaderText = "Orden de Trabajo";
            grdDet.Columns["NotaRecepcionF"].HeaderText = "Descripcion Orden Trabajo";
            grdDet.Columns["FallaReportada"].HeaderText = "Falla Reporada";
            grdDet.Columns["TipoNotaRecepcionF"].HeaderText = "Tipo";
            grdDet.Columns["NomPersonalResp"].HeaderText = "Responsable";
            grdDet.Columns["NomPersonalAyu1"].HeaderText = "Ayudante 1";
            grdDet.Columns["NomPersonalAyu2"].HeaderText = "Ayudante 2";



            //TAMAÑO AUTOMATICO DE COLUMNAS
            grdDet.Columns["idOrdenSer"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdDet.Columns["idOrdenTrabajo"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdDet.Columns["NotaRecepcionF"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdDet.Columns["FallaReportada"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdDet.Columns["TipoNotaRecepcionF"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdDet.Columns["NomPersonalResp"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdDet.Columns["NomPersonalAyu1"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdDet.Columns["NomPersonalAyu2"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
        }

        private void CargaASigna()
        {
            listaResponsable = LlenaComboAsigna(cmbOpera1, cmbDetOpera1);
            listaAyudante1 = LlenaComboAsigna(cmbOpera2, cmbDetOpera2);
            listaAyudante2 = LlenaComboAsigna(cmbOpera3, cmbDetOpera3);
        }
        private List<CatPersonal> LlenaComboAsigna(ComboBox control, ComboBox controlDet)
        {
            try
            {
                List<CatPersonal> Lista;


                //SE PUEDE ASIGNAR VARIAS VECES - NO BORRAR POSTERIOR CANDADO
                //    string filtroEsp = " AND  NOT per.idPersonal IN ( " +
                //"SELECT DISTINCT dros.idPersonalResp AS idPersonal " +
                //"FROM dbo.DetRecepcionOS dros " +
                //"INNER JOIN dbo.CabOrdenServicio cab ON cab.idOrdenSer = dros.idOrdenSer " +
                //"WHERE cab.Estatus IN  ('ASIG','DIAG') AND ISNULL(dros.idPersonalResp,0) > 0 " +
                //"UNION " +
                //"SELECT DISTINCT dros.idPersonalAyu1 AS idPersonal " +
                //"FROM dbo.DetRecepcionOS dros " +
                //"INNER JOIN dbo.CabOrdenServicio cab ON cab.idOrdenSer = dros.idOrdenSer " +
                //"WHERE cab.Estatus IN  ('ASIG','DIAG') AND ISNULL(dros.idPersonalAyu1,0) > 0 " +
                //"union " +
                //"SELECT DISTINCT dros.idPersonalAyu2 AS idPersonal " +
                //"FROM dbo.DetRecepcionOS dros " +
                //"INNER JOIN dbo.CabOrdenServicio cab ON cab.idOrdenSer = dros.idOrdenSer " +
                //"WHERE cab.Estatus IN  ('ASIG','DIAG') AND ISNULL(dros.idPersonalAyu2,0) > 0)";

                //resp = new CatPersonalSvc().getCatPersonalxFiltro(" per.idDepto IN (SELECT DISTINCT idDepto FROM dbo.RelPersonalRolOrdServ WHERE idTipOrdServ = " + cabos.idTipOrdServ + " AND idRol = 3) " +
                //"AND per.idPuesto IN (SELECT DISTINCT idPuesto FROM dbo.RelPersonalRolOrdServ WHERE idTipOrdServ = " + cabos.idTipOrdServ + " AND idRol = 3)" + filtroEsp);

                resp = new CatPersonalSvc().getCatPersonalxFiltro(" per.idDepto IN (SELECT DISTINCT idDepto FROM dbo.RelPersonalRolOrdServ WHERE idTipOrdServ = " + cabos.idTipOrdServ + " AND idRol = 3) " +
                "AND per.idPuesto IN (SELECT DISTINCT idPuesto FROM dbo.RelPersonalRolOrdServ WHERE idTipOrdServ = " + cabos.idTipOrdServ + " AND idRol = 3)");


                if (resp.typeResult == ResultTypes.success)
                {

                    Lista = (List<CatPersonal>)resp.result;
                    control.DataSource = null;
                    control.DataSource = Lista;

                    control.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                    control.AutoCompleteSource = AutoCompleteSource.ListItems;
                    control.DisplayMember = "NombreCompleto";
                    control.SelectedIndex = 0;

                    controlDet.DataSource = null;
                    controlDet.DataSource = Lista;
                    controlDet.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                    controlDet.AutoCompleteSource = AutoCompleteSource.ListItems;
                    controlDet.DisplayMember = "NomPuesto";
                    controlDet.SelectedIndex = 0;


                    return Lista;
                    ////Ayundate 1
                    //listaAyudante1 = (List<CatPersonal>)resp.result;
                    //cmbOpera2.DataSource = null;
                    //cmbOpera2.DataSource = listaAyudante1;
                    //cmbOpera2.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                    //cmbOpera2.AutoCompleteSource = AutoCompleteSource.ListItems;
                    //cmbOpera2.DisplayMember = "NombreCompleto";
                    //cmbOpera2.SelectedIndex = 0;

                    ////Ayundate 2
                    //listaAyudante2 = (List<CatPersonal>)resp.result;
                    //cmbOpera3.DataSource = null;
                    //cmbOpera3.DataSource = listaAyudante2;
                    //cmbOpera3.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                    //cmbOpera3.AutoCompleteSource = AutoCompleteSource.ListItems;
                    //cmbOpera3.DisplayMember = "NombreCompleto";
                    //cmbOpera3.SelectedIndex = 0;
                }
                else
                {
                    return null;
                    //no se encontro
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + ex.Message,
                  this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return null;
            }
        }
        private void Asigna()
        {
            //ASIGNAR TODOS
            DetRecepcionOS detRos;
            listaDetROS_Asigna = new List<DetRecepcionOS>();

            btnAsigna.Enabled = true;
            foreach (var item in listaDetOS)
            {
                if (item.Seleccionado)
                {
                    detRos = new DetRecepcionOS
                    {
                        idOrdenSer = item.idOrdenSer,
                        idOrdenTrabajo = item.idOrdenTrabajo,
                        idFamilia = item.idFamilia,
                        NotaRecepcion = item.NotaRecepcionF,
                        NotaRecepcionA = item.FallaReportada,
                        isCancelado = false,
                        motivoCancelacion = "",
                        idDivision = item.idDivision,
                        idPersonalResp = 0,
                        idPersonalAyu1 = 0,
                        idPersonalAyu2 = 0,
                        Inserta = false,
                        NomPersonalResp = "",
                        NomPersonalAyu1 = "",
                        NomPersonalAyu2 = ""
                    };
                    listaDetROS_Asigna.Add(detRos);
                }
            }
            //ASIGNAR TODOS
        }

        private void txtTodo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F3)
            {
                ConsultaElemento(txtTodo.Text);

            }

            if (e.KeyCode == Keys.Enter)
            {
                if (_idOrdenSer > 0 && _idOrdenTrabajo > 0)
                //if (_idOrdenSer > 0 )
                {
                    //**********************************
                    listaDetOSCheck = new List<DetOrdenServicio>();
                    //actividad = (CatActividades)cmbidServicio.SelectedItem;
                    //if (txtTodo.Tag == null)
                    //{
                    //    ConsultaElemento(txtTodo.Text);
                    //}
                    //else
                    //{
                    //    actividad = (CatActividades)txtTodo.Tag;
                    //}

                    //actividad = (CatActividades)txtTodo.Tag;
                    resp = new CatActividadesSvc().getCatActividadesxFilter(0, " c.NombreAct = '" + txtTodo.Text + "'");
                    if (resp.typeResult == ResultTypes.success)
                    {
                        actividad = ((List<CatActividades>)resp.result)[0];
                        txtTodo.Tag = actividad;
                    }
                    //verificar en la lista nuevas
                    if (listaDetOS != null)
                    {
                        if (listaDetOS.Count > 0)
                        {
                            foreach (var item in listaDetOS)
                            {
                                if (item.idActividad == actividad.idActividad && item.Estatus != "CAN")
                                {
                                    MessageBox.Show("No se Puede agregar la actividad; " + actividad.NombreAct + " ya esta en la Orden de Trabajo: " + item.idOrdenTrabajo, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    //cmbidServicio.Focus();
                                    txtTodo.Focus();
                                    return;
                                }
                            }
                        }
                    }




                    //Verificar si el servicio ya esta dado de alta en cualquiera de las ordenes de trabajo
                    //aqui

                    resp = new DetOrdenServicioSvc().getDetOrdenServicioxFilter(_idOrdenSer);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        listaDetOSCheck = (List<DetOrdenServicio>)resp.result;
                        foreach (var item in listaDetOSCheck)
                        {
                            if (item.idActividad == actividad.idActividad && item.Estatus != "CAN")
                            {
                                //No se puede Agregar por que ya esta esa actividad 
                                MessageBox.Show("No se Puede agregar la actividad; " + actividad.NombreAct + " ya esta en la Orden de Trabajo: " + item.idOrdenTrabajo, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                //cmbidServicio.Focus();
                                txtTodo.Focus();
                                return;
                            }
                        }
                    }
                    //**********************************

                    if (_idOrdenTrabajo == 0)
                    {
                        ContModificando++;
                    }
                    //AgregaOrdenTrabajoEspF(((CatActividades)cmbidServicio.SelectedItem).idActividad, eTipoElementoOrdenTrab.Actividad);
                    AgregaOrdenTrabajoEspF(((CatActividades)txtTodo.Tag).idActividad, eTipoElementoOrdenTrab.Actividad);
                    HabilitaModifica();
                    HabilitaBotones(opcHabilita.Editando);
                }
                //else if (_idOrdenSer > 0 && _idOrdenTrabajo == 0)
                //{

                //}
                else
                {
                    if (lblServicios.Text == "Actividades")
                    {
                        resp = new CatActividadesSvc().getCatActividadesxFilter(0, " c.NombreAct = '" + txtTodo.Text + "'");
                        if (resp.typeResult == ResultTypes.success)
                        {
                            actividad = ((List<CatActividades>)resp.result)[0];
                            txtTodo.Tag = actividad;
                        }
                        //AgregaOrdenTrabajo(((CatActividades)cmbidServicio.SelectedItem).idActividad.ToString(), eTipoElementoOrdenTrab.Actividad);
                        AgregaOrdenTrabajo(((CatActividades)txtTodo.Tag).idActividad.ToString(), eTipoElementoOrdenTrab.Actividad);
                    }
                    else if (lblServicios.Text == "Servicios")
                    {
                        resp = new CatServiciosSvc().getCatServiciosxFilter(0, " c.NomServicio = '" + txtTodo.Text + "'");
                        if (resp.typeResult == ResultTypes.success)
                        {
                            servicio = ((List<CatServicios>)resp.result)[0];
                            txtTodo.Tag = servicio;
                        }
                        //AgregaOrdenTrabajo(((CatServicios)cmbidServicio.SelectedItem).idServicio.ToString(), eTipoElementoOrdenTrab.Servicio);
                        AgregaOrdenTrabajo(((CatServicios)txtTodo.Tag).idServicio.ToString(), eTipoElementoOrdenTrab.Servicio);
                    }
                    HabilitaBotones(opcHabilita.DesHabilitaTodos);
                    HabilitaCampos(opcHabilita.Editando);
                }
                //cmbidServicio.SelectedIndex = 0;
                //cmbidServicio.Focus();
                if (txtTodo.Enabled)
                {
                    txtTodo.Text = "";
                    txtTodo.Focus();
                }
                else
                {
                    txtFallaReportada.Text = "";
                    txtFallaReportada.Focus();
                }


            }
        }

        private void txtTodo_KeyUp(object sender, KeyEventArgs e)
        {

        }

        private void ConsultaElemento(string TextoInicial)
        {
            int valor;
            if (lblServicios.Text == "Actividades")
            {
                string filtro1 = DeterminaFiltroTipo(eRelTipOrdServCat.CatActividades);

                fConsulta fcon = new fConsulta(opcBusqueda.CatActividades, _user, " Actividades", "idActividad", false, filtro1, " c.NombreAct ASC", TextoInicial);
                fcon.StartPosition = FormStartPosition.CenterScreen;
                valor = Convert.ToInt32(fcon.DevuelveValor());

                if (valor != null)
                {
                    if (valor > 0)
                    {
                        resp = new CatActividadesSvc().getCatActividadesxFilter(valor);
                        if (resp.typeResult == ResultTypes.success)
                        {
                            actividad = ((List<CatActividades>)resp.result)[0];
                            txtTodo.Tag = actividad;
                            txtTodo.Text = actividad.NombreAct;
                        }
                        else
                        {
                            txtTodo.Tag = null;

                            txtTodo.Text = "";
                        }

                        //CargaForma(opcBusqueda.CabOrdenServicio, txtidOrdenSer.Text);
                    }
                    else
                    {
                        txtTodo.Tag = null;
                    }


                }
                else
                {
                    txtTodo.Tag = null;
                }
            }
            else if (lblServicios.Text == "Servicios")
            {
                string filtro1 = DeterminaFiltroTipo(eRelTipOrdServCat.CatServicios);

                fConsulta fcon = new fConsulta(opcBusqueda.CatServicios, _user, " Servicios", "idServicio", false, filtro1, "", TextoInicial);
                fcon.StartPosition = FormStartPosition.CenterScreen;
                valor = Convert.ToInt32(fcon.DevuelveValor());

                if (valor != null)
                {
                    if (valor > 0)
                    {
                        resp = new CatServiciosSvc().getCatServiciosxFilter(valor);
                        if (resp.typeResult == ResultTypes.success)
                        {
                            servicio = ((List<CatServicios>)resp.result)[0];
                            txtTodo.Tag = servicio;
                            txtTodo.Text = servicio.NomServicio;
                        }
                        else
                        {
                            txtTodo.Tag = null;

                            txtTodo.Text = "";
                        }

                        //CargaForma(opcBusqueda.CabOrdenServicio, txtidOrdenSer.Text);
                    }
                    else
                    {
                        txtTodo.Tag = null;
                    }


                }
                else
                {
                    txtTodo.Tag = null;
                }
            }
        }

        private string DeterminaFiltroTipo(eRelTipOrdServCat rel)
        {
            eTipoOrdenServicio tipo = (eTipoOrdenServicio)_TipoOrdServ.idTipOrdServ;
            string Resp = "";
            switch (tipo)
            {
                case eTipoOrdenServicio.COMBUSTIBLE:
                    if (rel == eRelTipOrdServCat.CatActividades)
                    {

                    }
                    else if (rel == eRelTipOrdServCat.CatServicios)
                    {

                    }
                    break;
                case eTipoOrdenServicio.TALLER:
                    if (rel == eRelTipOrdServCat.CatActividades)
                    {
                        Resp = " c.Taller = 1";
                    }
                    else if (rel == eRelTipOrdServCat.CatServicios)
                    {

                    }
                    break;
                case eTipoOrdenServicio.LLANTAS:
                    if (rel == eRelTipOrdServCat.CatActividades)
                    {
                        Resp = " c.Llantera = 1";
                        //Resp = " idActividad IN (SELECT IdRelacion FROM dbo.RelTipOrdServ WHERE idTipOrdServ = " + _TipoOrdServ.idTipOrdServ + " AND NomCatalogo = 'CatActividades')";
                        //                        resp = new CatActividadesSvc().getCatActividadesxFilter(0,
                        //" idActividad IN (SELECT IdRelacion FROM dbo.RelTipOrdServ WHERE idTipOrdServ = " + _TipoOrdServ.idTipOrdServ + " AND NomCatalogo = 'CatActividades')");

                    }
                    else if (rel == eRelTipOrdServCat.CatServicios)
                    {

                    }
                    break;
                case eTipoOrdenServicio.LAVADERO:
                    if (rel == eRelTipOrdServCat.CatActividades)
                    {
                        Resp = " c.Lavadero = 1";
                    }
                    else if (rel == eRelTipOrdServCat.CatServicios)
                    {

                    }
                    break;
                case eTipoOrdenServicio.RESGUARDO:
                    break;
                case eTipoOrdenServicio.TODAS:
                    break;
                default:
                    break;
            }
            return Resp;
        }
        #region LLANTAS
        private void DibujaLinea(int xIni, int yIni, int xFin, int yFin, Control parent)
        {
            //ShapeContainer canvas = new ShapeContainer();
            //LineShape theLine = new LineShape();

            //canvas.Parent = parent;
            //theLine.Parent = canvas;
            //theLine.BorderWidth = 10;
            ////theLine.StartPoint = new Point(185, 25);
            ////theLine.EndPoint = new Point(183, 300);
            ////DibujaLinea(185, 25,183, 300)

            //theLine.StartPoint = new Point(xIni, yIni);
            //theLine.EndPoint = new Point(xFin, yFin);


            //  
            //  ' Set the starting and ending coordinates for the line.
            //  
            //  
            //  theLine.EndPoint = New System.Drawing.Point(183, 300)
        }

        //    Private Sub ParaLlantas(ByVal idUnidad As String)
        //    txtUniTrans.Text = idUnidad
        //    UniTrans = New UnidadesTranspClass(idUnidad)
        //    If UniTrans.Existe Then
        //        TipUni = New TipoUniTransClass(UniTrans.idTipoUnidad)
        //        If TipUni.Existe Then
        //            txtTipoUni.Text = TipUni.Clasificacion
        //            ClasLlan = New ClasLlanClass(TipUni.idClasLlan)
        //            If ClasLlan.Existe Then
        //                DibujaEsquema(TipUni.idClasLlan, TipUni.NoLlantas)
        //                If txtPosicion.Text<> "" Then
        //                    SeleccionaPosicion(Val(txtPosicion.Text))
        //                End If
        //            End If
        //        End If
        //    End If
        //End Sub
        private void ParaLLantas()
        {
            try
            {

                resp = new CatTipoUniTransSvc().getCatTipoUniTransxFilter(((CatUnidadTrans)cmbidUnidadTrans.SelectedItem).idTipoUnidad);
                if (resp.typeResult == ResultTypes.success)
                {
                    tipouni = ((List<CatTipoUniTrans>)resp.result)[0];
                    resp = new CatClasLlanSvc().getCatClasLlanxFilter(tipouni.idClasLlan);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        clasllan = ((List<CatClasLlan>)resp.result)[0];

                        DibujaEsquema(tipouni.idClasLlan, clasllan.NoLLantas);
                        //SeleccionaPosicion(Convert.ToInt32((CatLlantas)cmbPosicion.SelectedItem).Posicion));
                        if (cmbPosicion.SelectedItem != null)
                        {
                            if (int.TryParse(((CatLlantas)cmbPosicion.SelectedItem).Posicion.ToString(), out entero))
                            {
                                SeleccionaPosicion(entero);
                            }

                        }
                        GpoPosiciones.Visible = true;
                        //gpoPosicion.Visible
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + ex.Message,
                  this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void DibujaEsquema(int vidClasLlan, int TotalLlantas)
        {
            int vNoEje = 0;
            int vNoLlantas = 0;
            int LlantxLado = 0;
            int NoPosicion = 0;
            int FactorX = 201;
            int FactorY = 70;
            int PosX = 29;
            int PosFacLinX = 195;
            int PosY = 37;
            try
            {
                listaejellanta = new List<RelEjeLlanta>();
                System.Array.Resize(ref rdbPosicion, TotalLlantas);
                for (int i = 0; i < rdbPosicion.Length; i++)
                {
                    rdbPosicion[i] = new RadioButton();
                }
                LimpiaPosiciones();
                resp = new RelEjeLlantaSvc().getRelEjeLlantaxFilter(vidClasLlan);
                if (resp.typeResult == ResultTypes.success)
                {
                    listaejellanta = (List<RelEjeLlanta>)resp.result;
                    foreach (var item in listaejellanta)
                    {
                        vNoEje = item.EjeNo;
                        vNoLlantas = item.NoLlantas;
                        LlantxLado = vNoLlantas / 2;
                        //Posiciones Iniciales
                        if (LlantxLado == 0)
                        {
                            PosX = 95;
                            FactorX = 37;
                        }
                        else if (LlantxLado == 1)
                        {
                            PosX = 50;
                            FactorX = 37;
                        }
                        else
                        {
                            PosX = 25;
                            FactorX = 95;
                        }

                        for (int z = 0; z < vNoLlantas; z++)
                        {
                            rdbPosicion[NoPosicion].Name = "pos" + NoPosicion;
                            //Verificamos Nombre de la llanta()
                            resp = new CatLlantasSvc().getCatLlantasxFilter(0, "Posicion = " + (NoPosicion + 1) + " AND idUbicacion = '" + ((CatUnidadTrans)cmbidUnidadTrans.SelectedItem).idUnidadTrans + "'");
                            if (resp.typeResult == ResultTypes.success)
                            {
                                llanta = ((List<CatLlantas>)resp.result)[0];
                                rdbPosicion[NoPosicion].Text = (NoPosicion + 1) + " (" + llanta.idLlanta + " )";
                                rdbPosicion[NoPosicion].Enabled = true;
                            }
                            else
                            {
                                rdbPosicion[NoPosicion].Text = (NoPosicion + 1).ToString();
                                rdbPosicion[NoPosicion].Enabled = false;
                            }
                            rdbPosicion[NoPosicion].Checked = false;
                            //rdbPosicion[NoPosicion].Size = new Size(80, 64);
                            rdbPosicion[NoPosicion].Size = new Size(90, 64);
                            rdbPosicion[NoPosicion].Image = Fletera2.Properties.Resources.llanta2;
                            rdbPosicion[NoPosicion].TextImageRelation = TextImageRelation.Overlay;
                            rdbPosicion[NoPosicion].ImageAlign = ContentAlignment.TopCenter;
                            rdbPosicion[NoPosicion].TextAlign = ContentAlignment.BottomCenter;
                            rdbPosicion[NoPosicion].ForeColor = Color.Blue;

                            if (z == 0 && vNoLlantas > 2)
                            {
                                PosX = 5;
                            }
                            rdbPosicion[NoPosicion].Location = new Point(PosX, PosY);
                            rdbPosicion[NoPosicion].Click += RadioButtons_Click;
                            rdbPosicion[NoPosicion].KeyDown += ObjEnter_KeyDown;
                            rdbPosicion[NoPosicion].MouseHover += RadioButtons_MouseHover;
                            GpoPosiciones.Controls.Add(rdbPosicion[NoPosicion]);
                            PosX = PosX + FactorX;
                            if (z < (vNoLlantas - 1))
                            {
                                NoPosicion = NoPosicion + 1;
                            }
                            if (LlantxLado == 1)
                            {
                                PosX = PosX + PosFacLinX - FactorX;
                            }
                        }//for
                        PosY = PosY + FactorY;
                        NoPosicion = NoPosicion + 1;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + ex.Message,
                   this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

        }

        private void RadioButtons_Click(object sender, EventArgs e)
        {
            RadioButton button = sender as RadioButton;
            this.pos = this.ObtienePosicion(button.Text);
            this.SeleccionacmbPosicion(this.pos);
            object[] objArray1 = new object[] { "Posicion = ", this.pos, " AND idUbicacion = '", ((CatUnidadTrans)this.cmbidUnidadTrans.SelectedItem).idUnidadTrans, "'" };
            this.resp = new CatLlantasSvc().getCatLlantasxFilter(0, string.Concat(objArray1), "");
            if (this.resp.typeResult == ResultTypes.success)
            {
                this.llanta = ((List<CatLlantas>)this.resp.result)[0];
                this.resp = new CatEmpresasSvc().getCatEmpresasxFiltro(this.llanta.idEmpresa, "");
                if (this.resp.typeResult == ResultTypes.success)
                {
                    this.empresa = ((List<CatEmpresas>)this.resp.result)[0];
                    this.NomEmpresa = this.empresa.RazonSocial;
                    this.resp = new CatLLantasProductoSvc().getCatLLantasProductoxFilter(this.llanta.idProductoLlanta, "", "");
                    if (this.resp.typeResult == ResultTypes.success)
                    {
                        this.llantaProd = ((List<CatLLantasProducto>)this.resp.result)[0];
                        this.NomMarca = this.llantaProd.NombreMarca;
                        this.NomDisenio = this.llantaProd.Descripcion;
                        this.resp = new CatTipoLlantaSvc().getCatTipoLlantaxFilter(this.llanta.idTipoLLanta, "", "");
                        if (this.resp.typeResult == ResultTypes.success)
                        {
                            this.tipollanta = ((List<CatTipoLlanta>)this.resp.result)[0];
                            this.NomTipLlanta = this.tipollanta.Descripcion;
                        }
                        object[] objArray2 = new object[] {
                            "idLlanta = ", this.llanta.idLlanta, Environment.NewLine, "Empresa = ", this.NomEmpresa, Environment.NewLine, "Marca = ", this.NomMarca, Environment.NewLine, "Dise\x00f1o = ", this.NomDisenio, Environment.NewLine, "Medida = ", this.llantaProd.Medida, Environment.NewLine, "Profundidad = ",
                            this.llantaProd.Profundidad, Environment.NewLine, "TipoLlanta = ", this.NomTipLlanta, Environment.NewLine, "Ubicacion = ", this.llanta.idUbicacion, Environment.NewLine, "Posicion = ", this.llanta.Posicion
                        };
                        this.TextoToolTip = string.Concat(objArray2);
                    }
                    else
                    {
                        this.TextoToolTip = this.pos.ToString();
                    }
                    this.tt.IsBalloon = true;
                    this.tt.ShowAlways = true;
                    for (int i = 0; i < this.rdbPosicion.Length; i++)
                    {
                        this.rdbPosicion[i].BackColor = SystemColors.Control;
                    }
                    button.BackColor = Color.Yellow;
                }
            }
        }

        private void RadioButtons_MouseHover(object sender, EventArgs e)
        {
            RadioButton control = sender as RadioButton;
            this.pos = this.ObtienePosicion(control.Text);
            object[] objArray1 = new object[] { "Posicion = ", this.pos, " AND idUbicacion = '", ((CatUnidadTrans)this.cmbidUnidadTrans.SelectedItem).idUnidadTrans, "'" };
            this.resp = new CatLlantasSvc().getCatLlantasxFilter(0, string.Concat(objArray1), "");
            if (this.resp.typeResult == ResultTypes.success)
            {
                this.llanta = ((List<CatLlantas>)this.resp.result)[0];
                this.resp = new CatEmpresasSvc().getCatEmpresasxFiltro(this.llanta.idEmpresa, "");
                if (this.resp.typeResult == ResultTypes.success)
                {
                    this.empresa = ((List<CatEmpresas>)this.resp.result)[0];
                    this.NomEmpresa = this.empresa.RazonSocial;
                    this.resp = new CatLLantasProductoSvc().getCatLLantasProductoxFilter(this.llanta.idProductoLlanta, "", "");
                    if (this.resp.typeResult == ResultTypes.success)
                    {
                        this.llantaProd = ((List<CatLLantasProducto>)this.resp.result)[0];
                        this.NomMarca = this.llantaProd.NombreMarca;
                        this.NomDisenio = this.llantaProd.Descripcion;
                        this.resp = new CatTipoLlantaSvc().getCatTipoLlantaxFilter(this.llanta.idTipoLLanta, "", "");
                        if (this.resp.typeResult == ResultTypes.success)
                        {
                            this.tipollanta = ((List<CatTipoLlanta>)this.resp.result)[0];
                            this.NomTipLlanta = this.tipollanta.Descripcion;
                        }
                    }
                }
                object[] objArray2 = new object[] {
                    "idLlanta = ", this.llanta.idLlanta, Environment.NewLine, "Empresa = ", this.NomEmpresa, Environment.NewLine, "Marca = ", this.NomMarca, Environment.NewLine, "Dise\x00f1o = ", this.NomDisenio, Environment.NewLine, "Medida = ", this.llantaProd.Medida, Environment.NewLine, "Profundidad = ",
                    this.llantaProd.Profundidad, Environment.NewLine, "TipoLlanta = ", this.NomTipLlanta, Environment.NewLine, "Ubicacion = ", this.llanta.idUbicacion, Environment.NewLine, "Posicion = ", this.llanta.Posicion
                };
                this.TextoToolTip = string.Concat(objArray2);
            }
            else
            {
                this.TextoToolTip = this.pos.ToString();
            }
            this.tt.IsBalloon = true;
            this.tt.ShowAlways = true;
            this.tt.SetToolTip(control, this.TextoToolTip);
            Thread.Sleep(0x9c4);
        }

        private void LimpiaPosiciones()
        {
            int num_controles = this.GpoPosiciones.Controls.Count - 1;
            for (int n = num_controles; n >= 0; n--)
            {
                Control ctrl = GpoPosiciones.Controls[n];
                //if (ctrl is RadioButton)
                //{

                //}
                GpoPosiciones.Controls.Remove(ctrl);
                ctrl.Dispose();

            }
            //DibujaLinea(185, 25, 183, 300, GpoPosiciones);
        }



        ////////////////////////////////
        //    Private Sub DibujaEsquema(ByVal vidClasLlan As Integer, ByVal TotalLlantas As Integer)

        private int ObtienePosicion(string Cadena)
        {
            string PosiblePos = "";
            int RespuestaPos = 0;

            //PosiblePos = Mid(Cadena, 1, 2)
            PosiblePos = Cadena.Substring(0, 2);

            if (int.TryParse(PosiblePos, out entero))
            {
                RespuestaPos = entero;
            }
            else
            {
                PosiblePos = Cadena.Substring(1, 1);
                if (int.TryParse(PosiblePos, out entero))
                {
                    RespuestaPos = entero;
                }
            }
            return RespuestaPos;
        }
        private void SeleccionaPosicion(int Posicion)
        {
            if (rdbPosicion == null)
            {
                return;
            }
            if (Posicion > rdbPosicion.Length)
            {
                //MessageBox.Show("La Estructura del tipo de unidad " + txtTipoUni.Text + " No cuenta con la posicion " + txtPosicion.Text + Environment.NewLine + "Favor de seleccionar la Posicion Correspondiente", "Operación Exitosa", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //txtPosicion.Text = "";
                for (int i = 0; i < rdbPosicion.Length; i++)
                {
                    rdbPosicion[i].BackColor = SystemColors.Control;
                    rdbPosicion[i].Checked = false;
                }
            }
            else
            {
                rdbPosicion[Posicion - 1].Checked = true;
                for (int i = 0; i < rdbPosicion.Length; i++)
                {
                    rdbPosicion[i].BackColor = SystemColors.Control;
                }
                rdbPosicion[Posicion - 1].BackColor = Color.Yellow;

            }
        }

        private void ObjEnter_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("{TAB}");
                e.Handled = true;
            }
        }

        private void LlenaComboLlantasF(string iUnidadTrans)
        {
            listaLlantasF = null;
            listaLlantas = null;
            resp = new CatTipoUniTransSvc().getCatTipoUniTransxFilter(((CatUnidadTrans)cmbidUnidadTrans.SelectedItem).idTipoUnidad);
            if (resp.typeResult == ResultTypes.success)
            {
                tipouni = ((List<CatTipoUniTrans>)resp.result)[0];
                resp = new CatClasLlanSvc().getCatClasLlanxFilter(tipouni.idClasLlan);
                if (resp.typeResult == ResultTypes.success)
                {
                    clasllan = ((List<CatClasLlan>)resp.result)[0];
                    resp = new CatLlantasSvc().getCatLlantasxFilter(0, " idUbicacion = '" + iUnidadTrans + "'", " posicion");
                    if (resp.typeResult == ResultTypes.success)
                    {
                        listaLlantas = (List<CatLlantas>)resp.result;
                        resp = new CatLlantasSvc().getCatLlantasxFilter(0, " idUbicacion = '" + iUnidadTrans + "'", " posicion");
                        if (resp.typeResult == ResultTypes.success)
                        {
                            listaLlantasF = (List<CatLlantas>)resp.result;
                        }
                    }
                }
                bool bandNo = false;
                //if (llanta == null)
                //{
                //    llanta = new CatLlantas();
                //}
                if (listaLlantasF == null)
                {
                    listaLlantasF = new List<CatLlantas>();
                }

                for (int i = 0; i < clasllan.NoLLantas; i++)
                {
                    if (listaLlantas != null)
                    {
                        foreach (var item in listaLlantas)
                        {
                            if (i + 1 == item.Posicion)
                            {
                                bandNo = true;
                                break;
                            }
                            else
                            {
                                bandNo = false;
                            }
                        }
                    }
                    
                    if (!bandNo)
                    {
                        CatLlantas llanta = new CatLlantas
                        {
                            idLlanta = "",
                            idSisLlanta = 0,
                            idEmpresa = ((CatUnidadTrans)cmbidUnidadTrans.SelectedItem).idEmpresa,
                            Posicion = i + 1
                        };
                        this.llanta = llanta;
                        listaLlantasF.Add(llanta);
                    }
                }

                listaLlantasF = listaLlantasF.OrderBy(o => o.Posicion).ToList();
                cmbidLlanta.DataSource = null;
                cmbidLlanta.DataSource = listaLlantasF;
                cmbidLlanta.DisplayMember = "idLlanta";

                cmbPosicion.DataSource = null;
                cmbPosicion.DataSource = listaLlantasF;
                cmbPosicion.DisplayMember = "Posicion";
            }
            
            
        }

        //private void LlenaComboLlantas(string iUnidadTrans)
        //{
        //    resp = new CatLlantasSvc().getCatLlantasxFilter(0, " idUbicacion = '" + iUnidadTrans + "'", " posicion");
        //    if (resp.typeResult == ResultTypes.success)
        //    {
        //        listaLlantas = (List<CatLlantas>)resp.result;
        //        cmbidLlanta.DataSource = listaLlantas;
        //        cmbidLlanta.DisplayMember = "idLlanta";

        //        cmbPosicion.DataSource = listaLlantas;
        //        cmbPosicion.DisplayMember = "Posicion";
        //    }
        //}



        //    Private Sub LlenaComboLlantas(ByVal iUnidadTrans As String)
        //    llanta = New LlantasClass("0", OpcionLlanta.idLlanta)
        //    MyTablaComboLlanta = llanta.TablaLlantasCombo(" idUbicacion = '" & iUnidadTrans & "'")
        //    If MyTablaComboLlanta.Rows.Count > 0 Then
        //        cmbidLlanta.DataSource = MyTablaComboLlanta
        //        cmbidLlanta.ValueMember = "idLlanta"
        //        cmbidLlanta.DisplayMember = "idLlanta"

        //        cmbPosicion.DataSource = MyTablaComboLlanta
        //        cmbPosicion.ValueMember = "Posicion"
        //        cmbPosicion.DisplayMember = "Posicion"

        //    End If



        //End Sub


        #endregion

        private void cmbPosicion_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbPosicion == null || cmbPosicion.SelectedItem == null)
            {
                return;
            }
            if (int.TryParse(((CatLlantas)cmbPosicion.SelectedItem).Posicion.ToString(), out entero))
            {
                SeleccionaPosicion(entero);
            }

        }

        private void cmbidLlanta_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbidLlanta == null || cmbidLlanta.SelectedItem == null)
            {
                return;
            }

            if (int.TryParse(((CatLlantas)cmbidLlanta.SelectedItem).Posicion.ToString(), out entero))
            {
                SeleccionaPosicion(entero);
            }
        }

        private void cmbPosicion_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                //cmbidLlanta.Focus();

                if (((eTipoOrdenServicio)_TipoOrdServ.idTipOrdServ) == eTipoOrdenServicio.LLANTAS)
                {
                    txtTodo.Enabled = true;
                    txtFallaReportada.Enabled = false;
                    txtTodo.Focus();
                    txtTodo.SelectAll();
                }
                else
                {
                    txtTodo.Enabled = false;
                    txtFallaReportada.Enabled = true;
                    txtFallaReportada.Focus();
                    txtFallaReportada.SelectAll();

                }
            }
        }

        private void cmbidLlanta_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                txtTodo.Enabled = false;
                txtFallaReportada.Enabled = true;
                txtFallaReportada.Focus();
                txtFallaReportada.SelectAll();

            }
        }
        private void SeleccionacmbPosicion(int posicion)
        {
            try
            {
                int i = 0;
                foreach (CatLlantas item in cmbPosicion.Items)
                {
                    if (item.Posicion == posicion)
                    {
                        cmbPosicion.SelectedIndex = i;
                        return;
                    }
                    else
                    {
                        i++;
                    }
                }
            }
            catch (Exception e)
            {

                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + e.Message,
                  this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void CargaActividades()
        {
            listaActividades = new List<CatActividades>();

            //resp = new CatActividadesSvc().getCatActividadesxFilter(0,
            //" idActividad IN (SELECT IdRelacion FROM dbo.RelTipOrdServ WHERE idTipOrdServ = " + _TipoOrdServ.idTipOrdServ + " AND NomCatalogo = 'CatActividades')");
            resp = new CatActividadesSvc().getCatActividadesxFilter(0, "c.llantera = 1", " nombreAct");

            if (resp.typeResult == ResultTypes.success)
            {
                listaActividades = (List<CatActividades>)resp.result;

                if (listaActividades != null)
                {
                    if (listaActividades.Count > 0)
                    {
                        foreach (var item in listaActividades)
                        {
                            coleccion.Add(Convert.ToString(item.NombreAct));
                        }
                        lblServicios.Text = "Actividades";
                        txtTodo.AutoCompleteCustomSource = coleccion;
                        txtTodo.AutoCompleteMode = AutoCompleteMode.Suggest;
                        txtTodo.AutoCompleteSource = AutoCompleteSource.CustomSource;
                    }
                }
                //**************************************************

                //idTipoServicio_Act = ((CatTipoServicio)cmbOpciones.SelectedItem).idTipoServicio;


                HabilitaModifica();
                //txtFallaReportada.Text = detos.FallaReportada;

            }
        }

        private void grdDet_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //if (e.ColumnIndex == this.grdDet.Columns[0].Index)
            //{
            //    DataGridViewCheckBoxCell chkCelda = (DataGridViewCheckBoxCell)this.grdDet.Rows[e.RowIndex].Cells[0];

            //    if (Convert.ToBoolean(chkCelda.Value) == true)
            //    {
            //        chkCelda.Value = false;
            //        grdDet.Rows[e.RowIndex].Selected = false;
            //    }
            //    else
            //    {
            //        chkCelda.Value = true;
            //        grdDet.Rows[e.RowIndex].Selected = true;
            //    }
            //}
        }

        private void txtTodo_TextChanged(object sender, EventArgs e)
        {

        }
    }//
}
