﻿namespace Fletera2.Procesos
{
    partial class fFacturaDirecta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fFacturaDirecta));
            this.ToolStripMenu = new System.Windows.Forms.ToolStrip();
            this.btnNuevo = new System.Windows.Forms.ToolStripButton();
            this.btnGrabar = new System.Windows.Forms.ToolStripButton();
            this.btnEditar = new System.Windows.Forms.ToolStripButton();
            this.btnReporte = new System.Windows.Forms.ToolStripButton();
            this.btnCancelar = new System.Windows.Forms.ToolStripButton();
            this.btnSalir = new System.Windows.Forms.ToolStripButton();
            this.gpoCLiente = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtReferencia = new System.Windows.Forms.TextBox();
            this.cmbCCODIGOC01_CLI = new System.Windows.Forms.ComboBox();
            this.cmbConcepto = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtTipoUnidadTrans = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtIdUnidadTrans = new System.Windows.Forms.TextBox();
            this.dtpFechaMovimientoHora = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpFechaMovimiento = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.gpoAnexo20 = new System.Windows.Forms.GroupBox();
            this.cmbMetodoPago = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.cmbUsoCFDI = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.cmbFormaPago = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.gpoDetalle = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtCCODIGOP01_PRO = new System.Windows.Forms.TextBox();
            this.gpoImportes = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtTotTOTAL = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtTotSubTotal = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtTotIVA = new System.Windows.Forms.TextBox();
            this.lblManoObra = new System.Windows.Forms.Label();
            this.txtTotManoObra = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtTotRefacciones = new System.Windows.Forms.TextBox();
            this.lblTotServ = new System.Windows.Forms.Label();
            this.lblTotRef = new System.Windows.Forms.Label();
            this.rdbServ = new System.Windows.Forms.RadioButton();
            this.rdbProd = new System.Windows.Forms.RadioButton();
            this.lblCNOMBREPRODUCTO = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtTotal = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtPrecio = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtidunidad = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtCantidad = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtCCODIGOPRODUCTO = new System.Windows.Forms.TextBox();
            this.grdDetalle = new System.Windows.Forms.DataGridView();
            this.ToolStripMenu.SuspendLayout();
            this.gpoCLiente.SuspendLayout();
            this.gpoAnexo20.SuspendLayout();
            this.gpoDetalle.SuspendLayout();
            this.gpoImportes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdDetalle)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolStripMenu
            // 
            this.ToolStripMenu.AutoSize = false;
            this.ToolStripMenu.BackColor = System.Drawing.SystemColors.Control;
            this.ToolStripMenu.Font = new System.Drawing.Font("Berlin Sans FB", 11.25F);
            this.ToolStripMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnNuevo,
            this.btnGrabar,
            this.btnEditar,
            this.btnReporte,
            this.btnCancelar,
            this.btnSalir});
            this.ToolStripMenu.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.ToolStripMenu.Location = new System.Drawing.Point(0, 0);
            this.ToolStripMenu.Name = "ToolStripMenu";
            this.ToolStripMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.ToolStripMenu.Size = new System.Drawing.Size(1091, 65);
            this.ToolStripMenu.TabIndex = 10;
            this.ToolStripMenu.Text = "ToolStrip1";
            // 
            // btnNuevo
            // 
            this.btnNuevo.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnNuevo.Image = ((System.Drawing.Image)(resources.GetObject("btnNuevo.Image")));
            this.btnNuevo.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnNuevo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(54, 62);
            this.btnNuevo.Text = "&Nuevo";
            this.btnNuevo.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnNuevo.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnNuevo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnNuevo.Click += new System.EventHandler(this.btnNuevo_Click);
            // 
            // btnGrabar
            // 
            this.btnGrabar.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnGrabar.Image = ((System.Drawing.Image)(resources.GetObject("btnGrabar.Image")));
            this.btnGrabar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnGrabar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnGrabar.Name = "btnGrabar";
            this.btnGrabar.Size = new System.Drawing.Size(68, 62);
            this.btnGrabar.Text = "&Facturar";
            this.btnGrabar.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnGrabar.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnGrabar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnGrabar.Click += new System.EventHandler(this.btnGrabar_Click);
            // 
            // btnEditar
            // 
            this.btnEditar.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnEditar.Image = ((System.Drawing.Image)(resources.GetObject("btnEditar.Image")));
            this.btnEditar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnEditar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(52, 62);
            this.btnEditar.Text = "&Editar";
            this.btnEditar.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnEditar.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnEditar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnEditar.Visible = false;
            // 
            // btnReporte
            // 
            this.btnReporte.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnReporte.Image = ((System.Drawing.Image)(resources.GetObject("btnReporte.Image")));
            this.btnReporte.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnReporte.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnReporte.Name = "btnReporte";
            this.btnReporte.Size = new System.Drawing.Size(65, 62);
            this.btnReporte.Text = "&Reporte";
            this.btnReporte.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnReporte.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnReporte.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnReporte.Visible = false;
            // 
            // btnCancelar
            // 
            this.btnCancelar.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnCancelar.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar.Image")));
            this.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnCancelar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(69, 62);
            this.btnCancelar.Text = "&Cancelar";
            this.btnCancelar.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnCancelar.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnSalir.Image = ((System.Drawing.Image)(resources.GetObject("btnSalir.Image")));
            this.btnSalir.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnSalir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(55, 62);
            this.btnSalir.Text = "&S a l i r";
            this.btnSalir.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnSalir.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // gpoCLiente
            // 
            this.gpoCLiente.Controls.Add(this.label10);
            this.gpoCLiente.Controls.Add(this.txtReferencia);
            this.gpoCLiente.Controls.Add(this.cmbCCODIGOC01_CLI);
            this.gpoCLiente.Controls.Add(this.cmbConcepto);
            this.gpoCLiente.Controls.Add(this.label3);
            this.gpoCLiente.Controls.Add(this.txtTipoUnidadTrans);
            this.gpoCLiente.Controls.Add(this.label19);
            this.gpoCLiente.Controls.Add(this.txtIdUnidadTrans);
            this.gpoCLiente.Controls.Add(this.dtpFechaMovimientoHora);
            this.gpoCLiente.Controls.Add(this.label2);
            this.gpoCLiente.Controls.Add(this.dtpFechaMovimiento);
            this.gpoCLiente.Controls.Add(this.label9);
            this.gpoCLiente.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.gpoCLiente.Location = new System.Drawing.Point(12, 68);
            this.gpoCLiente.Name = "gpoCLiente";
            this.gpoCLiente.Size = new System.Drawing.Size(472, 180);
            this.gpoCLiente.TabIndex = 11;
            this.gpoCLiente.TabStop = false;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Blue;
            this.label10.Location = new System.Drawing.Point(4, 153);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(92, 18);
            this.label10.TabIndex = 87;
            this.label10.Text = "Referencia:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtReferencia
            // 
            this.txtReferencia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtReferencia.Enabled = false;
            this.txtReferencia.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtReferencia.Location = new System.Drawing.Point(135, 149);
            this.txtReferencia.MaxLength = 20;
            this.txtReferencia.Name = "txtReferencia";
            this.txtReferencia.Size = new System.Drawing.Size(226, 25);
            this.txtReferencia.TabIndex = 86;
            this.txtReferencia.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtReferencia_KeyPress);
            // 
            // cmbCCODIGOC01_CLI
            // 
            this.cmbCCODIGOC01_CLI.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.cmbCCODIGOC01_CLI.FormattingEnabled = true;
            this.cmbCCODIGOC01_CLI.Location = new System.Drawing.Point(72, 24);
            this.cmbCCODIGOC01_CLI.Name = "cmbCCODIGOC01_CLI";
            this.cmbCCODIGOC01_CLI.Size = new System.Drawing.Size(388, 26);
            this.cmbCCODIGOC01_CLI.TabIndex = 85;
            this.cmbCCODIGOC01_CLI.SelectedIndexChanged += new System.EventHandler(this.cmbCCODIGOC01_CLI_SelectedIndexChanged);
            this.cmbCCODIGOC01_CLI.KeyUp += new System.Windows.Forms.KeyEventHandler(this.cmbCCODIGOC01_CLI_KeyUp);
            // 
            // cmbConcepto
            // 
            this.cmbConcepto.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.cmbConcepto.FormattingEnabled = true;
            this.cmbConcepto.Location = new System.Drawing.Point(135, 118);
            this.cmbConcepto.Name = "cmbConcepto";
            this.cmbConcepto.Size = new System.Drawing.Size(226, 26);
            this.cmbConcepto.TabIndex = 84;
            this.cmbConcepto.SelectedIndexChanged += new System.EventHandler(this.cmbConcepto_SelectedIndexChanged);
            this.cmbConcepto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbConcepto_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Blue;
            this.label3.Location = new System.Drawing.Point(13, 121);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 18);
            this.label3.TabIndex = 83;
            this.label3.Text = "Concepto:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtTipoUnidadTrans
            // 
            this.txtTipoUnidadTrans.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtTipoUnidadTrans.Location = new System.Drawing.Point(224, 84);
            this.txtTipoUnidadTrans.Name = "txtTipoUnidadTrans";
            this.txtTipoUnidadTrans.Size = new System.Drawing.Size(137, 25);
            this.txtTipoUnidadTrans.TabIndex = 82;
            // 
            // label19
            // 
            this.label19.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Blue;
            this.label19.Location = new System.Drawing.Point(4, 91);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(133, 18);
            this.label19.TabIndex = 81;
            this.label19.Text = "Unidad Transporte";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtIdUnidadTrans
            // 
            this.txtIdUnidadTrans.Enabled = false;
            this.txtIdUnidadTrans.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtIdUnidadTrans.Location = new System.Drawing.Point(143, 83);
            this.txtIdUnidadTrans.Name = "txtIdUnidadTrans";
            this.txtIdUnidadTrans.Size = new System.Drawing.Size(75, 25);
            this.txtIdUnidadTrans.TabIndex = 80;
            this.txtIdUnidadTrans.TextChanged += new System.EventHandler(this.txtIdUnidadTrans_TextChanged);
            this.txtIdUnidadTrans.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtIdUnidadTrans_KeyUp);
            // 
            // dtpFechaMovimientoHora
            // 
            this.dtpFechaMovimientoHora.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.dtpFechaMovimientoHora.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpFechaMovimientoHora.Location = new System.Drawing.Point(354, 56);
            this.dtpFechaMovimientoHora.Name = "dtpFechaMovimientoHora";
            this.dtpFechaMovimientoHora.Size = new System.Drawing.Size(106, 25);
            this.dtpFechaMovimientoHora.TabIndex = 79;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Blue;
            this.label2.Location = new System.Drawing.Point(7, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 18);
            this.label2.TabIndex = 78;
            this.label2.Text = "Fecha:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dtpFechaMovimiento
            // 
            this.dtpFechaMovimiento.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.dtpFechaMovimiento.Location = new System.Drawing.Point(85, 56);
            this.dtpFechaMovimiento.Name = "dtpFechaMovimiento";
            this.dtpFechaMovimiento.Size = new System.Drawing.Size(263, 25);
            this.dtpFechaMovimiento.TabIndex = 77;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Blue;
            this.label9.Location = new System.Drawing.Point(10, 32);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(56, 18);
            this.label9.TabIndex = 73;
            this.label9.Text = "Cliente:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // gpoAnexo20
            // 
            this.gpoAnexo20.Controls.Add(this.cmbMetodoPago);
            this.gpoAnexo20.Controls.Add(this.label13);
            this.gpoAnexo20.Controls.Add(this.cmbUsoCFDI);
            this.gpoAnexo20.Controls.Add(this.label12);
            this.gpoAnexo20.Controls.Add(this.cmbFormaPago);
            this.gpoAnexo20.Controls.Add(this.label11);
            this.gpoAnexo20.Location = new System.Drawing.Point(490, 77);
            this.gpoAnexo20.Name = "gpoAnexo20";
            this.gpoAnexo20.Size = new System.Drawing.Size(358, 147);
            this.gpoAnexo20.TabIndex = 15;
            this.gpoAnexo20.TabStop = false;
            this.gpoAnexo20.Text = "ANEXO 20";
            // 
            // cmbMetodoPago
            // 
            this.cmbMetodoPago.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.cmbMetodoPago.FormattingEnabled = true;
            this.cmbMetodoPago.Location = new System.Drawing.Point(126, 90);
            this.cmbMetodoPago.Name = "cmbMetodoPago";
            this.cmbMetodoPago.Size = new System.Drawing.Size(226, 26);
            this.cmbMetodoPago.TabIndex = 78;
            this.cmbMetodoPago.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbMetodoPago_KeyPress);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Blue;
            this.label13.Location = new System.Drawing.Point(4, 93);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(121, 18);
            this.label13.TabIndex = 77;
            this.label13.Text = "Método de Pago:";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cmbUsoCFDI
            // 
            this.cmbUsoCFDI.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.cmbUsoCFDI.FormattingEnabled = true;
            this.cmbUsoCFDI.Location = new System.Drawing.Point(126, 58);
            this.cmbUsoCFDI.Name = "cmbUsoCFDI";
            this.cmbUsoCFDI.Size = new System.Drawing.Size(226, 26);
            this.cmbUsoCFDI.TabIndex = 76;
            this.cmbUsoCFDI.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbUsoCFDI_KeyPress);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Blue;
            this.label12.Location = new System.Drawing.Point(4, 61);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(89, 18);
            this.label12.TabIndex = 75;
            this.label12.Text = "Uso de CFDI";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cmbFormaPago
            // 
            this.cmbFormaPago.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.cmbFormaPago.FormattingEnabled = true;
            this.cmbFormaPago.Location = new System.Drawing.Point(126, 22);
            this.cmbFormaPago.Name = "cmbFormaPago";
            this.cmbFormaPago.Size = new System.Drawing.Size(226, 26);
            this.cmbFormaPago.TabIndex = 74;
            this.cmbFormaPago.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbFormaPago_KeyPress);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Blue;
            this.label11.Location = new System.Drawing.Point(4, 25);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(112, 18);
            this.label11.TabIndex = 73;
            this.label11.Text = "Forma de Pago";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // gpoDetalle
            // 
            this.gpoDetalle.Controls.Add(this.label1);
            this.gpoDetalle.Controls.Add(this.txtCCODIGOP01_PRO);
            this.gpoDetalle.Controls.Add(this.gpoImportes);
            this.gpoDetalle.Controls.Add(this.lblTotServ);
            this.gpoDetalle.Controls.Add(this.lblTotRef);
            this.gpoDetalle.Controls.Add(this.rdbServ);
            this.gpoDetalle.Controls.Add(this.rdbProd);
            this.gpoDetalle.Controls.Add(this.lblCNOMBREPRODUCTO);
            this.gpoDetalle.Controls.Add(this.label5);
            this.gpoDetalle.Controls.Add(this.txtTotal);
            this.gpoDetalle.Controls.Add(this.label4);
            this.gpoDetalle.Controls.Add(this.txtPrecio);
            this.gpoDetalle.Controls.Add(this.label8);
            this.gpoDetalle.Controls.Add(this.txtidunidad);
            this.gpoDetalle.Controls.Add(this.label14);
            this.gpoDetalle.Controls.Add(this.txtCantidad);
            this.gpoDetalle.Controls.Add(this.label15);
            this.gpoDetalle.Controls.Add(this.txtCCODIGOPRODUCTO);
            this.gpoDetalle.Controls.Add(this.grdDetalle);
            this.gpoDetalle.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.gpoDetalle.Location = new System.Drawing.Point(12, 248);
            this.gpoDetalle.Name = "gpoDetalle";
            this.gpoDetalle.Size = new System.Drawing.Size(1071, 305);
            this.gpoDetalle.TabIndex = 18;
            this.gpoDetalle.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(108, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 18);
            this.label1.TabIndex = 43;
            this.label1.Text = "Codigo";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtCCODIGOP01_PRO
            // 
            this.txtCCODIGOP01_PRO.Location = new System.Drawing.Point(111, 40);
            this.txtCCODIGOP01_PRO.Name = "txtCCODIGOP01_PRO";
            this.txtCCODIGOP01_PRO.Size = new System.Drawing.Size(54, 25);
            this.txtCCODIGOP01_PRO.TabIndex = 42;
            // 
            // gpoImportes
            // 
            this.gpoImportes.Controls.Add(this.label6);
            this.gpoImportes.Controls.Add(this.txtTotTOTAL);
            this.gpoImportes.Controls.Add(this.label7);
            this.gpoImportes.Controls.Add(this.txtTotSubTotal);
            this.gpoImportes.Controls.Add(this.label16);
            this.gpoImportes.Controls.Add(this.txtTotIVA);
            this.gpoImportes.Controls.Add(this.lblManoObra);
            this.gpoImportes.Controls.Add(this.txtTotManoObra);
            this.gpoImportes.Controls.Add(this.label18);
            this.gpoImportes.Controls.Add(this.txtTotRefacciones);
            this.gpoImportes.Location = new System.Drawing.Point(803, 89);
            this.gpoImportes.Name = "gpoImportes";
            this.gpoImportes.Size = new System.Drawing.Size(255, 176);
            this.gpoImportes.TabIndex = 41;
            this.gpoImportes.TabStop = false;
            this.gpoImportes.Text = "IMPORTES";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Blue;
            this.label6.Location = new System.Drawing.Point(16, 144);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 18);
            this.label6.TabIndex = 36;
            this.label6.Text = "TOTAL";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtTotTOTAL
            // 
            this.txtTotTOTAL.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtTotTOTAL.Location = new System.Drawing.Point(121, 139);
            this.txtTotTOTAL.Name = "txtTotTOTAL";
            this.txtTotTOTAL.Size = new System.Drawing.Size(123, 25);
            this.txtTotTOTAL.TabIndex = 35;
            this.txtTotTOTAL.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Blue;
            this.label7.Location = new System.Drawing.Point(16, 84);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(69, 18);
            this.label7.TabIndex = 34;
            this.label7.Text = "Sub Total";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtTotSubTotal
            // 
            this.txtTotSubTotal.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtTotSubTotal.Location = new System.Drawing.Point(121, 81);
            this.txtTotSubTotal.Name = "txtTotSubTotal";
            this.txtTotSubTotal.Size = new System.Drawing.Size(123, 25);
            this.txtTotSubTotal.TabIndex = 33;
            this.txtTotSubTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Blue;
            this.label16.Location = new System.Drawing.Point(19, 115);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(41, 18);
            this.label16.TabIndex = 32;
            this.label16.Text = "I.V.A.";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtTotIVA
            // 
            this.txtTotIVA.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtTotIVA.Location = new System.Drawing.Point(121, 108);
            this.txtTotIVA.Name = "txtTotIVA";
            this.txtTotIVA.Size = new System.Drawing.Size(123, 25);
            this.txtTotIVA.TabIndex = 31;
            this.txtTotIVA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblManoObra
            // 
            this.lblManoObra.AutoSize = true;
            this.lblManoObra.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblManoObra.ForeColor = System.Drawing.Color.Blue;
            this.lblManoObra.Location = new System.Drawing.Point(19, 50);
            this.lblManoObra.Name = "lblManoObra";
            this.lblManoObra.Size = new System.Drawing.Size(60, 18);
            this.lblManoObra.TabIndex = 30;
            this.lblManoObra.Text = "Servicios";
            this.lblManoObra.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtTotManoObra
            // 
            this.txtTotManoObra.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtTotManoObra.Location = new System.Drawing.Point(121, 50);
            this.txtTotManoObra.Name = "txtTotManoObra";
            this.txtTotManoObra.Size = new System.Drawing.Size(123, 25);
            this.txtTotManoObra.TabIndex = 29;
            this.txtTotManoObra.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Blue;
            this.label18.Location = new System.Drawing.Point(16, 22);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(84, 18);
            this.label18.TabIndex = 28;
            this.label18.Text = "Refacciones";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtTotRefacciones
            // 
            this.txtTotRefacciones.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtTotRefacciones.Location = new System.Drawing.Point(121, 19);
            this.txtTotRefacciones.Name = "txtTotRefacciones";
            this.txtTotRefacciones.Size = new System.Drawing.Size(123, 25);
            this.txtTotRefacciones.TabIndex = 27;
            this.txtTotRefacciones.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblTotServ
            // 
            this.lblTotServ.AutoSize = true;
            this.lblTotServ.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotServ.ForeColor = System.Drawing.Color.Blue;
            this.lblTotServ.Location = new System.Drawing.Point(614, 268);
            this.lblTotServ.Name = "lblTotServ";
            this.lblTotServ.Size = new System.Drawing.Size(118, 18);
            this.lblTotServ.TabIndex = 40;
            this.lblTotServ.Text = "Total de Servicios";
            this.lblTotServ.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTotRef
            // 
            this.lblTotRef.AutoSize = true;
            this.lblTotRef.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotRef.ForeColor = System.Drawing.Color.Blue;
            this.lblTotRef.Location = new System.Drawing.Point(349, 268);
            this.lblTotRef.Name = "lblTotRef";
            this.lblTotRef.Size = new System.Drawing.Size(142, 18);
            this.lblTotRef.TabIndex = 39;
            this.lblTotRef.Text = "Total de Refacciones";
            this.lblTotRef.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // rdbServ
            // 
            this.rdbServ.AutoSize = true;
            this.rdbServ.Checked = true;
            this.rdbServ.ForeColor = System.Drawing.Color.DarkGreen;
            this.rdbServ.Location = new System.Drawing.Point(13, 43);
            this.rdbServ.Name = "rdbServ";
            this.rdbServ.Size = new System.Drawing.Size(73, 22);
            this.rdbServ.TabIndex = 31;
            this.rdbServ.TabStop = true;
            this.rdbServ.Text = "&Servicio";
            this.rdbServ.UseVisualStyleBackColor = true;
            this.rdbServ.CheckedChanged += new System.EventHandler(this.rdbServ_CheckedChanged);
            this.rdbServ.Click += new System.EventHandler(this.rdbServ_Click);
            // 
            // rdbProd
            // 
            this.rdbProd.AutoSize = true;
            this.rdbProd.ForeColor = System.Drawing.Color.Blue;
            this.rdbProd.Location = new System.Drawing.Point(13, 19);
            this.rdbProd.Name = "rdbProd";
            this.rdbProd.Size = new System.Drawing.Size(89, 22);
            this.rdbProd.TabIndex = 30;
            this.rdbProd.Text = "&Refaccion";
            this.rdbProd.UseVisualStyleBackColor = true;
            this.rdbProd.CheckedChanged += new System.EventHandler(this.rdbProd_CheckedChanged);
            this.rdbProd.Click += new System.EventHandler(this.rdbProd_Click);
            // 
            // lblCNOMBREPRODUCTO
            // 
            this.lblCNOMBREPRODUCTO.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCNOMBREPRODUCTO.Font = new System.Drawing.Font("Berlin Sans FB", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCNOMBREPRODUCTO.ForeColor = System.Drawing.Color.Red;
            this.lblCNOMBREPRODUCTO.Location = new System.Drawing.Point(10, 66);
            this.lblCNOMBREPRODUCTO.Name = "lblCNOMBREPRODUCTO";
            this.lblCNOMBREPRODUCTO.Size = new System.Drawing.Size(749, 31);
            this.lblCNOMBREPRODUCTO.TabIndex = 29;
            this.lblCNOMBREPRODUCTO.Text = "Producto";
            this.lblCNOMBREPRODUCTO.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Blue;
            this.label5.Location = new System.Drawing.Point(809, 13);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 18);
            this.label5.TabIndex = 28;
            this.label5.Text = "Total";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtTotal
            // 
            this.txtTotal.Location = new System.Drawing.Point(812, 40);
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.Size = new System.Drawing.Size(81, 25);
            this.txtTotal.TabIndex = 27;
            this.txtTotal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTotal_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Blue;
            this.label4.Location = new System.Drawing.Point(755, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 18);
            this.label4.TabIndex = 26;
            this.label4.Text = "Precio";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtPrecio
            // 
            this.txtPrecio.Location = new System.Drawing.Point(752, 40);
            this.txtPrecio.Name = "txtPrecio";
            this.txtPrecio.Size = new System.Drawing.Size(54, 25);
            this.txtPrecio.TabIndex = 25;
            this.txtPrecio.TextChanged += new System.EventHandler(this.txtPrecio_TextChanged);
            this.txtPrecio.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPrecio_KeyPress);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Blue;
            this.label8.Location = new System.Drawing.Point(614, 13);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(57, 18);
            this.label8.TabIndex = 24;
            this.label8.Text = "Unidad";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label8.Visible = false;
            // 
            // txtidunidad
            // 
            this.txtidunidad.Location = new System.Drawing.Point(617, 40);
            this.txtidunidad.Name = "txtidunidad";
            this.txtidunidad.Size = new System.Drawing.Size(54, 25);
            this.txtidunidad.TabIndex = 23;
            this.txtidunidad.Visible = false;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Blue;
            this.label14.Location = new System.Drawing.Point(674, 13);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(69, 18);
            this.label14.TabIndex = 22;
            this.label14.Text = "Cantidad";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtCantidad
            // 
            this.txtCantidad.Location = new System.Drawing.Point(677, 40);
            this.txtCantidad.Name = "txtCantidad";
            this.txtCantidad.Size = new System.Drawing.Size(69, 25);
            this.txtCantidad.TabIndex = 21;
            this.txtCantidad.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCantidad_KeyPress);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Blue;
            this.label15.Location = new System.Drawing.Point(171, 13);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(67, 18);
            this.label15.TabIndex = 19;
            this.label15.Text = "Producto";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtCCODIGOPRODUCTO
            // 
            this.txtCCODIGOPRODUCTO.Location = new System.Drawing.Point(174, 40);
            this.txtCCODIGOPRODUCTO.Name = "txtCCODIGOPRODUCTO";
            this.txtCCODIGOPRODUCTO.Size = new System.Drawing.Size(438, 25);
            this.txtCCODIGOPRODUCTO.TabIndex = 20;
            this.txtCCODIGOPRODUCTO.TextChanged += new System.EventHandler(this.txtCCODIGOPRODUCTO_TextChanged);
            this.txtCCODIGOPRODUCTO.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtCCODIGOPRODUCTO_KeyUp);
            // 
            // grdDetalle
            // 
            this.grdDetalle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdDetalle.Location = new System.Drawing.Point(16, 110);
            this.grdDetalle.Name = "grdDetalle";
            this.grdDetalle.Size = new System.Drawing.Size(771, 155);
            this.grdDetalle.TabIndex = 18;
            // 
            // fFacturaDirecta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1091, 565);
            this.Controls.Add(this.gpoDetalle);
            this.Controls.Add(this.gpoAnexo20);
            this.Controls.Add(this.gpoCLiente);
            this.Controls.Add(this.ToolStripMenu);
            this.Name = "fFacturaDirecta";
            this.Text = "Facturacion Directa";
            this.Load += new System.EventHandler(this.fFacturaDirecta_Load);
            this.ToolStripMenu.ResumeLayout(false);
            this.ToolStripMenu.PerformLayout();
            this.gpoCLiente.ResumeLayout(false);
            this.gpoCLiente.PerformLayout();
            this.gpoAnexo20.ResumeLayout(false);
            this.gpoAnexo20.PerformLayout();
            this.gpoDetalle.ResumeLayout(false);
            this.gpoDetalle.PerformLayout();
            this.gpoImportes.ResumeLayout(false);
            this.gpoImportes.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdDetalle)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.ToolStrip ToolStripMenu;
        private System.Windows.Forms.ToolStripButton btnNuevo;
        private System.Windows.Forms.ToolStripButton btnGrabar;
        private System.Windows.Forms.ToolStripButton btnEditar;
        private System.Windows.Forms.ToolStripButton btnReporte;
        private System.Windows.Forms.ToolStripButton btnCancelar;
        private System.Windows.Forms.ToolStripButton btnSalir;
        private System.Windows.Forms.GroupBox gpoCLiente;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker dtpFechaMovimientoHora;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpFechaMovimiento;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtIdUnidadTrans;
        private System.Windows.Forms.TextBox txtTipoUnidadTrans;
        private System.Windows.Forms.ComboBox cmbConcepto;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox gpoAnexo20;
        private System.Windows.Forms.ComboBox cmbMetodoPago;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox cmbUsoCFDI;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cmbFormaPago;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.GroupBox gpoDetalle;
        private System.Windows.Forms.GroupBox gpoImportes;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtTotTOTAL;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtTotSubTotal;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtTotIVA;
        private System.Windows.Forms.Label lblManoObra;
        private System.Windows.Forms.TextBox txtTotManoObra;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtTotRefacciones;
        private System.Windows.Forms.Label lblTotServ;
        private System.Windows.Forms.Label lblTotRef;
        private System.Windows.Forms.RadioButton rdbServ;
        private System.Windows.Forms.RadioButton rdbProd;
        private System.Windows.Forms.Label lblCNOMBREPRODUCTO;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtTotal;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtPrecio;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtidunidad;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtCantidad;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtCCODIGOPRODUCTO;
        private System.Windows.Forms.DataGridView grdDetalle;
        private System.Windows.Forms.ComboBox cmbCCODIGOC01_CLI;
        private System.Windows.Forms.TextBox txtCCODIGOP01_PRO;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtReferencia;
    }
}