﻿namespace Fletera2.Procesos
{
    partial class fCapturaServCOM
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fCapturaServCOM));
            this.gpoDatosOS = new System.Windows.Forms.GroupBox();
            this.txtidUnidadTrans = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtTipoUnidad = new System.Windows.Forms.TextBox();
            this.txtNotaDiagnostico = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtNotaRecepcion = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtTipoOrdenServ = new System.Windows.Forms.TextBox();
            this.txtIdEmpleadoEnc = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtidOrdenTrabajo = new System.Windows.Forms.TextBox();
            this.dtpFechaHora = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpFecha = new System.Windows.Forms.DateTimePicker();
            this.txtIdEmpresa = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtidOrdenSer = new System.Windows.Forms.TextBox();
            this.ToolStripMenu = new System.Windows.Forms.ToolStrip();
            this.btnGrabar = new System.Windows.Forms.ToolStripButton();
            this.btnSalir = new System.Windows.Forms.ToolStripButton();
            this.gpoServ = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtPrecio_CIDPRODUCTO_SERV = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtCantidad_CIDPRODUCTO_SERV = new System.Windows.Forms.TextBox();
            this.cmbCIDPRODUCTO_SERV = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtTOTAL = new System.Windows.Forms.TextBox();
            this.gpoDatosOS.SuspendLayout();
            this.ToolStripMenu.SuspendLayout();
            this.gpoServ.SuspendLayout();
            this.SuspendLayout();
            // 
            // gpoDatosOS
            // 
            this.gpoDatosOS.Controls.Add(this.txtidUnidadTrans);
            this.gpoDatosOS.Controls.Add(this.label5);
            this.gpoDatosOS.Controls.Add(this.txtTipoUnidad);
            this.gpoDatosOS.Controls.Add(this.txtNotaDiagnostico);
            this.gpoDatosOS.Controls.Add(this.label7);
            this.gpoDatosOS.Controls.Add(this.txtNotaRecepcion);
            this.gpoDatosOS.Controls.Add(this.label6);
            this.gpoDatosOS.Controls.Add(this.label8);
            this.gpoDatosOS.Controls.Add(this.txtTipoOrdenServ);
            this.gpoDatosOS.Controls.Add(this.txtIdEmpleadoEnc);
            this.gpoDatosOS.Controls.Add(this.label10);
            this.gpoDatosOS.Controls.Add(this.label3);
            this.gpoDatosOS.Controls.Add(this.txtidOrdenTrabajo);
            this.gpoDatosOS.Controls.Add(this.dtpFechaHora);
            this.gpoDatosOS.Controls.Add(this.label2);
            this.gpoDatosOS.Controls.Add(this.dtpFecha);
            this.gpoDatosOS.Controls.Add(this.txtIdEmpresa);
            this.gpoDatosOS.Controls.Add(this.label4);
            this.gpoDatosOS.Controls.Add(this.label1);
            this.gpoDatosOS.Controls.Add(this.txtidOrdenSer);
            this.gpoDatosOS.Location = new System.Drawing.Point(12, 68);
            this.gpoDatosOS.Name = "gpoDatosOS";
            this.gpoDatosOS.Size = new System.Drawing.Size(592, 246);
            this.gpoDatosOS.TabIndex = 10;
            this.gpoDatosOS.TabStop = false;
            this.gpoDatosOS.Text = "Datos";
            // 
            // txtidUnidadTrans
            // 
            this.txtidUnidadTrans.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtidUnidadTrans.Location = new System.Drawing.Point(291, 112);
            this.txtidUnidadTrans.Name = "txtidUnidadTrans";
            this.txtidUnidadTrans.Size = new System.Drawing.Size(75, 25);
            this.txtidUnidadTrans.TabIndex = 91;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Blue;
            this.label5.Location = new System.Drawing.Point(1, 112);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(135, 18);
            this.label5.TabIndex = 90;
            this.label5.Text = "Unidad Transporte:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtTipoUnidad
            // 
            this.txtTipoUnidad.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtTipoUnidad.Location = new System.Drawing.Point(149, 112);
            this.txtTipoUnidad.Name = "txtTipoUnidad";
            this.txtTipoUnidad.Size = new System.Drawing.Size(136, 25);
            this.txtTipoUnidad.TabIndex = 89;
            // 
            // txtNotaDiagnostico
            // 
            this.txtNotaDiagnostico.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNotaDiagnostico.Location = new System.Drawing.Point(149, 209);
            this.txtNotaDiagnostico.Name = "txtNotaDiagnostico";
            this.txtNotaDiagnostico.Size = new System.Drawing.Size(375, 27);
            this.txtNotaDiagnostico.TabIndex = 87;
            this.txtNotaDiagnostico.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Blue;
            this.label7.Location = new System.Drawing.Point(9, 216);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(119, 18);
            this.label7.TabIndex = 88;
            this.label7.Text = "Nota Diagnostico";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtNotaRecepcion
            // 
            this.txtNotaRecepcion.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNotaRecepcion.Location = new System.Drawing.Point(149, 176);
            this.txtNotaRecepcion.Name = "txtNotaRecepcion";
            this.txtNotaRecepcion.Size = new System.Drawing.Size(375, 27);
            this.txtNotaRecepcion.TabIndex = 85;
            this.txtNotaRecepcion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Blue;
            this.label6.Location = new System.Drawing.Point(9, 183);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(70, 18);
            this.label6.TabIndex = 86;
            this.label6.Text = "Actividad";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Blue;
            this.label8.Location = new System.Drawing.Point(263, 21);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(154, 18);
            this.label8.TabIndex = 81;
            this.label8.Text = "Tipo de Orden Servicio";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtTipoOrdenServ
            // 
            this.txtTipoOrdenServ.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtTipoOrdenServ.Location = new System.Drawing.Point(422, 18);
            this.txtTipoOrdenServ.Name = "txtTipoOrdenServ";
            this.txtTipoOrdenServ.Size = new System.Drawing.Size(152, 25);
            this.txtTipoOrdenServ.TabIndex = 80;
            // 
            // txtIdEmpleadoEnc
            // 
            this.txtIdEmpleadoEnc.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIdEmpleadoEnc.Location = new System.Drawing.Point(149, 143);
            this.txtIdEmpleadoEnc.Name = "txtIdEmpleadoEnc";
            this.txtIdEmpleadoEnc.Size = new System.Drawing.Size(375, 27);
            this.txtIdEmpleadoEnc.TabIndex = 78;
            this.txtIdEmpleadoEnc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Blue;
            this.label10.Location = new System.Drawing.Point(9, 150);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(134, 18);
            this.label10.TabIndex = 79;
            this.label10.Text = "Tecnico Encargado:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Blue;
            this.label3.Location = new System.Drawing.Point(7, 83);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(108, 18);
            this.label3.TabIndex = 77;
            this.label3.Text = "Orden. Trabajo";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtidOrdenTrabajo
            // 
            this.txtidOrdenTrabajo.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtidOrdenTrabajo.Location = new System.Drawing.Point(149, 81);
            this.txtidOrdenTrabajo.Name = "txtidOrdenTrabajo";
            this.txtidOrdenTrabajo.Size = new System.Drawing.Size(108, 25);
            this.txtidOrdenTrabajo.TabIndex = 76;
            // 
            // dtpFechaHora
            // 
            this.dtpFechaHora.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.dtpFechaHora.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpFechaHora.Location = new System.Drawing.Point(418, 50);
            this.dtpFechaHora.Name = "dtpFechaHora";
            this.dtpFechaHora.Size = new System.Drawing.Size(106, 25);
            this.dtpFechaHora.TabIndex = 75;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Blue;
            this.label2.Location = new System.Drawing.Point(36, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 18);
            this.label2.TabIndex = 74;
            this.label2.Text = "Fecha:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dtpFecha
            // 
            this.dtpFecha.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.dtpFecha.Location = new System.Drawing.Point(149, 50);
            this.dtpFecha.Name = "dtpFecha";
            this.dtpFecha.Size = new System.Drawing.Size(263, 25);
            this.dtpFecha.TabIndex = 73;
            // 
            // txtIdEmpresa
            // 
            this.txtIdEmpresa.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtIdEmpresa.Location = new System.Drawing.Point(366, 80);
            this.txtIdEmpresa.Name = "txtIdEmpresa";
            this.txtIdEmpresa.Size = new System.Drawing.Size(208, 25);
            this.txtIdEmpresa.TabIndex = 72;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Blue;
            this.label4.Location = new System.Drawing.Point(294, 87);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 18);
            this.label4.TabIndex = 71;
            this.label4.Text = "Cliente:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(7, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 18);
            this.label1.TabIndex = 11;
            this.label1.Text = "Orden. Servicio";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtidOrdenSer
            // 
            this.txtidOrdenSer.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtidOrdenSer.Location = new System.Drawing.Point(149, 19);
            this.txtidOrdenSer.Name = "txtidOrdenSer";
            this.txtidOrdenSer.Size = new System.Drawing.Size(108, 25);
            this.txtidOrdenSer.TabIndex = 10;
            // 
            // ToolStripMenu
            // 
            this.ToolStripMenu.AutoSize = false;
            this.ToolStripMenu.BackColor = System.Drawing.SystemColors.Control;
            this.ToolStripMenu.Font = new System.Drawing.Font("Berlin Sans FB", 11.25F);
            this.ToolStripMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnGrabar,
            this.btnSalir});
            this.ToolStripMenu.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.ToolStripMenu.Location = new System.Drawing.Point(0, 0);
            this.ToolStripMenu.Name = "ToolStripMenu";
            this.ToolStripMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.ToolStripMenu.Size = new System.Drawing.Size(721, 65);
            this.ToolStripMenu.TabIndex = 11;
            this.ToolStripMenu.Text = "ToolStrip1";
            // 
            // btnGrabar
            // 
            this.btnGrabar.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnGrabar.Image = ((System.Drawing.Image)(resources.GetObject("btnGrabar.Image")));
            this.btnGrabar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnGrabar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnGrabar.Name = "btnGrabar";
            this.btnGrabar.Size = new System.Drawing.Size(60, 62);
            this.btnGrabar.Text = "&Grabar";
            this.btnGrabar.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnGrabar.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnGrabar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnGrabar.Click += new System.EventHandler(this.btnGrabar_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnSalir.Image = ((System.Drawing.Image)(resources.GetObject("btnSalir.Image")));
            this.btnSalir.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnSalir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(55, 62);
            this.btnSalir.Text = "&S a l i r";
            this.btnSalir.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnSalir.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // gpoServ
            // 
            this.gpoServ.Controls.Add(this.label13);
            this.gpoServ.Controls.Add(this.txtTOTAL);
            this.gpoServ.Controls.Add(this.label12);
            this.gpoServ.Controls.Add(this.txtPrecio_CIDPRODUCTO_SERV);
            this.gpoServ.Controls.Add(this.label11);
            this.gpoServ.Controls.Add(this.txtCantidad_CIDPRODUCTO_SERV);
            this.gpoServ.Controls.Add(this.cmbCIDPRODUCTO_SERV);
            this.gpoServ.Controls.Add(this.label9);
            this.gpoServ.Location = new System.Drawing.Point(12, 320);
            this.gpoServ.Name = "gpoServ";
            this.gpoServ.Size = new System.Drawing.Size(592, 126);
            this.gpoServ.TabIndex = 12;
            this.gpoServ.TabStop = false;
            this.gpoServ.Text = "Servicio COMERCIAL";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Blue;
            this.label12.Location = new System.Drawing.Point(313, 54);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(52, 18);
            this.label12.TabIndex = 100;
            this.label12.Text = "Precio:";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtPrecio_CIDPRODUCTO_SERV
            // 
            this.txtPrecio_CIDPRODUCTO_SERV.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtPrecio_CIDPRODUCTO_SERV.Location = new System.Drawing.Point(388, 50);
            this.txtPrecio_CIDPRODUCTO_SERV.Name = "txtPrecio_CIDPRODUCTO_SERV";
            this.txtPrecio_CIDPRODUCTO_SERV.Size = new System.Drawing.Size(136, 25);
            this.txtPrecio_CIDPRODUCTO_SERV.TabIndex = 99;
            this.txtPrecio_CIDPRODUCTO_SERV.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPrecio_CIDPRODUCTO_SERV_KeyPress);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Blue;
            this.label11.Location = new System.Drawing.Point(11, 54);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(73, 18);
            this.label11.TabIndex = 98;
            this.label11.Text = "Cantidad:";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtCantidad_CIDPRODUCTO_SERV
            // 
            this.txtCantidad_CIDPRODUCTO_SERV.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtCantidad_CIDPRODUCTO_SERV.Location = new System.Drawing.Point(151, 51);
            this.txtCantidad_CIDPRODUCTO_SERV.Name = "txtCantidad_CIDPRODUCTO_SERV";
            this.txtCantidad_CIDPRODUCTO_SERV.Size = new System.Drawing.Size(136, 25);
            this.txtCantidad_CIDPRODUCTO_SERV.TabIndex = 97;
            this.txtCantidad_CIDPRODUCTO_SERV.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCantidad_CIDPRODUCTO_SERV_KeyPress);
            // 
            // cmbCIDPRODUCTO_SERV
            // 
            this.cmbCIDPRODUCTO_SERV.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.cmbCIDPRODUCTO_SERV.FormattingEnabled = true;
            this.cmbCIDPRODUCTO_SERV.Location = new System.Drawing.Point(151, 19);
            this.cmbCIDPRODUCTO_SERV.Name = "cmbCIDPRODUCTO_SERV";
            this.cmbCIDPRODUCTO_SERV.Size = new System.Drawing.Size(373, 26);
            this.cmbCIDPRODUCTO_SERV.TabIndex = 96;
            this.cmbCIDPRODUCTO_SERV.KeyUp += new System.Windows.Forms.KeyEventHandler(this.cmbCIDPRODUCTO_SERV_KeyUp);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Blue;
            this.label9.Location = new System.Drawing.Point(15, 22);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(121, 18);
            this.label9.TabIndex = 95;
            this.label9.Text = "Servicio a Cobrar:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Blue;
            this.label13.Location = new System.Drawing.Point(313, 85);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(55, 18);
            this.label13.TabIndex = 102;
            this.label13.Text = "TOTAL";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtTOTAL
            // 
            this.txtTOTAL.Enabled = false;
            this.txtTOTAL.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtTOTAL.Location = new System.Drawing.Point(388, 81);
            this.txtTOTAL.Name = "txtTOTAL";
            this.txtTOTAL.Size = new System.Drawing.Size(136, 25);
            this.txtTOTAL.TabIndex = 101;
            // 
            // fCapturaServCOM
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(721, 458);
            this.Controls.Add(this.gpoServ);
            this.Controls.Add(this.ToolStripMenu);
            this.Controls.Add(this.gpoDatosOS);
            this.Name = "fCapturaServCOM";
            this.Text = "Servicio COMERCIAL";
            this.Load += new System.EventHandler(this.fCapturaServCOM_Load);
            this.gpoDatosOS.ResumeLayout(false);
            this.gpoDatosOS.PerformLayout();
            this.ToolStripMenu.ResumeLayout(false);
            this.ToolStripMenu.PerformLayout();
            this.gpoServ.ResumeLayout(false);
            this.gpoServ.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox gpoDatosOS;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtidOrdenSer;
        internal System.Windows.Forms.ToolStrip ToolStripMenu;
        private System.Windows.Forms.ToolStripButton btnGrabar;
        private System.Windows.Forms.ToolStripButton btnSalir;
        private System.Windows.Forms.TextBox txtIdEmpresa;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dtpFechaHora;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpFecha;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtidOrdenTrabajo;
        private System.Windows.Forms.TextBox txtIdEmpleadoEnc;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtTipoOrdenServ;
        private System.Windows.Forms.TextBox txtNotaDiagnostico;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtNotaRecepcion;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtidUnidadTrans;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtTipoUnidad;
        private System.Windows.Forms.GroupBox gpoServ;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtPrecio_CIDPRODUCTO_SERV;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtCantidad_CIDPRODUCTO_SERV;
        private System.Windows.Forms.ComboBox cmbCIDPRODUCTO_SERV;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtTOTAL;
    }
}