﻿namespace Fletera2.Procesos
{
    partial class fOtroConceptos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fOtroConceptos));
            this.ToolStripMenu = new System.Windows.Forms.ToolStrip();
            this.btnNuevo = new System.Windows.Forms.ToolStripButton();
            this.btnGrabar = new System.Windows.Forms.ToolStripButton();
            this.btnEditar = new System.Windows.Forms.ToolStripButton();
            this.btnReporte = new System.Windows.Forms.ToolStripButton();
            this.btnCancelar = new System.Windows.Forms.ToolStripButton();
            this.btnSalir = new System.Windows.Forms.ToolStripButton();
            this.label16 = new System.Windows.Forms.Label();
            this.txtNumFolio = new System.Windows.Forms.TextBox();
            this.txtNomOperador = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtidOperador = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNumGuiaId = new System.Windows.Forms.TextBox();
            this.cmbidConcepto = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.chkEnParcialidades = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtImporte = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtImporteApli = new System.Windows.Forms.TextBox();
            this.txtTractor = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtRemolque1 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtDolly = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtRemolque2 = new System.Windows.Forms.TextBox();
            this.dtpFechaHora = new System.Windows.Forms.DateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.dtpFecha = new System.Windows.Forms.DateTimePicker();
            this.label11 = new System.Windows.Forms.Label();
            this.txtNumLiquidacion = new System.Windows.Forms.TextBox();
            this.ToolStripMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // ToolStripMenu
            // 
            this.ToolStripMenu.AutoSize = false;
            this.ToolStripMenu.BackColor = System.Drawing.SystemColors.Control;
            this.ToolStripMenu.Font = new System.Drawing.Font("Berlin Sans FB", 11.25F);
            this.ToolStripMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnNuevo,
            this.btnGrabar,
            this.btnEditar,
            this.btnReporte,
            this.btnCancelar,
            this.btnSalir});
            this.ToolStripMenu.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.ToolStripMenu.Location = new System.Drawing.Point(0, 0);
            this.ToolStripMenu.Name = "ToolStripMenu";
            this.ToolStripMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.ToolStripMenu.Size = new System.Drawing.Size(638, 54);
            this.ToolStripMenu.TabIndex = 10;
            this.ToolStripMenu.Text = "ToolStrip1";
            // 
            // btnNuevo
            // 
            this.btnNuevo.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnNuevo.Image = ((System.Drawing.Image)(resources.GetObject("btnNuevo.Image")));
            this.btnNuevo.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnNuevo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(54, 51);
            this.btnNuevo.Text = "&Nuevo";
            this.btnNuevo.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnNuevo.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnNuevo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnNuevo.Click += new System.EventHandler(this.btnNuevo_Click);
            // 
            // btnGrabar
            // 
            this.btnGrabar.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnGrabar.Image = ((System.Drawing.Image)(resources.GetObject("btnGrabar.Image")));
            this.btnGrabar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnGrabar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnGrabar.Name = "btnGrabar";
            this.btnGrabar.Size = new System.Drawing.Size(60, 51);
            this.btnGrabar.Text = "&Grabar";
            this.btnGrabar.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnGrabar.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnGrabar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnGrabar.Click += new System.EventHandler(this.btnGrabar_Click);
            // 
            // btnEditar
            // 
            this.btnEditar.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnEditar.Image = ((System.Drawing.Image)(resources.GetObject("btnEditar.Image")));
            this.btnEditar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnEditar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(52, 51);
            this.btnEditar.Text = "&Editar";
            this.btnEditar.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnEditar.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnEditar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnEditar.Visible = false;
            // 
            // btnReporte
            // 
            this.btnReporte.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnReporte.Image = ((System.Drawing.Image)(resources.GetObject("btnReporte.Image")));
            this.btnReporte.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnReporte.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnReporte.Name = "btnReporte";
            this.btnReporte.Size = new System.Drawing.Size(65, 51);
            this.btnReporte.Text = "&Reporte";
            this.btnReporte.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnReporte.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnReporte.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnReporte.Visible = false;
            // 
            // btnCancelar
            // 
            this.btnCancelar.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnCancelar.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar.Image")));
            this.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnCancelar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(69, 51);
            this.btnCancelar.Text = "&Cancelar";
            this.btnCancelar.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnCancelar.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // btnSalir
            // 
            this.btnSalir.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnSalir.Image = ((System.Drawing.Image)(resources.GetObject("btnSalir.Image")));
            this.btnSalir.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnSalir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(55, 51);
            this.btnSalir.Text = "&S a l i r";
            this.btnSalir.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnSalir.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Blue;
            this.label16.Location = new System.Drawing.Point(12, 70);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(54, 18);
            this.label16.TabIndex = 34;
            this.label16.Text = "# Folio";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label16.Click += new System.EventHandler(this.label16_Click);
            // 
            // txtNumFolio
            // 
            this.txtNumFolio.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtNumFolio.Location = new System.Drawing.Point(115, 67);
            this.txtNumFolio.Name = "txtNumFolio";
            this.txtNumFolio.Size = new System.Drawing.Size(123, 25);
            this.txtNumFolio.TabIndex = 33;
            this.txtNumFolio.TextChanged += new System.EventHandler(this.txtNumFolio_TextChanged);
            // 
            // txtNomOperador
            // 
            this.txtNomOperador.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtNomOperador.Location = new System.Drawing.Point(244, 190);
            this.txtNomOperador.MaxLength = 100;
            this.txtNomOperador.Name = "txtNomOperador";
            this.txtNomOperador.Size = new System.Drawing.Size(318, 25);
            this.txtNomOperador.TabIndex = 32;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(12, 193);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 18);
            this.label1.TabIndex = 31;
            this.label1.Text = "Operador:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtidOperador
            // 
            this.txtidOperador.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtidOperador.Location = new System.Drawing.Point(115, 190);
            this.txtidOperador.Name = "txtidOperador";
            this.txtidOperador.Size = new System.Drawing.Size(123, 25);
            this.txtidOperador.TabIndex = 30;
            this.txtidOperador.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtidOperador_KeyUp);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Blue;
            this.label2.Location = new System.Drawing.Point(12, 154);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 18);
            this.label2.TabIndex = 36;
            this.label2.Text = "No. Viaje";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtNumGuiaId
            // 
            this.txtNumGuiaId.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtNumGuiaId.Location = new System.Drawing.Point(115, 151);
            this.txtNumGuiaId.Name = "txtNumGuiaId";
            this.txtNumGuiaId.Size = new System.Drawing.Size(123, 25);
            this.txtNumGuiaId.TabIndex = 35;
            this.txtNumGuiaId.TextChanged += new System.EventHandler(this.txtNumGuiaId_TextChanged);
            this.txtNumGuiaId.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtNumGuiaId_KeyUp);
            // 
            // cmbidConcepto
            // 
            this.cmbidConcepto.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.cmbidConcepto.FormattingEnabled = true;
            this.cmbidConcepto.Location = new System.Drawing.Point(115, 221);
            this.cmbidConcepto.Name = "cmbidConcepto";
            this.cmbidConcepto.Size = new System.Drawing.Size(228, 26);
            this.cmbidConcepto.TabIndex = 39;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Blue;
            this.label8.Location = new System.Drawing.Point(36, 223);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(40, 18);
            this.label8.TabIndex = 38;
            this.label8.Text = "Tipo:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chkEnParcialidades
            // 
            this.chkEnParcialidades.AutoSize = true;
            this.chkEnParcialidades.Location = new System.Drawing.Point(423, 230);
            this.chkEnParcialidades.Name = "chkEnParcialidades";
            this.chkEnParcialidades.Size = new System.Drawing.Size(139, 17);
            this.chkEnParcialidades.TabIndex = 40;
            this.chkEnParcialidades.Text = "Cobrar En Parcialidades";
            this.chkEnParcialidades.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Blue;
            this.label9.Location = new System.Drawing.Point(32, 256);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(74, 18);
            this.label9.TabIndex = 42;
            this.label9.Text = "Importe: $";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtImporte
            // 
            this.txtImporte.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtImporte.Location = new System.Drawing.Point(115, 253);
            this.txtImporte.Name = "txtImporte";
            this.txtImporte.Size = new System.Drawing.Size(123, 25);
            this.txtImporte.TabIndex = 41;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Blue;
            this.label3.Location = new System.Drawing.Point(247, 256);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(136, 18);
            this.label3.TabIndex = 44;
            this.label3.Text = "Importe Aplicado: $";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtImporteApli
            // 
            this.txtImporteApli.Enabled = false;
            this.txtImporteApli.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtImporteApli.Location = new System.Drawing.Point(385, 253);
            this.txtImporteApli.Name = "txtImporteApli";
            this.txtImporteApli.Size = new System.Drawing.Size(123, 25);
            this.txtImporteApli.TabIndex = 43;
            // 
            // txtTractor
            // 
            this.txtTractor.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtTractor.Location = new System.Drawing.Point(244, 151);
            this.txtTractor.Name = "txtTractor";
            this.txtTractor.Size = new System.Drawing.Size(75, 25);
            this.txtTractor.TabIndex = 45;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Blue;
            this.label4.Location = new System.Drawing.Point(244, 130);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 18);
            this.label4.TabIndex = 46;
            this.label4.Text = "Tractor";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Blue;
            this.label5.Location = new System.Drawing.Point(325, 130);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(75, 18);
            this.label5.TabIndex = 48;
            this.label5.Text = "Rem. 1";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtRemolque1
            // 
            this.txtRemolque1.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtRemolque1.Location = new System.Drawing.Point(325, 151);
            this.txtRemolque1.Name = "txtRemolque1";
            this.txtRemolque1.Size = new System.Drawing.Size(75, 25);
            this.txtRemolque1.TabIndex = 47;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Blue;
            this.label6.Location = new System.Drawing.Point(406, 130);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(75, 18);
            this.label6.TabIndex = 50;
            this.label6.Text = "Dolly";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtDolly
            // 
            this.txtDolly.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtDolly.Location = new System.Drawing.Point(406, 151);
            this.txtDolly.Name = "txtDolly";
            this.txtDolly.Size = new System.Drawing.Size(75, 25);
            this.txtDolly.TabIndex = 49;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Blue;
            this.label7.Location = new System.Drawing.Point(487, 130);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(75, 18);
            this.label7.TabIndex = 52;
            this.label7.Text = "Rem. 2";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtRemolque2
            // 
            this.txtRemolque2.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtRemolque2.Location = new System.Drawing.Point(487, 151);
            this.txtRemolque2.Name = "txtRemolque2";
            this.txtRemolque2.Size = new System.Drawing.Size(75, 25);
            this.txtRemolque2.TabIndex = 51;
            // 
            // dtpFechaHora
            // 
            this.dtpFechaHora.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.dtpFechaHora.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpFechaHora.Location = new System.Drawing.Point(384, 98);
            this.dtpFechaHora.Name = "dtpFechaHora";
            this.dtpFechaHora.Size = new System.Drawing.Size(106, 25);
            this.dtpFechaHora.TabIndex = 58;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Blue;
            this.label10.Location = new System.Drawing.Point(14, 103);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(52, 18);
            this.label10.TabIndex = 57;
            this.label10.Text = "Fecha:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label10.Click += new System.EventHandler(this.label10_Click);
            // 
            // dtpFecha
            // 
            this.dtpFecha.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.dtpFecha.Location = new System.Drawing.Point(115, 98);
            this.dtpFecha.Name = "dtpFecha";
            this.dtpFecha.Size = new System.Drawing.Size(263, 25);
            this.dtpFecha.TabIndex = 56;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Blue;
            this.label11.Location = new System.Drawing.Point(322, 60);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(108, 18);
            this.label11.TabIndex = 60;
            this.label11.Text = "No. Liquidación";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtNumLiquidacion
            // 
            this.txtNumLiquidacion.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtNumLiquidacion.Location = new System.Drawing.Point(439, 57);
            this.txtNumLiquidacion.Name = "txtNumLiquidacion";
            this.txtNumLiquidacion.Size = new System.Drawing.Size(123, 25);
            this.txtNumLiquidacion.TabIndex = 59;
            // 
            // fOtroConceptos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(638, 300);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtNumLiquidacion);
            this.Controls.Add(this.dtpFechaHora);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.dtpFecha);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtRemolque2);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtDolly);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtRemolque1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtTractor);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtImporteApli);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtImporte);
            this.Controls.Add(this.chkEnParcialidades);
            this.Controls.Add(this.cmbidConcepto);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtNumGuiaId);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.txtNumFolio);
            this.Controls.Add(this.txtNomOperador);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtidOperador);
            this.Controls.Add(this.ToolStripMenu);
            this.Name = "fOtroConceptos";
            this.Text = "fOtroConceptos";
            this.Load += new System.EventHandler(this.fOtroConceptos_Load);
            this.ToolStripMenu.ResumeLayout(false);
            this.ToolStripMenu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.ToolStrip ToolStripMenu;
        private System.Windows.Forms.ToolStripButton btnNuevo;
        private System.Windows.Forms.ToolStripButton btnGrabar;
        private System.Windows.Forms.ToolStripButton btnEditar;
        private System.Windows.Forms.ToolStripButton btnReporte;
        private System.Windows.Forms.ToolStripButton btnCancelar;
        private System.Windows.Forms.ToolStripButton btnSalir;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtNumFolio;
        private System.Windows.Forms.TextBox txtNomOperador;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtidOperador;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtNumGuiaId;
        private System.Windows.Forms.ComboBox cmbidConcepto;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox chkEnParcialidades;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtImporte;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtImporteApli;
        private System.Windows.Forms.TextBox txtTractor;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtRemolque1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtDolly;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtRemolque2;
        private System.Windows.Forms.DateTimePicker dtpFechaHora;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DateTimePicker dtpFecha;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtNumLiquidacion;
    }
}