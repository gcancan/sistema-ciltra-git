﻿namespace Fletera2.Procesos
{
    partial class fOrdenTrabajo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fOrdenTrabajo));
            this.ToolStripMenu = new System.Windows.Forms.ToolStrip();
            this.btnNuevo = new System.Windows.Forms.ToolStripButton();
            this.btnGrabar = new System.Windows.Forms.ToolStripButton();
            this.btnCancelar = new System.Windows.Forms.ToolStripButton();
            this.btnSalir = new System.Windows.Forms.ToolStripButton();
            this.gpoDatos = new System.Windows.Forms.GroupBox();
            this.txtTodo = new System.Windows.Forms.TextBox();
            this.cmbDivsiones = new System.Windows.Forms.ComboBox();
            this.lbldivision = new System.Windows.Forms.Label();
            this.cmbidLlanta = new System.Windows.Forms.ComboBox();
            this.lblnollanta = new System.Windows.Forms.Label();
            this.cmbPosicion = new System.Windows.Forms.ComboBox();
            this.lblposllanta = new System.Windows.Forms.Label();
            this.lblTipoOS = new System.Windows.Forms.Label();
            this.lblServicios = new System.Windows.Forms.Label();
            this.cmbOpciones = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbidUnidadTrans = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtFallaReportada = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.grdDet = new System.Windows.Forms.DataGridView();
            this.GpoPosiciones = new System.Windows.Forms.GroupBox();
            this.gpoOs = new System.Windows.Forms.GroupBox();
            this.btnAsigna = new System.Windows.Forms.Button();
            this.cmbDetOpera3 = new System.Windows.Forms.ComboBox();
            this.cmbDetOpera2 = new System.Windows.Forms.ComboBox();
            this.cmbDetOpera1 = new System.Windows.Forms.ComboBox();
            this.chkOpera3 = new System.Windows.Forms.CheckBox();
            this.chkOpera2 = new System.Windows.Forms.CheckBox();
            this.lblopera3 = new System.Windows.Forms.Label();
            this.lblopera2 = new System.Windows.Forms.Label();
            this.cmbOpera3 = new System.Windows.Forms.ComboBox();
            this.cmbOpera2 = new System.Windows.Forms.ComboBox();
            this.cmbOpera1 = new System.Windows.Forms.ComboBox();
            this.lblopera1 = new System.Windows.Forms.Label();
            this.grdCab = new System.Windows.Forms.DataGridView();
            this.ToolStripMenu.SuspendLayout();
            this.gpoDatos.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdDet)).BeginInit();
            this.gpoOs.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdCab)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolStripMenu
            // 
            this.ToolStripMenu.AutoSize = false;
            this.ToolStripMenu.BackColor = System.Drawing.SystemColors.Control;
            this.ToolStripMenu.Font = new System.Drawing.Font("Berlin Sans FB", 11.25F);
            this.ToolStripMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnNuevo,
            this.btnGrabar,
            this.btnCancelar,
            this.btnSalir});
            this.ToolStripMenu.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.ToolStripMenu.Location = new System.Drawing.Point(0, 0);
            this.ToolStripMenu.Name = "ToolStripMenu";
            this.ToolStripMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.ToolStripMenu.Size = new System.Drawing.Size(1153, 54);
            this.ToolStripMenu.TabIndex = 18;
            this.ToolStripMenu.Text = "ToolStrip1";
            // 
            // btnNuevo
            // 
            this.btnNuevo.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnNuevo.Image = ((System.Drawing.Image)(resources.GetObject("btnNuevo.Image")));
            this.btnNuevo.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnNuevo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(54, 51);
            this.btnNuevo.Text = "&Nuevo";
            this.btnNuevo.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnNuevo.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnNuevo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnNuevo.Click += new System.EventHandler(this.btnNuevo_Click);
            // 
            // btnGrabar
            // 
            this.btnGrabar.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnGrabar.Image = ((System.Drawing.Image)(resources.GetObject("btnGrabar.Image")));
            this.btnGrabar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnGrabar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnGrabar.Name = "btnGrabar";
            this.btnGrabar.Size = new System.Drawing.Size(60, 51);
            this.btnGrabar.Text = "&Grabar";
            this.btnGrabar.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnGrabar.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnGrabar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnGrabar.Click += new System.EventHandler(this.btnGrabar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnCancelar.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar.Image")));
            this.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnCancelar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(69, 51);
            this.btnCancelar.Text = "&Cancelar";
            this.btnCancelar.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnCancelar.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnSalir.Image = ((System.Drawing.Image)(resources.GetObject("btnSalir.Image")));
            this.btnSalir.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnSalir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(55, 51);
            this.btnSalir.Text = "&S a l i r";
            this.btnSalir.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnSalir.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // gpoDatos
            // 
            this.gpoDatos.Controls.Add(this.txtTodo);
            this.gpoDatos.Controls.Add(this.cmbDivsiones);
            this.gpoDatos.Controls.Add(this.lbldivision);
            this.gpoDatos.Controls.Add(this.cmbidLlanta);
            this.gpoDatos.Controls.Add(this.lblnollanta);
            this.gpoDatos.Controls.Add(this.cmbPosicion);
            this.gpoDatos.Controls.Add(this.lblposllanta);
            this.gpoDatos.Controls.Add(this.lblTipoOS);
            this.gpoDatos.Controls.Add(this.lblServicios);
            this.gpoDatos.Controls.Add(this.cmbOpciones);
            this.gpoDatos.Controls.Add(this.label3);
            this.gpoDatos.Controls.Add(this.cmbidUnidadTrans);
            this.gpoDatos.Controls.Add(this.label8);
            this.gpoDatos.Controls.Add(this.label1);
            this.gpoDatos.Controls.Add(this.txtFallaReportada);
            this.gpoDatos.Location = new System.Drawing.Point(0, 57);
            this.gpoDatos.Name = "gpoDatos";
            this.gpoDatos.Size = new System.Drawing.Size(706, 161);
            this.gpoDatos.TabIndex = 19;
            this.gpoDatos.TabStop = false;
            this.gpoDatos.Text = "Datos";
            // 
            // txtTodo
            // 
            this.txtTodo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTodo.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtTodo.Location = new System.Drawing.Point(173, 87);
            this.txtTodo.MaxLength = 150;
            this.txtTodo.Name = "txtTodo";
            this.txtTodo.Size = new System.Drawing.Size(523, 25);
            this.txtTodo.TabIndex = 84;
            this.txtTodo.TextChanged += new System.EventHandler(this.txtTodo_TextChanged);
            this.txtTodo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTodo_KeyDown);
            this.txtTodo.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtTodo_KeyUp);
            // 
            // cmbDivsiones
            // 
            this.cmbDivsiones.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.cmbDivsiones.FormattingEnabled = true;
            this.cmbDivsiones.Location = new System.Drawing.Point(173, 150);
            this.cmbDivsiones.Name = "cmbDivsiones";
            this.cmbDivsiones.Size = new System.Drawing.Size(238, 26);
            this.cmbDivsiones.TabIndex = 7;
            // 
            // lbldivision
            // 
            this.lbldivision.AutoSize = true;
            this.lbldivision.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbldivision.ForeColor = System.Drawing.Color.Blue;
            this.lbldivision.Location = new System.Drawing.Point(12, 153);
            this.lbldivision.Name = "lbldivision";
            this.lbldivision.Size = new System.Drawing.Size(60, 18);
            this.lbldivision.TabIndex = 83;
            this.lbldivision.Text = "División:";
            this.lbldivision.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cmbidLlanta
            // 
            this.cmbidLlanta.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbidLlanta.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.cmbidLlanta.FormattingEnabled = true;
            this.cmbidLlanta.Location = new System.Drawing.Point(616, 50);
            this.cmbidLlanta.Name = "cmbidLlanta";
            this.cmbidLlanta.Size = new System.Drawing.Size(80, 26);
            this.cmbidLlanta.TabIndex = 4;
            this.cmbidLlanta.SelectedIndexChanged += new System.EventHandler(this.cmbidLlanta_SelectedIndexChanged);
            this.cmbidLlanta.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbidLlanta_KeyPress);
            // 
            // lblnollanta
            // 
            this.lblnollanta.AutoSize = true;
            this.lblnollanta.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblnollanta.ForeColor = System.Drawing.Color.Blue;
            this.lblnollanta.Location = new System.Drawing.Point(534, 53);
            this.lblnollanta.Name = "lblnollanta";
            this.lblnollanta.Size = new System.Drawing.Size(76, 18);
            this.lblnollanta.TabIndex = 81;
            this.lblnollanta.Text = "No. Llanta";
            this.lblnollanta.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cmbPosicion
            // 
            this.cmbPosicion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPosicion.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.cmbPosicion.Location = new System.Drawing.Point(478, 51);
            this.cmbPosicion.Name = "cmbPosicion";
            this.cmbPosicion.Size = new System.Drawing.Size(50, 26);
            this.cmbPosicion.TabIndex = 3;
            this.cmbPosicion.SelectedIndexChanged += new System.EventHandler(this.cmbPosicion_SelectedIndexChanged);
            this.cmbPosicion.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbPosicion_KeyPress);
            // 
            // lblposllanta
            // 
            this.lblposllanta.AutoSize = true;
            this.lblposllanta.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblposllanta.ForeColor = System.Drawing.Color.Blue;
            this.lblposllanta.Location = new System.Drawing.Point(417, 59);
            this.lblposllanta.Name = "lblposllanta";
            this.lblposllanta.Size = new System.Drawing.Size(63, 18);
            this.lblposllanta.TabIndex = 79;
            this.lblposllanta.Text = "Posición:";
            this.lblposllanta.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTipoOS
            // 
            this.lblTipoOS.AutoSize = true;
            this.lblTipoOS.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTipoOS.ForeColor = System.Drawing.Color.Blue;
            this.lblTipoOS.Location = new System.Drawing.Point(456, 23);
            this.lblTipoOS.Name = "lblTipoOS";
            this.lblTipoOS.Size = new System.Drawing.Size(175, 18);
            this.lblTipoOS.TabIndex = 77;
            this.lblTipoOS.Text = "Tipo de Orden de Servicio";
            this.lblTipoOS.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblServicios
            // 
            this.lblServicios.AutoSize = true;
            this.lblServicios.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblServicios.ForeColor = System.Drawing.Color.Blue;
            this.lblServicios.Location = new System.Drawing.Point(12, 90);
            this.lblServicios.Name = "lblServicios";
            this.lblServicios.Size = new System.Drawing.Size(60, 18);
            this.lblServicios.TabIndex = 73;
            this.lblServicios.Text = "Servicios";
            this.lblServicios.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cmbOpciones
            // 
            this.cmbOpciones.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.cmbOpciones.FormattingEnabled = true;
            this.cmbOpciones.Location = new System.Drawing.Point(173, 51);
            this.cmbOpciones.Name = "cmbOpciones";
            this.cmbOpciones.Size = new System.Drawing.Size(238, 26);
            this.cmbOpciones.TabIndex = 2;
            this.cmbOpciones.SelectedIndexChanged += new System.EventHandler(this.cmbOpciones_SelectedIndexChanged);
            this.cmbOpciones.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbOpciones_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Blue;
            this.label3.Location = new System.Drawing.Point(12, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(133, 18);
            this.label3.TabIndex = 71;
            this.label3.Text = "Tipo Orden Servicio";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cmbidUnidadTrans
            // 
            this.cmbidUnidadTrans.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.cmbidUnidadTrans.FormattingEnabled = true;
            this.cmbidUnidadTrans.Location = new System.Drawing.Point(173, 19);
            this.cmbidUnidadTrans.Name = "cmbidUnidadTrans";
            this.cmbidUnidadTrans.Size = new System.Drawing.Size(238, 26);
            this.cmbidUnidadTrans.TabIndex = 1;
            this.cmbidUnidadTrans.SelectedIndexChanged += new System.EventHandler(this.cmbidUnidadTrans_SelectedIndexChanged);
            this.cmbidUnidadTrans.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbidUnidadTrans_KeyPress);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Blue;
            this.label8.Location = new System.Drawing.Point(11, 22);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(156, 18);
            this.label8.TabIndex = 21;
            this.label8.Text = "Unidad de Transporte:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(12, 126);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(117, 18);
            this.label1.TabIndex = 7;
            this.label1.Text = "Falla Reportada";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtFallaReportada
            // 
            this.txtFallaReportada.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFallaReportada.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtFallaReportada.Location = new System.Drawing.Point(173, 119);
            this.txtFallaReportada.MaxLength = 150;
            this.txtFallaReportada.Name = "txtFallaReportada";
            this.txtFallaReportada.Size = new System.Drawing.Size(523, 25);
            this.txtFallaReportada.TabIndex = 6;
            this.txtFallaReportada.TextChanged += new System.EventHandler(this.txtFallaReportada_TextChanged);
            this.txtFallaReportada.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFallaReportada_KeyPress);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.grdDet);
            this.groupBox1.Location = new System.Drawing.Point(0, 224);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(706, 170);
            this.groupBox1.TabIndex = 21;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "ORDENES DE TRABAJO";
            // 
            // grdDet
            // 
            this.grdDet.AllowUserToAddRows = false;
            this.grdDet.AllowUserToDeleteRows = false;
            this.grdDet.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdDet.Location = new System.Drawing.Point(6, 19);
            this.grdDet.Name = "grdDet";
            this.grdDet.ShowEditingIcon = false;
            this.grdDet.Size = new System.Drawing.Size(690, 149);
            this.grdDet.TabIndex = 24;
            this.grdDet.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdDet_CellContentClick);
            // 
            // GpoPosiciones
            // 
            this.GpoPosiciones.Location = new System.Drawing.Point(712, 57);
            this.GpoPosiciones.Name = "GpoPosiciones";
            this.GpoPosiciones.Size = new System.Drawing.Size(427, 405);
            this.GpoPosiciones.TabIndex = 108;
            this.GpoPosiciones.TabStop = false;
            this.GpoPosiciones.Text = "Posiciones";
            // 
            // gpoOs
            // 
            this.gpoOs.Controls.Add(this.btnAsigna);
            this.gpoOs.Controls.Add(this.cmbDetOpera3);
            this.gpoOs.Controls.Add(this.cmbDetOpera2);
            this.gpoOs.Controls.Add(this.cmbDetOpera1);
            this.gpoOs.Controls.Add(this.chkOpera3);
            this.gpoOs.Controls.Add(this.chkOpera2);
            this.gpoOs.Controls.Add(this.lblopera3);
            this.gpoOs.Controls.Add(this.lblopera2);
            this.gpoOs.Controls.Add(this.cmbOpera3);
            this.gpoOs.Controls.Add(this.cmbOpera2);
            this.gpoOs.Controls.Add(this.cmbOpera1);
            this.gpoOs.Controls.Add(this.lblopera1);
            this.gpoOs.Controls.Add(this.grdCab);
            this.gpoOs.Location = new System.Drawing.Point(6, 400);
            this.gpoOs.Name = "gpoOs";
            this.gpoOs.Size = new System.Drawing.Size(706, 115);
            this.gpoOs.TabIndex = 109;
            this.gpoOs.TabStop = false;
            this.gpoOs.Text = "ASIGNA TECNICOS";
            // 
            // btnAsigna
            // 
            this.btnAsigna.Enabled = false;
            this.btnAsigna.Image = global::Fletera2.Properties.Resources.AddMecanico;
            this.btnAsigna.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnAsigna.Location = new System.Drawing.Point(627, 14);
            this.btnAsigna.Name = "btnAsigna";
            this.btnAsigna.Size = new System.Drawing.Size(41, 39);
            this.btnAsigna.TabIndex = 100;
            this.btnAsigna.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnAsigna.UseVisualStyleBackColor = true;
            // 
            // cmbDetOpera3
            // 
            this.cmbDetOpera3.Enabled = false;
            this.cmbDetOpera3.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.cmbDetOpera3.FormattingEnabled = true;
            this.cmbDetOpera3.Location = new System.Drawing.Point(431, 79);
            this.cmbDetOpera3.Name = "cmbDetOpera3";
            this.cmbDetOpera3.Size = new System.Drawing.Size(180, 26);
            this.cmbDetOpera3.TabIndex = 99;
            // 
            // cmbDetOpera2
            // 
            this.cmbDetOpera2.Enabled = false;
            this.cmbDetOpera2.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.cmbDetOpera2.FormattingEnabled = true;
            this.cmbDetOpera2.Location = new System.Drawing.Point(431, 48);
            this.cmbDetOpera2.Name = "cmbDetOpera2";
            this.cmbDetOpera2.Size = new System.Drawing.Size(180, 26);
            this.cmbDetOpera2.TabIndex = 98;
            // 
            // cmbDetOpera1
            // 
            this.cmbDetOpera1.Enabled = false;
            this.cmbDetOpera1.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.cmbDetOpera1.FormattingEnabled = true;
            this.cmbDetOpera1.Location = new System.Drawing.Point(431, 15);
            this.cmbDetOpera1.Name = "cmbDetOpera1";
            this.cmbDetOpera1.Size = new System.Drawing.Size(180, 26);
            this.cmbDetOpera1.TabIndex = 97;
            // 
            // chkOpera3
            // 
            this.chkOpera3.AutoSize = true;
            this.chkOpera3.Location = new System.Drawing.Point(627, 86);
            this.chkOpera3.Name = "chkOpera3";
            this.chkOpera3.Size = new System.Drawing.Size(15, 14);
            this.chkOpera3.TabIndex = 96;
            this.chkOpera3.UseVisualStyleBackColor = true;
            // 
            // chkOpera2
            // 
            this.chkOpera2.AutoSize = true;
            this.chkOpera2.Location = new System.Drawing.Point(627, 59);
            this.chkOpera2.Name = "chkOpera2";
            this.chkOpera2.Size = new System.Drawing.Size(15, 14);
            this.chkOpera2.TabIndex = 95;
            this.chkOpera2.UseVisualStyleBackColor = true;
            // 
            // lblopera3
            // 
            this.lblopera3.AutoSize = true;
            this.lblopera3.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblopera3.ForeColor = System.Drawing.Color.Blue;
            this.lblopera3.Location = new System.Drawing.Point(7, 83);
            this.lblopera3.Name = "lblopera3";
            this.lblopera3.Size = new System.Drawing.Size(70, 18);
            this.lblopera3.TabIndex = 94;
            this.lblopera3.Text = "lblopera3";
            this.lblopera3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblopera2
            // 
            this.lblopera2.AutoSize = true;
            this.lblopera2.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblopera2.ForeColor = System.Drawing.Color.Blue;
            this.lblopera2.Location = new System.Drawing.Point(7, 51);
            this.lblopera2.Name = "lblopera2";
            this.lblopera2.Size = new System.Drawing.Size(70, 18);
            this.lblopera2.TabIndex = 93;
            this.lblopera2.Text = "lblopera2";
            this.lblopera2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cmbOpera3
            // 
            this.cmbOpera3.Enabled = false;
            this.cmbOpera3.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.cmbOpera3.FormattingEnabled = true;
            this.cmbOpera3.Location = new System.Drawing.Point(118, 79);
            this.cmbOpera3.Name = "cmbOpera3";
            this.cmbOpera3.Size = new System.Drawing.Size(307, 26);
            this.cmbOpera3.TabIndex = 92;
            // 
            // cmbOpera2
            // 
            this.cmbOpera2.Enabled = false;
            this.cmbOpera2.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.cmbOpera2.FormattingEnabled = true;
            this.cmbOpera2.Location = new System.Drawing.Point(118, 47);
            this.cmbOpera2.Name = "cmbOpera2";
            this.cmbOpera2.Size = new System.Drawing.Size(307, 26);
            this.cmbOpera2.TabIndex = 91;
            // 
            // cmbOpera1
            // 
            this.cmbOpera1.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.cmbOpera1.FormattingEnabled = true;
            this.cmbOpera1.Location = new System.Drawing.Point(118, 15);
            this.cmbOpera1.Name = "cmbOpera1";
            this.cmbOpera1.Size = new System.Drawing.Size(307, 26);
            this.cmbOpera1.TabIndex = 89;
            // 
            // lblopera1
            // 
            this.lblopera1.AutoSize = true;
            this.lblopera1.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblopera1.ForeColor = System.Drawing.Color.Blue;
            this.lblopera1.Location = new System.Drawing.Point(8, 18);
            this.lblopera1.Name = "lblopera1";
            this.lblopera1.Size = new System.Drawing.Size(66, 18);
            this.lblopera1.TabIndex = 90;
            this.lblopera1.Text = "lblopera1";
            this.lblopera1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // grdCab
            // 
            this.grdCab.AllowUserToAddRows = false;
            this.grdCab.AllowUserToDeleteRows = false;
            this.grdCab.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdCab.Location = new System.Drawing.Point(648, 53);
            this.grdCab.Name = "grdCab";
            this.grdCab.Size = new System.Drawing.Size(52, 52);
            this.grdCab.TabIndex = 22;
            // 
            // fOrdenTrabajo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1153, 527);
            this.Controls.Add(this.gpoOs);
            this.Controls.Add(this.GpoPosiciones);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gpoDatos);
            this.Controls.Add(this.ToolStripMenu);
            this.Name = "fOrdenTrabajo";
            this.Text = "Ordenes de Trabajo";
            this.Load += new System.EventHandler(this.fOrdenTrabajo_Load);
            this.ToolStripMenu.ResumeLayout(false);
            this.ToolStripMenu.PerformLayout();
            this.gpoDatos.ResumeLayout(false);
            this.gpoDatos.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdDet)).EndInit();
            this.gpoOs.ResumeLayout(false);
            this.gpoOs.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdCab)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.ToolStrip ToolStripMenu;
        private System.Windows.Forms.ToolStripButton btnNuevo;
        private System.Windows.Forms.ToolStripButton btnGrabar;
        private System.Windows.Forms.ToolStripButton btnCancelar;
        private System.Windows.Forms.ToolStripButton btnSalir;
        private System.Windows.Forms.GroupBox gpoDatos;
        private System.Windows.Forms.Label lblServicios;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbidUnidadTrans;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtFallaReportada;
        private System.Windows.Forms.Label lblTipoOS;
        private System.Windows.Forms.ComboBox cmbPosicion;
        private System.Windows.Forms.Label lblposllanta;
        private System.Windows.Forms.ComboBox cmbidLlanta;
        private System.Windows.Forms.Label lblnollanta;
        private System.Windows.Forms.ComboBox cmbDivsiones;
        private System.Windows.Forms.Label lbldivision;
        private System.Windows.Forms.ComboBox cmbOpciones;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView grdDet;
        private System.Windows.Forms.TextBox txtTodo;
        internal System.Windows.Forms.GroupBox GpoPosiciones;
        private System.Windows.Forms.GroupBox gpoOs;
        private System.Windows.Forms.Button btnAsigna;
        private System.Windows.Forms.ComboBox cmbDetOpera3;
        private System.Windows.Forms.ComboBox cmbDetOpera2;
        private System.Windows.Forms.ComboBox cmbDetOpera1;
        private System.Windows.Forms.CheckBox chkOpera3;
        private System.Windows.Forms.CheckBox chkOpera2;
        private System.Windows.Forms.Label lblopera3;
        private System.Windows.Forms.Label lblopera2;
        private System.Windows.Forms.ComboBox cmbOpera3;
        private System.Windows.Forms.ComboBox cmbOpera2;
        private System.Windows.Forms.ComboBox cmbOpera1;
        private System.Windows.Forms.Label lblopera1;
        private System.Windows.Forms.DataGridView grdCab;
    }
}