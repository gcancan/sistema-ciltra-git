﻿namespace Fletera2.Procesos
{
    partial class fFinalizaOs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fFinalizaOs));
            this.label1 = new System.Windows.Forms.Label();
            this.txtidOrdenSer = new System.Windows.Forms.TextBox();
            this.ToolStripMenu = new System.Windows.Forms.ToolStrip();
            this.btnNuevo = new System.Windows.Forms.ToolStripButton();
            this.btnGrabar = new System.Windows.Forms.ToolStripButton();
            this.btnEditar = new System.Windows.Forms.ToolStripButton();
            this.btnReporte = new System.Windows.Forms.ToolStripButton();
            this.btnCancelar = new System.Windows.Forms.ToolStripButton();
            this.btnSalir = new System.Windows.Forms.ToolStripButton();
            this.label4 = new System.Windows.Forms.Label();
            this.dtpFechaHora = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpFecha = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.txtTipoUnidad = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtTipoOrdenServ = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cmbMetodoPago = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.cmbUsoCFDI = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.cmbFormaPago = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.grdInsumosConc = new System.Windows.Forms.DataGridView();
            this.gpoImportes = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtTotalhr = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtTotTOTAL = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtTotSubTotal = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtTotIVA = new System.Windows.Forms.TextBox();
            this.lblManoObra = new System.Windows.Forms.Label();
            this.txtTotManoObra = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtTotRefacciones = new System.Windows.Forms.TextBox();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.grdDetOs = new System.Windows.Forms.DataGridView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.grdRefacciones = new System.Windows.Forms.DataGridView();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.grdDetOC = new System.Windows.Forms.DataGridView();
            this.grdCabOC = new System.Windows.Forms.DataGridView();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.grdDetPreSalida = new System.Windows.Forms.DataGridView();
            this.grdCabPreSalida = new System.Windows.Forms.DataGridView();
            this.label15 = new System.Windows.Forms.Label();
            this.txtidEmpleadoEntrega = new System.Windows.Forms.TextBox();
            this.txtidUnidadTrans = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtEstatus = new System.Windows.Forms.TextBox();
            this.txtIdEmpresa = new System.Windows.Forms.TextBox();
            this.cmbConcepto = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.ToolStripMenu.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdInsumosConc)).BeginInit();
            this.gpoImportes.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdDetOs)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdRefacciones)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdDetOC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdCabOC)).BeginInit();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdDetPreSalida)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdCabPreSalida)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(9, 70);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 18);
            this.label1.TabIndex = 7;
            this.label1.Text = "Orden. Servicio";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtidOrdenSer
            // 
            this.txtidOrdenSer.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtidOrdenSer.Location = new System.Drawing.Point(116, 68);
            this.txtidOrdenSer.Name = "txtidOrdenSer";
            this.txtidOrdenSer.Size = new System.Drawing.Size(108, 25);
            this.txtidOrdenSer.TabIndex = 6;
            this.txtidOrdenSer.TextChanged += new System.EventHandler(this.txtidOrdenSer_TextChanged);
            this.txtidOrdenSer.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtidOrdenSer_KeyPress);
            // 
            // ToolStripMenu
            // 
            this.ToolStripMenu.AutoSize = false;
            this.ToolStripMenu.BackColor = System.Drawing.SystemColors.Control;
            this.ToolStripMenu.Font = new System.Drawing.Font("Berlin Sans FB", 11.25F);
            this.ToolStripMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnNuevo,
            this.btnGrabar,
            this.btnEditar,
            this.btnReporte,
            this.btnCancelar,
            this.btnSalir});
            this.ToolStripMenu.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.ToolStripMenu.Location = new System.Drawing.Point(0, 0);
            this.ToolStripMenu.Name = "ToolStripMenu";
            this.ToolStripMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.ToolStripMenu.Size = new System.Drawing.Size(856, 65);
            this.ToolStripMenu.TabIndex = 9;
            this.ToolStripMenu.Text = "ToolStrip1";
            // 
            // btnNuevo
            // 
            this.btnNuevo.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnNuevo.Image = ((System.Drawing.Image)(resources.GetObject("btnNuevo.Image")));
            this.btnNuevo.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnNuevo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(54, 62);
            this.btnNuevo.Text = "&Nuevo";
            this.btnNuevo.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnNuevo.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnNuevo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnNuevo.Click += new System.EventHandler(this.btnNuevo_Click);
            // 
            // btnGrabar
            // 
            this.btnGrabar.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnGrabar.Image = ((System.Drawing.Image)(resources.GetObject("btnGrabar.Image")));
            this.btnGrabar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnGrabar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnGrabar.Name = "btnGrabar";
            this.btnGrabar.Size = new System.Drawing.Size(68, 62);
            this.btnGrabar.Text = "&Facturar";
            this.btnGrabar.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnGrabar.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnGrabar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnGrabar.Click += new System.EventHandler(this.btnGrabar_Click);
            // 
            // btnEditar
            // 
            this.btnEditar.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnEditar.Image = ((System.Drawing.Image)(resources.GetObject("btnEditar.Image")));
            this.btnEditar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnEditar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(52, 62);
            this.btnEditar.Text = "&Editar";
            this.btnEditar.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnEditar.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnEditar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnEditar.Visible = false;
            // 
            // btnReporte
            // 
            this.btnReporte.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnReporte.Image = ((System.Drawing.Image)(resources.GetObject("btnReporte.Image")));
            this.btnReporte.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnReporte.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnReporte.Name = "btnReporte";
            this.btnReporte.Size = new System.Drawing.Size(65, 62);
            this.btnReporte.Text = "&Reporte";
            this.btnReporte.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnReporte.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnReporte.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnReporte.Visible = false;
            // 
            // btnCancelar
            // 
            this.btnCancelar.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnCancelar.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar.Image")));
            this.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnCancelar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(69, 62);
            this.btnCancelar.Text = "&Cancelar";
            this.btnCancelar.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnCancelar.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnSalir.Image = ((System.Drawing.Image)(resources.GetObject("btnSalir.Image")));
            this.btnSalir.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnSalir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(55, 62);
            this.btnSalir.Text = "&S a l i r";
            this.btnSalir.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnSalir.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Blue;
            this.label4.Location = new System.Drawing.Point(248, 70);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 18);
            this.label4.TabIndex = 13;
            this.label4.Text = "Empresa";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dtpFechaHora
            // 
            this.dtpFechaHora.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.dtpFechaHora.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpFechaHora.Location = new System.Drawing.Point(370, 99);
            this.dtpFechaHora.Name = "dtpFechaHora";
            this.dtpFechaHora.Size = new System.Drawing.Size(106, 25);
            this.dtpFechaHora.TabIndex = 17;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Blue;
            this.label2.Location = new System.Drawing.Point(23, 104);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 18);
            this.label2.TabIndex = 16;
            this.label2.Text = "Fecha:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dtpFecha
            // 
            this.dtpFecha.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.dtpFecha.Location = new System.Drawing.Point(101, 99);
            this.dtpFecha.Name = "dtpFecha";
            this.dtpFecha.Size = new System.Drawing.Size(263, 25);
            this.dtpFecha.TabIndex = 15;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Blue;
            this.label3.Location = new System.Drawing.Point(482, 104);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(135, 18);
            this.label3.TabIndex = 54;
            this.label3.Text = "Unidad Transporte:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // txtTipoUnidad
            // 
            this.txtTipoUnidad.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtTipoUnidad.Location = new System.Drawing.Point(622, 101);
            this.txtTipoUnidad.Name = "txtTipoUnidad";
            this.txtTipoUnidad.Size = new System.Drawing.Size(136, 25);
            this.txtTipoUnidad.TabIndex = 53;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Blue;
            this.label8.Location = new System.Drawing.Point(556, 70);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(154, 18);
            this.label8.TabIndex = 62;
            this.label8.Text = "Tipo de Orden Servicio";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtTipoOrdenServ
            // 
            this.txtTipoOrdenServ.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtTipoOrdenServ.Location = new System.Drawing.Point(716, 63);
            this.txtTipoOrdenServ.Name = "txtTipoOrdenServ";
            this.txtTipoOrdenServ.Size = new System.Drawing.Size(123, 25);
            this.txtTipoOrdenServ.TabIndex = 61;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Location = new System.Drawing.Point(12, 208);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(795, 420);
            this.tabControl1.TabIndex = 64;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.groupBox1);
            this.tabPage3.Controls.Add(this.grdInsumosConc);
            this.tabPage3.Controls.Add(this.gpoImportes);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(787, 394);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Resumen";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cmbMetodoPago);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.cmbUsoCFDI);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.cmbFormaPago);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Location = new System.Drawing.Point(374, 15);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(358, 176);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "ANEXO 20";
            // 
            // cmbMetodoPago
            // 
            this.cmbMetodoPago.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.cmbMetodoPago.FormattingEnabled = true;
            this.cmbMetodoPago.Location = new System.Drawing.Point(126, 90);
            this.cmbMetodoPago.Name = "cmbMetodoPago";
            this.cmbMetodoPago.Size = new System.Drawing.Size(226, 26);
            this.cmbMetodoPago.TabIndex = 78;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Blue;
            this.label13.Location = new System.Drawing.Point(4, 93);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(121, 18);
            this.label13.TabIndex = 77;
            this.label13.Text = "Método de Pago:";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cmbUsoCFDI
            // 
            this.cmbUsoCFDI.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.cmbUsoCFDI.FormattingEnabled = true;
            this.cmbUsoCFDI.Location = new System.Drawing.Point(126, 58);
            this.cmbUsoCFDI.Name = "cmbUsoCFDI";
            this.cmbUsoCFDI.Size = new System.Drawing.Size(226, 26);
            this.cmbUsoCFDI.TabIndex = 76;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Blue;
            this.label12.Location = new System.Drawing.Point(4, 61);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(89, 18);
            this.label12.TabIndex = 75;
            this.label12.Text = "Uso de CFDI";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cmbFormaPago
            // 
            this.cmbFormaPago.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.cmbFormaPago.FormattingEnabled = true;
            this.cmbFormaPago.Location = new System.Drawing.Point(126, 22);
            this.cmbFormaPago.Name = "cmbFormaPago";
            this.cmbFormaPago.Size = new System.Drawing.Size(226, 26);
            this.cmbFormaPago.TabIndex = 74;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Blue;
            this.label11.Location = new System.Drawing.Point(4, 25);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(112, 18);
            this.label11.TabIndex = 73;
            this.label11.Text = "Forma de Pago";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // grdInsumosConc
            // 
            this.grdInsumosConc.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdInsumosConc.Location = new System.Drawing.Point(7, 197);
            this.grdInsumosConc.Name = "grdInsumosConc";
            this.grdInsumosConc.Size = new System.Drawing.Size(771, 191);
            this.grdInsumosConc.TabIndex = 13;
            this.grdInsumosConc.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdInsumosConc_CellContentClick);
            // 
            // gpoImportes
            // 
            this.gpoImportes.Controls.Add(this.label10);
            this.gpoImportes.Controls.Add(this.txtTotalhr);
            this.gpoImportes.Controls.Add(this.label6);
            this.gpoImportes.Controls.Add(this.txtTotTOTAL);
            this.gpoImportes.Controls.Add(this.label7);
            this.gpoImportes.Controls.Add(this.txtTotSubTotal);
            this.gpoImportes.Controls.Add(this.label16);
            this.gpoImportes.Controls.Add(this.txtTotIVA);
            this.gpoImportes.Controls.Add(this.lblManoObra);
            this.gpoImportes.Controls.Add(this.txtTotManoObra);
            this.gpoImportes.Controls.Add(this.label18);
            this.gpoImportes.Controls.Add(this.txtTotRefacciones);
            this.gpoImportes.Location = new System.Drawing.Point(10, 15);
            this.gpoImportes.Name = "gpoImportes";
            this.gpoImportes.Size = new System.Drawing.Size(358, 176);
            this.gpoImportes.TabIndex = 12;
            this.gpoImportes.TabStop = false;
            this.gpoImportes.Text = "IMPORTES";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Blue;
            this.label10.Location = new System.Drawing.Point(77, 53);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(32, 18);
            this.label10.TabIndex = 38;
            this.label10.Text = "Hrs.";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtTotalhr
            // 
            this.txtTotalhr.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtTotalhr.Location = new System.Drawing.Point(19, 50);
            this.txtTotalhr.Name = "txtTotalhr";
            this.txtTotalhr.Size = new System.Drawing.Size(52, 25);
            this.txtTotalhr.TabIndex = 37;
            this.txtTotalhr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Blue;
            this.label6.Location = new System.Drawing.Point(16, 144);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 18);
            this.label6.TabIndex = 36;
            this.label6.Text = "TOTAL";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtTotTOTAL
            // 
            this.txtTotTOTAL.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtTotTOTAL.Location = new System.Drawing.Point(225, 141);
            this.txtTotTOTAL.Name = "txtTotTOTAL";
            this.txtTotTOTAL.Size = new System.Drawing.Size(123, 25);
            this.txtTotTOTAL.TabIndex = 35;
            this.txtTotTOTAL.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Blue;
            this.label7.Location = new System.Drawing.Point(16, 84);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(69, 18);
            this.label7.TabIndex = 34;
            this.label7.Text = "Sub Total";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtTotSubTotal
            // 
            this.txtTotSubTotal.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtTotSubTotal.Location = new System.Drawing.Point(225, 81);
            this.txtTotSubTotal.Name = "txtTotSubTotal";
            this.txtTotSubTotal.Size = new System.Drawing.Size(123, 25);
            this.txtTotSubTotal.TabIndex = 33;
            this.txtTotSubTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Blue;
            this.label16.Location = new System.Drawing.Point(19, 115);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(41, 18);
            this.label16.TabIndex = 32;
            this.label16.Text = "I.V.A.";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtTotIVA
            // 
            this.txtTotIVA.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtTotIVA.Location = new System.Drawing.Point(225, 112);
            this.txtTotIVA.Name = "txtTotIVA";
            this.txtTotIVA.Size = new System.Drawing.Size(123, 25);
            this.txtTotIVA.TabIndex = 31;
            this.txtTotIVA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblManoObra
            // 
            this.lblManoObra.AutoSize = true;
            this.lblManoObra.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblManoObra.ForeColor = System.Drawing.Color.Blue;
            this.lblManoObra.Location = new System.Drawing.Point(115, 53);
            this.lblManoObra.Name = "lblManoObra";
            this.lblManoObra.Size = new System.Drawing.Size(104, 18);
            this.lblManoObra.TabIndex = 30;
            this.lblManoObra.Text = "Mano de Obra";
            this.lblManoObra.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtTotManoObra
            // 
            this.txtTotManoObra.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtTotManoObra.Location = new System.Drawing.Point(225, 50);
            this.txtTotManoObra.Name = "txtTotManoObra";
            this.txtTotManoObra.Size = new System.Drawing.Size(123, 25);
            this.txtTotManoObra.TabIndex = 29;
            this.txtTotManoObra.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Blue;
            this.label18.Location = new System.Drawing.Point(16, 22);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(84, 18);
            this.label18.TabIndex = 28;
            this.label18.Text = "Refacciones";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtTotRefacciones
            // 
            this.txtTotRefacciones.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtTotRefacciones.Location = new System.Drawing.Point(225, 19);
            this.txtTotRefacciones.Name = "txtTotRefacciones";
            this.txtTotRefacciones.Size = new System.Drawing.Size(123, 25);
            this.txtTotRefacciones.TabIndex = 27;
            this.txtTotRefacciones.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.grdDetOs);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(787, 394);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Orden Trabajo";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // grdDetOs
            // 
            this.grdDetOs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdDetOs.Location = new System.Drawing.Point(9, 18);
            this.grdDetOs.Name = "grdDetOs";
            this.grdDetOs.Size = new System.Drawing.Size(771, 196);
            this.grdDetOs.TabIndex = 1;
            this.grdDetOs.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdDetOs_CellContentClick);
            this.grdDetOs.DoubleClick += new System.EventHandler(this.grdDetOs_DoubleClick);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.grdRefacciones);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(787, 394);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Refacciones";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // grdRefacciones
            // 
            this.grdRefacciones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdRefacciones.Location = new System.Drawing.Point(7, 16);
            this.grdRefacciones.Name = "grdRefacciones";
            this.grdRefacciones.Size = new System.Drawing.Size(771, 300);
            this.grdRefacciones.TabIndex = 1;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.grdDetOC);
            this.tabPage4.Controls.Add(this.grdCabOC);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(787, 394);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Ordenes Compra";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // grdDetOC
            // 
            this.grdDetOC.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdDetOC.Location = new System.Drawing.Point(7, 175);
            this.grdDetOC.Name = "grdDetOC";
            this.grdDetOC.Size = new System.Drawing.Size(771, 176);
            this.grdDetOC.TabIndex = 4;
            // 
            // grdCabOC
            // 
            this.grdCabOC.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdCabOC.Location = new System.Drawing.Point(10, 6);
            this.grdCabOC.Name = "grdCabOC";
            this.grdCabOC.Size = new System.Drawing.Size(771, 142);
            this.grdCabOC.TabIndex = 3;
            this.grdCabOC.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdCabOC_CellContentClick);
            this.grdCabOC.Click += new System.EventHandler(this.grdCabOC_Click);
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.grdDetPreSalida);
            this.tabPage5.Controls.Add(this.grdCabPreSalida);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(787, 394);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Salidas Almacen";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // grdDetPreSalida
            // 
            this.grdDetPreSalida.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdDetPreSalida.Location = new System.Drawing.Point(6, 194);
            this.grdDetPreSalida.Name = "grdDetPreSalida";
            this.grdDetPreSalida.Size = new System.Drawing.Size(771, 176);
            this.grdDetPreSalida.TabIndex = 6;
            // 
            // grdCabPreSalida
            // 
            this.grdCabPreSalida.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdCabPreSalida.Location = new System.Drawing.Point(9, 25);
            this.grdCabPreSalida.Name = "grdCabPreSalida";
            this.grdCabPreSalida.Size = new System.Drawing.Size(771, 142);
            this.grdCabPreSalida.TabIndex = 5;
            this.grdCabPreSalida.Click += new System.EventHandler(this.grdCabPreSalida_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Blue;
            this.label15.Location = new System.Drawing.Point(20, 133);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(119, 18);
            this.label15.TabIndex = 66;
            this.label15.Text = "Personal Entrego";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtidEmpleadoEntrega
            // 
            this.txtidEmpleadoEntrega.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtidEmpleadoEntrega.Location = new System.Drawing.Point(145, 130);
            this.txtidEmpleadoEntrega.Name = "txtidEmpleadoEntrega";
            this.txtidEmpleadoEntrega.Size = new System.Drawing.Size(331, 25);
            this.txtidEmpleadoEntrega.TabIndex = 65;
            // 
            // txtidUnidadTrans
            // 
            this.txtidUnidadTrans.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtidUnidadTrans.Location = new System.Drawing.Point(764, 101);
            this.txtidUnidadTrans.Name = "txtidUnidadTrans";
            this.txtidUnidadTrans.Size = new System.Drawing.Size(75, 25);
            this.txtidUnidadTrans.TabIndex = 67;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Blue;
            this.label5.Location = new System.Drawing.Point(619, 137);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 18);
            this.label5.TabIndex = 69;
            this.label5.Text = "Estatus:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtEstatus
            // 
            this.txtEstatus.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtEstatus.Location = new System.Drawing.Point(683, 133);
            this.txtEstatus.Name = "txtEstatus";
            this.txtEstatus.Size = new System.Drawing.Size(156, 25);
            this.txtEstatus.TabIndex = 68;
            // 
            // txtIdEmpresa
            // 
            this.txtIdEmpresa.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtIdEmpresa.Location = new System.Drawing.Point(320, 63);
            this.txtIdEmpresa.Name = "txtIdEmpresa";
            this.txtIdEmpresa.Size = new System.Drawing.Size(208, 25);
            this.txtIdEmpresa.TabIndex = 70;
            // 
            // cmbConcepto
            // 
            this.cmbConcepto.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.cmbConcepto.FormattingEnabled = true;
            this.cmbConcepto.Location = new System.Drawing.Point(145, 161);
            this.cmbConcepto.Name = "cmbConcepto";
            this.cmbConcepto.Size = new System.Drawing.Size(226, 26);
            this.cmbConcepto.TabIndex = 72;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Blue;
            this.label9.Location = new System.Drawing.Point(23, 164);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(73, 18);
            this.label9.TabIndex = 71;
            this.label9.Text = "Concepto:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // fFinalizaOs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(856, 640);
            this.Controls.Add(this.cmbConcepto);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtIdEmpresa);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtEstatus);
            this.Controls.Add(this.txtidUnidadTrans);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.txtidEmpleadoEntrega);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtTipoOrdenServ);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtTipoUnidad);
            this.Controls.Add(this.dtpFechaHora);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dtpFecha);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.ToolStripMenu);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtidOrdenSer);
            this.Name = "fFinalizaOs";
            this.Text = "Finaliza Ordenes de Servicio;";
            this.Load += new System.EventHandler(this.fFinalizaOs_Load);
            this.ToolStripMenu.ResumeLayout(false);
            this.ToolStripMenu.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdInsumosConc)).EndInit();
            this.gpoImportes.ResumeLayout(false);
            this.gpoImportes.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdDetOs)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdRefacciones)).EndInit();
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdDetOC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdCabOC)).EndInit();
            this.tabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdDetPreSalida)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdCabPreSalida)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtidOrdenSer;
        internal System.Windows.Forms.ToolStrip ToolStripMenu;
        private System.Windows.Forms.ToolStripButton btnNuevo;
        private System.Windows.Forms.ToolStripButton btnGrabar;
        private System.Windows.Forms.ToolStripButton btnEditar;
        private System.Windows.Forms.ToolStripButton btnReporte;
        private System.Windows.Forms.ToolStripButton btnCancelar;
        private System.Windows.Forms.ToolStripButton btnSalir;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dtpFechaHora;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpFecha;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtTipoUnidad;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtTipoOrdenServ;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView grdDetOs;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtidEmpleadoEntrega;
        private System.Windows.Forms.TextBox txtidUnidadTrans;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtEstatus;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.GroupBox gpoImportes;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtTotTOTAL;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtTotSubTotal;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtTotIVA;
        private System.Windows.Forms.Label lblManoObra;
        private System.Windows.Forms.TextBox txtTotManoObra;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtTotRefacciones;
        private System.Windows.Forms.DataGridView grdRefacciones;
        private System.Windows.Forms.TextBox txtIdEmpresa;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.DataGridView grdDetOC;
        private System.Windows.Forms.DataGridView grdCabOC;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.DataGridView grdDetPreSalida;
        private System.Windows.Forms.DataGridView grdCabPreSalida;
        private System.Windows.Forms.ComboBox cmbConcepto;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtTotalhr;
        private System.Windows.Forms.DataGridView grdInsumosConc;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cmbMetodoPago;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox cmbUsoCFDI;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cmbFormaPago;
        private System.Windows.Forms.Label label11;
    }
}