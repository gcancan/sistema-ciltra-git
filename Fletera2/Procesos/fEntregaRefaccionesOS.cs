﻿using Core.Models;
using Fletera2.Clases;
using Fletera2Entidades;
using Fletera2Negocio;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Fletera2.Clases.Inicio;

namespace Fletera2.Procesos
{
    public partial class fEntregaRefaccionesOS : Form
    {
        Usuario _usuario;
        int _IdFolio;
        eTipoSalida _tipoSalida;

        List<CatPersonal> listRecibe;

        CabPreSalida cabsalida;
        DetPreSalida detsalida;

        DetOrdenServicio detos;
        DetRecepcionOS detrec;
        CabOrdenServicio cabos;

        List<DetPreSalida> listDetSal;

        bool Salida;

        CabPreOC cabPreocNew;
        DetPreOC detPreocNew;
        List<DetPreOC> listDetPreOCNew;

        Parametros param;

        CabPreOC cabPreOC;
        DetPreOC detPreOC;
        List<DetPreOC> listDetPreOC;

        CatActividades actividad;

        OperationResult resp;
        admExistencia existencia;
        Apartados apartado;
        Parametros parametro;

        public fEntregaRefaccionesOS(Usuario user, int IdPreSalida, eTipoSalida tipoSalida)
        {
            _usuario = user;
            _IdFolio = IdPreSalida;
            _tipoSalida = tipoSalida;
            InitializeComponent();
        }

        private void fEntregaRefaccionesOS_Load(object sender, EventArgs e)
        {
            resp = new ParametrosSvc().getParametrosxFiltro(1);
            if (resp.typeResult == ResultTypes.success)
            {
                parametro = ((List<Parametros>)resp.result)[0];
            }

            if (_IdFolio > 0)
            {
                ActivaBotones(opcHabilita.Editando);
                ActivaCampos(opcHabilita.DesHabilitaTodos);
                switch (_tipoSalida)
                {
                    case eTipoSalida.PRESALIDA:
                        CargaForma(_IdFolio.ToString(), Clases.opcBusqueda.PreSalidas);
                        break;
                    case eTipoSalida.PREORDENCOMPRA:
                        CargaForma(_IdFolio.ToString(), Clases.opcBusqueda.PreOC);
                        break;
                    default:
                        break;
                }
                cmbIdPersonalRec.Focus();
            }
            FormatoGeneral(grdRef);

        }
        private void CargaCombos(int idOrdenTrabajo)
        {
            string filtro = "Per.idPersonal IN (SELECT idPersonalResp FROM dbo.DetRecepcionOS WHERE idOrdenTrabajo = " + idOrdenTrabajo + ") " +
        " OR Per.idPersonal IN (SELECT ISNULL(idPersonalAyu1,0) FROM dbo.DetRecepcionOS WHERE idOrdenTrabajo = " + idOrdenTrabajo + ") " +
        " OR Per.idPersonal IN (SELECT ISNULL(idPersonalAyu2,0) FROM dbo.DetRecepcionOS WHERE idOrdenTrabajo = " + idOrdenTrabajo + ") ";

            //Personal Recibe
            OperationResult respPR = new CatPersonalSvc().getCatPersonalxFiltro(filtro, "  per.NombreCompleto");
            if (respPR.typeResult == ResultTypes.success)
            {
                listRecibe = (List<CatPersonal>)respPR.result;
                cmbIdPersonalRec.DataSource = listRecibe;
                cmbIdPersonalRec.DisplayMember = "NombreCompleto";
                cmbIdPersonalRec.SelectedIndex = 0;
            }
        }

        private void CargaForma(string Valor, Clases.opcBusqueda opc)
        {
            try
            {
                OperationResult resp;
                //PRE SALIDA
                if (opc == Clases.opcBusqueda.PreSalidas)
                {
                    resp = new PreSalidasSvc().getCabPreSalidaxFilter(Convert.ToInt32(Valor));
                    if (resp.typeResult == ResultTypes.success)
                    {
                        cabsalida = ((List<CabPreSalida>)resp.result)[0];
                        CargaCombos(cabsalida.idOrdenTrabajo);
                        txtIdFOLIO.Text = cabsalida.IdPreSalida.ToString();
                        dtpFecSolicitud.Value = cabsalida.FecSolicitud;
                        dtpFecSolicitudHora.Value = cabsalida.FecSolicitud;
                        SelComboRecibe(cabsalida.IdEmpleadoEnc);
                        txtidOrdenTrabajo.Text = cabsalida.idOrdenTrabajo.ToString();
                        //txtUserSolicitud.Text = cabsalida.NomUserSolicitud;
                        //txtEstatus.Text = cabsalida.Estatus;
                        //DetOrdenServicio
                        OperationResult resp2 = new DetOrdenServicioSvc().getDetOrdenServicioxFilter(0, " d.idOrdenTrabajo = " + cabsalida.idOrdenTrabajo);
                        if (resp2.typeResult == ResultTypes.success)
                        {
                            detos = ((List<DetOrdenServicio>)resp2.result)[0];
                            txtidOrdenSer.Text = detos.idOrdenSer.ToString();
                            //DetRecepcionOS
                            OperationResult resp3 = new DetRecepcionOSSvc().getDetOrdenServicioxFilter(detos.idOrdenSer, " d.idOrdenTrabajo = " + cabsalida.idOrdenTrabajo);
                            if (resp3.typeResult == ResultTypes.success)
                            {
                                detrec = ((List<DetRecepcionOS>)resp3.result)[0];


                                if (detos.idActividad > 0)
                                {
                                    resp = new CatActividadesSvc().getCatActividadesxFilter(detos.idActividad);
                                    if (resp.typeResult == ResultTypes.success)
                                    {
                                        actividad = ((List<CatActividades>)resp.result)[0];
                                        txtActividad.Text = actividad.NombreAct;
                                    }
                                }
                                else
                                {
                                    txtActividad.Text = detrec.NotaRecepcion;
                                }

                                txtIdEmpleadoEnc.Text = detrec.NomPersonalResp;
                                txtidPersonalAyu1.Text = detrec.NomPersonalAyu1;
                                txtidPersonalAyu2.Text = detrec.NomPersonalAyu2;


                                OperationResult resp4 = new CabOrdenServicioSvc().getCabOrdenServicioxFilter(detos.idOrdenSer);
                                if (resp4.typeResult == ResultTypes.success)
                                {
                                    cabos = ((List<CabOrdenServicio>)resp4.result)[0];
                                    txtNomEmpresa.Text = cabos.RazonSocial;
                                    txtidUnidadTrans.Text = cabos.idUnidadTrans;
                                    txtTipoUnidad.Text = cabos.NomTipoUnidad;
                                }
                                else
                                {
                                    MessageBox.Show("No Se encontró la  Orden de Servicio " + detos.idOrdenSer, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                            }
                            else
                            {
                                MessageBox.Show("No Se encontró la Detalle de la Orden de Trabajo " + cabsalida.idOrdenTrabajo, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }

                        }
                        else
                        {
                            //no se encontro 
                            MessageBox.Show("No Se encontró la Detalle de la Orden de Servicio " + cabsalida.idOrdenTrabajo, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }


                        //DETALLE
                        OperationResult respd = new PreSalidasSvc().getDetPreSalidaxFilter(cabsalida.IdPreSalida);
                        if (respd.typeResult == ResultTypes.success)
                        {
                            listDetSal = (List<DetPreSalida>)respd.result;
                            //Verificar Existencias
                            foreach (var item in listDetSal)
                            {
                                resp = new ComercialSvc().getadmExistenciaxProd(item.idProducto,
                                    Tools.FormatFecHora(DateTime.Now, true, false, false),
                                    Convert.ToInt32(parametro.idAlmacenSal));
                                if (resp.typeResult == ResultTypes.success)
                                {
                                    existencia = ((List<admExistencia>)resp.result)[0];
                                    item.Existencia = Convert.ToDecimal(existencia.Existencia);
                                }
                                else
                                {
                                    item.Existencia = 0;
                                }
                                resp = new PreSalidasSvc().getApartadoxProd(item.idProducto, "  c.Estatus = 'PEN'");
                                if (resp.typeResult == ResultTypes.success)
                                {
                                    apartado = ((List<Apartados>)resp.result)[0];
                                    item.Existencia = item.Existencia - apartado.Apartado;
                                }
                            }
                            //Verificar Existencias
                            grdRef.DataSource = null;
                            grdRef.DataSource = listDetSal;
                            FormatogrdDetPreSal(eTipoSalida.PRESALIDA);
                        }
                        else
                        {
                            grdRef.DataSource = null;
                        }
                        //DETALLE
                    }
                    else
                    {
                        //NO se encontro PRESALIDA
                        MessageBox.Show("No Se encontró la Presalida " + Valor, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else if (opc == Clases.opcBusqueda.PreOC)
                {
                    //PRE ORDEN DE COMPRA
                    resp = new OrdenCompraSvc().getCabPreOCxFilter(Convert.ToInt32(Valor));
                    if (resp.typeResult == ResultTypes.success)
                    {
                        cabPreOC = ((List<CabPreOC>)resp.result)[0];

                        txtIdFOLIO.Text = cabPreOC.IdPreOC.ToString();
                        dtpFecSolicitud.Value = cabPreOC.FechaPreOc;
                        dtpFecSolicitudHora.Value = cabPreOC.FechaPreOc;

                        //
                        txtidOrdenTrabajo.Text = cabPreOC.idOrdenTrabajo.ToString();
                        //txtEstatus.Text = cabPreOC.Estatus;

                        OperationResult resp2 = new DetOrdenServicioSvc().getDetOrdenServicioxFilter(0, " d.idOrdenTrabajo = " + cabPreOC.idOrdenTrabajo);
                        if (resp2.typeResult == ResultTypes.success)
                        {
                            detos = ((List<DetOrdenServicio>)resp2.result)[0];
                            txtidOrdenSer.Text = detos.idOrdenSer.ToString();

                            //txtUserSolicitud.Text = detos;
                            txtActividad.Text = detos.NotaRecepcion;

                            resp = new CabOrdenServicioSvc().getCabOrdenServicioxFilter(detos.idOrdenSer);
                            if (resp.typeResult == ResultTypes.success)
                            {
                                cabos = ((List<CabOrdenServicio>)resp.result)[0];
                                txtNomEmpresa.Text = cabos.RazonSocial;
                                txtidUnidadTrans.Text = cabos.idUnidadTrans;
                                txtTipoUnidad.Text = cabos.NomTipoUnidad;

                                resp = new DetRecepcionOSSvc().getDetOrdenServicioxFilter(detos.idOrdenSer, " d.idOrdenTrabajo = " + cabPreOC.idOrdenTrabajo);
                                if (resp.typeResult == ResultTypes.success)
                                {
                                    detrec = ((List<DetRecepcionOS>)resp.result)[0];
                                    txtActividad.Text = detrec.NotaRecepcion;
                                    txtIdEmpleadoEnc.Text = detrec.NomPersonalResp;
                                    SelComboRecibe(detrec.idPersonalResp);
                                }
                                else
                                {
                                    MessageBox.Show("No Se encontró la Orden de Trabajo " + cabsalida.idOrdenTrabajo.ToString(), "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                            }
                            else
                            {
                                MessageBox.Show("No Se encontró la Orden de Servicio " + detos.idOrdenSer.ToString(), "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }


                        }
                        else
                        {
                            MessageBox.Show("No Se encontró la Detalle de la Orden de Servicio " + cabPreOC.idOrdenTrabajo.ToString(), "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        txtidOrdenTrabajo.Text = cabPreOC.idOrdenTrabajo.ToString();
                        //txtUserSolicitud.Text = cabPreOC.UserSolicita;
                        //txtEstatus.Text = cabPreOC.Estatus;

                        //DETALLE
                        OperationResult respd = new OrdenCompraSvc().getDetPreOCxFilter(cabPreOC.IdPreOC);
                        if (respd.typeResult == ResultTypes.success)
                        {
                            listDetPreOC = (List<DetPreOC>)respd.result;
                            //Verificar Existencias
                            foreach (var item in listDetPreOC)
                            {
                                resp = new ComercialSvc().getadmExistenciaxProd(item.idProducto,
                                    Tools.FormatFecHora(DateTime.Now, true, false, false),
                                    Convert.ToInt32(parametro.idAlmacenSal));
                                if (resp.typeResult == ResultTypes.success)
                                {
                                    existencia = ((List<admExistencia>)resp.result)[0];
                                    item.Existencia = Convert.ToDecimal(existencia.Existencia);
                                }
                                else
                                {
                                    item.Existencia = 0;
                                }
                                resp = new PreSalidasSvc().getApartadoxProd(item.idProducto, "  c.Estatus = 'PEN'");
                                if (resp.typeResult == ResultTypes.success)
                                {
                                    apartado = ((List<Apartados>)resp.result)[0];
                                    item.Existencia = item.Existencia - apartado.Apartado;
                                }
                            }
                            //Verificar Existencias
                            grdRef.DataSource = null;
                            grdRef.DataSource = listDetPreOC;
                            FormatogrdDetPreSal(eTipoSalida.PREORDENCOMPRA);
                        }
                        else
                        {
                            MessageBox.Show("No Se encontró el Detalle de la Pre Orden de Compra " + cabPreOC.IdPreOC.ToString(), "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            grdRef.DataSource = null;
                        }
                        //DETALLE

                    }
                    else
                    {
                        MessageBox.Show("No Se encontró la Pre Orden Compra " + Valor, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + ex.Message,
                   this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

        }

        private void ActivaBotones(opcHabilita opc)
        {
            switch (opc)
            {
                case opcHabilita.DesHabilitaTodos:
                    break;
                case opcHabilita.Iniciando:
                    break;
                case opcHabilita.Editando:
                    btnNuevo.Enabled = false;
                    btnEditar.Enabled = true;
                    btnCancelar.Enabled = true;
                    btnGrabar.Enabled = true;
                    btnReporte.Enabled = false;
                    btnSalir.Enabled = false;
                    break;
                case opcHabilita.Nuevo:
                    break;
                case opcHabilita.Reporte:
                    break;
                default:
                    break;
            }
        }

        private void ActivaCampos(opcHabilita opc)
        {
            switch (opc)
            {
                case opcHabilita.DesHabilitaTodos:
                    gpoFaC.Enabled = false;
                    break;
                case opcHabilita.Iniciando:
                    break;
                case opcHabilita.Editando:
                    break;
                case opcHabilita.Nuevo:
                    break;
                case opcHabilita.Reporte:
                    break;
                default:
                    break;
            }
        }
        private void FormatoGeneral(DataGridView grid)
        {
            //ESTILO GENERAL
            grid.RowsDefaultCellStyle.BackColor = Color.White;
            grid.AlternatingRowsDefaultCellStyle.BackColor = Color.AliceBlue;

            grid.RowsDefaultCellStyle.SelectionForeColor = Color.Yellow;
            grid.RowsDefaultCellStyle.SelectionBackColor = Color.Gray;
            grid.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            grid.DefaultCellStyle.Font = new Font("Berlin Sans FB", 12);
            grid.ColumnHeadersDefaultCellStyle.Font = new Font("Berlin Sans FB", 12);
            grid.ColumnHeadersDefaultCellStyle.BackColor = Color.Yellow;
            grid.ColumnHeadersDefaultCellStyle.ForeColor = Color.Red;
            grid.EnableHeadersVisualStyles = false;
        }
        private void FormatogrdDetPreSal(eTipoSalida tipo)
        {
            try
            {
                Salida = true;
                switch (tipo)
                {
                    case eTipoSalida.PRESALIDA:
                        grdRef.Columns["IdPreSalida"].Visible = false;
                        //grdRef.Columns["Entregar"].Visible = false;
                        //grdRef.Columns["CantidadEnt"].Visible = false;
                        grdRef.Columns["Costo"].Visible = false;
                        grdRef.Columns["idUnidadProd"].Visible = false;
                        //grdRef.Columns["Existencia"].Visible = false;
                        grdRef.Columns["CantidadEnt"].Visible = false;


                        grdRef.Columns["Inserta"].Visible = false;

                        grdRef.Columns["Seleccionado"].DisplayIndex = 0;
                        grdRef.Columns["Cantidad"].DisplayIndex = 1;
                        grdRef.Columns["idProducto"].DisplayIndex = 2;
                        grdRef.Columns["CCODIGOPRODUCTO"].DisplayIndex = 3;
                        grdRef.Columns["CNOMBREPRODUCTO"].DisplayIndex = 4;
                        grdRef.Columns["CantidadEnt"].DisplayIndex = 5;

                        grdRef.Columns["Seleccionado"].HeaderText = "Salida Insumo";
                        grdRef.Columns["Cantidad"].HeaderText = "Cantidad";
                        grdRef.Columns["idProducto"].HeaderText = "Producto";
                        grdRef.Columns["CCODIGOPRODUCTO"].HeaderText = "Código";
                        grdRef.Columns["CNOMBREPRODUCTO"].HeaderText = "Descripción";
                        //grdRef.Columns["CantidadEnt"].HeaderText = "Entregado";
                        grdRef.Columns["CantEntregaCambio"].HeaderText = "Cant. Cambio Refacciones";
                        //grdDetPreSal.Columns["IdPreSalida"].HeaderText = "Folio";
                        //grdDetPreSal.Columns["IdPreSalida"].HeaderText = "Folio";

                        grdRef.Columns["Cantidad"].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
                        grdRef.Columns["idProducto"].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
                        grdRef.Columns["CCODIGOPRODUCTO"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                        grdRef.Columns["CNOMBREPRODUCTO"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                        grdRef.Columns["CantidadEnt"].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
                        break;
                    case eTipoSalida.PREORDENCOMPRA:
                        grdRef.Columns["IdPreOC"].Visible = false;
                        grdRef.Columns["Precio"].Visible = false;
                        grdRef.Columns["idUnidadProd"].Visible = false;
                        //grdRef.Columns["Existencia"].Visible = false;
                        grdRef.Columns["CantSinCosto"].Visible = false;
                        grdRef.Columns["CantFaltante"].Visible = false;
                        grdRef.Columns["PorcIVA"].Visible = false;
                        grdRef.Columns["Desc1"].Visible = false;
                        grdRef.Columns["Desc2"].Visible = false;
                        grdRef.Columns["Inserta"].Visible = false;


                        grdRef.Columns["Seleccionado"].DisplayIndex = 0;
                        grdRef.Columns["CantidadSol"].DisplayIndex = 1;
                        grdRef.Columns["idProducto"].DisplayIndex = 2;
                        grdRef.Columns["CCODIGOPRODUCTO"].DisplayIndex = 3;
                        grdRef.Columns["CNOMBREPRODUCTO"].DisplayIndex = 4;
                        grdRef.Columns["CantidadEnt"].DisplayIndex = 5;

                        grdRef.Columns["Seleccionado"].HeaderText = "Entregar";
                        grdRef.Columns["CantidadSol"].HeaderText = "Cantidad";
                        grdRef.Columns["Existencia"].HeaderText = "Existencia";

                        grdRef.Columns["idProducto"].HeaderText = "Producto";
                        grdRef.Columns["CCODIGOPRODUCTO"].HeaderText = "Código";
                        grdRef.Columns["CNOMBREPRODUCTO"].HeaderText = "Descripción";
                        grdRef.Columns["CantidadEnt"].HeaderText = "Entregado";
                        //grdDetPreSal.Columns["IdPreSalida"].HeaderText = "Folio";
                        //grdDetPreSal.Columns["IdPreSalida"].HeaderText = "Folio";

                        grdRef.Columns["CantidadSol"].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
                        grdRef.Columns["idProducto"].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
                        grdRef.Columns["CCODIGOPRODUCTO"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                        grdRef.Columns["CNOMBREPRODUCTO"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                        grdRef.Columns["CantidadEnt"].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
                        break;
                    default:
                        break;
                }
                Salida = false;
            }
            catch (Exception)
            {

                throw;
            }


        }

        private void btnGrabar_Click(object sender, EventArgs e)
        {
            try
            {
                int idProducto;
                decimal Cantidad;
                decimal CantidadEntregaCambio;
                bool BandPreOc = false;
                bool BandGrabado = false;
                //Checar que VAlidar
                if (!Validar())
                {
                    return;
                }

                caption = this.Name;
                buttons = MessageBoxButtons.OKCancel;
                //mensaje = "Desea guardar la Liquidacion para el operador: " + txtNomOperador.Text;
                mensaje = "!! Desea Entregar los Insumos y/o Refacciones del Folio ? " + txtIdFOLIO.Text + " !!";
                result = MessageBox.Show(mensaje, caption, buttons);
                if (result == System.Windows.Forms.DialogResult.OK)
                {
                    OperationResult resp = new ParametrosSvc().getParametrosxFiltro(1);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        param = ((List<Parametros>)resp.result)[0];
                    }

                    switch (_tipoSalida)
                    {
                        case eTipoSalida.PRESALIDA:

                            for (int i = 0; i < grdRef.Rows.Count; i++)
                            {
                                idProducto = (int)grdRef.Rows[i].Cells["idProducto"].Value;
                                Cantidad = (decimal)grdRef.Rows[i].Cells["CantidadEnt"].Value;
                                CantidadEntregaCambio = (decimal)grdRef.Rows[i].Cells["CantEntregaCambio"].Value;
                                foreach (var item in listDetSal)
                                {
                                    if (item.idProducto == idProducto)
                                    {
                                        item.CantidadEnt = Cantidad;
                                        item.CantEntregaCambio = CantidadEntregaCambio;
                                    }
                                }

                                if ((decimal)grdRef.Rows[i].Cells["Cantidad"].Value
                                    < (decimal)grdRef.Rows[i].Cells["CantidadEnt"].Value)
                                {
                                    if (!BandPreOc)
                                    {
                                        BandPreOc = true;
                                        cabPreocNew = new CabPreOC
                                        {
                                            IdPreOC = 0,
                                            idOrdenTrabajo = cabsalida.idOrdenTrabajo,
                                            FechaPreOc = DateTime.Now,
                                            UserSolicita = _usuario.nombreUsuario,
                                            idAlmacen = 0,
                                            Estatus = "PRE"
                                        };
                                    }
                                    Cantidad = (decimal)grdRef.Rows[i].Cells["Cantidad"].Value - (decimal)grdRef.Rows[i].Cells["CantidadEnt"].Value;




                                    //Agregar Detalle
                                    detPreocNew = new DetPreOC
                                    {
                                        IdPreOC = 0,
                                        idProducto = idProducto,
                                        idUnidadProd = 0,
                                        CantidadSol = Cantidad,

                                    };
                                    listDetPreOCNew.Add(detPreocNew);

                                }
                            }
                            //Actualizamos Cabecero de Presalida
                            cabsalida.IdPersonalRec = ((CatPersonal)cmbIdPersonalRec.SelectedItem).idPersonal;
                            cabsalida.UserEntrega = _usuario.nombreUsuario;
                            //cabsalida.EntregaCambio = chkEntregaCambio.Checked;
                            cabsalida.idPreOC = 0;
                            cabsalida.Estatus = "ENT";

                            //Primero COMERCIAL
                            //Aplicamos Cambios



                            break;
                        case eTipoSalida.PREORDENCOMPRA:
                            //CREAR CAB PRESALIDA
                            cabsalida = null;
                            cabsalida = new CabPreSalida
                            {
                                IdPreSalida = 0,
                                idOrdenTrabajo = Convert.ToInt32(txtidOrdenTrabajo.Text),
                                FecSolicitud = DateTime.Now,
                                UserSolicitud = "????",
                                IdEmpleadoEnc = detrec.idPersonalResp,
                                idAlmacen = Convert.ToInt32(param.idAlmacenSal),
                                Estatus = "ENT",
                                SubTotal = 0,
                                FecEntrega = DateTime.Now,
                                UserEntrega = _usuario.nombreUsuario,
                                IdPersonalRec = ((CatPersonal)cmbIdPersonalRec.SelectedItem).idPersonal,
                                //EntregaCambio = chkEntregaCambio.Checked,
                                idPreOC = cabPreOC.IdPreOC,
                                CIDDOCUMENTO_ENT = 0,
                                CSERIEDOCUMENTO_ENT = "",
                                CFOLIO_ENT = 0,
                                CIDDOCUMENTO_SAL = 0,
                                CSERIEDOCUMENTO_SAL = "",
                                CFOLIO_SAL = 0,
                                CIDALMACEN_COM = 0,
                            };

                            //CREAR DET PRESALIDA
                            //RECORRER LA PRE ORDEN DE COMPRA
                            listDetSal = new List<DetPreSalida>();
                            for (int i = 0; i < grdRef.Rows.Count; i++)
                            {
                                idProducto = (int)grdRef.Rows[i].Cells["idProducto"].Value;
                                Cantidad = (decimal)grdRef.Rows[i].Cells["CantidadEnt"].Value;



                                detsalida = new DetPreSalida
                                {
                                    IdPreSalida = 0,
                                    idProducto = idProducto,
                                    CCODIGOPRODUCTO = (string)grdRef.Rows[i].Cells["CCODIGOPRODUCTO"].Value,
                                    CNOMBREPRODUCTO = (string)grdRef.Rows[i].Cells["CNOMBREPRODUCTO"].Value,
                                    idUnidadProd = 0,
                                    Cantidad = Cantidad,
                                    CantidadEnt = Cantidad,
                                    Costo = 0,
                                    Existencia = 0
                                };
                                listDetSal.Add(detsalida);

                                //posible PREOC
                                if ((decimal)grdRef.Rows[i].Cells["CantidadSol"].Value
                                    < (decimal)grdRef.Rows[i].Cells["CantidadEnt"].Value)
                                {
                                    if (!BandPreOc)
                                    {
                                        BandPreOc = true;
                                        cabPreocNew = new CabPreOC
                                        {
                                            IdPreOC = 0,
                                            idOrdenTrabajo = cabsalida.idOrdenTrabajo,
                                            FechaPreOc = DateTime.Now,
                                            UserSolicita = _usuario.nombreUsuario,
                                            idAlmacen = 0,
                                            Estatus = "PRE"
                                        };
                                    }
                                    Cantidad = (decimal)grdRef.Rows[i].Cells["Cantidad"].Value - (decimal)grdRef.Rows[i].Cells["CantidadEnt"].Value;
                                    //Agregar Detalle
                                    detPreocNew = new DetPreOC
                                    {
                                        IdPreOC = 0,
                                        idProducto = idProducto,
                                        idUnidadProd = 0,
                                        CantidadSol = Cantidad
                                    };
                                    listDetPreOCNew.Add(detPreocNew);

                                }
                                else
                                {

                                }
                            }
                            //ACTUALIZAR CAB PREOC


                            cabPreOC.Estatus = "ENT";
                            cabPreOC.IdPreSalida = 0;
                            //ACTUALIZAR CAB PRESALIDA DESPUES DE COMERCIAL



                            break;
                        default:
                            break;
                    }

                    //realizar TRASPASO COMERCIAL
                    if (EnviaComercial())
                    {

                        //APLICAR CAMBIOS LOCALES
                        OperationResult respGraba;
                        //EN CASO DE CAB PREOC NUEVA
                        if (cabPreocNew != null)
                        {
                            respGraba = new OrdenCompraSvc().GuardaCabPreOC(ref cabPreocNew);
                            if (respGraba.typeResult == ResultTypes.success)
                            {
                                BandGrabado = true;
                                //Actualizamos el Detalle DE PREOC
                                DetPreOC detpreoc = new DetPreOC();
                                foreach (var item in listDetPreOCNew)
                                {
                                    item.IdPreOC = cabPreocNew.IdPreOC;
                                    item.Inserta = true;
                                    detpreoc = item;
                                    respGraba = new OrdenCompraSvc().GuardaDetPreOC(ref detpreoc);
                                    if (respGraba.typeResult == ResultTypes.success)
                                    {
                                        BandGrabado = true;
                                    }
                                    else
                                    {
                                        BandGrabado = false;
                                    }
                                }

                            }
                            else
                            {
                                BandGrabado = false;
                                //salir
                            }
                        }
                        //ACTUALIZAR PRESALIDA
                        respGraba = new PreSalidasSvc().GuardaCabPreSalida(ref cabsalida);
                        if (respGraba.typeResult == ResultTypes.success)
                        {

                            BandGrabado = true;
                            DetPreSalida sal = new DetPreSalida();
                            foreach (var item in listDetSal)
                            {
                                sal = item;
                                respGraba = new PreSalidasSvc().GuardDetPreSalida(ref sal);
                                if (respGraba.typeResult == ResultTypes.success)
                                {
                                    BandGrabado = true;
                                }
                                else
                                {
                                    BandGrabado = false;
                                }
                            }
                            if (cabPreOC != null)
                            {
                                cabPreOC.IdPreSalida = cabsalida.IdPreSalida;
                                respGraba = new OrdenCompraSvc().GuardaCabPreOC(ref cabPreOC);
                                if (respGraba.typeResult == ResultTypes.success)
                                {
                                    BandGrabado = true;
                                }
                                else
                                {
                                    BandGrabado = false;
                                }
                            }
                        }
                        else
                        {
                            BandGrabado = false;
                        }

                        if (BandGrabado)
                        {
                            //Mensaje
                            MessageBox.Show("Se Entrego Satisfactoriamente el FOLIO: " + txtIdFOLIO.Text, "Operación Exitosa", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            btnCancelar.PerformClick();
                        }
                        else
                        {
                            MessageBox.Show("Ocurrio un Error con el FOLIO : " + txtIdFOLIO.Text, "Operación Exitosa", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }

                        //switch (_tipoSalida)
                        //{
                        //    case TipoSalida.PRESALIDA:

                        //        break;
                        //    case TipoSalida.PREORDENCOMPRA:
                        //        ////GRABA CAB PRESALIDA
                        //        //respGraba = new PreSalidasSvc().GuardaCabPreSalida(ref cabsalida);
                        //        //if (respGraba.typeResult == ResultTypes.success)
                        //        //{
                        //        //    BandGrabado = true;
                        //        //    //GRABA DET PRESALIDA
                        //        //    DetPreSalida sal = new DetPreSalida();
                        //        //    foreach (var item in listDetSal)
                        //        //    {
                        //        //        sal = item;
                        //        //        respGraba = new PreSalidasSvc().GuardDetPreSalida(ref sal);
                        //        //        if (respGraba.typeResult == ResultTypes.success)
                        //        //        {
                        //        //            BandGrabado = true;
                        //        //        }
                        //        //        else
                        //        //        {
                        //        //            BandGrabado = false;
                        //        //        }
                        //        //    }
                        //        //}
                        //        //else
                        //        //{
                        //        //    BandGrabado = false;
                        //        //}

                        //        //    //EN CASO DE QUE HAIGA NUEVA CAB PREOC Y DET PREOC

                        //            break;
                        //    default:
                        //        break;
                        //}
                        //APLICAR CAMBIOS LOCALES
                    }
                    //FIN  COMERCIAL
                    else
                    {
                        //ocurrio un Error en Comercial
                        MessageBox.Show("Ocurrio un Error en COMERCIAL", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        btnCancelar.PerformClick();
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + ex.Message,
                  this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private bool Validar()
        {
            if (cmbIdPersonalRec.Text == "")
            {
                MessageBox.Show("Tiene que seleccionar quien recibe los insumos o Refacciones", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cmbIdPersonalRec.Focus();
                return false;
            }
            for (int i = 0; i < grdRef.Rows.Count; i++)
            {
                if (!(bool)grdRef.Rows[i].Cells["Seleccionado"].Value)
                {
                    MessageBox.Show("Todas los insumos o Refacciones deben estar Marcados", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    grdRef.Focus();
                    return false;
                }
                if ((decimal)grdRef.Rows[i].Cells["CantidadEnt"].Value < (decimal)grdRef.Rows[i].Cells["CantEntregaCambio"].Value)
                {
                    MessageBox.Show("No se puede Devolver mas de lo solicitado", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    grdRef.Focus();
                    return false;

                }
            }
            switch (_tipoSalida)
            {
                case eTipoSalida.PRESALIDA:
                    //DETALLE
                    OperationResult respd = new PreSalidasSvc().getDetPreSalidaxFilter(cabsalida.IdPreSalida);
                    if (respd.typeResult == ResultTypes.success)
                    {
                        listDetSal = (List<DetPreSalida>)respd.result;
                        //Verificar Existencias
                        foreach (var item in listDetSal)
                        {
                            resp = new ComercialSvc().getadmExistenciaxProd(item.idProducto,
                                Tools.FormatFecHora(DateTime.Now, true, false, false),
                                Convert.ToInt32(parametro.idAlmacenSal));
                            if (resp.typeResult == ResultTypes.success)
                            {
                                existencia = ((List<admExistencia>)resp.result)[0];
                                item.Existencia = Convert.ToDecimal(existencia.Existencia);
                            }
                            else
                            {
                                item.Existencia = 0;
                            }
                            resp = new PreSalidasSvc().getApartadoxProd(item.idProducto, "  c.Estatus = 'PEN'");
                            if (resp.typeResult == ResultTypes.success)
                            {
                                apartado = ((List<Apartados>)resp.result)[0];
                                item.Existencia = item.Existencia - apartado.Apartado;
                            }
                        }
                        //Verificar Existencias

                        foreach (var item in listDetSal)
                        {
                            if ( item.Existencia < item.Cantidad )
                            {
                                MessageBox.Show("El Producto " + item.CNOMBREPRODUCTO + " No tiene Existencia suficiente para darle Salida", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                grdRef.Focus();
                                return false;
                            }
                        }
                    }
                    //DETALLE
                    break;
                case eTipoSalida.PREORDENCOMPRA:
                    //DETALLE
                    resp = new OrdenCompraSvc().getDetPreOCxFilter(cabPreOC.IdPreOC);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        listDetPreOC = (List<DetPreOC>)resp.result;
                        //Verificar Existencias
                        foreach (var item in listDetPreOC)
                        {
                            resp = new ComercialSvc().getadmExistenciaxProd(item.idProducto,
                                Tools.FormatFecHora(DateTime.Now, true, false, false),
                                Convert.ToInt32(parametro.idAlmacenSal));
                            if (resp.typeResult == ResultTypes.success)
                            {
                                existencia = ((List<admExistencia>)resp.result)[0];
                                item.Existencia = Convert.ToDecimal(existencia.Existencia);
                            }
                            else
                            {
                                item.Existencia = 0;
                            }
                            resp = new PreSalidasSvc().getApartadoxProd(item.idProducto, "  c.Estatus = 'PEN'");
                            if (resp.typeResult == ResultTypes.success)
                            {
                                apartado = ((List<Apartados>)resp.result)[0];
                                item.Existencia = item.Existencia - apartado.Apartado;
                            }
                        }
                        //Verificar Existencias
                        foreach (var item in listDetPreOC)
                        {
                            if (item.CantidadSol < item.Existencia)
                            {
                                MessageBox.Show("El Producto " + item.CNOMBREPRODUCTO + " No tiene Existencia suficiente para darle Salida", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                grdRef.Focus();
                                return false;
                            }
                        }
                    }
                    //DETALLE
                    break;
                default:
                    break;
            }

            

            //For i = 0 To grdDetalle.Rows.Count - 1
            //    If Not grdDetalle.Item("Entregar", i).Value Then
            //        MsgBox("Todas los insumos o Refacciones deben estar Marcados", vbInformation, "Aviso" & Me.Text)
            //        'Exit For
            //        grdDetalle.Focus()
            //        Return False
            //    End If
            //Next
            //Return True


            return true;
        }

        private void grdRef_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void grdRef_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (Salida)
            {
                return;
            }
            switch (_tipoSalida)
            {
                case eTipoSalida.PRESALIDA:
                    if ((bool)grdRef.Rows[grdRef.CurrentCell.RowIndex].Cells["Seleccionado"].Value)
                    {
                        if ((decimal)grdRef.Rows[grdRef.CurrentCell.RowIndex].Cells["CantidadEnt"].Value >
                            (decimal)grdRef.Rows[grdRef.CurrentCell.RowIndex].Cells["Cantidad"].Value)
                        {
                            grdRef.Rows[grdRef.CurrentCell.RowIndex].Cells["CantidadEnt"].Value
                                = grdRef.Rows[grdRef.CurrentCell.RowIndex].Cells["Cantidad"].Value;
                        }
                        else if ((decimal)grdRef.Rows[grdRef.CurrentCell.RowIndex].Cells["CantidadEnt"].Value == 0)
                        {
                            grdRef.Rows[grdRef.CurrentCell.RowIndex].Cells["CantidadEnt"].Value
                               = grdRef.Rows[grdRef.CurrentCell.RowIndex].Cells["Cantidad"].Value;
                        }
                    }
                    break;
                case eTipoSalida.PREORDENCOMPRA:
                    if ((bool)grdRef.Rows[grdRef.CurrentCell.RowIndex].Cells["Seleccionado"].Value)
                    {
                        if ((decimal)grdRef.Rows[grdRef.CurrentCell.RowIndex].Cells["CantidadEnt"].Value >
                            (decimal)grdRef.Rows[grdRef.CurrentCell.RowIndex].Cells["CantidadSol"].Value)
                        {
                            grdRef.Rows[grdRef.CurrentCell.RowIndex].Cells["CantidadEnt"].Value
                                = grdRef.Rows[grdRef.CurrentCell.RowIndex].Cells["CantidadSol"].Value;
                        }
                        else if ((decimal)grdRef.Rows[grdRef.CurrentCell.RowIndex].Cells["CantidadEnt"].Value == 0)
                        {
                            grdRef.Rows[grdRef.CurrentCell.RowIndex].Cells["CantidadEnt"].Value
                               = grdRef.Rows[grdRef.CurrentCell.RowIndex].Cells["CantidadSol"].Value;
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        private bool EnviaComercial()
        {
            try
            {
                string DatoSalidaCOM = "";
                string DatoEntradaCOM = "";

                //SDK_Comercial.tDocumento ltDocto = new SDK_Comercial.tDocumento();
                //SDK_Comercial.tMovimiento ltMovto = new SDK_Comercial.tMovimiento();
                //int lIdDocto = 0;
                //int LIdMovto = 0;
                int cont = 0;

                bool Exito = false;
                //Checa Parametros
                OperationResult resp = new ParametrosSvc().getParametrosxFiltro(1);
                if (resp.typeResult == ResultTypes.success)
                {
                    param = ((List<Parametros>)resp.result)[0];
                    if (IniciarSesionSDK(param.idEmpresaCOMTaller))
                    {
                        //SALIDA
                        lError = SDK_Comercial.fSiguienteFolio(param.idConceptoSalAlm, vSerie, ref vFolio);
                        if (lError == 0)
                        {
                            SDK_Comercial.tDocumento ltDocto_Sal = new SDK_Comercial.tDocumento();
                            SDK_Comercial.tMovimiento ltMovto_Sal = new SDK_Comercial.tMovimiento();

                            int lIdDocto_Sal = 0;
                            int LIdMovto_Sal = 0;

                            ltDocto_Sal.aCodConcepto = param.idConceptoSalAlm;
                            ltDocto_Sal.aFecha = DateTime.Now.ToString("MM/dd/yyyy");
                            ltDocto_Sal.aCodigoCteProv = "";
                            ltDocto_Sal.aCodigoAgente = "";
                            ltDocto_Sal.aSistemaOrigen = 0;
                            ltDocto_Sal.aNumMoneda = 1;
                            ltDocto_Sal.aTipoCambio = 1;
                            ltDocto_Sal.aAfecta = 0;

                            lError = SDK_Comercial.fAltaDocumento(ref lIdDocto_Sal, ref ltDocto_Sal);
                            if (lError == 0)
                            {
                                //DETALLE DE SALIDA
                                CIDDOCUMENTO = lIdDocto_Sal;
                                cont = 0;
                                foreach (var item in listDetSal)
                                {
                                    cont += 1;
                                    //ltMovto.aCodAlmacen = almacen.CCODIGOALMACEN;
                                    ltMovto_Sal.aCodAlmacen = param.idAlmacenSal;
                                    ltMovto_Sal.aConsecutivo = cont;
                                    //MessageBox.Show(item.CCODIGOPRODUCTO.ToString() + " Productos", "Checando", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    ltMovto_Sal.aCodProdSer = item.CCODIGOPRODUCTO;
                                    ltMovto_Sal.aUnidades = (double)item.Cantidad;
                                    ltMovto_Sal.aCosto = (double)item.Costo;
                                    //ltMovto.aCosto = 0;
                                    //ltMovto.aReferencia = item.NoMov.ToString();
                                    ltMovto_Sal.aReferencia = "Orden Trabajo = " + cabsalida.idOrdenTrabajo;
                                    lError = SDK_Comercial.fAltaMovimiento(lIdDocto_Sal, ref LIdMovto_Sal, ref ltMovto_Sal);
                                    if (lError == 0)
                                    {
                                        Exito = true;
                                    }
                                    else
                                    {
                                        SDK_Comercial.MuestraError(lError);
                                        SDK_Comercial.fBorraDocumento();
                                        Exito = false;
                                        break;
                                    }
                                }

                                if (Exito)
                                {
                                    DatoSalidaCOM = " Salida: " + vSerie + vFolio.ToString() + " del Almacen " + param.idAlmacenSal;
                                    if (cabsalida == null)
                                    {
                                        cabsalida = new CabPreSalida();
                                    }
                                    cabsalida.CSERIEDOCUMENTO_SAL = vSerie.ToString();
                                    cabsalida.CFOLIO_SAL = vFolio;
                                    cabsalida.CIDDOCUMENTO_SAL = CIDDOCUMENTO;
                                    //TERMINA SALIDA
                                    //ENTRADA
                                    lError = SDK_Comercial.fSiguienteFolio(param.idConceptoEntAlm, vSerie, ref vFolio);
                                    if (lError == 0)
                                    {
                                        SDK_Comercial.tDocumento ltDocto_Ent = new SDK_Comercial.tDocumento();
                                        SDK_Comercial.tMovimiento ltMovto_Ent = new SDK_Comercial.tMovimiento();

                                        int lIdDocto_Ent = 0;
                                        int LIdMovto_Ent = 0;

                                        ltDocto_Ent.aCodConcepto = param.idConceptoEntAlm;
                                        ltDocto_Ent.aFecha = DateTime.Now.ToString("MM/dd/yyyy");
                                        ltDocto_Ent.aCodigoCteProv = "";
                                        ltDocto_Ent.aCodigoAgente = "";
                                        ltDocto_Ent.aSistemaOrigen = 0;
                                        ltDocto_Ent.aNumMoneda = 1;
                                        ltDocto_Ent.aTipoCambio = 1;
                                        ltDocto_Ent.aAfecta = 0;

                                        lError = SDK_Comercial.fAltaDocumento(ref lIdDocto_Ent, ref ltDocto_Ent);
                                        if (lError == 0)
                                        {
                                            //DETALLE DE ENTRADA

                                            CIDDOCUMENTO = lIdDocto_Ent;
                                            cont = 0;
                                            foreach (var item in listDetSal)
                                            {
                                                cont += 1;
                                                //ltMovto.aCodAlmacen = almacen.CCODIGOALMACEN;
                                                ltMovto_Ent.aCodAlmacen = param.idAlmacenEnt;
                                                ltMovto_Ent.aConsecutivo = cont;
                                                ltMovto_Ent.aCodProdSer = item.CCODIGOPRODUCTO;
                                                ltMovto_Ent.aUnidades = (double)item.Cantidad;
                                                ltMovto_Ent.aCosto = (double)item.Costo;
                                                //ltMovto.aCosto = 0;
                                                //ltMovto.aReferencia = item.NoMov.ToString();
                                                ltMovto_Ent.aReferencia = "Orden Trabajo = " + cabsalida.idOrdenTrabajo;
                                                lError = SDK_Comercial.fAltaMovimiento(lIdDocto_Ent, ref LIdMovto_Ent, ref ltMovto_Ent);
                                                if (lError == 0)
                                                {
                                                    Exito = true;
                                                }
                                                else
                                                {
                                                    SDK_Comercial.MuestraError(lError);
                                                    SDK_Comercial.fBorraDocumento();
                                                    Exito = false;
                                                    break;
                                                }
                                            }

                                            if (Exito)
                                            {
                                                DatoEntradaCOM = " Entrada: " + vSerie + vFolio.ToString() + " del Almacen " + param.idAlmacenEnt;
                                                if (cabsalida == null)
                                                {
                                                    cabsalida = new CabPreSalida();
                                                }

                                                cabsalida.CSERIEDOCUMENTO_ENT = vSerie.ToString();
                                                cabsalida.CFOLIO_ENT = vFolio;
                                                cabsalida.CIDDOCUMENTO_ENT = CIDDOCUMENTO;
                                                //TERMINA SALIDA
                                            }


                                        }
                                        else
                                        {
                                            SDK_Comercial.MuestraError(lError);
                                            Exito = false;
                                        }
                                    }
                                    else
                                    {
                                        SDK_Comercial.MuestraError(lError);
                                        SDK_Comercial.fBorraDocumento();
                                        Exito = false;

                                    }
                                }






                            }
                            else
                            {
                                SDK_Comercial.MuestraError(lError);
                                SDK_Comercial.fBorraDocumento();
                                Exito = false;
                            }
                        }
                        else
                        {
                            SDK_Comercial.MuestraError(lError);
                            Exito = false;
                        }
                    }

                }
                else
                {
                    //si no se encuentra cancelar
                }


                if (Exito)
                {
                    MessageBox.Show("Se realizo los siguientes Movimientos:" + DatoSalidaCOM +
                              System.Environment.NewLine + DatoEntradaCOM +
                              System.Environment.NewLine +
                              " para la Orden de Servicio:" + txtidOrdenSer.Text, "Operación Exitosa", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                return Exito;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + ex.Message,
                 this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;

            }
            finally
            {
                SDK_Comercial.fCierraEmpresa();
                SDK_Comercial.fTerminaSDK();
            }

        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            if (_IdFolio > 0)
            {
                Close();
            }
            else
            {
                //Cancelar();
            }
        }

        private void SelComboRecibe(int idPersonal)
        {
            try
            {
                int i = 0;
                foreach (CatPersonal item in cmbIdPersonalRec.Items)
                {
                    if (item.idPersonal == idPersonal)
                    {
                        cmbIdPersonalRec.SelectedIndex = i;
                        return;
                    }
                    else
                    {
                        i++;
                    }
                }
            }
            catch (Exception e)
            {

                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + e.Message,
                  this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void cmbIdPersonalRec_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cmbIdPersonalRec_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                //chkEntregaCambio.Focus();
            }
        }

        private void ActualizarExistencias(eTipoSalida tipo)
        {
            try
            {
                switch (tipo)
                {
                    case eTipoSalida.PRESALIDA:
                        break;
                    case eTipoSalida.PREORDENCOMPRA:
                        foreach (var item in listDetPreOC)
                        {
                            //resp = new ComercialSvc().
                        }
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + ex.Message,
this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                throw;
            }
        }
        public static bool IniciarSesionSDK(int vEmpresaComercial)
        {
            try
            {
                RegistryKey KeySistema = Registry.LocalMachine.OpenSubKey(szRegKeySistema);
                object lEntrada = KeySistema.GetValue("DirectorioBase");
                Directory.SetCurrentDirectory(lEntrada.ToString());
                string RutaDatosCOM = "";
                OperationResult Resp = new EmpresasCOMSvc().getEmpresasxId(vEmpresaComercial);
                if (Resp.typeResult == ResultTypes.success)
                {
                    EmpresasCOM empresa = ((List<EmpresasCOM>)Resp.result)[0];
                    RutaDatosCOM = empresa.CRUTADATOS;
                }
                else
                {
                    //mensaje
                }
                lError = SDK_Comercial.fSetNombrePAQ("CONTPAQ I COMERCIAL");
                if (lError != 0)
                {
                    if (lError == 999999)
                    {
                        bActivoSDK = true;
                    }
                    else
                    {
                        SDK_Comercial.MuestraError(lError);
                        bActivoSDK = false;
                    }
                }
                else
                {
                    lError = SDK_Comercial.fAbreEmpresa(RutaDatosCOM);
                    if (lError != 0)
                    {
                        bEmpresaSDKAbierta = false;
                        SDK_Comercial.MuestraError(lError);
                    }
                    else
                    {
                        bEmpresaSDKAbierta = true;
                        sRutaEmpresaAdmPAQ = RutaDatosCOM;
                    }
                }
                return bEmpresaSDKAbierta;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
                return false;
            }

        }
    }//
}
