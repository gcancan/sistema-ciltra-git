﻿using Core.Models;
using Fletera2.Clases;
using Fletera2Entidades;
using Fletera2Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fletera2.Procesos
{
    public partial class fPenPreSalidas : Form
    {
        Usuario _usuario;
        Parametros parametro;

        List<CabPreSalida> listaCabPreSal;
        List<DetPreSalida> listaDetPreSal;
        List<CabPreOC> listaCabPreOC;
        List<DetPreOC> listaDetPreOC;

        admExistencia existencia;
        Apartados apartado;
        OperationResult resp;

        //bool BandExito;
        //string PreSalaCancelar = "";
        //int Indice;
        //int IdOrdenTrabajo = 0;

        public fPenPreSalidas(Usuario usuario)
        {
            _usuario = usuario;
            //_parametro = param;
            InitializeComponent();
        }

        private void fPenPreSalidas_Load(object sender, EventArgs e)
        {
            Refresca();
           

        }

        private void Refresca()
        {
            resp = new ParametrosSvc().getParametrosxFiltro(1);
            if (resp.typeResult == ResultTypes.success)
            {
                parametro = ((List<Parametros>)resp.result)[0];
            }
            CargaCabPreSalidas();
            //CargaCabPreOC();
            InicializarFormatos();

        }
        private void InicializarFormatos()
        {
            //FormatoGeneral(grdCabPreOC);
            FormatoGeneral(grdCabPreSal);
            //FormatoGeneral(grdDetPreOC);
            FormatoGeneral(grdDetPreSal);
        }
        private void CargaCabPreSalidas()
        {
            try
            {
                //tdCab = PreSal.tbPreSalidas(" cab.estatus = 'PEN' ", "cab.IdPreSalida DESC")
                resp = new PreSalidasSvc().getCabPreSalidaxFilter(0, " c.estatus = 'PEN' ", "c.IdPreSalida DESC");
                if (resp.typeResult == ResultTypes.success)
                {
                    listaCabPreSal = (List<CabPreSalida>)resp.result;
                    grdCabPreSal.DataSource = null;
                    grdCabPreSal.DataSource = listaCabPreSal;
                    CargaDetPreSalidas(listaCabPreSal[0].IdPreSalida);
                    FormatogrdCabPreSal();
                }
                else
                {
                    grdCabPreSal.DataSource = null;
                }
                //List<CabPreSalida> listaCabPreSal;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }
        private void CargaDetPreSalidas(int IdPreSalida)
        {
            try
            {
                resp = new PreSalidasSvc().getDetPreSalidaxFilter(IdPreSalida);
                if (resp.typeResult == ResultTypes.success)
                {
                    listaDetPreSal = (List<DetPreSalida>)resp.result;
                    //Verificar Existencias
                    foreach (var item in listaDetPreSal)
                    {
                        resp = new ComercialSvc().getadmExistenciaxProd(item.idProducto, 
                            Tools.FormatFecHora(DateTime.Now,true,false,false), 
                            Convert.ToInt32(parametro.idAlmacenSal) );
                        if (resp.typeResult == ResultTypes.success)
                        {
                            existencia  = ((List<admExistencia>)resp.result)[0];
                            item.Existencia = Convert.ToDecimal(existencia.Existencia) ;
                        }
                        else
                        {
                            item.Existencia = 0;
                        }
                        resp = new PreSalidasSvc().getApartadoxProd(item.idProducto, "  c.Estatus = 'PEN'");
                        if (resp.typeResult == ResultTypes.success)
                        {
                            apartado = ((List<Apartados>)resp.result)[0];
                            item.Existencia = item.Existencia - apartado.Apartado;
                        }
                    }
                    //Verificar Existencias
                    grdDetPreSal.DataSource = null;
                    grdDetPreSal.DataSource = listaDetPreSal;
                    FormatogrdDetPreSal();
                    //List<DetPreSalida> listaDetPreSal;
                }
                else
                {
                    grdDetPreSal.DataSource = null;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }
        //private void CargaCabPreOC()
        //{
        //    try
        //    {
        //         resp = new OrdenCompraSvc().getCabPreOCxFilter(0, " ISNULL(c.idOC,0) > 0 AND c.Estatus = 'COM' ", " c.IdPreOC DESC");
        //        if (resp.typeResult == ResultTypes.success)
        //        {
        //            listaCabPreOC = (List<CabPreOC>)resp.result;
        //            grdCabPreOC.DataSource = null;
        //            grdCabPreOC.DataSource = listaCabPreOC;
        //            CargaDetPreOC(listaCabPreOC[0].IdPreOC);
        //            //List<CabPreOC> listaCabPreOC;
        //            FormatogrdCabPreOC();
        //        }
        //        else
        //        {
        //            grdCabPreOC.DataSource = null;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show("Error: " + ex.Message);
        //    }
        //}
        //private void CargaDetPreOC(int idOC)
        //{
        //    try
        //    {
        //         resp = new OrdenCompraSvc().getDetPreOCxFilter(idOC);
        //        if (resp.typeResult == ResultTypes.success)
        //        {
        //            listaDetPreOC = (List<DetPreOC>)resp.result;
        //            //Verificar Existencias
        //            foreach (var item in listaDetPreOC)
        //            {
        //                resp = new ComercialSvc().getadmExistenciaxProd(item.idProducto,
        //                    Tools.FormatFecHora(DateTime.Now, true, false, false),
        //                    Convert.ToInt32(parametro.idAlmacenSal));
        //                if (resp.typeResult == ResultTypes.success)
        //                {
        //                    existencia = ((List<admExistencia>)resp.result)[0];
        //                    item.Existencia = Convert.ToDecimal(existencia.Existencia);
        //                }
        //                else
        //                {
        //                    item.Existencia = 0;
        //                }
        //                resp = new PreSalidasSvc().getApartadoxProd(item.idProducto, "  c.Estatus = 'PEN'");
        //                if (resp.typeResult == ResultTypes.success)
        //                {
        //                    apartado = ((List<Apartados>)resp.result)[0];
        //                    item.Existencia = item.Existencia - apartado.Apartado;
        //                }
        //            }
        //            //Verificar Existencias

        //            grdDetPreOC.DataSource = null;
        //            grdDetPreOC.DataSource = listaDetPreOC;
        //            FormatogrdDetPreOC();
        //        }
        //        else
        //        {
        //            grdDetPreOC.DataSource = null;
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show("Error: " + ex.Message);
        //    }
        //}

        private void grdCabPreSal_Click(object sender, EventArgs e)
        {
            CargaDetPreSalidas(listaCabPreSal[grdCabPreSal.CurrentCell.RowIndex].IdPreSalida);
        }

        private void grdCabPreSal_DoubleClick(object sender, EventArgs e)
        {
            //PAra Enviar a la Pantalla de descarga
            //fEntregaRefaccionesOS frm = new fEntregaRefaccionesOS(listaCabOS[grdCab.CurrentCell.RowIndex].IdEmpresa, listaCabOS[grdCab.CurrentCell.RowIndex].idOrdenSer);
            fEntregaRefaccionesOS frm = new fEntregaRefaccionesOS(_usuario, listaCabPreSal[grdCabPreSal.CurrentCell.RowIndex].IdPreSalida, Clases.Inicio.eTipoSalida.PRESALIDA);
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            btnCancelar.PerformClick();
        }

        //private void grdCabPreOC_Click(object sender, EventArgs e)
        //{
        //    CargaDetPreOC(listaCabPreOC[grdCabPreOC.CurrentCell.RowIndex].IdPreOC);
        //}

        //private void grdCabPreOC_DoubleClick(object sender, EventArgs e)
        //{
        //    fEntregaRefaccionesOS frm = new fEntregaRefaccionesOS(_usuario,
        //    listaCabPreOC[grdCabPreOC.CurrentCell.RowIndex].IdPreOC,
        //    Clases.Inicio.eTipoSalida.PREORDENCOMPRA);
        //    frm.StartPosition = FormStartPosition.CenterParent;
        //    frm.ShowDialog();
        //    btnCancelar.PerformClick();

        //}

        #region Formatos
        private void FormatoGeneral(DataGridView grid)
        {
            //ESTILO GENERAL
            grid.RowsDefaultCellStyle.BackColor = Color.White;
            grid.AlternatingRowsDefaultCellStyle.BackColor = Color.AliceBlue;

            grid.RowsDefaultCellStyle.SelectionForeColor = Color.Yellow;
            grid.RowsDefaultCellStyle.SelectionBackColor = Color.Gray;
            grid.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            grid.DefaultCellStyle.Font = new Font("Berlin Sans FB", 12);
            grid.ColumnHeadersDefaultCellStyle.Font = new Font("Berlin Sans FB", 12);
            grid.ColumnHeadersDefaultCellStyle.BackColor = Color.Yellow;
            grid.ColumnHeadersDefaultCellStyle.ForeColor = Color.Red;
            grid.EnableHeadersVisualStyles = false;
        }
        private void FormatogrdCabPreSal()
        {
            //grdCabPreSal
            grdCabPreSal.Columns["IdEmpleadoEnc"].Visible = false;
            grdCabPreSal.Columns["UserSolicitud"].Visible = false;

            grdCabPreSal.Columns["idAlmacen"].Visible = false;
            grdCabPreSal.Columns["NomAlmacen"].Visible = false;
            grdCabPreSal.Columns["CIDALMACEN_COM"].Visible = false;
            grdCabPreSal.Columns["Estatus"].Visible = false;
            grdCabPreSal.Columns["SubTotal"].Visible = false;
            grdCabPreSal.Columns["FecCancela"].Visible = false;
            grdCabPreSal.Columns["UserCancela"].Visible = false;

            grdCabPreSal.Columns["MotivoCancela"].Visible = false;
            grdCabPreSal.Columns["FecEntrega"].Visible = false;
            grdCabPreSal.Columns["UserEntrega"].Visible = false;
            grdCabPreSal.Columns["IdPersonalRec"].Visible = false;
            grdCabPreSal.Columns["EntregaCambio"].Visible = false;
            grdCabPreSal.Columns["idPreOC"].Visible = false;
            grdCabPreSal.Columns["CSERIEDOCUMENTO_ENT"].Visible = false;
            grdCabPreSal.Columns["CIDDOCUMENTO_ENT"].Visible = false;
            grdCabPreSal.Columns["CFOLIO_ENT"].Visible = false;
            grdCabPreSal.Columns["CSERIEDOCUMENTO_SAL"].Visible = false;
            grdCabPreSal.Columns["CIDDOCUMENTO_SAL"].Visible = false;
            grdCabPreSal.Columns["CFOLIO_SAL"].Visible = false;
            grdCabPreSal.Columns["NomEmpleadoEnc"].Visible = false;
            grdCabPreSal.Columns["NomUserSolicitud"].Visible = false;



            grdCabPreSal.Columns["IdPreSalida"].HeaderText = "Pre Salida";
            grdCabPreSal.Columns["idOrdenTrabajo"].HeaderText = "O. Trabajo";
            grdCabPreSal.Columns["idOrdenSer"].HeaderText = "O. Servicio";
            grdCabPreSal.Columns["idUnidadTrans"].HeaderText = "Unidad";
            grdCabPreSal.Columns["FecSolicitud"].HeaderText = "Fecha";
            //grdCabPreSal.Columns["NomEmpleadoEnc"].HeaderText = "Encargado";
            //grdCabPreSal.Columns["NotaRecepcion"].HeaderText = "Actividad";
            //grdCabPreSal.Columns["NomSolicita"].HeaderText = "Solicita";

            grdCabPreSal.Columns["IdPreSalida"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdCabPreSal.Columns["idOrdenTrabajo"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdCabPreSal.Columns["FecSolicitud"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            grdCabPreSal.Columns["idOrdenSer"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdCabPreSal.Columns["idUnidadTrans"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            //grdCabPreSal.Columns["NomSolicita"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            grdCabPreSal.Columns["IdPreSalida"].DisplayIndex = 0;
            grdCabPreSal.Columns["idOrdenTrabajo"].DisplayIndex = 1;
            grdCabPreSal.Columns["idOrdenSer"].DisplayIndex = 2;
            grdCabPreSal.Columns["idUnidadTrans"].DisplayIndex = 3;
            grdCabPreSal.Columns["FecSolicitud"].DisplayIndex = 4;

        }
        private void FormatogrdDetPreSal()
        {
            grdDetPreSal.Columns["Seleccionado"].Visible = false;
            grdDetPreSal.Columns["IdPreSalida"].Visible = false;
            //grdDetPreSal.Columns["Entregar"].Visible = false;
            grdDetPreSal.Columns["CantidadEnt"].Visible = false;
            grdDetPreSal.Columns["Costo"].Visible = false;
            grdDetPreSal.Columns["idUnidadProd"].Visible = false;
            //grdDetPreSal.Columns["Existencia"].Visible = false;
            grdDetPreSal.Columns["Inserta"].Visible = false;
            grdDetPreSal.Columns["CantEntregaCambio"].Visible = false;

            grdDetPreSal.Columns["Cantidad"].HeaderText = "Cantidad";
            grdDetPreSal.Columns["idProducto"].HeaderText = "Producto";
            grdDetPreSal.Columns["CCODIGOPRODUCTO"].HeaderText = "Código";
            grdDetPreSal.Columns["CNOMBREPRODUCTO"].HeaderText = "Descripción";
            grdDetPreSal.Columns["Existencia"].HeaderText = "Existencia";
            //grdDetPreSal.Columns["IdPreSalida"].HeaderText = "Folio";

            grdDetPreSal.Columns["Cantidad"].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
            grdDetPreSal.Columns["idProducto"].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
            grdDetPreSal.Columns["CCODIGOPRODUCTO"].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
            grdDetPreSal.Columns["CNOMBREPRODUCTO"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            grdDetPreSal.Columns["Existencia"].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;

        }

        //private void FormatogrdCabPreOC()
        //{
        //    //public int  { get; set; }
        //    //public int  { get; set; }
        //    //public DateTime  { get; set; }

        //    grdCabPreOC.Columns["idSolOC"].Visible = false;
        //    grdCabPreOC.Columns["idCotizacionOC"].Visible = false;
        //    grdCabPreOC.Columns["idOC"].Visible = false;
        //    grdCabPreOC.Columns["UserSolicita"].Visible = false;
        //    grdCabPreOC.Columns["idAlmacen"].Visible = false;
        //    grdCabPreOC.Columns["Estatus"].Visible = false;
        //    grdCabPreOC.Columns["UserAutorizaOC"].Visible = false;
        //    grdCabPreOC.Columns["FecAutorizaOC"].Visible = false;
        //    grdCabPreOC.Columns["Observaciones"].Visible = false;
        //    grdCabPreOC.Columns["FecModifica"].Visible = false;
        //    grdCabPreOC.Columns["UserModifica"].Visible = false;
        //    grdCabPreOC.Columns["SubTotal"].Visible = false;
        //    grdCabPreOC.Columns["IVA"].Visible = false;
        //    grdCabPreOC.Columns["FecCancela"].Visible = false;
        //    grdCabPreOC.Columns["UserCancela"].Visible = false;
        //    grdCabPreOC.Columns["MotivoCancela"].Visible = false;
        //    grdCabPreOC.Columns["CSERIEDOCUMENTO"].Visible = false;
        //    grdCabPreOC.Columns["CIDDOCUMENTO"].Visible = false;
        //    grdCabPreOC.Columns["CFOLIO"].Visible = false;
        //    grdCabPreOC.Columns["IdPreSalida"].Visible = false;


        //    grdCabPreOC.Columns["IdPreOC"].HeaderText = "Pre Orden Compra";
        //    grdCabPreOC.Columns["idOrdenTrabajo"].HeaderText = "O. Trabajo";
        //    grdCabPreOC.Columns["FechaPreOc"].HeaderText = "Fecha";

        //    grdCabPreOC.Columns["IdPreOC"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
        //    grdCabPreOC.Columns["idOrdenTrabajo"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
        //    grdCabPreOC.Columns["FechaPreOc"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

        //}

        //private void FormatogrdDetPreOC()
        //{

        //    //public int idProducto { get; set; }
        //    //public string CCODIGOPRODUCTO { get; set; }
        //    //public string CNOMBREPRODUCTO { get; set; }
        //    //public decimal CantidadSol { get; set; }

        //    grdDetPreOC.Columns["IdPreOC"].Visible = false;
        //    grdDetPreOC.Columns["idUnidadProd"].Visible = false;
        //    grdDetPreOC.Columns["CantSinCosto"].Visible = false;
        //    grdDetPreOC.Columns["CantFaltante"].Visible = false;
        //    grdDetPreOC.Columns["Precio"].Visible = false;
        //    grdDetPreOC.Columns["PorcIVA"].Visible = false;
        //    grdDetPreOC.Columns["Desc1"].Visible = false;
        //    grdDetPreOC.Columns["Desc2"].Visible = false;
        //    grdDetPreOC.Columns["Inserta"].Visible = false;
        //    grdDetPreOC.Columns["Seleccionado"].Visible = false;
        //    grdDetPreOC.Columns["CantidadEnt"].Visible = false;


        //    grdDetPreOC.Columns["idProducto"].HeaderText = "Producto";
        //    grdDetPreOC.Columns["CCODIGOPRODUCTO"].HeaderText = "Codigo";
        //    grdDetPreOC.Columns["CNOMBREPRODUCTO"].HeaderText = "Descripcion";
        //    grdDetPreOC.Columns["CantidadSol"].HeaderText = "Cantidad";

        //    grdDetPreOC.Columns["CantidadSol"].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
        //    grdDetPreOC.Columns["idProducto"].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
        //    grdDetPreOC.Columns["CCODIGOPRODUCTO"].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
        //    grdDetPreOC.Columns["CNOMBREPRODUCTO"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

        //}
        #endregion

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Refresca();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void grdCabPreSal_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void grdCabPreOC_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void cancelarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                //string Motivo;
                //PreSalaCancelar = "";
                //BandExito = false;
                //Indice = grdOT.CurrentCell.RowIndex;
                //IdOrdenTrabajo = listaDetROS[Indice].idOrdenTrabajo;
                //caption = this.Name;
                //buttons = MessageBoxButtons.OKCancel;
                ////mensaje = "Desea guardar la Liquidacion para el operador: " + txtNomOperador.Text;
                //mensaje = "Desea Cancelar la Orden de Trabajo: " + IdOrdenTrabajo + " ?";


                ////checar si tiene presalidas pendientes
                //if (listCabPresal.Count > 0)
                //{
                //    foreach (var item in listCabPresal)
                //    {
                //        if (item.idOrdenTrabajo == IdOrdenTrabajo)
                //        {
                //            if (item.Estatus == "ENT")
                //            {
                //                //No se puede Cancelar
                //                MessageBox.Show("No se puede Cancelar la Orden de Trabajo: " + IdOrdenTrabajo + " ya que tiene Salida de Insumo Entregada (" + item.IdPreSalida + ")", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //                return;

                //            }
                //            else if (item.Estatus == "PEN")
                //            {
                //                //Para Cancelar
                //                PreSalaCancelar = PreSalaCancelar + " " + item.IdPreSalida + ",";


                //            }
                //        }
                //    }
                //}
                ////else
                ////{
                ////    //Continua
                ////}
                //if (PreSalaCancelar != "")
                //{

                //    if (PreSalaCancelar != "")
                //    {
                //        PreSalaCancelar = PreSalaCancelar.Substring(0, PreSalaCancelar.Length - 1);
                //    }
                //    mensaje = "Desea Cancelar la Orden de Trabajo: " + IdOrdenTrabajo + " ?" + Environment.NewLine
                //        + "Con las Pre Salidas Pendientes de Entrega = " + PreSalaCancelar;
                //}

                //result = MessageBox.Show(mensaje, caption, buttons);
                //if (result == System.Windows.Forms.DialogResult.OK)
                //{


                //    Motivo = "";
                //    while (Motivo == "" || Motivo == null)
                //    {
                //        fAuxiliar fcan = new fAuxiliar("Cancelacion de Orden de Trabajo: " + listaDetOS[Indice].idOrdenTrabajo, "Motivo Cancelación", 150, true, CharacterCasing.Upper, HorizontalAlignment.Left);
                //        fcan.StartPosition = FormStartPosition.CenterScreen;
                //        Motivo = fcan.DevuelveValor();

                //        if (Motivo == "" || Motivo == null)
                //        {
                //            mensaje = "El Motivo de la Cancelación no puede ser Vacia, Desea Abortar la Cancelación ?";
                //            result = MessageBox.Show(mensaje, caption, buttons);
                //            if (result == System.Windows.Forms.DialogResult.OK)
                //            {
                //                return;
                //            }

                //        }
                //    }
                //    listaDetROS[Indice].isCancelado = true;
                //    listaDetROS[Indice].motivoCancelacion = Motivo;
                //    listaDetOS[Indice].Estatus = "CAN";

                //    detos = listaDetOS[Indice];
                //    detros = listaDetROS[Indice];
                //    resp = new DetOrdenServicioSvc().GuardaDetOrdenServicio(ref detos);
                //    if (resp.typeResult == ResultTypes.success)
                //    {
                //        BandExito = true;
                //        resp = new DetRecepcionOSSvc().GuardaDetRecepcionOS(ref detros);
                //        if (resp.typeResult == ResultTypes.success)
                //        {
                //            BandExito = true;
                //            if (PreSalaCancelar != "")
                //            {
                //                foreach (var item in listCabPresal)
                //                {
                //                    if (item.idOrdenTrabajo == IdOrdenTrabajo)
                //                    {
                //                        if (item.Estatus == "PEN")
                //                        {
                //                            item.Estatus = "CAN";
                //                            item.FecCancela = DateTime.Now;
                //                            item.UserCancela = _Usuario.nombreUsuario;
                //                            item.MotivoCancela = "Aplicada por cancelación de Orden de trabajo";

                //                            cabpresal = item;
                //                            resp = new PreSalidasSvc().GuardaCabPreSalida(ref cabpresal);
                //                            if (resp.typeResult == ResultTypes.success)
                //                            {
                //                                BandExito = true;
                //                            }
                //                            else
                //                            {
                //                                BandExito = false;
                //                            }
                //                        }
                //                    }

                //                }
                //            }
                //        }
                //        else
                //        {
                //            BandExito = false;
                //        }



                //        if (BandExito)
                //        {
                //            if (PreSalaCancelar != "")
                //            {
                //                MessageBox.Show("Se Cancelo la Orden de Trabajo " + detos.idOrdenTrabajo + Environment.NewLine
                //                    + "Con las Pre Salidas: " + PreSalaCancelar, "Operación Exitosa", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //            }
                //            else
                //            {
                //                MessageBox.Show("Se Cancelo la Orden de Trabajo " + detos.idOrdenTrabajo, "Operación Exitosa", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //            }

                //            //actualizar detalle
                //            CargaForma(opcBusqueda.CabOrdenServicio, txtidOrdenSer.Text);
                //        }
                //        else
                //        {

                //        }

                //    }
                //    else
                //    {
                //        //ocurrio un error

                //    }
                //}





            }
            catch (Exception)
            {

                //MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + ex.Message,
                //  this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
    }//
}
