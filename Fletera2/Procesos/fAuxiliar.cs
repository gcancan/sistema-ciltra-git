﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fletera2.Procesos
{
    public partial class fAuxiliar : Form
    {
        string _Encabezado;
        string _ValorDevuelve;
        string _NomEtiqueta1;
        int _TamanioCampo1;
        bool _EsMultilinea;
        CharacterCasing _caracter;
        HorizontalAlignment _alinea;

        public fAuxiliar(string Encabezado, string NomEtiqueta1, int TamanioCampo1, bool EsMultilinea, 
            CharacterCasing caracter, HorizontalAlignment  alinea)
        {
            
            _Encabezado = Encabezado;
            _NomEtiqueta1 = NomEtiqueta1;
            _TamanioCampo1 = TamanioCampo1;
            _EsMultilinea = EsMultilinea;
            _caracter = caracter;
            _alinea = alinea;



            InitializeComponent();
        }

        private void fAuxiliar_Load(object sender, EventArgs e)
        {
            this.Text = _Encabezado;
            lblCampo1.Text = _NomEtiqueta1;
            //lblCampo1.AutoSize = false;
            txtCampo1.MaxLength = _TamanioCampo1;
            txtCampo1.Multiline = _EsMultilinea;
            txtCampo1.CharacterCasing = _caracter;
            txtCampo1.TextAlign = _alinea;
        }

        private void btnGrabar_Click(object sender, EventArgs e)
        {
            _ValorDevuelve = txtCampo1.Text;
            if (_ValorDevuelve != "")
            {
                DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        public string DevuelveValor()
        {
            try
            {
                DialogResult dResult = ShowDialog();
                if (dResult == DialogResult.OK && _ValorDevuelve != null)
                {
                    return _ValorDevuelve;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {

                return null;
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }
    }//
}
