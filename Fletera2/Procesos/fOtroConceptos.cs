﻿using Core.Interfaces;
using Core.Models;
using Fletera2.Clases;
using Fletera2Entidades;
using Fletera2Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fletera2.Procesos
{
    public partial class fOtroConceptos : Form
    {
        Usuario _usuario;
        List<CatConceptosLiq> listConceptos;
        List<CatPersonal> listOperador;
        List<CabGuia> listViajes;

        //Para MsgBox
        string caption;
        MessageBoxButtons buttons = MessageBoxButtons.YesNo;
        DialogResult result;
        string mensaje = "";

        public fOtroConceptos(Usuario user)
        {
            _usuario = user;
            InitializeComponent();
        }

        private void fOtroConceptos_Load(object sender, EventArgs e)
        {
            //Cargar Viajes de la empresa
            CargaViajes();
            CargaOperadores();
            CargaCombos();
            HabilitaCampos(opcHabilita.DesHabilitaTodos);

        }

        private void CargaViajes()
        {
            AutoCompleteStringCollection ColeccionX = new AutoCompleteStringCollection();

            OperationResult resp = new CabGuiaSvc().getCabGuiaxFilter(0,
            " c.idEmpresa = " + _usuario.personal.clave_empresa + " and isnull(c.NumLiquidacion,0) = 0");

            if (resp.typeResult == ResultTypes.success)
            {
                listViajes = (List<CabGuia>)resp.result;
                foreach (var item in listViajes)
                {
                    ColeccionX.Add(Convert.ToString(item.NumGuiaId));
                }
                txtNumGuiaId.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                txtNumGuiaId.AutoCompleteSource = AutoCompleteSource.CustomSource;
                txtNumGuiaId.AutoCompleteCustomSource = ColeccionX;
            }
        }
        private void CargaCombos()
        {
            OperationResult resp = new CatConceptosLiqSvc().getCatConceptosLiqxId(0, " activo = 1 ");
            if (resp.typeResult == ResultTypes.success)
            {
                listConceptos = (List<CatConceptosLiq>)resp.result;

                //cmbidEmpresa.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                //cmbidEmpresa.AutoCompleteSource = AutoCompleteSource.ListItems;
                cmbidConcepto.DataSource = listConceptos;
                cmbidConcepto.DisplayMember = "NomConcepto";
                cmbidConcepto.SelectedIndex = 0;
            }
        }
        private void CargaOperadores()
        {
            AutoCompleteStringCollection ColeccionX = new AutoCompleteStringCollection();
            //AutoCompleteStringCollection ColeccionY = new AutoCompleteStringCollection();

            OperationResult resp = new CatPersonalSvc().getCatPersonalxFiltro("  per.idPuesto = 7 and per.idEmpresa = " + _usuario.personal.clave_empresa, "  per.NombreCompleto");

            if (resp.typeResult == ResultTypes.success)
            {
                listOperador = (List<CatPersonal>)resp.result;
                foreach (var item in listOperador)
                {
                    ColeccionX.Add(Convert.ToString(item.idPersonal));
                    //ColeccionY.Add(Convert.ToString(item.NombreCompleto));
                }
                txtidOperador.Tag = listOperador;
                txtidOperador.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                txtidOperador.AutoCompleteSource = AutoCompleteSource.CustomSource;
                txtidOperador.AutoCompleteCustomSource = ColeccionX;

                //txtNomOperador.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                //txtNomOperador.AutoCompleteSource = AutoCompleteSource.CustomSource;
                //txtNomOperador.AutoCompleteCustomSource = ColeccionY;
            }
        }

        private void LimpiaCampos()
        {
            txtNumFolio.Clear();
            txtNumGuiaId.Clear();
            txtTractor.Clear();
            txtRemolque1.Clear();
            txtDolly.Clear();
            txtRemolque2.Clear();
            txtidOperador.Clear();
            txtNomOperador.Clear();

            cmbidConcepto.SelectedIndex = 0;
            chkEnParcialidades.Checked = true;
            txtImporte.Clear();
            txtImporteApli.Clear();

            dtpFecha.Value = DateTime.Now;
            dtpFecha.Value = DateTime.Now;   

        }

        private void HabilitaCampos(opcHabilita opc)
        {
            txtTractor.Enabled = false;
            txtRemolque1.Enabled = false;
            txtDolly.Enabled = false;
            txtRemolque2.Enabled = false;
            txtNomOperador.Enabled = false;
            txtImporteApli.Enabled = false;
            dtpFecha.Enabled = false;
            dtpFechaHora.Enabled = false;
            switch (opc)
            {
                case opcHabilita.DesHabilitaTodos:
                    txtNumFolio.Enabled = false;
                    txtNumGuiaId.Enabled = false;
                    txtidOperador.Enabled = false;
                    cmbidConcepto.Enabled = false;
                    chkEnParcialidades.Enabled = false;
                    txtImporte.Enabled = false;
                    break;
                case opcHabilita.Iniciando:
                    break;
                case opcHabilita.Editando:
                    break;
                case opcHabilita.Nuevo:
                    txtNumFolio.Enabled = false;
                    txtNumGuiaId.Enabled = true;
                    txtidOperador.Enabled = true;

                    cmbidConcepto.Enabled = true;
                    chkEnParcialidades.Enabled = true;
                    txtImporte.Enabled = true;

                    break;
                default:
                    break;
            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            HabilitaCampos(opcHabilita.Nuevo);
            txtNumGuiaId.Focus();
        }


        private void txtidOperador_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtNomOperador.Text = BuscaNomOperador(Convert.ToInt32(txtidOperador.Text));
            }
        }

        private void txtNumGuiaId_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                MapeaViaje(Convert.ToInt32(txtNumGuiaId.Text));
            }
        }

        private string BuscaNomOperador(int idOperador)
        {
            for (int i = 0; i < listOperador.Count; i++)
            {
                if (listOperador[i].idPersonal == idOperador)
                {
                    return listOperador[i].NombreCompleto;
                }
            }
            return "No se Encontro";
        }
        private void MapeaViaje(int NumGuiaId)
        {
            for (int i = 0; i < listViajes.Count; i++)
            {
                if (listViajes[i].NumGuiaId.ToString() == txtNumGuiaId.Text)
                {
                    txtTractor.Text = listViajes[i].idTractor;
                    txtRemolque1.Text = listViajes[i].idRemolque1;
                    txtDolly.Text = listViajes[i].idDolly;
                    txtRemolque2.Text = listViajes[i].idRemolque2;
                    txtidOperador.Text = listViajes[i].idOperador.ToString();
                    txtNomOperador.Text = BuscaNomOperador(listViajes[i].idOperador);
                    break;
                }
            }
        }

        private void label16_Click(object sender, EventArgs e)
        {

        }

        private void txtNumFolio_TextChanged(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void btnGrabar_Click(object sender, EventArgs e)
        {
            caption = this.Name;
            buttons = MessageBoxButtons.OKCancel;
            mensaje = "Desea Ingresar Otro Cargo/Abono para : "  + txtNomOperador.Text + " ? " ;
            result = MessageBox.Show(mensaje, caption, buttons);
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                if (!Validar())
                {
                    return;
                }
                OtrosConcLiq folio = mapForm();
                if (folio != null)
                {
                    OperationResult resp = new OtrosConcLiqSvc().GuardaOtrosConcLiq(ref folio);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        MessageBox.Show("Folio Creado Satisfactoriamente", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        //ImprimirTicketFolioDinero(folio, _usuario.personal, true, true);
                        Cancelar();
                        return;
                    }
                }

            }
        }
        private bool Validar()
        {
            if (txtImporte.Text == "")
            {
                MessageBox.Show("El importe no puede ser vacio", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtImporte.Focus();
                return false;

            }
            else if (Convert.ToDecimal(txtImporte.Text) <= 0   )
            {
                MessageBox.Show("El importe no puede ser Cero", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtImporte.Focus();
                return false;
            }

            return true;
           
        }

        private OtrosConcLiq mapForm()
        {
            try
            {
                return new OtrosConcLiq
                {
                    NumFolio = txtNumFolio.Tag == null ? 0 : ((OtrosConcLiq)txtNumFolio.Tag).NumFolio,
                    NumGuiaId = Convert.ToInt32(txtNumGuiaId.Text) ,
                    idOperador = Convert.ToInt32(txtidOperador.Text),
                    idConcepto = ((CatConceptosLiq)cmbidConcepto.SelectedValue).idConcepto,
                    Importe = (txtImporte.Text == "" ? 0 : Convert.ToDecimal(txtImporte.Text)),
                    EnParcialidades =  chkEnParcialidades.Checked,
                    userCrea = _usuario.nombreUsuario,
                    Fecha = DateTime.Now,
                    idEmpresa = Convert.ToInt32(_usuario.personal.clave_empresa),
                    Cancelado = false,
                    MotivoCancela = "",
                    ImporteAplicado = 0
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrio un error al querer obtener información del catálogo" +
                    Environment.NewLine + ex.Message, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }
        private void Cancelar()
        {
            LimpiaCampos();
            HabilitaCampos(opcHabilita.DesHabilitaTodos);
        }

        private void txtNumGuiaId_TextChanged(object sender, EventArgs e)
        {

        }
    }//
}
