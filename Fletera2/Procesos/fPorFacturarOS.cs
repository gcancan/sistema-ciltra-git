﻿using Core.Models;
using Fletera2Entidades;
using Fletera2Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fletera2.Procesos
{
    public partial class fPorFacturarOS : Form
    {
        Usuario _usuario;
        List<CabOrdenServicio> listaCabOS;
        List<DetOrdenServicio> listaDetOS;
        List<DOSInsumosVista> listaDetRef;


        public fPorFacturarOS(Usuario user)
        {
            _usuario = user;
            InitializeComponent();
        }

        private void fPorFacturarOS_Load(object sender, EventArgs e)
        {
            Refrescar();
        }

        private void Refrescar()
        {
            FormatoGeneral(grdCab);
            FormatoGeneral(grdDet);
            FormatoGeneral(grdRefac);
            CargaOrdenes();
        }

        private void CargaOrdenes()
        {
            //OperationResult respc = new CabOrdenServicioSvc().getCabOrdenServicioxFilter(0, " c.estatus = 'TER' and c.idTipOrdServ > 1 and isnull(c.CIDDOCUMENTO,0) = 0 ", " c.idOrdenSer desc");
            OperationResult respc = new CabOrdenServicioSvc().getCabOrdenServicioxFilter(0, " c.estatus = 'TER' and c.idTipOrdServ > 1 and isnull(c.CIDDOCUMENTO,0) = 0 ", " c.idOrdenSer desc");
            if (respc.typeResult == ResultTypes.success)
            {
                //List<CabOrdenServicio> listaCabOS;
                listaCabOS = (List<CabOrdenServicio>)respc.result;
                grdCab.DataSource = null;
                grdCab.DataSource = listaCabOS;
                CargaDetalleOS(listaCabOS[0].idOrdenSer);
                FormatogrdCab();
            }
            else
            {
                //No se encontraron 
            }
        }
        private void CargaDetalleOS(int idOrdenSer)
        {
            OperationResult respd = new DetOrdenServicioSvc().getDetOrdenServicioxFilter(idOrdenSer, " d.Estatus = 'TER'");
            if (respd.typeResult == ResultTypes.success)
            {
                listaDetOS = (List<DetOrdenServicio>)respd.result;
                grdDet.DataSource = null;
                grdDet.DataSource = listaDetOS;
                CargaRefaciones(idOrdenSer, listaDetOS[0].idOrdenActividad);
                FormatogrdDet();
            }
            else
            {
                grdDet.DataSource = null;
            }
        }

        private void CargaRefaciones(int idOrdenSer, int idOrdenActividad)
        {
            OperationResult respr = new DetOrdenServicioSvc().getDOSInsumosVistaxFilter(idOrdenSer, 
            " dap.idOrdenActividad = " + idOrdenActividad + " AND cabpresal.Estatus = 'ENT'");
            if (respr.typeResult == ResultTypes.success)
            {
                listaDetRef = (List<DOSInsumosVista>)respr.result;
                grdRefac.DataSource = null;
                grdRefac.DataSource = listaDetRef;
                FormatogrdRefac();
            }
            else
            {
                grdRefac.DataSource = null;
            }
        }

        private void grdCab_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void grdCab_Click(object sender, EventArgs e)
        {
            CargaDetalleOS(listaCabOS[grdCab.CurrentCell.RowIndex].idOrdenSer);
        }

        private void grdDet_Click(object sender, EventArgs e)
        {
            CargaRefaciones(listaDetOS[grdDet.CurrentCell.RowIndex].idOrdenSer, listaDetOS[grdDet.CurrentCell.RowIndex].idOrdenActividad);
        }

        private void grdCab_DoubleClick(object sender, EventArgs e)
        {
            //Enviar a facturar
            fFinalizaOs frm = new fFinalizaOs(_usuario,listaCabOS[grdCab.CurrentCell.RowIndex].IdEmpresa, listaCabOS[grdCab.CurrentCell.RowIndex].idOrdenSer);
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            btnCancelar.PerformClick();
        }

        private void FormatoGeneral(DataGridView grid)
        {
            //ESTILO GENERAL
            grid.RowsDefaultCellStyle.BackColor = Color.White;
            grid.AlternatingRowsDefaultCellStyle.BackColor = Color.AliceBlue;

            grid.RowsDefaultCellStyle.SelectionForeColor = Color.Yellow;
            grid.RowsDefaultCellStyle.SelectionBackColor = Color.Gray;
            grid.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            grid.DefaultCellStyle.Font = new Font("Berlin Sans FB", 12);
            grid.ColumnHeadersDefaultCellStyle.Font = new Font("Berlin Sans FB", 12);
            grid.ColumnHeadersDefaultCellStyle.BackColor = Color.Yellow;
            grid.ColumnHeadersDefaultCellStyle.ForeColor = Color.Red;
            grid.EnableHeadersVisualStyles = false;
        }

        private void FormatogrdCab()
        {
            //public int idOrdenSer { get; set; }
            //public string RazonSocial { get; set; }
            //public string idUnidadTrans { get; set; }
            //public string NomTipoUnidad { get; set; }
            //public DateTime FechaAsignado { get; set; }
            //public DateTime FechaTerminado { get; set; }
            //public string NomTipOrdServ { get; set; }
            //public string NomOperador { get; set; }
            //public string NomTipoServicio { get; set; }

            //public int IdEmpresa { get; set; }
            //public int idTipoOrden { get; set; }
            //public DateTime FechaRecepcion { get; set; }
            //public int idEmpleadoEntrega { get; set; }
            //public string NomEmpleadoEntrega { get; set; }
            //public string UsuarioRecepcion { get; set; }
            //public decimal Kilometraje { get; set; }
            //public string Estatus { get; set; }
            //public DateTime FechaDiagnostico { get; set; }
            //public DateTime FechaEntregado { get; set; }
            //public string UsuarioEntrega { get; set; }
            //public int idEmpleadoRecibe { get; set; }
            //public string NotaFinal { get; set; }
            //public string usuarioCan { get; set; }
            //public DateTime FechaCancela { get; set; }
            //public int idEmpleadoAutoriza { get; set; }
            //public DateTime fechaCaptura { get; set; }
            //public int idTipOrdServ { get; set; }
            //public int idTipoServicio { get; set; }
            //public int idPadreOrdSer { get; set; }
            //public int ClaveFicticia { get; set; }
            //public string NomEmpleadoAutoriza { get; set; }
            //public int idOperarador { get; set; }


            //OCULTAR COLUMNAS
            grdCab.Columns["IdEmpresa"].Visible = false;
            grdCab.Columns["idTipoOrden"].Visible = false;
            grdCab.Columns["FechaRecepcion"].Visible = false;
            grdCab.Columns["idEmpleadoEntrega"].Visible = false;
            grdCab.Columns["NomEmpleadoEntrega"].Visible = false;
            grdCab.Columns["UsuarioRecepcion"].Visible = false;
            grdCab.Columns["Kilometraje"].Visible = false;
            grdCab.Columns["Estatus"].Visible = false;
            grdCab.Columns["FechaDiagnostico"].Visible = false;
            grdCab.Columns["FechaEntregado"].Visible = false;
            grdCab.Columns["UsuarioEntrega"].Visible = false;
            grdCab.Columns["idEmpleadoRecibe"].Visible = false;
            grdCab.Columns["NotaFinal"].Visible = false;
            grdCab.Columns["usuarioCan"].Visible = false;
            grdCab.Columns["FechaCancela"].Visible = false;
            grdCab.Columns["idEmpleadoAutoriza"].Visible = false;
            grdCab.Columns["fechaCaptura"].Visible = false;
            grdCab.Columns["idTipOrdServ"].Visible = false;
            grdCab.Columns["idTipoServicio"].Visible = false;
            grdCab.Columns["idPadreOrdSer"].Visible = false;
            grdCab.Columns["ClaveFicticia"].Visible = false;
            grdCab.Columns["NomEmpleadoAutoriza"].Visible = false;
            grdCab.Columns["idOperarador"].Visible = false;
            //grdCab.Columns["x"].Visible = false;
            //grdCab.Columns["x"].Visible = false;
            //grdCab.Columns["x"].Visible = false;


            //HACER READONLY COLUMNAS
            //grdViajes.Columns["Seleccionado"].ReadOnly = true;

            //ENCABEZADO DE COLUMNAS
            grdCab.Columns["idOrdenSer"].HeaderText = "Orden Servicio";
            grdCab.Columns["RazonSocial"].HeaderText = "Cliente";
            grdCab.Columns["idUnidadTrans"].HeaderText = "Unidad";
            grdCab.Columns["NomTipoUnidad"].HeaderText = "Tipo";
            grdCab.Columns["FechaAsignado"].HeaderText = "Fecha Inicio";
            grdCab.Columns["FechaTerminado"].HeaderText = "Fecha Terminado";
            grdCab.Columns["NomTipOrdServ"].HeaderText = "Orden de";
            grdCab.Columns["NomOperador"].HeaderText = "Operador";
            grdCab.Columns["NomTipoServicio"].HeaderText = "Tipo";
            //grdCab.Columns["x"].HeaderText = "Estatus";
            //grdCab.Columns["x"].HeaderText = "Tipo Viaje";
            //grdCab.Columns["x"].HeaderText = "idTipoOrden";


            //FORMATO DE COLUMNAS
            //grdDatos.Columns["Precio"].DefaultCellStyle.Format = "C2";

            //ALINEACION DE COLUMNAS
            //grdDatos.Columns["Precio"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight;

            //TAMAÑO AUTOMATICO DE COLUMNAS
            grdCab.Columns["idOrdenSer"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdCab.Columns["RazonSocial"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdCab.Columns["idUnidadTrans"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdCab.Columns["NomTipoUnidad"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            //grdCab.Columns["idTipoOrden"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdCab.Columns["FechaAsignado"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdCab.Columns["FechaTerminado"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdCab.Columns["NomTipOrdServ"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdCab.Columns["NomOperador"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdCab.Columns["NomTipoServicio"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

            //grdCab.Columns["EstatusGuia"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            //grdCab.Columns["TipoViaje"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

        }
        private void FormatogrdDet()
        {
            //public int idOrdenTrabajo { get; set; }
            //public string NotaRecepcionF { get; set; }
            //public string TipoNotaRecepcionF { get; set; }
            //public string FallaReportada { get; set; }
            //public string NomPersonalResp { get; set; }
            //public string NomPersonalAyu1 { get; set; }
            //public string NomPersonalAyu2 { get; set; }

            //public string NomServicio { get; set; }
            //public bool Seleccionado { get; set; }
            //public int idOrdenSer { get; set; }
            //public int idOrdenActividad { get; set; }
            //public int idServicio { get; set; }
            //public int idActividad { get; set; }
            //public decimal DuracionActHr { get; set; }
            //public int NoPersonal { get; set; }
            //public int id_proveedor { get; set; }
            //public string Estatus { get; set; }
            //public decimal CostoManoObra { get; set; }
            //public decimal CostoProductos { get; set; }
            //public DateTime FechaDiag { get; set; }
            //public string UsuarioDiag { get; set; }
            //public DateTime FechaAsig { get; set; }
            //public string UsuarioAsig { get; set; }
            //public DateTime FechaPausado { get; set; }
            //public string NotaPausado { get; set; }
            //public DateTime FechaTerminado { get; set; }
            //public string UsuarioTerminado { get; set; }
            //public int idDivision { get; set; }
            //public string idLlanta { get; set; }
            //public int PosLlanta { get; set; }
            //public string NotaDiagnostico { get; set; }
            //public bool FaltInsumo { get; set; }
            //public bool Pendiente { get; set; }
            //public int CIDPRODUCTO_SERV { get; set; }
            //public decimal Cantidad_CIDPRODUCTO_SERV { get; set; }
            //public decimal Precio_CIDPRODUCTO_SERV { get; set; }

            //public int idFamilia { get; set; }
            //public string NotaRecepcion { get; set; }
            //public bool isCancelado { get; set; }
            //public string motivoCancelacion { get; set; }
            //public int idPersonalResp { get; set; }
            //public int idPersonalAyu1 { get; set; }
            //public int idPersonalAyu2 { get; set; }
            //public string NotaRecepcionA { get; set; }

            //OCULTAR COLUMNAS
            grdDet.Columns["NomServicio"].Visible = false;
            grdDet.Columns["Seleccionado"].Visible = false;
            grdDet.Columns["idOrdenSer"].Visible = false;
            grdDet.Columns["idOrdenActividad"].Visible = false;
            grdDet.Columns["idServicio"].Visible = false;
            grdDet.Columns["idActividad"].Visible = false;
            grdDet.Columns["DuracionActHr"].Visible = false;
            grdDet.Columns["NoPersonal"].Visible = false;
            grdDet.Columns["id_proveedor"].Visible = false;
            grdDet.Columns["Estatus"].Visible = false;
            grdDet.Columns["CostoManoObra"].Visible = false;
            grdDet.Columns["CostoProductos"].Visible = false;
            grdDet.Columns["FechaDiag"].Visible = false;
            grdDet.Columns["UsuarioDiag"].Visible = false;
            grdDet.Columns["FechaAsig"].Visible = false;
            grdDet.Columns["UsuarioAsig"].Visible = false;
            grdDet.Columns["FechaPausado"].Visible = false;
            grdDet.Columns["NotaPausado"].Visible = false;
            grdDet.Columns["FechaTerminado"].Visible = false;
            grdDet.Columns["UsuarioTerminado"].Visible = false;
            grdDet.Columns["idDivision"].Visible = false;
            grdDet.Columns["idLlanta"].Visible = false;
            grdDet.Columns["PosLlanta"].Visible = false;
            grdDet.Columns["NotaDiagnostico"].Visible = false;
            grdDet.Columns["FaltInsumo"].Visible = false;
            grdDet.Columns["Pendiente"].Visible = false;
            grdDet.Columns["CIDPRODUCTO_SERV"].Visible = false;
            grdDet.Columns["Cantidad_CIDPRODUCTO_SERV"].Visible = false;
            grdDet.Columns["Precio_CIDPRODUCTO_SERV"].Visible = false;
            grdDet.Columns["idFamilia"].Visible = false;
            grdDet.Columns["NotaRecepcion"].Visible = false;
            grdDet.Columns["isCancelado"].Visible = false;
            grdDet.Columns["motivoCancelacion"].Visible = false;
            grdDet.Columns["idPersonalResp"].Visible = false;
            grdDet.Columns["idPersonalAyu1"].Visible = false;
            grdDet.Columns["idPersonalAyu2"].Visible = false;
            grdDet.Columns["NotaRecepcionA"].Visible = false;
            grdDet.Columns["CIDPRODUCTO_ACT"].Visible = false;
            



            //HACER READONLY COLUMNAS
            //grdViajes.Columns["Seleccionado"].ReadOnly = true;

            //ENCABEZADO DE COLUMNAS
            grdDet.Columns["idOrdenTrabajo"].HeaderText = "No.";
            grdDet.Columns["NotaRecepcionF"].HeaderText = "Descripción";
            grdDet.Columns["TipoNotaRecepcionF"].HeaderText = "Tipo";
            grdDet.Columns["FallaReportada"].HeaderText = "Falla Reportada";
            grdDet.Columns["NomPersonalResp"].HeaderText = "Responsable";
            grdDet.Columns["NomPersonalAyu1"].HeaderText = "Ayudante 1";
            grdDet.Columns["NomPersonalAyu2"].HeaderText = "Ayudante 2";
            //grdDet.Columns["x"].HeaderText = "Nota";
            //grdDet.Columns["FechaHoraFin"].HeaderText = "Fecha Fin";
            //grdDet.Columns["EstatusGuia"].HeaderText = "Estatus";
            //grdDet.Columns["TipoViaje"].HeaderText = "Tipo Viaje";
            //grdDet.Columns["idTipoOrden"].HeaderText = "idTipoOrden";


            //FORMATO DE COLUMNAS
            //grdDatos.Columns["Precio"].DefaultCellStyle.Format = "C2";

            //ALINEACION DE COLUMNAS
            //grdDatos.Columns["Precio"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight;

            //TAMAÑO AUTOMATICO DE COLUMNAS
            grdDet.Columns["idOrdenTrabajo"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdDet.Columns["NotaRecepcionF"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdDet.Columns["TipoNotaRecepcionF"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdDet.Columns["FallaReportada"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

            grdDet.Columns["NomPersonalResp"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdDet.Columns["NomPersonalAyu1"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdDet.Columns["NomPersonalAyu2"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            //grdDet.Columns["x"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            //grdDet.Columns["x"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            //grdDet.Columns["x"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

            //grdDet.Columns["EstatusGuia"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            //grdDet.Columns["TipoViaje"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

        }
        private void FormatogrdRefac()
        {
            //OCULTAR COLUMNAS
            grdRefac.Columns["idOrdenSer"].Visible = false;
            grdRefac.Columns["idOrdenActividad"].Visible = false;
            grdRefac.Columns["NotaRecepcion"].Visible = false;
            grdRefac.Columns["CantCompra"].Visible = false;
            grdRefac.Columns["CantSalida"].Visible = false;
            grdRefac.Columns["Costo"].Visible = false;


            //HACER READONLY COLUMNAS
            //grdViajes.Columns["Seleccionado"].ReadOnly = true;

            //ENCABEZADO DE COLUMNAS
            grdRefac.Columns["idProducto"].HeaderText = "Producto";
            grdRefac.Columns["CCODIGOPRODUCTO"].HeaderText = "Codigo";
            grdRefac.Columns["CNOMBREPRODUCTO"].HeaderText = "Descripción";
            grdRefac.Columns["Cantidad"].HeaderText = "Cantidad";
            grdRefac.Columns["Costo"].HeaderText = "Costo";


            //FORMATO DE COLUMNAS
            //grdDatos.Columns["Precio"].DefaultCellStyle.Format = "C2";

            //ALINEACION DE COLUMNAS
            //grdDatos.Columns["Precio"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight;

            //TAMAÑO AUTOMATICO DE COLUMNAS
            grdRefac.Columns["idProducto"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdRefac.Columns["CCODIGOPRODUCTO"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdRefac.Columns["CNOMBREPRODUCTO"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            grdRefac.Columns["Cantidad"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdRefac.Columns["Costo"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;


        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Refrescar();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void grdDet_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }//
}
