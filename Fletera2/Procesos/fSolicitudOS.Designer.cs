﻿namespace Fletera2.Procesos
{
    partial class fSolicitudOS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fSolicitudOS));
            this.ToolStripMenu = new System.Windows.Forms.ToolStrip();
            this.btnNuevo = new System.Windows.Forms.ToolStripButton();
            this.btnGrabar = new System.Windows.Forms.ToolStripButton();
            this.btnEliminar = new System.Windows.Forms.ToolStripButton();
            this.btnReporte = new System.Windows.Forms.ToolStripButton();
            this.btnCancelar = new System.Windows.Forms.ToolStripButton();
            this.btnSalir = new System.Windows.Forms.ToolStripButton();
            this.gpoDatos = new System.Windows.Forms.GroupBox();
            this.btnAgregaOT = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.chkBoxTipoOrdServ = new System.Windows.Forms.CheckedListBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtEstatus = new System.Windows.Forms.TextBox();
            this.cmbidEmpleadoAutoriza = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbidOperador = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtRemolque2 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtDolly = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtRemolque1 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtTractor = new System.Windows.Forms.TextBox();
            this.dtpFechaHora = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpFecha = new System.Windows.Forms.DateTimePicker();
            this.cmbIdEmpresa = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtidPadreOrdSer = new System.Windows.Forms.TextBox();
            this.gpoOs = new System.Windows.Forms.GroupBox();
            this.grdCabOs = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.grdDetOs = new System.Windows.Forms.DataGridView();
            this.ToolStripMenu.SuspendLayout();
            this.gpoDatos.SuspendLayout();
            this.gpoOs.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdCabOs)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdDetOs)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolStripMenu
            // 
            this.ToolStripMenu.AutoSize = false;
            this.ToolStripMenu.BackColor = System.Drawing.SystemColors.Control;
            this.ToolStripMenu.Font = new System.Drawing.Font("Berlin Sans FB", 11.25F);
            this.ToolStripMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnNuevo,
            this.btnGrabar,
            this.btnEliminar,
            this.btnReporte,
            this.btnCancelar,
            this.btnSalir});
            this.ToolStripMenu.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.ToolStripMenu.Location = new System.Drawing.Point(0, 0);
            this.ToolStripMenu.Name = "ToolStripMenu";
            this.ToolStripMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.ToolStripMenu.Size = new System.Drawing.Size(895, 54);
            this.ToolStripMenu.TabIndex = 17;
            this.ToolStripMenu.Text = "ToolStrip1";
            // 
            // btnNuevo
            // 
            this.btnNuevo.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnNuevo.Image = ((System.Drawing.Image)(resources.GetObject("btnNuevo.Image")));
            this.btnNuevo.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnNuevo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(54, 51);
            this.btnNuevo.Text = "&Nuevo";
            this.btnNuevo.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnNuevo.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnNuevo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnNuevo.Click += new System.EventHandler(this.btnNuevo_Click);
            // 
            // btnGrabar
            // 
            this.btnGrabar.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnGrabar.Image = ((System.Drawing.Image)(resources.GetObject("btnGrabar.Image")));
            this.btnGrabar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnGrabar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnGrabar.Name = "btnGrabar";
            this.btnGrabar.Size = new System.Drawing.Size(60, 51);
            this.btnGrabar.Text = "&Grabar";
            this.btnGrabar.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnGrabar.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnGrabar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnGrabar.Click += new System.EventHandler(this.btnGrabar_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnEliminar.Image = ((System.Drawing.Image)(resources.GetObject("btnEliminar.Image")));
            this.btnEliminar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnEliminar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(100, 51);
            this.btnEliminar.Text = "&Eliminar Folio";
            this.btnEliminar.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnEliminar.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnEliminar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // btnReporte
            // 
            this.btnReporte.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnReporte.Image = ((System.Drawing.Image)(resources.GetObject("btnReporte.Image")));
            this.btnReporte.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnReporte.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnReporte.Name = "btnReporte";
            this.btnReporte.Size = new System.Drawing.Size(84, 51);
            this.btnReporte.Text = "&ReImprimir";
            this.btnReporte.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnReporte.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnReporte.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnReporte.Click += new System.EventHandler(this.btnReporte_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnCancelar.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar.Image")));
            this.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnCancelar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(69, 51);
            this.btnCancelar.Text = "&Cancelar";
            this.btnCancelar.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnCancelar.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnSalir.Image = ((System.Drawing.Image)(resources.GetObject("btnSalir.Image")));
            this.btnSalir.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnSalir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(55, 51);
            this.btnSalir.Text = "&S a l i r";
            this.btnSalir.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnSalir.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // gpoDatos
            // 
            this.gpoDatos.Controls.Add(this.btnAgregaOT);
            this.gpoDatos.Controls.Add(this.label6);
            this.gpoDatos.Controls.Add(this.chkBoxTipoOrdServ);
            this.gpoDatos.Controls.Add(this.label5);
            this.gpoDatos.Controls.Add(this.txtEstatus);
            this.gpoDatos.Controls.Add(this.cmbidEmpleadoAutoriza);
            this.gpoDatos.Controls.Add(this.label4);
            this.gpoDatos.Controls.Add(this.cmbidOperador);
            this.gpoDatos.Controls.Add(this.label3);
            this.gpoDatos.Controls.Add(this.label16);
            this.gpoDatos.Controls.Add(this.txtRemolque2);
            this.gpoDatos.Controls.Add(this.label17);
            this.gpoDatos.Controls.Add(this.txtDolly);
            this.gpoDatos.Controls.Add(this.label18);
            this.gpoDatos.Controls.Add(this.txtRemolque1);
            this.gpoDatos.Controls.Add(this.label19);
            this.gpoDatos.Controls.Add(this.txtTractor);
            this.gpoDatos.Controls.Add(this.dtpFechaHora);
            this.gpoDatos.Controls.Add(this.label2);
            this.gpoDatos.Controls.Add(this.dtpFecha);
            this.gpoDatos.Controls.Add(this.cmbIdEmpresa);
            this.gpoDatos.Controls.Add(this.label8);
            this.gpoDatos.Controls.Add(this.label1);
            this.gpoDatos.Controls.Add(this.txtidPadreOrdSer);
            this.gpoDatos.Location = new System.Drawing.Point(12, 57);
            this.gpoDatos.Name = "gpoDatos";
            this.gpoDatos.Size = new System.Drawing.Size(804, 224);
            this.gpoDatos.TabIndex = 12;
            this.gpoDatos.TabStop = false;
            this.gpoDatos.Text = "Datos";
            // 
            // btnAgregaOT
            // 
            this.btnAgregaOT.Image = global::Fletera2.Properties.Resources._1474630188_file_add;
            this.btnAgregaOT.Location = new System.Drawing.Point(399, 145);
            this.btnAgregaOT.Name = "btnAgregaOT";
            this.btnAgregaOT.Size = new System.Drawing.Size(75, 64);
            this.btnAgregaOT.TabIndex = 14;
            this.btnAgregaOT.UseVisualStyleBackColor = true;
            this.btnAgregaOT.Click += new System.EventHandler(this.btnAgregaOT_Click);
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Blue;
            this.label6.Location = new System.Drawing.Point(11, 145);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 64);
            this.label6.TabIndex = 78;
            this.label6.Text = "Tipo de Orden Servicio";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chkBoxTipoOrdServ
            // 
            this.chkBoxTipoOrdServ.FormattingEnabled = true;
            this.chkBoxTipoOrdServ.Location = new System.Drawing.Point(90, 145);
            this.chkBoxTipoOrdServ.Name = "chkBoxTipoOrdServ";
            this.chkBoxTipoOrdServ.Size = new System.Drawing.Size(292, 64);
            this.chkBoxTipoOrdServ.TabIndex = 13;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Blue;
            this.label5.Location = new System.Drawing.Point(653, 49);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 18);
            this.label5.TabIndex = 76;
            this.label5.Text = "Estatus:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtEstatus
            // 
            this.txtEstatus.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtEstatus.Location = new System.Drawing.Point(723, 46);
            this.txtEstatus.Name = "txtEstatus";
            this.txtEstatus.Size = new System.Drawing.Size(68, 25);
            this.txtEstatus.TabIndex = 6;
            // 
            // cmbidEmpleadoAutoriza
            // 
            this.cmbidEmpleadoAutoriza.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.cmbidEmpleadoAutoriza.FormattingEnabled = true;
            this.cmbidEmpleadoAutoriza.Location = new System.Drawing.Point(553, 115);
            this.cmbidEmpleadoAutoriza.Name = "cmbidEmpleadoAutoriza";
            this.cmbidEmpleadoAutoriza.Size = new System.Drawing.Size(238, 26);
            this.cmbidEmpleadoAutoriza.TabIndex = 12;
            this.cmbidEmpleadoAutoriza.KeyUp += new System.Windows.Forms.KeyEventHandler(this.cmbidEmpleadoAutoriza_KeyUp);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Blue;
            this.label4.Location = new System.Drawing.Point(474, 117);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 18);
            this.label4.TabIndex = 73;
            this.label4.Text = "Autoriza";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cmbidOperador
            // 
            this.cmbidOperador.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.cmbidOperador.FormattingEnabled = true;
            this.cmbidOperador.Location = new System.Drawing.Point(90, 113);
            this.cmbidOperador.Name = "cmbidOperador";
            this.cmbidOperador.Size = new System.Drawing.Size(238, 26);
            this.cmbidOperador.TabIndex = 1;
            this.cmbidOperador.KeyUp += new System.Windows.Forms.KeyEventHandler(this.cmbidOperador_KeyUp);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Blue;
            this.label3.Location = new System.Drawing.Point(11, 115);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 18);
            this.label3.TabIndex = 71;
            this.label3.Text = "Operador:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Blue;
            this.label16.Location = new System.Drawing.Point(490, 85);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(87, 18);
            this.label16.TabIndex = 70;
            this.label16.Text = "Remolque 2";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtRemolque2
            // 
            this.txtRemolque2.Enabled = false;
            this.txtRemolque2.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtRemolque2.Location = new System.Drawing.Point(592, 83);
            this.txtRemolque2.Name = "txtRemolque2";
            this.txtRemolque2.Size = new System.Drawing.Size(75, 25);
            this.txtRemolque2.TabIndex = 10;
            this.txtRemolque2.TextChanged += new System.EventHandler(this.txtRemolque2_TextChanged);
            this.txtRemolque2.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtRemolque2_KeyUp);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Blue;
            this.label17.Location = new System.Drawing.Point(362, 85);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(41, 18);
            this.label17.TabIndex = 68;
            this.label17.Text = "Dolly";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtDolly
            // 
            this.txtDolly.Enabled = false;
            this.txtDolly.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtDolly.Location = new System.Drawing.Point(409, 82);
            this.txtDolly.Name = "txtDolly";
            this.txtDolly.Size = new System.Drawing.Size(75, 25);
            this.txtDolly.TabIndex = 9;
            this.txtDolly.TextChanged += new System.EventHandler(this.txtDolly_TextChanged);
            this.txtDolly.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtDolly_KeyUp);
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Blue;
            this.label18.Location = new System.Drawing.Point(185, 86);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(75, 18);
            this.label18.TabIndex = 66;
            this.label18.Text = "Remolque 1";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtRemolque1
            // 
            this.txtRemolque1.Enabled = false;
            this.txtRemolque1.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtRemolque1.Location = new System.Drawing.Point(266, 82);
            this.txtRemolque1.Name = "txtRemolque1";
            this.txtRemolque1.Size = new System.Drawing.Size(75, 25);
            this.txtRemolque1.TabIndex = 8;
            this.txtRemolque1.TextChanged += new System.EventHandler(this.txtRemolque1_TextChanged);
            this.txtRemolque1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtRemolque1_KeyUp);
            // 
            // label19
            // 
            this.label19.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Blue;
            this.label19.Location = new System.Drawing.Point(22, 86);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(62, 18);
            this.label19.TabIndex = 64;
            this.label19.Text = "Tractor";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtTractor
            // 
            this.txtTractor.Enabled = false;
            this.txtTractor.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtTractor.Location = new System.Drawing.Point(90, 82);
            this.txtTractor.Name = "txtTractor";
            this.txtTractor.Size = new System.Drawing.Size(75, 25);
            this.txtTractor.TabIndex = 7;
            this.txtTractor.TextChanged += new System.EventHandler(this.txtTractor_TextChanged);
            this.txtTractor.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtTractor_KeyUp);
            // 
            // dtpFechaHora
            // 
            this.dtpFechaHora.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.dtpFechaHora.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpFechaHora.Location = new System.Drawing.Point(685, 15);
            this.dtpFechaHora.Name = "dtpFechaHora";
            this.dtpFechaHora.Size = new System.Drawing.Size(106, 25);
            this.dtpFechaHora.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Blue;
            this.label2.Location = new System.Drawing.Point(338, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 18);
            this.label2.TabIndex = 24;
            this.label2.Text = "Fecha:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dtpFecha
            // 
            this.dtpFecha.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.dtpFecha.Location = new System.Drawing.Point(416, 15);
            this.dtpFecha.Name = "dtpFecha";
            this.dtpFecha.Size = new System.Drawing.Size(263, 25);
            this.dtpFecha.TabIndex = 3;
            // 
            // cmbIdEmpresa
            // 
            this.cmbIdEmpresa.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.cmbIdEmpresa.FormattingEnabled = true;
            this.cmbIdEmpresa.Location = new System.Drawing.Point(90, 50);
            this.cmbIdEmpresa.Name = "cmbIdEmpresa";
            this.cmbIdEmpresa.Size = new System.Drawing.Size(238, 26);
            this.cmbIdEmpresa.TabIndex = 5;
            this.cmbIdEmpresa.SelectedIndexChanged += new System.EventHandler(this.cmbIdEmpresa_SelectedIndexChanged);
            this.cmbIdEmpresa.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbIdEmpresa_KeyPress);
            this.cmbIdEmpresa.KeyUp += new System.Windows.Forms.KeyEventHandler(this.cmbIdEmpresa_KeyUp);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Blue;
            this.label8.Location = new System.Drawing.Point(11, 52);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(65, 18);
            this.label8.TabIndex = 21;
            this.label8.Text = "CLIENTE";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(11, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 18);
            this.label1.TabIndex = 7;
            this.label1.Text = "FOLIO:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtidPadreOrdSer
            // 
            this.txtidPadreOrdSer.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtidPadreOrdSer.Location = new System.Drawing.Point(90, 19);
            this.txtidPadreOrdSer.Name = "txtidPadreOrdSer";
            this.txtidPadreOrdSer.Size = new System.Drawing.Size(123, 25);
            this.txtidPadreOrdSer.TabIndex = 2;
            this.txtidPadreOrdSer.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtidPadreOrdSer.TextChanged += new System.EventHandler(this.txtidPadreOrdSer_TextChanged);
            this.txtidPadreOrdSer.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtidPadreOrdSer_KeyDown);
            this.txtidPadreOrdSer.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtidPadreOrdSer_KeyPress);
            // 
            // gpoOs
            // 
            this.gpoOs.Controls.Add(this.grdCabOs);
            this.gpoOs.Location = new System.Drawing.Point(12, 287);
            this.gpoOs.Name = "gpoOs";
            this.gpoOs.Size = new System.Drawing.Size(854, 170);
            this.gpoOs.TabIndex = 13;
            this.gpoOs.TabStop = false;
            this.gpoOs.Text = "ORDENES DE SERVICIO";
            // 
            // grdCabOs
            // 
            this.grdCabOs.AllowUserToAddRows = false;
            this.grdCabOs.AllowUserToDeleteRows = false;
            this.grdCabOs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdCabOs.Location = new System.Drawing.Point(6, 19);
            this.grdCabOs.Name = "grdCabOs";
            this.grdCabOs.Size = new System.Drawing.Size(840, 143);
            this.grdCabOs.TabIndex = 15;
            this.grdCabOs.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdCabOs_CellContentClick);
            this.grdCabOs.Click += new System.EventHandler(this.grdCabOs_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.grdDetOs);
            this.groupBox1.Location = new System.Drawing.Point(12, 463);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(854, 153);
            this.groupBox1.TabIndex = 18;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "ORDENES DE TRABAJO";
            // 
            // grdDetOs
            // 
            this.grdDetOs.AllowUserToAddRows = false;
            this.grdDetOs.AllowUserToDeleteRows = false;
            this.grdDetOs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdDetOs.Location = new System.Drawing.Point(6, 19);
            this.grdDetOs.Name = "grdDetOs";
            this.grdDetOs.Size = new System.Drawing.Size(840, 134);
            this.grdDetOs.TabIndex = 17;
            // 
            // fSolicitudOS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(895, 628);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gpoOs);
            this.Controls.Add(this.gpoDatos);
            this.Controls.Add(this.ToolStripMenu);
            this.Name = "fSolicitudOS";
            this.Text = "Solicitud de Ordenes de Servicio";
            this.Load += new System.EventHandler(this.fSolicitudOS_Load);
            this.ToolStripMenu.ResumeLayout(false);
            this.ToolStripMenu.PerformLayout();
            this.gpoDatos.ResumeLayout(false);
            this.gpoDatos.PerformLayout();
            this.gpoOs.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdCabOs)).EndInit();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdDetOs)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.ToolStrip ToolStripMenu;
        private System.Windows.Forms.ToolStripButton btnNuevo;
        private System.Windows.Forms.ToolStripButton btnGrabar;
        private System.Windows.Forms.ToolStripButton btnEliminar;
        private System.Windows.Forms.ToolStripButton btnReporte;
        private System.Windows.Forms.ToolStripButton btnCancelar;
        private System.Windows.Forms.ToolStripButton btnSalir;
        private System.Windows.Forms.GroupBox gpoDatos;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtidPadreOrdSer;
        private System.Windows.Forms.ComboBox cmbIdEmpresa;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DateTimePicker dtpFechaHora;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpFecha;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtRemolque2;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtDolly;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtRemolque1;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtTractor;
        private System.Windows.Forms.ComboBox cmbidOperador;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbidEmpleadoAutoriza;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtEstatus;
        private System.Windows.Forms.CheckedListBox chkBoxTipoOrdServ;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnAgregaOT;
        private System.Windows.Forms.GroupBox gpoOs;
        private System.Windows.Forms.DataGridView grdCabOs;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView grdDetOs;
    }
}