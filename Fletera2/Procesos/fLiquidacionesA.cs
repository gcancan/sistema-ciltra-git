﻿using Core.Interfaces;
using Core.Models;
using Fletera2.Clases;
using Fletera2Entidades;
using Fletera2Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fletera2.Procesos
{
    public partial class fLiquidacionesA : Form
    {
        CabLiquidaciones CabLiq;
        List<DetLiquidaciones> listDetLiquidaciones;

        List<CabGuia> listViajes;
        List<Liquidaciones> listViajesDet;
        List<FoliosDinero> listFolioDin;
        List<ViajePorOperador> listViaxOper;
        List<OtrosConcLiq> listOtrosConc;
        List<CompGastosFoliosDin> listCompGastos;
        List<Empresa> listEmpresas;



        CatPersonal Operador = new CatPersonal();

        CatConceptosLiq concepto;


        ConsultasSQL objsql = new ConsultasSQL();
        string strsql;
        DataTable dtViaOp;
        //int IndiceList;

        Usuario _usuario;

        //Para MsgBox
        string caption;
        MessageBoxButtons buttons = MessageBoxButtons.YesNo;
        DialogResult result;
        string mensaje = "";

        //Para imprimir
        ImprimeLiquidacion Imprime = new ImprimeLiquidacion();
        PrinterSettings prtSettings;
        PrintDocument prtDoc;
        Font printFont = new System.Drawing.Font("Courier New", 7);
        int lineaActual = 0;
        int ContPaginas = 0;
        int NumRegistros = 1;
        int NumRenglonXHoja;
        int NumRenglones;
        public fLiquidacionesA(Usuario user)
        {
            _usuario = user;
            InitializeComponent();
        }

        private void label11_Click(object sender, EventArgs e)
        {

        }



        private void fLiquidacionesA_Load(object sender, EventArgs e)
        {
            Cancelar();

        }



        private void mnuOpcLiquidar_Click(object sender, EventArgs e)
        {
            //Seleccionar y Cargar
            if (grdViajexOpe.CurrentCell == null)
            {
                return;
            }

            //IndiceList = grdViajexOpe.CurrentCell.RowIndex;

            LimpiaLiquidacion();
            CargaLiquidacionxOperador(Convert.ToInt32(grdViajexOpe.Rows[grdViajexOpe.CurrentCell.RowIndex].Cells["idOperador"].Value));


            grdViajexOpe.Enabled = false;
        }

        private void CargaLiquidacionxOperador(int idOperador)
        {
            //int idOperador = Convert.ToInt32(dtViaOp.Rows[indice]["idoperador"]);
            //int idOperador = listViaxOper[indice].idOperador;

            //OPERADOR
            OperationResult respOp = new CatPersonalSvc().getCatPersonalxFiltro(" idPersonal = " + idOperador);
            if (respOp.typeResult == ResultTypes.success)
            {
                Operador = ((List<CatPersonal>)respOp.result)[0];
                txtidOperador.Text = Operador.idPersonal.ToString();
                txtNomOperador.Text = Operador.NombreCompleto;
                SeleccionaComboEmpresa(Operador.idEmpresa);
            }


            //VIAJES
            OperationResult respVi = new CabGuiaSvc().getCabGuiaxFilter(0, "c.IdEmpresa = " + _usuario.personal.empresa.clave +
            " AND c.idOperador = " + idOperador + "  AND c.EstatusGuia = 'FINALIZADO' AND ISNULL(c.NumLiquidacion,0) = 0" +
            " AND  C.FechaHoraViaje >= '" + Tools.FormatFecHora(dtpFechaIni.Value, true, false, false) + " 00:00:00' " +
            "AND C.FechaHoraViaje <= '" + Tools.FormatFecHora(dtpFechaFin.Value, true, false, false) + " 23:59:59' ");
            if (respVi.typeResult == ResultTypes.success)
            {
                listViajes = (List<CabGuia>)respVi.result;
                grdViajes.DataSource = null;
                grdViajes.DataSource = listViajes;

                FormatoGeneral(grdViajes);
                FormatogrdViajes();
            }

            //DETALLE DE VIAJES
            OperationResult respv = new CabGuiaSvc().getLiquidacionxFilter("c.IdEmpresa = " + _usuario.personal.empresa.clave +
            " AND c.idOperador = " + idOperador + "  AND c.EstatusGuia = 'FINALIZADO' AND ISNULL(c.NumLiquidacion,0) = 0" +
            " AND  C.FechaHoraViaje >= '" + Tools.FormatFecHora(dtpFechaIni.Value, true, false, false) + " 00:00:00' " +
            "AND C.FechaHoraViaje <= '" + Tools.FormatFecHora(dtpFechaFin.Value, true, false, false) + " 23:59:59' ");
            if (respv.typeResult == ResultTypes.success)
            {
                listViajesDet = (List<Liquidaciones>)respv.result;
                grdViajesDet.DataSource = null;
                grdViajesDet.DataSource = listViajesDet;

                FormatoGeneral(grdViajesDet);
                FormatogrdViajesDet();
            }

            //dinero x comprobar
            OperationResult respf = new FoliosDineroSvc().getFoliosDineroxId(0, " idEmpleadoRec = " + idOperador);
            if (respf.typeResult == ResultTypes.success)
            {
                listFolioDin = (List<FoliosDinero>)respf.result;
                grdFolioDinero.DataSource = null;
                grdFolioDinero.DataSource = listFolioDin;

                FormatoGeneral(grdFolioDinero);
                FormatogrdFolioDinero();
            }

            //otros conceptos
            OperationResult respo = new OtrosConcLiqSvc().getOtrosConcLiqxFilter(0, " o.NumLiquidacion = 0 AND o.idOperador = " + idOperador);
            if (respo.typeResult == ResultTypes.success)
            {
                listOtrosConc = (List<OtrosConcLiq>)respo.result;
                grdOtrosConc.DataSource = null;
                grdOtrosConc.DataSource = listOtrosConc;

                FormatoGeneral(grdOtrosConc);
                FormatogrdOtrosConceptos();


            }
            //Comprobacion de gastos
            string foliodin = "";
            if (listFolioDin != null)
            {
                if (listFolioDin.Count > 0)
                {
                    foreach (var item in listFolioDin)
                    {
                        foliodin = foliodin + item.FolioDin.ToString() + ",";
                    }
                }
            }
            if (foliodin != "")
            {
                //se quita el ultimo ,
                foliodin = foliodin.Substring(0, foliodin.Length - 1);

                OperationResult repcom = new CompGastosFoliosDinSvc().getCompGastosFoliosDinxId(0, " c.FolioDin in (" + foliodin + ") and c.idLiquidacion = 0 and c.idEmpresa = " + _usuario.personal.empresa.clave, " order by c.consecutivo");
                if (repcom.typeResult == ResultTypes.success)
                {
                    listCompGastos = (List<CompGastosFoliosDin>)repcom.result;
                    grdCompGastos.DataSource = null;
                    grdCompGastos.DataSource = listCompGastos;

                    FormatoGeneral(grdCompGastos);
                    FormatogrdComprobGastos();
                }
            }

            //finalmente
            CalculaResumen();
            //TotalesxPestania();

        }

        private void txtFiltro_TextChanged(object sender, EventArgs e)
        {
            var mySearch = (listViaxOper).FindAll(S => S.idOperador.ToString().IndexOf(txtFiltro.Text.Trim(), StringComparison.OrdinalIgnoreCase) >= 0 ||
                       S.NomOperador.IndexOf(txtFiltro.Text.Trim(), StringComparison.OrdinalIgnoreCase) >= 0);

            //ActualizaStatusControl(mySearch.Count);
            grdViajexOpe.DataSource = mySearch;
        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void LlenaCombos()
        {
            //OperationResult resp = new EmpresaSvc().getEmpresasALLorById(Convert.ToInt32(_usuario.personal.clave_empresa));
            OperationResult resp = new EmpresaSvc().getEmpresasALLorById();
            if (resp.typeResult == ResultTypes.success)
            {

                listEmpresas = (List<Empresa>)resp.result;
                cmbidEmpresa.DataSource = listEmpresas;
                cmbidEmpresa.DisplayMember = "Nombre";
                cmbidEmpresa.SelectedIndex = 0;


            }
        }

        private void SeleccionaComboEmpresa(int idEmpresa)
        {
            int i = 0;
            foreach (Empresa item in cmbidEmpresa.Items)
            {
                if (item.clave == idEmpresa)
                {
                    cmbidEmpresa.SelectedIndex = i;
                    return;
                }
                else
                {
                    i++;
                }
            }
        }

        private void CalculaResumen()
        {

            if (CabLiq == null)
            {
                CabLiq = new CabLiquidaciones();
            }

            //no de viajes
            if (listViajes != null)
            {
                CabLiq.NumViajes = listViajes.Count;
                txtNoViajes.Text = CabLiq.NumViajes.ToString();
            }

            //total a pagar por viajes
            CabLiq.idLiquidacion = 0;
            if (listViajesDet != null)
            {
                foreach (var item in listViajesDet)
                {
                    CabLiq.ViajesTot = CabLiq.ViajesTot + item.precioOperador;
                }
                txtTotalViajeOp.Text = CabLiq.ViajesTot.ToString("C2");
            }

            //Anticipos y Gastos x Comprobar
            if (listFolioDin != null)
            {
                CabLiq.GastosxCompTot = 0;
                CabLiq.AnticiposTot = 0;
                foreach (var item in listFolioDin)
                {
                    if (item.idTipoFolDin == 1)
                    {
                        CabLiq.GastosxCompTot = CabLiq.GastosxCompTot + item.Importe;
                    }
                    else
                    {
                        CabLiq.AnticiposTot = CabLiq.AnticiposTot + item.Importe;
                    }

                }
                txtGasxCompTot.Text = CabLiq.GastosxCompTot.ToString("C2");
                txtAnticposManTot.Text = CabLiq.AnticiposTot.ToString("C2");
            }

            //Otros conceptos
            if (listOtrosConc != null)
            {
                foreach (var item in listOtrosConc)
                {
                    OperationResult resp = new CatConceptosLiqSvc().getCatConceptosLiqxId(item.idConcepto);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        concepto = ((List<CatConceptosLiq>)resp.result)[0];
                        if (concepto.EsCargo)
                        {
                            CabLiq.OtrosConceptosTot = CabLiq.OtrosConceptosTot + (item.Importe * -1);
                        }
                        else
                        {
                            CabLiq.OtrosConceptosTot = CabLiq.OtrosConceptosTot + item.Importe;
                        }
                    }
                    else
                    {
                        CabLiq.OtrosConceptosTot = CabLiq.OtrosConceptosTot + (item.Importe * -1);
                    }
                }
                txtOtrosConcTot.Text = CabLiq.OtrosConceptosTot.ToString("C2");

            }
            //comprobacion de gastos
            if (listCompGastos != null)
            {
                foreach (var item in listCompGastos)
                {
                    CabLiq.ComprobacionGastosTot = CabLiq.ComprobacionGastosTot + (item.Importe + item.IVA);
                }
                txtComprobacionTot.Text = CabLiq.ComprobacionGastosTot.ToString("C2");
            }

            txtNoViajesRes.Text = CabLiq.NumViajes.ToString();
            txtViajesTot.Text = CabLiq.ViajesTot.ToString("C2");
            txtGastosxCompTot.Text = CabLiq.GastosxCompTot.ToString("C2");
            txtAnticiposTot.Text = CabLiq.AnticiposTot.ToString("C2");
            txtImporteComp.Text = CabLiq.OtrosConceptosTot.ToString("C2");
            txtGastosComprobados.Text = CabLiq.ComprobacionGastosTot.ToString("C2");

            //Calculamos TOTAL
            CabLiq.TOTAL = CabLiq.ViajesTot - CabLiq.CombustibleTot + CabLiq.GastosxCompTot - CabLiq.AnticiposTot - CabLiq.ComprobacionGastosTot + CabLiq.OtrosConceptosTot;
            txtTotalaPagar.Text = CabLiq.TOTAL.ToString("C2");


        }

        private void LimpiaLiquidacion()
        {
            grdViajes.DataSource = null;
            listViajes = null;
            txtNoViajes.Clear();

            grdViajesDet.DataSource = null;
            listViajesDet = null;
            txtTotalViajeOp.Clear();

            grdFolioDinero.DataSource = null;
            listFolioDin = null;
            txtGasxCompTot.Clear();
            txtAnticposManTot.Clear();

            grdOtrosConc.DataSource = null;
            listOtrosConc = null;
            txtOtrosConcTot.Clear();

            grdCompGastos.DataSource = null;
            listCompGastos = null;
            txtComprobacionTot.Clear();

            //Resumen
            txtViajesTot.Clear();
            txtCombustibleRes.Clear();
            txtGastosxCompTot.Clear();
            txtAnticiposTot.Clear();
            txtImporteComp.Clear();
            txtGastosComprobados.Clear();
            txtTotalaPagar.Clear();

            txtNoLiquidacion.Clear();
            txtidOperador.Clear();
            txtNomOperador.Clear();
            cmbidEmpresa.SelectedIndex = 0;
            dtpFecha.Value = DateTime.Now;
            dtpFechaHora.Value = DateTime.Now;
            txtNoViajesRes.Clear();


        }


        private void btnGrabar_Click(object sender, EventArgs e)
        {
            caption = this.Name;
            buttons = MessageBoxButtons.OKCancel;
            mensaje = "Desea guardar la Liquidacion para el operador: " + txtNomOperador.Text;
            result = MessageBox.Show(mensaje, caption, buttons);
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                if (!Validar())
                {
                    return;
                }

                if (CabLiq != null)
                {
                    CabLiq.idLiquidacion = txtNoLiquidacion.Tag == null ? 0 : ((CabLiquidaciones)txtNoLiquidacion.Tag).idLiquidacion;
                    CabLiq.Fecha = DateTime.Now;
                    CabLiq.idOperador = Convert.ToInt32(txtidOperador.Text);
                    CabLiq.idEmpresa = Convert.ToInt32(_usuario.personal.clave_empresa);
                    CabLiq.Estatus = "ACT";
                    CabLiq.NomOperador = txtNomOperador.Text;
                    CabLiq.Usuario = _usuario.nombreUsuario;

                    CabLiq.Observaciones = txtObservaciones.Text;
                    CabLiq.FechaCancela = DateTime.Now;
                    CabLiq.UsuarioCancela = "";
                    CabLiq.FechaIni = dtpFechaIni.Value;
                    CabLiq.FechaFin = dtpFechaFin.Value;
                }
                else
                {
                    CabLiq = mapForm();
                }

                OperationResult resp = new CabLiquidacionesSvc().GuardaCabLiquidaciones(ref CabLiq);
                if (resp.typeResult == ResultTypes.success)
                {

                    //Detalle Viajes
                    if (listViajesDet != null)
                    {
                        //Solo se le pone el idLiquidacion
                        if (listViajes != null)
                        {
                            foreach (var item in listViajes)
                            {
                                item.NumLiquidacion = CabLiq.idLiquidacion;
                            }
                        }

                        foreach (var item in listViajesDet)
                        {

                            if (listDetLiquidaciones == null)
                            {
                                listDetLiquidaciones = new List<DetLiquidaciones>();
                            }
                            listDetLiquidaciones.Add(new DetLiquidaciones
                            {
                                idDetLiq = 0,
                                idLiquidacion = CabLiq.idLiquidacion,
                                Tipo = "DetGuia",
                                NoDocumento = item.NumGuiaId,
                                Descripcion = item.nombreOrigen + " - " + item.Destino,
                                Importe = item.precioOperador,
                                IVA = 0,
                                EsAbono = true
                            }
                            );
                        }
                        // folios de dinero
                        if (listFolioDin != null)
                        {
                            foreach (var item in listFolioDin)
                            {
                                item.idLiquidacion = CabLiq.idLiquidacion;
                                item.ImporteEnt = 0;
                                item.ImporteComp = 0;
                                item.Estatus = "ACT";

                                listDetLiquidaciones.Add(new DetLiquidaciones
                                {
                                    idDetLiq = 0,
                                    idLiquidacion = CabLiq.idLiquidacion,
                                    Tipo = "FoliosDinero",
                                    NoDocumento = item.FolioDin,
                                    Descripcion = item.DescripcionFolDin + " - " + item.Concepto,
                                    Importe = item.Importe,
                                    IVA = 0,
                                    EsAbono = false
                                }
                                                            );
                            }
                        }
                        //Comprobacion de Gastos
                        if (listCompGastos != null)
                        {
                            foreach (var item in listCompGastos)
                            {
                                item.idLiquidacion = CabLiq.idLiquidacion;
                                listDetLiquidaciones.Add(new DetLiquidaciones
                                {
                                    idDetLiq = 0,
                                    idLiquidacion = CabLiq.idLiquidacion,
                                    Tipo = "CompGastosFoliosDin",
                                    NoDocumento = item.Folio,
                                    Descripcion = item.NomGasto + " - " + item.NoDocumento,
                                    Importe = item.Importe,
                                    IVA = item.IVA,
                                    EsAbono = false
                                });
                            }
                        }
                        //Otros Conceptos
                        if (listOtrosConc != null)
                        {
                            foreach (var item in listOtrosConc)
                            {
                                item.NumLiquidacion = CabLiq.idLiquidacion;
                                listDetLiquidaciones.Add(new DetLiquidaciones
                                {
                                    idDetLiq = 0,
                                    idLiquidacion = CabLiq.idLiquidacion,
                                    Tipo = "OtrosConcLiq",
                                    NoDocumento = item.NumFolio,
                                    Descripcion = item.NomConcepto,
                                    Importe = item.Importe,
                                    IVA = 0,
                                    EsAbono = false
                                });
                            }
                        }

                        int cont = 0;
                        foreach (var item in listDetLiquidaciones)
                        {
                            cont = cont + 1;
                            DetLiquidaciones folioDet = item;
                            OperationResult respd = new DetLiquidacionesSvc().GuardaCabLiquidaciones(ref folioDet);
                            if (respd.typeResult == ResultTypes.success)
                            {
                                if (cont == listDetLiquidaciones.Count)
                                {
                                    //ACTUALIZA VIAJES
                                    foreach (var itemv in listViajes)
                                    {
                                        CabGuia viaje = itemv;
                                        OperationResult opcV = new CabGuiaSvc().ActualizaCabGuia(ref viaje);
                                        if (opcV.typeResult == ResultTypes.success)
                                        {

                                        }
                                    }

                                    //ACTUALIZA FOLIOS DE DINERO
                                    foreach (var itemfd in listFolioDin)
                                    {
                                        FoliosDinero folio = itemfd;
                                        OperationResult opcfd = new FoliosDineroSvc().GuardaFoliosDinero(ref folio);
                                        if (opcfd.typeResult == ResultTypes.success)
                                        {

                                        }

                                    }

                                    //ACTUALIZA COMPROBACION GASTOS
                                    foreach (var itemcg in listCompGastos)
                                    {
                                        CompGastosFoliosDin compgas = itemcg;
                                        OperationResult opccg = new CompGastosFoliosDinSvc().GuardaCompGastosFoliosDin(ref compgas);
                                        if (opccg.typeResult == ResultTypes.success)
                                        {

                                        }
                                    }

                                    //ACTUALIZA OTROS CONCEPTOS
                                    foreach (var itemoc in listOtrosConc)
                                    {
                                        OtrosConcLiq otco = itemoc;
                                        OperationResult opcOc = new OtrosConcLiqSvc().GuardaOtrosConcLiq(ref otco);
                                        if (opcOc.typeResult == ResultTypes.success)
                                        {

                                        }
                                    }
                                    MessageBox.Show("Liquidacion No: " + CabLiq.idLiquidacion + " de " + CabLiq.NomOperador + " creada satisfactoriamente ", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    //Cancelar();
                                    //Consultar Datos para Impresion
                                    ParaImprimirLiquidacion(CabLiq.idLiquidacion);
                                    Cancelar();
                                    btnConsultar.PerformClick();
                                    return;
                                }
                            }
                        }

                    }
                    else
                    {
                        //Se cancela
                    }
                }



            }
        }
        private bool Validar()
        {
            //viajes a fueza
            //



            return true;
        }
        private CabLiquidaciones mapForm()
        {
            try
            {
                return new CabLiquidaciones
                {

                    idLiquidacion = txtNoLiquidacion.Tag == null ? 0 : ((CabLiquidaciones)txtNoLiquidacion.Tag).idLiquidacion,
                    Fecha = DateTime.Now,
                    idOperador = Convert.ToInt32(txtidOperador.Text),
                    idEmpresa = Convert.ToInt32(_usuario.personal.clave_empresa),
                    NumViajes = Convert.ToInt32(txtNoViajes.Text),

                    FechaIni = dtpFechaIni.Value,
                    FechaFin = dtpFechaFin.Value

                    //NumFolio = txtNumFolio.Tag == null ? 0 : ((OtrosConcLiq)txtNumFolio.Tag).NumFolio,

                    //idConcepto = ((CatConceptosLiq)cmbidConcepto.SelectedValue).idConcepto,
                    //Importe = (txtImporte.Text == "" ? 0 : Convert.ToDecimal(txtImporte.Text)),
                    //EnParcialidades = chkEnParcialidades.Checked,
                    //userCrea = _usuario.nombreUsuario,


                    //Cancelado = false,
                    //MotivoCancela = "",
                    //ImporteAplicado = 0
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrio un error al querer obtener información del catálogo" +
                    Environment.NewLine + ex.Message, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Cancelar();
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            OperationResult resp = new CabGuiaSvc().getViajePorOperadorxFilter("v.FechaHoraViaje >= '" + Tools.FormatFecHora(dtpFechaIni.Value, true, false, false) + " 00:00:00' " +
          "AND v.FechaHoraViaje <= '" + Tools.FormatFecHora(dtpFechaFin.Value, true, false, false) + " 23:59:59' and isnull(v.NumLiquidacion,0) = 0 AND v.IdEmpresa = " + _usuario.personal.empresa.clave + " AND v.EstatusGuia = 'FINALIZADO'",
          "p.NombreCompleto");
            if (resp.typeResult == ResultTypes.success)
            {
                listViaxOper = (List<ViajePorOperador>)resp.result;
                grdViajexOpe.DataSource = listViaxOper;
                FormatogrdViajexOpe();
                grdViajexOpe.Enabled = true;
            }
        }

        private void Cancelar()
        {
            LlenaCombos();
            LimpiaLiquidacion();
        }

        #region FormatosGrid

        private void FormatoGeneral(DataGridView grid)
        {
            //ESTILO GENERAL
            grid.RowsDefaultCellStyle.BackColor = Color.White;
            grid.AlternatingRowsDefaultCellStyle.BackColor = Color.AliceBlue;

            grid.RowsDefaultCellStyle.SelectionForeColor = Color.Yellow;
            grid.RowsDefaultCellStyle.SelectionBackColor = Color.Gray;
            grid.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            grid.DefaultCellStyle.Font = new Font("Berlin Sans FB", 12);
            grid.ColumnHeadersDefaultCellStyle.Font = new Font("Berlin Sans FB", 12);
            grid.ColumnHeadersDefaultCellStyle.BackColor = Color.Yellow;
            grid.ColumnHeadersDefaultCellStyle.ForeColor = Color.Red;
            grid.EnableHeadersVisualStyles = false;
        }
        private void FormatogrdViajexOpe()
        {
            //ESTILO GENERAL
            grdViajexOpe.RowsDefaultCellStyle.BackColor = Color.White;
            grdViajexOpe.AlternatingRowsDefaultCellStyle.BackColor = Color.AliceBlue;

            grdViajexOpe.RowsDefaultCellStyle.SelectionForeColor = Color.Yellow;
            grdViajexOpe.RowsDefaultCellStyle.SelectionBackColor = Color.Gray;
            grdViajexOpe.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            grdViajexOpe.DefaultCellStyle.Font = new Font("Berlin Sans FB", 12);
            grdViajexOpe.ColumnHeadersDefaultCellStyle.Font = new Font("Berlin Sans FB", 12);
            grdViajexOpe.ColumnHeadersDefaultCellStyle.BackColor = Color.Yellow;
            grdViajexOpe.ColumnHeadersDefaultCellStyle.ForeColor = Color.Red;
            grdViajexOpe.EnableHeadersVisualStyles = false;

            //
            //grdDatos.Columns["idClasificacion"].Visible = false;
            grdViajexOpe.Columns["Seleccionado"].Visible = false;
            grdViajexOpe.Columns["Seleccionado"].ReadOnly = true;

            grdViajexOpe.Columns["Seleccionado"].HeaderText = "Liquidado";
            grdViajexOpe.Columns["idOperador"].HeaderText = "Operador";
            grdViajexOpe.Columns["NomOperador"].HeaderText = "Nombre";
            grdViajexOpe.Columns["NumViaje"].HeaderText = "# de Viajes";

            //grdDatos.Columns["Precio"].DefaultCellStyle.Format = "C2";
            //grdDatos.Columns["Precio"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight;

            //grdViajexOpe.Columns["Seleccionado"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdViajexOpe.Columns["idOperador"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdViajexOpe.Columns["NomOperador"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdViajexOpe.Columns["NumViaje"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }

        private void FormatogrdViajes()
        {
            //OCULTAR COLUMNAS
            grdViajes.Columns["SerieGuia"].Visible = false;
            grdViajes.Columns["NumViaje"].Visible = false;
            grdViajes.Columns["IdEmpresa"].Visible = false;
            grdViajes.Columns["idOperador"].Visible = false;
            grdViajes.Columns["UserFinCancela"].Visible = false;
            grdViajes.Columns["MotivoCancelacion"].Visible = false;
            grdViajes.Columns["tara"].Visible = false;
            grdViajes.Columns["NoSerie"].Visible = false;
            grdViajes.Columns["NoFolio"].Visible = false;
            grdViajes.Columns["km"].Visible = false;
            grdViajes.Columns["fechaBachoco"].Visible = false;
            grdViajes.Columns["tara2"].Visible = false;
            grdViajes.Columns["externo"].Visible = false;
            grdViajes.Columns["NumLiquidacion"].Visible = false;

            //HACER READONLY COLUMNAS
            //grdViajes.Columns["Seleccionado"].ReadOnly = true;

            //ENCABEZADO DE COLUMNAS
            grdViajes.Columns["NumGuiaId"].HeaderText = "No. Viaje";
            grdViajes.Columns["idCliente"].HeaderText = "Cliente";
            grdViajes.Columns["NomCliente"].HeaderText = "Nombre";
            grdViajes.Columns["FechaHoraViaje"].HeaderText = "Fecha Ini";
            grdViajes.Columns["idTractor"].HeaderText = "Tractor";
            grdViajes.Columns["idRemolque1"].HeaderText = "Remolque1";
            grdViajes.Columns["idDolly"].HeaderText = "Dolly";
            grdViajes.Columns["idRemolque2"].HeaderText = "Remolque2";
            grdViajes.Columns["FechaHoraFin"].HeaderText = "Fecha Fin";
            grdViajes.Columns["EstatusGuia"].HeaderText = "Estatus";
            grdViajes.Columns["TipoViaje"].HeaderText = "Tipo Viaje";

            //FORMATO DE COLUMNAS
            //grdDatos.Columns["Precio"].DefaultCellStyle.Format = "C2";

            //ALINEACION DE COLUMNAS
            //grdDatos.Columns["Precio"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight;

            //TAMAÑO AUTOMATICO DE COLUMNAS
            grdViajes.Columns["NumGuiaId"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdViajes.Columns["idCliente"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdViajes.Columns["NomCliente"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdViajes.Columns["FechaHoraViaje"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

            grdViajes.Columns["idTractor"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdViajes.Columns["idRemolque1"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdViajes.Columns["idDolly"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdViajes.Columns["idRemolque2"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdViajes.Columns["FechaHoraFin"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

            grdViajes.Columns["EstatusGuia"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdViajes.Columns["TipoViaje"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

        }
        private void FormatogrdViajesDet()
        {
            //OCULTAR COLUMNAS
            grdViajesDet.Columns["idOperador"].Visible = false;
            grdViajesDet.Columns["NomOperador"].Visible = false;
            grdViajesDet.Columns["idEmpresa"].Visible = false;
            grdViajesDet.Columns["NomEmpresa"].Visible = false;
            grdViajesDet.Columns["UserViaje"].Visible = false;
            grdViajesDet.Columns["nombreUsuario"].Visible = false;

            //HACER READONLY COLUMNAS
            //grdViajesDet.Columns["Seleccionado"].ReadOnly = true;

            //ENCABEZADO DE COLUMNAS
            grdViajesDet.Columns["NumGuiaId"].HeaderText = "No. Viaje";
            grdViajesDet.Columns["idCliente"].HeaderText = "Cliente";
            grdViajesDet.Columns["NomCliente"].HeaderText = "Nombre";
            grdViajesDet.Columns["FechaHoraViaje"].HeaderText = "Fecha Ini";
            grdViajesDet.Columns["idTractor"].HeaderText = "Tractor";
            grdViajesDet.Columns["idRemolque1"].HeaderText = "Remolque1";
            grdViajesDet.Columns["idDolly"].HeaderText = "Dolly";
            grdViajesDet.Columns["idRemolque2"].HeaderText = "Remolque2";
            grdViajesDet.Columns["FechaHoraFin"].HeaderText = "Fecha Fin";
            grdViajesDet.Columns["TipoViaje"].HeaderText = "Tipo Viaje";
            grdViajesDet.Columns["VolDescarga"].HeaderText = "Volumen";
            grdViajesDet.Columns["idCargaGasolina"].HeaderText = "Vale Combustible";
            grdViajesDet.Columns["precioOperador"].HeaderText = "Importe";
            grdViajesDet.Columns["nombreOrigen"].HeaderText = "Origen";
            grdViajesDet.Columns["Destino"].HeaderText = "Destino";
            grdViajesDet.Columns["Producto"].HeaderText = "Producto";


            //FORMATO DE COLUMNAS
            //grdDatos.Columns["Precio"].DefaultCellStyle.Format = "C2";

            //ALINEACION DE COLUMNAS
            //grdDatos.Columns["Precio"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight;

            //TAMAÑO AUTOMATICO DE COLUMNAS
            grdViajesDet.Columns["NumGuiaId"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdViajesDet.Columns["idCliente"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdViajesDet.Columns["NomCliente"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdViajesDet.Columns["FechaHoraViaje"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

            grdViajesDet.Columns["idTractor"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdViajesDet.Columns["idRemolque1"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdViajesDet.Columns["idDolly"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdViajesDet.Columns["idRemolque2"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdViajesDet.Columns["FechaHoraFin"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;


            grdViajesDet.Columns["TipoViaje"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

            grdViajesDet.Columns["VolDescarga"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdViajesDet.Columns["idCargaGasolina"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdViajesDet.Columns["precioOperador"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

            grdViajesDet.Columns["nombreOrigen"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdViajesDet.Columns["Destino"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdViajesDet.Columns["Producto"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;



        }
        private void FormatogrdFolioDinero()
        {
            //OCULTAR COLUMNAS
            grdFolioDinero.Columns["idEmpleadoEnt"].Visible = false;
            grdFolioDinero.Columns["idEmpleadoRec"].Visible = false;
            grdFolioDinero.Columns["idEmpresa"].Visible = false;
            grdFolioDinero.Columns["RazonSocial"].Visible = false;
            grdFolioDinero.Columns["idTipoFolDin"].Visible = false;
            grdFolioDinero.Columns["ImporteEnt"].Visible = false;
            grdFolioDinero.Columns["ImporteComp"].Visible = false;
            grdFolioDinero.Columns["FolioDinPadre"].Visible = false;
            grdFolioDinero.Columns["idLiquidacion"].Visible = false;


            //HACER READONLY COLUMNAS
            //grdFolioDinero.Columns["Seleccionado"].ReadOnly = true;

            //ENCABEZADO DE COLUMNAS
            grdFolioDinero.Columns["FolioDin"].HeaderText = "Folio";
            grdFolioDinero.Columns["Fecha"].HeaderText = "Fecha";
            grdFolioDinero.Columns["NombreEnt"].HeaderText = "Entrega";
            grdFolioDinero.Columns["Estatus"].HeaderText = "Estatus";
            grdFolioDinero.Columns["NombreRec"].HeaderText = "Recibe";
            grdFolioDinero.Columns["Concepto"].HeaderText = "Concepto";
            grdFolioDinero.Columns["DescripcionFolDin"].HeaderText = "Tipo";
            grdFolioDinero.Columns["Importe"].HeaderText = "Importe";

            //FORMATO DE COLUMNAS
            //grdDatos.Columns["Precio"].DefaultCellStyle.Format = "C2";

            //ALINEACION DE COLUMNAS
            //grdDatos.Columns["Precio"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight;

            //TAMAÑO AUTOMATICO DE COLUMNAS
            grdFolioDinero.Columns["FolioDin"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdFolioDinero.Columns["Fecha"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdFolioDinero.Columns["NombreEnt"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdFolioDinero.Columns["Estatus"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

            grdFolioDinero.Columns["NombreRec"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdFolioDinero.Columns["Concepto"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdFolioDinero.Columns["DescripcionFolDin"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdFolioDinero.Columns["Importe"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

        }
        private void FormatogrdComprobGastos()
        {
            //OCULTAR COLUMNAS
            grdCompGastos.Columns["Consecutivo"].Visible = false;
            grdCompGastos.Columns["idGasto"].Visible = false;
            grdCompGastos.Columns["Deducible"].Visible = false;
            grdCompGastos.Columns["Autoriza"].Visible = false;
            grdCompGastos.Columns["NomAutoriza"].Visible = false;
            grdCompGastos.Columns["ReqComprobante"].Visible = false;
            grdCompGastos.Columns["ReqAutorizacion"].Visible = false;
            grdCompGastos.Columns["idLiquidacion"].Visible = false;
            grdCompGastos.Columns["idEmpresa"].Visible = false;
            grdCompGastos.Columns["RazonSocial"].Visible = false;

            //HACER READONLY COLUMNAS
            //grdCompGastos.Columns["Seleccionado"].ReadOnly = true;

            //ENCABEZADO DE COLUMNAS
            grdCompGastos.Columns["Folio"].HeaderText = "Folio";
            grdCompGastos.Columns["NoDocumento"].HeaderText = "Fecha";
            grdCompGastos.Columns["NomGasto"].HeaderText = "Entrega";
            grdCompGastos.Columns["Fecha"].HeaderText = "Estatus";
            grdCompGastos.Columns["Observaciones"].HeaderText = "Recibe";
            grdCompGastos.Columns["FolioDin"].HeaderText = "Concepto";
            grdCompGastos.Columns["Importe"].HeaderText = "Tipo";
            grdCompGastos.Columns["IVA"].HeaderText = "Importe";

            //FORMATO DE COLUMNAS
            //grdDatos.Columns["Precio"].DefaultCellStyle.Format = "C2";

            //ALINEACION DE COLUMNAS
            //grdDatos.Columns["Precio"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight;

            //TAMAÑO AUTOMATICO DE COLUMNAS
            grdCompGastos.Columns["Folio"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdCompGastos.Columns["NoDocumento"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdCompGastos.Columns["NomGasto"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdCompGastos.Columns["Fecha"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

            grdCompGastos.Columns["Observaciones"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdCompGastos.Columns["FolioDin"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdCompGastos.Columns["Importe"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdCompGastos.Columns["IVA"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

        }
        private void FormatogrdOtrosConceptos()
        {
            //OCULTAR COLUMNAS
            grdOtrosConc.Columns["idOperador"].Visible = false;
            grdOtrosConc.Columns["NomOperador"].Visible = false;
            grdOtrosConc.Columns["idConcepto"].Visible = false;
            grdOtrosConc.Columns["EnParcialidades"].Visible = false;
            grdOtrosConc.Columns["userCrea"].Visible = false;
            grdOtrosConc.Columns["idEmpresa"].Visible = false;
            grdOtrosConc.Columns["RazonSocial"].Visible = false;

            grdOtrosConc.Columns["NumLiquidacion"].Visible = false;
            grdOtrosConc.Columns["Cancelado"].Visible = false;
            grdOtrosConc.Columns["MotivoCancela"].Visible = false;
            grdOtrosConc.Columns["ImporteAplicado"].Visible = false;


            //HACER READONLY COLUMNAS
            grdOtrosConc.Columns["EsCargo"].ReadOnly = true;

            //ENCABEZADO DE COLUMNAS
            grdOtrosConc.Columns["NumFolio"].HeaderText = "Folio";
            grdOtrosConc.Columns["NumGuiaId"].HeaderText = "No. Viaje";
            grdOtrosConc.Columns["NomConcepto"].HeaderText = "Concepto";
            grdOtrosConc.Columns["Importe"].HeaderText = "Importe";
            grdOtrosConc.Columns["NomUserCrea"].HeaderText = "Capturo";
            grdOtrosConc.Columns["Fecha"].HeaderText = "Fecha";
            grdOtrosConc.Columns["EsCargo"].HeaderText = "Cargo";

            //FORMATO DE COLUMNAS
            //grdDatos.Columns["Precio"].DefaultCellStyle.Format = "C2";

            //ALINEACION DE COLUMNAS
            //grdDatos.Columns["Precio"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight;

            //TAMAÑO AUTOMATICO DE COLUMNAS
            grdOtrosConc.Columns["NumFolio"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdOtrosConc.Columns["NumGuiaId"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdOtrosConc.Columns["NomConcepto"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdOtrosConc.Columns["Importe"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdOtrosConc.Columns["NomUserCrea"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdOtrosConc.Columns["Fecha"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdOtrosConc.Columns["EsCargo"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

        }
        #endregion

      
        private void ParaImprimirLiquidacion(int idLiquidacion, CabLiquidaciones cabLiq = null)
        {
            List<DetLiqViaRep> DetLiqViaRep = null;
            List<DetLiqFolDinRep> detliqfd = null;
            List<DetLiqComGasRep> detliqcg = null;
            List<DetLiqOtrConRep> detliqoc = null;


            if (cabLiq == null)
            {
                OperationResult respcab = new CabLiquidacionesSvc().getCabGuiaxFilter(idLiquidacion);
                if (respcab.typeResult == ResultTypes.success)
                {
                    cabLiq = ((List<CabLiquidaciones>)respcab.result)[0];
                }
                else
                {
                    //Mensaje
                }
                 
            }

            //Detalles de Liquidacion

            //Viajes
            OperationResult respdv = new DetLiquidacionesRepSvc().getDetLiqViaRepxFilter(idLiquidacion, " tipo = 'DetGuia'");
            if (respdv.typeResult == ResultTypes.success)
            {
                 DetLiqViaRep = (List<DetLiqViaRep>)respdv.result;

                //FolioDinero
                OperationResult respdfd = new DetLiquidacionesRepSvc().getDetLiqFolDinxFilter(idLiquidacion, " d.Tipo = 'FoliosDinero'");
                if (respdfd.typeResult == ResultTypes.success)
                {
                    detliqfd = (List<DetLiqFolDinRep>)respdfd.result;
                }
                else
                {
                    detliqfd = null;
                }
                //Comprobacion Gastos
                OperationResult respdcg = new DetLiquidacionesRepSvc().getDetLiqComGasRepxFilter(idLiquidacion, " d.Tipo = 'CompGastosFoliosDin'");
                if (respdcg.typeResult == ResultTypes.success)
                {
                    detliqcg = (List<DetLiqComGasRep>)respdcg.result;
                }
                else
                {
                    detliqcg = null;
                }

                //Otros Conceptos
                OperationResult respdoc = new DetLiquidacionesRepSvc().getDetLiqOtrConRepxFilter(idLiquidacion, " d.Tipo = 'OtrosConcLiq'");
                if (respdoc.typeResult == ResultTypes.success)
                {
                    //List<DetLiqOtrConRep> detliqoc;
                    detliqoc =  (List<DetLiqOtrConRep>)respdoc.result;
                }
                else
                {
                    detliqoc = null;
                }
            }
            else
            {
                //mensaje
            }

            //Enviar para Imprimir
            ImprimeLiquidacion(cabLiq, DetLiqViaRep, detliqfd, detliqcg, detliqoc, true, true);

        }

        private void ImprimeLiquidacion(CabLiquidaciones cabLiq, List<DetLiqViaRep> detliqvia,
          List<DetLiqFolDinRep> detliqfd, List<DetLiqComGasRep> detliqcg,
          List<DetLiqOtrConRep> detliqoc, bool BandPreview, bool ActivaImprime)
        {
            try
            {
                Boolean vBandHorizontal = false;
                Imprime.FuenteImprimeCampos = new System.Drawing.Font("Courrier New", 7, FontStyle.Bold);
                //CODIGO DE BARRAS
                //Imprime.FuenteImprimeCampos2 = new Font("Free 3 of 9 Extended", 40);
                Imprime.FuenteImprimeCampos2 = new Font("Free 3 of 9 Extended", 20);
                Imprime.FuenteImprimeCampos3 = new System.Drawing.Font("Courrier New", 3, FontStyle.Bold);
                Imprime.FuenteImprimeCampos4 = new System.Drawing.Font("Courrier New", 7, FontStyle.Bold);
                Imprime.FuenteImprimeCampos5 = new System.Drawing.Font("Courrier New", 4, FontStyle.Bold);

                Imprime.TablaImprime = Imprime.ArmaTablaImprime(cabLiq, detliqvia, detliqfd, detliqcg, detliqoc);

                prtSettings = new PrinterSettings();
                prtDoc = new PrintDocument();
                prtDoc.PrintPage += prt_PrintPage;
                prtDoc.PrinterSettings = prtSettings;
                prtDoc.DefaultPageSettings.Landscape = vBandHorizontal;
                prtDoc.DefaultPageSettings.PaperSize = prtSettings.DefaultPageSettings.PaperSize;

                if (BandPreview)
                {
                    PrintPreviewDialog prtPrev = new PrintPreviewDialog();
                    prtPrev.Document = prtDoc;
                    prtPrev.WindowState = FormWindowState.Maximized;
                    prtPrev.Document.DefaultPageSettings.Landscape = vBandHorizontal;
                    if (vBandHorizontal)
                    {
                        prtPrev.PrintPreviewControl.Zoom = 2;
                    }
                    else
                    {
                        prtPrev.PrintPreviewControl.Zoom = 2;
                    }

                    if (!ActivaImprime)
                    {
                        //DirectCast((prtPrev.Controls[1]), ToolStrip).Items.RemoveAt(0);
                        ((ToolStrip)(prtPrev.Controls[1])).Items[0].Enabled = false;
                    }

                    prtPrev.ShowDialog();
                }
                else
                {
                    prtDoc.Print();
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrio un error al querer Imprimir la Liquidacion " +
                 Environment.NewLine + ex.Message, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        void prt_PrintPage(object sender, PrintPageEventArgs e)
        {
            try
            {
                Single yPos = e.MarginBounds.Top;
                Single leftMargin = e.MarginBounds.Left;
                Single LineHeight = printFont.GetHeight(e.Graphics);

                //////////////////////////////////////////////////////////////////////////////////////////
                //Imprime.imgLogo1 = Tools.getImage(Fletera2.Properties.Resources.Salida2);
                Imprime.imgLogo1 = Fletera2.Properties.Resources.Atlante;
                Imprime.TamanioimgLogo1 = new System.Drawing.Size(Imprime.imgLogo1.Width / 24, Imprime.imgLogo1.Height / 24);
                Imprime.PosicionimgLogo1 = new System.Drawing.Point(180, 3);

                if (ContPaginas == 0)
                {
                    yPos = Imprime.ImprimeLogo(e, lineaActual);
                }

                //Imprime.imgLogo2 = PtoVenta.UI.Properties.Resources.Report;
                //Imprime.TamanioimgLogo2 = new System.Drawing.Size(Imprime.imgLogo2.Width / 22, Imprime.imgLogo2.Height / 20);
                //Imprime.PosicionimgLogo2 = new System.Drawing.Point(210, 150);
                //////////////////////////////////////////////////////////////////////////////////////////
                yPos += LineHeight;
                do
                {
                    yPos += LineHeight;


                    yPos = Imprime.ImprimeLinea(e, lineaActual, ContPaginas + 1);


                    lineaActual += 1;

                } while (!(yPos >= e.MarginBounds.Bottom - 20 || lineaActual >= NumRegistros || ContPaginas == 2));

                ContPaginas += 1;
                //if (ContPaginas > 2)
                //{
                //    ContPaginas = 0;
                //    e.HasMorePages = false;
                //}
                //else
                //{
                //    e.HasMorePages = true;
                //}
                if (lineaActual < NumRegistros)
                {
                    if (NumRenglonXHoja > 0)
                    {
                        NumRenglones = NumRenglones + NumRenglonXHoja;
                    }
                    e.HasMorePages = true;
                }
                else
                {
                    lineaActual = 0;
                    ContPaginas = 0;
                    NumRenglones = NumRenglonXHoja;
                    e.HasMorePages = false;
                }
                //////////////////////////////////////////////////////////////////////////////////////////




            }

            catch (Exception ex)
            {
                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + ex.Message,
    this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void btnReporte_Click(object sender, EventArgs e)
        {
            //ParaImprimirLiquidacion(CabLiq.idLiquidacion);
            ParaImprimirLiquidacion(1);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //comprobacion de gastos
            var compfoldin = new Procesos.fCompGastosFoliosDin(_usuario);
            compfoldin.ShowDialog();
        }
    }//
}
