﻿using Core.Models;
using Fletera2.Clases;
using Fletera2Entidades;
using Fletera2Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Fletera2.Clases.Inicio;

namespace Fletera2.Procesos
{
    public partial class fCapturaServCOM : Form
    {
        int _idOrdenServicio = 0;
        int _idOrdenTrabajo = 0;
        Inicio.eTipoOrdenServicio _tipoOS;

        CabOrdenServicio cabos;
        DetOrdenServicio detos;
        DetRecepcionOS recos;

        List<admProductos> listServicios;

        ServicioxOrdenTrabajo _Respuesta;
        public fCapturaServCOM(int idOrdenServicio, int idOrdenTrabajo)
        {
            _idOrdenServicio = idOrdenServicio;
            _idOrdenTrabajo = idOrdenTrabajo;

            InitializeComponent();
        }

        private void fCapturaServCOM_Load(object sender, EventArgs e)
        {

            if (_idOrdenServicio > 0 && _idOrdenTrabajo > 0)
            {
                HabilitaCampos(opcHabilita.Iniciando);
                CargaForma(_idOrdenServicio.ToString(), opcBusqueda.OrdenTrabajo, _idOrdenTrabajo.ToString());

                CargaCombos();
            }

        }

        private void CargaForma(string Valor, opcBusqueda opc, string Valor2)
        {
            if (opc == opcBusqueda.OrdenTrabajo)
            {
                OperationResult resp = new CabOrdenServicioSvc().getCabOrdenServicioxFilter(Convert.ToInt32(Valor));
                if (resp.typeResult == ResultTypes.success)
                {
                    cabos = ((List<CabOrdenServicio>)resp.result)[0];

                    txtidOrdenSer.Text = cabos.idOrdenSer.ToString();
                    txtTipoOrdenServ.Text = cabos.NomTipOrdServ;
                    //_tipoOS = (eTipoClasCOM)txtTipoOrdenServ.Text;
                    _tipoOS = (Inicio.eTipoOrdenServicio)Convert.ToInt32(cabos.idTipOrdServ);
                    dtpFecha.Value = (DateTime)cabos.FechaTerminado;
                    dtpFechaHora.Value = (DateTime)cabos.FechaTerminado;
                    txtIdEmpresa.Text = cabos.RazonSocial;
                    txtidUnidadTrans.Text = cabos.idUnidadTrans;
                    txtTipoUnidad.Text = cabos.NomTipoUnidad;

                    OperationResult respdos = new DetOrdenServicioSvc().getDetOrdenServicioxFilter(
                        Convert.ToInt32(Valor), " d.idOrdenTrabajo = " + Convert.ToInt32(Valor2));
                    if (respdos.typeResult == ResultTypes.success)
                    {
                        detos = ((List<DetOrdenServicio>)respdos.result)[0];

                        txtidOrdenTrabajo.Text = detos.idOrdenTrabajo.ToString();
                        txtNotaDiagnostico.Text = detos.NotaDiagnostico;

                        OperationResult respRos = new DetRecepcionOSSvc().getDetOrdenServicioxFilter(Convert.ToInt32(Valor), " d.idOrdenTrabajo = " + Convert.ToInt32(Valor2));
                        if (respRos.typeResult == ResultTypes.success)
                        {
                            recos = ((List<DetRecepcionOS>)respRos.result)[0];

                            txtNotaRecepcion.Text = recos.NotaRecepcion;
                            txtIdEmpleadoEnc.Text = recos.NomPersonalResp;
                        }
                        else
                        {

                        }

                    }
                    else
                    {
                        //No se encontro
                    }


                }
                else
                {
                    //que pasa
                }
            }
        }

        private void HabilitaCampos(opcHabilita opc)
        {
            switch (opc)
            {
                case opcHabilita.DesHabilitaTodos:
                    gpoDatosOS.Enabled = false;
                    gpoServ.Enabled = false;
                    break;
                case opcHabilita.Iniciando:
                    gpoDatosOS.Enabled = false;
                    gpoServ.Enabled = true;

                    break;
                case opcHabilita.Editando:
                    break;
                case opcHabilita.Nuevo:
                    break;
                case opcHabilita.Reporte:
                    break;
                default:
                    break;
            }
        }

        private void CargaCombos()
        {
            try
            {
                OperationResult resp = new ComercialSvc().getadmProductosxFilter(0,
" p.CTIPOPRODUCTO = 3  AND p.CSTATUSPRODUCTO = 1 AND cv5.CCODIGOVALORCLASIFICACION IN (" + Inicio.DeterminaClasificacion(_tipoOS) + ") ");
                if (resp.typeResult == ResultTypes.success)
                {
                    listServicios = (List<admProductos>)resp.result;
                    cmbCIDPRODUCTO_SERV.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                    cmbCIDPRODUCTO_SERV.AutoCompleteSource = AutoCompleteSource.ListItems;
                    cmbCIDPRODUCTO_SERV.DataSource = null;
                    cmbCIDPRODUCTO_SERV.DataSource = listServicios;
                    cmbCIDPRODUCTO_SERV.DisplayMember = "CNOMBREPRODUCTO";
                    cmbCIDPRODUCTO_SERV.SelectedIndex = 0;

                    //Autocompletar
                }
            }
            catch (Exception e)
            {

                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + e.Message,
                 this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void cmbCIDPRODUCTO_SERV_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtCantidad_CIDPRODUCTO_SERV.Text = "1";
                txtPrecio_CIDPRODUCTO_SERV.Text = ((admProductos)cmbCIDPRODUCTO_SERV.SelectedItem).CPRECIO1.ToString();
                txtPrecio_CIDPRODUCTO_SERV.Focus();
                CalculaTotal();

            }
        }

        private void txtCantidad_CIDPRODUCTO_SERV_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                //Guardar
                txtPrecio_CIDPRODUCTO_SERV.Text = ((admProductos)cmbCIDPRODUCTO_SERV.SelectedItem).CPRECIO1.ToString();
                txtPrecio_CIDPRODUCTO_SERV.Focus();
                CalculaTotal();
            }
        }

        private void CalculaTotal()
        {
            decimal total;
            total = Convert.ToDecimal(txtCantidad_CIDPRODUCTO_SERV.Text) * Convert.ToDecimal(txtPrecio_CIDPRODUCTO_SERV.Text);
            txtTOTAL.Text = total.ToString("C2");

        }

        private void btnGrabar_Click(object sender, EventArgs e)
        {
            //_ValorDevuelve = grdDatos.Rows[grdDatos.CurrentCell.RowIndex].Cells[_CampoDevuelve].Value.ToString();

            if (txtCantidad_CIDPRODUCTO_SERV.Text != "" || txtPrecio_CIDPRODUCTO_SERV.Text != "")
            {
                _Respuesta = new ServicioxOrdenTrabajo
                {
                    idOrdenSer = detos.idOrdenSer,
                    idOrdenTrabajo = detos.idOrdenTrabajo,
                    CIDPRODUCTO_SERV = ((admProductos)cmbCIDPRODUCTO_SERV.SelectedItem).CIDPRODUCTO,
                    CCODIGOPRODUCTO_SERV = ((admProductos)cmbCIDPRODUCTO_SERV.SelectedItem).CCODIGOPRODUCTO,
                    CNOMBREPRODUCTO_SERV = ((admProductos)cmbCIDPRODUCTO_SERV.SelectedItem).CNOMBREPRODUCTO,
                    Cantidad_CIDPRODUCTO_SERV = Convert.ToDecimal(txtCantidad_CIDPRODUCTO_SERV.Text),
                    Precio_CIDPRODUCTO_SERV =  Convert.ToDecimal(txtPrecio_CIDPRODUCTO_SERV.Text)
                };
            }


            if (_Respuesta != null)
            {
                DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        public ServicioxOrdenTrabajo DevuelveValor()
        {
            try
            {
                DialogResult dResult = ShowDialog();
                if (dResult == DialogResult.OK && _Respuesta != null)
                {
                    return _Respuesta;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {

                return null;
            }
        }

        private void txtPrecio_CIDPRODUCTO_SERV_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                btnGrabar.PerformClick();
            }
        }
    }//
}
