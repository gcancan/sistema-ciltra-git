﻿using Core.Models;
using Fletera2.Clases;
using Fletera2Entidades;
using Fletera2Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Fletera2.Clases.Inicio;
using System.IO;
namespace Fletera2.Procesos
{
    public partial class fCancelaFactura : Form
    {
        Usuario _user;
        //Variables
        Parametros param;
        OperationResult resp;
        List<admConceptos> listConceptos;
        List<CatSAT> listFormaPago;
        List<CatSAT> listCFDI;
        List<CatSAT> listMetodoPago;
        List<admClientes> listClientes;
        List<CatEmpresas> listEmpresas;

        opcForma opcionForma;

        admDocumentos documentoCOM;
        CabMovimientos movimiento;
        CabOrdenServicio ordenservicio;
        List<DetMovimientos> listDetMov;
        CatUnidadTrans unidadtrans;

        decimal totRef = 0;
        decimal totServ = 0;
        decimal Subtot = 0;
        decimal totiva = 0;
        decimal total = 0;
        decimal ContRef = 0;
        decimal ContServ = 0;

        //Para Ejecutar
        string StrSql;
        int indice;
        string[] ArraySql;
        ConsultasSQL objsql = new ConsultasSQL();

        StringBuilder serieCerEmisor = new StringBuilder(3000);
        StringBuilder folioFiscalUUid = new StringBuilder(3000);
        StringBuilder serieCertSat = new StringBuilder(3000);
        StringBuilder fecha = new StringBuilder(3000);
        StringBuilder selloDigital = new StringBuilder(3000);
        StringBuilder selloSat = new StringBuilder(3000);
        StringBuilder cadenaOriginalCompSalt = new StringBuilder(3000);
        StringBuilder regimen = new StringBuilder(3000);
        StringBuilder lugarExp = new StringBuilder(3000);
        StringBuilder moneda = new StringBuilder(3000);
        StringBuilder folioFiscalOriginal = new StringBuilder(3000);
        StringBuilder serieFolioFiscal = new StringBuilder(3000);
        StringBuilder fechaFolioFiscal = new StringBuilder(3000);
        StringBuilder montoFolioFiscal = new StringBuilder(3000);

        

        public fCancelaFactura(Usuario user)
        {
            _user = user;
            InitializeComponent();
        }

        private void fCancelaFactura_Load(object sender, EventArgs e)
        {
            OperationResult resp = new ParametrosSvc().getParametrosxFiltro(1);
            if (resp.typeResult == ResultTypes.success)
            {
                param = ((List<Parametros>)resp.result)[0];
            }
            Cancelar();
        }
        private void Cancelar()
        {
            HabilitaBotones(opcHabilita.DesHabilitaTodos);
            HabilitaCampos(opcHabilita.DesHabilitaTodos);
            CargaCombos();

            FormatoGeneral(grdDetalle);
            //CargaProductos();
            CargaEmpresas();
            CargaClientes();
            LimpiaCampos();
            //LimpiaCamposDetalle();

        }
        private void HabilitaBotones(opcHabilita opc)
        {
            switch (opc)
            {
                case opcHabilita.DesHabilitaTodos:
                    btnNuevo.Enabled = true;
                    btnGrabar.Enabled = false;
                    btnCancelar.Enabled = false;
                    btnSalir.Enabled = true;
                    break;
                case opcHabilita.Iniciando:
                    break;
                case opcHabilita.Editando:
                    break;
                case opcHabilita.Nuevo:
                    btnNuevo.Enabled = false;
                    btnGrabar.Enabled = true;
                    btnCancelar.Enabled = true;
                    btnSalir.Enabled = false;
                    break;
                default:
                    break;
            }
        }
        private void HabilitaCampos(opcHabilita opc)
        {
            dtpFechaMovimiento.Enabled = false;
            dtpFechaMovimientoHora.Enabled = false;

            switch (opc)
            {
                case opcHabilita.DesHabilitaTodos:
                    gpoAnexo20.Enabled = false;
                    gpoCLiente.Enabled = false;
                    gpoImportes.Enabled = false;
                    //gpoDetalle.Enabled = false;

                    //txtidOrdenSer.Enabled = false;
                    //grdDetOs.Enabled = false;
                    //grdRefacciones.Enabled = false;

                    //grdCabOC.Enabled = false;
                    //grdDetOC.Enabled = false;
                    //grdCabPreSalida.Enabled = false;
                    //grdDetPreSalida.Enabled = false;

                    break;
                case opcHabilita.Iniciando:
                    break;
                case opcHabilita.Editando:
                    break;
                case opcHabilita.Nuevo:
                    //gpoCLiente.Enabled = true;
                    txtSerie.Enabled = true;
                    txtFolio.Enabled = true;
                    //txtIdUnidadTrans.Enabled = false;
                    //txtTipoUnidadTrans.Enabled = false;
                    //cmbConcepto.Enabled = false;
                    //gpoDetalle.Enabled = false;
                    gpoAnexo20.Enabled = false;
                    //txtCCODIGOC01_CLI.Enabled = true;
                    //txtIdUnidadTrans.Enabled = false;
                    //txtTipoUnidadTrans.Enabled = false;
                    //cmbConcepto.Enabled = false;

                    //txtidOrdenSer.Enabled = true;
                    //grdDetOs.Enabled = true;
                    //grdRefacciones.Enabled = true;
                    //grdCabOC.Enabled = true;
                    //grdDetOC.Enabled = true;
                    //grdCabPreSalida.Enabled = true;
                    //grdDetPreSalida.Enabled = true;


                    break;
                default:
                    break;
            }
        }
        private void LimpiaCampos()
        {
            txtFolio.Clear();
            txtSerie.Clear();
            cmbCCODIGOC01_CLI.SelectedIndex = 0;
            dtpFechaMovimiento.Value = DateTime.Now;
            dtpFechaMovimientoHora.Value = DateTime.Now;
            txtIdUnidadTrans.Clear();
            txtTipoUnidadTrans.Clear();
            cmbConcepto.SelectedIndex = 0;
            cmbMetodoPago.SelectedIndex = 0;
            cmbUsoCFDI.SelectedIndex = 0;
            cmbFormaPago.SelectedIndex = 0;
            txtReferencia.Clear();

            txtTotRefacciones.Clear();
            txtTotManoObra.Clear();
            txtTotManoObra.Clear();
            txtTotIVA.Clear();
            txtTotTOTAL.Clear();

            //Consecutivo = 0;
            grdDetalle.DataSource = null;
            FormatoGeneral(grdDetalle);
        }
        private void FormatoGeneral(DataGridView grid)
        {
            //ESTILO GENERAL
            grid.RowsDefaultCellStyle.BackColor = Color.White;
            grid.AlternatingRowsDefaultCellStyle.BackColor = Color.AliceBlue;

            grid.RowsDefaultCellStyle.SelectionForeColor = Color.Yellow;
            grid.RowsDefaultCellStyle.SelectionBackColor = Color.Gray;
            grid.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            grid.DefaultCellStyle.Font = new Font("Berlin Sans FB", 12);
            grid.ColumnHeadersDefaultCellStyle.Font = new Font("Berlin Sans FB", 12);
            grid.ColumnHeadersDefaultCellStyle.BackColor = Color.Yellow;
            grid.ColumnHeadersDefaultCellStyle.ForeColor = Color.Red;
            grid.EnableHeadersVisualStyles = false;
        }
        private void CargaCombos()
        {
            try
            {
                OperationResult resp = new ComercialSvc().getadmConceptosxFilter(0, " c.CIDDOCUMENTODE = 4");
                if (resp.typeResult == ResultTypes.success)
                {
                    listConceptos = (List<admConceptos>)resp.result;
                    cmbConcepto.DataSource = null;
                    cmbConcepto.DataSource = listConceptos;
                    cmbConcepto.DisplayMember = "CNOMBRECONCEPTO";
                    cmbConcepto.SelectedIndex = 0;
                }
                CargaAnexo20();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }
        private void CargaAnexo20()
        {
            try
            {
                //FORMA DE PAGO
                resp = new CatSATSvc().getCatSATxFilter(0, "c.Tipo = 'FORPAG'", " c.idclave");
                if (resp.typeResult == ResultTypes.success)
                {
                    listFormaPago = (List<CatSAT>)resp.result;
                    cmbFormaPago.DataSource = null;
                    cmbFormaPago.DataSource = listFormaPago;
                    cmbFormaPago.DisplayMember = "Descripcion";
                    cmbFormaPago.SelectedIndex = 0;
                }
                //USO DE CFDI
                resp = new CatSATSvc().getCatSATxFilter(0, "c.Tipo = 'CFDI'", " c.idclave");
                if (resp.typeResult == ResultTypes.success)
                {
                    listCFDI = (List<CatSAT>)resp.result;
                    cmbUsoCFDI.DataSource = null;
                    cmbUsoCFDI.DataSource = listCFDI;
                    cmbUsoCFDI.DisplayMember = "Descripcion";
                    cmbUsoCFDI.SelectedIndex = 0;

                }

                //METODO DE PAGO
                resp = new CatSATSvc().getCatSATxFilter(0, "c.Tipo = 'METPAG'", " c.idclave");
                if (resp.typeResult == ResultTypes.success)
                {
                    listMetodoPago = (List<CatSAT>)resp.result;
                    cmbMetodoPago.DataSource = null;
                    cmbMetodoPago.DataSource = listMetodoPago;
                    cmbMetodoPago.DisplayMember = "Descripcion";
                    cmbMetodoPago.SelectedIndex = 0;

                }

            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + ex.Message,
this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            }
        }
        private void CargaEmpresas()
        {
            resp = new CatEmpresasSvc().getCatEmpresasxFiltro();
            if (resp.typeResult == ResultTypes.success)
            {
                listEmpresas = (List<CatEmpresas>)resp.result;
            }
        }


        private void CargaClientes()
        {
            resp = new ComercialSvc().getadmClientesxFilter(0, "CTIPOCLIENTE in (1,2)");
            if (resp.typeResult == ResultTypes.success)
            {
                listClientes = (List<admClientes>)resp.result;
                cmbCCODIGOC01_CLI.DataSource = null;
                cmbCCODIGOC01_CLI.DataSource = listClientes;

                cmbCCODIGOC01_CLI.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                cmbCCODIGOC01_CLI.AutoCompleteSource = AutoCompleteSource.ListItems;
                cmbCCODIGOC01_CLI.DisplayMember = "CRAZONSOCIAL";
                cmbCCODIGOC01_CLI.SelectedIndex = 0;

            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            opcionForma = opcForma.Cancelar;
            HabilitaBotones(opcHabilita.Nuevo);
            HabilitaCampos(opcHabilita.Nuevo);
            //AutoAcompletaProductos();
            //CabMov = new CabMovimientos();
            //Consecutivo = 0;
            //cmbIdEmpresa.Focus();
            txtFolio.Focus();
            //cmbCCODIGOC01_CLI.Focus();
        }





        private void txtFolio_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                if (txtFolio.Text == "")
                {
                    MessageBox.Show("El campo Folio no puede ser vacio", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtFolio.Focus();
                    txtFolio.SelectAll();

                }
                else if (txtFolio.Text == "0")
                {
                    MessageBox.Show("El campo Folio no puede ser Cero", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtFolio.Focus();
                    txtFolio.SelectAll();

                }
                else
                {
                    if (Inicio.IsNumeric(txtFolio.Text))
                    {
                        if (VerificarDocumento(txtSerie.Text, Convert.ToInt32(txtFolio.Text)))
                        {
                            //Cargar Faaaactura
                            if (movimiento != null)
                            {
                                CargaFactura(movimiento, documentoCOM);
                            }

                        }
                        else
                        {
                            //MessageBox.Show("El Documento no", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtFolio.Focus();
                            txtFolio.SelectAll();

                        }

                    }
                    else
                    {
                        MessageBox.Show("El campo Folio tiene que ser numerico", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtFolio.Focus();
                        txtFolio.SelectAll();
                    }
                }
            }
        }

        private bool VerificarDocumento(string NoSerie, int Folio)
        {
            bool BExisteLocal = false;
            bool BExisteCOM = false;
            try
            {
                ordenservicio = null;
                documentoCOM = null;

                //Verificar que la factura Exista en Ordenes de servicio
                resp = new CabOrdenServicioSvc().getCabOrdenServicioxFilter(0, "c.CSERIEDOCUMENTO = '" + NoSerie + "' and c.CFOLIO = " + Folio);
                if (resp.typeResult == ResultTypes.success)
                {
                    ordenservicio = ((List<CabOrdenServicio>)resp.result)[0];
                    BExisteLocal = true;
                }

                //Verificar que la factura Exista en CabMovimientos
                resp = new MovimientosSvc().getCabMovimientosxFilter(0, "c.NoSerie = '" + NoSerie + "' and c.idmovimiento = " + Folio);
                if (resp.typeResult == ResultTypes.success)
                {
                    movimiento = ((List<CabMovimientos>)resp.result)[0];
                    BExisteLocal = true;
                }
                else
                {
                    if (!BExisteLocal)
                    {
                        MessageBox.Show("El Documento no Existe", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
                }

                //Verificar que la factura Exista en COMERCIAL
                resp = new ComercialSvc().getadmDocumentosxFilter(0, "c.CIDDOCUMENTODE = 4 and c.CSERIEDOCUMENTO = '" + NoSerie + "' and c.CFOLIO = " + Folio);
                if (resp.typeResult == ResultTypes.success)
                {
                    documentoCOM = ((List<admDocumentos>)resp.result)[0];
                    if (documentoCOM.CCANCELADO == 0)
                    {
                        BExisteCOM = true;
                    }
                    else
                    {
                        MessageBox.Show("El Documento Esta CANCELADO en COMERCIAL", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }

                }
                else
                {
                    MessageBox.Show("El Documento no Existe En comercial", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }


                if (BExisteLocal && BExisteCOM)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + ex.Message,
                this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;

            }
        }

        private void txtSerie_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                txtFolio.Focus();
            }
        }
        private void CargaFactura(CabMovimientos cab, admDocumentos docCom)
        {
            try
            {
                //if (ordenservicio!= null)
                //{
                //    resp = new MovimientosSvc().getDetMovimientosxFilter(cab.IdMovimiento);
                //    if (resp.typeResult == ResultTypes.success)
                //    {

                //    }
                //}

                resp = new MovimientosSvc().getDetMovimientosxFilter(cab.NoMovimiento);
                if (resp.typeResult == ResultTypes.success)
                {

                    //Mapear Detalle
                    listDetMov = (List<DetMovimientos>)resp.result;
                    grdDetalle.DataSource = null;
                    grdDetalle.DataSource = listDetMov;
                    FormatoGridEncabezados();
                    ActualizaTotales();

                    //Mapear Cabecero
                    SeleccionaCliente(((admClientes)cmbCCODIGOC01_CLI.SelectedItem).CCODIGOCLIENTE);
                    dtpFechaMovimiento.Value = cab.FechaMovimiento;
                    dtpFechaMovimientoHora.Value = cab.FechaMovimiento;
                    if (cab.Vehiculo != "")
                    {
                        txtIdUnidadTrans.Text = cab.Vehiculo;
                        resp = new CatUnidadTransSvc().geCatUnidadTransxFilter(cab.Vehiculo);
                        if (resp.typeResult == ResultTypes.success)
                        {
                            unidadtrans = ((List<CatUnidadTrans>)resp.result)[0];
                            txtTipoUnidadTrans.Text = unidadtrans.nomTipoUniTras;
                        }
                    }
                    SeleccionaConcepto(((admConceptos)cmbConcepto.SelectedItem).CCODIGOCONCEPTO);
                    txtReferencia.Text = cab.CREFEREN01;

                    VerificaDocumentoSDK(((admConceptos)cmbConcepto.SelectedItem).CCODIGOCONCEPTO, txtSerie.Text, txtFolio.Text);
                    //lError = SDK_Comercial.fDocumentoUUID(Concepto, Serie, double.Parse(Folio), lUUID.ToString());
                    //if (lError == 0)
                    //{
                    //    txtiUUID.Text = lUUID.ToString();
                    //}
                    //else
                    //{
                    //    SDK_Comercial.MuestraError(lError);
                    //    Exito = false;
                    //}
                }
                else
                {
                    //
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + ex.Message,
                  this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void FormatoGridEncabezados()
        {

            grdDetalle.Columns["NoMovimiento"].Visible = false;
            grdDetalle.Columns["CIDUNIDA01"].Visible = false;
            grdDetalle.Columns["IdMovimiento"].Visible = false;
            grdDetalle.Columns["NoSerie"].Visible = false;
            grdDetalle.Columns["TipoMovimiento"].Visible = false;
            grdDetalle.Columns["Costo"].Visible = false;
            grdDetalle.Columns["PorcDescto"].Visible = false;
            grdDetalle.Columns["ImpDescto"].Visible = false;
            grdDetalle.Columns["CIDALMACEN"].Visible = false;
            grdDetalle.Columns["Existencia"].Visible = false;
            grdDetalle.Columns["CantidadBASE"].Visible = false;
            grdDetalle.Columns["CIDUNIDA01BASE"].Visible = false;
            grdDetalle.Columns["tFactorUnidad"].Visible = false;
            grdDetalle.Columns["tNoEquivalente"].Visible = false;
            grdDetalle.Columns["EsListaPrecios"].Visible = false;
            grdDetalle.Columns["CIDUNIDA02"].Visible = false;
            grdDetalle.Columns["CantidadEQUI"].Visible = false;
            grdDetalle.Columns["COBSERVA01"].Visible = false;


            grdDetalle.Columns["Consecutivo"].HeaderText = "#";
            grdDetalle.Columns["CCODIGOP01_PRO"].HeaderText = "Codigo";
            grdDetalle.Columns["CNOMBREPRODUCTO"].HeaderText = "Producto";
            grdDetalle.Columns["Cantidad"].HeaderText = "Cantidad";
            grdDetalle.Columns["Precio"].HeaderText = "Precio";
            grdDetalle.Columns["PorcIVA"].HeaderText = "IVA";
            grdDetalle.Columns["EsServicio"].HeaderText = "Servicio";


            //gridVenta.Columns["PorcIVA"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;
            //gridVenta.Columns["PorcIVA"].DefaultCellStyle.Format = "P2";

            grdDetalle.Columns["Precio"].DefaultCellStyle.Format = "C2";
            grdDetalle.Columns["Precio"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight;
            grdDetalle.Columns["Total"].DefaultCellStyle.Format = "C2";
            grdDetalle.Columns["Total"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight;
            //grdDetalle.Columns["ImpDescto"].DefaultCellStyle.Format = "C2";
            //grdDetalle.Columns["ImpDescto"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight;

            //grdDetalle.Columns["PorcDescto"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;
            //gridVenta.Columns["PorcDescto"].DefaultCellStyle.Format = "P";
            grdDetalle.Columns["Cantidad"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;


            grdDetalle.Columns["Consecutivo"].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
            grdDetalle.Columns["CCODIGOP01_PRO"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdDetalle.Columns["CNOMBREPRODUCTO"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdDetalle.Columns["Cantidad"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            grdDetalle.Columns["Precio"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            grdDetalle.Columns["PorcIVA"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            grdDetalle.Columns["Total"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            grdDetalle.Columns["Consecutivo"].DisplayIndex = 1;
            grdDetalle.Columns["CCODIGOP01_PRO"].DisplayIndex = 2;
            grdDetalle.Columns["CNOMBREPRODUCTO"].DisplayIndex = 3;
            grdDetalle.Columns["Cantidad"].DisplayIndex = 4;
            grdDetalle.Columns["Precio"].DisplayIndex = 5;
            grdDetalle.Columns["PorcIVA"].DisplayIndex = 6;
            grdDetalle.Columns["Total"].DisplayIndex = 7;
            grdDetalle.Columns["EsServicio"].DisplayIndex = 8;

        }
        private void SeleccionaCliente(string clave)
        {
            try
            {
                int i = 0;
                foreach (admClientes item in cmbCCODIGOC01_CLI.Items)
                {
                    if (item.CCODIGOCLIENTE == clave)
                    {
                        cmbCCODIGOC01_CLI.SelectedIndex = i;
                        return;
                    }
                    else
                    {
                        i++;
                    }
                }
            }
            catch (Exception e)
            {

                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + e.Message,
                  this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void SeleccionaConcepto(string CCODIGOCONCEPTO)
        {
            try
            {
                int i = 0;
                foreach (admConceptos item in cmbConcepto.Items)
                {
                    if (item.CCODIGOCONCEPTO == CCODIGOCONCEPTO)
                    {
                        cmbConcepto.SelectedIndex = i;
                        return;
                    }
                    else
                    {
                        i++;
                    }
                }
            }
            catch (Exception e)
            {

                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + e.Message,
                  this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btnGrabar_Click(object sender, EventArgs e)
        {
            try
            {
                indice = 0;
                //busca
                if (EnviaComercial(((admConceptos)cmbConcepto.SelectedItem).CCODIGOCONCEPTO, txtSerie.Text, txtFolio.Text))
                {
                    //grabar datos locales
                    //Actualizar Servicios
                    if (ordenservicio != null)
                    {
                        StrSql = objsql.ActualizaCampoTabla("CabOrdenServicio",
                        "CIDDOCUMENTO", eTipoDato.TdNumerico, "0",
                        "idOrdenSer", eTipoDato.TdNumerico, ordenservicio.idOrdenSer.ToString());
                        System.Array.Resize(ref ArraySql, indice + 1);
                        ArraySql[indice] = StrSql;
                        indice += 1;

                        StrSql = objsql.ActualizaCampoTabla("CabOrdenServicio",
                       "CSERIEDOCUMENTO", eTipoDato.TdNumerico, "",
                       "idOrdenSer", eTipoDato.TdNumerico, ordenservicio.idOrdenSer.ToString());
                        System.Array.Resize(ref ArraySql, indice + 1);
                        ArraySql[indice] = StrSql;
                        indice += 1;
                    }

                    if (movimiento != null)
                    {
                        StrSql = objsql.ActualizaCampoTabla("CabMovimientos",
                        "Estatus", eTipoDato.TdCadena, "CAN",
                        "NoMovimiento", eTipoDato.TdNumerico, movimiento.NoMovimiento.ToString());
                        System.Array.Resize(ref ArraySql, indice + 1);
                        ArraySql[indice] = StrSql;
                        indice += 1;

                        StrSql = objsql.ActualizaCampoTabla("CabMovimientos",
                       "UsuarioCancela", eTipoDato.TdCadena, _user.nombreUsuario,
                       "NoMovimiento", eTipoDato.TdNumerico, movimiento.NoMovimiento.ToString());
                        System.Array.Resize(ref ArraySql, indice + 1);
                        ArraySql[indice] = StrSql;
                        indice += 1;

                        StrSql = objsql.ActualizaCampoTabla("CabMovimientos",
                       "FechaCancela", eTipoDato.TdCadena, Tools.FormatFecHora(DateTime.Now,true,true) ,
                       "NoMovimiento", eTipoDato.TdNumerico, movimiento.NoMovimiento.ToString());
                        System.Array.Resize(ref ArraySql, indice + 1);
                        ArraySql[indice] = StrSql;
                        indice += 1;
                    }
                    if (indice > 0)
                    {
                        //MessageBox.Show("Sentencias por Ejecutar local " + indice.ToString(), "Operación Exitosa", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        string Respuesta;
                        Respuesta = new UtilSvc().EjecutaSentenciaSql("", ref ArraySql, indice);
                        if (Respuesta == "EFECTUADO")
                        {
                            //MessageBox.Show("Se Facturo Satisfactoriamente la Order de Servicio: " + txtidOrdenSer.Text, "Operación Exitosa", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            //btnCancelar.PerformClick();
                            MessageBox.Show("Se Cancelo Satisfactoriamente la Factura: " + txtSerie.Text + " " + txtFolio.Text, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            Cancelar();

                        }
                    }
                    //Actualizar Movimientos

                }
                else
                {
                    //ocurrio un error

                }
            }
            catch (Exception)
            {

                throw;
            }
            //          if (fErr(Errores, fBuscarDocumento(concepto.c_str(), serie.c_str(),
            //folio.c_str())) == 0)
            //          {
            //              fErr(Errores, fDocumentoUUID(AnsiString(concepto).c_str(),
            //               serie.c_str(), folio.ToDouble(), datoSDK));
            //              UUID = datoSDK;
            //              if (!UUID.IsEmpty())
            //              {
            //                  if (fErr(Errores, fCancelaDocumento()) != 0)
            //                  {
            //                      Errores->Add("No se pudo cancelar el documento: " + serie +
            //                       " " + folio);
            //                      retorno = 1; // No se pudo cancelar el documento
            //                  }
            //                  else
            //                  { // Acuse de cancelación.

            //                      fCancelaUUID(UUID.c_str(), concepto.c_str(), contraseña);
            //                  }
            //              }
            //              else
            //              {
            //                  Errores->Add
            //                   ("Para realizar la cancelación, el documento con serie: " +
            //                   serie + " y folio: " + folio + " debe estar timbrado.");
            //                  retorno = 1; // No se pudo cancelar el documento
            //              }
            //          }
            //          else
            //          {
            //              Errores->Add("No se encontró el documento: " + serie + " " + folio);
            //              retorno = 1; // No se encontro el documento
            //          }
        }
        private void ActualizaTotales()
        {

            if (listDetMov != null)
            {
                if (listDetMov.Count > 0)
                {
                    foreach (var item in listDetMov)
                    {
                        if (item.EsServicio)
                        {
                            totServ = totServ + (item.Cantidad * item.Precio);
                            ContServ = ContServ + item.Cantidad;
                        }
                        else
                        {
                            totRef = totRef + (item.Cantidad * item.Precio);
                            ContRef = ContRef + item.Cantidad;
                        }
                        Subtot = Subtot + (item.Cantidad * item.Precio);
                        totiva = totiva + ((item.Cantidad * item.Precio) * (item.PorcIVA) / 100);
                    }
                    total = Subtot + totiva;

                    //desplegar
                    txtTotRefacciones.Text = totRef.ToString("C2");
                    txtTotManoObra.Text = totServ.ToString("C2");
                    txtTotSubTotal.Text = Subtot.ToString("C2");
                    txtTotIVA.Text = totiva.ToString("C2");
                    txtTotTOTAL.Text = total.ToString("C2");
                    //lblTotRef.Text = "Total de Refacciones : " + ContRef;
                    //lblTotServ.Text = "Total de Servicios : " + ContServ;
                }
            }
        }
        private bool EnviaComercial(string Concepto, string Serie, string Folio)
        {
            
            bool Exito = false;
            StringBuilder lUUID = new StringBuilder(50);
            StringBuilder conocept = new StringBuilder(Concepto);
            StringBuilder lserieDocto = new StringBuilder(Serie);
            //StringBuilder sbConcepto = new StringBuilder(50);
            //sbConcepto = Concepto;


            Parametro_PassFacturacion = "12345678a";
            StringBuilder asPassword = new StringBuilder("12345678a");
            StringBuilder aUUID = new StringBuilder(256);

            StringBuilder serieCerEmisor = new StringBuilder(3000);
            StringBuilder folioFiscalUUid = new StringBuilder(3000);
            StringBuilder serieCertSat = new StringBuilder(3000);
            StringBuilder fecha = new StringBuilder(3000);
            StringBuilder selloDigital = new StringBuilder(3000);
            StringBuilder selloSat = new StringBuilder(3000);
            StringBuilder cadenaOriginalCompSalt = new StringBuilder(3000);
            StringBuilder regimen = new StringBuilder(3000);
            StringBuilder lugarExp = new StringBuilder(3000);
            StringBuilder moneda = new StringBuilder(3000);
            StringBuilder folioFiscalOriginal = new StringBuilder(3000);
            StringBuilder serieFolioFiscal = new StringBuilder(3000);
            StringBuilder fechaFolioFiscal = new StringBuilder(3000);
            StringBuilder montoFolioFiscal = new StringBuilder(3000);
            try
            {
                if (IniciarSesionSDK(param.idEmpresaCOMTaller))
                {
                    //    lError = SDK_Comercial.fBuscarDocumento(Concepto, Serie, Folio);
                    //    if (lError == 0)
                    //    {
                    //        //lError = SDK_Comercial.fObtieneDatosCFDI(asPassword);
                    //        //lError = SDK_Comercial.fDocumentoUUID(Concepto, Serie, double.Parse(Folio), lUUID.ToString());
                    //        lError = SDK_Comercial.fGetDatosCFDI(serieCerEmisor, folioFiscalUUid, serieCertSat, fecha, selloDigital, selloSat, cadenaOriginalCompSalt, regimen,
                    //lugarExp, moneda, folioFiscalOriginal, serieFolioFiscal, fechaFolioFiscal, montoFolioFiscal);

                    //        if (lError == 0)
                    //        {
                    //            txtiUUID.Text = folioFiscalUUid.ToString();
                    //        }
                    //        else
                    //        {
                    //            SDK_Comercial.MuestraError(lError);
                    //            Exito = false;
                    //        }

                    //    }
                    //    else
                    //    {
                    //        SDK_Comercial.MuestraError(lError);
                    //        Exito = false;
                    //    }



                    lError = SDK_Comercial.fBuscarDocumento(Concepto, Serie, Folio);
                    if (lError == 0)
                    {
                        lError = SDK_Comercial.fCancelaDocumento();
                        if (lError == 0)
                        {
                            //lError = SDK_Comercial.fDocumentoUUID(conocept, lserieDocto, double.Parse(Folio), lUUID);
                            lError = SDK_Comercial.fGetDatosCFDI(serieCerEmisor, folioFiscalUUid, serieCertSat, fecha, selloDigital, selloSat, cadenaOriginalCompSalt, regimen,
                            lugarExp, moneda, folioFiscalOriginal, serieFolioFiscal, fechaFolioFiscal, montoFolioFiscal);
                            if (lError == 0)
                            {
                                byte lLicencia = 0;
                                lError = SDK_Comercial.fInicializaLicenseInfo(lLicencia);
                                if (lError == 0)
                                {
                                    Exito = true;
                                    //lError = SDK_Comercial.fCancelaUUID(folioFiscalUUid.ToString(), ((admConceptos)cmbConcepto.SelectedItem).CIDCONCEPTODOCUMENTO.ToString(), Parametro_PassFacturacion);
                                    //if (lError == 0)
                                    //{
                                    //    Exito = true;
                                    //}
                                    //else
                                    //{
                                    //    SDK_Comercial.MuestraError(lError);
                                    //    Exito = false;
                                    //}
                                }
                                else
                                {
                                    SDK_Comercial.MuestraError(lError);
                                    Exito = false;
                                }
                            }
                            else
                            {
                                SDK_Comercial.MuestraError(lError);
                                Exito = false;
                            }
                        }
                        else
                        {
                            SDK_Comercial.MuestraError(lError);
                            Exito = false;
                        }
                    }
                    else
                    {
                        SDK_Comercial.MuestraError(lError);
                        Exito = false;
                    }
                }
                return Exito;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + ex.Message,
                   this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                return false;
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Cancelar();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void txtFolio_TextChanged(object sender, EventArgs e)
        {

        }

        private void VerificaDocumentoSDK(string Concepto, string Serie, string Folio)
        {
            Parametro_PassFacturacion = "12345678a";
            try
            {
                if (IniciarSesionSDK(param.idEmpresaCOMTaller))
                {
                    lError = SDK_Comercial.fBuscarDocumento(Concepto, Serie, Folio);
                    if (lError == 0)
                    {
                        lError = SDK_Comercial.fGetDatosCFDI(serieCerEmisor, folioFiscalUUid, serieCertSat, fecha, selloDigital, selloSat, cadenaOriginalCompSalt, regimen,
lugarExp, moneda, folioFiscalOriginal, serieFolioFiscal, fechaFolioFiscal, montoFolioFiscal);
                        if (lError == 0)
                        {
                            txtiUUID.Text = folioFiscalUUid.ToString();
                            Inicio.ImprimeFactura(Serie + "" + Folio.ToString(), @"C:\Compac\Empresas\adprueba\XML_SDK\");
                            //byte lLicencia = 0;
                            //lError = SDK_Comercial.fInicializaLicenseInfo(lLicencia);
                            //if (lError == 0)
                            //{
                            //    lError = SDK_Comercial.fEmitirDocumento(Concepto, Serie, Convert.ToDouble(Folio), Parametro_PassFacturacion, "");
                            //    if (lError == 0)
                            //    {
                            //        lError = SDK_Comercial.fEntregEnDiscoXML(Concepto, Serie, Convert.ToDouble(Folio), 1, lPlantilla);
                            //        if (lError == 0)
                            //        {

                            //        }
                            //        else
                            //        {
                            //            SDK_Comercial.MuestraError(lError);
                            //            //Exito = false;
                            //        }

                            //    }
                            //    else
                            //    {
                            //        SDK_Comercial.MuestraError(lError);
                            //        //Exito = false;
                            //    }
                            //}
                            //else
                            //{
                            //    SDK_Comercial.MuestraError(lError);
                            //    //Exito = false;
                            //}
                        }
                        else
                        {
                            SDK_Comercial.MuestraError(lError);
                            //Exito = false;
                        }
                    }
                    else
                    {
                        SDK_Comercial.MuestraError(lError);
                        //Exito = false;
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void txtFolio_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

            }
        }
        //private void ImprimeFactura(string nombreDoc,string Ruta)
        //{
        //    ProcessStartInfo loPSI = new ProcessStartInfo();
        //    Process loProceso = new Process();
        //    try
        //    {

        //        string pdfPath = Path.Combine(Application.StartupPath, Ruta + nombreDoc + ".pdf");

        //        Process.Start(pdfPath);


        //        //loPSI.FileName = Ruta + nombreDoc + ".pdf";
        //        //loProceso = Process.Start(loPSI);
        //        //                Dim loPSI As New ProcessStartInfo
        //        //Dim loProceso As New Process

        //        //        'loPSI.FileName = "C:\Users\Guevara\Downloads\Dominio DDD Documento_completo.pdf"
        //        //        loPSI.FileName = Ruta & "\" & nombreDoc

        //        //        Try
        //        //            loProceso = Process.Start(loPSI)
        //        //        Catch Exp As Exception
        //        //            MessageBox.Show(Exp.Message, "Se produjo un error, consulte con soporte", MessageBoxButtons.OK, MessageBoxIcon.Information)
        //        //        End Try
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + ex.Message,
        //        this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        //    }
        //}
    }//
}
