﻿using Core.Models;
using Fletera2.Clases;
using Fletera2Entidades;
using Fletera2Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fletera2.Procesos
{
    public partial class fCompGastosFoliosDin : Form
    {
        Usuario _usuario;
        FoliosDinero folioxComp;
        CatGastos gasto;
        CabGuia viaje;


        List<CatGastos> listGastos;
        List<CatPersonal> listAutoriza;
        List<CompGastosFoliosDin> listComGastos;


        decimal tComprobado;
        decimal tSaldo;
        int Consecutivo;

        //Para MsgBox
        string caption;
        MessageBoxButtons buttons = MessageBoxButtons.YesNo;
        DialogResult result;
        string mensaje = "";

        ImprimeCompGastos Imprime = new ImprimeCompGastos();
        PrinterSettings prtSettings;
        PrintDocument prtDoc;

        Font printFont = new System.Drawing.Font("Courier New", 7);
        int lineaActual = 0;
        int ContPaginas = 0;
        int NumRegistros = 1;


        public fCompGastosFoliosDin(Usuario user)
        {
            _usuario = user;
            InitializeComponent();
        }

        private void fCompGastosFoliosDin_Load(object sender, EventArgs e)
        {
            Cancelar();
        }

        private void HabilitaBotonesMenuPrinc(opcHabilita opc)
        {
            switch (opc)
            {
                case opcHabilita.DesHabilitaTodos:
                    btnNuevo.Enabled = true;
                    btnGrabar.Enabled = false;
                    btnCancelar.Enabled = false;
                    btnSalir.Enabled = true;
                    break;
                case opcHabilita.Iniciando:
                    break;
                case opcHabilita.Editando:
                    break;
                case opcHabilita.Nuevo:
                    btnNuevo.Enabled = false;
                    btnGrabar.Enabled = true;
                    btnCancelar.Enabled = true;
                    btnSalir.Enabled = false;

                    break;
                default:
                    break;
            }
        }
        private void InactivosSiempre()
        {
            dtpFechaFol.Enabled = false;
            dtpFechaFolHora.Enabled = false;
            txtNomTipo.Enabled = false;
            txtNomEntrega.Enabled = false;
            txtNomRecibe.Enabled = false;
            txtImporteFol.Enabled = false;
            txtImporteCompFol.Enabled = false;
            txtSaldoFol.Enabled = false;
        }
        private void HabilitaCamposFD(opcHabilita opc)
        {

            switch (opc)
            {
                case opcHabilita.DesHabilitaTodos:
                    //gpoCom.Enabled = false;
                    gpoFaC.Enabled = false;
                    grdComprobantes.Enabled = false;
                    break;
                case opcHabilita.Iniciando:
                    break;
                case opcHabilita.Editando:
                    break;
                case opcHabilita.Nuevo:
                    //gpoCom.Enabled = false;
                    grdComprobantes.Enabled = true;
                    gpoFaC.Enabled = true;
                    InactivosSiempre();
                    txtFolioDin.Enabled = true;



                    break;
                default:
                    break;
            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            
            HabilitaBotonesMenuPrinc(opcHabilita.Nuevo);
            HabilitaCamposFD(opcHabilita.Nuevo);
            txtFolioDin.Focus();
        }

        private void txtFolioDin_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                CargaForma(txtFolioDin.Text, opcBusqueda.FoliosDinero);
                menu2.Items["btnMenu2Agre"].Select();

            }
        }

        private void CargaForma(string Valor, opcBusqueda opc)
        {
            if (Valor == "")
            {
                return;
            }

            if (opc == opcBusqueda.FoliosDinero)
            {
                OperationResult resp = new FoliosDineroSvc().getFoliosDineroxId(Convert.ToInt32(Valor));
                if (resp.typeResult == ResultTypes.success)
                {
                    folioxComp = ((List<FoliosDinero>)resp.result)[0];
                    txtFolioDin.Tag = folioxComp;
                    txtFolioDin.Text = folioxComp.FolioDin.ToString();
                    dtpFechaFol.Value = folioxComp.Fecha;
                    dtpFechaFolHora.Value = folioxComp.Fecha;
                    txtNomTipo.Text = folioxComp.DescripcionFolDin;
                    txtNomEntrega.Text = folioxComp.NombreEnt;
                    txtNomRecibe.Text = folioxComp.NombreRec;
                    txtImporteFol.Text = folioxComp.Importe.ToString();
                    txtImporteCompFol.Text = folioxComp.ImporteComp.ToString();
                    txtSaldoFol.Text = (folioxComp.Importe - folioxComp.ImporteComp).ToString();

                    txtFolioDin.Enabled = false;
                    HabilitaBotonesMenuAlt(opcHabilita.Iniciando);

                    //verificar si hay comprobaciones para ese folio
                    OperationResult respcg = new CompGastosFoliosDinSvc().getCompGastosFoliosDinxId(0, " c.FolioDin = " + Valor, " order by c.consecutivo");
                    if (respcg.typeResult == ResultTypes.success)
                    {
                        listComGastos = (List<CompGastosFoliosDin>)respcg.result;
                        grdComprobantes.DataSource = null;
                        grdComprobantes.DataSource = listComGastos;
                        if (listComGastos != null)
                        {
                            foreach (var item in listComGastos)
                            {
                                Consecutivo = item.Consecutivo;
                            }

                        }
                        else
                        {
                            Consecutivo = 0;
                        }
                        FormatoGrid();
                        ActualizaComprobado();
                    }
                    else
                    {
                        Consecutivo = 0;
                    }
                    
                }
            }
            else if (opc == opcBusqueda.CatGastos)
            {
                OperationResult respG = new CatGastosSvc().getCatGastosxFiltro(Convert.ToInt32(Valor));
                if (respG.typeResult == ResultTypes.success)
                {
                    gasto = ((List<CatGastos>)respG.result)[0];
                    HabilitaSegunGasto(gasto);
                    if (dtpFechaFC.Enabled)
                    {
                        dtpFechaFC.Focus();
                    }

                }
            }
        }

        private void HabilitaSegunGasto(CatGastos gasto)
        {
            HabilitaCamposCOM(opcHabilita.Iniciando);
            txtFolio.Enabled = false;
            cmbidGasto.Enabled = false;
            dtpFechaFC.Enabled = true;
            if (gasto.ReqAutorizacion)
            {
                chkAutoriza.Enabled = true;
                chkAutoriza.Checked = true;
                cmbAutoriza.Enabled = true;
            }
            else
            {
                chkAutoriza.Enabled = false;
                cmbAutoriza.Enabled = false;
            }
            if (gasto.ReqComprobante)
            {
                chkNoDocumento.Enabled = true;
                txtNoDocumento.Enabled = true;
                chkNoDocumento.Checked = true;
            }
            else
            {
                chkNoDocumento.Enabled = false;
                txtNoDocumento.Enabled = false;
            }
            if (gasto.Deducible)
            {
                txtIVA.Enabled = true;
            }
            else
            {
                txtIVA.Enabled = false;
            }
            txtImporte.Enabled = true;
            txtTotal.Enabled = false;
            txtObservaciones.Enabled = true;
        }
        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            //Preguntar
            Cancelar();
        }

        private void Cancelar()
        {
            LLenaCombos();
            HabilitaCamposFD(opcHabilita.DesHabilitaTodos);
            HabilitaCamposCOM(opcHabilita.DesHabilitaTodos);
            HabilitaBotonesMenuPrinc(opcHabilita.DesHabilitaTodos);
            HabilitaBotonesMenuAlt(opcHabilita.DesHabilitaTodos);
            LimpiaCamposFD();
            LimpiaCamposCom();
            grdComprobantes.DataSource = null;

        }
        private void LimpiaCamposFD()
        {
            dtpFechaFol.Value = DateTime.Now;
            dtpFechaFolHora.Value = DateTime.Now;
            txtNomTipo.Clear();
            txtNomEntrega.Clear();
            txtNomRecibe.Clear();
            txtImporteFol.Clear();
            txtImporteCompFol.Clear();
            txtSaldoFol.Clear();
            txtFolioDin.Clear();
        }

        private void LimpiaCamposCom()
        {
            txtFolio.Clear();
            dtpFechaFC.Value = DateTime.Now;
            cmbidGasto.SelectedIndex = 0;
            chkAutoriza.Enabled = false;
            //cmbAutoriza.SelectedIndex = 0;
            chkNoDocumento.Checked = false;
            txtNoDocumento.Clear();
            txtObservaciones.Clear();
            txtImporte.Clear();
            txtIVA.Clear();
            txtTotal.Clear();
        }
        private void HabilitaBotonesMenuAlt(opcHabilita opc)
        {
            switch (opc)
            {
                case opcHabilita.DesHabilitaTodos:
                    btnMenu2Agre.Enabled = false;
                    btnMenu2Borrar.Enabled = false;
                    btnMenu2Can.Enabled = false;
                    break;
                case opcHabilita.Iniciando:
                    btnMenu2Agre.Enabled = true;
                    btnMenu2Borrar.Enabled = false;
                    btnMenu2Can.Enabled = false;
                    break;
                case opcHabilita.Editando:
                    btnMenu2Agre.Enabled = false;
                    btnMenu2Borrar.Enabled = true;
                    btnMenu2Can.Enabled = true;
                    break;
                case opcHabilita.Nuevo:
                    btnMenu2Agre.Enabled = false;
                    btnMenu2Borrar.Enabled = false;
                    btnMenu2Can.Enabled = true;
                    break;
                default:
                    break;
            }
        }

        private void btnMenu2Agre_Click(object sender, EventArgs e)
        {
            if (txtFolioDin.Text != "")
            {
                if (folioxComp.Importe - folioxComp.ImporteComp == 0)
                {
                    MessageBox.Show("No se puede agregar mas comprobaciones para el folio " + txtFolioDin.Text , "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    menu2.Items["btnMenu2Agre"].Select();
                    return;

                }
                HabilitaBotonesMenuAlt(opcHabilita.Nuevo);
                HabilitaCamposCOM(opcHabilita.Nuevo);
                cmbidGasto.Focus();
            }


        }
        private void HabilitaCamposCOM(opcHabilita opc)
        {
            switch (opc)
            {
                case opcHabilita.DesHabilitaTodos:
                    gpoCom.Enabled = false;
                    break;
                case opcHabilita.Iniciando:
                    gpoCom.Enabled = true;
                    break;
                case opcHabilita.Editando:
                    break;
                case opcHabilita.Nuevo:
                    gpoCom.Enabled = true;
                    txtFolio.Enabled = false;
                    cmbidGasto.Enabled = true;
                    dtpFechaFC.Enabled = false;
                    chkAutoriza.Enabled = false;
                    cmbAutoriza.Enabled = false;
                    chkNoDocumento.Enabled = false;
                    txtNoDocumento.Enabled = false;
                    txtImporte.Enabled = false;
                    txtIVA.Enabled = false;
                    txtTotal.Enabled = false;
                    txtObservaciones.Enabled = false;
                    break;
                default:
                    break;
            }
        }

        private void cmbidGasto_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cmbidGasto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                CargaForma(((CatGastos)cmbidGasto.SelectedValue).idGasto.ToString(), opcBusqueda.CatGastos);

            }
        }

        private void LLenaCombos()
        {
            //Tipos de Gasto
            OperationResult resp = new CatGastosSvc().getCatGastosxFiltro(0, " G.Activo = 1");
            if (resp.typeResult == ResultTypes.success)
            {
                listGastos = (List<CatGastos>)resp.result;
                cmbidGasto.DataSource = listGastos;
                cmbidGasto.DisplayMember = "NomGasto";
                cmbidGasto.SelectedIndex = 0;
            }

            //Personal Autoriza
            OperationResult resppa = new CatPersonalSvc().getCatPersonalxFiltro("  per.idPuesto in (1,2) AND per.idEmpresa = " + _usuario.personal.empresa.clave);
            if (resppa.typeResult == ResultTypes.success)
            {
                listAutoriza = (List<CatPersonal>)resppa.result;
                cmbAutoriza.DataSource = listAutoriza;
                cmbAutoriza.DisplayMember = "NombreCompleto";
                cmbAutoriza.SelectedIndex = 0;
            }
        }


        private void txtFolio_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                if (cmbidGasto.Enabled)
                {
                    cmbidGasto.Focus();
                }
                else if (dtpFechaFC.Enabled)
                {
                    dtpFechaFC.Focus();
                }
            }
        }

        private void dtpFechaFC_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                if (cmbAutoriza.Enabled)
                {
                    cmbAutoriza.Focus();
                }
                else if (txtNoDocumento.Enabled)
                {
                    txtNoDocumento.Focus();
                }
                else if (txtImporte.Enabled)
                {
                    txtImporte.Focus();
                }
            }

        }

        private void cmbAutoriza_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                if (txtNoDocumento.Enabled)
                {
                    txtNoDocumento.Focus();
                }
                else if (txtImporte.Enabled)
                {
                    txtImporte.Focus();
                }
            }


        }

        private void txtNoDocumento_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                if (txtImporte.Enabled)
                {
                    txtImporte.Focus();
                }
            }
        }

        private void txtImporte_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                if (txtIVA.Enabled)
                {
                    txtIVA.Focus();
                }
                else if (txtObservaciones.Enabled)
                {
                    txtObservaciones.Focus();
                }
                CalculaTotal(Convert.ToDecimal((txtImporte.Text == "" ? "0" : txtImporte.Text)), Convert.ToDecimal((txtIVA.Text == "" ? "0" : txtIVA.Text)));
            }
        }

        private void txtIVA_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                txtObservaciones.Focus();
                CalculaTotal(Convert.ToDecimal((txtImporte.Text == "" ? "0" : txtImporte.Text)), Convert.ToDecimal((txtIVA.Text == "" ? "0" : txtIVA.Text)));
            }
        }

        private void txtTotal_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                txtObservaciones.Focus();
            }
        }

        private void txtObservaciones_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                if ((folioxComp.Importe - folioxComp.ImporteComp) < (Convert.ToDecimal((txtImporte.Text == "" ? "0" : txtImporte.Text)) + Convert.ToDecimal((txtIVA.Text == "" ? "0" : txtIVA.Text))))
                {
                    MessageBox.Show("No se puede comprobar mas de la cuenta ", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtImporte.Focus();
                    txtImporte.SelectAll();
                    return;
                }
                Consecutivo = Consecutivo + 1;


                AgregarListaComp(0, txtNoDocumento.Text,
               ((CatGastos)cmbidGasto.SelectedValue).idGasto,
               dtpFechaFC.Value,
               Convert.ToDecimal((txtImporte.Text == "" ? "0" : txtImporte.Text)),
               Convert.ToDecimal((txtIVA.Text == "" ? "0" : txtIVA.Text)),
               gasto.Deducible,
               ((CatPersonal)cmbAutoriza.SelectedValue).idPersonal,
               txtObservaciones.Text, gasto.ReqComprobante, gasto.ReqAutorizacion,
               Convert.ToInt32(txtFolioDin.Text),
               ((CatGastos)cmbidGasto.SelectedValue).NomGasto, 
               Consecutivo, 0, _usuario.personal.empresa.clave);

                HabilitaBotonesMenuAlt(opcHabilita.Iniciando);
                LimpiaCamposCom();
                HabilitaCamposCOM(opcHabilita.DesHabilitaTodos);
                menu2.Items["btnMenu2Agre"].Select();
                //menu2.Focus();
            }
            //Guardar al Grid



        }

        private void AgregarListaComp(int Folio, string NoDocumento, int idGasto, DateTime Fecha,
        decimal Importe, decimal IVA, bool Deducible, int Autoriza, string Observaciones,
        bool ReqComprobante, bool ReqAutorizacion, int FolioDin, string NomGasto, 
        int Consecutivo, int idLiquidacion, int idEmpresa)
        {
            CompGastosFoliosDin comp = new CompGastosFoliosDin
            {
                Folio = Folio,
                NoDocumento = NoDocumento,
                idGasto = idGasto,
                NomGasto = NomGasto,
                Fecha = Fecha,
                Importe = Importe,
                IVA = IVA,
                Deducible = Deducible,
                Autoriza = Autoriza,
                Observaciones = Observaciones,
                ReqComprobante = ReqComprobante,
                ReqAutorizacion = ReqAutorizacion,
                FolioDin = FolioDin,
                Consecutivo = Consecutivo,
                idLiquidacion = idLiquidacion,
                idEmpresa = idEmpresa
            };
            if (listComGastos == null)
            {
                listComGastos = new List<CompGastosFoliosDin>();
            }

            listComGastos.Add(comp);
            grdComprobantes.DataSource = null;
            grdComprobantes.DataSource = listComGastos;
            ActualizaComprobado();
            FormatoGrid();
            //FormatoGridEncabezados();
        }

        private void CalculaTotal(decimal Importe, decimal IVA)
        {
            txtTotal.Text = (Importe + IVA).ToString();
        }

        private void FormatoGrid()
        {
            //ESTILO GENERAL
            grdComprobantes.RowsDefaultCellStyle.BackColor = Color.White;
            grdComprobantes.AlternatingRowsDefaultCellStyle.BackColor = Color.AliceBlue;

            grdComprobantes.RowsDefaultCellStyle.SelectionForeColor = Color.Yellow;
            grdComprobantes.RowsDefaultCellStyle.SelectionBackColor = Color.Gray;
            grdComprobantes.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            grdComprobantes.DefaultCellStyle.Font = new Font("Berlin Sans FB", 12);
            grdComprobantes.ColumnHeadersDefaultCellStyle.Font = new Font("Berlin Sans FB", 12);
            grdComprobantes.ColumnHeadersDefaultCellStyle.BackColor = Color.Yellow;
            grdComprobantes.ColumnHeadersDefaultCellStyle.ForeColor = Color.Red;
            grdComprobantes.EnableHeadersVisualStyles = false;

            //ESPECIFICO
            grdComprobantes.Columns["Folio"].Visible = false;
            grdComprobantes.Columns["idGasto"].Visible = false;
            grdComprobantes.Columns["Autoriza"].Visible = false;
            grdComprobantes.Columns["NomAutoriza"].Visible = false;
            grdComprobantes.Columns["Deducible"].Visible = false;
            grdComprobantes.Columns["ReqComprobante"].Visible = false;
            grdComprobantes.Columns["ReqAutorizacion"].Visible = false;
            grdComprobantes.Columns["FolioDin"].Visible = false;

            grdComprobantes.Columns["NomAutoriza"].HeaderText = "Autoriza";
            //grdComprobantes.Columns["idOperador"].HeaderText = "Operador";
            //grdComprobantes.Columns["NomOperador"].HeaderText = "Nombre";
            //grdComprobantes.Columns["NumViaje"].HeaderText = "# de Viajes";

            grdComprobantes.Columns["Importe"].DefaultCellStyle.Format = "C2";
            grdComprobantes.Columns["Importe"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight;
            grdComprobantes.Columns["IVA"].DefaultCellStyle.Format = "C2";
            grdComprobantes.Columns["IVA"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight;


            grdComprobantes.Columns["Consecutivo"].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
            grdComprobantes.Columns["NoDocumento"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            grdComprobantes.Columns["Fecha"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            grdComprobantes.Columns["Importe"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            grdComprobantes.Columns["IVA"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            //grdComprobantes.Columns["NomAutoriza"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            grdComprobantes.Columns["Observaciones"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }

        private void txtObservaciones_TextChanged(object sender, EventArgs e)
        {
        }

        private void ActualizaComprobado()
        {
            if (listComGastos != null)
            {
                tComprobado = 0;
                tSaldo = 0;
                foreach (var item in listComGastos)
                {
                    tComprobado = tComprobado + (item.Importe + item.IVA);
                }
                tSaldo = folioxComp.Importe - tComprobado;
                txtImporteCompFol.Text = tComprobado.ToString();
                txtSaldoFol.Text = tSaldo.ToString();

                folioxComp.ImporteComp = tComprobado;
            }
        }

        private void txtFolioDin_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnGrabar_Click(object sender, EventArgs e)
        {
            bool Grabado = true;
            caption = this.Name;
            buttons = MessageBoxButtons.OKCancel;
            mensaje = "Desea guardar las comprobaciones para " + txtNomTipo.Text  + " con Folio :" + txtFolioDin.Text;
            result = MessageBox.Show(mensaje, caption, buttons);
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                if (!Validar())
                {
                    return;
                }

                foreach (var item in listComGastos)
                {
                    CompGastosFoliosDin folio = item;
                    if (folio.Folio == 0)
                    {
                        OperationResult resp = new CompGastosFoliosDinSvc().GuardaCompGastosFoliosDin(ref folio);
                        if (resp.typeResult == ResultTypes.success)
                        {
                            Grabado = true;
                        }
                        else
                        {
                            Grabado = false;
                            break;
                        }
                    }
                }
                if(Grabado)
                {
                    OperationResult respfd = new FoliosDineroSvc().GuardaFoliosDinero(ref folioxComp);
                    if (respfd.typeResult == ResultTypes.success)
                    {
                        MessageBox.Show("Comprobaciones Agregadas Satisfactoriamente para "
                        + txtNomTipo.Text + " con Folio: " + txtFolioDin.Text, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        MandarImprimir(Convert.ToInt32(txtFolioDin.Text));
                        Cancelar();

                    }
                    else
                    {
                        //ocurrrio errro
                    }
                }
                //CompGastosFoliosDin folio = mapForm();

            }
        }
        private bool Validar()
        {
            bool bsinNuevos = false;
            if (listComGastos != null)
            {
                foreach (var item in listComGastos)
                {
                    if (item.Folio == 0)
                    {
                        bsinNuevos = true;
                    }
                }
                if (!bsinNuevos)
                {
                    MessageBox.Show("Debe tener Comprobaciones nuevas para guardar", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    menu2.Items["btnMenu2Agre"].Select();
                    return false;

                }
            }
            else
            {
                MessageBox.Show("Debe tener Comprobaciones para guardar", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                menu2.Items["btnMenu2Agre"].Select();
                return false;

            }
            return true;
        }

        private void grdComprobantes_Click(object sender, EventArgs e)
        {
            CargaDeListaComprobaciones(Convert.ToInt32(grdComprobantes.Rows[grdComprobantes.CurrentCell.RowIndex].Cells["Consecutivo"].Value));
        }

        private void CargaDeListaComprobaciones(int Consecutivo)
        {
            if (listComGastos != null)
            {
                foreach (var item in listComGastos)
                {
                    if (item.Consecutivo == Consecutivo)
                    {
                        if (item.Folio > 0)
                        {
                            txtFolio.Text = item.Folio.ToString();
                        }
                        else
                        {
                            txtFolio.Text = "";
                        }
                        txtConsecutivo.Text = Consecutivo.ToString();
                        SeleccionaComboTipoGasto(item.idGasto);
                        chkAutoriza.Checked = item.ReqAutorizacion;
                        SeleccionaComboAutoriza(item.Autoriza);
                        chkNoDocumento.Checked = item.ReqComprobante;
                        txtNoDocumento.Text = item.NoDocumento;
                        txtImporte.Text = item.Importe.ToString();
                        txtIVA.Text = item.IVA.ToString();
                        txtTotal.Text = (item.Importe + item.IVA).ToString();
                        txtObservaciones.Text = item.Observaciones;

                        HabilitaBotonesMenuAlt(opcHabilita.Editando);
                        grdComprobantes.Enabled = false;
                        break;
                    }
                }
            }
        }

        private void SeleccionaComboTipoGasto(int idGasto)
        {
            int i = 0;
            foreach (CatGastos item in cmbidGasto.Items)
            {
                if (item.idGasto == idGasto)
                {
                    cmbidGasto.SelectedIndex = i;
                    return;
                }
                else
                {
                    i++;
                }
            }
        }
        private void SeleccionaComboAutoriza(int Autoriza)
        {
            int i = 0;
            foreach (CatPersonal item in cmbAutoriza.Items)
            {
                if (item.idPersonal == Autoriza)
                {
                    cmbAutoriza.SelectedIndex = i;
                    return;
                }
                else
                {
                    i++;
                }
            }
        }

        private void menu2_KeyDown(object sender, KeyEventArgs e)
        {
            //if (menu2.Items["btnMenu2Agre"].Select.Visible =)
            //{

            //}
        }

        private void btnMenu2Can_Click(object sender, EventArgs e)
        {
            LimpiaCamposCom();
            grdComprobantes.Enabled = true;
            grdComprobantes.Focus();
        }

        private void CalculaResumen()
        {

        }

        private void btnReporte_Click(object sender, EventArgs e)
        {
            MandarImprimir(Convert.ToInt32(txtFolioDin.Text)  );
        }
        private void MandarImprimir(int FolioDin)
        {
            OperationResult resp = new CompGastosFoliosDinSvc().getCompGastosFoliosDinxId(0, " c.FolioDin = " + FolioDin);
            if (resp.typeResult == ResultTypes.success)
            {
                //CompGastosFoliosDin folio = ((List<CompGastosFoliosDin>)resp.result)[0];
                List<CompGastosFoliosDin> folio = (List<CompGastosFoliosDin>)resp.result;
                if (folioxComp.NumGuiaId > 0)
                {
                    OperationResult respv = new CabGuiaSvc().getCabGuiaxFilter(folioxComp.NumGuiaId);
                    if (respv.typeResult == ResultTypes.success)
                    {
                        viaje = ((List<CabGuia>)respv.result)[0];
                    }
                }
                
                ImprimirComprobacionGastos(folio, _usuario.personal, folioxComp, viaje, true, true);
            }
            else if (resp.typeResult == ResultTypes.recordNotFound)
            {
                //No Se encontro
            }
        }
        private void ImprimirComprobacionGastos(List<CompGastosFoliosDin> compgasto, Personal personal, FoliosDinero foldin,
            CabGuia viaje ,bool BandPreview, bool ActivaImprime)
        {
            try
            {
                // listaDetVenta;
                Boolean vBandHorizontal = false;
                Imprime.FuenteImprimeCampos = new System.Drawing.Font("Courrier New", 7, FontStyle.Bold);
                //CODIGO DE BARRAS
                //Imprime.FuenteImprimeCampos2 = new Font("Free 3 of 9 Extended", 40);
                Imprime.FuenteImprimeCampos2 = new Font("Free 3 of 9 Extended", 20);
                Imprime.FuenteImprimeCampos3 = new System.Drawing.Font("Courrier New", 3, FontStyle.Bold);
                Imprime.FuenteImprimeCampos4 = new System.Drawing.Font("Courrier New", 7, FontStyle.Bold);
                Imprime.FuenteImprimeCampos5 = new System.Drawing.Font("Courrier New", 4, FontStyle.Bold);

                Imprime.TablaImprime = Imprime.ArmaTablaTicket(compgasto, personal, foldin, viaje);

                prtSettings = new PrinterSettings();
                prtDoc = new PrintDocument();
                prtDoc.PrintPage += prt_PrintPage;
                prtDoc.PrinterSettings = prtSettings;
                prtDoc.DefaultPageSettings.Landscape = vBandHorizontal;
                prtDoc.DefaultPageSettings.PaperSize = prtSettings.DefaultPageSettings.PaperSize;

                if (BandPreview)
                {
                    PrintPreviewDialog prtPrev = new PrintPreviewDialog();
                    prtPrev.Document = prtDoc;
                    prtPrev.WindowState = FormWindowState.Maximized;
                    prtPrev.Document.DefaultPageSettings.Landscape = vBandHorizontal;
                    if (vBandHorizontal)
                    {
                        prtPrev.PrintPreviewControl.Zoom = 2;
                    }
                    else
                    {
                        prtPrev.PrintPreviewControl.Zoom = 2;
                    }

                    if (!ActivaImprime)
                    {
                        //DirectCast((prtPrev.Controls[1]), ToolStrip).Items.RemoveAt(0);
                        ((ToolStrip)(prtPrev.Controls[1])).Items[0].Enabled = false;
                    }

                    prtPrev.ShowDialog();
                }
                else
                {
                    prtDoc.Print();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrio un error al querer Imprimir la Comprobacion" +
                   Environment.NewLine + ex.Message, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        void prt_PrintPage(object sender, PrintPageEventArgs e)
        {
            try
            {
                Single yPos = e.MarginBounds.Top;
                Single leftMargin = e.MarginBounds.Left;
                Single LineHeight = printFont.GetHeight(e.Graphics);

                //////////////////////////////////////////////////////////////////////////////////////////
                //Imprime.imgLogo1 = Tools.getImage(Fletera2.Properties.Resources.Salida2);
                Imprime.imgLogo1 = Fletera2.Properties.Resources.Atlante;
                Imprime.TamanioimgLogo1 = new System.Drawing.Size(Imprime.imgLogo1.Width / 24, Imprime.imgLogo1.Height / 24);
                Imprime.PosicionimgLogo1 = new System.Drawing.Point(180, 3);

                if (ContPaginas == 0)
                {
                    yPos = Imprime.ImprimeLogo(e, lineaActual);
                }

                //Imprime.imgLogo2 = PtoVenta.UI.Properties.Resources.Report;
                //Imprime.TamanioimgLogo2 = new System.Drawing.Size(Imprime.imgLogo2.Width / 22, Imprime.imgLogo2.Height / 20);
                //Imprime.PosicionimgLogo2 = new System.Drawing.Point(210, 150);
                //////////////////////////////////////////////////////////////////////////////////////////
                yPos += LineHeight;
                do
                {
                    yPos += LineHeight;


                    yPos = Imprime.ImprimeLinea(e, lineaActual, ContPaginas + 1);


                    lineaActual += 1;

                } while (!(yPos >= e.MarginBounds.Bottom - 20 || lineaActual >= NumRegistros || ContPaginas == 2));

                ContPaginas += 1;
                if (ContPaginas > 2)
                {
                    ContPaginas = 0;
                    e.HasMorePages = false;
                }
                else
                {
                    e.HasMorePages = true;
                }
                //////////////////////////////////////////////////////////////////////////////////////////




            }

            catch (Exception ex)
            {
                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + ex.Message,
    this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
    }//
}
