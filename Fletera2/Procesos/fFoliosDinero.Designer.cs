﻿namespace Fletera2.Procesos
{
    partial class fFoliosDinero
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fFoliosDinero));
            this.ToolStripMenu = new System.Windows.Forms.ToolStrip();
            this.btnNuevo = new System.Windows.Forms.ToolStripButton();
            this.btnGrabar = new System.Windows.Forms.ToolStripButton();
            this.btnEditar = new System.Windows.Forms.ToolStripButton();
            this.btnReporte = new System.Windows.Forms.ToolStripButton();
            this.btnCancelar = new System.Windows.Forms.ToolStripButton();
            this.btnSalir = new System.Windows.Forms.ToolStripButton();
            this.gpoDatos = new System.Windows.Forms.GroupBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtImporteFP = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtFolioDinPadre = new System.Windows.Forms.TextBox();
            this.cmbidTipoFolDin = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cmbidEmpresa = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtConcepto = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtEstatus = new System.Windows.Forms.TextBox();
            this.cmbidEmpleadoRec = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbidEmpleadoEnt = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.dtpFechaHora = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpFecha = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.txtFolioDin = new System.Windows.Forms.TextBox();
            this.gpoComprobantes = new System.Windows.Forms.GroupBox();
            this.grdComprobantes = new System.Windows.Forms.DataGridView();
            this.gpoImportes = new System.Windows.Forms.GroupBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtxComprobar = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtxEntregar = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtImporteComp = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtImporteEnt = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtImporte = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtRemolque2 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtDolly = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtRemolque1 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtTractor = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtNumGuiaId = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtidLiquidacion = new System.Windows.Forms.TextBox();
            this.ToolStripMenu.SuspendLayout();
            this.gpoDatos.SuspendLayout();
            this.gpoComprobantes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdComprobantes)).BeginInit();
            this.gpoImportes.SuspendLayout();
            this.SuspendLayout();
            // 
            // ToolStripMenu
            // 
            this.ToolStripMenu.AutoSize = false;
            this.ToolStripMenu.BackColor = System.Drawing.SystemColors.Control;
            this.ToolStripMenu.Font = new System.Drawing.Font("Berlin Sans FB", 11.25F);
            this.ToolStripMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnNuevo,
            this.btnGrabar,
            this.btnEditar,
            this.btnReporte,
            this.btnCancelar,
            this.btnSalir});
            this.ToolStripMenu.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.ToolStripMenu.Location = new System.Drawing.Point(0, 0);
            this.ToolStripMenu.Name = "ToolStripMenu";
            this.ToolStripMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.ToolStripMenu.Size = new System.Drawing.Size(902, 65);
            this.ToolStripMenu.TabIndex = 8;
            this.ToolStripMenu.Text = "ToolStrip1";
            // 
            // btnNuevo
            // 
            this.btnNuevo.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnNuevo.Image = ((System.Drawing.Image)(resources.GetObject("btnNuevo.Image")));
            this.btnNuevo.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnNuevo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(54, 62);
            this.btnNuevo.Text = "&Nuevo";
            this.btnNuevo.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnNuevo.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnNuevo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnNuevo.Click += new System.EventHandler(this.btnNuevo_Click);
            // 
            // btnGrabar
            // 
            this.btnGrabar.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnGrabar.Image = ((System.Drawing.Image)(resources.GetObject("btnGrabar.Image")));
            this.btnGrabar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnGrabar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnGrabar.Name = "btnGrabar";
            this.btnGrabar.Size = new System.Drawing.Size(60, 62);
            this.btnGrabar.Text = "&Grabar";
            this.btnGrabar.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnGrabar.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnGrabar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnGrabar.Click += new System.EventHandler(this.btnGrabar_Click);
            // 
            // btnEditar
            // 
            this.btnEditar.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnEditar.Image = global::Fletera2.Properties.Resources.Pegaso;
            this.btnEditar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnEditar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(52, 62);
            this.btnEditar.Text = "&Editar";
            this.btnEditar.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnEditar.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnEditar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnEditar.Visible = false;
            this.btnEditar.Click += new System.EventHandler(this.btnEditar_Click);
            // 
            // btnReporte
            // 
            this.btnReporte.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnReporte.Image = ((System.Drawing.Image)(resources.GetObject("btnReporte.Image")));
            this.btnReporte.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnReporte.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnReporte.Name = "btnReporte";
            this.btnReporte.Size = new System.Drawing.Size(92, 62);
            this.btnReporte.Text = "&ReImpresion";
            this.btnReporte.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnReporte.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnReporte.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnReporte.Click += new System.EventHandler(this.btnReporte_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnCancelar.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar.Image")));
            this.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnCancelar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(69, 62);
            this.btnCancelar.Text = "&Cancelar";
            this.btnCancelar.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnCancelar.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnSalir.Image = ((System.Drawing.Image)(resources.GetObject("btnSalir.Image")));
            this.btnSalir.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnSalir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(55, 62);
            this.btnSalir.Text = "&S a l i r";
            this.btnSalir.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnSalir.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // gpoDatos
            // 
            this.gpoDatos.Controls.Add(this.label16);
            this.gpoDatos.Controls.Add(this.txtRemolque2);
            this.gpoDatos.Controls.Add(this.label17);
            this.gpoDatos.Controls.Add(this.txtDolly);
            this.gpoDatos.Controls.Add(this.label18);
            this.gpoDatos.Controls.Add(this.txtRemolque1);
            this.gpoDatos.Controls.Add(this.label19);
            this.gpoDatos.Controls.Add(this.txtTractor);
            this.gpoDatos.Controls.Add(this.label20);
            this.gpoDatos.Controls.Add(this.txtNumGuiaId);
            this.gpoDatos.Controls.Add(this.label15);
            this.gpoDatos.Controls.Add(this.txtImporteFP);
            this.gpoDatos.Controls.Add(this.label14);
            this.gpoDatos.Controls.Add(this.txtFolioDinPadre);
            this.gpoDatos.Controls.Add(this.cmbidTipoFolDin);
            this.gpoDatos.Controls.Add(this.label8);
            this.gpoDatos.Controls.Add(this.cmbidEmpresa);
            this.gpoDatos.Controls.Add(this.label7);
            this.gpoDatos.Controls.Add(this.label6);
            this.gpoDatos.Controls.Add(this.txtConcepto);
            this.gpoDatos.Controls.Add(this.label5);
            this.gpoDatos.Controls.Add(this.txtEstatus);
            this.gpoDatos.Controls.Add(this.cmbidEmpleadoRec);
            this.gpoDatos.Controls.Add(this.label4);
            this.gpoDatos.Controls.Add(this.cmbidEmpleadoEnt);
            this.gpoDatos.Controls.Add(this.label3);
            this.gpoDatos.Controls.Add(this.dtpFechaHora);
            this.gpoDatos.Controls.Add(this.label2);
            this.gpoDatos.Controls.Add(this.dtpFecha);
            this.gpoDatos.Controls.Add(this.label1);
            this.gpoDatos.Controls.Add(this.txtFolioDin);
            this.gpoDatos.Location = new System.Drawing.Point(3, 65);
            this.gpoDatos.Name = "gpoDatos";
            this.gpoDatos.Size = new System.Drawing.Size(610, 267);
            this.gpoDatos.TabIndex = 9;
            this.gpoDatos.TabStop = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Blue;
            this.label15.Location = new System.Drawing.Point(397, 51);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(74, 18);
            this.label15.TabIndex = 30;
            this.label15.Text = "Importe: $";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtImporteFP
            // 
            this.txtImporteFP.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtImporteFP.Location = new System.Drawing.Point(480, 48);
            this.txtImporteFP.Name = "txtImporteFP";
            this.txtImporteFP.Size = new System.Drawing.Size(123, 25);
            this.txtImporteFP.TabIndex = 29;
            this.txtImporteFP.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Blue;
            this.label14.Location = new System.Drawing.Point(374, 20);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(98, 18);
            this.label14.TabIndex = 22;
            this.label14.Text = "Folio PADRE:";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtFolioDinPadre
            // 
            this.txtFolioDinPadre.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtFolioDinPadre.Location = new System.Drawing.Point(480, 17);
            this.txtFolioDinPadre.Name = "txtFolioDinPadre";
            this.txtFolioDinPadre.Size = new System.Drawing.Size(123, 25);
            this.txtFolioDinPadre.TabIndex = 21;
            this.txtFolioDinPadre.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFolioDinPadre.TextChanged += new System.EventHandler(this.txtFolioDinPadre_TextChanged);
            this.txtFolioDinPadre.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFolioDinPadre_KeyPress);
            // 
            // cmbidTipoFolDin
            // 
            this.cmbidTipoFolDin.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.cmbidTipoFolDin.FormattingEnabled = true;
            this.cmbidTipoFolDin.Location = new System.Drawing.Point(84, 112);
            this.cmbidTipoFolDin.Name = "cmbidTipoFolDin";
            this.cmbidTipoFolDin.Size = new System.Drawing.Size(238, 26);
            this.cmbidTipoFolDin.TabIndex = 20;
            this.cmbidTipoFolDin.SelectedIndexChanged += new System.EventHandler(this.cmbidTipoFolDin_SelectedIndexChanged);
            this.cmbidTipoFolDin.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbidTipoFolDin_KeyPress);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Blue;
            this.label8.Location = new System.Drawing.Point(5, 114);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(40, 18);
            this.label8.TabIndex = 19;
            this.label8.Text = "Tipo:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cmbidEmpresa
            // 
            this.cmbidEmpresa.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.cmbidEmpresa.FormattingEnabled = true;
            this.cmbidEmpresa.Location = new System.Drawing.Point(84, 17);
            this.cmbidEmpresa.Name = "cmbidEmpresa";
            this.cmbidEmpresa.Size = new System.Drawing.Size(208, 26);
            this.cmbidEmpresa.TabIndex = 18;
            this.cmbidEmpresa.SelectedIndexChanged += new System.EventHandler(this.cmbidEmpresa_SelectedIndexChanged);
            this.cmbidEmpresa.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbidEmpresa_KeyPress);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Blue;
            this.label7.Location = new System.Drawing.Point(5, 20);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(70, 18);
            this.label7.TabIndex = 17;
            this.label7.Text = "Empresa:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Blue;
            this.label6.Location = new System.Drawing.Point(5, 239);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(73, 18);
            this.label6.TabIndex = 16;
            this.label6.Text = "Concepto:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // txtConcepto
            // 
            this.txtConcepto.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtConcepto.Location = new System.Drawing.Point(84, 236);
            this.txtConcepto.MaxLength = 100;
            this.txtConcepto.Name = "txtConcepto";
            this.txtConcepto.Size = new System.Drawing.Size(366, 25);
            this.txtConcepto.TabIndex = 15;
            this.txtConcepto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtConcepto_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Blue;
            this.label5.Location = new System.Drawing.Point(465, 242);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 18);
            this.label5.TabIndex = 14;
            this.label5.Text = "Estatus:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtEstatus
            // 
            this.txtEstatus.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtEstatus.Location = new System.Drawing.Point(535, 239);
            this.txtEstatus.Name = "txtEstatus";
            this.txtEstatus.Size = new System.Drawing.Size(68, 25);
            this.txtEstatus.TabIndex = 13;
            // 
            // cmbidEmpleadoRec
            // 
            this.cmbidEmpleadoRec.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.cmbidEmpleadoRec.FormattingEnabled = true;
            this.cmbidEmpleadoRec.Location = new System.Drawing.Point(83, 204);
            this.cmbidEmpleadoRec.Name = "cmbidEmpleadoRec";
            this.cmbidEmpleadoRec.Size = new System.Drawing.Size(367, 26);
            this.cmbidEmpleadoRec.TabIndex = 12;
            this.cmbidEmpleadoRec.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbidEmpleadoRec_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Blue;
            this.label4.Location = new System.Drawing.Point(9, 207);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 18);
            this.label4.TabIndex = 11;
            this.label4.Text = "Recibe:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cmbidEmpleadoEnt
            // 
            this.cmbidEmpleadoEnt.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.cmbidEmpleadoEnt.FormattingEnabled = true;
            this.cmbidEmpleadoEnt.Location = new System.Drawing.Point(84, 173);
            this.cmbidEmpleadoEnt.Name = "cmbidEmpleadoEnt";
            this.cmbidEmpleadoEnt.Size = new System.Drawing.Size(366, 26);
            this.cmbidEmpleadoEnt.TabIndex = 10;
            this.cmbidEmpleadoEnt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbidEmpleadoEnt_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Blue;
            this.label3.Location = new System.Drawing.Point(5, 176);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 18);
            this.label3.TabIndex = 9;
            this.label3.Text = "Entrega:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dtpFechaHora
            // 
            this.dtpFechaHora.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.dtpFechaHora.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpFechaHora.Location = new System.Drawing.Point(353, 79);
            this.dtpFechaHora.Name = "dtpFechaHora";
            this.dtpFechaHora.Size = new System.Drawing.Size(106, 25);
            this.dtpFechaHora.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Blue;
            this.label2.Location = new System.Drawing.Point(6, 84);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 18);
            this.label2.TabIndex = 7;
            this.label2.Text = "Fecha:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dtpFecha
            // 
            this.dtpFecha.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.dtpFecha.Location = new System.Drawing.Point(84, 79);
            this.dtpFecha.Name = "dtpFecha";
            this.dtpFecha.Size = new System.Drawing.Size(263, 25);
            this.dtpFecha.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(5, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 18);
            this.label1.TabIndex = 5;
            this.label1.Text = "FOLIO:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtFolioDin
            // 
            this.txtFolioDin.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtFolioDin.Location = new System.Drawing.Point(84, 48);
            this.txtFolioDin.Name = "txtFolioDin";
            this.txtFolioDin.Size = new System.Drawing.Size(123, 25);
            this.txtFolioDin.TabIndex = 4;
            this.txtFolioDin.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFolioDin.TextChanged += new System.EventHandler(this.txtFolioDin_TextChanged);
            this.txtFolioDin.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFolioDin_KeyPress);
            // 
            // gpoComprobantes
            // 
            this.gpoComprobantes.Controls.Add(this.grdComprobantes);
            this.gpoComprobantes.Location = new System.Drawing.Point(0, 338);
            this.gpoComprobantes.Name = "gpoComprobantes";
            this.gpoComprobantes.Size = new System.Drawing.Size(883, 185);
            this.gpoComprobantes.TabIndex = 10;
            this.gpoComprobantes.TabStop = false;
            this.gpoComprobantes.Text = "Comprobantes";
            // 
            // grdComprobantes
            // 
            this.grdComprobantes.AllowUserToAddRows = false;
            this.grdComprobantes.AllowUserToDeleteRows = false;
            this.grdComprobantes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdComprobantes.Location = new System.Drawing.Point(21, 21);
            this.grdComprobantes.Name = "grdComprobantes";
            this.grdComprobantes.Size = new System.Drawing.Size(840, 143);
            this.grdComprobantes.TabIndex = 93;
            // 
            // gpoImportes
            // 
            this.gpoImportes.Controls.Add(this.label21);
            this.gpoImportes.Controls.Add(this.txtidLiquidacion);
            this.gpoImportes.Controls.Add(this.label13);
            this.gpoImportes.Controls.Add(this.txtxComprobar);
            this.gpoImportes.Controls.Add(this.label12);
            this.gpoImportes.Controls.Add(this.txtxEntregar);
            this.gpoImportes.Controls.Add(this.label11);
            this.gpoImportes.Controls.Add(this.txtImporteComp);
            this.gpoImportes.Controls.Add(this.label10);
            this.gpoImportes.Controls.Add(this.txtImporteEnt);
            this.gpoImportes.Controls.Add(this.label9);
            this.gpoImportes.Controls.Add(this.txtImporte);
            this.gpoImportes.Location = new System.Drawing.Point(619, 70);
            this.gpoImportes.Name = "gpoImportes";
            this.gpoImportes.Size = new System.Drawing.Size(267, 236);
            this.gpoImportes.TabIndex = 11;
            this.gpoImportes.TabStop = false;
            this.gpoImportes.Text = "Importes";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Blue;
            this.label13.Location = new System.Drawing.Point(7, 168);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(125, 18);
            this.label13.TabIndex = 36;
            this.label13.Text = "Por Comprobar: $";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtxComprobar
            // 
            this.txtxComprobar.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtxComprobar.Location = new System.Drawing.Point(138, 165);
            this.txtxComprobar.Name = "txtxComprobar";
            this.txtxComprobar.Size = new System.Drawing.Size(123, 25);
            this.txtxComprobar.TabIndex = 35;
            this.txtxComprobar.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Blue;
            this.label12.Location = new System.Drawing.Point(19, 108);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(107, 18);
            this.label12.TabIndex = 34;
            this.label12.Text = "Por Entregar: $";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtxEntregar
            // 
            this.txtxEntregar.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtxEntregar.Location = new System.Drawing.Point(138, 105);
            this.txtxEntregar.Name = "txtxEntregar";
            this.txtxEntregar.Size = new System.Drawing.Size(123, 25);
            this.txtxEntregar.TabIndex = 33;
            this.txtxEntregar.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Blue;
            this.label11.Location = new System.Drawing.Point(19, 139);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(110, 18);
            this.label11.TabIndex = 32;
            this.label11.Text = "Comprobado: $";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtImporteComp
            // 
            this.txtImporteComp.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtImporteComp.Location = new System.Drawing.Point(138, 136);
            this.txtImporteComp.Name = "txtImporteComp";
            this.txtImporteComp.Size = new System.Drawing.Size(123, 25);
            this.txtImporteComp.TabIndex = 31;
            this.txtImporteComp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Blue;
            this.label10.Location = new System.Drawing.Point(34, 77);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(96, 18);
            this.label10.TabIndex = 30;
            this.label10.Text = "Entregado: $ ";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtImporteEnt
            // 
            this.txtImporteEnt.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtImporteEnt.Location = new System.Drawing.Point(138, 74);
            this.txtImporteEnt.Name = "txtImporteEnt";
            this.txtImporteEnt.Size = new System.Drawing.Size(123, 25);
            this.txtImporteEnt.TabIndex = 29;
            this.txtImporteEnt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Blue;
            this.label9.Location = new System.Drawing.Point(55, 46);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(74, 18);
            this.label9.TabIndex = 28;
            this.label9.Text = "Importe: $";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label9.Click += new System.EventHandler(this.label9_Click);
            // 
            // txtImporte
            // 
            this.txtImporte.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtImporte.Location = new System.Drawing.Point(138, 43);
            this.txtImporte.Name = "txtImporte";
            this.txtImporte.Size = new System.Drawing.Size(123, 25);
            this.txtImporte.TabIndex = 27;
            this.txtImporte.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtImporte.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtImporte_KeyPress);
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Blue;
            this.label16.Location = new System.Drawing.Point(528, 121);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(75, 18);
            this.label16.TabIndex = 62;
            this.label16.Text = "Rem. 2";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtRemolque2
            // 
            this.txtRemolque2.Enabled = false;
            this.txtRemolque2.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtRemolque2.Location = new System.Drawing.Point(528, 142);
            this.txtRemolque2.Name = "txtRemolque2";
            this.txtRemolque2.Size = new System.Drawing.Size(75, 25);
            this.txtRemolque2.TabIndex = 61;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Blue;
            this.label17.Location = new System.Drawing.Point(447, 121);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(75, 18);
            this.label17.TabIndex = 60;
            this.label17.Text = "Dolly";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtDolly
            // 
            this.txtDolly.Enabled = false;
            this.txtDolly.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtDolly.Location = new System.Drawing.Point(447, 142);
            this.txtDolly.Name = "txtDolly";
            this.txtDolly.Size = new System.Drawing.Size(75, 25);
            this.txtDolly.TabIndex = 59;
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Blue;
            this.label18.Location = new System.Drawing.Point(366, 121);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(75, 18);
            this.label18.TabIndex = 58;
            this.label18.Text = "Rem. 1";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtRemolque1
            // 
            this.txtRemolque1.Enabled = false;
            this.txtRemolque1.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtRemolque1.Location = new System.Drawing.Point(366, 142);
            this.txtRemolque1.Name = "txtRemolque1";
            this.txtRemolque1.Size = new System.Drawing.Size(75, 25);
            this.txtRemolque1.TabIndex = 57;
            // 
            // label19
            // 
            this.label19.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Blue;
            this.label19.Location = new System.Drawing.Point(217, 146);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(62, 18);
            this.label19.TabIndex = 56;
            this.label19.Text = "Tractor";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtTractor
            // 
            this.txtTractor.Enabled = false;
            this.txtTractor.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtTractor.Location = new System.Drawing.Point(285, 142);
            this.txtTractor.Name = "txtTractor";
            this.txtTractor.Size = new System.Drawing.Size(75, 25);
            this.txtTractor.TabIndex = 55;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Blue;
            this.label20.Location = new System.Drawing.Point(5, 145);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(67, 18);
            this.label20.TabIndex = 54;
            this.label20.Text = "No. Viaje";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtNumGuiaId
            // 
            this.txtNumGuiaId.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtNumGuiaId.Location = new System.Drawing.Point(83, 142);
            this.txtNumGuiaId.Name = "txtNumGuiaId";
            this.txtNumGuiaId.Size = new System.Drawing.Size(123, 25);
            this.txtNumGuiaId.TabIndex = 53;
            this.txtNumGuiaId.TextChanged += new System.EventHandler(this.txtNumGuiaId_TextChanged);
            this.txtNumGuiaId.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNumGuiaId_KeyPress);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Blue;
            this.label21.Location = new System.Drawing.Point(26, 15);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(106, 18);
            this.label21.TabIndex = 38;
            this.label21.Text = "LIQUIDACION:";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtidLiquidacion
            // 
            this.txtidLiquidacion.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtidLiquidacion.Location = new System.Drawing.Point(138, 13);
            this.txtidLiquidacion.Name = "txtidLiquidacion";
            this.txtidLiquidacion.Size = new System.Drawing.Size(123, 25);
            this.txtidLiquidacion.TabIndex = 37;
            this.txtidLiquidacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // fFoliosDinero
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(902, 535);
            this.Controls.Add(this.gpoImportes);
            this.Controls.Add(this.gpoComprobantes);
            this.Controls.Add(this.gpoDatos);
            this.Controls.Add(this.ToolStripMenu);
            this.Name = "fFoliosDinero";
            this.Text = "Entrega Dinero";
            this.Load += new System.EventHandler(this.fFoliosDinero_Load);
            this.ToolStripMenu.ResumeLayout(false);
            this.ToolStripMenu.PerformLayout();
            this.gpoDatos.ResumeLayout(false);
            this.gpoDatos.PerformLayout();
            this.gpoComprobantes.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdComprobantes)).EndInit();
            this.gpoImportes.ResumeLayout(false);
            this.gpoImportes.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.ToolStrip ToolStripMenu;
        private System.Windows.Forms.ToolStripButton btnNuevo;
        private System.Windows.Forms.ToolStripButton btnGrabar;
        private System.Windows.Forms.ToolStripButton btnEditar;
        private System.Windows.Forms.ToolStripButton btnReporte;
        private System.Windows.Forms.ToolStripButton btnCancelar;
        private System.Windows.Forms.ToolStripButton btnSalir;
        private System.Windows.Forms.GroupBox gpoDatos;
        private System.Windows.Forms.GroupBox gpoComprobantes;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtFolioDin;
        private System.Windows.Forms.DateTimePicker dtpFechaHora;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpFecha;
        private System.Windows.Forms.ComboBox cmbidTipoFolDin;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cmbidEmpresa;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtConcepto;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtEstatus;
        private System.Windows.Forms.ComboBox cmbidEmpleadoRec;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbidEmpleadoEnt;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox gpoImportes;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtxComprobar;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtxEntregar;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtImporteComp;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtImporteEnt;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtImporte;
        private System.Windows.Forms.DataGridView grdComprobantes;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtFolioDinPadre;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtImporteFP;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtRemolque2;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtDolly;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtRemolque1;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtTractor;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtNumGuiaId;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtidLiquidacion;
    }
}