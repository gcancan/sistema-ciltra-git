﻿using Core.Models;
using Fletera2.Clases;
using Fletera2Entidades;
using Fletera2Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Fletera2.Clases.Inicio;

namespace Fletera2.Procesos
{
    public partial class fMovllantas : Form
    {
        //variables
        opcForma opcionForma;
        Usuario _Usuario;
        CatLlantas llanta;
        List<CatLugares> listaLugares;
        string _idLlanta;
        OperationResult resp;
        int _idEnum;

        List<CatLlantas> listaLlantas;

        List<CatLlantas> listaLlantasDes;
        CatLLantasProducto llantaProd;

        List<CatUnidadTrans> listaUnidades;
        CatUnidadTrans UnidadTransp;

        List<CatAlmacen> listaAlmacen;
        //CatAlmacen Almacen;

        int _idOrdenTrabajo;
        opServLlantas opcionAct;

        List<MovLLantas> listaMovLLantas;
        MovLLantas MovLlanta;

        List<CatLlantas> listaLlantasGraba;
        CatLlantas llantaGraba;

        List<LlantasHis> listaHistoria;
        LlantasHis llantaHis;
        public fMovllantas(Usuario User, string idLlanta = "", int idEnum = 0, int idOrdenTrabajo = 0)
        {
            _Usuario = User;
            _idLlanta = idLlanta;
            _idEnum = idEnum;
            _idOrdenTrabajo = idOrdenTrabajo;
            InitializeComponent();
        }

        private void fMovllantas_Load(object sender, EventArgs e)
        {
            Cancelar();
            CargaCombos();
            if (_idLlanta != "")
            {
                opcionForma = opcForma.Utilizar;
                HabilitaBotones(opcHabilita.Nuevo);
                HabilitaCampos(opcHabilita.Nuevo);

                //Carga Llanta para DESTINO
                CargaForma(opcBusqueda.CatLLantas, _idLlanta, eOpcionLlantas.Origen);
                cmbActividades.SelectedValue = _idEnum;
                txtidLlanta.Enabled = false;
                cmbActividades.Enabled = false;

                CargaxActividad((opServLlantas)_idEnum);

            }
            else
            {
                AutoAcompletarTexto(txtidLlanta, opcBusqueda.CatLLantas);
            }
        }

        private void CargaCombos()
        {
            AutoCompletarCombo(cmbActividades, opcBusqueda.opServLlantas);
            AutoCompletarCombo(cmbidtipoUbicacion, opcBusqueda.CatLugares);
            AutoCompletarCombo(cmbidtipoUbicacionDES, opcBusqueda.CatLugares);
        }

        private void Cancelar()
        {
            HabilitaBotones(opcHabilita.DesHabilitaTodos);
            HabilitaCampos(opcHabilita.DesHabilitaTodos);
            LimpiaCampos();
            LimpiaCamposCaracteristicas();
        }
        private void HabilitaBotones(opcHabilita opc)
        {
            try
            {
                switch (opc)
                {
                    case opcHabilita.DesHabilitaTodos:
                        btnNuevo.Enabled = true;
                        btnGrabar.Enabled = false;
                        btnCancelar.Enabled = false;
                        btnSalir.Enabled = true;
                        break;
                    case opcHabilita.Iniciando:
                        break;
                    case opcHabilita.Editando:
                        break;
                    case opcHabilita.Nuevo:
                        btnNuevo.Enabled = false;
                        btnGrabar.Enabled = true;
                        btnCancelar.Enabled = true;
                        btnSalir.Enabled = false;

                        break;
                    case opcHabilita.Reporte:
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + ex.Message,
                this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void HabilitaCampos(opcHabilita opc)
        {
            try
            {
                switch (opc)
                {
                    case opcHabilita.DesHabilitaTodos:
                        gpoMovimientos.Enabled = false;
                        gpoOrigen.Enabled = false;
                        gpoDestino.Enabled = false;
                        gpoCaract.Enabled = false;
                        break;
                    case opcHabilita.Iniciando:
                        gpoMovimientos.Enabled = true;
                        cmbActividades.Enabled = false;
                        gpoOrigen.Enabled = false;
                        gpoDestino.Enabled = false;
                        gpoCaract.Enabled = false;

                        break;
                    case opcHabilita.Editando:
                        break;
                    case opcHabilita.Nuevo:
                        gpoMovimientos.Enabled = true;
                        gpoOrigen.Enabled = false;
                        gpoDestino.Enabled = true;
                        gpoCaract.Enabled = false;

                        break;
                    case opcHabilita.Reporte:
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + ex.Message,
                this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            }
        }
        private void LimpiaCampos()
        {
            txtidLlanta.Clear();
            if (cmbActividades.Items.Count > 0)
            {
                cmbActividades.SelectedIndex = 0;
            }
            if (cmbidtipoUbicacion.Items.Count > 0)
            {
                cmbidtipoUbicacion.SelectedIndex = 0;
            }
            if (cmbidUbicacion.Items.Count > 0)
            {
                cmbidUbicacion.SelectedIndex = 0;
            }



            txtTipoUni.Clear();
            txtProfundidadAct.Clear();

            txtProfundidadFINAL.Clear();
            txtPosicion.Value = 0;
            if (cmbidtipoUbicacionDES.Items.Count > 0)
            {
                cmbidtipoUbicacionDES.SelectedIndex = 0;
            }
            if (cmbidUbicacionDES.Items.Count > 0)
            {
                cmbidUbicacionDES.SelectedIndex = 0;
            }
            txtTipoUniDES.Clear();
            txtProfundidadDES.Clear();
            txtProfundidadFINAL_DES.Clear();
            //txtPosicionDES.Value = 0;


        }
        private void LimpiaCamposCaracteristicas()
        {
            txtidProductoLlanta.Clear();
            txtidMarca.Clear();
            txtMedida.Clear();
            txtidTipoLLanta.Clear();
            txtidDisenio.Clear();
            txtProfundidad.Clear();
            txtidCondicion.Clear();
            txtidDisenioRev.Clear();

        }
        private void CargaForma(opcBusqueda opc, string Valor, eOpcionLlantas opcllanta)
        {
            try
            {
                if (opc == opcBusqueda.CatLLantas)
                {
                    resp = new CatLlantasSvc().getCatLlantasxFilter(0, "c.idLlanta = '" + Valor + "'");
                    if (resp.typeResult == ResultTypes.success)
                    {
                        llanta = ((List<CatLlantas>)resp.result)[0];
                        if (opcllanta == eOpcionLlantas.Origen)
                        {
                            txtidLlanta.Text = llanta.idLlanta;
                            //para caracteristicas - solo origen
                            txtidProductoLlanta.Text = llanta.idProductoLlanta.ToString();
                            resp = new CatLLantasProductoSvc().getCatLLantasProductoxFilter(llanta.idProductoLlanta);
                            if (resp.typeResult == ResultTypes.success)
                            {
                                llantaProd = ((List<CatLLantasProducto>)resp.result)[0];
                                txtidMarca.Text = llantaProd.NombreMarca;
                                txtMedida.Text = llantaProd.Medida;
                                txtidTipoLLanta.Text = llanta.NomTipoLlanta;
                                txtidDisenio.Text = llantaProd.Descripcion;
                                txtProfundidad.Text = llantaProd.Profundidad.ToString("N2");
                                txtidCondicion.Text = llanta.NomCondicion;
                                chkActivo.Checked = llantaProd.Activo;
                                txtidDisenioRev.Text = llanta.NomDisenioRev;

                                //ORIGEN
                                SeleccionacmbidtipoUbicacion(llanta.idtipoUbicacion);
                                //llena combo cmbidUbicacion

                                CargaComboUbicaciones(cmbidUbicacion, (opLugarLlantas)llanta.idtipoUbicacion, eOpcionLlantas.Origen, llanta.idUbicacion);

                                txtProfundidadAct.Text = llanta.ProfundidadAct.ToString();
                                txtPosicion.Value = llanta.Posicion;
                                //ORIGEN

                            }
                            //para caracteristicas - solo origen
                        }
                        else if (opcllanta == eOpcionLlantas.Destino)
                        {

                        }









                    }

                    if (llanta != null)
                    {

                        //origen
                        //                    (tipoub)cmbidtipoUbicacion
                        //cmbidUbicacion

                        //txtTipoUni
                        //txtProfundidadAct

                        //txtProfundidadFINAL
                        //txtPosicion
                    }
                    else
                    {
                        //buscar
                    }
                }
                else if (opc == opcBusqueda.CatUnidadTrans)
                {
                    resp = new CatUnidadTransSvc().geCatUnidadTransxFilter(Valor);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        UnidadTransp = ((List<CatUnidadTrans>)resp.result)[0];

                        switch (opcllanta)
                        {
                            case eOpcionLlantas.Origen:
                                break;
                            case eOpcionLlantas.Destino:
                                txtTipoUniDES.Text = UnidadTransp.nomTipoUniTras;
                                //txtProfundidadDES

                                //txtProfundidadFINAL_DES
                                //txtPosicionDES
                                break;
                            default:
                                break;
                        }
                    }
                }

            }
            catch
            {

            }
        }

        private void AutoCompletarCombo(ComboBox Control, opcBusqueda opc, string filtro = "", string OrderBy = "")
        {
            OperationResult resp = new OperationResult();
            //AutoCompleteStringCollection coleccion = new AutoCompleteStringCollection();
            //Servicios a partir de enum


            if (opc == opcBusqueda.CatLugares)
            {
                resp = new CatLugaresSvc().getCatLugaresxFilter(0, filtro, OrderBy);
            }
            else if (opc == opcBusqueda.opServLlantas)
            {
                //Servicios a partir de enum
                DataTable dt = new System.Data.DataTable();
                Tools obj = new Tools();
                dt = obj.EnumToDataTable2(typeof(opServLlantas));
                cmbActividades.DataSource = dt;
                cmbActividades.DisplayMember = "VALUE";
                cmbActividades.ValueMember = "KEY";
                //Servicios a partir de enum
            }
            if (resp.typeResult == ResultTypes.success)
            {
                if (opc == opcBusqueda.CatLugares)
                {
                    listaLugares = (List<CatLugares>)resp.result;
                    Control.DataSource = null;
                    Control.DataSource = listaLugares;

                    Control.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                    Control.AutoCompleteSource = AutoCompleteSource.ListItems;
                    Control.DisplayMember = "Descripcion";
                    Control.SelectedIndex = 0;


                }

            }

        }

        private void CargaComboUbicaciones(ComboBox combo, opLugarLlantas opc, eOpcionLlantas opcllanta, string Valor = "")
        {
            try
            {
                switch (opc)
                {
                    case opLugarLlantas.ALMACEN:
                        resp = new CatAlmacenSvc().getCatAlmacenxFilter(0,"", "OrdenLlanta ");
                        if (resp.typeResult == ResultTypes.success)
                        {
                            listaAlmacen = (List<CatAlmacen>)resp.result;
                            combo.DataSource = null;
                            combo.DataSource = listaAlmacen;
                            combo.DisplayMember = "NomAlmacen";
                            if (Valor != "")
                            {
                                SeleccionaAlmacen(Convert.ToInt32(Valor));
                            }


                        }
                        break;
                    case opLugarLlantas.PROVEEDOR:
                        break;
                    case opLugarLlantas.UNIDADES_TRANS:
                        resp = new CatUnidadTransSvc().geCatUnidadTransxFilter();
                        if (resp.typeResult == ResultTypes.success)
                        {
                            listaUnidades = (List<CatUnidadTrans>)resp.result;
                            combo.DataSource = null;
                            combo.DataSource = listaUnidades;
                            combo.DisplayMember = "idUnidadTrans";
                            if (Valor != "")
                            {
                                SeleccionaUnidadTrans(Valor, opcllanta);
                                resp = new CatUnidadTransSvc().geCatUnidadTransxFilter(Valor);
                                if (resp.typeResult == ResultTypes.success)
                                {
                                    UnidadTransp = ((List<CatUnidadTrans>)resp.result)[0];

                                    if (opcllanta == eOpcionLlantas.Origen)
                                    {
                                        txtTipoUni.Text = UnidadTransp.nomTipoUniTras;
                                    }
                                    else if (opcllanta == eOpcionLlantas.Destino)
                                    {
                                        txtTipoUniDES.Text = UnidadTransp.nomTipoUniTras;
                                    }


                                }
                            }
                        }
                        break;
                    case opLugarLlantas.PILA_DESHECHOS:
                        break;
                    case opLugarLlantas.SIN_UBICACION:
                        break;
                    default:
                        break;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void SeleccionacmbidtipoUbicacion(int idLugar)
        {
            try
            {
                int i = 0;
                foreach (CatLugares item in cmbidtipoUbicacion.Items)
                {
                    if (item.idLugar == idLugar)
                    {
                        cmbidtipoUbicacion.SelectedIndex = i;
                        return;
                    }
                    else
                    {
                        i++;
                    }
                }
            }
            catch (Exception e)
            {

                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + e.Message,
                  this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void SeleccionaUnidadTrans(string idUnidadTransp, eOpcionLlantas opc)
        {
            try
            {
                int i = 0;
                if (opc == eOpcionLlantas.Origen)
                {
                    foreach (CatUnidadTrans item in cmbidUbicacion.Items)
                    {
                        if (item.idUnidadTrans == idUnidadTransp)
                        {
                            cmbidUbicacion.SelectedIndex = i;
                            return;
                        }
                        else
                        {
                            i++;
                        }
                    }
                }
                else if (opc == eOpcionLlantas.Destino)
                {
                    foreach (CatUnidadTrans item in cmbidUbicacionDES.Items)
                    {
                        if (item.idUnidadTrans == idUnidadTransp)
                        {
                            cmbidUbicacionDES.SelectedIndex = i;
                            return;
                        }
                        else
                        {
                            i++;
                        }
                    }
                }

            }
            catch (Exception e)
            {

                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + e.Message,
                  this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void CargaxActividad(opServLlantas opc)
        {
            try
            {
                //PRimero la opcion
                switch (opc)
                {
                    case opServLlantas.DESMONTAR:
                        if (((CatLugares)cmbidtipoUbicacion.SelectedItem).idLugar == 3)
                        {
                            //gpoOrigen.Enabled = true;
                            //solo aplica para almacen
                            AutoCompletarCombo(cmbidtipoUbicacionDES, opcBusqueda.CatLugares, " c.Nomtabla = 'CATALMACEN'", " idlugar ");
                            CargaComboUbicaciones(cmbidUbicacionDES, opLugarLlantas.ALMACEN, eOpcionLlantas.Origen);
                            //ORIGEN
                            txtProfundidadDES.Text = llanta.ProfundidadAct.ToString("N2");
                            gpoOrigen.Enabled = true;
                            cmbidtipoUbicacion.Enabled = false;
                            cmbidUbicacion.Enabled = false;
                            txtTipoUni.Enabled = false;
                            txtProfundidadAct.Enabled = false;
                            txtPosicion.Enabled = false;
                            txtProfundidadFINAL.Enabled = true;

                            //DESTINO
                            gpoDestino.Enabled = true;
                            cmbidtipoUbicacionDES.Enabled = false;
                            cmbidUbicacionDES.Enabled = true;
                            cmbidUbicacionDES.Focus();
                            txtProfundidadDES.Enabled = false;
                            txtProfundidadFINAL_DES.Enabled = false;
                            cmbPosicionDES.Enabled = false;
                            txtidLlantaDES.Enabled = false;
                            

                        }
                        else
                        {
                            MessageBox.Show("No se puede Desmontar, porque la llanta no se encuentra en Unidad de Transporte", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }


                        break;
                    case opServLlantas.MONTAR_UNIDAD:
                        if (((CatLugares)cmbidtipoUbicacion.SelectedItem).idLugar == 1)
                        {
                            //CargaComboUbicaciones(cmbidUbicacion, opLugarLlantas.UNIDADES_TRANS, txtidLlanta.Text);
                            //CargaComboUbicaciones(cmbidUbicacion, (opLugarLlantas)llanta.idtipoUbicacion, llanta.idUbicacion);

                            //ORIGEN - ALMACEN
                            AutoCompletarCombo(cmbidtipoUbicacion, opcBusqueda.CatLugares, " c.Nomtabla = 'CATALMACEN'", " idlugar ");
                            CargaComboUbicaciones(cmbidUbicacionDES, opLugarLlantas.ALMACEN, eOpcionLlantas.Origen);
                            gpoOrigen.Enabled = true;
                            cmbidtipoUbicacion.Enabled = false;
                            cmbidUbicacion.Enabled = false;
                            txtTipoUni.Enabled = false;
                            txtProfundidadAct.Enabled = false;
                            txtPosicion.Enabled = false;
                            txtProfundidadFINAL.Enabled = true;


                            //DESTINO - UNIDAD
                            AutoCompletarCombo(cmbidtipoUbicacionDES, opcBusqueda.CatLugares, " c.Nomtabla = 'CATUNIDADTRANS'", " idlugar ");
                            CargaComboUbicaciones(cmbidUbicacionDES, opLugarLlantas.UNIDADES_TRANS, eOpcionLlantas.Destino, llanta.idUbicacion);
                            gpoDestino.Enabled = true;
                            cmbidtipoUbicacionDES.Enabled = false;
                            cmbidUbicacionDES.Enabled = true;
                            txtTipoUniDES.Enabled = false;
                            txtProfundidadDES.Enabled = false;
                            txtProfundidadFINAL_DES.Enabled = true;
                            cmbPosicionDES.Enabled = true;
                            txtidLlantaDES.Enabled = false;
                            txtProfundidadFINAL.Focus();
                        }
                        else
                        {
                            MessageBox.Show("No se puede Montar en unidad de transporte, porque la llanta no se encuentra en Almacen", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }

                            

                        break;
                    case opServLlantas.ROTAR_MISMA_UNIDAD:
                        if (((CatLugares)cmbidtipoUbicacion.SelectedItem).idLugar == 3)
                        {
                            //ORIGEN - UNIDAD IGUALES
                            //AutoCompletarCombo(cmbidtipoUbicacion, opcBusqueda.CatLugares, " c.Nomtabla = 'CATUNIDADTRANS'", " idlugar ");
                            gpoOrigen.Enabled = true;
                            cmbidtipoUbicacion.Enabled = false;
                            cmbidUbicacion.Enabled = false;
                            txtTipoUni.Enabled = false;
                            txtProfundidadAct.Enabled = false;
                            txtProfundidadFINAL.Enabled = true;
                            txtPosicion.Enabled = false;
                            //DESTINO - UNIDAD
                            AutoCompletarCombo(cmbidtipoUbicacionDES, opcBusqueda.CatLugares, " c.Nomtabla = 'CATUNIDADTRANS'", " idlugar ");
                            CargaComboUbicaciones(cmbidUbicacionDES, opLugarLlantas.UNIDADES_TRANS, eOpcionLlantas.Destino, llanta.idUbicacion);
                            gpoDestino.Enabled = true;
                            cmbidtipoUbicacionDES.Enabled = false;
                            cmbidUbicacionDES.Enabled = false;
                            txtTipoUniDES.Enabled = false;
                            txtProfundidadDES.Enabled = false;
                            txtProfundidadFINAL_DES.Enabled = true;
                            cmbPosicionDES.Enabled = true;
                            txtidLlantaDES.Enabled = false;
                            txtProfundidadFINAL.Focus();
                        }
                        else
                        {
                            MessageBox.Show("No se puede Montar en unidad de transporte, porque la llanta no se encuentra en Almacen", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                            
                        break;
                    case opServLlantas.CAMBIAR_OTRA_UNIDAD:
                        if (((CatLugares)cmbidtipoUbicacion.SelectedItem).idLugar == 3)
                        {
                            //origen
                            AutoCompletarCombo(cmbidtipoUbicacion, opcBusqueda.CatLugares, " c.Nomtabla = 'CATUNIDADTRANS'", " idlugar ");
                            //CargaComboUbicaciones(cmbidUbicacion, opLugarLlantas.UNIDADES_TRANS, txtidLlanta.Text);

                            gpoOrigen.Enabled = true;
                            cmbidtipoUbicacion.Enabled = false;
                            cmbidUbicacion.Enabled = false;
                            txtTipoUni.Enabled = false;
                            txtProfundidadAct.Enabled = false;
                            txtProfundidadFINAL.Enabled = true;
                            txtPosicion.Enabled = false;

                            //destino
                            AutoCompletarCombo(cmbidtipoUbicacionDES, opcBusqueda.CatLugares, " c.Nomtabla = 'CATUNIDADTRANS'", " idlugar ");
                            CargaComboUbicaciones(cmbidUbicacionDES, opLugarLlantas.UNIDADES_TRANS, eOpcionLlantas.Destino);
                            gpoDestino.Enabled = true;
                            cmbidtipoUbicacionDES.Enabled = false;
                            cmbidUbicacionDES.Enabled = true;
                            txtTipoUniDES.Enabled = false;
                            txtProfundidadDES.Enabled = false;
                            txtProfundidadFINAL_DES.Enabled = true;
                            cmbPosicionDES.Enabled = true;
                            txtidLlantaDES.Enabled = false;
                            txtProfundidadFINAL.Focus();
                        }
                        else
                        {
                            MessageBox.Show("No se puede Montar en unidad de transporte, porque la llanta no se encuentra en Almacen", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                           

                        break;
                    case opServLlantas.CAMBIO_ALMANCEN:
                        //Verificar
                        if (((CatLugares)cmbidtipoUbicacion.SelectedItem).idLugar == 1)
                        {
                            gpoOrigen.Enabled = true;
                            cmbidtipoUbicacion.Enabled = false;
                            cmbidUbicacion.Enabled = false;
                            txtTipoUni.Enabled = false;
                            txtProfundidadAct.Enabled = false;
                            txtPosicion.Enabled = false;
                            txtProfundidadFINAL.Enabled = false;

                            //DESTINO - ALMACEN
                            AutoCompletarCombo(cmbidtipoUbicacionDES, opcBusqueda.CatLugares, " c.Nomtabla = 'CATALMACEN'", " idlugar ");
                            CargaComboUbicaciones(cmbidUbicacionDES, opLugarLlantas.ALMACEN, eOpcionLlantas.Destino);
                            gpoDestino.Enabled = true;
                            cmbidtipoUbicacionDES.Enabled = false;
                            cmbidUbicacionDES.Focus();
                            txtProfundidadDES.Enabled = false;
                            txtProfundidadFINAL_DES.Enabled = false;
                            cmbPosicionDES.Enabled = false;
                            cmbidUbicacionDES.Enabled = true;
                            txtidLlantaDES.Enabled = false;
                        }
                        else
                        {
                            MessageBox.Show("No se puede Cambiar de Almacen, porque la llanta no se encuentra en Almacen", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        //dos almacenes
                        //ORIGEN - ALMACEN
                        //AutoCompletarCombo(cmbidtipoUbicacion, opcBusqueda.CatLugares, " c.Nomtabla = 'CATALMACEN'", " idlugar ");
                        //CargaComboUbicaciones(cmbidUbicacion, opLugarLlantas.ALMACEN, eOpcionLlantas.Origen);

                        break;
                    case opServLlantas.RECHAZADA_PROVEEDOR:
                        if (((CatLugares)cmbidtipoUbicacion.SelectedItem).idLugar == 2)
                        {
                            gpoOrigen.Enabled = true;
                            cmbidtipoUbicacion.Enabled = false;
                            cmbidUbicacion.Enabled = false;
                            txtTipoUni.Enabled = false;
                            txtProfundidadAct.Enabled = false;
                            txtPosicion.Enabled = false;
                            txtProfundidadFINAL.Enabled = true;

                            //DESTINO - ALMACEN
                            AutoCompletarCombo(cmbidtipoUbicacionDES, opcBusqueda.CatLugares, " c.Nomtabla = 'CATALMACEN'", " idlugar ");
                            CargaComboUbicaciones(cmbidUbicacionDES, opLugarLlantas.ALMACEN, eOpcionLlantas.Destino);
                            cmbidtipoUbicacionDES.Enabled = false;
                            cmbidUbicacionDES.Focus();
                            txtProfundidadDES.Enabled = false;
                            txtProfundidadFINAL_DES.Enabled = false;
                            cmbPosicionDES.Enabled = false;
                            cmbidUbicacionDES.Enabled = true;
                            txtidLlantaDES.Enabled = false;
                        }
                        else
                        {
                            MessageBox.Show("No se puede rechazar de Proveedor, porque la llanta no se encuentra con algun proveedor", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                            break;
                    default:
                        break;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        private void SeleccionaAlmacen(int idAlmacen)
        {
            try
            {
                int i = 0;
                foreach (CatAlmacen item in cmbidUbicacion.Items)
                {
                    if (item.idAlmacen == idAlmacen)
                    {
                        cmbidUbicacion.SelectedIndex = i;
                        return;
                    }
                    else
                    {
                        i++;
                    }
                }
            }
            catch (Exception e)
            {

                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + e.Message,
                  this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void cmbidUbicacionDES_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if ((opServLlantas)_idEnum == opServLlantas.CAMBIAR_OTRA_UNIDAD)
            if ((opServLlantas)cmbActividades.SelectedValue == opServLlantas.CAMBIAR_OTRA_UNIDAD)
            {

                if (cmbidUbicacionDES.SelectedItem != null)
                {
                    CargaForma(opcBusqueda.CatUnidadTrans, ((CatUnidadTrans)cmbidUbicacionDES.SelectedItem).idUnidadTrans, eOpcionLlantas.Destino);
                    resp = new CatLlantasSvc().getCatLlantasxFilter(0, "c.idUbicacion = '" + ((CatUnidadTrans)cmbidUbicacionDES.SelectedItem).idUnidadTrans + "'", " posicion ");
                    if (resp.typeResult == ResultTypes.success)
                    {
                        listaLlantasDes = (List<CatLlantas>)resp.result;
                        cmbPosicionDES.DataSource = null;
                        cmbPosicionDES.DataSource = listaLlantasDes;
                        cmbPosicionDES.DisplayMember = "Posicion";
                        cmbPosicionDES.SelectedIndex = 0;
                        txtidLlantaDES.Text = listaLlantasDes[0].idLlanta;
                        txtProfundidadDES.Text = listaLlantasDes[0].ProfundidadAct.ToString("N2");

                        //txtPosicionDES.Enabled = true;
                        //txtProfundidadFINAL_DES.Enabled = true;

                    }
                }

            }
            //else if ((opServLlantas)_idEnum == opServLlantas.ROTAR_MISMA_UNIDAD || (opServLlantas)_idEnum == opServLlantas.MONTAR_UNIDAD)
            else if ((opServLlantas)cmbActividades.SelectedValue == opServLlantas.MONTAR_UNIDAD)
            {
                CargaForma(opcBusqueda.CatUnidadTrans, llanta.idUbicacion, eOpcionLlantas.Destino);
                resp = new CatLlantasSvc().getCatLlantasxFilter(0, "c.idUbicacion = '" + llanta.idUbicacion + "' and c.posicion <> " + txtPosicion.Value, " posicion ");
                if (resp.typeResult == ResultTypes.success)
                {
                    listaLlantasDes = (List<CatLlantas>)resp.result;
                    cmbPosicionDES.DataSource = null;
                    cmbPosicionDES.DataSource = listaLlantasDes;
                    cmbPosicionDES.DisplayMember = "Posicion";
                    cmbPosicionDES.SelectedIndex = 0;
                    txtidLlantaDES.Text = listaLlantasDes[0].idLlanta;
                    txtProfundidadDES.Text = listaLlantasDes[0].ProfundidadAct.ToString("N2");

                    //txtPosicionDES.Enabled = true;
                    //txtProfundidadFINAL_DES.Enabled = true;

                }
            }
            else if ((opServLlantas)cmbActividades.SelectedValue == opServLlantas.ROTAR_MISMA_UNIDAD
            
            )
            {
                CargaForma(opcBusqueda.CatUnidadTrans, llanta.idUbicacion, eOpcionLlantas.Destino);
                resp = new CatLlantasSvc().getCatLlantasxFilter(0, "c.idUbicacion = '" + llanta.idUbicacion + "' and c.posicion <> " + txtPosicion.Value, " posicion ");
                if (resp.typeResult == ResultTypes.success)
                {
                    listaLlantasDes = (List<CatLlantas>)resp.result;
                    cmbPosicionDES.DataSource = null;
                    cmbPosicionDES.DataSource = listaLlantasDes;
                    cmbPosicionDES.DisplayMember = "Posicion";
                    cmbPosicionDES.SelectedIndex = 0;
                    txtidLlantaDES.Text = listaLlantasDes[0].idLlanta;
                    txtProfundidadDES.Text = listaLlantasDes[0].ProfundidadAct.ToString("N2");

                    //txtPosicionDES.Enabled = true;
                    //txtProfundidadFINAL_DES.Enabled = true;

                }

            }



        }

        private void cmbidUbicacionDES_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if ((opServLlantas)_idEnum == opServLlantas.CAMBIAR_OTRA_UNIDAD)
                {
                    CargaForma(opcBusqueda.CatUnidadTrans, ((CatUnidadTrans)cmbidUbicacionDES.SelectedItem).idUnidadTrans, eOpcionLlantas.Destino);
                    resp = new CatLlantasSvc().getCatLlantasxFilter(0, "c.idUbicacion = '" + ((CatUnidadTrans)cmbidUbicacionDES.SelectedItem).idUnidadTrans + "'", " posicion ");
                    if (resp.typeResult == ResultTypes.success)
                    {
                        listaLlantasDes = (List<CatLlantas>)resp.result;
                        cmbPosicionDES.DataSource = null;
                        cmbPosicionDES.DataSource = listaLlantasDes;
                        cmbPosicionDES.DisplayMember = "Posicion";
                        cmbPosicionDES.SelectedIndex = 0;
                        txtidLlantaDES.Text = listaLlantasDes[0].idLlanta;
                        txtProfundidadDES.Text = listaLlantasDes[0].ProfundidadAct.ToString("N2");

                    }

                }
            }
        }

        private void cmbPosicionDES_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbPosicionDES.Items.Count > 0)
            {
                if (cmbPosicionDES.SelectedItem != null)
                {
                    txtidLlantaDES.Text = ((CatLlantas)cmbPosicionDES.SelectedItem).idLlanta;
                    txtProfundidadDES.Text = ((CatLlantas)cmbPosicionDES.SelectedItem).ProfundidadAct.ToString("N2");

                }
            }
        }

        private void btnGrabar_Click(object sender, EventArgs e)
        {
            //se graba en 3 lados
            //MovLLantas - inserta
            //LlantasHis - inserta
            //CatLlantas - actualiza
            //determinar 
            //  idtipoDOC = Orden_Trabajo = 8
            eTipoDocumento TipoDoc = eTipoDocumento.Orden_Trabajo;

            if (_idEnum > 0)
            {
                opcionAct = (opServLlantas)_idEnum;
            }
            else
            {

                opcionAct = (opServLlantas)cmbActividades.SelectedValue;
            }

            if (!Validar(opcionAct))
            {
                return;
            }

            MovLlanta = new MovLLantas();
            listaMovLLantas = new List<MovLLantas>();
            llantaGraba = new CatLlantas();
            listaLlantasGraba = new List<CatLlantas>();
            llantaHis = new LlantasHis();
            listaHistoria = new List<LlantasHis>();

            switch (opcionAct)
            {
                case opServLlantas.DESMONTAR:
                    //movllantas
                    MovLlanta = MapeaMov((int)opcionAct, DateTime.Now, _Usuario.nombreUsuario, llanta.idSisLlanta,
                        ((CatLugares)cmbidtipoUbicacion.SelectedItem).idLugar,
                        ((CatUnidadTrans)cmbidUbicacion.SelectedItem).idUnidadTrans,
                        (int)txtPosicion.Value, ((CatLugares)cmbidtipoUbicacionDES.SelectedItem).idLugar,
                        ((CatAlmacen)cmbidUbicacionDES.SelectedItem).idAlmacen.ToString(), 0, txtObservaciones.Text);
                    listaMovLLantas.Add(MovLlanta);

                    //historia del destino y de la llanta
                    llantaHis = MapeaHis(
                    ((CatLugares)cmbidtipoUbicacionDES.SelectedItem).idLugar,
                     ((CatAlmacen)cmbidUbicacionDES.SelectedItem).idAlmacen.ToString(),
                     DateTime.Now, _Usuario.nombreUsuario,
                     llanta.idSisLlanta,
                     llanta.idLlanta,
                     false,
                     (int)TipoDoc, _idOrdenTrabajo.ToString(),
                     Convert.ToInt32(txtProfundidadFINAL.Text),
                     Convert.ToInt32(llanta.idCondicion),
                     Convert.ToInt32(txtPosicion.Value));

                    listaHistoria.Add(llantaHis);
                    //llanta - 
                    llanta.Posicion = (int)txtPosicion.Value;
                    llanta.idtipoUbicacion = ((CatLugares)cmbidtipoUbicacionDES.SelectedItem).idLugar;
                    llanta.idUbicacion = ((CatAlmacen)cmbidUbicacionDES.SelectedItem).idAlmacen.ToString();
                    llanta.UltFecha = DateTime.Now;
                    llanta.ProfundidadAct = Convert.ToInt32(txtProfundidadFINAL.Text);

                    listaLlantasGraba.Add(llanta);
                    break;
                case opServLlantas.MONTAR_UNIDAD:

                    //movllantas
                    MovLlanta = MapeaMov((int)opcionAct, DateTime.Now, _Usuario.nombreUsuario,
                        llanta.idSisLlanta,
                        ((CatLugares)cmbidtipoUbicacion.SelectedItem).idLugar,
                        ((CatAlmacen)cmbidUbicacion.SelectedItem).idAlmacen.ToString(),
                        0,
                        ((CatLugares)cmbidtipoUbicacionDES.SelectedItem).idLugar,
                        ((CatUnidadTrans)cmbidUbicacionDES.SelectedItem).idUnidadTrans,
                        (int)txtPosicion.Value,
                        txtObservaciones.Text);
                    listaMovLLantas.Add(MovLlanta);

                    //historia del destino y de la llanta
                    llantaHis = MapeaHis(
                    ((CatLugares)cmbidtipoUbicacionDES.SelectedItem).idLugar,
                     ((CatUnidadTrans)cmbidUbicacionDES.SelectedItem).idUnidadTrans,
                     DateTime.Now, _Usuario.nombreUsuario,
                     llanta.idSisLlanta,
                     llanta.idLlanta,
                     false,
                     (int)TipoDoc, _idOrdenTrabajo.ToString(),
                     Convert.ToInt32(txtProfundidadFINAL.Text),
                     Convert.ToInt32(llanta.idCondicion),
                     Convert.ToInt32(txtPosicion.Value));

                    listaHistoria.Add(llantaHis);
                    //llanta - 
                    llanta.Posicion = (int)txtPosicion.Value;
                    llanta.idtipoUbicacion = ((CatLugares)cmbidtipoUbicacionDES.SelectedItem).idLugar;
                    llanta.idUbicacion = ((CatUnidadTrans)cmbidUbicacionDES.SelectedItem).idUnidadTrans.ToString();
                    llanta.UltFecha = DateTime.Now;
                    llanta.ProfundidadAct = Convert.ToInt32(txtProfundidadFINAL_DES.Text);

                    listaLlantasGraba.Add(llanta);
                    break;
                case opServLlantas.ROTAR_MISMA_UNIDAD:
                    //POSICION 1
                    //movllantas

                    MovLlanta = MapeaMov((int)opcionAct, DateTime.Now, _Usuario.nombreUsuario,
                        llanta.idSisLlanta,
                        ((CatLugares)cmbidtipoUbicacion.SelectedItem).idLugar,
                        ((CatUnidadTrans)cmbidUbicacion.SelectedItem).idUnidadTrans,
                        (int)txtPosicion.Value,
                        ((CatLugares)cmbidtipoUbicacionDES.SelectedItem).idLugar,
                        ((CatUnidadTrans)cmbidUbicacionDES.SelectedItem).idUnidadTrans,
                        ((CatLlantas)cmbPosicionDES.SelectedItem).Posicion,
                        txtObservaciones.Text);

                    listaMovLLantas.Add(MovLlanta);
                    //historia
                    llantaHis = MapeaHis(
                        ((CatLugares)cmbidtipoUbicacionDES.SelectedItem).idLugar,
                        ((CatUnidadTrans)cmbidUbicacionDES.SelectedItem).idUnidadTrans,
                        DateTime.Now, _Usuario.nombreUsuario,
                        llanta.idSisLlanta,
                        llanta.idLlanta,
                        true,
                        (int)TipoDoc, _idOrdenTrabajo.ToString(),
                        Convert.ToInt32(txtProfundidadFINAL.Text),
                        Convert.ToInt32(llanta.idCondicion),
                        ((CatLlantas)cmbPosicionDES.SelectedItem).Posicion);
                    listaHistoria.Add(llantaHis);
                    //llanta
                    llanta.Posicion = ((CatLlantas)cmbPosicionDES.SelectedItem).Posicion;
                    llanta.idtipoUbicacion = ((CatLugares)cmbidtipoUbicacionDES.SelectedItem).idLugar;
                    llanta.idUbicacion = ((CatUnidadTrans)cmbidUbicacionDES.SelectedItem).idUnidadTrans;
                    llanta.UltFecha = DateTime.Now;
                    llanta.ProfundidadAct = Convert.ToInt32(txtProfundidadFINAL.Text);
                    listaLlantasGraba.Add(llanta);

                    //POSICION 2
                    //movllantas
                    MovLlanta = MapeaMov((int)opcionAct, DateTime.Now, _Usuario.nombreUsuario,
                       ((CatLlantas)cmbPosicionDES.SelectedItem).idSisLlanta,
                       ((CatLugares)cmbidtipoUbicacionDES.SelectedItem).idLugar,
                       ((CatUnidadTrans)cmbidUbicacionDES.SelectedItem).idUnidadTrans,
                       ((CatLlantas)cmbPosicionDES.SelectedItem).Posicion,
                       ((CatLugares)cmbidtipoUbicacion.SelectedItem).idLugar,
                       ((CatUnidadTrans)cmbidUbicacion.SelectedItem).idUnidadTrans,
                       (int)txtPosicion.Value,
                       txtObservaciones.Text);
                    listaMovLLantas.Add(MovLlanta);
                    //historia
                    llantaHis = MapeaHis(
                        ((CatLugares)cmbidtipoUbicacion.SelectedItem).idLugar,
                        ((CatUnidadTrans)cmbidUbicacion.SelectedItem).idUnidadTrans,
                        DateTime.Now, _Usuario.nombreUsuario,
                        ((CatLlantas)cmbPosicionDES.SelectedItem).idSisLlanta,
                        ((CatLlantas)cmbPosicionDES.SelectedItem).idLlanta,
                        true,
                        (int)TipoDoc, _idOrdenTrabajo.ToString(),
                        Convert.ToInt32(txtProfundidadFINAL_DES.Text),
                         Convert.ToInt32(((CatLlantas)cmbPosicionDES.SelectedItem).idCondicion),
                         (int)txtPosicion.Value);
                    listaHistoria.Add(llantaHis);

                    //llanta
                    ((CatLlantas)cmbPosicionDES.SelectedItem).Posicion = (int)txtPosicion.Value;
                    ((CatLlantas)cmbPosicionDES.SelectedItem).idtipoUbicacion = ((CatLugares)cmbidtipoUbicacion.SelectedItem).idLugar;
                    ((CatLlantas)cmbPosicionDES.SelectedItem).idUbicacion = ((CatUnidadTrans)cmbidUbicacion.SelectedItem).idUnidadTrans;
                    ((CatLlantas)cmbPosicionDES.SelectedItem).UltFecha = DateTime.Now;
                    ((CatLlantas)cmbPosicionDES.SelectedItem).ProfundidadAct = Convert.ToInt32(txtProfundidadFINAL_DES.Text);

                    listaLlantasGraba.Add(((CatLlantas)cmbPosicionDES.SelectedItem));
                    break;
                case opServLlantas.CAMBIAR_OTRA_UNIDAD:
                    //POSICION 1
                    //movllantas

                    MovLlanta = MapeaMov((int)opcionAct, DateTime.Now, _Usuario.nombreUsuario,
                        llanta.idSisLlanta,
                        ((CatLugares)cmbidtipoUbicacion.SelectedItem).idLugar,
                        ((CatUnidadTrans)cmbidUbicacion.SelectedItem).idUnidadTrans,
                        (int)txtPosicion.Value,
                        ((CatLugares)cmbidtipoUbicacionDES.SelectedItem).idLugar,
                        ((CatUnidadTrans)cmbidUbicacionDES.SelectedItem).idUnidadTrans,
                        ((CatLlantas)cmbPosicionDES.SelectedItem).Posicion,
                        txtObservaciones.Text);

                    listaMovLLantas.Add(MovLlanta);
                    //historia
                    llantaHis = MapeaHis(
                        ((CatLugares)cmbidtipoUbicacionDES.SelectedItem).idLugar,
                        ((CatUnidadTrans)cmbidUbicacionDES.SelectedItem).idUnidadTrans,
                        DateTime.Now, _Usuario.nombreUsuario,
                        llanta.idSisLlanta,
                        llanta.idLlanta,
                        true,
                        (int)TipoDoc, _idOrdenTrabajo.ToString(),
                        Convert.ToInt32(txtProfundidadFINAL.Text),
                        Convert.ToInt32(llanta.idCondicion),
                        ((CatLlantas)cmbPosicionDES.SelectedItem).Posicion);
                    listaHistoria.Add(llantaHis);
                    //llanta
                    llanta.Posicion = ((CatLlantas)cmbPosicionDES.SelectedItem).Posicion;
                    llanta.idtipoUbicacion = ((CatLugares)cmbidtipoUbicacionDES.SelectedItem).idLugar;
                    llanta.idUbicacion = ((CatUnidadTrans)cmbidUbicacionDES.SelectedItem).idUnidadTrans;
                    llanta.UltFecha = DateTime.Now;
                    llanta.ProfundidadAct = Convert.ToInt32(txtProfundidadFINAL.Text);
                    listaLlantasGraba.Add(llanta);

                    //POSICION 2
                    //movllantas
                    MovLlanta = MapeaMov((int)opcionAct, DateTime.Now, _Usuario.nombreUsuario,
                       ((CatLlantas)cmbPosicionDES.SelectedItem).idSisLlanta,
                       ((CatLugares)cmbidtipoUbicacionDES.SelectedItem).idLugar,
                       ((CatUnidadTrans)cmbidUbicacionDES.SelectedItem).idUnidadTrans,
                       ((CatLlantas)cmbPosicionDES.SelectedItem).Posicion,
                       ((CatLugares)cmbidtipoUbicacion.SelectedItem).idLugar,
                       ((CatUnidadTrans)cmbidUbicacion.SelectedItem).idUnidadTrans,
                       (int)txtPosicion.Value,
                       txtObservaciones.Text);
                    listaMovLLantas.Add(MovLlanta);
                    //historia
                    llantaHis = MapeaHis(
                        ((CatLugares)cmbidtipoUbicacion.SelectedItem).idLugar,
                        ((CatUnidadTrans)cmbidUbicacion.SelectedItem).idUnidadTrans,
                        DateTime.Now, _Usuario.nombreUsuario,
                        ((CatLlantas)cmbPosicionDES.SelectedItem).idSisLlanta,
                        ((CatLlantas)cmbPosicionDES.SelectedItem).idLlanta,
                        true,
                        (int)TipoDoc, _idOrdenTrabajo.ToString(),
                        Convert.ToInt32(txtProfundidadFINAL_DES.Text),
                         Convert.ToInt32(((CatLlantas)cmbPosicionDES.SelectedItem).idCondicion),
                         (int)txtPosicion.Value);
                    listaHistoria.Add(llantaHis);

                    //llanta
                    ((CatLlantas)cmbPosicionDES.SelectedItem).Posicion = (int)txtPosicion.Value;
                    ((CatLlantas)cmbPosicionDES.SelectedItem).idtipoUbicacion = ((CatLugares)cmbidtipoUbicacion.SelectedItem).idLugar;
                    ((CatLlantas)cmbPosicionDES.SelectedItem).idUbicacion = ((CatUnidadTrans)cmbidUbicacion.SelectedItem).idUnidadTrans;
                    ((CatLlantas)cmbPosicionDES.SelectedItem).UltFecha = DateTime.Now;
                    ((CatLlantas)cmbPosicionDES.SelectedItem).ProfundidadAct = Convert.ToInt32(txtProfundidadFINAL_DES.Text);
                    listaLlantasGraba.Add(((CatLlantas)cmbPosicionDES.SelectedItem));
                    break;
                case opServLlantas.CAMBIO_ALMANCEN:
                    //movllantas
                    MovLlanta = MapeaMov((int)opcionAct, DateTime.Now, _Usuario.nombreUsuario, llanta.idSisLlanta,
                        ((CatLugares)cmbidtipoUbicacion.SelectedItem).idLugar,
                        ((CatAlmacen)cmbidUbicacion.SelectedItem).idAlmacen.ToString(),
                        (int)txtPosicion.Value, ((CatLugares)cmbidtipoUbicacionDES.SelectedItem).idLugar,
                        ((CatAlmacen)cmbidUbicacionDES.SelectedItem).idAlmacen.ToString(), 0, txtObservaciones.Text);
                    listaMovLLantas.Add(MovLlanta);

                    //historia del destino y de la llanta
                    llantaHis = MapeaHis(
                    ((CatLugares)cmbidtipoUbicacionDES.SelectedItem).idLugar,
                     ((CatAlmacen)cmbidUbicacionDES.SelectedItem).idAlmacen.ToString(),
                     DateTime.Now, _Usuario.nombreUsuario,
                     llanta.idSisLlanta,
                     llanta.idLlanta,
                     false,
                     (int)TipoDoc, _idOrdenTrabajo.ToString(),
                     Convert.ToInt32(txtProfundidadFINAL.Text),
                     Convert.ToInt32(llanta.idCondicion),
                     Convert.ToInt32(txtPosicion.Value));

                    listaHistoria.Add(llantaHis);
                    //llanta - 
                    llanta.Posicion = (int)txtPosicion.Value;
                    llanta.idtipoUbicacion = ((CatLugares)cmbidtipoUbicacionDES.SelectedItem).idLugar;
                    llanta.idUbicacion = ((CatAlmacen)cmbidUbicacionDES.SelectedItem).idAlmacen.ToString();
                    llanta.UltFecha = DateTime.Now;
                    llanta.ProfundidadAct = Convert.ToInt32(txtProfundidadFINAL.Text);

                    listaLlantasGraba.Add(llanta);
                    break;
                case opServLlantas.RECHAZADA_PROVEEDOR:
                    break;
                default:
                    break;
            }

            //GRABAR
            bool BandExito = false;
            if (listaMovLLantas.Count > 0 && listaHistoria.Count > 0 && listaLlantasGraba.Count > 0)
            {
                foreach (var mov in listaMovLLantas)
                {
                    MovLlanta = mov;
                    resp = new MovLLantasSvc().GuardaMovLLantas(ref MovLlanta);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        BandExito = true;
                    }
                    else
                    {
                        BandExito = false;
                    }
                    if (!BandExito)
                    {
                        break;
                    }
                }
                if (BandExito)
                {
                    foreach (var his in listaHistoria)
                    {
                        llantaHis = his;
                        resp = new LlantasHisSvc().GuardaLlantasHis(ref llantaHis);
                        if (resp.typeResult == ResultTypes.success)
                        {
                            BandExito = true;
                        }
                        else
                        {
                            BandExito = false;
                        }
                        if (!BandExito)
                        {
                            break;
                        }
                    }
                    if (BandExito)
                    {
                        foreach (var lla in listaLlantasGraba)
                        {
                            llantaGraba = lla;
                            resp = new CatLlantasSvc().GuardaCatLlantas(ref llantaGraba);
                            if (resp.typeResult == ResultTypes.success)
                            {
                                BandExito = true;
                            }
                            else
                            {
                                BandExito = false;
                            }
                            if (!BandExito)
                            {
                                break;
                            }
                        }
                    }
                }
                else
                {
                    //salir
                }

                if (BandExito)
                {
                    MessageBox.Show("Se realizó el movimiento " + cmbActividades.Text + " para la llanta: " + txtidLlanta.Text, "Operación Exitosa", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    if (opcionForma == opcForma.Nuevo)
                    {
                        Cancelar();
                    }
                    else
                    {
                        Close();
                    }

                   
                    //CORRECTO
                }

            }

        }
        private MovLLantas MapeaMov(int TipoMov, DateTime Fecha, string Usuario, int idSisLlanta, int idtipoUbicacionOrig,
        string idUbicacionOrig, int PosicionOrig, int idtipoUbicacionDes, string idUbicacionDes, int PosicionDes, string Observaciones)
        {
            return new MovLLantas
            {
                idMovLlanta = 0,
                TipoMov = TipoMov,
                Fecha = Fecha,
                Usuario = Usuario,
                idSisLlanta = idSisLlanta,
                idtipoUbicacionOrig = idtipoUbicacionOrig,
                idUbicacionOrig = idUbicacionOrig,
                PosicionOrig = PosicionOrig,
                idtipoUbicacionDes = idtipoUbicacionDes,
                idUbicacionDes = idUbicacionDes,
                PosicionDes = PosicionDes,
                Observaciones = Observaciones

            };
        }
        private LlantasHis MapeaHis(int idtipoUbicacion, string idUbicacion, DateTime Fecha, string Usuario, int idSisLlanta,
            string idLlanta, bool EntSal, int idTipoDoc, string Documento, decimal Profundidad, int idCondicion, int Posicion)
        {
            return new LlantasHis
            {
                idSis = 0,
                idtipoUbicacion = idtipoUbicacion,
                idUbicacion = idUbicacion,
                Fecha = Fecha,
                Usuario = Usuario,
                idSisLlanta = idSisLlanta,
                idLlanta = idLlanta,
                EntSal = EntSal,
                idTipoDoc = idTipoDoc,
                Documento = Documento,
                Profundidad = Profundidad,
                idCondicion = idCondicion

            };
        }

        private bool Validar(opServLlantas opc)
        {
            int Entero = 0;
            switch (opc)
            {
                case opServLlantas.DESMONTAR:
                    if (!string.IsNullOrEmpty(txtProfundidadFINAL.Text))
                    {
                        if (!int.TryParse(txtProfundidadFINAL.Text, out Entero))
                        {
                            MessageBox.Show("La Profundidad Final de Origen tiene que ser Númerica", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtProfundidadFINAL.SelectAll();
                            txtProfundidadFINAL.Focus();
                            return false;
                        }
                    }
                    else
                    {
                        MessageBox.Show("La Profundidad Final de Origen no puede ser vacio", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtProfundidadFINAL.SelectAll();
                        txtProfundidadFINAL.Focus();
                        return false;
                    }

                    break;
                case opServLlantas.MONTAR_UNIDAD:
                    break;
                case opServLlantas.ROTAR_MISMA_UNIDAD:
                    if (!string.IsNullOrEmpty(txtProfundidadFINAL.Text))
                    {
                        if (!int.TryParse(txtProfundidadFINAL.Text, out Entero))
                        {
                            MessageBox.Show("La Profundidad Final de Origen tiene que ser Númerica", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtProfundidadFINAL.SelectAll();
                            txtProfundidadFINAL.Focus();
                            return false;
                        }
                    }
                    else
                    {
                        MessageBox.Show("La Profundidad Final de Origen no puede ser vacio", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtProfundidadFINAL.SelectAll();
                        txtProfundidadFINAL.Focus();
                        return false;
                    }
                    if (!string.IsNullOrEmpty(txtProfundidadFINAL_DES.Text))
                    {
                        if (!int.TryParse(txtProfundidadFINAL_DES.Text, out Entero))
                        {
                            MessageBox.Show("La Profundidad Final de Destino tiene que ser Númerica", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtProfundidadFINAL_DES.SelectAll();
                            txtProfundidadFINAL_DES.Focus();
                            return false;
                        }
                    }
                    else
                    {
                        MessageBox.Show("La Profundidad Final de Destino no puede ser vacio", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtProfundidadFINAL_DES.SelectAll();
                        txtProfundidadFINAL_DES.Focus();
                        return false;
                    }
                    break;
                case opServLlantas.CAMBIAR_OTRA_UNIDAD:
                    if (!string.IsNullOrEmpty(txtProfundidadFINAL.Text))
                    {
                        if (!int.TryParse(txtProfundidadFINAL.Text, out Entero))
                        {
                            MessageBox.Show("La Profundidad Final de Origen tiene que ser Númerica", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtProfundidadFINAL.SelectAll();
                            txtProfundidadFINAL.Focus();
                            return false;
                        }
                    }
                    else
                    {
                        MessageBox.Show("La Profundidad Final de Origen no puede ser vacio", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtProfundidadFINAL.SelectAll();
                        txtProfundidadFINAL.Focus();
                        return false;
                    }
                    if (!string.IsNullOrEmpty(txtProfundidadFINAL_DES.Text))
                    {
                        if (!int.TryParse(txtProfundidadFINAL_DES.Text, out Entero))
                        {
                            MessageBox.Show("La Profundidad Final de Destino tiene que ser Númerica", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtProfundidadFINAL_DES.SelectAll();
                            txtProfundidadFINAL_DES.Focus();
                            return false;
                        }
                    }
                    else
                    {
                        MessageBox.Show("La Profundidad Final de Destino no puede ser vacio", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtProfundidadFINAL_DES.SelectAll();
                        txtProfundidadFINAL_DES.Focus();
                        return false;
                    }
                    break;
                case opServLlantas.CAMBIO_ALMANCEN:
                    break;
                case opServLlantas.RECHAZADA_PROVEEDOR:
                    break;
                default:
                    break;
            }
            return true;
        }

        //21/abril/2018
        private void AutoAcompletarTexto(TextBox control, opcBusqueda opc)
        {
            OperationResult resp = new OperationResult();
            AutoCompleteStringCollection coleccion = new AutoCompleteStringCollection();

            if (opc == opcBusqueda.CatLLantas)
            {
                resp = new CatLlantasSvc().getCatLlantasxFilter();
            }
            if (resp.typeResult == ResultTypes.success)
            {
                if (opc == opcBusqueda.CatLLantas)
                {
                    listaLlantas = (List<CatLlantas>)resp.result;
                    foreach (var item in listaLlantas)
                    {
                        coleccion.Add(Convert.ToString(item.idLlanta));
                    }
                }
                control.AutoCompleteCustomSource = coleccion;
                control.AutoCompleteMode = AutoCompleteMode.Suggest;
                control.AutoCompleteSource = AutoCompleteSource.CustomSource;
            }
        }
        private void btnNuevo_Click(object sender, EventArgs e)
        {
            if (opcionForma == 0)
            {
                opcionForma = opcForma.Nuevo;
            }

            HabilitaBotones(opcHabilita.Nuevo);
            HabilitaCampos(opcHabilita.Iniciando);
            txtidLlanta.Enabled = true;
            txtidLlanta.Focus();
        }

        private void txtidLlanta_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtidLlanta_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                CargaForma(opcBusqueda.CatLLantas, txtidLlanta.Text, eOpcionLlantas.Origen);
                txtidLlanta.Enabled = false;
                cmbActividades.Enabled = true;
                cmbActividades.Focus();

            }
        }

        private void cmbActividades_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cmbActividades_KeyUp(object sender, KeyEventArgs e)
        {

        }

        private void cmbActividades_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                //opcionAct = (opServLlantas)cmbActividades.SelectedItem;
                opcionAct = (opServLlantas)cmbActividades.SelectedValue;
                CargaxActividad(opcionAct);
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Cancelar();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }
    }//
}
