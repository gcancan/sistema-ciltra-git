﻿namespace Fletera2.Procesos
{
    partial class fMovllantas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fMovllantas));
            this.ToolStripMenu = new System.Windows.Forms.ToolStrip();
            this.btnNuevo = new System.Windows.Forms.ToolStripButton();
            this.btnGrabar = new System.Windows.Forms.ToolStripButton();
            this.btnCancelar = new System.Windows.Forms.ToolStripButton();
            this.btnSalir = new System.Windows.Forms.ToolStripButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.gpoMovimientos = new System.Windows.Forms.GroupBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtObservaciones = new System.Windows.Forms.TextBox();
            this.gpoDestino = new System.Windows.Forms.GroupBox();
            this.cmbPosicionDES = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtidLlantaDES = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtProfundidadFINAL_DES = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtProfundidadDES = new System.Windows.Forms.TextBox();
            this.txtTipoUniDES = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.cmbidUbicacionDES = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.cmbidtipoUbicacionDES = new System.Windows.Forms.ComboBox();
            this.gpoOrigen = new System.Windows.Forms.GroupBox();
            this.Label26 = new System.Windows.Forms.Label();
            this.txtProfundidadFINAL = new System.Windows.Forms.TextBox();
            this.Label14 = new System.Windows.Forms.Label();
            this.txtProfundidadAct = new System.Windows.Forms.TextBox();
            this.txtTipoUni = new System.Windows.Forms.TextBox();
            this.Label12 = new System.Windows.Forms.Label();
            this.txtPosicion = new System.Windows.Forms.NumericUpDown();
            this.Label13 = new System.Windows.Forms.Label();
            this.cmbidUbicacion = new System.Windows.Forms.ComboBox();
            this.lblUbicacion = new System.Windows.Forms.Label();
            this.Label21 = new System.Windows.Forms.Label();
            this.cmbidtipoUbicacion = new System.Windows.Forms.ComboBox();
            this.cmbActividades = new System.Windows.Forms.ComboBox();
            this.lblopera1 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtidLlanta = new System.Windows.Forms.TextBox();
            this.gpoCaract = new System.Windows.Forms.GroupBox();
            this.txtidCondicion = new System.Windows.Forms.TextBox();
            this.txtidDisenio = new System.Windows.Forms.TextBox();
            this.txtidTipoLLanta = new System.Windows.Forms.TextBox();
            this.txtidMarca = new System.Windows.Forms.TextBox();
            this.txtidProductoLlanta = new System.Windows.Forms.TextBox();
            this.Label20 = new System.Windows.Forms.Label();
            this.Label8 = new System.Windows.Forms.Label();
            this.chkActivo = new System.Windows.Forms.CheckBox();
            this.gpoRevitalizado = new System.Windows.Forms.GroupBox();
            this.txtidDisenioRev = new System.Windows.Forms.TextBox();
            this.Label17 = new System.Windows.Forms.Label();
            this.Label3 = new System.Windows.Forms.Label();
            this.Label6 = new System.Windows.Forms.Label();
            this.txtProfundidad = new System.Windows.Forms.TextBox();
            this.Label5 = new System.Windows.Forms.Label();
            this.txtMedida = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.Label4 = new System.Windows.Forms.Label();
            this.Label16 = new System.Windows.Forms.Label();
            this.ToolStripMenu.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.gpoMovimientos.SuspendLayout();
            this.gpoDestino.SuspendLayout();
            this.gpoOrigen.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPosicion)).BeginInit();
            this.gpoCaract.SuspendLayout();
            this.gpoRevitalizado.SuspendLayout();
            this.SuspendLayout();
            // 
            // ToolStripMenu
            // 
            this.ToolStripMenu.AutoSize = false;
            this.ToolStripMenu.BackColor = System.Drawing.SystemColors.Control;
            this.ToolStripMenu.Font = new System.Drawing.Font("Berlin Sans FB", 11.25F);
            this.ToolStripMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnNuevo,
            this.btnGrabar,
            this.btnCancelar,
            this.btnSalir});
            this.ToolStripMenu.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.ToolStripMenu.Location = new System.Drawing.Point(0, 0);
            this.ToolStripMenu.Name = "ToolStripMenu";
            this.ToolStripMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.ToolStripMenu.Size = new System.Drawing.Size(996, 54);
            this.ToolStripMenu.TabIndex = 20;
            this.ToolStripMenu.Text = "ToolStrip1";
            // 
            // btnNuevo
            // 
            this.btnNuevo.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnNuevo.Image = ((System.Drawing.Image)(resources.GetObject("btnNuevo.Image")));
            this.btnNuevo.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnNuevo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(54, 51);
            this.btnNuevo.Text = "&Nuevo";
            this.btnNuevo.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnNuevo.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnNuevo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnNuevo.Click += new System.EventHandler(this.btnNuevo_Click);
            // 
            // btnGrabar
            // 
            this.btnGrabar.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnGrabar.Image = ((System.Drawing.Image)(resources.GetObject("btnGrabar.Image")));
            this.btnGrabar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnGrabar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnGrabar.Name = "btnGrabar";
            this.btnGrabar.Size = new System.Drawing.Size(60, 51);
            this.btnGrabar.Text = "&Grabar";
            this.btnGrabar.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnGrabar.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnGrabar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnGrabar.Click += new System.EventHandler(this.btnGrabar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnCancelar.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar.Image")));
            this.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnCancelar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(69, 51);
            this.btnCancelar.Text = "&Cancelar";
            this.btnCancelar.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnCancelar.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnSalir.Image = ((System.Drawing.Image)(resources.GetObject("btnSalir.Image")));
            this.btnSalir.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnSalir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(55, 51);
            this.btnSalir.Text = "&S a l i r";
            this.btnSalir.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnSalir.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.gpoMovimientos);
            this.groupBox1.Controls.Add(this.gpoCaract);
            this.groupBox1.Location = new System.Drawing.Point(0, 54);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(987, 334);
            this.groupBox1.TabIndex = 23;
            this.groupBox1.TabStop = false;
            // 
            // gpoMovimientos
            // 
            this.gpoMovimientos.Controls.Add(this.label22);
            this.gpoMovimientos.Controls.Add(this.txtObservaciones);
            this.gpoMovimientos.Controls.Add(this.gpoDestino);
            this.gpoMovimientos.Controls.Add(this.gpoOrigen);
            this.gpoMovimientos.Controls.Add(this.cmbActividades);
            this.gpoMovimientos.Controls.Add(this.lblopera1);
            this.gpoMovimientos.Controls.Add(this.label1);
            this.gpoMovimientos.Controls.Add(this.txtidLlanta);
            this.gpoMovimientos.Location = new System.Drawing.Point(0, 10);
            this.gpoMovimientos.Name = "gpoMovimientos";
            this.gpoMovimientos.Size = new System.Drawing.Size(679, 309);
            this.gpoMovimientos.TabIndex = 127;
            this.gpoMovimientos.TabStop = false;
            this.gpoMovimientos.Text = "Movimientos";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.label22.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label22.Location = new System.Drawing.Point(240, 22);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(105, 18);
            this.label22.TabIndex = 154;
            this.label22.Text = "Observaciones:";
            // 
            // txtObservaciones
            // 
            this.txtObservaciones.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtObservaciones.Enabled = false;
            this.txtObservaciones.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtObservaciones.Location = new System.Drawing.Point(352, 19);
            this.txtObservaciones.MaxLength = 150;
            this.txtObservaciones.Multiline = true;
            this.txtObservaciones.Name = "txtObservaciones";
            this.txtObservaciones.Size = new System.Drawing.Size(313, 57);
            this.txtObservaciones.TabIndex = 153;
            // 
            // gpoDestino
            // 
            this.gpoDestino.Controls.Add(this.cmbPosicionDES);
            this.gpoDestino.Controls.Add(this.label19);
            this.gpoDestino.Controls.Add(this.txtidLlantaDES);
            this.gpoDestino.Controls.Add(this.label7);
            this.gpoDestino.Controls.Add(this.txtProfundidadFINAL_DES);
            this.gpoDestino.Controls.Add(this.label9);
            this.gpoDestino.Controls.Add(this.txtProfundidadDES);
            this.gpoDestino.Controls.Add(this.txtTipoUniDES);
            this.gpoDestino.Controls.Add(this.label10);
            this.gpoDestino.Controls.Add(this.label11);
            this.gpoDestino.Controls.Add(this.cmbidUbicacionDES);
            this.gpoDestino.Controls.Add(this.label15);
            this.gpoDestino.Controls.Add(this.label18);
            this.gpoDestino.Controls.Add(this.cmbidtipoUbicacionDES);
            this.gpoDestino.ForeColor = System.Drawing.Color.Blue;
            this.gpoDestino.Location = new System.Drawing.Point(344, 85);
            this.gpoDestino.Name = "gpoDestino";
            this.gpoDestino.Size = new System.Drawing.Size(329, 209);
            this.gpoDestino.TabIndex = 152;
            this.gpoDestino.TabStop = false;
            this.gpoDestino.Text = "DESTINO";
            // 
            // cmbPosicionDES
            // 
            this.cmbPosicionDES.AutoCompleteCustomSource.AddRange(new string[] {
            "Regularización",
            "Vivienda"});
            this.cmbPosicionDES.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.cmbPosicionDES.FormattingEnabled = true;
            this.cmbPosicionDES.Location = new System.Drawing.Point(95, 109);
            this.cmbPosicionDES.Name = "cmbPosicionDES";
            this.cmbPosicionDES.Size = new System.Drawing.Size(52, 26);
            this.cmbPosicionDES.TabIndex = 162;
            this.cmbPosicionDES.SelectedIndexChanged += new System.EventHandler(this.cmbPosicionDES_SelectedIndexChanged);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Blue;
            this.label19.Location = new System.Drawing.Point(153, 113);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(54, 18);
            this.label19.TabIndex = 161;
            this.label19.Text = "Llanta:";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtidLlantaDES
            // 
            this.txtidLlantaDES.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtidLlantaDES.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtidLlantaDES.Location = new System.Drawing.Point(213, 110);
            this.txtidLlantaDES.MaxLength = 150;
            this.txtidLlantaDES.Name = "txtidLlantaDES";
            this.txtidLlantaDES.Size = new System.Drawing.Size(108, 25);
            this.txtidLlantaDES.TabIndex = 160;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.label7.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label7.Location = new System.Drawing.Point(4, 177);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(137, 18);
            this.label7.TabIndex = 159;
            this.label7.Text = "Profundidad Actual";
            // 
            // txtProfundidadFINAL_DES
            // 
            this.txtProfundidadFINAL_DES.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtProfundidadFINAL_DES.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtProfundidadFINAL_DES.Location = new System.Drawing.Point(174, 174);
            this.txtProfundidadFINAL_DES.MaxLength = 50;
            this.txtProfundidadFINAL_DES.Name = "txtProfundidadFINAL_DES";
            this.txtProfundidadFINAL_DES.Size = new System.Drawing.Size(80, 25);
            this.txtProfundidadFINAL_DES.TabIndex = 158;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.label9.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label9.Location = new System.Drawing.Point(5, 145);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(143, 18);
            this.label9.TabIndex = 157;
            this.label9.Text = "Ultima Profundidad:";
            // 
            // txtProfundidadDES
            // 
            this.txtProfundidadDES.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtProfundidadDES.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtProfundidadDES.Location = new System.Drawing.Point(174, 142);
            this.txtProfundidadDES.MaxLength = 50;
            this.txtProfundidadDES.Name = "txtProfundidadDES";
            this.txtProfundidadDES.Size = new System.Drawing.Size(80, 25);
            this.txtProfundidadDES.TabIndex = 156;
            // 
            // txtTipoUniDES
            // 
            this.txtTipoUniDES.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTipoUniDES.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtTipoUniDES.Location = new System.Drawing.Point(96, 77);
            this.txtTipoUniDES.MaxLength = 100;
            this.txtTipoUniDES.Name = "txtTipoUniDES";
            this.txtTipoUniDES.ReadOnly = true;
            this.txtTipoUniDES.Size = new System.Drawing.Size(225, 25);
            this.txtTipoUniDES.TabIndex = 154;
            this.txtTipoUniDES.Tag = "1";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.label10.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label10.Location = new System.Drawing.Point(7, 78);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(66, 18);
            this.label10.TabIndex = 155;
            this.label10.Text = "Tipo Uni:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.label11.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label11.Location = new System.Drawing.Point(10, 113);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(63, 18);
            this.label11.TabIndex = 153;
            this.label11.Text = "Posición:";
            // 
            // cmbidUbicacionDES
            // 
            this.cmbidUbicacionDES.AutoCompleteCustomSource.AddRange(new string[] {
            "Regularización",
            "Vivienda"});
            this.cmbidUbicacionDES.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.cmbidUbicacionDES.FormattingEnabled = true;
            this.cmbidUbicacionDES.Location = new System.Drawing.Point(96, 45);
            this.cmbidUbicacionDES.Name = "cmbidUbicacionDES";
            this.cmbidUbicacionDES.Size = new System.Drawing.Size(225, 26);
            this.cmbidUbicacionDES.TabIndex = 150;
            this.cmbidUbicacionDES.SelectedIndexChanged += new System.EventHandler(this.cmbidUbicacionDES_SelectedIndexChanged);
            this.cmbidUbicacionDES.KeyUp += new System.Windows.Forms.KeyEventHandler(this.cmbidUbicacionDES_KeyUp);
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.label15.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label15.Location = new System.Drawing.Point(7, 47);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(83, 24);
            this.label15.TabIndex = 151;
            this.label15.Text = "Ubicacion";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.label18.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label18.Location = new System.Drawing.Point(15, 16);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(75, 18);
            this.label18.TabIndex = 149;
            this.label18.Text = "Ubicación:";
            // 
            // cmbidtipoUbicacionDES
            // 
            this.cmbidtipoUbicacionDES.AutoCompleteCustomSource.AddRange(new string[] {
            "Regularización",
            "Vivienda"});
            this.cmbidtipoUbicacionDES.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.cmbidtipoUbicacionDES.FormattingEnabled = true;
            this.cmbidtipoUbicacionDES.Location = new System.Drawing.Point(96, 13);
            this.cmbidtipoUbicacionDES.Name = "cmbidtipoUbicacionDES";
            this.cmbidtipoUbicacionDES.Size = new System.Drawing.Size(225, 26);
            this.cmbidtipoUbicacionDES.TabIndex = 148;
            // 
            // gpoOrigen
            // 
            this.gpoOrigen.Controls.Add(this.Label26);
            this.gpoOrigen.Controls.Add(this.txtProfundidadFINAL);
            this.gpoOrigen.Controls.Add(this.Label14);
            this.gpoOrigen.Controls.Add(this.txtProfundidadAct);
            this.gpoOrigen.Controls.Add(this.txtTipoUni);
            this.gpoOrigen.Controls.Add(this.Label12);
            this.gpoOrigen.Controls.Add(this.txtPosicion);
            this.gpoOrigen.Controls.Add(this.Label13);
            this.gpoOrigen.Controls.Add(this.cmbidUbicacion);
            this.gpoOrigen.Controls.Add(this.lblUbicacion);
            this.gpoOrigen.Controls.Add(this.Label21);
            this.gpoOrigen.Controls.Add(this.cmbidtipoUbicacion);
            this.gpoOrigen.ForeColor = System.Drawing.Color.Red;
            this.gpoOrigen.Location = new System.Drawing.Point(6, 85);
            this.gpoOrigen.Name = "gpoOrigen";
            this.gpoOrigen.Size = new System.Drawing.Size(329, 209);
            this.gpoOrigen.TabIndex = 151;
            this.gpoOrigen.TabStop = false;
            this.gpoOrigen.Text = "ORIGEN";
            // 
            // Label26
            // 
            this.Label26.AutoSize = true;
            this.Label26.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.Label26.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.Label26.Location = new System.Drawing.Point(3, 173);
            this.Label26.Name = "Label26";
            this.Label26.Size = new System.Drawing.Size(137, 18);
            this.Label26.TabIndex = 159;
            this.Label26.Text = "Profundidad Actual";
            // 
            // txtProfundidadFINAL
            // 
            this.txtProfundidadFINAL.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtProfundidadFINAL.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtProfundidadFINAL.Location = new System.Drawing.Point(175, 170);
            this.txtProfundidadFINAL.MaxLength = 50;
            this.txtProfundidadFINAL.Name = "txtProfundidadFINAL";
            this.txtProfundidadFINAL.Size = new System.Drawing.Size(80, 25);
            this.txtProfundidadFINAL.TabIndex = 158;
            // 
            // Label14
            // 
            this.Label14.AutoSize = true;
            this.Label14.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.Label14.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.Label14.Location = new System.Drawing.Point(7, 141);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(141, 18);
            this.Label14.TabIndex = 157;
            this.Label14.Text = "Ulima. Profundidad:";
            // 
            // txtProfundidadAct
            // 
            this.txtProfundidadAct.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtProfundidadAct.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtProfundidadAct.Location = new System.Drawing.Point(175, 138);
            this.txtProfundidadAct.MaxLength = 50;
            this.txtProfundidadAct.Name = "txtProfundidadAct";
            this.txtProfundidadAct.Size = new System.Drawing.Size(80, 25);
            this.txtProfundidadAct.TabIndex = 156;
            // 
            // txtTipoUni
            // 
            this.txtTipoUni.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTipoUni.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtTipoUni.Location = new System.Drawing.Point(96, 77);
            this.txtTipoUni.MaxLength = 100;
            this.txtTipoUni.Name = "txtTipoUni";
            this.txtTipoUni.ReadOnly = true;
            this.txtTipoUni.Size = new System.Drawing.Size(227, 25);
            this.txtTipoUni.TabIndex = 154;
            this.txtTipoUni.Tag = "1";
            // 
            // Label12
            // 
            this.Label12.AutoSize = true;
            this.Label12.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.Label12.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.Label12.Location = new System.Drawing.Point(7, 78);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(66, 18);
            this.Label12.TabIndex = 155;
            this.Label12.Text = "Tipo Uni:";
            // 
            // txtPosicion
            // 
            this.txtPosicion.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtPosicion.Location = new System.Drawing.Point(96, 108);
            this.txtPosicion.Name = "txtPosicion";
            this.txtPosicion.Size = new System.Drawing.Size(45, 25);
            this.txtPosicion.TabIndex = 152;
            // 
            // Label13
            // 
            this.Label13.AutoSize = true;
            this.Label13.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.Label13.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.Label13.Location = new System.Drawing.Point(10, 113);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(63, 18);
            this.Label13.TabIndex = 153;
            this.Label13.Text = "Posición:";
            // 
            // cmbidUbicacion
            // 
            this.cmbidUbicacion.AutoCompleteCustomSource.AddRange(new string[] {
            "Regularización",
            "Vivienda"});
            this.cmbidUbicacion.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.cmbidUbicacion.FormattingEnabled = true;
            this.cmbidUbicacion.Location = new System.Drawing.Point(96, 45);
            this.cmbidUbicacion.Name = "cmbidUbicacion";
            this.cmbidUbicacion.Size = new System.Drawing.Size(225, 26);
            this.cmbidUbicacion.TabIndex = 150;
            // 
            // lblUbicacion
            // 
            this.lblUbicacion.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.lblUbicacion.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblUbicacion.Location = new System.Drawing.Point(7, 47);
            this.lblUbicacion.Name = "lblUbicacion";
            this.lblUbicacion.Size = new System.Drawing.Size(83, 24);
            this.lblUbicacion.TabIndex = 151;
            this.lblUbicacion.Text = "Ubicacion";
            // 
            // Label21
            // 
            this.Label21.AutoSize = true;
            this.Label21.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.Label21.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.Label21.Location = new System.Drawing.Point(15, 16);
            this.Label21.Name = "Label21";
            this.Label21.Size = new System.Drawing.Size(75, 18);
            this.Label21.TabIndex = 149;
            this.Label21.Text = "Ubicación:";
            // 
            // cmbidtipoUbicacion
            // 
            this.cmbidtipoUbicacion.AutoCompleteCustomSource.AddRange(new string[] {
            "Regularización",
            "Vivienda"});
            this.cmbidtipoUbicacion.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.cmbidtipoUbicacion.FormattingEnabled = true;
            this.cmbidtipoUbicacion.Location = new System.Drawing.Point(96, 13);
            this.cmbidtipoUbicacion.Name = "cmbidtipoUbicacion";
            this.cmbidtipoUbicacion.Size = new System.Drawing.Size(225, 26);
            this.cmbidtipoUbicacion.TabIndex = 148;
            // 
            // cmbActividades
            // 
            this.cmbActividades.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.cmbActividades.FormattingEnabled = true;
            this.cmbActividades.Location = new System.Drawing.Point(97, 50);
            this.cmbActividades.Name = "cmbActividades";
            this.cmbActividades.Size = new System.Drawing.Size(248, 26);
            this.cmbActividades.TabIndex = 76;
            this.cmbActividades.SelectedIndexChanged += new System.EventHandler(this.cmbActividades_SelectedIndexChanged);
            this.cmbActividades.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbActividades_KeyPress);
            this.cmbActividades.KeyUp += new System.Windows.Forms.KeyEventHandler(this.cmbActividades_KeyUp);
            // 
            // lblopera1
            // 
            this.lblopera1.AutoSize = true;
            this.lblopera1.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblopera1.ForeColor = System.Drawing.Color.Blue;
            this.lblopera1.Location = new System.Drawing.Point(9, 53);
            this.lblopera1.Name = "lblopera1";
            this.lblopera1.Size = new System.Drawing.Size(82, 18);
            this.lblopera1.TabIndex = 77;
            this.lblopera1.Text = "Movimiento";
            this.lblopera1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(9, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 18);
            this.label1.TabIndex = 26;
            this.label1.Text = "Llanta:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtidLlanta
            // 
            this.txtidLlanta.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtidLlanta.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtidLlanta.Location = new System.Drawing.Point(97, 15);
            this.txtidLlanta.MaxLength = 150;
            this.txtidLlanta.Name = "txtidLlanta";
            this.txtidLlanta.Size = new System.Drawing.Size(108, 25);
            this.txtidLlanta.TabIndex = 25;
            this.txtidLlanta.TextChanged += new System.EventHandler(this.txtidLlanta_TextChanged);
            this.txtidLlanta.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtidLlanta_KeyUp);
            // 
            // gpoCaract
            // 
            this.gpoCaract.Controls.Add(this.txtidCondicion);
            this.gpoCaract.Controls.Add(this.txtidDisenio);
            this.gpoCaract.Controls.Add(this.txtidTipoLLanta);
            this.gpoCaract.Controls.Add(this.txtidMarca);
            this.gpoCaract.Controls.Add(this.txtidProductoLlanta);
            this.gpoCaract.Controls.Add(this.Label20);
            this.gpoCaract.Controls.Add(this.Label8);
            this.gpoCaract.Controls.Add(this.chkActivo);
            this.gpoCaract.Controls.Add(this.gpoRevitalizado);
            this.gpoCaract.Controls.Add(this.Label3);
            this.gpoCaract.Controls.Add(this.Label6);
            this.gpoCaract.Controls.Add(this.txtProfundidad);
            this.gpoCaract.Controls.Add(this.Label5);
            this.gpoCaract.Controls.Add(this.txtMedida);
            this.gpoCaract.Controls.Add(this.label2);
            this.gpoCaract.Controls.Add(this.Label4);
            this.gpoCaract.Controls.Add(this.Label16);
            this.gpoCaract.ForeColor = System.Drawing.Color.Black;
            this.gpoCaract.Location = new System.Drawing.Point(685, 19);
            this.gpoCaract.Name = "gpoCaract";
            this.gpoCaract.Size = new System.Drawing.Size(292, 300);
            this.gpoCaract.TabIndex = 126;
            this.gpoCaract.TabStop = false;
            this.gpoCaract.Text = "Características";
            // 
            // txtidCondicion
            // 
            this.txtidCondicion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtidCondicion.Enabled = false;
            this.txtidCondicion.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtidCondicion.Location = new System.Drawing.Point(106, 208);
            this.txtidCondicion.MaxLength = 50;
            this.txtidCondicion.Name = "txtidCondicion";
            this.txtidCondicion.Size = new System.Drawing.Size(112, 25);
            this.txtidCondicion.TabIndex = 155;
            // 
            // txtidDisenio
            // 
            this.txtidDisenio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtidDisenio.Enabled = false;
            this.txtidDisenio.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtidDisenio.Location = new System.Drawing.Point(106, 146);
            this.txtidDisenio.MaxLength = 50;
            this.txtidDisenio.Name = "txtidDisenio";
            this.txtidDisenio.Size = new System.Drawing.Size(112, 25);
            this.txtidDisenio.TabIndex = 154;
            // 
            // txtidTipoLLanta
            // 
            this.txtidTipoLLanta.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtidTipoLLanta.Enabled = false;
            this.txtidTipoLLanta.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtidTipoLLanta.Location = new System.Drawing.Point(106, 115);
            this.txtidTipoLLanta.MaxLength = 50;
            this.txtidTipoLLanta.Name = "txtidTipoLLanta";
            this.txtidTipoLLanta.Size = new System.Drawing.Size(169, 25);
            this.txtidTipoLLanta.TabIndex = 153;
            // 
            // txtidMarca
            // 
            this.txtidMarca.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtidMarca.Enabled = false;
            this.txtidMarca.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtidMarca.Location = new System.Drawing.Point(106, 53);
            this.txtidMarca.MaxLength = 50;
            this.txtidMarca.Name = "txtidMarca";
            this.txtidMarca.Size = new System.Drawing.Size(169, 25);
            this.txtidMarca.TabIndex = 152;
            // 
            // txtidProductoLlanta
            // 
            this.txtidProductoLlanta.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtidProductoLlanta.Enabled = false;
            this.txtidProductoLlanta.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtidProductoLlanta.Location = new System.Drawing.Point(106, 19);
            this.txtidProductoLlanta.MaxLength = 50;
            this.txtidProductoLlanta.Name = "txtidProductoLlanta";
            this.txtidProductoLlanta.Size = new System.Drawing.Size(70, 25);
            this.txtidProductoLlanta.TabIndex = 151;
            // 
            // Label20
            // 
            this.Label20.AutoSize = true;
            this.Label20.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.Label20.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.Label20.Location = new System.Drawing.Point(6, 211);
            this.Label20.Name = "Label20";
            this.Label20.Size = new System.Drawing.Size(70, 18);
            this.Label20.TabIndex = 150;
            this.Label20.Text = "Condición";
            // 
            // Label8
            // 
            this.Label8.AutoSize = true;
            this.Label8.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.Label8.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.Label8.Location = new System.Drawing.Point(190, 25);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(52, 18);
            this.Label8.TabIndex = 145;
            this.Label8.Text = "Activo:";
            // 
            // chkActivo
            // 
            this.chkActivo.AutoSize = true;
            this.chkActivo.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.chkActivo.Location = new System.Drawing.Point(260, 25);
            this.chkActivo.Name = "chkActivo";
            this.chkActivo.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.chkActivo.Size = new System.Drawing.Size(15, 14);
            this.chkActivo.TabIndex = 14;
            this.chkActivo.UseVisualStyleBackColor = true;
            // 
            // gpoRevitalizado
            // 
            this.gpoRevitalizado.Controls.Add(this.txtidDisenioRev);
            this.gpoRevitalizado.Controls.Add(this.Label17);
            this.gpoRevitalizado.ForeColor = System.Drawing.Color.Black;
            this.gpoRevitalizado.Location = new System.Drawing.Point(9, 239);
            this.gpoRevitalizado.Name = "gpoRevitalizado";
            this.gpoRevitalizado.Size = new System.Drawing.Size(256, 53);
            this.gpoRevitalizado.TabIndex = 148;
            this.gpoRevitalizado.TabStop = false;
            this.gpoRevitalizado.Text = "Revitalizado";
            // 
            // txtidDisenioRev
            // 
            this.txtidDisenioRev.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtidDisenioRev.Enabled = false;
            this.txtidDisenioRev.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtidDisenioRev.Location = new System.Drawing.Point(93, 14);
            this.txtidDisenioRev.MaxLength = 50;
            this.txtidDisenioRev.Name = "txtidDisenioRev";
            this.txtidDisenioRev.Size = new System.Drawing.Size(146, 25);
            this.txtidDisenioRev.TabIndex = 138;
            // 
            // Label17
            // 
            this.Label17.AutoSize = true;
            this.Label17.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.Label17.Location = new System.Drawing.Point(39, 22);
            this.Label17.Name = "Label17";
            this.Label17.Size = new System.Drawing.Size(43, 13);
            this.Label17.TabIndex = 136;
            this.Label17.Text = "Diseño:";
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.Label3.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.Label3.Location = new System.Drawing.Point(3, 118);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(86, 18);
            this.Label3.TabIndex = 147;
            this.Label3.Text = "Tipo Llanta:";
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.Label6.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.Label6.Location = new System.Drawing.Point(5, 180);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(95, 18);
            this.Label6.TabIndex = 133;
            this.Label6.Text = "Profundidad:";
            // 
            // txtProfundidad
            // 
            this.txtProfundidad.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtProfundidad.Enabled = false;
            this.txtProfundidad.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtProfundidad.Location = new System.Drawing.Point(106, 177);
            this.txtProfundidad.MaxLength = 50;
            this.txtProfundidad.Name = "txtProfundidad";
            this.txtProfundidad.Size = new System.Drawing.Size(112, 25);
            this.txtProfundidad.TabIndex = 110;
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.Label5.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.Label5.Location = new System.Drawing.Point(6, 87);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(61, 18);
            this.Label5.TabIndex = 131;
            this.Label5.Text = "Medida:";
            // 
            // txtMedida
            // 
            this.txtMedida.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMedida.Enabled = false;
            this.txtMedida.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtMedida.Location = new System.Drawing.Point(106, 84);
            this.txtMedida.MaxLength = 50;
            this.txtMedida.Name = "txtMedida";
            this.txtMedida.Size = new System.Drawing.Size(70, 25);
            this.txtMedida.TabIndex = 109;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.label2.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label2.Location = new System.Drawing.Point(6, 149);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 18);
            this.label2.TabIndex = 128;
            this.label2.Text = "Diseño:";
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.Label4.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.Label4.Location = new System.Drawing.Point(15, 57);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(52, 18);
            this.Label4.TabIndex = 126;
            this.Label4.Text = "Marca:";
            // 
            // Label16
            // 
            this.Label16.AutoSize = true;
            this.Label16.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.Label16.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.Label16.Location = new System.Drawing.Point(3, 22);
            this.Label16.Name = "Label16";
            this.Label16.Size = new System.Drawing.Size(83, 18);
            this.Label16.TabIndex = 124;
            this.Label16.Text = "Id Producto";
            // 
            // fMovllantas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(996, 402);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.ToolStripMenu);
            this.Name = "fMovllantas";
            this.Text = "Movimientos de Llantas";
            this.Load += new System.EventHandler(this.fMovllantas_Load);
            this.ToolStripMenu.ResumeLayout(false);
            this.ToolStripMenu.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.gpoMovimientos.ResumeLayout(false);
            this.gpoMovimientos.PerformLayout();
            this.gpoDestino.ResumeLayout(false);
            this.gpoDestino.PerformLayout();
            this.gpoOrigen.ResumeLayout(false);
            this.gpoOrigen.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPosicion)).EndInit();
            this.gpoCaract.ResumeLayout(false);
            this.gpoCaract.PerformLayout();
            this.gpoRevitalizado.ResumeLayout(false);
            this.gpoRevitalizado.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.ToolStrip ToolStripMenu;
        private System.Windows.Forms.ToolStripButton btnNuevo;
        private System.Windows.Forms.ToolStripButton btnGrabar;
        private System.Windows.Forms.ToolStripButton btnCancelar;
        private System.Windows.Forms.ToolStripButton btnSalir;
        private System.Windows.Forms.GroupBox groupBox1;
        internal System.Windows.Forms.GroupBox gpoCaract;
        internal System.Windows.Forms.TextBox txtidTipoLLanta;
        internal System.Windows.Forms.TextBox txtidMarca;
        internal System.Windows.Forms.TextBox txtidProductoLlanta;
        internal System.Windows.Forms.Label Label20;
        internal System.Windows.Forms.Label Label8;
        internal System.Windows.Forms.CheckBox chkActivo;
        internal System.Windows.Forms.GroupBox gpoRevitalizado;
        internal System.Windows.Forms.Label Label17;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.Label Label6;
        internal System.Windows.Forms.TextBox txtProfundidad;
        internal System.Windows.Forms.Label Label5;
        internal System.Windows.Forms.TextBox txtMedida;
        internal System.Windows.Forms.Label label2;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.Label Label16;
        internal System.Windows.Forms.TextBox txtidCondicion;
        internal System.Windows.Forms.TextBox txtidDisenio;
        private System.Windows.Forms.GroupBox gpoMovimientos;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtidLlanta;
        internal System.Windows.Forms.TextBox txtidDisenioRev;
        private System.Windows.Forms.ComboBox cmbActividades;
        private System.Windows.Forms.Label lblopera1;
        internal System.Windows.Forms.GroupBox gpoOrigen;
        internal System.Windows.Forms.Label Label26;
        internal System.Windows.Forms.TextBox txtProfundidadFINAL;
        internal System.Windows.Forms.Label Label14;
        internal System.Windows.Forms.TextBox txtProfundidadAct;
        internal System.Windows.Forms.TextBox txtTipoUni;
        internal System.Windows.Forms.Label Label12;
        internal System.Windows.Forms.NumericUpDown txtPosicion;
        internal System.Windows.Forms.Label Label13;
        internal System.Windows.Forms.Label lblUbicacion;
        internal System.Windows.Forms.Label Label21;
        internal System.Windows.Forms.ComboBox cmbidtipoUbicacion;
        internal System.Windows.Forms.GroupBox gpoDestino;
        internal System.Windows.Forms.Label label7;
        internal System.Windows.Forms.TextBox txtProfundidadFINAL_DES;
        internal System.Windows.Forms.Label label9;
        internal System.Windows.Forms.TextBox txtProfundidadDES;
        internal System.Windows.Forms.TextBox txtTipoUniDES;
        internal System.Windows.Forms.Label label10;
        internal System.Windows.Forms.Label label11;
        internal System.Windows.Forms.ComboBox cmbidUbicacionDES;
        internal System.Windows.Forms.Label label15;
        internal System.Windows.Forms.Label label18;
        internal System.Windows.Forms.ComboBox cmbidtipoUbicacionDES;
        internal System.Windows.Forms.ComboBox cmbidUbicacion;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtidLlantaDES;
        internal System.Windows.Forms.ComboBox cmbPosicionDES;
        internal System.Windows.Forms.Label label22;
        internal System.Windows.Forms.TextBox txtObservaciones;
    }
}