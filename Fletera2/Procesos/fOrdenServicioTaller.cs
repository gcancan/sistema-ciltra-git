﻿using Core.Models;
using Fletera2.Clases;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Fletera2.Clases.Inicio;
using Fletera2Negocio;
using Fletera2Entidades;
using Fletera2.Comunes;
using Fletera;

namespace Fletera2.Procesos
{
    public partial class fOrdenServicioTaller : Form
    {
        eTipoOrdenServicio _TipoOS;
        //valido
        eTipoOrdenServicio TipoOS;
        eEstatusOS _estatus;
        eEstatusOS estatusValido;
        Usuario _Usuario;

        OperationResult resp;
        CabOrdenServicio cabos;
        DetOrdenServicio detos;
        DetRecepcionOS detros;
        List<DetOrdenServicio> listaDetOS;
        List<DetRecepcionOS> listaDetROS;
        List<DetRecepcionOS> listaDetROS_Asigna;

        List<CatPersonal> listaRecibe;
        List<CatPersonal> listaRecepciona;

        List<CatPersonal> listaResponsable;
        List<CatPersonal> listaAyudante1;
        List<CatPersonal> listaAyudante2;

        List<CabPreOC> listCabPreOC;
        List<DetPreOC> listDetPreOC;

        //CabPreSalida cabpresal;
        List<CabPreSalida> listCabPresal;
        List<DetPreSalida> listDetPresal;

        //PARA FINALIZAR
        string MensajeNoValido = "";

        //PARA CONVERTIR A ACTIVIDAD
        List<CatUnidadTrans> listaUnidadesaEnviar;

        int Indice;
        int IdOrdenTrabajo = 0;

        CabPreSalida cabpresal;
        bool BandExito;
        string PreSalaCancelar = "";

        int contVerTer = 0;

        //para llantas
        CatActividades actividad;

        

        public fOrdenServicioTaller(eEstatusOS estatus, eTipoOrdenServicio TipoOS, Usuario User)
        {
            _TipoOS = TipoOS;
            _estatus = estatus;
            _Usuario = User;
            InitializeComponent();
        }



        private void fOrdenServicioTaller_Load(object sender, EventArgs e)
        {
            EncabezadoPantalla();

            Cancelar();
            estatusValido = DeterminaEstatusValido(_estatus);
            CargaGpoOpera();

            FormatoGeneral(grdOT);
            FormatoGeneral(grdCabOC);
            FormatoGeneral(grdDetOC);
            FormatoGeneral(grdCabPreSalida);
            FormatoGeneral(grdDetPreSalida);
            
            if (_TipoOS == eTipoOrdenServicio.LLANTAS_ESP)
            {
                TipoOS = eTipoOrdenServicio.LLANTAS;
            }
            else
            {
                TipoOS = _TipoOS;
            }

        }

        private void Cancelar()
        {
            HabilitaBotones(opcHabilita.DesHabilitaTodos);
            HabilitaCampos(opcHabilita.DesHabilitaTodos);
            LimpiaCampos();
            LimpiaCamposOpera();
        }

        private void LimpiaCamposOpera()
        {
            switch (_estatus)
            {
                case eEstatusOS.PRE:
                    break;
                case eEstatusOS.REC:
                    cmbOpera1.DataSource = null;
                    break;
                case eEstatusOS.ASIG:
                    cmbOpera1.DataSource = null;
                    cmbOpera2.DataSource = null;
                    cmbOpera3.DataSource = null;

                    cmbDetOpera1.DataSource = null;
                    cmbDetOpera2.DataSource = null;
                    cmbDetOpera3.DataSource = null;
                    break;
                case eEstatusOS.TER:
                    cmbOpera1.DataSource = null;
                    break;
                case eEstatusOS.ENT:
                    cmbOpera1.DataSource = null;
                    break;
                case eEstatusOS.CAN:
                    break;
                default:
                    break;
            }
        }
        private void LimpiaCampos()
        {
            txtidOrdenSer.Clear();
            txtidPadreOrdSer.Clear();
            txtOrdenDe.Clear();
            txtidTipOrdServ.Clear();
            txtIdEmpresa.Clear();
            fechaCaptura.Value = DateTime.Now;
            fechaCapturaHora.Value = DateTime.Now;
            txtTipoUnidad.Clear();
            txtidUnidadTrans.Clear();
            txtidOperador.Clear();
            txtidEmpleadoAutoriza.Clear();
            txtEstatus.Clear();
            dtpFechaEstatus.Value = DateTime.Now;
            dtpFechaEstatusHora.Value = DateTime.Now;

            grdOT.DataSource = null;
            chkMarcaTodos.Checked = false;

        }

        private void HabilitaBotones(opcHabilita opc)
        {
            try
            {
                switch (opc)
                {
                    case opcHabilita.DesHabilitaTodos:
                        btnNuevo.Enabled = true;
                        btnGrabar.Enabled = false;
                        btnCancelar.Enabled = false;
                        btnSalir.Enabled = true;
                        break;
                    case opcHabilita.Iniciando:
                        break;
                    case opcHabilita.Editando:
                        break;
                    case opcHabilita.Nuevo:
                        btnNuevo.Enabled = false;
                        btnGrabar.Enabled = true;
                        btnCancelar.Enabled = true;
                        btnSalir.Enabled = false;

                        break;
                    case opcHabilita.Reporte:
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + ex.Message,
                this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void HabilitaCampos(opcHabilita opc)
        {
            try
            {
                switch (opc)
                {
                    case opcHabilita.DesHabilitaTodos:
                        gpoDatos.Enabled = false;
                        tbsDet.Enabled = false;
                        gpoOpera.Enabled = false;
                        break;
                    case opcHabilita.Iniciando:
                        gpoDatos.Enabled = false;
                        gpoOpera.Enabled = true;
                        tbsDet.Enabled = true;

                        break;
                    case opcHabilita.Editando:
                        break;
                    case opcHabilita.Nuevo:
                        gpoDatos.Enabled = true;
                        gpoOpera.Enabled = false;
                        tbsDet.Enabled = false;
                        break;
                    case opcHabilita.Reporte:
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + ex.Message,
                this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            HabilitaCampos(opcHabilita.Nuevo);
            HabilitaBotones(opcHabilita.Nuevo);
            LimpiaCampos();
            txtidOrdenSer.Focus();
        }

        private void txtidOrdenSer_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                if (txtidOrdenSer.Text != "")
                {
                    CargaForma(opcBusqueda.CabOrdenServicio, txtidOrdenSer.Text);
                    if (_TipoOS == eTipoOrdenServicio.LLANTAS_ESP)
                    {
                        chkMarcaTodos.Checked = true;
                    }
                }
                else
                {
                    txtidOrdenSer.Focus();
                }
            }
        }

        private void CargaForma(opcBusqueda opc, string Valor)
        {
            try
            {
                if (opc == opcBusqueda.CabOrdenServicio)
                {

                    resp = new CabOrdenServicioSvc().getCabOrdenServicioxFilter(Convert.ToInt32(Valor));
                    if (resp.typeResult == ResultTypes.success)
                    {
                        cabos = ((List<CabOrdenServicio>)resp.result)[0];

                        if (cabos.Estatus == estatusValido.ToString()
                        && (eTipoOrdenServicio)cabos.idTipOrdServ == TipoOS)
                        {
                            txtidPadreOrdSer.Text = cabos.idPadreOrdSer.ToString();
                            txtOrdenDe.Text = cabos.NomTipOrdServ;
                            txtidTipOrdServ.Text = cabos.NomTipoServicio;
                            txtIdEmpresa.Text = cabos.RazonSocial;
                            fechaCaptura.Value = cabos.fechaCaptura;
                            fechaCapturaHora.Value = cabos.fechaCaptura;
                            txtTipoUnidad.Text = cabos.NomTipoUnidad;
                            txtidUnidadTrans.Text = cabos.idUnidadTrans;
                            txtidOperador.Text = cabos.NomOperador;
                            txtidEmpleadoAutoriza.Text = cabos.NomEmpleadoAutoriza;
                            txtEstatus.Text = cabos.Estatus;
                            switch (estatusValido)
                            {
                                case eEstatusOS.PRE:
                                    if (_TipoOS == eTipoOrdenServicio.LLANTAS_ESP)
                                    {
                                        dtpFechaEstatus.Value = cabos.fechaCaptura;
                                        dtpFechaEstatusHora.Value = cabos.fechaCaptura;

                                        LlenaComboEntrega();
                                        SelComboOper1(cabos.idOperarador);

                                        listaResponsable = LlenaComboAsigna(cmbOpera2, cmbDetOpera2);
                                        listaAyudante1 = LlenaComboAsigna(cmbOpera3, cmbDetOpera3);

                                        

                                        cmbOpera1.Focus();

                                        
                                    }
                                    else
                                    {
                                        dtpFechaEstatus.Value = cabos.fechaCaptura;
                                        dtpFechaEstatusHora.Value = cabos.fechaCaptura;

                                        LlenaComboEntrega();
                                        SelComboOper1(cabos.idOperarador);
                                        cmbOpera1.Focus();
                                    }
                                    break;
                                case eEstatusOS.REC:
                                    dtpFechaEstatus.Value = (DateTime)cabos.FechaRecepcion;
                                    dtpFechaEstatusHora.Value = (DateTime)cabos.FechaRecepcion;

                                    listaResponsable = LlenaComboAsigna(cmbOpera1, cmbDetOpera1);
                                    listaAyudante1 = LlenaComboAsigna(cmbOpera2, cmbDetOpera2);
                                    listaAyudante2 = LlenaComboAsigna(cmbOpera3, cmbDetOpera3);

                                    //btnAsigna.Enabled = true;
                                    cmbOpera1.Focus();

                                    break;
                                case eEstatusOS.ASIG:

                                    dtpFechaEstatus.Value = (DateTime)cabos.FechaAsignado;
                                    dtpFechaEstatusHora.Value = (DateTime)cabos.FechaAsignado;

                                    //Ordenes de compra
                                    OperationResult rescoc = new OrdenCompraSvc().getCabPreOCxFilter(0, " idOrdenTrabajo IN (SELECT idOrdenTrabajo FROM dbo.DetOrdenServicio WHERE idOrdenSer = " + Convert.ToInt32(Valor) + ")");
                                    if (rescoc.typeResult == ResultTypes.success)
                                    {
                                        listCabPreOC = (List<CabPreOC>)rescoc.result;
                                        // List<> ;
                                        grdCabOC.DataSource = null;
                                        grdCabOC.DataSource = listCabPreOC;
                                        //FormatoGrid
                                        OperationResult resdoc = new OrdenCompraSvc().getDetPreOCxFilter(0, " IdPreOC IN (SELECT IdPreOC FROM dbo.CabPreOC WHERE idOrdenTrabajo IN (SELECT idOrdenTrabajo FROM dbo.DetOrdenServicio WHERE idOrdenSer = " + Convert.ToInt32(Valor) + "))");
                                        if (resdoc.typeResult == ResultTypes.success)
                                        {
                                            listDetPreOC = (List<DetPreOC>)resdoc.result;
                                            grdDetOC.DataSource = null;
                                            grdDetOC.DataSource = listDetPreOC;
                                            FiltraOrdenesCompra(0);
                                            //FormatoGrid
                                        }
                                        FormatogrdCabOC();
                                        FormatogrdDetOC();
                                    }
                                    //Presalidas de Almacen
                                    OperationResult rescps = new PreSalidasSvc().getCabPreSalidaxFilter(0, " c.idOrdenTrabajo IN (SELECT idOrdenTrabajo FROM dbo.DetOrdenServicio WHERE idOrdenSer = " + Convert.ToInt32(Valor) + ")");
                                    if (rescps.typeResult == ResultTypes.success)
                                    {
                                        listCabPresal = (List<CabPreSalida>)rescps.result;
                                        grdCabPreSalida.DataSource = null;
                                        grdCabPreSalida.DataSource = listCabPresal;
                                        OperationResult resdps = new PreSalidasSvc().getDetPreSalidaxFilter(0, " d.IdPreSalida IN (SELECT IdPreSalida FROM dbo.CabPreSalida WHERE idOrdenTrabajo IN (SELECT idOrdenTrabajo FROM dbo.DetOrdenServicio WHERE idOrdenSer = " + Convert.ToInt32(Valor) + "))");
                                        if (resdps.typeResult == ResultTypes.success)
                                        {
                                            listDetPresal = (List<DetPreSalida>)resdps.result;
                                            grdDetPreSalida.DataSource = null;
                                            grdDetPreSalida.DataSource = listDetPresal;
                                            FiltraSalidasAlmacen(0);

                                        }
                                        FormatogrdCabPreSalida();
                                        FormatogrdDetPreSalida();
                                    }
                                    break;
                                case eEstatusOS.TER:
                                    dtpFechaEstatus.Value = (DateTime)cabos.FechaTerminado;
                                    dtpFechaEstatusHora.Value = (DateTime)cabos.FechaTerminado;

                                    LlenaComboRecibe();
                                    SelComboOper1(cabos.idOperarador);
                                    cmbOpera1.Focus();
                                    break;
                                case eEstatusOS.ENT:
                                    break;
                                case eEstatusOS.CAN:
                                    break;
                                default:
                                    break;
                            }

                            //Cargar Detalle Orden de Servicio

                            resp = new DetOrdenServicioSvc().getDetOrdenServicioxFilter(Convert.ToInt32(Valor), " not d.Estatus = 'CAN' ");
                            if (resp.typeResult == ResultTypes.success)
                            {
                                listaDetOS = (List<DetOrdenServicio>)resp.result;
                                grdOT.DataSource = null;
                                grdOT.DataSource = listaDetOS;
                                FormatogrdDet();
                            }
                            else
                            {
                                //no debe de pasar
                            }

                            //Cargar Detalle Recepcion Orden de Servicio
                            resp = new DetRecepcionOSSvc().getDetOrdenServicioxFilter(Convert.ToInt32(Valor), " NOT dos.Estatus = 'CAN'");
                            if (resp.typeResult == ResultTypes.success)
                            {
                                listaDetROS = (List<DetRecepcionOS>)resp.result;
                            }
                            else
                            {
                                listaDetROS = null;
                            }

                            //solo en caso de TERMINACION
                            if (_estatus == eEstatusOS.TER)
                            {
                                //VerificacionTerminacion();
                            }

                            HabilitaCampos(opcHabilita.Iniciando);

                        }
                        else
                        {
                            string vnomEstatus = Inicio.NomEstatus(cabos.Estatus);
                            //mensaje
                            MessageBox.Show("La Orden de Servicio: " + txtidOrdenSer.Text + " se encuentra con el estatus: " + vnomEstatus, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtidOrdenSer.SelectAll();
                            txtidOrdenSer.Focus();
                            return;
                            //btnCancelar.PerformClick();
                            //btnNuevo.PerformClick();
                        }
                    }
                    else
                    {
                        //no se encontro
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + ex.Message,
                   this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private eEstatusOS DeterminaEstatusValido(eEstatusOS estatus)
        {
            switch (estatus)
            {
                case eEstatusOS.PRE:
                    return eEstatusOS.PRE;
                case eEstatusOS.REC:
                    return eEstatusOS.PRE;
                case eEstatusOS.ASIG:
                    return eEstatusOS.REC;
                case eEstatusOS.TER:
                    if (_TipoOS == eTipoOrdenServicio.LLANTAS_ESP)
                    {
                        return eEstatusOS.PRE;
                    }
                    else
                    {
                        return eEstatusOS.ASIG;
                    }
                    
                    
                case eEstatusOS.ENT:
                    return eEstatusOS.TER;
                case eEstatusOS.CAN:
                    return eEstatusOS.ASIG;
                default:
                    return eEstatusOS.PRE;
            }
        }

        private void txtidOrdenSer_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Cancelar();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }
        private void FormatoGeneral(DataGridView grid)
        {
            //ESTILO GENERAL
            grid.RowsDefaultCellStyle.BackColor = Color.White;
            grid.AlternatingRowsDefaultCellStyle.BackColor = Color.AliceBlue;

            grid.RowsDefaultCellStyle.SelectionForeColor = Color.Yellow;
            grid.RowsDefaultCellStyle.SelectionBackColor = Color.Gray;
            grid.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            grid.DefaultCellStyle.Font = new Font("Berlin Sans FB", 12);
            grid.ColumnHeadersDefaultCellStyle.Font = new Font("Berlin Sans FB", 12);
            grid.ColumnHeadersDefaultCellStyle.BackColor = Color.Yellow;
            grid.ColumnHeadersDefaultCellStyle.ForeColor = Color.Red;
            grid.EnableHeadersVisualStyles = false;
        }

        private void FormatogrdDet()
        {
            //OCULTAR COLUMNAS
            grdOT.Columns["idOrdenActividad"].Visible = false;
            grdOT.Columns["DuracionActHr"].Visible = false;
            grdOT.Columns["NoPersonal"].Visible = false;
            grdOT.Columns["id_proveedor"].Visible = false;
            grdOT.Columns["Estatus"].Visible = false;
            grdOT.Columns["CostoManoObra"].Visible = false;
            grdOT.Columns["CostoProductos"].Visible = false;
            grdOT.Columns["FechaDiag"].Visible = false;
            grdOT.Columns["FechaAsig"].Visible = false;
            grdOT.Columns["UsuarioAsig"].Visible = false;
            grdOT.Columns["FechaPausado"].Visible = false;
            grdOT.Columns["NotaPausado"].Visible = false;
            grdOT.Columns["FechaTerminado"].Visible = false;
            grdOT.Columns["UsuarioTerminado"].Visible = false;
            grdOT.Columns["idDivision"].Visible = false;
            grdOT.Columns["NotaDiagnostico"].Visible = false;
            grdOT.Columns["FaltInsumo"].Visible = false;
            grdOT.Columns["Pendiente"].Visible = false;
            grdOT.Columns["CIDPRODUCTO_SERV"].Visible = false;
            grdOT.Columns["Cantidad_CIDPRODUCTO_SERV"].Visible = false;
            grdOT.Columns["Precio_CIDPRODUCTO_SERV"].Visible = false;
            grdOT.Columns["idFamilia"].Visible = false;
            grdOT.Columns["isCancelado"].Visible = false;
            grdOT.Columns["motivoCancelacion"].Visible = false;
            grdOT.Columns["idPersonalResp"].Visible = false;
            grdOT.Columns["idPersonalAyu1"].Visible = false;
            grdOT.Columns["idPersonalAyu2"].Visible = false;
            grdOT.Columns["NotaRecepcion"].Visible = false;
            grdOT.Columns["NotaRecepcionA"].Visible = false;
            grdOT.Columns["idOrdenSer"].Visible = false;
            grdOT.Columns["idServicio"].Visible = false;
            grdOT.Columns["idActividad"].Visible = false;
            grdOT.Columns["usuarioDiag"].Visible = false;
            grdOT.Columns["NomServicio"].Visible = false;
            grdOT.Columns["FallaReportada"].Visible = false;
            grdOT.Columns["CIDPRODUCTO_ACT"].Visible = false;
            grdOT.Columns["DetRec"].Visible = false;
            

            if (_TipoOS == eTipoOrdenServicio.LLANTAS || _TipoOS == eTipoOrdenServicio.LLANTAS_ESP)
            {
                grdOT.Columns["idLlanta"].Visible = true;
                grdOT.Columns["PosLlanta"].Visible = true;
            }
            else
            {
                grdOT.Columns["idLlanta"].Visible = false;
                grdOT.Columns["PosLlanta"].Visible = false;

            }

            if (_estatus == eEstatusOS.ASIG)
            {
                grdOT.Columns["seleccionado"].Visible = true;
                grdOT.Columns["NomPersonalResp"].Visible = true;
                grdOT.Columns["NomPersonalAyu1"].Visible = true;
                grdOT.Columns["NomPersonalAyu2"].Visible = true;
            }
            else
            {
                grdOT.Columns["seleccionado"].Visible = false;
                grdOT.Columns["NomPersonalResp"].Visible = false;
                grdOT.Columns["NomPersonalAyu1"].Visible = false;
                grdOT.Columns["NomPersonalAyu2"].Visible = false;

            }
            if (_estatus == eEstatusOS.TER)
            {
                grdOT.Columns["seleccionado"].Visible = false;
                grdOT.Columns["NomPersonalResp"].Visible = true;
                grdOT.Columns["NomPersonalAyu1"].Visible = true;
                grdOT.Columns["NomPersonalAyu2"].Visible = true;
            }


            //HACER READONLY COLUMNAS
            //grdOT.Columns["Seleccionado"].ReadOnly = true;

            //ENCABEZADO DE COLUMNAS
            //grdOT.Columns["idOrdenSer"].HeaderText = "idOrdenSer";
            //grdOT.Columns["idServicio"].HeaderText = "idServicio";
            //grdOT.Columns["idActividad"].HeaderText = "idActividad";
            if (_estatus == eEstatusOS.ASIG)
            {
                grdOT.Columns["Seleccionado"].HeaderText = "Asignar";
            }
            grdOT.Columns["idOrdenTrabajo"].HeaderText = "No.";
            grdOT.Columns["idLlanta"].HeaderText = "Llanta";
            grdOT.Columns["PosLlanta"].HeaderText = "Posición";

            grdOT.Columns["NotaRecepcionF"].HeaderText = "Descripción";
            grdOT.Columns["FallaReportada"].HeaderText = "Falla Reportada";
            grdOT.Columns["NomServicio"].HeaderText = "Servicio";
            grdOT.Columns["TipoNotaRecepcionF"].HeaderText = "Tipo";

            grdOT.Columns["NomPersonalResp"].HeaderText = "Responsable";
            grdOT.Columns["NomPersonalAyu1"].HeaderText = "Ayudante 1";
            grdOT.Columns["NomPersonalAyu2"].HeaderText = "Ayudante 2";
            //grdOT.Columns["VolDescarga"].HeaderText = "Volumen";
            //grdOT.Columns["idCargaGasolina"].HeaderText = "Vale Combustible";
            //grdOT.Columns["precioOperador"].HeaderText = "Importe";
            //grdOT.Columns["nombreOrigen"].HeaderText = "Origen";
            //grdOT.Columns["Destino"].HeaderText = "Destino";
            //grdOT.Columns["Producto"].HeaderText = "Producto";


            //FORMATO DE COLUMNAS
            //grdDatos.Columns["Precio"].DefaultCellStyle.Format = "C2";

            //ALINEACION DE COLUMNAS
            //grdDatos.Columns["Precio"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight;

            //TAMAÑO AUTOMATICO DE COLUMNAS
            //grdOT.Columns["idOrdenSer"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            //grdOT.Columns["idServicio"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            //grdOT.Columns["idActividad"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdOT.Columns["seleccionado"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdOT.Columns["idOrdenTrabajo"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

            grdOT.Columns["idLlanta"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdOT.Columns["PosLlanta"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

            grdOT.Columns["NotaRecepcionF"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdOT.Columns["FallaReportada"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdOT.Columns["NomServicio"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdOT.Columns["TipoNotaRecepcionF"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

            grdOT.Columns["NomPersonalResp"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdOT.Columns["NomPersonalAyu1"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdOT.Columns["NomPersonalAyu2"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            //grdOT.Columns["idCargaGasolina"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            //grdOT.Columns["precioOperador"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            //grdOT.Columns["nombreOrigen"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            //grdOT.Columns["Destino"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            //grdOT.Columns["Producto"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

            if (_estatus == eEstatusOS.REC)
            {
                grdOT.Columns["idOrdenTrabajo"].DisplayIndex = 0;
                grdOT.Columns["NotaRecepcionF"].DisplayIndex = 1;
                grdOT.Columns["TipoNotaRecepcionF"].DisplayIndex = 2;

            }
            else if (_estatus == eEstatusOS.ASIG)
            {
                grdOT.Columns["seleccionado"].DisplayIndex = 0;
                grdOT.Columns["idOrdenTrabajo"].DisplayIndex = 1;
                grdOT.Columns["NotaRecepcionF"].DisplayIndex = 2;
                grdOT.Columns["TipoNotaRecepcionF"].DisplayIndex = 3;

            }


        }


        private void LlenaComboEntrega()
        {
            try
            {
                resp = new CatPersonalSvc().getCatPersonalxFiltro(" per.idpuesto in (7,36,37)");
                if (resp.typeResult == ResultTypes.success)
                {
                    listaRecepciona = (List<CatPersonal>)resp.result;
                    cmbOpera1.DataSource = null;
                    cmbOpera1.DataSource = listaRecepciona;

                    cmbOpera1.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                    cmbOpera1.AutoCompleteSource = AutoCompleteSource.ListItems;
                    cmbOpera1.DisplayMember = "NombreCompleto";
                    cmbOpera1.SelectedIndex = 0;
                }
                else
                {
                    //no se encontro
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + ex.Message,
                  this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void SelComboOper1(int idPersonal)
        {
            try
            {
                int i = 0;
                foreach (CatPersonal item in cmbOpera1.Items)
                {
                    if (item.idPersonal == idPersonal)
                    {
                        cmbOpera1.SelectedIndex = i;
                        return;
                    }
                    else
                    {
                        i++;
                    }
                }
            }
            catch (Exception e)
            {

                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + e.Message,
                  this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }


        private void btnGrabar_Click(object sender, EventArgs e)
        {
            try
            {
                //VALIDAR UBICACION
                PreSalaCancelar = "";
                string MensajeExito = "";
                bool bExito = false;
                var con = Core.Utils.BoundedContextFactory.ConnectionFactory().ConnectionString;

                switch (estatusValido)
                {
                    case eEstatusOS.PRE:
                        if(_TipoOS == eTipoOrdenServicio.LLANTAS_ESP)
                        {
                           

                            //TERMINACION
                        mensaje = "Desea Terminar la Orden de Servicio : " + cabos.idOrdenSer.ToString() + " ? ";
                            MensajeExito = "Se Termino la Orden : ";
                            //RECEPCIONAR
                            //cabos.Estatus = "REC";
                            cabos.idEmpleadoEntrega = ((CatPersonal)cmbOpera1.SelectedItem).idPersonal;
                            cabos.FechaRecepcion = DateTime.Now;
                            cabos.UsuarioRecepcion = _Usuario.nombreUsuario;
                            foreach (var item in listaDetOS)
                            {
                                item.Estatus = "REC";
                            }
                            //ASIGNAR
                            if (!Validar(estatusValido))
                            {
                                MessageBox.Show("Todos las Ordenes de Trabajo deben estar Asignadas", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                return;
                            }
                            //INVENTARIAR
                            foreach (var item in listaDetOS)
                            {
                                if (item.idLlanta == "")
                                {
                                    result = MessageBox.Show("Desea Inventariar la llanta para la posicion: " + item.PosLlanta, caption, buttons);
                                    if (result == System.Windows.Forms.DialogResult.Yes)
                                    {
                                        //inventariar
                                        var catLlaInv = new fInventarioLlantas(con, true, _Usuario.nombreUsuario, cabos.IdEmpresa, cabos.idUnidadTrans, item.PosLlanta);
                                        catLlaInv.ShowDialog();
                                        item.idLlanta = catLlaInv.idLlanta;
                                    }
                                       
                                }

                            }
                            //INVENTARIAR
                            //cabos.Estatus = "ASIG";
                            cabos.FechaAsignado = DateTime.Now;
                            cabos.UsuarioAsigna = _Usuario.nombreUsuario;
                            foreach (var item in listaDetOS)
                            {
                                item.UsuarioAsig = _Usuario.nombreUsuario;
                                item.Estatus = "ASIG";
                                item.FechaAsig = DateTime.Now;
                                if (item.CIDPRODUCTO_SERV == 0)
                                {
                                    item.CIDPRODUCTO_SERV = item.CIDPRODUCTO_ACT;
                                }

                            }
                            //TERMINAR
                            if (!VerificacionTerminacion())
                            {
                                return;
                            }
                            if (listCabPresal != null)
                            {
                                foreach (var item in listCabPresal)
                                {
                                    if (item.Estatus == "PEN")
                                    {
                                        PreSalaCancelar = PreSalaCancelar + " " + item.IdPreSalida + ",";
                                    }
                                }
                            }
                            cabos.Estatus = "TER";
                            cabos.FechaTerminado = DateTime.Now;
                            cabos.UsuarioTermina = _Usuario.nombreUsuario;
                            foreach (var item in listaDetOS)
                            {
                                item.UsuarioTerminado = _Usuario.nombreUsuario;
                                item.Estatus = "TER";
                                item.FechaTerminado = DateTime.Now;

                                if (item.CIDPRODUCTO_SERV == 0)
                                {
                                    item.CIDPRODUCTO_SERV = item.CIDPRODUCTO_ACT;
                                }
                                if (item.Cantidad_CIDPRODUCTO_SERV == 0)
                                {
                                    item.Cantidad_CIDPRODUCTO_SERV = 1;
                                }

                            }
                            if (_TipoOS == eTipoOrdenServicio.LLANTAS_ESP)
                            {
                                foreach (var item in listaDetOS)
                                {
                                    resp = new LlantasHisSvc().getLlantasHisxFilter(0, "c.idTipoDoc = 8 AND c.Documento = '" + item.idOrdenTrabajo + "'");
                                    if (resp.typeResult == ResultTypes.recordNotFound)
                                    {
                                        resp = new CatActividadesSvc().getCatActividadesxFilter(item.idActividad);
                                        if (resp.typeResult == ResultTypes.success)
                                        {
                                            actividad = ((List<CatActividades>)resp.result)[0];
                                            if (actividad.idEnum > 0)
                                            {
                                                fMovllantas fmov = new fMovllantas(_Usuario, item.idLlanta, actividad.idEnum, item.idOrdenTrabajo);
                                                fmov.StartPosition = FormStartPosition.CenterScreen;
                                                fmov.ShowDialog();
                                            }
                                        }
                                    }
                                }
                            }

                        }
                        else
                        {
                            //RECEPCION
                            //mensajes
                            mensaje = "Desea Recepcionar la Orden de Servicio : " + cabos.idOrdenSer.ToString() + " ? ";
                            MensajeExito = "Se Recepcionó la Orden : ";


                            //actualizar Cabecero
                            cabos.Estatus = "REC";
                            cabos.idEmpleadoEntrega = ((CatPersonal)cmbOpera1.SelectedItem).idPersonal;
                            cabos.FechaRecepcion = DateTime.Now;
                            cabos.UsuarioRecepcion = _Usuario.nombreUsuario;


                            //PARA UBICACIONES
                            //    'Actualiza Estatus Ubicacion en la Unidad segun el Tipo de Orden de Servicio
                            //opcUbicacionUnidad = ObtieneEstatusUbixTipoOrden(_TipoOrdenServicio)

                            //StrSql = objsql.ActualizaEstatusUbicacion(UniTrans.iUnidadTrans, opcUbicacionUnidad, "estatusUbicacion")
                            //ReDim Preserve ArraySql(indice)
                            //ArraySql(indice) = StrSql
                            //indice += 1

                            //StrSql = objsql.ActualizaEstatusUbicacion(UniTrans.iUnidadTrans, UniTrans.estatusUbicacion, "estatusUbicacionAnt")
                            //ReDim Preserve ArraySql(indice)
                            //ArraySql(indice) = StrSql
                            //indice += 1

                            //    'Inserta Kardex Entrada a Taller
                            //StrSql = objsql.InsertaKardexEntSalUnid(UniTrans.iUnidadTrans, 1,
                            //opcUbicacionUnidad.ToString(),
                            //"ENTRADA POR RECEPCION DE " & opcUbicacionUnidad.ToString(),
                            //txtOrdenServ.Text)
                            //ReDim Preserve ArraySql(indice)
                            //ArraySql(indice) = StrSql
                            //indice += 1
                            //PARA UBICACIONES

                            //DETALLE DE ORDEN DE SERVICIO
                            foreach (var item in listaDetOS)
                            {
                                item.Estatus = "REC";
                            }
                        }
                        

                        break;
                    case eEstatusOS.REC:
                        //ASIGNACION
                        mensaje = "Desea Asignar la Orden de Servicio : " + cabos.idOrdenSer.ToString() + " ? ";
                        MensajeExito = "Se Asignó la Orden : ";
                        //ASIGNAR
                        //Validar
                        if (!Validar(estatusValido))
                        {
                            MessageBox.Show("Todos las Ordenes de Trabajo deben estar Asignadas", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                        //            'VALIDAR EN QUE ESTATUS SE ENCUENTRA LA UNIDAD
                        //UniTrans = New UnidadesTranspClass(UniTrans.iUnidadTrans)
                        //If UniTrans.Existe Then
                        //    If ValidaUbicacionUnidad(UniTrans.estatusUbicacion) Then
                        //        MsgBox("La Unidad: " & UniTrans.iUnidadTrans &
                        //                            " no puede ser Recepcionada ya que se encuentra en " & CType(UniTrans.estatusUbicacion, opcEstatusUbiUni).ToString())
                        //        Exit Sub
                        //    End If
                        //End If

                        //Actualiza Cabecero
                        cabos.Estatus = "ASIG";
                        cabos.FechaAsignado = DateTime.Now;
                        cabos.UsuarioAsigna = _Usuario.nombreUsuario;

                        //Actualiza Detalla
                        foreach (var item in listaDetOS)
                        {
                            item.UsuarioAsig = _Usuario.nombreUsuario;
                            item.Estatus = "ASIG";
                            item.FechaAsig = DateTime.Now;
                            if (item.CIDPRODUCTO_SERV == 0)
                            {
                                item.CIDPRODUCTO_SERV = item.CIDPRODUCTO_ACT;
                            }

                        }

                        foreach (var item in listaDetROS)
                        {

                        }


                        break;
                    case eEstatusOS.ASIG:
                        //TERMINACION
                        mensaje = "Desea Terminar la Orden de Servicio : " + cabos.idOrdenSer.ToString() + " ? ";
                        MensajeExito = "Se Termino la Orden : ";
                        //ASIGNAR
                        //Validar
                        if (!VerificacionTerminacion())
                        {
                            return;
                        }
                        //VerificaCasosEspeciales();
                        //if (!Validar(estatusValido))
                        //{
                        //    MessageBox.Show(MensajeNoValido, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //    return;
                        //}

                        //Verificacion de PResalidas 
                        if (listCabPresal != null)
                        {
                            foreach (var item in listCabPresal)
                            {
                                if (item.Estatus == "PEN")
                                {
                                    PreSalaCancelar = PreSalaCancelar + " " + item.IdPreSalida + ",";
                                }
                            }
                        }


                        //si se cumple todo
                        cabos.FechaTerminado = DateTime.Now;
                        cabos.Estatus = "TER";
                        cabos.UsuarioTermina = _Usuario.nombreUsuario;

                        //Actualiza Detalle
                        foreach (var item in listaDetOS)
                        {
                            item.UsuarioTerminado = _Usuario.nombreUsuario;
                            item.Estatus = "TER";
                            item.FechaTerminado = DateTime.Now;

                            if (item.CIDPRODUCTO_SERV == 0)
                            {
                                item.CIDPRODUCTO_SERV = item.CIDPRODUCTO_ACT;
                            }
                            if (item.Cantidad_CIDPRODUCTO_SERV == 0)
                            {
                                item.Cantidad_CIDPRODUCTO_SERV = 1;
                            }

                        }
                        //llantera
                        if (_TipoOS == eTipoOrdenServicio.LLANTAS)
                        {
                            foreach (var item in listaDetOS)
                            {
                                resp = new LlantasHisSvc().getLlantasHisxFilter(0, "c.idTipoDoc = 8 AND c.Documento = '" + item.idOrdenTrabajo + "'");
                                if  (resp.typeResult == ResultTypes.recordNotFound)
                                {
                                    resp = new CatActividadesSvc().getCatActividadesxFilter(item.idActividad);
                                    if (resp.typeResult == ResultTypes.success)
                                    {
                                        actividad = ((List<CatActividades>)resp.result)[0];
                                        if (actividad.idEnum > 0)
                                        {
                                            fMovllantas fmov = new fMovllantas(_Usuario, item.idLlanta, actividad.idEnum, item.idOrdenTrabajo);
                                            fmov.StartPosition = FormStartPosition.CenterScreen;
                                            fmov.ShowDialog();
                                        }
                                    }
                                }

                                

                            }
                        }
                        //llantera

                        break;
                    case eEstatusOS.TER:
                        //ENTREGA
                        mensaje = "Desea Entregar la Orden de Servicio : " + cabos.idOrdenSer.ToString() + " ? ";
                        MensajeExito = "Se Entrego la Orden : ";
                        if (!Validar(estatusValido))
                        {
                            MessageBox.Show(MensajeNoValido, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                        



                        //si se cumple todo
                        cabos.FechaEntregado = DateTime.Now;
                        cabos.NotaFinal = "";
                        cabos.idEmpleadoRecibe = ((CatPersonal)cmbOpera1.SelectedItem).idPersonal;
                        cabos.Estatus = "ENT";
                        cabos.UsuarioEntrega = _Usuario.nombreUsuario;

                        //    '1 es Base x que esta Entregando la Unidad
                        //StrSql = objsql.ActualizaEstatusUbicacion(UniTrans.iUnidadTrans, 1, "estatusUbicacion")
                        //ReDim Preserve ArraySql(indice)
                        //ArraySql(indice) = StrSql
                        //indice += 1

                        //StrSql = objsql.ActualizaEstatusUbicacion(UniTrans.iUnidadTrans, UniTrans.estatusUbicacion, "estatusUbicacionAnt")
                        //ReDim Preserve ArraySql(indice)
                        //ArraySql(indice) = StrSql
                        //indice += 1

                        //StrSql = objsql.InsertaKardexEntSalUnid(UniTrans.iUnidadTrans, 0,
                        //opcUbicacionUnidad.ToString(),
                        //"SALIDA POR ENTREGA DE " & opcUbicacionUnidad.ToString(),
                        //txtOrdenServ.Text)
                        //ReDim Preserve ArraySql(indice)
                        //ArraySql(indice) = StrSql
                        //indice += 1
                        break;
                    case eEstatusOS.ENT:
                        break;
                    case eEstatusOS.CAN:
                        break;
                    default:
                        break;
                }

                caption = this.Name;
                buttons = MessageBoxButtons.OKCancel;
                if (PreSalaCancelar != "")
                {
                    PreSalaCancelar = PreSalaCancelar.Substring(0, PreSalaCancelar.Length - 1);

                    mensaje = mensaje + Environment.NewLine
                   + "y Cancelar las Pre Salidas Pendientes de Entrega = " + PreSalaCancelar;

                }

                
                result = MessageBox.Show(mensaje, caption, buttons);
                if (result == System.Windows.Forms.DialogResult.OK)
                {
                    //Cancelar Pre Salidas
                    if (PreSalaCancelar != "")
                    {
                        foreach (var item in listCabPresal)
                        {

                            if (item.Estatus == "PEN")
                            {
                                item.Estatus = "CAN";
                                item.FecCancela = DateTime.Now;
                                item.UserCancela = _Usuario.nombreUsuario;
                                item.MotivoCancela = "Aplicada por cancelación de Orden de trabajo";

                                cabpresal = item;
                                resp = new PreSalidasSvc().GuardaCabPreSalida(ref cabpresal);
                                if (resp.typeResult == ResultTypes.success)
                                {
                                    BandExito = true;
                                }
                                else
                                {
                                    BandExito = false;
                                }
                            }


                        }
                    }
                    //actualizar Cabecero OS
                    resp = new CabOrdenServicioSvc().GuardaCabOrdenServicio(ref cabos);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        bExito = true;

                        //Actualizar Detalle OS
                        foreach (var item in listaDetOS)
                        {
                            detos = item;
                            resp = new DetOrdenServicioSvc().GuardaDetOrdenServicio(ref detos);
                            if (resp.typeResult == ResultTypes.success)
                            {
                                bExito = true;
                            }
                            else
                            {
                                bExito = false;
                                //Ocurrio un Error
                            }

                        }
                        if (listaDetROS != null)
                        {
                            if (listaDetROS.Count > 0)
                            {
                                foreach (var item in listaDetROS)
                                {
                                    detros = item;
                                    resp = new DetRecepcionOSSvc().GuardaDetRecepcionOS(ref detros);
                                    if (resp.typeResult == ResultTypes.success)
                                    {
                                        bExito = true;
                                    }
                                    else
                                    {
                                        bExito = false;
                                        //Ocurrio un Error
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        //ocurrio un error
                        bExito = false;
                    }

                    if (bExito)
                    {
                        MessageBox.Show(MensajeExito + cabos.idOrdenSer.ToString(), "Operación Exitosa", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Cancelar();
                    }
                    else
                    {
                        MessageBox.Show("No " + MensajeExito + cabos.idOrdenSer.ToString(), "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }


            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + ex.Message,
                  this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void cmbOpera1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                switch (estatusValido)
                {
                    case eEstatusOS.PRE:
                        btnGrabar.PerformClick();
                        break;
                    case eEstatusOS.REC:
                        cmbOpera2.Focus();
                        break;
                    case eEstatusOS.ASIG:
                        break;
                    case eEstatusOS.TER:
                        break;
                    case eEstatusOS.ENT:
                        break;
                    case eEstatusOS.CAN:
                        break;
                    default:
                        break;
                }
            }
        }


        private void CargaGpoOpera()
        {
            chkMarcaTodos.Visible = false;
            switch (_estatus)
            {
                case eEstatusOS.PRE:
                    break;
                case eEstatusOS.REC:
                    lblopera1.Text = "Entrega:";
                    lblopera1.Visible = true;
                    lblopera2.Visible = false;
                    lblopera3.Visible = false;

                    cmbOpera1.Visible = true;
                    cmbOpera2.Visible = false;
                    cmbOpera3.Visible = false;

                    cmbDetOpera1.Visible = false;
                    cmbDetOpera2.Visible = false;
                    cmbDetOpera3.Visible = false;

                    chkOpera2.Visible = false;
                    chkOpera3.Visible = false;

                    btnAsigna.Visible = false;
                    
                    
                    break;
                case eEstatusOS.ASIG:
                    lblopera1.Text = "Encargado:";
                    lblopera2.Text = "Ayudante 1:";
                    lblopera3.Text = "Ayudante 2:";

                    lblopera1.Visible = true;
                    lblopera2.Visible = true;
                    lblopera3.Visible = true;

                    cmbOpera1.Visible = true;
                    cmbOpera2.Visible = true;
                    cmbOpera3.Visible = true;

                    cmbDetOpera1.Visible = true;
                    cmbDetOpera2.Visible = true;
                    cmbDetOpera3.Visible = true;

                    //lblDetOpera1.Visible = false;
                    //lblDetOpera2.Visible = false;
                    //lblDetOpera3.Visible = false;

                    chkOpera2.Visible = true;
                    chkOpera3.Visible = true;

                    btnAsigna.Visible = true;

                    chkMarcaTodos.Visible = true;
                    break;
                case eEstatusOS.TER:
                    if (_TipoOS == eTipoOrdenServicio.LLANTAS_ESP)
                    {
                        lblopera1.Text = "Entrega:";
                        lblopera2.Text = "Encargado:";
                        lblopera3.Text = "Ayudante :";

                        lblopera1.Visible = true;
                        lblopera2.Visible = true;
                        lblopera3.Visible = true;

                        cmbOpera1.Visible = true;
                        cmbOpera2.Visible = true;
                        cmbOpera3.Visible = true;

                        cmbDetOpera1.Visible = true;
                        cmbDetOpera2.Visible = true;
                        cmbDetOpera3.Visible = true;

                        //lblDetOpera1.Visible = false;
                        //lblDetOpera2.Visible = false;
                        //lblDetOpera3.Visible = false;

                        chkOpera2.Visible = true;
                        chkOpera3.Visible = true;
                        chkOpera2.Checked = true;
                        //chkOpera3.Checked = true;

                        btnAsigna.Visible = true;

                        chkMarcaTodos.Visible = true;

                    }
                    else
                    {
                        lblopera1.Text = "";
                        lblopera2.Text = "";
                        lblopera3.Text = "";

                        lblopera1.Visible = false;
                        lblopera2.Visible = false;
                        lblopera3.Visible = false;

                        cmbOpera1.Visible = false;
                        cmbOpera2.Visible = false;
                        cmbOpera3.Visible = false;

                        cmbDetOpera1.Visible = false;
                        cmbDetOpera2.Visible = false;
                        cmbDetOpera3.Visible = false;

                        chkOpera2.Visible = false;
                        chkOpera3.Visible = false;

                        btnAsigna.Visible = false;
                    }
                    
                    break;
                case eEstatusOS.ENT:
                    lblopera1.Text = "Recibe:";

                    lblopera1.Visible = true;
                    lblopera2.Visible = false;
                    lblopera3.Visible = false;

                    cmbOpera1.Visible = true;
                    cmbOpera2.Visible = false;
                    cmbOpera3.Visible = false;

                    cmbDetOpera1.Visible = false;
                    cmbDetOpera2.Visible = false;
                    cmbDetOpera3.Visible = false;

                    chkOpera2.Visible = false;
                    chkOpera3.Visible = false;

                    btnAsigna.Visible = false;
                    break;
                case eEstatusOS.CAN:
                    break;
                default:
                    break;
            }
        }

        private List<CatPersonal> LlenaComboAsigna(ComboBox control, ComboBox controlDet)
        {
            try
            {
                List<CatPersonal> Lista;


                //SE PUEDE ASIGNAR VARIAS VECES - NO BORRAR POSTERIOR CANDADO
                //    string filtroEsp = " AND  NOT per.idPersonal IN ( " +
                //"SELECT DISTINCT dros.idPersonalResp AS idPersonal " +
                //"FROM dbo.DetRecepcionOS dros " +
                //"INNER JOIN dbo.CabOrdenServicio cab ON cab.idOrdenSer = dros.idOrdenSer " +
                //"WHERE cab.Estatus IN  ('ASIG','DIAG') AND ISNULL(dros.idPersonalResp,0) > 0 " +
                //"UNION " +
                //"SELECT DISTINCT dros.idPersonalAyu1 AS idPersonal " +
                //"FROM dbo.DetRecepcionOS dros " +
                //"INNER JOIN dbo.CabOrdenServicio cab ON cab.idOrdenSer = dros.idOrdenSer " +
                //"WHERE cab.Estatus IN  ('ASIG','DIAG') AND ISNULL(dros.idPersonalAyu1,0) > 0 " +
                //"union " +
                //"SELECT DISTINCT dros.idPersonalAyu2 AS idPersonal " +
                //"FROM dbo.DetRecepcionOS dros " +
                //"INNER JOIN dbo.CabOrdenServicio cab ON cab.idOrdenSer = dros.idOrdenSer " +
                //"WHERE cab.Estatus IN  ('ASIG','DIAG') AND ISNULL(dros.idPersonalAyu2,0) > 0)";

                //resp = new CatPersonalSvc().getCatPersonalxFiltro(" per.idDepto IN (SELECT DISTINCT idDepto FROM dbo.RelPersonalRolOrdServ WHERE idTipOrdServ = " + cabos.idTipOrdServ + " AND idRol = 3) " +
                //"AND per.idPuesto IN (SELECT DISTINCT idPuesto FROM dbo.RelPersonalRolOrdServ WHERE idTipOrdServ = " + cabos.idTipOrdServ + " AND idRol = 3)" + filtroEsp);

                resp = new CatPersonalSvc().getCatPersonalxFiltro(" per.idDepto IN (SELECT DISTINCT idDepto FROM dbo.RelPersonalRolOrdServ WHERE idTipOrdServ = " + cabos.idTipOrdServ + " AND idRol = 3) " +
                "AND per.idPuesto IN (SELECT DISTINCT idPuesto FROM dbo.RelPersonalRolOrdServ WHERE idTipOrdServ = " + cabos.idTipOrdServ + " AND idRol = 3)");


                if (resp.typeResult == ResultTypes.success)
                {

                    Lista = (List<CatPersonal>)resp.result;
                    control.DataSource = null;
                    control.DataSource = Lista;

                    control.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                    control.AutoCompleteSource = AutoCompleteSource.ListItems;
                    control.DisplayMember = "NombreCompleto";
                    control.SelectedIndex = 0;

                    controlDet.DataSource = null;
                    controlDet.DataSource = Lista;
                    controlDet.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                    controlDet.AutoCompleteSource = AutoCompleteSource.ListItems;
                    controlDet.DisplayMember = "NomPuesto";
                    controlDet.SelectedIndex = 0;


                    return Lista;
                    ////Ayundate 1
                    //listaAyudante1 = (List<CatPersonal>)resp.result;
                    //cmbOpera2.DataSource = null;
                    //cmbOpera2.DataSource = listaAyudante1;
                    //cmbOpera2.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                    //cmbOpera2.AutoCompleteSource = AutoCompleteSource.ListItems;
                    //cmbOpera2.DisplayMember = "NombreCompleto";
                    //cmbOpera2.SelectedIndex = 0;

                    ////Ayundate 2
                    //listaAyudante2 = (List<CatPersonal>)resp.result;
                    //cmbOpera3.DataSource = null;
                    //cmbOpera3.DataSource = listaAyudante2;
                    //cmbOpera3.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                    //cmbOpera3.AutoCompleteSource = AutoCompleteSource.ListItems;
                    //cmbOpera3.DisplayMember = "NombreCompleto";
                    //cmbOpera3.SelectedIndex = 0;
                }
                else
                {
                    return null;
                    //no se encontro
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + ex.Message,
                  this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return null;
            }
        }

        private void chkOpera2_CheckedChanged(object sender, EventArgs e)
        {
            cmbOpera2.Enabled = chkOpera2.Checked;
        }

        private void chkOpera3_CheckedChanged(object sender, EventArgs e)
        {
            cmbOpera3.Enabled = chkOpera3.Checked;
        }

        private void btnAsigna_Click(object sender, EventArgs e)
        {
            bool BandExito = true;
            if (listaDetROS_Asigna != null)
            {
                if (listaDetROS_Asigna.Count > 0)
                {
                    foreach (var item in listaDetROS_Asigna)
                    {
                        if (_TipoOS == eTipoOrdenServicio.LLANTAS_ESP)
                        {
                            item.idPersonalResp = ((CatPersonal)cmbOpera2.SelectedItem).idPersonal;
                            item.NomPersonalResp = ((CatPersonal)cmbOpera2.SelectedItem).NombreCompleto;
                            if (chkOpera3.Checked)
                            {
                                if (item.idPersonalResp == ((CatPersonal)cmbOpera3.SelectedItem).idPersonal)
                                {
                                    BandExito = false;
                                    MessageBox.Show("El Ayudante tiene que ser diferente al Responsable", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    cmbOpera3.Focus();
                                    break;
                                }
                                item.idPersonalAyu1 = ((CatPersonal)cmbOpera3.SelectedItem).idPersonal;
                                item.NomPersonalAyu1 = ((CatPersonal)cmbOpera3.SelectedItem).NombreCompleto;
                            }
                        }
                        else
                        {
                            item.idPersonalResp = ((CatPersonal)cmbOpera1.SelectedItem).idPersonal;
                            item.NomPersonalResp = ((CatPersonal)cmbOpera1.SelectedItem).NombreCompleto;
                            if (chkOpera2.Checked)
                            {
                                if (item.idPersonalResp == ((CatPersonal)cmbOpera2.SelectedItem).idPersonal)
                                {
                                    BandExito = false;
                                    MessageBox.Show("El Ayudante tiene que ser diferente al Responsable", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    cmbOpera2.Focus();
                                    break;
                                }
                                item.idPersonalAyu1 = ((CatPersonal)cmbOpera2.SelectedItem).idPersonal;
                                item.NomPersonalAyu1 = ((CatPersonal)cmbOpera2.SelectedItem).NombreCompleto;
                            }
                            if (chkOpera3.Checked)
                            {
                                if (item.idPersonalResp == ((CatPersonal)cmbOpera3.SelectedItem).idPersonal)
                                {
                                    BandExito = false;
                                    MessageBox.Show("El Ayudante 2 tiene que ser diferente al Responsable", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    cmbOpera3.Focus();
                                    break;
                                }
                                if (chkOpera2.Checked)
                                {
                                    if (item.idPersonalAyu1 == ((CatPersonal)cmbOpera3.SelectedItem).idPersonal)
                                    {
                                        BandExito = false;
                                        MessageBox.Show("El Ayudante 2 tiene que ser diferente al Ayudante 1", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        cmbOpera3.Focus();
                                        break;
                                    }
                                }



                                item.idPersonalAyu2 = ((CatPersonal)cmbOpera3.SelectedItem).idPersonal;
                                item.NomPersonalAyu2 = ((CatPersonal)cmbOpera3.SelectedItem).NombreCompleto;
                            }
                        }
                        
                    }
                    //Agregar a lista det orden de servicio, y a la lista detRec
                    foreach (var item in listaDetROS_Asigna)
                    {
                        foreach (var itemd in listaDetOS)
                        {
                            if (item.idOrdenTrabajo == itemd.idOrdenTrabajo)
                            {
                                itemd.idPersonalResp = item.idPersonalResp;
                                itemd.NomPersonalResp = item.NomPersonalResp;
                                itemd.idPersonalAyu1 = item.idPersonalAyu1;
                                itemd.NomPersonalAyu1 = item.NomPersonalAyu1;
                                itemd.idPersonalAyu2 = item.idPersonalAyu2;
                                itemd.NomPersonalAyu2 = item.NomPersonalAyu2;
                            }
                        }
                        foreach (var itemdr in listaDetROS)
                        {
                            if (item.idOrdenTrabajo == itemdr.idOrdenTrabajo)
                            {
                                itemdr.idPersonalResp = item.idPersonalResp;
                                itemdr.NomPersonalResp = item.NomPersonalResp;
                                itemdr.idPersonalAyu1 = item.idPersonalAyu1;
                                itemdr.NomPersonalAyu1 = item.NomPersonalAyu1;
                                itemdr.idPersonalAyu2 = item.idPersonalAyu2;
                                itemdr.NomPersonalAyu2 = item.NomPersonalAyu2;
                            }
                        }

                    }


                    foreach (var item in listaDetOS)
                    {
                        item.Seleccionado = false;
                    }

                    FormatogrdDet();
                    grdOT.Refresh();
                    //btnAsigna.Enabled = true;
                    if (BandExito)
                    {
                        btnAsigna.Enabled = false;
                    }
                    else
                    {
                        btnAsigna.Enabled = true;
                    }

                }
                else
                {

                }



            }
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void asignarToolStripMenuItem_Click(object sender, EventArgs e)
        {


        }

        private void desAsignarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (var item in listaDetOS)
            {
                if (item.Seleccionado)
                {
                    item.idPersonalResp = 0;
                    item.NomPersonalResp = "";
                    item.idPersonalAyu1 = 0;
                    item.NomPersonalAyu1 = "";
                    item.idPersonalAyu2 = 0;
                    item.NomPersonalAyu2 = "";
                }
            }
        }

        private void asignarSeleccionadoToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private bool Validar(eEstatusOS opc)
        {
            bool resp = true;
            switch (opc)
            {
                case eEstatusOS.PRE:
                    break;
                case eEstatusOS.REC:
                    //ASIGNAR
                    foreach (var item in listaDetOS)
                    {
                        if (item.idPersonalResp == 0)
                        {
                            resp = false;
                            break;
                        }
                    }
                    break;
                case eEstatusOS.ASIG:
                    //terminar
                    MensajeNoValido = "";
                    foreach (var item in listaDetOS)
                    {
                        if (item.TipoNotaRecepcionF.ToUpper() != "ACTIVIDAD")
                        {
                            resp = false;
                            MensajeNoValido = "La Orden de Trabajo: " + item.idOrdenTrabajo + " Es una Falla Reportada";
                            break;
                        }
                    }

                    //if (MensajeNoValido == "")
                    //{
                    //    if (listCabPreOC != null)
                    //    {
                    //        if (listCabPreOC.Count > 0)
                    //        {
                    //            foreach (var item in listCabPreOC)
                    //            {
                    //                if (item.Estatus.ToUpper() != "ENT")
                    //                {
                    //                    resp = false;
                    //                    MensajeNoValido = "La Orden de Trabajo" + item.idOrdenTrabajo + " Tiene una Pre Orden de Compra no Entregada";
                    //                    break;

                    //                }
                    //            }
                    //        }
                    //    }
                    //}
                    if (MensajeNoValido == "")
                    {
                        if (listCabPresal != null)
                        {
                            if (listCabPresal.Count > 0)
                            {
                                foreach (var item in listCabPresal)
                                {
                                    if (item.Estatus.ToUpper() != "ENT")
                                    {
                                        resp = false;
                                        MensajeNoValido = "La Orden de Trabajo" + item.idOrdenTrabajo + " Tiene una Pre Salida no Entregada";
                                        break;

                                    }
                                }
                            }
                        }
                    }



                    break;
                case eEstatusOS.TER:
                    //para entregar
                    if (((CatPersonal)cmbOpera1.SelectedItem).idPersonal == 0)
                    {
                        resp = false;
                        MensajeNoValido = "La Persona que Recibe no puede ser Vacío";
                        break;
                    }
                    if (MensajeNoValido == "")
                    {
                        if (listaDetOS.Count > 0)
                        {
                            foreach (var item in listaDetOS)
                            {
                                if (item.Estatus.Trim().ToUpper() != "TER")
                                {
                                    resp = false;
                                    MensajeNoValido = "Todas las Ordenes de Trabajo deben de Estar Terminadas";
                                }
                            }
                        }
                    }


                    break;
                case eEstatusOS.ENT:
                    break;
                case eEstatusOS.CAN:
                    break;
                default:
                    break;
            }

            return resp;
        }

        private void FiltraOrdenesCompra(int IndiceList)
        {
            int idPreOc = listCabPreOC[IndiceList].IdPreOC;

            var mySearch = (listDetPreOC).FindAll(S => S.IdPreOC.ToString().IndexOf(idPreOc.ToString().Trim(), StringComparison.OrdinalIgnoreCase) >= 0);

            grdDetOC.DataSource = null;
            grdDetOC.DataSource = mySearch;

            FormatogrdCabOC();
            FormatogrdDetOC();
        }
        private void FiltraSalidasAlmacen(int IndiceList)
        {
            int idPreSal = listCabPresal[IndiceList].IdPreSalida;

            var mySearch = (listDetPresal).FindAll(S => S.IdPreSalida.ToString().IndexOf(idPreSal.ToString().Trim(), StringComparison.OrdinalIgnoreCase) >= 0);

            grdDetPreSalida.DataSource = null;
            grdDetPreSalida.DataSource = mySearch;

            FormatogrdCabPreSalida();
            FormatogrdDetPreSalida();

        }

        private void grdCabPreSalida_Click(object sender, EventArgs e)
        {
            FiltraSalidasAlmacen(grdCabPreSalida.CurrentCell.RowIndex);
        }

        private void grdCabOC_Click(object sender, EventArgs e)
        {
            FiltraOrdenesCompra(grdCabOC.CurrentCell.RowIndex);
        }

        private void FormatogrdCabOC()
        {
            //Ocultar Columnas
            grdCabOC.Columns["idSolOC"].Visible = false;
            grdCabOC.Columns["idCotizacionOC"].Visible = false;
            grdCabOC.Columns["idOC"].Visible = false;
            grdCabOC.Columns["UserSolicita"].Visible = false;
            grdCabOC.Columns["idAlmacen"].Visible = false;
            grdCabOC.Columns["UserAutorizaOC"].Visible = false;
            grdCabOC.Columns["FecAutorizaOC"].Visible = false;
            grdCabOC.Columns["Observaciones"].Visible = false;
            grdCabOC.Columns["FecModifica"].Visible = false;
            grdCabOC.Columns["UserModifica"].Visible = false;

            grdCabOC.Columns["SubTotal"].Visible = false;
            grdCabOC.Columns["IVA"].Visible = false;
            grdCabOC.Columns["FecCancela"].Visible = false;
            grdCabOC.Columns["UserCancela"].Visible = false;
            grdCabOC.Columns["MotivoCancela"].Visible = false;
            grdCabOC.Columns["CIDDOCUMENTO"].Visible = false;

            //encabezado
            grdCabOC.Columns["IdPreOC"].HeaderText = "Folio";
            grdCabOC.Columns["idOrdenTrabajo"].HeaderText = "Orden Trabajo";
            grdCabOC.Columns["FechaPreOc"].HeaderText = "Fecha";
            grdCabOC.Columns["Estatus"].HeaderText = "Estatus";
            grdCabOC.Columns["CSERIEDOCUMENTO"].HeaderText = "Serie";
            grdCabOC.Columns["CFOLIO"].HeaderText = "Folio";
            grdCabOC.Columns["IdPreSalida"].HeaderText = "Pre Salida";


            //tamaño
            grdCabOC.Columns["IdPreOC"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdCabOC.Columns["idOrdenTrabajo"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdCabOC.Columns["FechaPreOc"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdCabOC.Columns["Estatus"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

            grdCabOC.Columns["CSERIEDOCUMENTO"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdCabOC.Columns["CFOLIO"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdCabOC.Columns["IdPreSalida"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

            //Ordenar Columnas
            grdCabOC.Columns["IdPreOC"].DisplayIndex = 0;
            grdCabOC.Columns["idOrdenTrabajo"].DisplayIndex = 1;
            grdCabOC.Columns["FechaPreOc"].DisplayIndex = 2;
        }
        private void FormatogrdDetOC()
        {
            //Ocultar Columnas

            grdDetOC.Columns["IdPreOC"].Visible = false;
            grdDetOC.Columns["idUnidadProd"].Visible = false;
            grdDetOC.Columns["CantSinCosto"].Visible = false;
            grdDetOC.Columns["CantFaltante"].Visible = false;
            grdDetOC.Columns["Precio"].Visible = false;
            grdDetOC.Columns["PorcIVA"].Visible = false;
            grdDetOC.Columns["Desc1"].Visible = false;
            grdDetOC.Columns["Desc2"].Visible = false;
            grdDetOC.Columns["Inserta"].Visible = false;
            grdDetOC.Columns["Seleccionado"].Visible = false;
            grdDetOC.Columns["CantidadEnt"].Visible = false;

            //grdDetOC.Columns["SubTotal"].Visible = false;
            //grdDetOC.Columns["IVA"].Visible = false;
            //grdDetOC.Columns["FecCancela"].Visible = false;
            //grdDetOC.Columns["UserCancela"].Visible = false;
            //grdDetOC.Columns["MotivoCancela"].Visible = false;
            //grdDetOC.Columns["CIDDOCUMENTO"].Visible = false;

            //encabezado
            grdDetOC.Columns["IdPreOC"].HeaderText = "Folio";
            grdDetOC.Columns["idProducto"].HeaderText = "Producto";
            grdDetOC.Columns["CCODIGOPRODUCTO"].HeaderText = "Codigo";
            grdDetOC.Columns["CNOMBREPRODUCTO"].HeaderText = "Descripcion";
            grdDetOC.Columns["CantidadSol"].HeaderText = "Cantidad";


            //tamaño
            grdDetOC.Columns["IdPreOC"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdDetOC.Columns["idProducto"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdDetOC.Columns["CCODIGOPRODUCTO"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdDetOC.Columns["CNOMBREPRODUCTO"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdDetOC.Columns["CantidadSol"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

            //Ordenar Columnas
            grdDetOC.Columns["IdPreOC"].DisplayIndex = 0;
            grdDetOC.Columns["idProducto"].DisplayIndex = 1;
            grdDetOC.Columns["CCODIGOPRODUCTO"].DisplayIndex = 2;
            grdDetOC.Columns["CNOMBREPRODUCTO"].DisplayIndex = 3;
            grdDetOC.Columns["CantidadSol"].DisplayIndex = 4;
        }

        private void FormatogrdCabPreSalida()
        {
            //Ocultar Columnas
            grdCabPreSalida.Columns["UserSolicitud"].Visible = false;
            grdCabPreSalida.Columns["NomUserSolicitud"].Visible = false;
            grdCabPreSalida.Columns["IdEmpleadoEnc"].Visible = false;
            grdCabPreSalida.Columns["NomEmpleadoEnc"].Visible = false;

            grdCabPreSalida.Columns["idAlmacen"].Visible = false;
            grdCabPreSalida.Columns["NomAlmacen"].Visible = false;
            grdCabPreSalida.Columns["CIDALMACEN_COM"].Visible = false;
            grdCabPreSalida.Columns["SubTotal"].Visible = false;
            grdCabPreSalida.Columns["FecCancela"].Visible = false;
            grdCabPreSalida.Columns["UserCancela"].Visible = false;

            grdCabPreSalida.Columns["MotivoCancela"].Visible = false;
            grdCabPreSalida.Columns["FecEntrega"].Visible = false;
            grdCabPreSalida.Columns["UserEntrega"].Visible = false;
            grdCabPreSalida.Columns["IdPersonalRec"].Visible = false;
            grdCabPreSalida.Columns["EntregaCambio"].Visible = false;
            grdCabPreSalida.Columns["idPreOC"].Visible = false;
            grdCabPreSalida.Columns["CIDDOCUMENTO_ENT"].Visible = false;
            grdCabPreSalida.Columns["CIDDOCUMENTO_SAL"].Visible = false;
            //grdCabPreSalida.Columns["x"].Visible = false;
            //grdCabPreSalida.Columns["x"].Visible = false;

            //encabezado
            grdCabPreSalida.Columns["IdPreSalida"].HeaderText = "Folio";
            grdCabPreSalida.Columns["idOrdenTrabajo"].HeaderText = "Orden Trabajo";
            grdCabPreSalida.Columns["FecSolicitud"].HeaderText = "Fecha";
            grdCabPreSalida.Columns["Estatus"].HeaderText = "Estatus";
            grdCabPreSalida.Columns["CSERIEDOCUMENTO_ENT"].HeaderText = "Serie Ent";
            grdCabPreSalida.Columns["CFOLIO_ENT"].HeaderText = "Folio Ent";
            grdCabPreSalida.Columns["CSERIEDOCUMENTO_SAL"].HeaderText = "Serie Sal";
            grdCabPreSalida.Columns["CFOLIO_SAL"].HeaderText = "Folio Sal";
            //grdCabPreSalida.Columns["EntregaCambio"].HeaderText = "Serie Sal";
            //grdCabPreSalida.Columns["x"].HeaderText = "Serie Sal";


            //tamaño
            grdCabPreSalida.Columns["IdPreSalida"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdCabPreSalida.Columns["idOrdenTrabajo"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdCabPreSalida.Columns["FecSolicitud"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdCabPreSalida.Columns["Estatus"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdCabPreSalida.Columns["CSERIEDOCUMENTO_ENT"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdCabPreSalida.Columns["CFOLIO_ENT"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdCabPreSalida.Columns["CSERIEDOCUMENTO_SAL"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdCabPreSalida.Columns["CFOLIO_SAL"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            //grdCabPreSalida.Columns["x"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            //grdCabPreSalida.Columns["x"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            //grdCabPreSalida.Columns["x"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;



            //Ordenar Columnas
            grdCabPreSalida.Columns["IdPreSalida"].DisplayIndex = 0;
            grdCabPreSalida.Columns["idOrdenTrabajo"].DisplayIndex = 1;
            grdCabPreSalida.Columns["FecSolicitud"].DisplayIndex = 2;

            grdCabPreSalida.Columns["Estatus"].DisplayIndex = 3;
            grdCabPreSalida.Columns["CSERIEDOCUMENTO_ENT"].DisplayIndex = 4;
            grdCabPreSalida.Columns["CFOLIO_ENT"].DisplayIndex = 5;
            grdCabPreSalida.Columns["CSERIEDOCUMENTO_SAL"].DisplayIndex = 6;
            grdCabPreSalida.Columns["CFOLIO_SAL"].DisplayIndex = 7;

            //grdCabPreSalida.Columns["x"].DisplayIndex = 2;
            //grdCabPreSalida.Columns["x"].DisplayIndex = 2;
        }

        //
        private void FormatogrdDetPreSalida()
        {
            //public int idProducto { get; set; }
            //public string CCODIGOPRODUCTO { get; set; }
            //public string CNOMBREPRODUCTO { get; set; }
            //public decimal CantidadEnt { get; set; }


            //    public bool Seleccionado { get; set; }
            //public int IdPreSalida { get; set; }
            //public int idUnidadProd { get; set; }
            //public decimal Cantidad { get; set; }
            //public decimal Costo { get; set; }
            //public decimal Existencia { get; set; }
            //public bool Inserta { get; set; }
            //Ocultar Columnas

            grdDetPreSalida.Columns["Seleccionado"].Visible = false;
            grdDetPreSalida.Columns["IdPreSalida"].Visible = false;
            grdDetPreSalida.Columns["idUnidadProd"].Visible = false;
            grdDetPreSalida.Columns["Cantidad"].Visible = false;
            grdDetPreSalida.Columns["Costo"].Visible = false;
            grdDetPreSalida.Columns["Existencia"].Visible = false;
            grdDetPreSalida.Columns["Inserta"].Visible = false;
            //grdDetPreSalida.Columns["x"].Visible = false;
            //grdDetPreSalida.Columns["x"].Visible = false;
            //grdDetPreSalida.Columns["x"].Visible = false;
            //grdDetPreSalida.Columns["x"].Visible = false;



            //encabezado
            grdDetPreSalida.Columns["IdPreSalida"].HeaderText = "Folio";
            grdDetPreSalida.Columns["idProducto"].HeaderText = "Producto";
            grdDetPreSalida.Columns["CCODIGOPRODUCTO"].HeaderText = "Codigo";
            grdDetPreSalida.Columns["CNOMBREPRODUCTO"].HeaderText = "Descripcion";
            grdDetPreSalida.Columns["CantidadEnt"].HeaderText = "Cantidad";


            //tamaño
            grdDetPreSalida.Columns["IdPreSalida"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdDetPreSalida.Columns["idProducto"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdDetPreSalida.Columns["CCODIGOPRODUCTO"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdDetPreSalida.Columns["CNOMBREPRODUCTO"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdDetPreSalida.Columns["CantidadEnt"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

            //Ordenar Columnas
            grdDetPreSalida.Columns["IdPreSalida"].DisplayIndex = 0;
            grdDetPreSalida.Columns["idProducto"].DisplayIndex = 1;
            grdDetPreSalida.Columns["CCODIGOPRODUCTO"].DisplayIndex = 2;
            grdDetPreSalida.Columns["CNOMBREPRODUCTO"].DisplayIndex = 3;
            grdDetPreSalida.Columns["CantidadEnt"].DisplayIndex = 4;
        }

        private void grdCabPreSalida_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void EncabezadoPantalla()
        {
            switch (_estatus)
            {
                case eEstatusOS.PRE:
                    break;
                case eEstatusOS.REC:
                    this.Text = this.Text + " - RECEPCION ";
                    break;
                case eEstatusOS.ASIG:
                    this.Text = this.Text + " - ASIGNACION ";
                    break;
                case eEstatusOS.TER:
                    this.Text = this.Text + " - TERMINAR ";
                    break;
                case eEstatusOS.ENT:
                    this.Text = this.Text + " - ENTREGAR ";
                    break;
                case eEstatusOS.CAN:
                    break;
                default:
                    break;
            }
        }

        private void LlenaComboRecibe()
        {
            try
            {
                resp = new CatPersonalSvc().getCatPersonalxFiltro(" per.idempresa = " + _Usuario.personal.empresa.clave);
                if (resp.typeResult == ResultTypes.success)
                {
                    listaRecibe = (List<CatPersonal>)resp.result;
                    cmbOpera1.DataSource = null;
                    cmbOpera1.DataSource = listaRecibe;

                    cmbOpera1.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                    cmbOpera1.AutoCompleteSource = AutoCompleteSource.ListItems;
                    cmbOpera1.DisplayMember = "NombreCompleto";
                    cmbOpera1.SelectedIndex = 0;
                }
                else
                {
                    //no se encontro
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + ex.Message,
                  this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private bool VerificacionTerminacion()
        {
            try
            {

                bool bCambio = false;
                bool bNoCambio = false;
                //CREAR LISTA DE UNIDAD
                CatTipoOrdServ cat = new CatTipoOrdServ();
                listaUnidadesaEnviar = new List<CatUnidadTrans>();
                if (txtidUnidadTrans.Text != "")
                {
                    AgregaUnidadListaEnvio(txtidUnidadTrans.Text);
                }
                //
                resp = new CatTipoOrdServSvc().getCatTipoOrdServxFilter(cabos.idTipOrdServ);
                if (resp.typeResult == ResultTypes.success)
                {
                    cat = ((List<CatTipoOrdServ>)resp.result)[0];
                }

                //Verificar que todas las Ordenes de trabajo sean Actividades
                foreach (var item in listaDetOS)
                {
                    //while (item.TipoNotaRecepcionF.ToUpper().Trim() != "ACTIVIDAD")
                    //{
                    //    fOrdenTrabajo fot = new fOrdenTrabajo(_Usuario, listaUnidadesaEnviar, cat, 0, item.idOrdenSer, item.idOrdenTrabajo);
                    //    fot.StartPosition = FormStartPosition.CenterParent;
                    //    fot.DevuelveValor();
                    //    CargaForma(opcBusqueda.CabOrdenServicio, txtidOrdenSer.Text);
                    //    bCambio = true;
                    //}
                    if (item.TipoNotaRecepcionF.ToUpper().Trim() != "ACTIVIDAD")
                    {

                        fOrdenTrabajo fot = new fOrdenTrabajo(_Usuario, listaUnidadesaEnviar, cat, false, 0, item.idOrdenSer, item.idOrdenTrabajo);
                        fot.StartPosition = FormStartPosition.CenterParent;
                        //fot.DevuelveValor();
                        DialogResult res = fot.ShowDialog();
                        CargaForma(opcBusqueda.CabOrdenServicio, txtidOrdenSer.Text);
                        if (res == DialogResult.OK)
                        {
                            bCambio = true;

                        }
                        else
                        {
                            bNoCambio = true;
                        }


                    }
                }



                if (bNoCambio)
                {
                    //RE - Carga
                    //CargaForma(opcBusqueda.CabOrdenServicio, txtidOrdenSer.Text);
                    mensaje = "Desea Cancelar la Terminación la Orden de Servicio : " + cabos.idOrdenSer.ToString() + " ? ";
                    caption = this.Name;
                    buttons = MessageBoxButtons.OKCancel;
                    result = MessageBox.Show(mensaje, caption, buttons);
                    if (result == System.Windows.Forms.DialogResult.OK)
                    {
                        return false;
                    }
                    else
                    {
                        CargaForma(opcBusqueda.CabOrdenServicio, txtidOrdenSer.Text);
                        VerificacionTerminacion();
                        return true;

                    }
                }
                else
                {
                    return true;


                    //Preguntar si quieres cancelar el proceso
                }
            }
            catch (Exception ex)
            {
                return false;
                MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + ex.Message,
                 this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void AgregaUnidadListaEnvio(string idUnidadTrans)
        {
            CatUnidadTrans unidad;
            OperationResult resp = new CatUnidadTransSvc().geCatUnidadTransxFilter(idUnidadTrans);
            if (resp.typeResult == ResultTypes.success)
            {
                unidad = ((List<CatUnidadTrans>)resp.result)[0];

                listaUnidadesaEnviar.Add(unidad);
                //listaUnidadesaEnviar = new List<CatUnidadTrans>();
            }
            else
            {
                //que pasa ?
            }
        }

        private void chkMarcaTodos_CheckedChanged(object sender, EventArgs e)
        {
            if (listaDetOS != null)
            {
                if (listaDetOS.Count > 0)
                {
                    foreach (var item in listaDetOS)
                    {
                        item.Seleccionado = chkMarcaTodos.Checked;
                    }
                    grdOT.Refresh();
                    VerificaActiva();
                }
            }
        }

        //private void VerificaCasosEspeciales()
        //{
        //    int contPen = 0;
        //    int contEnt = 0;
        //    int contTot = 0;
        //    int Index = 0;
        //    int IdOrdenTrabajo;
        //    //Recorrer cada Orden de Trabajo
        //    foreach (var item in listaDetOS)
        //    {
        //        IdOrdenTrabajo = item.idOrdenTrabajo;
        //        //Checar las presalidas de cada Orden de trabajo
        //        foreach (var itemP in listCabPresal)
        //        {
        //            if (item.idOrdenTrabajo == itemP.idOrdenTrabajo)
        //            {
        //                contTot++;
        //                if (itemP.Estatus == "PEN")
        //                {
        //                    contPen++;
        //                }
        //                else
        //                {
        //                    contEnt++;
        //                }
        //            }
        //            else
        //            {
        //                // break;
        //            }

        //        }
        //        if (contEnt == contTot)
        //        {
        //            //TODOS estan Entregados, se palomea la actividad y no se puede desmarcar
        //            listaDetOS[Index].Seleccionado = true;
        //            foreach (DataGridViewRow row in grdOT.Rows)
        //            {
        //                if (Convert.ToBoolean(row.Cells["Seleccionado"].Value))
        //                {
        //                    row.ReadOnly = true;
        //                }
        //            }
        //            grdOT.PerformLayout();
        //            contTot = 0;
        //            contPen = 0;
        //            contEnt = 0;
        //            Index++;
        //        }
        //        else
        //        {
        //            if (contPen == contTot)
        //            {

        //            }
        //            else if (contPen > 0)
        //            {
        //                foreach (var itemP2 in listCabPresal)
        //                {
        //                    if (itemP2.idOrdenTrabajo == IdOrdenTrabajo)
        //                    {
        //                        caption = this.Name;
        //                        buttons = MessageBoxButtons.OKCancel;
        //                        mensaje = "La Orden de Trabajo " + IdOrdenTrabajo + " tiene la Pre Salida " + itemP2.IdPreSalida + " Pendiente, Desea Cancelarla ?";
        //                        result = MessageBox.Show(mensaje, caption, buttons);
        //                        if (result == System.Windows.Forms.DialogResult.OK)
        //                        {
        //                            itemP2.Estatus = "CAN";
        //                            itemP2.FecCancela = DateTime.Now;
        //                            itemP2.UserCancela = _Usuario.nombreUsuario;
        //                            //itemP2.MotivoCancela = string(input("Motivo Cancelación:  "));
        //                        }
        //                        else
        //                        {
        //                            return;
        //                        }

        //                    }
        //                }



        //            }
        //        }
        //    }



        ////caso 1: insumo pendiente
        //foreach (var item in listCabPresal)
        //{
        //    if (item.Estatus == "PEN")
        //    {
        //        //checar la actividad
        //        foreach (var itemA in listaDetOS)
        //        {
        //            if(itemA.idOrdenTrabajo == item.idOrdenTrabajo)
        //            {
        //                //Mensaje
        //            }
        //        }
        //    }
        //}
        //}

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {
            if (_estatus == eEstatusOS.TER)
            {
                asignarToolStripMenuItem.Visible = false;
                nuevoToolStripMenuItem.Visible = true;
                cambiarToolStripMenuItem.Visible = true;
                eliminarToolStripMenuItem.Visible = true;
            }
            else if (_estatus == eEstatusOS.ASIG)
            {
                asignarToolStripMenuItem.Visible = true;
                nuevoToolStripMenuItem.Visible = false;
                cambiarToolStripMenuItem.Visible = false;
                eliminarToolStripMenuItem.Visible = false;

            }
            else
            {
                asignarToolStripMenuItem.Visible = false;
                nuevoToolStripMenuItem.Visible = false;
                cambiarToolStripMenuItem.Visible = false;
                eliminarToolStripMenuItem.Visible = false;
            }
        }

        private void eliminarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                string Motivo;
                PreSalaCancelar = "";
                BandExito = false;
                Indice = grdOT.CurrentCell.RowIndex;
                IdOrdenTrabajo = listaDetROS[Indice].idOrdenTrabajo;
                caption = this.Name;
                buttons = MessageBoxButtons.OKCancel;
                //mensaje = "Desea guardar la Liquidacion para el operador: " + txtNomOperador.Text;
                mensaje = "Desea Cancelar la Orden de Trabajo: " + IdOrdenTrabajo + " ?";


                //checar si tiene presalidas pendientes
                if (listCabPresal != null)
                {
                    if (listCabPresal.Count > 0)
                    {
                        foreach (var item in listCabPresal)
                        {
                            if (item.idOrdenTrabajo == IdOrdenTrabajo)
                            {
                                if (item.Estatus == "ENT")
                                {
                                    //No se puede Cancelar
                                    MessageBox.Show("No se puede Cancelar la Orden de Trabajo: " + IdOrdenTrabajo + " ya que tiene Salida de Insumo Entregada (" + item.IdPreSalida + ")", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    return;
                                }
                                else if (item.Estatus == "PEN")
                                {
                                    //Para Cancelar
                                    PreSalaCancelar = PreSalaCancelar + " " + item.IdPreSalida + ",";
                                }
                            }
                        }
                    }
                }

                //else
                //{
                //    //Continua
                //}
                if (PreSalaCancelar != "")
                {

                    if (PreSalaCancelar != "")
                    {
                        PreSalaCancelar = PreSalaCancelar.Substring(0, PreSalaCancelar.Length - 1);
                    }
                    mensaje = "Desea Cancelar la Orden de Trabajo: " + IdOrdenTrabajo + " ?" + Environment.NewLine
                        + "Con las Pre Salidas Pendientes de Entrega = " + PreSalaCancelar;
                }

                result = MessageBox.Show(mensaje, caption, buttons);
                if (result == System.Windows.Forms.DialogResult.OK)
                {


                    Motivo = "";
                    while (Motivo == "" || Motivo == null)
                    {
                        fAuxiliar fcan = new fAuxiliar("Cancelacion de Orden de Trabajo: " + listaDetOS[Indice].idOrdenTrabajo, "Motivo Cancelación", 150, true, CharacterCasing.Upper, HorizontalAlignment.Left);
                        fcan.StartPosition = FormStartPosition.CenterScreen;
                        Motivo = fcan.DevuelveValor();

                        if (Motivo == "" || Motivo == null)
                        {
                            mensaje = "El Motivo de la Cancelación no puede ser Vacia, Desea Abortar la Cancelación ?";
                            result = MessageBox.Show(mensaje, caption, buttons);
                            if (result == System.Windows.Forms.DialogResult.OK)
                            {
                                return;
                            }

                        }
                    }
                    listaDetROS[Indice].isCancelado = true;
                    listaDetROS[Indice].motivoCancelacion = Motivo;
                    listaDetOS[Indice].Estatus = "CAN";

                    detos = listaDetOS[Indice];
                    detros = listaDetROS[Indice];
                    resp = new DetOrdenServicioSvc().GuardaDetOrdenServicio(ref detos);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        BandExito = true;
                        resp = new DetRecepcionOSSvc().GuardaDetRecepcionOS(ref detros);
                        if (resp.typeResult == ResultTypes.success)
                        {
                            BandExito = true;
                            if (PreSalaCancelar != "")
                            {
                                foreach (var item in listCabPresal)
                                {
                                    if (item.idOrdenTrabajo == IdOrdenTrabajo)
                                    {
                                        if (item.Estatus == "PEN")
                                        {
                                            item.Estatus = "CAN";
                                            item.FecCancela = DateTime.Now;
                                            item.UserCancela = _Usuario.nombreUsuario;
                                            item.MotivoCancela = "Aplicada por cancelación de Orden de trabajo";

                                            cabpresal = item;
                                            resp = new PreSalidasSvc().GuardaCabPreSalida(ref cabpresal);
                                            if (resp.typeResult == ResultTypes.success)
                                            {
                                                BandExito = true;
                                            }
                                            else
                                            {
                                                BandExito = false;
                                            }
                                        }
                                    }

                                }
                            }
                        }
                        else
                        {
                            BandExito = false;
                        }



                        if (BandExito)
                        {
                            if (PreSalaCancelar != "")
                            {
                                MessageBox.Show("Se Cancelo la Orden de Trabajo " + detos.idOrdenTrabajo + Environment.NewLine
                                    + "Con las Pre Salidas: " + PreSalaCancelar, "Operación Exitosa", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                            else
                            {
                                MessageBox.Show("Se Cancelo la Orden de Trabajo " + detos.idOrdenTrabajo, "Operación Exitosa", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }

                            //actualizar detalle
                            CargaForma(opcBusqueda.CabOrdenServicio, txtidOrdenSer.Text);
                        }
                        else
                        {

                        }

                    }
                    else
                    {
                        //ocurrio un error

                    }
                }





            }
            catch (Exception)
            {

                //MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + ex.Message,
                //  this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void cambiarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                BandExito = false;
                Indice = grdOT.CurrentCell.RowIndex;
                IdOrdenTrabajo = listaDetROS[Indice].idOrdenTrabajo;
                if (Indice >= 0)
                {

                    //bool bCambio = false;
                    //CREAR LISTA DE UNIDAD
                    CatTipoOrdServ cat = new CatTipoOrdServ();
                    listaUnidadesaEnviar = new List<CatUnidadTrans>();
                    if (txtidUnidadTrans.Text != "")
                    {
                        AgregaUnidadListaEnvio(txtidUnidadTrans.Text);
                    }
                    //
                    resp = new CatTipoOrdServSvc().getCatTipoOrdServxFilter(cabos.idTipOrdServ);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        cat = ((List<CatTipoOrdServ>)resp.result)[0];
                    }

                    fOrdenTrabajo fot = new fOrdenTrabajo(_Usuario, listaUnidadesaEnviar, cat, false, 0,
                        listaDetOS[Indice].idOrdenSer, listaDetOS[Indice].idOrdenTrabajo);
                    fot.StartPosition = FormStartPosition.CenterParent;
                    //fot.DevuelveValor();
                    //actualizar detalle
                    CargaForma(opcBusqueda.CabOrdenServicio, txtidOrdenSer.Text);
                    //bCambio = true;

                }

            }
            catch (Exception)
            {
                //MessageBox.Show("Ocurrio un Error !!! " + System.Environment.NewLine + "Error: " + ex.Message,
                // this.Name, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            }
        }

        private void nuevoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Indice = grdOT.CurrentCell.RowIndex;

                CatTipoOrdServ cat = new CatTipoOrdServ();
                listaUnidadesaEnviar = new List<CatUnidadTrans>();
                if (txtidUnidadTrans.Text != "")
                {
                    AgregaUnidadListaEnvio(txtidUnidadTrans.Text);
                }
                //
                resp = new CatTipoOrdServSvc().getCatTipoOrdServxFilter(cabos.idTipOrdServ);
                if (resp.typeResult == ResultTypes.success)
                {
                    cat = ((List<CatTipoOrdServ>)resp.result)[0];
                }
                //listaDetOS[Indice].idOrdenSer
                fOrdenTrabajo fot = new fOrdenTrabajo(_Usuario, listaUnidadesaEnviar, cat, false, 0,
                         Convert.ToInt32(txtidOrdenSer.Text), 0, listaDetOS[Indice].idPersonalResp,
                         listaDetOS[Indice].idPersonalAyu1, listaDetOS[Indice].idPersonalAyu2);
                fot.StartPosition = FormStartPosition.CenterParent;
                //fot.DevuelveValor();
                CargaForma(opcBusqueda.CabOrdenServicio, txtidOrdenSer.Text);
            }
            catch (Exception)
            {


            }
        }

        private void txtidOrdenSer_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F3)
            {
                fConsulta busq = new fConsulta(opcBusqueda.CabOrdenServicio, _Usuario,
               " Ordenes de Servicio", "idOrdenSer", false,
               " c.estatus = '" + estatusValido.ToString() + "' and c.idTipOrdServ = " + (int)TipoOS, " C.idOrdenSer DESC");
                busq.StartPosition = FormStartPosition.CenterScreen;
                string valor = busq.DevuelveValor();

                if (valor != null)
                {
                    if (Convert.ToInt32(valor) > 0)
                    {
                        txtidOrdenSer.Text = valor;
                        CargaForma(opcBusqueda.CabOrdenServicio, txtidOrdenSer.Text);
                        if (_TipoOS == eTipoOrdenServicio.LLANTAS_ESP)
                        {
                            chkMarcaTodos.Checked = true;
                        }

                    }
                    else
                    {

                    }


                }
            }
        }

        private void Asigna()
        {
            //ASIGNAR TODOS
            DetRecepcionOS detRos;
            listaDetROS_Asigna = new List<DetRecepcionOS>();

            btnAsigna.Enabled = true;
            foreach (var item in listaDetOS)
            {
                if (item.Seleccionado)
                {
                    detRos = new DetRecepcionOS
                    {
                        idOrdenSer = item.idOrdenSer,
                        idOrdenTrabajo = item.idOrdenTrabajo,
                        idFamilia = item.idFamilia,
                        NotaRecepcion = item.NotaRecepcionF,
                        NotaRecepcionA = item.FallaReportada,
                        isCancelado = false,
                        motivoCancelacion = "",
                        idDivision = item.idDivision,
                        idPersonalResp = 0,
                        idPersonalAyu1 = 0,
                        idPersonalAyu2 = 0,
                        Inserta = false,
                        NomPersonalResp = "",
                        NomPersonalAyu1 = "",
                        NomPersonalAyu2 = ""
                    };
                    listaDetROS_Asigna.Add(detRos);
                }
            }
            //ASIGNAR TODOS
        }

        private void grdOT_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void grdOT_Click(object sender, EventArgs e)
        {
            VerificaActiva();
        }

        private void VerificaActiva()
        {
            bool BandOK = false;

            for (int i = 0; i < grdOT.Rows.Count; i++)
            {
                if ((bool)grdOT.Rows[i].Cells["Seleccionado"].Value)
                {
                    BandOK = true;
                    break;
                }
            }

            if (BandOK)
            {
                Asigna();
            }
        }
    }//
}
