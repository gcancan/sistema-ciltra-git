﻿namespace Fletera2.Procesos
{
    partial class fCompGastosFoliosDin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fCompGastosFoliosDin));
            this.ToolStripMenu = new System.Windows.Forms.ToolStrip();
            this.btnNuevo = new System.Windows.Forms.ToolStripButton();
            this.btnGrabar = new System.Windows.Forms.ToolStripButton();
            this.btnEditar = new System.Windows.Forms.ToolStripButton();
            this.btnReporte = new System.Windows.Forms.ToolStripButton();
            this.btnCancelar = new System.Windows.Forms.ToolStripButton();
            this.btnSalir = new System.Windows.Forms.ToolStripButton();
            this.gpoFaC = new System.Windows.Forms.GroupBox();
            this.txtNomRecibe = new System.Windows.Forms.TextBox();
            this.txtNomEntrega = new System.Windows.Forms.TextBox();
            this.txtNomTipo = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtSaldoFol = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtImporteCompFol = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtImporteFol = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dtpFechaFolHora = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpFechaFol = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.txtFolioDin = new System.Windows.Forms.TextBox();
            this.gpoCom = new System.Windows.Forms.GroupBox();
            this.txtConsecutivo = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtObservaciones = new System.Windows.Forms.TextBox();
            this.chkNoDocumento = new System.Windows.Forms.CheckBox();
            this.txtNoDocumento = new System.Windows.Forms.TextBox();
            this.chkAutoriza = new System.Windows.Forms.CheckBox();
            this.label15 = new System.Windows.Forms.Label();
            this.dtpFechaFC = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.txtTotal = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtIVA = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtImporte = new System.Windows.Forms.TextBox();
            this.cmbidGasto = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.cmbAutoriza = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtFolio = new System.Windows.Forms.TextBox();
            this.grdComprobantes = new System.Windows.Forms.DataGridView();
            this.menu2 = new System.Windows.Forms.ToolStrip();
            this.btnMenu2Agre = new System.Windows.Forms.ToolStripButton();
            this.btnMenu2Can = new System.Windows.Forms.ToolStripButton();
            this.btnMenu2Borrar = new System.Windows.Forms.ToolStripButton();
            this.ToolStripMenu.SuspendLayout();
            this.gpoFaC.SuspendLayout();
            this.gpoCom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdComprobantes)).BeginInit();
            this.menu2.SuspendLayout();
            this.SuspendLayout();
            // 
            // ToolStripMenu
            // 
            this.ToolStripMenu.AutoSize = false;
            this.ToolStripMenu.BackColor = System.Drawing.SystemColors.Control;
            this.ToolStripMenu.Dock = System.Windows.Forms.DockStyle.None;
            this.ToolStripMenu.Font = new System.Drawing.Font("Berlin Sans FB", 11.25F);
            this.ToolStripMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnNuevo,
            this.btnGrabar,
            this.btnEditar,
            this.btnReporte,
            this.btnCancelar,
            this.btnSalir});
            this.ToolStripMenu.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.ToolStripMenu.Location = new System.Drawing.Point(0, 0);
            this.ToolStripMenu.Name = "ToolStripMenu";
            this.ToolStripMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.ToolStripMenu.Size = new System.Drawing.Size(409, 65);
            this.ToolStripMenu.TabIndex = 9;
            this.ToolStripMenu.Text = "ToolStrip1";
            // 
            // btnNuevo
            // 
            this.btnNuevo.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnNuevo.Image = ((System.Drawing.Image)(resources.GetObject("btnNuevo.Image")));
            this.btnNuevo.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnNuevo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(54, 62);
            this.btnNuevo.Text = "&Nuevo";
            this.btnNuevo.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnNuevo.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnNuevo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnNuevo.Click += new System.EventHandler(this.btnNuevo_Click);
            // 
            // btnGrabar
            // 
            this.btnGrabar.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnGrabar.Image = ((System.Drawing.Image)(resources.GetObject("btnGrabar.Image")));
            this.btnGrabar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnGrabar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnGrabar.Name = "btnGrabar";
            this.btnGrabar.Size = new System.Drawing.Size(60, 62);
            this.btnGrabar.Text = "&Grabar";
            this.btnGrabar.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnGrabar.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnGrabar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnGrabar.Click += new System.EventHandler(this.btnGrabar_Click);
            // 
            // btnEditar
            // 
            this.btnEditar.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnEditar.Image = ((System.Drawing.Image)(resources.GetObject("btnEditar.Image")));
            this.btnEditar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnEditar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(52, 62);
            this.btnEditar.Text = "&Editar";
            this.btnEditar.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnEditar.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnEditar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnEditar.Visible = false;
            // 
            // btnReporte
            // 
            this.btnReporte.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnReporte.Image = ((System.Drawing.Image)(resources.GetObject("btnReporte.Image")));
            this.btnReporte.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnReporte.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnReporte.Name = "btnReporte";
            this.btnReporte.Size = new System.Drawing.Size(65, 62);
            this.btnReporte.Text = "&Reporte";
            this.btnReporte.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnReporte.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnReporte.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnReporte.Click += new System.EventHandler(this.btnReporte_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnCancelar.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar.Image")));
            this.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnCancelar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(69, 62);
            this.btnCancelar.Text = "&Cancelar";
            this.btnCancelar.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnCancelar.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnSalir.Image = ((System.Drawing.Image)(resources.GetObject("btnSalir.Image")));
            this.btnSalir.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnSalir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(55, 62);
            this.btnSalir.Text = "&S a l i r";
            this.btnSalir.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnSalir.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // gpoFaC
            // 
            this.gpoFaC.Controls.Add(this.txtNomRecibe);
            this.gpoFaC.Controls.Add(this.txtNomEntrega);
            this.gpoFaC.Controls.Add(this.txtNomTipo);
            this.gpoFaC.Controls.Add(this.label5);
            this.gpoFaC.Controls.Add(this.txtSaldoFol);
            this.gpoFaC.Controls.Add(this.label11);
            this.gpoFaC.Controls.Add(this.txtImporteCompFol);
            this.gpoFaC.Controls.Add(this.label9);
            this.gpoFaC.Controls.Add(this.txtImporteFol);
            this.gpoFaC.Controls.Add(this.label8);
            this.gpoFaC.Controls.Add(this.label4);
            this.gpoFaC.Controls.Add(this.label3);
            this.gpoFaC.Controls.Add(this.dtpFechaFolHora);
            this.gpoFaC.Controls.Add(this.label2);
            this.gpoFaC.Controls.Add(this.dtpFechaFol);
            this.gpoFaC.Controls.Add(this.label1);
            this.gpoFaC.Controls.Add(this.txtFolioDin);
            this.gpoFaC.Location = new System.Drawing.Point(12, 68);
            this.gpoFaC.Name = "gpoFaC";
            this.gpoFaC.Size = new System.Drawing.Size(479, 303);
            this.gpoFaC.TabIndex = 10;
            this.gpoFaC.TabStop = false;
            this.gpoFaC.Text = "Folio a Comprobar";
            // 
            // txtNomRecibe
            // 
            this.txtNomRecibe.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNomRecibe.Location = new System.Drawing.Point(90, 140);
            this.txtNomRecibe.Name = "txtNomRecibe";
            this.txtNomRecibe.Size = new System.Drawing.Size(375, 27);
            this.txtNomRecibe.TabIndex = 11;
            this.txtNomRecibe.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtNomEntrega
            // 
            this.txtNomEntrega.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtNomEntrega.Location = new System.Drawing.Point(90, 111);
            this.txtNomEntrega.Name = "txtNomEntrega";
            this.txtNomEntrega.Size = new System.Drawing.Size(375, 25);
            this.txtNomEntrega.TabIndex = 10;
            this.txtNomEntrega.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtNomTipo
            // 
            this.txtNomTipo.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtNomTipo.Location = new System.Drawing.Point(90, 80);
            this.txtNomTipo.Name = "txtNomTipo";
            this.txtNomTipo.Size = new System.Drawing.Size(263, 25);
            this.txtNomTipo.TabIndex = 9;
            this.txtNomTipo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Blue;
            this.label5.Location = new System.Drawing.Point(274, 250);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 18);
            this.label5.TabIndex = 36;
            this.label5.Text = "Saldo: $";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtSaldoFol
            // 
            this.txtSaldoFol.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtSaldoFol.Location = new System.Drawing.Point(342, 247);
            this.txtSaldoFol.Name = "txtSaldoFol";
            this.txtSaldoFol.Size = new System.Drawing.Size(123, 25);
            this.txtSaldoFol.TabIndex = 14;
            this.txtSaldoFol.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Blue;
            this.label11.Location = new System.Drawing.Point(223, 219);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(110, 18);
            this.label11.TabIndex = 34;
            this.label11.Text = "Comprobado: $";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtImporteCompFol
            // 
            this.txtImporteCompFol.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtImporteCompFol.Location = new System.Drawing.Point(342, 216);
            this.txtImporteCompFol.Name = "txtImporteCompFol";
            this.txtImporteCompFol.Size = new System.Drawing.Size(123, 25);
            this.txtImporteCompFol.TabIndex = 13;
            this.txtImporteCompFol.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Blue;
            this.label9.Location = new System.Drawing.Point(259, 188);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(74, 18);
            this.label9.TabIndex = 30;
            this.label9.Text = "Importe: $";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtImporteFol
            // 
            this.txtImporteFol.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtImporteFol.Location = new System.Drawing.Point(342, 185);
            this.txtImporteFol.Name = "txtImporteFol";
            this.txtImporteFol.Size = new System.Drawing.Size(123, 25);
            this.txtImporteFol.TabIndex = 12;
            this.txtImporteFol.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Blue;
            this.label8.Location = new System.Drawing.Point(11, 83);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(40, 18);
            this.label8.TabIndex = 25;
            this.label8.Text = "Tipo:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Blue;
            this.label4.Location = new System.Drawing.Point(12, 147);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 18);
            this.label4.TabIndex = 23;
            this.label4.Text = "Recibe:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Blue;
            this.label3.Location = new System.Drawing.Point(11, 115);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 18);
            this.label3.TabIndex = 21;
            this.label3.Text = "Entrega:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dtpFechaFolHora
            // 
            this.dtpFechaFolHora.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.dtpFechaFolHora.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpFechaFolHora.Location = new System.Drawing.Point(359, 50);
            this.dtpFechaFolHora.Name = "dtpFechaFolHora";
            this.dtpFechaFolHora.Size = new System.Drawing.Size(106, 25);
            this.dtpFechaFolHora.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Blue;
            this.label2.Location = new System.Drawing.Point(12, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 18);
            this.label2.TabIndex = 10;
            this.label2.Text = "Fecha:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dtpFechaFol
            // 
            this.dtpFechaFol.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.dtpFechaFol.Location = new System.Drawing.Point(90, 50);
            this.dtpFechaFol.Name = "dtpFechaFol";
            this.dtpFechaFol.Size = new System.Drawing.Size(263, 25);
            this.dtpFechaFol.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(11, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 18);
            this.label1.TabIndex = 7;
            this.label1.Text = "FOLIO:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtFolioDin
            // 
            this.txtFolioDin.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtFolioDin.Location = new System.Drawing.Point(90, 19);
            this.txtFolioDin.Name = "txtFolioDin";
            this.txtFolioDin.Size = new System.Drawing.Size(123, 25);
            this.txtFolioDin.TabIndex = 6;
            this.txtFolioDin.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFolioDin.TextChanged += new System.EventHandler(this.txtFolioDin_TextChanged);
            this.txtFolioDin.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFolioDin_KeyPress);
            // 
            // gpoCom
            // 
            this.gpoCom.Controls.Add(this.txtConsecutivo);
            this.gpoCom.Controls.Add(this.label14);
            this.gpoCom.Controls.Add(this.label13);
            this.gpoCom.Controls.Add(this.txtObservaciones);
            this.gpoCom.Controls.Add(this.chkNoDocumento);
            this.gpoCom.Controls.Add(this.txtNoDocumento);
            this.gpoCom.Controls.Add(this.chkAutoriza);
            this.gpoCom.Controls.Add(this.label15);
            this.gpoCom.Controls.Add(this.dtpFechaFC);
            this.gpoCom.Controls.Add(this.label6);
            this.gpoCom.Controls.Add(this.txtTotal);
            this.gpoCom.Controls.Add(this.label7);
            this.gpoCom.Controls.Add(this.txtIVA);
            this.gpoCom.Controls.Add(this.label10);
            this.gpoCom.Controls.Add(this.txtImporte);
            this.gpoCom.Controls.Add(this.cmbidGasto);
            this.gpoCom.Controls.Add(this.label12);
            this.gpoCom.Controls.Add(this.cmbAutoriza);
            this.gpoCom.Controls.Add(this.label16);
            this.gpoCom.Controls.Add(this.txtFolio);
            this.gpoCom.Location = new System.Drawing.Point(497, 68);
            this.gpoCom.Name = "gpoCom";
            this.gpoCom.Size = new System.Drawing.Size(389, 303);
            this.gpoCom.TabIndex = 11;
            this.gpoCom.TabStop = false;
            this.gpoCom.Text = "Comprobante";
            // 
            // txtConsecutivo
            // 
            this.txtConsecutivo.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtConsecutivo.Location = new System.Drawing.Point(315, 19);
            this.txtConsecutivo.Name = "txtConsecutivo";
            this.txtConsecutivo.Size = new System.Drawing.Size(62, 25);
            this.txtConsecutivo.TabIndex = 45;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Blue;
            this.label14.Location = new System.Drawing.Point(222, 22);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(87, 18);
            this.label14.TabIndex = 44;
            this.label14.Text = "Consecutivo:";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Blue;
            this.label13.Location = new System.Drawing.Point(6, 240);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(105, 18);
            this.label13.TabIndex = 43;
            this.label13.Text = "Observaciones:";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtObservaciones
            // 
            this.txtObservaciones.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtObservaciones.Location = new System.Drawing.Point(6, 264);
            this.txtObservaciones.Name = "txtObservaciones";
            this.txtObservaciones.Size = new System.Drawing.Size(371, 25);
            this.txtObservaciones.TabIndex = 30;
            this.txtObservaciones.TextChanged += new System.EventHandler(this.txtObservaciones_TextChanged);
            this.txtObservaciones.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtObservaciones_KeyPress);
            // 
            // chkNoDocumento
            // 
            this.chkNoDocumento.AutoSize = true;
            this.chkNoDocumento.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.chkNoDocumento.ForeColor = System.Drawing.Color.Blue;
            this.chkNoDocumento.Location = new System.Drawing.Point(6, 150);
            this.chkNoDocumento.Name = "chkNoDocumento";
            this.chkNoDocumento.Size = new System.Drawing.Size(102, 22);
            this.chkNoDocumento.TabIndex = 25;
            this.chkNoDocumento.Text = "Documento";
            this.chkNoDocumento.UseVisualStyleBackColor = true;
            // 
            // txtNoDocumento
            // 
            this.txtNoDocumento.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNoDocumento.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtNoDocumento.Location = new System.Drawing.Point(114, 145);
            this.txtNoDocumento.Name = "txtNoDocumento";
            this.txtNoDocumento.Size = new System.Drawing.Size(123, 25);
            this.txtNoDocumento.TabIndex = 26;
            this.txtNoDocumento.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNoDocumento_KeyPress);
            // 
            // chkAutoriza
            // 
            this.chkAutoriza.AutoSize = true;
            this.chkAutoriza.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.chkAutoriza.ForeColor = System.Drawing.Color.Blue;
            this.chkAutoriza.Location = new System.Drawing.Point(6, 114);
            this.chkAutoriza.Name = "chkAutoriza";
            this.chkAutoriza.Size = new System.Drawing.Size(83, 22);
            this.chkAutoriza.TabIndex = 23;
            this.chkAutoriza.Text = "Autoriza";
            this.chkAutoriza.UseVisualStyleBackColor = true;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Blue;
            this.label15.Location = new System.Drawing.Point(12, 88);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(52, 18);
            this.label15.TabIndex = 38;
            this.label15.Text = "Fecha:";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dtpFechaFC
            // 
            this.dtpFechaFC.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.dtpFechaFC.Location = new System.Drawing.Point(114, 83);
            this.dtpFechaFC.Name = "dtpFechaFC";
            this.dtpFechaFC.Size = new System.Drawing.Size(263, 25);
            this.dtpFechaFC.TabIndex = 22;
            this.dtpFechaFC.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dtpFechaFC_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Blue;
            this.label6.Location = new System.Drawing.Point(199, 236);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 18);
            this.label6.TabIndex = 36;
            this.label6.Text = "Total: ";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtTotal
            // 
            this.txtTotal.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtTotal.Location = new System.Drawing.Point(254, 233);
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.Size = new System.Drawing.Size(123, 25);
            this.txtTotal.TabIndex = 29;
            this.txtTotal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTotal_KeyPress);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Blue;
            this.label7.Location = new System.Drawing.Point(212, 205);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(36, 18);
            this.label7.TabIndex = 34;
            this.label7.Text = "IVA:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtIVA
            // 
            this.txtIVA.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtIVA.Location = new System.Drawing.Point(254, 202);
            this.txtIVA.Name = "txtIVA";
            this.txtIVA.Size = new System.Drawing.Size(123, 25);
            this.txtIVA.TabIndex = 28;
            this.txtIVA.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtIVA_KeyPress);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Blue;
            this.label10.Location = new System.Drawing.Point(185, 174);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(63, 18);
            this.label10.TabIndex = 30;
            this.label10.Text = "Importe:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtImporte
            // 
            this.txtImporte.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtImporte.Location = new System.Drawing.Point(254, 171);
            this.txtImporte.Name = "txtImporte";
            this.txtImporte.Size = new System.Drawing.Size(123, 25);
            this.txtImporte.TabIndex = 27;
            this.txtImporte.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtImporte_KeyPress);
            // 
            // cmbidGasto
            // 
            this.cmbidGasto.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.cmbidGasto.FormattingEnabled = true;
            this.cmbidGasto.Location = new System.Drawing.Point(114, 50);
            this.cmbidGasto.Name = "cmbidGasto";
            this.cmbidGasto.Size = new System.Drawing.Size(263, 26);
            this.cmbidGasto.TabIndex = 21;
            this.cmbidGasto.SelectedIndexChanged += new System.EventHandler(this.cmbidGasto_SelectedIndexChanged);
            this.cmbidGasto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbidGasto_KeyPress);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Blue;
            this.label12.Location = new System.Drawing.Point(11, 52);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(82, 18);
            this.label12.TabIndex = 25;
            this.label12.Text = "Tipo Gasto:";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cmbAutoriza
            // 
            this.cmbAutoriza.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.cmbAutoriza.FormattingEnabled = true;
            this.cmbAutoriza.Location = new System.Drawing.Point(114, 112);
            this.cmbAutoriza.Name = "cmbAutoriza";
            this.cmbAutoriza.Size = new System.Drawing.Size(263, 26);
            this.cmbAutoriza.TabIndex = 24;
            this.cmbAutoriza.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbAutoriza_KeyPress);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Blue;
            this.label16.Location = new System.Drawing.Point(11, 22);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(56, 18);
            this.label16.TabIndex = 7;
            this.label16.Text = "FOLIO:";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtFolio
            // 
            this.txtFolio.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.txtFolio.Location = new System.Drawing.Point(114, 19);
            this.txtFolio.Name = "txtFolio";
            this.txtFolio.Size = new System.Drawing.Size(102, 25);
            this.txtFolio.TabIndex = 20;
            this.txtFolio.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFolio_KeyPress);
            // 
            // grdComprobantes
            // 
            this.grdComprobantes.AllowUserToAddRows = false;
            this.grdComprobantes.AllowUserToDeleteRows = false;
            this.grdComprobantes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdComprobantes.Location = new System.Drawing.Point(12, 377);
            this.grdComprobantes.Name = "grdComprobantes";
            this.grdComprobantes.Size = new System.Drawing.Size(874, 143);
            this.grdComprobantes.TabIndex = 94;
            this.grdComprobantes.Click += new System.EventHandler(this.grdComprobantes_Click);
            // 
            // menu2
            // 
            this.menu2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.menu2.AutoSize = false;
            this.menu2.BackColor = System.Drawing.SystemColors.Control;
            this.menu2.Dock = System.Windows.Forms.DockStyle.None;
            this.menu2.Font = new System.Drawing.Font("Berlin Sans FB", 11.25F);
            this.menu2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnMenu2Agre,
            this.btnMenu2Can,
            this.btnMenu2Borrar});
            this.menu2.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.VerticalStackWithOverflow;
            this.menu2.Location = new System.Drawing.Point(898, 69);
            this.menu2.Name = "menu2";
            this.menu2.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.menu2.Size = new System.Drawing.Size(65, 302);
            this.menu2.TabIndex = 95;
            this.menu2.Text = "ToolStrip1";
            this.menu2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.menu2_KeyDown);
            // 
            // btnMenu2Agre
            // 
            this.btnMenu2Agre.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnMenu2Agre.Image = ((System.Drawing.Image)(resources.GetObject("btnMenu2Agre.Image")));
            this.btnMenu2Agre.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnMenu2Agre.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnMenu2Agre.Name = "btnMenu2Agre";
            this.btnMenu2Agre.Size = new System.Drawing.Size(63, 38);
            this.btnMenu2Agre.Text = "Agregar";
            this.btnMenu2Agre.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnMenu2Agre.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnMenu2Agre.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnMenu2Agre.Click += new System.EventHandler(this.btnMenu2Agre_Click);
            // 
            // btnMenu2Can
            // 
            this.btnMenu2Can.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnMenu2Can.Image = ((System.Drawing.Image)(resources.GetObject("btnMenu2Can.Image")));
            this.btnMenu2Can.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnMenu2Can.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnMenu2Can.Name = "btnMenu2Can";
            this.btnMenu2Can.Size = new System.Drawing.Size(63, 38);
            this.btnMenu2Can.Text = "&Cancelar";
            this.btnMenu2Can.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnMenu2Can.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnMenu2Can.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnMenu2Can.Click += new System.EventHandler(this.btnMenu2Can_Click);
            // 
            // btnMenu2Borrar
            // 
            this.btnMenu2Borrar.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.btnMenu2Borrar.Image = global::Fletera2.Properties.Resources.deleteCancelar;
            this.btnMenu2Borrar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnMenu2Borrar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnMenu2Borrar.Name = "btnMenu2Borrar";
            this.btnMenu2Borrar.Size = new System.Drawing.Size(63, 38);
            this.btnMenu2Borrar.Text = "&Borrar";
            this.btnMenu2Borrar.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnMenu2Borrar.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnMenu2Borrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // fCompGastosFoliosDin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(972, 534);
            this.Controls.Add(this.menu2);
            this.Controls.Add(this.grdComprobantes);
            this.Controls.Add(this.gpoCom);
            this.Controls.Add(this.gpoFaC);
            this.Controls.Add(this.ToolStripMenu);
            this.Name = "fCompGastosFoliosDin";
            this.Text = "Comprobación de Gastos";
            this.Load += new System.EventHandler(this.fCompGastosFoliosDin_Load);
            this.ToolStripMenu.ResumeLayout(false);
            this.ToolStripMenu.PerformLayout();
            this.gpoFaC.ResumeLayout(false);
            this.gpoFaC.PerformLayout();
            this.gpoCom.ResumeLayout(false);
            this.gpoCom.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdComprobantes)).EndInit();
            this.menu2.ResumeLayout(false);
            this.menu2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.ToolStrip ToolStripMenu;
        private System.Windows.Forms.ToolStripButton btnNuevo;
        private System.Windows.Forms.ToolStripButton btnGrabar;
        private System.Windows.Forms.ToolStripButton btnEditar;
        private System.Windows.Forms.ToolStripButton btnReporte;
        private System.Windows.Forms.ToolStripButton btnCancelar;
        private System.Windows.Forms.ToolStripButton btnSalir;
        private System.Windows.Forms.GroupBox gpoFaC;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtFolioDin;
        private System.Windows.Forms.DateTimePicker dtpFechaFolHora;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpFechaFol;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtImporteFol;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtImporteCompFol;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtSaldoFol;
        private System.Windows.Forms.GroupBox gpoCom;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtTotal;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtIVA;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtImporte;
        private System.Windows.Forms.ComboBox cmbidGasto;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cmbAutoriza;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtFolio;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.DateTimePicker dtpFechaFC;
        private System.Windows.Forms.CheckBox chkAutoriza;
        private System.Windows.Forms.CheckBox chkNoDocumento;
        private System.Windows.Forms.TextBox txtNoDocumento;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtObservaciones;
        private System.Windows.Forms.DataGridView grdComprobantes;
        private System.Windows.Forms.TextBox txtNomRecibe;
        private System.Windows.Forms.TextBox txtNomEntrega;
        private System.Windows.Forms.TextBox txtNomTipo;
        internal System.Windows.Forms.ToolStrip menu2;
        private System.Windows.Forms.ToolStripButton btnMenu2Agre;
        private System.Windows.Forms.ToolStripButton btnMenu2Borrar;
        private System.Windows.Forms.ToolStripButton btnMenu2Can;
        private System.Windows.Forms.TextBox txtConsecutivo;
        private System.Windows.Forms.Label label14;
    }
}