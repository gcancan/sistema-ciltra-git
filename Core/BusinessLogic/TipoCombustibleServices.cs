﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Core.Utils;
using CoreFletera.Models;
using Core.Models;
using System.Data.SqlClient;

namespace CoreFletera.BusinessLogic
{
    public class TipoCombustibleServices
    {
        public OperationResult saveTipoCombustible(ref TipoCombustible tipoCombustible)
        {
            try
            {
                using (SqlConnection conection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comand = conection.CreateCommand())
                    {
                        comand.CommandType = CommandType.StoredProcedure;
                        comand.CommandText = "sp_saveTipoCombustible";

                        comand.Parameters.Add(new SqlParameter("@tipoCombustible", tipoCombustible.tipoCombustible));
                        comand.Parameters.Add(new SqlParameter("@usuario", tipoCombustible.usuario));
                        var spValor = comand.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var spMensaje = comand.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        var id = comand.Parameters.Add(new SqlParameter("@id", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        conection.Open();
                        var i = comand.ExecuteNonQuery();
                        if ((int)spValor.Value == 0)
                        {
                            tipoCombustible.idTipoCombustible = (int)id.Value;
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = tipoCombustible, };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }

        internal OperationResult getTiposCombustibleAllOrById(int idTipoCombustible)
        {
            try
            {
                using (SqlConnection conection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comand = conection.CreateCommand())
                    {
                        comand.CommandType = CommandType.StoredProcedure;
                        comand.CommandText = "sp_getTiposCombustiblesAllOrById";

                        comand.Parameters.Add(new SqlParameter("@id", idTipoCombustible));

                        var spValor = comand.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var spMensaje = comand.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                        conection.Open();
                        List<TipoCombustible> list = new List<TipoCombustible>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                TipoCombustible tipoCom = new TipoCombustible
                                {
                                    idTipoCombustible = (int)sqlReader["idTipoCombustible"],
                                    tipoCombustible = (string)sqlReader["tipoCombustible"],
                                    usuario = (string)sqlReader["usuario"],
                                    ultimoPrecio = (decimal)sqlReader["precio"]
                                };
                                list.Add(tipoCom);
                            }
                        }

                        if (idTipoCombustible == 0)
                        {
                            return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = list };
                            //como el id es 0 me traera todos, por eso el result es toda la lista (list)
                        }
                        else
                        {
                            return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = list[0] };
                            // como el id es diferente de 0 solo me traera un registro por eso de la lista solo tomo el primero (list[0])
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }
    }
}
