﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using System.Data;
using Core.Utils;
using System.Data.SqlClient;
namespace CoreFletera.BusinessLogic
{
    public class UbicacionUnidadesServicios
    {
        public OperationResult saveUbicacionUnidades(List<UbicacionUnidades> listaUbicaciones)
        {
            using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
            {
                con.Open();
                using (SqlTransaction tran = con.BeginTransaction("save"))
                {
                    using (SqlCommand comand = CrearSqlCommand.getCommand(con, "sp_saveUbicacionUnidades"))
                    {
                        comand.Transaction = tran;
                        comand.Parameters.Add(new SqlParameter("@idUnidad", ""));
                        comand.Parameters.Add(new SqlParameter("@tipoMovimiento", 0));
                        comand.Parameters.Add(new SqlParameter("@lugar", ""));
                        comand.Parameters.Add(new SqlParameter("@descripcion", ""));
                        comand.Parameters.Add(new SqlParameter("@folio", 0));
                        comand.Parameters.Add(new SqlParameter("@estatus", 0));

                        foreach (var item in listaUbicaciones)
                        {
                            comand.Parameters["@idUnidad"].Value = item.unidadTrans.clave;
                            comand.Parameters["@tipoMovimiento"].Value = (int)item.tipoMovimiento;
                            comand.Parameters["@lugar"].Value = item.ubicacionActual.ToString();
                            comand.Parameters["@descripcion"].Value = item.descripcion;
                            comand.Parameters["@folio"].Value = item.folio;
                            comand.Parameters["@estatus"].Value = (int)item.ubicacionNew;

                            comand.ExecuteNonQuery();
                            if ((int)comand.Parameters["@valor"].Value != 0)
                            {
                                tran.Rollback("save");
                                return new OperationResult(comand);
                            }
                        }
                        tran.Commit();
                        return new OperationResult(comand);
                    }
                }
            }
        }
    }
}
