﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using Core.Utils;
using CoreFletera.Models;
using System.Data;
using System.Data.SqlClient;
using Core.BusinessLogic;

namespace CoreFletera.BusinessLogic
{
    public class SolicitudOrdenCompraServices
    {
        public OperationResult saveSolicitudOrdenCompra(ref SolicitudOrdenCompra solicitud, List<int> listaPreOC)
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    connection.Open();
                    using (SqlTransaction transaction = connection.BeginTransaction("saveSolicitud"))
                    {
                        using (SqlCommand commad = connection.CreateCommand())
                        {
                            commad.Transaction = transaction;
                            commad.CommandType = CommandType.StoredProcedure;
                            commad.CommandText = "sp_saveSolicitudOrdenCompra";

                            commad.Parameters.Add(new SqlParameter("@idSolicitudOrdenCompra", solicitud.idSolicitudOrdenCompra));
                            commad.Parameters.Add(new SqlParameter("@idEmpresa", solicitud.empresa.clave));
                            commad.Parameters.Add(new SqlParameter("@fechaSolicitud", solicitud.fecha));
                            commad.Parameters.Add(new SqlParameter("@usuario", solicitud.usuario));
                            commad.Parameters.Add(new SqlParameter("@isLlanta", solicitud.isLlantas));
                            commad.Parameters.Add(new SqlParameter("@observaciones", solicitud.obsevaciones));
                            commad.Parameters.Add(new SqlParameter("@estatus", solicitud.estatus));

                            SqlParameter spValor = commad.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                            SqlParameter spId = commad.Parameters.Add(new SqlParameter("@id", SqlDbType.Int) { Direction = ParameterDirection.Output });
                            SqlParameter spMensaje = commad.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                            commad.ExecuteNonQuery();
                            if ((int)spValor.Value == 0)
                            {
                                solicitud.idSolicitudOrdenCompra = (int)spId.Value;
                                commad.CommandText = "sp_saveDetallesSolicitudOrdenCompra";
                                foreach (var item in solicitud.listaDetalles)
                                {
                                    commad.Parameters.Clear();
                                    commad.Parameters.Add(new SqlParameter("@idSolicitudOrdenCompra", solicitud.idSolicitudOrdenCompra));
                                    commad.Parameters.Add(new SqlParameter("@idDetalleSolicitudOrdenCompra", item.idDetalleSolitudOrdenCompra));
                                    commad.Parameters.Add(new SqlParameter("@medida", item.medidaLlanta));
                                    commad.Parameters.Add(new SqlParameter("@idProductoLlanta", item.productoLlanta == null ? null : (int ?)item.productoLlanta.idLLantasProducto));
                                    commad.Parameters.Add(new SqlParameter("@idProducto", item.producto == null ? null : (int ?)item.producto.idProducto));
                                    commad.Parameters.Add(new SqlParameter("@idUnidadTrans", item.unidadTrans == null ? null : item.unidadTrans.clave));
                                    commad.Parameters.Add(new SqlParameter("@cantidad", item.cantidad));

                                    spValor = commad.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                                    spId = commad.Parameters.Add(new SqlParameter("@id", SqlDbType.Int) { Direction = ParameterDirection.Output });
                                    spMensaje = commad.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                                    commad.ExecuteNonQuery();
                                    if ((int)spValor.Value == 0)
                                    {
                                        item.idDetalleSolitudOrdenCompra = (int)spId.Value;
                                    }
                                    else
                                    {
                                        transaction.Rollback("saveSolicitud");
                                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = null };
                                    }
                                }
                                commad.CommandType = CommandType.Text;
                                commad.CommandText = upDatePreOc;
                                foreach (int item in listaPreOC)
                                {
                                    commad.Parameters.Clear();
                                    commad.Parameters.Add(new SqlParameter("@idPreOc", item));
                                    commad.Parameters.Add(new SqlParameter("@idSolicitud", solicitud.idSolicitudOrdenCompra));
                                    spValor = commad.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                                    spMensaje = commad.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                                    commad.ExecuteNonQuery();
                                    if ((int)spValor.Value != 0)
                                    {
                                        transaction.Rollback("saveSolicitud");
                                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = null };
                                    }
                                }
                                transaction.Commit();
                                return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value , result = solicitud};
                            }
                            else
                            {
                                transaction.Rollback("saveSolicitud");
                                return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = null };
                            }                            
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }

        string upDatePreOc = @"
BEGIN TRY
    UPDATE  dbo.CabPreOC
    SET     Estatus = 'SOL' ,
            idSolOC = @idSolicitud
    WHERE   IdPreOC = @idPreOc;
    
    SET @valor = 0;
    SET @mensaje = 'CAMBIOS REALIZADOS';
END TRY
BEGIN CATCH
    SET @valor = 1;
    SET @mensaje = ERROR_MESSAGE();
END CATCH;";

        internal OperationResult getSolicitudOrdComAllOrById(int idSolicitud = 0)
        {
            try
            {
                using (SqlConnection conection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand commad = conection.CreateCommand())
                    {
                        commad.CommandType = CommandType.StoredProcedure;
                        commad.CommandText = "sp_getSolicitudOrdComAllOrBy";

                        commad.Parameters.Add(new SqlParameter("@idSolicitud", idSolicitud));

                        SqlParameter spValor = commad.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        SqlParameter spMensaje = commad.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                        conection.Open();
                        List<SolicitudOrdenCompra> lista = new List<SolicitudOrdenCompra>();
                        using (SqlDataReader sqlReader = commad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                SolicitudOrdenCompra solicitud = new mapComunes().mapSolicitudOrden(sqlReader);
                                if (solicitud == null)
                                {
                                    return new OperationResult { valor = 1, mensaje = "Error Al leer los resultados de la busqueda (Solicitud Orden De Compra)", result = null };
                                }

                                solicitud.listaDetalles = new List<DetalleSolicitudOrdenCompra>();
                                var resp = getDetSolicituById(solicitud.idSolicitudOrdenCompra, solicitud.isLlantas);
                                if (resp.typeResult == ResultTypes.success)
                                {
                                    solicitud.listaDetalles = (List<DetalleSolicitudOrdenCompra>)resp.result;
                                }
                                else
                                {
                                    return resp;
                                }
                                lista.Add(solicitud);
                            }
                        }
                        if (idSolicitud == 0)
                        {
                            return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = lista };
                        }
                        else
                        {
                            if (lista.Count > 0)
                            {
                                return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = lista[0] };
                            }
                            else
                            {
                                return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = null };
                            }
                            
                        }                        
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }

        internal OperationResult getDetSolicituById(int idSolicitud, bool isLlanta)
        {
            try
            {
                using (SqlConnection conection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand commad = conection.CreateCommand())
                    {
                        commad.CommandType = CommandType.StoredProcedure;
                        commad.CommandText = "sp_getDetSolicituById";

                        commad.Parameters.Add(new SqlParameter("@idSolicitud", idSolicitud));
                        commad.Parameters.Add(new SqlParameter("@isLlantas", isLlanta));

                        SqlParameter spValor = commad.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        SqlParameter spMensaje = commad.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                        conection.Open();
                        List<DetalleSolicitudOrdenCompra> lista = new List<DetalleSolicitudOrdenCompra>();
                        using (SqlDataReader sqlReader = commad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                DetalleSolicitudOrdenCompra detalle = new mapComunes().mapDetalleSolicitudOrdCom(sqlReader, isLlanta);
                                if (detalle == null)
                                {
                                    return new OperationResult { valor = 1, mensaje = "Error Al leer los resultados de la busqueda (Detalle Solicitud Orden De Compra)", result = null };
                                }
                                lista.Add(detalle);
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = lista };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }

        internal OperationResult cancelarSolicitudOrdCom(int idSolicitud)
        {
            try
            {
                using (SqlConnection conection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand commad = conection.CreateCommand())
                    {
                        commad.CommandType = CommandType.StoredProcedure;
                        commad.CommandText = "sp_cancelarSolicitudOrdCom";

                        commad.Parameters.Add(new SqlParameter("@idSolicitud", idSolicitud));

                        SqlParameter spValor = commad.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        SqlParameter spMensaje = commad.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                        conection.Open();
                        commad.ExecuteNonQuery();
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = null };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }
    }
}
