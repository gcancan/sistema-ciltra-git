﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using Core.BusinessLogic;
using Core.Utils;
using System.Data.SqlClient;
using System.Data;
namespace CoreFletera.BusinessLogic
{
    public class EvaluacionDesempeñoService
    {
        public OperationResult buscarDescripcionesEvaluacion()
        {
			try
			{
				using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
				{
					using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getDescripcionesEvaluacion"))
					{
						List<DescripcionEvaluacion> lista = new List<DescripcionEvaluacion>();
						con.Open();
						using (SqlDataReader reader = comad.ExecuteReader())
						{
							while (reader.Read())
							{
								DescripcionEvaluacion des = new mapComunes().mapDescripcionEvaluacion(reader);
								if (des == null) return new OperationResult(ResultTypes.error, "ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");
								lista.Add(des);
							}
						}
						return new OperationResult(comad, lista);
					}
				}
			}
			catch (Exception ex)
			{
				return new OperationResult(ex);
			}
        }
		public OperationResult saveEvaluacionDesempeño(ref EvaluacionDesempeño evaluacion)
		{
			try
			{
				using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
				{
					con.Open();
					using (SqlTransaction tran = con.BeginTransaction())
					{
						using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_saveEvaluacionDesempeño", true))
						{
							comad.Transaction = tran;
							comad.Parameters.Add(new SqlParameter("@idEvaluacionDesempeño", evaluacion.idEvaluacionDesempeño));
							comad.Parameters.Add(new SqlParameter("@fecha", evaluacion.fecha));
							comad.Parameters.Add(new SqlParameter("@idPersonalEvalua", evaluacion.personalEvalua.clave));
							comad.Parameters.Add(new SqlParameter("@idPersonalEvaluada", evaluacion.personalEvaluada.clave));
							comad.Parameters.Add(new SqlParameter("@contDelCalf", evaluacion.conDelCalf));
							comad.Parameters.Add(new SqlParameter("@contDefinicion", evaluacion.contDefiniciones));
							comad.Parameters.Add(new SqlParameter("@resultado", evaluacion.resultado));
							comad.Parameters.Add(new SqlParameter("@mejoras", evaluacion.factoresMejora));
							comad.Parameters.Add(new SqlParameter("@observaciones", evaluacion.observaciones));
							comad.Parameters.Add(new SqlParameter("@compromiso", evaluacion.compromiso));
							comad.Parameters.Add(new SqlParameter("@comentarios", evaluacion.comentarios));
							comad.Parameters.Add(new SqlParameter("@usuarioCierra", evaluacion.usuarioCierra));
							comad.Parameters.Add(new SqlParameter("@fechaCierra", evaluacion.fechaCierra));
							comad.Parameters.Add(new SqlParameter("@estatus", evaluacion.estatus));

							int id = 0;
							string mensaje = string.Empty;
							comad.ExecuteNonQuery();
							if (CrearSqlCommand.validarCorrecto(comad, out id))
							{
								mensaje = (string)comad.Parameters["@mensaje"].Value;
								evaluacion.idEvaluacionDesempeño = id;
								comad.CommandText = "sp_saveDetalleEvaluacionDesempeño";
								foreach (var detalle in evaluacion.listaDetalles)
								{
									comad.Parameters.Clear();
									foreach (var param in CrearSqlCommand.getPatametros(true))
									{
										comad.Parameters.Add(param);
									}
									comad.Parameters.Add(new SqlParameter("@idEvaluacionDesempeño", evaluacion.idEvaluacionDesempeño));
									comad.Parameters.Add(new SqlParameter("@idDetalleEvaluacionDesempeño", detalle.idDetalleEvaluacionDesempeño));
									comad.Parameters.Add(new SqlParameter("@idDescripcionEvaluacion", detalle.descripcionEvaluacion.idDescripcionEvaluacion));
									comad.Parameters.Add(new SqlParameter("@valoracion", detalle.valoracion));
									comad.Parameters.Add(new SqlParameter("@porcentaje", detalle.porcentaje));
									comad.Parameters.Add(new SqlParameter("@conCalf", detalle.contCalf));
									comad.Parameters.Add(new SqlParameter("@resultado", detalle.resultado));
									comad.ExecuteNonQuery();
									if (CrearSqlCommand.validarCorrecto(comad, out id))
									{
										detalle.idDetalleEvaluacionDesempeño = id;
									}
									else
									{
										tran.Rollback();
										return new OperationResult(comad);
									}
								}
							}
							else
							{
								tran.Rollback();
								return new OperationResult(comad);
							}

							tran.Commit();
							comad.Parameters["@mensaje"].Value = mensaje;
							return new OperationResult(comad, evaluacion);
						}
					}
				}
			}
			catch (Exception ex)
			{
				return new OperationResult(ex);
			}
		}
		public OperationResult getEvaluacionesByDepartamento(int idDepto)
		{
			try
			{
				using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
				{
					using (SqlCommand comand = CrearSqlCommand.getCommand(con, "sp_getEvaluacionesByDepto"))
					{
						comand.Parameters.Add(new SqlParameter("@idDepto", idDepto));
						List<EvaluacionDesempeño> listaEvaluaciones = new List<EvaluacionDesempeño>();
						con.Open();
						using (SqlDataReader reader = comand.ExecuteReader())
						{
							while (reader.Read())
							{
								EvaluacionDesempeño evaluacion = new mapComunes().mapEvaluacionDesempeño(reader);
								if (evaluacion == null) return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR LOS RESULTADOS DE LA BUSQUEDA");
								listaEvaluaciones.Add(evaluacion);
							}
						}
						var resp = getDetallesEvaluacionByFolios(listaEvaluaciones.Select(s => s.idEvaluacionDesempeño.ToString()).ToList());
						if (resp.typeResult==ResultTypes.success)
						{
							var listaDet = resp.result as List<DetalleEvaluacionDesempeño>;
							foreach (var item in listaEvaluaciones)
							{
								item.listaDetalles = listaDet.FindAll(s => s.idEvaluacionDesempeño == item.idEvaluacionDesempeño);
							}
						}
						else
						{
							return resp;
						}

						return new OperationResult(comand, listaEvaluaciones);
					}
				}
			}
			catch (Exception ex)
			{
				return new OperationResult(ex);
			}
		}
		public OperationResult getEvaluacionesByIdPersonal(int idPersonal)
		{
			try
			{
				using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
				{
					using (SqlCommand comand = CrearSqlCommand.getCommand(con, "sp_getEvaluacionesByIdPersonal"))
					{
						comand.Parameters.Add(new SqlParameter("@idPersonal", idPersonal));
						List<EvaluacionDesempeño> listaEvaluaciones = new List<EvaluacionDesempeño>();
						con.Open();
						using (SqlDataReader reader = comand.ExecuteReader())
						{
							while (reader.Read())
							{
								EvaluacionDesempeño evaluacion = new mapComunes().mapEvaluacionDesempeño(reader);
								if (evaluacion == null) return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR LOS RESULTADOS DE LA BUSQUEDA");
								listaEvaluaciones.Add(evaluacion);
							}
						}
						var resp = getDetallesEvaluacionByFolios(listaEvaluaciones.Select(s => s.idEvaluacionDesempeño.ToString()).ToList());
						if (resp.typeResult == ResultTypes.success)
						{
							var listaDet = resp.result as List<DetalleEvaluacionDesempeño>;
							foreach (var item in listaEvaluaciones)
							{
								item.listaDetalles = listaDet.FindAll(s => s.idEvaluacionDesempeño == item.idEvaluacionDesempeño);
							}
						}
						else
						{
							return resp;
						}

						return new OperationResult(comand, listaEvaluaciones);
					}
				}
			}
			catch (Exception ex)
			{
				return new OperationResult(ex);
			}
		}
		public OperationResult getAlltEvaluaciones()
		{
			try
			{
				using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
				{
					using (SqlCommand comand = CrearSqlCommand.getCommand(con, "sp_getAllEvaluaciones"))
					{						
						List<EvaluacionDesempeño> listaEvaluaciones = new List<EvaluacionDesempeño>();
						con.Open();
						using (SqlDataReader reader = comand.ExecuteReader())
						{
							while (reader.Read())
							{
								EvaluacionDesempeño evaluacion = new mapComunes().mapEvaluacionDesempeño(reader);
								if (evaluacion == null) return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR LOS RESULTADOS DE LA BUSQUEDA");
								listaEvaluaciones.Add(evaluacion);
							}
						}
						var resp = getDetallesEvaluacionByFolios(listaEvaluaciones.Select(s => s.idEvaluacionDesempeño.ToString()).ToList());
						if (resp.typeResult == ResultTypes.success)
						{
							var listaDet = resp.result as List<DetalleEvaluacionDesempeño>;
							foreach (var item in listaEvaluaciones)
							{
								item.listaDetalles = listaDet.FindAll(s => s.idEvaluacionDesempeño == item.idEvaluacionDesempeño);
							}
						}
						else
						{
							return resp;
						}
						return new OperationResult(comand, listaEvaluaciones);
					}
				}
			}
			catch (Exception ex)
			{
				return new OperationResult(ex);
			}
		}
		public OperationResult getDetallesEvaluacionByFolios(List<string> listaId)
		{
			try
			{
				string xml = GenerateXML.generarListaCadena(listaId);
				using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
				{
					using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getDetallesEvaluacionesById"))
					{
						comad.Parameters.Add(new SqlParameter("@xml", xml));
						con.Open();
						List<DetalleEvaluacionDesempeño> lista = new List<DetalleEvaluacionDesempeño>();
						using (SqlDataReader reader = comad.ExecuteReader())
						{
							while (reader.Read())
							{
								DetalleEvaluacionDesempeño detalle = new mapComunes().mapDetalleEvaluacion(reader);
								if (detalle == null) new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");
								lista.Add(detalle);
							}
						}
						return new OperationResult(comad, lista);
					}
				}
			}
			catch (Exception ex)
			{
				return new OperationResult(ex);
			}
		}
    }
}
