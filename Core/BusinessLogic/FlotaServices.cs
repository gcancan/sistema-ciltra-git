﻿using Core.Interfaces;
using Core.Models;
using Core.Utils;
using CoreFletera.BusinessLogic;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
//using CoreFletera.Models;
using Core.BusinessLogic;

namespace CoreFletera.BusinessLogic
{
    public class FlotaServices
    {
        public OperationResult saveFlota(ref Flota flota)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    con.Open();
                    using (SqlTransaction tran = con.BeginTransaction())
                    {
                        using (SqlCommand comando = CrearSqlCommand.getCommand(con, "sp_saveFlota", true))
                        {
                            comando.Transaction = tran;

                            comando.Parameters.Add(new SqlParameter("@idFlota", flota.idFlota));
                            comando.Parameters.Add(new SqlParameter("@idEmpresa", flota.empresa.clave));
                            comando.Parameters.Add(new SqlParameter("@idZonaOperativa", flota.zonaOperativa.idZonaOperativa));
                            comando.Parameters.Add(new SqlParameter("@idCliente", flota.cliente == null ? null : (int?)flota.cliente.clave));
                            comando.Parameters.Add(new SqlParameter("@idTractor", flota.tractor == null ? null : flota.tractor.clave));
                            comando.Parameters.Add(new SqlParameter("@idRemolque1", flota.remolque1 == null ? null : flota.remolque1.clave));
                            comando.Parameters.Add(new SqlParameter("@idDolly", flota.dolly == null ? null : flota.dolly.clave));
                            comando.Parameters.Add(new SqlParameter("@idRemolque2", flota.remolque2 == null ? null : flota.remolque2.clave));

                            comando.ExecuteNonQuery();
                            int id = 0;
                            if (CrearSqlCommand.validarCorrecto(comando, out id))
                            {
                                flota.idFlota = id;
                                tran.Commit();
                            }
                            else
                            {
                                tran.Rollback();
                            }
                            return new OperationResult(comando, flota);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult eliminarFlota(Flota flota)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comando = CrearSqlCommand.getCommand(con, "sp_eliminarFlota"))
                    {
                        comando.Parameters.Add(new SqlParameter("@idFlota", flota.idFlota));
                        con.Open();
                        comando.ExecuteNonQuery();
                        return new OperationResult(comando);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getFlota(OperacionFletera operacionFletera)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand commad = CrearSqlCommand.getCommand(con, "sp_getFlota"))
                    {
                        commad.Parameters.Add(new SqlParameter("@idEmpresa", operacionFletera.empresa.clave));
                        commad.Parameters.Add(new SqlParameter("@idZonaOperativa", operacionFletera.zonaOperativa.idZonaOperativa));
                        commad.Parameters.Add(new SqlParameter("@idCliente", operacionFletera.cliente == null ? null : (int?)operacionFletera.cliente.clave));
                        con.Open();
                        List<Flota> listaFlota = new List<Flota>();
                        using (SqlDataReader sqlReader = commad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                Flota flota = new mapComunes().mapFlota(sqlReader);
                                listaFlota.Add(flota);
                            }
                        }
                        return new OperationResult(commad, listaFlota);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
}
