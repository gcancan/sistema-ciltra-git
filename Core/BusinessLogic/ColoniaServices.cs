﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using System.Data;
using System.Data.SqlClient;
using Core.Utils;
using Core.BusinessLogic;
using CoreFletera.BusinessLogic;

namespace CoreFletera.BusinessLogic
{
    public class ColoniaServices
    {
        public OperationResult saveColonia(ref Colonia colonia)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    con.Open();
                    using (SqlTransaction tran = con.BeginTransaction())
                    {
                        using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_saveColonia", true))
                        {
                            comad.Transaction = tran;
                            comad.Parameters.Add(new SqlParameter("@idCiudad", colonia.ciudad.idCiudad));
                            comad.Parameters.Add(new SqlParameter("@nombreColonia", colonia.nombre));
                            comad.Parameters.Add(new SqlParameter("@cp", colonia.cp));
                            comad.ExecuteNonQuery();
                            int id = 0;
                            if (CrearSqlCommand.validarCorrecto(comad, out id))
                            {
                                colonia.idColonia = id;
                                tran.Commit();
                            }
                            else
                            {
                                tran.Rollback();
                            }
                            return new OperationResult(comad, colonia);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
}