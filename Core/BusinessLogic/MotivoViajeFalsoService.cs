﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using System.Data.SqlClient;
using System.Data;
using Core.Utils;
namespace Core.BusinessLogic
{
    public class MotivoViajeFalsoService
    {
        public OperationResult getMotivosAllOrById(int idMotivo = 0)
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        #region script
                        command.CommandText = @"
BEGIN TRY 
    --DECLARE @id INT = 0;
    --DECLARE @valor INT = 0;
    --DECLARE @mensaje NVARCHAR(MAX);
    
    SELECT  idMotivo ,
            motivo ,
            imputable
    FROM    dbo.motivosViajesEnFalso
    WHERE   idMotivo LIKE CASE @id
                            WHEN 0 THEN '%'
                            ELSE CAST(@id AS NVARCHAR(20))
                          END;

    IF @@ROWCOUNT > 0
        BEGIN 
            SET @valor = 0;
            SET @mensaje = 'Se encontraron Registros';
        END;
    ELSE
        BEGIN
            SET @valor = 3;
            SET @mensaje = 'No se encontro motivos';
        END;
END TRY
BEGIN CATCH
    SET @valor = 1;
    SET @mensaje = ERROR_MESSAGE();
END CATCH;
";
                        #endregion

                        command.Parameters.Add(new SqlParameter("@id", idMotivo));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var spMensaje = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                        connection.Open();
                        SqlDataReader i = command.ExecuteReader();
                        List<MotivoViajeFalso> listMotivos = new List<MotivoViajeFalso>();
                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                MotivoViajeFalso motivo = new MotivoViajeFalso()
                                {
                                    idMotivo = (int)sqlReader["idMotivo"],
                                    motivo = (string)sqlReader["motivo"],
                                    imputable = (eImputable)sqlReader["imputable"]
                                };
                                listMotivos.Add(motivo);
                            }
                        }

                        if (idMotivo != 0)
                        {
                            return new OperationResult {valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = listMotivos[0] };
                        }
                        else
                        {
                            return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = listMotivos };
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult {valor = 1, mensaje = ex.Message, result = null };
            }
        }
    }
}
