﻿using Core.Interfaces;
using Core.Models;
using Core.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.BusinessLogic
{
    public class FraccionServices
    {
        public OperationResult getFraccionALLorById(int id)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getFraccionALLorById";
                        command.Parameters.Add(new SqlParameter("@id", id));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        connection.Open();
                        var i = command.ExecuteReader();
                        List<Fraccion> listFranccion = new List<Fraccion>();

                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                Fraccion franccion = (new Fraccion
                                {
                                    clave_empresa = (int)sqlReader["Clave_empresa"],
                                    clave = (int)sqlReader["Clave"],
                                    descripcion = (string)sqlReader["Descripcion"]
                                });

                                //OperationResult serv = mapProductos((int)sqlReader["Clave"]);
                                //if (serv.typeResult == ResultTypes.success)
                                //{
                                //    franccion.listProductos = new List<Producto>();
                                //    if (serv.result != null)
                                //    {
                                //        foreach (Producto item in (List<Producto>)serv.result)
                                //        {
                                //            franccion.listProductos.Add(item);
                                //        }
                                //    }
                                //}
                                //else
                                //{
                                //    return new OperationResult
                                //    {
                                //        valor = serv.valor,
                                //        mensaje = serv.mensaje + " error al buscar los productos de la fraccion con clave: " + ((int)sqlReader["Clave"]).ToString(),
                                //        result = null
                                //    };
                                //}

                                listFranccion.Add(franccion);
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)codigo.Value, result = listFranccion };
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }

        private OperationResult mapProductos(int idFraccion)
        {
            try
            {
                OperationResult resp = new ProductoSvc().getALLProductosByIdORidFraccion(idFraccion);
                if (resp.typeResult == ResultTypes.error)
                {
                    return new OperationResult { result = null, valor = 1, mensaje = resp.mensaje };
                }
                else if (resp.typeResult == ResultTypes.recordNotFound)
                {
                    return new OperationResult { result = null, valor = 0, mensaje = resp.mensaje };
                }
                else if (resp.typeResult == ResultTypes.success)
                {
                    List<Producto> lisTProducto = (List<Producto>)resp.result;
                    return new OperationResult { result = lisTProducto, valor = 0, mensaje = resp.mensaje };
                }
            }
            catch (Exception e)
            {
                return new OperationResult { result = null, valor = 1, mensaje = e.Message };
            }
            return new OperationResult();
        }
    }
}
