﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using System.Data;
using System.Data.SqlClient;
using Core.Utils;
using Core.BusinessLogic;

namespace CoreFletera.BusinessLogic
{
    public class SolicitudOrdenServicioServices
    {
        public OperationResult saveSolicitudOrdenServicio(ref MasterOrdServ masterOrdServ, ref List<CargaCombustibleExtra> listaCargaExtra,bool sincronizarCombustible = false)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    con.Open();
                    using (SqlTransaction tran = con.BeginTransaction())
                    {
                        using (SqlCommand commad = CrearSqlCommand.getCommand(con, "sp_saveMasterOrdenServicio", true))
                        {
                            commad.Transaction = tran;

                            #region solicitud de ordenes de servicio
                            if (masterOrdServ != null)
                            {
                                commad.Parameters.Add(new SqlParameter("@idPadreOrdSer", masterOrdServ.idPadreOrdSer));
                                commad.Parameters.Add(new SqlParameter("@idEmpresa", masterOrdServ.empresa == null ? null : (int?)masterOrdServ.empresa.clave));
                                commad.Parameters.Add(new SqlParameter("@idZonaOperativa", masterOrdServ.zonaOperativa == null ? null : (int?)masterOrdServ.zonaOperativa.idZonaOperativa));
                                commad.Parameters.Add(new SqlParameter("@idCliente", masterOrdServ.cliente == null ? null : (int?)masterOrdServ.cliente.clave));
                                commad.Parameters.Add(new SqlParameter("@idTractor", masterOrdServ.tractor.clave));
                                commad.Parameters.Add(new SqlParameter("@idTolva1", masterOrdServ.tolva1 == null ? null : masterOrdServ.tolva1.clave));
                                commad.Parameters.Add(new SqlParameter("@idDolly", masterOrdServ.dolly == null ? null : masterOrdServ.dolly.clave));
                                commad.Parameters.Add(new SqlParameter("@idTolva2", masterOrdServ.tolva2 == null ? null : masterOrdServ.tolva2.clave));
                                commad.Parameters.Add(new SqlParameter("@idChofer", masterOrdServ.chofer.clave));
                                commad.Parameters.Add(new SqlParameter("@fecha", masterOrdServ.fechaCreacion));
                                commad.Parameters.Add(new SqlParameter("@userCrea", masterOrdServ.userCrea));
                                commad.Parameters.Add(new SqlParameter("@UserModifica", string.IsNullOrEmpty(masterOrdServ.userModifica) ? null : masterOrdServ.userModifica));
                                commad.Parameters.Add(new SqlParameter("@FechaModifica", masterOrdServ.fechaModificacion));
                                commad.Parameters.Add(new SqlParameter("@UserCancela", string.IsNullOrEmpty(masterOrdServ.userCancela) ? null : masterOrdServ.userCancela));
                                commad.Parameters.Add(new SqlParameter("@FechaCancela", masterOrdServ.fechaCancela));
                                commad.Parameters.Add(new SqlParameter("@MotivoCancela", string.IsNullOrEmpty(masterOrdServ.motivoCancela) ? null : masterOrdServ.motivoCancela));
                                commad.Parameters.Add(new SqlParameter("@Estatus", masterOrdServ.estatus));
                                commad.Parameters.Add(new SqlParameter("@km", masterOrdServ.km));

                                commad.ExecuteNonQuery();
                                int id = 0;
                                if (CrearSqlCommand.validarCorrecto(commad, out id))
                                {
                                    masterOrdServ.idPadreOrdSer = id;
                                    foreach (var cabOrdenServicio in masterOrdServ.listaCabOrdenServicio)
                                    {
                                        commad.CommandText = "sp_saveCabOrdenServicio";
                                        commad.Parameters.Clear();
                                        foreach (var param in CrearSqlCommand.getPatametros(true))
                                        {
                                            commad.Parameters.Add(param);
                                        }
                                        cabOrdenServicio.idPadreOrdSer = masterOrdServ.idPadreOrdSer;
                                        commad.Parameters.Add(new SqlParameter("@idPadreOrdSer", cabOrdenServicio.idPadreOrdSer));
                                        commad.Parameters.Add(new SqlParameter("@idOrdenServ", cabOrdenServicio.idOrdenSer));
                                        commad.Parameters.Add(new SqlParameter("@IdEmpresa", cabOrdenServicio.empresa.clave));
                                        commad.Parameters.Add(new SqlParameter("@idZonaOperativa", cabOrdenServicio.zonaOperativa == null ? null : (int?)cabOrdenServicio.zonaOperativa.idZonaOperativa));
                                        commad.Parameters.Add(new SqlParameter("@idCliente", cabOrdenServicio.cliente == null ? null : (int?)cabOrdenServicio.cliente.clave));
                                        commad.Parameters.Add(new SqlParameter("@idUnidadTrans", cabOrdenServicio.unidad.clave));
                                        commad.Parameters.Add(new SqlParameter("@idTipoOrden", cabOrdenServicio.enumTipoOrden));
                                        commad.Parameters.Add(new SqlParameter("@FechaRecepcion", cabOrdenServicio.fechaRecepcion == null ? null : cabOrdenServicio.fechaRecepcion));
                                        commad.Parameters.Add(new SqlParameter("@idEmpleadoEntrega", cabOrdenServicio.idEmpleadoEntrega == null ? null : cabOrdenServicio.idEmpleadoEntrega));
                                        commad.Parameters.Add(new SqlParameter("@UsuarioRecepcion", cabOrdenServicio.usuarioRecepcion));
                                        commad.Parameters.Add(new SqlParameter("@Kilometraje", cabOrdenServicio.km));
                                        commad.Parameters.Add(new SqlParameter("@Estatus", cabOrdenServicio.estatus));
                                        commad.Parameters.Add(new SqlParameter("@FechaDiagnostico", cabOrdenServicio.fechaDiagnostico == null ? null : cabOrdenServicio.fechaDiagnostico));
                                        commad.Parameters.Add(new SqlParameter("@FechaAsignado", cabOrdenServicio.fechaAsignado == null ? null : cabOrdenServicio.fechaAsignado));
                                        commad.Parameters.Add(new SqlParameter("@FechaTerminado", cabOrdenServicio.fechaTerminado == null ? null : cabOrdenServicio.fechaTerminado));
                                        commad.Parameters.Add(new SqlParameter("@FechaEntregado", cabOrdenServicio.fechaEntregado == null ? null : cabOrdenServicio.fechaEntregado));
                                        commad.Parameters.Add(new SqlParameter("@UsuarioEntrega", cabOrdenServicio.usuarioEntrega));
                                        commad.Parameters.Add(new SqlParameter("@idEmpleadoRecibe", cabOrdenServicio.idEmpleadoRecibe == null ? null : cabOrdenServicio.idEmpleadoRecibe));
                                        commad.Parameters.Add(new SqlParameter("@NotaFinal", cabOrdenServicio.notaFinal));
                                        commad.Parameters.Add(new SqlParameter("@usuarioCan", cabOrdenServicio.usuarioCancela));
                                        commad.Parameters.Add(new SqlParameter("@FechaCancela", cabOrdenServicio.fechaCancela == null ? null : cabOrdenServicio.fechaCancela));
                                        commad.Parameters.Add(new SqlParameter("@idEmpleadoAutoriza", cabOrdenServicio.idEmpleadoAutoriza));
                                        commad.Parameters.Add(new SqlParameter("@fechaCaptura", cabOrdenServicio.fechaCaptura));
                                        commad.Parameters.Add(new SqlParameter("@idTipOrdServ", cabOrdenServicio.enumTipoOrdServ));
                                        commad.Parameters.Add(new SqlParameter("@idTipoServicio", cabOrdenServicio.enumtipoServicio));
                                        commad.Parameters.Add(new SqlParameter("@externo", cabOrdenServicio.externo));
                                        commad.Parameters.Add(new SqlParameter("@UsuarioAsigna", cabOrdenServicio.usuarioAsigna));
                                        commad.Parameters.Add(new SqlParameter("@UsuarioTermina", cabOrdenServicio.usuarioTermina));
                                        commad.Parameters.Add(new SqlParameter("@idTipoCombustibleExtra", cabOrdenServicio.cargaCombustibleExtra == null
                                            ? 0 : cabOrdenServicio.cargaCombustibleExtra.tipoOrdenCombustible.idTipoOrdenCombustible));
                                        commad.Parameters.Add(new SqlParameter("@numViaje", cabOrdenServicio.cargaCombustibleExtra == null
                                            ? null : cabOrdenServicio.cargaCombustibleExtra.numViaje));
                                        commad.Parameters.Add(new SqlParameter("@litros", cabOrdenServicio.cargaCombustibleExtra == null
                                            ? null : cabOrdenServicio.cargaCombustibleExtra.ltCombustible));
                                        var idValeExtra = commad.Parameters.Add(new SqlParameter("@idValeExtra", SqlDbType.Int, 255)
                                        {
                                            Direction = ParameterDirection.Output,
                                            Value = cabOrdenServicio.cargaCombustibleExtra == null ? 0 : cabOrdenServicio.cargaCombustibleExtra.idCargaCombustibleExtra
                                        });

                                        commad.ExecuteNonQuery();
                                        id = 0;
                                        if (CrearSqlCommand.validarCorrecto(commad, out id))
                                        {
                                            cabOrdenServicio.idOrdenSer = id;
                                            if (cabOrdenServicio.cargaCombustibleExtra != null)
                                            {
                                                cabOrdenServicio.cargaCombustibleExtra.idCargaCombustibleExtra = (int)idValeExtra.Value == 0 ? cabOrdenServicio.cargaCombustibleExtra.idCargaCombustibleExtra : (int)idValeExtra.Value;
                                            }

                                            foreach (var detOrdenServicio in cabOrdenServicio.listaDetOrdenServicio)
                                            {
                                                detOrdenServicio.idOrdenServicio = cabOrdenServicio.idOrdenSer;
                                                commad.CommandText = "sp_saveDetOrdenServicio";
                                                commad.Parameters.Clear();
                                                foreach (var param in CrearSqlCommand.getPatametros(true))
                                                {
                                                    commad.Parameters.Add(param);
                                                }
                                                commad.Parameters.Add(new SqlParameter("@idOrdenTrabajo", detOrdenServicio.idOrdenTrabajo));
                                                commad.Parameters.Add(new SqlParameter("@idOrdenServicio", detOrdenServicio.idOrdenServicio));
                                                commad.Parameters.Add(new SqlParameter("@idServicio", detOrdenServicio.servicio == null ? 0 : (int?)detOrdenServicio.servicio.idServicio));
                                                commad.Parameters.Add(new SqlParameter("@idActividad", detOrdenServicio.actividad == null ? 0 : (int?)detOrdenServicio.actividad.idActividad));
                                                commad.Parameters.Add(new SqlParameter("@Estatus", detOrdenServicio.estatus));
                                                commad.Parameters.Add(new SqlParameter("@idLlanta", detOrdenServicio.llanta == null ? "" : detOrdenServicio.llanta.clave));
                                                commad.Parameters.Add(new SqlParameter("@PosLlanta", detOrdenServicio.pocision));
                                                commad.Parameters.Add(new SqlParameter("@NotaRecepcion", detOrdenServicio.ordenTaller.descripcion));
                                                commad.Parameters.Add(new SqlParameter("@NotaRecepcionA", detOrdenServicio.ordenTaller.descripcionA));
                                                commad.Parameters.Add(new SqlParameter("@idActividadPendiente", detOrdenServicio.idActividadPendiente));

                                                commad.ExecuteNonQuery();
                                                id = 0;
                                                if (CrearSqlCommand.validarCorrecto(commad, out id))
                                                {
                                                    detOrdenServicio.idOrdenTrabajo = id;
                                                }
                                                else
                                                {
                                                    tran.Rollback();
                                                    return new OperationResult(commad);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            tran.Rollback();
                                            return new OperationResult(commad);
                                        }
                                    }
                                }
                                else
                                {
                                    tran.Rollback();
                                    return new OperationResult(commad);
                                }
                            }
                            #endregion

                            #region cargas de combustible extra.
                            if (listaCargaExtra.Count > 0)
                            {
                                commad.CommandText = "sp_saveCargaCombustibleExtra";
                                foreach (var carga in listaCargaExtra)
                                {
                                    commad.Parameters.Clear();
                                    foreach (var param in CrearSqlCommand.getPatametros(true))
                                    {
                                        commad.Parameters.Add(param);
                                    }
                                    commad.Parameters.Add(new SqlParameter("@idVCExtra", carga.idCargaCombustibleExtra));
                                    commad.Parameters.Add(new SqlParameter("@idTipoOSComb", carga.tipoOrdenCombustible.idTipoOrdenCombustible));
                                    commad.Parameters.Add(new SqlParameter("@idUnidadTrans", carga.unidadTransporte.clave));
                                    commad.Parameters.Add(new SqlParameter("@usuario", carga.userCrea));
                                    commad.Parameters.Add(new SqlParameter("@idChofer", carga.chofer.clave));
                                    commad.Parameters.Add(new SqlParameter("@numViaje", carga.numViaje));
                                    commad.Parameters.Add(new SqlParameter("@fecha", carga.fecha));
                                    commad.Parameters.Add(new SqlParameter("@fechaCombustible", carga.fechaCombustible));
                                    commad.Parameters.Add(new SqlParameter("@ltCombutible", carga.ltCombustible));
                                    commad.Parameters.Add(new SqlParameter("@precio", carga.precioCombustible));
                                    commad.Parameters.Add(new SqlParameter("@idDespachador", (carga.despachador == null) ? null : new int?(carga.despachador.clave)));
                                    commad.Parameters.Add(new SqlParameter("@externo", carga.externo));
                                    commad.Parameters.Add(new SqlParameter("@ticket", carga.ticket));
                                    commad.Parameters.Add(new SqlParameter("@folioImpreso", carga.folioImpreso));
                                    commad.Parameters.Add(new SqlParameter("@idProveedor", (carga.proveedor == null) ? null : new int?(carga.proveedor.idProveedor)));
                                    commad.ExecuteNonQuery();
                                    int id = 0;
                                    if (CrearSqlCommand.validarCorrecto(commad, out id))
                                    {
                                        carga.idCargaCombustibleExtra = id;
                                    }
                                    else
                                    {
                                        tran.Rollback();
                                        return new OperationResult(commad);
                                    }
                                }
                            }
                            #endregion

                            if (masterOrdServ.cargaCombustible != null)
                            {
                                MasterOrdServ _masterOrdServ = masterOrdServ;
                                CabOrdenServicio cabOrdenServicioCombustible = _masterOrdServ.listaCabOrdenServicio.Find
                                    (s => s.enumTipoOrdServ == enumTipoOrdServ.COMBUSTIBLE && s.unidad.clave == _masterOrdServ.cargaCombustible.unidadTransporte.clave);
                                if (cabOrdenServicioCombustible != null)
                                {
                                    commad.CommandText = "sp_saveCargaCombustibleV2";
                                    commad.Parameters.Clear();
                                    foreach (var param in CrearSqlCommand.getPatametros(true))
                                    {
                                        commad.Parameters.Add(param);
                                    }
                                    masterOrdServ.cargaCombustible.idOrdenServ = cabOrdenServicioCombustible.idOrdenSer;

                                    commad.Parameters.Add(new SqlParameter("@idOrdenServicio", masterOrdServ.cargaCombustible.idOrdenServ));
                                    commad.Parameters.Add(new SqlParameter("@fecha", masterOrdServ.cargaCombustible.fechaCombustible));
                                    commad.Parameters.Add(new SqlParameter("@kmFinal", masterOrdServ.cargaCombustible.kmFinal));
                                    commad.Parameters.Add(new SqlParameter("@kmRecorridos", masterOrdServ.cargaCombustible.kmRecorridos));
                                    commad.Parameters.Add(new SqlParameter("@kmDescpachador", masterOrdServ.cargaCombustible.kmDespachador));
                                    commad.Parameters.Add(new SqlParameter("@ltCombustible", masterOrdServ.cargaCombustible.ltCombustible));
                                    commad.Parameters.Add(new SqlParameter("@ltCombustibleUtilizado", masterOrdServ.cargaCombustible.ltCombustibleUtilizado));
                                    commad.Parameters.Add(new SqlParameter("@ltCombustiblePTO", masterOrdServ.cargaCombustible.ltCombustiblePTO));
                                    commad.Parameters.Add(new SqlParameter("@ltCombustibleEST", masterOrdServ.cargaCombustible.ltCombustibleEST));
                                    commad.Parameters.Add(new SqlParameter("@tiempoTotal", masterOrdServ.cargaCombustible.tiempoTotal));
                                    commad.Parameters.Add(new SqlParameter("@tiempoPTO", masterOrdServ.cargaCombustible.tiempoPTO));
                                    commad.Parameters.Add(new SqlParameter("@tiempoEST", masterOrdServ.cargaCombustible.tiempoEST));
                                    commad.Parameters.Add(new SqlParameter("@sello1", masterOrdServ.cargaCombustible.sello1));
                                    commad.Parameters.Add(new SqlParameter("@sello2", masterOrdServ.cargaCombustible.sello2));
                                    commad.Parameters.Add(new SqlParameter("@selloRes", masterOrdServ.cargaCombustible.selloReserva));
                                    commad.Parameters.Add(new SqlParameter("@idDespachador", masterOrdServ.cargaCombustible.despachador.clave));
                                    commad.Parameters.Add(new SqlParameter("@idTipoCombustible", null));
                                    commad.Parameters.Add(new SqlParameter("@precioCombustible", masterOrdServ.cargaCombustible.precioCombustible));
                                    commad.Parameters.Add(new SqlParameter("@importe", (masterOrdServ.cargaCombustible.ltCombustible * masterOrdServ.cargaCombustible.precioCombustible)));
                                    commad.Parameters.Add(new SqlParameter("@ticket", masterOrdServ.cargaCombustible.ticket));
                                    commad.Parameters.Add(new SqlParameter("@idProveedor", masterOrdServ.cargaCombustible.proveedor == null ? null : (int?)masterOrdServ.cargaCombustible.proveedor.idProveedor));
                                    commad.Parameters.Add(new SqlParameter("@observaciones", masterOrdServ.cargaCombustible.observacioses));
                                    commad.Parameters.Add(new SqlParameter("@folioImpreso", masterOrdServ.cargaCombustible.folioImpreso));
                                    commad.Parameters.Add(new SqlParameter("@externo", masterOrdServ.cargaCombustible.isExterno));
                                    commad.Parameters.Add(new SqlParameter("@proveedorExtra", masterOrdServ.cargaCombustible.proveedorExtra == null ? null : (int?)masterOrdServ.cargaCombustible.proveedorExtra.idProveedor));
                                    commad.Parameters.Add(new SqlParameter("@ticketExtra", masterOrdServ.cargaCombustible.ticketExtra));
                                    commad.Parameters.Add(new SqlParameter("@ltsExtra", masterOrdServ.cargaCombustible.ltsExtra));
                                    commad.Parameters.Add(new SqlParameter("@precioExtra", masterOrdServ.cargaCombustible.proveedorExtra));
                                    commad.Parameters.Add(new SqlParameter("@folioImpresoExtra", masterOrdServ.cargaCombustible.folioImpresoExtra));
                                    commad.Parameters.Add(new SqlParameter("@importeExtra", 0));
                                    commad.Parameters.Add(new SqlParameter("@ecm", masterOrdServ.cargaCombustible.ecm));
                                    commad.Parameters.Add(new SqlParameter("@isHubodometro", masterOrdServ.cargaCombustible.isHubodometo));
                                    commad.Parameters.Add(new SqlParameter("@idZonaOperativa", masterOrdServ.cargaCombustible.zonaOperativa.idZonaOperativa));

                                    commad.ExecuteNonQuery();
                                    int id = 0;
                                    if (CrearSqlCommand.validarCorrecto(commad, out id))
                                    {
                                        masterOrdServ.cargaCombustible.idCargaCombustible = id;
                                        if (masterOrdServ.cargaCombustible.isExterno == false)
                                        {
                                            KardexCombustible kardexCombustible = new KardexCombustible()
                                            {
                                                cantidad = masterOrdServ.cargaCombustible.ltCombustible,
                                                empresa = cabOrdenServicioCombustible.empresa,
                                                fecha = masterOrdServ.cargaCombustible.fechaCombustible,
                                                idOperacion = masterOrdServ.cargaCombustible.idCargaCombustible,
                                                personal = masterOrdServ.cargaCombustible.despachador,
                                                tanque = masterOrdServ.cargaCombustible.tanqueCombustible,
                                                unidadTransporte = cabOrdenServicioCombustible.unidad,
                                                tipoMovimiento = eTipoMovimiento.SALIDA,
                                                tipoOperacion = TipoOperacionDiesel.CARGA
                                            };

                                            OperationResult result2 = new KardexCombustibleServices().savekardexCombustible(ref kardexCombustible, false);
                                            if (result2.typeResult > ResultTypes.success)
                                            {
                                                tran.Rollback();
                                                return result2;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        tran.Rollback();
                                        return new OperationResult(commad);
                                    }
                                }
                            }

                            List<CabOrdenServicio> listaCab = new List<CabOrdenServicio>();
                            List<DetOrdenServicio> listaDetOrdenServicio = new List<DetOrdenServicio>();
                            if (sincronizarCombustible == true)
                            {
                                if (new CierreProcesoViajeServices().empresaSincronizaViajes(masterOrdServ.empresa.clave).typeResult == ResultTypes.success)
                                {
                                    listaCab = masterOrdServ.listaCabOrdenServicio; //.FindAll(s => s.enumTipoOrdServ == enumTipoOrdServ.TALLER || s.enumTipoOrdServ == enumTipoOrdServ.LAVADERO);
                                    if (listaCab != null)
                                    {
                                        var resp = sincronizarCitasServicio(masterOrdServ, listaCab);
                                        if (resp.typeResult != ResultTypes.success)
                                        {
                                            tran.Rollback();
                                            return resp;
                                        }
                                        if (resp.typeResult == ResultTypes.success)
                                        {
                                            List<RegistroCitaServicio> listaCitas = resp.result as List<RegistroCitaServicio>;
                                            commad.CommandText = "sp_sincronizarCabOrdenServicio";
                                            commad.Parameters.Clear();
                                            foreach (var param in CrearSqlCommand.getPatametros())
                                            {
                                                commad.Parameters.Add(param);
                                            }
                                            string xmlCitas = GenerateXML.generarListaRelOrdenServCitas(listaCitas);
                                            commad.Parameters.Add(new SqlParameter("@xml", xmlCitas));
                                            commad.ExecuteNonQuery();
                                            if (!CrearSqlCommand.validarCorrecto(commad))
                                            {
                                                tran.Rollback();
                                                return new OperationResult(commad);
                                            }
                                        }
                                    }
                                }
                            }
                          

                            tran.Commit();
                            return new OperationResult(commad, masterOrdServ);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult sincronizarCitasServicio(MasterOrdServ masterOrdServ, List<CabOrdenServicio> listaCabOrdenServicio)
        {
            try
            {
                List<RegistroCitaServicio> listaRegistrosCitas = new List<RegistroCitaServicio>();
                List<CabOrdenServicio> listaServicio = listaCabOrdenServicio.FindAll(s => s.enumTipoOrdServ == enumTipoOrdServ.TALLER || s.enumTipoOrdServ == enumTipoOrdServ.LAVADERO);
                List<CabOrdenServicio> listaCombustible = listaCabOrdenServicio.FindAll(s => s.enumTipoOrdServ == enumTipoOrdServ.COMBUSTIBLE || s.enumTipoOrdServ == enumTipoOrdServ.COMBUSTIBLE_EXTRA);
                List<CabOrdenServicio> listaLlantas = listaCabOrdenServicio.FindAll(s => s.enumTipoOrdServ == enumTipoOrdServ.LLANTAS);

                using (SqlConnection con = BoundedContextFactory.ConnectionFactoryINTELISIS())
                {
                    con.Open();
                    using (SqlTransaction tran = con.BeginTransaction())
                    {
                        using (SqlCommand comad = CrearSqlCommand.getCommand(con, ""))
                        {
                            comad.Transaction = tran;

                            if (listaServicio != null && listaServicio.Count > 0 )
                            {
                                comad.CommandText = "CILTRA_sincronizarCitasServicio";
                                string xml = GenerateXML.generarListaCitasServicio(masterOrdServ, listaServicio);
                                comad.Parameters.Add(new SqlParameter("@xml", xml));
                                
                                using (SqlDataReader reader = comad.ExecuteReader())
                                {
                                    while (reader.Read())
                                    {
                                        listaRegistrosCitas.Add(new RegistroCitaServicio
                                        {
                                            idINTELISIS = (int)reader["id"],
                                            idOrdenServicio = (string)reader["idOrdenServicio"],
                                            modulo = (string)reader["modulo"]
                                        });
                                    }
                                }
                                if (!CrearSqlCommand.validarCorrecto(comad))
                                {
                                    tran.Rollback();
                                    return new OperationResult(comad);
                                }
                            }

                            if (listaCombustible!= null && listaCombustible.Count > 0)
                            {
                                comad.CommandText = "CILTRA_sincronizarInvCargasCombustible";
                                comad.Parameters.Clear();
                                foreach (var param in CrearSqlCommand.getPatametros())
                                {
                                    comad.Parameters.Add(param);
                                }
                                string xml = GenerateXML.generarListaInvCombustible(masterOrdServ, listaCombustible);
                                comad.Parameters.Add(new SqlParameter("@xml", xml));
                                
                                using (SqlDataReader reader = comad.ExecuteReader())
                                {
                                    while (reader.Read())
                                    {
                                        listaRegistrosCitas.Add(new RegistroCitaServicio
                                        {
                                            idINTELISIS = (int)reader["id"],
                                            idOrdenServicio = (string)reader["idOrdenServicio"],
                                            modulo = (string)reader["modulo"]
                                        });
                                    }
                                }
                                if (!CrearSqlCommand.validarCorrecto(comad))
                                {
                                    tran.Rollback();
                                    return new OperationResult(comad);
                                }
                            }

                            if (listaLlantas != null && listaLlantas.Count > 0)
                            {
                                comad.CommandText = "CILTRA_sincronizarSolicitudLlantas";
                                comad.Parameters.Clear();
                                foreach (var param in CrearSqlCommand.getPatametros())
                                {
                                    comad.Parameters.Add(param);
                                }
                                string xml = GenerateXML.generarListaLlantas(masterOrdServ, listaLlantas);
                                comad.Parameters.Add(new SqlParameter("@xml", xml));

                                using (SqlDataReader reader = comad.ExecuteReader())
                                {
                                    while (reader.Read())
                                    {
                                        listaRegistrosCitas.Add(new RegistroCitaServicio
                                        {
                                            idINTELISIS = (int)reader["id"],
                                            idOrdenServicio = (string)reader["idOrdenServicio"],
                                            modulo = (string)reader["modulo"]
                                        });
                                    }
                                }
                                if (!CrearSqlCommand.validarCorrecto(comad))
                                {
                                    tran.Rollback();
                                    return new OperationResult(comad);
                                }
                            }
                            
                            tran.Commit();
                            return new OperationResult(comad, listaRegistrosCitas);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        public OperationResult getMasterOrdenServicioByFolio(int folio)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(con, "sp_getMasterOrdenServByFolio"))
                    {

                        command.Parameters.Add(new SqlParameter("@folio", folio));

                        MasterOrdServ masterOrdServ = new MasterOrdServ();
                        con.Open();
                        using (SqlDataReader sqlReader = command.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                masterOrdServ = new MasterOrdServ
                                {
                                    idPadreOrdSer = (int)sqlReader["idPadreOrdSer"],
                                    fechaCreacion = (DateTime)sqlReader["FechaCreacion"],
                                    userCrea = (string)sqlReader["UserCrea"],
                                    userModifica = (string)sqlReader["userModifica"],
                                    userCancela = (string)sqlReader["userCancela"],
                                    fechaModificacion = DBNull.Value.Equals(sqlReader["FechaModifica"]) ? null : (DateTime?)sqlReader["FechaModifica"],
                                    fechaCancela = DBNull.Value.Equals(sqlReader["FechaCancela"]) ? null : (DateTime?)sqlReader["FechaCancela"],
                                    motivoCancela = (string)sqlReader["motivoCancela"],
                                    estatus = (string)sqlReader["estatus"],
                                    listaCabOrdenServicio = new List<CabOrdenServicio>(),
                                    listTiposOrdenServicio = new List<TipoOrdenServicio>(),
                                    km = (decimal)sqlReader["km"]
                                };
                                masterOrdServ.chofer = new mapComunes().mapPersonal(sqlReader);

                                Empresa empresa = null;
                                if (!DBNull.Value.Equals(sqlReader["idEmpresa"]))
                                {
                                    var respEmp = new EmpresaServices().getEmpresasALLorById((int)sqlReader["idEmpresa"]);
                                    if (respEmp.typeResult == ResultTypes.success)
                                    {
                                        empresa = (respEmp.result as List<Empresa>)[0];
                                    }
                                }
                                masterOrdServ.empresa = empresa;

                                ZonaOperativa zonaOperativa = null;
                                if (!DBNull.Value.Equals(sqlReader["idZonaOperativa"]))
                                {
                                    zonaOperativa = new mapComunes().mapZonaOperativa(sqlReader);
                                }
                                masterOrdServ.zonaOperativa = zonaOperativa;

                                Cliente cliente = null;
                                if (!DBNull.Value.Equals(sqlReader["idCliente"]))
                                {
                                    var respClie = new ClienteServices().getClientesALLorById(0, (int)sqlReader["idCliente"]);
                                    if (respClie.typeResult == ResultTypes.success)
                                    {
                                        cliente = (respClie.result as List<Cliente>)[0];
                                    }
                                }
                                masterOrdServ.cliente = cliente;

                                if (masterOrdServ.chofer == null)
                                {
                                    return new OperationResult(ResultTypes.error, "ERROR AL LEER LOS DATOS DEL CHOFER");
                                }

                                if (!string.IsNullOrEmpty((string)sqlReader["idTractor"]))
                                {
                                    var resp = new UnidadTransporteServices().getUnidadesALLorById(0, (string)sqlReader["idTractor"]);
                                    if (resp.typeResult == ResultTypes.success)
                                    {
                                        masterOrdServ.tractor = (resp.result as List<UnidadTransporte>)[0];
                                    }
                                    else
                                    {
                                        return resp;
                                    }
                                }

                                if (!string.IsNullOrEmpty((string)sqlReader["idTolva1"]))
                                {
                                    var resp = new UnidadTransporteServices().getUnidadesALLorById(0, (string)sqlReader["idTolva1"]);
                                    if (resp.typeResult == ResultTypes.success)
                                    {
                                        masterOrdServ.tolva1 = (resp.result as List<UnidadTransporte>)[0];
                                    }
                                    else
                                    {
                                        return resp;
                                    }
                                }

                                if (!string.IsNullOrEmpty((string)sqlReader["idDolly"]))
                                {
                                    var resp = new UnidadTransporteServices().getUnidadesALLorById(0, (string)sqlReader["idDolly"]);
                                    if (resp.typeResult == ResultTypes.success)
                                    {
                                        masterOrdServ.dolly = (resp.result as List<UnidadTransporte>)[0];
                                    }
                                    else
                                    {
                                        return resp;
                                    }
                                }

                                if (!string.IsNullOrEmpty((string)sqlReader["idTolva2"]))
                                {
                                    var resp = new UnidadTransporteServices().getUnidadesALLorById(0, (string)sqlReader["idTolva2"]);
                                    if (resp.typeResult == ResultTypes.success)
                                    {
                                        masterOrdServ.tolva2 = (resp.result as List<UnidadTransporte>)[0];
                                    }
                                    else
                                    {
                                        return resp;
                                    }
                                }

                                var respCabOrdenserv = getCapOrdenServicioByFolioPadre(masterOrdServ.idPadreOrdSer);
                                if (respCabOrdenserv.typeResult == ResultTypes.success)
                                {
                                    masterOrdServ.listaCabOrdenServicio = respCabOrdenserv.result as List<CabOrdenServicio>;
                                }
                                else
                                {
                                    return respCabOrdenserv;
                                }

                                List<DetOrdenServicio> listDetOrdenServ = new List<DetOrdenServicio>();
                                var respDetOrdenServicio = getDetOrdenServicioByFolioPadre(masterOrdServ.idPadreOrdSer);
                                if (respDetOrdenServicio.typeResult == ResultTypes.success)
                                {
                                    listDetOrdenServ = respDetOrdenServicio.result as List<DetOrdenServicio>;
                                }
                                else
                                {
                                    return respDetOrdenServicio;
                                }

                                foreach (var itemCab in masterOrdServ.listaCabOrdenServicio)
                                {
                                    itemCab.listaDetOrdenServicio = listDetOrdenServ.FindAll(s => s.idOrdenServicio == itemCab.idOrdenSer).ToList();
                                }

                                foreach (var itemCab in masterOrdServ.listaCabOrdenServicio)
                                {

                                }

                            }
                        }
                        return new OperationResult(command, masterOrdServ);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getCapOrdenServicioByFolioPadre(int folio)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(con, "sp_getCapOrdenServicioByFolioPadre"))
                    {

                        command.Parameters.Add(new SqlParameter("@folio", folio));

                        List<CabOrdenServicio> listaCabOrdenServicio = new List<CabOrdenServicio>();
                        con.Open();
                        using (SqlDataReader sqlReader = command.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                CabOrdenServicio cabOrdenServicio = new mapComunes().mapCapOrdenServicio(sqlReader);
                                if (cabOrdenServicio == null)
                                {
                                    return new OperationResult(ResultTypes.error, "Error al leer información de la orden de servicio.");
                                }

                                listaCabOrdenServicio.Add(cabOrdenServicio);
                            }
                        }
                        return new OperationResult(command, listaCabOrdenServicio);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getCapOrdenServicioByFolioOrden(int folio)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(con, "sp_getCapOrdenServicioByFolioOrden"))
                    {

                        command.Parameters.Add(new SqlParameter("@folio", folio));

                        CabOrdenServicio cabOrdenServicio = new CabOrdenServicio();
                        con.Open();
                        using (SqlDataReader sqlReader = command.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                cabOrdenServicio = new mapComunes().mapCapOrdenServicio(sqlReader);
                                if (cabOrdenServicio == null)
                                {
                                    return new OperationResult(ResultTypes.error, "Error al leer información de la orden de servicio.");
                                }
                            }
                        }

                        var resp = getDetOrdenServicioByFolioOrdenServicio(cabOrdenServicio.idOrdenSer);
                        if (resp.typeResult == ResultTypes.success)
                        {
                            cabOrdenServicio.listaDetOrdenServicio = resp.result as List<DetOrdenServicio>;
                        }
                        else
                        {
                            return resp;
                        }

                        var respAct = getActividadProductoByIdOrdenSer(cabOrdenServicio.idOrdenSer);
                        if (respAct.typeResult == ResultTypes.success)
                        {
                            cabOrdenServicio.listaActividades = respAct.result as List<ActividadOrdenServicio>;
                        }
                        else if (respAct.typeResult == ResultTypes.recordNotFound)
                        {
                            cabOrdenServicio.listaActividades = new List<ActividadOrdenServicio>();
                        }
                        else
                        {
                            return respAct;
                        }

                        return new OperationResult(command, cabOrdenServicio);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getActividadProductoByIdOrdenSer(int idOrdenServicio)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getActividadProductoByIdOrdenSer"))
                    {
                        comad.Parameters.Add(new SqlParameter("@idOrdenServ", idOrdenServicio));
                        con.Open();
                        List<ActividadOrdenServicio> lista = new List<ActividadOrdenServicio>();
                        using (SqlDataReader sqlReader = comad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                ActividadOrdenServicio actividadOrdenServicio = new mapComunes().mapActividadOrdenServicio(sqlReader);
                                if (actividadOrdenServicio == null)
                                {
                                    return new OperationResult(ResultTypes.error, "ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");
                                }
                                lista.Add(actividadOrdenServicio);
                            }
                        }
                        return new OperationResult(comad, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getDetOrdenServicioByFolioPadre(int folio)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(con, "sp_getDetOrdenServicioByFolioPadre"))
                    {

                        command.Parameters.Add(new SqlParameter("@folio", folio));

                        List<DetOrdenServicio> listaDetOrdenServicio = new List<DetOrdenServicio>();
                        con.Open();
                        using (SqlDataReader sqlReader = command.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                DetOrdenServicio detOrdenServicio = new mapComunes().mapDetOrdenServicio(sqlReader);

                                var resp = new UnidadTransporteServices().getUnidadesALLorById(0, (string)sqlReader["DetOrdenServicio_idUnidadTrans"]);
                                if (resp.typeResult == ResultTypes.success)
                                {
                                    detOrdenServicio.unidad = (resp.result as List<UnidadTransporte>)[0];
                                }

                                listaDetOrdenServicio.Add(detOrdenServicio);
                            }
                        }
                        return new OperationResult(command, listaDetOrdenServicio);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getDetOrdenServicioByFolioOrdenServicio(int folio)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(con, "sp_getDetOrdenServicioByFolioOrdenServicio"))
                    {

                        command.Parameters.Add(new SqlParameter("@folio", folio));

                        List<DetOrdenServicio> listaDetOrdenServicio = new List<DetOrdenServicio>();
                        con.Open();
                        using (SqlDataReader sqlReader = command.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                DetOrdenServicio detOrdenServicio = new mapComunes().mapDetOrdenServicio(sqlReader);

                                var resp = new UnidadTransporteServices().getUnidadesALLorById(0, (string)sqlReader["DetOrdenServicio_idUnidadTrans"]);
                                if (resp.typeResult == ResultTypes.success)
                                {
                                    detOrdenServicio.unidad = (resp.result as List<UnidadTransporte>)[0];
                                }

                                listaDetOrdenServicio.Add(detOrdenServicio);
                            }
                        }
                        return new OperationResult(command, listaDetOrdenServicio);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        public OperationResult actualizarCabOrdenServicio(ref CabOrdenServicio cabOrdenServicio)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    con.Open();
                    using (SqlTransaction tran = con.BeginTransaction())
                    {
                        using (SqlCommand command = CrearSqlCommand.getCommand(con, "sp_actualizarCabOrdenServicio"))
                        {
                            CabOrdenServicio cabOrdenServicio_new = null;
                            command.Transaction = tran;
                            command.Parameters.Add(new SqlParameter("@idOrdenServicio", cabOrdenServicio.idOrdenSer));
                            command.Parameters.Add(new SqlParameter("@FechaRecepcion", !cabOrdenServicio.fechaRecepcion.HasValue ? null : cabOrdenServicio.fechaRecepcion));
                            command.Parameters.Add(new SqlParameter("@UsuarioRecepcion", string.IsNullOrEmpty(cabOrdenServicio.usuarioRecepcion) ? null : cabOrdenServicio.usuarioRecepcion));
                            command.Parameters.Add(new SqlParameter("@idEmpleadoEntrega", !cabOrdenServicio.idEmpleadoEntrega.HasValue ? null : cabOrdenServicio.idEmpleadoEntrega));
                            command.Parameters.Add(new SqlParameter("@FechaAsignado", !cabOrdenServicio.fechaAsignado.HasValue ? null : cabOrdenServicio.fechaAsignado));
                            command.Parameters.Add(new SqlParameter("@UsuarioAsigna", string.IsNullOrEmpty(cabOrdenServicio.usuarioAsigna) ? null : cabOrdenServicio.usuarioAsigna));
                            command.Parameters.Add(new SqlParameter("@FechaTerminado", !cabOrdenServicio.fechaTerminado.HasValue ? null : cabOrdenServicio.fechaTerminado));
                            command.Parameters.Add(new SqlParameter("@UsuarioTermina", string.IsNullOrEmpty(cabOrdenServicio.usuarioTermina) ? null : cabOrdenServicio.usuarioTermina));
                            command.Parameters.Add(new SqlParameter("@FechaEntregado", !cabOrdenServicio.fechaEntregado.HasValue ? null : cabOrdenServicio.fechaEntregado));
                            command.Parameters.Add(new SqlParameter("@UsuarioEntrega", string.IsNullOrEmpty(cabOrdenServicio.usuarioEntrega) ? null : cabOrdenServicio.usuarioEntrega));
                            command.Parameters.Add(new SqlParameter("@idEmpleadoRecibe", cabOrdenServicio.idEmpleadoRecibe.HasValue ? cabOrdenServicio.idEmpleadoRecibe : null));
                            command.Parameters.Add(new SqlParameter("@estatus", cabOrdenServicio.estatus));
                            command.Parameters.Add(new SqlParameter("@observaciones", cabOrdenServicio.observaciones));
                            command.Parameters.Add(new SqlParameter("@idPersonaResp", cabOrdenServicio.listaDetOrdenServicio[0].responsable == null ? null : (int?)cabOrdenServicio.listaDetOrdenServicio[0].responsable.clave));
                            command.Parameters.Add(new SqlParameter("@idAuxiliar1", cabOrdenServicio.listaDetOrdenServicio[0].auxiliar1 == null ? null : (int?)cabOrdenServicio.listaDetOrdenServicio[0].auxiliar1.clave));
                            command.Parameters.Add(new SqlParameter("@idAuxiliar2", cabOrdenServicio.listaDetOrdenServicio[0].auxiliar2 == null ? null : (int?)cabOrdenServicio.listaDetOrdenServicio[0].auxiliar2.clave));

                            command.ExecuteNonQuery();
                            if (!CrearSqlCommand.validarCorrecto(command))
                            {
                                tran.Rollback();
                                return new OperationResult(command);
                            }
                            else
                            {
                                DateTime fecha = DateTime.Now;
                                command.CommandText = "sp_saveActividadOdenServicio";
                                foreach (ActividadOrdenServicio actOrdServ in cabOrdenServicio.listaActividades)
                                {
                                    command.Parameters.Clear();
                                    foreach (var item in CrearSqlCommand.getPatametros(true))
                                    {
                                        command.Parameters.Add(item);
                                    }
                                    command.Parameters.Add(new SqlParameter("@fecha", fecha));
                                    command.Parameters.Add(new SqlParameter("@usuario", actOrdServ.usuario));
                                    command.Parameters.Add(new SqlParameter("@idActividadOrdenServicio", actOrdServ.idActividadOrdenServicio));
                                    command.Parameters.Add(new SqlParameter("@idPadreOrdenServicio", actOrdServ.idPadreOrdenServicio));
                                    command.Parameters.Add(new SqlParameter("@idOrdenServicio", actOrdServ.idOrdenServicio));
                                    command.Parameters.Add(new SqlParameter("@idTipoOrdenServicio", (int)actOrdServ.enumTipoOrdServ));
                                    command.Parameters.Add(new SqlParameter("@idActividad", actOrdServ.actividad == null ? null : (int?)actOrdServ.actividad.idActividad));
                                    command.Parameters.Add(new SqlParameter("@idServicio", actOrdServ.servicio == null ? null : (int?)actOrdServ.servicio.idServicio));
                                    command.Parameters.Add(new SqlParameter("@idResponsable", actOrdServ.idResponsable == null ? null : actOrdServ.idResponsable));
                                    command.Parameters.Add(new SqlParameter("@idAuxiliar1", actOrdServ.idAuxiliar1 == null ? null : actOrdServ.idAuxiliar1));
                                    command.Parameters.Add(new SqlParameter("@idAuxiliar2", actOrdServ.idAuxiliar2 == null ? null : actOrdServ.idAuxiliar2));
                                    command.Parameters.Add(new SqlParameter("@posicion", !actOrdServ.posicion.HasValue ? null : actOrdServ.posicion));
                                    command.Parameters.Add(new SqlParameter("@activo", actOrdServ.activo));
                                    command.Parameters.Add(new SqlParameter("@pendiente", actOrdServ.pendiente));
                                    command.Parameters.Add(new SqlParameter("@idActividadPendiente", actOrdServ.idActividadPendiente));

                                    command.ExecuteNonQuery();
                                    int id = 0;
                                    if (!CrearSqlCommand.validarCorrecto(command, out id))
                                    {
                                        tran.Rollback();
                                        return new OperationResult(command);
                                    }
                                    else
                                    {
                                        actOrdServ.idActividadOrdenServicio = id;
                                    }
                                }
                            }

                            if (cabOrdenServicio.estatus == "TER")
                            {
                                command.CommandText = "sp_saveActividadesPendientes";
                                foreach (var actPend in cabOrdenServicio.listaActividades.FindAll(s => s.enumActividadOrdenServ == enumActividadOrdenServ.PENDIENTE))
                                {
                                    command.Parameters.Clear();
                                    foreach (var param in CrearSqlCommand.getPatametros(true))
                                    {
                                        command.Parameters.Add(param);
                                    }
                                    command.Parameters.Add(new SqlParameter("@fecha", cabOrdenServicio.fechaTerminado.Value));
                                    command.Parameters.Add(new SqlParameter("@idTipoOrden", cabOrdenServicio.enumTipoOrden));
                                    command.Parameters.Add(new SqlParameter("@idtipoOrdenServ", cabOrdenServicio.enumTipoOrdServ));
                                    command.Parameters.Add(new SqlParameter("@idOrdenServ", cabOrdenServicio.idOrdenSer));
                                    command.Parameters.Add(new SqlParameter("@idTipoServicio", cabOrdenServicio.enumtipoServicio));
                                    command.Parameters.Add(new SqlParameter("@idUnidad", cabOrdenServicio.unidad.clave));
                                    command.Parameters.Add(new SqlParameter("@idActividad", actPend.actividad == null ? null : (int?)actPend.actividad.idActividad));
                                    command.Parameters.Add(new SqlParameter("@idServicio", actPend.servicio == null ? null : (int?)actPend.servicio.idServicio));
                                    command.Parameters.Add(new SqlParameter("@usuario", cabOrdenServicio.usuarioTermina));
                                    command.Parameters.Add(new SqlParameter("@motivoCancelacion", actPend.motivoCancelacion));
                                    command.Parameters.Add(new SqlParameter("@posicion", actPend.posicion));
                                    command.Parameters.Add(new SqlParameter("@idMotivoCancelacion", actPend.motivoCancelacionObj == null ? null : (int?)actPend.motivoCancelacionObj.idMotivoCancelacion));

                                    command.ExecuteNonQuery();
                                    int id = 0;
                                    if (!CrearSqlCommand.validarCorrecto(command, out id))
                                    {
                                        tran.Rollback();
                                        return new OperationResult(command);
                                    }
                                    else
                                    {
                                        int idd = id;
                                    }
                                }
                            }

                            tran.Commit();
                            return new OperationResult(command, cabOrdenServicio_new);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult saveActividadesPendientes(ref List<ActividadPendiente> listActPend)
        {
            try
            {
                DateTime fecha = DateTime.Now;
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    con.Open();
                    using (SqlTransaction tran = con.BeginTransaction())
                    {
                        using (SqlCommand command = CrearSqlCommand.getCommand(con, "sp_saveActividadesPendientes", true))
                        {
                            command.Transaction = tran;
                            foreach (ActividadPendiente actPend in listActPend)
                            {
                                command.Parameters.Clear();
                                foreach (var param in CrearSqlCommand.getPatametros(true))
                                {
                                    command.Parameters.Add(param);
                                }
                                command.Parameters.Add(new SqlParameter("@fecha", fecha));
                                command.Parameters.Add(new SqlParameter("@idTipoOrden", actPend.enumTipoOrden));
                                command.Parameters.Add(new SqlParameter("@idtipoOrdenServ", actPend.enumTipoOrdServ));
                                command.Parameters.Add(new SqlParameter("@idOrdenServ", actPend.idOrdenServicio));
                                command.Parameters.Add(new SqlParameter("@idTipoServicio", actPend.enumTipoServicio));
                                command.Parameters.Add(new SqlParameter("@idUnidad", actPend.unidad.clave));
                                command.Parameters.Add(new SqlParameter("@idActividad", actPend.actividad == null ? null : (int?)actPend.actividad.idActividad));
                                command.Parameters.Add(new SqlParameter("@idServicio", actPend.servicio == null ? null : (int?)actPend.servicio.idServicio));
                                command.Parameters.Add(new SqlParameter("@usuario", actPend.usuario));
                                command.Parameters.Add(new SqlParameter("@posicion", actPend.posicion));

                                command.ExecuteNonQuery();
                                int id = 0;
                                if (!CrearSqlCommand.validarCorrecto(command, out id))
                                {
                                    tran.Rollback();
                                    return new OperationResult(command);
                                }
                                else
                                {
                                    int idd = id;
                                }
                            }
                            tran.Commit();
                            return new OperationResult(command, listActPend);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        public OperationResult getActividadesPendientes(int idEmpresa, DateTime fechaInicial, DateTime fechaFinal, List<string> listaIds)
        {
            try
            {
                string xml = GenerateXML.generarListaCadena(listaIds);
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand commad = CrearSqlCommand.getCommand(con, "sp_getActividadesPendientes"))
                    {
                        commad.Parameters.Add(new SqlParameter("@idEmpresa", idEmpresa));
                        commad.Parameters.Add(new SqlParameter("@fechaInicial", fechaInicial));
                        commad.Parameters.Add(new SqlParameter("@fechaFinal", fechaFinal));
                        commad.Parameters.Add(new SqlParameter("@xml", xml));

                        List<ActividadPendiente> listActPendientes = new List<ActividadPendiente>();
                        con.Open();
                        using (SqlDataReader sqlReader = commad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                ActividadPendiente actividad = new mapComunes().mapActividadPendiente(sqlReader);
                                if (actividad == null)
                                {
                                    return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");
                                }
                                listActPendientes.Add(actividad);
                            }
                        }
                        return new OperationResult(commad, listActPendientes);

                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getActividadesPendientes(List<string> listaIds)
        {
            try
            {
                string xml = GenerateXML.generarListaCadena(listaIds);
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand commad = CrearSqlCommand.getCommand(con, "sp_getActividadesPendientesByUnidades"))
                    {
                        commad.Parameters.Add(new SqlParameter("@xml", xml));

                        List<ActividadPendiente> listActPendientes = new List<ActividadPendiente>();
                        con.Open();
                        using (SqlDataReader sqlReader = commad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                ActividadPendiente actividad = new mapComunes().mapActividadPendiente(sqlReader);
                                if (actividad == null)
                                {
                                    return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");
                                }
                                listActPendientes.Add(actividad);
                            }
                        }
                        return new OperationResult(commad, listActPendientes);

                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getActividadesPendientes(UnidadTransporte unidad)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand commad = CrearSqlCommand.getCommand(con, "sp_getActividadesPendientesByUnidad"))
                    {
                        commad.Parameters.Add(new SqlParameter("@idUnidad", unidad.clave));

                        List<ActividadPendiente> listActPendientes = new List<ActividadPendiente>();
                        con.Open();
                        using (SqlDataReader sqlReader = commad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                ActividadPendiente actividad = new mapComunes().mapActividadPendiente(sqlReader);
                                if (actividad == null)
                                {
                                    return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");
                                }
                                listActPendientes.Add(actividad);
                            }
                        }
                        return new OperationResult(commad, listActPendientes);

                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult cancelarActividadOrdenServicio(ActividadOrdenServicio actividadOrdenServicio)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_cancelarActividadOrdenServicio"))
                    {
                        comad.Parameters.Add(new SqlParameter("@idActividadOS", actividadOrdenServicio.idActividadOrdenServicio));
                        comad.Parameters.Add(new SqlParameter("@motivoCandelacion", actividadOrdenServicio.motivoCancelacion));
                        comad.Parameters.Add(new SqlParameter("@usuario", actividadOrdenServicio.usuarioCancela));
                        comad.Parameters.Add(new SqlParameter("@idMotivoCancelacion", actividadOrdenServicio.motivoCancelacionObj == null ?
                            null : (int?)actividadOrdenServicio.motivoCancelacionObj.idMotivoCancelacion));
                        comad.Parameters.Add(new SqlParameter("@idActividadPendiente", actividadOrdenServicio.idActividadPendiente));
                        comad.Parameters.Add(new SqlParameter("@idOrdenTrabajo", actividadOrdenServicio.idOrdenTrabajo));
                        con.Open();
                        comad.ExecuteNonQuery();
                        return new OperationResult(comad);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult bajaActividadesPendientes(List<string> listId, string usuario, string motivoCancelacion, int idMotivo)
        {
            try
            {
                string xml = GenerateXML.generarListaCadena(listId);
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    con.Open();
                    using (SqlTransaction tran = con.BeginTransaction())
                    {
                        using (SqlCommand commad = CrearSqlCommand.getCommand(con, "sp_bajaActividadesPendientes"))
                        {
                            commad.Transaction = tran;
                            commad.Parameters.Add(new SqlParameter("@xml", xml));
                            commad.Parameters.Add(new SqlParameter("usuario", usuario));
                            commad.Parameters.Add(new SqlParameter("motivo", motivoCancelacion));
                            commad.Parameters.Add(new SqlParameter("idMotivo", idMotivo));

                            commad.ExecuteNonQuery();
                            if (!CrearSqlCommand.validarCorrecto(commad))
                            {
                                tran.Rollback();
                                return new OperationResult(commad);
                            }
                            tran.Commit();
                            return new OperationResult(commad);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getReporteOrdenesServicio(int idEmpresa, DateTime fechaInicio, DateTime fechaFinal,
            List<string> listaClientes, List<string> listaUnidades, List<string> listaTipoOrden, List<string> listaTipoOrdenServ, List<string> listaEstatus)
        {
            int orden = 0;
            try
            {
                if (listaClientes.Count == 0)
                {
                    listaClientes.Add("0");
                }
                string xmlClientes = GenerateXML.generarListaCadena(listaClientes);
                string xmlUnidades = GenerateXML.generarListaCadena(listaUnidades);
                string xmlTipoOrden = GenerateXML.generarListaCadena(listaTipoOrden);
                string xmlTipoOrdenServ = GenerateXML.generarListaCadena(listaTipoOrdenServ);
                string xmlEstatus = GenerateXML.generarListaCadena(listaEstatus);

                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getReporteOrdenesServicio"))
                    {
                        comad.Parameters.Add(new SqlParameter("@idEmpresa", idEmpresa));
                        comad.Parameters.Add(new SqlParameter("@fechaInicio", fechaInicio));
                        comad.Parameters.Add(new SqlParameter("@fechaFinal", fechaFinal));
                        comad.Parameters.Add(new SqlParameter("@xmlClientes", xmlClientes));
                        comad.Parameters.Add(new SqlParameter("@xmlUnidades", xmlUnidades));
                        comad.Parameters.Add(new SqlParameter("@xmlTipoOrden", xmlTipoOrden));
                        comad.Parameters.Add(new SqlParameter("@xmlTipoOrdenServicio", xmlTipoOrdenServ));
                        comad.Parameters.Add(new SqlParameter("@xmlEstatus", xmlEstatus));
                        con.Open();

                        List<ReporteOrdenesServicio> lista = new List<ReporteOrdenesServicio>();
                        using (SqlDataReader sqlReader = comad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                orden = (int)sqlReader["ordenServ"];
                                if (orden == 63457)
                                {

                                }
                                lista.Add(new ReporteOrdenesServicio
                                {
                                    clave = (string)sqlReader["clave"],
                                    folioPadre = (int)sqlReader["folioPadre"],
                                    ordenServ = DBNull.Value.Equals(sqlReader["ordenServ"]) ? 0 : (int)sqlReader["ordenServ"],
                                    tipoOrden = (string)sqlReader["tipoServicio"],
                                    tipoOrdenServicio = (string)sqlReader["tipoOrdenServicio"],
                                    fechaCaptura = (DateTime)sqlReader["fechaCaptura"],
                                    usuarioCaptura = (string)sqlReader["usuraioCaptura"],
                                    fechaRecepcion = DBNull.Value.Equals(sqlReader["FechaRecepcion"]) ? null : (DateTime?)sqlReader["FechaRecepcion"],
                                    usuarioRecepcion = DBNull.Value.Equals(sqlReader["usuarioRecepcion"]) ? string.Empty : (string)sqlReader["usuarioRecepcion"],
                                    fechaTerminacion = DBNull.Value.Equals(sqlReader["FechaTerminado"]) ? null : (DateTime?)sqlReader["FechaTerminado"],
                                    usuarioTermina = (string)sqlReader["usuarioTermina"]
                                });
                            }
                        }

                        return new OperationResult(comad, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
    public class RegistroCitaServicio
    {
        public string idOrdenServicio { get; set; }
        public int idINTELISIS { get; set; }
        public string modulo { get; set; }
    }
}
