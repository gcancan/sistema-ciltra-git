﻿namespace Core.BusinessLogic
{
    using Core.Models;
    using Core.Utils;
    using CoreFletera.BusinessLogic;
    using CoreFletera.Models;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class OperacionServices
    {
        public OperationResult agregarInsumosExtra(List<ProductosActividad> listProducto, int ordenServ)
        {
            OperationResult result;
            try
            {
                string str = GenerateXML.generarListaInsumos(listProducto, ordenServ);
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_agregarInsumosExtra";
                        command.Parameters.Add(new SqlParameter("@xml", str));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        command.ExecuteNonQuery();
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            return result;
        }

        public OperationResult cargaCombustibleByIdVale(int idVale)
        {
            DateTime? nullable = null;
            int? nullable2 = null;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_cargaCombustibleByIdVale"))
                    {
                        command.Parameters.Add(new SqlParameter("@idVale", idVale));
                        connection.Open();
                        CargaCombustible combustible = new CargaCombustible();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                combustible = new CargaCombustible
                                {
                                    idCargaCombustible = (int)reader["idCargaCombustible"],
                                    idOrdenServ = (int)reader["idOrdenServ"],
                                    fechaCombustible = DBNull.Value.Equals(reader["fechaCombustible"]) ? nullable : new DateTime?((DateTime)reader["fechaCombustible"]),
                                    kmFinal = (decimal)reader["KMFinal"],
                                    kmRecorridos = (decimal)reader["kmRecorridos"],
                                    ltCombustible = (decimal)reader["ltCombustible"],
                                    ltCombustibleUtilizado = (decimal)reader["ltCombustibleUtilizado"],
                                    ltCombustiblePTO = (decimal)reader["ltCombustiblePTO"],
                                    ltCombustibleEST = (decimal)reader["ltCombustibleEST"],
                                    tiempoTotal = DBNull.Value.Equals(reader["tiempoTotal"]) ? "" : ((string)reader["tiempoTotal"]),
                                    tiempoPTO = DBNull.Value.Equals(reader["tiempoPTO"]) ? "" : ((string)reader["tiempoPTO"]),
                                    tiempoEST = DBNull.Value.Equals(reader["tiempoEST"]) ? "" : ((string)reader["tiempoEST"]),
                                    sello1 = DBNull.Value.Equals(reader["sello1"]) ? nullable2 : new int?((int)reader["sello1"]),
                                    sello2 = DBNull.Value.Equals(reader["sello2"]) ? nullable2 : new int?((int)reader["sello2"]),
                                    selloReserva = DBNull.Value.Equals(reader["selloRes"]) ? nullable2 : new int?((int)reader["selloRes"]),
                                    kmDespachador = (decimal)reader["kmDespachador"],
                                    fechaEntrada = DBNull.Value.Equals(reader["fechaEntrada"]) ? null : (DateTime?)reader["fechaEntrada"],
                                    ticket = (string)reader["ticket"],
                                    folioImpreso = (string)reader["folioImpreso"],
                                    isExterno = (bool)reader["externo"],
                                    observacioses = (string)reader["observaciones"],
                                    precioCombustible = (decimal)reader["precioCombustible"],
                                    zonaOperativa = null,
                                    facturaPemex = (string)reader["factura"],
                                    facturaProveedor = (string)reader["facturaProveedor"],         
                                    unidadTransporte = new UnidadTransporte { clave = (string)reader["idUnidad"] }
                                };

                                var respUnid = new UnidadTransporteServices().getUnidadesALLorById(0, combustible.unidadTransporte.clave);
                                if (respUnid.typeResult == ResultTypes.success)
                                {
                                    combustible.unidadTransporte = (respUnid.result as List<UnidadTransporte>)[0];
                                }
                                else
                                {
                                    combustible.unidadTransporte = null;
                                }

                                if (!DBNull.Value.Equals(reader["idProveedor"]))
                                {
                                    if ((int)reader["idProveedor"] != 0)
                                    {
                                        OperationResult result2 = new CargaCombuctibleServices().getProveedor((int)reader["idProveedor"]);
                                        if (result2.typeResult == ResultTypes.success)
                                        {
                                            combustible.proveedor = (ProveedorCombustible)result2.result;
                                        }
                                    }
                                }
                                if (!DBNull.Value.Equals(reader["persona_idPersonal"]))
                                {
                                    Personal personal = new mapComunes().mapPersonal(reader, "");
                                    if (personal == null)
                                    {
                                        return new OperationResult
                                        {
                                            valor = 1,
                                            mensaje = "Ocurrio un error al buscar al Despachador"
                                        };
                                    }
                                    combustible.despachador = personal;
                                }
                                else
                                {
                                    combustible.despachador = null;
                                }
                                OperationResult result = new MasterOrdServService().getMasterOrdServ((int)reader["idPadreOrdSer"]);
                                if (result.typeResult == ResultTypes.success)
                                {
                                    combustible.masterOrdServ = (MasterOrdServ)result.result;
                                }
                                else
                                {
                                    return result;
                                }
                                if (combustible.masterOrdServ.tractor.empresa.clave == 1)
                                {
                                    result = new CartaPorteServices().getDetallesCartaPorteByIdCargaGas((int)reader["idCargaCombustible"]);
                                }
                                else
                                {
                                    result = new CartaPorteServices().getDetallesCartaPorteByIdCargaGasAtlante((int)reader["idCargaCombustible"]);
                                }
                                combustible.listCP = new List<CartaPorte>();
                                if (result.typeResult == ResultTypes.success)
                                {
                                    combustible.listCP = (List<CartaPorte>)result.result;
                                }
                                if (!DBNull.Value.Equals(reader["proveedorExtra"]))
                                {
                                    if ((int)reader["proveedorExtra"] != 0)
                                    {
                                        OperationResult result4 = new CargaCombuctibleServices().getProveedor((int)reader["proveedorExtra"]);
                                        if (result4.typeResult == ResultTypes.success)
                                        {
                                            combustible.proveedorExtra = (ProveedorCombustible)result4.result;
                                            combustible.proveedorExtra.precio = (decimal)reader["precioExtra"];
                                        }
                                    }
                                }
                                else
                                {
                                    combustible.proveedorExtra = null;
                                }
                                combustible.ticketExtra = (string)reader["ticketExtra"];
                                combustible.ltsExtra = (decimal)reader["ltsExtra"];
                                combustible.folioImpresoExtra = (string)reader["folioImpresoExtra"];

                                var respEmp = new EmpresaServices().getEmpresasALLorById((int)reader["carga_IdEmpresa"]);
                                if (respEmp.typeResult == ResultTypes.success)
                                {
                                    combustible.empresa = (respEmp.result as List<Empresa>)[0];
                                }
                                else
                                {
                                    return respEmp;
                                }

                                var respZona = new ZonaOpetaivaServices().getZonaOperativaById((int)reader["carga_zonaOperativa"]);
                                if (respZona.typeResult == ResultTypes.success)
                                {
                                    combustible.zonaOperativa = respZona.result as ZonaOperativa;
                                }
                                else if (respZona.typeResult == ResultTypes.recordNotFound)
                                {
                                    combustible.zonaOperativa = null;
                                }
                                else
                                {
                                    return respZona;
                                }
                                var resp = new CargaTicketDieselServices().getCargaTicketsDieselByVale(combustible.idCargaCombustible);
                                if (resp.typeResult == ResultTypes.success)
                                {
                                    combustible.listaCargaTickets = resp.result as List<CargaTicketDiesel>;
                                }
                                else
                                {
                                    combustible.listaCargaTickets = new List<CargaTicketDiesel>();
                                }
                            }
                        }

                        return new OperationResult(command, combustible);
                    }
                }
            }
            catch (Exception exception)
            {
                return new OperationResult(exception);
            }

        }

        public OperationResult entregarOrdenServicio(int idOrdenServ, Personal personal, string usuario)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_entregarOrdenServicio";
                        command.Parameters.Add(new SqlParameter("@idOrdenServ", idOrdenServ));
                        command.Parameters.Add(new SqlParameter("@usuario", usuario));
                        command.Parameters.Add(new SqlParameter("@idPersonal", personal.clave));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        command.ExecuteNonQuery();
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            return result;
        }

        public OperationResult getInsumosByNombre(string nombre = "", bool taller = true, bool lavadero = true, bool llantera = true)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getInsumosByNombre";
                        command.Parameters.Add(new SqlParameter("@nombre", nombre));
                        command.Parameters.Add(new SqlParameter("@taller", taller));
                        command.Parameters.Add(new SqlParameter("@lavadero", lavadero));
                        command.Parameters.Add(new SqlParameter("@llantera", llantera));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        List<ProductoInsumo> list = new List<ProductoInsumo>();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                ProductoInsumo item = new ProductoInsumo
                                {
                                    idInsumo = (int)reader["id"],
                                    clave = (string)reader["clave"],
                                    nombre = (string)reader["nombre"],
                                    tipoProducto = (int)reader["tipoProducto"],
                                    precio = (decimal)reader["precio"]
                                };
                                list.Add(item);
                            }
                        }
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = list
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            return result;
        }

        public OperationResult getSalidaEntradaByCamion(string idCamion)
        {
            OperationResult result2;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getSalidaEntradaByCamion";
                        command.Parameters.Add(new SqlParameter("@idTractor", idCamion));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        SalidaEntrada entrada = new SalidaEntrada();
                        using (SqlDataReader reader2 = reader)
                        {
                            while (reader2.Read())
                            {
                                entrada = new SalidaEntrada
                                {
                                    idOperacion = (int)reader2["idSalidaEntrada"],
                                    fecha = new DateTime?((DateTime)reader2["fecha"]),
                                    km = (decimal)reader2["KM"],
                                    tipoMoviemiento = (eTipoMovimiento)((int)reader2["tipoOperacion"])
                                };
                                OperationResult result = new MasterOrdServService().getMasterOrdServ((int)reader2[""]);
                            }
                        }
                        result2 = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = entrada
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result2 = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            return result2;
        }

        public OperationResult getSalidaEntradaByidORden(int idOrden)
        {
            OperationResult result2;
            DateTime? nullable = null;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getSalidasEntradasByIdOrden";
                        command.Parameters.Add(new SqlParameter("@folio", idOrden));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        List<SalidaEntrada> list = new List<SalidaEntrada>();
                        using (SqlDataReader reader2 = reader)
                        {
                            while (reader2.Read())
                            {
                                SalidaEntrada item = new SalidaEntrada
                                {
                                    idOperacion = (int)reader2["idSalidaEntrada"],
                                    fecha = DBNull.Value.Equals(reader2["fecha"]) ? nullable : new DateTime?((DateTime)reader2["fecha"]),
                                    fechaFin = DBNull.Value.Equals(reader2["fechaFin"]) ? nullable : new DateTime?((DateTime)reader2["fechaFin"]),
                                    tipoMoviemiento = (eTipoMovimiento)((int)reader2["tipoOperacion"]),
                                    km = (decimal)reader2["KM"]
                                };
                                if (!DBNull.Value.Equals(reader2["persona_idPersonal"]))
                                {
                                    Personal personal = new mapComunes().mapPersonal(reader2, "");
                                    if (personal == null)
                                    {
                                        return new OperationResult
                                        {
                                            valor = 1,
                                            mensaje = "Ocurrio un error al buscar al Despachador"
                                        };
                                    }
                                    item.chofer = personal;
                                }
                                else
                                {
                                    item.chofer = null;
                                }
                                OperationResult result = new MasterOrdServService().getMasterOrdServ((int)reader2["idPadreOrdSer"]);
                                if (result.typeResult == ResultTypes.success)
                                {
                                    item.masterOrdServ = (MasterOrdServ)result.result;
                                }
                                else
                                {
                                    return result;
                                }
                                result = new UnidadTransporteServices().getUnidadesALLorById(0, (string)reader2["idUnidadTrans"]);
                                if (result.typeResult == ResultTypes.success)
                                {
                                    item.unidadTransporte = ((List<UnidadTransporte>)result.result)[0];
                                }
                                result = new UsuarioServices().getAllUserOrById((int)reader2["idUsuario"], false);
                                if (result.typeResult == ResultTypes.success)
                                {
                                    item.usuario = ((List<Usuario>)result.result)[0];
                                }
                                list.Add(item);
                            }
                        }
                        result2 = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = list
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result2 = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            return result2;
        }

        public OperationResult getSalidasEntradasByIdOrdenServ(int idOrden)
        {
            OperationResult result3;
            DateTime? nullable = null;
            int? nullable2 = null;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getSalidasEntradasByIdOrdenServ";
                        command.Parameters.Add(new SqlParameter("@idOrden", idOrden));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        CargaCombustible combustible = new CargaCombustible();
                        using (SqlDataReader reader2 = reader)
                        {
                            while (reader2.Read())
                            {
                                combustible = new CargaCombustible
                                {
                                    idCargaCombustible = (int)reader2["idCargaCombustible"],
                                    idOrdenServ = (int)reader2["idOrdenServ"],
                                    fechaCombustible = DBNull.Value.Equals(reader2["fechaCombustible"]) ? nullable : new DateTime?((DateTime)reader2["fechaCombustible"]),
                                    kmFinal = (decimal)reader2["KMFinal"],
                                    kmRecorridos = (decimal)reader2["kmRecorridos"],
                                    ltCombustible = (decimal)reader2["ltCombustible"],
                                    ltCombustibleUtilizado = (decimal)reader2["ltCombustibleUtilizado"],
                                    ltCombustiblePTO = (decimal)reader2["ltCombustiblePTO"],
                                    ltCombustibleEST = (decimal)reader2["ltCombustibleEST"],
                                    tiempoTotal = DBNull.Value.Equals(reader2["tiempoTotal"]) ? "" : ((string)reader2["tiempoTotal"]),
                                    tiempoPTO = DBNull.Value.Equals(reader2["tiempoPTO"]) ? "" : ((string)reader2["tiempoPTO"]),
                                    tiempoEST = DBNull.Value.Equals(reader2["tiempoEST"]) ? "" : ((string)reader2["tiempoEST"]),
                                    sello1 = DBNull.Value.Equals(reader2["sello1"]) ? nullable2 : new int?((int)reader2["sello1"]),
                                    sello2 = DBNull.Value.Equals(reader2["sello2"]) ? nullable2 : new int?((int)reader2["sello2"]),
                                    selloReserva = DBNull.Value.Equals(reader2["selloRes"]) ? 0 : new int?((int)reader2["selloRes"]),
                                    kmDespachador = (decimal)reader2["kmDespachador"],
                                    fechaEntrada = DBNull.Value.Equals(reader2["fechaEntrada"]) ? null : (DateTime?)reader2["fechaEntrada"],
                                    ticket = (string)reader2["ticket"],
                                    folioImpreso = (string)reader2["folioImpreso"],
                                    isExterno = (bool)reader2["externo"],
                                    observacioses = (string)reader2["observaciones"],
                                    precioCombustible = (decimal)reader2["precioCombustible"],
                                    sumaLitrosExtra = (decimal)reader2["sumaLtsExtra"],
                                    ecm = (bool)reader2["ecm"],
                                    facturaPemex = (string)reader["factura"],
                                    facturaProveedor = (string)reader["facturaProveedor"]
                                };
                                if (!DBNull.Value.Equals(reader2["idProveedor"]))
                                {
                                    if (((int)reader2["idProveedor"]) == 0)
                                    {
                                        combustible.proveedor = null;
                                    }
                                    else
                                    {
                                        OperationResult result2 = new CargaCombuctibleServices().getProveedor((int)reader2["idProveedor"]);
                                        if (result2.typeResult == ResultTypes.success)
                                        {
                                            combustible.proveedor = (ProveedorCombustible)result2.result;
                                        }
                                    }
                                }
                                if (!DBNull.Value.Equals(reader2["persona_idPersonal"]))
                                {
                                    Personal personal = new mapComunes().mapPersonal(reader2, "");
                                    if (personal == null)
                                    {
                                        return new OperationResult
                                        {
                                            valor = 1,
                                            mensaje = "Ocurrio un error al buscar al Despachador"
                                        };
                                    }
                                    combustible.despachador = personal;
                                }
                                else
                                {
                                    combustible.despachador = null;
                                }
                                OperationResult result = new MasterOrdServService().getMasterOrdServ((int)reader2["idPadreOrdSer"]);
                                if (result.typeResult == ResultTypes.success)
                                {
                                    combustible.masterOrdServ = (MasterOrdServ)result.result;
                                }
                                else
                                {
                                    return result;
                                }
                                if (combustible.masterOrdServ.tractor.empresa.clave == 1)
                                {
                                    result = new CartaPorteServices().getDetallesCartaPorteByIdCargaGas((int)reader2["idCargaCombustible"]);
                                }
                                else
                                {
                                    result = new CartaPorteServices().getDetallesCartaPorteByIdCargaGasAtlante((int)reader2["idCargaCombustible"]);
                                }
                                combustible.listCP = new List<CartaPorte>();
                                if (result.typeResult == ResultTypes.success)
                                {
                                    combustible.listCP = (List<CartaPorte>)result.result;
                                }
                                if (!DBNull.Value.Equals(reader2["proveedorExtra"]))
                                {
                                    OperationResult result4 = new CargaCombuctibleServices().getProveedor((int)reader2["proveedorExtra"]);
                                    if (result4.typeResult == ResultTypes.success)
                                    {
                                        combustible.proveedorExtra = (ProveedorCombustible)result4.result;
                                        combustible.proveedorExtra.precio = (decimal)reader2["precioExtra"];
                                    }
                                }
                                else
                                {
                                    combustible.proveedorExtra = null;
                                }
                                combustible.ticketExtra = (string)reader2["ticketExtra"];
                                combustible.ltsExtra = (decimal)reader2["ltsExtra"];
                                combustible.folioImpresoExtra = (string)reader2["folioImpresoExtra"];

                                var respEmp = new EmpresaServices().getEmpresasALLorById((int)reader["carga_IdEmpresa"]);
                                if (respEmp.typeResult == ResultTypes.success)
                                {
                                    combustible.empresa = (respEmp.result as List<Empresa>)[0];
                                }
                                else
                                {
                                    return respEmp;
                                }

                                var respZona = new ZonaOpetaivaServices().getZonaOperativaById((int)reader["carga_zonaOperativa"]);
                                if (respZona.typeResult == ResultTypes.success)
                                {
                                    combustible.zonaOperativa = respZona.result as ZonaOperativa;
                                }
                                else if (respZona.typeResult == ResultTypes.recordNotFound)
                                {
                                    combustible.zonaOperativa = null;
                                }
                                else
                                {
                                    return respZona;
                                }
                            }
                        }
                        result3 = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = combustible
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result3 = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            return result3;
        }

        public OperationResult getTiposOrdenServicio(int idTipoOrdenServicio = 0)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getTiposOrdenServicio";
                        command.Parameters.Add(new SqlParameter("@idTipoOrden", idTipoOrdenServicio));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        List<TiposDeOrdenServicio> list = new List<TiposDeOrdenServicio>();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                TiposDeOrdenServicio item = new TiposDeOrdenServicio
                                {
                                    idTipOrdServ = (int)reader["idTipOrdServ"],
                                    NomTipOrdServ = (string)reader["NomTipOrdServ"]
                                };
                                list.Add(item);
                            }
                        }
                        if (idTipoOrdenServicio == 0)
                        {
                            return new OperationResult
                            {
                                valor = new int?((int)parameter.Value),
                                mensaje = (string)parameter2.Value,
                                result = list
                            };
                        }
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = list[0]
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            return result;
        }

        internal OperationResult saveCargaGasolina(ref CargaCombustible cargaGas, int idEmpresa, KardexCombustible kardexCombustible = null)
        {
            OperationResult result3;
            try
            {
                string str = "";
                if (cargaGas.listCP.Count == 0)
                {
                    str = null;
                }
                else
                {
                    str = GenerateXML.generateListConsecutivos(cargaGas.listCP);
                }
                string str2 = "";
                if ((cargaGas.listCombustibleExtra == null) || (cargaGas.listCombustibleExtra.Count == 0))
                {
                    str2 = null;
                }
                else
                {
                    str2 = GenerateXML.generateListCargasExtea(cargaGas.listCombustibleExtra);
                }
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    connection.Open();
                    using (SqlTransaction transaction = connection.BeginTransaction())
                    {
                        using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_saveCargaGasolina", true))
                        {
                            command.Transaction = transaction;
                            command.Parameters.Add(new SqlParameter("@idOrdenServ", cargaGas.idOrdenServ));
                            command.Parameters.Add(new SqlParameter("@idCargaCombustible", cargaGas.idCargaCombustible));
                            command.Parameters.Add(new SqlParameter("@kmDespachador", cargaGas.kmDespachador));
                            command.Parameters.Add(new SqlParameter("@kmRecorridos", cargaGas.kmRecorridos));
                            command.Parameters.Add(new SqlParameter("@ltCombustible", cargaGas.ltCombustible));
                            command.Parameters.Add(new SqlParameter("@ltCombustibleUtilizado", cargaGas.ltCombustibleUtilizado));
                            command.Parameters.Add(new SqlParameter("@ltCombustiblePTO", cargaGas.ltCombustiblePTO));
                            command.Parameters.Add(new SqlParameter("@ltCombustibleEST", cargaGas.ltCombustibleEST));
                            command.Parameters.Add(new SqlParameter("@tiempoTotal", cargaGas.tiempoTotal));
                            command.Parameters.Add(new SqlParameter("@tiempoPTO", cargaGas.tiempoPTO));
                            command.Parameters.Add(new SqlParameter("@tiempoEST", cargaGas.tiempoEST));
                            int? nullable = null;
                            command.Parameters.Add(new SqlParameter("@idDespachador", (cargaGas.despachador == null) ? nullable : new int?(cargaGas.despachador.clave)));
                            command.Parameters.Add(new SqlParameter("@sello1", !cargaGas.sello1.HasValue ? nullable : cargaGas.sello1));
                            command.Parameters.Add(new SqlParameter("@sello2", !cargaGas.sello2.HasValue ? nullable : cargaGas.sello2));
                            int? selloReserva = cargaGas.selloReserva;
                            command.Parameters.Add(new SqlParameter("@selloRes", !selloReserva.HasValue ? nullable : cargaGas.selloReserva));
                            command.Parameters.Add(new SqlParameter("@xml", str));
                            command.Parameters.Add(new SqlParameter("@xmlExtra", str2));
                            command.Parameters.Add(new SqlParameter("@usuarioCrea", cargaGas.usuarioCrea));
                            command.Parameters.Add(new SqlParameter("@proveedorExtra", (cargaGas.proveedorExtra == null) ? null : new int?(cargaGas.proveedorExtra.idProveedor)));
                            command.Parameters.Add(new SqlParameter("@ticketExtra", cargaGas.ticketExtra));
                            command.Parameters.Add(new SqlParameter("@ltsExtra", cargaGas.ltsExtra));
                            command.Parameters.Add(new SqlParameter("@precioExtra", (cargaGas.proveedorExtra == null) ? decimal.Zero : cargaGas.proveedorExtra.precio));
                            command.Parameters.Add(new SqlParameter("@folioImpresoExtra", cargaGas.folioImpresoExtra));
                            command.Parameters.Add(new SqlParameter("@importeExtra", cargaGas.importeExtra));
                            command.Parameters.Add(new SqlParameter("@isExterno", cargaGas.isExterno));
                            command.Parameters.Add(new SqlParameter("@ticket", cargaGas.ticket));
                            command.Parameters.Add(new SqlParameter("@idProveedor", (cargaGas.proveedor == null) ? 0 : cargaGas.proveedor.idProveedor));
                            command.Parameters.Add(new SqlParameter("@precio", cargaGas.precioCombustible));
                            command.Parameters.Add(new SqlParameter("@observaciones", cargaGas.observacioses));
                            command.Parameters.Add(new SqlParameter("@folioImpreso", cargaGas.folioImpreso));
                            command.Parameters.Add(new SqlParameter("@fechaCombustible", cargaGas.fechaCombustible));
                            command.Parameters.Add(new SqlParameter("@ecm", cargaGas.ecm));
                            command.Parameters.Add(new SqlParameter("@idZonaOperativa", cargaGas.zonaOperativa == null ? 1 : cargaGas.zonaOperativa.idZonaOperativa));
                            command.Parameters.Add(new SqlParameter("@idChofer", cargaGas.idChofer));
                            command.Parameters.Add(new SqlParameter("@isCR", cargaGas.isCR));
                            command.Parameters.Add(new SqlParameter("@isHubodometro", cargaGas.isHubodometo));
                            SqlParameter parameter1 = new SqlParameter("@inicial", SqlDbType.Decimal)
                            {
                                Direction = ParameterDirection.Output
                            };
                            SqlParameter parameter = command.Parameters.Add(parameter1);
                            SqlParameter parameter3 = new SqlParameter("@final", SqlDbType.Decimal)
                            {
                                Direction = ParameterDirection.Output
                            };
                            SqlParameter parameter2 = command.Parameters.Add(parameter3);
                            int num = command.ExecuteNonQuery();
                            int id = 0;
                            if (CrearSqlCommand.validarCorrecto(command, out id))
                            {
                                cargaGas.idCargaCombustible = id;
                                cargaGas.kmInicial = (decimal)parameter.Value;
                                cargaGas.kmFinal = (decimal)parameter2.Value;
                                OperationResult result = new OperationResult();
                                if (idEmpresa == 1)
                                {
                                    result = new CartaPorteServices().getDetallesCartaPorteByIdCargaGas(cargaGas.idCargaCombustible);
                                }
                                else
                                {
                                    result = new CartaPorteServices().getDetallesCartaPorteByIdCargaGasAtlante(cargaGas.idCargaCombustible);
                                }
                                cargaGas.listCP = new List<CartaPorte>();
                                if (result.typeResult == ResultTypes.success)
                                {
                                    cargaGas.listCP = (List<CartaPorte>)result.result;
                                }
                                if (!cargaGas.isExterno && (kardexCombustible != null))
                                {
                                    kardexCombustible.idOperacion = new int?(cargaGas.idCargaCombustible);
                                    kardexCombustible.fecha = cargaGas.fechaCombustible;
                                    OperationResult result2 = new KardexCombustibleServices().savekardexCombustible(ref kardexCombustible, false);
                                    if (result2.typeResult > ResultTypes.success)
                                    {
                                        transaction.Rollback();
                                        return result2;
                                    }
                                }
                            }
                            else
                            {
                                transaction.Rollback();
                                return new OperationResult(command, null);
                            }
                            transaction.Commit();
                            result3 = new OperationResult(command, cargaGas);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                result3 = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            return result3;
        }

        internal OperationResult saveCargaGasolinaExterno(ref CargaCombustible cargaGas, int idEmpresa, Personal operador, UnidadTransporte tractor,
            UnidadTransporte remolque1, UnidadTransporte dolly, UnidadTransporte remolque2, ZonaOperativa zonaOperativa = null, string unidadCarga = "")
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    connection.Open();
                    using (SqlTransaction transaction = connection.BeginTransaction())
                    {
                        using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_saveCargaGasolinaExterno", true))
                        {
                            command.Transaction = transaction;
                            command.CommandType = CommandType.StoredProcedure;
                            command.CommandText = "sp_saveCargaGasolinaExterno";
                            command.Parameters.Add(new SqlParameter("@idOrdenServ", cargaGas.idOrdenServ));
                            command.Parameters.Add(new SqlParameter("@idCargaCombustible", cargaGas.idCargaCombustible));
                            command.Parameters.Add(new SqlParameter("@kmDespachador", cargaGas.kmDespachador));
                            command.Parameters.Add(new SqlParameter("@kmRecorridos", cargaGas.kmRecorridos));
                            command.Parameters.Add(new SqlParameter("@ltCombustible", cargaGas.ltCombustible));
                            command.Parameters.Add(new SqlParameter("@ltCombustibleUtilizado", cargaGas.ltCombustibleUtilizado));
                            command.Parameters.Add(new SqlParameter("@ltCombustiblePTO", cargaGas.ltCombustiblePTO));
                            command.Parameters.Add(new SqlParameter("@ltCombustibleEST", cargaGas.ltCombustibleEST));
                            command.Parameters.Add(new SqlParameter("@tiempoTotal", cargaGas.tiempoTotal));
                            command.Parameters.Add(new SqlParameter("@tiempoPTO", cargaGas.tiempoPTO));
                            command.Parameters.Add(new SqlParameter("@tiempoEST", cargaGas.tiempoEST));
                            int? nullable = null;
                            command.Parameters.Add(new SqlParameter("@idDespachador", (cargaGas.despachador == null) ? nullable : new int?(cargaGas.despachador.clave)));
                            command.Parameters.Add(new SqlParameter("@sello1", !cargaGas.sello1.HasValue ? nullable : cargaGas.sello1));
                            command.Parameters.Add(new SqlParameter("@sello2", !cargaGas.sello2.HasValue ? nullable : cargaGas.sello2));
                            command.Parameters.Add(new SqlParameter("@selloRes", !cargaGas.selloReserva.HasValue ? nullable : cargaGas.selloReserva));
                            command.Parameters.Add(new SqlParameter("@usuarioCrea", cargaGas.usuarioCrea));
                            command.Parameters.Add(new SqlParameter("@idUnidad", tractor.clave));
                            command.Parameters.Add(new SqlParameter("@remolque1", (remolque1 == null) ? "" : remolque1.clave));
                            command.Parameters.Add(new SqlParameter("@dolly", (dolly == null) ? "" : dolly.clave));
                            command.Parameters.Add(new SqlParameter("@remolque2", (remolque2 == null) ? "" : remolque2.clave));
                            command.Parameters.Add(new SqlParameter("@idOperador", operador.clave));
                            command.Parameters.Add(new SqlParameter("@ticket", cargaGas.ticket));
                            command.Parameters.Add(new SqlParameter("@idProveedor", cargaGas.proveedor.idProveedor));
                            command.Parameters.Add(new SqlParameter("@precio", cargaGas.proveedor.precio));
                            command.Parameters.Add(new SqlParameter("@observaciones", cargaGas.observacioses));
                            command.Parameters.Add(new SqlParameter("@folioImpreso", cargaGas.folioImpreso));
                            command.Parameters.Add(new SqlParameter("@fecha", cargaGas.fechaCombustible));
                            command.Parameters.Add(new SqlParameter("@ecm", cargaGas.ecm));
                            command.Parameters.Add(new SqlParameter("@idEmpresa", idEmpresa));
                            command.Parameters.Add(new SqlParameter("@idZonaOperativa", zonaOperativa == null ? null : (int?)zonaOperativa.idZonaOperativa));
                            command.Parameters.Add(new SqlParameter("@idCliente", cargaGas.idCliente));
                            command.Parameters.Add(new SqlParameter("@unidadCarga", unidadCarga));
                            command.Parameters.Add(new SqlParameter("@isCR", cargaGas.isCR));
                            command.Parameters.Add(new SqlParameter("@isHubodometro", cargaGas.isHubodometo));

                            int num = command.ExecuteNonQuery();
                            if (((int)command.Parameters["@valor"].Value) == 0)
                            {
                                cargaGas.idCargaCombustible = (int)command.Parameters["@id"].Value;
                                if (cargaGas.listViajes.Count > 0)
                                {
                                    command.CommandText = "sp_asignarValeViaje";
                                    command.Parameters.Clear();
                                    foreach (SqlParameter parameter in CrearSqlCommand.getPatametros(false))
                                    {
                                        command.Parameters.Add(parameter);
                                    }
                                    command.Parameters.Add(new SqlParameter("@idVale", cargaGas.idCargaCombustible));
                                    command.Parameters.Add(new SqlParameter("@numViaje", SqlDbType.BigInt));
                                    foreach (Viaje viaje in cargaGas.listViajes)
                                    {
                                        command.Parameters["@numViaje"].Value = viaje.NumGuiaId;
                                        command.ExecuteNonQuery();
                                        if (((int)command.Parameters["@valor"].Value) != 0)
                                        {
                                            transaction.Rollback();
                                            return new OperationResult(command, null);
                                        }
                                    }
                                }

                                foreach (var tickets in cargaGas.listaTickets)
                                {
                                    command.CommandText = "sp_saveTicketsExterno";
                                    command.Parameters.Clear();
                                    foreach (var param in CrearSqlCommand.getPatametros(true))
                                    {
                                        command.Parameters.Add(param);
                                    }
                                    command.Parameters.Add(new SqlParameter("@idCargaCombustible", cargaGas.idCargaCombustible));
                                    command.Parameters.Add(new SqlParameter("@idEmpresa", tickets.empresa.clave));
                                    command.Parameters.Add(new SqlParameter("@idZonaOperativa", tickets.zonaOperativa.idZonaOperativa));
                                    command.Parameters.Add(new SqlParameter("@idCliente", tickets.cliente == null ? null : (int?)tickets.cliente.clave));
                                    command.Parameters.Add(new SqlParameter("@idProveedor", tickets.Proveedor.idProveedor));
                                    command.Parameters.Add(new SqlParameter("@folioTicket", tickets.ticket));
                                    command.Parameters.Add(new SqlParameter("@litros", tickets.litros));
                                    command.Parameters.Add(new SqlParameter("@precio", tickets.precio));
                                    command.Parameters.Add(new SqlParameter("@IEPS", tickets.IEPS));
                                    command.Parameters.Add(new SqlParameter("@importe", tickets.importe));
                                    command.Parameters.Add(new SqlParameter("@isExtra", tickets.isExtra));
                                    command.Parameters.Add(new SqlParameter("@folioImpreso", tickets.folioImpreso));

                                    command.ExecuteNonQuery();
                                    int id = 0;
                                    if (CrearSqlCommand.validarCorrecto(command, out id))
                                    {
                                        tickets.idTicketsExterno = id;
                                    }
                                    else
                                    {
                                        transaction.Rollback();
                                        return new OperationResult(command);
                                    }
                                }
                            }
                            else
                            {
                                transaction.Rollback();
                                return new OperationResult(command, null);
                            }
                            transaction.Commit();
                            result = new OperationResult(command, cargaGas);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            return result;
        }

        public OperationResult saveDetOrdenActividadExterno(List<OrdenTaller> listaOrdenes, Proveedor proveedor, string factura)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    connection.Open();
                    using (SqlTransaction transaction = connection.BeginTransaction())
                    {
                        using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_actualizarOrdenExterna", false))
                        {
                            DateTime now = DateTime.Now;
                            command.Transaction = transaction;
                            command.Parameters.Add(new SqlParameter("@idOrdenServicio", listaOrdenes[0].idOrdenServ));
                            command.Parameters.Add(new SqlParameter("@fecha", now));
                            command.Parameters.Add(new SqlParameter("@idProveedor", proveedor.idProveedor));
                            command.Parameters.Add(new SqlParameter("@factura", factura));
                            command.ExecuteNonQuery();
                            if (!CrearSqlCommand.validarCorrecto(command))
                            {
                                transaction.Rollback();
                                return new OperationResult(command, null);
                            }
                            foreach (OrdenTaller taller in listaOrdenes)
                            {
                                command.CommandText = "sp_actualizarDetalleOrdenExterna";
                                command.Parameters.Clear();
                                foreach (SqlParameter parameter in CrearSqlCommand.getPatametros(false))
                                {
                                    command.Parameters.Add(parameter);
                                }
                                command.Parameters.Add(new SqlParameter("@idOrdenTrabajo", taller.idOrdenTrabajo));
                                command.Parameters.Add(new SqlParameter("@fecha", now));
                                command.Parameters.Add(new SqlParameter("@costoManoObra", taller.costoActividad));
                                command.Parameters.Add(new SqlParameter("@costoProductos", taller.listaProductosCOM.Sum<ProductosCOM>(s => s.precio * s.cantidad)));
                                command.Parameters.Add(new SqlParameter("@usuario", "admin"));
                                command.ExecuteNonQuery();
                                if (!CrearSqlCommand.validarCorrecto(command))
                                {
                                    transaction.Rollback();
                                    return new OperationResult(command, null);
                                }
                                foreach (ProductosCOM scom in taller.listaProductosCOM)
                                {
                                    command.CommandText = "sp_saveDetOrdenActividadExterno";
                                    command.Parameters.Clear();
                                    foreach (SqlParameter parameter2 in CrearSqlCommand.getPatametros(false))
                                    {
                                        command.Parameters.Add(parameter2);
                                    }
                                    command.Parameters.Add(new SqlParameter("@idOrdenServicio", taller.idOrdenServ));
                                    command.Parameters.Add(new SqlParameter("@idOrdenActividad", taller.idOrdenActividad));
                                    command.Parameters.Add(new SqlParameter("@idProducto", scom.idProducto));
                                    command.Parameters.Add(new SqlParameter("@cantidad", scom.cantidad));
                                    command.Parameters.Add(new SqlParameter("@precio", scom.precio));
                                    command.ExecuteNonQuery();
                                    if (!CrearSqlCommand.validarCorrecto(command))
                                    {
                                        transaction.Rollback();
                                        return new OperationResult(command, null);
                                    }
                                }
                            }
                            transaction.Commit();
                            result = new OperationResult(command, null);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult(exception);
            }
            return result;
        }

        public OperationResult saveOperacion(List<string> lista, eTipoMovimiento tipoMovimiento)
        {
            OperationResult result;
            try
            {
                object obj2 = GenerateXML.generateListID(lista);
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_saveOperacionSalidasEntradas";
                        command.Parameters.Add(new SqlParameter("@xml", obj2));
                        command.Parameters.Add(new SqlParameter("@tipoMovimiento", (int)tipoMovimiento));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter4 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter4);
                        SqlParameter parameter5 = new SqlParameter("@identificador", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter3 = command.Parameters.Add(parameter5);
                        connection.Open();
                        int num = command.ExecuteNonQuery();
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            return result;
        }

        public OperationResult savePreOrdenCompraDirecta(ref PreOrdenCompra preOrdenCompra)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    connection.Open();
                    using (SqlTransaction transaction = connection.BeginTransaction())
                    {
                        using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_savePreOC", true))
                        {
                            command.Transaction = transaction;
                            command.Parameters.Add(new SqlParameter("@idOrdenTrabajo", null));
                            command.Parameters.Add(new SqlParameter("@usuario", preOrdenCompra.usuario));
                            command.ExecuteNonQuery();
                            int id = 0;
                            if (CrearSqlCommand.validarCorrecto(command, out id))
                            {
                                preOrdenCompra.idPreOrdenCompra = id;
                                foreach (DetallePreOrdenCompra compra in preOrdenCompra.listaDetalles)
                                {
                                    command.CommandText = "sp_saveDetPreOC";
                                    command.Parameters.Clear();
                                    foreach (SqlParameter parameter in CrearSqlCommand.getPatametros(false))
                                    {
                                        command.Parameters.Add(parameter);
                                    }
                                    command.Parameters.Add(new SqlParameter("@IdPreOC", preOrdenCompra.idPreOrdenCompra));
                                    command.Parameters.Add(new SqlParameter("@idProducto", compra.producto.idProducto));
                                    command.Parameters.Add(new SqlParameter("@CantidadSol", compra.cantidad));
                                    command.ExecuteNonQuery();
                                    if (!CrearSqlCommand.validarCorrecto(command))
                                    {
                                        transaction.Rollback();
                                        return new OperationResult(command, null);
                                    }
                                }
                            }
                            else
                            {
                                transaction.Rollback();
                                return new OperationResult(command, null);
                            }
                            transaction.Commit();
                            result = new OperationResult(command, preOrdenCompra);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult(exception);
            }
            return result;
        }

        public OperationResult savePreSalidaAlm(OrdenTaller orden, PreSalidaAlmacen preSalidaAlmacen, List<ProductosCOM> listaProducto, Personal persona, string usuario)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    connection.Open();
                    using (SqlTransaction transaction = connection.BeginTransaction())
                    {
                        using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_saveDetOrdenActividadProductos", false))
                        {
                            command.Transaction = transaction;
                            foreach (ProductosCOM scom in listaProducto)
                            {
                                command.Parameters.Clear();
                                foreach (SqlParameter parameter in CrearSqlCommand.getPatametros(false))
                                {
                                    command.Parameters.Add(parameter);
                                }
                                command.Parameters.Add(new SqlParameter("@idOrdenSer", orden.idOrdenServ));
                                command.Parameters.Add(new SqlParameter("@idOrdenActividad", orden.idOrdenActividad));
                                command.Parameters.Add(new SqlParameter("@idProducto", scom.idProducto));
                                command.Parameters.Add(new SqlParameter("@cantidad", scom.cantidad));
                                command.Parameters.Add(new SqlParameter("@costo", scom.precio));
                                command.Parameters.Add(new SqlParameter("@existencia", scom.existencia));
                                command.ExecuteNonQuery();
                                if (((int)command.Parameters["@valor"].Value) != 0)
                                {
                                    transaction.Rollback();
                                    return new OperationResult(command, null);
                                }
                            }
                            int num = 0;
                            foreach (ProductosCOM scom2 in listaProducto)
                            {
                                if (scom2.cantPreSalida > decimal.Zero)
                                {
                                    if (preSalidaAlmacen.idPreSalida == 0)
                                    {
                                        command.CommandText = "sp_saveCapPreSalida";
                                        command.Parameters.Clear();
                                        foreach (SqlParameter parameter2 in CrearSqlCommand.getPatametros(true))
                                        {
                                            command.Parameters.Add(parameter2);
                                        }
                                        command.Parameters.Add(new SqlParameter("@idPreSalida", preSalidaAlmacen.idPreSalida));
                                        command.Parameters.Add(new SqlParameter("@idOrdenTrabajo", orden.idOrdenTrabajo));
                                        command.Parameters.Add(new SqlParameter("@fecha", preSalidaAlmacen.fecha));
                                        command.Parameters.Add(new SqlParameter("@idPersona", persona.clave));
                                        command.Parameters.Add(new SqlParameter("@estatus", preSalidaAlmacen.estatus));
                                        command.Parameters.Add(new SqlParameter("@importe", preSalidaAlmacen.total));
                                        command.Parameters.Add(new SqlParameter("@CIDDOCUMENTO_ENT", preSalidaAlmacen.CIDDOCUMENTO_ENT));
                                        command.Parameters.Add(new SqlParameter("@CSERIEDOCUMENTO_ENT", preSalidaAlmacen.CSERIEDOCUMENTO_ENT));
                                        command.Parameters.Add(new SqlParameter("@CFOLIO_ENT", preSalidaAlmacen.CFOLIO_ENT));
                                        command.Parameters.Add(new SqlParameter("@CIDDOCUMENTO_SAL", preSalidaAlmacen.CIDDOCUMENTO_SAL));
                                        command.Parameters.Add(new SqlParameter("@CSERIEDOCUMENTO_SAL", preSalidaAlmacen.CSERIEDOCUMENTO_SAL));
                                        command.Parameters.Add(new SqlParameter("@CFOLIO_SAL", preSalidaAlmacen.CFOLIO_SAL));
                                        command.ExecuteNonQuery();
                                        if (((int)command.Parameters["@valor"].Value) != 0)
                                        {
                                            transaction.Rollback();
                                            return new OperationResult(command, null);
                                        }
                                        preSalidaAlmacen.idPreSalida = (int)command.Parameters["@id"].Value;
                                    }
                                    command.CommandText = "sp_saveDetCapPreSalida";
                                    command.Parameters.Clear();
                                    foreach (SqlParameter parameter3 in CrearSqlCommand.getPatametros(false))
                                    {
                                        command.Parameters.Add(parameter3);
                                    }
                                    command.Parameters.Add(new SqlParameter("@idPreSalida", preSalidaAlmacen.idPreSalida));
                                    command.Parameters.Add(new SqlParameter("@idProducto", scom2.idProducto));
                                    command.Parameters.Add(new SqlParameter("@cantidad", scom2.cantPreSalida));
                                    command.Parameters.Add(new SqlParameter("@precio", scom2.precio));
                                    command.Parameters.Add(new SqlParameter("@existencia", scom2.existencia));
                                    command.ExecuteNonQuery();
                                    if (((int)command.Parameters["@valor"].Value) != 0)
                                    {
                                        transaction.Rollback();
                                        return new OperationResult(command, null);
                                    }
                                }
                                if (scom2.cantPreOC > decimal.Zero)
                                {
                                    if (num == 0)
                                    {
                                        command.CommandText = "sp_savePreOC";
                                        command.Parameters.Clear();
                                        foreach (SqlParameter parameter4 in CrearSqlCommand.getPatametros(true))
                                        {
                                            command.Parameters.Add(parameter4);
                                        }
                                        command.Parameters.Add(new SqlParameter("@idOrdenTrabajo", orden.idOrdenTrabajo));
                                        command.Parameters.Add(new SqlParameter("@usuario", usuario));
                                        command.ExecuteNonQuery();
                                        if (((int)command.Parameters["@valor"].Value) != 0)
                                        {
                                            transaction.Rollback();
                                            return new OperationResult(command, null);
                                        }
                                        num = (int)command.Parameters["@id"].Value;
                                    }
                                    command.CommandText = "sp_saveDetPreOC";
                                    command.Parameters.Clear();
                                    foreach (SqlParameter parameter5 in CrearSqlCommand.getPatametros(false))
                                    {
                                        command.Parameters.Add(parameter5);
                                    }
                                    command.Parameters.Add(new SqlParameter("@IdPreOC", num));
                                    command.Parameters.Add(new SqlParameter("@idProducto", scom2.idProducto));
                                    command.Parameters.Add(new SqlParameter("@CantidadSol", scom2.cantPreOC));
                                    command.ExecuteNonQuery();
                                    if (((int)command.Parameters["@valor"].Value) != 0)
                                    {
                                        transaction.Rollback();
                                        return new OperationResult(command, null);
                                    }
                                }
                            }
                            transaction.Commit();
                            result = new OperationResult(command, null);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult(exception);
            }
            return result;
        }

        public OperationResult saveSalidaEntrada(List<SalidaEntrada> lista)
        {
            OperationResult result;
            try
            {
                object obj2 = GenerateXML.generateListSalidas(lista);
                if (obj2 == null)
                {
                    result = new OperationResult
                    {
                        valor = 1,
                        mensaje = "Error al construir el XML"
                    };
                }
                else
                {
                    using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                    {
                        using (SqlCommand command = connection.CreateCommand())
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.CommandText = "sp_saveSalidasEntradas";
                            command.Parameters.Add(new SqlParameter("@xml", obj2));
                            SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                            {
                                Direction = ParameterDirection.Output
                            };
                            SqlParameter parameter = command.Parameters.Add(parameter1);
                            SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                            {
                                Direction = ParameterDirection.Output
                            };
                            SqlParameter parameter2 = command.Parameters.Add(parameter3);
                            connection.Open();
                            int num = command.ExecuteNonQuery();
                            result = new OperationResult
                            {
                                valor = new int?((int)parameter.Value),
                                mensaje = (string)parameter2.Value,
                                result = lista
                            };
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            return result;
        }

        public OperationResult saveSalidaEntrada(ref SalidaEntrada salidaEntrada, int idUsuario)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_saveSalidasEntradas";
                        command.Parameters.Add(new SqlParameter("@idPadreOrdSer", salidaEntrada.masterOrdServ.idPadreOrdSer));
                        command.Parameters.Add(new SqlParameter("@tipoMovimiento", (int)salidaEntrada.tipoMoviemiento));
                        command.Parameters.Add(new SqlParameter("@idChofer", salidaEntrada.chofer.clave));
                        command.Parameters.Add(new SqlParameter("@km", salidaEntrada.km));
                        command.Parameters.Add(new SqlParameter("@id", salidaEntrada.idOperacion));
                        command.Parameters.Add(new SqlParameter("@fecha", salidaEntrada.fecha));
                        command.Parameters.Add(new SqlParameter("@idUsuario", idUsuario));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter5 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter5);
                        SqlParameter parameter6 = new SqlParameter("@identificador", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter3 = command.Parameters.Add(parameter6);
                        SqlParameter parameter7 = new SqlParameter("@FinalKm", SqlDbType.Decimal)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter4 = command.Parameters.Add(parameter7);
                        connection.Open();
                        int num = command.ExecuteNonQuery();
                        salidaEntrada.idOperacion = (int)parameter3.Value;
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = salidaEntrada
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            return result;
        }
        public OperationResult ligarViajesConVale(CargaCombustible carga)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_ligarViajesConVale"))
                    {
                        comad.Parameters.Add(new SqlParameter("@idTractor", carga.unidadTransporte.clave));
                        comad.Parameters.Add(new SqlParameter("@idVale", carga.idCargaCombustible));
                        con.Open();
                        comad.ExecuteNonQuery();
                        return new OperationResult(comad);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult cancelaValeCombustible(int idVale)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    con.Open();
                    using (SqlTransaction tran = con.BeginTransaction())
                    {
                        using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_cancelarValeCarga"))
                        {
                            comad.Transaction = tran;
                            comad.Parameters.Add(new SqlParameter("@idVale", idVale));
                            comad.ExecuteNonQuery();
                            if (CrearSqlCommand.validarCorrecto(comad))
                            {
                                tran.Commit();
                            }
                            else
                            {
                                tran.Rollback();
                            }
                            return new OperationResult(comad);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getCargasExtraByVale(int idVale)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getCargasExtraByVale"))
                    {
                        comad.Parameters.Add(new SqlParameter("@idVale", idVale));
                        con.Open();
                        List<int> listaId = new List<int>();
                        using (SqlDataReader reader = comad.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                listaId.Add((int)reader["idCargaExtra"]);
                            }
                        }
                        return new OperationResult(comad, listaId);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
}

