﻿namespace CoreFletera.BusinessLogic
{
    using Core.Models;
    using Core.Utils;
    using CoreFletera.Models;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Runtime.InteropServices;

    public class DetallesConsumoUnidadesServices
    {
        internal OperationResult getDetallesConsumoUnidades(int idEmpresa, int idCliente, DateTime fechaInicio, DateTime fechaFin, List<string> listaTractores, bool todos = false)
        {
            OperationResult result;
            try
            {
                string str = GenerateXML.generarListaUnidades(listaTractores);
                if (string.IsNullOrEmpty(str))
                {
                    result = new OperationResult
                    {
                        valor = 2,
                        mensaje = "error al construir el XML"
                    };
                }
                else
                {
                    using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                    {
                        using (SqlCommand command = connection.CreateCommand())
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.CommandText = "sp_getDetallesConsumoUnidades";
                            command.Parameters.Add(new SqlParameter("@idEmpresa", idEmpresa));
                            command.Parameters.Add(new SqlParameter("@idCliente", (idCliente == 0) ? "%" : idCliente.ToString()));
                            command.Parameters.Add(new SqlParameter("@fechaInicio", fechaInicio));
                            command.Parameters.Add(new SqlParameter("@fechaFin", fechaFin));
                            command.Parameters.Add(new SqlParameter("@xml", str));
                            command.Parameters.Add(new SqlParameter("@todos", todos));
                            SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                            {
                                Direction = ParameterDirection.Output
                            };
                            SqlParameter parameter = command.Parameters.Add(parameter1);
                            SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                            {
                                Direction = ParameterDirection.Output
                            };
                            SqlParameter parameter2 = command.Parameters.Add(parameter3);
                            List<DetallesConsumoUnidades> list = new List<DetallesConsumoUnidades>();
                            connection.Open();
                            using (SqlDataReader reader = command.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    DetallesConsumoUnidades item = new DetallesConsumoUnidades
                                    {
                                        claveUnidad = (string)reader["claveUnidad"],
                                        kmRecorridosSen = (decimal)reader["kmRecorridosSen"],
                                        kmRecorridosFull = (decimal)reader["kmRecorridosFull"],
                                        kmRecorridos = (decimal)reader["kmRecorridos"],
                                        ltsConsumidosSen = (decimal)reader["ltsConsumidosSen"],
                                        ltsConsumidosFull = (decimal)reader["ltsConsumidosFull"],
                                        ltsConsumidos = (decimal)reader["ltsConsumidos"],
                                        viajesSen = (decimal)reader["viajesSen"],
                                        viajesFull = (decimal)reader["viajesFull"],
                                        viajes = (decimal)reader["viajes"],
                                        casetasSen = (decimal)reader["casetasSen"],
                                        casetasFull = (decimal)reader["casetasFull"],
                                        casetas = (decimal)reader["casetas"],
                                        dieselSen = (decimal)reader["dieselSen"],
                                        dieselFull = (decimal)reader["dieselFull"],
                                        diesel = (decimal)reader["diesel"],
                                        operadoresSen = (decimal)reader["operadoresSen"],
                                        operadoresFull = (decimal)reader["operadoresFull"],
                                        operadores = (decimal)reader["operadores"],
                                        lavadero = (decimal)reader["lavadero"],
                                        llantas = (decimal)reader["llantas"],
                                        mtto = (decimal)reader["mtto"]
                                    };
                                    list.Add(item);
                                }
                            }
                            result = new OperationResult
                            {
                                valor = new int?((int)parameter.Value),
                                mensaje = (string)parameter2.Value,
                                result = list
                            };
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            return result;
        }
    }
}
