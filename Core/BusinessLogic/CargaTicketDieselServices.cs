﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using System.Data;
using System.Data.SqlClient;
using Core.Utils;
using Core.BusinessLogic;

namespace CoreFletera.BusinessLogic
{
    public class CargaTicketDieselServices
    {
        public OperationResult saveCargaticketDiesel(ref CargaTicketDiesel carga)
        {
            try
            {
                using (SqlConnection con  = BoundedContextFactory.ConnectionFactory())
                {
                    con.Open();
                    using (SqlTransaction tran = con.BeginTransaction())
                    {
                        using (SqlCommand commad= CrearSqlCommand.getCommand(con, "save_CargaTicketDiesel", true))
                        {
                            commad.Transaction = tran;

                            commad.Parameters.Add(new SqlParameter("@idCargaTicket", carga.clave));
                            commad.Parameters.Add(new SqlParameter("@idEmpresa", carga.empresa.clave));
                            commad.Parameters.Add(new SqlParameter("@idZonaOperativa", carga.zonaOperativa.idZonaOperativa));
                            commad.Parameters.Add(new SqlParameter("@idUnidad", carga.tractor.clave));
                            commad.Parameters.Add(new SqlParameter("@idProveedor", carga.proveedor.idProveedor));
                            commad.Parameters.Add(new SqlParameter("@ticket", carga.ticket));
                            commad.Parameters.Add(new SqlParameter("@precio", carga.precio));
                            commad.Parameters.Add(new SqlParameter("@litros", carga.litros));
                            commad.Parameters.Add(new SqlParameter("@folioImpreso", carga.folioImpreso));
                            commad.Parameters.Add(new SqlParameter("@observaciones", carga.observaciones));
                            commad.Parameters.Add(new SqlParameter("@idOperador", carga.operador.clave));
                            commad.Parameters.Add(new SqlParameter("@idValeCarga", carga.carga == null ? null : (int?)carga.carga.idCargaCombustible));

                            commad.ExecuteNonQuery();
                            int id = 0;
                            if (CrearSqlCommand.validarCorrecto(commad, out id))
                            {
                                carga.clave = id;
                                tran.Commit();
                            }
                            else
                            {
                                tran.Rollback();
                            }
                            return new OperationResult(commad, carga);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getCargaTicketsDieselByEmpresaZonaOperativa(int idEmpresa, int idZonaOperativa)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "getCargaTicketsDieselByEmpresaZonaOperativa"))
                    {
                        comad.Parameters.Add(new SqlParameter("@idEmpresa", idEmpresa));
                        comad.Parameters.Add(new SqlParameter("@idZonaOperativa", idZonaOperativa));
                        con.Open();
                        List<CargaTicketDiesel> lista = new List<CargaTicketDiesel>();
                        using (SqlDataReader reader = comad.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CargaTicketDiesel carga = new mapComunes().mapCargaTicketDiesel(reader);

                                if (carga == null) return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");
                                
                                lista.Add(carga);
                            }
                        }
                        return new OperationResult(comad, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getCargaTicketsDieselByUnidad(string idUnidad)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "getCargaTicketsDieselByUnidad"))
                    {
                        comad.Parameters.Add(new SqlParameter("@idUnidad", idUnidad));
                        con.Open();
                        List<CargaTicketDiesel> lista = new List<CargaTicketDiesel>();
                        using (SqlDataReader reader = comad.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CargaTicketDiesel carga = new mapComunes().mapCargaTicketDiesel(reader);

                                if (carga == null) return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");

                                lista.Add(carga);
                            }
                        }
                        return new OperationResult(comad, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getCargaTicketsDieselByVale(int idVale)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "getCargaTicketsDieselByVale"))
                    {
                        comad.Parameters.Add(new SqlParameter("@idVale", idVale));
                        con.Open();
                        List<CargaTicketDiesel> lista = new List<CargaTicketDiesel>();
                        using (SqlDataReader reader = comad.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CargaTicketDiesel carga = new mapComunes().mapCargaTicketDiesel(reader);

                                if (carga == null) return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");

                                lista.Add(carga);
                            }
                        }
                        return new OperationResult(comad, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

    }
}
