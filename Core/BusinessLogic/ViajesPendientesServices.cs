﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using Core.Utils;
using System.Data;
using System.Data.SqlClient;
namespace CoreFletera.BusinessLogic
{
    public class ViajesPendientesServices
    {
        public OperationResult saveViajesCargasPendientes(ref Viaje viaje, ref CargaCombustible carga)
        {
            string xml = GenerateXML.generateListDetalles(viaje.listDetalles);
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_saveViajesPendientes";

                        command.Parameters.Add(new SqlParameter("@idCliente", viaje.cliente.clave));
                        command.Parameters.Add(new SqlParameter("@IdEmpresa", viaje.IdEmpresa));
                        command.Parameters.Add(new SqlParameter("@fecha", viaje.FechaHoraViaje));
                        command.Parameters.Add(new SqlParameter("@idOperador", viaje.operador.clave));
                        command.Parameters.Add(new SqlParameter("@idTractor", viaje.tractor.clave));
                        command.Parameters.Add(new SqlParameter("@idRemolque1", viaje.remolque.clave));
                        command.Parameters.Add(new SqlParameter("@idDolly", viaje.dolly == null ? null : viaje.dolly.clave));
                        command.Parameters.Add(new SqlParameter("@idRemolque2", viaje.remolque2 == null ? null : viaje.remolque2.clave));
                        command.Parameters.Add(new SqlParameter("@UserViaje", viaje.UserViaje));
                        command.Parameters.Add(new SqlParameter("@tipoViaje", viaje.tipoViaje));
                        command.Parameters.Add(new SqlParameter("@isExterno", viaje.isExterno));
                        command.Parameters.Add(new SqlParameter("@XML", xml));

                        command.Parameters.Add(new SqlParameter("@km", carga.kmRecorridos));
                        command.Parameters.Add(new SqlParameter("@kmDespachador", carga.kmDespachador));
                        command.Parameters.Add(new SqlParameter("@ltCombustible", carga.ltCombustible));
                        command.Parameters.Add(new SqlParameter("@ltCombustibleUtilizado", carga.ltCombustibleUtilizado));
                        command.Parameters.Add(new SqlParameter("@ltCombustiblePTO", carga.ltCombustiblePTO));
                        command.Parameters.Add(new SqlParameter("@ltCombustibleEst", carga.ltCombustibleEST));
                        command.Parameters.Add(new SqlParameter("@tiempo", carga.tiempoTotal));
                        command.Parameters.Add(new SqlParameter("@tiempoPTO", carga.tiempoPTO));
                        command.Parameters.Add(new SqlParameter("@tiempoEst", carga.tiempoEST));
                        command.Parameters.Add(new SqlParameter("@sello1", carga.sello1));
                        command.Parameters.Add(new SqlParameter("@sello2", carga.sello2));
                        command.Parameters.Add(new SqlParameter("@selloRes", carga.selloReserva));
                        command.Parameters.Add(new SqlParameter("@ticket", carga.ticket));
                        command.Parameters.Add(new SqlParameter("@folioImpreso", carga.folioImpreso));
                        command.Parameters.Add(new SqlParameter("@idDespachador", carga.despachador.clave));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var spViaje = command.Parameters.Add(new SqlParameter("@idViaje", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var spvale = command.Parameters.Add(new SqlParameter("@idVale", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var spMensaje = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                        connection.Open();
                        var i = command.ExecuteNonQuery();
                        if ((int)spValor.Value == 0)
                        {
                            viaje.NumGuiaId = (int)spViaje.Value;
                            carga.idCargaCombustible = (int)spvale.Value;
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = new List<object>() { viaje, carga } };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }
    }
}
