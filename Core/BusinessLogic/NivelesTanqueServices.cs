﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoreFletera.Models;
using Core.BusinessLogic;
using Core.Models;
using Core.Utils;
using System.Data;
using System.Data.SqlClient;
namespace CoreFletera.BusinessLogic
{
    public class NivelesTanqueServices
    {
        public OperationResult getNivelTanqueByZonaOperativa(ZonaOperativa zonaOperativa)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand commad = CrearSqlCommand.getCommand(con, "sp_getNivelTanqueByZonaOperativa"))
                    {
                        commad.Parameters.Add(new SqlParameter("@idZonaOperativa", zonaOperativa.idZonaOperativa));
                        NivelesTanque nivelesTanque = new NivelesTanque();
                        con.Open();
                        using (SqlDataReader sqlReader = commad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                nivelesTanque = new mapComunes().mapNivelTanque(sqlReader);
                                if (nivelesTanque == null)
                                {
                                    return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER EL RESULTADO DE LA BUSQUEDA");
                                }
                            }
                        }

                        var resp = getDetallesNivelTanqueByidTanques(new List<int> { nivelesTanque.idNivel });
                        if (resp.typeResult == ResultTypes.success)
                        {
                            List<DetalleNivelesTanque> lista = resp.result as List<DetalleNivelesTanque>;
                            nivelesTanque.listaDetalles = lista.FindAll(s => s.idNivel == nivelesTanque.idNivel);
                        }
                        else
                        {
                            return resp;
                        }
                        return new OperationResult(commad, nivelesTanque);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        public OperationResult getNivelTanqueByFiltros(ZonaOperativa zonaOperativa, DateTime fechaInicial, DateTime fechaFinal)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand commad = CrearSqlCommand.getCommand(con, "sp_getNivelTanqueByFiltros"))
                    {
                        commad.Parameters.Add(new SqlParameter("@idZonaOperativa", zonaOperativa.idZonaOperativa));
                        commad.Parameters.Add(new SqlParameter("@fechaInicial", fechaInicial));
                        commad.Parameters.Add(new SqlParameter("@fechaFinal", fechaFinal));
                        List< NivelesTanque> listaNiveles = new List<NivelesTanque>();
                        con.Open();
                        using (SqlDataReader sqlReader = commad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                NivelesTanque nivel = new mapComunes().mapNivelTanque(sqlReader);
                                if (nivel == null)
                                {
                                    return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER EL RESULTADO DE LA BUSQUEDA");
                                }
                                listaNiveles.Add(nivel);
                            }
                        }
                        if (listaNiveles.Count > 0)
                        {
                            var resp = getDetallesNivelTanqueByidTanques(listaNiveles.Select(s => s.idNivel).ToList());
                            if (resp.typeResult == ResultTypes.success)
                            {
                                List<DetalleNivelesTanque> lista = resp.result as List<DetalleNivelesTanque>;
                                foreach (var nvel in listaNiveles)
                                {
                                    nvel.listaDetalles = lista.FindAll(s => s.idNivel == nvel.idNivel);
                                }
                            }
                            else
                            {
                                return resp;
                            }
                        }                        
                        return new OperationResult(commad, listaNiveles);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getDetallesNivelTanqueByidTanques(List<int> listaids)
        {
            try
            {
                string xml = GenerateXML.generarListaIds(listaids);
                if (string.IsNullOrEmpty(xml))
                {
                    return new OperationResult(ResultTypes.warning, "Ocurrio un error al generar el XML");
                }
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand commad = CrearSqlCommand.getCommand(con, "sp_getDetallesNivelTanqueByidTanques"))
                    {
                        commad.Parameters.Add(new SqlParameter("@xml", xml));
                        List<DetalleNivelesTanque> lista = new List<DetalleNivelesTanque>();
                        con.Open();
                        using (SqlDataReader sqlReader = commad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                DetalleNivelesTanque detalle = new mapComunes().mapDetalleNivelTanque(sqlReader);
                                if (detalle == null)
                                {
                                    return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER EL RESULTADO DE LA BUSQUEDA");
                                }
                                else
                                {
                                    lista.Add(detalle);
                                }
                            }

                        }
                        return new OperationResult(commad, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        public OperationResult saveNivelTanques(ref NivelesTanque nivelesTanque)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    con.Open();
                    using (SqlTransaction tran = con.BeginTransaction())
                    {
                        using (SqlCommand commad = CrearSqlCommand.getCommand(con, "sp_saveNivelTanques", true))
                        {
                            commad.Transaction = tran;
                            commad.Parameters.Add(new SqlParameter("@idNivel", nivelesTanque.idNivel));
                            commad.Parameters.Add(new SqlParameter("@idTanque", nivelesTanque.tanqueCombustible.idTanqueCombustible));
                            commad.Parameters.Add(new SqlParameter("@fecha", nivelesTanque.fecha));
                            commad.Parameters.Add(new SqlParameter("@idPersonaAbre", nivelesTanque.idPersonaAbre));
                            commad.Parameters.Add(new SqlParameter("@fechaInicio", nivelesTanque.fechaInicio));
                            commad.Parameters.Add(new SqlParameter("@fechaFin", nivelesTanque.fechaFinal));
                            commad.Parameters.Add(new SqlParameter("@idPersonaCierra", nivelesTanque.idPersonaCierra));

                            commad.ExecuteNonQuery();
                            int id = 0;
                            if (CrearSqlCommand.validarCorrecto(commad, out id))
                            {
                                nivelesTanque.idNivel = id;
                                commad.CommandText = "sp_saveDetalleNivelTanques";
                                foreach (DetalleNivelesTanque detalle in nivelesTanque.listaDetalles)
                                {
                                    commad.Parameters.Clear();
                                    foreach (SqlParameter prm in CrearSqlCommand.getPatametros(true))
                                    {
                                        commad.Parameters.Add(prm);
                                    }
                                    commad.Parameters.Add(new SqlParameter("@idDetalleNivel", detalle.idDetalleNivel));
                                    commad.Parameters.Add(new SqlParameter("@idNivel", nivelesTanque.idNivel));
                                    commad.Parameters.Add(new SqlParameter("@idBomba", detalle.bombasCombustible.idBombaCombustible));
                                    commad.Parameters.Add(new SqlParameter("@nivelInicial", detalle.nivelInicial));
                                    commad.Parameters.Add(new SqlParameter("@nivelFinal", detalle.nivelFinal));

                                    commad.ExecuteNonQuery();
                                    if (CrearSqlCommand.validarCorrecto(commad, out id))
                                    {
                                        detalle.idDetalleNivel = id;
                                    }
                                    else
                                    {
                                        tran.Rollback();
                                        return new OperationResult(commad);
                                    }
                                }
                                if (nivelesTanque.cerrado)
                                {
                                    KardexCombustible kardex = new KardexCombustible
                                    {
                                        cantidad = nivelesTanque.totalDespacho,
                                        empresa = null,
                                        tipoMovimiento = eTipoMovimiento.SALIDA,
                                        tipoOperacion = TipoOperacionDiesel.POR_CIERRE_NIVEL,
                                        idOperacion = nivelesTanque.idNivel,
                                        idInventarioDiesel = 0,
                                        personal = new Personal { clave = (int)nivelesTanque.idPersonaCierra },
                                        tanque = nivelesTanque.tanqueCombustible,
                                        unidadTransporte = null
                                    };
                                    var respKardex = new KardexCombustibleServices().savekardexCombustible(ref kardex);
                                    if (respKardex.typeResult != ResultTypes.success)
                                    {
                                        tran.Rollback();
                                        return respKardex;
                                    }
                                }
                            }
                            else
                            {
                                tran.Rollback();
                                return new OperationResult(commad);
                            }
                            tran.Commit();
                            return new OperationResult(commad, nivelesTanque);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        public OperationResult getResumenCargasInternas(DateTime fechaInicial, DateTime fechaFinal, int idZonaOperativa = 1)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand commad = CrearSqlCommand.getCommand(con, "sp_getResumenCargasInternas"))
                    {
                        commad.Parameters.Add(new SqlParameter("@fechaInicial", fechaInicial));
                        commad.Parameters.Add(new SqlParameter("@fechaFinal", fechaFinal));
                        commad.Parameters.Add(new SqlParameter("@idZonaOpetativa", idZonaOperativa));

                        List<ResumenCargasInternas> lista = new List<ResumenCargasInternas>();
                        con.Open();
                        using (SqlDataReader sqlReader = commad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                ResumenCargasInternas resumen = new ResumenCargasInternas
                                {
                                    empresa = (string)sqlReader["empresa"],
                                    clave = (int)sqlReader["clave"],
                                    extra = Convert.ToBoolean(sqlReader["extra"]),
                                    lts = (decimal)sqlReader["lts"],
                                    unidadTransporte = (string)sqlReader["unidadTransporte"]
                                };
                                lista.Add(resumen);
                            }
                        }
                        return new OperationResult(commad, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
}
