﻿using Core.Models;
using Core.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Interfaces;
namespace Core.BusinessLogic
{
    public class ViajesBachocoServices
    {
        public OperationResult saveViajeBachoco(ref ViajeBachoco viajeBachoco)
        {
            try
            {
                var XML = GenerateXML.generateListDetalles(viajeBachoco.listaCartaPortes);
                //return new OperationResult { mensaje = "Error al contruir el XML", valor = 1 };
                if (XML == null)
                {
                    return new OperationResult { mensaje = "Error al contruir el XML", valor = 1 };
                }
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_saveViajeBachoco";
                        command.Parameters.Add(new SqlParameter("@id", viajeBachoco.idViaje));
                        command.Parameters.Add(new SqlParameter("@idCliente", viajeBachoco.cliente.clave));
                        command.Parameters.Add(new SqlParameter("@idTractor", viajeBachoco.tractor.clave));
                        command.Parameters.Add(new SqlParameter("@idTolva1", viajeBachoco.tolva1.clave));
                        command.Parameters.Add(new SqlParameter("@idDolly", viajeBachoco.dolly == null ? null : viajeBachoco.dolly.clave));
                        command.Parameters.Add(new SqlParameter("@idTolva2", viajeBachoco.tolva2 == null ? null : viajeBachoco.tolva2.clave));
                        command.Parameters.Add(new SqlParameter("@fechaViaje", viajeBachoco.fechaViaje));
                        command.Parameters.Add(new SqlParameter("@idChofer", viajeBachoco.chofer.clave));
                        command.Parameters.Add(new SqlParameter("@idUsuario", viajeBachoco.usuarioBascula.idUsuario));
                        command.Parameters.Add(new SqlParameter("@tara", viajeBachoco.tara));
                        command.Parameters.Add(new SqlParameter("@estatus", (int)viajeBachoco.estatus));

                        command.Parameters.Add(new SqlParameter("@xml", XML));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        var identificador = command.Parameters.Add(new SqlParameter("@identificador", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        connection.Open();
                        var i = command.ExecuteReader();

                        OperationResult resp = new OperationResult();
                        resp = getCartaPortesBachocoById((int)identificador.Value);
                        if (resp.typeResult == ResultTypes.success)
                        {
                            viajeBachoco.listaCartaPortes = new List<CartaPorteBachoco>();
                            viajeBachoco.listaCartaPortes = (List<CartaPorteBachoco>)resp.result;
                        }

                        viajeBachoco.idViaje = (int)identificador.Value;
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)codigo.Value, result = viajeBachoco };
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }

        public OperationResult getViajeBachocoByIdCamion(string idCamion)
        {
            try
            {

                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getViajeBachocoByIdCamion";
                        command.Parameters.Add(new SqlParameter("@idCamion", idCamion));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                        connection.Open();
                        var i = command.ExecuteReader();
                        ViajeBachoco viajeBachoco = new ViajeBachoco();
                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                viajeBachoco.idViaje = (int)sqlReader["idViaje"];

                                OperationResult resp = new OperationResult();

                                resp = new ClienteSvc().getClientesALLorById((int)sqlReader["idCliente"]);
                                if (resp.typeResult == ResultTypes.success)
                                {
                                    Cliente unidad = ((List<Cliente>)resp.result)[0];
                                    viajeBachoco.cliente = unidad;
                                }
                                else
                                {
                                    return new OperationResult { valor = resp.valor, mensaje = resp.mensaje };
                                }

                                resp = new UnidadTransporteSvc().getUnidadesALLorById(0,(string)sqlReader["idTractor"]);
                                if (resp.typeResult == ResultTypes.success)
                                {
                                    UnidadTransporte unidad = ((List<UnidadTransporte>)resp.result)[0];
                                    viajeBachoco.tractor = unidad;
                                }
                                else
                                {
                                    return new OperationResult { valor = resp.valor, mensaje = resp.mensaje };
                                }

                                resp = new UnidadTransporteSvc().getUnidadesALLorById(0,(string)sqlReader["idTolva1"]);
                                if (resp.typeResult == ResultTypes.success)
                                {
                                    UnidadTransporte unidad = ((List<UnidadTransporte>)resp.result)[0];
                                    viajeBachoco.tolva1 = unidad;
                                }
                                else
                                {
                                    return new OperationResult { valor = resp.valor, mensaje = resp.mensaje };
                                }

                                if (DBNull.Value.Equals(sqlReader["idDolly"]))
                                {
                                    viajeBachoco.dolly = null;
                                }
                                else
                                {
                                    resp = new UnidadTransporteSvc().getUnidadesALLorById(0,(string)sqlReader["idDolly"]);
                                    if (resp.typeResult == ResultTypes.success)
                                    {
                                        UnidadTransporte unidad = ((List<UnidadTransporte>)resp.result)[0];
                                        viajeBachoco.dolly = unidad;
                                    }
                                    else
                                    {
                                        return new OperationResult { valor = resp.valor, mensaje = resp.mensaje };
                                    }
                                }

                                if (DBNull.Value.Equals(sqlReader["idTolva2"]))
                                {
                                    viajeBachoco.dolly = null;
                                }
                                else
                                {
                                    resp = new UnidadTransporteSvc().getUnidadesALLorById(0,(string)sqlReader["idTolva2"]);
                                    if (resp.typeResult == ResultTypes.success)
                                    {
                                        UnidadTransporte unidad = ((List<UnidadTransporte>)resp.result)[0];
                                        viajeBachoco.tolva2 = unidad;
                                    }
                                    else
                                    {
                                        return new OperationResult { valor = resp.valor, mensaje = resp.mensaje };
                                    }
                                }

                                viajeBachoco.fechaViaje = (DateTime)sqlReader["fechaViaje"];
                                viajeBachoco.fechaReal = (DateTime)sqlReader["fechaReal"];

                                resp = new OperadorSvc().getPersonalALLorById((int)sqlReader["idChofer"]);
                                if (resp.typeResult == ResultTypes.success)
                                {
                                    Personal personal = ((List<Personal>)resp.result)[0];
                                    viajeBachoco.chofer = personal;
                                }
                                else
                                {
                                    return new OperationResult { valor = resp.valor, mensaje = resp.mensaje };
                                }

                                resp = new UsuarioSvc().getAllUserOrById((int)sqlReader["idUsuario"]);
                                if (resp.typeResult == ResultTypes.success)
                                {
                                    Usuario usuario = ((List<Usuario>)resp.result)[0];
                                    viajeBachoco.usuarioBascula = usuario;
                                }
                                else
                                {
                                    return new OperationResult { valor = resp.valor, mensaje = resp.mensaje };
                                }
                                DateTime? date = null;
                                viajeBachoco.fechaFin = DBNull.Value.Equals(sqlReader["fechaFin"]) ? date : (DateTime)sqlReader["fechaFin"];

                                viajeBachoco.tara = (decimal)sqlReader["tara"];
                                viajeBachoco.estatus = (eEstatus)(int)sqlReader["estatus"];

                                viajeBachoco.listaCartaPortes = new List<CartaPorteBachoco>();
                                resp = getCartaPortesBachocoById((int)sqlReader["idViaje"]);
                                if (resp.typeResult == ResultTypes.error)
                                {
                                    return new OperationResult { valor = resp.valor, mensaje = resp.mensaje };
                                }
                                else if (resp.typeResult == ResultTypes.recordNotFound)
                                {
                                    viajeBachoco.listaCartaPortes = new List<CartaPorteBachoco>();
                                }
                                else if (resp.typeResult == ResultTypes.success)
                                {
                                    viajeBachoco.listaCartaPortes =  (List<CartaPorteBachoco>)resp.result;
                                }
                            }
                        }

                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)codigo.Value, result = viajeBachoco };
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }

        public OperationResult getViajeBachocoByFiltros(
            DateTime fechaInicio,
            DateTime fechaFin,
            string idDestino,
            string idOperador,
            string idTractor,
            string estatus,
            int idcliente)
        {
            try
            {

                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getViajeBachocoByFiltros";
                        command.Parameters.Add(new SqlParameter("@fechaInicio", fechaInicio));
                        command.Parameters.Add(new SqlParameter("@fechaFin", fechaFin));
                        command.Parameters.Add(new SqlParameter("@idDestino", idDestino));
                        command.Parameters.Add(new SqlParameter("@idOperador", idOperador));
                        command.Parameters.Add(new SqlParameter("@idTractor", idTractor));
                        command.Parameters.Add(new SqlParameter("@estatus", estatus));
                        command.Parameters.Add(new SqlParameter("@idcliente", idcliente));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                        connection.Open();
                        var i = command.ExecuteReader();
                        List< ViajeBachoco> list = new List<ViajeBachoco>();
                        
                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                ViajeBachoco viajeBachoco = new ViajeBachoco();
                                viajeBachoco.idViaje = (int)sqlReader["idViaje"];

                                OperationResult resp = new OperationResult();

                                resp = new ClienteSvc().getClientesALLorById((int)sqlReader["idCliente"]);
                                if (resp.typeResult == ResultTypes.success)
                                {
                                    Cliente unidad = ((List<Cliente>)resp.result)[0];
                                    viajeBachoco.cliente = unidad;
                                }
                                else
                                {
                                    return new OperationResult { valor = resp.valor, mensaje = resp.mensaje };
                                }

                                resp = new UnidadTransporteSvc().getUnidadesALLorById(0,(string)sqlReader["idTractor"]);
                                if (resp.typeResult == ResultTypes.success)
                                {
                                    UnidadTransporte unidad = ((List<UnidadTransporte>)resp.result)[0];
                                    viajeBachoco.tractor = unidad;
                                }
                                else
                                {
                                    return new OperationResult { valor = resp.valor, mensaje = resp.mensaje };
                                }

                                resp = new UnidadTransporteSvc().getUnidadesALLorById(0,(string)sqlReader["idTolva1"]);
                                if (resp.typeResult == ResultTypes.success)
                                {
                                    UnidadTransporte unidad = ((List<UnidadTransporte>)resp.result)[0];
                                    viajeBachoco.tolva1 = unidad;
                                }
                                else
                                {
                                    return new OperationResult { valor = resp.valor, mensaje = resp.mensaje };
                                }

                                if (DBNull.Value.Equals(sqlReader["idDolly"]))
                                {
                                    viajeBachoco.dolly = null;
                                }
                                else
                                {
                                    resp = new UnidadTransporteSvc().getUnidadesALLorById(0,(string)sqlReader["idDolly"]);
                                    if (resp.typeResult == ResultTypes.success)
                                    {
                                        UnidadTransporte unidad = ((List<UnidadTransporte>)resp.result)[0];
                                        viajeBachoco.dolly = unidad;
                                    }
                                    else
                                    {
                                        return new OperationResult { valor = resp.valor, mensaje = resp.mensaje };
                                    }
                                }

                                if (DBNull.Value.Equals(sqlReader["idTolva2"]))
                                {
                                    viajeBachoco.dolly = null;
                                }
                                else
                                {
                                    resp = new UnidadTransporteSvc().getUnidadesALLorById(0,(string)sqlReader["idTolva2"]);
                                    if (resp.typeResult == ResultTypes.success)
                                    {
                                        UnidadTransporte unidad = ((List<UnidadTransporte>)resp.result)[0];
                                        viajeBachoco.tolva2 = unidad;
                                    }
                                    else
                                    {
                                        return new OperationResult { valor = resp.valor, mensaje = resp.mensaje };
                                    }
                                }

                                viajeBachoco.fechaViaje = (DateTime)sqlReader["fechaViaje"];
                                viajeBachoco.fechaReal = (DateTime)sqlReader["fechaReal"];

                                resp = new OperadorSvc().getPersonalALLorById((int)sqlReader["idChofer"]);
                                if (resp.typeResult == ResultTypes.success)
                                {
                                    Personal personal = ((List<Personal>)resp.result)[0];
                                    viajeBachoco.chofer = personal;
                                }
                                else
                                {
                                    return new OperationResult { valor = resp.valor, mensaje = resp.mensaje };
                                }

                                resp = new UsuarioSvc().getAllUserOrById((int)sqlReader["idUsuario"]);
                                if (resp.typeResult == ResultTypes.success)
                                {
                                    Usuario usuario = ((List<Usuario>)resp.result)[0];
                                    viajeBachoco.usuarioBascula = usuario;
                                }
                                else
                                {
                                    return new OperationResult { valor = resp.valor, mensaje = resp.mensaje };
                                }
                                DateTime? date = null;
                                viajeBachoco.fechaFin = DBNull.Value.Equals(sqlReader["fechaFin"]) ? date : (DateTime)sqlReader["fechaFin"];

                                viajeBachoco.tara = (decimal)sqlReader["tara"];
                                viajeBachoco.estatus = (eEstatus)(int)sqlReader["estatus"];

                                viajeBachoco.listaCartaPortes = new List<CartaPorteBachoco>();
                                resp = getCartaPortesBachocoById((int)sqlReader["idViaje"]);
                                if (resp.typeResult == ResultTypes.error)
                                {
                                    return new OperationResult { valor = resp.valor, mensaje = resp.mensaje };
                                }
                                else if (resp.typeResult == ResultTypes.recordNotFound)
                                {
                                    viajeBachoco.listaCartaPortes = new List<CartaPorteBachoco>();
                                }
                                else if (resp.typeResult == ResultTypes.success)
                                {
                                    viajeBachoco.listaCartaPortes = (List<CartaPorteBachoco>)resp.result;
                                }

                                list.Add(viajeBachoco);
                            }
                        }

                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)codigo.Value, result = list };
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }

        public OperationResult getViajeBachocoByFechas(DateTime fechaInicio, DateTime fechaFin)         
        {
            try
            {

                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getViajeBachocoByFechas";
                        command.Parameters.Add(new SqlParameter("@fechaInicio", fechaInicio));
                        command.Parameters.Add(new SqlParameter("@fechaFin", fechaFin));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                        connection.Open();
                        var i = command.ExecuteReader();
                        List<ViajeBachoco> list = new List<ViajeBachoco>();

                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                ViajeBachoco viajeBachoco = new ViajeBachoco();
                                viajeBachoco.idViaje = (int)sqlReader["idViaje"];

                                OperationResult resp = new OperationResult();

                                resp = new ClienteSvc().getClientesALLorById((int)sqlReader["idCliente"]);
                                if (resp.typeResult == ResultTypes.success)
                                {
                                    Cliente unidad = ((List<Cliente>)resp.result)[0];
                                    viajeBachoco.cliente = unidad;
                                }
                                else
                                {
                                    return new OperationResult { valor = resp.valor, mensaje = resp.mensaje };
                                }

                                resp = new UnidadTransporteSvc().getUnidadesALLorById(0,(string)sqlReader["idTractor"]);
                                if (resp.typeResult == ResultTypes.success)
                                {
                                    UnidadTransporte unidad = ((List<UnidadTransporte>)resp.result)[0];
                                    viajeBachoco.tractor = unidad;
                                }
                                else
                                {
                                    return new OperationResult { valor = resp.valor, mensaje = resp.mensaje };
                                }

                                resp = new UnidadTransporteSvc().getUnidadesALLorById(0,(string)sqlReader["idTolva1"]);
                                if (resp.typeResult == ResultTypes.success)
                                {
                                    UnidadTransporte unidad = ((List<UnidadTransporte>)resp.result)[0];
                                    viajeBachoco.tolva1 = unidad;
                                }
                                else
                                {
                                    return new OperationResult { valor = resp.valor, mensaje = resp.mensaje };
                                }

                                if (DBNull.Value.Equals(sqlReader["idDolly"]))
                                {
                                    viajeBachoco.dolly = null;
                                }
                                else
                                {
                                    resp = new UnidadTransporteSvc().getUnidadesALLorById(0,(string)sqlReader["idDolly"]);
                                    if (resp.typeResult == ResultTypes.success)
                                    {
                                        UnidadTransporte unidad = ((List<UnidadTransporte>)resp.result)[0];
                                        viajeBachoco.dolly = unidad;
                                    }
                                    else
                                    {
                                        return new OperationResult { valor = resp.valor, mensaje = resp.mensaje };
                                    }
                                }

                                if (DBNull.Value.Equals(sqlReader["idTolva2"]))
                                {
                                    viajeBachoco.dolly = null;
                                }
                                else
                                {
                                    resp = new UnidadTransporteSvc().getUnidadesALLorById(0,(string)sqlReader["idTolva2"]);
                                    if (resp.typeResult == ResultTypes.success)
                                    {
                                        UnidadTransporte unidad = ((List<UnidadTransporte>)resp.result)[0];
                                        viajeBachoco.tolva2 = unidad;
                                    }
                                    else
                                    {
                                        return new OperationResult { valor = resp.valor, mensaje = resp.mensaje };
                                    }
                                }

                                viajeBachoco.fechaViaje = (DateTime)sqlReader["fechaViaje"];
                                viajeBachoco.fechaReal = (DateTime)sqlReader["fechaReal"];

                                resp = new OperadorSvc().getPersonalALLorById((int)sqlReader["idChofer"]);
                                if (resp.typeResult == ResultTypes.success)
                                {
                                    Personal personal = ((List<Personal>)resp.result)[0];
                                    viajeBachoco.chofer = personal;
                                }
                                else
                                {
                                    return new OperationResult { valor = resp.valor, mensaje = resp.mensaje };
                                }

                                resp = new UsuarioSvc().getAllUserOrById((int)sqlReader["idUsuario"]);
                                if (resp.typeResult == ResultTypes.success)
                                {
                                    Usuario usuario = ((List<Usuario>)resp.result)[0];
                                    viajeBachoco.usuarioBascula = usuario;
                                }
                                else
                                {
                                    return new OperationResult { valor = resp.valor, mensaje = resp.mensaje };
                                }
                                DateTime? date = null;
                                viajeBachoco.fechaFin = DBNull.Value.Equals(sqlReader["fechaFin"]) ? date : (DateTime)sqlReader["fechaFin"];

                                viajeBachoco.tara = (decimal)sqlReader["tara"];
                                viajeBachoco.estatus = (eEstatus)(int)sqlReader["estatus"];

                                viajeBachoco.listaCartaPortes = new List<CartaPorteBachoco>();
                                resp = getCartaPortesBachocoById((int)sqlReader["idViaje"]);
                                if (resp.typeResult == ResultTypes.error)
                                {
                                    return new OperationResult { valor = resp.valor, mensaje = resp.mensaje };
                                }
                                else if (resp.typeResult == ResultTypes.recordNotFound)
                                {
                                    viajeBachoco.listaCartaPortes = new List<CartaPorteBachoco>();
                                }
                                else if (resp.typeResult == ResultTypes.success)
                                {
                                    viajeBachoco.listaCartaPortes = (List<CartaPorteBachoco>)resp.result;
                                }

                                list.Add(viajeBachoco);
                            }
                        }

                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)codigo.Value, result = list };
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }

        public OperationResult getCartaPortesBachocoById(int id)
        {
            try
            {

                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getCartaPortesBachocoById";
                        command.Parameters.Add(new SqlParameter("@id", id));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                        connection.Open();
                        var i = command.ExecuteReader();
                        List<CartaPorteBachoco> list = new List<CartaPorteBachoco>();
                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                CartaPorteBachoco cartaPorte = new CartaPorteBachoco();
                                cartaPorte.idCartaPorte = (int)sqlReader["idCartaPorte"];
                                cartaPorte.idViaje = (int)sqlReader["idViaje"];

                                OperationResult resp = new OperationResult();
                                resp = new ProductoSvc().getALLProductosByIdORidFraccion(3, (int)sqlReader["idProducto"]);
                                if (resp.typeResult == ResultTypes.success)
                                {
                                    Producto producto = ((List<Producto>)resp.result)[0];
                                    cartaPorte.producto = producto;
                                }
                                else
                                {
                                    return new OperationResult { valor = resp.valor, mensaje = resp.mensaje };
                                }

                                resp = new ZonasSvc().getZonasALLorById((int)sqlReader["idDestino"]);
                                if (resp.typeResult == ResultTypes.success)
                                {
                                    Zona zona = ((List<Zona>)resp.result)[0];
                                    cartaPorte.destino = zona;
                                }
                                else
                                {
                                    return new OperationResult { valor = resp.valor, mensaje = resp.mensaje };
                                }
                                cartaPorte.remision = (string)sqlReader["remision"];
                                cartaPorte.fechaLlegada = (DateTime)sqlReader["horaLlegada"];
                                cartaPorte.fechaCaptura = (DateTime)sqlReader["horaCaptura"];
                                cartaPorte.horaImpresion = (DateTime)sqlReader["horaImpresion"];
                                cartaPorte.pesoBruto = (decimal)sqlReader["pesoBruto"];
                                cartaPorte.volumenCarga = (decimal)sqlReader["volumenCarga"];
                                cartaPorte.precio = (decimal)sqlReader["precio"];
                                cartaPorte.iva = (decimal)sqlReader["iva"];
                                cartaPorte.retencion = (decimal)sqlReader["retencion"];
                                cartaPorte.observaciones = (string)sqlReader["observaciones"];

                                list.Add(cartaPorte);
                            }
                        }

                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)codigo.Value, result = list };
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }

        public OperationResult cerrarViajeBachoco(int idViaje)
        {
            try
            {

                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_cerrarViajeBachoco";
                        command.Parameters.Add(new SqlParameter("@idViaje", idViaje));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                        connection.Open();
                        var i = command.ExecuteNonQuery();                        

                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)codigo.Value, result = null };
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }

        public OperationResult finalizarViajeBachoco(int idViaje, eEstatus estatus, int idUsuario, string motivo)
        {
            try
            {

                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_finalizarViajeBachoco";
                        command.Parameters.Add(new SqlParameter("@idViaje", idViaje));
                        command.Parameters.Add(new SqlParameter("@estatus", (int)estatus));
                        command.Parameters.Add(new SqlParameter("@idUsuario", idUsuario));
                        command.Parameters.Add(new SqlParameter("@motivo", motivo));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                        connection.Open();
                        var i = command.ExecuteNonQuery();

                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)codigo.Value, result = null };
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }
    }
}
