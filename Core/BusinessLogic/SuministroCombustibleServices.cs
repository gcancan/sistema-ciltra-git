﻿namespace CoreFletera.BusinessLogic
{
    using Core.BusinessLogic;
    using Core.Models;
    using Core.Utils;
    using CoreFletera.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Runtime.InteropServices;

    public class SuministroCombustibleServices
    {
        public OperationResult getDetalleSuministroCombustible(DateTime fechaInicio, DateTime fechaFin, int idTanque, int idEmpresa = 0)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getDetalleSuministroCombustible", false))
                    {
                        command.Parameters.Add(new SqlParameter("@fechaInicio", fechaInicio));
                        command.Parameters.Add(new SqlParameter("@fechaFin", fechaFin));
                        command.Parameters.Add(new SqlParameter("@idTanque", idTanque));
                        command.Parameters.Add(new SqlParameter("@idEmpresa", idEmpresa));
                        List<ReporteSuministrosCombustible> list = new List<ReporteSuministrosCombustible>();
                        connection.Open();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (true)
                            {
                                if (!reader.Read())
                                {
                                    break;
                                }
                                ReporteSuministrosCombustible item = new ReporteSuministrosCombustible();
                                item.folio = (int)reader["folio"];
                                item.fecha = (DateTime)reader["fecha"];
                                item.empresa = (string)reader["empresa"];
                                item.tanque = (string)reader["tanque"];
                                item.factura = (string)reader["factura"];
                                item.noPipa = (string)reader["noPipa"];
                                item.placasPipa = (string)reader["placasPipa"];
                                item.ltsTotales = (decimal)reader["ltsTotales"];
                                item.precio = (decimal)reader["precio"];
                                item.importe = (decimal)reader["importe"];
                                list.Add(item);
                            }
                        }
                        result = new OperationResult(command, list);
                    }
                }
            }
            catch (Exception exception1)
            {
                result = new OperationResult(exception1);
            }
            return result;
        }

        public OperationResult getNivelesIniciales(int idTanque, DateTime fecha)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getNivelesIniciales", false))
                    {
                        command.Parameters.Add(new SqlParameter("@idTanque", idTanque));
                        command.Parameters.Add(new SqlParameter("@fecha", fecha));
                        connection.Open();
                        List<NivelInicial> listaIniciales = new List<NivelInicial>();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (true)
                            {
                                if (!reader.Read())
                                {
                                    break;
                                }
                                NivelInicial inicial1 = new NivelInicial();
                                inicial1.idTanque = (int)reader["idTanque"];
                                inicial1.cantidad = (decimal)reader["existencia"];
                                inicial1.fecha = (DateTime)reader["fecha"];
                                inicial1.precio = (decimal)reader["precio"];
                                inicial1.factura = (string)reader["factura"];
                                listaIniciales.Add(inicial1);
                            }
                        }
                        result = new OperationResult(command, listaIniciales);
                    }
                }
            }
            catch (Exception exception1)
            {
                result = new OperationResult(exception1);
            }
            return result;
        }
        public OperationResult getNivelesIniciales(int idTanque)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getUltimosNivelesIniciales", false))
                    {
                        command.Parameters.Add(new SqlParameter("@idTanque", idTanque));
                        connection.Open();
                        List<NivelInicial> listaIniciales = new List<NivelInicial>();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (true)
                            {
                                if (!reader.Read())
                                {
                                    break;
                                }
                                NivelInicial inicial1 = new NivelInicial();
                                inicial1.idTanque = (int)reader["idTanque"];
                                inicial1.cantidad = (decimal)reader["existencia"];
                                inicial1.fecha = (DateTime)reader["fecha"];
                                inicial1.precio = (decimal)reader["precio"];
                                inicial1.factura = (string)reader["factura"];
                                listaIniciales.Add(inicial1);
                            }
                        }
                        result = new OperationResult(command, listaIniciales);
                    }
                }
            }
            catch (Exception exception1)
            {
                result = new OperationResult(exception1);
            }
            return result;
        }
        public OperationResult saveNivelesIniciales(DateTime fecha, List<NivelInicial> listaNiveles, int idTanque, List<Despacho> listaDespachos, CierreProcesoCombustible cierreProceso = null)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    con.Open();
                    using (SqlTransaction tran = con.BeginTransaction())
                    {
                        using (SqlCommand comand = CrearSqlCommand.getCommand(con, "sp_borrarNivelesIniciales"))
                        {
                            comand.Transaction = tran;
                            comand.Parameters.Add(new SqlParameter("@fecha", fecha));
                            comand.Parameters.Add(new SqlParameter("@idTanque", idTanque));

                            comand.ExecuteNonQuery();
                            if (CrearSqlCommand.validarCorrecto(comand))
                            {
                                comand.CommandText = "sp_saveNivelesIniciales";
                                foreach (var item in listaNiveles)
                                {
                                    comand.Parameters.Clear();
                                    foreach (var param in CrearSqlCommand.getPatametros())
                                    {
                                        comand.Parameters.Add(param);
                                    }
                                    comand.Parameters.Add(new SqlParameter("@fecha", fecha));
                                    comand.Parameters.Add(new SqlParameter("@idTanque", idTanque));
                                    comand.Parameters.Add(new SqlParameter("@existencia", item.cantidadFinal));
                                    comand.Parameters.Add(new SqlParameter("@precio", item.precio));
                                    comand.Parameters.Add(new SqlParameter("@factura", item.factura));
                                    comand.ExecuteNonQuery();
                                    if (!CrearSqlCommand.validarCorrecto(comand))
                                    {
                                        tran.Rollback();
                                        return new OperationResult(comand);
                                    }
                                }
                                comand.CommandText = "sp_actualizarPreciosDespachos";

                                comand.Parameters.Clear();
                                foreach (var param in CrearSqlCommand.getPatametros())
                                {
                                    comand.Parameters.Add(param);
                                }
                                string xml = GenerateXML.generarListaActualizarCargas(listaDespachos);
                                comand.Parameters.Add(new SqlParameter("@xml", xml));
                                comand.ExecuteNonQuery();
                                if (!CrearSqlCommand.validarCorrecto(comand))
                                {
                                    tran.Rollback();
                                    return new OperationResult(comand);
                                }

                                if (cierreProceso != null)
                                {
                                    comand.Parameters.Clear();
                                    foreach (var param in CrearSqlCommand.getPatametros(true))
                                    {
                                        comand.Parameters.Add(param);
                                    }
                                    comand.CommandText = "saveCierreProcesosCombustible";
                                    comand.Parameters.Add(new SqlParameter("@idZonaOperativa", cierreProceso.zonaOperativa.idZonaOperativa));
                                    comand.Parameters.Add(new SqlParameter("@idTanque", cierreProceso.tanque.idTanqueCombustible));
                                    comand.Parameters.Add(new SqlParameter("@usuario", cierreProceso.usuario));
                                    comand.Parameters.Add(new SqlParameter("@fechaInicio", cierreProceso.fechaInicio));
                                    comand.Parameters.Add(new SqlParameter("@fechaFin", cierreProceso.fechaFin));
                                    comand.Parameters.Add(new SqlParameter("@xml", xml));
                                    comand.ExecuteNonQuery();
                                    int id = 0;
                                    if (CrearSqlCommand.validarCorrecto(comand, out id))
                                    {
                                        cierreProceso.idCierreProcesosCombustible = id;
                                    }
                                    else
                                    {
                                        tran.Rollback();
                                        return new OperationResult(comand);
                                    }
                                }
                            }
                            else
                            {
                                tran.Rollback();
                                return new OperationResult(comand);
                            }
                            tran.Commit();
                            return new OperationResult(comand, cierreProceso);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult saveSuministroCombustible(ref SuministroCombustible suministro)
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    connection.Open();
                    using (SqlTransaction transaction = connection.BeginTransaction())
                    {
                        using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_saveSuministroCombustible", true))
                        {
                            command.Transaction = transaction;
                            command.Parameters.Add(new SqlParameter("@idSuministro", suministro.idSuministroCombustible));
                            command.Parameters.Add(new SqlParameter("@fecha", suministro.fecha));
                            command.Parameters.Add(new SqlParameter("@ltsTotales", suministro.ltsCombustible));
                            command.Parameters.Add(new SqlParameter("@idUsuario", suministro.usuario.idUsuario));
                            command.Parameters.Add(new SqlParameter("@idTanque", suministro.tanqueCombustible.idTanqueCombustible));
                            command.Parameters.Add(new SqlParameter("@factura", suministro.factura));
                            command.Parameters.Add(new SqlParameter("@noPipa", suministro.noPipa));
                            command.Parameters.Add(new SqlParameter("@placasPipa", suministro.placasPipa));
                            command.Parameters.Add(new SqlParameter("@operador", suministro.operador));
                            command.Parameters.Add(new SqlParameter("@idPersonaRecive", suministro.PersonalRecibe.clave));
                            command.Parameters.Add(new SqlParameter("@fechaRecive", suministro.fechaRecibe));
                            command.Parameters.Add(new SqlParameter("@importe", suministro.importe));
                            command.ExecuteNonQuery();
                            int id = 0;
                            if (!CrearSqlCommand.validarCorrecto(command, out id))
                            {
                                transaction.Rollback();
                                return new OperationResult(command, null);
                            }
                            else
                            {
                                suministro.idSuministroCombustible = id;
                                KardexCombustible combustible1 = new KardexCombustible();
                                combustible1.idInventarioDiesel = 0;
                                combustible1.cantidad = suministro.ltsCombustible;
                                combustible1.empresa = null;
                                combustible1.idOperacion = new int?(suministro.idSuministroCombustible);
                                combustible1.personal = suministro.usuario.personal;
                                combustible1.tanque = suministro.tanqueCombustible;
                                combustible1.tipoOperacion = TipoOperacionDiesel.SUMINISTRO;
                                combustible1.tipoMovimiento = eTipoMovimiento.ENTRADA;
                                combustible1.unidadTransporte = null;
                                combustible1.fecha = suministro.fechaRecibe == null ? DateTime.Now : (suministro.fechaRecibe);
                                KardexCombustible kardexCombustible = combustible1;
                                OperationResult result = new KardexCombustibleServices().savekardexCombustible(ref kardexCombustible, true);
                                if (result.typeResult == ResultTypes.success)
                                {
                                    command.CommandText = "sp_saveDetalleSuministroCombustible";
                                    foreach (var current in suministro.listaDetalles)
                                    {
                                        if (current.ltsCombustible > 0M)
                                        {
                                            command.Parameters.Clear();
                                            foreach (SqlParameter parameter in CrearSqlCommand.getPatametros(true))
                                            {
                                                command.Parameters.Add(parameter);
                                            }
                                            command.Parameters.Add(new SqlParameter("@idSuministro", suministro.idSuministroCombustible));
                                            command.Parameters.Add(new SqlParameter("@idDetalleSuministro", current.idDetalleSuministroCombustible));
                                            command.Parameters.Add(new SqlParameter("@idEmpresa", current.empresa.clave));
                                            command.Parameters.Add(new SqlParameter("@ltsTotales", current.ltsCombustible));
                                            command.Parameters.Add(new SqlParameter("@idTanque", suministro.tanqueCombustible.idTanqueCombustible));
                                            command.ExecuteNonQuery();
                                            if (!CrearSqlCommand.validarCorrecto(command, out id))
                                            {
                                                transaction.Rollback();
                                                return new OperationResult(command, null);
                                            }
                                            else
                                            {
                                                current.idDetalleSuministroCombustible = id;
                                                KardexCombustible combustible4 = new KardexCombustible();
                                                combustible4.idInventarioDiesel = 0;
                                                combustible4.cantidad = current.ltsCombustible;
                                                combustible4.empresa = current.empresa;
                                                combustible4.idOperacion = new int?(suministro.idSuministroCombustible);
                                                combustible4.personal = suministro.usuario.personal;
                                                combustible4.tanque = suministro.tanqueCombustible;
                                                combustible4.tipoOperacion = TipoOperacionDiesel.SUMINISTRO;
                                                combustible4.tipoMovimiento = eTipoMovimiento.ENTRADA;
                                                combustible4.unidadTransporte = null;
                                                combustible4.fecha = suministro.fechaRecibe == null ? DateTime.Now : (suministro.fechaRecibe);
                                                KardexCombustible combustible3 = combustible4;
                                                OperationResult result3 = new KardexCombustibleServices().savekardexCombustible(ref combustible3, true);
                                                if (result3.typeResult == ResultTypes.success)
                                                {
                                                    continue;
                                                }
                                                else
                                                {
                                                    transaction.Rollback();
                                                    return result3;
                                                }

                                            }

                                        }
                                    }

                                }
                            }
                            transaction.Commit();
                            return new OperationResult(command, suministro);
                        } //fin using command
                    } //Fin using transaction
                }//FIN usin connection
            }//FIN TRY
            catch (Exception exception1)
            {
                return new OperationResult(exception1);

            }
        }//FIN

        public OperationResult getSuministrosCombustible(int idTanque)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getSuministroByIdTanque"))
                    {
                        comad.Parameters.Add(new SqlParameter("@idTanque", idTanque));
                        List<SuministroCombustible> listaSuministro = new List<SuministroCombustible>();
                        con.Open();
                        using (SqlDataReader sqlReader = comad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                SuministroCombustible suministro = new mapComunes().mapSuministro(sqlReader);
                                if (suministro == null) return new OperationResult(ResultTypes.error, "ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");

                                listaSuministro.Add(suministro);
                            }
                        }
                        var resp = getDetalleSuministrosCombustible(idTanque);
                        if (resp.typeResult == ResultTypes.success)
                        {
                            List<DetalleSuminstroCombustible> listaDetalles = resp.result as List<DetalleSuminstroCombustible>;
                            foreach (var suministro in listaSuministro)
                            {
                                suministro.listaDetalles = listaDetalles.FindAll(s => s.idSuministroCombustible == suministro.idSuministroCombustible);
                            }
                        }
                        else
                        {
                            return resp;
                        }
                        return new OperationResult(comad, listaSuministro);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getDetalleSuministrosCombustible(int idTanque)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getDetalleSuministroByIdTanque"))
                    {
                        comad.Parameters.Add(new SqlParameter("@idTanque", idTanque));
                        List<DetalleSuminstroCombustible> listaDetalleSuministro = new List<DetalleSuminstroCombustible>();
                        con.Open();
                        using (SqlDataReader sqlReader = comad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                DetalleSuminstroCombustible detalle = new mapComunes().mapDetallesSuministro(sqlReader);
                                if (detalle == null) return new OperationResult(ResultTypes.error, "ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA (detalles)");

                                listaDetalleSuministro.Add(detalle);
                            }
                        }
                        return new OperationResult(comad, listaDetalleSuministro);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        
        public OperationResult actualizarSuministro(SuministroCombustible suministro)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    con.Open();
                    using (SqlTransaction tran = con.BeginTransaction())
                    {
                        using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_actualizarSuministro"))
                        {
                            comad.Transaction = tran;
                            comad.Parameters.Add(new SqlParameter("@idSuministro", suministro.idSuministroCombustible));
                            comad.Parameters.Add(new SqlParameter("@factura", suministro.factura));
                            comad.Parameters.Add(new SqlParameter("@importe", suministro.importe));
                            comad.ExecuteNonQuery();
                            if (!CrearSqlCommand.validarCorrecto(comad))
                            {
                                tran.Rollback();                                
                            }
                            else
                            {
                                tran.Commit();
                            }
                            return new OperationResult(comad);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
}
