namespace Core.BusinessLogic
{
    using Core.Models;
    using Core.Utils;
    using CoreFletera.BusinessLogic;
    using CoreFletera.Models;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Runtime.InteropServices;

    public class CargaCombuctibleServices
    {
        public OperationResult aplicarFolioSalidaAlmacen(List<CargasCombustibleCOM> lista, int lIdDocto)
        {
            OperationResult result;
            try
            {
                string str = GenerateXML.generarListaCargas(lista);
                if (string.IsNullOrEmpty(str))
                {
                    result = new OperationResult
                    {
                        valor = 1,
                        mensaje = "Error Al generar el XML"
                    };
                }
                else
                {
                    using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                    {
                        using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_aplicarFolioSalidaAlmacen", false))
                        {
                            command.Parameters.Add(new SqlParameter("@xml", str));
                            command.Parameters.Add(new SqlParameter("@lIdDocto", lIdDocto));
                            connection.Open();
                            List<CargasCombustibleCOM> list = new List<CargasCombustibleCOM>();
                            using (SqlDataReader reader = command.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    CargasCombustibleCOM item = new CargasCombustibleCOM
                                    {
                                        idCargaCombustible = (int)reader["idCargaCombustible"],
                                        fecha = (DateTime)reader["fecha"],
                                        empresa = (string)reader["NomBaseDatosCOM"],
                                        lts = (decimal)reader["lts"]
                                    };
                                    list.Add(item);
                                }
                            }
                            result = new OperationResult(command, list);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
            return result;
        }

        internal OperationResult asignarValeExternoCartaPortes(List<CartaPorte> listDetalles, int idCargaCombustible)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    connection.Open();
                    using (SqlTransaction transaction = connection.BeginTransaction())
                    {
                        using (SqlCommand command = connection.CreateCommand())
                        {
                            command.Transaction = transaction;
                            command.CommandType = CommandType.Text;
                            command.CommandText = "UPDATE dbo.DetGuia SET idCargaGasolina = @idCarga WHERE Consecutivo = @consecutivo";
                            foreach (CartaPorte porte in listDetalles)
                            {
                                command.Parameters.Clear();
                                command.Parameters.Add(new SqlParameter("@idCarga", idCargaCombustible));
                                command.Parameters.Add(new SqlParameter("@consecutivo", porte.Consecutivo));
                                command.ExecuteNonQuery();
                            }
                            transaction.Commit();
                            result = new OperationResult
                            {
                                valor = 0,
                                mensaje = ""
                            };
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            return result;
        }

        public OperationResult findUltimaCargaByIdUnidad(string idUnidadTrans)
        {
            OperationResult result;
            DateTime? nullable = null;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_findUltimaCargaByIdUnidad";
                        command.Parameters.Add(new SqlParameter("@idUnidad", idUnidadTrans));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        List<CargaCombustible> list = new List<CargaCombustible>();
                        using (SqlDataReader reader2 = reader)
                        {
                            while (reader2.Read())
                            {
                                CargaCombustible item = new CargaCombustible
                                {
                                    idCargaCombustible = (int)reader2["idCargaCombustible"],
                                    idSalidaEntrada = (int)reader2["idSalidaEntrada"],
                                    idOrdenServ = (int)reader2["idOrdenServ"],
                                    fechaCombustible = DBNull.Value.Equals(reader2["fechaCombustible"]) ? nullable : new DateTime?((DateTime)reader2["fechaCombustible"]),
                                    kmFinal = (decimal)reader2["kmFinal"],
                                    kmRecorridos = (decimal)reader2["kmRecorridos"],
                                    kmDespachador = (decimal)reader2["kmDespachador"],
                                    ltCombustible = (decimal)reader2["ltCombustible"],
                                    ltCombustibleUtilizado = (decimal)reader2["ltCombustibleUtilizado"],
                                    ltCombustiblePTO = (decimal)reader2["ltCombustiblePTO"],
                                    ltCombustibleEST = (decimal)reader2["ltCombustibleEST"],
                                    tiempoTotal = (string)reader2["tiempoTotal"],
                                    tiempoPTO = (string)reader2["tiempoPTO"],
                                    tiempoEST = (string)reader2["tiempoEST"],
                                    sello1 = new int?((int)reader2["sello1"]),
                                    sello2 = new int?((int)reader2["sello2"]),
                                    selloReserva = new int?((int)reader2["selloRes"])
                                };
                                list.Add(item);
                            }
                        }
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = list
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
            return result;
        }

        public OperationResult getCargaCombustibleByIdUnidadTrans(string idUnidadTrans)
        {
            OperationResult result;
            DateTime? nullable = null;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getCargasConbustibleByIdUnidad";
                        command.Parameters.Add(new SqlParameter("@idUnidad", idUnidadTrans));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        List<CargaCombustible> list = new List<CargaCombustible>();
                        using (SqlDataReader reader2 = reader)
                        {
                            while (reader2.Read())
                            {
                                CargaCombustible item = new CargaCombustible
                                {
                                    idCargaCombustible = (int)reader2["idCargaCombustible"],
                                    idSalidaEntrada = (int)reader2["idSalidaEntrada"],
                                    idOrdenServ = (int)reader2["idOrdenServ"],
                                    fechaCombustible = DBNull.Value.Equals(reader2["fechaCombustible"]) ? nullable : new DateTime?((DateTime)reader2["fechaCombustible"]),
                                    kmFinal = (decimal)reader2["kmFinal"],
                                    kmRecorridos = (decimal)reader2["kmRecorridos"],
                                    kmDespachador = (decimal)reader2["kmDespachador"],
                                    ltCombustible = (decimal)reader2["ltCombustible"],
                                    ltCombustibleUtilizado = (decimal)reader2["ltCombustibleUtilizado"],
                                    ltCombustiblePTO = (decimal)reader2["ltCombustiblePTO"],
                                    ltCombustibleEST = (decimal)reader2["ltCombustibleEST"],
                                    tiempoTotal = (string)reader2["tiempoTotal"],
                                    tiempoPTO = (string)reader2["tiempoPTO"],
                                    tiempoEST = (string)reader2["tiempoEST"],
                                    sello1 = new int?((int)reader2["sello1"]),
                                    sello2 = new int?((int)reader2["sello2"]),
                                    selloReserva = new int?((int)reader2["selloRes"])
                                };
                                list.Add(item);
                            }
                        }
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = list
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
            return result;
        }

        public OperationResult getCargaCombustibleExternoByTicket(string ticket)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_CargaCombustibleExternoByTicket";
                        command.Parameters.Add(new SqlParameter("@ticket", ticket));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        int num = 0;
                        connection.Open();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                num = (int)reader["folioOrden"];
                            }
                        }
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = num
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            return result;
        }

        public OperationResult getCargaCombustibleExtra(int idCargaExtra)
        {
            OperationResult result;
            DateTime? nullable = null;
            int? nullable2 = null;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getCargaCombustibleExtra";
                        command.Parameters.Add(new SqlParameter("@id", idCargaExtra));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        CargaCombustibleExtra extra = new CargaCombustibleExtra();
                        connection.Open();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CargaCombustibleExtra extra1 = new CargaCombustibleExtra
                                {
                                    idCargaCombustibleExtra = (int)reader["idVCExtra"]
                                };
                                TipoOrdenCombustible combustible1 = new TipoOrdenCombustible
                                {
                                    idTipoOrdenCombustible = (int)reader["idTipoOSComb"],
                                    nombreTipoOrdenCombustible = (string)reader["NomTipoOSComb"],
                                    requiereViaje = !DBNull.Value.Equals(reader["RequiereViaje"]) && ((bool)reader["RequiereViaje"]),
                                    tipoUnidad = (string)reader["TipoUnidad"],
                                    operativo = (bool)reader["operativo"],
                                    activo = (bool)reader["activo"]
                                };
                                extra1.tipoOrdenCombustible = combustible1;
                                UnidadTransporte transporte1 = new UnidadTransporte
                                {
                                    clave = (string)reader["idUnidadTrans"]
                                };
                                extra1.unidadTransporte = transporte1;
                                extra1.fecha = (DateTime)reader["FechaCrea"];
                                extra1.userCrea = (string)reader["UserCrea"];
                                TipoCombustible combustible2 = new TipoCombustible
                                {
                                    idTipoCombustible = (int)reader["idTipoCombustible"]
                                };
                                extra1.tipoCombustible = combustible2;
                                extra1.fechaCombustible = DBNull.Value.Equals(reader["fechaCombustible"]) ? nullable : new DateTime?((DateTime)reader["fechaCombustible"]);
                                extra1.ltCombustible = new decimal?(DBNull.Value.Equals(reader["ltCombustible"]) ? decimal.Zero : ((decimal)reader["ltCombustible"]));
                                extra1.precioCombustible = new decimal?(DBNull.Value.Equals(reader["precioCombustible"]) ? decimal.Zero : ((decimal)reader["precioCombustible"]));
                                extra1.importe = new decimal?(DBNull.Value.Equals(reader["importe"]) ? decimal.Zero : ((decimal)reader["importe"]));
                                Personal personal1 = new Personal
                                {
                                    clave = DBNull.Value.Equals(reader["idDespachador"]) ? 0 : ((int)reader["idDespachador"])
                                };
                                extra1.despachador = personal1;
                                Personal personal2 = new Personal
                                {
                                    clave = (int)reader["idChofer"]
                                };
                                extra1.chofer = personal2;
                                extra1.idCargaCombustible = DBNull.Value.Equals(reader["idCargaCombustible"]) ? nullable2 : new int?((int)reader["idCargaCombustible"]);
                                extra1.numViaje = DBNull.Value.Equals(reader["NumViaje"]) ? nullable2 : new int?((int)reader["NumViaje"]);
                                extra = extra1;
                            }
                        }
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = extra
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            return result;
        }

        public OperationResult getCargaCombustibleExtraLibresByUnidad(string idUnidad)
        {            
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getCargaCombustibleExtraLibresByUnidad"))
                    {
                        
                        command.Parameters.Add(new SqlParameter("@idUnidad", idUnidad));

                        List<CargaCombustibleExtra> ListaExtra = new List<CargaCombustibleExtra>();
                        connection.Open();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CargaCombustibleExtra extra = new CargaCombustibleExtra
                                {
                                    idCargaCombustibleExtra = (int)reader["idVCExtra"]
                                };
                                TipoOrdenCombustible combustible1 = new TipoOrdenCombustible
                                {
                                    idTipoOrdenCombustible = (int)reader["idTipoOSComb"],
                                    nombreTipoOrdenCombustible = (string)reader["NomTipoOSComb"],
                                    requiereViaje = !DBNull.Value.Equals(reader["RequiereViaje"]) && ((bool)reader["RequiereViaje"]),
                                    tipoUnidad = (string)reader["TipoUnidad"],
                                    operativo = (bool)reader["operativo"],
                                    activo = (bool)reader["activo"]
                                };
                                extra.tipoOrdenCombustible = combustible1;
                                UnidadTransporte transporte1 = new UnidadTransporte
                                {
                                    clave = (string)reader["idUnidadTrans"]
                                };
                                extra.unidadTransporte = transporte1;
                                extra.fecha = (DateTime)reader["FechaCrea"];
                                extra.userCrea = (string)reader["UserCrea"];
                                TipoCombustible combustible2 = new TipoCombustible
                                {
                                    idTipoCombustible = (int)reader["idTipoCombustible"]
                                };
                                extra.tipoCombustible = combustible2;
                                extra.fechaCombustible = DBNull.Value.Equals(reader["fechaCombustible"]) ? null : new DateTime?((DateTime)reader["fechaCombustible"]);
                                extra.ltCombustible = new decimal?(DBNull.Value.Equals(reader["ltCombustible"]) ? decimal.Zero : ((decimal)reader["ltCombustible"]));
                                extra.precioCombustible = new decimal?(DBNull.Value.Equals(reader["precioCombustible"]) ? decimal.Zero : ((decimal)reader["precioCombustible"]));
                                extra.importe = new decimal?(DBNull.Value.Equals(reader["importe"]) ? decimal.Zero : ((decimal)reader["importe"]));
                                Personal personal1 = new Personal
                                {
                                    clave = DBNull.Value.Equals(reader["idDespachador"]) ? 0 : ((int)reader["idDespachador"])
                                };
                                extra.despachador = personal1;
                                Personal personal2 = new Personal
                                {
                                    clave = (int)reader["idChofer"]
                                };
                                extra.chofer = personal2;
                                extra.idCargaCombustible = DBNull.Value.Equals(reader["idCargaCombustible"]) ? null : new int?((int)reader["idCargaCombustible"]);
                                extra.numViaje = DBNull.Value.Equals(reader["NumViaje"]) ? null : new int?((int)reader["NumViaje"]);
                                ListaExtra.Add(extra);
                            }
                        }
                        return new OperationResult(command, ListaExtra);
                    }
                }
            }
            catch (Exception exception)
            {
                return new OperationResult(exception);
            }
        }

        public OperationResult getCargaCombustibleExtraByOrdenServ(int idOrdenServ)
        {
            OperationResult result;
            DateTime? nullable = null;
            int? nullable2 = null;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getCargaCombustibleExtraByIdOrdenServ";
                        command.Parameters.Add(new SqlParameter("@id", idOrdenServ));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        CargaCombustibleExtra extra = new CargaCombustibleExtra();
                        connection.Open();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CargaCombustibleExtra extra1 = new CargaCombustibleExtra
                                {
                                    idCargaCombustibleExtra = (int)reader["idVCExtra"]
                                };
                                TipoOrdenCombustible combustible1 = new TipoOrdenCombustible
                                {
                                    idTipoOrdenCombustible = (int)reader["idTipoOSComb"],
                                    nombreTipoOrdenCombustible = (string)reader["NomTipoOSComb"],
                                    requiereViaje = !DBNull.Value.Equals(reader["RequiereViaje"]) && ((bool)reader["RequiereViaje"]),
                                    tipoUnidad = (string)reader["TipoUnidad"],
                                    operativo = (bool)reader["operativo"],
                                    activo = (bool)reader["activo"]
                                };
                                extra1.tipoOrdenCombustible = combustible1;
                                UnidadTransporte transporte1 = new UnidadTransporte
                                {
                                    clave = (string)reader["idUnidadTrans"]
                                };
                                extra1.unidadTransporte = transporte1;
                                extra1.fecha = (DateTime)reader["FechaCrea"];
                                extra1.userCrea = (string)reader["UserCrea"];
                                TipoCombustible combustible2 = new TipoCombustible
                                {
                                    idTipoCombustible = (int)reader["idTipoCombustible"]
                                };
                                extra1.tipoCombustible = combustible2;
                                extra1.fechaCombustible = DBNull.Value.Equals(reader["fechaCombustible"]) ? nullable : new DateTime?((DateTime)reader["fechaCombustible"]);
                                extra1.ltCombustible = new decimal?(DBNull.Value.Equals(reader["ltCombustible"]) ? decimal.Zero : ((decimal)reader["ltCombustible"]));
                                extra1.precioCombustible = new decimal?(DBNull.Value.Equals(reader["precioCombustible"]) ? decimal.Zero : ((decimal)reader["precioCombustible"]));
                                extra1.importe = new decimal?(DBNull.Value.Equals(reader["importe"]) ? decimal.Zero : ((decimal)reader["importe"]));
                                Personal personal1 = new Personal
                                {
                                    clave = DBNull.Value.Equals(reader["idDespachador"]) ? 0 : ((int)reader["idDespachador"])
                                };
                                extra1.despachador = personal1;
                                Personal personal2 = new Personal
                                {
                                    clave = (int)reader["idChofer"]
                                };
                                extra1.chofer = personal2;
                                extra1.idCargaCombustible = DBNull.Value.Equals(reader["idCargaCombustible"]) ? nullable2 : new int?((int)reader["idCargaCombustible"]);
                                extra1.numViaje = DBNull.Value.Equals(reader["NumViaje"]) ? nullable2 : new int?((int)reader["NumViaje"]);
                                extra = extra1;
                            }
                        }
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = extra
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            return result;
        }

        public OperationResult getCargasCom()
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getCargasCom", false))
                    {
                        connection.Open();
                        List<CargasCombustibleCOM> list = new List<CargasCombustibleCOM>();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CargasCombustibleCOM item = new CargasCombustibleCOM
                                {
                                    idCargaCombustible = (int)reader["idCargaCombustible"],
                                    fecha = (DateTime)reader["fecha"],
                                    empresa = (string)reader["NomBaseDatosCOM"],
                                    lts = (decimal)reader["lts"],
                                    extra = (int)reader["extra"]
                                };
                                list.Add(item);
                            }
                        }
                        result = new OperationResult(command, list);
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
            return result;
        }

        public OperationResult getDespachosCombustibleByFiltros(DateTime fechaInicio, DateTime fechaFin, int idTanque, string idEmpresa = "%")
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getDespachosCombustibleByFiltros", false))
                    {
                        command.Parameters.Add(new SqlParameter("@fechaInicial", fechaInicio));
                        command.Parameters.Add(new SqlParameter("@fechaFinal", fechaFin));
                        command.Parameters.Add(new SqlParameter("@idEmpresa", idEmpresa));
                        command.Parameters.Add(new SqlParameter("@idTanque", idTanque));
                        List<Despacho> list = new List<Despacho>();
                        connection.Open();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Despacho item = new Despacho
                                {
                                    fecha = (DateTime)reader["fecha"],
                                    kmECM = (decimal)reader["kmsECM"],
                                    ltsECM = (decimal)reader["ltsECM"],
                                    despachado = (decimal)reader["despachado"],
                                    noVale = (int)reader["noVale"],
                                    cliente = (string)reader["cliente"],
                                    unidad = (string)reader["unidad"],
                                    km = (decimal)reader["km"],
                                    operador = (string)reader["operador"],
                                    isExtra = Convert.ToBoolean(reader["isExtra"])
                                };
                                list.Add(item);
                            }
                        }
                        result = new OperationResult(command, list);
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult(exception);
            }
            return result;
        }

        public OperationResult getExistenciasDieselActual(int idTanque)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getExistenciasDieselActual", false))
                    {
                        command.Parameters.Add(new SqlParameter("@idTanque", idTanque));
                        Dictionary<string, decimal> dictionary = new Dictionary<string, decimal>();
                        connection.Open();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                dictionary.Add((string)reader["empresa"], (decimal)reader["existencia"]);
                            }
                        }
                        result = new OperationResult(command, dictionary);
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult(exception);
            }
            return result;
        }

        public OperationResult getFechas()
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getFechas";
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        List<DateTime> list = new List<DateTime>();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                list.Add((DateTime)reader["fecha"]);
                            }
                        }
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = list
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
            return result;
        }

        public OperationResult getLtsCombustibleByDia(DateTime fecha)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getLtsCombustibleByDia";
                        command.Parameters.Add(new SqlParameter("@fecha", fecha));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        List<CargaCombustibleByDia> list = new List<CargaCombustibleByDia>();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CargaCombustibleByDia item = new CargaCombustibleByDia
                                {
                                    lts = (decimal)reader["lts"],
                                    empresa = (string)reader["empresa"],
                                    idEmpresa = (int)reader["idEmpresa"]
                                };
                                list.Add(item);
                            }
                        }
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = list
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
            return result;
        }

        public OperationResult getProveedor(int idProveedor = 0)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText = "\r\nSELECT  idProveedor ,\r\n        nombre ,\r\n        precio\r\nFROM    dbo.proveedores\r\nWHERE   idProveedor LIKE CASE @idProveedor\r\n                           WHEN 0 THEN '%'\r\n                           ELSE CONVERT(NVARCHAR(MAX), @idProveedor)\r\n                         END; ";
                        command.Parameters.Add(new SqlParameter("@idProveedor", idProveedor));
                        connection.Open();
                        List<ProveedorCombustible> list = new List<ProveedorCombustible>();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                ProveedorCombustible item = new ProveedorCombustible
                                {
                                    idProveedor = (int)reader["idProveedor"],
                                    nombre = (string)reader["nombre"],
                                    precio = (decimal)reader["precio"]
                                };
                                list.Add(item);
                            }
                        }
                        if (idProveedor == 0)
                        {
                            return new OperationResult
                            {
                                valor = 0,
                                mensaje = "",
                                result = list
                            };
                        }
                        result = new OperationResult
                        {
                            valor = 0,
                            mensaje = "",
                            result = list[0]
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            return result;
        }

        public OperationResult getTickets()
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getTickets", false))
                    {
                        List<Tickets> list = new List<Tickets>();
                        connection.Open();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Tickets item = new Tickets
                                {
                                    fecha = (DateTime)reader["fecha"],
                                    unidadTransporte = (string)reader["unidadTransporte"],
                                    ordenServicio = (int)reader["ordenServicio"],
                                    ticket = (string)reader["ticket"],
                                    lts = (decimal)reader["lts"],
                                    folioImpreso = (string)reader["folioImpreso"]
                                };
                                list.Add(item);
                            }
                        }
                        result = new OperationResult(command, list);
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult(exception);
            }
            return result;
        }

        public OperationResult getTicketsByFechas(DateTime fechaInicial, DateTime fechaFinal)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getTicketsByFechas", false))
                    {
                        command.Parameters.Add(new SqlParameter("@fechaInicial", fechaInicial));
                        command.Parameters.Add(new SqlParameter("@fechaFinal", fechaFinal));
                        List<Tickets> list = new List<Tickets>();
                        connection.Open();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Tickets item = new Tickets
                                {
                                    fecha = (DateTime)reader["fecha"],
                                    unidadTransporte = (string)reader["unidadTransporte"],
                                    ordenServicio = (int)reader["ordenServicio"],
                                    ticket = (string)reader["ticket"],
                                    lts = (decimal)reader["lts"],
                                    folioImpreso = (string)reader["folioImpreso"]
                                };
                                list.Add(item);
                            }
                        }
                        result = new OperationResult(command, list);
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult(exception);
            }
            return result;
        }

        public OperationResult getTicketsByFechas(DateTime fechaInicial, DateTime fechaFinal, int? idEmpresa)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getTicketsByFechasAndIdEmpresa", false))
                    {
                        command.Parameters.Add(new SqlParameter("@fechaInicial", fechaInicial));
                        command.Parameters.Add(new SqlParameter("@fechaFinal", fechaFinal));
                        command.Parameters.Add(new SqlParameter("@idEmpresa", idEmpresa));
                        List<Tickets> list = new List<Tickets>();
                        connection.Open();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Tickets item = new Tickets
                                {
                                    fecha = (DateTime)reader["fecha"],
                                    unidadTransporte = (string)reader["unidadTransporte"],
                                    ordenServicio = (int)reader["ordenServicio"],
                                    ticket = (string)reader["ticket"],
                                    lts = (decimal)reader["lts"],
                                    folioImpreso = (string)reader["folioImpreso"]
                                };
                                list.Add(item);
                            }
                        }
                        result = new OperationResult(command, list);
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult(exception);
            }
            return result;
        }

        public OperationResult getTipoOrdenCombustible(int id = 0)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getTipoOrdenCombustible";
                        command.Parameters.Add(new SqlParameter("@id", id));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        List<TipoOrdenCombustible> list = new List<TipoOrdenCombustible>();
                        connection.Open();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                TipoOrdenCombustible item = new TipoOrdenCombustible
                                {
                                    idTipoOrdenCombustible = (int)reader["idTipoOSComb"],
                                    nombreTipoOrdenCombustible = (string)reader["NomTipoOSComb"],
                                    requiereViaje = !DBNull.Value.Equals(reader["RequiereViaje"]) && ((bool)reader["RequiereViaje"]),
                                    tipoUnidad = (string)reader["TipoUnidad"]
                                };
                                list.Add(item);
                            }
                        }
                        if (id != 0)
                        {
                            return new OperationResult
                            {
                                valor = new int?((int)parameter.Value),
                                mensaje = (string)parameter.Value,
                                result = list[0]
                            };
                        }
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter.Value,
                            result = list
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            return result;
        }

        public OperationResult getTipoOrdenCombustibleExtra()
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getTipoOrdenCombustibleExtra", false))
                    {
                        List<TipoOrdenCombustible> list = new List<TipoOrdenCombustible>();
                        connection.Open();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                TipoOrdenCombustible item = new mapComunes().mapTipoOrdenCombustible(reader);
                                if (item == null)
                                {
                                    return new OperationResult(ResultTypes.error, "Ocurrio un error al leer los resultados de la b\x00fasqueda", null);
                                }
                                list.Add(item);
                            }
                        }
                        result = new OperationResult(command, list);
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult(exception);
            }
            return result;
        }

        public OperationResult insertFechaCargaCombustible(DateTime fecha)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_insertFechaCargaCombustible";
                        command.Parameters.Add(new SqlParameter("@fecha", fecha));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        command.ExecuteNonQuery();
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
            return result;
        }

        public OperationResult saveCargaCombustibleEXtra(ref CargaCombustibleExtra carga, ref KardexCombustible kardexCombustible)
        {
            OperationResult result2;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    connection.Open();
                    using (SqlTransaction transaction = connection.BeginTransaction())
                    {
                        using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_saveCargaCombustibleExtra", true))
                        {
                            command.Transaction = transaction;
                            command.Parameters.Add(new SqlParameter("@idVCExtra", carga.idCargaCombustibleExtra));
                            command.Parameters.Add(new SqlParameter("@idTipoOSComb", carga.tipoOrdenCombustible.idTipoOrdenCombustible));
                            command.Parameters.Add(new SqlParameter("@idUnidadTrans", carga.unidadTransporte.clave));
                            command.Parameters.Add(new SqlParameter("@usuario", carga.userCrea));
                            command.Parameters.Add(new SqlParameter("@idChofer", carga.chofer.clave));
                            command.Parameters.Add(new SqlParameter("@numViaje", carga.numViaje));
                            command.Parameters.Add(new SqlParameter("@fecha", carga.fecha));
                            command.Parameters.Add(new SqlParameter("@fechaCombustible", carga.fechaCombustible));
                            command.Parameters.Add(new SqlParameter("@ltCombutible", carga.ltCombustible));
                            command.Parameters.Add(new SqlParameter("@precio", carga.precioCombustible));
                            command.Parameters.Add(new SqlParameter("@idDespachador", (carga.despachador == null) ? null : new int?(carga.despachador.clave)));
                            command.Parameters.Add(new SqlParameter("@externo", carga.externo));
                            command.Parameters.Add(new SqlParameter("@ticket", carga.ticket));
                            command.Parameters.Add(new SqlParameter("@folioImpreso", carga.folioImpreso));
                            command.Parameters.Add(new SqlParameter("@idProveedor", (carga.proveedor == null) ? null : new int?(carga.proveedor.idProveedor)));
                            command.ExecuteNonQuery();
                            int id = 0;
                            if (CrearSqlCommand.validarCorrecto(command, out id))
                            {
                                carga.idCargaCombustibleExtra = id;
                                if (kardexCombustible != null)
                                {
                                    OperationResult result = new KardexCombustibleServices().savekardexCombustible(ref kardexCombustible, false);
                                    if (result.typeResult > ResultTypes.success)
                                    {
                                        transaction.Rollback();
                                        return result;
                                    }
                                }
                                transaction.Commit();
                            }
                            else
                            {
                                transaction.Rollback();
                            }
                            result2 = new OperationResult(command, carga);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                result2 = new OperationResult(exception);
            }
            return result2;
        }

        public OperationResult saveTipoCombustibleEtra(ref TipoOrdenCombustible tipoOrden)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    connection.Open();
                    using (SqlTransaction transaction = connection.BeginTransaction())
                    {
                        using (SqlCommand command = CrearSqlCommand.getCommand(connection, "get_saveTipoOrdenCombustible", true))
                        {
                            command.Transaction = transaction;
                            command.Parameters.Add(new SqlParameter("@idTipoOS", tipoOrden.idTipoOrdenCombustible));
                            command.Parameters.Add(new SqlParameter("@descripcion", tipoOrden.nombreTipoOrdenCombustible));
                            command.Parameters.Add(new SqlParameter("@viaje", tipoOrden.requiereViaje));
                            command.Parameters.Add(new SqlParameter("@operativo", tipoOrden.operativo));
                            command.Parameters.Add(new SqlParameter("@activo", tipoOrden.activo));
                            command.ExecuteNonQuery();
                            int id = 0;
                            if (CrearSqlCommand.validarCorrecto(command, out id))
                            {
                                tipoOrden.idTipoOrdenCombustible = id;
                                transaction.Commit();
                            }
                            else
                            {
                                transaction.Rollback();
                            }
                            result = new OperationResult(command, null);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult(exception);
            }
            return result;
        }
        public OperationResult integracionValesCombustible(int idVale, List<CargaCombustibleExtra> listaCargasExtra, List<CargaTicketDiesel> listaCargasTickets)
        {
            try
            {
                string xml = GenerateXML.generarListaCadena(listaCargasExtra.Select(s => s.idCargaCombustibleExtra.ToString()).ToList());
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    con.Open();
                    using (SqlTransaction tran = con.BeginTransaction())
                    {
                        using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_ligarCargasExtraVale"))
                        {
                            comad.Transaction = tran;
                            comad.Parameters.Add(new SqlParameter("@idVale", idVale));
                            comad.Parameters.Add(new SqlParameter("@xml", xml));

                            comad.ExecuteNonQuery();
                            if (!CrearSqlCommand.validarCorrecto(comad))
                            {
                                tran.Rollback();
                                return new OperationResult(comad);
                            }

                            string cadenaTicket = string.Empty;
                            string cadenaFolioImpreso = string.Empty;
                            decimal sumaPrecio = 0;
                            decimal sumaLitros = 0;
                            int? idProducto = null;
                            decimal precio = 0;

                            if (listaCargasTickets.Count > 0)
                            {
                                foreach (var ticket in listaCargasTickets)
                                {
                                    cadenaTicket += ticket.ticket + ",";
                                    cadenaFolioImpreso += ticket.folioImpreso + ",";
                                    sumaPrecio += ticket.precio;
                                    sumaLitros += ticket.litros;
                                    comad.Parameters.Clear();
                                    comad.CommandText = "sp_insertTicketExternoByCargaTickets";
                                    foreach (var item in CrearSqlCommand.getPatametros())
                                    {
                                        comad.Parameters.Add(item);
                                    }
                                    comad.Parameters.Add(new SqlParameter("@folioImpreso", ticket.folioImpreso));
                                    comad.Parameters.Add(new SqlParameter("@idCargaCombustible", idVale));
                                    comad.Parameters.Add(new SqlParameter("@idEmpresa", ticket.empresa.clave));
                                    comad.Parameters.Add(new SqlParameter("@idZonaOperativa", ticket.zonaOperativa.idZonaOperativa));
                                    //comad.Parameters.Add(new SqlParameter("@idCliente", ticket.));
                                    comad.Parameters.Add(new SqlParameter("@idProveedor", ticket.proveedor.idProveedor));
                                    comad.Parameters.Add(new SqlParameter("@folioTickets", ticket.folioImpreso));
                                    comad.Parameters.Add(new SqlParameter("@litros", ticket.litros));
                                    comad.Parameters.Add(new SqlParameter("@precio", ticket.precio));
                                    comad.Parameters.Add(new SqlParameter("@IEPS", .3521));
                                    comad.Parameters.Add(new SqlParameter("@importe", ticket.importe));
                                    comad.Parameters.Add(new SqlParameter("@isExtra", true));
                                    comad.Parameters.Add(new SqlParameter("@idCargaTicketDiesel", ticket.clave));

                                    comad.ExecuteNonQuery();
                                    if (!CrearSqlCommand.validarCorrecto(comad))
                                    {
                                        tran.Rollback();
                                        return new OperationResult(comad);
                                    }
                                    idProducto = ticket.proveedor.idProveedor;
                                }
                                precio = sumaPrecio / listaCargasTickets.Count;
                                
                            }

                            comad.Parameters.Clear();
                            comad.CommandText = "sp_actualizarValeParaCargasExtras";
                            foreach (var item in CrearSqlCommand.getPatametros())
                            {
                                comad.Parameters.Add(item);
                            }
                            
                            comad.Parameters.Add(new SqlParameter("@idProveedor", idProducto));
                            comad.Parameters.Add(new SqlParameter("@ticket", cadenaTicket.Trim().Trim(',')));
                            comad.Parameters.Add(new SqlParameter("@litros", sumaLitros));
                            comad.Parameters.Add(new SqlParameter("@precio", precio));
                            comad.Parameters.Add(new SqlParameter("@folioImpreso", cadenaFolioImpreso.Trim().Trim(',')));
                            comad.Parameters.Add(new SqlParameter("@importe", precio * sumaLitros));
                            comad.Parameters.Add(new SqlParameter("@idVale", idVale));

                            comad.ExecuteNonQuery();
                            if (!CrearSqlCommand.validarCorrecto(comad))
                            {
                                tran.Rollback();
                                return new OperationResult(comad);
                            }

                            tran.Commit();
                            return new OperationResult(comad);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult desLigarCargasExtra(int idValeExtra)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    con.Open();
                    using (SqlTransaction tran = con.BeginTransaction())
                    {
                        using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_DesLigarCargasExtra"))
                        {
                            comad.Transaction = tran;
                            comad.Parameters.Add(new SqlParameter("@idValeExtra", idValeExtra));

                            comad.ExecuteNonQuery();
                            if (!CrearSqlCommand.validarCorrecto(comad))
                            {
                                tran.Rollback();
                                return new OperationResult(comad);
                            }
                            tran.Commit();
                            return new OperationResult(comad);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult desLigarCargasTicket(int idVale, int idCargaTicket, List<CargaTicketDiesel> listaCargasTickets)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    con.Open();
                    using (SqlTransaction tran = con.BeginTransaction())
                    {
                        using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_desligarCargaTicket"))
                        {
                            comad.Transaction = tran;
                            comad.Parameters.Add(new SqlParameter("@idCargaTicket", idCargaTicket));

                            comad.ExecuteNonQuery();
                            if (!CrearSqlCommand.validarCorrecto(comad))
                            {
                                tran.Rollback();
                                return new OperationResult(comad);
                            }

                            string cadenaTicket = string.Empty;
                            string cadenaFolioImpreso = string.Empty;
                            decimal sumaPrecio = 0;
                            decimal sumaLitros = 0;
                            int? idProducto = null;
                            decimal precio = 0;

                            if (listaCargasTickets.Count > 0)
                            {
                                foreach (var ticket in listaCargasTickets)
                                {
                                    cadenaTicket += ticket.ticket + ",";
                                    cadenaFolioImpreso += ticket.folioImpreso + ",";
                                    sumaPrecio += ticket.precio;
                                    sumaLitros += ticket.litros;
                                    comad.Parameters.Clear();
                                    comad.CommandText = "sp_insertTicketExternoByCargaTickets";
                                    foreach (var item in CrearSqlCommand.getPatametros())
                                    {
                                        comad.Parameters.Add(item);
                                    }
                                    comad.Parameters.Add(new SqlParameter("@folioImpreso", ticket.folioImpreso));
                                    comad.Parameters.Add(new SqlParameter("@idCargaCombustible", idVale));
                                    comad.Parameters.Add(new SqlParameter("@idEmpresa", ticket.empresa.clave));
                                    comad.Parameters.Add(new SqlParameter("@idZonaOperativa", ticket.zonaOperativa.idZonaOperativa));
                                    //comad.Parameters.Add(new SqlParameter("@idCliente", ticket.));
                                    comad.Parameters.Add(new SqlParameter("@idProveedor", ticket.proveedor.idProveedor));
                                    comad.Parameters.Add(new SqlParameter("@folioTickets", ticket.folioImpreso));
                                    comad.Parameters.Add(new SqlParameter("@litros", ticket.litros));
                                    comad.Parameters.Add(new SqlParameter("@precio", ticket.precio));
                                    comad.Parameters.Add(new SqlParameter("@IEPS", .3521));
                                    comad.Parameters.Add(new SqlParameter("@importe", ticket.importe));
                                    comad.Parameters.Add(new SqlParameter("@isExtra", true));
                                    comad.Parameters.Add(new SqlParameter("@idCargaTicketDiesel", ticket.clave));

                                    comad.ExecuteNonQuery();
                                    if (!CrearSqlCommand.validarCorrecto(comad))
                                    {
                                        tran.Rollback();
                                        return new OperationResult(comad);
                                    }
                                    idProducto = ticket.proveedor.idProveedor;
                                }
                                precio = sumaPrecio / listaCargasTickets.Count;

                            }

                            comad.Parameters.Clear();
                            comad.CommandText = "sp_actualizarValeParaCargasExtras";
                            foreach (var item in CrearSqlCommand.getPatametros())
                            {
                                comad.Parameters.Add(item);
                            }

                            comad.Parameters.Add(new SqlParameter("@idProveedor", idProducto));
                            comad.Parameters.Add(new SqlParameter("@ticket", cadenaTicket.Trim().Trim(',')));
                            comad.Parameters.Add(new SqlParameter("@litros", sumaLitros));
                            comad.Parameters.Add(new SqlParameter("@precio", precio));
                            comad.Parameters.Add(new SqlParameter("@folioImpreso", cadenaFolioImpreso.Trim().Trim(',')));
                            comad.Parameters.Add(new SqlParameter("@importe", precio * sumaLitros));
                            comad.Parameters.Add(new SqlParameter("@idVale", idVale));

                            comad.ExecuteNonQuery();
                            if (!CrearSqlCommand.validarCorrecto(comad))
                            {
                                tran.Rollback();
                                return new OperationResult(comad);
                            }

                            tran.Commit();
                            return new OperationResult(comad);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getUltimoCierreProcesoCombustible(int idTanque)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getCierreProcesoCombustibleByTanque"))
                    {
                        comad.Parameters.Add(new SqlParameter("@idTanque", idTanque));
                        CierreProcesoCombustible cierre = new CierreProcesoCombustible();
                        con.Open();
                        using (SqlDataReader reader = comad.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                cierre = new mapComunes().mapCierreProcesoCombustible(reader);
                                if (cierre == null) return new OperationResult(ResultTypes.error, "ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");
                                
                            }
                        }
                        return new OperationResult(comad, cierre);
                    }                    
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getValesCargaByViaje(string idUnidad, DateTime fecha)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getValesCargaByViaje"))
                    {
                        comad.Parameters.Add(new SqlParameter("@idUnidad", idUnidad));
                        comad.Parameters.Add(new SqlParameter("@fecha", fecha));
                        List<ResumenCargaCombustible> listaResumen = new List<ResumenCargaCombustible>();
                        con.Open();
                        using (SqlDataReader sqlReader = comad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                ResumenCargaCombustible resumen = new mapComunes().mapResumenCarga(sqlReader);
                                if (resumen == null) return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");
                                listaResumen.Add(resumen);
                            }
                        }
                        return new OperationResult(comad, listaResumen);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
}
