﻿namespace CoreFletera.BusinessLogic
{
    using Core.Models;
    using Core.Utils;
    using CoreFletera.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Runtime.CompilerServices;

    public class RequisicionCompraServices
    {
        private string strConsultaComplemento = "\r\nBEGIN TRY\r\n    SELECT  compra.CIDDOCUMENTO AS idCompra ,\r\n            ( compra.CNETO + compra.CIMPUESTO1 ) AS importeCompra ,\r\n            ( compra.CSERIEDOCUMENTO + CAST(compra.CFOLIO AS NVARCHAR(MAX)) ) AS folioCompra ,\r\n            ( avisoEnt.CSERIEDOCUMENTO\r\n              + CAST(avisoEnt.CFOLIO AS NVARCHAR(MAX)) ) AS folioAvisoEntrada ,\r\n            avisoEnt.CFECHA AS fechaEntrada ,\r\n            ( factProv.CSERIEDOCUMENTO\r\n              + CAST(factProv.CFOLIO AS NVARCHAR(MAX)) ) AS folioFactProv ,\r\n            factProv.CFECHA AS fechaFactProv ,\r\n            ( factProv.CNETO + factProv.CIMPUESTO1 ) AS importe ,\r\n            factProv.CPENDIENTE AS saldoPendiente\r\n    FROM    dbo.admDocumentos AS compra\r\n            LEFT JOIN dbo.admDocumentos AS avisoEnt ON avisoEnt.CIDDOCUMENTOORIGEN = compra.CIDDOCUMENTO\r\n            LEFT JOIN dbo.admDocumentos AS factProv ON factProv.CIDDOCUMENTOORIGEN = avisoEnt.CIDDOCUMENTO\r\n            LEFT JOIN dbo.admDocumentos AS pago ON pago.CIDDOCUMENTOORIGEN = factProv.CIDDOCUMENTO\r\n    WHERE   compra.CIDCONCEPTODOCUMENTO = 19\r\n            AND compra.CIDDOCUMENTO IN ( {0} );     \r\n    IF @@ROWCOUNT > 0\r\n        BEGIN\r\n            SET @valor = 0;\r\n            SET @mensaje = 'registros comercial encontrados';\r\n        END;\r\n    ELSE\r\n        BEGIN\r\n            SET @valor = 3;\r\n            SET @mensaje = 'registros comercial no encontrados';\r\n        END;    \r\nEND TRY\r\nBEGIN CATCH\r\n    SET @valor = 1;\r\n    SET @mensaje = ERROR_MESSAGE();\r\nEND CATCH;\r\n";

        public OperationResult getComplementoRequisicionCompra(string cadenaIdsCompra)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactoryTaller(false))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommandText(connection, string.Format(this.strConsultaComplemento, cadenaIdsCompra), false))
                    {
                        connection.Open();
                        List<ComplementoRequisicionCompra> list = new List<ComplementoRequisicionCompra>();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                DateTime? nullable;
                                decimal? nullable2;
                                ComplementoRequisicionCompra item = new ComplementoRequisicionCompra
                                {
                                    idCompra = (int)reader["idCompra"],
                                    importeCompra = Convert.ToDecimal(reader["importeCompra"]),
                                    avisoEntrada = DBNull.Value.Equals(reader["folioAvisoEntrada"]) ? string.Empty : ((string)reader["folioAvisoEntrada"]),
                                    fechaAvisoEntrada = DBNull.Value.Equals(reader["fechaEntrada"]) ? ((DateTime?)(nullable = null)) : ((DateTime?)reader["fechaEntrada"]),
                                    folioFacProv = DBNull.Value.Equals(reader["folioFactProv"]) ? string.Empty : ((string)reader["folioFactProv"]),
                                    fechaFolioFacProv = DBNull.Value.Equals(reader["fechaFactProv"]) ? ((DateTime?)(nullable = null)) : ((DateTime?)reader["fechaFactProv"]),
                                    importe = DBNull.Value.Equals(reader["importe"]) ? ((decimal?)(nullable2 = null)) : new decimal?(Convert.ToDecimal(reader["importe"])),
                                    saldo = DBNull.Value.Equals(reader["saldoPendiente"]) ? ((decimal?)(nullable2 = null)) : new decimal?(Convert.ToDecimal(reader["saldoPendiente"]))
                                };
                                list.Add(item);
                            }
                        }
                        result = new OperationResult(command, list);
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult(exception);
            }
            return result;
        }

        public OperationResult getRequisicioCompraByFecha(DateTime fechaInicial, DateTime fechaFinal)
        {
            OperationResult result2;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getRequisicioCompraByFecha", false))
                    {
                        command.Parameters.Add(new SqlParameter("@fechaInicial", fechaInicial));
                        command.Parameters.Add(new SqlParameter("@fechaFinal", fechaFinal));
                        connection.Open();
                        List<RequisicionCompra> list = new List<RequisicionCompra>();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                int? nullable;
                                DateTime? nullable2;
                                RequisicionCompra item = new RequisicionCompra
                                {
                                    idOrdenServ = DBNull.Value.Equals(reader["idOrdenServ"]) ? ((int?)(nullable = null)) : ((int?)reader["idOrdenServ"]),
                                    fechaOrdenServ = DBNull.Value.Equals(reader["fechaOrdenServ"]) ? ((DateTime?)(nullable2 = null)) : ((DateTime?)reader["fechaOrdenServ"]),
                                    idUnidadTrans = DBNull.Value.Equals(reader["idUnidadTrans"]) ? string.Empty : ((string)reader["idUnidadTrans"]),
                                    idPreOC = (int)reader["idPreOC"],
                                    fechaPreOC = (DateTime)reader["fechaPreOC"],
                                    idSolicitud = DBNull.Value.Equals(reader["idSolicitud"]) ? ((int?)(nullable = null)) : ((int?)reader["idSolicitud"]),
                                    FechaSolicitud = DBNull.Value.Equals(reader["FechaSolicitud"]) ? ((DateTime?)(nullable2 = null)) : ((DateTime?)reader["FechaSolicitud"]),
                                    idCotizacion = DBNull.Value.Equals(reader["idCotizacion"]) ? ((int?)(nullable = null)) : ((int?)reader["idCotizacion"]),
                                    fechaCotizacion = DBNull.Value.Equals(reader["fechaCotizacion"]) ? ((DateTime?)(nullable2 = null)) : ((DateTime?)reader["fechaCotizacion"]),
                                    idCompra = DBNull.Value.Equals(reader["idCompra"]) ? ((int?)(nullable = null)) : ((int?)reader["idCompra"]),
                                    ordenCompra = DBNull.Value.Equals(reader["ordenCompra"]) ? string.Empty : ((string)reader["ordenCompra"]),
                                    fechaOrdenCompra = DBNull.Value.Equals(reader["fechaOrdenCompra"]) ? ((DateTime?)(nullable2 = null)) : ((DateTime?)reader["fechaOrdenCompra"])
                                };
                                list.Add(item);
                            }
                        }
                        string str = string.Empty;
                        foreach (int? nullable3 in from s in list select s.idCompra)
                        {
                            if (nullable3.HasValue)
                            {
                                str = str + nullable3.Value + ",";
                            }
                        }
                        char[] trimChars = new char[] { ',' };
                        OperationResult result = this.getComplementoRequisicionCompra(str.Trim().Trim(trimChars));
                        if (result.typeResult == ResultTypes.error)
                        {
                            return result;
                        }
                        if (result.typeResult == ResultTypes.success)
                        {
                            List<ComplementoRequisicionCompra> list2 = result.result as List<ComplementoRequisicionCompra>;
                            foreach (RequisicionCompra item in list)
                            {
                                item.complemento = list2.Find(delegate (ComplementoRequisicionCompra s)
                                {
                                    int? idCompra = item.idCompra;
                                    return (s.idCompra == idCompra.GetValueOrDefault()) ? idCompra.HasValue : false;
                                });
                            }
                        }
                        result2 = new OperationResult(command, list);
                    }
                }
            }
            catch (Exception exception)
            {
                result2 = new OperationResult(exception);
            }
            return result2;
        }
    }
}
