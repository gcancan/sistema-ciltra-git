﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using Core.Utils;
using Core.BusinessLogic;
using System.Data;
using System.Data.SqlClient;
namespace CoreFletera.BusinessLogic
{
    public class OrdenesTrabajoServices
    {
        public OperationResult getRelacionServicios(int idTipoOrden)
        {
            try
            {
                using (SqlConnection conection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comand = conection.CreateCommand())
                    {
                        comand.CommandType = CommandType.StoredProcedure;

                        //comand.CommandText = "sp_relacionServicios";
                        comand.CommandText = "sp_getAllServiciosByTipoOrden";

                        comand.Parameters.Add(new SqlParameter("@idTipoOrden", idTipoOrden));
                        SqlParameter spValor = comand.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        SqlParameter spMensaje = comand.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        conection.Open();
                        List<Servicio> listaServicios = new List<Servicio>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                var det = new mapComunes().mapServicio(sqlReader);
                                if (det == null)
                                {
                                    return new OperationResult { valor = 1, mensaje = "Ocurrio un error al obtener datos del servicio" };
                                }
                                listaServicios.Add(det);
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = listaServicios };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }
        public OperationResult getAllServicios()
        {
            try
            {
                using (SqlConnection conection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comand = conection.CreateCommand())
                    {
                        comand.CommandType = CommandType.StoredProcedure;
                        comand.CommandText = "sp_getAllServiciosV2";

                        SqlParameter spValor = comand.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        SqlParameter spMensaje = comand.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        conection.Open();
                        List<Servicio> listaServicios = new List<Servicio>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                var det = new mapComunes().mapServicio(sqlReader);
                                if (det == null)
                                {
                                    return new OperationResult { valor = 1, mensaje = "Ocurrio un error al obtener datos del servicio" };
                                }
                                listaServicios.Add(det);
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = listaServicios };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }

        public OperationResult getTipoDeOrden(int idTipoOrdenServ)
        {
            try
            {
                using (SqlConnection conection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comand = conection.CreateCommand())
                    {
                        comand.CommandType = CommandType.StoredProcedure;
                        comand.CommandText = "sp_getTipoDeOrden";

                        comand.Parameters.Add(new SqlParameter("@idTipoOrden", idTipoOrdenServ));

                        SqlParameter spValor = comand.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        SqlParameter spMensaje = comand.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        conection.Open();
                        List<TipoOrden> listaTipoOrden = new List<TipoOrden>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                var det = new TipoOrden()
                                {
                                    idTipoOrden = (int)sqlReader["idTipoOrden"],
                                    nombreTipoOrden = (string)sqlReader["NomTipoOrden"]
                                };
                                listaTipoOrden.Add(det);
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = listaTipoOrden };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }

        public OperationResult getRelacionActividad(int idTipoOrden)
        {
            try
            {
                using (SqlConnection conection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comand = conection.CreateCommand())
                    {
                        comand.CommandType = CommandType.StoredProcedure;
                        comand.CommandText = "sp_relacionActividades";
                        comand.Parameters.Add(new SqlParameter("@idTipoOrden", idTipoOrden));
                        SqlParameter spValor = comand.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        SqlParameter spMensaje = comand.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        conection.Open();
                        List<Actividad> listaActividad = new List<Actividad>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                Actividad det = new Actividad
                                {
                                    CIDPRODUCTO = (int)sqlReader["CIDPRODUCTO"],
                                    CCODIGOPRODUCTO = (string)sqlReader["CCODIGOPRODUCTO"],
                                    idActividad = (int)sqlReader["idActividad"],
                                    nombre = (string)sqlReader["NombreAct"],
                                    costo = (decimal)sqlReader["costo"],
                                    duracion = (decimal)sqlReader["duracion"],
                                    idDivicion = (int)sqlReader["idDivicion"],
                                    taller = (bool)sqlReader["taller"],
                                    lavadero = (bool)sqlReader["lavadero"],
                                    llantera = (bool)sqlReader["llantera"] 
                                };
                                listaActividad.Add(det);
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = listaActividad };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }

        public OperationResult getAllActividad()
        {
            try
            {
                using (SqlConnection conection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comand = conection.CreateCommand())
                    {
                        comand.CommandType = CommandType.StoredProcedure;
                        comand.CommandText = "sp_getAllActividadesV2";

                        SqlParameter spValor = comand.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        SqlParameter spMensaje = comand.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        conection.Open();
                        List<Actividad> listaActividad = new List<Actividad>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                Actividad det = new Actividad
                                {
                                    idActividad = (int)sqlReader["idActividad"],
                                    nombre = (string)sqlReader["NombreAct"],
                                    costo = (decimal)sqlReader["costo"],
                                    duracion = (decimal)sqlReader["duracion"]
                                    //idDivicion = (int)sqlReader["idDivicion"]
                                };
                                listaActividad.Add(det);
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = listaActividad };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }

        public OperationResult getDivisiones()
        {
            try
            {
                using (SqlConnection conection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comand = conection.CreateCommand())
                    {
                        comand.CommandType = CommandType.Text;
                        comand.CommandText = "SELECT * FROM dbo.CatDivision";
                        conection.Open();
                        List<Division> listaDivision = new List<Division>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                Division det = new Division
                                {
                                    idDivision = (int)sqlReader["idDivision"],
                                    nombre = (string)sqlReader["NomDivision"],

                                };
                                listaDivision.Add(det);
                            }
                        }
                        return new OperationResult { valor = 0, mensaje = "", result = listaDivision };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }

        public OperationResult getListaActividadesByIdServicio(int idSercicio)
        {
            try
            {
                using (SqlConnection conection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comand = conection.CreateCommand())
                    {
                        comand.CommandType = CommandType.StoredProcedure;
                        comand.CommandText = "sp_getActividadesByIdServ";
                        comand.Parameters.Add(new SqlParameter("@idServicio", idSercicio));
                        SqlParameter spValor = comand.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        SqlParameter spMensaje = comand.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        conection.Open();
                        List<Actividad> listaActividad = new List<Actividad>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                Actividad det = new Actividad
                                {
                                    idActividad = (int)sqlReader["idActividad"],
                                    nombre = (string)sqlReader["NombreAct"],
                                    costo = (decimal)sqlReader["costo"],
                                    duracion = (decimal)sqlReader["duracion"]
                                };
                                listaActividad.Add(det);
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = listaActividad };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }

        public OperationResult saveDetOrdenServ(List<DetOrdenServicio> list)
        {
            try
            {
                //string xml = GenerateXML.generarListaDetOrdenServ(list);
                //if (string.IsNullOrEmpty(xml))
                //{
                //    return new OperationResult { valor = 1, mensaje = "ERROR AL CONSTRUIR EL XML" }; 
                //}
                using (SqlConnection conection = BoundedContextFactory.ConnectionFactory())
                {
                    conection.Open();
                    using (SqlTransaction tran = conection.BeginTransaction())
                    {
                        //using (SqlCommand comand = conection.CreateCommand())
                        //{
                        //    comand.CommandType = CommandType.StoredProcedure;
                        //    comand.CommandText = "sp_saveDetOrdenServ";
                        //    comand.Parameters.Add(new SqlParameter("@xml", xml));
                        //    SqlParameter spValor = comand.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        //    SqlParameter spMensaje = comand.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        //    conection.Open();
                        //    comand.ExecuteNonQuery();
                        //    return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value };
                        //}
                        using (SqlCommand command = CrearSqlCommand.getCommand(conection, "sp_saveDetOrdenServ"))
                        {
                            command.Transaction = tran;
                            foreach (DetOrdenServicio det in list)
                            {
                                command.Parameters.Clear();
                                foreach (var item in CrearSqlCommand.getPatametros())
                                {
                                    command.Parameters.Add(item);
                                }
                                command.Parameters.Add(new SqlParameter("@idOrdenServicio", det.tipoOrdenServicio.idOrdenSer));
                                command.Parameters.Add(new SqlParameter("@idServicio", det.servicio == null ? 0 : det.servicio.idServicio));
                                command.Parameters.Add(new SqlParameter("@idActividad", det.actividad == null ? 0 : det.actividad.idActividad));
                                command.Parameters.Add(new SqlParameter("@estatus", det.estatus));
                                command.Parameters.Add(new SqlParameter("@idOrdenTrabajo", det.idOrdenTrabajo));
                                command.Parameters.Add(new SqlParameter("@idDivision", det.division.idDivision));
                                command.Parameters.Add(new SqlParameter("@idLlanta", det.llanta == null ? "" : det.llanta.clave));
                                command.Parameters.Add(new SqlParameter("@posicionLlanta", det.llanta == null ? 0 : det.llanta.posicion));
                                command.Parameters.Add(new SqlParameter("@notaRecep", det.descripcion));
                                command.Parameters.Add(new SqlParameter("@costoActividad", det.costoActividad));
                                command.Parameters.Add(new SqlParameter("@responsable", det.responsable == null ? 0 : det.responsable.clave));
                                command.Parameters.Add(new SqlParameter("@auxiliar1", det.auxiliar1 == null ? 0 : det.auxiliar1.clave));
                                command.Parameters.Add(new SqlParameter("@auxiliar2", det.auxiliar2 == null ? 0 : det.auxiliar2.clave));

                                command.ExecuteNonQuery();
                                if ((int)command.Parameters["@valor"].Value != 0)
                                {
                                    tran.Rollback();
                                    return new OperationResult(command);
                                }
                                if (det.listaActividades != null)
                                {
                                    if (det.listaActividades.Count > 0)
                                    {
                                        List<DetOrdenServicio> newlist = new List<DetOrdenServicio>();
                                        foreach (var item in det.listaActividades)
                                        {
                                            DetOrdenServicio detNew = new DetOrdenServicio()
                                            {
                                                actividad = item,
                                                descripcion = det.descripcion,
                                                division = item.division,
                                                idOrdenTrabajo = 0,
                                                estatus = det.estatus,
                                                servicio = null,
                                                tipoOrdenServicio = det.tipoOrdenServicio,
                                                listaActividades = null,
                                                llanta = null,
                                                tipoOrden = det.tipoOrden
                                            };

                                            command.Parameters.Clear();
                                            foreach (var itemP in CrearSqlCommand.getPatametros())
                                            {
                                                command.Parameters.Add(itemP);
                                            }
                                            command.Parameters.Add(new SqlParameter("@idOrdenServicio", det.tipoOrdenServicio.idOrdenSer));
                                            command.Parameters.Add(new SqlParameter("@idServicio", detNew.servicio == null ? 0 : detNew.servicio.idServicio));
                                            command.Parameters.Add(new SqlParameter("@idActividad", detNew.actividad == null ? 0 : detNew.actividad.idActividad));
                                            command.Parameters.Add(new SqlParameter("@estatus", detNew.estatus));
                                            command.Parameters.Add(new SqlParameter("@idOrdenTrabajo", 0));
                                            command.Parameters.Add(new SqlParameter("@idDivision", detNew.division.idDivision));
                                            command.Parameters.Add(new SqlParameter("@idLlanta", detNew.llanta == null ? "" : detNew.llanta.clave));
                                            command.Parameters.Add(new SqlParameter("@posicionLlanta", detNew.llanta == null ? 0 : detNew.llanta.posicion));
                                            command.Parameters.Add(new SqlParameter("@notaRecep", detNew.descripcion));
                                            command.Parameters.Add(new SqlParameter("@costoActividad", detNew.costoActividad));

                                            command.ExecuteNonQuery();
                                            if ((int)command.Parameters["@valor"].Value != 0)
                                            {
                                                tran.Rollback();
                                                return new OperationResult(command);
                                            }
                                        }
                                    }
                                }
                            }
                            tran.Commit();
                            return new OperationResult(command);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }

        public OperationResult saveRelServAct(Servicio servicio, List<Actividad> listActividad, bool activo = true)
        {
            try
            {
                using (SqlConnection conection = BoundedContextFactory.ConnectionFactory())
                {
                    conection.Open();
                    using (SqlTransaction trans = conection.BeginTransaction("save"))
                    {
                        using (SqlCommand comand = conection.CreateCommand())
                        {
                            comand.Transaction = trans;
                            comand.CommandType = CommandType.StoredProcedure;
                            comand.CommandText = "sp_saveRelServAct";

                            SqlParameter spValor = comand.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                            SqlParameter spMensaje = comand.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                            foreach (var item in listActividad)
                            {
                                comand.Parameters.Clear();
                                comand.Parameters.Add(spValor);
                                comand.Parameters.Add(spMensaje);

                                comand.Parameters.Add(new SqlParameter("@idServicio", servicio.idServicio));
                                comand.Parameters.Add(new SqlParameter("@idActividad", item.idActividad));
                                comand.Parameters.Add(new SqlParameter("@estatus", activo));

                                comand.ExecuteNonQuery();

                                if ((int)spValor.Value != 0)
                                {
                                    trans.Rollback("save");
                                    return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value };
                                }
                            }
                            trans.Commit();
                            return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value };
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }

        public OperationResult enviarTallerExterno(int idOrdenServicio)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    con.Open();
                    using (SqlTransaction tran = con.BeginTransaction())
                    {
                        using (SqlCommand command = CrearSqlCommand.getCommand(con, "sp_enviarTallerExterno"))
                        {
                            command.Transaction = tran;
                            command.Parameters.Add(new SqlParameter("@idOrden", idOrdenServicio));
                            command.ExecuteNonQuery();
                            if ((int)command.Parameters["@valor"].Value == 0)
                            {
                                tran.Commit();
                            }
                            else
                            {
                                tran.Rollback();
                            }
                            return new OperationResult(command);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
}
