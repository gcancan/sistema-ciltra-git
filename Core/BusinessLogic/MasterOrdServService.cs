﻿using Core.Models;
using Core.Utils;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.BusinessLogic
{
    public class MasterOrdServService
    {
        public OperationResult getMasterOrdServ(int folio)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getMasterOrdServ";

                        command.Parameters.Add(new SqlParameter("@folio", folio));

                        SqlParameter valor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        SqlParameter mensaje = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                        connection.Open();
                        var i = command.ExecuteReader();
                        MasterOrdServ masterOrdServ = new MasterOrdServ();
                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                masterOrdServ.idPadreOrdSer = (int)sqlReader["idPadreOrdSer"];
                                var res = new UnidadTransporteServices().getUnidadesALLorById(0, (string)sqlReader["idTractor"]);
                                if (res.typeResult != ResultTypes.success)
                                {
                                    return new OperationResult { valor = res.valor, mensaje = "Error al buscar el tractor:" + Environment.NewLine + res.mensaje, result = null };
                                }
                                else
                                {
                                    masterOrdServ.tractor = ((List<UnidadTransporte>)res.result)[0];
                                }

                                if (!string.IsNullOrEmpty((string)sqlReader["idTolva1"]))
                                {
                                    res = new UnidadTransporteServices().getUnidadesALLorById(0, (string)sqlReader["idTolva1"]);
                                    if (res.typeResult != ResultTypes.success)
                                    {
                                        return new OperationResult { valor = res.valor, mensaje = "Error al buscar la tolva 1:" + Environment.NewLine + res.mensaje, result = null };
                                    }
                                    else
                                    {
                                        masterOrdServ.tolva1 = ((List<UnidadTransporte>)res.result)[0];
                                    }
                                }

                                if (!string.IsNullOrEmpty(sqlReader["idDolly"].ToString().Trim()))
                                {
                                    res = new UnidadTransporteServices().getUnidadesALLorById(0, (string)sqlReader["idDolly"]);
                                    if (res.typeResult != ResultTypes.success)
                                    {
                                        return new OperationResult { valor = res.valor, mensaje = "Error al buscar el Dolly" + Environment.NewLine + res.mensaje, result = null };
                                    }
                                    else
                                    {
                                        masterOrdServ.dolly = ((List<UnidadTransporte>)res.result)[0];
                                    }
                                }
                                else
                                {
                                    masterOrdServ.dolly = null;
                                }

                                if (!string.IsNullOrEmpty(sqlReader["idTolva2"].ToString().Trim()))
                                {
                                    res = new UnidadTransporteServices().getUnidadesALLorById(0, (string)sqlReader["idTolva2"]);
                                    if (res.typeResult != ResultTypes.success)
                                    {
                                        return new OperationResult { valor = res.valor, mensaje = "Error al buscar la tolva 2:" + Environment.NewLine + res.mensaje, result = null };
                                    }
                                    else
                                    {
                                        masterOrdServ.tolva2 = ((List<UnidadTransporte>)res.result)[0];
                                    }
                                }
                                else
                                {
                                    masterOrdServ.tolva2 = null;
                                }

                                var chofer = new mapComunes().mapPersonal(sqlReader);
                                if (chofer != null)
                                {
                                    masterOrdServ.chofer = chofer;
                                }
                                else
                                {
                                    return new OperationResult { valor = 1, mensaje = "Error al buscar el chofer:", result = null };
                                }

                                masterOrdServ.fechaCreacion = (DateTime)sqlReader["FechaCreacion"];

                                masterOrdServ.listTiposOrdenServicio = new List<TipoOrdenServicio>();
                                var r = new TipoOrdenServicioServices().getOrdenServByIdPadreOrdSer((int)sqlReader["idPadreOrdSer"]);
                                if (r.typeResult == ResultTypes.success)
                                {
                                    masterOrdServ.listTiposOrdenServicio = ((List<TipoOrdenServicio>)r.result);
                                }
                                else
                                {
                                    return r;
                                }
                            }

                        }

                        return new OperationResult { valor = (int)valor.Value, mensaje = (string)mensaje.Value, result = masterOrdServ };
                    }
                }
            }
            catch (Exception e)
            {
                return new OperationResult { valor = 1, mensaje = e.Message, result = null };
            }
        }

        public OperationResult getMasterOrdServByCamion(string idTractor)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getMasterOrdServByCamion";

                        command.Parameters.Add(new SqlParameter("@idTractor", idTractor));

                        SqlParameter valor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        SqlParameter mensaje = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                        connection.Open();
                        var i = command.ExecuteReader();
                        MasterOrdServ masterOrdServ = new MasterOrdServ();
                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                masterOrdServ.idPadreOrdSer = (int)sqlReader["idPadreOrdSer"];
                                var res = new UnidadTransporteServices().getUnidadesALLorById(0, (string)sqlReader["idTractor"]);
                                if (res.typeResult != ResultTypes.success)
                                {
                                    return new OperationResult { valor = res.valor, mensaje = "Error al buscar el tractor:" + Environment.NewLine + res.mensaje, result = null };
                                }
                                else
                                {
                                    masterOrdServ.tractor = ((List<UnidadTransporte>)res.result)[0];
                                }

                                if (!string.IsNullOrEmpty((string)sqlReader["idTolva1"]))
                                {
                                    res = new UnidadTransporteServices().getUnidadesALLorById(0, (string)sqlReader["idTolva1"]);
                                    if (res.typeResult != ResultTypes.success)
                                    {
                                        return new OperationResult { valor = res.valor, mensaje = "Error al buscar la tolva 1:" + Environment.NewLine + res.mensaje, result = null };
                                    }
                                    else
                                    {
                                        masterOrdServ.tolva1 = ((List<UnidadTransporte>)res.result)[0];
                                    }
                                }

                                if (!string.IsNullOrEmpty(sqlReader["idDolly"].ToString().Trim()))
                                {
                                    res = new UnidadTransporteServices().getUnidadesALLorById(0, (string)sqlReader["idDolly"]);
                                    if (res.typeResult != ResultTypes.success)
                                    {
                                        return new OperationResult { valor = res.valor, mensaje = "Error al buscar el Dolly" + Environment.NewLine + res.mensaje, result = null };
                                    }
                                    else
                                    {
                                        masterOrdServ.dolly = ((List<UnidadTransporte>)res.result)[0];
                                    }
                                }

                                if (!string.IsNullOrEmpty(sqlReader["idTolva2"].ToString().Trim()))
                                {
                                    res = new UnidadTransporteServices().getUnidadesALLorById(0, (string)sqlReader["idTolva2"]);
                                    if (res.typeResult != ResultTypes.success)
                                    {
                                        return new OperationResult { valor = res.valor, mensaje = "Error al buscar la tolva 2:" + Environment.NewLine + res.mensaje, result = null };
                                    }
                                    else
                                    {
                                        masterOrdServ.tolva2 = ((List<UnidadTransporte>)res.result)[0];
                                    }
                                }

                                var chofer = new mapComunes().mapPersonal(sqlReader);
                                if (chofer != null)
                                {
                                    masterOrdServ.chofer = chofer;
                                }
                                else
                                {
                                    return new OperationResult { valor = 1, mensaje = "Error al buscar el chofer:", result = null };
                                }

                                masterOrdServ.fechaCreacion = (DateTime)sqlReader["FechaCreacion"];

                                masterOrdServ.listTiposOrdenServicio = new List<TipoOrdenServicio>();
                                var r = new TipoOrdenServicioServices().getOrdenServByIdPadreOrdSer((int)sqlReader["idPadreOrdSer"]);
                                if (r.typeResult == ResultTypes.success)
                                {
                                    masterOrdServ.listTiposOrdenServicio = ((List<TipoOrdenServicio>)r.result);
                                }
                                else
                                {
                                    return r;
                                }
                            }

                        }

                        return new OperationResult { valor = (int)valor.Value, mensaje = (string)mensaje.Value, result = masterOrdServ };
                    }
                }
            }
            catch (Exception e)
            {
                return new OperationResult { valor = 1, mensaje = e.Message, result = null };
            }
        }

        public OperationResult getActividadesByOrdenSer(int folio)
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand commad = connection.CreateCommand())
                    {
                        commad.CommandType = CommandType.StoredProcedure;
                        commad.CommandText = "sp_getActividadesByOrdenSer";

                        commad.Parameters.Add(new SqlParameter("@folio", folio));
                        var spValor = commad.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var spMensaje = commad.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        connection.Open();
                        List<Actividad> listActividad = new List<Actividad>();
                        using (SqlDataReader sqlReader = commad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                Actividad actividad = new Actividad
                                {
                                    idActividad = (int)sqlReader["idActividad"],
                                    nombre = (string)sqlReader["NombreAct"],
                                    costo = DBNull.Value.Equals(sqlReader["CostoManoObra"]) ? 0 : (decimal)sqlReader["CostoManoObra"],
                                    duracion = Convert.ToDecimal((string)sqlReader["DuracionHoras"]),
                                    estatus = (string)sqlReader["Estatus"]
                                };

                                if ((int)sqlReader["idPersonal1"] != 0)
                                {
                                    var resp = new OperadoresServices().getPersonalALLorById((int)sqlReader["idPersonal1"]);
                                    if (resp.typeResult == ResultTypes.success)
                                    {
                                        actividad.personal1 = ((List<Personal>)resp.result)[0];
                                    }
                                    else
                                    {
                                        actividad.personal1 = null;
                                    }
                                }
                                else
                                {
                                    actividad.personal1 = null;
                                }

                                if ((int)sqlReader["idPersonal2"] != 0)
                                {
                                    var resp = new OperadoresServices().getPersonalALLorById((int)sqlReader["idPersonal2"]);
                                    if (resp.typeResult == ResultTypes.success)
                                    {
                                        actividad.personal2 = ((List<Personal>)resp.result)[0];
                                    }
                                    else
                                    {
                                        actividad.personal2 = null;
                                    }
                                }
                                else
                                {
                                    actividad.personal2 = null;
                                }
                                if ((int)sqlReader["idPersonal3"] != 0)
                                {
                                    var resp = new OperadoresServices().getPersonalALLorById((int)sqlReader["idPersonal3"]);
                                    if (resp.typeResult == ResultTypes.success)
                                    {
                                        actividad.personal3 = ((List<Personal>)resp.result)[0];
                                    }
                                    else
                                    {
                                        actividad.personal3 = null;
                                    }
                                }
                                else
                                {
                                    actividad.personal3 = null;
                                }

                                listActividad.Add(actividad);
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = listActividad };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, result = ex.Message };
            }
        }

        public OperationResult getTrabajosByOrdenSer(int folio)
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand commad = connection.CreateCommand())
                    {
                        commad.CommandType = CommandType.StoredProcedure;
                        commad.CommandText = "sp_getTrabajosByOrdenSer";

                        commad.Parameters.Add(new SqlParameter("@folio", folio));
                        var spValor = commad.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var spMensaje = commad.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        connection.Open();
                        List<OrdenTaller> listActividad = new List<OrdenTaller>();
                        using (SqlDataReader sqlReader = commad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                OrdenTaller ordenTrabajo = new OrdenTaller
                                {
                                    idOrdenServ = (int)sqlReader["idOrdenSer"],
                                    idOrdenTrabajo = (int)sqlReader["idOrdenTrabajo"],
                                    idOrdenActividad = (int)sqlReader["idOrdenActividad"],
                                    estatus = (string)sqlReader["Estatus"],
                                    descripcionOS = (string)sqlReader["descripcionOS"],
                                    descripcionA = (string)sqlReader["NotaRecepcionA"],
                                    preSalidas = ((string)sqlReader["preSalidas"]).Trim(','),
                                    clave_llanta = (string)sqlReader["clave_llanta"]
                                };

                                if (DBNull.Value.Equals(sqlReader["idServicio"]))
                                {
                                    ordenTrabajo.servicio = null;
                                }
                                else
                                {
                                    ordenTrabajo.servicio = new Servicio
                                    {
                                        idServicio = (int)sqlReader["idServicio"],
                                        nombre = (string)sqlReader["NomServicio"],
                                        descipcion = sqlReader["desServicio"] as string,
                                        costo = (decimal)sqlReader["CostoServ"],
                                        tipoOrden = new TipoOrden
                                        {
                                            idTipoOrden = (int)sqlReader["idTipOrdServ"],
                                            nombreTipoOrden = (string)sqlReader["NomTipOrdServ"]
                                        }

                                    };
                                }

                                if (DBNull.Value.Equals(sqlReader["idActividad"]))
                                {
                                    ordenTrabajo.actividad = null;
                                }
                                else
                                {
                                    ordenTrabajo.actividad = new Actividad
                                    {
                                        idActividad = (int)sqlReader["idActividad"],
                                        nombre = (string)sqlReader["NombreAct"],
                                        costo = DBNull.Value.Equals(sqlReader["CostoManoObra"]) ? 0 : (decimal)sqlReader["CostoManoObra"],
                                        duracion = Convert.ToDecimal(sqlReader["duracion"])
                                    };
                                }

                                if ((int)sqlReader["idPersonal1"] != 0)
                                {
                                    var resp = new OperadoresServices().getPersonalALLorById((int)sqlReader["idPersonal1"]);
                                    if (resp.typeResult == ResultTypes.success)
                                    {
                                        ordenTrabajo.personal1 = ((List<Personal>)resp.result)[0];
                                    }
                                    else
                                    {
                                        ordenTrabajo.personal1 = null;
                                    }
                                }
                                else
                                {
                                    ordenTrabajo.personal1 = null;
                                }

                                if ((int)sqlReader["idPersonal2"] != 0)
                                {
                                    var resp = new OperadoresServices().getPersonalALLorById((int)sqlReader["idPersonal2"]);
                                    if (resp.typeResult == ResultTypes.success)
                                    {
                                        ordenTrabajo.personal2 = ((List<Personal>)resp.result)[0];
                                    }
                                    else
                                    {
                                        ordenTrabajo.personal2 = null;
                                    }
                                }
                                else
                                {
                                    ordenTrabajo.personal2 = null;
                                }
                                if ((int)sqlReader["idPersonal3"] != 0)
                                {
                                    var resp = new OperadoresServices().getPersonalALLorById((int)sqlReader["idPersonal3"]);
                                    if (resp.typeResult == ResultTypes.success)
                                    {
                                        ordenTrabajo.personal3 = ((List<Personal>)resp.result)[0];
                                    }
                                    else
                                    {
                                        ordenTrabajo.personal3 = null;
                                    }
                                }
                                else
                                {
                                    ordenTrabajo.personal3 = null;
                                }

                                listActividad.Add(ordenTrabajo);
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = listActividad };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, result = ex.Message };
            }
        }

        public OperationResult getProductosByIdActividad(int idActividad, int idOrdenServ)
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand commad = connection.CreateCommand())
                    {
                        commad.CommandType = CommandType.StoredProcedure;
                        commad.CommandText = "sp_getProductosByidActividad";

                        commad.Parameters.Add(new SqlParameter("@idActividad", idActividad));
                        commad.Parameters.Add(new SqlParameter("@idOrdenServ", idOrdenServ));
                        var spValor = commad.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var spMensaje = commad.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        connection.Open();
                        List<ProductosActividad> listProductos = new List<ProductosActividad>();
                        using (SqlDataReader sqlReader = commad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                ProductosActividad producto = new ProductosActividad
                                {
                                    idActicidad = (int)sqlReader["idActividad"],
                                    idProducto = (int)sqlReader["idProducto"],
                                    nombre = (string)sqlReader["nombre"],
                                    cantidad = (decimal)sqlReader["cantidad"],
                                    um = (string)sqlReader["um"],
                                    costo = (decimal)sqlReader["precio"],
                                    cantMin = Convert.ToDecimal(sqlReader["cantMin"]),
                                    cantMax = Convert.ToDecimal(sqlReader["cantMax"])
                                };
                                listProductos.Add(producto);
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = listProductos };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, result = ex.Message };
            }
        }

        public OperationResult cancelarOrdenTrabajo(OrdenTaller orden, string motivo, string usuario)
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand commad = connection.CreateCommand())
                    {
                        commad.CommandType = CommandType.StoredProcedure;
                        commad.CommandText = "sp_cancelarOrdenTrabajo";

                        commad.Parameters.Add(new SqlParameter("@idServicio", orden.servicio == null ? 0 : orden.servicio.idServicio));
                        commad.Parameters.Add(new SqlParameter("@idOrdenSer", orden.idOrdenServ));
                        commad.Parameters.Add(new SqlParameter("@idOrdenTrabajo", orden.idOrdenTrabajo));
                        commad.Parameters.Add(new SqlParameter("@motivo", motivo));
                        commad.Parameters.Add(new SqlParameter("@usuario", usuario));
                        var spValor = commad.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var spMensaje = commad.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                        connection.Open();
                        commad.ExecuteNonQuery();

                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        public OperationResult cancelarOrdenTrabajo_v2(OrdenTaller orden, string motivo, string usuario)
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand commad = connection.CreateCommand())
                    {
                        commad.CommandType = CommandType.StoredProcedure;
                        commad.CommandText = "sp_cancelarOrdenTrabajo_v2";
                        
                        commad.Parameters.Add(new SqlParameter("@idOrdenSer", orden.idOrdenServ));
                        commad.Parameters.Add(new SqlParameter("@idOrdenTrabajo", orden.idOrdenTrabajo));
                        commad.Parameters.Add(new SqlParameter("@motivo", motivo));
                        commad.Parameters.Add(new SqlParameter("@usuario", usuario));
                        var spValor = commad.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var spMensaje = commad.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                        connection.Open();
                        commad.ExecuteNonQuery();

                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        public OperationResult cancelarOrdenServicio(TipoOrdenServicio orden, string motivo, string usuario)
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand commad = connection.CreateCommand())
                    {
                        commad.CommandType = CommandType.StoredProcedure;
                        commad.CommandText = "sp_cancelarOrdenServicio";
                        
                        commad.Parameters.Add(new SqlParameter("@idOrdenSer", orden.idOrdenSer));
                        commad.Parameters.Add(new SqlParameter("@motivo", motivo));
                        commad.Parameters.Add(new SqlParameter("@usuario", usuario));

                        var spValor = commad.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var spMensaje = commad.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                        connection.Open();
                        commad.ExecuteNonQuery();

                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult cancelarOrdenServicio_v2(TipoOrdenServicio orden, string motivo, string usuario)
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand commad = connection.CreateCommand())
                    {
                        commad.CommandType = CommandType.StoredProcedure;
                        commad.CommandText = "sp_cancelarOrdenServicio_v2";

                        commad.Parameters.Add(new SqlParameter("@idOrdenSer", orden.idOrdenSer));
                        commad.Parameters.Add(new SqlParameter("@motivo", motivo));
                        commad.Parameters.Add(new SqlParameter("@usuario", usuario));

                        var spValor = commad.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var spMensaje = commad.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                        connection.Open();
                        commad.ExecuteNonQuery();

                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
}
