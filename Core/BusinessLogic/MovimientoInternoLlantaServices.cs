﻿namespace CoreFletera.BusinessLogic
{
    using Core.Models;
    using Core.Utils;
    using CoreFletera.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;

    public class MovimientoInternoLlantaServices
    {
        public OperationResult guardarMovimientoInterno(List<MovimientoInternoLlanta> listaMovimientos)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    connection.Open();
                    using (SqlTransaction transaction = connection.BeginTransaction())
                    {
                        using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_guardarMovimientoInterno", false))
                        {
                            command.Transaction = transaction;
                            foreach (MovimientoInternoLlanta llanta in listaMovimientos)
                            {
                                command.Parameters.Clear();
                                foreach (SqlParameter parameter in CrearSqlCommand.getPatametros(false))
                                {
                                    command.Parameters.Add(parameter);
                                }
                                command.Parameters.Add(new SqlParameter("@tipoUbicacionAnt", (int)llanta.tipoUbicacionAnt));
                                command.Parameters.Add(new SqlParameter("@idUbicacionAnt", llanta.idUbicacionAnt));
                                command.Parameters.Add(new SqlParameter("@posicionAnt", !llanta.posicionAnt.HasValue ? 0 : llanta.posicionAnt));
                                command.Parameters.Add(new SqlParameter("@profundidadAnterior", llanta.profundidadAnterior));
                                command.Parameters.Add(new SqlParameter("@kmAnterior", llanta.kmAnterior));
                                command.Parameters.Add(new SqlParameter("@tipoUbicacionActual", (int)llanta.tipoUbicacionActual));
                                command.Parameters.Add(new SqlParameter("@idUbicacionActual", llanta.idUbicacionActual));
                                command.Parameters.Add(new SqlParameter("@posicionActual", llanta.posicionActual));
                                command.Parameters.Add(new SqlParameter("@profundidadActual", llanta.profundidadActual));
                                command.Parameters.Add(new SqlParameter("@kmActual", llanta.kmActual));
                                command.Parameters.Add(new SqlParameter("@fecha", llanta.fecha));
                                command.Parameters.Add(new SqlParameter("@usuario", llanta.usuario.nombreUsuario));
                                command.Parameters.Add(new SqlParameter("@idLlanta", llanta.llanta.idLlanta));
                                command.Parameters.Add(new SqlParameter("@folioLlanta", llanta.llanta.clave));
                                command.Parameters.Add(new SqlParameter("@tipoMovimiento", (int)llanta.tipoMovimiento));
                                command.Parameters.Add(new SqlParameter("@tipoDocumento", (int)llanta.tipoDocumento));
                                command.Parameters.Add(new SqlParameter("@idDocumento", llanta.idDocumento));
                                command.Parameters.Add(new SqlParameter("@condicionLlanta", 2));
                                command.ExecuteNonQuery();
                                if (!CrearSqlCommand.validarCorrecto(command))
                                {
                                    transaction.Rollback();
                                }
                            }
                            transaction.Commit();
                            result = new OperationResult(command, null);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult(exception);
            }
            return result;
        }
    }
}
