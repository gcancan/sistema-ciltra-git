﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using Core.Utils;
using System.Data;
using System.Data.SqlClient;

namespace CoreFletera.BusinessLogic
{
    public class SincronizacionServices
    {
        /// <summary>
        /// Busca todos los proveedores dados de alta en el comercial.
        /// retorta una lista de proveedores
        /// </summary>
        /// <returns></returns>
        public OperationResult getConsultaProveedores()
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactoryTaller())
                {
                    using (SqlCommand comman = con.CreateCommand())
                    {
                        comman.CommandType = CommandType.Text;
                        comman.CommandText = sqlProveedores;

                        var valor = comman.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var mensaje = comman.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        List<Proveedor> listProveedor = new List<Proveedor>();
                        con.Open();
                        using (SqlDataReader sqlReader = comman.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                Proveedor proveedor = new Proveedor
                                {
                                    CIDCLIENTEPROVEEDOR = (int)sqlReader["CIDCLIENTEPROVEEDOR"],
                                    CCODIGOCLIENTE = (string)sqlReader["CCODIGOCLIENTE"],
                                    razonSocial = (string)sqlReader["CRAZONSOCIAL"],
                                    RFC = (string)sqlReader["CRFC"],
                                    email = (string)sqlReader["CEMAIL1"],
                                    CESTATUS = (int)sqlReader["CESTATUS"]
                                };
                                listProveedor.Add(proveedor);
                            }
                        }
                        return new OperationResult(comman, listProveedor);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        /// <summary>
        /// Consulta de todos las unidades de medida del comercial
        /// </summary>
        /// <returns></returns>
        public OperationResult getConsultaUnidades()
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactoryTaller())
                {
                    using (SqlCommand comman = con.CreateCommand())
                    {
                        comman.CommandType = CommandType.Text;
                        comman.CommandText = sqlUnidadesMedida;

                        var valor = comman.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var mensaje = comman.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        List<UnidadesMedidaCOM> listUnidadesMedidaCOM = new List<UnidadesMedidaCOM>();
                        con.Open();
                        using (SqlDataReader sqlReader = comman.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                UnidadesMedidaCOM unidadesMedidaCOM = new UnidadesMedidaCOM
                                {
                                    CIDUNIDAD = (int)sqlReader["CIDUNIDAD"],
                                    CNOMBREUNIDAD = (string)sqlReader["CNOMBREUNIDAD"],
                                    CABREVIATURA = (string)sqlReader["CABREVIATURA"],
                                    CDESPLIEGUE = (string)sqlReader["CDESPLIEGUE"],
                                    CCLAVEINT = (string)sqlReader["CCLAVEINT"],
                                    CCLAVESAT = (string)sqlReader["CCLAVESAT"],
                                };
                                listUnidadesMedidaCOM.Add(unidadesMedidaCOM);
                            }
                        }
                        return new OperationResult(comman, listUnidadesMedidaCOM);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        /// <summary>
        /// Funcion para insertar o actualizar los proveedores de comercial 
        /// a la base de la fletera.
        /// </summary>
        /// <param name="ListaProveedores"></param>
        /// <returns></returns>
        internal OperationResult sincronizarProveedores(List<Proveedor> ListaProveedores)
        {
            try
            {
                string xml = GenerateXML.generarListaProveedores(ListaProveedores);
                if (string.IsNullOrEmpty(xml))
                {
                    return new OperationResult { valor = 1, mensaje = "ERROR AL GENERAR EL XML" };
                }
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comand = CrearSqlCommand.getCommand(con, "sp_sincronizarProveedores")) 
                    {
                        comand.Parameters.Add(new SqlParameter("@xml", xml));
                        con.Open();
                        comand.ExecuteNonQuery();
                        return new OperationResult(comand);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }


        #region CONSULTAS
        /// <summary>
        /// Consulta para buscar todos los proveedores de comercial
        /// </summary>
        string sqlProveedores = @"
BEGIN TRY
    SELECT  CIDCLIENTEPROVEEDOR ,
            CCODIGOCLIENTE ,
            CRAZONSOCIAL ,
            CRFC ,
            CEMAIL1,
            c.CESTATUS
    FROM    dbo.admClientes AS c
    WHERE   CTIPOCLIENTE IN ( 2, 3 );
    IF @@ROWCOUNT > 0
        BEGIN
            SET @valor = 0;
            SET @mensaje = 'REGISTROS ENCONTRADOS';
        END;
    ELSE
        BEGIN
            SET @valor = 3;
            SET @mensaje = 'NO SE ENCONTRARON PROVEEDORES';
        END;
END TRY 
BEGIN CATCH
    SET @mensaje = ERROR_MESSAGE();
    SET @valor = 1;
END CATCH;";

        public string sqlUnidadesMedida = @"
BEGIN TRY
    SELECT  CIDUNIDAD ,
            CNOMBREUNIDAD ,
            CABREVIATURA ,
            CDESPLIEGUE ,
            CCLAVEINT ,
            CCLAVESAT
    FROM    dbo.admUnidadesMedidaPeso;
    IF @@ROWCOUNT > 0
        BEGIN
            SET @valor = 0;
            SET @mensaje = 'REGISTROS ENCONTRADOS';
        END;
    ELSE
        BEGIN
            SET @valor = 3;
            SET @mensaje = 'NO SE ENCONTRARON PROVEEDORES';
        END;
END TRY 
BEGIN CATCH
    SET @mensaje = ERROR_MESSAGE();
    SET @valor = 1;
END CATCH;
";
        #endregion
    }
}
