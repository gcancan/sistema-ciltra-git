﻿using Core.Models;
using Core.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.BusinessLogic
{
    public class MenusAplicacionService
    {
        public OperationResult getAllMenus()
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "getAllMenusAplicacion";

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var mensaje = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        connection.Open();
                        var i = command.ExecuteReader();
                        List<MenusAplicacion> listMenusAplicacion = new List<MenusAplicacion>();

                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                MenusAplicacion menus = new MenusAplicacion
                                {
                                    idMenuAplicacion = (int)sqlReader["idMenuAplicacion"],
                                    clave = (string)sqlReader["clave"],
                                    nombre = (string)sqlReader["nombre"],
                                    privilegio = new Privilegio
                                    {
                                        idPrivilegio = (int)sqlReader["privilegio_idPrivilegio"],
                                        clave = (string)sqlReader["privilegio_clave"],
                                        descripcion = (string)sqlReader["privilegio_descripcion"],
                                        nombre = (string)sqlReader["nombre"]
                                    }
                                };

                                if ((int)sqlReader["idPadre"] !=0)
                                {
                                    menus.idPadre = (int)sqlReader["idPadre"];
                                }
                                listMenusAplicacion.Add(menus);
                                //{
                                //    idMenuAplicacion = (int)sqlReader["idMenuAplicacion"],
                                //    clave = (string)sqlReader["clave"],
                                //    nombre = (string)sqlReader["nombre"],
                                //    idPadre = sqlReader["idPadre"] == null ?  : (int)sqlReader["idPadre"]
                                //});
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)mensaje.Value, result = listMenusAplicacion };
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }

        public OperationResult getAllMenusAplicacionByIdUser(int idUsuario)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "getAllMenusAplicacionByIdUser";

                        command.Parameters.Add(new SqlParameter("@idUsuario", idUsuario));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var mensaje = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        connection.Open();
                        var i = command.ExecuteReader();
                        List<MenusAplicacion> listMenusAplicacion = new List<MenusAplicacion>();

                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                MenusAplicacion menus = new MenusAplicacion
                                {
                                    idMenuAplicacion = (int)sqlReader["idMenuAplicacion"],
                                    clave = (string)sqlReader["clave"],
                                    nombre = (string)sqlReader["nombre"]
                                };

                                if ((int)sqlReader["idPadre"] != 0)
                                {
                                    menus.idPadre = (int)sqlReader["idPadre"];
                                }
                                listMenusAplicacion.Add(menus);
                                //{
                                //    idMenuAplicacion = (int)sqlReader["idMenuAplicacion"],
                                //    clave = (string)sqlReader["clave"],
                                //    nombre = (string)sqlReader["nombre"],
                                //    idPadre = sqlReader["idPadre"] == null ?  : (int)sqlReader["idPadre"]
                                //});
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)mensaje.Value, result = listMenusAplicacion };
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }
    }
}
