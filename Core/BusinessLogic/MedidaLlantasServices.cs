﻿namespace CoreFletera.BusinessLogic
{
    using Core.Models;
    using Core.Utils;
    using CoreFletera.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;

    public class MedidaLlantasServices
    {
        public OperationResult getMedidasLlantas()
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getMedidas", false))
                    {
                        List<MedidaLlanta> list = new List<MedidaLlanta>();
                        connection.Open();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                MedidaLlanta item = new MedidaLlanta
                                {
                                    idMedidaLlanta = (int)reader["idMedidaLlanta"],
                                    medida = (string)reader["medida"]
                                };
                                list.Add(item);
                            }
                        }
                        result = new OperationResult(command, list);
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult(exception);
            }
            return result;
        }

        public OperationResult saveMedidaLlantas(ref MedidaLlanta medidaLlanta)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    connection.Open();
                    using (SqlTransaction transaction = connection.BeginTransaction())
                    {
                        using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_saveMedidaLlantas", true))
                        {
                            command.Transaction = transaction;
                            command.Parameters.Add(new SqlParameter("@clave", medidaLlanta.idMedidaLlanta));
                            command.Parameters.Add(new SqlParameter("@medida", medidaLlanta.medida));
                            command.ExecuteNonQuery();
                            int id = 0;
                            if (CrearSqlCommand.validarCorrecto(command, out id))
                            {
                                medidaLlanta.idMedidaLlanta = id;
                                transaction.Commit();
                            }
                            else
                            {
                                transaction.Rollback();
                            }
                            result = new OperationResult(command, medidaLlanta);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult(exception);
            }
            return result;
        }
    }
}
