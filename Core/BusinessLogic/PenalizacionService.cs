﻿using Core.Interfaces;
using Core.Models;
using Core.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.BusinessLogic
{
    public class PenalizacionService
    {
        public OperationResult getPenalizacionesByFechas(DateTime fechaInicio, DateTime fechaFin, int idCliente = 0)
        {
            try
            {

                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getPenalizacionesByFechas";
                        command.Parameters.Add(new SqlParameter("@fechaInicio", fechaInicio));
                        command.Parameters.Add(new SqlParameter("@fechaFin", fechaFin));
                        command.Parameters.Add(new SqlParameter("@idCliente", idCliente));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                        connection.Open();
                        var i = command.ExecuteReader();
                        List<Penalizacion> list = new List<Penalizacion>();
                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                Penalizacion penalizacion = new Penalizacion();
                                penalizacion.idPenalizacion = (int)sqlReader["idPenalizacion"];
                                penalizacion.idViaje = (int)sqlReader["idViaje"];
                                penalizacion.fecha = (DateTime)sqlReader["fecha"];
                                penalizacion.capacidadViaje = (decimal)sqlReader["capacidadViaje"];
                                penalizacion.sumaCargas = (decimal)sqlReader["sumaCargas"];
                                penalizacion.penalizacion = (decimal)sqlReader["penalizacion"];
                                penalizacion.cargaMinima = (decimal)sqlReader["cargaMinima"];
                                penalizacion.diferencia = (decimal)sqlReader["diferencia"];
                                penalizacion.precio = (decimal)sqlReader["precio"];
                                penalizacion.importe = (decimal)sqlReader["importe"];
                                penalizacion.tractor = (string)sqlReader["idTractor"];
                                penalizacion.remolque1 = (string)sqlReader["idRemolque1"];
                                penalizacion.remolque2 = (string)sqlReader["idRemolque2"];
                                penalizacion.operador = (string)sqlReader["operador"];
                                list.Add(penalizacion);
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)codigo.Value, result = list };
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }

        public OperationResult getDetallesPensalizaciones(int idViaje)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getDetallesPensalizacione";
                        command.Parameters.Add(new SqlParameter("@idViaje", idViaje));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                        connection.Open();
                        var i = command.ExecuteReader();
                        List<DetallePenalizacion> list = new List<DetallePenalizacion>();
                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                DetallePenalizacion detalle = new DetallePenalizacion()
                                {
                                    remision = (string)sqlReader["NoRemision"],
                                    destino = (string)sqlReader["destino"],
                                    producto = (string)sqlReader["producto"]
                                };
                                list.Add(detalle);
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)codigo.Value, result = list };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }
    }
}
