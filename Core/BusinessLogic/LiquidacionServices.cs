﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using Core.BusinessLogic;
using CoreFletera.BusinessLogic;
using Core.Utils;
using System.Data;
using System.Data.SqlClient;
namespace CoreFletera.BusinessLogic
{
    public class LiquidacionServices
    {
        public OperationResult saveLiquidacion(ref Liquidacion liquidacion)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    con.Open();
                    using (SqlTransaction tran = con.BeginTransaction())
                    {
                        using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_saveLiquidacionV2", true))
                        {
                            comad.Transaction = tran;
                            comad.Parameters.Add(new SqlParameter("@idLiquidacion", liquidacion.idLiquidacion));
                            comad.Parameters.Add(new SqlParameter("@fechaInicio", liquidacion.fechaInicio));
                            comad.Parameters.Add(new SqlParameter("@fechaFin", liquidacion.fechaFin));
                            comad.Parameters.Add(new SqlParameter("@usuarioInicio", liquidacion.usuarioInicio));
                            comad.Parameters.Add(new SqlParameter("@usuarioTermino", liquidacion.usuarioFin));
                            comad.Parameters.Add(new SqlParameter("@total", liquidacion.total));
                            comad.Parameters.Add(new SqlParameter("@activo", liquidacion.activo));
                            comad.Parameters.Add(new SqlParameter("@liquidado", liquidacion.liquidado));
                            comad.ExecuteNonQuery();
                            int id = 0;
                            if (!CrearSqlCommand.validarCorrecto(comad, out id))
                            {
                                tran.Rollback();
                                return new OperationResult(comad);
                            }
                            else
                            {
                                liquidacion.idLiquidacion = id;

                                foreach (DetalleLiquidacion detLiquidacion in liquidacion.listaDetalleLiquidacion)
                                {
                                    comad.CommandText = "sp_saveDetalleLiquidacionV2";
                                    detLiquidacion.idLiquidacion = liquidacion.idLiquidacion;
                                    comad.Parameters.Clear();
                                    foreach (SqlParameter param in CrearSqlCommand.getPatametros(true))
                                    {
                                        comad.Parameters.Add(param);
                                    }
                                    comad.Parameters.Add(new SqlParameter("@idDetalleLiquidacion", detLiquidacion.idDetalleLiquidacion));
                                    comad.Parameters.Add(new SqlParameter("@idLiquidacion", detLiquidacion.idLiquidacion));
                                    comad.Parameters.Add(new SqlParameter("@idDepartamento", detLiquidacion.departamento.clave));
                                    comad.ExecuteNonQuery();
                                    if (!CrearSqlCommand.validarCorrecto(comad, out id))
                                    {
                                        tran.Rollback();
                                        return new OperationResult(comad);
                                    }
                                    else
                                    {
                                        detLiquidacion.idDetalleLiquidacion = id;
                                    }
                                }


                                foreach (var detPersonaLiquidacion in liquidacion.listaDetallePersonalLiquidacion)
                                {
                                    detPersonaLiquidacion.idLiquidacion = liquidacion.idLiquidacion;
                                    comad.CommandText = "sp_saveDetallePersonaLiquidacion";
                                    comad.Parameters.Clear();
                                    foreach (SqlParameter param in CrearSqlCommand.getPatametros(true))
                                    {
                                        comad.Parameters.Add(param);
                                    }
                                    comad.Parameters.Add(new SqlParameter("@idDetallePersonaLiquidacion", detPersonaLiquidacion.idDetallePersonaLiquidacion));
                                    comad.Parameters.Add(new SqlParameter("@idLiquidacion", detPersonaLiquidacion.idLiquidacion));
                                    comad.Parameters.Add(new SqlParameter("@idPersonal", detPersonaLiquidacion.personal.clave));
                                    comad.Parameters.Add(new SqlParameter("@totalViajesBalanceados", detPersonaLiquidacion.totalViajesBalanceados));
                                    comad.Parameters.Add(new SqlParameter("@totalViajesSecos", detPersonaLiquidacion.totalViajesSecos));
                                    comad.Parameters.Add(new SqlParameter("@totalPagoFijo", detPersonaLiquidacion.totalPagoFijo));
                                    comad.Parameters.Add(new SqlParameter("@totalPagoApoyo", detPersonaLiquidacion.totalPagoApoyo));
                                    comad.Parameters.Add(new SqlParameter("@totalDescuentos", detPersonaLiquidacion.totalDescuento));
                                    comad.Parameters.Add(new SqlParameter("@totalExtras", detPersonaLiquidacion.totalExtras));
                                    comad.Parameters.Add(new SqlParameter("@totalHorasExtras", detPersonaLiquidacion.totalHorasExtras));
                                    comad.Parameters.Add(new SqlParameter("@sePagaViaje", detPersonaLiquidacion.sePagaViaje));
                                    comad.Parameters.Add(new SqlParameter("@totalAPagar", detPersonaLiquidacion.totalPago));
                                    comad.Parameters.Add(new SqlParameter("@activo", detPersonaLiquidacion.activo));
                                    comad.ExecuteNonQuery();
                                    int idDetPersona = 0;
                                    if (!CrearSqlCommand.validarCorrecto(comad, out idDetPersona))
                                    {
                                        tran.Rollback();
                                        return new OperationResult(comad);
                                    }
                                    else
                                    {
                                        detPersonaLiquidacion.idDetallePersonaLiquidacion = idDetPersona;
                                    }

                                    foreach (ConceptoLiquidacion concepto in detPersonaLiquidacion.listaConceptoLiquidacion)
                                    {
                                        concepto.idLiquidacion = liquidacion.idLiquidacion;
                                        concepto.idDetallePersonaLiquidacion = detPersonaLiquidacion.idDetallePersonaLiquidacion;
                                        comad.CommandText = "sp_saveConceptoLiquidacion";
                                        comad.Parameters.Clear();
                                        foreach (SqlParameter param in CrearSqlCommand.getPatametros(true))
                                        {
                                            comad.Parameters.Add(param);
                                        }
                                        comad.Parameters.Add(new SqlParameter("@idConceptoLiquidacion", concepto.idConceptoLiquidacion));
                                        comad.Parameters.Add(new SqlParameter("@idLiquidacion", concepto.idLiquidacion));
                                        comad.Parameters.Add(new SqlParameter("@idDetallePersonalLiquidacion", concepto.idDetallePersonaLiquidacion));
                                        comad.Parameters.Add(new SqlParameter("@tipoConceptoLiquidacion", (int)concepto.tipoConceptoLiquidacion));
                                        comad.Parameters.Add(new SqlParameter("@idCliente", concepto.idCliente));
                                        comad.Parameters.Add(new SqlParameter("@folioViaje", concepto.folioViaje));
                                        comad.Parameters.Add(new SqlParameter("@noViaje", concepto.noViaje));
                                        comad.Parameters.Add(new SqlParameter("@destinos", concepto.destinos));
                                        comad.Parameters.Add(new SqlParameter("@noDestinos", concepto.noDestinos));
                                        comad.Parameters.Add(new SqlParameter("@noViajeOperador", concepto.noViajeOperador));
                                        comad.Parameters.Add(new SqlParameter("@fechaInicial", concepto.fechaInicial));
                                        comad.Parameters.Add(new SqlParameter("@fechaFinal", concepto.fechaFinal));
                                        comad.Parameters.Add(new SqlParameter("@cantidad", concepto.cantidad));
                                        comad.Parameters.Add(new SqlParameter("@doble", concepto.doble));
                                        comad.Parameters.Add(new SqlParameter("@motivoDoble", concepto.motivosDoble));
                                        comad.Parameters.Add(new SqlParameter("@total", concepto.total));
                                        comad.Parameters.Add(new SqlParameter("@fecha", concepto.fecha == null ? null : concepto.fecha));
                                        comad.Parameters.Add(new SqlParameter("@tipoIncidencia", concepto.tipoIncidencia));
                                        comad.Parameters.Add(new SqlParameter("@motivoDescuento", concepto.motivoDescuentos));
                                        comad.Parameters.Add(new SqlParameter("@folioViajeDescuento", concepto.folioViajeDescuento));
                                        comad.Parameters.Add(new SqlParameter("@observaciones", concepto.observaciones));
                                        comad.Parameters.Add(new SqlParameter("@usuario", concepto.usuario));
                                        comad.Parameters.Add(new SqlParameter("@noHoras", concepto.noHoras));
                                        comad.Parameters.Add(new SqlParameter("@activo", concepto.activo));
                                        comad.Parameters.Add(new SqlParameter("@cartaPorte", concepto.cartaPorte));
                                        comad.Parameters.Add(new SqlParameter("@tractor", concepto.tractor));
                                        comad.Parameters.Add(new SqlParameter("@origen", concepto.origenes));
                                        comad.Parameters.Add(new SqlParameter("@fechaCarga", concepto.fechaCarga));
                                        comad.Parameters.Add(new SqlParameter("@fechaDescarga", concepto.fechaDescarga));
                                        comad.Parameters.Add(new SqlParameter("@remision", concepto.remision));
                                        comad.Parameters.Add(new SqlParameter("@modalidad", concepto.modalidad));

                                        comad.ExecuteNonQuery();
                                        int idConcepto = 0;
                                        if (!CrearSqlCommand.validarCorrecto(comad, out idConcepto))
                                        {
                                            tran.Rollback();
                                            return new OperationResult(comad);
                                        }
                                        else
                                        {
                                            concepto.idConceptoLiquidacion = idConcepto;
                                        }
                                    }
                                }
                            }
                            tran.Commit();
                            return new OperationResult(comad, liquidacion);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getLiquidacionesByFolio(int folio)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getLiquidacionByFolio"))
                    {
                        comad.Parameters.Add(new SqlParameter("@folio", folio));
                        con.Open();
                        Liquidacion liquidacion = new Liquidacion();
                        using (SqlDataReader reader = comad.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                liquidacion = new mapComunes().mapLiquidacion(reader);
                                if (liquidacion == null)
                                {
                                    return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADO DE LA BUSQUEDA (nivel 1)");
                                }
                            }
                        }
                        var respDiasInhabil = getInhabiles();
                        if (respDiasInhabil.typeResult == ResultTypes.error) return respDiasInhabil;

                        List<DateTime> listaInhabil = respDiasInhabil.result as List<DateTime> ;


                        var respDetalleLiquidacion = getDetalleLiquidacionesByFolio(folio);
                        if (respDetalleLiquidacion.typeResult != ResultTypes.success) return respDetalleLiquidacion;
                        liquidacion.listaDetalleLiquidacion = respDetalleLiquidacion.result as List<DetalleLiquidacion>;
                         
                        var respDetPersonal = getDetallePersonalLiquidacionesByFolio(folio);
                        if (respDetPersonal.typeResult != ResultTypes.success) return respDetPersonal;
                        liquidacion.listaDetallePersonalLiquidacion = (respDetPersonal.result as List<DetallePersonaLiquidacion>).OrderBy(s=> s.personal.nombre).ToList();

                        var respConceptosViajes = getcConceptosViajesByFolio(folio, listaInhabil);
                        if (respConceptosViajes.typeResult == ResultTypes.error) return respConceptosViajes;

                        List<ConceptoLiquidacion> listaConceptoViajes = respConceptosViajes.result as List<ConceptoLiquidacion>;
                        foreach (var detallePersonal in liquidacion.listaDetallePersonalLiquidacion)
                        {
                            foreach (var concepto in listaConceptoViajes.FindAll(s => s.idDetallePersonaLiquidacion == detallePersonal.idDetallePersonaLiquidacion).ToList())
                            {
                                concepto.detalleHeader = detallePersonal;
                                detallePersonal.listaConceptoLiquidacion.Add(concepto);
                            }
                        }

                        var respConceptosPagosFijos = getcConceptosPagosFijosByFolio(folio);
                        if (respConceptosPagosFijos.typeResult == ResultTypes.error) return respConceptosPagosFijos;

                        List<ConceptoLiquidacion> listaConceptoPagosFijos = respConceptosPagosFijos.result as List<ConceptoLiquidacion>;
                        foreach (var detallePersonal in liquidacion.listaDetallePersonalLiquidacion)
                        {
                            foreach (var concepto in listaConceptoPagosFijos.FindAll(s => s.idDetallePersonaLiquidacion == detallePersonal.idDetallePersonaLiquidacion).ToList())
                            {
                                concepto.detalleHeader = detallePersonal;
                                detallePersonal.listaConceptoLiquidacion.Add(concepto);
                            }
                        }

                        var respConceptosDescuentos = getcConceptosDescuentos(folio);
                        if (respConceptosDescuentos.typeResult == ResultTypes.error) return respConceptosDescuentos;

                        List<ConceptoLiquidacion> listaConceptoDescuentos = respConceptosDescuentos.result as List<ConceptoLiquidacion>;
                        foreach (var detallePersonal in liquidacion.listaDetallePersonalLiquidacion)
                        {
                            List<ViajesOperador> listaViajesOperador = detallePersonal.listaConceptoLiquidacion.Where(s => s.tipoConceptoLiquidacion == TipoConceptoLiquidacion.VIAJES_ALIMENTOS_BALANCEADOS
                            || s.tipoConceptoLiquidacion == TipoConceptoLiquidacion.VIAJES_SECOS_REFRIGERADOS).
                            Select(s => new ViajesOperador { folioViaje = s.folioViaje.Value, pagoViaje = s.total }).ToList();

                            foreach (var concepto in listaConceptoDescuentos.FindAll(s => s.idDetallePersonaLiquidacion == detallePersonal.idDetallePersonaLiquidacion).ToList())
                            {
                                concepto.detalleHeader = detallePersonal;
                                concepto.listaViajesOperador = listaViajesOperador;
                                concepto.viajesOperadorDecuento = concepto.idViajeDescuento == null ? null : listaViajesOperador.Find(s => s.folioViaje == concepto.idViajeDescuento); 
                                detallePersonal.listaConceptoLiquidacion.Add(concepto);
                            }
                        }

                        var respConceptosExtras = getcConceptosExtras(folio);
                        if (respConceptosExtras.typeResult == ResultTypes.error) return respConceptosExtras;

                        List<ConceptoLiquidacion> listaConceptoExtras = respConceptosExtras.result as List<ConceptoLiquidacion>;
                        foreach (var detallePersonal in liquidacion.listaDetallePersonalLiquidacion)
                        {
                            foreach (var concepto in listaConceptoExtras.FindAll(s => s.idDetallePersonaLiquidacion == detallePersonal.idDetallePersonaLiquidacion).ToList())
                            {
                                concepto.detalleHeader = detallePersonal;
                                detallePersonal.listaConceptoLiquidacion.Add(concepto);
                            }
                        }

                        return new OperationResult(comad, liquidacion);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getDetalleLiquidacionesByFolio(int folio)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getDetalleLiquidacionByFolio"))
                    {
                        comad.Parameters.Add(new SqlParameter("@folio", folio));
                        con.Open();
                        List<DetalleLiquidacion> lista = new List<DetalleLiquidacion>();
                        using (SqlDataReader reader = comad.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                DetalleLiquidacion detalle = new mapComunes().mapDetalleLiquidacion(reader);
                                if (detalle == null)
                                {
                                    return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADO DE LA BUSQUEDA (nivel 2)");
                                }
                                lista.Add(detalle);
                            }
                        }
                        return new OperationResult(comad, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getDetallePersonalLiquidacionesByFolio(int folio)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getDetallePersonalLiquidacionByFolio"))
                    {
                        comad.Parameters.Add(new SqlParameter("@folio", folio));
                        con.Open();
                        List<DetallePersonaLiquidacion> lista = new List<DetallePersonaLiquidacion>();
                        using (SqlDataReader reader = comad.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                DetallePersonaLiquidacion detalle = new mapComunes().mapDetallePersonalLiquidacion(reader);
                                if (detalle == null)
                                {
                                    return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADO DE LA BUSQUEDA (nivel 3)");
                                }
                                lista.Add(detalle);
                            }
                        }
                        return new OperationResult(comad, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        public OperationResult getcConceptosViajesByFolio(int folio, List<DateTime> listaDiasInhabiles)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getConceptosViajesByFolio"))
                    {
                        comad.Parameters.Add(new SqlParameter("@folio", folio));
                        con.Open();
                        List<ConceptoLiquidacion> lista = new List<ConceptoLiquidacion>();
                        using (SqlDataReader reader = comad.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                ConceptoLiquidacion concepto = new mapComunes().mapConceptoViaje(reader, listaDiasInhabiles);
                                if (concepto == null)
                                {
                                    return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADO DE LA BUSQUEDA (nivel 4.1)");
                                }
                                lista.Add(concepto);
                            }
                        }
                        return new OperationResult(comad, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getcConceptosPagosFijosByFolio(int folio)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getConceptosPagosFijosByFolio"))
                    {
                        comad.Parameters.Add(new SqlParameter("@folio", folio));
                        con.Open();
                        List<ConceptoLiquidacion> lista = new List<ConceptoLiquidacion>();
                        using (SqlDataReader reader = comad.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                ConceptoLiquidacion concepto = new mapComunes().mapConceptoPagosFijos(reader);
                                if (concepto == null)
                                {
                                    return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADO DE LA BUSQUEDA (nivel 4.2)");
                                }
                                lista.Add(concepto);
                            }
                        }
                        return new OperationResult(comad, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getcConceptosDescuentos(int folio)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getConceptosDescuentosByFolio"))
                    {
                        comad.Parameters.Add(new SqlParameter("@folio", folio));
                        con.Open();
                        List<ConceptoLiquidacion> lista = new List<ConceptoLiquidacion>();
                        using (SqlDataReader reader = comad.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                ConceptoLiquidacion concepto = new mapComunes().mapConceptoDescuentos(reader);
                                if (concepto == null)
                                {
                                    return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADO DE LA BUSQUEDA (nivel 4.3)");
                                }
                                lista.Add(concepto);
                            }
                        }
                        return new OperationResult(comad, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        public OperationResult getcConceptosExtras(int folio)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getConceptosExtrasByFolio"))
                    {
                        comad.Parameters.Add(new SqlParameter("@folio", folio));
                        con.Open();
                        List<ConceptoLiquidacion> lista = new List<ConceptoLiquidacion>();
                        using (SqlDataReader reader = comad.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                ConceptoLiquidacion concepto = new mapComunes().mapConceptoExtras(reader);
                                if (concepto == null)
                                {
                                    return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADO DE LA BUSQUEDA (nivel 4.3)");
                                }
                                lista.Add(concepto);
                            }
                        }
                        return new OperationResult(comad, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult bajarConceptoLiquidacion(int idConceptoLiquidacion)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_bajaConceptoLiquidacion"))
                    {
                        comad.Parameters.Add(new SqlParameter("@idConceptoLiquidacion", idConceptoLiquidacion));
                        con.Open();
                        comad.ExecuteNonQuery();
                        return new OperationResult(comad);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        public OperationResult getAllLiquidaciones()
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "getAllLiquidaciones"))
                    {
                        
                        con.Open();
                        List<Liquidacion> lista = new List<Liquidacion>();
                        using (SqlDataReader reader = comad.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Liquidacion liquidacion = new mapComunes().mapLiquidacion(reader);
                                if (liquidacion == null)
                                {
                                    return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADO DE LA BUSQUEDA");
                                }

                                var respDetalle = getDetalleLiquidacionesByFolio(liquidacion.idLiquidacion);
                                if (respDetalle.typeResult == ResultTypes.success)
                                {
                                    liquidacion.listaDetalleLiquidacion = respDetalle.result as List<DetalleLiquidacion>;
                                }
                                else
                                {
                                    return respDetalle;
                                }

                                lista.Add(liquidacion);
                            }
                        }
                        return new OperationResult(comad, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        public OperationResult getAllLiquidacionesByIdDetpto(int idDepartamento)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "getAllLiquidacionesByDepto"))
                    {
                        comad.Parameters.Add(new SqlParameter("@idDepartamento", idDepartamento));
                        con.Open();
                        List<Liquidacion> lista = new List<Liquidacion>();
                        using (SqlDataReader reader = comad.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Liquidacion liquidacion = new mapComunes().mapLiquidacion(reader);
                                if (liquidacion == null)
                                {
                                    return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADO DE LA BUSQUEDA");
                                }

                                var respDetalle = getDetalleLiquidacionesByFolio(liquidacion.idLiquidacion);
                                if (respDetalle.typeResult == ResultTypes.success)
                                {
                                    liquidacion.listaDetalleLiquidacion = respDetalle.result as List<DetalleLiquidacion>;
                                }
                                else
                                {
                                    return respDetalle;
                                }

                                lista.Add(liquidacion);
                            }
                        }
                        return new OperationResult(comad, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        public OperationResult getInhabiles()
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getDiasInhabiles"))
                    {

                        con.Open();
                        List<DateTime> lista = new List<DateTime>();
                        using (SqlDataReader reader = comad.ExecuteReader())
                        {
                            while (reader.Read())
                            {     
                                lista.Add((DateTime)reader["fecha"]);
                            }
                        }
                        return new OperationResult(comad, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        public OperationResult validarDepartamentoEnLiquidacion(List<string> listaIdDeptos)
        {
            try
            {
                string xml = GenerateXML.generarListaCadena(listaIdDeptos);
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_validarDepartamentoEnLiquidacion"))
                    {
                        con.Open();
                        comad.Parameters.Add(new SqlParameter("xml", xml));
                        comad.ExecuteNonQuery();
                        return new OperationResult(comad);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult validarDepartamentoFechasEnLiquidacion(List<classCadenas> lista)
        {
            try
            {
                string xml = GenerateXML.generarListaCadena(lista);
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_validarLiquidacionFechas"))
                    {
                        con.Open();
                        comad.Parameters.Add(new SqlParameter("xml", xml));
                        comad.ExecuteNonQuery();
                        return new OperationResult(comad);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
}
