﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using Core.Interfaces;
using CoreFletera.Models;

namespace Core.BusinessLogic
{
    class mapComunes
    {
        private SqlDataReader reader;

        internal Cliente mapCliente(SqlDataReader sqlReader, bool incluirColonia = false)
        {
            try
            {
                Cliente cliente = new Cliente
                {
                    clave = (int)sqlReader["cliente_idCliente"],
                    nombre = (string)sqlReader["cliente_nombre"],
                    domicilio = (string)sqlReader["cliente_Domicilio"],
                    rfc = (string)sqlReader["cliente_rfc"],
                    estado = (string)sqlReader["cliente_estado"],
                    pais = (string)sqlReader["cliente_pais"],
                    municipio = (string)sqlReader["cliente_municipio"],
                    colonia = (string)sqlReader["cliente_colonia"],
                    cp = (string)sqlReader["cliente_cp"],
                    telefono = (string)sqlReader["cliente_telefono"],
                    omitirRemision = !DBNull.Value.Equals(sqlReader["omitirRemision"]) && Convert.ToBoolean(sqlReader["omitirRemision"]),
                    cobroXkm = Convert.ToBoolean(sqlReader["cobroXkm"]),
                    cveINTELISIS = (string)sqlReader["cliente_cveINTELISIS"]
                };
                Impuestos impuestos1 = new Impuestos
                {
                    clave_empresa = (int)sqlReader["impuesto_idEmpresa"],
                    clave = (int)sqlReader["impuesto_idImpuesto"],
                    descripcion = (string)sqlReader["impuesto_descripcion"],
                    valor = (decimal)sqlReader["impuesto_valor"],
                    activo = (bool)sqlReader["impuesto_activo"]
                };
                cliente.impuesto = impuestos1;
                Impuestos impuestos2 = new Impuestos
                {
                    clave_empresa = (int)sqlReader["impuestoFlete_idEmpresa"],
                    clave = (int)sqlReader["impuestoFlete_idImpuesto"],
                    descripcion = (string)sqlReader["impuestoFlete_Descripcion"],
                    valor = (decimal)sqlReader["impuestoFlete_valor"],
                    activo = (bool)sqlReader["impuestoFlete_activo"]
                };
                cliente.impuestoFlete = impuestos2;
                cliente.empresa = this.mapEmpresa(sqlReader, "");

                cliente.fraccion = null;
                if (!DBNull.Value.Equals(sqlReader["fraccion_clave"]))
                {
                    cliente.fraccion = new Fraccion
                    {
                        clave = (int)sqlReader["fraccion_clave"],
                        descripcion = (string)sqlReader["fraccion_descripcion"]
                    };
                }

                if (incluirColonia)
                {
                    cliente.activo = (bool)sqlReader["Cli_activo"];
                    cliente.alias = (string)sqlReader["cli_alias"];
                    cliente.penalizacion = (decimal)sqlReader["cli_penalizacion"];
                    if (!DBNull.Value.Equals(sqlReader["colonia_id"]))
                    {
                        cliente.objColonia = mapColonia(sqlReader);
                    }
                }
                else
                {
                    cliente.objColonia = null;
                }

                return cliente;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal DescripcionEvaluacion mapDescripcionEvaluacion(SqlDataReader reader)
        {
            try
            {
                DescripcionEvaluacion descripcion = new DescripcionEvaluacion
                {
                    factorCalificacion = new FactorCalificacion
                    {
                        idFactorCalificacion = (int)reader["factor_clave"],
                        factorCalificacion = (string)reader["factor_factorCalificacion"],
                        activo = (bool)reader["factor_activo"]
                    },
                    idDescripcionEvaluacion = (int)reader["descripcion_clave"],
                    descripcion = (string)reader["descripcion_descripcion"],
                    porcentaje = (decimal)reader["descripcion_porcentaje"],
                    activo = (bool)reader["descripcion_activo"]
                };
                return descripcion;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal EvaluacionDesempeño mapEvaluacionDesempeño(SqlDataReader reader)
        {
            try
            {
                EvaluacionDesempeño evaluacion = new EvaluacionDesempeño
                {
                    idEvaluacionDesempeño = (int)reader["evaluacion_clave"],
                    fecha = (DateTime)reader["evaluacion_fecha"],
                    conDelCalf = (int)reader["evaluacion_contDefCal"],
                    listaDetalles = new List<DetalleEvaluacionDesempeño>(),
                    personalEvalua = new Personal { clave = (int)reader["per_clave"], nombre = (string)reader["per_nombreCompleto"] },
                    factoresMejora = (string)reader["evaluacion_mejoras"],
                    observaciones = (string)reader["evaluacion_observaciones"],
                    compromiso = (string)reader["evaluacion_compromiso"],
                    comentarios = (string)reader["evaluacion_comentarios"],
                    estatus = (string)reader["evaluacion_estatus"],
                    usuarioCierra = (string)reader["evaluacion_usuario_cierra"],
                    fechaCierra = DBNull.Value.Equals(reader["evaluacion_fechaCierra"]) ? null : (DateTime?)reader["evaluacion_fechaCierra"]
                };
                Personal personalEvaluada = mapPersonal_v2(reader);
                evaluacion.personalEvaluada = personalEvaluada;
                return evaluacion;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal DetalleEvaluacionDesempeño mapDetalleEvaluacion(SqlDataReader reader)
        {
            try
            {
                DetalleEvaluacionDesempeño detalle = new DetalleEvaluacionDesempeño
                {
                    idDetalleEvaluacionDesempeño = (int)reader["detalle_clave"],
                    idEvaluacionDesempeño = (int)reader["detalle_idEvaluacion"],
                    contCalf = (int)reader["detalle_conCalf"],
                    porcentaje = (decimal)reader["detalle_porcentaje"],
                    valoracion = (int)reader["detalle_valoracion"]
                };
                DescripcionEvaluacion descripcion = mapDescripcionEvaluacion(reader);
                detalle.descripcionEvaluacion = descripcion;

                return detalle;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal TipoGranja mapTipoGranja(SqlDataReader reader)
        {
            try
            {
                return new TipoGranja
                {
                    tipoGranja = (string)reader["tipoGranja_tipoGranja"],
                    idTipoGranja = (int)reader["tipoGranja_clave"],
                    activo = Convert.ToBoolean(reader["tipoGranja_activo"])
                };
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        internal TipoGranja mapTipoGranja2(SqlDataReader reader)
        {
            try
            {
                return new TipoGranja
                {
                    tipoGranja = (string)reader["tipoGranja2_tipoGranja"],
                    idTipoGranja = (int)reader["tipoGranja2_clave"],
                    activo = Convert.ToBoolean(reader["tipoGranja2_activo"])
                };
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal Presupuesto mapPresupuesto(SqlDataReader reader)
        {
            try
            {
                Presupuesto presupuesto = new Presupuesto
                {
                    idPresupuesto = (int)reader["presupuesto_clave"],
                    anio = (int)reader["presupuesto_anio"],
                    empresa = new Empresa
                    {
                        clave = (int)reader["emp_clave"],
                        nombre = (string)reader["emp_nombre"],
                    },
                    estatus = (bool)reader["presupuesto_activo"],
                    fechaRegistro = (DateTime)reader["presupuesto_fechaRegistro"],
                    fechaBaja = DBNull.Value.Equals(reader["presupuesto_fechaBaja"]) ? null : (DateTime?)reader["presupuesto_fechaBaja"],
                    listaDetalles = new List<DetallePresupuesto>(),
                    //usuario = (string)reader[""],
                    usuarioBaja = DBNull.Value.Equals(reader["presupuesto_usuarioBaja"]) ? null : (string)reader["presupuesto_usuarioBaja"],
                };

                ZonaOperativa zona = mapZonaOperativa(reader);
                if (zona == null) return null;

                presupuesto.sucursal = zona;

                Cliente cliente = mapCliente(reader);
                if (cliente == null) return null;

                presupuesto.cliente = cliente;

                return presupuesto;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public DetallePresupuesto mapDetallePresupuesto(SqlDataReader reader)
        {
            try
            {
                DetallePresupuesto detalle = new DetallePresupuesto
                {
                    idDetallePresupuesto = (int)reader["detalle_idDetalle"],
                    idPresupuesto = (int)reader["detalle_idPresupuesto"],
                    enero = (decimal)reader["detalle_enero"],
                    febrero = (decimal)reader["detalle_febrero"],
                    marzo = (decimal)reader["detalle_marzo"],
                    abril = (decimal)reader["detalle_abril"],
                    mayo = (decimal)reader["detalle_mayo"],
                    junio = (decimal)reader["detalle_junio"],
                    julio = (decimal)reader["detalle_julio"],
                    agosto = (decimal)reader["detalle_agosto"],
                    septiembre = (decimal)reader["detalle_septiembre"],
                    octubre = (decimal)reader["detalle_octubre"],
                    noviembre = (decimal)reader["detalle_noviembre"],
                    diciembre = (decimal)reader["detalle_diciembre"],
                    indicador = (Indicador)reader["detalle_indicador"]

                };

                if (!DBNull.Value.Equals(reader["modalidad_clave"]))
                {
                    Modalidades modalidad = mapModalidad(reader);
                    if (modalidad == null) return null;
                    detalle.modalidad = modalidad;
                }

                return detalle;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal ReporteIndicadorViajes mapReporteIndicadorViajes(SqlDataReader reader)
        {
            return new ReporteIndicadorViajes
            {
                idCliente = (int)reader["idCliente"],
                cliente = (string)reader["Cliente"],
                modalidad = DBNull.Value.Equals(reader["Modalidad"]) ? string.Empty : (string)reader["Modalidad"],
                subModalidad = (decimal)reader["SubModalidad"],
                eneroTon = (decimal)reader["EneroTon"],
                febreroTon = (decimal)reader["FebreroTon"],
                marzoTon = (decimal)reader["MarzoTon"],
                abrilTon = (decimal)reader["AbrilTon"],
                mayoTon = (decimal)reader["MayoTon"],
                junioTon = (decimal)reader["JunioTon"],
                julioTon = (decimal)reader["JulioTon"],
                agostoTon = (decimal)reader["AgostoTon"],
                septiembreTon = (decimal)reader["SeptiembreTon"],
                octubreTon = (decimal)reader["OctubreTon"],
                noviembreTon = (decimal)reader["NoviembreTon"],
                diciembreTon = (decimal)reader["DiciembreTon"],
                eneroVen = (decimal)reader["EneroVen"],
                febreroVen = (decimal)reader["FebreroVen"],
                marzoVen = (decimal)reader["MarzoVen"],
                abrilVen = (decimal)reader["AbrilVen"],
                mayoVen = (decimal)reader["MayoVen"],
                junioVen = (decimal)reader["JunioVen"],
                julioVen = (decimal)reader["JulioVen"],
                agostoVen = (decimal)reader["AgostoVen"],
                septiembreVen = (decimal)reader["SeptiembreVen"],
                octubreVen = (decimal)reader["OctubreVen"],
                noviembreVen = (decimal)reader["NoviembreVen"],
                diciembreVen = (decimal)reader["DiciembreVen"],
            };
        }
        internal ReporteIndicadorCombustible mapReporteIndicadorCombustible(SqlDataReader reader)
        {
            return new ReporteIndicadorCombustible
            {
                idCliente = (int)reader["idCliente"],
                cliente = (string)reader["Cliente"],
                modalidad = DBNull.Value.Equals(reader["Modalidad"]) ? string.Empty : (string)reader["Modalidad"],
                subModalidad = (decimal)reader["SubModalidad"],
                eneroKm = (decimal)reader["EneroKm"],
                febreroKm = (decimal)reader["FebreroKm"],
                marzoKm = (decimal)reader["MarzoKm"],
                abrilKm = (decimal)reader["AbrilKm"],
                mayoKm = (decimal)reader["MayoKm"],
                junioKm = (decimal)reader["JunioKm"],
                julioKm = (decimal)reader["JulioKm"],
                agostoKm = (decimal)reader["AgostoKm"],
                septiembreKm = (decimal)reader["SeptiembreKm"],
                octubreKm = (decimal)reader["OctubreKm"],
                noviembreKm = (decimal)reader["NoviembreKm"],
                diciembreKm = (decimal)reader["DiciembreKm"],
                eneroLitro = (decimal)reader["EneroLitro"],
                febreroLitro = (decimal)reader["FebreroLitro"],
                marzoLitro = (decimal)reader["MarzoLitro"],
                abrilLitro = (decimal)reader["AbrilLitro"],
                mayoLitro = (decimal)reader["MayoLitro"],
                junioLitro = (decimal)reader["JunioLitro"],
                julioLitro = (decimal)reader["JulioLitro"],
                agostoLitro = (decimal)reader["AgostoLitro"],
                septiembreLitro = (decimal)reader["SeptiembreLitro"],
                octubreLitro = (decimal)reader["OctubreLitro"],
                noviembreLitro = (decimal)reader["NoviembreLitro"],
                diciembreLitro = (decimal)reader["DiciembreLitro"],
            };
        }
        internal Modalidades mapModalidad(SqlDataReader reader)
        {
            try
            {
                return new Modalidades
                {
                    idModalidad = (int)reader["modalidad_clave"],
                    modalidad = (string)reader["modalidad_modalidad"],
                    subModalidad = (int)reader["modalidad_subModalidad"],
                    unidadMedida = (string)reader["modalidad_unidadMedida"],
                };
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal CitaMantenimiento mapCitaMatto(SqlDataReader sqlReader)
        {
            try
            {
                CitaMantenimiento cita = new CitaMantenimiento
                {
                    idCitaMantenimiento = (int)sqlReader["cita_clave"],
                    fechaCaptura = (DateTime)sqlReader["cita_fechaCaptura"],
                    fechaCita = (DateTime)sqlReader["cita_fechaCita"],
                    usuario = (string)sqlReader["cita_usuario"],
                    activo = (bool)sqlReader["cita_activo"]
                };

                UnidadTransporte unidad = mapUnidadesv2(sqlReader);
                if (unidad == null) return null;
                cita.unidad = unidad;

                Servicio servicio = mapServicio(sqlReader);
                if (servicio == null) return null;
                cita.servicio = servicio;

                return cita;

            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal TipoAnticipo mapTipoAnticipo(SqlDataReader sqlReader)
        {
            try
            {
                TipoAnticipo tipoAnticipo = new TipoAnticipo
                {
                    idTipoAnticipo = (int)sqlReader["tipoAnticipo_clave"],
                    anticipo = (string)sqlReader["tipoAnticipo_anticipo"],
                    activo = (bool)sqlReader["tipoAnticipo_activo"],
                    obs = (bool)sqlReader["tipoAnticipo_obs"]
                };
                return tipoAnticipo;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal Colonia mapColonia(SqlDataReader sqlReader)
        {
            try
            {
                Colonia colonia = new Colonia
                {
                    idColonia = (int)sqlReader["colonia_id"],
                    nombre = (string)sqlReader["colonia_nombre"],
                    cp = (string)sqlReader["colonia_cp"]
                };
                colonia.ciudad = mapCiudad(sqlReader);
                return colonia;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal CargaTicketDiesel mapCargaTicketDiesel(SqlDataReader reader)
        {
            try
            {
                CargaTicketDiesel carga = new CargaTicketDiesel
                {
                    clave = (int)reader["idCargaTicketDiesel"],
                    empresa = new Empresa { clave = (int)reader["carga_empresa"] },
                    zonaOperativa = mapZonaOperativa(reader),
                    tractor = mapUnidades("", reader),
                    proveedor = mapProveedorV2(reader),
                    ticket = (string)reader["ticket"],
                    precio = (decimal)reader["precio"],
                    litros = (decimal)reader["litros"],
                    folioImpreso = (string)reader["folioImpreso"],
                    observaciones = (string)reader["observaciones"],
                    operador = new Personal { clave = (int)reader["carga_idOperador"] },
                    carga = DBNull.Value.Equals(reader["idValeCarga"]) ? null : new CargaCombustible { idCargaCombustible = (int)reader["idValeCarga"] }
                };
                return carga;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public Ciudad mapCiudad(SqlDataReader sqlReader)
        {
            try
            {
                Ciudad ciudad = new Ciudad
                {
                    idCiudad = (int)sqlReader["ciudad_id"],
                    nombre = (string)sqlReader["ciudad_nombre"]
                };
                ciudad.estado = mapEstados(sqlReader);
                return ciudad;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal Puesto mapPuesto(SqlDataReader sqlReader)
        {
            try
            {
                Puesto puesto = new Puesto
                {
                    idPuesto = (int)sqlReader["puesto_id"],
                    nombrePuesto = (string)sqlReader["puesto_nombre"],
                    activo = (bool)sqlReader["puesto_activo"]
                };
                return puesto;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal NivelesTanque mapNivelTanque(SqlDataReader sqlReader)
        {
            try
            {
                NivelesTanque nivelesTanque = new NivelesTanque
                {
                    idNivel = (int)sqlReader["nivel_idNivelTanque"],
                    fecha = (DateTime)sqlReader["nivel_fecha"],
                    fechaInicio = (DateTime)sqlReader["nivel_fechaInicio"],
                    fechaFinal = DBNull.Value.Equals(sqlReader["nivel_fechaFinal"]) ? null : (DateTime?)sqlReader["nivel_fechaFinal"],
                    idPersonaAbre = (int)sqlReader["nivel_idPersonalAbre"],
                    personaAbre = (string)sqlReader["nivel_persona_abre"],
                    idPersonaCierra = DBNull.Value.Equals(sqlReader["nivel_idPersonalCierra"]) ? null : (int?)sqlReader["nivel_idPersonalCierra"],
                    personaCierra = (string)sqlReader["nivel_persona_cierra"]
                };
                TanqueCombustible tanqueCombustible = mapTanqueCombustible(sqlReader);
                if (tanqueCombustible == null)
                {
                    return null;
                }
                nivelesTanque.tanqueCombustible = tanqueCombustible;
                return nivelesTanque;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal ConfiguracionEnvioCorreo mapCorreosElectronicos(SqlDataReader sqlReader)
        {
            try
            {
                ConfiguracionEnvioCorreo envioCorreo = new ConfiguracionEnvioCorreo
                {
                    idConfiguracionEnvioCorreo = (int)sqlReader["correo_clave"],
                    direccion = (string)sqlReader["correo_direccion"],
                    dominio = (string)sqlReader["correo_dominio"],
                    enumDominios = (enumDominiosCorreos)sqlReader["correo_tipoDominio"],
                    activo = Convert.ToBoolean(sqlReader["correo_activo"]),
                    nomDes = (string)sqlReader["correo_nomDes"],
                    listaProcesosEnvioCorreo = new List<enumProcesosEnvioCorreo>()
                };
                return envioCorreo;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal MotivoCancelacion mapMotivoCancelacion(SqlDataReader sqlReader)
        {
            try
            {
                MotivoCancelacion motivo = new MotivoCancelacion
                {
                    idMotivoCancelacion = (int)sqlReader["motivo_clave"],
                    activo = (bool)sqlReader["motivo_activo"],
                    descripcion = (string)sqlReader["motivo_descripcion"],
                    requiereObservacion = (bool)sqlReader["motivo_requiere"],
                    usuario = (string)sqlReader["motivo_usuario"],
                    listaProcesos = new List<enumProcesos>()
                };
                return motivo;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal Departamento mapDepartamento(SqlDataReader sqlReader)
        {
            try
            {
                Departamento departamento = new Departamento
                {
                    clave = (int)sqlReader["departamento_id"],
                    descripcion = (string)sqlReader["departamento_nombre"],
                    activo = (bool)sqlReader["departamento_activo"]
                };
                return departamento;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal FacturaProveedorCombustible mapFacturaProveedor(SqlDataReader reader)
        {
            try
            {
                FacturaProveedorCombustible fact = new FacturaProveedorCombustible
                {
                    idFacturaProveedorCombustible = (int)reader["idFacturaProveedorCombustible"],
                    factura = (string)reader["factura"],
                    folioFiscal = (string)reader["folioFical"],
                    usuario = (string)reader["usuario"],
                    fechaCaptura = (DateTime)reader["fechaCaptura"],
                    fechaFactura = (DateTime)reader["fechaFactura"],
                    tipoCombustible = new TipoCombustible
                    {
                        idTipoCombustible = (int)reader["idTipoCombustible"],
                        tipoCombustible = (string)reader["tipoCombustible"]
                    },
                    listaDetalles = new List<DetalleFacturaProveedorCombustible>()
                };
                fact.empresa = mapEmpresa(reader);
                fact.zonaOperativa = mapZonaOperativa(reader);
                fact.proveedor = mapProveedorV2(reader);

                return fact;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal Flota mapFlota(SqlDataReader sqlReader)
        {
            try
            {
                Flota flota = new Flota
                {
                    idFlota = (int)sqlReader["idFlota"],
                    empresa = mapEmpresa(sqlReader),
                    zonaOperativa = mapZonaOperativa(sqlReader),
                    cliente = DBNull.Value.Equals(sqlReader["cliente_idCliente"]) ? null : mapCliente(sqlReader)
                };
                flota.tractor = DBNull.Value.Equals(sqlReader["idTractor"]) ? null : new UnidadTransporte
                {
                    clave = (string)sqlReader["idTractor"]
                };
                flota.remolque1 = DBNull.Value.Equals(sqlReader["idRemolque1"]) ? null : new UnidadTransporte
                {
                    clave = (string)sqlReader["idRemolque1"]
                };
                flota.dolly = DBNull.Value.Equals(sqlReader["idDolly"]) ? null : new UnidadTransporte
                {
                    clave = (string)sqlReader["idDolly"]
                };
                flota.remolque2 = DBNull.Value.Equals(sqlReader["idRemolque2"]) ? null : new UnidadTransporte
                {
                    clave = (string)sqlReader["idRemolque2"]
                };
                return flota;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal DetalleFacturaProveedorCombustible mapDetalleFacturaProveedor(SqlDataReader reader)
        {
            try
            {
                DetalleFacturaProveedorCombustible detalle = new DetalleFacturaProveedorCombustible
                {
                    idDetallaFacturaProveedorCombustible = (int)reader["idDetalleFacturaProveedorCombustible"],
                    idFacturaProveedorCombustible = (int)reader["idFacturaProveedorCombustible"],
                    idCargaCombustible = (int)reader["idCargaCombustible"],
                    ticket = (string)reader["ticket"],
                    litros = (decimal)reader["litros"],
                    precioNeto = (decimal)reader["precio"],
                    importe = (decimal)reader["importe"],
                    subTotalIEPS = (decimal)reader["subTotal"],
                    iva = (decimal)reader["iva"],
                    IEMPS = (decimal)reader["IEPS"],
                    activo = (bool)reader["activo"],
                    normal = (bool)reader["normal"],
                    unidadTrans = (string)reader["idUnidadTrans"],
                    folioImpreso = (string)reader["folioImpreso"],
                    idOrdenServ = (int)reader["idOrdenServ"],
                    fecha = (DateTime)reader["fecha"],
                    viajes = (string)reader["viajes"],
                    operador = (string)reader["operador"],
                    idCliente = DBNull.Value.Equals(reader["idCliente"]) ? null : (int?)reader["idCliente"],
                    cliente = (string)reader["operacion"]
                };
                return detalle;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal DetalleNivelesTanque mapDetalleNivelTanque(SqlDataReader sqlReader)
        {
            try
            {
                DetalleNivelesTanque detalleNivelesTanque = new DetalleNivelesTanque
                {
                    idDetalleNivel = (int)sqlReader["detalle_idDetalleNivelesTanque"],
                    idNivel = (int)sqlReader["detalle_idNivelTanque"],
                    nivelInicial = (decimal)sqlReader["detalle_nivelInicial"],
                    nivelFinal = DBNull.Value.Equals(sqlReader["detalle_nivelFinal"]) ? null : (decimal?)sqlReader["detalle_nivelFinal"]
                };
                BombasCombustible bombas = mapBomba(sqlReader);
                if (bombas == null)
                {
                    return null;
                }
                detalleNivelesTanque.bombasCombustible = bombas;
                return detalleNivelesTanque;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        internal ProveedorCombustible mapProveedorV2(SqlDataReader sqlReader)
        {
            try
            {
                ProveedorCombustible proveedor = new ProveedorCombustible
                {
                    idProveedor = (int)sqlReader["proveedorV2_clave"],
                    nombre = (string)sqlReader["proveedorV2_razonSocial"],
                    RFC = (string)sqlReader["proveedorV2_RFC"],
                    direccion = (string)sqlReader["proveedorV2_direccion"],
                    telefono = (string)sqlReader["proveedorV2_telefono"],
                    estatus = (bool)sqlReader["proveedorV2_estatus"],
                    precio = (decimal)sqlReader["proveedorV2_precio"],
                    vehiculos = (bool)sqlReader["proveedorV2_vehiculo"],
                    llantasNuevas = (bool)sqlReader["proveedorV2_llantasNuevas"],
                    renovadorLlantas = (bool)sqlReader["proveedorV2_renovadorLlantas"],
                    reparadorLlantas = (bool)sqlReader["proveedorV2_reparadorLlantas"],
                    diesel = (bool)sqlReader["proveedorV2_diesel"],
                };
                Colonia colonia = null;
                if (!DBNull.Value.Equals(sqlReader["colonia_id"]))
                {
                    colonia = mapColonia(sqlReader);
                }
                proveedor.colonia = colonia;
                return proveedor;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal BombasCombustible mapBomba(SqlDataReader sqlReader)
        {
            try
            {
                return new BombasCombustible
                {
                    idBombaCombustible = (int)sqlReader["bomba_idBombda"],
                    idTanque = (int)sqlReader["bomba_idTanque"],
                    numero = (int)sqlReader["bomba_numero"],
                    activo = (bool)sqlReader["bomba_activo"]
                };
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public TanqueCombustible mapTanqueCombustible(SqlDataReader sqlReader)
        {
            try
            {
                TanqueCombustible tanqueCombustible = new TanqueCombustible
                {
                    idTanqueCombustible = (int)sqlReader["tanque_idTanqueCombustible"],
                    nombre = (string)sqlReader["tanque_nombre"],
                    activo = (bool)sqlReader["tanque_activo"]
                };
                ZonaOperativa zonaOperativa = mapZonaOperativa(sqlReader);
                if (zonaOperativa == null)
                {
                    return null;
                }
                tanqueCombustible.zonaOperativa = zonaOperativa;
                return tanqueCombustible;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal MesContable mapMesContable(SqlDataReader sqlReader)
        {
            try
            {
                return new MesContable
                {
                    idMesContable = (int)sqlReader["mes_idMesContable"],
                    idEmpresa = (int)sqlReader["mes_idEmpresa"],
                    mes = (int)sqlReader["mes_mes"],
                    anio = (int)sqlReader["mes_anio"],
                    fechaInicio = (DateTime)sqlReader["mes_fechaInicio"],
                    fechaFin = DBNull.Value.Equals(sqlReader["mes_fechaFin"]) ? null : (DateTime?)sqlReader["mes_fechaFin"],
                };
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal Caseta mapCaseta(SqlDataReader sqlReader)
        {
            try
            {
                Caseta caseta = new Caseta
                {
                    idCaseta = (int)sqlReader["idCaseta"],
                    via = (string)sqlReader["via"],
                    nombreCaseta = (string)sqlReader["nombreCaseta"],
                    vigencia = !DBNull.Value.Equals("vigencia") ? null : (DateTime?)sqlReader["vigencia"],
                    estatus = (bool)sqlReader["casetaActivo"],
                    costo3Ejes = (decimal)sqlReader["costo3Ejes"],
                    costo5Ejes = (decimal)sqlReader["costo5Ejes"],
                    costo6Ejes = (decimal)sqlReader["costo6Ejes"],
                    costo9Ejes = (decimal)sqlReader["costo9Ejes"],
                    paginaFacturacion = (string)sqlReader["paginaFacturacion"]
                };
                Estado estado = mapEstados(sqlReader);
                if (estado == null)
                {
                    return null;
                }
                else
                {
                    caseta.estado = estado;
                }
                return caseta;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal TipoUnidad mapTipoUnidad(SqlDataReader sqlReader)
        {
            try
            {
                TipoUnidad tipoUnidad = new TipoUnidad
                {
                    clave = (int)sqlReader["tipoUnidad_clave"],
                    descripcion = (string)sqlReader["tipoUnidad_descripcion"],
                    bCombustible = (bool)sqlReader["tipoUnidad_combustible"],
                    TonelajeMax = Convert.ToDecimal(sqlReader["tipoUnidad_tonMax"]),
                    clasificacion = (string)sqlReader["tipoUnidad_clasificacion"],
                    cantRefacciones = (int)sqlReader["tipoUnidad_noRefacciones"],
                    listaDescripcionLlantas = new List<DescripcionLlantas>(),
                    activo = Convert.ToBoolean(sqlReader["tipoUnidad_activo"])
                };
                return tipoUnidad;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal SuministroCombustible mapSuministro(SqlDataReader sqlReader)
        {
            try
            {
                SuministroCombustible suministro = new SuministroCombustible
                {
                    idSuministroCombustible = (int)sqlReader["suministro_clave"],
                    fecha = (DateTime)sqlReader["suministro_fecha"],
                    ltsCombustible = (decimal)sqlReader["suministro_litros"],
                    usuario = new Usuario { idUsuario = (int)sqlReader["suministro_idUsuario"] },
                    factura = (string)sqlReader["suministro_factura"],
                    importe = (decimal)sqlReader["suministro_importe"],
                    noPipa = (string)sqlReader["suministro_noPipa"],
                    placasPipa = (string)sqlReader["suministro_placasPipa"],
                    operador = (string)sqlReader["suministro_operador"],
                    fechaRecibe = (DateTime)sqlReader["suministro_fechaRecibe"]
                };
                TanqueCombustible tanque = mapTanqueCombustible(sqlReader);
                if (tanque != null)
                {
                    suministro.tanqueCombustible = tanque;
                }
                else
                {
                    return null;
                }

                Personal personal = mapPersonal(sqlReader);
                if (personal != null)
                {
                    suministro.PersonalRecibe = personal;
                }
                else
                {
                    return null;
                }

                return suministro;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal DetalleSuminstroCombustible mapDetallesSuministro(SqlDataReader sqlReader)
        {
            try
            {
                DetalleSuminstroCombustible detalleSuminstro = new DetalleSuminstroCombustible
                {
                    idDetalleSuministroCombustible = (int)sqlReader["detalleSuministro_clave"],
                    idSuministroCombustible = (int)sqlReader["detalleSuministro_idSuministro"],
                    ltsCombustible = (decimal)sqlReader["detalleSuministro_litros"]
                };
                Empresa empresa = mapEmpresa(sqlReader);
                if (empresa == null) return null;

                detalleSuminstro.empresa = empresa;

                return detalleSuminstro;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal DescripcionLlantas mapDescripcionLlantas(SqlDataReader sqlReader)
        {
            try
            {
                DescripcionLlantas descripcion = new DescripcionLlantas
                {
                    idDescripcionLlantas = (int)sqlReader["idConfigEjesLlantas"],
                    idTipoUnidad = (int)sqlReader["idTipoUniTrans"],
                    noEje = (int)sqlReader["noEje"],
                    cantLlantas = (int)sqlReader["noLlantas"],
                    tipoEjeLlanta = (enumTipoEjeLlanta)sqlReader["tipoEje"]
                };
                return descripcion;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal ZonaOperativa mapZonaOperativa(SqlDataReader sqlReader)
        {
            try
            {
                return new ZonaOperativa
                {
                    idZonaOperativa = (int)sqlReader["zonaO_idZonaOperativa"],
                    nombre = (string)sqlReader["zonaO_nombre"],
                    activo = (bool)sqlReader["zonaO_activo"]
                };
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal OperacionFletera mapOperacionFletera(SqlDataReader sqlReader)
        {
            try
            {
                OperacionFletera operacion = new OperacionFletera();
                operacion.cliente = mapCliente(sqlReader);
                operacion.empresa = mapEmpresa(sqlReader);
                operacion.zonaOperativa = mapZonaOperativa(sqlReader);
                return operacion;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal Tramo mapTramo(SqlDataReader sqlReader)
        {
            try
            {
                return new Tramo
                {
                    idTramo = (int)sqlReader["idTramo"],
                    km = (decimal)sqlReader["km"],
                    destino = mapDestinoCorto(sqlReader),
                    origen = mapOrigen(sqlReader)
                };
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal Base mapBase(SqlDataReader sqlReader)
        {
            try
            {
                Base base1 = new Base
                {
                    idBase = (int)sqlReader["base_idBase"],
                    nombreBase = (string)sqlReader["base_nombreBase"],
                    activo = (bool)sqlReader["base_activo"]
                };
                Estado estado1 = new Estado
                {
                    idEstado = (int)sqlReader["base_estado_idEstado"],
                    nombre = (string)sqlReader["base_estado_nombre"]
                };
                Pais pais1 = new Pais
                {
                    idPais = (int)sqlReader["base_pais_idPais"],
                    nombrePais = (string)sqlReader["base_pais_nombre"]
                };
                estado1.pais = pais1;
                base1.estado = estado1;
                return base1;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal Ruta mapRutas(SqlDataReader sqlReader)
        {
            try
            {
                Ruta ruta1 = new Ruta
                {
                    idRuta = (int)sqlReader["idRuta"],
                    cobroPorViaje = (bool)sqlReader["cobroPorViaje"],
                    km = (decimal)sqlReader["ruta_km"],
                    origen = this.mapOrigen(sqlReader),
                    destino = this.mapDestinoCorto(sqlReader)
                };
                TarifaRuta ruta3 = new TarifaRuta
                {
                    precioSencillo = (decimal)sqlReader["tarifaSencillo"],
                    precioFull = (decimal)sqlReader["tarifaFull"]
                };
                ruta1.tarifaRuta = ruta3;
                PagoOperador operador1 = new PagoOperador
                {
                    precioSencillo = (decimal)sqlReader["pagoOperadorSencillo"],
                    precioFull = (decimal)sqlReader["pagoOperadorFull"],
                    tipoPago = (ClasificacionPagoChofer)sqlReader["tipoPagoOperador"]
                };
                ruta1.pagoOperador = operador1;
                CasetaRuta ruta4 = new CasetaRuta
                {
                    incluyeCaseta = (bool)sqlReader["incluyeCaseta"],
                    precio = (decimal)sqlReader["costoCaseta"]
                };
                ruta1.casetas = ruta4;
                ruta1.activo = (bool)sqlReader["ruta_activo"];
                ruta1.clasificacion = DBNull.Value.Equals(sqlReader["idClasificacionProducto"]) ? null : new ClasificacionProductos()
                {
                    idClasificacionProducto = (int)sqlReader["idClasificacionProducto"],
                    descripcion = (string)sqlReader["clas_clasificacion"],
                    diesel = Convert.ToBoolean(sqlReader["diesel"])
                };
                return ruta1;
            }
            catch (Exception)
            {
                return null;
            }
        }

        internal ReporteUsuarios mapReporteUsuarios(SqlDataReader sqlReader, bool conPrivilegios = false)
        {
            try
            {
                var usuario = new ReporteUsuarios
                {
                    idUsuario = (int)sqlReader["idUsuario"],
                    nombreUsuario = (string)sqlReader["nombreUsuario"],
                    contraseña = (string)sqlReader["contrasena"],
                    personal = (string)sqlReader["nombre_personal"],
                    zonaOperativa = (string)sqlReader["zona_nombre"],
                    cliente = (string)sqlReader["nombreCliente"],
                    empresa = (string)sqlReader["empresa_nombre"],
                    estatus = (string)sqlReader["usuario_estatus"],
                };

                if (conPrivilegios)
                {
                    ReportePrivilegios privilegios = mapReportePrivilegios(sqlReader);
                    if (privilegios == null)
                        return null;
                    else
                        usuario.privilegios = privilegios;
                }

                return usuario;

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        internal ReportePrivilegios mapReportePrivilegios(SqlDataReader sqlReader)
        {
            try
            {
                return new ReportePrivilegios
                {
                    idPrivilegio = (int)sqlReader["privilegio_id"],
                    clave = (string)sqlReader["privilegio_clave"],
                    titulo = (string)sqlReader["privilegio_titulo"],
                    descripcion = (string)sqlReader["privilegio_descripcion"],
                    estatus = (string)sqlReader["privilegio_estatus"]

                };
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal TableroControlUnidades mapTableroControlUnidades(SqlDataReader sqlReader)
        {
            int idTableroControlUnidades = (int)sqlReader["idTableroControlUnidades"];
            if (idTableroControlUnidades == 18230)
            {

            }
            try
            {
                return new TableroControlUnidades
                {
                    idTableroControlUnidades = (int)sqlReader["idTableroControlUnidades"],
                    numGuiaId = (int)sqlReader["numGuiaId"],
                    fechaPrimeraEntradaPlanta = DBNull.Value.Equals(sqlReader["ultimaEntradaPlanta"]) ? null : (DateTime?)sqlReader["ultimaEntradaPlanta"],
                    fechaSalidaPlanta = DBNull.Value.Equals(sqlReader["fechaSalidaPlanta"]) ? null : (DateTime?)sqlReader["fechaSalidaPlanta"],
                    fechaLlegadaDestino = DBNull.Value.Equals(sqlReader["fechaLlegadaDestino"]) ? null : (DateTime?)sqlReader["fechaLlegadaDestino"],
                    fechaSalidaDestino = DBNull.Value.Equals(sqlReader["fechaSalidaDestino"]) ? null : (DateTime?)sqlReader["fechaSalidaDestino"],
                    fechaEntradaPlanta = DBNull.Value.Equals(sqlReader["fechaEntradaPlanta"]) ? null : (DateTime?)sqlReader["fechaEntradaPlanta"],
                    fechaUltimaEntradaPlanta = DBNull.Value.Equals(sqlReader["fechaUltimaEntradaPlanta"]) ? null : (DateTime?)sqlReader["fechaUltimaEntradaPlanta"],
                    listaDetalles = new List<DetalleTableroControlUnidades>(),
                    fechaOS = DBNull.Value.Equals(sqlReader["fechaOS"]) ? null : (DateTime?)sqlReader["fechaOS"],
                    UltimoNumGuiaId = (int)sqlReader["viajeAnterior"],
                    fechaEntradaTallerTrayectoDestino = DBNull.Value.Equals(sqlReader["fechaEntradaTallerTrayectoDestino"]) ? null : (DateTime?)sqlReader["fechaEntradaTallerTrayectoDestino"],
                    fechaSalidaTallerTrayectoDestino = DBNull.Value.Equals(sqlReader["fechaSalidaTallerTrayectoDestino"]) ? null : (DateTime?)sqlReader["fechaSalidaTallerTrayectoDestino"],
                    fechaEntradaTallerTrayectoPlanta = DBNull.Value.Equals(sqlReader["fechaEntradaTallerTrayectoPlanta"]) ? null : (DateTime?)sqlReader["fechaEntradaTallerTrayectoPlanta"],
                    fechaSalidaTallerTrayectoPlanta = DBNull.Value.Equals(sqlReader["fechaSalidaTallerTrayectoPlanta"]) ? null : (DateTime?)sqlReader["fechaSalidaTallerTrayectoPlanta"],
                    taller = Convert.ToBoolean(sqlReader["taller"]),
                    tallerExterno = Convert.ToBoolean(sqlReader["tallerExterno"]),
                    geoCerca = DBNull.Value.Equals(sqlReader["geoCerca"]) ? string.Empty : (string)sqlReader["geoCerca"],
                    operacion = (string)sqlReader["operacion"],
                    listaComentarios = new List<ComentarioTablero>()
                };
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal ClasificacionRutas mapClasificacion(SqlDataReader sqlReader)
        {
            try
            {
                ClasificacionRutas clasificacion = new ClasificacionRutas
                {
                    idClasificacion = (int)sqlReader["idClasificacion"],
                    clasificacion = Convert.ToChar(sqlReader["clave"]),
                    kmMenor = (decimal)sqlReader["kmMenor"],
                    kmMayor = (decimal)sqlReader["kmMayor"],
                    estatus = Convert.ToBoolean(sqlReader["estatus"])
                };

                Cliente cliente = mapCliente(sqlReader);
                if (cliente == null)
                {
                    return null;
                }
                clasificacion.cliente = cliente;
                return clasificacion;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal DetalleTableroControlUnidades mapDetalleTableroControlUnidades(SqlDataReader sqlReader)
        {
            try
            {
                return new DetalleTableroControlUnidades
                {
                    idDetalleTableroControlUnidades = (int)sqlReader["idDetalleTableroControlUnidades"],
                    idTableroControlUnidades = (int)sqlReader["idTableroControlUnidades"],
                    consecutivo = (int)sqlReader["consecutivo"],
                    tiempo = (int)sqlReader["tiempo"],
                    fechaLLegada = DBNull.Value.Equals(sqlReader["fechaLLegada"]) ? null : (DateTime?)sqlReader["fechaLLegada"],
                    fechaSalida = DBNull.Value.Equals(sqlReader["fechaSalida"]) ? null : (DateTime?)sqlReader["fechaSalida"]
                };
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal SueldoFijo mapSueldoFijo(SqlDataReader sqlReader)
        {
            try
            {
                SueldoFijo sueldo = new SueldoFijo
                {
                    idSueldoFijo = (int)sqlReader["idSueldoFijo"],
                    importe = (decimal)sqlReader["importe"],
                    estatus = Convert.ToBoolean(sqlReader["estatus"]),
                    autoriza = (string)sqlReader["autoriza"]
                };
                Personal operador = mapPersonal(sqlReader);
                if (operador == null)
                {
                    return null;
                }
                sueldo.operador = operador;
                return sueldo;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal InsumosOrdenTrabajo mapInsumosOrdenTrabajo(SqlDataReader sqlReader)
        {
            try
            {
                InsumosOrdenTrabajo insumosOrdenTrabajo = new InsumosOrdenTrabajo()
                {
                    idPreSalida = (int)sqlReader["IdPreSalida"],
                    idOrdenSer = (int)sqlReader["idOrdenSer"],
                    idOrdenTrabajo = (int)sqlReader["idOrdenTrabajo"],
                    idUnidadTransporte = (string)sqlReader["idUnidadTrans"],
                    fechaSolicitud = (DateTime)sqlReader["FecSolicitud"],
                    fechaEntrega = DBNull.Value.Equals(sqlReader["FecEntrega"]) ? null : (DateTime?)sqlReader["FecEntrega"],
                    idProducto = (int)sqlReader["idProducto"],
                    nombreProducto = (string)sqlReader["nombreProducto"],
                    cantidad = (decimal)sqlReader["Cantidad"],
                    estatus = (string)sqlReader["Estatus"]
                };
                return insumosOrdenTrabajo;
            }
            catch (Exception EX)
            {
                return null;
            }
        }

        internal PagoPorModalidad mapPagoPorModalidad(SqlDataReader sqlReader)
        {
            try
            {
                PagoPorModalidad pago = new PagoPorModalidad
                {
                    idPagoPorModalidad = (int)sqlReader["idPagoModalidad"],
                    noViaje = (int)sqlReader["noViaje"],
                    sencillo_24 = (decimal)sqlReader["sencillo_24"],
                    sencillo_30 = (decimal)sqlReader["sencillo_30"],
                    full = (decimal)sqlReader["full"],
                    estatus = (bool)sqlReader["modalidadEstaus"]
                };

                ClasificacionRutas cla = mapClasificacion(sqlReader);
                if (cla == null)
                {
                    return null;
                }
                pago.clasificacion = cla;

                return pago;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal CondicionLlanta mapCondicionLlanta(SqlDataReader sqlReader)
        {
            try
            {
                return new CondicionLlanta
                {
                    descripcion = (string)sqlReader["condicion_descripcion"],
                    idCondicion = (int)sqlReader["idCondicion"]
                };
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal Producto mapProductos(SqlDataReader sqlReader)
        {
            try
            {
                return new Producto
                {
                    clave_empresa = (int)sqlReader["Clave_empresa"],
                    clave_fraccion = (int)sqlReader["Clave_fraccion"],
                    clave = (int)sqlReader["Clave"],
                    descripcion = (string)sqlReader["Descripcion"],
                    embalaje = (string)sqlReader["Embalaje"],
                    clave_unidad_de_medida = (string)sqlReader["Clave_unidad_de_medida"],
                    clave_externo = (string)sqlReader["Clave_externo"],
                    combustible = DBNull.Value.Equals(sqlReader["Combustible"]) ? false : Convert.ToBoolean(sqlReader["Combustible"]),
                    clasificacion = new ClasificacionProductos
                    {
                        idClasificacionProducto = DBNull.Value.Equals(sqlReader["idClasificacionProducto"]) ? 10 : (int)sqlReader["idClasificacionProducto"],
                        descripcion = (string)sqlReader["cla_clasificacion"],
                        diesel = Convert.ToBoolean(sqlReader["diesel"])
                    }
                };
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal Zona mapDestino(SqlDataReader sqlReader)
        {
            try
            {
                if ((int)sqlReader["destino_clave"] == 1612)
                {

                }
                Zona zona = new Zona
                {
                    clave = (int)sqlReader["destino_clave"],
                    clave_empresa = (int)sqlReader["destino_empresa_clave"],
                    descripcion = (string)sqlReader["destino_descripcion"],
                    activo = (bool)sqlReader["destino_activo"],
                    costoSencillo = (decimal)sqlReader["destino_costoSencillo"],
                    costoSencillo35 = (decimal)sqlReader["destino_costo_sencillo35"],
                    costoFull = (decimal)sqlReader["destino_costoFull"],
                    km = (decimal)sqlReader["destino_km"],
                    claveZona = (string)sqlReader["destino_claveZona"],
                    claveCliente = DBNull.Value.Equals(sqlReader["destino_cliente_clave"]) ? null : (int?)sqlReader["destino_cliente_clave"],
                    claveExtra = (string)sqlReader["destino_claveExtra"],
                    correo = (string)sqlReader["destino_correo"],
                    viaje = Convert.ToBoolean(sqlReader["viaje"]),
                    viajeSencillo = (decimal)sqlReader["viajeSencillo"],
                    viajeFull = (decimal)sqlReader["viajeFull"],
                    incluyeCaseta = Convert.ToBoolean(sqlReader["incluyeCaseta"]),
                    precioCasetas = (decimal)sqlReader["costoCaseta"],
                    sencillo_24 = (decimal)sqlReader["sencillo_24"],
                    sencillo_30 = (decimal)sqlReader["sencillo_30"],
                    sencillo_35 = (decimal)sqlReader["sencillo_35"],
                    full = (decimal)sqlReader["full"],
                    clasificacionPagoChofer = (ClasificacionPagoChofer)sqlReader["ClasificacionPagoChofer"],
                    geoCerca = DBNull.Value.Equals(sqlReader["geoCerca"]) ? string.Empty : ((string)sqlReader["geoCerca"]),
                    baseDestino = DBNull.Value.Equals(sqlReader["destino_idBase"]) ? null : new Base()
                    {
                        idBase = (int)sqlReader["destino_idBase"],
                        nombreBase = (string)sqlReader["destino_nombreBase"],
                        activo = (bool)sqlReader["destino_baseActivo"]
                    },
                    clasificacion = DBNull.Value.Equals(sqlReader["idClasificacionProducto"]) ? null : new ClasificacionProductos()
                    {
                        idClasificacionProducto = (int)sqlReader["idClasificacionProducto"],
                        descripcion = (string)sqlReader["cla_clasificacion"],
                        diesel = Convert.ToBoolean(sqlReader["diesel"])
                    },
                    listaAnticipo = new List<Anticipo>(),
                    listaCasetas = new List<Caseta>()
                };
                zona.estado = this.mapEstados(sqlReader);

                if (DBNull.Value.Equals(sqlReader["cliente_idCliente"]))
                {
                    zona.cliente = null;
                }
                else
                {
                    zona.cliente = this.mapCliente(sqlReader);
                }

                zona.precioFijoSencillo = (decimal)sqlReader["precioFijoSencillo"];
                zona.precioFijoSencillo35 = (decimal)sqlReader["precioFijoSencillo35"];
                zona.precioFijoFull = (decimal)sqlReader["precioFijoFull"];
                zona.tiempoIda = (string)sqlReader["tiempoIda"];
                zona.tiempoGranja = (string)sqlReader["tiempoGranja"];
                zona.tiempoRetorno = (string)sqlReader["tiempoRetorno"];
                if (DBNull.Value.Equals(sqlReader["od_idOrigen"]))
                {
                    zona.origenPegaso = null;
                }
                else
                {
                    OrigenPegaso pegaso1 = new OrigenPegaso
                    {
                        idOrigen = (int)sqlReader["od_idOrigen"],
                        nombre = (string)sqlReader["od_nombre"],
                        idCliente = DBNull.Value.Equals(sqlReader["od_idCliente"]) ? null : (int?)sqlReader["od_idCliente"]
                    };
                    zona.origenPegaso = pegaso1;
                }

                if (DBNull.Value.Equals(sqlReader["tipoGranja_clave"]))
                {
                    zona.categoria = null;
                }
                else
                {
                    zona.categoria = mapTipoGranja(sqlReader);
                }

                if (DBNull.Value.Equals(sqlReader["tipoGranja2_clave"]))
                {
                    zona.categoria2 = null;
                }
                else
                {
                    zona.categoria2 = mapTipoGranja2(sqlReader);
                }

                return zona;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        internal DatosIngresoPersonal mapDatosIngreso(SqlDataReader reader)
        {
            DatosIngresoPersonal datosIngreso = new DatosIngresoPersonal
            {
                idDatoIngreso = (int)reader["ingreso_clave"],
                idPersonal = (int)reader["ingreso_idPersonal"],
                fechaInicio = (DateTime)reader["ingreso_fechaInicio"],
                fechaFinCapacitacion = (DateTime)reader["ingreso_finCapacitacion"],
                fechaBaja = DBNull.Value.Equals(reader["ingreso_fechaBaja"]) ? null : (DateTime?)reader["ingreso_fechaBaja"],
                usuarioBaja = DBNull.Value.Equals(reader["ingreso_usuarioBaja"]) ? string.Empty : (string)reader["ingreso_usuarioBaja"],
                motivoBaja = DBNull.Value.Equals(reader["ingreso_motivoBaja"]) ? string.Empty : (string)reader["ingreso_motivoBaja"],
                observaciones = DBNull.Value.Equals(reader["ingreso_observaciones"]) ? string.Empty : (string)reader["ingreso_observaciones"],
                salarioDiario = (decimal)reader["ingreso_salarioDiario"],
                salarioHora = (decimal)reader["ingreso_salarioHora"],
                maximoHorasExtra = (int)reader["ingreso_maximoHora"],
                tipoIngreso = (TipoIngreso)reader["ingreso_tipoIngreso"],
                usuarioRegistra = (string)reader["ingreso_usuarioRegistro"]
            };
            Puesto puesto = mapPuesto(reader);
            Departamento departamento = mapDepartamento(reader);
            if (puesto == null || departamento == null)
            {
                return null;
            }
            datosIngreso.puesto = puesto;
            datosIngreso.departamento = departamento;
            return datosIngreso;
        }

        internal Zona mapDestinoCorto(SqlDataReader sqlReader)
        {
            try
            {
                var zona = new Zona
                {
                    clave = (int)sqlReader["destino_clave"],
                    descripcion = (string)sqlReader["destino_descripcion"],
                    km = (decimal)sqlReader["destino_km"],
                    baseDestino = (DBNull.Value.Equals(sqlReader["destino_idBase"]) ? null : new Base
                    {
                        idBase = (int)sqlReader["destino_idBase"],
                        nombreBase = (string)sqlReader["destino_nombreBase"],
                        activo = (bool)sqlReader["destino_baseActivo"]
                    })
                };
                return zona;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal Estado mapEstados(SqlDataReader sqlReader)
        {
            try
            {
                Estado estado1 = new Estado
                {
                    idEstado = (int)sqlReader["estado_idEstado"],
                    nombre = (string)sqlReader["estado_NombreEstado"]
                };
                Pais pais1 = new Pais
                {
                    idPais = (int)sqlReader["estado_idPais"],
                    nombrePais = (string)sqlReader["estado_pais"]
                };
                estado1.pais = pais1;
                return estado1;
            }
            catch (Exception)
            {
                return null;
            }
        }

        internal Personal mapPersonal(SqlDataReader sqlReader, string cadena = "")
        {
            try
            {
                return new Personal
                {
                    clave = (int)sqlReader[cadena + "persona_idPersonal"],
                    nombre = (string)sqlReader[cadena + "personal_nombre"],
                    rfid = DBNull.Value.Equals(sqlReader[cadena + "personal_rfid"]) ? "" : (string)sqlReader[cadena + "personal_rfid"],
                    empresa = mapEmpresa(sqlReader, cadena),
                    cveINTELISIS = (string)sqlReader["personal_cveINTELISIS"],
                    cveAgente = DBNull.Value.Equals(sqlReader["personal_cveAgente"]) ? null : (string)sqlReader["personal_cveAgente"]
                };
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        internal Empresa mapEmpresa(SqlDataReader sqlReader, string cadena = "")
        {
            try
            {
                return new Empresa
                {
                    clave = (int)sqlReader[cadena + "empresa_idEmpresa"],
                    nombre = (string)sqlReader[cadena + "empresa_nombre"],
                    direccion = (string)sqlReader[cadena + "empresa_direccion"],
                    telefono = (string)sqlReader[cadena + "empresa_telefono"],
                    fax = (string)sqlReader[cadena + "empresa_fax"],
                    estado = (string)sqlReader[cadena + "empresa_estado"],
                    pais = (string)sqlReader[cadena + "empresa_pais"],
                    municipio = (string)sqlReader[cadena + "empresa_municipio"],
                    colonia = (string)sqlReader[cadena + "empresa_colonia"],
                    cp = (string)sqlReader[cadena + "empresa_cp"],
                    rfc = (string)sqlReader[cadena + "empresa_rfc"],
                    nombreComercial = DBNull.Value.Equals(sqlReader[cadena + "nombreComercial"]) ? "" : (string)sqlReader[cadena + "nombreComercial"],
                    cveINTELISIS = (string)sqlReader["empresa_cveINTELISIS"]
                };
            }
            catch (Exception ex)
            {
                var x = ex.Message;
                return null;
            }
        }

        internal Revitalizaciones mapRevitalizacion(SqlDataReader sqlReader)
        {
            DateTime? fecha = null;
            try
            {
                Revitalizaciones revita = new Revitalizaciones
                {
                    idRevitalizacion = (int)sqlReader["idRevitalizacion"],
                    fechaSalida = (DateTime)sqlReader["fechaInicio"],
                    fechaEntrada = DBNull.Value.Equals(sqlReader["fechaFin"]) ? fecha : (DateTime)sqlReader["fechaFin"],
                    estatus = (string)sqlReader["estatus"],
                    usuario = (string)sqlReader["usuario"]
                };
                revita.proveedor = mapProveedor(sqlReader);
                if (revita.proveedor == null)
                {
                    return null;
                }
                return revita;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal DetalleRevitalizacion mapDetalleRevitalizacion(SqlDataReader sqlReader)
        {
            try
            {
                DetalleRevitalizacion detalle = new DetalleRevitalizacion
                {
                    idDetalleRevitalizacion = (int)sqlReader["iddetalleRevitalizacion"],
                    profundidadAct = (decimal)sqlReader["profundidadAct"],
                    profundidadNueva = (decimal)sqlReader["profundidadNueva"],
                    diseño = new Diseño
                    {
                        idDiseño = (int)sqlReader["diseño_idDiseño"],
                        descripcion = (string)sqlReader["diseño_descripcion"]
                    }
                };
                detalle.llanta = mapLlanta(sqlReader);
                if (detalle.llanta == null)
                {
                    return null;
                }
                return detalle;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal RescateLlantas mapRescate(SqlDataReader sqlReader)
        {
            try
            {
                DateTime? fecha = null;
                RescateLlantas rescate = new RescateLlantas
                {
                    idRescateLlanta = (int)sqlReader["idRescateLlanta"],
                    fechaSolicitud = (DateTime)sqlReader["fechaSolicitud"],
                    fechaSalida = (DateTime)sqlReader["fechaSalida"],
                    fechaEntrada = DBNull.Value.Equals(sqlReader["fechaEntrada"]) ? fecha : (DateTime)sqlReader["fechaEntrada"],
                    estatus = (string)sqlReader["estatus"],
                    unidadTransporte = mapUnidades("", sqlReader),
                    usuario = (string)sqlReader["usuario"],
                    unicacion = (string)sqlReader["ubicacion"],
                    chofer = new Personal { clave = (int)sqlReader["idChofer"] },
                    Llantero = new Personal { clave = (int)sqlReader["idLlantero"] }
                };
                return rescate;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private OperationResult mapFraccion(int idFraccion)
        {
            try
            {
                OperationResult resp = new FraccionSvc().getFraccionALLorById(idFraccion);
                if (resp.typeResult == ResultTypes.error)
                {
                    return new OperationResult { result = null, valor = 1, mensaje = resp.mensaje };
                }
                else if (resp.typeResult == ResultTypes.recordNotFound)
                {
                    return new OperationResult { result = null, valor = 0, mensaje = resp.mensaje };
                }
                else if (resp.typeResult == ResultTypes.success)
                {
                    List<Fraccion> listCliente = (List<Fraccion>)resp.result;
                    return new OperationResult { result = listCliente[0], valor = 0, mensaje = resp.mensaje };
                }
            }
            catch (Exception e)
            {
                return new OperationResult { result = null, valor = 1, mensaje = e.Message };
            }
            return new OperationResult();
        }

        internal DetalleRescateLlanta mapDetalleRescate(SqlDataReader sqlReader)
        {
            try
            {
                DetalleRescateLlanta detalle = new DetalleRescateLlanta
                {
                    idDetalleRescateLlanta = (int)sqlReader["idDetalleRescateLlantas"],
                    llantaLleva = new Llanta { idLlanta = (int)sqlReader["idLlantaLleva"] },
                    llantaTrae = DBNull.Value.Equals(sqlReader["idLlantaTrae"]) ? null : new Llanta { idLlanta = (int)sqlReader["idLlantaTrae"] },
                    posicion = (int)sqlReader["posicion"],
                    profundidad = (decimal)sqlReader["profundidad"],
                    condicion = DBNull.Value.Equals(sqlReader["idCondicion"]) ? null : mapCondicionLlanta(sqlReader)
                };

                if (!DBNull.Value.Equals(sqlReader["idMotivoRescate"]))
                {
                    detalle.motivoRescate = mapMotivoRescate(sqlReader);
                    if (detalle.motivoRescate == null)
                    {
                        return null;
                    }
                }
                else
                {
                    detalle.motivoRescate = null;
                }

                return detalle;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal Origen mapOrigen(SqlDataReader sqlReader)
        {
            try
            {
                return new Origen
                {
                    idOrigen = (int)sqlReader["idOrigen"],
                    nombreOrigen = (string)sqlReader["nombreOrigen"],
                    estado = (string)sqlReader["estado"],
                    clasificacion = (string)sqlReader["clasificacion"],
                    km = (decimal)sqlReader["km"],
                    activo = (bool)sqlReader["activo"],
                    baseOrigen = DBNull.Value.Equals(sqlReader["base_idBase"]) ? null : this.mapBase(sqlReader)
                };
            }
            catch (Exception)
            {
                return null;
            }
        }

        internal UnidadTransporte mapUnidades(string tipoUnidad, SqlDataReader sqlReader)
        {
            try
            {
                return new UnidadTransporte
                {
                    clave = (string)sqlReader[tipoUnidad + "Clave"],
                    //modelo = (string)sqlReader["Modelo"],
                    //tipo_de_combustible = (string)sqlReader["Tipo_de_combustible"],
                    estatus = (string)sqlReader[tipoUnidad + "Estatus"],
                    //numero_de_placa = (string)sqlReader["Numero_de_placa"],
                    rfid = (string)sqlReader[tipoUnidad + "rfid"],
                    empresa = new Empresa
                    {
                        clave = (int)sqlReader[tipoUnidad + "Clave_empresa"],
                        nombre = (string)sqlReader[tipoUnidad + "empresa_nombre"],
                        telefono = (string)sqlReader[tipoUnidad + "empresa_telefono"],
                        direccion = (string)sqlReader[tipoUnidad + "empresa_direccion"],
                        fax = (string)sqlReader[tipoUnidad + "empresa_fax"]
                    },
                    tipoUnidad = new TipoUnidad
                    {
                        clave = (int)sqlReader[tipoUnidad + "Clave_tipo_de_unidad"],
                        clasificacion = (string)sqlReader[tipoUnidad + "tipoUnidad_clasificacion"],
                        descripcion = (string)sqlReader[tipoUnidad + "tipoUnidad_descripcion"],
                        NoEjes = (string)sqlReader[tipoUnidad + "tipoUnidad_noEjes"],
                        NoLlantas = (string)sqlReader[tipoUnidad + "tipoUnidad_NoLlantas"],
                        TonelajeMax = Convert.ToDecimal((string)sqlReader[tipoUnidad + "tipoUnidad_TonelajeMax"]),
                        bCombustible = DBNull.Value.Equals(sqlReader[tipoUnidad + "tipoUnidad_bCombustible"]) ? false : (bool)sqlReader[tipoUnidad + "tipoUnidad_bCombustible"]
                    },
                    tipoMotor = DBNull.Value.Equals(sqlReader[tipoUnidad + "motor_idTipoMotor"]) ? null : new TipoMotor
                    {
                        diTipoMotor = (int)sqlReader[tipoUnidad + "motor_idTipoMotor"],
                        tipoMotor = (string)sqlReader[tipoUnidad + "motor_tipoMotor"],
                        porcTolerancia = (decimal)sqlReader[tipoUnidad + "motor_tolerancia"]
                    }
                };
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal ContactoEmergencia mapContactosEmergencia(SqlDataReader reader)
        {
            try
            {
                return new ContactoEmergencia
                {
                    idContactoEmergencia = (int)reader["contacto_clave"],
                    idPersonal = (int)reader["contacto_idPersonal"],
                    nombres = (string)reader["contacto_nombres"],
                    apellidoPaterno = (string)reader["contacto_apellidoPaterno"],
                    apellidoMaterno = (string)reader["contacto_apellidoMaterno"],
                    telefonoCelular = (string)reader["contacto_telefonoCelular"],
                    telefonoDomicilio = (string)reader["contacto_telefonoDomicilio"],
                    parentezco = (string)reader["contacto_parentezco"],
                    usuario = (string)reader["contacto_usuarioRegistra"],
                };
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal UnidadTransporte mapUnidadesv2(SqlDataReader sqlReader)
        {
            try
            {
                UnidadTransporte unidad = new UnidadTransporte
                {
                    clave = (string)sqlReader["unidad_clave"],
                    estatus = (string)sqlReader["unidad_estatus"],
                    rfid = (string)sqlReader["unidad_rfid"],
                    capacidadTanque = Convert.ToInt32(sqlReader["unidad_capTanque"]),
                    fechaAdquisicion = DBNull.Value.Equals(sqlReader["unidad_fechaAdquisicion"]) ? null : (DateTime?)sqlReader["unidad_fechaAdquisicion"],
                    tipoAdquisicion = (enumTipoAdquisicion)sqlReader["unidad_tipoAdquisicion"],
                    odometro = (int)sqlReader["unidad_odometro"],
                    placas = (string)sqlReader["unidad_placas"],
                    documento = (string)sqlReader["unidad_documento"],
                    anio = (int)sqlReader["unidad_anio"],
                    mttoPrev = DBNull.Value.Equals(sqlReader["unidad_mttoPrev"]) ? 0 : (int)sqlReader["unidad_mttoPrev"],
                    VIN = (string)sqlReader["unidad_VIN"]
                };

                TipoUnidad tipo = mapTipoUnidad(sqlReader);
                unidad.tipoUnidad = tipo;

                if (!DBNull.Value.Equals(sqlReader["idMarca"]))
                {
                    unidad.marca = new Marca
                    {
                        clave = (int)sqlReader["idMarca"],
                        descripcion = (string)sqlReader["NombreMarca"],
                        activo = (bool)sqlReader["activo"],
                        llantas = (bool)sqlReader["llantas"],
                        uniTrans = (bool)sqlReader["uniTrans"]
                    };
                }
                else
                {
                    unidad.marca = null;
                }

                unidad.empresa = mapEmpresa(sqlReader);

                if (!DBNull.Value.Equals(sqlReader["proveedorV2_clave"]))
                {
                    unidad.proveedor = mapProveedorV2(sqlReader);
                }
                else
                {
                    unidad.proveedor = null;
                }

                if (!DBNull.Value.Equals(sqlReader["modelo_idModelo"]))
                {
                    unidad.modelo = mapModelo(sqlReader);
                }

                return unidad;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        internal Modelo mapModelo(SqlDataReader reader)
        {
            try
            {
                Modelo modelo = new Modelo
                {
                    idModelo = (int)reader["modelo_idModelo"],
                    modelo = (string)reader["modelo_modelo"],
                    activo = (bool)reader["modelo_activo"],
                    marca = new Marca
                    {
                        clave = (int)reader["modelo_marca_idMarca"],
                        descripcion = (string)reader["modelo_marca_nombre"],
                        activo = (bool)reader["modelo_marca_activo"],
                        llantas = (bool)reader["modelo_marca_llanta"],
                        uniTrans = (bool)reader["modelo_marca_uniTrans"]
                    }
                };
                return modelo;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        internal TipoOrdenCombustible mapTipoOrdenCombustible(SqlDataReader sqlReader)
        {
            try
            {
                TipoOrdenCombustible tipoOrdenCombustible = new TipoOrdenCombustible
                {
                    idTipoOrdenCombustible = (int)sqlReader["idTipoOSComb"],
                    nombreTipoOrdenCombustible = (string)sqlReader["NomTipoOSComb"],
                    requiereViaje = (bool)sqlReader["RequiereViaje"],
                    tipoUnidad = (string)sqlReader["TipoUnidad"]
                };
                return tipoOrdenCombustible;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal DetallePreOrdenCompra mapDetalleOrdenCompra(SqlDataReader sqlReader)
        {
            try
            {
                DetallePreOrdenCompra detalle = new DetallePreOrdenCompra
                {
                    idDetallePreOrdCom = (int)sqlReader["IdPreOC"],
                    cantidadSolicitada = (decimal)sqlReader["CantidadSol"],
                    cantidad = DBNull.Value.Equals(sqlReader["CantSinCosto"]) ? 0 : (decimal)sqlReader["CantSinCosto"],
                    faltante = DBNull.Value.Equals(sqlReader["CantFaltante"]) ? 0 : (decimal)sqlReader["CantFaltante"],
                    porcIVA = DBNull.Value.Equals(sqlReader["PorcIVA"]) ? 0 : (decimal)sqlReader["PorcIVA"],
                    precio = DBNull.Value.Equals(sqlReader["Precio"]) ? 0 : (decimal)sqlReader["Precio"],

                };

                ProductosCOM producto = mapProductosCOM(sqlReader);
                if (producto == null)
                {
                    return null;
                }
                detalle.producto = producto;

                return detalle;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal ProductosCOM mapProductosCOM(SqlDataReader sqlReader)
        {
            try
            {
                return new ProductosCOM
                {
                    idProducto = (int)sqlReader["CIDPRODUCTO"],
                    codigo = (string)sqlReader["CCODIGOPRODUCTO"],
                    estatus = Convert.ToBoolean(sqlReader["CSTATUSPRODUCTO"]),
                    nombre = (string)sqlReader["CNOMBREPRODUCTO"],
                    precio = DBNull.Value.Equals(sqlReader["precio"]) ? 0 : (decimal)sqlReader["precio"],
                    lavadero = Convert.ToBoolean(sqlReader["lavadero"]),
                    llantera = Convert.ToBoolean(sqlReader["llantera"]),
                    taller = Convert.ToBoolean(sqlReader["taller"])
                };
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal LLantasProducto mapLLantaProducto(SqlDataReader sqlReader)
        {
            try
            {
                LLantasProducto producto1 = new LLantasProducto
                {
                    idLLantasProducto = (int)sqlReader["idProductoLlanta"]
                };
                Diseño diseño1 = new Diseño
                {
                    idDiseño = (int)sqlReader["idDisenio"],
                    descripcion = (string)sqlReader["Descripcion"],
                    medida = DBNull.Value.Equals(sqlReader["medida_idMedida"]) ? null : new MedidaLlanta
                    {
                        idMedidaLlanta = (int)sqlReader["medida_idMedida"],
                        medida = (string)sqlReader["medida"]
                    },
                    profundidad = DBNull.Value.Equals(sqlReader["diseño_profundidad"]) ? 0 : ((int)sqlReader["diseño_profundidad"]),
                    tipoLlanta = DBNull.Value.Equals(sqlReader["tipollanta_idTipoLlanta"]) ? null : new TipoLlanta()
                    {
                        clave = (int)sqlReader["tipollanta_idTipoLlanta"],
                        nombre = (string)sqlReader["tipollanta_descripcion"]
                    }
                };
                producto1.diseño = diseño1;
                producto1.medida = (string)sqlReader["Medida"];
                Marca marca1 = new Marca
                {
                    clave = (int)sqlReader["idMarca"],
                    descripcion = (string)sqlReader["NombreMarca"]
                };
                producto1.marca = marca1;
                producto1.profundidad = (decimal)sqlReader["Profundidad"];
                producto1.activo = Convert.ToBoolean(sqlReader["Activo"]);
                return producto1;

            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal Llanta mapLlanta(SqlDataReader sqlReader)
        {
            try
            {
                var llanta = new Llanta
                {
                    idLlanta = (int)sqlReader["idLLanta"],
                    clave = (string)sqlReader["llanta_clave"],
                    noSerie = DBNull.Value.Equals(sqlReader["noSerie"]) ? "" : (string)sqlReader["noSerie"],
                    idEmpresa = (int)sqlReader["llanta_idEmpresa"],
                    diseño = (string)sqlReader["diseño"],
                    medida = (string)sqlReader["medida"],
                    profundidad = (decimal)sqlReader["profundidad"],
                    factura = (string)sqlReader["factura"],
                    fecha = (DateTime)sqlReader["fecha"],
                    costo = (decimal)sqlReader["costo"],
                    rfid = (string)sqlReader["rfid"],
                    posicion = (int)sqlReader["posicion"],
                    año = (int)sqlReader["anio"],
                    mes = (int)sqlReader["mes"],
                    estatus = (string)sqlReader["estatus"],
                    observaciones = (string)sqlReader["observaciones"],
                    marca = new Marca
                    {
                        clave = (int)sqlReader["marca_clave"],
                        descripcion = (string)sqlReader["marca_nombre"]
                    },
                    Lugar = (string)sqlReader["lugar"],
                    profundidadActual = DBNull.Value.Equals(sqlReader["profundidadActual"]) ? 0 : (decimal)sqlReader["profundidadActual"],
                    idDetalleCompra = DBNull.Value.Equals(sqlReader["idDetalleCompra"]) ? 0 : (int)sqlReader["idDetalleCompra"]
                };

                if (DBNull.Value.Equals(sqlReader["clave"]))
                {
                    llanta.unidadTransporte = null;
                }
                else
                {
                    llanta.unidadTransporte = mapUnidades("", sqlReader);
                }

                if (DBNull.Value.Equals(sqlReader["tipoLlanta_clave"]))
                {
                    llanta.tipoLlanta = new TipoLlanta
                    {
                        clave = 0,
                        nombre = "SIN ASIGNAR"
                    };
                }
                else
                {
                    llanta.tipoLlanta = new TipoLlanta
                    {
                        clave = (int)sqlReader["tipoLlanta_clave"],
                        nombre = (string)sqlReader["tipoLlanta_nombre"]
                    };
                }

                if (DBNull.Value.Equals(sqlReader["idAlmacen"]))
                {
                    llanta.almacen = null;
                }
                else
                {
                    llanta.almacen = new Almacen
                    {
                        idAlmacen = (int)sqlReader["idAlmacen"],
                        nombreAlmacen = (string)sqlReader["nombreAlmacen"]
                    };
                }
                if (DBNull.Value.Equals(sqlReader["idProveedor"]))
                {
                    llanta.proveedor = null;
                }
                else
                {
                    llanta.proveedor = mapProveedor(sqlReader);
                }

                return llanta;
            }
            catch (Exception ex)
            {
                var X = ex;
                return null;
            }
        }
        internal Servicio mapServicio(SqlDataReader sqlReader, string str = "")
        {
            try
            {
                return new Servicio
                {
                    idServicio = (int)sqlReader["idServicio"],
                    tipoOrden = new TipoOrden
                    {
                        idTipoOrden = (int)sqlReader["tipo_idTipoServicio"],
                        nombreTipoOrden = (string)sqlReader["tipo_nombreTipoOrden"]
                    },
                    nombre = (string)sqlReader["NomServicio"],
                    descipcion = (string)sqlReader["Descripcion"],
                    km = (decimal)sqlReader["CadaKms"],
                    dias = (decimal)sqlReader["CadaTiempoDias"],
                    costo = (decimal)sqlReader[string.Format("{0}Costo", str)],
                    idDivicion = (int)sqlReader["idDivision"],
                    taller = (bool)sqlReader["servicio_taller"],
                    lavadero = (bool)sqlReader["servicio_lavadero"],
                    llantera = (bool)sqlReader["servicio_llantera"],
                    activo = Convert.ToBoolean(sqlReader["servicio_Activo"])
                };
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal ActividadPendiente mapActividadPendiente(SqlDataReader sqlReader)
        {
            try
            {
                ActividadPendiente actividad = new ActividadPendiente
                {
                    idActividadPendiente = (int)sqlReader["actPend_clave"],
                    unidad = new UnidadTransporte { clave = (string)sqlReader["actPend_idUnidad"] },
                    idOrdenServicio = (int)sqlReader["actPend_idOrdenServ"],
                    fecha = (DateTime)sqlReader["actPend_fecha"],
                    enumTipoOrden = (enumTipoOrden)sqlReader["actPend_idTipoOrden"],
                    enumTipoOrdServ = (enumTipoOrdServ)sqlReader["actPend_idTipoOrdenServ"],
                    enumTipoServicio = (enumTipoServicio)sqlReader["actPend_idTipoServicio"],
                    posicion = (int)sqlReader["actPend_posicion"],
                    usuario = (string)sqlReader["actPend_usuario"],
                    estatus = (bool)sqlReader["actPend_estatus"],
                };

                if (!DBNull.Value.Equals(sqlReader["actividad_idActividad"]))
                {
                    actividad.actividad = mapActividad(sqlReader);
                }
                if (!DBNull.Value.Equals(sqlReader["idServicio"]))
                {
                    actividad.servicio = mapServicio(sqlReader);
                }
                return actividad;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal ResumenCargaCombustible mapResumenCarga(SqlDataReader sqlReader)
        {
            try
            {
                return new ResumenCargaCombustible
                {
                    idValeCarga = (int)sqlReader["idVale"],
                    idOrdenServ = (int)sqlReader["idOrdenServ"],
                    idOrdenPadre = (int)sqlReader["idOrdenPadre"],
                    idUnidad = (string)sqlReader["idUnidad"],
                    fechaCarga = (DateTime)sqlReader["fechaCarga"],
                    litros = (decimal)sqlReader["litros"]
                };
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal Proveedor mapProveedor(SqlDataReader sqlReader)
        {
            try
            {
                return new Proveedor
                {
                    idProveedor = (int)sqlReader["idProveedor"],
                    razonSocial = (string)sqlReader["RazonSocial"],
                    direccion = (string)sqlReader["DireccionProv"],
                    email = DBNull.Value.Equals(sqlReader["EmailProv"]) ? string.Empty : (string)sqlReader["EmailProv"],
                    idColonia = (int)sqlReader["idColonia"],
                    RFC = (string)sqlReader["RFCProv"],
                    tipoPersona = (string)sqlReader["TipoPersona"],
                    ultimoCosto = (decimal)sqlReader["ultimoCosto"]
                };
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal SolicitudOrdenCompra mapSolicitudOrden(SqlDataReader sqlReader)
        {
            try
            {
                SolicitudOrdenCompra solicitud = new SolicitudOrdenCompra
                {
                    idSolicitudOrdenCompra = (int)sqlReader["idSolicitudOrdenCompra"],
                    fecha = (DateTime)sqlReader["fecha"],
                    usuario = (string)sqlReader["usuario"],
                    obsevaciones = (string)sqlReader["obsevaciones"],
                    estatus = (string)sqlReader["estatus"],
                    isLlantas = Convert.ToBoolean(sqlReader["isLlantas"])
                };

                solicitud.empresa = mapEmpresa(sqlReader);
                if (solicitud.empresa == null)
                {
                    return null;
                }

                return solicitud;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal CierreProcesoCombustible mapCierreProcesoCombustible(SqlDataReader reader)
        {
            try
            {
                CierreProcesoCombustible cierre = new CierreProcesoCombustible();
                cierre = new CierreProcesoCombustible
                {
                    idCierreProcesosCombustible = (int)reader["cierre_clave"],
                    fechaInicio = (DateTime)reader["cierre_fechaInicial"],
                    fechaFin = (DateTime)reader["cierre_fechaFinal"],
                    usuario = (string)reader["cierre_usuario"]
                };
                TanqueCombustible tanque = mapTanqueCombustible(reader);
                if (tanque == null) return null;

                cierre.tanque = tanque;
                cierre.zonaOperativa = tanque.zonaOperativa;

                return cierre;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal DetalleSolicitudOrdenCompra mapDetalleSolicitudOrdCom(SqlDataReader sqlReader, bool isLlanta)
        {
            try
            {
                DetalleSolicitudOrdenCompra detalle = new DetalleSolicitudOrdenCompra
                {
                    idDetalleSolitudOrdenCompra = (int)sqlReader["idDetalleSolitudOrdenCompra"],
                    cantidad = (int)sqlReader["cantidad"],
                    medidaLlanta = (string)sqlReader["medida"]
                };


                if (isLlanta)
                {
                    if (!DBNull.Value.Equals(sqlReader["idProductoLlanta"]))
                    {
                        detalle.productoLlanta = mapLLantaProducto(sqlReader);
                        if (detalle.productoLlanta == null)
                        {
                            return null;
                        }
                    }

                    detalle.unidadTrans = mapUnidades("", sqlReader);
                    if (detalle.unidadTrans == null)
                    {
                        return null;
                    }
                }
                else
                {
                    ProductosCOM producto = mapProductosCOM(sqlReader);
                    if (producto == null)
                    {
                        return null;
                    }
                    detalle.producto = producto;
                }
                return detalle;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal MotivosRescate mapMotivoRescate(SqlDataReader sqlReader)
        {
            try
            {
                return new MotivosRescate
                {
                    idMotivoRescate = (int)sqlReader["idMotivoRescate"],
                    descripcionMotivo = (string)sqlReader["descripcionMotivo"],
                    idAlmacen = (int)sqlReader["idAlmacen"],
                    activoMotivo = Convert.ToBoolean(sqlReader["activoMotivo"])
                };
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal OperationResult mapViajesV2(SqlDataReader sqlReader)
        {
            try
            {
                Viaje viaje = (new Viaje
                {
                    NumGuiaId = (int)sqlReader["NumGuiaId"],
                    SerieGuia = (string)sqlReader["SerieGuia"],
                    NumViaje = (int)sqlReader["NumViaje"],
                    FechaHoraViaje = (DateTime)sqlReader["FechaHoraViaje"],
                    EstatusGuia = (string)sqlReader["EstatusGuia"],
                    FechaHoraFin = DBNull.Value.Equals(sqlReader["FechaHoraFin"]) ? null : (DateTime?)sqlReader["FechaHoraFin"],
                    tipoViaje = Convert.ToChar(sqlReader["tipoViaje"]),
                    tara = DBNull.Value.Equals(sqlReader["tara"]) ? 0 : (decimal)sqlReader["tara"],
                    IdEmpresa = (int)sqlReader["idEmpresa"],
                    km = DBNull.Value.Equals(sqlReader["km"]) ? 0 : (decimal)sqlReader["km"],
                    fechaBachoco = DBNull.Value.Equals(sqlReader["fechaBachoco"]) ? null : (DateTime?)sqlReader["fechaBachoco"],
                    isExterno = (bool)sqlReader["externo"],
                    tara2 = DBNull.Value.Equals(sqlReader["tara2"]) ? 0 : (decimal)sqlReader["tara2"]
                });

                viaje.cliente = new mapComunes().mapCliente(sqlReader);

                viaje.operador = new mapComunes().mapPersonal(sqlReader);

                UnidadTransporte tr = new mapComunes().mapUnidades("tractor_", sqlReader);
                if (tr != null)
                {
                    viaje.tractor = tr;
                }
                else
                {
                    return new OperationResult { valor = 1, mensaje = "Ocurrio un error al buscar el tractor del viaje " + ((int)sqlReader["NumGuiaId"]).ToString() };
                }

                UnidadTransporte remolque1 = new mapComunes().mapUnidades("remolque1_", sqlReader);
                if (tr != null)
                {
                    viaje.remolque = remolque1;
                }
                else
                {
                    return new OperationResult { valor = 1, mensaje = "Ocurrio un error al buscar el remolque 1 del viaje " + ((int)sqlReader["NumGuiaId"]).ToString() };
                }

                if ((string)sqlReader["idDolly"] == "")
                {
                    viaje.dolly = null;
                }
                else
                {
                    UnidadTransporte dolly = new mapComunes().mapUnidades("dolly_", sqlReader);
                    if (dolly != null)
                    {
                        viaje.dolly = dolly;
                    }
                    else
                    {
                        return new OperationResult { valor = 1, mensaje = "Ocurrio un error al buscar el dolly del viaje " + ((int)sqlReader["NumGuiaId"]).ToString() };
                    }
                }
                if ((string)sqlReader["idRemolque2"] == "")
                {
                    viaje.remolque2 = null;
                }
                else
                {
                    UnidadTransporte remolque2 = new mapComunes().mapUnidades("remolque2_", sqlReader);
                    if (remolque2 != null)
                    {
                        viaje.remolque2 = remolque2;
                    }
                    else
                    {
                        return new OperationResult { valor = 1, mensaje = "Ocurrio un error al buscar el remolque 2 del viaje " + ((int)sqlReader["NumGuiaId"]).ToString() };
                    }
                }
                viaje.listDetalles = new List<CartaPorte>();
                return new OperationResult { valor = 0, mensaje = "", result = viaje };
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal PreOrdenCompra mapPreOrdenCompra(SqlDataReader sqlReader)
        {
            try
            {
                return new PreOrdenCompra
                {
                    idPreOrdenCompra = (int)sqlReader["IdPreOC"],
                    idOrdenCompra = DBNull.Value.Equals(sqlReader["idOC"]) ? null : (int?)sqlReader["idOC"],
                    ordenServicio = (int)sqlReader["idOrdenSer"],
                    NomTipOrdServ = (string)sqlReader["NomTipOrdServ"],
                    unidadTransporte = (string)sqlReader["idUnidadTrans"],
                    idOrdenTrabajo = DBNull.Value.Equals(sqlReader["idOrdenTrabajo"]) ? null : (int?)sqlReader["idOrdenTrabajo"],
                    idCotizacion = DBNull.Value.Equals(sqlReader["idCotizacionOC"]) ? null : (int?)sqlReader["idCotizacionOC"],
                    fechaPreOrden = (DateTime)sqlReader["FechaPreOc"],
                    usuario = (string)sqlReader["UserSolicita"],
                    estatus = (string)sqlReader["Estatus"],
                    usuarioAutorizaOC = DBNull.Value.Equals(sqlReader["UserAutorizaOC"]) ? string.Empty : (string)sqlReader["UserAutorizaOC"],
                    fechaAutoriza = DBNull.Value.Equals(sqlReader["FecAutorizaOC"]) ? null : (DateTime?)sqlReader["FecAutorizaOC"],
                    observaciones = DBNull.Value.Equals(sqlReader["Observaciones"]) ? string.Empty : (string)sqlReader["Observaciones"],
                    fechaModifica = DBNull.Value.Equals(sqlReader["FecModifica"]) ? null : (DateTime?)sqlReader["FecModifica"],
                    usuarioModifica = DBNull.Value.Equals(sqlReader["UserModifica"]) ? string.Empty : (string)sqlReader["UserModifica"],
                    subTotal = DBNull.Value.Equals(sqlReader["SubTotal"]) ? 0 : (decimal)sqlReader["SubTotal"],
                    IVA = DBNull.Value.Equals(sqlReader["IVA"]) ? 0 : (decimal)sqlReader["IVA"],
                    fechaCancela = DBNull.Value.Equals(sqlReader["FecCancela"]) ? null : (DateTime?)sqlReader["FecCancela"],
                    usuarioCancela = DBNull.Value.Equals(sqlReader["UserCancela"]) ? string.Empty : (string)sqlReader["UserCancela"],
                    motioCancelacion = DBNull.Value.Equals(sqlReader["MotivoCancela"]) ? string.Empty : (string)sqlReader["MotivoCancela"],
                };
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal DetalleCotizaciones mapDetallesCotizaciones(SqlDataReader sqlReader)
        {
            try
            {
                DetalleCotizaciones detalle = new DetalleCotizaciones
                {
                    idDetalle = (int)sqlReader["idDetalle"],
                    costo = (decimal)sqlReader["costo"],
                    diasEntrega = (int)sqlReader["diasEntreda"],
                    diasCredito = (int)sqlReader["diasCredito"],
                    importe = (decimal)sqlReader["importe"],
                    elegido = Convert.ToBoolean(sqlReader["elegido"])
                };

                detalle.proveedor = mapProveedor(sqlReader);

                return detalle;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        internal Mantenimiento mapMantenimiento(SqlDataReader sqlReader)
        {
            int idOrden = (int)sqlReader["idOrdenSer"];
            if (idOrden == 41461)
            {

            }
            try
            {

                Mantenimiento mantenimiento = new Mantenimiento()
                {
                    idOrdenSer = (int)sqlReader["idOrdenSer"],
                    empresa = mapEmpresa(sqlReader),
                    tipoServicio = new TipoServicio
                    {
                        idTipoServicio = (int)sqlReader["idTipoServicio"],
                        nombreServicio = (string)sqlReader["tipoServicio"]
                    },
                    tipoOrdenServ = new TipoOrdenServ
                    {
                        idTipoOrdServ = (int)sqlReader["idTipOrdServ"],
                        nombreTioServ = (string)sqlReader["tipoOrdenSer"]
                    },
                    unidadTransporte = mapUnidades("", sqlReader),
                    externo = Convert.ToBoolean(sqlReader["externo"]),
                    fechaRec = (DateTime)sqlReader["fechaRec"],
                    fechaAsig = (DateTime)sqlReader["fechaAsig"],
                    fechaTer = (DateTime)sqlReader["fechaTer"],
                    listaActividades = new List<DetalleMantenimiento>()
                };
                return mantenimiento;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        internal DetalleMantenimiento mapDetMantenimiento(SqlDataReader sqlReader)
        {
            try
            {
                int id = (int)sqlReader["idOrdenSer"];
                DetalleMantenimiento detMantenimiento = new DetalleMantenimiento()
                {
                    idOrdenSer = (int)sqlReader["idOrdenSer"],
                    idOrdenTrabajo = (int)sqlReader["idOrdenTrabajo"],
                    idOrdenActividad = (int)sqlReader["idOrdenActividad"],
                    idUnidadTransporte = (string)sqlReader["idUnidadTrans"],
                    idActividad = (int)sqlReader["idActividad"],
                    actividad = (string)sqlReader["NombreAct"],
                    responsable = (string)sqlReader["responsable"],
                    aux1 = (string)sqlReader["aux1"],
                    aux2 = (string)sqlReader["aux2"],
                    fechaTer = (DateTime)sqlReader["FechaTerminado"],
                    costoManoObra = (decimal)sqlReader["CostoManoObra"],
                    listaInsumos = new List<InsumosOrdenTrabajo>()
                };
                return detMantenimiento;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal ResumenCartaPorte mapResumenCartaPorte(SqlDataReader sqlReader)
        {
            try
            {
                //int id = (int)sqlReader["idOrdenSer"];
                ResumenCartaPorte resumenCartaPorte = new ResumenCartaPorte()
                {
                    numGuiaId = (int)sqlReader["numGuiaId"],
                    consecutivo = (int)sqlReader["consecutivo"],
                    idEmpresa = (int)sqlReader["idEmpresa"],
                    idCliente = (int)sqlReader["idCliente"],
                    origen = (string)sqlReader["origen"],
                    destino = (string)sqlReader["destino"],
                    producto = (string)sqlReader["producto"],
                    km = (decimal)sqlReader["km"],
                    idCargaCombustible = (int)sqlReader["idCargaGasolina"]
                };
                return resumenCartaPorte;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public KardexCombustible mapKardexCombustible(SqlDataReader sqlReader)
        {
            int id = (int)sqlReader["idKardexCombustible"];
            try
            {
                if (id == 39)
                {

                }
                KardexCombustible kardex = new KardexCombustible
                {
                    idInventarioDiesel = (int)sqlReader["idKardexCombustible"],
                    fecha = (DateTime)sqlReader["fecha"],
                    tipoMovimiento = (eTipoMovimiento)((int)sqlReader["tipoMovimiento"]),
                    tipoOperacion = (TipoOperacionDiesel)((int)sqlReader["tipoOperacion"]),
                    idOperacion = DBNull.Value.Equals(sqlReader["idOperacion"]) ? null : (int?)sqlReader["idOperacion"],
                    cantidadInicial = (decimal)sqlReader["cantidadInicial"],
                    cantidad = (decimal)sqlReader["cantidad"],
                    cantidadFinal = (decimal)sqlReader["cantidadFinal"],
                    observaciones = (string)sqlReader["observaciones"]
                };
                if (DBNull.Value.Equals(sqlReader["kardex_idEmpresa"]))
                {
                    kardex.empresa = null;
                }
                else
                {
                    kardex.empresa = new Empresa { clave = (int)sqlReader["kardex_idEmpresa"], nombre = (string)sqlReader["kardex_nombreEmpresa"] };
                }

                TanqueCombustible tanque = mapTanqueCombustible(sqlReader);
                if (tanque == null)
                {
                    return null;
                }
                kardex.tanque = tanque;

                Personal personal = mapPersonal(sqlReader);
                if (personal == null)
                {
                    return null;
                }
                kardex.personal = personal;

                if (DBNull.Value.Equals(sqlReader["idUnidadTransporte"]))
                {
                    kardex.unidadTransporte = null;
                }
                else
                {
                    kardex.unidadTransporte = new UnidadTransporte { clave = (string)sqlReader["idUnidadTransporte"] };
                }


                return kardex;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public DetOrdenServicio mapDetOrdenServicio(SqlDataReader sqlReader)
        {
            try
            {
                DetOrdenServicio detOrdenServicio = new DetOrdenServicio
                {
                    idOrdenServicio = (int)sqlReader["DetOrdenServicio_idOrdenSer"],
                    idOrdenActividad = (int)sqlReader["DetOrdenServicio_idOrdenActividad"],
                    duracion = (decimal)sqlReader["DetOrdenServicio_DuracionActHr"],
                    estatus = (string)sqlReader["DetOrdenServicio_Estatus"],
                    fechaDiagnostico = DBNull.Value.Equals(sqlReader["DetOrdenServicio_FechaDiag"]) ? null : (DateTime?)sqlReader["DetOrdenServicio_FechaDiag"],
                    usuarioDiagnostico = (string)sqlReader["DetOrdenServicio_UsuarioDiag"],
                    fechaAsignacion = DBNull.Value.Equals(sqlReader["DetOrdenServicio_FechaAsig"]) ? null : (DateTime?)sqlReader["DetOrdenServicio_FechaAsig"],
                    usuarioAsignacion = (string)sqlReader["DetOrdenServicio_UsuarioAsig"],
                    fechaPausado = DBNull.Value.Equals(sqlReader["DetOrdenServicio_FechaPausado"]) ? null : (DateTime?)sqlReader["DetOrdenServicio_FechaPausado"],
                    notaPausa = (string)sqlReader["DetOrdenServicio_NotaPausado"],
                    fechaTerminado = DBNull.Value.Equals(sqlReader["DetOrdenServicio_FechaTerminado"]) ? null : (DateTime?)sqlReader["DetOrdenServicio_FechaTerminado"],
                    usuarioTerminado = (string)sqlReader["DetOrdenServicio_UsuarioTerminado"],
                    idOrdenTrabajo = (int)sqlReader["DetOrdenServicio_idOrdenTrabajo"],
                    idPadreOrdSer = (int)sqlReader["DetOrdenServicio_idPadreOrdSer"],
                    notaDiagnostico = (string)sqlReader["DetOrdenServicio_NotaDiagnostico"],
                    pocision = (int)sqlReader["DetOrdenServicio_PosLlanta"],
                    descripcion = (string)sqlReader["DetRecepcionOS_NotaRecepcion"],
                    responsable = (int)sqlReader["DetRecepcionOS_idPersonalResp"] == 0 ? null : new Personal
                    {
                        clave = (int)sqlReader["DetRecepcionOS_idPersonalResp"]
                    },
                    auxiliar1 = (int)sqlReader["DetRecepcionOS_idPersonalAyu1"] == 0 ? null : new Personal
                    {
                        clave = (int)sqlReader["DetRecepcionOS_idPersonalAyu1"]
                    },
                    auxiliar2 = (int)sqlReader["DetRecepcionOS_idPersonalAyu2"] == 0 ? null : new Personal
                    {
                        clave = (int)sqlReader["DetRecepcionOS_idPersonalAyu2"]
                    },
                    enumTipoOrden = (enumTipoOrden)sqlReader["DetOrdenServicio_idTipoOrden"],
                    enumTipoOrdServ = (enumTipoOrdServ)sqlReader["DetOrdenServicio_idTipOrdServ"],
                    enumTipoServicio = (enumTipoServicio)sqlReader["DetOrdenServicio_idTipoServicio"],
                    idActividadPendiente = (int)sqlReader["DetRecepcionOS_idActividadPendiente"]
                };
                Actividad actividad = null;
                Servicio servicio = null;
                if (!DBNull.Value.Equals(sqlReader["actividad_idActividad"]))
                {
                    actividad = mapActividad(sqlReader);
                    if (actividad == null)
                    {
                        return null;
                    }
                    detOrdenServicio.actividad = actividad;
                }

                if (!DBNull.Value.Equals(sqlReader["idServicio"]))
                {
                    servicio = mapServicio(sqlReader, "servicio_");
                    if (servicio == null)
                    {
                        return null;
                    }
                    detOrdenServicio.servicio = servicio;
                }

                detOrdenServicio.ordenTaller = new OrdenTaller
                {
                    actividad = actividad,
                    servicio = servicio,
                    cancelado = Convert.ToBoolean(sqlReader["DetRecepcionOS_isCancelado"]),
                    descripcionOS = (string)sqlReader["DetRecepcionOS_NotaRecepcion"],
                    descripcionA = (string)sqlReader["DetRecepcionOS_NotaRecepcionA"],
                    estatus = (string)sqlReader["DetOrdenServicio_Estatus"],
                    idOrdenTrabajo = (int)sqlReader["DetRecepcionOS_idOrdenTrabajo"],
                    personal1 = (int)sqlReader["DetRecepcionOS_idPersonalResp"] == 0 ? null : new Personal
                    {
                        clave = (int)sqlReader["DetRecepcionOS_idPersonalResp"]
                    },
                    personal2 = (int)sqlReader["DetRecepcionOS_idPersonalAyu1"] == 0 ? null : new Personal
                    {
                        clave = (int)sqlReader["DetRecepcionOS_idPersonalAyu1"]
                    },
                    personal3 = (int)sqlReader["DetRecepcionOS_idPersonalAyu2"] == 0 ? null : new Personal
                    {
                        clave = (int)sqlReader["DetRecepcionOS_idPersonalAyu2"]
                    },
                    idOrdenActividad = (int)sqlReader["DetOrdenServicio_idOrdenActividad"],
                    idOrdenServ = (int)sqlReader["DetOrdenServicio_idOrdenSer"],
                };

                if (!DBNull.Value.Equals(sqlReader["idLLanta"]))
                {
                    Llanta llanta = mapLlanta(sqlReader);
                    if (llanta == null)
                    {
                        return null;
                    }
                    detOrdenServicio.llanta = llanta;
                }
                else
                {
                    detOrdenServicio.llanta = null;
                }

                return detOrdenServicio;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public Actividad mapActividad(SqlDataReader sqlReader)
        {
            try
            {
                Actividad actividad = new Actividad
                {
                    idActividad = (int)sqlReader["actividad_idActividad"],
                    nombre = (string)sqlReader["actividad_nombre"],
                    costo = (decimal)sqlReader["actividad_costo"],
                    idDivicion = (int)sqlReader["actividad_idDivicion"],
                    duracion = Convert.ToDecimal(sqlReader["actividad_duracion"]),
                    lavadero = Convert.ToBoolean(sqlReader["actividad_lavadero"]),
                    taller = Convert.ToBoolean(sqlReader["actividad_taller"]),
                    llantera = Convert.ToBoolean(sqlReader["actividad_llantera"]),
                    CCODIGOPRODUCTO = (string)sqlReader["actividad_CCODIGOPRODUCTO"],
                    CIDPRODUCTO = (int)sqlReader["actividad_CIDPRODUCTO"],
                    activo = (bool)sqlReader["actividad_activo"]
                };
                return actividad;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public CabOrdenServicio mapCapOrdenServicio(SqlDataReader sqlReader)
        {
            try
            {
                CabOrdenServicio cabOrdenServicio = new CabOrdenServicio
                {
                    idOrdenSer = (int)sqlReader["idOrdenSer"],
                    enumTipoOrden = (enumTipoOrden)(int)sqlReader["idTipoOrden"],
                    fechaRecepcion = DBNull.Value.Equals(sqlReader["FechaRecepcion"]) ? null : (DateTime?)sqlReader["FechaRecepcion"],
                    idEmpleadoEntrega = DBNull.Value.Equals(sqlReader["idEmpleadoEntrega"]) ? null : (int?)sqlReader["idEmpleadoEntrega"],
                    usuarioRecepcion = DBNull.Value.Equals(sqlReader["UsuarioRecepcion"]) ? string.Empty : (string)sqlReader["UsuarioRecepcion"],
                    km = DBNull.Value.Equals(sqlReader["Kilometraje"]) ? "0" : ((decimal)sqlReader["Kilometraje"]).ToString(),
                    estatus = (string)sqlReader["Estatus"],
                    fechaDiagnostico = DBNull.Value.Equals(sqlReader["FechaDiagnostico"]) ? null : (DateTime?)sqlReader["FechaDiagnostico"],
                    fechaAsignado = DBNull.Value.Equals(sqlReader["FechaAsignado"]) ? null : (DateTime?)sqlReader["FechaAsignado"],
                    fechaTerminado = DBNull.Value.Equals(sqlReader["FechaTerminado"]) ? null : (DateTime?)sqlReader["FechaTerminado"],
                    fechaEntregado = DBNull.Value.Equals(sqlReader["FechaEntregado"]) ? null : (DateTime?)sqlReader["FechaEntregado"],
                    usuarioEntrega = DBNull.Value.Equals(sqlReader["UsuarioEntrega"]) ? null : (string)sqlReader["UsuarioEntrega"],
                    idEmpleadoRecibe = DBNull.Value.Equals(sqlReader["idEmpleadoRecibe"]) ? null : (int?)sqlReader["idEmpleadoRecibe"],
                    notaFinal = DBNull.Value.Equals(sqlReader["NotaFinal"]) ? null : (string)sqlReader["NotaFinal"],
                    usuarioCancela = DBNull.Value.Equals(sqlReader["usuarioCan"]) ? null : (string)sqlReader["usuarioCan"],
                    fechaCancela = DBNull.Value.Equals(sqlReader["FechaCancela"]) ? null : (DateTime?)sqlReader["FechaCancela"],
                    idEmpleadoAutoriza = (int)sqlReader["idEmpleadoAutoriza"],
                    fechaCaptura = (DateTime)sqlReader["fechaCaptura"],
                    enumTipoOrdServ = (enumTipoOrdServ)sqlReader["idTipOrdServ"],
                    enumtipoServicio = (enumTipoServicio)sqlReader["idTipoServicio"],
                    idPadreOrdSer = (int)sqlReader["idPadreOrdSer"],
                    externo = Convert.ToBoolean(sqlReader["externo"]),
                    usuarioAsigna = DBNull.Value.Equals(sqlReader["UsuarioAsigna"]) ? null : (string)sqlReader["UsuarioAsigna"],
                    usuarioTermina = DBNull.Value.Equals(sqlReader["UsuarioTermina"]) ? null : (string)sqlReader["UsuarioTermina"],
                    observaciones = DBNull.Value.Equals(sqlReader["observaciones"]) ? string.Empty : (string)sqlReader["observaciones"],
                    listaActividades = new List<ActividadOrdenServicio>()
                };
                cabOrdenServicio.empresa = new mapComunes().mapEmpresa(sqlReader);

                var resp = new UnidadTransporteServices().getUnidadesALLorById(0, (string)sqlReader["idUnidadTrans"]);
                if (resp.typeResult == ResultTypes.success)
                {
                    cabOrdenServicio.unidad = (resp.result as List<UnidadTransporte>)[0];
                }
                return cabOrdenServicio;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public ActividadOrdenServicio mapActividadOrdenServicio(SqlDataReader sqlReader)
        {
            try
            {
                ActividadOrdenServicio actividadOrdenServicio = new ActividadOrdenServicio()
                {
                    idActividadOrdenServicio = (int)sqlReader["idActividadOrdenServivio"],
                    fecha = (DateTime)sqlReader["fecha"],
                    usuario = (string)sqlReader["usuario"],
                    idPadreOrdenServicio = (int)sqlReader["idPadreOrdenServicio"],
                    idOrdenServicio = (int)sqlReader["idOrdenServicio"],
                    enumTipoOrdServ = (enumTipoOrdServ)(int)sqlReader["idTipoOrdenServicio"],
                    idResponsable = DBNull.Value.Equals(sqlReader["idResponsable"]) ? null : (int?)sqlReader["idResponsable"],
                    idAuxiliar1 = DBNull.Value.Equals(sqlReader["idAuxiliar1"]) ? null : (int?)sqlReader["idAuxiliar1"],
                    idAuxiliar2 = DBNull.Value.Equals(sqlReader["idAuxiliar2"]) ? null : (int?)sqlReader["idAuxiliar2"],
                    activo = (bool)sqlReader["activo"],
                    fechaCancelacion = DBNull.Value.Equals(sqlReader["fechaCancelacion"]) ? null : (DateTime?)sqlReader["fechaCancelacion"],
                    motivoCancelacion = DBNull.Value.Equals(sqlReader["motivoCancelacion"]) ? string.Empty : (string)sqlReader["motivoCancelacion"],
                    usuarioCancela = DBNull.Value.Equals(sqlReader["usuarioCancela"]) ? string.Empty : (string)sqlReader["usuarioCancela"],
                    posicion = DBNull.Value.Equals(sqlReader["posicion"]) ? null : (int?)sqlReader["posicion"],
                };

                Actividad actividad = mapActividad(sqlReader);
                actividadOrdenServicio.actividad = actividad == null ? null : actividad;

                Servicio servicio = mapServicio(sqlReader);
                actividadOrdenServicio.servicio = servicio == null ? null : servicio;

                return actividadOrdenServicio;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public Personal mapPersonal_v2(SqlDataReader sqlReader)
        {
            try
            {
                if ((int)sqlReader["idPersonal"] == 21945)
                {

                }
                Personal personal = new Personal
                {
                    clave = (int)sqlReader["idPersonal"],
                    apellidoPaterno = (string)sqlReader["ApellidoPat"],
                    apellidoMaterno = (string)sqlReader["ApellidoMat"],
                    nombres = (string)sqlReader["Nombre"],
                    nombre = (string)sqlReader["NombreCompleto"],
                    direccion = DBNull.Value.Equals(sqlReader["Direccion"]) ? string.Empty : (string)sqlReader["Direccion"],
                    telefono = DBNull.Value.Equals(sqlReader["TelefonoPersonal"]) ? string.Empty : (string)sqlReader["TelefonoPersonal"],
                    estatus = (DBNull.Value.Equals(sqlReader["Estatus"]) ? string.Empty : (string)sqlReader["Estatus"]).ToUpper() == "ACT" ? "ACTIVO" : "BAJA",
                    rfid = DBNull.Value.Equals(sqlReader["rfid"]) ? string.Empty : (string)sqlReader["rfid"],
                    fechaNacimiento = DBNull.Value.Equals(sqlReader["fechaNacimiento"]) ? null : (DateTime?)sqlReader["fechaNacimiento"],
                    fechaIngreso = DBNull.Value.Equals(sqlReader["fechaIngreso"]) ? null : (DateTime?)sqlReader["fechaIngreso"],
                    tipoSangre = (string)sqlReader["tipoSangre"],
                    calle = (string)sqlReader["persona_calle"],
                    numero = (string)sqlReader["persona_numero"],
                    cruce1 = (string)sqlReader["persona_cruce1"],
                    cruce2 = (string)sqlReader["persona_cruce2"],
                    ciudad = (string)sqlReader["persona_ciudad"],
                    coloniaStr = (string)sqlReader["persona_colonia"],
                    cp = (int)sqlReader["persona_cp"],
                    telefonoCelular = (string)sqlReader["persona_telefonoCelular"],
                    correoElectronico = (string)sqlReader["persona_correoElectronico"],
                    genero = (string)sqlReader["persona_genero"],
                    estadoCivil = (string)sqlReader["persona_estadoCivil"],
                    tallaCamisa = (string)sqlReader["persona_tallaCamisa"],
                    tallaZapatos = (string)sqlReader["persona_tallaZapatos"],
                    cveINTELISIS = (string)sqlReader["personal_cveINTELISIS"],
                    cveAgente = DBNull.Value.Equals(sqlReader["personal_cveAgente"]) ? null : (string)sqlReader["personal_cveAgente"]
                };
                Empresa empresa = mapEmpresa(sqlReader);
                Puesto puesto = mapPuesto(sqlReader);

                Departamento departamento = null;
                if (!DBNull.Value.Equals(sqlReader["departamento_id"]))
                {
                    departamento = mapDepartamento(sqlReader);
                }

                Colonia colonia = null;
                if (!DBNull.Value.Equals(sqlReader["colonia_id"]))
                {
                    colonia = mapColonia(sqlReader);
                }

                personal.empresa = empresa;
                personal.puesto = puesto;
                personal.departamento = departamento;
                personal.colonia = colonia;

                if (!DBNull.Value.Equals(sqlReader["apoyo_clave"]))
                {
                    personal.apoyoPersonal = new ApoyoPersonal
                    {
                        idApoyo = (int)sqlReader["apoyo_clave"],
                        idPersonal = (int)sqlReader["apoyo_idPersonal"],
                        metaViaje = (int)sqlReader["apoyo_viajesMeta"],
                        pagoApoyo = (decimal)sqlReader["apoyo_pago"],
                        activo = (bool)sqlReader["apoyo_activo"]
                    };
                }
                else
                {
                    personal.apoyoPersonal = null;
                }

                return personal;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public Liquidacion mapLiquidacion(SqlDataReader reader)
        {
            try
            {
                Liquidacion liquidacion = new Liquidacion
                {
                    idLiquidacion = (int)reader["liquidacion_clave"],
                    fecha = (DateTime)reader["liquidacion_fecha"],
                    fechaInicio = (DateTime)reader["liquidacion_FechaInicio"],
                    fechaFin = (DateTime)reader["liquidacion_FechaFin"],
                    usuarioInicio = (string)reader["liquidacion_usuarioInicio"],
                    usuarioFin = (string)reader["liquidacion_usarioTermino"],
                    activo = (bool)reader["liquidacion_activo"],
                    liquidado = (bool)reader["liquidacion_liquidado"],
                    listaDetalleLiquidacion = new List<DetalleLiquidacion>(),
                    listaDetallePersonalLiquidacion = new List<DetallePersonaLiquidacion>(),
                    total = (decimal)reader["liquidacion_total"]
                };
                return liquidacion;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public DetalleLiquidacion mapDetalleLiquidacion(SqlDataReader reader)
        {
            try
            {
                DetalleLiquidacion liquidacion = new DetalleLiquidacion
                {
                    idDetalleLiquidacion = (int)reader["detalleLiq_clave"],
                    idLiquidacion = (int)reader["detalleLiq_idLiquidacion"],
                };

                Departamento departamento = mapDepartamento(reader);
                if (departamento == null) return null;

                liquidacion.departamento = departamento;

                return liquidacion;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        internal DetallePersonaLiquidacion mapDetallePersonalLiquidacion(SqlDataReader reader)
        {
            try
            {
                DetallePersonaLiquidacion detalle = new DetallePersonaLiquidacion
                {
                    idDetallePersonaLiquidacion = (int)reader["detPersonalLiquidacion_clave"],
                    idLiquidacion = (int)reader["detPersonalLiquidacion_idLiquidacion"],
                    activo = (bool)reader["detPersonalLiquidacion_activo"],
                    listaConceptoLiquidacion = new List<ConceptoLiquidacion>()
                };
                Personal personal = mapPersonal_v2(reader);
                if (personal == null) return null;

                detalle.personal = personal;

                return detalle;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        internal ConceptoLiquidacion mapConceptoViaje(SqlDataReader reader, List<DateTime> diasInhabil)
        {
            try
            {
                ConceptoLiquidacion concepto = new ConceptoLiquidacion
                {
                    idConceptoLiquidacion = (int)reader["concepto_clave"],
                    idLiquidacion = (int)reader["concepto_idLiquidacion"],
                    idDetallePersonaLiquidacion = (int)reader["concepto_idDetallePersonalLiquidacion"],
                    tipoConceptoLiquidacion = (TipoConceptoLiquidacion)reader["concepto_tipoConcepto"],
                    folioViaje = (int)reader["concepto_folioViaje"],
                    noViaje = (int)reader["concepto_noViaje"],
                    destinos = (string)reader["concepto_destinos"],
                    noDestinos = (int)reader["concepto_noDestinos"],
                    noViajeOperador = (int)reader["concepto_noViajeOperador"],
                    motivosDoble = (MotivosDoble)reader["concepto_motivoDoble"],
                    fechaInicial = (DateTime)reader["concepto_fechaInicial"],
                    fechaFinal = DBNull.Value.Equals(reader["concepto_fechaFinal"]) ? null : (DateTime?)reader["concepto_fechaFinal"],
                    cantidad = (decimal)reader["concepto_cantidad"],
                    idCliente = (int)reader["concepto_idCliente"],
                    cartaPorte = DBNull.Value.Equals(reader["cartaPorte"]) ? null : (int?)reader["cartaPorte"],
                    tractor = DBNull.Value.Equals(reader["tractor"]) ? string.Empty : (string)reader["tractor"],
                    origenes = DBNull.Value.Equals(reader["origen"]) ? string.Empty : (string)reader["origen"],
                    fechaCarga = DBNull.Value.Equals(reader["fechaCarga"]) ? null : (DateTime?)reader["fechaCarga"],
                    fechaDescarga = DBNull.Value.Equals(reader["fechaDescarga"]) ? null : (DateTime?)reader["fechaDescarga"],
                    remision = DBNull.Value.Equals(reader["remision"]) ? string.Empty : (string)reader["remision"],
                    modalidad = DBNull.Value.Equals(reader["modalidad"]) ? string.Empty : (string)reader["modalidad"],
                    listaDiasInhabil = diasInhabil
                };
                return concepto;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        internal ConceptoLiquidacion mapConceptoPagosFijos(SqlDataReader reader)
        {
            try
            {
                ConceptoLiquidacion concepto = new ConceptoLiquidacion
                {
                    idConceptoLiquidacion = (int)reader["concepto_clave"],
                    idLiquidacion = (int)reader["concepto_idLiquidacion"],
                    idDetallePersonaLiquidacion = (int)reader["concepto_idDetallePersonalLiquidacion"],
                    tipoConceptoLiquidacion = (TipoConceptoLiquidacion)reader["concepto_tipoConcepto"],
                    fecha = (DateTime)reader["concepto_fecha"],
                    cantidad = (decimal)reader["concepto_cantidad"],
                    listaTiposIndicencias = new List<TipoIncidencia>
                        {
                            TipoIncidencia.ASISTENCIA,
                            TipoIncidencia.FALTA
                        },
                    tipoIncidencia = (TipoIncidencia)reader["concepto_tipoIncidencia"]
                };
                return concepto;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        internal ConceptoLiquidacion mapConceptoDescuentos(SqlDataReader reader)
        {
            try
            {
                ConceptoLiquidacion concepto = new ConceptoLiquidacion
                {
                    idConceptoLiquidacion = (int)reader["concepto_clave"],
                    idLiquidacion = (int)reader["concepto_idLiquidacion"],
                    idDetallePersonaLiquidacion = (int)reader["concepto_idDetallePersonalLiquidacion"],
                    tipoConceptoLiquidacion = (TipoConceptoLiquidacion)reader["concepto_tipoConcepto"],
                    cantidad = (decimal)reader["concepto_cantidad"],
                    listaMotivosDescuento = new List<MotivoDescuentos>
                    {
                        MotivoDescuentos.OTRO,
                        MotivoDescuentos.COMBUSTIBLE,
                        MotivoDescuentos.DAÑO_O_PERDIDA,
                        MotivoDescuentos.ZONA_DE_ROBO
                    },
                    motivoDescuentos = (MotivoDescuentos)reader["concepto_motivoDescuento"],
                    observaciones = (string)reader["concepto_observaciones"],
                    idViajeDescuento = DBNull.Value.Equals(reader["concepto_viajeDescuento"]) ? null : (int?)reader["concepto_viajeDescuento"]
                };
                return concepto;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        internal ConceptoLiquidacion mapConceptoExtras(SqlDataReader reader)
        {
            try
            {
                ConceptoLiquidacion concepto = new ConceptoLiquidacion
                {
                    idConceptoLiquidacion = (int)reader["concepto_clave"],
                    idLiquidacion = (int)reader["concepto_idLiquidacion"],
                    idDetallePersonaLiquidacion = (int)reader["concepto_idDetallePersonalLiquidacion"],
                    tipoConceptoLiquidacion = (TipoConceptoLiquidacion)reader["concepto_tipoConcepto"],
                    cantidad = (decimal)reader["concepto_cantidad"],
                    observaciones = (string)reader["concepto_observaciones"],
                    noHoras = (int)reader["concepto_noHojas"],
                    fecha = DBNull.Value.Equals(reader["concepto_fecha"]) ? null : (DateTime?)reader["concepto_fecha"]
                };
                return concepto;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        internal DocumentacionPersonal mapDocumentacionPersonal(SqlDataReader reader)
        {
            try
            {
                DocumentacionPersonal documentacion = new DocumentacionPersonal
                {
                    idDocumentacionPersonal = (int)reader["documentacion_clave"],
                    idPersonal = (int)reader["documentacion_idPersonal"],
                    numeroSeguroSocial = (string)reader["documentacion_NSS"],
                    CURP = (string)reader["documentacion_CURP"],
                    INE = (string)reader["documentacion_INE"],
                    RFC = (string)reader["documentacion_RFC"],
                    noExamenPsicofisico = (string)reader["documentacion_noExamen"],
                    fechaVencimientoExamen = DBNull.Value.Equals(reader["documentacion_fechaVencimiento"]) ? null : (DateTime?)reader["documentacion_fechaVencimiento"],
                    banco = (string)reader["documentacion_banco"],
                    cuenta = (string)reader["documentacion_cuenta"],
                    clabe = (string)reader["documentacion_clabe"],
                    observaciones = (string)reader["documentacion_observaciones"],
                    usuario = (string)reader["documentacion_usuario"],
                };
                return documentacion;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        internal LicenciaConducir mapLicenciasConducir(SqlDataReader reader)
        {
            try
            {
                LicenciaConducir licencia = new LicenciaConducir
                {
                    idLicencia = (int)reader["licencia_clave"],
                    idPersonal = (int)reader["licencia_idPersonal"],
                    noLicencia = (string)reader["licencia_noLicencia"],
                    tipoLicencia = (string)reader["licencia_tipoLicencia"],
                    vencimiento = (DateTime)reader["licencia_fechaVencimiento"],
                    clasificacionLicencia = (ClasificacionLicencia)Convert.ToInt32(reader["licencia_clasificacion"]),
                    usuario = (string)reader["licencia_usuario"]
                };
                return licencia;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal AnticipoMaster mapAnticipoMaster(SqlDataReader reader)
        {
            try
            {
                AnticipoMaster anticipo = new AnticipoMaster
                {
                    idAnticipoMaster = (int)reader["anticipo_clave"],
                    folioViaje = (int)reader["anticipo_folioViaje"],
                    fechaPago = (DateTime)reader["anticipo_fechaPago"],
                    usuario = (string)reader["anticipo_usuario"],
                    listaDetalles = new List<DetalleAnticipoMaster>(),
                    cveINTELISIS = DBNull.Value.Equals(reader["cveINTELISIS"]) ? 0 : (int)reader["cveINTELISIS"]
                };
                return anticipo;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        internal DetalleAnticipoMaster mapDetalleAnticipoMaster(SqlDataReader reader)
        {
            try
            {
                DetalleAnticipoMaster detalle = new DetalleAnticipoMaster
                {
                    idDetalleAnticipoMaster = (int)reader["detalleAnt_clave"],
                    idAnticipoMaster = (int)reader["detalleAnt_idAnticipoMaster"],
                    importe = (decimal)reader["detalleAnt_importe"],
                    tipoAnticipo = new TipoAnticipo()
                };
                TipoAnticipo tipoAnticipo = mapTipoAnticipo(reader);
                if (tipoAnticipo == null) return null;
                detalle.tipoAnticipo = tipoAnticipo;
                return detalle;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        internal UnidadTransporte mapUnidadTransporte(SqlDataReader reader)
        {
            try
            {
                UnidadTransporte transporte = new UnidadTransporte
                {
                    clave = (string)reader["Clave"],
                    estatus = (string)reader["Estatus"],
                    rfid = (string)reader["rfid"],
                    VIN = DBNull.Value.Equals(reader["VIN"]) ? string.Empty : (string)reader["VIN"],
                    anio = DBNull.Value.Equals(reader["año"]) ? 0 : (int)reader["año"],
                    placas = DBNull.Value.Equals(reader["placas"]) ? string.Empty : (string)reader["placas"],
                };
                Empresa empresa = new Empresa
                {
                    clave = (int)reader["Clave_empresa"],
                    nombre = (string)reader["empresa_nombre"],
                    telefono = (string)reader["empresa_telefono"],
                    direccion = (string)reader["empresa_direccion"],
                    fax = (string)reader["empresa_fax"]
                };
                transporte.empresa = empresa;
                TipoUnidad tipounidad = new TipoUnidad
                {
                    clave = (int)reader["Clave_tipo_de_unidad"],
                    clasificacion = (string)reader["tipoUnidad_clasificacion"],
                    descripcion = (string)reader["tipoUnidad_descripcion"],
                    NoEjes = (string)reader["tipoUnidad_noEjes"],
                    NoLlantas = (string)reader["tipoUnidad_NoLlantas"],
                    TonelajeMax = Convert.ToDecimal((string)reader["tipoUnidad_TonelajeMax"]),
                    bCombustible = !DBNull.Value.Equals(reader["tipoUnidad_bCombustible"]) && ((bool)reader["tipoUnidad_bCombustible"])
                };
                transporte.tipoUnidad = tipounidad;
                transporte.tipoMotor = DBNull.Value.Equals(reader["motor_idTipoMotor"]) ? null : new TipoMotor();

                if (!DBNull.Value.Equals(reader["marca_clave"]))
                {
                    Marca marca = new Marca 
                    {
                        clave = (int)reader["marca_clave"],
                        descripcion = (string)reader["marca_nombre"],
                        uniTrans = (bool)reader["marca_uniTrans"],
                        activo = (bool)reader["marca_activo"],
                        llantas = (bool)reader["marca_llantas"]
                    };
                    transporte.marca = marca;
                }
                else
                {
                    transporte.marca = null;
                }

                if (!DBNull.Value.Equals(reader["modelo_clave"]))
                {
                    Modelo modelo = new Modelo
                    {
                        idModelo = (int)reader["modelo_clave"],
                        marca = new Marca { clave = (int)reader["modelo_idMarca"] },
                        modelo = (string)reader["modelo_nombre"],
                        activo = (bool)reader["modelo_activo"]
                    };
                    transporte.modelo = modelo;
                }
                else
                {
                    transporte.modelo = null;
                }

                return transporte;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
