﻿namespace CoreFletera.BusinessLogic
{
    using Core.BusinessLogic;
    using Core.Models;
    using Core.Utils;
    using CoreFletera.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;

    public class RutasServices
    {
        public OperationResult getAllRutas()
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getAllRutas", false))
                    {
                        List<Ruta> list = new List<Ruta>();
                        connection.Open();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Ruta item = new mapComunes().mapRutas(reader);
                                if (item != null)
                                {
                                    list.Add(item);
                                }
                            }
                        }
                        result = new OperationResult(command, list);
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult(exception);
            }
            return result;
        }

        public OperationResult quitarRelacionRutaCasetas(int idRuta, int idCaseta)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    connection.Open();
                    using (SqlTransaction transaction = connection.BeginTransaction())
                    {
                        using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_quitarRelacionRutaCasetas", false))
                        {
                            command.Transaction = transaction;
                            command.Parameters.Add(new SqlParameter("@idRuta", idRuta));
                            command.Parameters.Add(new SqlParameter("@idCaseta", idCaseta));
                            command.ExecuteNonQuery();
                            if (((int)command.Parameters["@valor"].Value) == 0)
                            {
                                transaction.Commit();
                            }
                            else
                            {
                                transaction.Rollback();
                            }
                            result = new OperationResult(command, null);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult(exception);
            }
            return result;
        }

        public OperationResult saveRuta(ref Ruta ruta)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    connection.Open();
                    using (SqlTransaction transaction = connection.BeginTransaction())
                    {
                        using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_saveRuta", true))
                        {
                            command.Transaction = transaction;
                            command.Parameters.Add(new SqlParameter("@idRuta", ruta.idRuta));
                            command.Parameters.Add(new SqlParameter("@idOrigen", ruta.origen.idOrigen));
                            command.Parameters.Add(new SqlParameter("@idDestino", ruta.destino.clave));
                            command.Parameters.Add(new SqlParameter("@km", ruta.km));
                            command.Parameters.Add(new SqlParameter("@cobroPorViaje", ruta.cobroPorViaje));
                            command.Parameters.Add(new SqlParameter("@tarifaSencillo", ruta.tarifaRuta.precioSencillo));
                            command.Parameters.Add(new SqlParameter("@tarifaFull", ruta.tarifaRuta.precioFull));
                            command.Parameters.Add(new SqlParameter("@pagoOperadorSencillo", ruta.pagoOperador.precioSencillo));
                            command.Parameters.Add(new SqlParameter("@pagoOperadorFull", ruta.pagoOperador.precioFull));
                            command.Parameters.Add(new SqlParameter("@tipoPagoOperador", (int)ruta.pagoOperador.tipoPago));
                            command.Parameters.Add(new SqlParameter("@costoCaseta", ruta.casetas.precio));
                            command.Parameters.Add(new SqlParameter("@incluyeCaseta", ruta.casetas.incluyeCaseta));
                            command.Parameters.Add(new SqlParameter("@activo", ruta.activo));
                            command.Parameters.Add(new SqlParameter("@idClasificacion", (ruta.clasificacion == null) ? null : new int?(ruta.clasificacion.idClasificacionProducto)));
                            command.ExecuteNonQuery();
                            if (((int)command.Parameters["@valor"].Value) == 0)
                            {
                                ruta.idRuta = (int)command.Parameters["@id"].Value;
                                if (ruta.listCaseta != null)
                                {
                                    command.CommandText = "sp_saveRelacionRutaCasetas";
                                    foreach (Caseta caseta in ruta.listCaseta)
                                    {
                                        command.Parameters.Clear();
                                        foreach (SqlParameter parameter in CrearSqlCommand.getPatametros(false))
                                        {
                                            command.Parameters.Add(parameter);
                                        }
                                        command.Parameters.Add(new SqlParameter("@idRuta", ruta.idRuta));
                                        command.Parameters.Add(new SqlParameter("@idCaseta", caseta.idCaseta));
                                        command.ExecuteNonQuery();
                                        if (((int)command.Parameters["@valor"].Value) != 0)
                                        {
                                            transaction.Rollback();
                                            return new OperationResult(command, null);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                transaction.Rollback();
                            }
                            transaction.Commit();
                            result = new OperationResult(command, ruta);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult(exception);
            }
            return result;
        }
    }
}
