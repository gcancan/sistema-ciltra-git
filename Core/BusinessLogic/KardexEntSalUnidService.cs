﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using Core.Utils;
using System.Data;
using System.Data.SqlClient;
namespace CoreFletera.BusinessLogic
{
    public class KardexEntSalUnidService
    {
        internal OperationResult getKardexEntSalByFechas(DateTime fechaInicial, DateTime fechaFinal, string idUnidadTrans)
        {
            try
            {
                using (SqlConnection conection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand command = conection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getKardexEntSalByFechas";

                        command.Parameters.Add(new SqlParameter("@fechaInicial", fechaInicial));
                        command.Parameters.Add(new SqlParameter("@fechaFinal", fechaFinal));
                        command.Parameters.Add(new SqlParameter("@idUnidadTrans", idUnidadTrans));
                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var spMensaje = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        conection.Open();
                        List<KardexEntSalUnid> listaKardex = new List<KardexEntSalUnid>();
                        using (SqlDataReader sqlReader = command.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                KardexEntSalUnid kardex = new KardexEntSalUnid()
                                {
                                    idKardexEntSalUnid = (int)sqlReader["idKardexEntSalUnid"],
                                    fecha = (DateTime)sqlReader["fecha"],
                                    idUnidadTrans =(string)sqlReader["idUnidadTrans"],
                                    tipoMovimiento = (eTipoMovimiento)sqlReader["tipoMovimiento"],
                                    lugar = (string)sqlReader["lugar"],
                                    descripcion = (string)sqlReader["descripcion"],
                                    folio = (int)sqlReader["folio"]
                                };
                                listaKardex.Add(kardex);
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = listaKardex };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }
        internal OperationResult getDetalleKardex(KardexEntSalUnid kardex)
        {
            try
            {
                using (SqlConnection conection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand command = conection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getDetallesKardex";

                        command.Parameters.Add(new SqlParameter("@idTipoMovimiento", (int)kardex.tipoMovimiento));
                        command.Parameters.Add(new SqlParameter("@idUnidad", kardex.idUnidadTrans));
                        command.Parameters.Add(new SqlParameter("@descripcion", kardex.descripcion));
                        command.Parameters.Add(new SqlParameter("@folio", kardex.folio));
                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var spMensaje = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        conection.Open();
                        List<string> listaDetalle = new List<string>();
                        using (SqlDataReader sqlReader = command.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {                                
                                listaDetalle.Add((string)sqlReader["detalle"]);
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = listaDetalle };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }
    }
}
