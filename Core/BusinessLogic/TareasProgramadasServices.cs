﻿using Core.Interfaces;
using Core.Models;
using Core.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Core.BusinessLogic
{
    public class TareasProgramadasServices
    {
        public OperationResult saveTareasProgramadas(List<TareaProgramada> listTareas)
        {
            try
            {
                var XML = GenerateXML.generateListtareas(listTareas);
                //return new OperationResult { mensaje = "Error al contruir el XML", valor = 1 };
                if (XML == null)
                {
                    return new OperationResult { mensaje = "Error al contruir el XML", valor = 1 };
                }
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_saveTareasProgramadas";
                        
                        command.Parameters.Add(new SqlParameter("@xml", XML));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        //var identificador = command.Parameters.Add(new SqlParameter("@identificador", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        connection.Open();
                        var i = command.ExecuteReader();
                        
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)codigo.Value, result = new object() };
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }

        public OperationResult getTareasProgramadas(int idCliente, int id=0)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getTareasProgramadas";

                        command.Parameters.Add(new SqlParameter("@id", id));
                        command.Parameters.Add(new SqlParameter("@idCliente", idCliente));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        //var identificador = command.Parameters.Add(new SqlParameter("@identificador", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        connection.Open();
                        var i = command.ExecuteReader();
                        List<TareaProgramada> listTarea = new List<TareaProgramada>();
                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                listTarea.Add(new TareaProgramada
                                {
                                    idTareaProgramada = (int)sqlReader["idTareaProgramada"],
                                    idProducto = (int)sqlReader["idProducto"],
                                    DescripcionProducto = (string)sqlReader["DescripcionProducto"],
                                    valorProgramado = (decimal)sqlReader["valorProgramado"],
                                    destino = (string)sqlReader["destino"],
                                    remision = (int)sqlReader["remision"],
                                    hoja = (string)sqlReader["hoja"],
                                    estatus = (bool)sqlReader["estatus"],
                                    fecha = (DateTime)sqlReader["fecha"]
                                });
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)codigo.Value, result = listTarea };
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }
    }
}
