﻿using Core.Models;
using Core.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoreFletera.BusinessLogic;
using CoreFletera.Models;

namespace Core.BusinessLogic
{
    public class TipoOrdenServicioServices
    {
        public OperationResult getOrdenServByIdPadreOrdSer(int idPadreOrdSer)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getOrdenServByIdPadreOrdSer";
                        command.Parameters.Add(new SqlParameter("@idPadreOrdSer", idPadreOrdSer));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        connection.Open();
                        var i = command.ExecuteReader();
                        List<TipoOrdenServicio> listTipoOrdenServicio = new List<TipoOrdenServicio>();

                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                TipoOrdenServicio tipoOrden = new TipoOrdenServicio()
                                {
                                    idTipoOrdenServicio = (int)sqlReader["idTipOrdServ"],
                                    TipoServicio = (string)sqlReader["NomTipOrdServ"],
                                    idOrdenSer = (int)sqlReader["idOrdenSer"]
                                    ,
                                    unidadTrans = ((List<UnidadTransporte>)(new UnidadTransporteServices().getUnidadesALLorById(0, (string)sqlReader["idUnidadTrans"]).result))[0],
                                    estatus = (string)sqlReader["Estatus"]
                                };
                                if ((int)sqlReader["idEmpleadoEntrega"] != 0)
                                {
                                    var resp = new OperadoresServices().getPersonalALLorById((int)sqlReader["idEmpleadoEntrega"]);
                                    if (resp.typeResult == ResultTypes.success)
                                    {
                                        tipoOrden.personaEntrega = ((List<Personal>)resp.result)[0];
                                    }
                                    else
                                    {
                                        tipoOrden.personaEntrega = null;
                                    }
                                }
                                else
                                    tipoOrden.personaEntrega = null;

                                listTipoOrdenServicio.Add(tipoOrden);

                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)codigo.Value, result = listTipoOrdenServicio };
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }

        public OperationResult getOrdenServById(int idOrden)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getOrdenServById";
                        command.Parameters.Add(new SqlParameter("@idOrden", idOrden));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        connection.Open();
                        var i = command.ExecuteReader();
                        TipoOrdenServicio tipoOrden = new TipoOrdenServicio();

                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                tipoOrden = new TipoOrdenServicio()
                                {
                                    idTipoOrdenServicio = (int)sqlReader["idTipOrdServ"],
                                    TipoServicio = (string)sqlReader["NomTipOrdServ"],
                                    idOrdenSer = (int)sqlReader["idOrdenSer"],
                                    unidadTrans = ((List<UnidadTransporte>)(new UnidadTransporteServices().getUnidadesALLorById(0, (string)sqlReader["idUnidadTrans"]).result))[0],
                                    estatus = (string)sqlReader["Estatus"],
                                    fechaCaptura = (DateTime)sqlReader["fechaCaptura"],
                                    usuarioCrea = (string)sqlReader["usuarioCrea"],
                                    chofer = (string)sqlReader["chofer"],
                                    tipoOrden = new CoreFletera.Models.TipoOrden
                                    {
                                        idTipoOrden = (int)sqlReader["idTipoServicio"],
                                        nombreTipoOrden = (string)sqlReader["NomTipoServicio"]
                                    },
                                    externo = (bool)sqlReader["externo"],
                                    idChofer = (int)sqlReader["idChofer"]

                                };
                                if ((int)sqlReader["idEmpleadoEntrega"] != 0)
                                {
                                    var resp = new OperadoresServices().getPersonalALLorById((int)sqlReader["idEmpleadoEntrega"]);
                                    if (resp.typeResult == ResultTypes.success)
                                    {
                                        tipoOrden.personaEntrega = ((List<Personal>)resp.result)[0];
                                    }
                                    else
                                    {
                                        tipoOrden.personaEntrega = null;
                                    }
                                }
                                else
                                {
                                    tipoOrden.personaEntrega = null;
                                }

                                if (!DBNull.Value.Equals(sqlReader["idProveedor"]))
                                {
                                    var r = new mapComunes().mapProveedor(sqlReader);
                                    tipoOrden.proveedor = r;
                                }
                                else
                                {
                                    tipoOrden.proveedor = null;
                                }

                                tipoOrden.factura = (string)sqlReader["factura"];
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)codigo.Value, result = tipoOrden };
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }

        public OperationResult getOrdenServByFechas(DateTime fechaInicio, DateTime fechaFin, int _tipoOrden = 0)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getOrdenServByFecha";
                        command.Parameters.Add(new SqlParameter("@fechaInicio", fechaInicio));
                        command.Parameters.Add(new SqlParameter("@fechaFin", fechaFin));
                        command.Parameters.Add(new SqlParameter("@tipoOrden", _tipoOrden));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        connection.Open();
                        var i = command.ExecuteReader();
                        List<TipoOrdenServicio> lista = new List<TipoOrdenServicio>();

                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                TipoOrdenServicio tipoOrden = new TipoOrdenServicio()
                                {
                                    idTipoOrdenServicio = (int)sqlReader["idTipOrdServ"],
                                    TipoServicio = (string)sqlReader["NomTipOrdServ"],
                                    idOrdenSer = (int)sqlReader["idOrdenSer"],
                                    unidadTrans = ((List<UnidadTransporte>)(new UnidadTransporteServices().getUnidadesALLorById(0, (string)sqlReader["idUnidadTrans"]).result))[0],
                                    estatus = (string)sqlReader["Estatus"],
                                    fechaCaptura = (DateTime)sqlReader["fechaCaptura"],
                                    usuarioCrea = (string)sqlReader["usuarioCrea"],
                                    chofer = (string)sqlReader["chofer"]
                                };
                                if ((int)sqlReader["idEmpleadoEntrega"] != 0)
                                {
                                    var resp = new OperadoresServices().getPersonalALLorById((int)sqlReader["idEmpleadoEntrega"]);
                                    if (resp.typeResult == ResultTypes.success)
                                    {
                                        tipoOrden.personaEntrega = ((List<Personal>)resp.result)[0];
                                    }
                                    else
                                    {
                                        tipoOrden.personaEntrega = null;
                                    }
                                }
                                else
                                {
                                    tipoOrden.personaEntrega = null;
                                }

                                lista.Add(tipoOrden);

                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)codigo.Value, result = lista };
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }

        public OperationResult getUltimaOrdenServDiagByIdUnidadTrans(string unidadTransporte)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = CrearSqlCommand.getCommand(connection, "sp_getUltimaOrdenServDiagByIdUnidadTrans"))
                    {
                        command.Parameters.Add(new SqlParameter("@unidadTransporte", unidadTransporte));
                        connection.Open();
                        var i = command.ExecuteReader();
                        TipoOrdenServicio tipoOrden = new TipoOrdenServicio();

                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                tipoOrden = new TipoOrdenServicio()
                                {
                                    idTipoOrdenServicio = (int)sqlReader["idTipOrdServ"],
                                    TipoServicio = (string)sqlReader["NomTipOrdServ"],
                                    idOrdenSer = (int)sqlReader["idOrdenSer"],
                                    unidadTrans = ((List<UnidadTransporte>)(new UnidadTransporteServices().getUnidadesALLorById(0, (string)sqlReader["idUnidadTrans"]).result))[0],
                                    estatus = (string)sqlReader["Estatus"],
                                    fechaCaptura = (DateTime)sqlReader["fechaCaptura"],
                                    usuarioCrea = (string)sqlReader["usuarioCrea"],
                                    chofer = (string)sqlReader["chofer"],
                                    tipoOrden = new TipoOrden
                                    {
                                        idTipoOrden = (int)sqlReader["idTipoServicio"],
                                        nombreTipoOrden = (string)sqlReader["NomTipoServicio"]
                                    }
                                };
                                if ((int)sqlReader["idEmpleadoEntrega"] != 0)
                                {
                                    var resp = new OperadoresServices().getPersonalALLorById((int)sqlReader["idEmpleadoEntrega"]);
                                    if (resp.typeResult == ResultTypes.success)
                                    {
                                        tipoOrden.personaEntrega = ((List<Personal>)resp.result)[0];
                                    }
                                    else
                                    {
                                        tipoOrden.personaEntrega = null;
                                    }
                                }
                                else
                                    tipoOrden.personaEntrega = null;
                            }
                        }
                        return new OperationResult(command, tipoOrden);
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }
    }

}
