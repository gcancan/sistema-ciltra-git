﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using Core.Utils;
using System.Data;
using System.Data.SqlClient;
using Core.BusinessLogic;
namespace CoreFletera.BusinessLogic
{
    public class RescateUnidadServices
    {
        internal OperationResult saveRescateLlantas(ref RescateLlantas rescate)
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    connection.Open();
                    using (SqlTransaction transaction = connection.BeginTransaction("saveRescate"))
                    {
                        using (SqlCommand command = connection.CreateCommand())
                        {
                            command.Transaction = transaction;
                            command.CommandType = CommandType.StoredProcedure;
                            command.CommandText = "sp_saveRescateLlantas";

                            command.Parameters.Add(new SqlParameter("@idRescateLlanta", rescate.idRescateLlanta));
                            command.Parameters.Add(new SqlParameter("@idUnidadTransporte", rescate.unidadTransporte.clave));
                            command.Parameters.Add(new SqlParameter("@idChofer", rescate.chofer.clave));
                            command.Parameters.Add(new SqlParameter("@idLlantero", rescate.Llantero.clave));
                            command.Parameters.Add(new SqlParameter("@fechaSolicitud", rescate.fechaSolicitud));
                            command.Parameters.Add(new SqlParameter("@fechaSalida", rescate.fechaSalida));
                            command.Parameters.Add(new SqlParameter("@fechaEntrada", rescate.fechaEntrada));
                            command.Parameters.Add(new SqlParameter("@ubicacion", rescate.unicacion));
                            command.Parameters.Add(new SqlParameter("@estatus", rescate.estatus));
                            command.Parameters.Add(new SqlParameter("@usuario", rescate.usuario));

                            SqlParameter spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                            SqlParameter spId = command.Parameters.Add(new SqlParameter("@id", SqlDbType.Int) { Direction = ParameterDirection.Output });
                            SqlParameter spMensaje = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                            command.ExecuteNonQuery();

                            if ((int)spValor.Value == 0)
                            {
                                rescate.idRescateLlanta = (int)spId.Value;
                                foreach (DetalleRescateLlanta item in rescate.listaDetalles)
                                {
                                    command.Parameters.Clear();
                                    command.CommandText = "sp_saveDetalleRescateLlanta";

                                    command.Parameters.Add(new SqlParameter("@idRescateLlanta", rescate.idRescateLlanta));
                                    command.Parameters.Add(new SqlParameter("@idDetalleRescateLlanta", item.idDetalleRescateLlanta));
                                    command.Parameters.Add(new SqlParameter("@idLlantaLleva", item.llantaLleva.idLlanta));
                                    command.Parameters.Add(new SqlParameter("@idLlantaTrae", item.llantaTrae == null ? null : (int?)item.llantaTrae.idLlanta));
                                    command.Parameters.Add(new SqlParameter("@posicion", item.posicion));
                                    command.Parameters.Add(new SqlParameter("@profuncidad", item.profundidad));
                                    command.Parameters.Add(new SqlParameter("@idCondicion", item.condicion == null ? null : (int?)item.condicion.idCondicion));
                                    command.Parameters.Add(new SqlParameter("@idMotivoRescate", item.motivoRescate == null ? null : (int?)item.motivoRescate.idMotivoRescate));
                                    command.Parameters.Add(new SqlParameter("@usuario", rescate.usuario));
                                    command.Parameters.Add(new SqlParameter("@idUnidadTransporte", rescate.unidadTransporte.clave));

                                    spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                                    spId = command.Parameters.Add(new SqlParameter("@id", SqlDbType.Int) { Direction = ParameterDirection.Output });
                                    spMensaje = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                                    command.ExecuteNonQuery();

                                    if ((int)spValor.Value == 0)
                                    {
                                        item.idDetalleRescateLlanta = (int)spId.Value;
                                    }
                                    else
                                    {
                                        transaction.Rollback("saveRescate");
                                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = null };
                                    }
                                }

                                transaction.Commit();
                                return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = rescate };
                            }
                            else
                            {
                                transaction.Rollback("saveRescate");
                                return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = null };
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }

        internal OperationResult getRescateByid(int idRescate)
        {
            try
            {
                using (SqlConnection conection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comand = conection.CreateCommand())
                    {
                        comand.CommandType = CommandType.StoredProcedure;
                        comand.CommandText = "sp_getRescateByid";
                        comand.Parameters.Add(new SqlParameter("@idRescate", idRescate));

                        SqlParameter spValor = comand.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        SqlParameter spMensaje = comand.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                        conection.Open();
                        RescateLlantas rescate = new RescateLlantas();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                rescate = new mapComunes().mapRescate(sqlReader);
                                if (rescate == null)
                                {
                                    return new OperationResult { valor = 1, mensaje = "ERROR AL MAPEAR LOS RESULTADOS DE LA BUSQUEDA" };
                                }

                                OperationResult resp = new OperadoresServices().getPersonalALLorById(rescate.chofer.clave);
                                if (resp.typeResult == ResultTypes.success)
                                {
                                    rescate.chofer = ((List<Personal>)resp.result)[0];
                                }
                                else
                                {
                                    return resp;
                                }
                                resp = new OperadoresServices().getPersonalALLorById(rescate.Llantero.clave);
                                if (resp.typeResult == ResultTypes.success)
                                {
                                    rescate.Llantero = ((List<Personal>)resp.result)[0];
                                }
                                else
                                {
                                    return resp;
                                }

                                resp = getDetallesRescateByIdRescate(rescate.idRescateLlanta);
                                if (resp.typeResult == ResultTypes.success)
                                {
                                    rescate.listaDetalles = new List<DetalleRescateLlanta>();
                                    rescate.listaDetalles = (List<DetalleRescateLlanta>)resp.result;
                                }
                                else
                                {
                                    return resp;
                                }

                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = rescate };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }

        internal OperationResult getDetallesRescateByIdRescate(int idRescate)
        {
            try
            {
                using (SqlConnection conection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comand = conection.CreateCommand())
                    {
                        comand.CommandType = CommandType.StoredProcedure;
                        comand.CommandText = "sp_getDetallesRescateByIdRescate";
                        comand.Parameters.Add(new SqlParameter("@idRescate", idRescate));

                        SqlParameter spValor = comand.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        SqlParameter spMensaje = comand.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                        conection.Open();
                        List<DetalleRescateLlanta> listaDetalle = new List<DetalleRescateLlanta>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                DetalleRescateLlanta detalle = new mapComunes().mapDetalleRescate(sqlReader);
                                if (detalle == null)
                                {
                                    return new OperationResult { valor = 1, mensaje = "ERROR AL MAPEAR LOS RESULTADOS DE LA BUSQUEDA" };
                                }

                                OperationResult resp = new LlantasServices().getLlantaByIdLlanta(detalle.llantaLleva.idLlanta);
                                if (resp.typeResult == ResultTypes.success)
                                {
                                    detalle.llantaLleva = (Llanta)resp.result;
                                }
                                else
                                {
                                    return resp;
                                }
                                if (detalle.llantaTrae != null)
                                {
                                    resp = new LlantasServices().getLlantaByIdLlanta(detalle.llantaTrae.idLlanta);
                                    if (resp.typeResult == ResultTypes.success)
                                    {
                                        detalle.llantaTrae = (Llanta)resp.result;
                                    }
                                    else
                                    {
                                        return resp;
                                    }
                                }

                                listaDetalle.Add(detalle);
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = listaDetalle };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }

        internal OperationResult getMotivoRescateAllOrBy(int idMotivoRescate=0)
        {
            try
            {
                using (SqlConnection conection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comand = conection.CreateCommand())
                    {
                        comand.CommandType = CommandType.StoredProcedure;
                        comand.CommandText = "sp_getMotivoRescateAllOrBy";
                        comand.Parameters.Add(new SqlParameter("@idMotivo", idMotivoRescate));

                        SqlParameter spValor = comand.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        SqlParameter spMensaje = comand.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                        conection.Open();
                        List<MotivosRescate> lista = new List<MotivosRescate>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                MotivosRescate detalle = new mapComunes().mapMotivoRescate(sqlReader);
                                if (lista == null)
                                {
                                    return new OperationResult { valor = 1, mensaje = "ERROR AL MAPEAR LOS RESULTADOS DE LA BUSQUEDA" };
                                }
                                lista.Add(detalle);
                            }
                        }
                        if (idMotivoRescate == 0)
                        {
                            return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = lista };
                        }
                        else
                        {
                            return new OperationResult
                            {
                                valor = (int)spValor.Value,
                                mensaje = (string)spMensaje.Value,
                                result = (int)spValor.Value == 0 ? lista[0] : null
                            };
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }
    }
}
