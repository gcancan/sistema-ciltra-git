﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using Core.Utils;
using System.Data;
using System.Data.SqlClient;
namespace CoreFletera.BusinessLogic
{
    public class BitacoraAccesoSercices
    {
        public OperationResult saveBitacoraAcceso(ref BitacoraAcceso bitacoraAcceso)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    con.Open();
                    using (SqlTransaction tran = con.BeginTransaction())
                    {
                        using (SqlCommand comad = CrearSqlCommand.getCommand(con , "sp_saveBitacoraAcceso", true))
                        {
                            comad.Transaction = tran;
                            comad.Parameters.Add(new SqlParameter("@acceso", bitacoraAcceso.acceso));
                            comad.Parameters.Add(new SqlParameter("@usuario", bitacoraAcceso.usuario));
                            comad.Parameters.Add(new SqlParameter("@contraseña", bitacoraAcceso.contraseña));
                            comad.Parameters.Add(new SqlParameter("@nombreEquipo", bitacoraAcceso.nombreEquipo));
                            comad.ExecuteNonQuery();
                            int id = 0;
                            if (CrearSqlCommand.validarCorrecto(comad, out id))
                            {
                                bitacoraAcceso.idBitacoraAcceso = id;
                                tran.Commit();
                            }
                            else
                            {
                                tran.Rollback();
                            }
                            return new OperationResult(comad, bitacoraAcceso);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
}
