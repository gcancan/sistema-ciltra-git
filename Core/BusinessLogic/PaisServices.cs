﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using Core.Utils;
using System.Data;
using System.Data.SqlClient;
namespace CoreFletera.BusinessLogic
{
    public class PaisServices
    {
        public OperationResult getAllPais()
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getAllPais"))
                    {
                        con.Open();
                        List<Pais> lista = new List<Pais>();
                        using (SqlDataReader sqlReader = comad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                lista.Add(new Pais
                                {
                                    idPais = (int)sqlReader["idPais"],
                                    nombrePais = (string)sqlReader["NomPais"]
                                });
                            }
                        }
                        return new OperationResult(comad, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult savePais(ref Pais pais)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    con.Open();
                    using (SqlTransaction tran = con.BeginTransaction())
                    {
                        using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_savePais", true))
                        {
                            comad.Transaction = tran;
                            comad.Parameters.Add(new SqlParameter("@nombrePais", pais.nombrePais));
                            int id = 0;
                            comad.ExecuteNonQuery();
                            if (CrearSqlCommand.validarCorrecto(comad, out id)) 
                            {
                                pais.idPais = id;
                                tran.Commit();
                            }
                            else
                            {
                                tran.Rollback();
                            }
                            return new OperationResult(comad, pais);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
}
