﻿namespace Core.BusinessLogic
{
    using Core.Models;
    using Core.Utils;
    using CoreFletera.BusinessLogic;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;

    public class MarcasServices
    {
        public OperationResult getMarcasALLorById(int id)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getMarcasALLorById";
                        command.Parameters.Add(new SqlParameter("@id", id));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        List<Marca> list = new List<Marca>();
                        using (SqlDataReader reader2 = reader)
                        {
                            while (reader2.Read())
                            {
                                Marca item = new Marca
                                {
                                    clave = (int)reader2["Clave"],
                                    descripcion = (string)reader2["Descripcion"],
                                    llantas = (bool)reader2["llantas"],
                                    uniTrans = (bool)reader2["uniTrans"],
                                    activo = (bool)reader2["activo"]
                                };
                                list.Add(item);
                            }
                        }
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = list
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
            return result;
        }

        public OperationResult getMarcasLlantas()
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getAllMarcasLlantas", false))
                    {
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        List<Marca> list = new List<Marca>();
                        using (SqlDataReader reader2 = reader)
                        {
                            while (reader2.Read())
                            {
                                Marca item = new Marca
                                {
                                    clave = (int)reader2["Clave"],
                                    descripcion = (string)reader2["Descripcion"],
                                    llantas = (bool)reader2["llantas"],
                                    uniTrans = (bool)reader2["uniTrans"],
                                    activo = (bool)reader2["activo"]
                                };
                                list.Add(item);
                            }
                        }
                        result = new OperationResult(command, list);
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult(exception);
            }
            return result;
        }
        public OperationResult getMarcasUnidades()
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getAllMarcasUnidades", false))
                    {
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        List<Marca> list = new List<Marca>();
                        using (SqlDataReader reader2 = reader)
                        {
                            while (reader2.Read())
                            {
                                Marca item = new Marca
                                {
                                    clave = (int)reader2["Clave"],
                                    descripcion = (string)reader2["Descripcion"],
                                    llantas = (bool)reader2["llantas"],
                                    uniTrans = (bool)reader2["uniTrans"],
                                    activo = (bool)reader2["activo"]
                                };
                                list.Add(item);
                            }
                        }
                        result = new OperationResult(command, list);
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult(exception);
            }
            return result;
        }

        public OperationResult saveMarcaLlantas(ref Marca marca)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    connection.Open();
                    using (SqlTransaction transaction = connection.BeginTransaction())
                    {
                        using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_saveMarcas", true))
                        {
                            command.Transaction = transaction;
                            command.Parameters.Add(new SqlParameter("@clave", marca.clave));
                            command.Parameters.Add(new SqlParameter("@nombre", marca.descripcion));
                            command.Parameters.Add(new SqlParameter("@llantas", marca.llantas));
                            command.Parameters.Add(new SqlParameter("@uniTrans", marca.uniTrans));
                            command.Parameters.Add(new SqlParameter("@activo", marca.activo));
                            command.ExecuteNonQuery();
                            int id = 0;
                            if (CrearSqlCommand.validarCorrecto(command, out id))
                            {
                                marca.clave = id;
                                transaction.Commit();
                            }
                            else
                            {
                                transaction.Rollback();
                            }
                            result = new OperationResult(command, marca);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult(exception);
            }
            return result;
        }
    }
}
