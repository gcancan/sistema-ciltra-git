﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using Core.Utils;
using Core.BusinessLogic;
using System.Data;
using System.Data.SqlClient;
namespace CoreFletera.BusinessLogic
{
    public class AnticipoServices
    {
        public OperationResult getTipoAnticipos()
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getTiposAnticipos"))
                    {
                        List<TipoAnticipo> listaTipoAnticipo = new List<TipoAnticipo>();
                        con.Open();
                        using (SqlDataReader sqlReader = comad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                TipoAnticipo tipoAnticipo = new mapComunes().mapTipoAnticipo(sqlReader);

                                if (tipoAnticipo == null) return new OperationResult(ResultTypes.warning, "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");

                                listaTipoAnticipo.Add(tipoAnticipo);
                            }
                        }
                        return new OperationResult(comad, listaTipoAnticipo);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
}
