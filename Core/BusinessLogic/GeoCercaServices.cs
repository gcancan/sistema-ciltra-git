﻿namespace CoreFletera.BusinessLogic
{
    using Core.BusinessLogic;
    using Core.Models;
    using Core.Utils;
    using CoreFletera.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;

    public class GeoCercaServices
    {
        public OperationResult getGeoCerca()
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getGeoCercas", false))
                    {
                        connection.Open();
                        List<GeoCerca> list = new List<GeoCerca>();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                GeoCerca item = new GeoCerca
                                {
                                    idGeoCerca = (int)reader["idGeoCerca"],
                                    nombre = (string)reader["nombre"],
                                    granja = (bool)reader["granja"],
                                    planta = (bool)reader["planta"],
                                    taller = (bool)reader["taller"],
                                    clasificacion = (enumClasificacionGeoCerca)Convert.ToInt32(reader["clasificacion"])
                                };
                                if (DBNull.Value.Equals(reader["destino_clave"]))
                                {
                                    item.zona = null;
                                }
                                else
                                {
                                    Zona zona = new mapComunes().mapDestino(reader);
                                    item.zona = zona;
                                }
                                list.Add(item);
                            }
                        }
                        result = new OperationResult(command, list);
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult(exception);
            }
            return result;
        }

        public OperationResult getGeoCerca(string nombreGeoCerca, string strConnnestion)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = new SqlConnection(strConnnestion))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getGeoCercaBynombreGeoCerca", false))
                    {
                        command.Parameters.Add(new SqlParameter("nombreGeoCerca", nombreGeoCerca));
                        connection.Open();
                        GeoCerca geo = new GeoCerca();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                geo = new GeoCerca
                                {
                                    idGeoCerca = (int)reader["idGeoCerca"],
                                    nombre = (string)reader["nombre"],
                                    granja = (bool)reader["granja"],
                                    planta = (bool)reader["planta"],
                                    taller = (bool)reader["taller"],
                                    clasificacion = (enumClasificacionGeoCerca)Convert.ToInt32(reader["clasificacion"])
                                };
                                if (DBNull.Value.Equals(reader["destino_clave"]))
                                {
                                    geo.zona = null;
                                }
                                else
                                {
                                    Zona zona = new mapComunes().mapDestino(reader);
                                    geo.zona = zona;
                                }
                            }
                        }
                        result = new OperationResult(command, geo);
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult(exception);
            }
            return result;
        }

        public OperationResult saveGeoCerca(ref GeoCerca geoCerca)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    connection.Open();
                    using (SqlTransaction transaction = connection.BeginTransaction())
                    {
                        using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_saveGeoCerca", true))
                        {
                            command.Transaction = transaction;
                            command.Parameters.Add(new SqlParameter("@idGeoCerca", geoCerca.idGeoCerca));
                            command.Parameters.Add(new SqlParameter("@nombre", geoCerca.nombre));
                            command.Parameters.Add(new SqlParameter("@granja", geoCerca.granja));
                            command.Parameters.Add(new SqlParameter("@planta", geoCerca.planta));
                            command.Parameters.Add(new SqlParameter("@taller", geoCerca.taller));
                            command.Parameters.Add(new SqlParameter("@idGranja", (geoCerca.clasificacion == enumClasificacionGeoCerca.GRANJA) ? new int?(geoCerca.zona.clave) : null));
                            command.Parameters.Add(new SqlParameter("@activo", geoCerca.activo));
                            command.Parameters.Add(new SqlParameter("@clasificacion", geoCerca.clasificacion));
                            command.ExecuteNonQuery();
                            int id = 0;
                            if (CrearSqlCommand.validarCorrecto(command, out id))
                            {
                                geoCerca.idGeoCerca = id;
                                transaction.Commit();
                            }
                            else
                            {
                                transaction.Rollback();
                            }
                            result = new OperationResult(command, geoCerca);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult(exception);
            }
            return result;
        }
    }
}
