﻿namespace CoreFletera.BusinessLogic
{
    using Core.Models;
    using Core.Utils;
    using CoreFletera.Models;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Runtime.InteropServices;

    public class DiseñoLlantaServices
    {
        public OperationResult getDiseñosLlanta()
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getDiseñosLlantas", false))
                    {
                        connection.Open();
                        List<Diseño> list = new List<Diseño>();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Diseño item = new Diseño
                                {
                                    idDiseño = (int)reader["idDisenio"],
                                    descripcion = (string)reader["diseño"],
                                    activo = Convert.ToBoolean(reader["diseño_activo"]),
                                    medida = DBNull.Value.Equals(reader["idMedidaLlanta"]) ? null : new MedidaLlanta
                                    {
                                        idMedidaLlanta = (int)reader["idMedidaLlanta"],
                                        medida = (string)reader["medida"]
                                    },
                                    profundidad = (int)reader["profundidad"],
                                    tipoLlanta = DBNull.Value.Equals(reader["idTipoLLanta"]) ? null : new TipoLlanta()
                                    {
                                        clave = (int)reader["idTipoLLanta"],
                                        nombre = (string)reader["tipoLlanta"]
                                    }
                                };
                                list.Add(item);
                            }
                        }
                        result = new OperationResult(command, list);
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            return result;
        }

        public OperationResult getDiseñosLlantaAllOrById(int idDiseño = 0)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getDise\x00f1osLlantaAllOrById";
                        command.Parameters.Add(new SqlParameter("@idDise\x00f1o", idDiseño));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        List<Diseño> list = new List<Diseño>();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Diseño item = new Diseño
                                {
                                    idDiseño = (int)reader["idDisenio"],
                                    descripcion = (string)reader["Descripcion"]
                                };
                                list.Add(item);
                            }
                        }
                        if (idDiseño == 0)
                        {
                            return new OperationResult
                            {
                                valor = new int?((int)parameter.Value),
                                mensaje = (string)parameter2.Value,
                                result = list
                            };
                        }
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = list[0]
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            return result;
        }

        public OperationResult saveDiseñoLlanta(ref Diseño diseño)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    connection.Open();
                    using (SqlTransaction transaction = connection.BeginTransaction())
                    {
                        using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_saveDise\x00f1oLlantas", true))
                        {
                            command.Transaction = transaction;
                            command.Parameters.Add(new SqlParameter("@idDise\x00f1o", diseño.idDiseño));
                            command.Parameters.Add(new SqlParameter("@dise\x00f1o", diseño.descripcion));
                            command.Parameters.Add(new SqlParameter("@activo", 1));
                            command.Parameters.Add(new SqlParameter("@idMedidaLlanta", diseño.medida.idMedidaLlanta));
                            command.Parameters.Add(new SqlParameter("@idTipoLlanta", diseño.tipoLlanta.clave));
                            command.Parameters.Add(new SqlParameter("@profundidad", diseño.profundidad));
                            command.ExecuteNonQuery();
                            int id = 0;
                            if (CrearSqlCommand.validarCorrecto(command, out id))
                            {
                                diseño.idDiseño = id;
                                transaction.Commit();
                            }
                            else
                            {
                                transaction.Rollback();
                            }
                            result = new OperationResult(command, diseño);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult(exception);
            }
            return result;
        }
    }
}
