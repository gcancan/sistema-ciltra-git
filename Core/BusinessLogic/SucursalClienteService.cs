﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using Core.Utils;
using System.Data;
using System.Data.SqlClient;
namespace CoreFletera.BusinessLogic
{
    public class SucursalClienteService
    {
        public OperationResult sincronizarSucursalCliente(List<SucursalCliente> listaSucursal)
        {
			try
			{
				string xml = GenerateXML.getListaSucursalClientesSinc(listaSucursal);
				using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
				{
					con.Open();
					using (SqlTransaction tran = con.BeginTransaction())
					{
						using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_sincronizarSucursalCliente", false, true))
						{
							comad.Transaction = tran;
							comad.Parameters.Add(new SqlParameter("@xml", xml));
							comad.ExecuteNonQuery();
							if (!CrearSqlCommand.validarCorrecto(comad))
							{
								tran.Rollback();
								return new OperationResult(comad);
							}
							tran.Commit();
							return new OperationResult(comad);
						}
					}
				}
			}
			catch (Exception ex)
			{
				return new OperationResult(ex);
			}
        }
    }
}
