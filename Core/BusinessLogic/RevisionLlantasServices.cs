﻿namespace CoreFletera.BusinessLogic
{
    using Core.Models;
    using Core.Utils;
    using CoreFletera.Models;
    using System;
    using System.Data.SqlClient;

    public class RevisionLlantasServices
    {
        public OperationResult saveRevisionLlanta(ref RevisionLlantas revisionLlantas)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    connection.Open();
                    using (SqlTransaction transaction = connection.BeginTransaction())
                    {
                        using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_saveRevisionLlantas", true))
                        {
                            command.Transaction = transaction;
                            command.Parameters.Add(new SqlParameter("@fechaRevision", revisionLlantas.fechaRevision));
                            command.Parameters.Add(new SqlParameter("@usuario", revisionLlantas.usuario));
                            command.Parameters.Add(new SqlParameter("@idOrdenServ", revisionLlantas.idOrdenServ));
                            command.Parameters.Add(new SqlParameter("@idUnidadTrans", revisionLlantas.idUnidadTrans));
                            command.Parameters.Add(new SqlParameter("@idEmpresa", revisionLlantas.idEmpresa));
                            command.Parameters.Add(new SqlParameter("@km", revisionLlantas.km));
                            command.ExecuteNonQuery();
                            int id = 0;
                            if (CrearSqlCommand.validarCorrecto(command, out id))
                            {
                                revisionLlantas.idRevisionLlanta = id;
                                command.CommandText = "sp_saveDetalleRevisionLlanta";
                                foreach (DetalleRevisionLlantas llantas in revisionLlantas.listaDetalles)
                                {
                                    command.Parameters.Clear();
                                    foreach (SqlParameter parameter in CrearSqlCommand.getPatametros(true))
                                    {
                                        command.Parameters.Add(parameter);
                                    }
                                    command.Parameters.Add(new SqlParameter("@idRevisionLlanta", revisionLlantas.idRevisionLlanta));
                                    command.Parameters.Add(new SqlParameter("@idLlanta", llantas.llanta.idLlanta));
                                    command.Parameters.Add(new SqlParameter("@profAnterior", llantas.profundidadAnterior));
                                    command.Parameters.Add(new SqlParameter("@profNueva", llantas.profundidadNueva));
                                    command.Parameters.Add(new SqlParameter("@kmAnterior", llantas.kmAnterior));
                                    command.Parameters.Add(new SqlParameter("@kmNuevo", llantas.kmNuevo));
                                    command.ExecuteNonQuery();
                                    id = 0;
                                    if (CrearSqlCommand.validarCorrecto(command, out id))
                                    {
                                        llantas.idDetalleRevisionLlanta = id;
                                    }
                                    else
                                    {
                                        transaction.Rollback();
                                        return new OperationResult(command, null);
                                    }
                                }
                            }
                            else
                            {
                                transaction.Rollback();
                            }
                            transaction.Commit();
                            result = new OperationResult(command, revisionLlantas);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult(exception);
            }
            return result;
        }
    }
}
