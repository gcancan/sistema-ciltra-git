﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using Core.Utils;
using Core.BusinessLogic;
using System.Data;
using System.Data.SqlClient;
namespace CoreFletera.BusinessLogic
{
    public class InsersionKmServices
    {
        public OperationResult saveInsersionKm(ref InsersionKm insersionKm)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    con.Open();
                    using (SqlTransaction tran = con.BeginTransaction())
                    {
                        using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_insersionKm", true))
                        {
                            comad.Transaction = tran;
                            comad.Parameters.Add(new SqlParameter("@idUnidadTransporte", insersionKm.unidadTransporte.clave));
                            comad.Parameters.Add(new SqlParameter("@usuarioCaptura", insersionKm.usuarioCaptura));
                            comad.Parameters.Add(new SqlParameter("@fechaInsersion", insersionKm.fechaInsersion));
                            comad.Parameters.Add(new SqlParameter("@tipoInsersion", (int)insersionKm.enumTipoInsersion));
                            comad.Parameters.Add(new SqlParameter("@idServicio", insersionKm.servicio == null ? null : (int?)insersionKm.servicio.idServicio));
                            comad.Parameters.Add(new SqlParameter("@km", insersionKm.km));
                            comad.ExecuteNonQuery();
                            int id = 0;
                            if (CrearSqlCommand.validarCorrecto(comad, out id))
                            {
                                insersionKm.idInsersionKm = id;
                                tran.Commit();
                            }
                            else
                            {
                                tran.Rollback();
                            }
                            return new OperationResult(comad, insersionKm);                            
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getUltimaInsercionByUnidadAndTipo(enumTipoInsersionKM tipoInsersion, string idUnidad)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(con, "sp_getUltimaInsercionByUnidadAndTipo"))
                    {
                        command.Parameters.Add(new SqlParameter("@idUnidad", idUnidad));
                        command.Parameters.Add(new SqlParameter("@tipoInsersion", (int)tipoInsersion));
                        con.Open();
                        InsersionKm insersion = new InsersionKm();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                insersion = new InsersionKm
                                {
                                    idInsersionKm = (int)reader["insersion_clave"],
                                    enumTipoInsersion = (enumTipoInsersionKM)reader["insersion_tipoInsersion"],
                                    fechaInsersion =  (DateTime)reader["insersion_fechaInsersion"],
                                    km = (decimal)reader["insersion_km"],
                                    fechaCaptura = (DateTime)reader["insersion_fechaCaptura"],
                                    usuarioCaptura = (string)reader["insersion_usuario"],
                                    unidadTransporte = new UnidadTransporte { clave = (string)reader["insersion_IdUnidad"] },
                                    servicio = DBNull.Value.Equals(reader["insersion_idServicio"]) ?
                                        null : new Servicio { idServicio = (int)reader["insersion_idServicio"], descipcion = (string)reader["insersion_servicio"] }
                                };
                            }
                        }
                        return new OperationResult(command, insersion);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
}
