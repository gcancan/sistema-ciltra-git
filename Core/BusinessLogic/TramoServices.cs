﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using Core.Utils;
using System.Data;
using System.Data.SqlClient;
using Core.BusinessLogic;

namespace CoreFletera.BusinessLogic
{
    public class TramoServices
    {
        public OperationResult saveTramo(ref Tramo tramo)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    con.Open();
                    using (SqlTransaction tran = con.BeginTransaction())
                    {
                        using (SqlCommand commad = CrearSqlCommand.getCommand(con, "sp_saveTramo", true))
                        {
                            commad.Transaction = tran;
                            commad.Parameters.Add(new SqlParameter("@idTramo", tramo.idTramo));
                            commad.Parameters.Add(new SqlParameter("@idDestino", tramo.destino.clave));
                            commad.Parameters.Add(new SqlParameter("@idOrigen", tramo.origen.idOrigen));
                            commad.Parameters.Add(new SqlParameter("@km", tramo.km));
                            int id = 0;
                            commad.ExecuteNonQuery();
                            if (CrearSqlCommand.validarCorrecto(commad, out id))
                            {
                                tramo.idTramo = id;
                                tran.Commit();
                            }
                            else
                            {
                                tran.Rollback();
                            }
                            return new OperationResult(commad, tramo);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getAllTramos()
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {

                    using (SqlCommand commad = CrearSqlCommand.getCommand(con, "sp_getAllTramos"))
                    {
                        con.Open();
                        List<Tramo> lista = new List<Tramo>();
                        using (SqlDataReader sqlReader = commad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                Tramo tramo = new mapComunes().mapTramo(sqlReader);
                                if (tramo == null)
                                {
                                    return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");
                                }
                                lista.Add(tramo);
                            }
                        }
                            return new OperationResult(commad, lista);
                    }

                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
}
