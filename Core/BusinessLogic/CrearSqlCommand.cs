﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.BusinessLogic
{
    public static class CrearSqlCommand
    {

        public static SqlCommand getCommand(SqlConnection con, string commadText, bool incluidID = false, bool contadores = false )
        {
            SqlCommand commad = con.CreateCommand();
            commad.CommandType = CommandType.StoredProcedure;
            commad.CommandText = commadText;

            //getPatametros(ref commad, incluidID);
            foreach (var item in getPatametros(incluidID, contadores))
            {
                commad.Parameters.Add(item);
            }
            return commad;
        }

        public static SqlCommand getCommandText(SqlConnection con, string commadText, bool incluidID = false, bool contadores = false)
        {
            SqlCommand commad = con.CreateCommand();
            commad.CommandType = CommandType.Text ;
            commad.CommandText = commadText;
            foreach (var item in getPatametros(incluidID))
            {
                commad.Parameters.Add(item);
            }
            return commad;
        }

        public static List<SqlParameter> getPatametros(bool incnluiID = false, bool contadores = false)
        {
            List<SqlParameter> listParameter = new List<SqlParameter>();
            if (incnluiID)
            {
                listParameter.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output, Value = 3 });
                listParameter.Add(new SqlParameter("@id", SqlDbType.Int) { Direction = ParameterDirection.Output, Value = 0 });
                listParameter.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = System.Data.ParameterDirection.Output, Value = "" });
            }
            else
            {
                listParameter.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output, Value = 3 });
                listParameter.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output, Value = "" });
            }

            if (contadores)
            {
                listParameter.Add(new SqlParameter("@nuevos", SqlDbType.Int) { Direction = ParameterDirection.Output, Value = 0 });
                listParameter.Add(new SqlParameter("@actualizados", SqlDbType.Int) { Direction = ParameterDirection.Output, Value = 0 });
            }

            return listParameter;
        }
        public static bool validarCorrecto(SqlCommand command)
        {
            if ((int)command.Parameters["@valor"].Value == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static bool validarCorrecto(SqlCommand command, out int id)
        {
            if ((int)command.Parameters["@valor"].Value == 0)
            {
                id = (int)command.Parameters["@id"].Value;
                return true;
            }
            else
            {
                id = 0;
                return false;
            }
        }
    }
}
