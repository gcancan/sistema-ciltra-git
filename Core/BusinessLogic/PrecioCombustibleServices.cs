﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using System.Data;
using System.Data.SqlClient;
using Core.Utils;

namespace CoreFletera.BusinessLogic
{
    public class PrecioCombustibleServices
    {
        public OperationResult savePrecioCombustible(ref PrecioCombustible precioCombustible)
        {
            try
            {
                using (SqlConnection conection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comand = conection.CreateCommand())
                    {
                        comand.CommandType = CommandType.StoredProcedure;
                        comand.CommandText = "sp_savePrecioTipoCombustible";

                        comand.Parameters.Add(new SqlParameter("@idTipoCombustible", precioCombustible.tipoCombustible.idTipoCombustible));
                        comand.Parameters.Add(new SqlParameter("@usuario", precioCombustible.usuario));
                        comand.Parameters.Add(new SqlParameter("@fecha", precioCombustible.fecha));
                        comand.Parameters.Add(new SqlParameter("@precio", precioCombustible.precio));

                        var spValor = comand.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var spMensaje = comand.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        var id = comand.Parameters.Add(new SqlParameter("@id", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        conection.Open();
                        var i = comand.ExecuteNonQuery();
                        if ((int)spValor.Value == 0)
                        {
                            precioCombustible.idPrecioCombustible = (int)id.Value;
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = precioCombustible, };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }
    }
}
