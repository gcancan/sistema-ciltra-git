﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using Core.Utils;
using System.Data;
using System.Data.SqlClient;
using Core.BusinessLogic;

namespace CoreFletera.BusinessLogic
{
    class TicketsCombustibleServices
    {
        public OperationResult getTicketsCombustibleByProveedor(int idProveedor, int idEmpresa, int idTipoCombustible)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getTicketsCombustibleByProveedor"))
                    {
                        comad.Parameters.Add(new SqlParameter("@idProveedor", idProveedor));
                        comad.Parameters.Add(new SqlParameter("@idEmpresa", idEmpresa));
                        comad.Parameters.Add(new SqlParameter("@idTipoCombustible", idTipoCombustible));
                        con.Open();
                        List<TicketsCombustible> lista = new List<TicketsCombustible>();
                        using (SqlDataReader reader = comad.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                TicketsCombustible tickets = new TicketsCombustible
                                {
                                    ticket = (string)reader["ticket"],
                                    fecha = (DateTime)reader["fecha"],
                                    vale = (int)reader["vale"],
                                    unidad = (string)reader["unidad"],
                                    folioImpreso = (string)reader["folioImpreso"],
                                    idOrdenServ = (int)reader["idOrdenServ"],
                                    litros = (decimal)reader["litros"],
                                    precio = (decimal)reader["precio"],
                                    importe = (decimal)reader["importe"],
                                    //subTotal = (decimal)reader["subTotal"],
                                    normal = Convert.ToBoolean(reader["normal"]),
                                    operador = (string)reader["operador"],
                                    folioViaje = (int)reader["folioViaje"],
                                    valorIEPS = (decimal)reader["valorIEPS"]
                                };
                                lista.Add(tickets);
                            }
                        }
                        return new OperationResult(comad, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult saveFaturaProveedorCombustible(ref FacturaProveedorCombustible factura)
        {
            try
            {
                string xml = GenerateXML.generarListaTickets(factura.listaDetalles);
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    con.Open();
                    using (SqlTransaction tran = con.BeginTransaction())
                    {
                        using (SqlCommand comand = CrearSqlCommand.getCommand(con, "sp_saveFacturaProveedor", true))
                        {
                            comand.Transaction = tran;
                            comand.Parameters.Add(new SqlParameter("@idEmpresa", factura.empresa.clave));
                            comand.Parameters.Add(new SqlParameter("@idZonaOperativa", factura.zonaOperativa?.idZonaOperativa));
                            comand.Parameters.Add(new SqlParameter("@idProveedor", factura.proveedor.idProveedor));
                            comand.Parameters.Add(new SqlParameter("@factura", factura.factura));
                            comand.Parameters.Add(new SqlParameter("@folioFical", factura.folioFiscal));
                            comand.Parameters.Add(new SqlParameter("@fechaFactura", factura.fechaFactura));
                            comand.Parameters.Add(new SqlParameter("@idTipoCombustible", factura.tipoCombustible.idTipoCombustible));
                            comand.Parameters.Add(new SqlParameter("@litros", factura.litros));
                            comand.Parameters.Add(new SqlParameter("@importe", factura.importe));
                            comand.Parameters.Add(new SqlParameter("@IEPS", factura.IEPS));
                            comand.Parameters.Add(new SqlParameter("@usuario", factura.usuario));
                            comand.Parameters.Add(new SqlParameter("@fechaCaptura", factura.fechaCaptura));
                            int id = 0;
                            comand.ExecuteNonQuery();
                            if (CrearSqlCommand.validarCorrecto(comand, out id))
                            {
                                factura.idFacturaProveedorCombustible = id;
                                comand.Parameters.Clear();
                                comand.CommandText = "sp_saveDetalleFacturaProveedor";
                                foreach (var param in CrearSqlCommand.getPatametros())
                                {
                                    comand.Parameters.Add(param);
                                }
                                comand.Parameters.Add(new SqlParameter("@xml", xml));
                                comand.Parameters.Add(new SqlParameter("@factura", factura.factura));
                                comand.Parameters.Add(new SqlParameter("@idFacturaProveedorCombustible", factura.idFacturaProveedorCombustible));
                                comand.ExecuteNonQuery();
                                if (!CrearSqlCommand.validarCorrecto(comand))
                                {
                                    factura.idFacturaProveedorCombustible = 0;
                                    tran.Rollback();
                                }
                                else
                                {
                                    tran.Commit();
                                }
                            }
                            else
                            {
                                tran.Rollback();
                            }

                            return new OperationResult(comand, factura);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getFacturasProveedorDieselByFiltros(int idEmpresa, DateTime fechaInicial, DateTime fechaFinal,
            List<string> listaZonasOperativas, bool todasZonasOpe,
            List<string> listaProveedores, bool todosProveedores,
            int idTipoCombustible)
        {
            try
            {
                string xmlZonas = GenerateXML.generarListaCadena(listaZonasOperativas);
                string xmlProveedores = GenerateXML.generarListaCadena(listaProveedores);
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "getFacturasProveedorCombustibleByFiltros"))
                    {
                        comad.Parameters.Add(new SqlParameter("@idEmpresa", idEmpresa));
                        //comad.Parameters.Add(new SqlParameter("@xmlZonas", xmlZonas));
                        //comad.Parameters.Add(new SqlParameter("@todoZonas", todasZonasOpe));
                        comad.Parameters.Add(new SqlParameter("@xmlProveedor", xmlProveedores));
                        comad.Parameters.Add(new SqlParameter("@todoProveedor", todosProveedores));
                        comad.Parameters.Add(new SqlParameter("@fechaInicial", fechaInicial));
                        comad.Parameters.Add(new SqlParameter("@fechaFinal", fechaFinal));
                        comad.Parameters.Add(new SqlParameter("@idTipoCombustible", idTipoCombustible));
                        con.Open();
                        List<FacturaProveedorCombustible> listaFacturas = new List<FacturaProveedorCombustible>();
                        using (SqlDataReader reader = comad.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                FacturaProveedorCombustible fac = new mapComunes().mapFacturaProveedor(reader);
                                if (fac != null)
                                {
                                    listaFacturas.Add(fac);
                                }
                                else
                                {
                                    return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");
                                }
                            }
                        }

                        var respDet = getDetallesFacturasProveedorDieselByIds(listaFacturas.Select(s => s.idFacturaProveedorCombustible.ToString()).ToList());
                        if (respDet.typeResult == ResultTypes.success)
                        {
                            var listaDetalles = respDet.result as List<DetalleFacturaProveedorCombustible>;
                            foreach (var factura in listaFacturas)
                            {
                                factura.listaDetalles = listaDetalles.FindAll(s => s.idFacturaProveedorCombustible == factura.idFacturaProveedorCombustible);
                            }
                        }
                        else
                        {
                            return respDet;
                        }

                        return new OperationResult(comad, listaFacturas);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getDetallesFacturasProveedorDieselByIds(List<string> listaIds)
        {
            try
            {
                string xml = GenerateXML.generarListaCadena(listaIds);
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "getDetallesFacturasProveedorDieselByIds"))
                    {

                        comad.Parameters.Add(new SqlParameter("@xml", xml));
                        con.Open();
                        List<DetalleFacturaProveedorCombustible> listaDetalles = new List<DetalleFacturaProveedorCombustible>();
                        using (SqlDataReader reader = comad.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                DetalleFacturaProveedorCombustible det = new mapComunes().mapDetalleFacturaProveedor(reader);
                                if (det != null)
                                {
                                    listaDetalles.Add(det);
                                }
                                else
                                {
                                    return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");
                                }
                            }
                        }
                        return new OperationResult(comad, listaDetalles);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getFacturasProveedorByFactura(int idProveedor, string strFactura)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "getFacturasProveedorByFactura"))
                    {
                        comad.Parameters.Add(new SqlParameter("@idProveedor", idProveedor));
                        comad.Parameters.Add(new SqlParameter("@factura", strFactura));
                        con.Open();
                        FacturaProveedorCombustible factura = new FacturaProveedorCombustible();
                        using (SqlDataReader reader = comad.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                factura = new mapComunes().mapFacturaProveedor(reader);
                                if (factura == null)
                                {
                                    return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");
                                }
                            }
                        }

                        var respDet = getDetallesFacturasProveedorDieselByIds(new List<string> { factura.idFacturaProveedorCombustible.ToString() });
                        if (respDet.typeResult == ResultTypes.success)
                        {
                            var listaDetalles = respDet.result as List<DetalleFacturaProveedorCombustible>;
                            factura.listaDetalles = listaDetalles.FindAll(s => s.idFacturaProveedorCombustible == factura.idFacturaProveedorCombustible);
                        }
                        else
                        {
                            return respDet;
                        }
                        return new OperationResult(comad, factura);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
}
