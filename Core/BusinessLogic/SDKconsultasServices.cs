﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Utils;
using System.Data;
using System.Data.SqlClient;

namespace Core.BusinessLogic
{
    public class SDKconsultasServices
    {
        public OperationResult existeProductoComercial(string id, string empresa = "")
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactoryTaller())
                {
                    using (var command = connection.CreateCommand())
                    {                        
                        command.CommandType = CommandType.Text;
                        command.Parameters.Add(new SqlParameter("@id", id));
                        command.CommandText =
                            @"BEGIN TRY
                                DECLARE @mensaje NVARCHAR(MAX) = '';
                                DECLARE @valor INT = 1;
                                --DECLARE @id NVARCHAR(MAX);
                                DECLARE @existe BIT = 0;
                                SELECT  @existe = CASE COUNT(1)
                                                    WHEN 0 THEN 0
                                                    ELSE 1
                                                  END
                                FROM    dbo.admProductos
                                WHERE   CCODIGOPRODUCTO = @id;
                                IF @existe = 1
                                    BEGIN
                                        SET @mensaje = 'El producto con clave: ' + @id + ' Existe';
                                        SET @valor = 0;
                                    END;
                                ELSE
                                    IF @existe = 0
                                        BEGIN 
                                            SET @mensaje = 'El producto con clave: ' + @id
                                                + ' No está dado de alta en el comercial';
                                            SET @valor = 3;
                                        END;
            
                                SELECT  @valor AS valor ,
                                        @mensaje AS mensaje;
                            END TRY
                            BEGIN CATCH
                                SET @valor = 1;
                                SET @mensaje = ERROR_MESSAGE();
                                SELECT  @valor AS valor ,
                                        @mensaje AS mensaje;
                            END CATCH;";                        
                        
                        connection.Open();
                        var i = command.ExecuteReader();
                        int spValor = 1;
                        string codigo = "";

                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                spValor = (int)sqlReader["valor"];
                                codigo = (string)sqlReader["mensaje"];                              
                            }
                        }
                        return new OperationResult { valor = spValor, mensaje = codigo};
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }

        public OperationResult updateMovimiento(string idMovimiento, string remision, string Producto)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory(true))
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.Parameters.Add(new SqlParameter("@Producto", Producto));
                        command.Parameters.Add(new SqlParameter("@remision", remision));
                        command.Parameters.Add(new SqlParameter("@idMovimiento", Convert.ToInt32(idMovimiento)));
                        command.CommandText =
                            @"BEGIN TRY
                                DECLARE @mensaje NVARCHAR(MAX) = '';
                                DECLARE @valor INT = 1;
                                --DECLARE @Producto NVARCHAR(50);
                                --DECLARE @remision NVARCHAR(50);
                                --DECLARE @idMovimiento NVARCHAR(25);

                                UPDATE  dbo.admMovimientos
                                SET     CTEXTOEXTRA1 = @Producto ,
                                        CTEXTOEXTRA2 = @remision
                                WHERE   CIDMOVIMIENTO = @idMovimiento;
                                SET @valor = 0;
                                SET @mensaje = 'CAMBIOS GUARDADOS CORRECTAMENTE';

                                SELECT  @valor AS valor ,
                                        @mensaje AS mensaje;
                            END TRY
                            BEGIN CATCH
                                SET @valor = 1;
                                SET @mensaje = ERROR_MESSAGE();
                                SELECT  @valor AS valor ,
                                        @mensaje AS mensaje;
                            END CATCH;";

                        connection.Open();
                        var i = command.ExecuteReader();
                        int spValor = 1;
                        string codigo = "";

                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                spValor = (int)sqlReader["valor"];
                                codigo = (string)sqlReader["mensaje"];
                            }
                        }
                        return new OperationResult { valor = spValor, mensaje = codigo };
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }

        public OperationResult buscarDatosCliente(int id)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory(true))
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.Parameters.Add(new SqlParameter("@id", id));
                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        command.CommandText =
                            @"BEGIN TRY
                                SELECT  C.CRAZONSOCIAL AS nombre ,
                                        C.CRFC AS rfc ,
                                        D.CNOMBRECALLE AS calle ,
                                        D.CIDDIRECCION ,
                                        D.CESTADO AS estado ,
                                        D.CPAIS AS pais ,
                                        D.CMUNICIPIO AS municipio ,
                                        D.CCOLONIA AS colonia ,
                                        D.CCODIGOPOSTAL AS cp,
                                        D.CTELEFONO1 AS telefono
                                FROM    dbo.admClientes AS C
                                        JOIN dbo.admDomicilios AS D ON C.CIDCLIENTEPROVEEDOR = D.CIDCATALOGO
                                WHERE   D.CTIPOCATALOGO = 1
                                        AND C.CCODIGOCLIENTE = CONVERT(NVARCHAR(50), @id);
                                IF @@ROWCOUNT <> 0
                                    BEGIN
                                        SET @mensaje = 'Se encontro resultado';
                                        SET @valor = 0;
                                    END;
                                ELSE
                                    BEGIN
                                        SET @mensaje = 'No se encontro el cliente';
                                        SET @valor = 3;
                                    END;
                            END TRY
                            BEGIN CATCH
                                SET @mensaje = ERROR_MESSAGE();
                                SET @valor = 1;
                            END CATCH;";


                        connection.Open();
                        var i = command.ExecuteReader();

                        ComercialCliente cliente = new ComercialCliente();
                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                cliente.nombre = (string)sqlReader["nombre"];
                                cliente.rfc = (string)sqlReader["rfc"];
                                cliente.calle = (string)sqlReader["calle"];
                                cliente.estado = (string)sqlReader["estado"];
                                cliente.municipio = (string)sqlReader["municipio"];
                                cliente.pais = (string)sqlReader["pais"];
                                cliente.colonia = (string)sqlReader["colonia"];
                                cliente.cp = (string)sqlReader["cp"];
                                cliente.telefono = (string)sqlReader["telefono"];
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)codigo.Value, result = cliente };
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }

        public OperationResult buscarDatosEmpresa(int id)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory(true))
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.Parameters.Add(new SqlParameter("@id", id));
                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        command.CommandText =
                            @"BEGIN TRY
                                SELECT  p.CNOMBREEMPRESA AS nombre ,
                                        p.CRFCEMPRESA AS rfc ,
                                        D.CNOMBRECALLE AS calle ,
                                        D.CIDDIRECCION ,
                                        D.CESTADO AS estado ,
                                        D.CPAIS AS pais ,
                                        D.CMUNICIPIO AS municipio ,
                                        D.CCOLONIA AS colonia ,
                                        D.CCODIGOPOSTAL AS cp,
                                        D.CTELEFONO1 AS telefono
                                FROM    dbo.admParametros AS p
                                        JOIN dbo.admDomicilios AS D ON p.CIDEMPRESA = D.CIDCATALOGO
                                WHERE   D.CTIPOCATALOGO = 4
                                        AND p.CIDEMPRESA = @id;
                                IF @@ROWCOUNT <> 0
                                    BEGIN
                                        SET @mensaje = 'Se encontro resultado';
                                        SET @valor = 0;
                                    END;
                                ELSE
                                    BEGIN
                                        SET @mensaje = 'No se encontro los datos de la empresa';
                                        SET @valor = 3;
                                    END;
                            END TRY
                            BEGIN CATCH
                                SET @mensaje = ERROR_MESSAGE();
                                SET @valor = 1;
                            END CATCH;";


                        connection.Open();
                        var i = command.ExecuteReader();

                        ComercialEmpresa empresa = new ComercialEmpresa();
                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                empresa.nombre = (string)sqlReader["nombre"];
                                empresa.rfc = (string)sqlReader["rfc"];
                                empresa.calle = (string)sqlReader["calle"];
                                empresa.estado = (string)sqlReader["estado"];
                                empresa.municipio = (string)sqlReader["municipio"];
                                empresa.pais = (string)sqlReader["pais"];
                                empresa.colonia = (string)sqlReader["colonia"];
                                empresa.cp = (string)sqlReader["cp"];
                                empresa.telefono = (string)sqlReader["telefono"];
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)codigo.Value, result = empresa };
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }
    }    
}
