﻿namespace CoreFletera.BusinessLogic
{
    using Core.BusinessLogic;
    using Core.Models;
    using Core.Utils;
    using CoreFletera.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Runtime.InteropServices;

    public class KardexCombustibleServices
    {
        internal OperationResult ajustarKardexCombustible(int idOperacion, DateTime? fecha, decimal cantidad, TipoOperacionDiesel tipoOperacion)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    connection.Open();
                    using (SqlTransaction transaction = connection.BeginTransaction())
                    {
                        using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_ajustarKardexCombustible", false))
                        {
                            command.Transaction = transaction;
                            command.Parameters.Add(new SqlParameter("@idOperacion", idOperacion));
                            command.Parameters.Add(new SqlParameter("@tipoOperacion", (int)tipoOperacion));
                            command.Parameters.Add(new SqlParameter("@cantidad", cantidad));
                            command.Parameters.Add(new SqlParameter("@fecha", fecha));
                            command.ExecuteNonQuery();
                            if (!CrearSqlCommand.validarCorrecto(command))
                            {
                                transaction.Rollback();
                                return new OperationResult(command, null);
                            }
                            transaction.Commit();
                            result = new OperationResult(command, null);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult(exception);
            }
            return result;
        }

        public OperationResult getKardexCombustible(int idTanque, DateTime fechaInicial, DateTime fechaFin)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getKardexCombustible", false))
                    {
                        List<KardexCombustible> list = new List<KardexCombustible>();
                        command.Parameters.Add(new SqlParameter("@idTanque", idTanque));
                        command.Parameters.Add(new SqlParameter("@fechaInicial", fechaInicial));
                        command.Parameters.Add(new SqlParameter("@fechaFinal", fechaFin));
                        connection.Open();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                KardexCombustible item = new mapComunes().mapKardexCombustible(reader);
                                if (item == null)
                                {
                                    return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA.", null);
                                }
                                list.Add(item);
                            }
                        }
                        result = new OperationResult(command, list);
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult(exception);
            }
            return result;
        }

        public OperationResult getResumenKardexCombustible(DateTime fechaInicio, DateTime fechaFin, int idTanque, int idEmpresa = 0)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getResumenKardexCombustible", false))
                    {
                        command.Parameters.Add(new SqlParameter("@fechaInicio", fechaInicio));
                        command.Parameters.Add(new SqlParameter("@fechaFin", fechaFin));
                        command.Parameters.Add(new SqlParameter("idTanque", idTanque));
                        command.Parameters.Add(new SqlParameter("idEmpresa", idEmpresa));
                        List<ResumenKardexCombustible> list = new List<ResumenKardexCombustible>();
                        connection.Open();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                ResumenKardexCombustible item = new ResumenKardexCombustible
                                {
                                    idEmpresa = (int)reader["idEmpresa"],
                                    empresa = (string)reader["empresa"],
                                    dia = (DateTime)reader["dia"],
                                    cantInicial = (decimal)reader["cantInicial"],
                                    entradas = (decimal)reader["entradas"],
                                    salidas = (decimal)reader["salidas"],
                                    cantFinal = (decimal)reader["cantFinal"]
                                };
                                list.Add(item);
                            }
                        }
                        result = new OperationResult(command, list);
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult(exception);
            }
            return result;
        }

        public OperationResult savekardexCombustible(ref KardexCombustible kardexCombustible, bool isSuministro = false)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    connection.Open();
                    using (SqlTransaction transaction = connection.BeginTransaction())
                    {
                        using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_savekardexCombustible", true))
                        {
                            command.Transaction = transaction;
                            command.Parameters.Add(new SqlParameter("@idTanque", kardexCombustible.tanque.idTanqueCombustible));
                            command.Parameters.Add(new SqlParameter("@idEmpresa", (kardexCombustible.empresa == null) ? null : new int?(kardexCombustible.empresa.clave)));
                            command.Parameters.Add(new SqlParameter("@idUnidadTransporte", kardexCombustible.unidadTransporte?.clave));
                            command.Parameters.Add(new SqlParameter("@tipoOperacion", kardexCombustible.tipoOperacion));
                            command.Parameters.Add(new SqlParameter("@tipoMovimiento", kardexCombustible.tipoMovimiento));
                            command.Parameters.Add(new SqlParameter("@idOperacion", kardexCombustible.idOperacion));
                            command.Parameters.Add(new SqlParameter("@cantidad", kardexCombustible.cantidad));
                            command.Parameters.Add(new SqlParameter("@idPersonal", kardexCombustible.personal.clave));
                            command.Parameters.Add(new SqlParameter("@observaciones", (kardexCombustible.observaciones == null) ? string.Empty : kardexCombustible.observaciones));
                            DateTime? fecha = kardexCombustible.fecha;
                            command.Parameters.Add(new SqlParameter("@fecha", fecha == null ? null : (DateTime?)kardexCombustible.fecha));
                            command.Parameters.Add(new SqlParameter("@isSuministro", isSuministro));
                            command.ExecuteNonQuery();
                            int id = 0;
                            if (CrearSqlCommand.validarCorrecto(command, out id))
                            {
                                kardexCombustible.idInventarioDiesel = id;
                                transaction.Commit();
                            }
                            else
                            {
                                transaction.Rollback();
                            }
                            result = new OperationResult(command, kardexCombustible);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult(exception);
            }
            return result;
        }
        public OperationResult getExisteciaDiarias(int idTanque, List<DateTime> listaFechas, int idEmpresa, bool todasEmpresas = false)
        {
            try
            {
                string xmlFechas = GenerateXML.generarListaFechas(listaFechas);
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getExistencias"))
                    {
                        comad.Parameters.Add(new SqlParameter("@idTanque", idTanque));
                        comad.Parameters.Add(new SqlParameter("@xmlFechas", xmlFechas));
                        comad.Parameters.Add(new SqlParameter("@idEmpresaParam", idEmpresa));
                        comad.Parameters.Add(new SqlParameter("@todosEmpresas", todasEmpresas));
                        List<ExistenciaDiario> listaExistencias = new List<ExistenciaDiario>();
                        con.Open();
                        using (SqlDataReader reader = comad.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                ExistenciaDiario existencia = new ExistenciaDiario
                                {
                                    empresa = new mapComunes().mapEmpresa(reader),
                                    fecha = (DateTime)reader["fecha"],
                                    inicial = DBNull.Value.Equals(reader["inicial"]) ? 0 : (decimal)reader["inicial"],
                                    entradas = DBNull.Value.Equals(reader["entradas"]) ? 0 : (decimal)reader["entradas"],
                                    salidas = DBNull.Value.Equals(reader["salidas"]) ? 0 : (decimal)reader["salidas"],
                                    final = DBNull.Value.Equals(reader["final"]) ? 0 : (decimal)reader["final"]
                                };
                                listaExistencias.Add(existencia);
                            }
                        }
                        return new OperationResult(comad, listaExistencias);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
}
