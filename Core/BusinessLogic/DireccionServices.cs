﻿using Core.BusinessLogic;
using Core.Models;
using Core.Utils;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.BusinessLogic
{
    public class DireccionServices
    {
        public OperationResult getALLColoniasById(int id = 0)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = CrearSqlCommand.getCommand(connection, "sp_getALLColoniaById"))
                    {
                        command.Parameters.Add(new SqlParameter("@id", id));

                        connection.Open();

                        List<Colonia> listcolonia = new List<Colonia>();

                        using (SqlDataReader sqlReader = command.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                Colonia colonia = new mapComunes().mapColonia(sqlReader);
                                if (colonia == null)
                                {
                                    return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");
                                }
                                listcolonia.Add(colonia);
                            }
                        }
                        return new OperationResult(command, listcolonia);
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }
        public OperationResult getALLColoniasByNombre(string nombre)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = CrearSqlCommand.getCommand(connection, "sp_getALLColoniaByNombre"))
                    {
                        command.Parameters.Add(new SqlParameter("@nombre", nombre));

                        connection.Open();

                        List<Colonia> listcolonia = new List<Colonia>();

                        using (SqlDataReader sqlReader = command.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                Colonia colonia = new mapComunes().mapColonia(sqlReader);
                                if (colonia == null)
                                {
                                    return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");
                                }
                                listcolonia.Add(colonia);
                            }
                        }
                        return new OperationResult(command, listcolonia);
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }
    }
}

