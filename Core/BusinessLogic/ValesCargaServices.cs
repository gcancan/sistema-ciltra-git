﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using Core.Utils;
using System.Data;
using System.Data.SqlClient;
namespace CoreFletera.BusinessLogic
{
    public class ValesCargaServices
    {
        internal OperationResult agregarValesCarga(List<ValeCarga> listaVales)
        {
            try
            {
                string xml = GenerateXML.generarListaValesCarga2(listaVales);
                if (string.IsNullOrEmpty(xml))
                {
                    return new OperationResult { valor = 1, mensaje = "OCURRIO UN ERROR AL GENERAR EL XML" };
                }
                using (SqlConnection conection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand command = conection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_agregarValesCarga";
                        command.Parameters.Add(new SqlParameter("@xml", xml));
                        SqlParameter spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        SqlParameter spMensaje = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                        conection.Open();
                        command.ExecuteNonQuery();
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }
    }
}
