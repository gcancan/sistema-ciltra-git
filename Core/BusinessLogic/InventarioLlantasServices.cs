﻿namespace CoreFletera.BusinessLogic
{
    using Core.Models;
    using Core.Utils;
    using CoreFletera.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;

    public class InventarioLlantasServices
    {
        public OperationResult getInventarioLlantas(List<string> listaUnidades)
        {
            OperationResult result;
            try
            {
                string str = GenerateXML.generarListaCadena(listaUnidades);
                if (string.IsNullOrEmpty(str))
                {
                    result = new OperationResult(ResultTypes.warning, "OCURRIO UN ERROR AL ARMAR EL XML", null);
                }
                else
                {
                    using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                    {
                        using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getInventarioLlantas", false))
                        {
                            command.Parameters.Add(new SqlParameter("@xml", str));
                            connection.Open();
                            List<InventarioLlantas> list = new List<InventarioLlantas>();
                            using (SqlDataReader reader = command.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    InventarioLlantas llantas = new InventarioLlantas
                                    {
                                        idEmpresa = (int)reader["idEmpresa"],
                                        empresa = (string)reader["empresa"],
                                        idTipoUniTrans = (int)reader["idTipoUniTras"],
                                        tipoUnidad = (string)reader["tipoUnidad"],
                                        clave = (string)reader["clave"],
                                        noLlantas = (int)reader["noLlantas"],
                                        inv = (int)reader["inv"],
                                        listaDetalles = new List<DetalleInventarioLlantas>()
                                    };
                                    list.Add(llantas);
                                }
                            }
                            command.CommandText = "sp_getDetalleInventarioLlantas";
                            List<DetalleInventarioLlantas> list2 = new List<DetalleInventarioLlantas>();
                            using (SqlDataReader reader2 = command.ExecuteReader())
                            {
                                while (reader2.Read())
                                {
                                    DetalleInventarioLlantas llantas2 = new DetalleInventarioLlantas
                                    {
                                        idLlanta = (int)reader2["idLlanta"],
                                        folioLlanta = (string)reader2["folioLlanta"],
                                        clave = (string)reader2["clave"],
                                        profunidad = Convert.ToInt32(reader2["profundidad"]),
                                        posision = (int)reader2["posision"]
                                    };
                                    list2.Add(llantas2);
                                }
                            }
                            foreach (InventarioLlantas item in list)
                            {
                                item.listaDetalles = list2.FindAll(s => s.clave == item.clave);
                            }
                            result = new OperationResult(command, list);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult(exception);
            }
            return result;
        }
    }
}
