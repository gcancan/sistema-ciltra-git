﻿using Core.Models;
using CoreFletera.Models;
using Core.Utils;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.BusinessLogic;

namespace CoreFletera.BusinessLogic
{
    public class OrdenCompraServices
    {
        public OperationResult saveOrdenCompraLlantas(ref OrdenCompra orden)
        {
            try
            {
                string xml = GenerateXML.generarListaDetallesOrdenCompraLlanta(orden.listaDetalles);
                if (string.IsNullOrEmpty(xml))
                {
                    return new OperationResult { valor = 2, mensaje = "OCURRIO UN ERROR DURANTE LA CREACION DEL XML" };
                }
                using (SqlConnection connction = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand command = connction.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_saveOrdenCompraLlantas";

                        command.Parameters.Add(new SqlParameter("@idOrdenCompra", orden.idOrdenCompra));//CSERIEDOCUMENTO
                        command.Parameters.Add(new SqlParameter("@idCotizacion", orden.idCotizacion));
                        command.Parameters.Add(new SqlParameter("@CIDDOCUMENTO", orden.CIDDOCUMENTO));
                        command.Parameters.Add(new SqlParameter("@CSERIEDOCUMENTO", orden.CSERIEDOCUMENTO));
                        command.Parameters.Add(new SqlParameter("@CFOLIO", orden.CFOLIO));
                        command.Parameters.Add(new SqlParameter("@fecha", orden.fecha));
                        command.Parameters.Add(new SqlParameter("@idProveedor", orden.proveedor.idProveedor));/*PONER EL PROVEEDOR*/
                        command.Parameters.Add(new SqlParameter("@idEmpresa", orden.empresa.clave));
                        command.Parameters.Add(new SqlParameter("@estatus", orden.estatus));
                        command.Parameters.Add(new SqlParameter("@usuario", orden.usuario));
                        command.Parameters.Add(new SqlParameter("@isLlantas", orden.isLLanta));
                        command.Parameters.Add(new SqlParameter("@xml", xml));

                        SqlParameter spID = command.Parameters.Add(new SqlParameter("@id", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        SqlParameter spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        SqlParameter spMensaje = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                        connction.Open();
                        command.ExecuteNonQuery();

                        if ((int)spValor.Value == 0)
                        {
                            orden.idOrdenCompra = (int)spID.Value;
                        }

                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }

        public OperationResult saveOrdenCompraLlantas(OrdenCompra orden)
        {
            try
            {
                string xml = GenerateXML.generarListaDetallesOrdenCompraLlanta(orden.listaDetalles);
                if (string.IsNullOrEmpty(xml))
                {
                    return new OperationResult { valor = 2, mensaje = "OCURRIO UN ERROR DURANTE LA CREACION DEL XML" };
                }
                using (SqlConnection connction = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand command = connction.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_saveOrdenCompraLlantas";

                        command.Parameters.Add(new SqlParameter("@idOrdenCompra", orden.idOrdenCompra));//CSERIEDOCUMENTO
                        command.Parameters.Add(new SqlParameter("@CIDDOCUMENTO", orden.CIDDOCUMENTO));
                        command.Parameters.Add(new SqlParameter("@CSERIEDOCUMENTO", orden.CSERIEDOCUMENTO));
                        command.Parameters.Add(new SqlParameter("@CFOLIO", orden.CFOLIO));
                        command.Parameters.Add(new SqlParameter("@fecha", orden.fecha));
                        command.Parameters.Add(new SqlParameter("@idProveedor", 1));/*PONER EL PROVEEDOR*/
                        command.Parameters.Add(new SqlParameter("@idEmpresa", orden.empresa.clave));
                        command.Parameters.Add(new SqlParameter("@estatus", orden.estatus));
                        command.Parameters.Add(new SqlParameter("@usuario", orden.usuario));
                        command.Parameters.Add(new SqlParameter("@isLlantas", 1));
                        command.Parameters.Add(new SqlParameter("@xml", xml));

                        SqlParameter spID = command.Parameters.Add(new SqlParameter("@id", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        SqlParameter spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        SqlParameter spMensaje = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                        connction.Open();
                        command.ExecuteNonQuery();

                        if ((int)spValor.Value == 0)
                        {
                            orden.idOrdenCompra = (int)spID.Value;
                        }

                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, identity = (int)spID.Value };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }

        public OperationResult getOrdenCompraByFolio(Empresa empresa, string folio)
        {
            try
            {
                using (SqlConnection conection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand command = conection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getOrdenCompraByFolio";
                        command.Parameters.Add(new SqlParameter("@empresa", empresa.clave));
                        command.Parameters.Add(new SqlParameter("@folio", folio));

                        SqlParameter spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        SqlParameter spMensaje = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        OrdenCompra ordenCompra = new OrdenCompra();
                        conection.Open();
                        using (SqlDataReader sqlReader = command.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                ordenCompra = new OrdenCompra
                                {
                                    idOrdenCompra = (int)sqlReader["idOrdenCompra"],
                                    CFOLIO = (double)sqlReader["CFOLIO"],
                                    CIDDOCUMENTO = (int)sqlReader["CIDDOCUMENTO"],
                                    CSERIEDOCUMENTO = (string)sqlReader["CSERIEDOCUMENTO"],
                                    estatus = (string)sqlReader["estatus"],
                                    empresa = new mapComunes().mapEmpresa(sqlReader),
                                    fecha = (DateTime)sqlReader["fecha"],
                                    folioFactura = "",
                                    isLLanta = Convert.ToBoolean(sqlReader["isLlantas"]),
                                    proveedor = new Proveedor
                                    {
                                        idProveedor = (int)sqlReader["idProveedor"],
                                        razonSocial = (string)sqlReader["RazonSocial"],
                                        direccion = (string)sqlReader["DireccionProv"],
                                        email = (string)sqlReader["EmailProv"],
                                        RFC = (string)sqlReader["RFCProv"],
                                        tipoPersona = (string)sqlReader["TipoPersona"],
                                        idColonia = (int)sqlReader["idColonia"]
                                    },
                                    usuario = (string)sqlReader["usuario"]
                                };
                                OperationResult resp = getDetallesOrdenById((int)sqlReader["idOrdenCompra"]);
                                if (resp.typeResult == ResultTypes.success)
                                {
                                    ordenCompra.listaDetalles = (List<DetalleOrdenCompra>)resp.result;
                                }
                                else
                                {
                                    return resp;
                                }
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = ordenCompra };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }

        public OperationResult getDetallesOrdenById(int idOrdenCompra)
        {
            try
            {
                using (SqlConnection conection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand command = conection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getDetallesOrdenById";
                        command.Parameters.Add(new SqlParameter("@idOrdenCompra", idOrdenCompra));

                        SqlParameter spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        SqlParameter spMensaje = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        List<DetalleOrdenCompra> listaDetalles = new List<DetalleOrdenCompra>();
                        conection.Open();
                        using (SqlDataReader sqlReader = command.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                DetalleOrdenCompra detalle = new DetalleOrdenCompra
                                {
                                    idDetalleCompra = (int)sqlReader["idDetalleOrdenCompra"],
                                    cantidad = (decimal)sqlReader["cantidad"],
                                    pendientes = (decimal)sqlReader["pendientes"],
                                    factura = (string)sqlReader["factura"],
                                };
                                detalle.llantaProducto = new mapComunes().mapLLantaProducto(sqlReader);
                                detalle.unidadTransporte = new mapComunes().mapUnidades("", sqlReader);
                                listaDetalles.Add(detalle);
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = listaDetalles };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }

        public OperationResult getOrdenCompraByIdCotizacion(int idCotizacion)
        {
            try
            {
                using (SqlConnection conection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand command = conection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getOrdenCompraByIdCotizacion";
                        command.Parameters.Add(new SqlParameter("@idCotizacion", idCotizacion));

                        SqlParameter spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        SqlParameter spMensaje = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                        List<OrdenCompra> ListaOrdenCompra = new List<OrdenCompra>();
                        conection.Open();
                        using (SqlDataReader sqlReader = command.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                OrdenCompra ordenCompra = new OrdenCompra
                                {
                                    idOrdenCompra = (int)sqlReader["idOrdenCompra"],
                                    CFOLIO = (double)sqlReader["CFOLIO"],
                                    CIDDOCUMENTO = (int)sqlReader["CIDDOCUMENTO"],
                                    CSERIEDOCUMENTO = (string)sqlReader["CSERIEDOCUMENTO"],
                                    estatus = (string)sqlReader["estatus"],
                                    empresa = new mapComunes().mapEmpresa(sqlReader),
                                    fecha = (DateTime)sqlReader["fecha"],
                                    folioFactura = "",
                                    isLLanta = Convert.ToBoolean(sqlReader["isLlantas"]),
                                    proveedor = new Proveedor
                                    {
                                        idProveedor = (int)sqlReader["idProveedor"],
                                        razonSocial = (string)sqlReader["RazonSocial"],
                                        direccion = (string)sqlReader["DireccionProv"],
                                        email = DBNull.Value.Equals(sqlReader["EmailProv"]) ? string.Empty : (string)sqlReader["EmailProv"],
                                        RFC = (string)sqlReader["RFCProv"],
                                        tipoPersona = (string)sqlReader["TipoPersona"],
                                        idColonia = (int)sqlReader["idColonia"]
                                    },
                                    usuario = (string)sqlReader["usuario"]
                                };

                                ListaOrdenCompra.Add(ordenCompra);

                                //OperationResult resp = getDetallesOrdenById((int)sqlReader["idOrdenCompra"]);
                                //if (resp.typeResult == ResultTypes.success)
                                //{
                                //    ordenCompra.listaDetalles = (List<DetalleOrdenCompra>)resp.result;
                                //}
                                //else
                                //{
                                //    return resp;
                                //}
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = ListaOrdenCompra };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }
    }
}
