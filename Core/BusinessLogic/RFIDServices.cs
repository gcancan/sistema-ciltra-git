﻿using Core.Models;
using Core.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.BusinessLogic
{
    public class RFIDServices
    {
         public OperationResult findByRFID(string rfid, ref ResultadoBusquedaRFID tipo)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_findByRFID";
                        command.Parameters.Add(new SqlParameter("@rfid", rfid));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                        var tipoRes = command.Parameters.Add(new SqlParameter("@tipoRes", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var id = command.Parameters.Add(new SqlParameter("@id", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        connection.Open();
                        var i = command.ExecuteNonQuery();

                        if (((ResultTypes)spValor.Value) == ResultTypes.success)
                        {
                            tipo = (ResultadoBusquedaRFID)tipoRes.Value;
                            if (tipo == ResultadoBusquedaRFID.UNIDAD_TRANSPORTE)
                            {
                                var resp = new UnidadTransporteServices().getUnidadesALLorById(0, (string)id.Value);
                                return new OperationResult { valor = resp.valor, mensaje = resp.mensaje, result = ((List<UnidadTransporte>)resp.result)[0]};
                            }
                            if (tipo == ResultadoBusquedaRFID.OPERADOR)
                            {
                                var resp = new OperadoresServices().getOperadoresALLorById(Convert.ToInt32((string)id.Value), 0);
                                return new OperationResult { valor = resp.valor, mensaje = resp.mensaje, result = ((List<Personal>)resp.result)[0] };
                            }
                        }                       
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)codigo.Value, result = null };
                    }
                }
            }
            catch (Exception x )
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }

        public OperationResult asignarRFIDUnidades(ref UnidadTransporte unidadTrans, string rfid)
        {
            try
            {
                using (SqlConnection conection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comand = conection.CreateCommand())
                    {
                        comand.CommandType = CommandType.StoredProcedure;
                        comand.CommandText = "sp_asignarRFIDUnidades";

                        comand.Parameters.Add(new SqlParameter("@clave", unidadTrans.clave));
                        comand.Parameters.Add(new SqlParameter("@rfid", rfid));

                        var spValor = comand.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var spMensaje = comand.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                        conection.Open();
                        var i = comand.ExecuteNonQuery();

                        if ((int)spValor.Value == 0)
                        {
                            unidadTrans.rfid = rfid;
                            return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = unidadTrans };
                        }
                        else
                        {
                            return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = null };
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message, result = null };
            }
        }

        public OperationResult asignarRFIDOperador(ref Personal personal, string rfid)
        {
            try
            {
                using (SqlConnection conection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comand = conection.CreateCommand())
                    {
                        comand.CommandType = CommandType.StoredProcedure;
                        comand.CommandText = "sp_asignarRFIDOperador";

                        comand.Parameters.Add(new SqlParameter("@clave", personal.clave));
                        comand.Parameters.Add(new SqlParameter("@rfid", rfid));

                        var spValor = comand.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var spMensaje = comand.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                        conection.Open();
                        var i = comand.ExecuteNonQuery();

                        if ((int)spValor.Value == 0)
                        {
                            personal.rfid = rfid;
                            return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = personal };
                        }
                        else
                        {
                            return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = null };
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message, result = null };
            }
        }
    }
}
