﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using Core.BusinessLogic;
using Core.Utils;
using System.Data;
using System.Data.SqlClient;
namespace CoreFletera.BusinessLogic
{
    public class CotizacionesServices
    {
        public OperationResult saveCotizacion(ref CotizacionSolicitudOrdCompra cotizacion)
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    connection.Open();
                    using (SqlTransaction transaction = connection.BeginTransaction("saveCotizacion"))
                    {
                        using (SqlCommand command = connection.CreateCommand())
                        {
                            command.Transaction = transaction;
                            command.CommandType = CommandType.StoredProcedure;
                            command.CommandText = "saveCotizacion";

                            command.Parameters.Add(new SqlParameter("@idCotizacion", cotizacion.idCotizacionSolicitudOrdCompra));
                            command.Parameters.Add(new SqlParameter("@idEmpresa", cotizacion.empresa.clave));
                            command.Parameters.Add(new SqlParameter("@idSolicitud", cotizacion.solicitud == null ? null :(int?) cotizacion.solicitud.idSolicitudOrdenCompra));
                            command.Parameters.Add(new SqlParameter("@fechaCotizacion", cotizacion.fecha));
                            command.Parameters.Add(new SqlParameter("@usuarioCotiza", cotizacion.usuarioCotiza));
                            command.Parameters.Add(new SqlParameter("@fechaAutoriza", cotizacion.fechaAutorizacion));
                            command.Parameters.Add(new SqlParameter("@usuarioAutoriza", cotizacion.usuarioAprueba));
                            command.Parameters.Add(new SqlParameter("@estatus", cotizacion.estatus));
                            command.Parameters.Add(new SqlParameter("@isLlantas", cotizacion.isLlantas));

                            SqlParameter spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                            SqlParameter spId = command.Parameters.Add(new SqlParameter("@id", SqlDbType.Int) { Direction = ParameterDirection.Output });
                            SqlParameter spMensaje = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                            command.ExecuteNonQuery();
                            if ((int)spValor.Value == 0)
                            {
                                
                                cotizacion.idCotizacionSolicitudOrdCompra = (int)spId.Value;
                                foreach (var item in cotizacion.listDetalle)
                                {
                                    command.CommandText = "saveDetalleCotizacion";
                                    command.Parameters.Clear();

                                    command.Parameters.Add(new SqlParameter("@idCotizacion", cotizacion.idCotizacionSolicitudOrdCompra));
                                    command.Parameters.Add(new SqlParameter("@idDetalleCotizacion", item.idDetalleCotizacionSolicitudOrdCompra));
                                    command.Parameters.Add(new SqlParameter("@idUnidadTrans", item.unidadTransporte == null ? null : item.unidadTransporte.clave));
                                    command.Parameters.Add(new SqlParameter("@idProveedor", item.proveedor.idProveedor));
                                    command.Parameters.Add(new SqlParameter("@medidaLlanta", item.medidaLlanta));
                                    command.Parameters.Add(new SqlParameter("@idProductoLlanta", item.llantaProducto == null ? null : (int?)item.llantaProducto.idLLantasProducto));
                                    command.Parameters.Add(new SqlParameter("@idProducto", item.producto == null ? null : (int?)item.producto.idProducto));
                                    command.Parameters.Add(new SqlParameter("@cantidad", item.cantidad));
                                    command.Parameters.Add(new SqlParameter("@costoUnitario", item.costo));
                                    command.Parameters.Add(new SqlParameter("@importe", item.importe));

                                    spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                                    spId = command.Parameters.Add(new SqlParameter("@id", SqlDbType.Int) { Direction = ParameterDirection.Output });
                                    spMensaje = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                                    command.ExecuteNonQuery();
                                    if ((int)spValor.Value == 0)
                                    {
                                        command.CommandText = "saveDetDetalleCotizacion";
                                        item.idDetalleCotizacionSolicitudOrdCompra = (int)spId.Value;

                                        foreach (var itemDet in item.listDetalles)
                                        {
                                            command.Parameters.Clear();

                                            command.Parameters.Add(new SqlParameter("@idDetalle", itemDet.idDetalle));
                                            command.Parameters.Add(new SqlParameter("@idDetalleCotizacion", item.idDetalleCotizacionSolicitudOrdCompra));
                                            command.Parameters.Add(new SqlParameter("@idProveedor", itemDet.proveedor.idProveedor));
                                            command.Parameters.Add(new SqlParameter("@costo", itemDet.costo));
                                            command.Parameters.Add(new SqlParameter("@diasEntrega", itemDet.diasEntrega));
                                            command.Parameters.Add(new SqlParameter("@diasCredito", itemDet.diasCredito));
                                            command.Parameters.Add(new SqlParameter("@importe", itemDet.importe));
                                            command.Parameters.Add(new SqlParameter("@elegido", itemDet.elegido));

                                            spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                                            spId = command.Parameters.Add(new SqlParameter("@id", SqlDbType.Int) { Direction = ParameterDirection.Output });
                                            spMensaje = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                                            command.ExecuteNonQuery();
                                            if ((int)spValor.Value == 0)
                                            {
                                                itemDet.idDetalle = (int)spId.Value;
                                            }
                                            else
                                            {
                                                transaction.Rollback("saveCotizacion");
                                                return new OperationResult(command);
                                            }
                                        }

                                    }
                                    else
                                    {
                                        transaction.Rollback("saveCotizacion");
                                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = null };
                                    }
                                }
                            }
                            else
                            {
                                transaction.Rollback("saveCotizacion");
                                return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = null };
                            }
                            transaction.Commit();
                            return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = cotizacion };
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }

        internal OperationResult getPreOrdCompraByFiltros(DateTime fechaInicio, DateTime fechaFin, int idEmpresa, string tipoOrden = "%")
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getPreOrdCompraByFiltros"))
                    {
                        command.Parameters.Add(new SqlParameter("@fechaInicio", fechaInicio));
                        command.Parameters.Add(new SqlParameter("@fechaFin", fechaFin));
                        command.Parameters.Add(new SqlParameter("@tipoOrden", tipoOrden));
                        command.Parameters.Add(new SqlParameter("@idEmpresa", idEmpresa));

                        List<PreOrdenCompra> listaPreOrd = new List<PreOrdenCompra>();
                        connection.Open();
                        using (SqlDataReader sqlReader = command.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                PreOrdenCompra preOrd = new mapComunes().mapPreOrdenCompra(sqlReader);
                                if (preOrd == null)
                                {
                                    return new OperationResult { valor = 1, mensaje = "OCURRIO UN ERROR AL LEER LA INFORMACIÓN DE LA BUSQUEDA" };
                                }
                                listaPreOrd.Add(preOrd);
                            }
                        }
                        return new OperationResult(command, listaPreOrd);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        public OperationResult getCotizacionAllOrById(int idCotizacion = 0, bool detalle = true)
        {
            DateTime? fecha = null;
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand command = con.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getCotizacionAllOrById";

                        command.Parameters.Add(new SqlParameter("@idCotizacion", idCotizacion));

                        SqlParameter spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        SqlParameter spMensaje = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                        con.Open();
                        List<CotizacionSolicitudOrdCompra> lista = new List<CotizacionSolicitudOrdCompra>();
                        using (SqlDataReader sqlReader = command.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                CotizacionSolicitudOrdCompra cotizacion = new CotizacionSolicitudOrdCompra
                                {
                                    idCotizacionSolicitudOrdCompra = (int)sqlReader["idCotizacionSolicitudOrdCompra"],
                                    fecha = (DateTime)sqlReader["fecha"],
                                    usuarioCotiza = (string)sqlReader["usuarioCotiza"],
                                    fechaAutorizacion = DBNull.Value.Equals(sqlReader["fechaAutorizacion"]) ? fecha : (DateTime)sqlReader["fechaAutorizacion"],
                                    usuarioAprueba = (string)sqlReader["usuarioAprueba"],
                                    estatus = (string)sqlReader["estatus"],
                                    isLlantas = Convert.ToBoolean(sqlReader["isLlantas"])
                                };
                                cotizacion.empresa = new mapComunes().mapEmpresa(sqlReader);
                                if (cotizacion.empresa == null)
                                {
                                    return new OperationResult { valor = 1, mensaje = "ERROR AL OBTENER LOS VALORES DE LA BUSQUEDE" };
                                }

                                OperationResult resp = new OperationResult();
                                if (!DBNull.Value.Equals(sqlReader["idSolicitudOrdenCompra"]))
                                {
                                    resp = new SolicitudOrdenCompraServices().getSolicitudOrdComAllOrById((int)sqlReader["idSolicitudOrdenCompra"]);
                                    if (resp.typeResult == ResultTypes.success)
                                    {
                                        cotizacion.solicitud = (SolicitudOrdenCompra)resp.result;
                                    }
                                    else
                                    {
                                        return resp;
                                    }
                                }
                                

                                if (detalle)
                                {
                                    resp = getDetallesCotizacionById(cotizacion.idCotizacionSolicitudOrdCompra, cotizacion.isLlantas);
                                    if (resp.typeResult == ResultTypes.success)
                                    {
                                        cotizacion.listDetalle = (List<DetalleCotizacionSolicitudOrdCompra>)resp.result;
                                    }
                                    else
                                    {
                                        return resp;
                                    }
                                }
                                else
                                {
                                    cotizacion.listDetalle = new List<DetalleCotizacionSolicitudOrdCompra>();
                                }
                                
                                lista.Add(cotizacion);
                            }
                        }
                        if (idCotizacion == 0)
                        {
                            return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = lista };
                        }
                        else
                        {
                            return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = (int)spValor.Value == 0 ? lista[0] : new CotizacionSolicitudOrdCompra() };
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }

        public OperationResult getCotizacionAllOrByIdSolicitud(int idSolicitud)
        {
            DateTime? fecha = null;
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(con, "sp_getCotizacionAllOrByIdSolicitud"))
                    {
                        command.Parameters.Add(new SqlParameter("@idSolicitud", idSolicitud));
                        con.Open();
                        CotizacionSolicitudOrdCompra cotizacion = new CotizacionSolicitudOrdCompra();
                        using (SqlDataReader sqlReader = command.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                cotizacion = new CotizacionSolicitudOrdCompra
                                {
                                    idCotizacionSolicitudOrdCompra = (int)sqlReader["idCotizacionSolicitudOrdCompra"],
                                    fecha = (DateTime)sqlReader["fecha"],
                                    usuarioCotiza = (string)sqlReader["usuarioCotiza"],
                                    fechaAutorizacion = DBNull.Value.Equals(sqlReader["fechaAutorizacion"]) ? fecha : (DateTime)sqlReader["fechaAutorizacion"],
                                    usuarioAprueba = (string)sqlReader["usuarioAprueba"],
                                    estatus = (string)sqlReader["estatus"],
                                    isLlantas = Convert.ToBoolean(sqlReader["isLlantas"])
                                };
                                cotizacion.empresa = new mapComunes().mapEmpresa(sqlReader);
                                if (cotizacion.empresa == null)
                                {
                                    return new OperationResult { valor = 1, mensaje = "ERROR AL OBTENER LOS VALORES DE LA BUSQUEDA" };
                                }

                                OperationResult resp = new OperationResult();
                                resp = new SolicitudOrdenCompraServices().getSolicitudOrdComAllOrById((int)sqlReader["idSolicitudOrdenCompra"]);
                                if (resp.typeResult == ResultTypes.success)
                                {
                                    cotizacion.solicitud = (SolicitudOrdenCompra)resp.result;
                                }
                                else
                                {
                                    return resp;
                                }

                                resp = getDetallesCotizacionById(cotizacion.idCotizacionSolicitudOrdCompra, cotizacion.isLlantas);
                                if (resp.typeResult == ResultTypes.success)
                                {
                                    cotizacion.listDetalle = (List<DetalleCotizacionSolicitudOrdCompra>)resp.result;
                                }
                                else
                                {
                                    return resp;
                                }
                            }
                        }
                        return new OperationResult(command, cotizacion);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        private OperationResult getDetallesCotizacionById(int idCotizacion, bool isLlantas = true)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand command = con.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getDetalleCotizacionAllOrById";

                        command.Parameters.Add(new SqlParameter("@idCotizacion", idCotizacion));

                        SqlParameter spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        SqlParameter spMensaje = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                        List<DetalleCotizacionSolicitudOrdCompra> lista = new List<DetalleCotizacionSolicitudOrdCompra>();
                        con.Open();
                        using (SqlDataReader sqlReader = command.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                DetalleCotizacionSolicitudOrdCompra detalle = new DetalleCotizacionSolicitudOrdCompra
                                {
                                    idDetalleCotizacionSolicitudOrdCompra = (int)sqlReader["idDetalleCotizacionSolicitudOrdCompra"],
                                    cantidad = Convert.ToInt32(sqlReader["cantidad"]),
                                    costo = (decimal)sqlReader["costoUnitario"],
                                    importe = (decimal)sqlReader["importe"],
                                    medidaLlanta = DBNull.Value.Equals(sqlReader["medidaLlanta"]) ? string.Empty : (string)sqlReader["medidaLlanta"]
                                };

                                detalle.proveedor = new mapComunes().mapProveedor(sqlReader);
                                if (detalle.proveedor == null)
                                {
                                    return new OperationResult { valor = 1, mensaje = "ERROR AL LEER LOS VALORES DE LA BUSQUEDA(PROVEEDOR)" };
                                }
                                if (isLlantas)
                                {
                                    detalle.llantaProducto = new mapComunes().mapLLantaProducto(sqlReader);
                                    if (detalle.llantaProducto == null)
                                    {
                                        return new OperationResult { valor = 1, mensaje = "ERROR AL LEER LOS VALORES DE LA BUSQUEDA(TIPO DE LLANTA)" };
                                    }
                                    detalle.unidadTransporte = new mapComunes().mapUnidades("", sqlReader);
                                    if (detalle.unidadTransporte == null)
                                    {
                                        return new OperationResult { valor = 1, mensaje = "ERROR AL LEER LOS VALORES DE LA BUSQUEDA(UNIDAD DE TRANSPORTE)" };
                                    }
                                }
                                else
                                {
                                    detalle.producto = new mapComunes().mapProductosCOM(sqlReader);
                                }
                                detalle.listDetalles = new List<DetalleCotizaciones>();
                                OperationResult respDet = getDetalleCotizacion(detalle.idDetalleCotizacionSolicitudOrdCompra);
                                if (respDet.typeResult == ResultTypes.success)
                                {
                                    detalle.listDetalles = respDet.result as List<DetalleCotizaciones>;
                                }
                                else
                                {
                                    return respDet;
                                }

                                lista.Add(detalle);
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = lista };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }

        private OperationResult getDetalleCotizacion(int idDetalleCotizacion)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(con, "sp_getDetDetalleCotizacion"))
                    {
                        command.Parameters.Add(new SqlParameter("@idDetalleCotizacion", idDetalleCotizacion));

                        con.Open();
                        List<DetalleCotizaciones> lista = new List<DetalleCotizaciones>();
                        using (SqlDataReader sqlReader = command.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                DetalleCotizaciones detalle = new mapComunes().mapDetallesCotizaciones(sqlReader);
                                if (detalle != null)
                                {
                                    lista.Add(detalle);
                                }
                                else
                                {
                                    return new OperationResult { valor = 1, mensaje = "Error al leer los resultados de la busqueda" };
                                }
                            }
                        }

                        return new OperationResult(command, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        public OperationResult getPreOrdCompraByEmpresa(int idEmpresa)
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getPreOrdCompraByEmpresa"))
                    {
                        command.Parameters.Add(new SqlParameter("@idEmpresa", idEmpresa));

                        List<PreOrdenCompra> listaPreOrd = new List<PreOrdenCompra>();
                        connection.Open();
                        using (SqlDataReader sqlReader = command.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                PreOrdenCompra preOrd = new mapComunes().mapPreOrdenCompra(sqlReader);
                                if (preOrd == null)
                                {
                                    return new OperationResult { valor = 1, mensaje = "OCURRIO UN ERROR AL LEER LA INFORMACIÓN DE LA BUSQUEDA" };
                                }
                                listaPreOrd.Add(preOrd);
                            }
                        }
                        return new OperationResult(command, listaPreOrd);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        public OperationResult getDetPreOrdCompraBy(int idPreOrd)
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getDetPreOrdCompraBy"))
                    {
                        command.Parameters.Add(new SqlParameter("@idPreOrd", idPreOrd));

                        List<DetallePreOrdenCompra> listaDetalle = new List<DetallePreOrdenCompra>();
                        connection.Open();
                        using (SqlDataReader sqlReader = command.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                DetallePreOrdenCompra detalle = new mapComunes().mapDetalleOrdenCompra(sqlReader);
                                if (detalle == null)
                                {
                                    return new OperationResult { valor = 1, mensaje = "OCURRIO UN ERROR AL LEER LA INFORMACIÓN DE LA BUSQUEDA" };
                                }
                                listaDetalle.Add(detalle);
                            }
                        }
                        return new OperationResult(command, listaDetalle);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
}
