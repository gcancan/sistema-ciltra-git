﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoreFletera.Models;
using Core.BusinessLogic;
using Core.Models;
using Core.Utils;
using System.Data;
using System.Data.SqlClient;
namespace CoreFletera.BusinessLogic
{
    public class TanqueCombustibleServices
    {
        public OperationResult getTanqueByZonaOperativa(ZonaOperativa zonaOperativa)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand commad = CrearSqlCommand.getCommand(con, "sp_getTanqueByZonaOperativa"))
                    {
                        commad.Parameters.Add(new SqlParameter("@idZonaOperativa", zonaOperativa.idZonaOperativa));
                        TanqueCombustible tanqueCombustible = new TanqueCombustible();
                        con.Open();
                        using (SqlDataReader sqlReader = commad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                tanqueCombustible = new mapComunes().mapTanqueCombustible(sqlReader);
                                if (tanqueCombustible == null)
                                {
                                    return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER EL RESULTADO DE LA BUSQUEDA");
                                }
                            }

                        }
                        if (tanqueCombustible.idTanqueCombustible != 0)
                        {
                            tanqueCombustible.listaBombas = new List<BombasCombustible>();
                            var resp = getBombasByIdTanque(new List<int> { tanqueCombustible.idTanqueCombustible });
                            if (resp.typeResult == ResultTypes.success)
                            {
                                List<BombasCombustible> list = resp.result as List<BombasCombustible>;
                                tanqueCombustible.listaBombas = list.FindAll(s => s.idTanque == tanqueCombustible.idTanqueCombustible);
                            }
                            else if (resp.typeResult != ResultTypes.recordNotFound)
                            {
                                return resp;
                            }
                        }
                        

                        return new OperationResult(commad, tanqueCombustible);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        public OperationResult getBombasByIdTanque(List<int> listaid)
        {
            try
            {
                string xml = GenerateXML.generarListaIds(listaid);
                if (string.IsNullOrEmpty(xml))
                {
                    return new OperationResult(ResultTypes.warning, "Ocurrio un error al generar el XML");
                }
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand commad = CrearSqlCommand.getCommand(con, "sp_getBombasByidsTanques"))
                    {
                        commad.Parameters.Add(new SqlParameter("@xml", xml));
                        List<BombasCombustible> listaBombas = new List<BombasCombustible>();
                        con.Open();
                        using (SqlDataReader sqlReader = commad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                BombasCombustible bomba = new mapComunes().mapBomba(sqlReader);
                                if (bomba == null)
                                {
                                    return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER EL RESULTADO DE LA BUSQUEDA");
                                }
                                else
                                {
                                    listaBombas.Add(bomba);
                                }
                            }

                        }
                        return new OperationResult(commad, listaBombas);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
}
