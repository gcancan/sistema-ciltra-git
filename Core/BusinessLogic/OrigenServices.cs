﻿namespace Core.BusinessLogic
{
    using Core.Models;
    using Core.Utils;
    using CoreFletera.BusinessLogic;
    using CoreFletera.Models;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Runtime.InteropServices;

    public class OrigenServices
    {
        public OperationResult getDestinosAtlante(int idEmpresa)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getDestinoByIdEmpresa";
                        command.Parameters.Add(new SqlParameter("@idEmpresa", idEmpresa));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        List<Zona> list = new List<Zona>();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Zona item = new Zona
                                {
                                    clave = (int)reader["Clave"],
                                    descripcion = (string)reader["Descripcion"],
                                    km = (decimal)reader["km"],
                                    estado = new mapComunes().mapEstados(reader),
                                    activo = (bool)reader["activo"],
                                    correo = DBNull.Value.Equals(reader["correo"]) ? string.Empty : ((string)reader["correo"]),
                                    baseDestino = DBNull.Value.Equals(reader["destino_idBase"]) ? null : new Base
                                    {
                                        idBase = (int)reader["destino_idBase"],
                                        nombreBase = (string)reader["destino_nombreBase"],
                                        activo = Convert.ToBoolean(reader["destino_baseActivo"])
                                    }
                                };
                                list.Add(item);
                            }
                        }
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = list
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            return result;
        }

        internal OperationResult getOrigenesAll()
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "getOrigenesAll_v2", false))
                    {
                        connection.Open();
                        List<Origen> list = new List<Origen>();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Origen item = new mapComunes().mapOrigen(reader);
                                if (item != null)
                                {
                                    list.Add(item);
                                }
                            }
                        }
                        result = new OperationResult(command, list);
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            return result;
        }

        internal OperationResult getOrigenesAll(int idCliente = 0, int idOrigen = 0)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "getOrigenesAll";
                        command.Parameters.Add(new SqlParameter("@idCliente", idCliente));
                        command.Parameters.Add(new SqlParameter("@idOrigen", idOrigen));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        List<Origen> list = new List<Origen>();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Origen item = new mapComunes().mapOrigen(reader);
                                list.Add(item);
                            }
                        }
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = list
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            return result;
        }

        internal OperationResult saveOrigenCliente(ref Origen origen)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_saveOrigenCliente";
                        command.Parameters.Add(new SqlParameter("@idCliente", origen.idCliente));
                        command.Parameters.Add(new SqlParameter("@nombre", origen.nombreOrigen));
                        command.Parameters.Add(new SqlParameter("@estado", origen.estado));
                        command.Parameters.Add(new SqlParameter("@clasificacion", origen.clasificacion));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter4 = new SqlParameter("@identity", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter4);
                        SqlParameter parameter5 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter3 = command.Parameters.Add(parameter5);
                        connection.Open();
                        command.ExecuteNonQuery();
                        origen.idOrigen = (int)parameter2.Value;
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter3.Value,
                            result = origen
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            return result;
        }

        internal OperationResult saveOrigenCliente_v2(ref Origen origen)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    connection.Open();
                    using (SqlTransaction transaction = connection.BeginTransaction())
                    {
                        using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_saveOrigenCliente_v2", true))
                        {
                            command.Transaction = transaction;
                            command.Parameters.Add(new SqlParameter("@idOrigen", origen.idOrigen));
                            command.Parameters.Add(new SqlParameter("@nombre", origen.nombreOrigen));
                            command.Parameters.Add(new SqlParameter("@estado", origen.estado));
                            //command.Parameters.Add(new SqlParameter("@clasificacion", origen.clasificacion));
                            command.Parameters.Add(new SqlParameter("@km", origen.km));
                            command.Parameters.Add(new SqlParameter("@activo", origen.activo));
                            command.Parameters.Add(new SqlParameter("@idBase", origen.baseOrigen.idBase));
                            command.ExecuteNonQuery();
                            if (((int)command.Parameters["@valor"].Value) == 0)
                            {
                                origen.idOrigen = (int)command.Parameters["@id"].Value;
                                transaction.Commit();
                            }
                            else
                            {
                                transaction.Rollback();
                            }
                            result = new OperationResult(command, origen);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            return result;
        }
    }
}
