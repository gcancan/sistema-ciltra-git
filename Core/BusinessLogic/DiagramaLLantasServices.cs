﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using Core.Utils;
using System.Data;
using System.Data.SqlClient;
namespace CoreFletera.BusinessLogic
{
    public class DiagramaLLantasServices
    {
        internal OperationResult getDiagramaByTipoUnidadTrans(TipoUnidad tipoUnidad)
        {
            try
            {
                using (SqlConnection conection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comand = conection.CreateCommand())
                    {
                        comand.CommandType = CommandType.StoredProcedure;
                        comand.CommandText = "sp_getDiagramaLLantaByTipoUnidad";
                        comand.Parameters.Add(new SqlParameter("@idTipoUnidad", tipoUnidad.clave));
                        SqlParameter spValor = comand.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        SqlParameter spMensaje = comand.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        conection.Open();
                        DiagramaLLantas diagramaLLanta = new DiagramaLLantas();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                diagramaLLanta = new DiagramaLLantas()
                                {
                                    idClasificacionLLanta = (int)sqlReader["idClasificacionLLanta"],
                                    nombre = (string)sqlReader["nombre"],
                                    numEjes = (int)sqlReader["numEjes"],
                                    numLLantas = (int)sqlReader["numLLantas"]
                                };
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = diagramaLLanta };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }
        public OperationResult getDetalleDiagrama(int id)
        {
            try
            {
                using (SqlConnection conection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comand = conection.CreateCommand())
                    {
                        comand.CommandType = CommandType.StoredProcedure;
                        comand.CommandText = "sp_getDetalleDiagrama";
                        comand.Parameters.Add(new SqlParameter("@id", id));
                        SqlParameter spValor = comand.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        SqlParameter spMensaje = comand.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        conection.Open();
                        List< DetallesDiagrama> listaDetalles = new List<DetallesDiagrama>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                var detalle = new DetallesDiagrama()
                                {
                                    eje = (int)sqlReader["eje"],
                                    llantas = (int)sqlReader["llantas"]
                                };
                                listaDetalles.Add(detalle);
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = listaDetalles };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }
    }
}
