﻿namespace CoreFletera.BusinessLogic
{
    using Core.Models;
    using Core.Utils;
    using CoreFletera.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;

    public class ReporteCartaPorteServices
    {
        public OperationResult getReporteCartaPorteV2(int idEmpresa, DateTime fechaInicio, DateTime fechaFin, List<string> listaTractores, List<string> listaOperadores,
            List<string> listaClientes, List<string> listaRutas, List<string> listaModalidades, List<string> listaEstatus, bool todosOperadores = true, bool todosTractores = true)
        {
            int CABGUIA = 0;
            try
            {
                string strListaTractores = GenerateXML.generarListaCadena(listaTractores);
                string strListaOperadores = GenerateXML.generarListaCadena(listaOperadores);
                string strListaClientes = GenerateXML.generarListaCadena(listaClientes);
                string strListaRutas = GenerateXML.generarListaCadena(listaRutas);
                string strListaModalidades = GenerateXML.generarListaCadena(listaModalidades);
                string strListaEstatus = GenerateXML.generarListaCadena(listaEstatus);
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getCartaPortesV2_v2", false))
                    {
                        command.Parameters.Add(new SqlParameter("@fechaInicial", fechaInicio));
                        command.Parameters.Add(new SqlParameter("@fechaFinal", fechaFin));
                        command.Parameters.Add(new SqlParameter("@xmlTractores", strListaTractores));
                        command.Parameters.Add(new SqlParameter("@xmlOperadores", strListaOperadores));
                        command.Parameters.Add(new SqlParameter("@xmlClientes", strListaClientes));
                        command.Parameters.Add(new SqlParameter("@xmlRutas", strListaRutas));
                        command.Parameters.Add(new SqlParameter("@xmlModalidades", strListaModalidades));
                        command.Parameters.Add(new SqlParameter("@xmlEstatus", strListaEstatus));
                        command.Parameters.Add(new SqlParameter("@todosOperadores", todosOperadores));
                        command.Parameters.Add(new SqlParameter("@todosTractores", todosTractores));
                        command.Parameters.Add(new SqlParameter("@idEmpresa", idEmpresa));
                        //connection.ConnectionTimeout = 10000;

                        var s = connection.ConnectionTimeout;

                        command.CommandTimeout = 3800;

                        connection.Open();
                        List<ReporteCartaPorte> list = new List<ReporteCartaPorte>();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CABGUIA = (int)reader["folio"];
                                if (CABGUIA == 99546)
                                {

                                }
                                ReporteCartaPorte item = new ReporteCartaPorte
                                {
                                    folio = (int)reader["folio"],
                                    cartaPorte = (int)reader["cartaPorte"],
                                    idEmpresa = (int)reader["idEmpresa"],
                                    fechaPrograma = DBNull.Value.Equals(reader["fechaPrograma"]) ? null : (DateTime?)reader["fechaPrograma"],
                                    empresa = (string)reader["empresa"],
                                    idCliente = (int)reader["IdCliente"],
                                    cliente = (string)reader["cliente"],
                                    idOperador = (int)reader["idOperador"],
                                    operador = (string)reader["operador"],
                                    operadorFin = (string)reader["operadorFin"],
                                    operadorAyuda = (string)reader["operadorAyuda"],
                                    operadorCap = (string)reader["operadorCap"],
                                    estatus = (string)reader["estatus"],
                                    modalidad = (string)reader["modalidad"],
                                    tc = (string)reader["tc"],
                                    rem1 = (string)reader["rem1"],
                                    dolly = (string)reader["dolly"],
                                    rem2 = (string)reader["rem2"],
                                    fechaInicio = (DateTime)reader["fechaInicio"],
                                    fechaFin = DBNull.Value.Equals(reader["fechaFin"]) ? null : ((DateTime?)reader["fechaFin"]),
                                    km = (decimal)reader["km"],
                                    idOrigen = DBNull.Value.Equals(reader["idOrigen"]) ? null : ((int?)reader["idOrigen"]),
                                    origen = DBNull.Value.Equals(reader["origen"]) ? string.Empty : ((string)reader["origen"]),
                                    idDestino = (int)reader["idDestino"],
                                    destino = (string)reader["destino"],
                                    orden = (string)reader["orden"],
                                    producto = (string)reader["producto"],
                                    capacidadIns = (decimal)reader["capacidadIns"],
                                    toneladas = (decimal)reader["toneladas"],
                                    toneladasDespachadas = (decimal)reader["toneladasDespachadas"],
                                    precioVar = (decimal)reader["precioVar"],
                                    subTotalVar = (decimal)reader["subTotalVar"],
                                    precioFijo = (decimal)reader["precioFijo"],
                                    subTotal = (decimal)reader["subTotal"],
                                    cobroXKm = (bool)reader["cobroXkm"],
                                    viaje = (bool)reader["viaje"],
                                    impIva = (decimal)reader["impIva"],
                                    impRetencion = (decimal)reader["ImpRetencion"],
                                    vFalso = (bool)reader["vFalso"],
                                    pagoOperador = (decimal)reader["pagoOperador"],
                                    pagoOperadorCap = (decimal)reader["pagoOperadorCap"],
                                    vale = (int)reader["vale"],
                                    usuario = (string)reader["usuario"],
                                    usuarioFin = (string)reader["usuarioFin"],
                                    usuarioAjusta = (string)reader["usuarioAjusta"],
                                    fechaSalidaPlantaCSI = DBNull.Value.Equals(reader["fechaSalidaPlantaCSI"]) ? null : (DateTime?)reader["fechaSalidaPlantaCSI"],
                                    fechaEntradaPlantaCSI = DBNull.Value.Equals(reader["fechaEntradaPlantaCSI"]) ? null : (DateTime?)reader["fechaEntradaPlantaCSI"],
                                    tiempoIda = (string)reader["tiempoIda"],
                                    tiempoGranja = (string)reader["tiempoGranja"],
                                    tiempoRetorno = (string)reader["tiempoRetorno"],
                                    totalDespachados = (decimal)reader["totalDespachados"],
                                    zona = (string)reader["zona"],
                                    isReEnviar = Convert.ToBoolean(reader["isReEnvio"]),
                                    imputable = (string)reader["imputable"],
                                    fechaInicioCarga = DBNull.Value.Equals(reader["fechaInicioCarga"]) ? null : (DateTime?)reader["fechaInicioCarga"],
                                    noFactura = (string)reader["folioFactura"],
                                    usuarioFactura = (string)reader["usuarioFacturo"],
                                    kmECM = (decimal)reader["kmECM"],
                                    _ltsVale = (decimal)reader["ltsVale"],
                                    folioImpreso = (string)reader["folioImpreso"],
                                    ticket = (string)reader["ticket"],
                                    ordenServ = (int)reader["ordenServ"],
                                    precioLts1 = (decimal)reader["precioLts1"],
                                    precioLts2 = (decimal)reader["precioLts2"],
                                    importeLts = (decimal)reader["importeLts"],
                                    facturaDiesel = (string)reader["facturaDiesel"],
                                    proveedorCombustible = (string)reader["proveedorCombustible"],
                                    kmFisico = (decimal)reader["kmFisico"],
                                    kmOrigen = (decimal)reader["kmOrigen"],
                                    kmOrigenDestino = (decimal)reader["kmOrigenDestino"],
                                    kmDestino = (decimal)reader["kmDestino"]
                                };
                                list.Add(item);
                            }
                        }
                        return new OperationResult(command, list);
                    }
                }
            }
            catch (Exception exception)
            {
                return new OperationResult(exception);
            }
        }
        public OperationResult getReporteCartaPorteV3(int idEmpresa, DateTime fechaInicio, DateTime fechaFin, List<string> listaTractores, List<string> listaOperadores,
            List<string> listaClientes, List<string> listaRutas, List<string> listaModalidades, List<string> listaEstatus, bool todosOperadores = true, bool todosTractores = true)
        {
            int CABGUIA = 0;
            try
            {
                string strListaTractores = GenerateXML.generarListaCadena(listaTractores);
                string strListaOperadores = GenerateXML.generarListaCadena(listaOperadores);
                string strListaClientes = GenerateXML.generarListaCadena(listaClientes);
                string strListaRutas = GenerateXML.generarListaCadena(listaRutas);
                string strListaModalidades = GenerateXML.generarListaCadena(listaModalidades);
                string strListaEstatus = GenerateXML.generarListaCadena(listaEstatus);
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getCartaPortesNewV3", false))
                    {
                        command.Parameters.Add(new SqlParameter("@fechaInicial", fechaInicio));
                        command.Parameters.Add(new SqlParameter("@fechaFinal", fechaFin));
                        command.Parameters.Add(new SqlParameter("@xmlTractores", strListaTractores));
                        command.Parameters.Add(new SqlParameter("@xmlOperadores", strListaOperadores));
                        command.Parameters.Add(new SqlParameter("@xmlClientes", strListaClientes));
                        command.Parameters.Add(new SqlParameter("@xmlRutas", strListaRutas));
                        command.Parameters.Add(new SqlParameter("@xmlModalidades", strListaModalidades));
                        command.Parameters.Add(new SqlParameter("@xmlEstatus", strListaEstatus));
                        command.Parameters.Add(new SqlParameter("@todosOperadores", todosOperadores));
                        command.Parameters.Add(new SqlParameter("@todosTractores", todosTractores));
                        command.Parameters.Add(new SqlParameter("@idEmpresa", idEmpresa));
                        //connection.ConnectionTimeout = 10000;

                        var s = connection.ConnectionTimeout;

                        command.CommandTimeout = 3800;

                        connection.Open();
                        List<ReporteCartaPorte> list = new List<ReporteCartaPorte>();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CABGUIA = (int)reader["folio"];
                                if (CABGUIA == 127752)
                                {

                                }
                                ReporteCartaPorte item = new ReporteCartaPorte();

                                item.folio = (int)reader["folio"];
                                item.cartaPorte = (int)reader["cartaPorte"];
                                item.fechaPrograma = DBNull.Value.Equals(reader["fechaPrograma"]) ? null : (DateTime?)reader["fechaPrograma"];
                                item.fechaInicioCarga = DBNull.Value.Equals(reader["fechaInicioCarga"]) ? null : (DateTime?)reader["fechaInicioCarga"];
                                item.idEmpresa = (int)reader["idEmpresa"];
                                item.empresa = (string)reader["empresa"];
                                item.idCliente = (int)reader["IdCliente"];
                                item.cliente = (string)reader["cliente"];
                                item.idOperador = (int)reader["idOperador"];
                                item.operador = (string)reader["operador"];
                                item.operadorFin = (string)reader["operadorFin"];
                                item.operadorAyuda = (string)reader["operadorAyuda"];
                                item.operadorCap = (string)reader["operadorCap"];
                                item.estatus = (string)reader["estatus"];
                                item.modalidad = (string)reader["modalidad"];
                                item.tc = (string)reader["tc"];
                                item.rem1 = (string)reader["rem1"];
                                item.dolly = (string)reader["dolly"];
                                item.rem2 = (string)reader["rem2"];
                                item.fechaInicio = (DateTime)reader["fechaInicio"];
                                item.fechaFin = DBNull.Value.Equals(reader["fechaFin"]) ? null : ((DateTime?)reader["fechaFin"]);
                                item.km = (decimal)reader["km"];
                                item.idOrigen = DBNull.Value.Equals(reader["idOrigen"]) ? null : ((int?)reader["idOrigen"]);
                                item.origen = DBNull.Value.Equals(reader["origen"]) ? string.Empty : ((string)reader["origen"]);
                                item.idDestino = (int)reader["idDestino"];
                                item.destino = (string)reader["destino"];
                                item.orden = (string)reader["orden"];
                                item.idProducto = (int)reader["idProducto"];
                                item.producto = (string)reader["producto"];
                                item.capacidadIns = (decimal)reader["capacidadIns"];
                                item.toneladas = (decimal)reader["toneladas"];
                                item.toneladasDespachadas = (decimal)reader["toneladasDespachadas"];
                                item.precioVar = (decimal)reader["precioVar"];
                                item.subTotalVar = (decimal)reader["subTotalVar"];
                                item.precioFijo = (decimal)reader["precioFijo"];
                                item.subTotal = (decimal)reader["subTotal"];
                                item.cobroXKm = (bool)reader["cobroXkm"];
                                item.viaje = (bool)reader["viaje"];
                                item.impIva = (decimal)reader["impIva"];
                                item.impRetencion = (decimal)reader["ImpRetencion"];
                                item.vFalso = (bool)reader["vFalso"];
                                item.pagoOperador = (decimal)reader["pagoOperador"];
                                item.pagoOperadorCap = (decimal)reader["pagoOperadorCap"];
                                item.vale = (int)reader["vale"];
                                item.usuario = (string)reader["usuario"];
                                item.usuarioFin = (string)reader["usuarioFin"];
                                item.usuarioAjusta = (string)reader["usuarioAjusta"];
                                item.fechaSalidaPlantaCSI = DBNull.Value.Equals(reader["fechaSalidaPlantaCSI"]) ? null : (DateTime?)reader["fechaSalidaPlantaCSI"];
                                item.fechaEntradaPlantaCSI = DBNull.Value.Equals(reader["fechaEntradaPlantaCSI"]) ? null : (DateTime?)reader["fechaEntradaPlantaCSI"];
                                item.tiempoIda = (string)reader["tiempoIda"];
                                item.tiempoGranja = (string)reader["tiempoGranja"];
                                item.tiempoRetorno = (string)reader["tiempoRetorno"];
                                item.totalDespachados = (decimal)reader["totalDespachados"];
                                item.zona = (string)reader["zona"];
                                item.isReEnviar = Convert.ToBoolean(reader["isReEnvio"]);
                                item.imputable = (string)reader["imputable"];
                                item.noFactura = (string)reader["folioFactura"];
                                item.usuarioFactura = (string)reader["usuarioFacturo"];
                                item.kmECM = (decimal)reader["kmECM"];
                                item._ltsVale = (decimal)reader["ltsVale"];
                                item.folioImpreso = (string)reader["folioImpreso"];
                                item.ticket = (string)reader["ticket"];
                                item.ordenServ = (int)reader["ordenServ"];
                                item.precioLts1 = (decimal)reader["precioLts1"];
                                item.precioLts2 = (decimal)reader["precioLts2"];
                                item.importeLts = (decimal)reader["importeLts"];
                                item.facturaDiesel = (string)reader["facturaDiesel"];
                                item.proveedorCombustible = (string)reader["proveedorCombustible"];
                                item.kmFisico = (decimal)reader["kmFisico"];
                                item.kmOrigen = (decimal)reader["kmOrigen"];
                                item.kmOrigenDestino = (decimal)reader["kmOrigenDestino"];
                                item.kmDestino = (decimal)reader["kmDestino"];
                                item.estatusProducto = (EstatusProducto)(int)reader["estatusProducto"];
                                item.noViajesOperadores = (int)reader["noViajeOperador"];
                                item.cveEmpresa = (string)reader["cveEmpresa"];
                                item.cveUEN = (int)reader["cveUEN"];
                                item.cveCliente = (string)reader["cveClientes"];
                                item.cveSucCliente = (string)reader["cveSucCliente"];
                                item.cvePersonal = (string)reader["cvePersonal"];
                                item.cveOperador = DBNull.Value.Equals(reader["cveOperador"]) ? null : (string)reader["cveOperador"];
                                item.cveSucursal = (int)reader["cveSucursal"];
                                item.cveAgenteOperador = DBNull.Value.Equals(reader["cveOperadorCveAgente"]) ? null : (string)reader["cveOperadorCveAgente"];
                                item.cveTipoGranja = DBNull.Value.Equals(reader["cveTipoGranja"]) ? 0 : (int)reader["cveTipoGranja"];
                                item.tipoGranja = DBNull.Value.Equals(reader["tipoGranja"]) ? string.Empty : (string)reader["tipoGranja"];
                                item.cveTipoGranja2 = DBNull.Value.Equals(reader["cveTipoGranja2"]) ? 0 : (int)reader["cveTipoGranja2"];
                                item.tipoGranja2 = DBNull.Value.Equals(reader["tipoGranja2"]) ? string.Empty : (string)reader["tipoGranja2"];

                                list.Add(item);
                            }
                        }
                        return new OperationResult(command, list);
                    }
                }
            }
            catch (Exception exception)
            {
                return new OperationResult(exception);
            }
        }

        public OperationResult getReporteCartaPorteByOperadores(int idEmpresa, DateTime fechaInicio, DateTime fechaFin, List<string> listaOperadores)
        {
            int CABGUIA = 0;
            try
            {
                string strListaOperadores = GenerateXML.generarListaCadena(listaOperadores);

                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getCartaPortesByOperadores", false))
                    {
                        command.Parameters.Add(new SqlParameter("@fechaInicial", fechaInicio));
                        command.Parameters.Add(new SqlParameter("@fechaFinal", fechaFin));
                        command.Parameters.Add(new SqlParameter("@xmlOperadores", strListaOperadores));
                        command.Parameters.Add(new SqlParameter("@idEmpresa", idEmpresa));
                        //connection.ConnectionTimeout = 10000;

                        var s = connection.ConnectionTimeout;

                        command.CommandTimeout = 3800;

                        connection.Open();
                        List<ReporteCartaPorte> list = new List<ReporteCartaPorte>();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CABGUIA = (int)reader["folio"];
                                if (CABGUIA == 99546)
                                {

                                }
                                ReporteCartaPorte item = new ReporteCartaPorte
                                {
                                    folio = (int)reader["folio"],
                                    cartaPorte = (int)reader["cartaPorte"],
                                    fechaPrograma = DBNull.Value.Equals(reader["fechaPrograma"]) ? null : (DateTime?)reader["fechaPrograma"],
                                    fechaInicioCarga = DBNull.Value.Equals(reader["fechaInicioCarga"]) ? null : (DateTime?)reader["fechaInicioCarga"],
                                    idEmpresa = (int)reader["idEmpresa"],
                                    empresa = (string)reader["empresa"],
                                    idCliente = (int)reader["IdCliente"],
                                    cliente = (string)reader["cliente"],
                                    idOperador = (int)reader["idOperador"],
                                    idOperadorFin = (int)reader["idOperadorFin"],
                                    operador = (string)reader["operador"],
                                    operadorFin = (string)reader["operadorFin"],
                                    operadorAyuda = (string)reader["operadorAyuda"],
                                    operadorCap = (string)reader["operadorCap"],
                                    estatus = (string)reader["estatus"],
                                    modalidad = (string)reader["modalidad"],
                                    tc = (string)reader["tc"],
                                    rem1 = (string)reader["rem1"],
                                    dolly = (string)reader["dolly"],
                                    rem2 = (string)reader["rem2"],
                                    fechaInicio = (DateTime)reader["fechaInicio"],
                                    fechaFin = DBNull.Value.Equals(reader["fechaFin"]) ? null : ((DateTime?)reader["fechaFin"]),
                                    km = (decimal)reader["km"],
                                    idOrigen = DBNull.Value.Equals(reader["idOrigen"]) ? null : ((int?)reader["idOrigen"]),
                                    origen = DBNull.Value.Equals(reader["origen"]) ? string.Empty : ((string)reader["origen"]),
                                    idDestino = (int)reader["idDestino"],
                                    destino = (string)reader["destino"],
                                    orden = (string)reader["orden"],
                                    idProducto = (int)reader["idProducto"],
                                    producto = (string)reader["producto"],
                                    capacidadIns = (decimal)reader["capacidadIns"],
                                    toneladas = (decimal)reader["toneladas"],
                                    toneladasDespachadas = (decimal)reader["toneladasDespachadas"],
                                    precioVar = (decimal)reader["precioVar"],
                                    subTotalVar = (decimal)reader["subTotalVar"],
                                    precioFijo = (decimal)reader["precioFijo"],
                                    subTotal = (decimal)reader["subTotal"],
                                    cobroXKm = (bool)reader["cobroXkm"],
                                    viaje = (bool)reader["viaje"],
                                    impIva = (decimal)reader["impIva"],
                                    impRetencion = (decimal)reader["ImpRetencion"],
                                    vFalso = (bool)reader["vFalso"],
                                    pagoOperador = (decimal)reader["pagoOperador"],
                                    pagoOperadorCap = (decimal)reader["pagoOperadorCap"],
                                    vale = (int)reader["vale"],
                                    usuario = (string)reader["usuario"],
                                    usuarioFin = (string)reader["usuarioFin"],
                                    usuarioAjusta = (string)reader["usuarioAjusta"],
                                    fechaSalidaPlantaCSI = DBNull.Value.Equals(reader["fechaSalidaPlantaCSI"]) ? null : (DateTime?)reader["fechaSalidaPlantaCSI"],
                                    fechaEntradaPlantaCSI = DBNull.Value.Equals(reader["fechaEntradaPlantaCSI"]) ? null : (DateTime?)reader["fechaEntradaPlantaCSI"],
                                    tiempoIda = (string)reader["tiempoIda"],
                                    tiempoGranja = (string)reader["tiempoGranja"],
                                    tiempoRetorno = (string)reader["tiempoRetorno"],
                                    totalDespachados = (decimal)reader["totalDespachados"],
                                    zona = (string)reader["zona"],
                                    isReEnviar = Convert.ToBoolean(reader["isReEnvio"]),
                                    imputable = (string)reader["imputable"],
                                    noFactura = (string)reader["folioFactura"],
                                    usuarioFactura = (string)reader["usuarioFacturo"],
                                    kmECM = (decimal)reader["kmECM"],
                                    _ltsVale = (decimal)reader["ltsVale"],
                                    folioImpreso = (string)reader["folioImpreso"],
                                    ticket = (string)reader["ticket"],
                                    ordenServ = (int)reader["ordenServ"],
                                    precioLts1 = (decimal)reader["precioLts1"],
                                    precioLts2 = (decimal)reader["precioLts2"],
                                    importeLts = (decimal)reader["importeLts"],
                                    facturaDiesel = (string)reader["facturaDiesel"],
                                    proveedorCombustible = (string)reader["proveedorCombustible"],
                                    kmFisico = (decimal)reader["kmFisico"],
                                    kmOrigen = (decimal)reader["kmOrigen"],
                                    kmOrigenDestino = (decimal)reader["kmOrigenDestino"],
                                    kmDestino = (decimal)reader["kmDestino"],
                                    estatusProducto = (EstatusProducto)reader["estatusProducto"],
                                    noViajesOperadores = (int)reader["noViajeOperador"]
                                };
                                list.Add(item);
                            }
                        }
                        return new OperationResult(command, list);
                    }
                }
            }
            catch (Exception exception)
            {
                return new OperationResult(exception);
            }
        }
        public OperationResult getReporteCartaPorteByOperadoresAtlante(int idEmpresa, DateTime fechaInicio, DateTime fechaFin, List<string> listaOperadores)
        {
            int CABGUIA = 0;
            try
            {
                string strListaOperadores = GenerateXML.generarListaCadena(listaOperadores);

                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getCartaPortesByOperadoresAtlante", false))
                    {
                        command.Parameters.Add(new SqlParameter("@fechaInicial", fechaInicio));
                        command.Parameters.Add(new SqlParameter("@fechaFinal", fechaFin));
                        command.Parameters.Add(new SqlParameter("@xmlOperadores", strListaOperadores));
                        command.Parameters.Add(new SqlParameter("@idEmpresa", idEmpresa));
                        //connection.ConnectionTimeout = 10000;

                        var s = connection.ConnectionTimeout;

                        command.CommandTimeout = 3800;

                        connection.Open();
                        List<ReporteCartaPorte> list = new List<ReporteCartaPorte>();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CABGUIA = (int)reader["folio"];
                                if (CABGUIA == 99546)
                                {

                                }
                                ReporteCartaPorte item = new ReporteCartaPorte
                                {
                                    folio = (int)reader["folio"],
                                    cartaPorte = (int)reader["cartaPorte"],
                                    fechaPrograma = DBNull.Value.Equals(reader["fechaPrograma"]) ? null : (DateTime?)reader["fechaPrograma"],
                                    fechaInicioCarga = DBNull.Value.Equals(reader["fechaInicioCarga"]) ? null : (DateTime?)reader["fechaInicioCarga"],
                                    idEmpresa = (int)reader["idEmpresa"],
                                    empresa = (string)reader["empresa"],
                                    idCliente = (int)reader["IdCliente"],
                                    cliente = (string)reader["cliente"],
                                    idOperador = (int)reader["idOperador"],
                                    idOperadorFin = (int)reader["idOperadorFin"],
                                    operador = (string)reader["operador"],
                                    operadorFin = (string)reader["operadorFin"],
                                    operadorAyuda = (string)reader["operadorAyuda"],
                                    operadorCap = (string)reader["operadorCap"],
                                    estatus = (string)reader["estatus"],
                                    modalidad = (string)reader["modalidad"],
                                    tc = (string)reader["tc"],
                                    rem1 = (string)reader["rem1"],
                                    dolly = (string)reader["dolly"],
                                    rem2 = (string)reader["rem2"],
                                    fechaInicio = (DateTime)reader["fechaInicio"],
                                    fechaFin = DBNull.Value.Equals(reader["fechaFin"]) ? null : ((DateTime?)reader["fechaFin"]),
                                    km = (decimal)reader["km"],
                                    idOrigen = DBNull.Value.Equals(reader["idOrigen"]) ? null : ((int?)reader["idOrigen"]),
                                    origen = DBNull.Value.Equals(reader["origen"]) ? string.Empty : ((string)reader["origen"]),
                                    idDestino = (int)reader["idDestino"],
                                    destino = (string)reader["destino"],
                                    orden = (string)reader["orden"],
                                    idProducto = (int)reader["idProducto"],
                                    producto = (string)reader["producto"],
                                    capacidadIns = (decimal)reader["capacidadIns"],
                                    toneladas = (decimal)reader["toneladas"],
                                    toneladasDespachadas = (decimal)reader["toneladasDespachadas"],
                                    precioVar = (decimal)reader["precioVar"],
                                    subTotalVar = (decimal)reader["subTotalVar"],
                                    precioFijo = (decimal)reader["precioFijo"],
                                    subTotal = (decimal)reader["subTotal"],
                                    cobroXKm = (bool)reader["cobroXkm"],
                                    viaje = (bool)reader["viaje"],
                                    impIva = (decimal)reader["impIva"],
                                    impRetencion = (decimal)reader["ImpRetencion"],
                                    vFalso = (bool)reader["vFalso"],
                                    pagoOperador = (decimal)reader["pagoOperador"],
                                    pagoOperadorCap = (decimal)reader["pagoOperadorCap"],
                                    vale = (int)reader["vale"],
                                    usuario = (string)reader["usuario"],
                                    usuarioFin = (string)reader["usuarioFin"],
                                    usuarioAjusta = (string)reader["usuarioAjusta"],
                                    fechaSalidaPlantaCSI = DBNull.Value.Equals(reader["fechaSalidaPlantaCSI"]) ? null : (DateTime?)reader["fechaSalidaPlantaCSI"],
                                    fechaEntradaPlantaCSI = DBNull.Value.Equals(reader["fechaEntradaPlantaCSI"]) ? null : (DateTime?)reader["fechaEntradaPlantaCSI"],
                                    tiempoIda = (string)reader["tiempoIda"],
                                    tiempoGranja = (string)reader["tiempoGranja"],
                                    tiempoRetorno = (string)reader["tiempoRetorno"],
                                    totalDespachados = (decimal)reader["totalDespachados"],
                                    zona = (string)reader["zona"],
                                    isReEnviar = Convert.ToBoolean(reader["isReEnvio"]),
                                    imputable = (string)reader["imputable"],
                                    noFactura = (string)reader["folioFactura"],
                                    usuarioFactura = (string)reader["usuarioFacturo"],
                                    kmECM = (decimal)reader["kmECM"],
                                    _ltsVale = (decimal)reader["ltsVale"],
                                    folioImpreso = (string)reader["folioImpreso"],
                                    ticket = (string)reader["ticket"],
                                    ordenServ = (int)reader["ordenServ"],
                                    precioLts1 = (decimal)reader["precioLts1"],
                                    precioLts2 = (decimal)reader["precioLts2"],
                                    importeLts = (decimal)reader["importeLts"],
                                    facturaDiesel = (string)reader["facturaDiesel"],
                                    proveedorCombustible = (string)reader["proveedorCombustible"],
                                    kmFisico = (decimal)reader["kmFisico"],
                                    kmOrigen = (decimal)reader["kmOrigen"],
                                    kmOrigenDestino = (decimal)reader["kmOrigenDestino"],
                                    kmDestino = (decimal)reader["kmDestino"],
                                    estatusProducto = (EstatusProducto)reader["estatusProducto"],
                                    noViajesOperadores = (int)reader["noViajeOperador"],
                                    fechaCarga = DBNull.Value.Equals(reader["fechaCarga"]) ? null : (DateTime?)reader["fechaCarga"],
                                    fechaDescarga = DBNull.Value.Equals(reader["fechaDescarga"]) ? null : (DateTime?)reader["fechaDescarga"]
                                };
                                list.Add(item);
                            }
                        }
                        return new OperationResult(command, list);
                    }
                }
            }
            catch (Exception exception)
            {
                return new OperationResult(exception);
            }
        }

        public OperationResult getViajesFacturar(int idEmpresa, DateTime fechaInicio, DateTime fechaFin, bool byFechaDespacho, int idCliente, int idZonaOpetativa,
            List<string> listaRutas, List<string> listaModalidades)
        {
            int CABGUIA = 0;
            try
            {
                string strListaRutas = GenerateXML.generarListaCadena(listaRutas);
                string strListaModalidades = GenerateXML.generarListaCadena(listaModalidades);
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, (byFechaDespacho ? "sp_getViajesAFacturadosByDespacho" : "sp_getViajesAFacturadosByPrograma"), false))
                    {
                        command.Parameters.Add(new SqlParameter("@fechaInicial", fechaInicio));
                        command.Parameters.Add(new SqlParameter("@fechaFinal", fechaFin));
                        command.Parameters.Add(new SqlParameter("@xmlRutas", strListaRutas));
                        command.Parameters.Add(new SqlParameter("@xmlModalidades", strListaModalidades));
                        command.Parameters.Add(new SqlParameter("@idCliente", idCliente));
                        command.Parameters.Add(new SqlParameter("@idZonaOperativa", idZonaOpetativa));
                        //connection.ConnectionTimeout = 10000;
                        command.CommandTimeout = 100000;
                        connection.Open();
                        List<ReporteCartaPorte> list = new List<ReporteCartaPorte>();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CABGUIA = (int)reader["folio"];
                                ReporteCartaPorte item = new ReporteCartaPorte
                                {
                                    folio = (int)reader["folio"],
                                    cartaPorte = (int)reader["cartaPorte"],
                                    idEmpresa = (int)reader["idEmpresa"],
                                    fechaPrograma = DBNull.Value.Equals(reader["fechaPrograma"]) ? null : (DateTime?)reader["fechaPrograma"],
                                    empresa = (string)reader["empresa"],
                                    idCliente = (int)reader["IdCliente"],
                                    cliente = (string)reader["cliente"],
                                    idOperador = (int)reader["idOperador"],
                                    operador = (string)reader["operador"],
                                    operadorFin = (string)reader["operadorFin"],
                                    operadorAyuda = (string)reader["operadorAyuda"],
                                    operadorCap = (string)reader["operadorCap"],
                                    estatus = (string)reader["estatus"],
                                    modalidad = (string)reader["modalidad"],
                                    tc = (string)reader["tc"],
                                    rem1 = (string)reader["rem1"],
                                    rem2 = (string)reader["rem2"],
                                    fechaInicio = (DateTime)reader["fechaInicio"],
                                    fechaFin = DBNull.Value.Equals(reader["fechaFin"]) ? null : ((DateTime?)reader["fechaFin"]),
                                    km = (decimal)reader["km"],
                                    idOrigen = DBNull.Value.Equals(reader["idOrigen"]) ? null : ((int?)reader["idOrigen"]),
                                    origen = DBNull.Value.Equals(reader["origen"]) ? string.Empty : ((string)reader["origen"]),
                                    idDestino = (int)reader["idDestino"],
                                    destino = (string)reader["destino"],
                                    orden = (string)reader["orden"],
                                    producto = (string)reader["producto"],
                                    capacidadIns = (decimal)reader["capacidadIns"],
                                    toneladas = (decimal)reader["toneladas"],
                                    toneladasDespachadas = (decimal)reader["toneladasDespachadas"],
                                    precioVar = (decimal)reader["precioVar"],
                                    subTotalVar = (decimal)reader["subTotalVar"],
                                    precioFijo = (decimal)reader["precioFijo"],
                                    subTotal = (decimal)reader["subTotal"],
                                    cobroXKm = (bool)reader["cobroXkm"],
                                    viaje = (bool)reader["viaje"],
                                    impIva = (decimal)reader["impIva"],
                                    impRetencion = (decimal)reader["ImpRetencion"],
                                    vFalso = (bool)reader["vFalso"],
                                    pagoOperador = (decimal)reader["pagoOperador"],
                                    pagoOperadorCap = (decimal)reader["pagoOperadorCap"],
                                    vale = (int)reader["vale"],
                                    usuario = (string)reader["usuario"],
                                    usuarioFin = (string)reader["usuarioFin"],
                                    usuarioAjusta = (string)reader["usuarioAjusta"],
                                    fechaSalidaPlantaCSI = DBNull.Value.Equals(reader["fechaSalidaPlantaCSI"]) ? null : (DateTime?)reader["fechaSalidaPlantaCSI"],
                                    fechaEntradaPlantaCSI = DBNull.Value.Equals(reader["fechaEntradaPlantaCSI"]) ? null : (DateTime?)reader["fechaEntradaPlantaCSI"],
                                    tiempoIda = (string)reader["tiempoIda"],
                                    tiempoGranja = (string)reader["tiempoGranja"],
                                    tiempoRetorno = (string)reader["tiempoRetorno"],
                                    totalDespachados = (decimal)reader["totalDespachados"],
                                    zona = (string)reader["zona"],
                                    isReEnviar = Convert.ToBoolean(reader["isReEnvio"]),
                                    imputable = (string)reader["imputable"]
                                };
                                list.Add(item);
                            }
                        }
                        return new OperationResult(command, list);
                    }
                }
            }
            catch (Exception exception)
            {
                return new OperationResult(exception);
            }
        }
    }
}
