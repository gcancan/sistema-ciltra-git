﻿namespace CoreFletera.BusinessLogic
{
    using Core.Models;
    using Core.Utils;
    using CoreFletera.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;

    public class PanelOSTransitoServices
    {
        public OperationResult getPanelOSTransitoByTipoOrden(int idTipoOrdenServ)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getPanelOSTransitoByTipoOrden", false))
                    {
                        command.Parameters.Add(new SqlParameter("@tipoOrden", idTipoOrdenServ));
                        List<PanelOSTransito> list = new List<PanelOSTransito>();
                        connection.Open();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                PanelOSTransito item = new PanelOSTransito
                                {
                                    unidadTrans = (string)reader["unidadTrans"],
                                    chofer = (string)reader["chofer"],
                                    empresa = (string)reader["empresa"],
                                    patios = Convert.ToBoolean(reader["patios"]),
                                    idOrdenServ = (int)reader["idOrdenSer"],
                                    idPadre = (int)reader["idPadre"],
                                    fechaCaptura = (DateTime)reader["fechaCaptura"]
                                };
                                list.Add(item);
                            }
                        }
                        result = new OperationResult(command, list);
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult(exception);
            }
            return result;
        }
    }
}
