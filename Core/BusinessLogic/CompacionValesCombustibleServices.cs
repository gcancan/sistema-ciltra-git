﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using System.Data;
using System.Data.SqlClient;
using Core.Utils;
namespace CoreFletera.BusinessLogic
{
    public class CompacionValesCombustibleServices
    {
        public OperationResult getListaValesLis(string unidadTras, string mes, int año)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(con, "sp_getListaValesLis"))
                    {
                        command.Parameters.Add(new SqlParameter("@unidadTrans", unidadTras));
                        command.Parameters.Add(new SqlParameter("@mes", mes));
                        command.Parameters.Add(new SqlParameter("@anio", año));

                        List<ComparacionValesCombustible> lista = new List<ComparacionValesCombustible>();
                        con.Open();
                        using (SqlDataReader sqlReader = command.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                lista.Add(new ComparacionValesCombustible
                                {
                                    id = (int)sqlReader["idVale"],
                                    mes = (string)sqlReader["mes"],
                                    año = (int)sqlReader["anio"],
                                    unidadTransporte = (string)sqlReader["unidadTransporte"],
                                    vale = (string)sqlReader["vale"],
                                    carga = (decimal)sqlReader["carga"],
                                    mach = false
                                });
                            }
                        }
                        return new OperationResult(command, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        public OperationResult getListaVales(string unidadTras, int mes, int año)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(con, "sp_getListaVales"))
                    {
                        command.Parameters.Add(new SqlParameter("@unidadTrans", unidadTras));
                        command.Parameters.Add(new SqlParameter("@mes", mes));
                        command.Parameters.Add(new SqlParameter("@anio", año));

                        List<ComparacionValesCombustible> lista = new List<ComparacionValesCombustible>();
                        con.Open();
                        using (SqlDataReader sqlReader = command.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                lista.Add(new ComparacionValesCombustible
                                {
                                    id = (int)sqlReader["idVale"],
                                    mes = (string)sqlReader["mes"],
                                    año = (int)sqlReader["anio"],
                                    unidadTransporte = (string)sqlReader["unidadTransporte"],
                                    vale = (string)sqlReader["vale"],
                                    carga = (decimal)sqlReader["carga"],
                                    mach = false
                                });
                            }
                        }
                        return new OperationResult(command, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        public OperationResult saveValesLIS(List<ComparacionValesCombustible> lista)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    con.Open();
                    using (SqlTransaction tran = con.BeginTransaction())
                    {
                        using (SqlCommand command = CrearSqlCommand.getCommand(con, "sp_saveValesLIS"))
                        {
                            command.Transaction = tran;
                            command.Parameters.Add(new SqlParameter("@mes", SqlDbType.NVarChar));
                            command.Parameters.Add(new SqlParameter("@unidadTrans", SqlDbType.NVarChar));
                            command.Parameters.Add(new SqlParameter("@vale", SqlDbType.NVarChar));
                            command.Parameters.Add(new SqlParameter("@año", SqlDbType.Int));
                            command.Parameters.Add(new SqlParameter("@carga", SqlDbType.Decimal));

                            foreach (var item in lista)
                            {
                                command.Parameters["@mes"].Value = item.mes;
                                command.Parameters["@unidadTrans"].Value = item.unidadTransporte;
                                command.Parameters["@vale"].Value = item.vale;
                                command.Parameters["@año"].Value = item.año;
                                command.Parameters["@carga"].Value = item.carga;
                                command.ExecuteNonQuery();                               

                                if ((int)command.Parameters["@valor"].Value != 0)
                                {
                                    tran.Rollback();
                                    return new OperationResult(command);
                                }
                            }
                            tran.Commit();
                            return new OperationResult(command);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
}
