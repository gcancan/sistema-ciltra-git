﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using Core.Utils;
using System.Data;
using System.Data.SqlClient;
namespace CoreFletera.BusinessLogic
{
    public class PlazaService
    {
        public OperationResult sincronizarPlazas(List<Plaza> listaPlaza)
        {
			try
			{
				string xml = GenerateXML.generarListaPlazasSinc(listaPlaza);
				using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
				{
					con.Open();
					using (SqlTransaction tran = con.BeginTransaction())
					{
						using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_sincronizarPlazas", false, true))
						{
							comad.Transaction = tran;
							comad.Parameters.Add(new SqlParameter("@xml", xml));
							comad.ExecuteNonQuery();
							if (!CrearSqlCommand.validarCorrecto(comad))
							{
								tran.Rollback();
							}
							else
							{
								tran.Commit();
							}
							return new OperationResult(comad);
						}
					}
				}
			}
			catch (Exception ex)
			{
				return new OperationResult(ex);
			}
        }
    }
}
