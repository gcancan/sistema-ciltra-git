﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using Core.Utils;
using Core.BusinessLogic;
using System.Data;
using System.Data.SqlClient;
namespace CoreFletera.BusinessLogic
{
    public class MesContableServices
    {
        public OperationResult getMesContable(int mes, int anio, int idEmpresa)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand commad = CrearSqlCommand.getCommand(con, "sp_consultarMesContable"))
                    {
                        commad.Parameters.Add(new SqlParameter("@mes", mes));
                        commad.Parameters.Add(new SqlParameter("@anio", anio));
                        commad.Parameters.Add(new SqlParameter("@idEmpresa", idEmpresa));
                        MesContable mesContable = new MesContable();
                        con.Open();
                        using (SqlDataReader sqlReader = commad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                mesContable = new mapComunes().mapMesContable(sqlReader);
                                if (mesContable == null)
                                {
                                    return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");
                                }
                            }
                        }
                        return new OperationResult(commad, mesContable);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult mesContableAbiertoByIdEmpresa(int idEmpresa)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand commad = CrearSqlCommand.getCommand(con, "sp_mesContableAbiertoByIdEmpresa"))
                    {                        
                        commad.Parameters.Add(new SqlParameter("@idEmpresa", idEmpresa));
                        MesContable mesContable = new MesContable();
                        con.Open();
                        using (SqlDataReader sqlReader = commad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                mesContable = new mapComunes().mapMesContable(sqlReader);
                                if (mesContable == null)
                                {
                                    return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");
                                }
                            }
                        }
                        return new OperationResult(commad, mesContable);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult abrirMesContable(ref MesContable mesContable)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    con.Open();
                    using (SqlTransaction tran = con.BeginTransaction())
                    {
                        using (SqlCommand commad = CrearSqlCommand.getCommand(con, "sp_abrirMesContable", true))
                        {
                            commad.Transaction = tran;
                            commad.Parameters.Add(new SqlParameter("@mes", mesContable.mes));
                            commad.Parameters.Add(new SqlParameter("@anio", mesContable.anio));
                            commad.Parameters.Add(new SqlParameter("@idEmpresa", mesContable.idEmpresa));
                            commad.Parameters.Add(new SqlParameter("@fechaInicial", mesContable.fechaInicio));

                            commad.ExecuteNonQuery();
                            int id = 0;
                            if (CrearSqlCommand.validarCorrecto(commad, out id))
                            {
                                mesContable.idMesContable = id;
                                tran.Commit();
                            }
                            else
                            {
                                tran.Rollback();
                            }                            
                            return new OperationResult(commad, mesContable);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult cerrarMesContable(ref MesContable mesContable)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    con.Open();
                    using (SqlTransaction tran = con.BeginTransaction())
                    {
                        using (SqlCommand commad = CrearSqlCommand.getCommand(con, "sp_cerrarMesContable"))
                        {
                            commad.Transaction = tran;
                            commad.Parameters.Add(new SqlParameter("@idMesContable", mesContable.idMesContable));
                            commad.Parameters.Add(new SqlParameter("@fechaFin", mesContable.fechaFin));

                            commad.ExecuteNonQuery();
                            if (CrearSqlCommand.validarCorrecto(commad))
                            {
                                tran.Commit();
                            }
                            else
                            {
                                tran.Rollback();
                            }
                            return new OperationResult(commad, mesContable);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
}
