﻿using Core.Models;
using Core.Utils;
using System.Data;
using System.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoreFletera.Models;

namespace CoreFletera.BusinessLogic
{
    public class BitacoraLavaderoServices
    {
        internal OperationResult getBitacoraLavadero(int idEmpresa, DateTime fechaInicio, DateTime fechaFin, List<string> listaTractores)
        {
            try
            {
                string xml = GenerateXML.generarListaUnidades(listaTractores);
                if (string.IsNullOrEmpty(xml))
                {
                    return new OperationResult { valor = 2, mensaje = "error al construir el XML" };
                }
                using (SqlConnection sqlConection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand command = sqlConection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getServiciosLavadero";

                        command.Parameters.Add(new SqlParameter("@idEmpresa", idEmpresa));
                        command.Parameters.Add(new SqlParameter("@fechaInicio", fechaInicio));
                        command.Parameters.Add(new SqlParameter("@fechaFin", fechaFin));
                        command.Parameters.Add(new SqlParameter("@xml", xml));

                        SqlParameter spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        SqlParameter spMensaje = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        List<BitacoraLavadero> lista = new List<BitacoraLavadero>();
                        sqlConection.Open();
                        using (SqlDataReader sqlReader = command.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                lista.Add(new BitacoraLavadero
                                {
                                    idPadreOrdSer = (int)sqlReader["idPadreOrdSer"],
                                    idOrdenServ = (int)sqlReader["idOrdenSer"],
                                    fechaSolicitud = (DateTime)sqlReader["FechaCreacion"],
                                    fechaRecepcion = DBNull.Value.Equals(sqlReader["FechaRecepcion"]) ? null : (DateTime?)sqlReader["FechaRecepcion"],
                                    fechaTerminado = DBNull.Value.Equals(sqlReader["FechaTerminado"]) ? null : (DateTime?)sqlReader["FechaTerminado"],
                                    unidadTrans = (string)sqlReader["idUnidadTrans"],
                                    estatus = (string)sqlReader["Estatus"],
                                    personal = DBNull.Value.Equals(sqlReader["personal"]) ? string.Empty : (string)sqlReader["personal"],
                                    costoInspecciones = (decimal)sqlReader["costoInspecciones"],
                                    costoInsumos = (decimal)sqlReader["costoInsumos"],
                                    usuario = (string)sqlReader["UserCrea"]
                                });
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = lista };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
}
