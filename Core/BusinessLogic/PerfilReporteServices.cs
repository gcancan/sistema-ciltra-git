﻿namespace Core.BusinessLogic
{
    using Core.Models;
    using Core.Utils;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;

    public class PerfilReporteServices
    {
        public OperationResult getDetallesPerfilesReportes(int id)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getAllDetallesPerfilesReportes";
                        command.Parameters.Add(new SqlParameter("@id", id));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        List<DetallesPerfilReporte> list = new List<DetallesPerfilReporte>();
                        using (SqlDataReader reader2 = reader)
                        {
                            while (reader2.Read())
                            {
                                DetallesPerfilReporte item = new DetallesPerfilReporte
                                {
                                    idDetallePerfilReporte = (int)reader2["idDetallePerfilReporte"],
                                    idPerfilReporte = (int)reader2["idPerfilReporte"],
                                    nombreDetalle = (string)reader2["nombreDetalle"]
                                };
                                list.Add(item);
                            }
                        }
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = list
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
            return result;
        }

        public OperationResult getPerfilesReportes(int idUsuario, enumPerfilRporte tipoReporte)
        {
            OperationResult result2;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getAllPerfilesReportes";
                        command.Parameters.Add(new SqlParameter("@idUsuario", idUsuario));
                        command.Parameters.Add(new SqlParameter("@tipoReporte", (int)tipoReporte));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        List<PerfilReporte> list = new List<PerfilReporte>();
                        using (SqlDataReader reader2 = reader)
                        {
                            while (reader2.Read())
                            {
                                PerfilReporte item = new PerfilReporte
                                {
                                    idPerfilReporte = (int)reader2["idPerfilReporte"],
                                    nombrePerfil = (string)reader2["nombrePerfil"]
                                };
                                OperationResult result = new OperationResult();
                                result = this.getDetallesPerfilesReportes((int)reader2["idPerfilReporte"]);
                                item.listDetalles = (List<DetallesPerfilReporte>)result.result;
                                list.Add(item);
                            }
                        }
                        result2 = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = list
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result2 = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
            return result2;
        }

        public OperationResult savePerfilReporte(ref PerfilReporte perfilReporte)
        {
            OperationResult result;
            try
            {
                object obj2 = GenerateXML.generateListDetalles(perfilReporte.listDetalles);
                if (obj2 == null)
                {
                    result = new OperationResult
                    {
                        mensaje = "Error al contruir el XML",
                        valor = 1
                    };
                }
                else
                {
                    using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                    {
                        using (SqlCommand command = connection.CreateCommand())
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.CommandText = "sp_savePerfilReporte";
                            command.Parameters.Add(new SqlParameter("@nombrePerfil", perfilReporte.nombrePerfil));
                            command.Parameters.Add(new SqlParameter("@idUsuario", perfilReporte.usuraio.idUsuario));
                            command.Parameters.Add(new SqlParameter("@tipoReporte", (int)perfilReporte.tipoReporte));
                            command.Parameters.Add(new SqlParameter("@activo", perfilReporte.activo));
                            command.Parameters.Add(new SqlParameter("@xml", obj2));
                            SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                            {
                                Direction = ParameterDirection.Output
                            };
                            SqlParameter parameter = command.Parameters.Add(parameter1);
                            SqlParameter parameter4 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 255)
                            {
                                Direction = ParameterDirection.Output
                            };
                            SqlParameter parameter2 = command.Parameters.Add(parameter4);
                            SqlParameter parameter5 = new SqlParameter("@identificador", SqlDbType.Int)
                            {
                                Direction = ParameterDirection.Output
                            };
                            SqlParameter parameter3 = command.Parameters.Add(parameter5);
                            connection.Open();
                            using (SqlDataReader reader2 = command.ExecuteReader())
                            {
                                perfilReporte.listDetalles = new List<DetallesPerfilReporte>();
                                while (reader2.Read())
                                {
                                    DetallesPerfilReporte item = new DetallesPerfilReporte
                                    {
                                        idDetallePerfilReporte = (int)reader2["idDetallePerfilReporte"],
                                        idPerfilReporte = (int)reader2["idPerfilReporte"],
                                        nombreDetalle = (string)reader2["nombreDetalle"]
                                    };
                                    perfilReporte.listDetalles.Add(item);
                                }
                            }
                            perfilReporte.idPerfilReporte = (int)parameter3.Value;
                            result = new OperationResult
                            {
                                valor = new int?((int)parameter.Value),
                                mensaje = (string)parameter2.Value,
                                result = perfilReporte
                            };
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
            return result;
        }
    }
}
