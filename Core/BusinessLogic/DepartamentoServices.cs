﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using Core.Utils;
using System.Data;
using System.Data.SqlClient;
using CoreFletera.BusinessLogic;
namespace Core.BusinessLogic
{
    public class DepartamentoServices
    {
        public OperationResult getDepartamentosALLorById(int id)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getDepartamentosALLorById";
                        command.Parameters.Add(new SqlParameter("@id", id));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        connection.Open();
                        var i = command.ExecuteReader();
                        List<Departamento> listDepartamentos = new List<Departamento>();

                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {

                                listDepartamentos.Add (new Departamento
                                {
                                   clave_empresa = (int)sqlReader["Clave_empresa"],
                                   clave = (int)sqlReader["Clave"],
                                   descripcion = (string)sqlReader["Descripcion"],
                                   es_almacen = (bool)sqlReader["Es_almacen"],
                                   activo = (bool)sqlReader["Activo"]
                                });
                            }
                        }

                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)codigo.Value, result = listDepartamentos };
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }
        public OperationResult getALLDepartamentosById(int id = 0)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = CrearSqlCommand.getCommand(connection , "sp_getALLDepartamentosById"))
                    {
                        command.Parameters.Add(new SqlParameter("@id", id));

                        connection.Open();
                        
                        List<Departamento> listDepartamentos = new List<Departamento>();

                        using (SqlDataReader sqlReader = command.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                Departamento departamento = new mapComunes().mapDepartamento(sqlReader);
                                if (departamento == null)
                                {
                                    return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");
                                }
                                listDepartamentos.Add(departamento);
                            }
                        }
                        return new OperationResult (command, listDepartamentos);
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }

        public OperationResult saveDepartamento(ref Departamento departamento)
        {
            try
            {
                using (SqlConnection con  = BoundedContextFactory.ConnectionFactory())
                {
                    con.Open();
                    using (SqlTransaction tran = con. BeginTransaction())
                    {
                        using (SqlCommand comand = CrearSqlCommand.getCommand(con, "sp_saveDepaartamento", true))
                        {
                            comand.Transaction = tran;
                            comand.Parameters.Add(new SqlParameter("@nombreDepto", departamento.descripcion));
                            comand.Parameters.Add(new SqlParameter("@activo", departamento.activo));
                            comand.Parameters.Add(new SqlParameter("@idDepartamento", departamento.clave));
                            int id = 0;
                            comand.ExecuteNonQuery();
                            if (CrearSqlCommand.validarCorrecto(comand, out id))
                            {
                                departamento.clave = id;
                                tran.Commit();
                            }
                            else
                            {
                                tran.Rollback();
                            }
                            return new OperationResult(comand, departamento);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult sincronizarDepartamentos(List<Departamento> listaDepartamentos)
        {
            try
            {
                string xml = GenerateXML.generarListaDeptosSin(listaDepartamentos);
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    con.Open();
                    using (SqlTransaction tran = con.BeginTransaction())
                    {
                        using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_sincronizarDepartamentos", false, true))
                        {
                            comad.Transaction = tran;
                            comad.Parameters.Add(new SqlParameter("@xml", xml));
                            comad.ExecuteNonQuery();
                            if (!CrearSqlCommand.validarCorrecto(comad))
                            {
                                tran.Rollback();
                            }
                            else
                            {
                                tran.Commit();
                            }
                            return new OperationResult(comad);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
}
