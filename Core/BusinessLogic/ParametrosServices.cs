﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using Core.Utils;
using System.Data;
using System.Data.SqlClient;
namespace CoreFletera.BusinessLogic
{
    public class ParametrosServices
    {
        public OperationResult getParametrosSistema()
        {
            try
            {
                using (SqlConnection conn = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(conn, "sp_getParametrosSistema"))
                    {
                        Parametros parametros = new Parametros();
                        conn.Open();
                        using (SqlDataReader sqlReader = command.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                parametros = new Parametros
                                {
                                    idConceptoEntAlm = (string)sqlReader["idConceptoEntAlm"],
                                    idAlmacenSal = (string)sqlReader["idAlmacenSal"],
                                    idAlmacenEnt = (string)sqlReader["idAlmacenEnt"],
                                    idConceptoSalAlm = (string)sqlReader["idConceptoSalAlm"]
                                };
                            }
                        }
                        return new OperationResult(command, parametros);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
}
