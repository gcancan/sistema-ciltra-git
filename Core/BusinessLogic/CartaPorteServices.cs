﻿namespace Core.BusinessLogic
{
    using Core.Interfaces;
    using Core.Models;
    using Core.Utils;
    using CoreFletera.BusinessLogic;
    using CoreFletera.Models;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Runtime.InteropServices;

    public class CartaPorteServices
    {
        public OperationResult activarViajeBachoco(int idViaje, eEstatus estatus)
        {
            OperationResult result2;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_ActivarViajeBachoco";
                        command.Parameters.Add(new SqlParameter("@idViaje", idViaje));
                        command.Parameters.Add(new SqlParameter("@estatus", estatus.ToString()));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        OperationResult result = this.getCartaPorteById(idViaje);
                        if (result.typeResult == ResultTypes.success)
                        {
                            return new OperationResult
                            {
                                valor = new int?((int)parameter.Value),
                                mensaje = (string)parameter2.Value,
                                result = ((List<Viaje>)result.result)[0]
                            };
                        }
                        result2 = result;
                    }
                }
            }
            catch (Exception exception)
            {
                result2 = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
            return result2;
        }

        public OperationResult asignarCargaCombustibleViaje(int idVale, int idViaje)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_asignarValeAViajes";
                        command.Parameters.Add(new SqlParameter("@idVale", idVale));
                        command.Parameters.Add(new SqlParameter("@idViaje", idViaje));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        int num = command.ExecuteNonQuery();
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            return result;
        }

        public OperationResult cambiarValesCarga(List<ValeCarga> listVales, int valeGasolina, int userFin, DateTime fechaInicio, DateTime fechaFin, bool finalizar = true)
        {
            OperationResult result;
            try
            {
                string str = GenerateXML.generarListaValesCarga(listVales);
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_cambiarValesCarga";
                        command.Parameters.Add(new SqlParameter("@xml", str));
                        command.Parameters.Add(new SqlParameter("@idValeGasolina", valeGasolina));
                        command.Parameters.Add(new SqlParameter("@finalizar", finalizar));
                        command.Parameters.Add(new SqlParameter("@userFin", userFin));
                        command.Parameters.Add(new SqlParameter("@fechaInicio", fechaInicio));
                        command.Parameters.Add(new SqlParameter("@fechaFin", fechaFin));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        command.ExecuteNonQuery();
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            return result;
        }

        internal OperationResult cancelarViajeBachoco(int numGuiaId, eEstatus estatus, string motivo)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_cancelarViajeBachoco";
                        command.Parameters.Add(new SqlParameter("@id", numGuiaId));
                        command.Parameters.Add(new SqlParameter("@estatus", estatus.ToString()));
                        command.Parameters.Add(new SqlParameter("@motivo", motivo));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = null
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
            return result;
        }

        public OperationResult cerrarCancelarCartaPorte(int idCartaPorte, TipoAccion tipoAccion, int idUsuario, string motivo = "", DateTime? fecha = new DateTime?())
        {
            OperationResult result2;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_cerrarCancelarCartaPorte";
                        command.Parameters.Add(new SqlParameter("@id", idCartaPorte));
                        command.Parameters.Add(new SqlParameter("@accion", tipoAccion.ToString()));
                        command.Parameters.Add(new SqlParameter("@idUsuario", idUsuario));
                        command.Parameters.Add(new SqlParameter("@motivo", motivo));
                        command.Parameters.Add(new SqlParameter("@fecha", fecha));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        List<Viaje> list = new List<Viaje>();
                        using (SqlDataReader reader2 = reader)
                        {
                            while (reader2.Read())
                            {
                                Viaje item = new Viaje
                                {
                                    NumGuiaId = (int)reader2["NumGuiaId"],
                                    SerieGuia = (string)reader2["SerieGuia"],
                                    NumViaje = (int)reader2["NumViaje"],
                                    FechaHoraViaje = (DateTime)reader2["FechaHoraViaje"],
                                    FechaHoraFin = new DateTime?((DateTime)reader2["FechaHoraFin"]),
                                    EstatusGuia = (string)reader2["EstatusGuia"],
                                    tara = DBNull.Value.Equals(reader2["tara"]) ? decimal.Zero : ((decimal)reader2["tara"]),
                                    IdEmpresa = (int)reader2["IdEmpresa"]
                                };
                                OperationResult result = new OperationResult();
                                result = this.mapCliente((int)reader2["idCliente"]);
                                if (result.typeResult == ResultTypes.success)
                                {
                                    item.cliente = (Cliente)result.result;
                                }
                                else
                                {
                                    return new OperationResult
                                    {
                                        valor = result.valor,
                                        mensaje = result.mensaje,
                                        result = null
                                    };
                                }
                                result = this.mapOperador((int)reader2["idOperador"]);
                                if (result.typeResult == ResultTypes.success)
                                {
                                    item.operador = (Personal)result.result;
                                }
                                else
                                {
                                    return new OperationResult
                                    {
                                        valor = result.valor,
                                        mensaje = result.mensaje,
                                        result = null
                                    };
                                }
                                result = this.mapUnidadTransporte((string)reader2["idTractor"]);
                                if (result.typeResult == ResultTypes.success)
                                {
                                    item.tractor = (UnidadTransporte)result.result;
                                }
                                else
                                {
                                    return new OperationResult
                                    {
                                        valor = result.valor,
                                        mensaje = result.mensaje,
                                        result = null
                                    };
                                }
                                result = this.mapUnidadTransporte((string)reader2["idRemolque1"]);
                                if (result.typeResult == ResultTypes.success)
                                {
                                    item.remolque = (UnidadTransporte)result.result;
                                }
                                else
                                {
                                    return new OperationResult
                                    {
                                        valor = result.valor,
                                        mensaje = result.mensaje,
                                        result = null
                                    };
                                }
                                if (((string)reader2["idDolly"]) != "")
                                {
                                    result = this.mapUnidadTransporte((string)reader2["idDolly"]);
                                    if (result.typeResult != ResultTypes.success)
                                    {
                                        return new OperationResult
                                        {
                                            valor = result.valor,
                                            mensaje = result.mensaje,
                                            result = null
                                        };
                                    }
                                    item.dolly = (UnidadTransporte)result.result;
                                }
                                else
                                {
                                    item.dolly = null;
                                }
                                if (((string)reader2["idRemolque2"]) != "")
                                {
                                    result = this.mapUnidadTransporte((string)reader2["idRemolque2"]);
                                    if (result.typeResult != ResultTypes.success)
                                    {
                                        return new OperationResult
                                        {
                                            valor = result.valor,
                                            mensaje = result.mensaje,
                                            result = null
                                        };
                                    }
                                    item.remolque2 = (UnidadTransporte)result.result;
                                }
                                else
                                {
                                    item.remolque2 = null;
                                }
                                item.listDetalles = new List<CartaPorte>();
                                if (item.IdEmpresa == 1)
                                {
                                    result = this.getDetallesCartaPorteById((int)reader2["NumGuiaId"], (int)reader2["idCliente"]);
                                    if (result.typeResult != ResultTypes.success)
                                    {
                                        return new OperationResult
                                        {
                                            valor = result.valor,
                                            mensaje = result.mensaje,
                                            result = null
                                        };
                                    }
                                    item.listDetalles = (List<CartaPorte>)result.result;
                                }
                                else
                                {
                                    result = this.getDetallesCartaPorteByIdAtlante((int)reader2["NumGuiaId"], (int)reader2["idCliente"]);
                                    if (result.typeResult == ResultTypes.success)
                                    {
                                        item.listDetalles = (List<CartaPorte>)result.result;
                                    }
                                    else
                                    {
                                        return new OperationResult
                                        {
                                            valor = result.valor,
                                            mensaje = result.mensaje,
                                            result = null
                                        };
                                    }
                                }
                                list.Add(item);
                            }
                        }
                        result2 = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = list
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result2 = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
            return result2;
        }

        internal OperationResult checarCartaPorte(ref Viaje viaje, int idUsuario)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_checarViaje";
                        command.Parameters.Add(new SqlParameter("@idUsuario", idUsuario));
                        DateTime now = DateTime.Now;
                        command.Parameters.Add(new SqlParameter("@fecha", now));
                        command.Parameters.Add(new SqlParameter("@id", viaje.NumGuiaId));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        int num = command.ExecuteNonQuery();
                        if (((int)parameter.Value) == 0)
                        {
                            viaje.EstatusGuia = "CHECADO";
                            viaje.idUsuarioCheco = new int?(idUsuario);
                            viaje.fechaChecado = now;
                        }
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = viaje
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    result = null,
                    mensaje = exception.Message
                };
            }
            return result;
        }

        internal OperationResult ConsultarViajesActivos(int idCliente)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getTotalViajesActivos";
                        command.Parameters.Add(new SqlParameter("@idCliente", idCliente));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        List<object> list = new List<object>();
                        using (SqlDataReader reader2 = command.ExecuteReader())
                        {
                            while (reader2.Read())
                            {
                                list.Add((DateTime)reader2["fecha"]);
                                list.Add((int)reader2["countViajes"]);
                            }
                        }
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = list
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    result = null,
                    mensaje = exception.Message
                };
            }
            return result;
        }

        internal OperationResult ConsultarViajesFinalizados(int idCliente)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getTotalViajesFinalizados";
                        command.Parameters.Add(new SqlParameter("@idCliente", idCliente));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        List<object> list = new List<object>();
                        using (SqlDataReader reader2 = command.ExecuteReader())
                        {
                            while (reader2.Read())
                            {
                                list.Add((DateTime)reader2["fecha"]);
                                list.Add((int)reader2["countViajes"]);
                            }
                        }
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = list
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    result = null,
                    mensaje = exception.Message
                };
            }
            return result;
        }

        public OperationResult getCartaPorteByFecha(DateTime fechaInicio, DateTime fechaFin, string idCliente, int idEmpresa)
        {
            OperationResult result2;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getCartaPorteByFecha";
                        command.Parameters.Add(new SqlParameter("@idCliente", idCliente));
                        command.Parameters.Add(new SqlParameter("@fechaInicio", fechaInicio));
                        command.Parameters.Add(new SqlParameter("@fechaFin", fechaFin));
                        command.Parameters.Add(new SqlParameter("@idEmpresa", idEmpresa));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        OperationResult result = new OperationResult();
                        using (SqlDataReader reader2 = reader)
                        {
                            result = this.obtenerViajes(reader2);
                        }
                        result2 = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = result.result
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result2 = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
            return result2;
        }

        public OperationResult getCartaPorteByfiltros(DateTime fechaInicio, DateTime fechaFin, string idDestino, string idOperador, string idTractor, string estatus, string idcliente, int idEmpresa, string tipoViaje = "%")
        {
            OperationResult result2;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getCartaPorteByFiltros";
                        command.Parameters.Add(new SqlParameter("@fechaInicio", fechaInicio));
                        command.Parameters.Add(new SqlParameter("@fechaFin", fechaFin));
                        command.Parameters.Add(new SqlParameter("@idDestino", idDestino));
                        command.Parameters.Add(new SqlParameter("@idOperador", idOperador));
                        command.Parameters.Add(new SqlParameter("@idTractor", idTractor));
                        command.Parameters.Add(new SqlParameter("@estatus", estatus));
                        command.Parameters.Add(new SqlParameter("@idcliente", idcliente));
                        command.Parameters.Add(new SqlParameter("@idEmpresa", idEmpresa));
                        command.Parameters.Add(new SqlParameter("@tipoViaje", tipoViaje));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        OperationResult result = new OperationResult();
                        using (SqlDataReader reader2 = reader)
                        {
                            result = this.obtenerViajes_v2(reader2);
                        }
                        result2 = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = result.result
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result2 = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
            return result2;
        }

        public OperationResult getCartaPorteById(int clave)
        {
            OperationResult result2;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getCartaPorteById";
                        command.Parameters.Add(new SqlParameter("@clave", clave));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        OperationResult result = new OperationResult();
                        using (SqlDataReader reader2 = reader)
                        {
                            result = this.obtenerViajes(reader2);
                        }
                        result2 = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = result.result
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result2 = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
            return result2;
        }

        public OperationResult getCartaPorteById(int clave, string strConnection)
        {
            OperationResult result2;
            try
            {
                using (SqlConnection connection = new SqlConnection(strConnection))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getCartaPorteById";
                        command.Parameters.Add(new SqlParameter("@clave", clave));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        OperationResult result = new OperationResult();
                        using (SqlDataReader reader2 = reader)
                        {
                            result = this.obtenerViajes(reader2);
                        }
                        result2 = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = result.result
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result2 = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
            return result2;
        }

        public OperationResult getCartaPorteByIdCamion(string idCamion)
        {
            OperationResult result2;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getCartaPorteByIdCamion";
                        command.Parameters.Add(new SqlParameter("@idCamion", idCamion));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        OperationResult result = new OperationResult();
                        using (SqlDataReader reader2 = reader)
                        {
                            result = this.obtenerViajes(reader2);
                        }
                        result2 = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = result.result
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result2 = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
            return result2;
        }

        public OperationResult getCartaPortesFinalizados(DateTime fechaInicio, DateTime fechaFin, int idCliente = 0)
        {
            OperationResult result2;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getCartaPortesFinalizados";
                        command.Parameters.Add(new SqlParameter("@idCliente", idCliente));
                        command.Parameters.Add(new SqlParameter("@fechaInicio", fechaInicio));
                        command.Parameters.Add(new SqlParameter("@fechaFin", fechaFin));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        OperationResult result = new OperationResult();
                        using (SqlDataReader reader2 = reader)
                        {
                            result = this.obtenerViajes(reader2);
                        }
                        result2 = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = result.result
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result2 = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
            return result2;
        }

        public OperationResult getDetallesCartaPorteById(int idCartaPorte, int idCliente)
        {
            OperationResult result2;
            if (idCartaPorte == 0x2973)
            {
            }
            DateTime? nullable = null;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getDetallesCartaPorteById";
                        command.Parameters.Add(new SqlParameter("@id", idCartaPorte));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        if (idCartaPorte == 0x6dcd)
                        {
                        }
                        List<CartaPorte> list = new List<CartaPorte>();
                        using (SqlDataReader reader2 = reader)
                        {
                            while (reader2.Read())
                            {
                                CartaPorte item = new CartaPorte
                                {
                                    Consecutivo = (int)reader2["Consecutivo"],
                                    numGuiaId = (int)reader2["NumGuiaId"],
                                    serieGuia = (string)reader2["SerieGuia"],
                                    remision = (string)reader2["NoRemision"],
                                    volumenDescarga = (decimal)reader2["VolDescarga"],
                                    PorcIVA = (decimal)reader2["PorcIVA"],
                                    PorcRetencion = (decimal)reader2["PorcRetencion"],
                                    idTarea = (int)reader2["idTarea"],
                                    precio = (decimal)reader2["precio"],
                                    tara = DBNull.Value.Equals(reader2["tara"]) ? decimal.Zero : ((decimal)reader2["tara"]),
                                    pesoBruto = DBNull.Value.Equals(reader2["pesoBruto"]) ? decimal.Zero : ((decimal)reader2["pesoBruto"]),
                                    horaCaptura = DBNull.Value.Equals(reader2["horaCaptura"]) ? nullable : new DateTime?((DateTime)reader2["horaCaptura"]),
                                    horaLlegada = DBNull.Value.Equals(reader2["horaLlegada"]) ? nullable : new DateTime?((DateTime)reader2["horaLlegada"]),
                                    horaImpresion = DBNull.Value.Equals(reader2["horaImpresion"]) ? nullable : new DateTime?((DateTime)reader2["horaImpresion"]),
                                    observaciones = (string)reader2["observaciones"],
                                    remolque = (int)reader2["tolva"],
                                    idCargaGasolina = DBNull.Value.Equals(reader2["idCargaGasolina"]) ? 0 : ((int)reader2["idCargaGasolina"]),
                                    fCartaPorte = (int)reader2["fCartaPorte"],
                                    importeReal = (decimal)reader2["subTotal"],
                                    timbrado = Convert.ToBoolean(reader2["timbrado"]),
                                    ordenServicio = (int)reader2["ordenServicio"],
                                    ticket = (string)reader2["ticket"],
                                    viaje = (bool)reader2["viaje"],
                                    cobroXkm = Convert.ToBoolean(reader2["cobroXkm"]),
                                    precioFijo = (decimal)reader2["precioFijo"],
                                    km = (decimal)reader2["detkm"],
                                    vDescarga = (decimal)reader2["vDescarga"],
                                    isReEnvio = Convert.ToBoolean(reader2["isReEnvio"])
                                };
                                OperationResult result = new OperationResult();
                                item.producto = new mapComunes().mapProductos(reader2);
                                Zona zona = new mapComunes().mapDestino(reader2);
                                item.zonaSelect = zona;
                                decimal precio = item.precio;
                                list.Add(item);
                            }
                        }
                        result2 = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = list
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result2 = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
            return result2;
        }
        public OperationResult getDetallesCartaPorteByIdGlobal(int idCartaPorte, int idCliente)
        {
            OperationResult result2;
            if (idCartaPorte == 0x2973)
            {
            }
            DateTime? nullable = null;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getDetallesCartaPorteByIdGlobal";
                        command.Parameters.Add(new SqlParameter("@id", idCartaPorte));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        if (idCartaPorte == 0x6dcd)
                        {
                        }
                        List<CartaPorte> list = new List<CartaPorte>();
                        using (SqlDataReader reader2 = reader)
                        {
                            while (reader2.Read())
                            {
                                CartaPorte item = new CartaPorte
                                {
                                    Consecutivo = (int)reader2["Consecutivo"],
                                    numGuiaId = (int)reader2["NumGuiaId"],
                                    serieGuia = (string)reader2["SerieGuia"],
                                    remision = (string)reader2["NoRemision"],
                                    volumenDescarga = (decimal)reader2["VolDescarga"],
                                    PorcIVA = (decimal)reader2["PorcIVA"],
                                    PorcRetencion = (decimal)reader2["PorcRetencion"],
                                    idTarea = (int)reader2["idTarea"],
                                    precio = (decimal)reader2["precio"],
                                    tara = DBNull.Value.Equals(reader2["tara"]) ? decimal.Zero : ((decimal)reader2["tara"]),
                                    pesoBruto = DBNull.Value.Equals(reader2["pesoBruto"]) ? decimal.Zero : ((decimal)reader2["pesoBruto"]),
                                    horaCaptura = DBNull.Value.Equals(reader2["horaCaptura"]) ? nullable : new DateTime?((DateTime)reader2["horaCaptura"]),
                                    horaLlegada = DBNull.Value.Equals(reader2["horaLlegada"]) ? nullable : new DateTime?((DateTime)reader2["horaLlegada"]),
                                    horaImpresion = DBNull.Value.Equals(reader2["horaImpresion"]) ? nullable : new DateTime?((DateTime)reader2["horaImpresion"]),
                                    observaciones = (string)reader2["observaciones"],
                                    remolque = (int)reader2["tolva"],
                                    idCargaGasolina = DBNull.Value.Equals(reader2["idCargaGasolina"]) ? 0 : ((int)reader2["idCargaGasolina"]),
                                    fCartaPorte = (int)reader2["fCartaPorte"],
                                    importeReal = (decimal)reader2["subTotal"],
                                    timbrado = Convert.ToBoolean(reader2["timbrado"]),
                                    ordenServicio = (int)reader2["ordenServicio"],
                                    ticket = (string)reader2["ticket"],
                                    viaje = (bool)reader2["viaje"],
                                    cobroXkm = Convert.ToBoolean(reader2["cobroXkm"]),
                                    precioFijo = (decimal)reader2["precioFijo"],
                                    km = (decimal)reader2["detkm"],
                                    vDescarga = (decimal)reader2["vDescarga"],
                                    isReEnvio = Convert.ToBoolean(reader2["isReEnvio"]),
                                    unidadMedida = DBNull.Value.Equals(reader["unidadMedida"]) ? string.Empty : (string)reader["unidadMedida"]
                                };
                                OperationResult result = new OperationResult();
                                item.producto = new mapComunes().mapProductos(reader2);
                                Zona zona = new mapComunes().mapDestino(reader2);
                                item.zonaSelect = zona;
                                decimal precio = item.precio;

                                if (DBNull.Value.Equals(reader2["cliente_idCliente"]))
                                {
                                    item.cliente = null;
                                }
                                else
                                {
                                    Cliente cliente = new mapComunes().mapCliente(reader2);
                                    item.cliente = cliente;
                                }

                                list.Add(item);
                            }
                        }
                        result2 = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = list
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result2 = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
            return result2;
        }

        public OperationResult getDetallesCartaPorteById(int idCartaPorte, int idCliente, string strConnection)
        {
            OperationResult result2;
            if (idCartaPorte == 0x2973)
            {
            }
            DateTime? nullable = null;
            try
            {
                using (SqlConnection connection = new SqlConnection(strConnection))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getDetallesCartaPorteById";
                        command.Parameters.Add(new SqlParameter("@id", idCartaPorte));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        if (idCartaPorte == 0x7204)
                        {
                        }
                        List<CartaPorte> list = new List<CartaPorte>();
                        using (SqlDataReader reader2 = reader)
                        {
                            while (reader2.Read())
                            {
                                CartaPorte item = new CartaPorte
                                {
                                    Consecutivo = (int)reader2["Consecutivo"],
                                    numGuiaId = (int)reader2["NumGuiaId"],
                                    serieGuia = (string)reader2["SerieGuia"],
                                    remision = (string)reader2["NoRemision"],
                                    volumenDescarga = (decimal)reader2["VolDescarga"],
                                    PorcIVA = (decimal)reader2["PorcIVA"],
                                    PorcRetencion = (decimal)reader2["PorcRetencion"],
                                    idTarea = (int)reader2["idTarea"],
                                    precio = (decimal)reader2["precio"],
                                    tara = DBNull.Value.Equals(reader2["tara"]) ? decimal.Zero : ((decimal)reader2["tara"]),
                                    pesoBruto = DBNull.Value.Equals(reader2["pesoBruto"]) ? decimal.Zero : ((decimal)reader2["pesoBruto"]),
                                    horaCaptura = DBNull.Value.Equals(reader2["horaCaptura"]) ? nullable : new DateTime?((DateTime)reader2["horaCaptura"]),
                                    horaLlegada = DBNull.Value.Equals(reader2["horaLlegada"]) ? nullable : new DateTime?((DateTime)reader2["horaLlegada"]),
                                    horaImpresion = DBNull.Value.Equals(reader2["horaImpresion"]) ? nullable : new DateTime?((DateTime)reader2["horaImpresion"]),
                                    observaciones = (string)reader2["observaciones"],
                                    remolque = (int)reader2["tolva"],
                                    idCargaGasolina = DBNull.Value.Equals(reader2["idCargaGasolina"]) ? 0 : ((int)reader2["idCargaGasolina"]),
                                    fCartaPorte = (int)reader2["fCartaPorte"],
                                    importeReal = (decimal)reader2["subTotal"],
                                    timbrado = Convert.ToBoolean(reader2["timbrado"]),
                                    ordenServicio = (int)reader2["ordenServicio"],
                                    ticket = (string)reader2["ticket"],
                                    viaje = (bool)reader2["viaje"],
                                    cobroXkm = Convert.ToBoolean(reader2["cobroXkm"]),
                                    precioFijo = (decimal)reader2["precioFijo"],
                                    km = (decimal)reader2["detkm"],
                                    vDescarga = (decimal)reader2["vDescarga"],
                                    isReEnvio = Convert.ToBoolean(reader2["isReEnvio"])
                                };
                                OperationResult result = new OperationResult();
                                item.producto = new mapComunes().mapProductos(reader2);
                                Zona zona = new mapComunes().mapDestino(reader2);
                                item.zonaSelect = zona;
                                decimal precio = item.precio;
                                list.Add(item);
                            }
                        }
                        result2 = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = list
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result2 = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
            return result2;
        }

        public OperationResult getDetallesCartaPorteByIdAtlante(int idCartaPorte, int idCliente)
        {
            OperationResult result2;
            if (idCartaPorte == 0)
            {
            }
            DateTime? nullable = null;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getDetallesCartaPorteByIdAtlante";
                        command.Parameters.Add(new SqlParameter("@id", idCartaPorte));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        List<CartaPorte> list = new List<CartaPorte>();
                        using (SqlDataReader reader2 = reader)
                        {
                            while (reader2.Read())
                            {
                                CartaPorte item = new CartaPorte
                                {
                                    Consecutivo = (int)reader2["Consecutivo"],
                                    numGuiaId = (int)reader2["NumGuiaId"],
                                    serieGuia = (string)reader2["SerieGuia"],
                                    remision = (string)reader2["NoRemision"],
                                    volumenDescarga = (decimal)reader2["VolDescarga"],
                                    PorcIVA = (decimal)reader2["PorcIVA"],
                                    PorcRetencion = (decimal)reader2["PorcRetencion"],
                                    idTarea = (int)reader2["idTarea"],
                                    precio = (decimal)reader2["precio"],
                                    tara = DBNull.Value.Equals(reader2["tara"]) ? decimal.Zero : ((decimal)reader2["tara"]),
                                    pesoBruto = DBNull.Value.Equals(reader2["pesoBruto"]) ? decimal.Zero : ((decimal)reader2["pesoBruto"]),
                                    horaCaptura = DBNull.Value.Equals(reader2["horaCaptura"]) ? nullable : new DateTime?((DateTime)reader2["horaCaptura"]),
                                    horaLlegada = DBNull.Value.Equals(reader2["horaLlegada"]) ? nullable : new DateTime?((DateTime)reader2["horaLlegada"]),
                                    horaImpresion = DBNull.Value.Equals(reader2["horaImpresion"]) ? nullable : new DateTime?((DateTime)reader2["horaImpresion"]),
                                    observaciones = (string)reader2["observaciones"],
                                    idCargaGasolina = DBNull.Value.Equals(reader2["idCargaGasolina"]) ? 0 : ((int)reader2["idCargaGasolina"]),
                                    incluyeCaseta = Convert.ToBoolean(reader2["incluyeCaseta"]),
                                    precioCaseta = (decimal)reader2["precioCaseta"],
                                    fCartaPorte = (int)reader2["fCartaPorte"],
                                    importeReal = (decimal)reader2["subTotal"],
                                    timbrado = Convert.ToBoolean(reader2["timbrado"]),
                                    ordenServicio = (int)reader2["ordenServicio"],
                                    ticket = (string)reader2["ticket"],
                                    isReEnvio = Convert.ToBoolean(reader2["isReEnvio"])
                                };
                                OperationResult result = new OperationResult();
                                item.producto = new mapComunes().mapProductos(reader2);
                                item.origen = new mapComunes().mapOrigen(reader2);
                                Zona zona = new mapComunes().mapDestino(reader2);
                                item.zonaSelect = zona;
                                decimal precio = item.precio;
                                list.Add(item);
                            }
                        }
                        result2 = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = list
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result2 = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message + " " + idCartaPorte.ToString(),
                    result = null
                };
            }
            return result2;
        }

        public OperationResult getDetallesCartaPorteByIdAtlante(int idCartaPorte, int idCliente, string strConnection)
        {
            OperationResult result2;
            if (idCartaPorte == 0x7204)
            {
            }
            DateTime? nullable = null;
            try
            {
                using (SqlConnection connection = new SqlConnection(strConnection))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getDetallesCartaPorteByIdAtlante";
                        command.Parameters.Add(new SqlParameter("@id", idCartaPorte));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        List<CartaPorte> list = new List<CartaPorte>();
                        using (SqlDataReader reader2 = reader)
                        {
                            while (reader2.Read())
                            {
                                CartaPorte item = new CartaPorte
                                {
                                    Consecutivo = (int)reader2["Consecutivo"],
                                    numGuiaId = (int)reader2["NumGuiaId"],
                                    serieGuia = (string)reader2["SerieGuia"],
                                    remision = (string)reader2["NoRemision"],
                                    volumenDescarga = (decimal)reader2["VolDescarga"],
                                    PorcIVA = (decimal)reader2["PorcIVA"],
                                    PorcRetencion = (decimal)reader2["PorcRetencion"],
                                    idTarea = (int)reader2["idTarea"],
                                    precio = (decimal)reader2["precio"],
                                    tara = DBNull.Value.Equals(reader2["tara"]) ? decimal.Zero : ((decimal)reader2["tara"]),
                                    pesoBruto = DBNull.Value.Equals(reader2["pesoBruto"]) ? decimal.Zero : ((decimal)reader2["pesoBruto"]),
                                    horaCaptura = DBNull.Value.Equals(reader2["horaCaptura"]) ? nullable : new DateTime?((DateTime)reader2["horaCaptura"]),
                                    horaLlegada = DBNull.Value.Equals(reader2["horaLlegada"]) ? nullable : new DateTime?((DateTime)reader2["horaLlegada"]),
                                    horaImpresion = DBNull.Value.Equals(reader2["horaImpresion"]) ? nullable : new DateTime?((DateTime)reader2["horaImpresion"]),
                                    observaciones = (string)reader2["observaciones"],
                                    idCargaGasolina = DBNull.Value.Equals(reader2["idCargaGasolina"]) ? 0 : ((int)reader2["idCargaGasolina"]),
                                    incluyeCaseta = Convert.ToBoolean(reader2["incluyeCaseta"]),
                                    precioCaseta = (decimal)reader2["precioCaseta"],
                                    fCartaPorte = (int)reader2["fCartaPorte"],
                                    importeReal = (decimal)reader2["subTotal"],
                                    timbrado = Convert.ToBoolean(reader2["timbrado"]),
                                    ordenServicio = (int)reader2["ordenServicio"],
                                    ticket = (string)reader2["ticket"]
                                };
                                OperationResult result = new OperationResult();
                                item.producto = new mapComunes().mapProductos(reader2);
                                item.origen = new mapComunes().mapOrigen(reader2);
                                Zona zona = new mapComunes().mapDestino(reader2);
                                item.zonaSelect = zona;
                                decimal precio = item.precio;
                                list.Add(item);
                            }
                        }
                        result2 = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = list
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result2 = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
            return result2;
        }

        public OperationResult getDetallesCartaPorteByIdCargaGas(int idCargaGas)
        {
            OperationResult result2;
            if (idCargaGas == 0x362)
            {
            }
            DateTime? nullable = null;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getCartaPortesByIdCargaGas";
                        command.Parameters.Add(new SqlParameter("@idCargaGas", idCargaGas));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        List<CartaPorte> list = new List<CartaPorte>();
                        using (SqlDataReader reader2 = reader)
                        {
                            while (reader2.Read())
                            {
                                CartaPorte item = new CartaPorte
                                {
                                    Consecutivo = (int)reader2["Consecutivo"],
                                    numGuiaId = (int)reader2["NumGuiaId"],
                                    serieGuia = (string)reader2["SerieGuia"],
                                    remision = (string)reader2["NoRemision"],
                                    volumenDescarga = (decimal)reader2["VolDescarga"],
                                    PorcIVA = (decimal)reader2["PorcIVA"],
                                    PorcRetencion = (decimal)reader2["PorcRetencion"],
                                    idTarea = (int)reader2["idTarea"],
                                    precio = (decimal)reader2["precio"],
                                    tara = DBNull.Value.Equals(reader2["tara"]) ? decimal.Zero : ((decimal)reader2["tara"]),
                                    pesoBruto = DBNull.Value.Equals(reader2["pesoBruto"]) ? decimal.Zero : ((decimal)reader2["pesoBruto"]),
                                    horaCaptura = DBNull.Value.Equals(reader2["horaCaptura"]) ? nullable : new DateTime?((DateTime)reader2["horaCaptura"]),
                                    horaLlegada = DBNull.Value.Equals(reader2["horaLlegada"]) ? nullable : new DateTime?((DateTime)reader2["horaLlegada"]),
                                    horaImpresion = DBNull.Value.Equals(reader2["horaImpresion"]) ? nullable : new DateTime?((DateTime)reader2["horaImpresion"])
                                };
                                OperationResult result = new OperationResult();
                                result = this.mapProducto((int)reader2["IdProdTraf"], 1);
                                if (result.typeResult == ResultTypes.success)
                                {
                                    item.producto = (Producto)result.result;
                                }
                                else
                                {
                                    return new OperationResult
                                    {
                                        valor = result.valor,
                                        mensaje = result.mensaje,
                                        result = null
                                    };
                                }
                                result = this.mapZona((int)reader2["IdPlazaTraf"]);
                                if (result.typeResult == ResultTypes.success)
                                {
                                    item.zonaSelect = (Zona)result.result;
                                }
                                else
                                {
                                    return new OperationResult
                                    {
                                        valor = result.valor,
                                        mensaje = result.mensaje,
                                        result = null
                                    };
                                }
                                decimal precio = item.precio;
                                list.Add(item);
                            }
                        }
                        result2 = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = list
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result2 = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
            return result2;
        }

        public OperationResult getDetallesCartaPorteByIdCargaGasAtlante(int idCargaGas)
        {
            OperationResult result2;
            DateTime? nullable = null;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getDetallesCartaPorteByIdCargaGasAtlante";
                        command.Parameters.Add(new SqlParameter("@idCargaGas", idCargaGas));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        List<CartaPorte> list = new List<CartaPorte>();
                        using (SqlDataReader reader2 = reader)
                        {
                            while (reader2.Read())
                            {
                                CartaPorte item = new CartaPorte
                                {
                                    Consecutivo = (int)reader2["Consecutivo"],
                                    numGuiaId = (int)reader2["NumGuiaId"],
                                    serieGuia = (string)reader2["SerieGuia"],
                                    remision = (string)reader2["NoRemision"],
                                    volumenDescarga = (decimal)reader2["VolDescarga"],
                                    PorcIVA = (decimal)reader2["PorcIVA"],
                                    PorcRetencion = (decimal)reader2["PorcRetencion"],
                                    idTarea = (int)reader2["idTarea"],
                                    precio = (decimal)reader2["precio"],
                                    tara = DBNull.Value.Equals(reader2["tara"]) ? decimal.Zero : ((decimal)reader2["tara"]),
                                    pesoBruto = DBNull.Value.Equals(reader2["pesoBruto"]) ? decimal.Zero : ((decimal)reader2["pesoBruto"]),
                                    horaCaptura = DBNull.Value.Equals(reader2["horaCaptura"]) ? nullable : new DateTime?((DateTime)reader2["horaCaptura"]),
                                    horaLlegada = DBNull.Value.Equals(reader2["horaLlegada"]) ? nullable : new DateTime?((DateTime)reader2["horaLlegada"]),
                                    horaImpresion = DBNull.Value.Equals(reader2["horaImpresion"]) ? nullable : new DateTime?((DateTime)reader2["horaImpresion"]),
                                    observaciones = (string)reader2["observaciones"],
                                    incluyeCaseta = Convert.ToBoolean(reader2["incluyeCaseta"]),
                                    precioCaseta = (decimal)reader2["precioCaseta"]
                                };
                                OperationResult result = new OperationResult();
                                item.producto = new mapComunes().mapProductos(reader2);
                                item.origen = new mapComunes().mapOrigen(reader2);
                                Zona zona = new mapComunes().mapDestino(reader2);
                                item.zonaSelect = zona;
                                decimal precio = item.precio;
                                list.Add(item);
                            }
                        }
                        result2 = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = list
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result2 = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
            return result2;
        }

        public OperationResult getResumenCartaPortesByidCargaCombustible(int idCargaCombustible)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getResumenCartaPortesByidCargaCombustible", false))
                    {
                        command.Parameters.Add(new SqlParameter("@idCargaCombustible", idCargaCombustible));
                        connection.Open();
                        List<ResumenCartaPorte> list = new List<ResumenCartaPorte>();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                ResumenCartaPorte item = new mapComunes().mapResumenCartaPorte(reader);
                                if (item == null)
                                {
                                    return new OperationResult(ResultTypes.error, "Ocurrio un error al leer los resultados de la b\x00fasqueda", null);
                                }
                                list.Add(item);
                            }
                        }
                        result = new OperationResult(command, list);
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult(exception);
            }
            return result;
        }

        public OperationResult getTop1DetallesCartaPorteById(int idViaje)
        {
            OperationResult result2;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getTop1DetallesCartaPorteById", false))
                    {
                        command.Parameters.Add(new SqlParameter("@id", idViaje));
                        connection.Open();
                        List<CartaPorte> list = new List<CartaPorte>();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                DateTime? nullable;
                                CartaPorte item = new CartaPorte
                                {
                                    Consecutivo = (int)reader["Consecutivo"],
                                    numGuiaId = (int)reader["NumGuiaId"],
                                    serieGuia = (string)reader["SerieGuia"],
                                    remision = (string)reader["NoRemision"],
                                    volumenDescarga = (decimal)reader["VolDescarga"],
                                    PorcIVA = (decimal)reader["PorcIVA"],
                                    PorcRetencion = (decimal)reader["PorcRetencion"],
                                    idTarea = (int)reader["idTarea"],
                                    precio = (decimal)reader["precio"],
                                    tara = DBNull.Value.Equals(reader["tara"]) ? decimal.Zero : ((decimal)reader["tara"]),
                                    pesoBruto = DBNull.Value.Equals(reader["pesoBruto"]) ? decimal.Zero : ((decimal)reader["pesoBruto"]),
                                    horaCaptura = DBNull.Value.Equals(reader["horaCaptura"]) ? ((DateTime?)(nullable = null)) : ((DateTime?)reader["horaCaptura"]),
                                    horaLlegada = DBNull.Value.Equals(reader["horaLlegada"]) ? ((DateTime?)(nullable = null)) : ((DateTime?)reader["horaLlegada"]),
                                    horaImpresion = DBNull.Value.Equals(reader["horaImpresion"]) ? ((DateTime?)(nullable = null)) : ((DateTime?)reader["horaImpresion"]),
                                    observaciones = (string)reader["observaciones"],
                                    remolque = (int)reader["tolva"],
                                    idCargaGasolina = DBNull.Value.Equals(reader["idCargaGasolina"]) ? 0 : ((int)reader["idCargaGasolina"]),
                                    fCartaPorte = (int)reader["fCartaPorte"],
                                    importeReal = (decimal)reader["subTotal"]
                                };
                                OperationResult result = new OperationResult();
                                item.producto = new mapComunes().mapProductos(reader);
                                Zona zona = new mapComunes().mapDestino(reader);
                                item.zonaSelect = zona;
                                decimal precio = item.precio;
                                list.Add(item);
                            }
                        }
                        result2 = new OperationResult(command, list);
                    }
                }
            }
            catch (Exception exception)
            {
                result2 = new OperationResult(exception);
            }
            return result2;
        }

        public OperationResult getUltimoViajeByIdTractor(string idTractor)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getUltimoViajeByIdTractor", false))
                    {
                        command.Parameters.Add(new SqlParameter("@idTractor", idTractor));
                        connection.Open();
                        int num = 0;
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                num = (int)reader["NumGuiaId"];
                            }
                        }
                        result = new OperationResult(command, num);
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
            return result;
        }

        public OperationResult getValesCargaByIdViaje(Viaje viaje)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getValesCargaByIdViaje";
                        command.Parameters.Add(new SqlParameter("@idViaje", viaje.NumGuiaId));
                        command.Parameters.Add(new SqlParameter("@idCliente", viaje.cliente.clave));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        List<ValeCarga> list = new List<ValeCarga>();
                        connection.Open();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                ValeCarga item = new ValeCarga
                                {
                                    idValeCarga = (int)reader["idValeCarga"],
                                    remision = (string)reader["noRemision"],
                                    numGuiaId = (int)reader["numGuiaId"],
                                    producto = (((int)reader["idProducto"]) == 0) ? null : new mapComunes().mapProductos(reader),
                                    origen = (((int)reader["idOrigen"]) == 0) ? null : new mapComunes().mapOrigen(reader),
                                    destino = (((int)reader["idDestino"]) == 0) ? null : new mapComunes().mapDestino(reader),
                                    cartaPorte = (int)reader["cartaPorte"],
                                    volDescarga = (decimal)reader["volDescarga"],
                                    precio = DBNull.Value.Equals(reader["precio"]) ? decimal.Zero : ((decimal)reader["precio"]),
                                    cliente = new mapComunes().mapCliente(reader),
                                    vDescargado = (decimal)reader["vDescarga"],
                                    fechaCarga = DBNull.Value.Equals(reader["fechaCarga"]) ? null : (DateTime?)reader["fechaCarga"],
                                    fechaDescarga = DBNull.Value.Equals(reader["fechaDescarga"]) ? null : (DateTime?)reader["fechaDescarga"]
                                };
                                list.Add(item);
                            }
                        }
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = list
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            return result;
        }

        public OperationResult getViajeByEventoCSI(DateTime fecha, string idTractor)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getViajeByEventoCSI", false))
                    {
                        command.Parameters.Add(new SqlParameter("@fecha", fecha));
                        command.Parameters.Add(new SqlParameter("@idTractor", idTractor));
                        connection.Open();
                        int num = 0;
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                num = (int)reader["folio"];
                            }
                        }
                        result = new OperationResult(command, num);
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult(exception);
            }
            return result;
        }

        public OperationResult getViajeByEventoCSI(ref List<ParadaEquipo> listaParadas)
        {

            try
            {
                string xml = GenerateXML.generarListaParadas(listaParadas);
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_ligaPNAViaje", false))
                    {
                        command.Parameters.Add(new SqlParameter("@xml", xml));

                        List<ParadaEquipo> lista = new List<ParadaEquipo>();
                        connection.Open();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                lista.Add(new ParadaEquipo
                                {
                                    idParadaEquipo = (int)reader["id"],
                                    folioViaje = (int)reader["noViaje"]
                                });
                            }
                        }

                        foreach (var parada in listaParadas)
                        {
                            ParadaEquipo par = lista.Find(s => s.idParadaEquipo == parada.idParadaEquipo);
                            if (par != null)
                            {
                                parada.folioViaje = par.folioViaje;
                            }
                        }

                        return new OperationResult(command, lista);
                    }
                }
            }
            catch (Exception exception)
            {
                return new OperationResult(exception);
            }
        }

        public OperationResult getViajesActivosByIdTractor(string idTractor)
        {
            OperationResult result2;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getViajesActivosByIdTractor";
                        command.Parameters.Add(new SqlParameter("@idTractor", idTractor));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        OperationResult result = new OperationResult();
                        using (SqlDataReader reader2 = reader)
                        {
                            result = this.obtenerViajes_v2(reader2);
                        }
                        result2 = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = result.result
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result2 = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
            return result2;
        }

        public OperationResult getViajesOperador(int idOperador, DateTime fechaInicio, DateTime fechaFin)
        {
            OperationResult result3;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getViajesOperador", false))
                    {
                        command.Parameters.Add(new SqlParameter("@idOperador", idOperador));
                        command.Parameters.Add(new SqlParameter("@fechaInicio", fechaInicio));
                        command.Parameters.Add(new SqlParameter("@fechaFin", fechaFin));
                        connection.Open();
                        List<Viaje> list = new List<Viaje>();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                OperationResult result = new mapComunes().mapViajesV2(reader);
                                if (result.typeResult != ResultTypes.success)
                                {
                                    return result;
                                }
                                Viaje item = result.result as Viaje;
                                OperationResult result2 = this.getTop1DetallesCartaPorteById(item.NumGuiaId);
                                if (result2.typeResult == ResultTypes.success)
                                {
                                    item.listDetalles = result2.result as List<CartaPorte>;
                                }
                                else
                                {
                                    return result2;
                                }
                                list.Add(item);
                            }
                        }
                        result3 = new OperationResult(command, list);
                    }
                }
            }
            catch (Exception exception)
            {
                result3 = new OperationResult(exception);
            }
            return result3;
        }

        public OperationResult getViajesParaLiquidar(DateTime fechaInicio, DateTime fechaFin, int idOperador)
        {
            OperationResult result2;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getViajesLiquidacion", false))
                    {
                        command.Parameters.Add(new SqlParameter("@fechaInicio", fechaInicio));
                        command.Parameters.Add(new SqlParameter("@fechaFin", fechaFin));
                        command.Parameters.Add(new SqlParameter("@idOperador", idOperador));
                        List<Viaje> list = new List<Viaje>();
                        connection.Open();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                OperationResult result = this.obtenerViajes_v2(reader);
                                if (result.typeResult > ResultTypes.success)
                                {
                                    return result;
                                }
                                list.Add(result.result as Viaje);
                            }
                        }
                        result2 = new OperationResult(command, list);
                    }
                }
            }
            catch (Exception exception)
            {
                result2 = new OperationResult(exception);
            }
            return result2;
        }

        internal OperationResult getViajesParaTimbrar()
        {
            OperationResult result2;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getViajesParaTimbrar";
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        OperationResult result = new OperationResult();
                        using (SqlDataReader reader2 = reader)
                        {
                            result = this.obtenerViajes(reader2);
                        }
                        result2 = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = result.result
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result2 = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
            return result2;
        }

        private OperationResult mapCliente(int idCliente)
        {
            try
            {
                OperationResult result = new ClienteSvc().getClientesALLorById(0, idCliente, false);
                if (result.typeResult == ResultTypes.error)
                {
                    return new OperationResult
                    {
                        result = null,
                        valor = 1,
                        mensaje = result.mensaje
                    };
                }
                if (result.typeResult == ResultTypes.recordNotFound)
                {
                    return new OperationResult
                    {
                        result = null,
                        valor = 3,
                        mensaje = result.mensaje
                    };
                }
                if (result.typeResult == ResultTypes.success)
                {
                    List<Cliente> list = (List<Cliente>)result.result;
                    return new OperationResult
                    {
                        result = list[0],
                        valor = 0,
                        mensaje = result.mensaje
                    };
                }
            }
            catch (Exception exception)
            {
                return new OperationResult
                {
                    result = null,
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            return new OperationResult();
        }

        private OperationResult mapOperador(int idOperador)
        {
            try
            {
                OperationResult result = new OperadorSvc().getOperadoresALLorById(idOperador, 0);
                if (result.typeResult == ResultTypes.error)
                {
                    return new OperationResult
                    {
                        result = null,
                        valor = 1,
                        mensaje = result.mensaje
                    };
                }
                if (result.typeResult == ResultTypes.recordNotFound)
                {
                    return new OperationResult
                    {
                        result = null,
                        valor = 0,
                        mensaje = result.mensaje
                    };
                }
                if (result.typeResult == ResultTypes.success)
                {
                    List<Personal> list = (List<Personal>)result.result;
                    return new OperationResult
                    {
                        result = list[0],
                        valor = 0,
                        mensaje = result.mensaje
                    };
                }
            }
            catch (Exception exception)
            {
                return new OperationResult
                {
                    result = null,
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            return new OperationResult();
        }

        private OperationResult mapProducto(int idProducto, int idCliente)
        {
            try
            {
                OperationResult result = new ProductoSvc().getALLProductosByIdORidFraccion(idCliente, idProducto);
                if (result.typeResult == ResultTypes.error)
                {
                    return new OperationResult
                    {
                        result = null,
                        valor = 1,
                        mensaje = result.mensaje
                    };
                }
                if (result.typeResult == ResultTypes.recordNotFound)
                {
                    return new OperationResult
                    {
                        result = null,
                        valor = 0,
                        mensaje = result.mensaje
                    };
                }
                if (result.typeResult == ResultTypes.success)
                {
                    List<Producto> list = (List<Producto>)result.result;
                    return new OperationResult
                    {
                        result = list[0],
                        valor = 0,
                        mensaje = result.mensaje
                    };
                }
            }
            catch (Exception exception)
            {
                return new OperationResult
                {
                    result = null,
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            return new OperationResult();
        }

        private OperationResult mapUnidadTransporte(string id)
        {
            try
            {
                OperationResult result = new UnidadTransporteSvc().getUnidadesALLorById(0, id);
                if (result.typeResult == ResultTypes.error)
                {
                    return new OperationResult
                    {
                        result = null,
                        valor = 1,
                        mensaje = result.mensaje
                    };
                }
                if (result.typeResult == ResultTypes.recordNotFound)
                {
                    return new OperationResult
                    {
                        result = null,
                        valor = 3,
                        mensaje = result.mensaje
                    };
                }
                if (result.typeResult == ResultTypes.success)
                {
                    List<UnidadTransporte> list = (List<UnidadTransporte>)result.result;
                    return new OperationResult
                    {
                        result = list[0],
                        valor = 0,
                        mensaje = result.mensaje
                    };
                }
            }
            catch (Exception exception)
            {
                return new OperationResult
                {
                    result = null,
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            return new OperationResult();
        }

        private OperationResult mapZona(int idZona)
        {
            try
            {
                OperationResult result = new ZonasSvc().getZonasALLorById(idZona, "");
                if (result.typeResult == ResultTypes.error)
                {
                    return new OperationResult
                    {
                        result = null,
                        valor = 1,
                        mensaje = result.mensaje
                    };
                }
                if (result.typeResult == ResultTypes.recordNotFound)
                {
                    return new OperationResult
                    {
                        result = null,
                        valor = 0,
                        mensaje = result.mensaje
                    };
                }
                if (result.typeResult == ResultTypes.success)
                {
                    List<Zona> list = (List<Zona>)result.result;
                    return new OperationResult
                    {
                        result = list[0],
                        valor = 0,
                        mensaje = result.mensaje
                    };
                }
            }
            catch (Exception exception)
            {
                return new OperationResult
                {
                    result = null,
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            return new OperationResult();
        }

        private OperationResult obtenerViajes(SqlDataReader sqlReader)
        {
            try
            {
                List<Viaje> list = new List<Viaje>();
                DateTime? nullable = null;
                while (sqlReader.Read())
                {
                    if (69712 == ((int)sqlReader["NumGuiaId"]))
                    {
                    }
                    Viaje viaje = new Viaje
                    {
                        NumGuiaId = (int)sqlReader["NumGuiaId"],
                        SerieGuia = (string)sqlReader["SerieGuia"],
                        NumViaje = (int)sqlReader["NumViaje"],
                        FechaHoraViaje = (DateTime)sqlReader["FechaHoraViaje"],
                        EstatusGuia = (string)sqlReader["EstatusGuia"],
                        FechaHoraFin = DBNull.Value.Equals(sqlReader["FechaHoraFin"]) ? nullable : new DateTime?((DateTime)sqlReader["FechaHoraFin"]),
                        tipoViaje = Convert.ToChar(sqlReader["tipoViaje"]),
                        tara = DBNull.Value.Equals(sqlReader["tara"]) ? decimal.Zero : ((decimal)sqlReader["tara"]),
                        IdEmpresa = (int)sqlReader["idEmpresa"],
                        km = DBNull.Value.Equals(sqlReader["km"]) ? decimal.Zero : ((decimal)sqlReader["km"]),
                        fechaBachoco = DBNull.Value.Equals(sqlReader["fechaBachoco"]) ? nullable : new DateTime?((DateTime)sqlReader["fechaBachoco"]),
                        isExterno = (bool)sqlReader["externo"],
                        tara2 = DBNull.Value.Equals(sqlReader["tara2"]) ? decimal.Zero : ((decimal)sqlReader["tara2"]),
                        viajeFalso = Convert.ToBoolean(sqlReader["vFalso"]),
                        cobroXKm = DBNull.Value.Equals(sqlReader["cobroXkm"]) ? false : Convert.ToBoolean(sqlReader["cobroXkm"]),
                        noAjustar = Convert.ToBoolean(sqlReader["noAjustar"]),
                        taller = (bool)sqlReader["taller"],
                        tallerExterno = (bool)sqlReader["tallerExterno"],
                        noViaje = (int)sqlReader["noviajeOperador"],
                        editoNoViaje = (bool)sqlReader["editoNoViaje"],
                        FechaInicioCarga = DBNull.Value.Equals(sqlReader["fechaInicioCarga"]) ? null : (DateTime?)sqlReader["fechaInicioCarga"],
                        FechaPrograma = DBNull.Value.Equals(sqlReader["fechaPrograma"]) ? null : (DateTime?)sqlReader["fechaPrograma"],
                        obsViajeFalso = (string)sqlReader["obsViajeFalso"],
                        estatusProducto = (EstatusProducto)sqlReader["estatusProducto"],
                        motivoViajeFalso = (string)sqlReader["motivoViajeFalso"],
                        idMotivoViajeFalso = DBNull.Value.Equals(sqlReader["idMotivoViajeFalso"]) ? null : (int?)sqlReader["idMotivoViajeFalso"],
                        imputable = DBNull.Value.Equals(sqlReader["imputable"]) ? null : (int?)sqlReader["imputable"],
                        UserViaje = (int)sqlReader["UserViaje"],
                        pagoOperadores = (decimal)sqlReader["vPagoOperador"],
                        pagoCasetas = (decimal)sqlReader["vPagoCasetas"],
                        FechaSalida = DBNull.Value.Equals(sqlReader["fechaSalidaPlantaCSI"]) ? null : (DateTime?)sqlReader["fechaSalidaPlantaCSI"]
                    };
                    if (DBNull.Value.Equals(sqlReader["cliente_idCliente"]))
                    {
                        viaje.cliente = null;
                    }
                    else
                    {
                        viaje.cliente = new mapComunes().mapCliente(sqlReader);
                    }

                    viaje.operador = new mapComunes().mapPersonal(sqlReader, "");
                    if (!DBNull.Value.Equals(sqlReader["zonaO_idZonaOperativa"]))
                    {
                        ZonaOperativa zona = new mapComunes().mapZonaOperativa(sqlReader);
                        viaje.zonaOperativa = zona;
                    }
                    if (!DBNull.Value.Equals(sqlReader["Aux_persona_idPersonal"]))
                    {
                        viaje.operadorCapacitacion = new mapComunes().mapPersonal(sqlReader, "Aux_");
                    }
                    if (!DBNull.Value.Equals(sqlReader["Ayu_persona_idPersonal"]))
                    {
                        viaje.ayudante = new mapComunes().mapPersonal(sqlReader, "Ayu_");
                    }
                    if (!DBNull.Value.Equals(sqlReader["Fin_persona_idPersonal"]))
                    {
                        viaje.OperadorFinViaje = new mapComunes().mapPersonal(sqlReader, "Fin_");
                    }
                    UnidadTransporte tractor = new UnidadTransporte
                    {
                        clave = (string)sqlReader["idTractor"]
                    };
                    viaje.tractor = tractor;


                    if (((string)sqlReader["idRemolque1"]) == "")
                    {
                        viaje.remolque = null;
                    }
                    else
                    {
                        UnidadTransporte rem1 = new UnidadTransporte
                        {
                            clave = (string)sqlReader["idRemolque1"]
                        };
                        viaje.remolque = rem1;
                    }

                    //UnidadTransporte rem1 = new UnidadTransporte
                    //{
                    //    clave = (string)sqlReader["idRemolque1"]
                    //};
                    //item.remolque = rem1;


                    if (((string)sqlReader["idDolly"]) == "")
                    {
                        viaje.dolly = null;
                    }
                    else
                    {
                        UnidadTransporte dolly = new UnidadTransporte
                        {
                            clave = (string)sqlReader["idDolly"]
                        };
                        viaje.dolly = dolly;
                    }
                    if (((string)sqlReader["idRemolque2"]) == "")
                    {
                        viaje.remolque2 = null;
                    }
                    else
                    {
                        UnidadTransporte rem2 = new UnidadTransporte
                        {
                            clave = (string)sqlReader["idRemolque2"]
                        };
                        viaje.remolque2 = rem2;
                    }

                    viaje.listDetalles = new List<CartaPorte>();

                    var respAnt = new ZonaServices().getAnticiposByNumViaje(viaje.NumGuiaId);
                    if (respAnt.typeResult == ResultTypes.success)
                    {
                        viaje.listaAnticipos = respAnt.result as List<Anticipo>;
                    }
                    else
                    {
                        viaje.listaAnticipos = new List<Anticipo>();
                    }

                    list.Add(viaje);
                }
                return new OperationResult
                {
                    valor = 0,
                    mensaje = "Registros Encontrados",
                    result = list
                };
            }
            catch (Exception exception)
            {
                return new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
        }

        private OperationResult obtenerViajes_v2(SqlDataReader sqlReader)
        {
            int num = 0;
            try
            {
                List<Viaje> list = new List<Viaje>();
                while (sqlReader.Read())
                {
                    num = (int)sqlReader["NumGuiaId"];
                    if (0x13f2 == ((int)sqlReader["NumGuiaId"]))
                    {
                    }
                    OperationResult result = new mapComunes().mapViajesV2(sqlReader);
                    if (result.typeResult == ResultTypes.success)
                    {
                        list.Add(result.result as Viaje);
                    }
                    else
                    {
                        result.mensaje = result.mensaje + Environment.NewLine + $"Viaje {Convert.ToString(sqlReader["NumGuiaId"])}";
                        return result;
                    }
                }
                return new OperationResult
                {
                    valor = 0,
                    mensaje = "Registros Encontrados",
                    result = list
                };
            }
            catch (Exception exception)
            {
                return new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
        }

        public OperationResult saveAjusteViaje(ref Viaje viaje, int? idOperadorFinaliza = new int?())
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    connection.Open();
                    using (SqlTransaction transaction = connection.BeginTransaction())
                    {
                        using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_saveAjusteViajes", false))
                        {
                            int? nullable;
                            command.Transaction = transaction;
                            command.Parameters.Add(new SqlParameter("@NumGuiaId", viaje.NumGuiaId));
                            command.Parameters.Add(new SqlParameter("@idCliente", viaje.cliente.clave));
                            command.Parameters.Add(new SqlParameter("@IdEmpresa", viaje.IdEmpresa));
                            command.Parameters.Add(new SqlParameter("@FechaHoraViaje", viaje.FechaHoraViaje));
                            command.Parameters.Add(new SqlParameter("@FechaHoraFin", viaje.FechaHoraFin));
                            command.Parameters.Add(new SqlParameter("@idOperador", viaje.operador.clave));
                            command.Parameters.Add(new SqlParameter("@idOperador2", (viaje.operadorCapacitacion == null) ? ((int?)(nullable = null)) : new int?(viaje.operadorCapacitacion.clave)));
                            command.Parameters.Add(new SqlParameter("@idTractor", viaje.tractor.clave));
                            command.Parameters.Add(new SqlParameter("@idRemolque1", viaje.remolque.clave));
                            command.Parameters.Add(new SqlParameter("@idDolly", viaje.dolly?.clave));
                            command.Parameters.Add(new SqlParameter("@idRemolque2", viaje.remolque2?.clave));
                            command.Parameters.Add(new SqlParameter("@EstatusGuia", viaje.EstatusGuia));
                            command.Parameters.Add(new SqlParameter("@usuarioCambia", viaje.usuarioCambia));
                            command.Parameters.Add(new SqlParameter("@idOperadorFin", idOperadorFinaliza));
                            command.Parameters.Add(new SqlParameter("@viajeFalso", viaje.viajeFalso));
                            command.Parameters.Add(new SqlParameter("@imputable", viaje.imputable));
                            command.Parameters.Add(new SqlParameter("@editoNoViaje", viaje.editoNoViaje));
                            command.Parameters.Add(new SqlParameter("@noViaje", viaje.noViaje));
                            command.Parameters.Add(new SqlParameter("@idAyudante", (viaje.ayudante == null) ? ((int?)(nullable = null)) : new int?(viaje.ayudante.clave)));
                            command.Parameters.Add(new SqlParameter("@fechaInicioCarga", viaje.FechaInicioCarga));
                            command.Parameters.Add(new SqlParameter("@fechaPrograma", viaje.FechaPrograma));
                            command.Parameters.Add(new SqlParameter("@obsViajeFalso", viaje.obsViajeFalso));
                            command.Parameters.Add(new SqlParameter("@estatusProducto", (int)viaje.estatusProducto));
                            command.Parameters.Add(new SqlParameter("@idMotivo", viaje.idMotivoViajeFalso));
                            command.Parameters.Add(new SqlParameter("@motivoViajeFalso", viaje.motivoViajeFalso));
                            //command.Parameters.Add(new SqlParameter("@noOperadorViaje", viaje.noViaje));
                            int num = command.ExecuteNonQuery();
                            if (((int)command.Parameters["@valor"].Value) == 0)
                            {
                                command.CommandText = "sp_saveAjusteDetallesViajes";
                                foreach (CartaPorte porte in viaje.listDetalles)
                                {
                                    command.Parameters.Clear();
                                    foreach (SqlParameter parameter in CrearSqlCommand.getPatametros(false))
                                    {
                                        command.Parameters.Add(parameter);
                                    }
                                    command.Parameters.Add(new SqlParameter("@consecutivo", porte.Consecutivo));
                                    command.Parameters.Add(new SqlParameter("@remision", porte.remision));
                                    command.Parameters.Add(new SqlParameter("@VolDescarga", porte.volumenDescarga));
                                    command.Parameters.Add(new SqlParameter("@precio", porte.precio));
                                    command.Parameters.Add(new SqlParameter("@PorcIVA", porte.PorcIVA));
                                    command.Parameters.Add(new SqlParameter("@PorcRetencion", porte.PorcRetencion));
                                    command.Parameters.Add(new SqlParameter("@idProducto", porte.producto.clave));
                                    command.Parameters.Add(new SqlParameter("@idOrigen", (porte.origen == null) ? ((int?)(nullable = null)) : new int?(porte.origen.idOrigen)));
                                    command.Parameters.Add(new SqlParameter("@idDestino", porte.zonaSelect.clave));
                                    command.Parameters.Add(new SqlParameter("@importe", porte.importeReal));
                                    command.Parameters.Add(new SqlParameter("@impIva", porte.ImporIVA));
                                    command.Parameters.Add(new SqlParameter("@impRetencion", porte.ImpRetencion));
                                    command.Parameters.Add(new SqlParameter("@usuarioAjusta", porte.usuarioAjuste));
                                    command.Parameters.Add(new SqlParameter("@viaje", porte.viaje));
                                    command.Parameters.Add(new SqlParameter("@pagoOperador", porte.precioOperador));
                                    command.Parameters.Add(new SqlParameter("@precioCaseta", porte.precioCaseta));
                                    command.Parameters.Add(new SqlParameter("@incluyeCaseta", porte.incluyeCaseta));
                                    command.Parameters.Add(new SqlParameter("@baja", porte.baja));
                                    command.Parameters.Add(new SqlParameter("@viajeFalso", porte.viajeFalso));
                                    command.Parameters.Add(new SqlParameter("@vDescarga", porte.vDescarga));
                                    command.Parameters.Add(new SqlParameter("@km", porte.km));
                                    command.Parameters.Add(new SqlParameter("@precioFijo", porte.precioFijo));
                                    command.Parameters.Add(new SqlParameter("@isReEnvio", porte.isReEnvio));
                                    command.Parameters.Add(new SqlParameter("@idVale", porte.idCargaGasolina));
                                    command.ExecuteNonQuery();
                                    if (((int)command.Parameters["@valor"].Value) != 0)
                                    {
                                        transaction.Rollback();
                                        return new OperationResult(command, null);
                                    }
                                }
                            }
                            else
                            {
                                transaction.Rollback();
                            }
                            transaction.Commit();
                            result = new OperationResult(command, null);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
            return result;
        }

        public OperationResult saveCartaPorte(Viaje cartaPorte, ref int ordenTrabajo, bool omitirRegraRemisiones = false, bool genSalida = false, bool genOrdenCom = false)
        {
            OperationResult result;
            try
            {
                DateTime? nullable = null;
                string str = GenerateXML.generateListDetalles(cartaPorte.listDetalles);
                if (str == null)
                {
                    result = new OperationResult
                    {
                        mensaje = "Error al contruir el XML",
                        valor = 1
                    };
                }
                else
                {
                    using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                    {
                        using (SqlCommand command = connection.CreateCommand())
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.CommandText = "sp_saveCartaPorte";
                            command.Parameters.Add(new SqlParameter("@NumGuiaId", cartaPorte.NumGuiaId));
                            command.Parameters.Add(new SqlParameter("@SerieGuia", cartaPorte.SerieGuia));
                            command.Parameters.Add(new SqlParameter("@NumViaje", cartaPorte.NumViaje));
                            command.Parameters.Add(new SqlParameter("@idCliente", cartaPorte.cliente.clave));
                            command.Parameters.Add(new SqlParameter("@IdEmpresa", cartaPorte.IdEmpresa));
                            command.Parameters.Add(new SqlParameter("@FechaHoraViaje", cartaPorte.FechaHoraViaje));
                            command.Parameters.Add(new SqlParameter("@idOperador", cartaPorte.operador.clave));
                            command.Parameters.Add(new SqlParameter("@idTractor", cartaPorte.tractor.clave));
                            command.Parameters.Add(new SqlParameter("@idRemolque1", cartaPorte.remolque.clave));
                            command.Parameters.Add(new SqlParameter("@idDolly", cartaPorte.dolly?.clave));
                            command.Parameters.Add(new SqlParameter("@idRemolque2", cartaPorte.remolque2?.clave));
                            command.Parameters.Add(new SqlParameter("@EstatusGuia", cartaPorte.EstatusGuia));
                            command.Parameters.Add(new SqlParameter("@UserViaje", cartaPorte.UserViaje));
                            command.Parameters.Add(new SqlParameter("@tipoViaje", cartaPorte.tipoViaje));
                            command.Parameters.Add(new SqlParameter("@tara", cartaPorte.tara));
                            command.Parameters.Add(new SqlParameter("@tara2", cartaPorte.tara2));
                            command.Parameters.Add(new SqlParameter("@xml", str));
                            command.Parameters.Add(new SqlParameter("@fechaBachoco", cartaPorte.fechaBachoco));
                            command.Parameters.Add(new SqlParameter("@genSalida", genSalida));
                            command.Parameters.Add(new SqlParameter("@genOrdenCom", genOrdenCom));
                            command.Parameters.Add(new SqlParameter("@isExterno", cartaPorte.isExterno));
                            SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                            {
                                Direction = ParameterDirection.Output
                            };
                            SqlParameter parameter = command.Parameters.Add(parameter1);
                            SqlParameter parameter5 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                            {
                                Direction = ParameterDirection.Output
                            };
                            SqlParameter parameter2 = command.Parameters.Add(parameter5);
                            SqlParameter parameter6 = new SqlParameter("@identificador", SqlDbType.Int)
                            {
                                Direction = ParameterDirection.Output
                            };
                            SqlParameter parameter3 = command.Parameters.Add(parameter6);
                            SqlParameter parameter7 = new SqlParameter("@spOrdenTrabajo", SqlDbType.Int)
                            {
                                Direction = ParameterDirection.Output
                            };
                            SqlParameter parameter4 = command.Parameters.Add(parameter7);
                            connection.Open();
                            SqlDataReader reader = command.ExecuteReader();
                            Viaje viaje = new Viaje();
                            using (SqlDataReader reader2 = reader)
                            {
                                while (reader2.Read())
                                {
                                    viaje = new Viaje
                                    {
                                        NumGuiaId = (int)reader2["NumGuiaId"],
                                        SerieGuia = (string)reader2["SerieGuia"],
                                        NumViaje = (int)reader2["NumViaje"],
                                        FechaHoraViaje = (DateTime)reader2["FechaHoraViaje"],
                                        EstatusGuia = (string)reader2["EstatusGuia"],
                                        UserViaje = (int)reader2["UserViaje"],
                                        tara = (decimal)reader2["tara"],
                                        fechaBachoco = DBNull.Value.Equals(reader2["fechaBachoco"]) ? nullable : new DateTime?((DateTime)reader2["fechaBachoco"]),
                                        isExterno = (bool)reader2["externo"],
                                        tara2 = (decimal)reader2["tara2"]
                                    };
                                    OperationResult result2 = new OperationResult();
                                    result2 = this.mapCliente((int)reader2["idCliente"]);
                                    if (result2.typeResult == ResultTypes.success)
                                    {
                                        viaje.cliente = (Cliente)result2.result;
                                    }
                                    else
                                    {
                                        return new OperationResult
                                        {
                                            valor = result2.valor,
                                            mensaje = result2.mensaje,
                                            result = null
                                        };
                                    }
                                    result2 = this.mapOperador((int)reader2["idOperador"]);
                                    if (result2.typeResult == ResultTypes.success)
                                    {
                                        viaje.operador = (Personal)result2.result;
                                    }
                                    else
                                    {
                                        return new OperationResult
                                        {
                                            valor = result2.valor,
                                            mensaje = result2.mensaje,
                                            result = null
                                        };
                                    }
                                    result2 = this.mapUnidadTransporte((string)reader2["idTractor"]);
                                    if (result2.typeResult == ResultTypes.success)
                                    {
                                        viaje.tractor = (UnidadTransporte)result2.result;
                                    }
                                    else
                                    {
                                        return new OperationResult
                                        {
                                            valor = result2.valor,
                                            mensaje = result2.mensaje,
                                            result = null
                                        };
                                    }
                                    result2 = this.mapUnidadTransporte((string)reader2["idRemolque1"]);
                                    if (result2.typeResult == ResultTypes.success)
                                    {
                                        viaje.remolque = (UnidadTransporte)result2.result;
                                    }
                                    else
                                    {
                                        return new OperationResult
                                        {
                                            valor = result2.valor,
                                            mensaje = result2.mensaje,
                                            result = null
                                        };
                                    }
                                    if (((string)reader2["idDolly"]) != "")
                                    {
                                        result2 = this.mapUnidadTransporte((string)reader2["idDolly"]);
                                        if (result2.typeResult != ResultTypes.success)
                                        {
                                            return new OperationResult
                                            {
                                                valor = result2.valor,
                                                mensaje = result2.mensaje,
                                                result = null
                                            };
                                        }
                                        viaje.dolly = (UnidadTransporte)result2.result;
                                    }
                                    else
                                    {
                                        viaje.dolly = null;
                                    }
                                    if (((string)reader2["idRemolque2"]) != "")
                                    {
                                        result2 = this.mapUnidadTransporte((string)reader2["idRemolque2"]);
                                        if (result2.typeResult != ResultTypes.success)
                                        {
                                            return new OperationResult
                                            {
                                                valor = result2.valor,
                                                mensaje = result2.mensaje,
                                                result = null
                                            };
                                        }
                                        viaje.remolque2 = (UnidadTransporte)result2.result;
                                    }
                                    else
                                    {
                                        viaje.remolque2 = null;
                                    }
                                    viaje.listDetalles = new List<CartaPorte>();
                                    result2 = this.getDetallesCartaPorteById((int)reader2["NumGuiaId"], (int)reader2["idCliente"]);
                                    if (result2.typeResult == ResultTypes.recordNotFound)
                                    {
                                        viaje.listDetalles = new List<CartaPorte>();
                                    }
                                    else if (result2.typeResult == ResultTypes.success)
                                    {
                                        viaje.listDetalles = (List<CartaPorte>)result2.result;
                                    }
                                    else
                                    {
                                        return new OperationResult
                                        {
                                            valor = result2.valor,
                                            mensaje = result2.mensaje,
                                            result = null
                                        };
                                    }
                                    viaje.listDetalles = new List<CartaPorte>();
                                    if (cartaPorte.IdEmpresa == 1)
                                    {
                                        result2 = this.getDetallesCartaPorteById((int)reader2["NumGuiaId"], (int)reader2["idCliente"]);
                                    }
                                    else
                                    {
                                        result2 = this.getDetallesCartaPorteByIdAtlante((int)reader2["NumGuiaId"], (int)reader2["idCliente"]);
                                    }
                                    if (result2.typeResult == ResultTypes.recordNotFound)
                                    {
                                        viaje.listDetalles = new List<CartaPorte>();
                                    }
                                    else if (result2.typeResult == ResultTypes.success)
                                    {
                                        viaje.listDetalles = (List<CartaPorte>)result2.result;
                                    }
                                    else
                                    {
                                        return new OperationResult
                                        {
                                            valor = result2.valor,
                                            mensaje = result2.mensaje,
                                            result = null
                                        };
                                    }
                                }
                            }
                            ordenTrabajo = (int)parameter4.Value;
                            result = new OperationResult
                            {
                                valor = new int?((int)parameter.Value),
                                mensaje = (string)parameter2.Value,
                                result = viaje
                            };
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
            return result;
        }

        public OperationResult saveCartaPorte_V2(ref Viaje viaje, ref int ordenTrabajo, bool omitirRegraRemisiones = false, bool genSalida = false, bool genOrdenCom = false)
        {
            try
            {
                string str = GenerateXML.generateListDetalles(viaje.listDetalles);
                if (str == null)
                {
                    return new OperationResult
                    {
                        mensaje = "Error al contruir el XML",
                        valor = 1
                    };
                }
                else
                {
                    using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                    {
                        connection.Open();
                        using (SqlTransaction transaction = connection.BeginTransaction("save"))
                        {
                            using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_saveCartaPorteNew_V2")) // connection.CreateCommand())
                            {
                                command.Transaction = transaction;
                                command.Parameters.Add(new SqlParameter("@NumGuiaId", viaje.NumGuiaId));
                                command.Parameters.Add(new SqlParameter("@SerieGuia", viaje.SerieGuia));
                                command.Parameters.Add(new SqlParameter("@NumViaje", viaje.NumViaje));
                                command.Parameters.Add(new SqlParameter("@idCliente", viaje.cliente == null ? null : (int?)viaje.cliente.clave));
                                command.Parameters.Add(new SqlParameter("@IdEmpresa", viaje.IdEmpresa));
                                command.Parameters.Add(new SqlParameter("@FechaHoraViaje", viaje.FechaHoraViaje));
                                command.Parameters.Add(new SqlParameter("@idOperador", viaje.operador.clave));
                                command.Parameters.Add(new SqlParameter("@idOperador2", (viaje.operadorCapacitacion == null) ? null : (int?)(viaje.operadorCapacitacion.clave)));
                                command.Parameters.Add(new SqlParameter("@idTractor", viaje.tractor.clave));
                                command.Parameters.Add(new SqlParameter("@idRemolque1", viaje.remolque?.clave));
                                command.Parameters.Add(new SqlParameter("@idDolly", viaje.dolly?.clave));
                                command.Parameters.Add(new SqlParameter("@idRemolque2", viaje.remolque2?.clave));
                                command.Parameters.Add(new SqlParameter("@EstatusGuia", viaje.EstatusGuia));
                                command.Parameters.Add(new SqlParameter("@UserViaje", viaje.UserViaje));
                                command.Parameters.Add(new SqlParameter("@tipoViaje", viaje.tipoViaje));
                                command.Parameters.Add(new SqlParameter("@tara", viaje.tara));
                                command.Parameters.Add(new SqlParameter("@tara2", viaje.tara2));
                                command.Parameters.Add(new SqlParameter("@xml", str));
                                command.Parameters.Add(new SqlParameter("@fechaBachoco", viaje.fechaBachoco));
                                command.Parameters.Add(new SqlParameter("@genSalida", genSalida));
                                command.Parameters.Add(new SqlParameter("@genOrdenCom", genOrdenCom));
                                command.Parameters.Add(new SqlParameter("@isExterno", viaje.isExterno));
                                command.Parameters.Add(new SqlParameter("@cobroXkm", viaje.cobroXKm));
                                command.Parameters.Add(new SqlParameter("@FechaPrograma", viaje.FechaPrograma));
                                command.Parameters.Add(new SqlParameter("@taller", viaje.taller));
                                command.Parameters.Add(new SqlParameter("@tallerExterno", viaje.tallerExterno));
                                command.Parameters.Add(new SqlParameter("@idZonaOperativa", viaje.zonaOperativa == null ? null : (int?)viaje.zonaOperativa.idZonaOperativa));
                                command.Parameters.Add(new SqlParameter("@fechaInicioCarga", viaje.FechaInicioCarga));
                                command.Parameters.Add(new SqlParameter("@noViaje", viaje.noViaje));
                                command.Parameters.Add(new SqlParameter("@editoNoViaje", viaje.editoNoViaje));
                                command.Parameters.Add(new SqlParameter("@pagoOperadores", viaje.pagoOperadores));
                                command.Parameters.Add(new SqlParameter("@pagoCasetas", viaje.pagoCasetas));
                                command.Parameters.Add(new SqlParameter("@FechaHoraFin", viaje.FechaHoraFin));
                                command.Parameters.Add(new SqlParameter("@fechaSalidaPlanta", viaje.FechaSalida));
                                command.Parameters.Add(new SqlParameter("@UserFinCancela", viaje.UserFinViaje));


                                var idParam = command.Parameters.Add(new SqlParameter("@identificador", SqlDbType.Int) { Direction = ParameterDirection.Output });

                                var idOrdenTrabajo = command.Parameters.Add(new SqlParameter("@spOrdenTrabajo", SqlDbType.Int) { Direction = ParameterDirection.Output });

                                command.ExecuteNonQuery();
                                if (CrearSqlCommand.validarCorrecto(command))
                                {
                                    viaje.NumGuiaId = (int)idParam.Value;
                                    ordenTrabajo = (int)idOrdenTrabajo.Value;
                                    command.CommandText = "sp_saveDetallesCartaNew_v2";
                                    foreach (CartaPorte porte in viaje.listDetalles)
                                    {
                                        command.Parameters.Clear();
                                        foreach (var param in CrearSqlCommand.getPatametros())
                                        {
                                            command.Parameters.Add(param);
                                        }

                                        idParam = command.Parameters.Add(new SqlParameter("@identificador", SqlDbType.Int) { Direction = ParameterDirection.Output });
                                        command.Parameters.Add(new SqlParameter("@idTarea", porte.idTarea));
                                        command.Parameters.Add(new SqlParameter("@idCliente", viaje.cliente != null ? viaje.cliente.clave : porte.cliente.clave));
                                        command.Parameters.Add(new SqlParameter("@Consecutivo", porte.Consecutivo));
                                        command.Parameters.Add(new SqlParameter("@idViaje", viaje.NumGuiaId));
                                        command.Parameters.Add(new SqlParameter("@NoRemision", string.IsNullOrEmpty(porte.remision) ? string.Empty : porte.remision));
                                        command.Parameters.Add(new SqlParameter("@IdProdTraf", porte.producto.clave));
                                        command.Parameters.Add(new SqlParameter("@IdPlazaTraf", porte.zonaSelect.clave));
                                        command.Parameters.Add(new SqlParameter("@VolDescarga", porte.volumenDescarga));
                                        command.Parameters.Add(new SqlParameter("@precio", porte.precio));
                                        command.Parameters.Add(new SqlParameter("@PorcIVA", porte.PorcIVA));
                                        command.Parameters.Add(new SqlParameter("@ImpIVA", porte.ImporIVA));
                                        command.Parameters.Add(new SqlParameter("@PorcRetencion", porte.PorcRetencion));
                                        command.Parameters.Add(new SqlParameter("@ImpRetencion", porte.ImpRetencion));
                                        command.Parameters.Add(new SqlParameter("@subTotal", porte.importeReal));
                                        command.Parameters.Add(new SqlParameter("@fCartaPorte", SqlDbType.BigInt));
                                        command.Parameters.Add(new SqlParameter("@tara", porte.tara));
                                        command.Parameters.Add(new SqlParameter("@horaLlegada", porte.horaLlegada));
                                        command.Parameters.Add(new SqlParameter("@horaCaptura", porte.horaCaptura));
                                        command.Parameters.Add(new SqlParameter("@horaImpresion", porte.horaImpresion));
                                        command.Parameters.Add(new SqlParameter("@observaciones", porte.observaciones));
                                        command.Parameters.Add(new SqlParameter("@pesoBruto", porte.pesoBruto));
                                        command.Parameters.Add(new SqlParameter("@idOrigen", (porte.origen == null) ? null : (int?)(porte.origen.idOrigen)));
                                        command.Parameters.Add(new SqlParameter("@remolque", porte.remolque));
                                        command.Parameters.Add(new SqlParameter("@viaje", porte.viaje));
                                        command.Parameters.Add(new SqlParameter("@precioOperador", porte.precioOperador));
                                        command.Parameters.Add(new SqlParameter("@incluyeCaseta", porte.incluyeCaseta));
                                        command.Parameters.Add(new SqlParameter("@precioCaseta", porte.precioCaseta));
                                        command.Parameters.Add(new SqlParameter("@precioFijo", porte.precioFijo));
                                        command.Parameters.Add(new SqlParameter("@km", porte.km));
                                        command.Parameters.Add(new SqlParameter("@cobroXkm", porte.cobroXkm));
                                        command.Parameters.Add(new SqlParameter("@vDescarga", porte.vDescarga));
                                        command.Parameters.Add(new SqlParameter("@unidadMedida", string.IsNullOrEmpty(porte.unidadMedida) ? string.Empty : porte.unidadMedida));
                                        command.ExecuteNonQuery();
                                        if (CrearSqlCommand.validarCorrecto(command))
                                        {
                                            porte.Consecutivo = (int)idParam.Value;
                                            porte.numGuiaId = viaje.NumGuiaId;
                                        }
                                        else
                                        {
                                            transaction.Rollback("save");
                                            return new OperationResult(command);
                                        }
                                    }
                                }

                                if (viaje.listaAnticipos != null)
                                {
                                    command.CommandText = "get_limpiarAnticiposViajes";
                                    command.Parameters.Clear();
                                    foreach (var param in CrearSqlCommand.getPatametros())
                                    {
                                        command.Parameters.Add(param);
                                    }
                                    command.Parameters.Add(new SqlParameter("@numGuiaId", viaje.NumGuiaId));
                                    command.ExecuteNonQuery();
                                    if (!CrearSqlCommand.validarCorrecto(command))
                                    {
                                        transaction.Rollback("save");
                                        return new OperationResult(command);
                                    }

                                    command.CommandText = "get_saveAnticiposViajes";
                                    foreach (var anticipo in viaje.listaAnticipos)
                                    {
                                        command.Parameters.Clear();
                                        foreach (var param in CrearSqlCommand.getPatametros())
                                        {
                                            command.Parameters.Add(param);
                                        }
                                        command.Parameters.Add(new SqlParameter("@numGuiaId", viaje.NumGuiaId));
                                        command.Parameters.Add(new SqlParameter("@idAnticipo", anticipo.idAnticipo));
                                        command.Parameters.Add(new SqlParameter("@costo", anticipo.costo));
                                        command.Parameters.Add(new SqlParameter("@observaciones", string.IsNullOrEmpty(anticipo.observaciones) ? string.Empty : anticipo.observaciones));
                                        command.ExecuteNonQuery();
                                        if (!CrearSqlCommand.validarCorrecto(command))
                                        {
                                            transaction.Rollback("save");
                                            return new OperationResult(command);
                                        }
                                    }
                                }

                                if (viaje.listaAnticiposMaster != null)
                                {
                                    if (viaje.listaAnticiposMaster.Count > 0)
                                    {
                                        foreach (var anticipo in viaje.listaAnticiposMaster)
                                        {
                                            command.CommandText = "sp_saveAnticipoMaster";
                                            command.Parameters.Clear();
                                            foreach (var param in CrearSqlCommand.getPatametros(true))
                                            {
                                                command.Parameters.Add(param);
                                            }
                                            anticipo.folioViaje = viaje.NumGuiaId;
                                            command.Parameters.Add(new SqlParameter("@idAnticipo", anticipo.idAnticipoMaster));
                                            command.Parameters.Add(new SqlParameter("@folioViaje", anticipo.folioViaje));
                                            command.Parameters.Add(new SqlParameter("@fecha", anticipo.fechaPago));
                                            command.Parameters.Add(new SqlParameter("@usuario", anticipo.usuario));
                                            command.ExecuteNonQuery();
                                            int idAnti = 0;
                                            if (CrearSqlCommand.validarCorrecto(command, out idAnti))
                                            {
                                                anticipo.idAnticipoMaster = idAnti;

                                                foreach (var detAnt in anticipo.listaDetalles)
                                                {
                                                    command.CommandText = "sp_saveDetalleAnticipoMaster";
                                                    command.Parameters.Clear();
                                                    foreach (var param in CrearSqlCommand.getPatametros(true))
                                                    {
                                                        command.Parameters.Add(param);
                                                    }
                                                    command.Parameters.Add(new SqlParameter("@idDetalleAnticipo", detAnt.idDetalleAnticipoMaster));
                                                    command.Parameters.Add(new SqlParameter("@idAnticipo", anticipo.idAnticipoMaster));
                                                    command.Parameters.Add(new SqlParameter("@tipoAnticipo", detAnt.tipoAnticipo.idTipoAnticipo));
                                                    command.Parameters.Add(new SqlParameter("@importe", detAnt.importe));
                                                    int idDetAnt = 0;
                                                    command.ExecuteNonQuery();
                                                    if (CrearSqlCommand.validarCorrecto(command, out idDetAnt))
                                                    {
                                                        detAnt.idDetalleAnticipoMaster = idDetAnt;
                                                    }
                                                    else
                                                    {
                                                        transaction.Rollback("save");
                                                        return new OperationResult(command);
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                transaction.Rollback("save");
                                                return new OperationResult(command);
                                            }

                                            if (new CierreProcesoViajeServices().empresaSincronizaViajes(viaje.IdEmpresa).typeResult == ResultTypes.success)
                                            {
                                                if (anticipo.cveINTELISIS == 0)
                                                {
                                                    var respAntiINTELISIS = saveAnticipoINTELISIS(anticipo, viaje);
                                                    if (respAntiINTELISIS.typeResult == ResultTypes.success)
                                                    {
                                                        AnticipoMaster anticipoMas = respAntiINTELISIS.result as AnticipoMaster;
                                                        command.CommandText = "sp_updateAnticpoMaster";
                                                        command.Parameters.Clear();
                                                        foreach (var param in CrearSqlCommand.getPatametros())
                                                        {
                                                            command.Parameters.Add(param);
                                                        }
                                                        command.Parameters.Add(new SqlParameter("@idAnticipo", anticipoMas.idAnticipoMaster));
                                                        command.Parameters.Add(new SqlParameter("@cveIntelisis", anticipoMas.cveINTELISIS));
                                                        command.ExecuteNonQuery();
                                                        if (!CrearSqlCommand.validarCorrecto(command))
                                                        {
                                                            transaction.Rollback();
                                                            return new OperationResult(command);
                                                        }
                                                        anticipo.cveINTELISIS = anticipoMas.cveINTELISIS;
                                                    }
                                                    else
                                                    {
                                                        transaction.Rollback("save");
                                                        return respAntiINTELISIS;
                                                    }
                                                }                                                
                                            }
                                        }
                                    }
                                }

                                transaction.Commit();
                                return new OperationResult(command, viaje);
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                return new OperationResult(exception);
            }
        }

        OperationResult saveAnticipoINTELISIS(AnticipoMaster anticipo, Viaje viaje)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactoryINTELISIS())
                {
                    con.Open();
                    using (SqlTransaction tran = con.BeginTransaction())
                    {
                        using (SqlCommand comad = CrearSqlCommand.getCommand(con, "CILTRA_saveAnticipo", true))
                        {
                            comad.Transaction = tran;
                            comad.Parameters.Add(new SqlParameter("@idAnticipo", anticipo.idAnticipoMaster));
                            comad.Parameters.Add(new SqlParameter("@empresa", viaje.IdEmpresa));
                            comad.Parameters.Add(new SqlParameter("@fecha", anticipo.fechaPago));
                            var e = viaje.listDetalles.Find(s => s.cliente != null).cliente.cveINTELISIS;
                            comad.Parameters.Add(new SqlParameter("@UEN", viaje.listDetalles.Find(s=>s.cliente != null).cliente.cveINTELISIS));
                            comad.Parameters.Add(new SqlParameter("@usuario", viaje.cveUsuario));
                            comad.Parameters.Add(new SqlParameter("@acreedor", viaje.operador.cveAgente));
                            comad.Parameters.Add(new SqlParameter("@importe", anticipo.importe));
                            comad.Parameters.Add(new SqlParameter("@observaciones", string.Empty));
                            comad.Parameters.Add(new SqlParameter("@sucursal", viaje.zonaOperativa.cveINTELISIS));

                            comad.ExecuteNonQuery();
                            int id = 0;
                            if (CrearSqlCommand.validarCorrecto(comad, out id))
                            {
                                anticipo.cveINTELISIS = id;                                
                            }
                            else
                            {
                                tran.Rollback();
                                return new OperationResult(comad);
                            }
                            tran.Commit();
                            return new OperationResult(comad, anticipo);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        public OperationResult saveCartaPorteAtlante(Viaje cartaPorte, bool omitirRegraRemisiones = false)
        {
            OperationResult result;
            try
            {
                string str = GenerateXML.generateListDetalles(cartaPorte.listDetalles);
                if (str == null)
                {
                    result = new OperationResult
                    {
                        mensaje = "Error al contruir el XML",
                        valor = 1
                    };
                }
                else
                {
                    using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                    {
                        using (SqlCommand command = connection.CreateCommand())
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.CommandText = "sp_saveCartaPorte";
                            command.Parameters.Add(new SqlParameter("@NumGuiaId", cartaPorte.NumGuiaId));
                            command.Parameters.Add(new SqlParameter("@SerieGuia", cartaPorte.SerieGuia));
                            command.Parameters.Add(new SqlParameter("@NumViaje", cartaPorte.NumViaje));
                            command.Parameters.Add(new SqlParameter("@idCliente", cartaPorte.cliente.clave));
                            command.Parameters.Add(new SqlParameter("@IdEmpresa", cartaPorte.IdEmpresa));
                            command.Parameters.Add(new SqlParameter("@FechaHoraViaje", cartaPorte.FechaHoraViaje));
                            command.Parameters.Add(new SqlParameter("@idOperador", cartaPorte.operador.clave));
                            command.Parameters.Add(new SqlParameter("@idTractor", cartaPorte.tractor.clave));
                            command.Parameters.Add(new SqlParameter("@idRemolque1", cartaPorte.remolque.clave));
                            command.Parameters.Add(new SqlParameter("@idDolly", cartaPorte.dolly?.clave));
                            command.Parameters.Add(new SqlParameter("@idRemolque2", cartaPorte.remolque2?.clave));
                            command.Parameters.Add(new SqlParameter("@EstatusGuia", cartaPorte.EstatusGuia));
                            command.Parameters.Add(new SqlParameter("@UserViaje", cartaPorte.UserViaje));
                            command.Parameters.Add(new SqlParameter("@tipoViaje", cartaPorte.tipoViaje));
                            command.Parameters.Add(new SqlParameter("@tara", cartaPorte.tara));
                            command.Parameters.Add(new SqlParameter("@km", cartaPorte.km));
                            command.Parameters.Add(new SqlParameter("@xml", str));
                            command.Parameters.Add(new SqlParameter("@omitirRegraRemisiones", omitirRegraRemisiones ? 1 : 0));
                            SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                            {
                                Direction = ParameterDirection.Output
                            };
                            SqlParameter parameter = command.Parameters.Add(parameter1);
                            SqlParameter parameter4 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                            {
                                Direction = ParameterDirection.Output
                            };
                            SqlParameter parameter2 = command.Parameters.Add(parameter4);
                            SqlParameter parameter5 = new SqlParameter("@identificador", SqlDbType.Int)
                            {
                                Direction = ParameterDirection.Output
                            };
                            SqlParameter parameter3 = command.Parameters.Add(parameter5);
                            connection.Open();
                            SqlDataReader reader = command.ExecuteReader();
                            Viaje viaje = new Viaje();
                            using (SqlDataReader reader2 = reader)
                            {
                                while (reader2.Read())
                                {
                                    viaje = new Viaje
                                    {
                                        NumGuiaId = (int)reader2["NumGuiaId"],
                                        SerieGuia = (string)reader2["SerieGuia"],
                                        NumViaje = (int)reader2["NumViaje"],
                                        FechaHoraViaje = (DateTime)reader2["FechaHoraViaje"],
                                        EstatusGuia = (string)reader2["EstatusGuia"],
                                        UserViaje = (int)reader2["UserViaje"],
                                        tara = (decimal)reader2["tara"],
                                        km = (decimal)reader2["km"]
                                    };
                                    OperationResult result2 = new OperationResult();
                                    result2 = this.mapCliente((int)reader2["idCliente"]);
                                    if (result2.typeResult == ResultTypes.success)
                                    {
                                        viaje.cliente = (Cliente)result2.result;
                                    }
                                    else
                                    {
                                        return new OperationResult
                                        {
                                            valor = result2.valor,
                                            mensaje = result2.mensaje,
                                            result = null
                                        };
                                    }
                                    result2 = this.mapOperador((int)reader2["idOperador"]);
                                    if (result2.typeResult == ResultTypes.success)
                                    {
                                        viaje.operador = (Personal)result2.result;
                                    }
                                    else
                                    {
                                        return new OperationResult
                                        {
                                            valor = result2.valor,
                                            mensaje = result2.mensaje,
                                            result = null
                                        };
                                    }
                                    result2 = this.mapUnidadTransporte((string)reader2["idTractor"]);
                                    if (result2.typeResult == ResultTypes.success)
                                    {
                                        viaje.tractor = (UnidadTransporte)result2.result;
                                    }
                                    else
                                    {
                                        return new OperationResult
                                        {
                                            valor = result2.valor,
                                            mensaje = result2.mensaje,
                                            result = null
                                        };
                                    }
                                    result2 = this.mapUnidadTransporte((string)reader2["idRemolque1"]);
                                    if (result2.typeResult == ResultTypes.success)
                                    {
                                        viaje.remolque = (UnidadTransporte)result2.result;
                                    }
                                    else
                                    {
                                        return new OperationResult
                                        {
                                            valor = result2.valor,
                                            mensaje = result2.mensaje,
                                            result = null
                                        };
                                    }
                                    if (((string)reader2["idDolly"]) != "")
                                    {
                                        result2 = this.mapUnidadTransporte((string)reader2["idDolly"]);
                                        if (result2.typeResult != ResultTypes.success)
                                        {
                                            return new OperationResult
                                            {
                                                valor = result2.valor,
                                                mensaje = result2.mensaje,
                                                result = null
                                            };
                                        }
                                        viaje.dolly = (UnidadTransporte)result2.result;
                                    }
                                    else
                                    {
                                        viaje.dolly = null;
                                    }
                                    if (((string)reader2["idRemolque2"]) != "")
                                    {
                                        result2 = this.mapUnidadTransporte((string)reader2["idRemolque2"]);
                                        if (result2.typeResult != ResultTypes.success)
                                        {
                                            return new OperationResult
                                            {
                                                valor = result2.valor,
                                                mensaje = result2.mensaje,
                                                result = null
                                            };
                                        }
                                        viaje.remolque2 = (UnidadTransporte)result2.result;
                                    }
                                    else
                                    {
                                        viaje.remolque2 = null;
                                    }
                                    viaje.listDetalles = new List<CartaPorte>();
                                    result2 = this.getDetallesCartaPorteById((int)reader2["NumGuiaId"], (int)reader2["idCliente"]);
                                    if (result2.typeResult == ResultTypes.recordNotFound)
                                    {
                                        viaje.listDetalles = new List<CartaPorte>();
                                    }
                                    else if (result2.typeResult == ResultTypes.success)
                                    {
                                        viaje.listDetalles = (List<CartaPorte>)result2.result;
                                    }
                                    else
                                    {
                                        return new OperationResult
                                        {
                                            valor = result2.valor,
                                            mensaje = result2.mensaje,
                                            result = null
                                        };
                                    }
                                    viaje.listDetalles = new List<CartaPorte>();
                                    result2 = this.getDetallesCartaPorteByIdAtlante((int)reader2["NumGuiaId"], (int)reader2["idCliente"]);
                                    if (result2.typeResult == ResultTypes.recordNotFound)
                                    {
                                        viaje.listDetalles = new List<CartaPorte>();
                                    }
                                    else if (result2.typeResult == ResultTypes.success)
                                    {
                                        viaje.listDetalles = (List<CartaPorte>)result2.result;
                                    }
                                    else
                                    {
                                        return new OperationResult
                                        {
                                            valor = result2.valor,
                                            mensaje = result2.mensaje,
                                            result = null
                                        };
                                    }
                                }
                            }
                            result = new OperationResult
                            {
                                valor = new int?((int)parameter.Value),
                                mensaje = (string)parameter2.Value,
                                result = viaje
                            };
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
            return result;
        }

        public OperationResult saveDetalleViaje(List<CartaPorte> listaDetalles, int numGuiaId)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    connection.Open();
                    using (SqlTransaction transaction = connection.BeginTransaction())
                    {
                        using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_saveDetallesCartaNew_v2", true))
                        {
                            command.Transaction = transaction;
                            foreach (CartaPorte porte in listaDetalles)
                            {
                                command.Parameters.Clear();
                                foreach (SqlParameter parameter in CrearSqlCommand.getPatametros(false))
                                {
                                    command.Parameters.Add(parameter);
                                }
                                SqlParameter parameter1 = new SqlParameter("@identificador", SqlDbType.Int)
                                {
                                    Direction = ParameterDirection.Output
                                };
                                command.Parameters.Add(parameter1);
                                command.Parameters.Add(new SqlParameter("@idTarea", porte.idTarea));
                                command.Parameters.Add(new SqlParameter("@Consecutivo", porte.Consecutivo));
                                command.Parameters.Add(new SqlParameter("@idViaje", numGuiaId));
                                command.Parameters.Add(new SqlParameter("@NoRemision", porte.remision));
                                command.Parameters.Add(new SqlParameter("@IdProdTraf", porte.producto.clave));
                                command.Parameters.Add(new SqlParameter("@IdPlazaTraf", porte.zonaSelect.clave));
                                command.Parameters.Add(new SqlParameter("@VolDescarga", porte.volumenDescarga));
                                command.Parameters.Add(new SqlParameter("@precio", porte.precio));
                                command.Parameters.Add(new SqlParameter("@PorcIVA", porte.PorcIVA));
                                command.Parameters.Add(new SqlParameter("@ImpIVA", porte.ImporIVA));
                                command.Parameters.Add(new SqlParameter("@PorcRetencion", porte.PorcRetencion));
                                command.Parameters.Add(new SqlParameter("@ImpRetencion", porte.ImpRetencion));
                                command.Parameters.Add(new SqlParameter("@subTotal", porte.importeReal));
                                command.Parameters.Add(new SqlParameter("@fCartaPorte", SqlDbType.BigInt));
                                command.Parameters.Add(new SqlParameter("@tara", porte.tara));
                                command.Parameters.Add(new SqlParameter("@horaLlegada", porte.horaLlegada));
                                command.Parameters.Add(new SqlParameter("@horaCaptura", porte.horaCaptura));
                                command.Parameters.Add(new SqlParameter("@horaImpresion", porte.horaImpresion));
                                command.Parameters.Add(new SqlParameter("@observaciones", porte.observaciones));
                                command.Parameters.Add(new SqlParameter("@pesoBruto", porte.pesoBruto));
                                command.Parameters.Add(new SqlParameter("@idOrigen", (porte.origen == null) ? null : new int?(porte.origen.idOrigen)));
                                command.Parameters.Add(new SqlParameter("@remolque", porte.remolque));
                                command.Parameters.Add(new SqlParameter("@viaje", porte.viaje));
                                command.Parameters.Add(new SqlParameter("@precioOperador", porte.precioOperador));
                                command.Parameters.Add(new SqlParameter("@incluyeCaseta", porte.incluyeCaseta));
                                command.Parameters.Add(new SqlParameter("@precioCaseta", porte.precioCaseta));
                                command.Parameters.Add(new SqlParameter("@precioFijo", porte.precioFijo));
                                command.Parameters.Add(new SqlParameter("@km", porte.km));
                                command.Parameters.Add(new SqlParameter("@cobroXkm", porte.cobroXkm));
                                command.ExecuteNonQuery();
                                if (((int)command.Parameters["@valor"].Value) == 0)
                                {
                                    porte.Consecutivo = (int)command.Parameters["@identificador"].Value;
                                    porte.numGuiaId = numGuiaId;
                                }
                                else
                                {
                                    transaction.Rollback();
                                    return new OperationResult(command, null);
                                }
                            }
                            transaction.Commit();
                            result = new OperationResult(command, null);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult(exception);
            }
            return result;
        }

        internal OperationResult saveRemisionesLiberadas(Viaje viaje)
        {
            OperationResult result;
            try
            {
                string str = GenerateXML.generarListaRemisionesLiberadas(viaje);
                if (string.IsNullOrEmpty(str))
                {
                    result = new OperationResult
                    {
                        valor = 1,
                        mensaje = "Error al construir el XML"
                    };
                }
                else
                {
                    using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                    {
                        using (SqlCommand command = connection.CreateCommand())
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.CommandText = "sp_saveRemisionesLiberadas";
                            command.Parameters.Add(new SqlParameter("@xml", str));
                            SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                            {
                                Direction = ParameterDirection.Output
                            };
                            SqlParameter parameter = command.Parameters.Add(parameter1);
                            SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                            {
                                Direction = ParameterDirection.Output
                            };
                            SqlParameter parameter2 = command.Parameters.Add(parameter3);
                            connection.Open();
                            int num = command.ExecuteNonQuery();
                            result = new OperationResult
                            {
                                valor = new int?((int)parameter.Value),
                                mensaje = (string)parameter2.Value,
                                result = null
                            };
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            return result;
        }

        public OperationResult saveValesCarga(List<ValeCarga> listVales)
        {
            OperationResult result;
            try
            {
                string str = GenerateXML.generarListaValesCarga(listVales);
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_saveValesCarga";
                        command.Parameters.Add(new SqlParameter("@xml", str));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        List<ValeCarga> list = new List<ValeCarga>();
                        connection.Open();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                ValeCarga item = new ValeCarga
                                {
                                    idValeCarga = (int)reader["idValeCarga"],
                                    remision = (string)reader["noRemision"],
                                    numGuiaId = (int)reader["numGuiaId"],
                                    producto = (((int)reader["idProducto"]) == 0) ? null : new mapComunes().mapProductos(reader),
                                    origen = (((int)reader["idOrigen"]) == 0) ? null : new mapComunes().mapOrigen(reader),
                                    destino = (((int)reader["idDestino"]) == 0) ? null : new mapComunes().mapDestino(reader),
                                    cartaPorte = (int)reader["cartaPorte"],
                                    volDescarga = (decimal)reader["volDescarga"],
                                    precio = DBNull.Value.Equals(reader["precio"]) ? decimal.Zero : ((decimal)reader["precio"]),
                                    cliente = new mapComunes().mapCliente(reader)
                                };
                                list.Add(item);
                            }
                        }
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = list
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            return result;
        }
        public OperationResult updateDetalleViaje(CartaPorte cartaPorte)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_updateDetalleViaje", false))
                    {
                        command.Parameters.Add(new SqlParameter("@cartaPorte", cartaPorte.Consecutivo));
                        command.Parameters.Add(new SqlParameter("@folio", cartaPorte.fCartaPorte));
                        connection.Open();
                        command.ExecuteNonQuery();
                        result = new OperationResult(command, null);
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult(exception);
            }
            return result;
        }

        public OperationResult getDatosUltimoViajeByTC(string tc)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getDatosUltimoViajeByTC"))
                    {
                        comad.Parameters.Add(new SqlParameter("@tc", tc));
                        UltimoViaje ultimoViaje = new UltimoViaje();
                        con.Open();
                        using (SqlDataReader sqlReader = comad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                ultimoViaje = new UltimoViaje
                                {
                                    idRemolque = (string)sqlReader["idRemolque1"],
                                    idDolly = DBNull.Value.Equals(sqlReader["idDolly"]) ? null : (string)sqlReader["idDolly"],
                                    idRemolque2 = DBNull.Value.Equals(sqlReader["idRemolque2"]) ? null : (string)sqlReader["idRemolque2"],
                                    idOperador = (int)sqlReader["idOperador"]
                                };
                            }
                        }
                        return new OperationResult(comad, ultimoViaje);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getUltimaFechaEntradaPlanta(string tractor, DateTime? fecha = null)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getUltimaFechaEntradaPlanta"))
                    {
                        comad.Parameters.Add(new SqlParameter("@tractor", tractor));
                        comad.Parameters.Add(new SqlParameter("@fecha", fecha));
                        con.Open();
                        DateTime? fechaEntrada = null;
                        using (SqlDataReader sqlReader = comad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                fechaEntrada = !DBNull.Value.Equals(sqlReader["fecha"]) ? (DateTime?)sqlReader["fecha"] : null;
                            }
                        }
                        return new OperationResult(comad, fechaEntrada);
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public OperationResult ligarViajeVale(int idViaje, int idVale)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_ligarViajeVale"))
                    {
                        comad.Parameters.Add(new SqlParameter("idViaje", idViaje));
                        comad.Parameters.Add(new SqlParameter("idVale", idVale));
                        con.Open();
                        comad.ExecuteNonQuery();
                        return new OperationResult(comad);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getNoViajeOperador(DateTime fecha, int idOperador)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getNoViajeOperador"))
                    {
                        comad.Parameters.Add(new SqlParameter("fecha", fecha));
                        comad.Parameters.Add(new SqlParameter("idOperador", idOperador));

                        con.Open();
                        NoViajeOperador noViajeOperador = new NoViajeOperador();
                        using (SqlDataReader reader = comad.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                noViajeOperador = new NoViajeOperador
                                {
                                    noViaje = (int)reader["noViaje"],
                                    fechaFin = (DateTime)reader["fechaFin"],
                                    horas = (int)reader["hora"]
                                };
                            }
                        }

                        return new OperationResult(comad, noViajeOperador);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult bajarRemisiones(List<string> listaConsecutivos, int idUsuario, string motivo)
        {
            try
            {
                string xml = GenerateXML.generarListaCadena(listaConsecutivos);
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    con.Open();
                    using (SqlTransaction tran = con.BeginTransaction())
                    {
                        using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_bajarRemisiones"))
                        {
                            comad.Transaction = tran;
                            comad.Parameters.Add(new SqlParameter("@xml", xml));
                            comad.Parameters.Add(new SqlParameter("@motivo", motivo));
                            comad.Parameters.Add(new SqlParameter("@idUsuario", idUsuario));

                            comad.ExecuteNonQuery();
                            if (CrearSqlCommand.validarCorrecto(comad))
                            {
                                tran.Commit();
                            }
                            else
                            {
                                tran.Rollback();
                            }
                            return new OperationResult(comad);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult sincronizarViajes(List<ReporteCartaPorteViajes> listViajes, List<ReporteCartaPorte> listaCartaPortes)
        {
            try
            {
                string xmlViajes = GenerateXML.generarListaViajes(listViajes);


                using (SqlConnection con = BoundedContextFactory.ConnectionFactoryINTELISIS())
                {
                    con.Open();
                    using (SqlTransaction tran = con.BeginTransaction())
                    {
                        using (SqlCommand comad = CrearSqlCommand.getCommand(con, "CILTRA_sp_sincronizarViajes"))
                        {
                            comad.Transaction = tran;
                            comad.Parameters.Add(new SqlParameter("@xmlViajes", xmlViajes));
                            List<RegistroINTELISIS> listaRegistro = new List<RegistroINTELISIS>();
                            using (SqlDataReader reader = comad.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    listaRegistro.Add(new RegistroINTELISIS
                                    {
                                        id = (int)reader["id"],
                                        folio = (int)reader["folio"],
                                        cveCliente = (string)reader["cveCliente"],
                                        fechaReg = (DateTime)reader["fechaReg"]
                                    });
                                }
                            }
                            if (!CrearSqlCommand.validarCorrecto(comad))
                            {
                                tran.Rollback();
                                return new OperationResult(comad);
                            }

                            foreach (var cp in listaCartaPortes)
                            {
                                cp.id = listaRegistro.Find(s => s.folio == cp.folio && cp.cveCliente == s.cveCliente).id;
                            }

                            string xmlCartaPortes = GenerateXML.generarListaCartaPorte(listaCartaPortes);

                            comad.CommandText = "CILTRA_sp_sincronizarCartaPortes";
                            comad.Parameters.Clear();
                            foreach (var param in CrearSqlCommand.getPatametros())
                            {
                                comad.Parameters.Add(param);
                            }
                            comad.Parameters.Add(new SqlParameter("@xmlCartaPortes", xmlCartaPortes));
                            //comad.ExecuteNonQuery();
                            List<RegistroCartaPortesINTELISIS> listaCp = new List<RegistroCartaPortesINTELISIS>();
                            using (SqlDataReader reader = comad.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    listaCp.Add(new RegistroCartaPortesINTELISIS
                                    {
                                        folio = (int)reader["folio"],
                                        consecutivo = (int)reader["consecutivo"],
                                        cveINTELISIS = (int)reader["id"],
                                        renglon = Convert.ToDecimal(reader["renglon"]),
                                        renglonId = Convert.ToDecimal(reader["renglonId"])
                                    });
                                }
                            }
                            if (!CrearSqlCommand.validarCorrecto(comad))
                            {
                                tran.Rollback();
                                return new OperationResult(comad);
                            }

                            //string xmlListaReg = GenerateXML.generarListaReg(listaRegistro);
                            //comad.CommandText = "CILTRA_sp_afectarViajes";
                            //comad.Parameters.Clear();
                            //foreach (var param in CrearSqlCommand.getPatametros())
                            //{
                            //    comad.Parameters.Add(param);
                            //}
                            //comad.Parameters.Add(new SqlParameter("@xmlListaReg", xmlListaReg));
                            //comad.ExecuteNonQuery();
                            //if (!CrearSqlCommand.validarCorrecto(comad))
                            //{
                            //    tran.Rollback();
                            //    return new OperationResult(comad);
                            //}

                            tran.Commit();
                            return new OperationResult(comad, listaRegistro, listaCp);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        private OperationResult afectarViajes(List<RegistroINTELISIS> listaRegistro)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactoryINTELISIS())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "CILTRA_sp_afectarViajes"))
                    {
                        string xmlListaReg = GenerateXML.generarListaReg(listaRegistro);

                        comad.Parameters.Add(new SqlParameter("@xmlListaReg", xmlListaReg));
                        con.Open();
                        comad.ExecuteNonQuery();
                        if (!CrearSqlCommand.validarCorrecto(comad))
                        {

                            return new OperationResult(comad);
                        }
                        return new OperationResult(comad);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        public OperationResult saveComentarioViajes(int numViaje, string usuario, string comentario, DateTime fecha)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    con.Open();
                    using (SqlTransaction tran = con.BeginTransaction())
                    {
                        using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_saveComentarioViaje"))
                        {
                            comad.Transaction = tran;
                            comad.Parameters.Add(new SqlParameter("@numGuiaId", numViaje));
                            comad.Parameters.Add(new SqlParameter("@usuario", usuario));
                            comad.Parameters.Add(new SqlParameter("@comentario", comentario));
                            comad.Parameters.Add(new SqlParameter("@fecha", fecha));
                            comad.ExecuteNonQuery();

                            if (!CrearSqlCommand.validarCorrecto(comad))
                                tran.Rollback();
                            else
                                tran.Commit();

                            return new OperationResult(comad);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        public OperationResult getListaComentariosViajes(List<string> listaId)
        {
            try
            {
                string xml = GenerateXML.generarListaCadena(listaId);
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getComentariosViajes"))
                    {
                        comad.Parameters.Add(new SqlParameter("@xml", xml));

                        con.Open();
                        List<ComentarioViaje> listaComentarios = new List<ComentarioViaje>();
                        using (SqlDataReader reader = comad.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                ComentarioViaje comentario = new ComentarioViaje
                                {
                                    idComentario = (int)reader["idComentarioViaje"],
                                    idViaje = (int)reader["NumGuiaId"],
                                    usuario = (string)reader["usuario"],
                                    comentario = (string)reader["comentario"],
                                    fecha = (DateTime)reader["fecha"]
                                };
                                listaComentarios.Add(comentario);
                            }
                        }
                        return new OperationResult(comad, listaComentarios);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getAnticipoMasterByFolioViaje(int folioViaje)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "getAnticipoMasterByFolioViaje"))
                    {
                        comad.Parameters.Add(new SqlParameter("@folioViaje", folioViaje));
                        List<AnticipoMaster> listaAnt = new List<AnticipoMaster>();
                        con.Open();
                        using (SqlDataReader reader = comad.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                AnticipoMaster anticipo = new mapComunes().mapAnticipoMaster(reader);
                                if (anticipo == null) return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");
                                listaAnt.Add(anticipo);
                            }
                        }

                        var resp = getDetalleAnticipoMasterByFolioViaje(folioViaje);
                        if (resp.typeResult == ResultTypes.success)
                        {
                            var listaDet = resp.result as List<DetalleAnticipoMaster>;
                            foreach (var ant in listaAnt)
                            {
                                ant.listaDetalles = listaDet.FindAll(s => s.idAnticipoMaster == ant.idAnticipoMaster);
                            }
                        }
                        else
                        {
                            return resp;
                        }

                        return new OperationResult(comad, listaAnt);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getDetalleAnticipoMasterByFolioViaje(int folioViaje)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getDetalleAnticipoMasterByFolioViaje"))
                    {
                        comad.Parameters.Add(new SqlParameter("@folioViaje", folioViaje));
                        List<DetalleAnticipoMaster> listaAnt = new List<DetalleAnticipoMaster>();
                        con.Open();
                        using (SqlDataReader reader = comad.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                DetalleAnticipoMaster detalle = new mapComunes().mapDetalleAnticipoMaster(reader);
                                if (detalle == null) return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");
                                listaAnt.Add(detalle);
                            }
                        }

                        return new OperationResult(comad, listaAnt);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }

    public class RegistroINTELISIS
    {
        public int id { get; set; }
        public int folio { get; set; }
        public string cveCliente { get; set; }
        public DateTime fechaReg { get; set; }
    }

    public class RegistroCartaPortesINTELISIS
    {
        public int cveINTELISIS { get; set; }
        public int folio { get; set; }
        public int consecutivo { get; set; }
        public decimal renglon { get; set; }
        public decimal renglonId { get; set; }
    }
}
