﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using Core.Utils;
using System.Data.SqlClient;
using System.Data;
using Core.BusinessLogic;

namespace CoreFletera.BusinessLogic
{
    public class RelacionTiposOrdenServServices
    {
        public OperationResult getTiposOrdenes()
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand command = con.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getTiposOrdenes";

                        SqlParameter spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        SqlParameter spMensaje = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                        con.Open();
                        List<TipoOrdenServClass> tiposOrden = new List<TipoOrdenServClass>();
                        using (SqlDataReader sqlReader = command.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                tiposOrden.Add(new TipoOrdenServClass
                                {
                                    idTipoOrdServ = (int)sqlReader["idTipOrdServ"],
                                    nomTipoOrdenSer = (string)sqlReader["NomTipOrdServ"]
                                });
                            }
                        }

                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = tiposOrden };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getAllActividades()
        {
            try
            {
                using (SqlConnection conection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comand = conection.CreateCommand())
                    {
                        comand.CommandType = CommandType.StoredProcedure;
                        comand.CommandText = "sp_getAllActividades";

                        SqlParameter spValor = comand.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        SqlParameter spMensaje = comand.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        conection.Open();
                        List<Actividad> listaActividad = new List<Actividad>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                Actividad det = new Actividad
                                {
                                    idActividad = (int)sqlReader["idActividad"],
                                    nombre = (string)sqlReader["NombreAct"],
                                    costo = (decimal)sqlReader["costo"],
                                    duracion = (decimal)sqlReader["duracion"]
                                };
                                listaActividad.Add(det);
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = listaActividad };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }
        public OperationResult getAllServicios()
        {
            try
            {
                using (SqlConnection conection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comand = conection.CreateCommand())
                    {
                        comand.CommandType = CommandType.StoredProcedure;
                        comand.CommandText = "sp_getAllServicios";

                        SqlParameter spValor = comand.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        SqlParameter spMensaje = comand.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        conection.Open();
                        List<Servicio> listaServicios = new List<Servicio>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                var det = new mapComunes().mapServicio(sqlReader);
                                if (det == null)
                                {
                                    return new OperationResult { valor = 1, mensaje = "Ocurrio un error al obtener datos del servicio" };
                                }
                                listaServicios.Add(det);
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = listaServicios };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }

        public OperationResult getAllTipoDeOrden()
        {
            try
            {
                using (SqlConnection conection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comand = conection.CreateCommand())
                    {
                        comand.CommandType = CommandType.StoredProcedure;
                        comand.CommandText = "sp_getAllTipoDeOrden";
                        

                        SqlParameter spValor = comand.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        SqlParameter spMensaje = comand.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        conection.Open();
                        List<TipoOrden> listaTipoOrden = new List<TipoOrden>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                var det = new TipoOrden()
                                {
                                    idTipoOrden = (int)sqlReader["idTipoOrden"],
                                    nombreTipoOrden = (string)sqlReader["NomTipoOrden"]
                                };
                                listaTipoOrden.Add(det);
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = listaTipoOrden };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }

        public OperationResult saveRelacioOrdenServ(TipoOrdenServClass tipoOrden, CatTipoOrdenServ catalogo, int idRelacion)
        {
            try
            {
                using (SqlConnection conection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comand = conection.CreateCommand())
                    {
                        comand.CommandType = CommandType.StoredProcedure;
                        comand.CommandText = "sp_saveRelacioOrdenServ";

                        var resp = EnumDesCatalogo.objEnum(catalogo);
                        EnumDesCatalogo x = (EnumDesCatalogo)resp;

                        comand.Parameters.Add(new SqlParameter("@idTipoOrden", tipoOrden.idTipoOrdServ));
                        comand.Parameters.Add(new SqlParameter("@nombreCatalogo", x.catalogo));
                        comand.Parameters.Add(new SqlParameter("@nombreId", x.idRelacion));
                        comand.Parameters.Add(new SqlParameter("@idRelacion", idRelacion));

                        SqlParameter spValor = comand.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        SqlParameter spMensaje = comand.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                        conection.Open();
                        comand.ExecuteNonQuery();

                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }
        public OperationResult deleteRelacioOrdenServ(TipoOrdenServClass tipoOrden, CatTipoOrdenServ catalogo, int idRelacion)
        {
            try
            {
                using (SqlConnection conection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comand = conection.CreateCommand())
                    {
                        comand.CommandType = CommandType.StoredProcedure;
                        comand.CommandText = "sp_deleteRelacioOrdenServ";

                        var resp = EnumDesCatalogo.objEnum(catalogo);
                        EnumDesCatalogo x = (EnumDesCatalogo)resp;

                        comand.Parameters.Add(new SqlParameter("@idTipoOrden", tipoOrden.idTipoOrdServ));
                        comand.Parameters.Add(new SqlParameter("@nombreCatalogo", x.catalogo));
                        comand.Parameters.Add(new SqlParameter("@nombreId", x.idRelacion));
                        comand.Parameters.Add(new SqlParameter("@idRelacion", idRelacion));

                        SqlParameter spValor = comand.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        SqlParameter spMensaje = comand.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                        conection.Open();
                        comand.ExecuteNonQuery();

                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }
    }
}
