﻿namespace CoreFletera.BusinessLogic
{
    using Core.BusinessLogic;
    using Core.Models;
    using Core.Utils;
    using CoreFletera.Models;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Runtime.InteropServices;

    public class CondicionesServices
    {
        internal OperationResult getCondicionAllOrById(int idCondicion = 0)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getCondicionAllOrById";
                        command.Parameters.Add(new SqlParameter("@idCondicion", idCondicion));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        List<CondicionLlanta> list = new List<CondicionLlanta>();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CondicionLlanta item = new mapComunes().mapCondicionLlanta(reader);
                                if (item == null)
                                {
                                    return new OperationResult
                                    {
                                        valor = 1,
                                        mensaje = "ERROR AL MAPEAR LOS RESULTADOS DE LA BUSQUEDA"
                                    };
                                }
                                list.Add(item);
                            }
                        }
                        if (idCondicion == 0)
                        {
                            return new OperationResult
                            {
                                valor = new int?((int)parameter.Value),
                                mensaje = (string)parameter2.Value,
                                result = list
                            };
                        }
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = (((int)parameter.Value) == 0) ? list[0] : null
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            return result;
        }

        public OperationResult saveCondicionLlantas(ref CondicionLlanta condicionLlanta)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    connection.Open();
                    using (SqlTransaction transaction = connection.BeginTransaction())
                    {
                        using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_saveCondicionLlantas", true))
                        {
                            command.Transaction = transaction;
                            command.Parameters.Add(new SqlParameter("@idCondicionLlanta", condicionLlanta.idCondicion));
                            command.Parameters.Add(new SqlParameter("@clave", condicionLlanta.clave));
                            command.Parameters.Add(new SqlParameter("@descripcion", condicionLlanta.descripcion));
                            command.ExecuteNonQuery();
                            int id = 0;
                            if (CrearSqlCommand.validarCorrecto(command, out id))
                            {
                                condicionLlanta.idCondicion = id;
                                transaction.Commit();
                            }
                            else
                            {
                                transaction.Rollback();
                            }
                            result = new OperationResult(command, condicionLlanta);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult(exception);
            }
            return result;
        }
    }
}
