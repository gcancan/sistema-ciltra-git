﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using Core.Utils;
using System.Data;
using System.Data.SqlClient;
using Core.BusinessLogic;

namespace CoreFletera.BusinessLogic
{
    public class ServiciosServices
    {
        public OperationResult saveServicio(ref Servicio servicio)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    con.Open();
                    using (SqlTransaction tran = con.BeginTransaction())
                    {
                        using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_saveServicio", true))
                        {
                            comad.Transaction = tran;
                            comad.Parameters.Add(new SqlParameter("@idServicio", servicio.idServicio));
                            comad.Parameters.Add(new SqlParameter("@idTipoServicio", null));
                            comad.Parameters.Add(new SqlParameter("@nombre", servicio.nombre));
                            comad.Parameters.Add(new SqlParameter("@estatus", servicio.activo ? "ACT" : "BAJ"));
                            comad.Parameters.Add(new SqlParameter("@taller", servicio.taller));
                            comad.Parameters.Add(new SqlParameter("@lavadero", servicio.lavadero));
                            comad.Parameters.Add(new SqlParameter("@llantera", servicio.llantera));
                            comad.ExecuteNonQuery();
                            int id = 0;
                            if (CrearSqlCommand.validarCorrecto(comad, out id))
                            {
                                servicio.idServicio = id;
                            }
                            else
                            {
                                tran.Rollback();
                                return new OperationResult(comad);
                            }
                            tran.Commit();
                            return new OperationResult(comad, servicio);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getAllServicios()
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getAllServicios_V2"))
                    {
                        con.Open();
                        List<Servicio> listaServicio = new List<Servicio>();
                        using (SqlDataReader sqlReader = comad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                Servicio servicio = new mapComunes().mapServicio(sqlReader);
                                if (servicio == null)
                                {
                                    return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTRADOS DE LA BUSQUEDA");
                                }
                                listaServicio.Add(servicio);

                            }
                        }
                        return new OperationResult(comad, listaServicio);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
}
