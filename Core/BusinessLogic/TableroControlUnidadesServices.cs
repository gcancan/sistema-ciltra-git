﻿namespace CoreFletera.BusinessLogic
{
    using Core.BusinessLogic;
    using Core.Models;
    using Core.Utils;
    using CoreFletera.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;

    public class TableroControlUnidadesServices
    {
        public OperationResult getDetallesTablero(int idTablero)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getDetallesTablero", false))
                    {
                        command.Parameters.Add(new SqlParameter("@idTablero", idTablero));
                        connection.Open();
                        List<DetalleTableroControlUnidades> list = new List<DetalleTableroControlUnidades>();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                DetalleTableroControlUnidades item = new mapComunes().mapDetalleTableroControlUnidades(reader);
                                if (item == null)
                                {
                                    return new OperationResult(ResultTypes.error, "Ocurrio un error al leer los resultados de la b\x00fasqueda", null);
                                }
                                list.Add(item);
                            }
                        }
                        result = new OperationResult(command, list);
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult(exception);
            }
            return result;
        }

        public OperationResult getComentariosByIdTablero(int idTablero)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getComentariosByIdTablero", false))
                    {
                        command.Parameters.Add(new SqlParameter("@idTablero", idTablero));
                        connection.Open();
                        List<ComentarioTablero> list = new List<ComentarioTablero>();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                ComentarioTablero item = new ComentarioTablero
                                {
                                    idTablero = (int)reader["idTablero"],
                                    usuario = (string)reader["usuario"],
                                    comentario = (string)reader["comentario"],
                                    fecha = (DateTime)reader["fecha"]
                                };
                                list.Add(item);
                            }
                        }
                        result = new OperationResult(command, list);
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult(exception);
            }
            return result;
        }

        public OperationResult saveComentarioTablero(ComentarioTablero comentario)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_saveComentarioTablero", false))
                    {
                        command.Parameters.Add(new SqlParameter("@idTablero", comentario.idTablero));
                        command.Parameters.Add(new SqlParameter("@usuario", comentario.usuario));
                        command.Parameters.Add(new SqlParameter("@comentario", comentario.comentario));
                        connection.Open();
                        command.ExecuteNonQuery();                        
                        result = new OperationResult(command);
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult(exception);
            }
            return result;
        }

        public OperationResult getDetallesTablero(int idTablero, string strConnection)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = new SqlConnection(strConnection))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getDetallesTablero", false))
                    {
                        command.Parameters.Add(new SqlParameter("@idTablero", idTablero));
                        connection.Open();
                        List<DetalleTableroControlUnidades> list = new List<DetalleTableroControlUnidades>();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                DetalleTableroControlUnidades item = new mapComunes().mapDetalleTableroControlUnidades(reader);
                                if (item == null)
                                {
                                    return new OperationResult(ResultTypes.error, "Ocurrio un error al leer los resultados de la b\x00fasqueda", null);
                                }
                                list.Add(item);
                            }
                        }
                        result = new OperationResult(command, list);
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult(exception);
            }
            return result;
        }

        private OperationResult getDetallesTablero2()
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getDetallesTablero2", false))
                    {
                        connection.Open();
                        List<DetalleTableroControlUnidades> list = new List<DetalleTableroControlUnidades>();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                DetalleTableroControlUnidades item = new mapComunes().mapDetalleTableroControlUnidades(reader);
                                if (item == null)
                                {
                                    return new OperationResult(ResultTypes.error, "Ocurrio un error al leer los resultados de la b\x00fasqueda", null);
                                }
                                list.Add(item);
                            }
                        }
                        result = new OperationResult(command, list);
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult(exception);
            }
            return result;
        }

        public OperationResult getTableroActivoByTractor()
        {
            OperationResult result2;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getUltimoTableroActivoByTractor", false))
                    {
                        connection.Open();
                        TableroControlUnidades unidades = new TableroControlUnidades();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                unidades = new mapComunes().mapTableroControlUnidades(reader);
                                if (unidades == null)
                                {
                                    return new OperationResult(ResultTypes.error, "Ocurrio un error al leer los resultados de la b\x00fasqueda", null);
                                }
                            }
                        }
                        unidades.listaDetalles = new List<DetalleTableroControlUnidades>();
                        OperationResult result = this.getDetallesTablero(unidades.idTableroControlUnidades);
                        if (result.typeResult == ResultTypes.success)
                        {
                            unidades.listaDetalles = result.result as List<DetalleTableroControlUnidades>;
                        }
                        else
                        {
                            return result;
                        }
                        result2 = new OperationResult(command, unidades);
                    }
                }
            }
            catch (Exception exception)
            {
                result2 = new OperationResult(exception);
            }
            return result2;
        }

        public OperationResult getTableroControlUnidad()
        {
            OperationResult result2;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getTableroControlUnidades", false))
                    {
                        connection.Open();
                        List<TableroControlUnidades> list = new List<TableroControlUnidades>();
                        TableroControlUnidades item = new TableroControlUnidades();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                item = new mapComunes().mapTableroControlUnidades(reader);
                                if (item == null)
                                {
                                    return new OperationResult(ResultTypes.error, "Ocurrio un error al leer los resultados de la b\x00fasqueda", null);
                                }
                                list.Add(item);
                            }
                        }
                        OperationResult result = this.getDetallesTablero2();
                        if (result.typeResult != ResultTypes.success)
                        {
                            return result;
                        }
                        List<DetalleTableroControlUnidades> list2 = result.result as List<DetalleTableroControlUnidades>;
                        foreach (TableroControlUnidades tab in list)
                        {
                            tab.listaDetalles = list2.FindAll(s => s.idTableroControlUnidades == tab.idTableroControlUnidades);
                        }
                        result2 = new OperationResult(command, list);
                    }
                }
            }
            catch (Exception exception)
            {
                result2 = new OperationResult(exception);
            }
            return result2;
        }
        public OperationResult getTableroControlUnidadesByOperaciones(int idEmpresa, List<string> listIdZonas, List<string> listOperaciones)
        {
            OperationResult result2;
            try
            {
                string xmlZonas = GenerateXML.generarListaCadena(listIdZonas);
                string xmlOperaciones = GenerateXML.generarListaCadena(listOperaciones);
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getTableroControlUnidadesByOperaciones", false))
                    {
                        command.Parameters.Add(new SqlParameter("@idEmpresa", idEmpresa));
                        command.Parameters.Add(new SqlParameter("@xmlZonas", xmlZonas));
                        command.Parameters.Add(new SqlParameter("@xmlOperaciones", xmlOperaciones));
                        connection.Open();
                        command.CommandTimeout = 1000;
                        List<TableroControlUnidades> list = new List<TableroControlUnidades>();
                        TableroControlUnidades item = new TableroControlUnidades();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                item = new mapComunes().mapTableroControlUnidades(reader);
                                if (item == null)
                                {
                                    return new OperationResult(ResultTypes.error, "Ocurrio un error al leer los resultados de la b\x00fasqueda", null);
                                }
                                var respComentarios = getComentariosByIdTablero(item.idTableroControlUnidades);
                                if (respComentarios.typeResult == ResultTypes.success)
                                {
                                    item.listaComentarios = respComentarios.result as List<ComentarioTablero>;
                                }
                                list.Add(item);
                            }
                        }
                        OperationResult result = this.getDetallesTablero2();
                        if (result.typeResult == ResultTypes.error)
                        {
                            return result;
                        }
                        List<DetalleTableroControlUnidades> list2 = result.result as List<DetalleTableroControlUnidades>;
                        foreach (TableroControlUnidades tab in list)
                        {
                            tab.listaDetalles = list2.FindAll(s => s.idTableroControlUnidades == tab.idTableroControlUnidades);
                        }
                        result2 = new OperationResult(command, list);                       

                    }
                }
            }
            catch (Exception exception)
            {
                result2 = new OperationResult(exception);
            }
            return result2;
        }

        public OperationResult getUltimoTableroActivoByTractor(string idTractor)
        {
            OperationResult result2;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getUltimoTableroActivoByTractor", false))
                    {
                        command.Parameters.Add(new SqlParameter("@idTractor", idTractor));
                        connection.Open();
                        TableroControlUnidades unidades = new TableroControlUnidades();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                unidades = new mapComunes().mapTableroControlUnidades(reader);
                                if (unidades == null)
                                {
                                    return new OperationResult(ResultTypes.error, "Ocurrio un error al leer los resultados de la b\x00fasqueda", null);
                                }
                            }
                        }
                        unidades.listaDetalles = new List<DetalleTableroControlUnidades>();
                        OperationResult result = this.getDetallesTablero(unidades.idTableroControlUnidades);
                        if (result.typeResult == ResultTypes.success)
                        {
                            unidades.listaDetalles = result.result as List<DetalleTableroControlUnidades>;
                        }
                        else
                        {
                            return result;
                        }
                        result2 = new OperationResult(command, unidades);
                    }
                }
            }
            catch (Exception exception)
            {
                result2 = new OperationResult(exception);
            }
            return result2;
        }

        public OperationResult getUltimoTableroActivoByTractor(string idTractor, string strConnection)
        {
            OperationResult result2;
            try
            {
                using (SqlConnection connection = new SqlConnection(strConnection))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getUltimoTableroActivoByTractor", false))
                    {
                        command.Parameters.Add(new SqlParameter("@idTractor", idTractor));
                        connection.Open();
                        TableroControlUnidades unidades = new TableroControlUnidades();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                unidades = new mapComunes().mapTableroControlUnidades(reader);
                                if (unidades == null)
                                {
                                    return new OperationResult(ResultTypes.error, "Ocurrio un error al leer los resultados de la b\x00fasqueda", null);
                                }
                            }
                        }
                        unidades.listaDetalles = new List<DetalleTableroControlUnidades>();
                        OperationResult result = this.getDetallesTablero(unidades.idTableroControlUnidades, strConnection);
                        if (result.typeResult == ResultTypes.success)
                        {
                            unidades.listaDetalles = result.result as List<DetalleTableroControlUnidades>;
                        }
                        else if (result.typeResult == ResultTypes.error)
                        {
                            return result;
                        }
                        result2 = new OperationResult(command, unidades);
                    }
                }
            }
            catch (Exception exception)
            {
                result2 = new OperationResult(exception);
            }
            return result2;
        }

        public OperationResult iniciarTrayectoGranja(ref TableroControlUnidades tablero)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    connection.Open();
                    using (SqlTransaction transaction = connection.BeginTransaction())
                    {
                        using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_iniciarTrayectoGranja", true))
                        {
                            command.Transaction = transaction;
                            command.Parameters.Add(new SqlParameter("@numGuiaId", tablero.viaje.NumGuiaId));
                            command.Parameters.Add(new SqlParameter("@idTractor", tablero.tractor.clave));
                            command.Parameters.Add(new SqlParameter("@idRemolque1", tablero.remolque1?.clave));
                            command.Parameters.Add(new SqlParameter("@idRemolque2", tablero.remolque2?.clave));
                            command.Parameters.Add(new SqlParameter("@idOperador", tablero.operador.clave));
                            command.Parameters.Add(new SqlParameter("@fechaSalidaPlanta", tablero.viaje.FechaHoraViaje));
                            command.Parameters.Add(new SqlParameter("@taller", tablero.taller));
                            command.Parameters.Add(new SqlParameter("@tallerExterno", tablero.tallerExterno));
                            command.ExecuteNonQuery();
                            int id = 0;
                            if (CrearSqlCommand.validarCorrecto(command, out id))
                            {
                                tablero.idTableroControlUnidades = id;
                                command.CommandText = "sp_saveDetalleTrayectoGranja";
                                foreach (DetalleTableroControlUnidades unidades in tablero.listaDetalles)
                                {
                                    command.Parameters.Clear();
                                    foreach (SqlParameter parameter in CrearSqlCommand.getPatametros(true))
                                    {
                                        command.Parameters.Add(parameter);
                                    }
                                    command.Parameters.Add(new SqlParameter("@idDetalle", unidades.idDetalleTableroControlUnidades));
                                    command.Parameters.Add(new SqlParameter("@idTableroControlUnidades", tablero.idTableroControlUnidades));
                                    command.Parameters.Add(new SqlParameter("@consecutivo", unidades.cartaPorte.Consecutivo));
                                    command.Parameters.Add(new SqlParameter("@tiempo", unidades.tiempo));
                                    command.Parameters.Add(new SqlParameter("@fechaLLegada", unidades.fechaLLegada));
                                    command.Parameters.Add(new SqlParameter("@fechaSalida", unidades.fechaSalida));
                                    command.ExecuteNonQuery();
                                    if (CrearSqlCommand.validarCorrecto(command, out id))
                                    {
                                        unidades.idDetalleTableroControlUnidades = id;
                                    }
                                    else
                                    {
                                        transaction.Rollback();
                                        return new OperationResult(command, null);
                                    }
                                }
                            }
                            else
                            {
                                transaction.Rollback();
                                return new OperationResult(command, null);
                            }
                            transaction.Commit();
                            return new OperationResult(command, null);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult(exception);
            }
            return result;
        }

        public OperationResult registrarEntradaGranja(int idTablero, int idDetalleTablero, DateTime fecha)
        {
            OperationResult result;
            using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_registrarEntradaGranja", false))
                    {
                        command.Transaction = transaction;
                        command.Parameters.Add(new SqlParameter("@idTablero", idTablero));
                        command.Parameters.Add(new SqlParameter("@idDetalleTablero", idDetalleTablero));
                        command.Parameters.Add(new SqlParameter("@fecha", fecha));
                        command.ExecuteNonQuery();
                        if (CrearSqlCommand.validarCorrecto(command))
                        {
                            transaction.Commit();
                        }
                        else
                        {
                            transaction.Rollback();
                        }
                        result = new OperationResult(command, null);
                    }
                }
            }
            return result;
        }

        public OperationResult iniciarViajeTablero(int idTablero, int folioViaje, DateTime fecha, string strConnection)
        {
            OperationResult result;
            using (SqlConnection connection = new SqlConnection(strConnection))
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_iniciarViajeEnTablero", false))
                    {
                        command.Transaction = transaction;
                        command.Parameters.Add(new SqlParameter("@idTablero", idTablero));
                        command.Parameters.Add(new SqlParameter("@folioViaje", folioViaje));
                        command.Parameters.Add(new SqlParameter("@fecha", fecha));
                        command.ExecuteNonQuery();
                        if (CrearSqlCommand.validarCorrecto(command))
                        {
                            transaction.Commit();
                        }
                        else
                        {
                            transaction.Rollback();
                        }
                        result = new OperationResult(command, null);
                    }
                }
            }
            return result;
        }
        public OperationResult terminarViajeTablero(int idTablero, int folioViaje, DateTime fecha, string strConnection)
        {
            OperationResult result;
            using (SqlConnection connection = new SqlConnection(strConnection))
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_terminarViajeTablero", false))
                    {
                        command.Transaction = transaction;
                        command.Parameters.Add(new SqlParameter("@idTablero", idTablero));
                        command.Parameters.Add(new SqlParameter("@folioViaje", folioViaje));
                        command.Parameters.Add(new SqlParameter("@fecha", fecha));
                        command.ExecuteNonQuery();
                        if (CrearSqlCommand.validarCorrecto(command))
                        {
                            transaction.Commit();
                        }
                        else
                        {
                            transaction.Rollback();
                        }
                        result = new OperationResult(command, null);
                    }
                }
            }
            return result;
        }
        public OperationResult registrarEntradaGranja(int idTablero, int idDetalleTablero, DateTime fecha, string strConnection)
        {
            OperationResult result;
            using (SqlConnection connection = new SqlConnection(strConnection))
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_registrarEntradaGranja", false))
                    {
                        command.Transaction = transaction;
                        command.Parameters.Add(new SqlParameter("@idTablero", idTablero));
                        command.Parameters.Add(new SqlParameter("@idDetalleTablero", idDetalleTablero));
                        command.Parameters.Add(new SqlParameter("@fecha", fecha));
                        command.ExecuteNonQuery();
                        if (CrearSqlCommand.validarCorrecto(command))
                        {
                            transaction.Commit();
                        }
                        else
                        {
                            transaction.Rollback();
                        }
                        result = new OperationResult(command, null);
                    }
                }
            }
            return result;
        }
        public OperationResult regresarAGranja(int idTablero, int idDetalleTablero, DateTime fecha, string strConnection)
        {
            OperationResult result;
            using (SqlConnection connection = new SqlConnection(strConnection))
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_regresarGranja", false))
                    {
                        command.Transaction = transaction;
                        command.Parameters.Add(new SqlParameter("@idTablero", idTablero));
                        command.Parameters.Add(new SqlParameter("@idDetalleTablero", idDetalleTablero));
                        command.Parameters.Add(new SqlParameter("@fecha", fecha));
                        command.ExecuteNonQuery();
                        if (CrearSqlCommand.validarCorrecto(command))
                        {
                            transaction.Commit();
                        }
                        else
                        {
                            transaction.Rollback();
                        }
                        result = new OperationResult(command, null);
                    }
                }
            }
            return result;
        }

        public OperationResult regresarEventoEnPlanta(int idTablero, string strConnection)
        {
            OperationResult result;
            using (SqlConnection connection = new SqlConnection(strConnection))
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_regresarEventoEnPlanta", false))
                    {
                        command.Transaction = transaction;
                        command.Parameters.Add(new SqlParameter("@idTablero", idTablero));
                        command.ExecuteNonQuery();
                        if (CrearSqlCommand.validarCorrecto(command))
                        {
                            transaction.Commit();
                        }
                        else
                        {
                            transaction.Rollback();
                        }
                        result = new OperationResult(command, null);
                    }
                }
            }
            return result;
        }
        public OperationResult entradaTallerTrayectoDestino(int idTablero, DateTime fecha, string strConnection)
        {
            OperationResult result;
            using (SqlConnection connection = new SqlConnection(strConnection))
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_entradaTallerTrayectoDestino", false))
                    {
                        command.Transaction = transaction;
                        command.Parameters.Add(new SqlParameter("@idTablero", idTablero));
                        command.Parameters.Add(new SqlParameter("@fecha", fecha));
                        command.ExecuteNonQuery();
                        if (CrearSqlCommand.validarCorrecto(command))
                        {
                            transaction.Commit();
                        }
                        else
                        {
                            transaction.Rollback();
                        }
                        result = new OperationResult(command, null);
                    }
                }
            }
            return result;
        }
        public OperationResult salidaTallerTrayectoDestino(int idTablero, DateTime fecha, string strConnection)
        {
            OperationResult result;
            using (SqlConnection connection = new SqlConnection(strConnection))
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_salidaTallerTrayectoDestino", false))
                    {
                        command.Transaction = transaction;
                        command.Parameters.Add(new SqlParameter("@idTablero", idTablero));
                        command.Parameters.Add(new SqlParameter("@fecha", fecha));
                        command.ExecuteNonQuery();
                        if (CrearSqlCommand.validarCorrecto(command))
                        {
                            transaction.Commit();
                        }
                        else
                        {
                            transaction.Rollback();
                        }
                        result = new OperationResult(command, null);
                    }
                }
            }
            return result;
        }

        public OperationResult entradaTallerTrayectoPlanta(int idTablero, DateTime fecha, string strConnection)
        {
            OperationResult result;
            using (SqlConnection connection = new SqlConnection(strConnection))
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_entradaTallerTrayectoPlanta", false))
                    {
                        command.Transaction = transaction;
                        command.Parameters.Add(new SqlParameter("@idTablero", idTablero));
                        command.Parameters.Add(new SqlParameter("@fecha", fecha));
                        command.ExecuteNonQuery();
                        if (CrearSqlCommand.validarCorrecto(command))
                        {
                            transaction.Commit();
                        }
                        else
                        {
                            transaction.Rollback();
                        }
                        result = new OperationResult(command, null);
                    }
                }
            }
            return result;
        }
        public OperationResult salidaTallerTrayectoPlanta(int idTablero, DateTime fecha, string strConnection)
        {
            OperationResult result;
            using (SqlConnection connection = new SqlConnection(strConnection))
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_salidaTallerTrayectoPlanta", false))
                    {
                        command.Transaction = transaction;
                        command.Parameters.Add(new SqlParameter("@idTablero", idTablero));
                        command.Parameters.Add(new SqlParameter("@fecha", fecha));
                        command.ExecuteNonQuery();
                        if (CrearSqlCommand.validarCorrecto(command))
                        {
                            transaction.Commit();
                        }
                        else
                        {
                            transaction.Rollback();
                        }
                        result = new OperationResult(command, null);
                    }
                }
            }
            return result;
        }

        public OperationResult registrarSalidaGranja(int idTablero, int idDetalleTablero, DateTime fecha)
        {
            OperationResult result;
            using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_registrarSalidaGranja", false))
                    {
                        command.Transaction = transaction;
                        command.Parameters.Add(new SqlParameter("@idTablero", idTablero));
                        command.Parameters.Add(new SqlParameter("@idDetalleTablero", idDetalleTablero));
                        command.Parameters.Add(new SqlParameter("@fecha", fecha));
                        command.ExecuteNonQuery();
                        if (CrearSqlCommand.validarCorrecto(command))
                        {
                            transaction.Commit();
                        }
                        else
                        {
                            transaction.Rollback();
                        }
                        result = new OperationResult(command, null);
                    }
                }
            }
            return result;
        }

        public OperationResult registrarSalidaGranja(int idTablero, int idDetalleTablero, DateTime fecha, string strConnection)
        {
            OperationResult result;
            using (SqlConnection connection = new SqlConnection(strConnection))
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_registrarSalidaGranja", false))
                    {
                        command.Transaction = transaction;
                        command.Parameters.Add(new SqlParameter("@idTablero", idTablero));
                        command.Parameters.Add(new SqlParameter("@idDetalleTablero", idDetalleTablero));
                        command.Parameters.Add(new SqlParameter("@fecha", fecha));
                        command.ExecuteNonQuery();
                        if (CrearSqlCommand.validarCorrecto(command))
                        {
                            transaction.Commit();
                        }
                        else
                        {
                            transaction.Rollback();
                        }
                        result = new OperationResult(command, null);
                    }
                }
            }
            return result;
        }

        public OperationResult saveRegistroCSI(string strConnection, ref RegistroCSI registroCSI)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = new SqlConnection(strConnection))
                {
                    connection.Open();
                    using (SqlTransaction transaction = connection.BeginTransaction())
                    {
                        using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_saveRegistroCSI", true))
                        {
                            command.Transaction = transaction;
                            command.Parameters.Add(new SqlParameter("@idRegistro", registroCSI.idRegistroCSI));
                            command.Parameters.Add(new SqlParameter("@serie", registroCSI.serie));
                            command.Parameters.Add(new SqlParameter("@fechaCSI", registroCSI.fechaCSI));
                            command.Parameters.Add(new SqlParameter("@geoCerca", registroCSI.geoCerca));
                            command.Parameters.Add(new SqlParameter("@isEntrada", registroCSI.isEntrada));
                            command.Parameters.Add(new SqlParameter("@isPlanta", registroCSI.isPlanta));
                            command.Parameters.Add(new SqlParameter("@isTaller", registroCSI.isTaller));
                            command.ExecuteNonQuery();
                            int id = 0;
                            if (CrearSqlCommand.validarCorrecto(command, out id))
                            {
                                registroCSI.idRegistroCSI = id;
                                transaction.Commit();
                            }
                            else
                            {
                                transaction.Rollback();
                            }
                            result = new OperationResult(command, null);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult(exception);
            }
            return result;
        }
    }
}
