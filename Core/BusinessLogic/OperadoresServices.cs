﻿using Core.Interfaces;
using Core.Models;
using CoreFletera.Models;
using Core.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoreFletera.BusinessLogic;
namespace Core.BusinessLogic
{
    public class OperadoresServices
    {
        public OperationResult getOperadoresALLorById(int id, int idEmpresa)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getOperadoresALLorById";
                        command.Parameters.Add(new SqlParameter("@id", id));
                        command.Parameters.Add(new SqlParameter("@idEmpresa", idEmpresa));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        connection.Open();
                        var i = command.ExecuteReader();
                        List<Personal> listOperadores = new List<Personal>();

                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                Personal operadores = new Personal();

                                operadores = new mapComunes().mapPersonal(sqlReader);

                                //operadores = (new Personal
                                //{
                                //    clave = (int)sqlReader["persona_idPersonal"],
                                //    nombre = (string)sqlReader["Nombre"],
                                //    //tipo_Contrato = (string)sqlReader["Tipo_contrato"],
                                //    rfid = DBNull.Value.Equals(sqlReader["rfid"]) ? "" : (string)sqlReader["rfid"]
                                //});

                                //operadores.empresa = new Empresa
                                //{
                                //    clave = (int)sqlReader["Clave_empresa"],
                                //    nombre = (string)sqlReader["empresa_nombre"],
                                //    telefono = (string)sqlReader["empresa_telefono"],
                                //    direccion = (string)sqlReader["empresa_direccion"],
                                //    fax = (string)sqlReader["empresa_fax"],
                                //    estado = (string)sqlReader["estado"],
                                //    pais = (string)sqlReader["pais"],
                                //    municipio = (string)sqlReader["municipio"],
                                //    colonia = (string)sqlReader["colonia"],
                                //    cp = (string)sqlReader["cp"],
                                //    rfc = (string)sqlReader["rfc"]
                                //};                               

                                listOperadores.Add(operadores);
                            }
                        }

                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)codigo.Value, result = listOperadores };
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }
        public OperationResult getOperadoresByIdCliente(int idCliente)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = CrearSqlCommand.getCommand(connection, "sp_getOperadoresByIdCliente"))
                    {
                        command.Parameters.Add(new SqlParameter("@idCliente", idCliente));                       
                        connection.Open();
                        var i = command.ExecuteReader();
                        List<Personal> listOperadores = new List<Personal>();

                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                Personal operadores = new Personal();
                                operadores = new mapComunes().mapPersonal(sqlReader);
                                listOperadores.Add(operadores);
                            }
                        }
                        return new OperationResult(command, listOperadores);
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult (x);
            }
        }

        public OperationResult getOperadoresByNombre(string nombre, int idEmpresa = 0, int idCliente = 0)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getChoferByNombre";
                        command.Parameters.Add(new SqlParameter("@nombre", nombre));
                        command.Parameters.Add(new SqlParameter("@idEmpresa", idEmpresa));
                        command.Parameters.Add(new SqlParameter("@idCliente", idCliente));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        connection.Open();
                        var i = command.ExecuteReader();
                        List<Personal> listOperadores = new List<Personal>();

                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                Personal operadores = new Personal();
                                operadores = new mapComunes().mapPersonal(sqlReader);
                                listOperadores.Add(operadores);
                            }
                        }

                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)codigo.Value, result = listOperadores };
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }
        public OperationResult getOperadoresByNombreAndCliente(string nombre, int idCliente)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = CrearSqlCommand.getCommand(connection, "sp_getOperadoresByNombreAndCliente"))
                    {                        
                        command.Parameters.Add(new SqlParameter("@nombre", nombre));
                        command.Parameters.Add(new SqlParameter("@idCliente", idCliente));

                        connection.Open();
                        var i = command.ExecuteReader();
                        List<Personal> listOperadores = new List<Personal>();

                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                Personal operadores = new Personal();
                                operadores = new mapComunes().mapPersonal(sqlReader);
                                listOperadores.Add(operadores);
                            }
                        }
                        return new OperationResult(command, listOperadores);
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult (x);
            }
        }

        public OperationResult getLavadoresByNombre(string nombre, int idEmpresa = 0)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getLavadorByNombre";
                        command.Parameters.Add(new SqlParameter("@nombre", nombre));
                        command.Parameters.Add(new SqlParameter("@idEmpresa", idEmpresa));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        connection.Open();
                        var i = command.ExecuteReader();
                        List<Personal> listOperadores = new List<Personal>();

                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                Personal operadores = new Personal();
                                operadores = new mapComunes().mapPersonal(sqlReader);
                                listOperadores.Add(operadores);
                            }
                        }

                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)codigo.Value, result = listOperadores };
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }

        public OperationResult getLlanterosByNombre(string nombre, int idEmpresa = 0)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getLlanterosByNombre";
                        command.Parameters.Add(new SqlParameter("@nombre", nombre));
                        command.Parameters.Add(new SqlParameter("@idEmpresa", idEmpresa));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        connection.Open();
                        var i = command.ExecuteReader();
                        List<Personal> listOperadores = new List<Personal>();

                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                Personal operadores = new Personal();
                                operadores = new mapComunes().mapPersonal(sqlReader);
                                listOperadores.Add(operadores);
                            }
                        }

                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)codigo.Value, result = listOperadores };
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }

        public OperationResult getMecanicosByNombre(string nombre, int idEmpresa = 0)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getMecanicosByNombre";
                        command.Parameters.Add(new SqlParameter("@nombre", nombre));
                        command.Parameters.Add(new SqlParameter("@idEmpresa", idEmpresa));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        connection.Open();
                        var i = command.ExecuteReader();
                        List<Personal> listOperadores = new List<Personal>();

                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                Personal operadores = new Personal();
                                operadores = new mapComunes().mapPersonal(sqlReader);
                                listOperadores.Add(operadores);
                            }
                        }

                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)codigo.Value, result = listOperadores };
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }

        public OperationResult getPersonalByNombre(string nombre, int idEmpresa = 0)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getPersonalByNombre";
                        command.Parameters.Add(new SqlParameter("@nombre", nombre));
                        command.Parameters.Add(new SqlParameter("@idEmpresa", idEmpresa));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        connection.Open();
                        var i = command.ExecuteReader();
                        List<Personal> listOperadores = new List<Personal>();

                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                Personal operadores = new Personal();
                                operadores = new mapComunes().mapPersonal(sqlReader);                                
                                listOperadores.Add(operadores);
                            }
                        }

                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)codigo.Value, result = listOperadores };
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }

        public OperationResult getDespachadoresByNombre(string nombre = "%")
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getDespachadorByNombre";
                        command.Parameters.Add(new SqlParameter("@nombre", nombre));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        connection.Open();
                        var i = command.ExecuteReader();
                        List<Personal> listOperadores = new List<Personal>();

                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                Personal operadores = new Personal();
                                operadores = new mapComunes().mapPersonal(sqlReader);                                

                                listOperadores.Add(operadores);
                            }
                        }

                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)codigo.Value, result = listOperadores };
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }

        public OperationResult getPersonalALLorById(int id)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getPersonalALLorById";
                        command.Parameters.Add(new SqlParameter("@id", id));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        connection.Open();
                        var i = command.ExecuteReader();
                        List<Personal> listOperadores = new List<Personal>();

                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                Personal operadores = new Personal();
                                operadores = new mapComunes().mapPersonal(sqlReader);
                                //operadores = (new Personal
                                //{
                                //    clave = (int)sqlReader["Clave"],
                                //    nombre = (string)sqlReader["Nombre"],
                                //    //tipo_Contrato = (string)sqlReader["Tipo_contrato"],
                                //    rfid = (string)sqlReader["rfid"]
                                //});

                                //operadores.empresa = new Empresa
                                //{
                                //    clave = (int)sqlReader["Clave_empresa"],
                                //    nombre = (string)sqlReader["empresa_nombre"],
                                //    telefono = (string)sqlReader["empresa_telefono"],
                                //    direccion = (string)sqlReader["empresa_direccion"],
                                //    fax = (string)sqlReader["empresa_fax"],
                                //    estado = (string)sqlReader["estado"],
                                //    pais = (string)sqlReader["pais"],
                                //    municipio = (string)sqlReader["municipio"],
                                //    colonia = (string)sqlReader["colonia"],
                                //    cp = (string)sqlReader["cp"],
                                //    rfc = (string)sqlReader["rfc"]
                                //};

                                //OperationResult serv = new OperationResult();
                                ///////////////////////////////////////////////////////////////////////////////
                                //serv = mapEmpresa((int)sqlReader["Clave_empresa"]);
                                //if (serv.typeResult == ResultTypes.success)
                                //{
                                //    operadores.empresa = (Empresa)serv.result;
                                //}
                                //else
                                //{
                                //    serv.mensaje += "Error el buscar la empresa con clave " + ((int)sqlReader["Clave_empresa"]).ToString();
                                //    return serv;
                                //}
                                /////////////////////////////////////////////////////////////////////////////
                                ////serv = mapDEpartamento((int)sqlReader["Clave_departamento"]);
                                ////if (serv.typeResult == ResultTypes.success)
                                ////{
                                ////    operadores.departamento = (Departamento)serv.result;
                                ////}
                                ////else
                                ////{
                                ////    serv.mensaje += "Error el buscar el departamento con clave " + ((int)sqlReader["Clave_departamento"]).ToString();
                                ////    return serv;
                                ////}
                                /////////////////////////////////////////////////////////////////////////////
                                ////serv = mapCategoria((int)sqlReader["Clave_categoria"]);
                                ////if (serv.typeResult == ResultTypes.success)
                                ////{
                                ////    operadores.categoriaEmpleado = (CategoriaEmpleado)serv.result;
                                ////}
                                ////else
                                ////{
                                ////    serv.mensaje += "Error el buscar la categoria con clave " + ((int)sqlReader["Clave_categoria"]).ToString();
                                ////    return serv;
                                ////}
                                /////////////////////////////////////////////////////////////////////////////

                                listOperadores.Add(operadores);
                            }
                        }

                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)codigo.Value, result = listOperadores };
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }

        private OperationResult mapCategoria(int idCategoria)
        {
            try
            {
                OperationResult resp = new CategoriaEmpleadoSvc().getCategoriaEmpleadoALLorById(idCategoria);
                if (resp.typeResult == ResultTypes.error)
                {
                    return new OperationResult { result = null, valor = 1, mensaje = resp.mensaje };
                }
                else if (resp.typeResult == ResultTypes.recordNotFound)
                {
                    return new OperationResult { result = null, valor = 0, mensaje = resp.mensaje };
                }
                else if (resp.typeResult == ResultTypes.success)
                {
                    List<CategoriaEmpleado> lisTEmpresa = (List<CategoriaEmpleado>)resp.result;
                    return new OperationResult { result = lisTEmpresa[0], valor = 0, mensaje = resp.mensaje };
                }
            }
            catch (Exception e)
            {
                return new OperationResult { result = null, valor = 1, mensaje = e.Message };
            }
            return new OperationResult();
        }

        private OperationResult mapEmpresa(int idEmpresa)
        {
            try
            {
                OperationResult resp = new EmpresaSvc().getEmpresasALLorById(idEmpresa);
                if (resp.typeResult == ResultTypes.error)
                {
                    return new OperationResult { result = null, valor = 1, mensaje = resp.mensaje };
                }
                else if (resp.typeResult == ResultTypes.recordNotFound)
                {
                    return new OperationResult { result = null, valor = 0, mensaje = resp.mensaje };
                }
                else if (resp.typeResult == ResultTypes.success)
                {
                    List<Empresa> lisTEmpresa = (List<Empresa>)resp.result;
                    return new OperationResult { result = lisTEmpresa[0], valor = 0, mensaje = resp.mensaje };
                }
            }
            catch (Exception e)
            {
                return new OperationResult { result = null, valor = 1, mensaje = e.Message };
            }
            return new OperationResult();
        }

        private OperationResult mapDEpartamento(int idDepartamento)
        {
            try
            {
                OperationResult resp = new DepartamentoSvc().getDepartamentosALLorById(idDepartamento);
                if (resp.typeResult == ResultTypes.error)
                {
                    return new OperationResult { result = null, valor = 1, mensaje = resp.mensaje };
                }
                else if (resp.typeResult == ResultTypes.recordNotFound)
                {
                    return new OperationResult { result = null, valor = 0, mensaje = resp.mensaje };
                }
                else if (resp.typeResult == ResultTypes.success)
                {
                    List<Departamento> lisTDep = (List<Departamento>)resp.result;
                    return new OperationResult { result = lisTDep[0], valor = 0, mensaje = resp.mensaje };
                }
            }
            catch (Exception e)
            {
                return new OperationResult { result = null, valor = 1, mensaje = e.Message };
            }
            return new OperationResult();
        }

        public OperationResult getOperadoresViaje(DateTime fechaInicio, DateTime fechaFin)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comand = CrearSqlCommand.getCommand(con, "sp_getOperadoresViaje"))
                    {
                        comand.Parameters.Add(new SqlParameter("@fechaInicio", fechaInicio));
                        comand.Parameters.Add(new SqlParameter("@fechaFin", fechaFin));

                        List<Personal> lista = new List<Personal>();
                        con.Open();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                Personal operador = new mapComunes().mapPersonal(sqlReader);
                                if (operador != null)
                                {
                                    lista.Add(operador);
                                }
                            }
                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        } 
        public OperationResult savePersonal(ref Personal personal)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    con.Open();
                    using (SqlTransaction tran = con.BeginTransaction())
                    {
                        using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_savePersonal", true))
                        {
                            comad.Transaction = tran;
                            comad.Parameters.Add(new SqlParameter("@idPersonal", personal.clave));
                            comad.Parameters.Add(new SqlParameter("@idEmpresa", personal.empresa.clave));
                            comad.Parameters.Add(new SqlParameter("@nombreCompleto", personal.nombre));
                            comad.Parameters.Add(new SqlParameter("@nombres", personal.nombres));
                            comad.Parameters.Add(new SqlParameter("@apPaterno", personal.apellidoPaterno));
                            comad.Parameters.Add(new SqlParameter("@apMaterno", personal.apellidoMaterno));
                            comad.Parameters.Add(new SqlParameter("@idPuesto", personal.puesto.idPuesto));
                            comad.Parameters.Add(new SqlParameter("@direccion", personal.direccion));
                            comad.Parameters.Add(new SqlParameter("@idColonia", personal.colonia == null ? null : (int?)personal.colonia.idColonia));
                            comad.Parameters.Add(new SqlParameter("@telefono", personal.telefono));
                            comad.Parameters.Add(new SqlParameter("@estatus", personal.estatusBD));
                            comad.Parameters.Add(new SqlParameter("@idDepartamento", personal.departamento.clave));
                            comad.Parameters.Add(new SqlParameter("@fechaNacimiento", personal.fechaNacimiento));
                            comad.Parameters.Add(new SqlParameter("@fechaIngreso", personal.fechaIngreso));
                            comad.Parameters.Add(new SqlParameter("@usuario", personal.usuario));

                            comad.Parameters.Add(new SqlParameter("@tipoSangre", personal.tipoSangre));
                            comad.Parameters.Add(new SqlParameter("@calle", personal.calle));
                            comad.Parameters.Add(new SqlParameter("@numero", personal.numero));
                            comad.Parameters.Add(new SqlParameter("@cruce1", personal.cruce1));
                            comad.Parameters.Add(new SqlParameter("@cruce2", personal.cruce2));
                            comad.Parameters.Add(new SqlParameter("@ciudad", personal.ciudad));
                            comad.Parameters.Add(new SqlParameter("@colonia", personal.coloniaStr));
                            comad.Parameters.Add(new SqlParameter("@cp", personal.cp));
                            comad.Parameters.Add(new SqlParameter("@telefonoCelular", personal.telefonoCelular));
                            comad.Parameters.Add(new SqlParameter("@correoElectronico", personal.correoElectronico));
                            comad.Parameters.Add(new SqlParameter("@genero", personal.genero));
                            comad.Parameters.Add(new SqlParameter("@estadoCivil", personal.estadoCivil));
                            comad.Parameters.Add(new SqlParameter("@tallaCamisa", personal.tallaCamisa));
                            comad.Parameters.Add(new SqlParameter("@tallaZapatos", personal.tallaZapatos));
                            

                            comad.ExecuteNonQuery();
                            int id = 0;
                            if (CrearSqlCommand.validarCorrecto(comad, out id))
                            {
                                personal.clave = id;

                                if (personal.listaAjustes.Count > 0)
                                {
                                    comad.CommandText = "sp_saveRegistroAjustes";
                                    DateTime fecha = DateTime.Now;
                                    foreach (var ajuste in personal.listaAjustes)
                                    {
                                        comad.Parameters.Clear();
                                        foreach (var param  in CrearSqlCommand.getPatametros())
                                        {
                                            comad.Parameters.Add(param);
                                        }
                                        comad.Parameters.Add(new SqlParameter("@fecha", fecha));
                                        comad.Parameters.Add(new SqlParameter("@nombreTable", ajuste.nombreTabla));
                                        comad.Parameters.Add(new SqlParameter("@idTabla", ajuste.idTabla));
                                        comad.Parameters.Add(new SqlParameter("@columna", ajuste.nombreColumna));
                                        comad.Parameters.Add(new SqlParameter("@valAnterior", ajuste.valorAnterior));
                                        comad.Parameters.Add(new SqlParameter("@valNuevo", ajuste.valorNuevo));
                                        comad.Parameters.Add(new SqlParameter("@usuario", ajuste.usuario));

                                        comad.ExecuteNonQuery();
                                        if (!CrearSqlCommand.validarCorrecto(comad))
                                        {
                                            tran.Rollback();
                                            return new OperationResult(comad);
                                        }
                                    }
                                }

                                if (personal.contactoEmergencia != null)
                                {
                                    ContactoEmergencia contacto = personal.contactoEmergencia;
                                    comad.Parameters.Clear();
                                    comad.CommandText = "sp_saveContactoEmergencia";
                                    foreach (var param in CrearSqlCommand.getPatametros(true))
                                    {
                                        comad.Parameters.Add(param);
                                    }
                                    comad.Parameters.Add(new SqlParameter("@idContacto", contacto.idContactoEmergencia));
                                    comad.Parameters.Add(new SqlParameter("@idPersonal", personal.clave));
                                    comad.Parameters.Add(new SqlParameter("@parentezco", contacto.parentezco));
                                    comad.Parameters.Add(new SqlParameter("@nombres", contacto.nombres));
                                    comad.Parameters.Add(new SqlParameter("@apellidoPaterno", contacto.apellidoPaterno));
                                    comad.Parameters.Add(new SqlParameter("@apellidoMaterno", contacto.apellidoMaterno));
                                    comad.Parameters.Add(new SqlParameter("@telefonoDomicilio", contacto.telefonoDomicilio));
                                    comad.Parameters.Add(new SqlParameter("@telefonoCelular", contacto.telefonoCelular));
                                    comad.Parameters.Add(new SqlParameter("@usuarioRegistra", contacto.usuario));
                                    int idContacto;
                                    comad.ExecuteNonQuery();
                                    if (CrearSqlCommand.validarCorrecto(comad, out idContacto))
                                    {
                                        personal.contactoEmergencia.idContactoEmergencia = idContacto;
                                    }
                                    else
                                    {
                                        tran.Rollback();
                                        return new OperationResult(comad);
                                    }
                                }
                                if (personal.documentacionPersonal != null)
                                {
                                    var contratacion = personal.documentacionPersonal;
                                    if (contratacion.licenciaEstatal != null)
                                    {
                                        comad.Parameters.Clear();
                                        comad.CommandText = "sp_saveLicenciasConducir";
                                        LicenciaConducir licenciaEstatal = contratacion.licenciaEstatal;
                                        foreach (var param in CrearSqlCommand.getPatametros(true))
                                        {
                                            comad.Parameters.Add(param);
                                        }
                                        comad.Parameters.Add(new SqlParameter("@idLicencia", licenciaEstatal.idLicencia));
                                        comad.Parameters.Add(new SqlParameter("@idPersonal", personal.clave));
                                        comad.Parameters.Add(new SqlParameter("@noLicencia", licenciaEstatal.noLicencia));
                                        comad.Parameters.Add(new SqlParameter("@tipoLicencia", licenciaEstatal.tipoLicencia));
                                        comad.Parameters.Add(new SqlParameter("@fechaVencimiento", licenciaEstatal.vencimiento));
                                        comad.Parameters.Add(new SqlParameter("@clasificacionLicencia", licenciaEstatal.clasificacionLicencia));
                                        comad.Parameters.Add(new SqlParameter("@usuarioRegistro", licenciaEstatal.usuario));
                                        comad.ExecuteNonQuery();
                                        int idLic = 0;
                                        if (CrearSqlCommand.validarCorrecto(comad, out idLic))
                                        {
                                            personal.documentacionPersonal.licenciaEstatal.idLicencia = idLic;
                                        }
                                        else
                                        {
                                            tran.Rollback();
                                            return new OperationResult(comad);
                                        }
                                    }

                                    if (contratacion.licenciaFederal != null)
                                    {
                                        comad.Parameters.Clear();
                                        comad.CommandText = "sp_saveLicenciasConducir";
                                        LicenciaConducir licenciaEstatal = contratacion.licenciaFederal;
                                        foreach (var param in CrearSqlCommand.getPatametros(true))
                                        {
                                            comad.Parameters.Add(param);
                                        }
                                        comad.Parameters.Add(new SqlParameter("@idLicencia", licenciaEstatal.idLicencia));
                                        comad.Parameters.Add(new SqlParameter("@idPersonal", personal.clave));
                                        comad.Parameters.Add(new SqlParameter("@noLicencia", licenciaEstatal.noLicencia));
                                        comad.Parameters.Add(new SqlParameter("@tipoLicencia", licenciaEstatal.tipoLicencia));
                                        comad.Parameters.Add(new SqlParameter("@fechaVencimiento", licenciaEstatal.vencimiento));
                                        comad.Parameters.Add(new SqlParameter("@clasificacionLicencia", licenciaEstatal.clasificacionLicencia));
                                        comad.Parameters.Add(new SqlParameter("@usuarioRegistro", licenciaEstatal.usuario));
                                        comad.ExecuteNonQuery();
                                        int idLic = 0;
                                        if (CrearSqlCommand.validarCorrecto(comad, out idLic))
                                        {
                                            personal.documentacionPersonal.licenciaFederal.idLicencia = idLic;
                                        }
                                        else
                                        {
                                            tran.Rollback();
                                            return new OperationResult(comad);
                                        }
                                    }

                                    comad.Parameters.Clear();
                                    comad.CommandText = "sp_saveDocumentacionPersonal";
                                    
                                    foreach (var param in CrearSqlCommand.getPatametros(true))
                                    {
                                        comad.Parameters.Add(param);
                                    }
                                    comad.Parameters.Add(new SqlParameter("@idDocumentoPersonal", contratacion.idDocumentacionPersonal));
                                    comad.Parameters.Add(new SqlParameter("@idPersonal", personal.clave));
                                    comad.Parameters.Add(new SqlParameter("@noSocial", contratacion.numeroSeguroSocial));
                                    comad.Parameters.Add(new SqlParameter("@CURP", contratacion.CURP));
                                    comad.Parameters.Add(new SqlParameter("@INE", contratacion.INE));
                                    comad.Parameters.Add(new SqlParameter("@RFC", contratacion.RFC));
                                    comad.Parameters.Add(new SqlParameter("@idLicenciaEstatal", contratacion.licenciaEstatal == null ? null : (int?)contratacion.licenciaEstatal.idLicencia));
                                    comad.Parameters.Add(new SqlParameter("@idLicenciaFederal", contratacion.licenciaFederal == null ? null : (int?)contratacion.licenciaFederal.idLicencia));
                                    comad.Parameters.Add(new SqlParameter("@noExamenPsicofisico", contratacion.noExamenPsicofisico));
                                    comad.Parameters.Add(new SqlParameter("@fechaVencimiento", contratacion.fechaVencimientoExamen));
                                    comad.Parameters.Add(new SqlParameter("@banco", contratacion.banco));
                                    comad.Parameters.Add(new SqlParameter("@cuenta", contratacion.cuenta));
                                    comad.Parameters.Add(new SqlParameter("@clabe", contratacion.clabe));
                                    comad.Parameters.Add(new SqlParameter("@observaciones", contratacion.observaciones));
                                    comad.Parameters.Add(new SqlParameter("@usuarioRegistro", contratacion.usuario));
                                    int iddoc = 0;
                                    comad.ExecuteNonQuery();
                                    if (CrearSqlCommand.validarCorrecto(comad, out iddoc))
                                    {
                                        personal.documentacionPersonal.idDocumentacionPersonal = iddoc;
                                    }
                                    else
                                    {
                                        tran.Rollback();
                                        return new OperationResult(comad);
                                    }
                                }

                                if (personal.ingreso != null)
                                {
                                    comad.Parameters.Clear();
                                    comad.CommandText = "sp_saveDatosIngreso";
                                    foreach (var param in CrearSqlCommand.getPatametros(true))
                                    {
                                        comad.Parameters.Add(param);
                                    }
                                    DatosIngresoPersonal dtoIngreso = personal.ingreso;
                                    comad.Parameters.Add(new SqlParameter("@idIngreso", dtoIngreso.idDatoIngreso));
                                    comad.Parameters.Add(new SqlParameter ("@idPersonal", personal.clave));
                                    comad.Parameters.Add(new SqlParameter("@fechaInicio", dtoIngreso.fechaInicio));
                                    comad.Parameters.Add(new SqlParameter("@fechaCapacitacion", dtoIngreso.fechaFinCapacitacion));
                                    comad.Parameters.Add(new SqlParameter("@fechaBaja", dtoIngreso.fechaBaja));
                                    comad.Parameters.Add(new SqlParameter("@motivoBaja", dtoIngreso.motivoBaja));
                                    comad.Parameters.Add(new SqlParameter("@usuarioBaja", dtoIngreso.usuarioBaja));
                                    comad.Parameters.Add(new SqlParameter("@idPuesto", dtoIngreso.puesto.idPuesto));
                                    comad.Parameters.Add(new SqlParameter("@idDepartamento", dtoIngreso.departamento.clave));
                                    comad.Parameters.Add(new SqlParameter("@salarioDiario", dtoIngreso.salarioDiario));
                                    comad.Parameters.Add(new SqlParameter("@salarioHora", dtoIngreso.salarioHora));
                                    comad.Parameters.Add(new SqlParameter("@maximoHora", dtoIngreso.maximoHorasExtra));
                                    comad.Parameters.Add(new SqlParameter("@observaciones", dtoIngreso.observaciones));
                                    comad.Parameters.Add(new SqlParameter("@tipoIngreso", dtoIngreso.tipoIngreso));
                                    comad.Parameters.Add(new SqlParameter("@usuarioRegistro", dtoIngreso.usuarioRegistra));
                                    int idDto = 0;
                                    comad.ExecuteNonQuery();
                                    if (CrearSqlCommand.validarCorrecto(comad, out idDto))
                                    {
                                        personal.ingreso.idDatoIngreso = idDto;
                                    }
                                    else
                                    {
                                        tran.Rollback();
                                        return new OperationResult(comad);
                                    }
                                }

                                if (personal.reingreso != null)
                                {
                                    comad.Parameters.Clear();
                                    comad.CommandText = "sp_saveDatosIngreso";
                                    foreach (var param in CrearSqlCommand.getPatametros(true))
                                    {
                                        comad.Parameters.Add(param);
                                    }
                                    DatosIngresoPersonal dtoIngreso = personal.reingreso;
                                    comad.Parameters.Add(new SqlParameter("@idIngreso", dtoIngreso.idDatoIngreso));
                                    comad.Parameters.Add(new SqlParameter("@idPersonal", personal.clave));
                                    comad.Parameters.Add(new SqlParameter("@fechaInicio", dtoIngreso.fechaInicio));
                                    comad.Parameters.Add(new SqlParameter("@fechaCapacitacion", dtoIngreso.fechaFinCapacitacion));
                                    comad.Parameters.Add(new SqlParameter("@fechaBaja", dtoIngreso.fechaBaja));
                                    comad.Parameters.Add(new SqlParameter("@motivoBaja", dtoIngreso.motivoBaja));
                                    comad.Parameters.Add(new SqlParameter("@usuarioBaja", dtoIngreso.usuarioBaja));
                                    comad.Parameters.Add(new SqlParameter("@idPuesto", dtoIngreso.puesto.idPuesto));
                                    comad.Parameters.Add(new SqlParameter("@idDepartamento", dtoIngreso.departamento.clave));
                                    comad.Parameters.Add(new SqlParameter("@salarioDiario", dtoIngreso.salarioDiario));
                                    comad.Parameters.Add(new SqlParameter("@salarioHora", dtoIngreso.salarioHora));
                                    comad.Parameters.Add(new SqlParameter("@maximoHora", dtoIngreso.maximoHorasExtra));
                                    comad.Parameters.Add(new SqlParameter("@observaciones", dtoIngreso.observaciones));
                                    comad.Parameters.Add(new SqlParameter("@tipoIngreso", dtoIngreso.tipoIngreso));
                                    comad.Parameters.Add(new SqlParameter("@usuarioRegistro", dtoIngreso.usuarioRegistra));
                                    int idDto = 0;
                                    comad.ExecuteNonQuery();
                                    if (CrearSqlCommand.validarCorrecto(comad, out idDto))
                                    {
                                        personal.reingreso.idDatoIngreso = idDto;
                                    }
                                    else
                                    {
                                        tran.Rollback();
                                        return new OperationResult(comad);
                                    }
                                }
                                if (personal.cambioPuesto != null)
                                {
                                    comad.Parameters.Clear();
                                    comad.CommandText = "sp_saveDatosIngreso";
                                    foreach (var param in CrearSqlCommand.getPatametros(true))
                                    {
                                        comad.Parameters.Add(param);
                                    }
                                    DatosIngresoPersonal dtoIngreso = personal.cambioPuesto;
                                    comad.Parameters.Add(new SqlParameter("@idIngreso", dtoIngreso.idDatoIngreso));
                                    comad.Parameters.Add(new SqlParameter("@idPersonal", personal.clave));
                                    comad.Parameters.Add(new SqlParameter("@fechaInicio", dtoIngreso.fechaInicio));
                                    comad.Parameters.Add(new SqlParameter("@fechaCapacitacion", dtoIngreso.fechaFinCapacitacion));
                                    comad.Parameters.Add(new SqlParameter("@fechaBaja", dtoIngreso.fechaBaja));
                                    comad.Parameters.Add(new SqlParameter("@motivoBaja", dtoIngreso.motivoBaja));
                                    comad.Parameters.Add(new SqlParameter("@usuarioBaja", dtoIngreso.usuarioBaja));
                                    comad.Parameters.Add(new SqlParameter("@idPuesto", dtoIngreso.puesto.idPuesto));
                                    comad.Parameters.Add(new SqlParameter("@idDepartamento", dtoIngreso.departamento.clave));
                                    comad.Parameters.Add(new SqlParameter("@salarioDiario", dtoIngreso.salarioDiario));
                                    comad.Parameters.Add(new SqlParameter("@salarioHora", dtoIngreso.salarioHora));
                                    comad.Parameters.Add(new SqlParameter("@maximoHora", dtoIngreso.maximoHorasExtra));
                                    comad.Parameters.Add(new SqlParameter("@observaciones", dtoIngreso.observaciones));
                                    comad.Parameters.Add(new SqlParameter("@tipoIngreso", dtoIngreso.tipoIngreso));
                                    comad.Parameters.Add(new SqlParameter("@usuarioRegistro", dtoIngreso.usuarioRegistra));
                                    int idDto = 0;
                                    comad.ExecuteNonQuery();
                                    if (CrearSqlCommand.validarCorrecto(comad, out idDto))
                                    {
                                        personal.cambioPuesto.idDatoIngreso = idDto;
                                    }
                                    else
                                    {
                                        tran.Rollback();
                                        return new OperationResult(comad);
                                    }
                                }
                            }
                            else
                            {
                                tran.Rollback();
                                return new OperationResult(comad);
                            }

                            tran.Commit();
                            return new OperationResult(comad);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getPersonal_v2()
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getPersonalV2"))
                    {
                        
                        con.Open();
                        List<Personal> listaPersonal = new List<Personal>();
                        using (SqlDataReader sqlReader = comad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                Personal personal = new mapComunes().mapPersonal_v2(sqlReader);
                                if (personal == null)
                                {
                                    return new OperationResult(ResultTypes.error, "ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA.");
                                }
                                listaPersonal.Add(personal);
                            }
                        }
                        return new OperationResult(comad, listaPersonal);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        public OperationResult getPersonalById_v2(int idPersonal)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getPersonalByIdV2"))
                    {
                        comad.Parameters.Add(new SqlParameter("@idPersonal", idPersonal));
                    con.Open();
                        Personal personal = new Personal();
                        using (SqlDataReader sqlReader = comad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                personal = new mapComunes().mapPersonal_v2(sqlReader);
                                if (personal == null)
                                {
                                    return new OperationResult(ResultTypes.error, "ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA.");
                                }
                               
                            }
                        }
                        return new OperationResult(comad, personal);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getPersonal_v2ByClave(int idPersonal)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getPersonalV2ByClave"))
                    {
                        con.Open();
                        comad.Parameters.Add(new SqlParameter("@clave", idPersonal));
                        Personal personal = new Personal();
                        using (SqlDataReader sqlReader = comad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                personal = new mapComunes().mapPersonal_v2(sqlReader);
                                if (personal == null)
                                {
                                    return new OperationResult(ResultTypes.error, "ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA.");
                                }                                
                            }
                        }
                        return new OperationResult(comad, personal);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getPersonalByDepartamentos(List<string> listaDeptos)
        {
            try
            {
                string xml = GenerateXML.generarListaCadena(listaDeptos);
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getPersonalByBepartamentos"))
                    {
                        comad.Parameters.Add(new SqlParameter("@xml", xml));
                        con.Open();
                        List<Personal> listaPersonal = new List<Personal>();
                        using (SqlDataReader sqlReader = comad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                Personal personal = new mapComunes().mapPersonal_v2(sqlReader);
                                if (personal == null)
                                {
                                    return new OperationResult(ResultTypes.error, "ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA.");
                                }
                                listaPersonal.Add(personal);
                            }
                        }
                        return new OperationResult(comad, listaPersonal);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getPersonalByDepartamentosAndPuesto(List<string> listaDeptos, List<string> listaPuestos)
        {
            try
            {
                string xml = GenerateXML.generarListaCadena(listaDeptos);
                string xmlPuestos = GenerateXML.generarListaCadena(listaPuestos);
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getPersonalByBepartamentosAndPuestos"))
                    {
                        comad.Parameters.Add(new SqlParameter("@xml", xml));
                        comad.Parameters.Add(new SqlParameter("@xmlPuestos", xmlPuestos));
                        con.Open();
                        List<Personal> listaPersonal = new List<Personal>();
                        using (SqlDataReader sqlReader = comad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                Personal personal = new mapComunes().mapPersonal_v2(sqlReader);
                                if (personal == null)
                                {
                                    return new OperationResult(ResultTypes.error, "ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA.");
                                }
                                listaPersonal.Add(personal);
                            }
                        }
                        return new OperationResult(comad, listaPersonal);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getPersonalConApoyo()
        {
            try
            {               
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getPersonalConApoyo"))
                    {                       
                        con.Open();
                        List<Personal> listaPersonal = new List<Personal>();
                        using (SqlDataReader sqlReader = comad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                Personal personal = new mapComunes().mapPersonal_v2(sqlReader);
                                if (personal == null)
                                {
                                    return new OperationResult(ResultTypes.error, "ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA.");
                                }
                                listaPersonal.Add(personal);
                            }
                        }
                        return new OperationResult(comad, listaPersonal);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getPersonal_v2ByIdEmpresa(int idEmpresa)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getPersonalV2ByIdEmpresa"))
                    {
                        comad.Parameters.Add(new SqlParameter("@idEmpresa", idEmpresa));
                        con.Open();
                        List<Personal> listaPersonal = new List<Personal>();
                        using (SqlDataReader sqlReader = comad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                Personal personal = new mapComunes().mapPersonal_v2(sqlReader);
                                if (personal == null)
                                {
                                    return new OperationResult(ResultTypes.error, "ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA.");
                                }
                                listaPersonal.Add(personal);
                            }
                        }
                        return new OperationResult(comad, listaPersonal);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        public OperationResult getPersonal_v2ByDepartamento(int idDepto)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getPersonalV2ByDepartamento"))
                    {
                        comad.Parameters.Add(new SqlParameter("@idDepto", idDepto));
                        con.Open();
                        List<Personal> listaPersonal = new List<Personal>();
                        using (SqlDataReader sqlReader = comad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                Personal personal = new mapComunes().mapPersonal_v2(sqlReader);
                                if (personal == null)
                                {
                                    return new OperationResult(ResultTypes.error, "ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA.");
                                }
                                listaPersonal.Add(personal);
                            }
                        }
                        return new OperationResult(comad, listaPersonal);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getPersonal_v2ByIdPersonal(int idPersonal)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getPersonalV2ByIdPersonal"))
                    {
                        comad.Parameters.Add(new SqlParameter("@idPersonal", idPersonal));
                        con.Open();
                        List<Personal> listaPersonal = new List<Personal>();
                        using (SqlDataReader sqlReader = comad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                Personal personal = new mapComunes().mapPersonal_v2(sqlReader);
                                if (personal == null)
                                {
                                    return new OperationResult(ResultTypes.error, "ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA.");
                                }
                                listaPersonal.Add(personal);
                            }
                        }
                        return new OperationResult(comad, listaPersonal);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getPersonal_v2Activos()
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getPersonalV2Activo"))
                    {                        
                        con.Open();
                        List<Personal> listaPersonal = new List<Personal>();
                        using (SqlDataReader sqlReader = comad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                Personal personal = new mapComunes().mapPersonal_v2(sqlReader);
                                if (personal == null)
                                {
                                    return new OperationResult(ResultTypes.error, "ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA.");
                                }
                                listaPersonal.Add(personal);
                            }
                        }
                        return new OperationResult(comad, listaPersonal);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        public OperationResult savePagosApoyos(List<Personal> listaPersonal)
        {
            try
            {
                string xml = GenerateXML.generateListaPagosApoyos(listaPersonal);
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    con.Open();
                    using (SqlTransaction tran = con.BeginTransaction())
                    {
                        using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_saveApoyosPersonal"))
                        {
                            comad.Transaction = tran;
                            comad.Parameters.Add(new SqlParameter("@xml", xml));
                            comad.ExecuteNonQuery();
                            if (CrearSqlCommand.validarCorrecto(comad))
                            {
                                tran.Commit();
                            }
                            else
                            {
                                tran.Rollback();
                            }
                            return new OperationResult(comad);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getListaDatosIngreso(int idPersonal)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getDatosIngresoByIdPersonal"))
                    {
                        comad.Parameters.Add(new SqlParameter("@idPersonal", idPersonal));
                        con.Open();
                        List<DatosIngresoPersonal> listaIngresos = new List<DatosIngresoPersonal>();
                        using (SqlDataReader reader = comad.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                DatosIngresoPersonal datosIngreso = new mapComunes().mapDatosIngreso(reader);
                                if (datosIngreso == null) return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOR DE LA BUSQUEDA");

                                listaIngresos.Add(datosIngreso);
                            }
                        }
                        return new OperationResult(comad, listaIngresos);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getContactoEmergencia(int idPersonal)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getGetContactoEmergenciaByidPersonal"))
                    {
                        comad.Parameters.Add(new SqlParameter("@idPersonal", idPersonal));
                        con.Open();
                        List<ContactoEmergencia> lista = new List<ContactoEmergencia>();
                        using (SqlDataReader reader = comad.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                ContactoEmergencia contacto = new mapComunes().mapContactosEmergencia(reader);
                                if (contacto == null) return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOR DE LA BUSQUEDA");

                                lista.Add(contacto);
                            }
                        }
                        return new OperationResult(comad, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getDocumentosPersonalByIdPersonal(int idPersonal)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getDocumentosPersonalByIdPersonal"))
                    {
                        comad.Parameters.Add(new SqlParameter("@idPersonal", idPersonal));
                        con.Open();
                        List<DocumentacionPersonal> lista = new List<DocumentacionPersonal>();
                        using (SqlDataReader reader = comad.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                DocumentacionPersonal documentacion = new mapComunes().mapDocumentacionPersonal(reader);
                                if (documentacion == null) return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOR DE LA BUSQUEDA");

                                lista.Add(documentacion);
                            }
                        }
                        return new OperationResult(comad, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getLicenciasConducirByIdPersonal(int idPersonal)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getLicenciasConducirByIdPersonal"))
                    {
                        comad.Parameters.Add(new SqlParameter("@idPersonal", idPersonal));
                        con.Open();
                        List<LicenciaConducir> lista = new List<LicenciaConducir>();
                        using (SqlDataReader reader = comad.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                LicenciaConducir licencia = new mapComunes().mapLicenciasConducir(reader);
                                if (licencia == null) return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOR DE LA BUSQUEDA");

                                lista.Add(licencia);
                            }
                        }
                        return new OperationResult(comad, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult sincronizarPersonal(List<Personal> listaPersonal, string usuario)
        {
            try
            {
                string xml = GenerateXML.generarListaPersonalSinc(listaPersonal);
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    con.Open();
                    using (SqlTransaction tran = con.BeginTransaction())
                    {
                        using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_sincronizarPersonal", false, true))
                        {
                            comad.Transaction = tran;
                            comad.Parameters.Add(new SqlParameter("@usuario", usuario));
                            comad.Parameters.Add(new SqlParameter("@xml", xml));
                            comad.ExecuteNonQuery();
                            if (CrearSqlCommand.validarCorrecto(comad))
                            {
                                tran.Commit();
                            }
                            else
                            {
                                tran.Rollback();
                            }
                            return new OperationResult(comad);
                        }
                    }
                }
                return new OperationResult(ResultTypes.success, "");
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
}
