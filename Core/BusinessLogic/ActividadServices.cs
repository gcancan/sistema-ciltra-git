﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using System.Data;
using System.Data.SqlClient;
using Core.Utils;
using Core.BusinessLogic;

namespace CoreFletera.BusinessLogic
{
    public class ActividadServices
    {
        public OperationResult saveActividad(ref Actividad actividad)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    con.Open();
                    using (SqlTransaction tran = con.BeginTransaction())
                    {
                        using (SqlCommand comando = CrearSqlCommand.getCommand(con, "sp_saveActividades", true))
                        {
                            comando.Transaction = tran;
                            comando.Parameters.Add(new SqlParameter("@idActividad", actividad.idActividad));
                            comando.Parameters.Add(new SqlParameter("@CIDPRODUCTO", actividad.CIDPRODUCTO));
                            comando.Parameters.Add(new SqlParameter("@nombre", actividad.nombre));
                            comando.Parameters.Add(new SqlParameter("@costo", actividad.costo));
                            comando.Parameters.Add(new SqlParameter("@Duracion", actividad.duracion));
                            comando.Parameters.Add(new SqlParameter("@CCODIGOPRODUCTO", string.IsNullOrEmpty(actividad.CCODIGOPRODUCTO) ? string.Empty : actividad.CCODIGOPRODUCTO));
                            comando.Parameters.Add(new SqlParameter("@lavadero", actividad.lavadero));
                            comando.Parameters.Add(new SqlParameter("@llantera", actividad.llantera));
                            comando.Parameters.Add(new SqlParameter("@taller", actividad.taller));
                            comando.Parameters.Add(new SqlParameter("@activo", actividad.activo));
                            comando.Parameters.Add(new SqlParameter("@idDivicion", actividad.division == null ? 0 : actividad.division.idDivision));

                            comando.ExecuteNonQuery();
                            if ((int)comando.Parameters["@valor"].Value == 0)
                            {
                                actividad.idActividad = (int)comando.Parameters["@id"].Value;
                                tran.Commit();
                            }
                            else
                            {
                                tran.Rollback();
                            }
                            return new OperationResult(comando);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        public OperationResult saveRelActividadProducto(Actividad actividad)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    con.Open();
                    using (SqlTransaction tran = con.BeginTransaction())
                    {
                        using (SqlCommand comando = CrearSqlCommand.getCommand(con, "sp_saveRelActividadProducto"))
                        {
                            comando.Transaction = tran;

                            foreach (var producto in actividad.listProductosCOM)
                            {
                                comando.Parameters.Clear();
                                foreach (var c in CrearSqlCommand.getPatametros())
                                {
                                    comando.Parameters.Add(c);
                                }
                                comando.Parameters.Add(new SqlParameter("@CIDPRODUCTO", actividad.CIDPRODUCTO));
                                comando.Parameters.Add(new SqlParameter("@idProducto", producto.idProducto));
                                comando.Parameters.Add(new SqlParameter("@cantidad", producto.cantidad));

                                comando.ExecuteNonQuery();
                                if ((int)comando.Parameters["@valor"].Value != 0)
                                {
                                    tran.Rollback();
                                    return new OperationResult(comando);
                                }
                            }
                            tran.Commit();
                            return new OperationResult(comando);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        public OperationResult quitarRelActividadProducto(int CIDPRODUCTO, int idProducto)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    con.Open();
                    using (SqlTransaction tran = con.BeginTransaction())
                    {
                        using (SqlCommand comando = CrearSqlCommand.getCommand(con, "sp_quitarRelActividadProducto"))
                        {
                            comando.Transaction = tran;

                            comando.Parameters.Add(new SqlParameter("@CIDPRODUCTO", CIDPRODUCTO));
                            comando.Parameters.Add(new SqlParameter("@idProducto", idProducto));

                            comando.ExecuteNonQuery();
                            if ((int)comando.Parameters["@valor"].Value != 0)
                            {
                                tran.Rollback();
                                return new OperationResult(comando);
                            }
                            tran.Commit();
                            return new OperationResult(comando);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        internal OperationResult getProductosCOMByActividad(int CIDPRODUCTO)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comand = CrearSqlCommand.getCommand(con, "sp_getProductosCOMByActividad"))
                    {
                        comand.Parameters.Add(new SqlParameter("@CIDPRODUCTO", CIDPRODUCTO));
                        List<ProductosCOM> lista = new List<ProductosCOM>();
                        con.Open();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                ProductosCOM productosCOM = new mapComunes().mapProductosCOM(sqlReader);
                                if (productosCOM != null)
                                {
                                    productosCOM.cantidad = (decimal)sqlReader["Cantidad"];
                                    lista.Add(productosCOM);
                                }
                            }
                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getAllActividades(bool soloActivos = false)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comand = CrearSqlCommand.getCommand(con, "sp_getAllActividades_V2"))
                    {
                        comand.Parameters.Add(new SqlParameter("soloActivos", soloActivos));
                        con.Open();
                        List<Actividad> listaActividad = new List<Actividad>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                Actividad actividad = new mapComunes().mapActividad(sqlReader);
                                if (actividad == null)
                                {
                                    return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");
                                }
                                listaActividad.Add(actividad);
                            }
                        }
                        return new OperationResult(comand, listaActividad);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getAllActividadesByTipoOrdenServ(enumTipoOrdServ enumTipoOrd)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comand = CrearSqlCommand.getCommand(con, "sp_getAllActividadesByTipoOrdenServ"))
                    {
                        comand.Parameters.Add(new SqlParameter("@taller", enumTipoOrd == enumTipoOrdServ.TALLER));
                        comand.Parameters.Add(new SqlParameter("@lavadero", enumTipoOrd == enumTipoOrdServ.LAVADERO));
                        comand.Parameters.Add(new SqlParameter("@llantas", enumTipoOrd == enumTipoOrdServ.LLANTAS));
                        con.Open();
                        List<Actividad> listaActividad = new List<Actividad>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                Actividad actividad = new mapComunes().mapActividad(sqlReader);
                                if (actividad == null)
                                {
                                    return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");
                                }
                                listaActividad.Add(actividad);
                            }
                        }
                        return new OperationResult(comand, listaActividad);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
}
