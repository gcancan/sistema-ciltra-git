﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Utils;
using Core.Models;
using CoreFletera.Models;
using System.Data;
using System.Data.SqlClient;
namespace CoreFletera.BusinessLogic
{
    public class BitacoraCambioPreciosService
    {
        public OperationResult getBitacoraPrecios(DateTime fechaInicial, DateTime fechaFinal)
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getBitacoraPrecios";
                        command.Parameters.Add(new SqlParameter("@fechaInicial", fechaInicial));
                        command.Parameters.Add(new SqlParameter("@fechaFinal", fechaFinal));
                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var spMensaje = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                        connection.Open();
                        List<BitacoraCambioPrecios> listBitacora = new List<BitacoraCambioPrecios>();
                        using (SqlDataReader sqlReader = command.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                BitacoraCambioPrecios bitacora = new BitacoraCambioPrecios
                                {
                                    idBitacoraCambioPrecios = (int)sqlReader["idBitacoraCambioPrecios"],
                                    fecha = (DateTime)sqlReader["fecha"],
                                    usuario = (string)sqlReader["usuario"],
                                    cliente = (string)sqlReader["cliente"],
                                    tipoPrecio =(eTiposPrecios)sqlReader["tipoPrecio"],
                                    precioAnterior =(decimal)sqlReader["precioAnterior"],
                                    precioNuevo = (decimal)sqlReader["precioNuevo"]
                                };
                                listBitacora.Add(bitacora);
                            }
                        }

                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = listBitacora };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }

        internal OperationResult getDetalleBitacoraCambioPrecios(int idBitacoraCambioPrecios)
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getDetalleBitacoraCambioPrecios";
                        command.Parameters.Add(new SqlParameter("@idBitacoraCambioPrecios", idBitacoraCambioPrecios));
                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var spMensaje = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                        connection.Open();
                        List<DetallesBitacoraCambioPrecios> listDetalleBitacora = new List<DetallesBitacoraCambioPrecios>();
                        using (SqlDataReader sqlReader = command.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                DetallesBitacoraCambioPrecios detalle = new DetallesBitacoraCambioPrecios
                                {
                                    idDetalleBitacoraCambioPrecios = (int)sqlReader["idDetalleBitacoraCambioPrecios"],
                                    idBitacoraCambioPrecios = (int)sqlReader["idBitacoraCambioPrecios"],
                                    destino = (string)sqlReader["destino"]
                                    
                                };
                                listDetalleBitacora.Add(detalle);
                            }
                        }

                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = listDetalleBitacora };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }
    }
}
