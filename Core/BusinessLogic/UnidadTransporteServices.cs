﻿namespace Core.BusinessLogic
{
    using Core.Interfaces;
    using Core.Models;
    using Core.Utils;
    using CoreFletera.BusinessLogic;
    using CoreFletera.Models;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Runtime.InteropServices;

    public class UnidadTransporteServices
    {
        public OperationResult buscarRemolques(int idEmpresa = 0, string idUnidad = "%")
        {
            OperationResult result;
            string str = "";
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_buscarRemolques";
                        command.Parameters.Add(new SqlParameter("@id", idUnidad));
                        command.Parameters.Add(new SqlParameter("@idEmpresa", idEmpresa));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        List<UnidadTransporte> list = new List<UnidadTransporte>();
                        using (SqlDataReader reader2 = reader)
                        {
                            while (reader2.Read())
                            {
                                UnidadTransporte item = new UnidadTransporte();
                                UnidadTransporte transporte1 = new UnidadTransporte
                                {
                                    clave = (string)reader2["Clave"],
                                    estatus = (string)reader2["Estatus"],
                                    rfid = (string)reader2["rfid"]
                                };
                                Empresa empresa1 = new Empresa
                                {
                                    clave = (int)reader2["Clave_empresa"],
                                    nombre = (string)reader2["empresa_nombre"],
                                    telefono = (string)reader2["empresa_telefono"],
                                    direccion = (string)reader2["empresa_direccion"],
                                    fax = (string)reader2["empresa_fax"]
                                };
                                transporte1.empresa = empresa1;
                                TipoUnidad unidad1 = new TipoUnidad
                                {
                                    clave = (int)reader2["Clave_tipo_de_unidad"],
                                    clasificacion = (string)reader2["tipoUnidad_clasificacion"],
                                    descripcion = (string)reader2["tipoUnidad_descripcion"],
                                    NoEjes = (string)reader2["tipoUnidad_noEjes"],
                                    NoLlantas = (string)reader2["tipoUnidad_NoLlantas"],
                                    TonelajeMax = Convert.ToDecimal((string)reader2["tipoUnidad_TonelajeMax"])
                                };
                                transporte1.tipoUnidad = unidad1;
                                item = transporte1;
                                list.Add(item);
                            }
                        }
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = list
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message + str,
                    result = null
                };
            }
            return result;
        }

        internal OperationResult getDollys(int idEmpresa, string clave = "%", int idZonaOperativa = 0, int idCliente = 0)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getDollys";
                        command.Parameters.Add(new SqlParameter("@clave", clave));
                        command.Parameters.Add(new SqlParameter("@idEmpresa", idEmpresa));
                        command.Parameters.Add(new SqlParameter("@idZonaOperativa", idZonaOperativa));
                        command.Parameters.Add(new SqlParameter("@idCliente", idCliente));

                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        List<UnidadTransporte> list = new List<UnidadTransporte>();
                        using (SqlDataReader reader2 = reader)
                        {
                            while (reader2.Read())
                            {
                                UnidadTransporte item = new UnidadTransporte();
                                UnidadTransporte transporte1 = new UnidadTransporte
                                {
                                    clave = (string)reader2["Clave"],
                                    estatus = (string)reader2["Estatus"],
                                    rfid = (string)reader2["rfid"]
                                };
                                Empresa empresa1 = new Empresa
                                {
                                    clave = (int)reader2["Clave_empresa"],
                                    nombre = (string)reader2["empresa_nombre"],
                                    telefono = (string)reader2["empresa_telefono"],
                                    direccion = (string)reader2["empresa_direccion"],
                                    fax = (string)reader2["empresa_fax"]
                                };
                                transporte1.empresa = empresa1;
                                TipoUnidad unidad1 = new TipoUnidad
                                {
                                    clave = (int)reader2["Clave_tipo_de_unidad"],
                                    clasificacion = (string)reader2["tipoUnidad_clasificacion"],
                                    descripcion = (string)reader2["tipoUnidad_descripcion"],
                                    NoEjes = (string)reader2["tipoUnidad_noEjes"],
                                    NoLlantas = (string)reader2["tipoUnidad_NoLlantas"],
                                    TonelajeMax = Convert.ToDecimal((string)reader2["tipoUnidad_TonelajeMax"]),
                                    bCombustible = !DBNull.Value.Equals(reader2["tipoUnidad_bCombustible"]) && ((bool)reader2["tipoUnidad_bCombustible"])
                                };
                                transporte1.tipoUnidad = unidad1;
                                transporte1.tipoMotor = DBNull.Value.Equals(reader2["motor_idTipoMotor"]) ? null : new TipoMotor();
                                item = transporte1;
                                list.Add(item);
                            }
                        }
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = list
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
            return result;
        }

        internal OperationResult getDollysSinFlota(int idEmpresa, string clave = "%")
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getDollysSinFlota";
                        command.Parameters.Add(new SqlParameter("@clave", clave));
                        command.Parameters.Add(new SqlParameter("@idEmpresa", idEmpresa));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        List<UnidadTransporte> list = new List<UnidadTransporte>();
                        using (SqlDataReader reader2 = reader)
                        {
                            while (reader2.Read())
                            {
                                UnidadTransporte item = new UnidadTransporte();
                                UnidadTransporte transporte1 = new UnidadTransporte
                                {
                                    clave = (string)reader2["Clave"],
                                    estatus = (string)reader2["Estatus"],
                                    rfid = (string)reader2["rfid"]
                                };
                                Empresa empresa1 = new Empresa
                                {
                                    clave = (int)reader2["Clave_empresa"],
                                    nombre = (string)reader2["empresa_nombre"],
                                    telefono = (string)reader2["empresa_telefono"],
                                    direccion = (string)reader2["empresa_direccion"],
                                    fax = (string)reader2["empresa_fax"]
                                };
                                transporte1.empresa = empresa1;
                                TipoUnidad unidad1 = new TipoUnidad
                                {
                                    clave = (int)reader2["Clave_tipo_de_unidad"],
                                    clasificacion = (string)reader2["tipoUnidad_clasificacion"],
                                    descripcion = (string)reader2["tipoUnidad_descripcion"],
                                    NoEjes = (string)reader2["tipoUnidad_noEjes"],
                                    NoLlantas = (string)reader2["tipoUnidad_NoLlantas"],
                                    TonelajeMax = Convert.ToDecimal((string)reader2["tipoUnidad_TonelajeMax"]),
                                    bCombustible = !DBNull.Value.Equals(reader2["tipoUnidad_bCombustible"]) && ((bool)reader2["tipoUnidad_bCombustible"])
                                };
                                transporte1.tipoUnidad = unidad1;
                                transporte1.tipoMotor = DBNull.Value.Equals(reader2["motor_idTipoMotor"]) ? null : new TipoMotor();
                                item = transporte1;
                                list.Add(item);
                            }
                        }
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = list
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
            return result;
        }

        internal OperationResult getTolvas(int idEmpresa, string clave = "%", int idZonaOperativa = 0, int idCliente = 0)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getTolvas"))
                    {                        
                        command.Parameters.Add(new SqlParameter("@clave", clave));
                        command.Parameters.Add(new SqlParameter("@idEmpresa", idEmpresa));
                        command.Parameters.Add(new SqlParameter("@idZonaOperativa", idZonaOperativa));
                        command.Parameters.Add(new SqlParameter("@idCliente", idCliente));
                        
                        connection.Open();
                        
                        List<UnidadTransporte> list = new List<UnidadTransporte>();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                UnidadTransporte unidad = new mapComunes().mapUnidadTransporte(reader);
                                if (unidad == null) return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");
                                list.Add(unidad);
                            }
                        }
                        return new OperationResult(command, list);
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
            return result;
        }
        internal OperationResult getTolvasSinFlota(int idEmpresa, string clave = "%")
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getTolvasSinFlota";
                        command.Parameters.Add(new SqlParameter("@clave", clave));
                        command.Parameters.Add(new SqlParameter("@idEmpresa", idEmpresa));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        List<UnidadTransporte> list = new List<UnidadTransporte>();
                        using (SqlDataReader reader2 = reader)
                        {
                            while (reader2.Read())
                            {
                                UnidadTransporte item = new UnidadTransporte();
                                UnidadTransporte transporte1 = new UnidadTransporte
                                {
                                    clave = (string)reader2["Clave"],
                                    estatus = (string)reader2["Estatus"],
                                    rfid = (string)reader2["rfid"]
                                };
                                Empresa empresa1 = new Empresa
                                {
                                    clave = (int)reader2["Clave_empresa"],
                                    nombre = (string)reader2["empresa_nombre"],
                                    telefono = (string)reader2["empresa_telefono"],
                                    direccion = (string)reader2["empresa_direccion"],
                                    fax = (string)reader2["empresa_fax"]
                                };
                                transporte1.empresa = empresa1;
                                TipoUnidad unidad1 = new TipoUnidad
                                {
                                    clave = (int)reader2["Clave_tipo_de_unidad"],
                                    clasificacion = (string)reader2["tipoUnidad_clasificacion"],
                                    descripcion = (string)reader2["tipoUnidad_descripcion"],
                                    NoEjes = (string)reader2["tipoUnidad_noEjes"],
                                    NoLlantas = (string)reader2["tipoUnidad_NoLlantas"],
                                    TonelajeMax = Convert.ToDecimal((string)reader2["tipoUnidad_TonelajeMax"]),
                                    bCombustible = !DBNull.Value.Equals(reader2["tipoUnidad_bCombustible"]) && ((bool)reader2["tipoUnidad_bCombustible"])
                                };
                                transporte1.tipoUnidad = unidad1;
                                transporte1.tipoMotor = DBNull.Value.Equals(reader2["motor_idTipoMotor"]) ? null : new TipoMotor();
                                item = transporte1;
                                list.Add(item);
                            }
                        }
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = list
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
            return result;
        }

        internal OperationResult getTractores(int idEmpresa, string clave = "%", int idZonaOperativa = 0, int idCliente = 0)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getTractores"))
                    {                       
                        command.Parameters.Add(new SqlParameter("@clave", clave));
                        command.Parameters.Add(new SqlParameter("@idEmpresa", idEmpresa));
                        command.Parameters.Add(new SqlParameter("@idZonaOperativa", idZonaOperativa));
                        command.Parameters.Add(new SqlParameter("@idCliente", idCliente));
                        
                        connection.Open();

                        List<UnidadTransporte> list = new List<UnidadTransporte>();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                UnidadTransporte unidad = new mapComunes().mapUnidadTransporte(reader);
                                if (unidad == null) return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");
                                list.Add(unidad);
                            }
                        }
                        return new OperationResult(command, list);
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
            return result;
        }
        internal OperationResult getTractoresByOperacion(int idEmpresa, int idZonaOperativa, int idCliente)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getTractoresByOperacion"))
                    {
                        command.Parameters.Add(new SqlParameter("@idEmpresa", idEmpresa));
                        command.Parameters.Add(new SqlParameter("@idZonaOperativa", idZonaOperativa));
                        command.Parameters.Add(new SqlParameter("@idCliente", idCliente));

                        connection.Open();
                        List<UnidadTransporte> list = new List<UnidadTransporte>();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                UnidadTransporte unidad = new mapComunes().mapUnidadTransporte(reader);
                                if (unidad == null) return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");
                                list.Add(unidad);
                            }
                        }
                        return new OperationResult(command, list);
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
            return result;
        }
        internal OperationResult getUnidadesCarganByEmpresaZonaOperativa(int idEmpresa, int idZonaOperativa)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getUnidadesCarganByEmpresaZonaOperativa"))
                    {
                        command.Parameters.Add(new SqlParameter("@idEmpresa", idEmpresa));
                        command.Parameters.Add(new SqlParameter("@idZonaOperativa", idZonaOperativa));
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        List<UnidadTransporte> list = new List<UnidadTransporte>();
                        using (SqlDataReader reader2 = reader)
                        {
                            while (reader2.Read())
                            {
                                UnidadTransporte item = new UnidadTransporte();
                                UnidadTransporte transporte1 = new UnidadTransporte
                                {
                                    clave = (string)reader2["Clave"],
                                    estatus = (string)reader2["Estatus"],
                                    rfid = (string)reader2["rfid"]
                                };
                                Empresa empresa1 = new Empresa
                                {
                                    clave = (int)reader2["Clave_empresa"],
                                    nombre = (string)reader2["empresa_nombre"],
                                    telefono = (string)reader2["empresa_telefono"],
                                    direccion = (string)reader2["empresa_direccion"],
                                    fax = (string)reader2["empresa_fax"]
                                };
                                transporte1.empresa = empresa1;
                                TipoUnidad unidad1 = new TipoUnidad
                                {
                                    clave = (int)reader2["Clave_tipo_de_unidad"],
                                    clasificacion = (string)reader2["tipoUnidad_clasificacion"],
                                    descripcion = (string)reader2["tipoUnidad_descripcion"],
                                    NoEjes = (string)reader2["tipoUnidad_noEjes"],
                                    NoLlantas = (string)reader2["tipoUnidad_NoLlantas"],
                                    TonelajeMax = Convert.ToDecimal((string)reader2["tipoUnidad_TonelajeMax"]),
                                    bCombustible = !DBNull.Value.Equals(reader2["tipoUnidad_bCombustible"]) && ((bool)reader2["tipoUnidad_bCombustible"])
                                };
                                transporte1.tipoUnidad = unidad1;
                                transporte1.tipoMotor = DBNull.Value.Equals(reader2["motor_idTipoMotor"]) ? null : new TipoMotor();
                                item = transporte1;
                                list.Add(item);
                            }
                        }
                        return new OperationResult(command, list);
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
            return result;
        }
        internal OperationResult getRemolquesByOperacion(int idEmpresa, int idZonaOperativa, int idCliente)
        {
            OperationResult result;
            string idUnidad = string.Empty;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getRemolquesByOperacion"))
                    {
                        command.Parameters.Add(new SqlParameter("@idEmpresa", idEmpresa));
                        command.Parameters.Add(new SqlParameter("@idZonaOperativa", idZonaOperativa));
                        command.Parameters.Add(new SqlParameter("@idCliente", idCliente));

                        connection.Open();

                        List<UnidadTransporte> list = new List<UnidadTransporte>();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                UnidadTransporte unidad = new mapComunes().mapUnidadTransporte(reader);
                                if (unidad == null) return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");
                                list.Add(unidad);
                            }
                        }
                        return new OperationResult(command, list);
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
            return result;
        }

        internal OperationResult getDollysByOperacion(int idEmpresa, int idZonaOperativa, int idCliente)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getDollysByOperacion"))
                    {
                        command.Parameters.Add(new SqlParameter("@idEmpresa", idEmpresa));
                        command.Parameters.Add(new SqlParameter("@idZonaOperativa", idZonaOperativa));
                        command.Parameters.Add(new SqlParameter("@idCliente", idCliente));

                        connection.Open();
                        
                        List<UnidadTransporte> list = new List<UnidadTransporte>();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                UnidadTransporte unidad = new mapComunes().mapUnidadTransporte(reader);
                                if (unidad == null) return new OperationResult(ResultTypes.error, "ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");
                                list.Add(unidad);
                            }
                        }
                        return new OperationResult(command, list);
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
            return result;
        }

        internal OperationResult getTractoresSinFlota(int idEmpresa, string clave = "%")
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getTractoresSinFlota";
                        command.Parameters.Add(new SqlParameter("@clave", clave));
                        command.Parameters.Add(new SqlParameter("@idEmpresa", idEmpresa));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        List<UnidadTransporte> list = new List<UnidadTransporte>();
                        using (SqlDataReader reader2 = reader)
                        {
                            while (reader2.Read())
                            {
                                UnidadTransporte item = new UnidadTransporte();
                                UnidadTransporte transporte1 = new UnidadTransporte
                                {
                                    clave = (string)reader2["Clave"],
                                    estatus = (string)reader2["Estatus"],
                                    rfid = (string)reader2["rfid"]
                                };
                                Empresa empresa1 = new Empresa
                                {
                                    clave = (int)reader2["Clave_empresa"],
                                    nombre = (string)reader2["empresa_nombre"],
                                    telefono = (string)reader2["empresa_telefono"],
                                    direccion = (string)reader2["empresa_direccion"],
                                    fax = (string)reader2["empresa_fax"]
                                };
                                transporte1.empresa = empresa1;
                                TipoUnidad unidad1 = new TipoUnidad
                                {
                                    clave = (int)reader2["Clave_tipo_de_unidad"],
                                    clasificacion = (string)reader2["tipoUnidad_clasificacion"],
                                    descripcion = (string)reader2["tipoUnidad_descripcion"],
                                    NoEjes = (string)reader2["tipoUnidad_noEjes"],
                                    NoLlantas = (string)reader2["tipoUnidad_NoLlantas"],
                                    TonelajeMax = Convert.ToDecimal((string)reader2["tipoUnidad_TonelajeMax"]),
                                    bCombustible = !DBNull.Value.Equals(reader2["tipoUnidad_bCombustible"]) && ((bool)reader2["tipoUnidad_bCombustible"])
                                };
                                transporte1.tipoUnidad = unidad1;
                                transporte1.tipoMotor = DBNull.Value.Equals(reader2["motor_idTipoMotor"]) ? null : new TipoMotor();
                                item = transporte1;
                                list.Add(item);
                            }
                        }
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = list
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
            return result;
        }

        public OperationResult getUnidadBySerieGps(string strConnection, string serie)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = new SqlConnection(strConnection))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getUnidadesBySerie", false))
                    {
                        command.Parameters.Add(new SqlParameter("@serie", serie));
                        UnidadTransporte unidad = new UnidadTransporte();
                        connection.Open();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                unidad = new mapComunes().mapUnidades("", reader);
                                if (unidad == null)
                                {
                                    return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDAS", null);
                                }
                            }
                        }
                        result = new OperationResult(command, unidad);
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult(exception);
            }
            return result;
        }

        public OperationResult getUnidadesALLorById(int idEmpresa, string id)
        {
            OperationResult result;
            string str = "";
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getUnidadesALLorById"))
                    {                        
                        command.Parameters.Add(new SqlParameter("@id", id));
                        command.Parameters.Add(new SqlParameter("@idEmpresa", idEmpresa));
                        
                        connection.Open();
                        List<UnidadTransporte> list = new List<UnidadTransporte>();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                UnidadTransporte unidad = new mapComunes().mapUnidadTransporte(reader);
                                if (unidad == null) return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");
                                list.Add(unidad);
                            }
                        }
                        return new OperationResult(command, list);
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message + str,
                    result = null
                };
            }
            return result;
        }

        public OperationResult getUnidadesByGrupos(int idEmpresa, List<string> listaCadena)
        {
            OperationResult result;
            try
            {
                string str = GenerateXML.generarListaCadena(listaCadena);
                if (string.IsNullOrEmpty(str))
                {
                    result = new OperationResult(ResultTypes.warning, "OCURRIO UN ERROR AL ARMAR EL XML", null);
                }
                else
                {
                    using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                    {
                        using (SqlCommand command = connection.CreateCommand())
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.CommandText = "sp_getUnidadesByGrupos";
                            command.Parameters.Add(new SqlParameter("@xml", str));
                            command.Parameters.Add(new SqlParameter("@idEmpresa", idEmpresa));
                            SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                            {
                                Direction = ParameterDirection.Output
                            };
                            SqlParameter parameter = command.Parameters.Add(parameter1);
                            SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                            {
                                Direction = ParameterDirection.Output
                            };
                            SqlParameter parameter2 = command.Parameters.Add(parameter3);
                            connection.Open();
                            SqlDataReader reader = command.ExecuteReader();
                            List<UnidadTransporte> list = new List<UnidadTransporte>();
                            using (SqlDataReader reader2 = reader)
                            {
                                while (reader2.Read())
                                {
                                    UnidadTransporte transporte1 = new UnidadTransporte
                                    {
                                        clave = (string)reader2["Clave"],
                                        estatus = (string)reader2["Estatus"],
                                        rfid = (string)reader2["rfid"]
                                    };
                                    Empresa empresa1 = new Empresa
                                    {
                                        clave = (int)reader2["Clave_empresa"],
                                        nombre = (string)reader2["empresa_nombre"],
                                        telefono = (string)reader2["empresa_telefono"],
                                        direccion = (string)reader2["empresa_direccion"],
                                        fax = (string)reader2["empresa_fax"]
                                    };
                                    transporte1.empresa = empresa1;
                                    TipoUnidad unidad1 = new TipoUnidad
                                    {
                                        clave = (int)reader2["Clave_tipo_de_unidad"],
                                        clasificacion = (string)reader2["tipoUnidad_clasificacion"],
                                        descripcion = (string)reader2["tipoUnidad_descripcion"],
                                        NoEjes = (string)reader2["tipoUnidad_noEjes"],
                                        NoLlantas = (string)reader2["tipoUnidad_NoLlantas"],
                                        TonelajeMax = Convert.ToDecimal((string)reader2["tipoUnidad_TonelajeMax"]),
                                        bCombustible = !DBNull.Value.Equals(reader2["tipoUnidad_bCombustible"]) && ((bool)reader2["tipoUnidad_bCombustible"])
                                    };
                                    transporte1.tipoUnidad = unidad1;
                                    transporte1.tipoMotor = DBNull.Value.Equals(reader2["motor_idTipoMotor"]) ? null : new TipoMotor();
                                    transporte1.ubicacion = (EstatusUbicacion)reader2["estatusUbicacion"];
                                    UnidadTransporte item = transporte1;
                                    list.Add(item);
                                }
                            }
                            result = new OperationResult
                            {
                                valor = new int?((int)parameter.Value),
                                mensaje = (string)parameter2.Value,
                                result = list
                            };
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
            return result;
        }

        private OperationResult mapDEpartamento(int idDepartamento)
        {
            try
            {
                OperationResult result = new DepartamentoSvc().getDepartamentosALLorById(idDepartamento);
                if (result.typeResult == ResultTypes.error)
                {
                    return new OperationResult
                    {
                        result = null,
                        valor = 1,
                        mensaje = result.mensaje
                    };
                }
                if (result.typeResult == ResultTypes.recordNotFound)
                {
                    return new OperationResult
                    {
                        result = null,
                        valor = 0,
                        mensaje = result.mensaje
                    };
                }
                if (result.typeResult == ResultTypes.success)
                {
                    List<Departamento> list = (List<Departamento>)result.result;
                    return new OperationResult
                    {
                        result = list[0],
                        valor = 0,
                        mensaje = result.mensaje
                    };
                }
            }
            catch (Exception exception)
            {
                return new OperationResult
                {
                    result = null,
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            return new OperationResult();
        }

        private OperationResult mapEmpresa(int idEmpresa)
        {
            try
            {
                OperationResult result = new EmpresaSvc().getEmpresasALLorById(idEmpresa);
                if (result.typeResult == ResultTypes.error)
                {
                    return new OperationResult
                    {
                        result = null,
                        valor = 1,
                        mensaje = result.mensaje
                    };
                }
                if (result.typeResult == ResultTypes.recordNotFound)
                {
                    return new OperationResult
                    {
                        result = null,
                        valor = 0,
                        mensaje = result.mensaje
                    };
                }
                if (result.typeResult == ResultTypes.success)
                {
                    List<Empresa> list = (List<Empresa>)result.result;
                    return new OperationResult
                    {
                        result = list[0],
                        valor = 0,
                        mensaje = result.mensaje
                    };
                }
            }
            catch (Exception exception)
            {
                return new OperationResult
                {
                    result = null,
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            return new OperationResult();
        }

        private OperationResult mapMarca(int idMarca)
        {
            try
            {
                OperationResult result = new MarcasSvc().getMarcasALLorById(idMarca);
                if (result.typeResult == ResultTypes.error)
                {
                    return new OperationResult
                    {
                        result = null,
                        valor = 1,
                        mensaje = result.mensaje
                    };
                }
                if (result.typeResult == ResultTypes.recordNotFound)
                {
                    return new OperationResult
                    {
                        result = null,
                        valor = 0,
                        mensaje = result.mensaje
                    };
                }
                if (result.typeResult == ResultTypes.success)
                {
                    List<Marca> list = (List<Marca>)result.result;
                    return new OperationResult
                    {
                        result = list[0],
                        valor = 0,
                        mensaje = result.mensaje
                    };
                }
            }
            catch (Exception exception)
            {
                return new OperationResult
                {
                    result = null,
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            return new OperationResult();
        }

        private OperationResult mapTipoTransporte(int idTipoTransporte)
        {
            try
            {
                OperationResult result = new TipoUnidadSvc().getTiposUnidadesALLorById(idTipoTransporte);
                if (result.typeResult == ResultTypes.error)
                {
                    return new OperationResult
                    {
                        result = null,
                        valor = 1,
                        mensaje = result.mensaje
                    };
                }
                if (result.typeResult == ResultTypes.recordNotFound)
                {
                    return new OperationResult
                    {
                        result = null,
                        valor = 0,
                        mensaje = result.mensaje
                    };
                }
                if (result.typeResult == ResultTypes.success)
                {
                    List<TipoUnidad> list = (List<TipoUnidad>)result.result;
                    return new OperationResult
                    {
                        result = list[0],
                        valor = 0,
                        mensaje = result.mensaje
                    };
                }
            }
            catch (Exception exception)
            {
                return new OperationResult
                {
                    result = null,
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            return new OperationResult();
        }
        public OperationResult saveUnidadTransporte(ref UnidadTransporte unidadTransporte)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    con.Open();
                    using (SqlTransaction tran = con.BeginTransaction())
                    {
                        using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_saveUnidadTransporte"))
                        {
                            comad.Transaction = tran;
                            comad.Parameters.Add(new SqlParameter("@accion", (int)unidadTransporte.tipoAccion));
                            comad.Parameters.Add(new SqlParameter("@idUnidadTrans", unidadTransporte.clave));
                            comad.Parameters.Add(new SqlParameter("@descripcionUni", unidadTransporte.tipoUnidad.descripcion));
                            comad.Parameters.Add(new SqlParameter("@idTipoUnidad", unidadTransporte.tipoUnidad.clave));
                            comad.Parameters.Add(new SqlParameter("@idMarca", unidadTransporte.marca.clave));
                            comad.Parameters.Add(new SqlParameter("@idModelo", unidadTransporte.modelo == null ? null : (int?)unidadTransporte.modelo.idModelo));
                            comad.Parameters.Add(new SqlParameter("@idEmpresa", unidadTransporte.empresa.clave));
                            comad.Parameters.Add(new SqlParameter("@estatus", unidadTransporte.estatus));
                            comad.Parameters.Add(new SqlParameter("@capTanque", unidadTransporte.capacidadTanque));
                            comad.Parameters.Add(new SqlParameter("@fechaAdquisicion", unidadTransporte.fechaAdquisicion));
                            comad.Parameters.Add(new SqlParameter("@tipoAdquisicion", (int)unidadTransporte.tipoAdquisicion));
                            comad.Parameters.Add(new SqlParameter("@odometro", unidadTransporte.odometro));
                            comad.Parameters.Add(new SqlParameter("@placas", unidadTransporte.placas));
                            comad.Parameters.Add(new SqlParameter("@idProveedor", unidadTransporte.proveedor == null ? null : (int?)unidadTransporte.proveedor.idProveedor));
                            comad.Parameters.Add(new SqlParameter("@documento", unidadTransporte.documento));
                            comad.Parameters.Add(new SqlParameter("@año", unidadTransporte.anio));
                            comad.Parameters.Add(new SqlParameter("@mttoPrev", unidadTransporte.mttoPrev));
                            comad.Parameters.Add(new SqlParameter("@VIN", unidadTransporte.VIN));

                            comad.ExecuteNonQuery();
                            if (!CrearSqlCommand.validarCorrecto(comad))
                            {
                                tran.Rollback();
                                return new OperationResult(comad, unidadTransporte);
                            }

                            if (unidadTransporte.listaAjustes.Count > 0)
                            {
                                comad.CommandText = "sp_saveRegistroAjustes";
                                DateTime fecha = DateTime.Now;
                                foreach (var ajuste in unidadTransporte.listaAjustes)
                                {
                                    comad.Parameters.Clear();
                                    foreach (var param in CrearSqlCommand.getPatametros())
                                    {
                                        comad.Parameters.Add(param);
                                    }
                                    comad.Parameters.Add(new SqlParameter("@fecha", fecha));
                                    comad.Parameters.Add(new SqlParameter("@nombreTable", ajuste.nombreTabla));
                                    comad.Parameters.Add(new SqlParameter("@idTabla", ajuste.idTabla));
                                    comad.Parameters.Add(new SqlParameter("@columna", ajuste.nombreColumna));
                                    comad.Parameters.Add(new SqlParameter("@valAnterior", ajuste.valorAnterior));
                                    comad.Parameters.Add(new SqlParameter("@valNuevo", ajuste.valorNuevo));
                                    comad.Parameters.Add(new SqlParameter("@usuario", ajuste.usuario));

                                    comad.ExecuteNonQuery();
                                    if (!CrearSqlCommand.validarCorrecto(comad))
                                    {
                                        tran.Rollback();
                                        return new OperationResult(comad);
                                    }
                                }
                            }

                            tran.Commit();
                            return new OperationResult(comad, unidadTransporte);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getAllUnidadesTransV2()
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getAllUnidadesTransV2"))
                    {
                        List<UnidadTransporte> listaUnidades = new List<UnidadTransporte>();
                        con.Open();
                        using (SqlDataReader sqlReader = comad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                UnidadTransporte unidad = new mapComunes().mapUnidadesv2(sqlReader);
                                if (unidad == null)
                                {
                                    return new OperationResult(ResultTypes.error, "ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA.");
                                }
                                listaUnidades.Add(unidad);
                            }
                        }
                        return new OperationResult(comad, listaUnidades);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult sincronizarUnidadesTransporte(List<UnidadTransporte> listaUnidades)
        {
            try
            {
                string xml = GenerateXML.generarListaUnidadesSinc(listaUnidades);
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    con.Open();
                    using (SqlTransaction tran = con.BeginTransaction())
                    {
                        using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_sincronizarUnidadesTransporte", false, true))
                        {
                            comad.Transaction = tran;
                            comad.Parameters.Add(new SqlParameter("@xml", xml));
                            comad.ExecuteNonQuery();
                            if (CrearSqlCommand.validarCorrecto(comad))
                            {
                                tran.Commit();
                            }
                            else
                            {
                                tran.Rollback();
                            }
                            return new OperationResult(comad);
                        }
                    }
                }
                return new OperationResult();
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
}
