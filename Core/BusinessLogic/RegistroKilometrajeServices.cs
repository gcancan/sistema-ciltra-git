﻿namespace Core.BusinessLogic
{
    using Core.Models;
    using Core.Utils;
    using System;
    using System.Data;
    using System.Data.SqlClient;

    public class RegistroKilometrajeServices
    {
        public OperationResult getUltimoKM(string idUnidadTrans)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getUltimoKM";
                        command.Parameters.Add(new SqlParameter("@idUnidad", idUnidadTrans));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter4 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter4);
                        SqlParameter parameter5 = new SqlParameter("@km", SqlDbType.Decimal)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter3 = command.Parameters.Add(parameter5);
                        connection.Open();
                        int num = command.ExecuteNonQuery();
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = (decimal)parameter3.Value
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = "Error durante la operaci\x00f3n" + exception.Message,
                    result = 0
                };
            }
            return result;
        }

        internal OperationResult getUltimoKM(string idUnidadTrans, DateTime fecha)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getUltimoKMByFecha";
                        command.Parameters.Add(new SqlParameter("@idUnidad", idUnidadTrans));
                        command.Parameters.Add(new SqlParameter("@fecha", fecha));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter4 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter4);
                        SqlParameter parameter5 = new SqlParameter("@km", SqlDbType.Decimal)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter3 = command.Parameters.Add(parameter5);
                        connection.Open();
                        int num = command.ExecuteNonQuery();
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = (decimal)parameter3.Value
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = "Error durante la operaci\x00f3n" + exception.Message,
                    result = 0
                };
            }
            return result;
        }

        public OperationResult getUltimoKMVerificacion(string idUnidadTrans)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getUltimoKMVerificacion";
                        command.Parameters.Add(new SqlParameter("@idUnidad", idUnidadTrans));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter4 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter4);
                        SqlParameter parameter5 = new SqlParameter("@km", SqlDbType.Decimal)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter3 = command.Parameters.Add(parameter5);
                        connection.Open();
                        int num = command.ExecuteNonQuery();
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = (decimal)parameter3.Value
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = "Error durante la operaci\x00f3n" + exception.Message,
                    result = 0
                };
            }
            return result;
        }

        public OperationResult getUltimoRegistroValido(string idUnidadTrans)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getRegistroKmByIdUnidad";
                        command.Parameters.Add(new SqlParameter("@idUnidad", idUnidadTrans));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        RegistroKilometraje kilometraje = new RegistroKilometraje();
                        using (SqlDataReader reader2 = reader)
                        {
                            while (reader2.Read())
                            {
                                kilometraje.idRegistroKilometraje = (int)reader2["idRegistrosKilometraje"];
                                kilometraje.fecha = new DateTime?((DateTime)reader2["fecha"]);
                                kilometraje.kmInicial = (decimal)reader2["kmInicial"];
                                kilometraje.kmFinal = (decimal)reader2["kmFinal"];
                                kilometraje.idUsuario = (int)reader2["usuario"];
                                kilometraje.idUnidadTrans = (string)reader2["idUnidadTrans"];
                                kilometraje.validado = (bool)reader2["validado"];
                                kilometraje.nombreTabla = (string)reader2["nomTabla"];
                                kilometraje.idTabla = (int)reader2["idTabla"];
                            }
                        }
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = kilometraje
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = "Error durante la operaci\x00f3n" + exception.Message,
                    result = null
                };
            }
            return result;
        }

        public OperationResult saveRegistokilometraje(ref RegistroKilometraje registroKM, decimal km)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_saveRegistrosKilometrajes";
                        command.Parameters.Add(new SqlParameter("@idUnidadTrans", registroKM.idUnidadTrans));
                        command.Parameters.Add(new SqlParameter("@km", km));
                        command.Parameters.Add(new SqlParameter("@usuario", registroKM.idUsuario));
                        command.Parameters.Add(new SqlParameter("@nomTabla", registroKM.nombreTabla));
                        command.Parameters.Add(new SqlParameter("@idTabla", registroKM.idTabla));
                        command.Parameters.Add(new SqlParameter("@validado", registroKM.validado));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter6 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter6);
                        SqlParameter parameter7 = new SqlParameter("@identity", SqlDbType.Int, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter3 = command.Parameters.Add(parameter7);
                        SqlParameter parameter8 = new SqlParameter("@kmInicial", SqlDbType.Decimal)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter4 = command.Parameters.Add(parameter8);
                        SqlParameter parameter9 = new SqlParameter("@kmFinal", SqlDbType.Decimal)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter5 = command.Parameters.Add(parameter9);
                        connection.Open();
                        int num = command.ExecuteNonQuery();
                        registroKM.kmInicial = (decimal)parameter4.Value;
                        registroKM.kmFinal = (decimal)parameter5.Value;
                        registroKM.idRegistroKilometraje = (int)parameter3.Value;
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = registroKM,
                            identity = new int?((int)parameter3.Value)
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = "Error al intentar insertar el registro" + exception.Message,
                    result = null
                };
            }
            return result;
        }
    }
}
