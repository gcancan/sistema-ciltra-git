﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using Core.Utils;
using System.Data;
using System.Data.SqlClient;
namespace CoreFletera.BusinessLogic
{
    public class RegistroAjustesServices
    {
        public OperationResult saveRegistrosAjustes(List<RegistroAjustes> listaAjustes)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    con.Open();
                    using (SqlTransaction tran = con.BeginTransaction())
                    {
                        using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_saveRegistroAjustes"))
                        {
                            comad.Transaction = tran;
                            DateTime fecha = DateTime.Now;
                            foreach (var ajuste in listaAjustes)
                            {
                                comad.Parameters.Clear();
                                foreach (var param in CrearSqlCommand.getPatametros())
                                {
                                    comad.Parameters.Add(param);
                                }

                                comad.Parameters.Add(new SqlParameter("@fecha", fecha));
                                comad.Parameters.Add(new SqlParameter("@nombreTable", ajuste.nombreTabla));
                                comad.Parameters.Add(new SqlParameter("@idTabla", ajuste.idTabla));
                                comad.Parameters.Add(new SqlParameter("@columna", ajuste.nombreColumna));
                                comad.Parameters.Add(new SqlParameter("@valAnterior", ajuste.valorAnterior));
                                comad.Parameters.Add(new SqlParameter("@valNuevo", ajuste.valorNuevo));
                                comad.Parameters.Add(new SqlParameter("@usuario", ajuste.usuario));

                                comad.ExecuteNonQuery();
                                if (!CrearSqlCommand.validarCorrecto(comad))
                                {
                                    tran.Rollback();
                                    return new OperationResult(comad);
                                }
                            }
                            tran.Commit();
                            return new OperationResult(comad);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
}
