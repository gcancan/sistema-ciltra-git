﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using Core.Utils;
using System.Data;
using System.Data.SqlClient;
namespace CoreFletera.BusinessLogic
{
    public class InspeccionesServices
    {
        public OperationResult saveInspecciones(ref Inspecciones inspeccion)
        {
            try
            {
                string xmlInsumos = GenerateXML.generarListaInsumosInspeccion(inspeccion.listaInsumos);
                string xmlTiposUnidad = GenerateXML.generarListaTiposUnidad(inspeccion.listaTipoUnidades);
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comand = connection.CreateCommand())
                    {
                        comand.CommandType = CommandType.StoredProcedure;
                        comand.CommandText = "sp_saveInspecciones";

                        comand.Parameters.Add(new SqlParameter("@idInspeccion", inspeccion.idInspeccion));
                        comand.Parameters.Add(new SqlParameter("@idTipOrdServ", inspeccion.tipoOrdenServicio.idTipOrdServ));
                        comand.Parameters.Add(new SqlParameter("@nombreInspeccion", inspeccion.nombreInspeccion));
                        comand.Parameters.Add(new SqlParameter("@estatus", inspeccion.estatus));
                        comand.Parameters.Add(new SqlParameter("@xmlInsumos", xmlInsumos));
                        comand.Parameters.Add(new SqlParameter("@xmlTiposUnidad", xmlTiposUnidad));
                        comand.Parameters.Add(new SqlParameter("@dias", inspeccion.dias));
                        comand.Parameters.Add(new SqlParameter("@claveDiagrama", inspeccion.claveDiagrama));
                        SqlParameter spValor = comand.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        SqlParameter spId = comand.Parameters.Add(new SqlParameter("@id", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        SqlParameter spMensaje = comand.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        connection.Open();
                        var i = comand.ExecuteNonQuery();
                        if ((int)spValor.Value == 0)
                        {
                            inspeccion.idInspeccion = (int)spId.Value;
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = inspeccion };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }

        public OperationResult getInspeccionesByIdTipoUniTrans(int idTipoUnidadTrans, int idTipoOrdenServicio, int idOrden)
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comand = connection.CreateCommand())
                    {
                        comand.CommandType = CommandType.StoredProcedure;
                        comand.CommandText = "sp_getInspeccionesByIdTipoUniTrans";

                        comand.Parameters.Add(new SqlParameter("@idTipoUnidadTrans", idTipoUnidadTrans));
                        comand.Parameters.Add(new SqlParameter("@idTipoOrdenServicio", idTipoOrdenServicio));
                        comand.Parameters.Add(new SqlParameter("@idOrden", idOrden));

                        SqlParameter spValor = comand.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        SqlParameter spMensaje = comand.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        connection.Open();
                        List<Inspecciones> lista = new List<Inspecciones>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                lista.Add(new Inspecciones
                                {
                                    idInspeccion = (int)sqlReader["idInspeccion"],
                                    claveDiagrama = (string)sqlReader["claveDiagrama"],
                                    nombreInspeccion = (string)sqlReader["nombreInspeccion"],
                                    tipoOrdenServicio = new TiposDeOrdenServicio
                                    {
                                        idTipOrdServ = (int)sqlReader["idTipOrdServ"],
                                        NomTipOrdServ = (string)sqlReader["NomTipOrdServ"]
                                    },
                                    checado = Convert.ToBoolean((int)sqlReader["checado"]),
                                    dias = (decimal)sqlReader["dias"],
                                    diasPasados = (int)sqlReader["diasPasados"],
                                    grupo = new GrupoTipoUnidades { grupo= (string)sqlReader["grupo"]},
                                    opcional = Convert.ToBoolean(sqlReader["opcional"])
                                });
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = lista };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }

        public OperationResult getInspeccionesRealizadas(int idOrden)
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comand = connection.CreateCommand())
                    {
                        comand.CommandType = CommandType.StoredProcedure;
                        comand.CommandText = "sp_getInspeccionesRealizadasByOrden";

                        comand.Parameters.Add(new SqlParameter("@idOrden", idOrden));

                        SqlParameter spValor = comand.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        SqlParameter spMensaje = comand.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        connection.Open();
                        List<Inspecciones> lista = new List<Inspecciones>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                lista.Add(new Inspecciones
                                {
                                    idInspeccion = (int)sqlReader["idInspeccion"],
                                    claveDiagrama = (string)sqlReader["claveDiagrama"],
                                    nombreInspeccion = (string)sqlReader["nombreInspeccion"],                                    
                                    checado = true,
                                    dias = (decimal)sqlReader["dias"],
                                    diasPasados = 0                                   
                                });
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = lista };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }

        public OperationResult getInspeccionesByFiltros(int idTipoOrden, GrupoTipoUnidades grupo)
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comand = connection.CreateCommand())
                    {
                        comand.CommandType = CommandType.StoredProcedure;
                        comand.CommandText = "sp_getInspeccionesByFiltros";

                        comand.Parameters.Add(new SqlParameter("@idTipoOrdenServicio", idTipoOrden));
                        comand.Parameters.Add(new SqlParameter("@grupo", grupo.grupo));

                        SqlParameter spValor = comand.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        SqlParameter spMensaje = comand.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        connection.Open();
                        List<Inspecciones> lista = new List<Inspecciones>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                lista.Add(new Inspecciones
                                {
                                    idInspeccion = (int)sqlReader["idInspeccion"],
                                    claveDiagrama = (string)sqlReader["claveDiagrama"],
                                    nombreInspeccion = (string)sqlReader["nombreInspeccion"],
                                    tipoOrdenServicio = new TiposDeOrdenServicio
                                    {
                                        idTipOrdServ = (int)sqlReader["idTipOrdServ"],
                                        NomTipOrdServ = (string)sqlReader["NomTipOrdServ"]
                                    },
                                    checado = Convert.ToBoolean((int)sqlReader["checado"]),
                                    dias = (decimal)sqlReader["dias"],
                                    diasPasados = (int)sqlReader["diasPasados"],
                                    grupo = new GrupoTipoUnidades { grupo = (string)sqlReader["grupo"] }
                                });

                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = lista };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }

        public OperationResult getInsumosByInspeccion(int idInspeccion, int idOrden)
        {
            try
            {
                using (SqlConnection connetion = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comand = connetion.CreateCommand())
                    {
                        comand.CommandType = CommandType.StoredProcedure;
                        comand.CommandText = "sp_getInsumosByInspeccion";
                        comand.Parameters.Add(new SqlParameter("@idInspeccion", idInspeccion));
                        comand.Parameters.Add(new SqlParameter("@idOrden", idOrden));
                        SqlParameter spValor = comand.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        SqlParameter spMensaje = comand.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        connetion.Open();
                        List<ProductoInsumo> list = new List<ProductoInsumo>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                ProductoInsumo insumo = new ProductoInsumo
                                {
                                    idInsumo = (int)sqlReader["id"],
                                    clave = (string)sqlReader["clave"],
                                    nombre = (string)sqlReader["nombre"],
                                    tipoProducto = (int)sqlReader["tipoProducto"],
                                    precio = (decimal)sqlReader["precio"],
                                    cantMaxima = (decimal)sqlReader["cantidad"],
                                    cMax = (decimal)sqlReader["cantidadMaxima"]
                                };
                                list.Add(insumo);
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = list };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }
    }
}
