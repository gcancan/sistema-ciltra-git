﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using Core.Utils;
using System.Data;
using System.Data.SqlClient;
using Core.BusinessLogic;
namespace CoreFletera.BusinessLogic
{
    public class ModalidadService
    {
        public OperationResult getAllModalidades()
        {
			try
			{
				using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
				{
					using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getAllModalidades")) 
					{
						con.Open();
						List<Modalidades> lista = new List<Modalidades>();
						using (SqlDataReader reader = comad.ExecuteReader())
						{
							while (reader.Read())
							{
								Modalidades modalidad = new mapComunes().mapModalidad(reader);
								if (modalidad == null) return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");

								lista.Add(modalidad);
							}
						}
						return new OperationResult(comad, lista);
					}
				}
			}
			catch (Exception ex)
			{
				return new OperationResult(ex);
			}
        }
    }
}
