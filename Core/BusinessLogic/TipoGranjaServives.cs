﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using Core.Utils;
using Core.BusinessLogic;
using System.Data;
using System.Data.SqlClient;
namespace CoreFletera.BusinessLogic
{
    public class TipoGranjaServives
    {
        public OperationResult saveTipoGranja(ref TipoGranja tipoGranja, string usuario)
        {
			try
			{
				using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
				{
					con.Open();
					using (SqlTransaction tran = con.BeginTransaction())
					{
						using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_saveTipoGranja", true))
						{
							comad.Transaction = tran;
							comad.Parameters.Add(new SqlParameter("@idTipoGranja", tipoGranja.idTipoGranja));
							comad.Parameters.Add(new SqlParameter("@tipoGranja", tipoGranja.tipoGranja));
							comad.Parameters.Add(new SqlParameter("@activo", tipoGranja.activo));
							comad.Parameters.Add(new SqlParameter("@usuario", usuario));

							comad.ExecuteNonQuery();
							int id = 0;
							if (CrearSqlCommand.validarCorrecto(comad, out id))
							{
								tipoGranja.idTipoGranja = id;
								tran.Commit();
							}
							else
							{
								tran.Rollback();
							}
							return new OperationResult(comad, tipoGranja);
						}
					}
				}
			}
			catch (Exception ex)
			{
				return new OperationResult(ex);
			}
        }
		public OperationResult getAllTiposGranjas()
		{
			try
			{
				using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
				{
					using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getAllTiposGranja"))
					{
						con.Open();
						List<TipoGranja> lista = new List<TipoGranja>();
						using (SqlDataReader reader = comad.ExecuteReader())
						{
							while (reader.Read())
							{
								TipoGranja tipoGranja = new mapComunes().mapTipoGranja(reader);
								if (tipoGranja == null) return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");

								lista.Add(tipoGranja);
							}
						}
						return new OperationResult(comad, lista);
					}
				}
			}
			catch (Exception ex)
			{
				return new OperationResult(ex);
			}
		}
    }
}
