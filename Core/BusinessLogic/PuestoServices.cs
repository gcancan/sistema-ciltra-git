﻿using Core.BusinessLogic;
using Core.Models;
using Core.Utils;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.BusinessLogic
{
    public class PuestoServices
    {
        public OperationResult getALLPuestosById(int id = 0)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = CrearSqlCommand.getCommand(connection, "sp_getALLPuestoById"))
                    {
                        command.Parameters.Add(new SqlParameter("@id", id));

                        connection.Open();

                        List<Puesto> listPuesto = new List<Puesto>();

                        using (SqlDataReader sqlReader = command.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                Puesto departamento = new mapComunes().mapPuesto(sqlReader);
                                if (departamento == null)
                                {
                                    return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");
                                }
                                listPuesto.Add(departamento);
                            }
                        }
                        return new OperationResult(command, listPuesto);
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }
        public OperationResult savePuesto(ref Puesto puesto)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    con.Open();
                    using (SqlTransaction tran = con.BeginTransaction())
                    {
                        using (SqlCommand comand = CrearSqlCommand.getCommand(con, "sp_savePuesto", true))
                        {
                            comand.Transaction = tran;
                            comand.Parameters.Add(new SqlParameter("@nombrePuesto", puesto.nombrePuesto));
                            comand.Parameters.Add(new SqlParameter("@activo", puesto.activo));
                            comand.Parameters.Add(new SqlParameter("@idPuesto", puesto.idPuesto));
                            int id = 0;
                            comand.ExecuteNonQuery();
                            if (CrearSqlCommand.validarCorrecto(comand, out id))
                            {
                                puesto.idPuesto = id;
                                tran.Commit();
                            }
                            else
                            {
                                tran.Rollback();
                            }
                            return new OperationResult(comand, puesto);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult sincronizarPuestos(List<Puesto> listaPuestos)
        {
            try
            {
                string xml = GenerateXML.generarListasPuestosSinc(listaPuestos);
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    con.Open();
                    using (SqlTransaction tran = con.BeginTransaction())
                    {
                        using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_sincronizarPuestos", false, true))
                        {
                            comad.Transaction = tran;
                            comad.Parameters.Add(new SqlParameter("@xml", xml));
                            comad.ExecuteNonQuery();
                            if (!CrearSqlCommand.validarCorrecto(comad))
                            {
                                tran.Rollback();
                                return new OperationResult(comad);
                            }
                            tran.Commit();
                            return new OperationResult(comad);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
}
