﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using Core.BusinessLogic;
using System.Data;
using System.Data.SqlClient;
using Core.Utils;
namespace CoreFletera.BusinessLogic
{
    public class CasetaServices
    {
        public OperationResult getCasetasByIdEstado(int idEstado = 0)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comand = CrearSqlCommand.getCommand(con, "sp_getCasetasByEstado"))
                    {
                        comand.Parameters.Add(new SqlParameter("@idEstado", idEstado));
                        con.Open();
                        List<Caseta> listCasetas = new List<Caseta>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                Caseta caseta = new mapComunes().mapCaseta(sqlReader);
                                if (caseta != null)
                                {
                                    listCasetas.Add(caseta);
                                }
                                else
                                {
                                    return new OperationResult() { valor = 1, mensaje = "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA" };
                                }
                            }
                        }
                        return new OperationResult(comand, listCasetas);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        public OperationResult getCasetasByIdRuta(int idRuta)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comand = CrearSqlCommand.getCommand(con, "sp_getCasetaByRuta"))
                    {
                        comand.Parameters.Add(new SqlParameter("@idRuta", idRuta));
                        con.Open();
                        List<Caseta> listCasetas = new List<Caseta>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                Caseta caseta = new mapComunes().mapCaseta(sqlReader);
                                if (caseta != null)
                                {
                                    listCasetas.Add(caseta);
                                }
                                else
                                {
                                    return new OperationResult() { valor = 1, mensaje = "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA" };
                                }
                            }
                        }
                        return new OperationResult(comand, listCasetas);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        internal OperationResult saveCaseta(ref Caseta caseta)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    con.Open();
                    using (SqlTransaction tran = con.BeginTransaction())
                    {
                        using (SqlCommand comand = CrearSqlCommand.getCommand(con, "sp_saveCaseta", true))
                        {
                            comand.Transaction = tran;
                            comand.Parameters.Add(new SqlParameter("@idCasetas", caseta.idCaseta));
                            comand.Parameters.Add(new SqlParameter("@via", caseta.via));
                            comand.Parameters.Add(new SqlParameter("@idEstado", caseta.estado.idEstado));
                            comand.Parameters.Add(new SqlParameter("@nombreCaseta", caseta.nombreCaseta));
                            comand.Parameters.Add(new SqlParameter("@vigencia", caseta.vigencia));
                            comand.Parameters.Add(new SqlParameter("@costo3Ejes", caseta.costo3Ejes));
                            comand.Parameters.Add(new SqlParameter("@costo5Ejes", caseta.costo5Ejes));
                            comand.Parameters.Add(new SqlParameter("@costo6Ejes", caseta.costo6Ejes));
                            comand.Parameters.Add(new SqlParameter("@costo9Ejes", caseta.costo9Ejes));
                            comand.Parameters.Add(new SqlParameter("@activo", caseta.estatus));
                            comand.Parameters.Add(new SqlParameter("@paginaFacturacion", caseta.paginaFacturacion));

                            comand.ExecuteNonQuery();
                            if ((int)comand.Parameters["@valor"].Value == 0)
                            {
                                caseta.idCaseta = (int)comand.Parameters["@id"].Value;
                                tran.Commit();
                            }
                            else
                            {
                                tran.Rollback();
                            }
                            return new OperationResult(comand, caseta);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
}
