﻿using Core.Models;
using Core.Utils;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.BusinessLogic
{
    public class BitacoraEntradaSalidasService
    {
        public OperationResult getBitacoraEntradaSalidas(DateTime fechaInicial, DateTime fechaFinal, int idEmpresa)
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getBitacoraEntradaSalidas";
                        command.Parameters.Add(new SqlParameter("@fechaInicial", fechaInicial));
                        command.Parameters.Add(new SqlParameter("@fechaFinal", fechaFinal));
                        command.Parameters.Add(new SqlParameter("@idEmpresa", idEmpresa));
                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var spMensaje = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                        connection.Open();
                        List<BitacoraEntradaSalidas> listBitacora = new List<BitacoraEntradaSalidas>();
                        DateTime? fecha = null;
                        using (SqlDataReader sqlReader = command.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                BitacoraEntradaSalidas bitacora = new BitacoraEntradaSalidas
                                {
                                    idBitacora = (int)sqlReader["idBitacora"],
                                    folio = (int)sqlReader["folio"],
                                    fechaEntrada = (DateTime)sqlReader["fechaEntrada"],
                                    fechaSalida = DBNull.Value.Equals(sqlReader["fechaSalida"]) ? fecha : (DateTime)sqlReader["fechaSalida"],
                                    unidad = (string)sqlReader["unidad"],
                                    chofer = (string)sqlReader["chofer"],
                                    guardia = (string)sqlReader["guardia"],
                                    isOrden = Convert.ToBoolean(sqlReader["isOrden"]),
                                };
                                listBitacora.Add(bitacora);
                            }
                        }

                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = listBitacora };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }

        public OperationResult getMotivosEntrada(int folio, bool isOrden, string idUnidad)
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getMotivosEntrada";
                        command.Parameters.Add(new SqlParameter("@folio", folio));
                        command.Parameters.Add(new SqlParameter("@isOrden", Convert.ToInt32(isOrden)));
                        command.Parameters.Add(new SqlParameter("@idUnidad", idUnidad));
                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var spMensaje = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                        connection.Open();
                        List<string> listMotivos = new List<string>();
                        //DateTime? fecha = null;
                        using (SqlDataReader sqlReader = command.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                listMotivos.Add((string)sqlReader["motivo"]);
                            }
                        }

                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = listMotivos };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }
    }
}
