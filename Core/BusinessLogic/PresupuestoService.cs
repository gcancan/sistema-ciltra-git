﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoreFletera.Models;
using System.Data;
using System.Data.SqlClient;
using Core.Utils;
using Core.BusinessLogic;
namespace CoreFletera.BusinessLogic
{
    public class PresupuestoService
    {
        public OperationResult savePresupuesto(ref Presupuesto presupuesto)
        {
			try
			{
				string xml = GenerateXML.generarDetallesPresupuestos(presupuesto.listaDetalles);
				using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
				{
					con.Open();
					using (SqlTransaction tran = con.BeginTransaction())
					{
						using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_savePresupuestos", true))
						{
							comad.Transaction = tran;

							comad.Parameters.Add(new SqlParameter("@idPresupuesto", presupuesto.idPresupuesto));
							comad.Parameters.Add(new SqlParameter("@idEmpresa", presupuesto.empresa.clave));
							comad.Parameters.Add(new SqlParameter("@idSucursal", presupuesto.sucursal.idZonaOperativa));
							comad.Parameters.Add(new SqlParameter("@idCliente", presupuesto.cliente.clave));
							comad.Parameters.Add(new SqlParameter("@anio", presupuesto.anio));
							comad.Parameters.Add(new SqlParameter("@usuario", presupuesto.usuario));
							comad.Parameters.Add(new SqlParameter("@fechaRegistro", presupuesto.fechaRegistro));
							comad.Parameters.Add(new SqlParameter("@estatus", presupuesto.estatus));
							comad.Parameters.Add(new SqlParameter("@fechaBaja", presupuesto.fechaBaja));
							comad.Parameters.Add(new SqlParameter("@usuarioBaja", presupuesto.usuarioBaja));
							comad.Parameters.Add(new SqlParameter("@xml", xml));

							comad.ExecuteNonQuery();
							int id = 0;
							if (CrearSqlCommand.validarCorrecto(comad, out id))
							{
								presupuesto.idPresupuesto = id;
								tran.Commit();
							}
							else
							{
								tran.Rollback();
							}
							return new OperationResult(comad, presupuesto);
						}
						
					}
				}
			}
			catch (Exception ex)
			{
				return new OperationResult(ex);
			}
        }

		public OperationResult getAllPresupuestos()
		{
			try
			{
				using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
				{
					using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getAllPresupuestos"))
					{
						List<Presupuesto> lista = new List<Presupuesto>();
						con.Open();
						using (SqlDataReader reader = comad.ExecuteReader())
						{
							while (reader.Read())
							{
								Presupuesto presupuesto = new mapComunes().mapPresupuesto(reader);
								if (presupuesto == null) return new OperationResult(ResultTypes.error, "ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");

								lista.Add(presupuesto);
							}
						}
						return new OperationResult(comad, lista);
					}
				}
			}
			catch (Exception ex)
			{
				return new OperationResult(ex);
			}
		}
		public OperationResult getAllPresupuestosByClave(int clave)
		{
			try
			{
				using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
				{
					using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getAllPresupuestosByClave"))
					{
						comad.Parameters.Add(new SqlParameter("@clave", clave));
						Presupuesto presupuesto = new Presupuesto();
						con.Open();
						using (SqlDataReader reader = comad.ExecuteReader())
						{
							while (reader.Read())
							{
								presupuesto = new mapComunes().mapPresupuesto(reader);
								if (presupuesto == null) return new OperationResult(ResultTypes.error, "ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");								
							}
						}
						return new OperationResult(comad, presupuesto);
					}
				}
			}
			catch (Exception ex)
			{
				return new OperationResult(ex);
			}
		}
		public OperationResult bajarDetallePresupuesto(int idPresupuesto, int idModalidad, string usuario, string motivo)
		{
			try
			{
				using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
				{
					using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_bajarDetallePresupuesto"))
					{
						comad.Parameters.Add(new SqlParameter("@idPresupuesto", idPresupuesto));
						comad.Parameters.Add(new SqlParameter("@idModalidad", idModalidad));
						comad.Parameters.Add(new SqlParameter("@usuario", usuario));
						comad.Parameters.Add(new SqlParameter("@motivo", motivo));

						con.Open();
						comad.ExecuteNonQuery();
						return new OperationResult(comad);
					}
				}
			}
			catch (Exception ex)
			{
				return new OperationResult(ex);
			}
		}

		public OperationResult getDetallePresupuestosByClave(int clave)
		{
			try
			{
				using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
				{
					using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getDetallePresupuestosByClave"))
					{
						comad.Parameters.Add(new SqlParameter("@clave", clave));
						List<DetallePresupuesto> lista = new List<DetallePresupuesto> ();
						con.Open();
						using (SqlDataReader reader = comad.ExecuteReader())
						{
							while (reader.Read())
							{
								DetallePresupuesto  detalle = new mapComunes().mapDetallePresupuesto(reader);
								if (detalle == null) return new OperationResult(ResultTypes.error, "ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");

								lista.Add(detalle);
							}
						}
						return new OperationResult(comad, lista);
					}
				}
			}
			catch (Exception ex)
			{
				return new OperationResult(ex);
			}
		}
		public OperationResult getAllIndicadoresViajes(int año, List<string> listaIdClientes)
		{
			try
			{
				string xml = GenerateXML.generarListaCadena(listaIdClientes);
				using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
				{
					using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getIndicadoresViajes"))
					{
						comad.Parameters.Add(new SqlParameter("año", año));
						comad.Parameters.Add(new SqlParameter("xml", xml));
						con.Open();
						List<ReporteIndicadorViajes> listaReporte = new List<ReporteIndicadorViajes>();
						using (SqlDataReader reader = comad.ExecuteReader())
						{
							while (reader.Read())
							{
								ReporteIndicadorViajes reporte = new mapComunes().mapReporteIndicadorViajes(reader);
								if (reporte == null) return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA DE INDICADORES DE VIAJES");

								listaReporte.Add(reporte);
							}
						}
						return new OperationResult(comad, listaReporte);
					}
				}
			}
			catch (Exception ex)
			{
				return new OperationResult(ex);
			}
		}
		public OperationResult getAllIndicadoresCombustible(int año, List<string> listaZonaOpe, List<string> listaIdClientes)
		{
			try
			{
				string xmlCliente = GenerateXML.generarListaCadena(listaIdClientes);
				string xmlZona = GenerateXML.generarListaCadena(listaZonaOpe);
				using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
				{
					using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getIndicadoresCombustible"))
					{
						comad.Parameters.Add(new SqlParameter("año", año));
						comad.Parameters.Add(new SqlParameter("xmlCliente", xmlCliente));
						comad.Parameters.Add(new SqlParameter("@xmlZonas", xmlZona));
						con.Open();
						List<ReporteIndicadorCombustible> listaReporte = new List<ReporteIndicadorCombustible>();
						using (SqlDataReader reader = comad.ExecuteReader())
						{
							while (reader.Read())
							{
								ReporteIndicadorCombustible reporte = new mapComunes().mapReporteIndicadorCombustible(reader);
								if (reporte == null) return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA DE INDICADORES DE VIAJES");

								listaReporte.Add(reporte);
							}
						}
						return new OperationResult(comad, listaReporte);
					}
				}
			}
			catch (Exception ex)
			{
				return new OperationResult(ex);
			}
		}
		public OperationResult getPresupuestosByClientes(int año, List<string> listaIdClientes)
		{
			try
			{
				string xml = GenerateXML.generarListaCadena(listaIdClientes);
				using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
				{
					using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getPresupuestosByidClientes"))
					{
						comad.Parameters.Add(new SqlParameter("año", año));
						comad.Parameters.Add(new SqlParameter("xml", xml));
						con.Open();
						List<Presupuesto> listaPResupuesto = new List<Presupuesto>();
						using (SqlDataReader reader = comad.ExecuteReader())
						{
							while (reader.Read())
							{
								Presupuesto reporte = new mapComunes().mapPresupuesto(reader);
								if (reporte == null) return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA DE INDICADORES DE VIAJES");

								listaPResupuesto.Add(reporte);
							}
						}

						var resp = getDetallesPresupuestosById(listaPResupuesto.Select(s => s.idPresupuesto.ToString()).ToList());
						if (resp.typeResult == ResultTypes.success)
						{
							List<DetallePresupuesto> listaDetalles = resp.result as List<DetallePresupuesto>;
							foreach (var item in listaPResupuesto)
							{
								item.listaDetalles = listaDetalles.FindAll(s => s.idPresupuesto == item.idPresupuesto);
							}
						}
						else
						{
							return resp;
						}

						return new OperationResult(comad, listaPResupuesto);
					}
				}
			}
			catch (Exception ex)
			{
				return new OperationResult(ex);
			}
		}

		public OperationResult getDetallesPresupuestosById(List<string> listaId)
		{
			try
			{
				string xml = GenerateXML.generarListaCadena(listaId);
				using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
				{
					using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getDetallePresupuestosById"))
					{

						comad.Parameters.Add(new SqlParameter("xml", xml));
						con.Open();
						List<DetallePresupuesto> listaPResupuesto = new List<DetallePresupuesto>();
						using (SqlDataReader reader = comad.ExecuteReader())
						{
							while (reader.Read())
							{
								DetallePresupuesto reporte = new mapComunes().mapDetallePresupuesto(reader);
								if (reporte == null) return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA DE INDICADORES DE VIAJES");

								listaPResupuesto.Add(reporte);
							}
						}
						return new OperationResult(comad, listaPResupuesto);
					}
				}
			}
			catch (Exception ex)
			{
				return new OperationResult(ex);
			}
		}
	}
}
