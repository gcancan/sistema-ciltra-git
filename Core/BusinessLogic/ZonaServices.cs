﻿namespace Core.BusinessLogic
{
    using Core.Models;
    using Core.Utils;
    using CoreFletera.BusinessLogic;
    using CoreFletera.Models;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;

    public class ZonaServices
    {
        internal OperationResult borrarOrigenDestino(OrigenDestinos relacion)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_borrarOrigenDestino";
                        command.Parameters.Add(new SqlParameter("@idOrigenDestino", relacion.idOrigenDestino));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        List<OrigenPegaso> list = new List<OrigenPegaso>();
                        connection.Open();
                        command.ExecuteNonQuery();
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = list
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            return result;
        }

        internal OperationResult borrarOrigenDestinoByRuta(int idOrigen, int idDestino, int idCliente)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_borrarOrigenDestinoByRuta", false))
                    {
                        command.Parameters.Add(new SqlParameter("@idOrigen", idOrigen));
                        command.Parameters.Add(new SqlParameter("@idDestino", idDestino));
                        command.Parameters.Add(new SqlParameter("@idCliente", idCliente));
                        connection.Open();
                        command.ExecuteNonQuery();
                        result = new OperationResult(command, null);
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            return result;
        }

        public OperationResult deleteRutasDestino(Zona zona)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_deleteRutasDestino";
                        command.Parameters.Add(new SqlParameter("@clave", zona.clave));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        int num = command.ExecuteNonQuery();
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
            return result;
        }

        public OperationResult getDestinosByIdEmpresa(int idEmpresa)
        {
            OperationResult result;
            using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
            {
                using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getDestinosByIdEmpresa", false))
                {
                    command.Parameters.Add(new SqlParameter("@idEmpresa", idEmpresa));
                    List<Zona> list = new List<Zona>();
                    connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Zona item = new Zona
                            {
                                clave = (int)reader["Clave"],
                                descripcion = (string)reader["Descripcion"]
                            };
                            list.Add(item);
                        }
                    }
                    result = new OperationResult(command, list);
                }
            }
            return result;
        }

        public OperationResult getDestinosByOrigenAndIdCliente(int idCliente, int idOrigen)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getDestinosByOrigenAndIdCliente";
                        command.Parameters.Add(new SqlParameter("@idCliente", idCliente));
                        command.Parameters.Add(new SqlParameter("@idOrigen", idOrigen));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        List<Zona> list = new List<Zona>();
                        connection.Open();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Zona item = new mapComunes().mapDestino(reader);
                                list.Add(item);
                            }
                        }
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = list
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            return result;
        }

        public OperationResult getDestinosIncompletosAllById(int id, string idCliente)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getDestinosIncompletosAllById";
                        command.Parameters.Add(new SqlParameter("@id", id));
                        command.Parameters.Add(new SqlParameter("@idCliente", (idCliente == "") ? "%" : idCliente));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        List<Zona> list = new List<Zona>();
                        using (SqlDataReader reader2 = reader)
                        {
                            while (reader2.Read())
                            {
                                Zona item = new Zona
                                {
                                    clave_empresa = (int)reader2["Clave_empresa"],
                                    clave = (int)reader2["Clave"],
                                    descripcion = (string)reader2["Descripcion"],
                                    activo = (bool)reader2["Activo"],
                                    costoSencillo = (decimal)reader2["costoSencillo"],
                                    costoSencillo35 = (decimal)reader2["Precio_sencillo35"],
                                    costoFull = (decimal)reader2["costoFull"],
                                    km = (decimal)reader2["km"],
                                    claveZona = (string)reader2["claveZona"],
                                    claveCliente = (int)reader2["Clave_cliente"],
                                    claveExtra = (string)reader2["claveExtra"],
                                    correo = (string)reader2["correo"]
                                };
                                list.Add(item);
                            }
                        }
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = list
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
            return result;
        }

        internal OperationResult getOrigenByIdCliente(int clave)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getOrigenByIdCliene";
                        command.Parameters.Add(new SqlParameter("@idCliente", clave));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        List<OrigenPegaso> list = new List<OrigenPegaso>();
                        connection.Open();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                OrigenPegaso item = new OrigenPegaso
                                {
                                    idOrigen = (int)reader["idOrigen"],
                                    nombre = (string)reader["nombre"],
                                    idCliente = DBNull.Value.Equals(reader["idCliente"]) ? null : (int?)reader["idCliente"],
                                    idEmpresa = (int)reader["idEmpresa"]
                                };
                                list.Add(item);
                            }
                        }
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = list
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            return result;
        }
        internal OperationResult getOrigenByIdEmpresa(int clave)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getOrigenByIdEmpresa";
                        command.Parameters.Add(new SqlParameter("@idEmpresa", clave));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        List<OrigenPegaso> list = new List<OrigenPegaso>();
                        connection.Open();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                OrigenPegaso item = new OrigenPegaso
                                {
                                    idOrigen = (int)reader["idOrigen"],
                                    nombre = (string)reader["nombre"],
                                    idCliente = DBNull.Value.Equals(reader["idCliente"]) ? null : (int?)reader["idCliente"],
                                    idEmpresa = (int)reader["idEmpresa"]
                                };
                                list.Add(item);
                            }
                        }
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = list
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            return result;
        }

        internal OperationResult getPrecios(int idCliente, eTiposPrecios tipoPrecio, int idOrigen)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getPrecios";
                        command.Parameters.Add(new SqlParameter("@idCliente", idCliente));
                        command.Parameters.Add(new SqlParameter("@tipoPrecio", (int)tipoPrecio));
                        command.Parameters.Add(new SqlParameter("@idOrigen", idOrigen));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        List<decimal> list = new List<decimal>();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                list.Add((decimal)reader["Precios"]);
                            }
                        }
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = list
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
            return result;
        }

        public OperationResult getRelacionesOrigenDestinosByIdCliente(int idCliente)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getOrigenesDestinoByIdCliente";
                        command.Parameters.Add(new SqlParameter("@idCliente", idCliente));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        List<OrigenDestinoClientes> list = new List<OrigenDestinoClientes>();
                        connection.Open();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                OrigenDestinoClientes item = new OrigenDestinoClientes
                                {
                                    idOrigenDestinoClientes = (int)reader["idOrigenDestino"],
                                    idOrigen = (int)reader["idOrigen"],
                                    origen = (string)reader["nombreOrigen"],
                                    kmOrigen = (decimal)reader["kmOrigen"],
                                    idDestino = (int)reader["idDestino"],
                                    destino = (string)reader["Descripcion"],
                                    kmDestino = (decimal)reader["kmDestino"],
                                    precioSencillo = (decimal)reader["precioSencillo"],
                                    precioFull = (decimal)reader["precioFull"],
                                    km = (decimal)reader["km"],
                                    viaje = Convert.ToBoolean(reader["viaje"]),
                                    viajeSencillo = (decimal)reader["viajeSencillo"],
                                    viajeFull = (decimal)reader["viajeFull"],
                                    incluyeCaseta = Convert.ToBoolean(reader["incluyeCaseta"]),
                                    precioCasetas = (decimal)reader["costoCaseta"],
                                    tipoPago = (ClasificacionPagoChofer)reader["tipoPagoOperador"],
                                    clasificacion = DBNull.Value.Equals(reader["idClasificacionProducto"]) ? null : new ClasificacionProductos()
                                    {
                                        idClasificacionProducto = (int)reader["idClasificacionProducto"],
                                        descripcion = (string)reader["cla_clasificacion"],
                                        diesel = Convert.ToBoolean(reader["diesel"])
                                    }
                                };
                                list.Add(item);
                            }
                        }
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = list
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 0,
                    mensaje = exception.Message
                };
            }
            return result;
        }

        public OperationResult getZonasALLorById(int id, string idCliente)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getZonasALLorById";
                        command.Parameters.Add(new SqlParameter("@id", id));
                        command.Parameters.Add(new SqlParameter("@idCliente", (idCliente == "") ? "%" : idCliente));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        List<Zona> list = new List<Zona>();
                        using (SqlDataReader reader2 = reader)
                        {
                            while (reader2.Read())
                            {
                                Zona item = new mapComunes().mapDestino(reader2);
                                if (item == null)
                                {
                                    return new OperationResult
                                    {
                                        valor = 1,
                                        mensaje = "Error al leer la informacion de la Ruta"
                                    };
                                }
                                list.Add(item);
                            }
                        }
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = list
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
            return result;
        }

        public OperationResult getZonasByEmpresaCliente(int idEmpresa, int idCliente)
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getZonasByEmpresaCliente"))
                    {

                        command.Parameters.Add(new SqlParameter("@idEmpresa", idEmpresa));
                        command.Parameters.Add(new SqlParameter("@idCliente", idCliente));

                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        List<Zona> list = new List<Zona>();
                        using (SqlDataReader reader2 = reader)
                        {
                            while (reader2.Read())
                            {
                                Zona item = new mapComunes().mapDestino(reader2);
                                if (item == null)
                                {
                                    return new OperationResult
                                    {
                                        valor = 1,
                                        mensaje = "Error al leer la informacion de la Ruta"
                                    };
                                }
                                list.Add(item);
                            }
                        }
                        return new OperationResult(command, list);
                    }
                }
            }
            catch (Exception exception)
            {
                return new OperationResult(exception);
            }
        }

        internal OperationResult getZonasByPrecioAndOrigen(int idCliente, eTiposPrecios tipoPrecio, int idOrigen, decimal precio)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getZonasByPrecioAndOrigen";
                        command.Parameters.Add(new SqlParameter("@idCliente", idCliente));
                        command.Parameters.Add(new SqlParameter("@tipoPrecio", (int)tipoPrecio));
                        command.Parameters.Add(new SqlParameter("@idOrigen", idOrigen));
                        command.Parameters.Add(new SqlParameter("@precio", precio));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        List<Zona> list = new List<Zona>();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Zona item = new Zona
                                {
                                    clave = (int)reader["Clave"],
                                    descripcion = (string)reader["Descripcion"],
                                    claveZona = (string)reader["CveZona"]
                                };
                                list.Add(item);
                            }
                        }
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = list
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
            return result;
        }

        public OperationResult saveDestino_v2(ref Zona zona)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    connection.Open();
                    using (SqlTransaction transaction = connection.BeginTransaction())
                    {
                        using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_saveDestino_v2", true))
                        {
                            command.Transaction = transaction;
                            command.Parameters.Add(new SqlParameter("@clave", zona.clave));
                            command.Parameters.Add(new SqlParameter("@idEmpresa", zona.clave_empresa));
                            command.Parameters.Add(new SqlParameter("@idBase", (zona.baseDestino == null) ? null : new int?(zona.baseDestino.idBase)));
                            command.Parameters.Add(new SqlParameter("@descripcion", zona.descripcion));
                            command.Parameters.Add(new SqlParameter("@km", zona.km));
                            command.Parameters.Add(new SqlParameter("@activo", zona.activo));
                            command.Parameters.Add(new SqlParameter("@correo", (zona.correo == null) ? "" : zona.correo));
                            command.Parameters.Add(new SqlParameter("@idEstado", (zona.estado == null) ? 1 : zona.estado.idEstado));
                            int num = command.ExecuteNonQuery();
                            if (((int)command.Parameters["@valor"].Value) == 0)
                            {
                                zona.clave = (int)command.Parameters["@id"].Value;
                                transaction.Commit();
                            }
                            else
                            {
                                transaction.Rollback();
                            }
                            result = new OperationResult(command, zona);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
            return result;
        }

        internal OperationResult saveNuevosPrecios(int idCliente, eTiposPrecios tipoPrecio, decimal precioAnterior, decimal precioNuevo, List<Zona> list, string usuario)
        {
            OperationResult result;
            try
            {
                string str = GenerateXML.generarListZonas(list);
                if (string.IsNullOrEmpty(str))
                {
                    result = new OperationResult
                    {
                        valor = 2,
                        mensaje = "No se pudo construir el xml correctamente"
                    };
                }
                else
                {
                    using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                    {
                        using (SqlCommand command = connection.CreateCommand())
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.CommandText = "sp_saveNuevosPrecios";
                            command.Parameters.Add(new SqlParameter("@idCliente", idCliente));
                            command.Parameters.Add(new SqlParameter("@tipoPrecio", (int)tipoPrecio));
                            command.Parameters.Add(new SqlParameter("@precioAnterior", precioAnterior));
                            command.Parameters.Add(new SqlParameter("@precioNuevo", precioNuevo));
                            command.Parameters.Add(new SqlParameter("@xml", str));
                            command.Parameters.Add(new SqlParameter("@usuario", usuario));
                            SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                            {
                                Direction = ParameterDirection.Output
                            };
                            SqlParameter parameter = command.Parameters.Add(parameter1);
                            SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                            {
                                Direction = ParameterDirection.Output
                            };
                            SqlParameter parameter2 = command.Parameters.Add(parameter3);
                            connection.Open();
                            int num = command.ExecuteNonQuery();
                            result = new OperationResult
                            {
                                valor = new int?((int)parameter.Value),
                                mensaje = (string)parameter2.Value
                            };
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
            return result;
        }

        internal OperationResult saveOrigenDestino(ref OrigenDestinos relacion)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_saveOrigenDestino";
                        command.Parameters.Add(new SqlParameter("@idOrigen", relacion.origen.idOrigen));
                        command.Parameters.Add(new SqlParameter("@idDestino", relacion.destino.clave));
                        command.Parameters.Add(new SqlParameter("@preSencillo", relacion.precioSencillo));
                        command.Parameters.Add(new SqlParameter("@preFull", relacion.precioFull));
                        command.Parameters.Add(new SqlParameter("@km", relacion.km));
                        command.Parameters.Add(new SqlParameter("@viaje", relacion.viaje));
                        command.Parameters.Add(new SqlParameter("@idCliente", relacion.idCliente));
                        command.Parameters.Add(new SqlParameter("@viajeSencillo", relacion.viajeSencillo));
                        command.Parameters.Add(new SqlParameter("@viajeFull", relacion.viajeFull));
                        command.Parameters.Add(new SqlParameter("@incluyeCaseta", relacion.incluyeCaseta));
                        command.Parameters.Add(new SqlParameter("@costoCaseta", relacion.precioCasetas));
                        command.Parameters.Add(new SqlParameter("@tipoPago", (int)relacion.tipoPago));
                        command.Parameters.Add(new SqlParameter("@idClasificacion", (relacion.clasificacion == null) ? null : new int?(relacion.clasificacion.idClasificacionProducto)));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter4 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter4);
                        SqlParameter parameter5 = new SqlParameter("@identity", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter3 = command.Parameters.Add(parameter5);
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        relacion.idOrigenDestino = (int)parameter3.Value;
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = relacion,
                            identity = new int?((int)parameter3.Value)
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
            return result;
        }

        public OperationResult saveRutasDestino(ref Zona zona)
        {

            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    connection.Open();
                    using (SqlTransaction tran = connection.BeginTransaction())
                    {
                        using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_saveRutasDestino"))// connection.CreateCommand())
                        {
                            command.Transaction = tran;
                            command.Parameters.Add(new SqlParameter("@clave", zona.clave));
                            command.Parameters.Add(new SqlParameter("@claveEmpresa", zona.clave_empresa));
                            command.Parameters.Add(new SqlParameter("@claveCliente", zona.claveCliente));
                            command.Parameters.Add(new SqlParameter("@precioSencillo", zona.costoSencillo));
                            command.Parameters.Add(new SqlParameter("@costoSencillo35", zona.costoSencillo35));
                            command.Parameters.Add(new SqlParameter("@precioFull", zona.costoFull));
                            command.Parameters.Add(new SqlParameter("@descripcion", zona.descripcion));
                            command.Parameters.Add(new SqlParameter("@km", zona.km));
                            command.Parameters.Add(new SqlParameter("@correo", (zona.correo == null) ? "" : zona.correo));
                            command.Parameters.Add(new SqlParameter("@claveExtra", zona.claveExtra));
                            command.Parameters.Add(new SqlParameter("@idEstado", (zona.estado == null) ? 1 : zona.estado.idEstado));
                            command.Parameters.Add(new SqlParameter("@sencillo_24", zona.sencillo_24));
                            command.Parameters.Add(new SqlParameter("@sencillo_30", zona.sencillo_30));
                            command.Parameters.Add(new SqlParameter("@sencillo_35", zona.sencillo_35));
                            command.Parameters.Add(new SqlParameter("@full", zona.full));
                            command.Parameters.Add(new SqlParameter("@precioFijoSen", zona.precioFijoSencillo));
                            command.Parameters.Add(new SqlParameter("@precioFijoSen35", zona.precioFijoSencillo35));
                            command.Parameters.Add(new SqlParameter("@precioFijoFull", zona.precioFijoFull));
                            command.Parameters.Add(new SqlParameter("@idOrigen", zona.origenPegaso.idOrigen));
                            command.Parameters.Add(new SqlParameter("@viaje", zona.viaje));
                            command.Parameters.Add(new SqlParameter("@tiempoIda", zona.tiempoIda));
                            command.Parameters.Add(new SqlParameter("@tiempoGranja", zona.tiempoGranja));
                            command.Parameters.Add(new SqlParameter("@tiempoRetorno", zona.tiempoRetorno));
                            command.Parameters.Add(new SqlParameter("@activo", zona.activo));
                            command.Parameters.Add(new SqlParameter("@claveZona", zona.claveZona));
                            command.Parameters.Add(new SqlParameter("@idCategoria", zona.categoria == null ? null : (int?)zona.categoria.idTipoGranja));
                            command.Parameters.Add(new SqlParameter("@idCategoria2", zona.categoria2 == null ? null : (int?)zona.categoria2.idTipoGranja));


                            var idd = command.Parameters.Add(new SqlParameter("@identity", SqlDbType.Int)
                            {
                                Direction = ParameterDirection.Output
                            });


                            command.ExecuteNonQuery();
                            if (CrearSqlCommand.validarCorrecto(command))
                            {
                                zona.clave = (int)idd.Value;
                            }
                            else
                            {
                                tran.Rollback();
                                return new OperationResult(command);
                            }

                            if (zona.listaAnticipo != null)
                            {
                                if (zona.listaAnticipo.Count > 0)
                                {
                                    command.CommandText = "sp_saveAnticipoDestino";
                                    foreach (var item in zona.listaAnticipo)
                                    {
                                        command.Parameters.Clear();
                                        foreach (var param in CrearSqlCommand.getPatametros(true))
                                        {
                                            command.Parameters.Add(param);
                                        }
                                        command.Parameters.Add(new SqlParameter("@idAnticipo", item.idAnticipo));
                                        command.Parameters.Add(new SqlParameter("@idDestino", zona.clave));
                                        command.Parameters.Add(new SqlParameter("@idTipoAnticipo", item.tipoAnticipo.idTipoAnticipo));
                                        command.Parameters.Add(new SqlParameter("@sencillo2", item.sencillo2Ejes));
                                        command.Parameters.Add(new SqlParameter("@sencillo3", item.sencillo3Ejes));
                                        command.Parameters.Add(new SqlParameter("@full", item.full));

                                        command.ExecuteNonQuery();
                                        int id = 0;
                                        if (CrearSqlCommand.validarCorrecto(command, out id))
                                        {
                                            item.idAnticipo = id;
                                        }
                                        else
                                        {
                                            tran.Rollback();
                                            return new OperationResult(command);
                                        }
                                    }
                                }
                            }

                            if (zona.listaCasetas != null)
                            {
                                if (zona.listaCasetas.Count > 0)
                                {
                                    command.CommandText = "sp_saveCasetasDestinos";
                                    command.Parameters.Clear();
                                    foreach (var param  in CrearSqlCommand.getPatametros())
                                    {
                                        command.Parameters.Add(param);
                                    }
                                    string xml = GenerateXML.generarListaCadena(zona.listaCasetas.Select(s => s.idCaseta.ToString()).ToList());
                                    command.Parameters.Add(new SqlParameter("@idDestino", zona.clave));
                                    command.Parameters.Add(new SqlParameter("@xml", xml));
                                    command.ExecuteNonQuery();

                                    if (!CrearSqlCommand.validarCorrecto(command))
                                    {
                                        tran.Rollback();
                                        return new OperationResult(command);
                                    }
                                }
                            }
                            tran.Commit();
                            return new OperationResult(command, zona);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        internal OperationResult saveRutasDestinoAtlante(ref Zona destino)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_saveDestinosAtlante";
                        command.Parameters.Add(new SqlParameter("@descripcion", destino.descripcion));
                        command.Parameters.Add(new SqlParameter("@claveEmpresa", destino.clave_empresa));
                        command.Parameters.Add(new SqlParameter("@idEstado", destino.estado.idEstado));
                        command.Parameters.Add(new SqlParameter("@correo", destino.correo));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter4 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter4);
                        SqlParameter parameter5 = new SqlParameter("@identity", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter3 = command.Parameters.Add(parameter5);
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        destino.clave = (int)parameter3.Value;
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = destino,
                            identity = new int?((int)parameter3.Value)
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
            return result;
        }

        public OperationResult saveRutasDestinoIncompletos(Zona zona)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_saveRutasDestinoIncompletos";
                        command.Parameters.Add(new SqlParameter("@clave", zona.clave));
                        command.Parameters.Add(new SqlParameter("@precioSencillo", zona.costoSencillo));
                        command.Parameters.Add(new SqlParameter("@costoSencillo35", zona.costoSencillo35));
                        command.Parameters.Add(new SqlParameter("@precioFull", zona.costoFull));
                        command.Parameters.Add(new SqlParameter("@km", zona.km));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        Zona zona2 = new Zona();
                        using (SqlDataReader reader2 = reader)
                        {
                            while (reader2.Read())
                            {
                                zona2 = new Zona
                                {
                                    clave_empresa = (int)reader2["Clave_empresa"],
                                    clave = (int)reader2["Clave"],
                                    descripcion = (string)reader2["Descripcion"],
                                    activo = (bool)reader2["Activo"],
                                    costoSencillo = (decimal)reader2["costoSencillo"],
                                    costoSencillo35 = (decimal)reader2["Precio_sencillo35"],
                                    costoFull = (decimal)reader2["costoFull"],
                                    km = (decimal)reader2["km"],
                                    claveZona = (string)reader2["claveZona"],
                                    claveCliente = (int)reader2["Clave_cliente"],
                                    claveExtra = (string)reader2["claveExtra"]
                                };
                            }
                        }
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = zona2
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
            return result;
        }
        public OperationResult saveNewOrigenPegaso(ref OrigenPegaso origen)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    con.Open();
                    using (SqlTransaction tran = con.BeginTransaction())
                    {
                        using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_saveNewOrigenPegaso", true))
                        {
                            comad.Transaction = tran;

                            comad.Parameters.Add(new SqlParameter("@idCliente", origen.idCliente));
                            comad.Parameters.Add(new SqlParameter("@origen", origen.nombre));
                            comad.Parameters.Add(new SqlParameter("@idEmpresa", origen.idEmpresa));
                            comad.ExecuteNonQuery();
                            int id = 0;
                            if (CrearSqlCommand.validarCorrecto(comad, out id))
                            {
                                origen.idOrigen = id;
                            }
                            else
                            {
                                tran.Rollback();
                                return new OperationResult(comad);
                            }
                            tran.Commit();
                            return new OperationResult(comad, origen);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getAnticipos(int idDestino)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getAnticiposByIdDestino"))
                    {
                        List<Anticipo> lista = new List<Anticipo>();
                        comad.Parameters.Add(new SqlParameter("@idDestino", idDestino));
                        con.Open();
                        using (SqlDataReader sqlReader = comad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                Anticipo anticipo = new Anticipo
                                {
                                    idAnticipo = (int)sqlReader["anticipo_id"],
                                    idDestino = (int)sqlReader["anticipo_idDestino"],
                                    full = (decimal)sqlReader["anticipo_full"],
                                    sencillo2Ejes = (decimal)sqlReader["anticipo_2ejes"],
                                    sencillo3Ejes  =(decimal)sqlReader["anticipo_3ejes"]                                    
                                };
                                TipoAnticipo tipoAnticipo = new mapComunes().mapTipoAnticipo(sqlReader);
                                if (tipoAnticipo == null) return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");

                                anticipo.tipoAnticipo = tipoAnticipo;
                                lista.Add(anticipo);
                            }
                        }
                        return new OperationResult(comad, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        public OperationResult getAnticiposByNumViaje(int idViaje)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getAnticiposByNumViaje"))
                    {
                        List<Anticipo> lista = new List<Anticipo>();
                        comad.Parameters.Add(new SqlParameter("@numGuiaId", idViaje));
                        con.Open();
                        using (SqlDataReader sqlReader = comad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                Anticipo anticipo = new Anticipo
                                {
                                    idAnticipo = (int)sqlReader["anticipo_id"],
                                    idDestino = (int)sqlReader["anticipo_idDestino"],
                                    full = (decimal)sqlReader["anticipo_full"],
                                    sencillo2Ejes = (decimal)sqlReader["anticipo_2ejes"],
                                    sencillo3Ejes = (decimal)sqlReader["anticipo_3ejes"],
                                    costo = (decimal)sqlReader["costo"],
                                    observaciones = (string)sqlReader["observaciones"]
                                };
                                TipoAnticipo tipoAnticipo = new mapComunes().mapTipoAnticipo(sqlReader);
                                if (tipoAnticipo == null) return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");

                                anticipo.tipoAnticipo = tipoAnticipo;
                                lista.Add(anticipo);
                            }
                        }
                        return new OperationResult(comad, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        public OperationResult getCasetasByDestino(int idDestino)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getCasetaByDestino"))
                    {
                        List<Caseta> lista = new List<Caseta>();
                        comad.Parameters.Add(new SqlParameter("@idDestino", idDestino));
                        con.Open();
                        using (SqlDataReader sqlReader = comad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                Caseta caseta = new mapComunes().mapCaseta(sqlReader);
                                if (caseta == null) return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");                                
                                lista.Add(caseta);
                            }
                        }
                        return new OperationResult(comad, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
}
