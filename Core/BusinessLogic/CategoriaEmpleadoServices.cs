﻿using Core.Models;
using Core.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.BusinessLogic
{
    public class CategoriaEmpleadoServices
    {
        public OperationResult getCategoriaEmpleadoALLorById(int id)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getCategoriaEmpleadoALLorById";
                        command.Parameters.Add(new SqlParameter("@id", id));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        connection.Open();
                        var i = command.ExecuteReader();
                        List<CategoriaEmpleado> listCategoria = new List<CategoriaEmpleado>();

                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                listCategoria.Add(new CategoriaEmpleado
                                {
                                    clave_empresa = (int)sqlReader["Clave_empresa"],
                                    clave = (int)sqlReader["Clave"],
                                    descripcion = (string)sqlReader["Descripcion"],
                                    activo = (bool)sqlReader["Activo"]
                                });
                            }
                        }

                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)codigo.Value, result = listCategoria };
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }
    }
}
