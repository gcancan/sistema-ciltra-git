﻿namespace Core.BusinessLogic
{
    using Core.Models;
    using Core.Utils;
    using CoreFletera.Models;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using CoreFletera.BusinessLogic;
    using CoreFletera.Models;
    public class TipoUnidadTransporteServices
    {
        public OperationResult getGruposTiposUnidad()
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getGruposTiposUnidad";
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        List<GrupoTipoUnidades> list = new List<GrupoTipoUnidades>();
                        using (SqlDataReader reader2 = reader)
                        {
                            while (reader2.Read())
                            {
                                GrupoTipoUnidades item = new GrupoTipoUnidades
                                {
                                    grupo = (string)reader2["grupo"]
                                };
                                list.Add(item);
                            }
                        }
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = list
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
            return result;
        }

        public OperationResult getGruposTiposUnidad(int idEmpresa)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getGruposTiposUnidadByEmpresa";
                        command.Parameters.Add(new SqlParameter("@idEmpresa", idEmpresa));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        List<GrupoTipoUnidades> list = new List<GrupoTipoUnidades>();
                        using (SqlDataReader reader2 = reader)
                        {
                            while (reader2.Read())
                            {
                                GrupoTipoUnidades item = new GrupoTipoUnidades
                                {
                                    grupo = (string)reader2["grupo"]
                                };
                                list.Add(item);
                            }
                        }
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = list
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
            return result;
        }

        public OperationResult getTiposUnidadesALLorById(int id)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getTiposUnidadesALLorById";
                        command.Parameters.Add(new SqlParameter("@id", id));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        List<TipoUnidad> list = new List<TipoUnidad>();
                        using (SqlDataReader reader2 = reader)
                        {
                            while (reader2.Read())
                            {
                                TipoUnidad item = new TipoUnidad
                                {
                                    clave = (int)reader2["Clave"],
                                    descripcion = (string)reader2["Descripcion"],
                                    NoEjes = (string)reader2["NoEjes"],
                                    NoLlantas = (string)reader2["NoLlantas"],
                                    TonelajeMax = Convert.ToDecimal((string)reader2["TonelajeMax"]),
                                    clasificacion = DBNull.Value.Equals(reader2["clasificacion"]) ? "" : ((string)reader2["clasificacion"]),
                                    bCombustible = !DBNull.Value.Equals(reader2["bCombustible"]) && ((bool)reader2["bCombustible"])
                                };
                                list.Add(item);
                            }
                        }
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = list
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
            return result;
        }
        public OperationResult saveTipoUnidad(ref TipoUnidad tipoUnidad)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    con.Open();
                    using (SqlTransaction tran = con.BeginTransaction())
                    {
                        using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_saveTipoUnidad", true))
                        {
                            comad.Transaction = tran;
                            comad.Parameters.Add(new SqlParameter("@idTipoUnidad", tipoUnidad.clave));
                            comad.Parameters.Add(new SqlParameter("@nombreTipoUnidad", tipoUnidad.descripcion));
                            comad.Parameters.Add(new SqlParameter("@noLlantas", tipoUnidad.NoLlantas.ToString()));
                            comad.Parameters.Add(new SqlParameter("@noEjes", tipoUnidad.NoEjes.ToString()));
                            comad.Parameters.Add(new SqlParameter("@tonMax", tipoUnidad.TonelajeMax.ToString()));
                            comad.Parameters.Add(new SqlParameter("@clasificacion", tipoUnidad.clasificacion));
                            comad.Parameters.Add(new SqlParameter("@combustible", tipoUnidad.bCombustible));
                            comad.Parameters.Add(new SqlParameter("@noRefacciones", tipoUnidad.cantRefacciones));
                            comad.Parameters.Add(new SqlParameter("@activo", tipoUnidad.activo));

                            comad.ExecuteNonQuery();
                            int id = 0;
                            if (CrearSqlCommand.validarCorrecto(comad, out id))
                            {
                                tipoUnidad.clave = id;
                                comad.CommandText = "sp_saveConfigEjeLlantas";
                                foreach (var config in tipoUnidad.listaDescripcionLlantas)
                                {
                                    comad.Parameters.Clear();
                                    foreach (var param in CrearSqlCommand.getPatametros(true))
                                    {
                                        comad.Parameters.Add(param);
                                    }
                                    comad.Parameters.Add(new SqlParameter("@idTipoUnidad", tipoUnidad.clave));
                                    comad.Parameters.Add(new SqlParameter("@noEje", config.noEje));
                                    comad.Parameters.Add(new SqlParameter("@tipoEje", (int)config.tipoEjeLlanta));
                                    comad.Parameters.Add(new SqlParameter("@noLlantas", config.cantLlantas));

                                    comad.ExecuteNonQuery();
                                    if (CrearSqlCommand.validarCorrecto(comad, out id))
                                    {
                                        config.idDescripcionLlantas = id;
                                    }
                                    else
                                    {
                                        tran.Rollback();
                                        return new OperationResult(comad);
                                    }
                                }
                                
                            }
                            else
                            {
                                tran.Rollback();
                                return new OperationResult(comad);
                            }
                            tran.Commit();
                            return new OperationResult(comad, tipoUnidad);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        public OperationResult getAllTipoUnidades()
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getAllTipoUnidades"))
                    {
                        List<TipoUnidad> listaTipoUnidad = new List<TipoUnidad>();
                        con.Open();
                        using (SqlDataReader sqlReader = comad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                TipoUnidad tipoUnidad = new mapComunes().mapTipoUnidad(sqlReader);
                                if (tipoUnidad == null)
                                {
                                    return new OperationResult(ResultTypes.error, "ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");
                                }
                                listaTipoUnidad.Add(tipoUnidad);
                            }
                        }
                        return new OperationResult(comad, listaTipoUnidad);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getAllDescripcionLlantaByIdTipoUnidad(int idTipoUnidad = 0)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "getAllDescripcionLlantaByIdTipoUnidad"))
                    {
                        List<DescripcionLlantas> listaDescripcion = new List<DescripcionLlantas>();
                        con.Open();
                        using (SqlDataReader sqlReader = comad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                DescripcionLlantas descripcion = new mapComunes().mapDescripcionLlantas(sqlReader);
                                if (descripcion == null)
                                {
                                    return new OperationResult(ResultTypes.error, "ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");
                                }
                                listaDescripcion.Add(descripcion);
                            }
                        }
                        return new OperationResult(comad, listaDescripcion);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        public OperationResult sincronizarTiposUnidad(List<TipoUnidad> listaTiposUnidad)
        {
            try
            {
                string xml = GenerateXML.generarListaTipoSinc(listaTiposUnidad);
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    con.Open();
                    using (SqlTransaction tran = con.BeginTransaction("tr"))
                    {
                        using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_sincronizarTiposUnidad", false, true))
                        {
                            comad.Transaction = tran;
                            comad.Parameters.Add(new SqlParameter("@xml", xml));
                            comad.ExecuteNonQuery();
                            if (!CrearSqlCommand.validarCorrecto(comad))
                            {
                                tran.Rollback("tr");
                                return new OperationResult(comad);
                            }
                            tran.Commit();
                            return new OperationResult(comad);
                        }
                    }
                }
            }
            catch (Exception ex)     
            {
                return new OperationResult(ex);
            }
        }
    }
}
