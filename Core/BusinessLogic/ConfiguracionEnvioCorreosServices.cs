﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using Core.Utils;
using System.Data;
using System.Data.SqlClient;
using Core.BusinessLogic;

namespace CoreFletera.BusinessLogic
{
    public class ConfiguracionEnvioCorreosServices
    {
        public OperationResult saveCorreoElectronico(ref ConfiguracionEnvioCorreo envioCorreo)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    con.Open();
                    using (SqlTransaction tran = con.BeginTransaction())
                    {
                        using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_saveCorreoElectronico", true))
                        {
                            comad.Transaction = tran;
                            comad.Parameters.Add(new SqlParameter("@idCorreo", envioCorreo.idConfiguracionEnvioCorreo));
                            comad.Parameters.Add(new SqlParameter("@direccion", envioCorreo.direccion));
                            comad.Parameters.Add(new SqlParameter("@dominio", envioCorreo.dominio));
                            comad.Parameters.Add(new SqlParameter("@tipoDominio", (int)envioCorreo.enumDominios));
                            comad.Parameters.Add(new SqlParameter("@activo", envioCorreo.activo));
                            comad.Parameters.Add(new SqlParameter("@nombre", envioCorreo.nomDes));
                            int id = 0;
                            comad.ExecuteNonQuery();
                            if (CrearSqlCommand.validarCorrecto(comad, out id))
                            {
                                envioCorreo.idConfiguracionEnvioCorreo = id;
                                foreach (var proceso in envioCorreo.listaProcesosEnvioCorreo)
                                {
                                    comad.CommandText = "sp_saveRelacionCorreoElectronico";
                                    comad.Parameters.Clear();
                                    foreach (var param in CrearSqlCommand.getPatametros())
                                    {
                                        comad.Parameters.Add(param);
                                    }
                                    comad.Parameters.Add(new SqlParameter("@idCorreo", envioCorreo.idConfiguracionEnvioCorreo));
                                    comad.Parameters.Add(new SqlParameter("@tipoProceso", (int)proceso));
                                    comad.ExecuteNonQuery();
                                    if (!CrearSqlCommand.validarCorrecto(comad))
                                    {
                                        tran.Rollback();
                                        return new OperationResult(comad);
                                    }
                                }
                            }
                            else
                            {
                                tran.Rollback();
                                return new OperationResult(comad);
                            }
                            tran.Commit();
                            return new OperationResult(comad, envioCorreo);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getAllCorreosElectronicos()
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getAllCorreosElectronicos"))
                    {
                        List<ConfiguracionEnvioCorreo> lista = new List<ConfiguracionEnvioCorreo>();
                        con.Open();
                        using (SqlDataReader sqlReader = comad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                ConfiguracionEnvioCorreo envioCorreo = new mapComunes().mapCorreosElectronicos(sqlReader);
                                if (envioCorreo == null)
                                {
                                    return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");
                                }
                                lista.Add(envioCorreo);
                            }
                        }
                        return new OperationResult(comad, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getProcesosByIdCorreo(int idCorreo)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getProcesosByIdCorreo"))
                    {
                        comad.Parameters.Add(new SqlParameter("@idCorreo", idCorreo));
                        List<enumProcesosEnvioCorreo> lista = new List<enumProcesosEnvioCorreo>();
                        con.Open();
                        using (SqlDataReader sqlReader = comad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                lista.Add((enumProcesosEnvioCorreo)sqlReader["tipoProceso"]);
                            }
                        }
                        return new OperationResult(comad, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getCorreosByProceso(enumProcesosEnvioCorreo enumProcesos)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getCorreosByProcesp"))
                    {
                        comad.Parameters.Add(new SqlParameter("@tipoProceso", (int)enumProcesos));
                        List<ConfiguracionEnvioCorreo> lista = new List<ConfiguracionEnvioCorreo>();
                        con.Open();
                        using (SqlDataReader sqlReader = comad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                ConfiguracionEnvioCorreo envioCorreo = new mapComunes().mapCorreosElectronicos(sqlReader);
                                if (envioCorreo == null)
                                {
                                    return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");
                                }
                                lista.Add(envioCorreo);
                            }
                        }
                        return new OperationResult(comad, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
}
