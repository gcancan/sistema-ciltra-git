﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using System.Data;
using System.Data.SqlClient;
using Core.Utils;

namespace Core.BusinessLogic
{
    public class MotivoEntradaService
    {
        public OperationResult getMotivos()
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_SalidasGetMotivos";

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        connection.Open();
                        var i = command.ExecuteReader();
                        List<MotivoEntrada> list = new List<MotivoEntrada>();

                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                MotivoEntrada operadores = new MotivoEntrada();
                                operadores = (new MotivoEntrada
                                {
                                    idMotivo = (int)sqlReader["idMotivo"],
                                    motivo = (string)sqlReader["descripcion"]                                  
                                });
                                list.Add(operadores);
                            }
                        }

                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)codigo.Value, result = list };
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }
    }
}
