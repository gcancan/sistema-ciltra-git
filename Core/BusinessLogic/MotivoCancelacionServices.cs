﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using Core.Utils;
using System.Data;
using System.Data.SqlClient;
using Core.BusinessLogic;

namespace CoreFletera.BusinessLogic
{
    public class MotivoCancelacionServices
    {
        public OperationResult saveMotivosCancelacion(ref MotivoCancelacion motivo)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    con.Open();
                    using (SqlTransaction tran = con.BeginTransaction())
                    {
                        using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_saveMotivosCancelacion", true))
                        {
                            comad.Transaction = tran;
                            comad.Parameters.Add(new SqlParameter("@idMotivoCancelacion", motivo.idMotivoCancelacion));
                            comad.Parameters.Add(new SqlParameter("@descripcion", motivo.descripcion));
                            comad.Parameters.Add(new SqlParameter("@requiereObservacion", motivo.requiereObservacion));
                            comad.Parameters.Add(new SqlParameter("@activo", motivo.activo));
                            comad.Parameters.Add(new SqlParameter("@usuario", motivo.usuario));
                            comad.ExecuteNonQuery();
                            int id = 0;
                            if (CrearSqlCommand.validarCorrecto(comad, out id))
                            {
                                motivo.idMotivoCancelacion = id;
                                if (motivo.activo)
                                {
                                    comad.CommandText = "sp_saveRelacionMotivosCancelacionProcesos";
                                    foreach (var relacion in motivo.listaProcesos)
                                    {
                                        comad.Parameters.Clear();
                                        foreach (var param in CrearSqlCommand.getPatametros())
                                        {
                                            comad.Parameters.Add(param);
                                        }
                                        comad.Parameters.Add(new SqlParameter("@idMotivoCancelacion", motivo.idMotivoCancelacion));
                                        comad.Parameters.Add(new SqlParameter("@idProceso", (int)relacion));
                                        comad.ExecuteNonQuery();
                                        if (!CrearSqlCommand.validarCorrecto(comad))
                                        {
                                            tran.Rollback();
                                            return new OperationResult(comad);
                                        }
                                    }
                                }
                                
                            }
                            else
                            {
                                tran.Rollback();
                                return new OperationResult(comad);
                            }

                            tran.Commit();
                            return new OperationResult(comad, motivo);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getAllMotivosCancelacion()
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(con, "sp_getAllMotivosCancelacion"))
                    {
                        List<MotivoCancelacion> lista = new List<MotivoCancelacion>();
                        con.Open();
                        using (SqlDataReader sqlReader = command.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                MotivoCancelacion motivo = new mapComunes().mapMotivoCancelacion(sqlReader);
                                if (motivo == null)
                                {
                                    return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");
                                }
                                lista.Add(motivo);
                            }
                        }
                        return new OperationResult(command, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getAllMotivosByProceso(enumProcesos enumProcesos)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(con, "sp_getAllMotivosByProceso"))
                    {
                        command.Parameters.Add(new SqlParameter("@idProceso", (int)enumProcesos));
                        List<MotivoCancelacion> lista = new List<MotivoCancelacion>();
                        con.Open();
                        using (SqlDataReader sqlReader = command.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                MotivoCancelacion motivo = new mapComunes().mapMotivoCancelacion(sqlReader);
                                if (motivo == null)
                                {
                                    return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");
                                }
                                lista.Add(motivo);
                            }
                        }
                        return new OperationResult(command, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getProcesosBiIdMOtivoCancelacion(int idMotivoCancelacion)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(con, "sp_getAllProcesosByIdMotivo"))
                    {
                        command.Parameters.Add(new SqlParameter("@idMotivoCancelacion", idMotivoCancelacion));
                        List<enumProcesos> lista = new List<enumProcesos>();
                        con.Open();
                        using (SqlDataReader sqlReader = command.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                enumProcesos proceso = (enumProcesos)sqlReader["idProceso"];                                
                                lista.Add(proceso);
                            }
                        }
                        return new OperationResult(command, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
}
