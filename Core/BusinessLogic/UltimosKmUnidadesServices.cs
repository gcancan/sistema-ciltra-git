﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using Core.Utils;
using System.Data;
using System.Data.SqlClient;
namespace CoreFletera.BusinessLogic
{
    public class UltimosKmUnidadesServices
    {
        public OperationResult getUltimosKmUnidades(int idEmpresa, List<string> listaZonas, List<string> listaOperaciones)
        {
            int id = 0;
            try
            {
                var xmlZonas = GenerateXML.generarListaCadena(listaZonas);
                var xmlClientes = GenerateXML.generarListaCadena(listaOperaciones);
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(con, "sp_getUltimosKmTractores"))
                    {
                        command.Parameters.Add(new SqlParameter("@idEmpresa", idEmpresa));
                        command.Parameters.Add(new SqlParameter("@xmlZonas", xmlZonas));
                        command.Parameters.Add(new SqlParameter("@xmlClientes", xmlClientes));
                        con.Open();
                        List<UltimosKmUnidades> lista = new List<UltimosKmUnidades>();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                id = (int)reader["vale"];
                                if (id == 41245)
                                {

                                }
                                UltimosKmUnidades ultimosKm = new UltimosKmUnidades
                                {
                                    unidad = (string)reader["idUnidad"],
                                    empresa = (string)reader["empresa"],
                                    zonaOperativa = (string)reader["zonaOperativa"],
                                    operacion = (string)reader["operacion"],
                                    fecha = (DateTime)reader["fecha"],
                                    km = (decimal)reader["km"],
                                    vale = (int)reader["vale"]
                                };
                                lista.Add(ultimosKm);
                            }
                        }
                        return new OperationResult(command, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
}
