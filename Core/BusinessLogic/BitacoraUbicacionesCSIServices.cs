﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using Core.Utils;
using System.Data;
using System.Data.SqlClient;

namespace CoreFletera.BusinessLogic
{
    public class BitacoraUbicacionesCSIServices
    {
        public OperationResult getBitacoraUbicacionCSI(DateTime fechaInicial, DateTime fechaFinal, int idEmpresa, List<string> listaZonas, List<string> listaOperaciones)
        {
            try
            {
                string xmlZonas = GenerateXML.generarListaCadena(listaZonas);
                string xmlOperaciones = GenerateXML.generarListaCadena(listaOperaciones);
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getRegistrosCsiByFechas"))
                    {
                        comad.Connection = con;
                        comad.Parameters.Add(new SqlParameter("@idEmpresa", idEmpresa));
                        comad.Parameters.Add(new SqlParameter("@fechaInicial", fechaInicial));
                        comad.Parameters.Add(new SqlParameter("@fechaFinal", fechaFinal));
                        comad.Parameters.Add(new SqlParameter("@xmlZonas", xmlZonas));
                        comad.Parameters.Add(new SqlParameter("@xmlOperaciones", xmlOperaciones));
                        List<DetallesRegistosCSI> list = new List<DetallesRegistosCSI>();
                        con.Open();
                        using (SqlDataReader sqlReader = comad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                DetallesRegistosCSI detallesRegistos = new DetallesRegistosCSI
                                {
                                    idTractor = (string)sqlReader["idTractor"],
                                    fecha = (DateTime)sqlReader["fecha"],
                                    GeoCerca = new GeoCerca
                                    {
                                        idGeoCerca = (int)sqlReader["idGeoCerca"],
                                        nombre = (string)sqlReader["nombre"],
                                        granja = (bool)sqlReader["granja"],
                                        planta = (bool)sqlReader["planta"],
                                        taller = (bool)sqlReader["taller"],
                                        clasificacion = (enumClasificacionGeoCerca)Convert.ToInt32(sqlReader["clasificacion"])
                                    },
                                    tipoRegistroCSI = (TipoRegistroCSI)Convert.ToInt32(sqlReader["isEntrada"])
                                };
                                list.Add(detallesRegistos);
                            }
                        }
                        return new OperationResult(comad, list);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
}
