﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using Core.Utils;
using System.Data;
using System.Data.SqlClient;
using CoreFletera.BusinessLogic;

namespace Core.BusinessLogic
{
    public class EstadoServices
    {
        public OperationResult getEstadosAllorById(int id=0)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getEstadosAllorById";
                        command.Parameters.Add(new SqlParameter("@id", id));

                        var sValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var sMensaje = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        connection.Open();
                        var i = command.ExecuteReader();
                        List<Estado> listEstado = new List<Estado>();
                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                Estado estado = new mapComunes().mapEstados(sqlReader);
                                if (estado == null)
                                {
                                    return new OperationResult { valor = 1, mensaje = "Error al construir el estado", result = null };
                                }
                                listEstado.Add(estado);
                            }
                        }

                        return new OperationResult { valor = (int)sValor.Value, mensaje = (string)sMensaje.Value, identity = 0, result = listEstado};
                    }
                }
            }
            catch (Exception e)
            {
                return new OperationResult { valor = 1, mensaje = e.Message, result = null };
            }
        }
        public OperationResult saveEstado(ref Estado estado)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    con.Open();
                    using (SqlTransaction tran = con.BeginTransaction())
                    {
                        using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_saveEstado", true))
                        {
                            comad.Transaction = tran;
                            comad.Parameters.Add(new SqlParameter("@nombreEstado", estado.nombre));
                            comad.Parameters.Add(new SqlParameter("@idPais", estado.pais.idPais));
                            int id = 0;
                            comad.ExecuteNonQuery();
                            if (CrearSqlCommand.validarCorrecto(comad, out id))
                            {
                                estado.idEstado = id;
                                tran.Commit();
                            }
                            else
                            {
                                tran.Rollback();
                            }
                            return new OperationResult(comad, estado);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
}
