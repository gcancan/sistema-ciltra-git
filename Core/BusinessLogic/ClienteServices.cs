﻿namespace Core.BusinessLogic
{
    using Core.Interfaces;
    using Core.Models;
    using Core.Utils;
    using CoreFletera.BusinessLogic;
    using CoreFletera.Models;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;

    public class ClienteServices
    {
        public OperationResult getClientesALLorById(int idEmpresa, int id)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getClientesALLorById";
                        command.Parameters.Add(new SqlParameter("@id", id));
                        command.Parameters.Add(new SqlParameter("@idEmpresa", idEmpresa));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        List<Cliente> list = new List<Cliente>();
                        using (SqlDataReader reader2 = reader)
                        {
                            while (reader2.Read())
                            {
                                Cliente item = new mapComunes().mapCliente(reader2);
                                list.Add(item);
                            }
                        }
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = list
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
            return result;
        }

        public OperationResult getClientesByRuta(int idOrigen, int idDestino, int? idClasificacion)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getClientesByRuta", false))
                    {
                        command.Parameters.Add(new SqlParameter("@idOrigen", idOrigen));
                        command.Parameters.Add(new SqlParameter("@idDestino", idDestino));
                        command.Parameters.Add(new SqlParameter("@idClasificacion", idClasificacion));
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        List<Cliente> list = new List<Cliente>();
                        using (SqlDataReader reader2 = reader)
                        {
                            while (reader2.Read())
                            {
                                Cliente item = new mapComunes().mapCliente(reader2);
                                list.Add(item);
                            }
                        }
                        result = new OperationResult(command, list);
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
            return result;
        }

        public OperationResult getClientesByZonasOperativas(List<string> listaId, int idEmpresa)
        {
            try
            {
                string xml = GenerateXML.generarListaCadena(listaId);
                if (string.IsNullOrEmpty(xml))
                {
                    return new OperationResult(ResultTypes.error, "ERROR AL CONSTRUIR EL XML");
                }
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand commad = CrearSqlCommand.getCommand(con, "sp_getClientesByZonasOperativas"))
                    {
                        commad.Parameters.Add(new SqlParameter("xml", xml));
                        commad.Parameters.Add(new SqlParameter("idEmpresa", idEmpresa));
                        List<OperacionFletera> listaOperacion = new List<OperacionFletera>();
                        con.Open();
                        using (SqlDataReader sqlReader = commad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                OperacionFletera cliente = new mapComunes().mapOperacionFletera(sqlReader);
                                listaOperacion.Add(cliente);
                            }
                        }
                        return new OperationResult(commad, listaOperacion);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        private OperationResult mapFraccion(int idFraccion)
        {
            try
            {
                OperationResult result = new FraccionSvc().getFraccionALLorById(idFraccion);
                if (result.typeResult == ResultTypes.error)
                {
                    return new OperationResult
                    {
                        result = null,
                        valor = 1,
                        mensaje = result.mensaje
                    };
                }
                if (result.typeResult == ResultTypes.recordNotFound)
                {
                    return new OperationResult
                    {
                        result = null,
                        valor = 0,
                        mensaje = result.mensaje
                    };
                }
                if (result.typeResult == ResultTypes.success)
                {
                    List<Fraccion> list = (List<Fraccion>)result.result;
                    return new OperationResult
                    {
                        result = list[0],
                        valor = 0,
                        mensaje = result.mensaje
                    };
                }
            }
            catch (Exception exception)
            {
                return new OperationResult
                {
                    result = null,
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            return new OperationResult();
        }

        private OperationResult mapImpuesto(int idCliente)
        {
            try
            {
                OperationResult result = new ImpuestosSvc().getImpuestosALLorById(idCliente);
                if (result.typeResult == ResultTypes.error)
                {
                    return new OperationResult
                    {
                        result = null,
                        valor = 1,
                        mensaje = result.mensaje
                    };
                }
                if (result.typeResult == ResultTypes.recordNotFound)
                {
                    return new OperationResult
                    {
                        result = null,
                        valor = 0,
                        mensaje = result.mensaje
                    };
                }
                if (result.typeResult == ResultTypes.success)
                {
                    List<Impuestos> list = (List<Impuestos>)result.result;
                    return new OperationResult
                    {
                        result = list[0],
                        valor = 0,
                        mensaje = result.mensaje
                    };
                }
            }
            catch (Exception exception)
            {
                return new OperationResult
                {
                    result = null,
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            return new OperationResult();
        }
        public OperationResult saveCliente(ref Cliente cliente)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    con.Open();
                    using (SqlTransaction tran = con.BeginTransaction())
                    {
                        using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_saveClientes", true))
                        {
                            comad.Transaction = tran;
                            comad.Parameters.Add(new SqlParameter("@idCliente", cliente.clave));
                            comad.Parameters.Add(new SqlParameter("@idEmpresa", cliente.empresa.clave));
                            comad.Parameters.Add(new SqlParameter("@nombre", cliente.nombre));
                            comad.Parameters.Add(new SqlParameter("@domicilio", cliente.domicilio));
                            comad.Parameters.Add(new SqlParameter("@RFC", cliente.rfc));
                            comad.Parameters.Add(new SqlParameter("@iva", cliente.impuesto.clave));
                            comad.Parameters.Add(new SqlParameter("@activo", cliente.activo));
                            comad.Parameters.Add(new SqlParameter("@flete", cliente.impuestoFlete.clave));
                            comad.Parameters.Add(new SqlParameter("@idFraccion", cliente.fraccion == null ? null : (int?)cliente.fraccion.clave));
                            comad.Parameters.Add(new SqlParameter("@penalizacion", cliente.penalizacion));
                            comad.Parameters.Add(new SqlParameter("@telefono", cliente.telefono));
                            comad.Parameters.Add(new SqlParameter("@idColonia", cliente.objColonia.idColonia));
                            comad.Parameters.Add(new SqlParameter("@omitirRemision", cliente.omitirRemision));
                            comad.Parameters.Add(new SqlParameter("@cobroKM", cliente.cobroXkm));
                            comad.Parameters.Add(new SqlParameter("@alias", cliente.alias));
                            comad.ExecuteNonQuery();
                            int id = 0;
                            if (CrearSqlCommand.validarCorrecto(comad, out id))
                            {
                                cliente.clave = id;
                                tran.Commit();
                            }
                            else
                            {
                                tran.Rollback();
                            }
                            return new OperationResult(comad, cliente);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getAllClienteByEmpresa(int idEmpresa)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getAllClienteByEmpresa"))
                    {
                        comad.Parameters.Add(new SqlParameter("@idEmpresa", idEmpresa));
                        List<Cliente> listaCliente = new List<Cliente>();
                        con.Open();
                        using (SqlDataReader sqlReader = comad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                Cliente cliente = new mapComunes().mapCliente(sqlReader, true);
                                if (cliente == null)
                                {
                                    return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");
                                }
                                listaCliente.Add(cliente);
                            }
                        }
                        return new OperationResult(comad, listaCliente);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public OperationResult sincronizarClientes(int idEmpresa, List<Cliente> listaCliente)
        {
            try
            {
                string xml = GenerateXML.getListaClientesSinc(listaCliente);
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    con.Open();
                    using (SqlTransaction tran =  con.BeginTransaction())
                    {
                        using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_sincronizarClientes", false, true))
                        {
                            comad.Transaction = tran;
                            comad.Parameters.Add(new SqlParameter("@idEmpresa", idEmpresa));
                            comad.Parameters.Add(new SqlParameter("@xml", xml));
                            comad.ExecuteNonQuery();
                            if (!CrearSqlCommand.validarCorrecto(comad))
                            {
                                tran.Rollback();
                                return new OperationResult(comad);
                            }
                            tran.Commit();
                            return new OperationResult(comad);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
}
