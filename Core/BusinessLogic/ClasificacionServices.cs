﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using System.Data.SqlClient;
using System.Data;
using Core.Utils;
namespace Core.BusinessLogic
{
    public class ClasificacionServices
    {
        public OperationResult getAllClasificaciones()
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getAllClasificaciones";
                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var spMensaje = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        connection.Open();
                        List<Clasificacion> listClas = new List<Clasificacion>();
                        using (SqlDataReader sqlReader = command.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                Clasificacion clas = new Clasificacion
                                {
                                    idClasificacion = (int)sqlReader["idClasificacion"],
                                    clasificacion = (string)sqlReader["clasificacion"]
                                };
                                listClas.Add(clas);
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = listClas };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message, result = null };
            }
        }
    }
}
