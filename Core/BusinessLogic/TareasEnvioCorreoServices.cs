﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoreFletera.Models;
using Core.Models;
using Core.Utils;
using System.Data;
using System.Data.SqlClient;
using Core.BusinessLogic;
namespace CoreFletera.BusinessLogic
{
    public class TareasEnvioCorreoServices
    {
        public OperationResult getTareasEnviosCorreo()
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand commad = CrearSqlCommand.getCommand(connection, "sp_getTareasEnvioCorreo"))
                    {
                        List<TareasEnvioCorreo> listaTareas = new List<TareasEnvioCorreo>();
                        connection.Open();
                        using (SqlDataReader sqlReader = commad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                TareasEnvioCorreo tarea = new TareasEnvioCorreo
                                {
                                    idTareasEnvioCorreo = (int)sqlReader["tarea_clave"],
                                    fechaCreacion = (DateTime)sqlReader["tarea_fechaCreacion"],
                                    fechaEjecucion = (DateTime)sqlReader["tarea_fechaEjecucion"],
                                    descripcion = (string)sqlReader["tarea_descripcion"],
                                    titulo = (string)sqlReader["tarea_titulo"],
                                    Texto = (string)sqlReader["tarea_texto"],
                                    activo = (bool)sqlReader["tarea_activo"],
                                    diario = (bool)sqlReader["tarea_diario"]
                                };
                                listaTareas.Add(tarea);
                            }
                           
                        }

                        var respDetalle = getDetalleTareasEnviosCorreo(listaTareas.Select(s => s.idTareasEnvioCorreo.ToString()).ToList());
                        if (respDetalle.typeResult == ResultTypes.success)
                        {
                            List<DetalleTareasEnvioCorreo> listaDetalle = respDetalle.result as List<DetalleTareasEnvioCorreo>;
                            foreach (var item in listaTareas)
                            {
                                item.listaDetalles = listaDetalle.FindAll(s => s.idTareasEnvioCorreo == item.idTareasEnvioCorreo).ToList();
                            }
                        }

                        return new OperationResult(commad, listaTareas);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        public OperationResult getDetalleTareasEnviosCorreo(List<string> listaId)
        {
            try
            {
                string xml = GenerateXML.generarListaCadena(listaId);
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand commad = CrearSqlCommand.getCommand(connection, "sp_getDetallesTareasEnvioCorreo"))
                    {
                        commad.Parameters.Add(new SqlParameter("@xml", xml));
                        List<DetalleTareasEnvioCorreo> listaDetalle = new List<DetalleTareasEnvioCorreo>();
                        connection.Open();
                        using (SqlDataReader sqlReader = commad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                DetalleTareasEnvioCorreo detalle = new DetalleTareasEnvioCorreo
                                {
                                    idDetalleTareasEnvioCorreo = (int)sqlReader["idDetalleTareasEnvioCorreo"],
                                    idTareasEnvioCorreo = (int)sqlReader["idTareasEnvioCorreo"]
                                };
                                ConfiguracionEnvioCorreo correo = new mapComunes().mapCorreosElectronicos(sqlReader);
                                if (correo != null)
                                {
                                    detalle.correoElectronico = correo;
                                }
                                listaDetalle.Add(detalle);
                            }

                        }
                        return new OperationResult(commad, listaDetalle);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getCumpleAñosPersonal()
        {
            try
            {                
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand commad = CrearSqlCommand.getCommand(connection, "sp_getCumpleañosPersonal"))
                    {                        
                        List<CumpleañosPersonal> listaCumpleAños = new List<CumpleañosPersonal>();
                        connection.Open();
                        using (SqlDataReader sqlReader = commad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                CumpleañosPersonal cumpleaños = new CumpleañosPersonal
                                {
                                    idPersonal = (int)sqlReader["idPersonal"],
                                    nombre = (string)sqlReader["nombre"],
                                    empresa = (string)sqlReader["Empresa"],
                                    departamento = (string)sqlReader["departamento"],
                                    puesto = (string)sqlReader["puesto"],
                                    fechaNacimiento = (DateTime)sqlReader["fechaNacimiento"],
                                    dia = (int)sqlReader["dia"],
                                    años = (int)sqlReader["años"],
                                };
                                listaCumpleAños.Add(cumpleaños);
                            }
                        }
                        return new OperationResult(commad, listaCumpleAños);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult saveTareaCorreosEnviados(TareasEnvioCorreo tarea)
        {
            try
            {                
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    connection.Open();
                    using (SqlTransaction tran = connection.BeginTransaction())
                    {                       
                        using (SqlCommand commad = CrearSqlCommand.getCommand(connection, "sp_saveTareasCorreosEnviados"))
                        {
                            commad.Transaction = tran;
                            commad.Parameters.Add(new SqlParameter("@idTarea", tarea.idTareasEnvioCorreo));
                            commad.ExecuteNonQuery();
                            if (CrearSqlCommand.validarCorrecto(commad))
                            {
                                tran.Commit();
                            }
                            else
                            {
                                tran.Rollback();
                            }
                            return new OperationResult(commad);
                        }
                    }                    
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        public OperationResult validarTareaDiaria(TareasEnvioCorreo tarea)
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    connection.Open();
                    using (SqlTransaction tran = connection.BeginTransaction())
                    {
                        using (SqlCommand commad = CrearSqlCommand.getCommand(connection, "sp_validarTareaDiaria"))
                        {
                            commad.Transaction = tran;
                            commad.Parameters.Add(new SqlParameter("@idTarea", tarea.idTareasEnvioCorreo));
                            commad.ExecuteNonQuery();
                            if (CrearSqlCommand.validarCorrecto(commad))
                            {
                                tran.Commit();
                            }
                            else
                            {
                                tran.Rollback();
                            }
                            return new OperationResult(commad);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
}
