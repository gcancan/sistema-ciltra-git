﻿using Core.Models;
using Core.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.BusinessLogic
{
    public class SucursalesServices
    {
        public OperationResult getSucursalALLorById(int id)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getSucursalALLorById";
                        command.Parameters.Add(new SqlParameter("@id", id));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        connection.Open();
                        var i = command.ExecuteReader();
                        List<Sucursal> listSucursal = new List<Sucursal>();

                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                listSucursal.Add(new Sucursal
                                {
                                    clave_empresa = (int)sqlReader["Clave_empresa"],
                                    clave = (int)sqlReader["Clave"],
                                    nombre = (string)sqlReader["Nombre"],                                   
                                    activo = (bool)sqlReader["Activo"]
                                });
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)codigo.Value, result = listSucursal };
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }
    }
}
