﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using System.Data;
using System.Data.SqlClient;
using Core.Utils;
namespace CoreFletera.BusinessLogic
{
    public class ProductosCOMServices
    {
        public OperationResult saveProductoCOM(ref ProductosCOM producto)
        {
            try
            {
                using (SqlConnection conection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comand = conection.CreateCommand())
                    {
                        comand.CommandType = CommandType.StoredProcedure;
                        comand.CommandText = "sp_saveProductoCOM";
                        comand.Parameters.Add(new SqlParameter("@idProducto", producto.idProducto));
                        comand.Parameters.Add(new SqlParameter("@codigo", producto.codigo));
                        comand.Parameters.Add(new SqlParameter("@nombre", producto.nombre));
                        comand.Parameters.Add(new SqlParameter("@precio", producto.precio));
                        comand.Parameters.Add(new SqlParameter("@taller", Convert.ToInt32(producto.taller)));
                        comand.Parameters.Add(new SqlParameter("@lavadero", Convert.ToInt32(producto.lavadero)));
                        comand.Parameters.Add(new SqlParameter("@llantera", Convert.ToInt32(producto.llantera)));

                        SqlParameter spId = comand.Parameters.Add(new SqlParameter("@id", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        SqlParameter spValor = comand.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        SqlParameter spMensaje = comand.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                        conection.Open();
                        comand.ExecuteNonQuery();
                        if ((int)spValor.Value == 0)
                        {
                            producto.idProducto = (int)spId.Value;
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = producto };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }

        public OperationResult sincronizarProductoCOM(ProductosCOM producto)
        {
            try
            {
                using (SqlConnection conection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comand = conection.CreateCommand())
                    {
                        comand.CommandType = CommandType.StoredProcedure;
                        comand.CommandText = "sp_SincronizarProductoCOM";
                        comand.Parameters.Add(new SqlParameter("@idProducto", producto.idProducto));
                        comand.Parameters.Add(new SqlParameter("@codigo", producto.codigo));
                        comand.Parameters.Add(new SqlParameter("@nombre", producto.nombre));
                        comand.Parameters.Add(new SqlParameter("@precio", producto.precio));
                        comand.Parameters.Add(new SqlParameter("@taller", Convert.ToInt32(producto.taller)));
                        comand.Parameters.Add(new SqlParameter("@lavadero", Convert.ToInt32(producto.lavadero)));
                        comand.Parameters.Add(new SqlParameter("@llantera", Convert.ToInt32(producto.llantera)));
                        
                        SqlParameter spValor = comand.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        SqlParameter spMensaje = comand.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                        conection.Open();
                        comand.ExecuteNonQuery();
                        
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = producto };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }
        public OperationResult getInsumosCOM()
        {
            try
            {
                using (SqlConnection conection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comand = conection.CreateCommand())
                    {
                        comand.CommandType = CommandType.StoredProcedure;
                        comand.CommandText = "sp_getInsumosCOM";

                        SqlParameter spValor = comand.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        SqlParameter spMensaje = comand.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                        conection.Open();
                        comand.ExecuteNonQuery();
                        List<ProductosCOM> lista = new List<ProductosCOM>();

                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                ProductosCOM producto = new ProductosCOM
                                {
                                    idProducto = (int)sqlReader["idProducto"],
                                    codigo = (string)sqlReader["codigo"],
                                    nombre = (string)sqlReader["nombre"],
                                    precio = (decimal)sqlReader["precio"],
                                    lavadero = Convert.ToBoolean(sqlReader["lavadero"]),
                                    llantera = Convert.ToBoolean(sqlReader["llantera"]),
                                    taller = Convert.ToBoolean(sqlReader["taller"])
                                };
                                lista.Add(producto);
                            }
                        }

                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = lista };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }
        public OperationResult getInsumosCOMbyIdOrdenActividad(int idOrdenActividad)
        {
            try
            {
                using (SqlConnection conection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comand = CrearSqlCommand.getCommand(conection, "sp_getInsumosCOMbyIdOrdenActividad"))
                    {
                        comand.Parameters.Add(new SqlParameter("@idOrdenActividad", idOrdenActividad));
                        conection.Open();
                        comand.ExecuteNonQuery();
                        List<ProductosCOM> lista = new List<ProductosCOM>();

                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                ProductosCOM producto = new ProductosCOM
                                {
                                    idProducto = (int)sqlReader["idProducto"],
                                    codigo = (string)sqlReader["codigo"],
                                    nombre = (string)sqlReader["nombre"],
                                    precio = (decimal)sqlReader["precio"],
                                    lavadero = Convert.ToBoolean(sqlReader["lavadero"]),
                                    llantera = Convert.ToBoolean(sqlReader["llantera"]),
                                    taller = Convert.ToBoolean(sqlReader["taller"]),
                                    cantidad = (decimal)sqlReader["cantidad"]
                                };
                                lista.Add(producto);
                            }
                        }
                        return new OperationResult (comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }

        public OperationResult deleteProductoCOM(ProductosCOM producto)
        {
            try
            {
                using (SqlConnection conection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comand = conection.CreateCommand())
                    {
                        comand.CommandType = CommandType.StoredProcedure;
                        comand.CommandText = "sp_deleteProductoCOM";
                        comand.Parameters.Add(new SqlParameter("@idProducto", producto.idProducto));

                        SqlParameter spValor = comand.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        SqlParameter spMensaje = comand.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                        conection.Open();
                        comand.ExecuteNonQuery();

                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = null };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }
    }
}
