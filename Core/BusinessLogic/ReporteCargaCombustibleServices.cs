﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using Core.Utils;
using System.Data;
using System.Data.SqlClient;
namespace CoreFletera.BusinessLogic
{
    public class ReporteCargaCombustibleServices
    {
        public OperationResult getReporteCombustibleByFiltros(int idEmpresa, DateTime fechaInicial, DateTime fechaFinal, List<string> listaZonas, bool todosZonas,
            List<string> listaProveedores, bool todosProveedores, List<string> listaUnidades, bool todasUnidades, List<string> listaOperadores, bool todosOperadores)
        {
            int id = 0;
            try
            {
                string xmlZonas = GenerateXML.generarListaCadena(listaZonas);
                string xmlProveedores = GenerateXML.generarListaCadena(listaProveedores);
                string xmlUnidades = GenerateXML.generarListaCadena(listaUnidades);
                string xmlOperadores = GenerateXML.generarListaCadena(listaOperadores);
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "getReporteCargasDiesel"))
                    {
                        comad.CommandTimeout = 1800;
                        comad.Parameters.Add(new SqlParameter("@idEmpresa", idEmpresa));
                        comad.Parameters.Add(new SqlParameter("@fechaInicial", fechaInicial));
                        comad.Parameters.Add(new SqlParameter("@fechaFinal", fechaFinal));
                        comad.Parameters.Add(new SqlParameter("@xmlZonas", xmlZonas));
                        comad.Parameters.Add(new SqlParameter("@todoZonas", todosZonas));
                        comad.Parameters.Add(new SqlParameter("@xmlProveedor", xmlProveedores));
                        comad.Parameters.Add(new SqlParameter("@todoProveedor", todosProveedores));
                        comad.Parameters.Add(new SqlParameter("@xmlUnidades", xmlUnidades));
                        comad.Parameters.Add(new SqlParameter("@todoUnidades", todasUnidades));
                        comad.Parameters.Add(new SqlParameter("@xmlOperadores", xmlOperadores));
                        comad.Parameters.Add(new SqlParameter("@todoOperadores", todosOperadores));
                        con.Open();
                        List<ReporteCargasDiesel> lista = new List<ReporteCargasDiesel>();
                        using (SqlDataReader reader = comad.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                id = (int)reader["vale"];
                                if (id == 40922)
                                {

                                }
                                ReporteCargasDiesel reporte = new ReporteCargasDiesel
                                {
                                    vale = (int)reader["vale"],
                                    idEmpresa = (int)reader["idEmpresa"],
                                    empresa = (string)reader["empresa"],
                                    fecha = (DateTime)reader["fecha"],
                                    operación = (string)reader["operacion"],
                                    unidad = (string)reader["unidad"],
                                    litros = (decimal)reader["litros"],
                                    rem1 = (string)reader["rem1"],
                                    dolly = (string)reader["dolly"],
                                    rem2 = (string)reader["rem2"],
                                    capacidad = (decimal)reader["capcidad"],
                                    idOperador = (int)reader["idOperador"],
                                    operador = (string)reader["operador"],
                                    kmCarga = (decimal)reader["kmInicial"],
                                    kmInsercion = DBNull.Value.Equals(reader["kmInsercion"]) ? 0 : (decimal)reader["kmInsercion"],
                                    hrInicial = (decimal)reader["hrInicial"],
                                    fechaUltimaCarga = (DateTime)reader["fechaUltimaCarga"],
                                    fechaUltimaInsersion = DBNull.Value.Equals(reader["fechaInsercion"]) ? null : (DateTime?)reader["fechaInsercion"],
                                    kmFinal = (decimal)reader["kmFinal"],
                                    hrFinal = (decimal)reader["hrFinal"],
                                    tripECM = (decimal)reader["kmECM"],
                                    factura = (string)reader["factura"],
                                    facturaProveedor = (string)reader["facturaProveedor"],
                                    litrosECM = (decimal)reader["litrosECM"],
                                    litrosPTO = (decimal)reader["litrosPTO"],
                                    litrosEST = (decimal)reader["litrosEST"],
                                    despachador = (string)reader["despachador"],
                                    precio = (decimal)reader["precio"],
                                    precio2 = (decimal)reader["precio2"],
                                    importe = (decimal)reader["importe"],
                                    litrosExtra = (decimal)reader["litrosExtra"],
                                    ticket = (string)reader["ticket"],
                                    folioImpreso = (string)reader["folioImpreso"],
                                    idOrdenServ = (int)reader["idOrdenServ"],
                                    idProveedor = (int)reader["idProveedor"],
                                    proveedor = (string)reader["proveedor"],
                                    proveedorExtra = (string)reader["proveedorExtra"],
                                    viajes = (string)reader["viajes"],
                                    usuarioConcilio = (string)reader["usuarioConcilia"],
                                    fechaConciliacion = DBNull.Value.Equals(reader["fechaConciliacion"]) ? null : (DateTime?)reader["fechaConciliacion"],
                                    tipoCarga = (string)reader["tipoCarga"],
                                    tipoCargaExtra = (string)reader["tipoCargaExtra"],
                                    isCR = (bool)reader["isCR"],
                                    varIEPS = (decimal)reader["valorIEPS"]
                                };
                                lista.Add(reporte);
                            }
                        }
                        return new OperationResult(comad, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
}
