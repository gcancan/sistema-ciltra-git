﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using Core.Utils;
using System.Data;
using System.Data.SqlClient;
using Core.BusinessLogic;

namespace CoreFletera.BusinessLogic
{
    public class RevitalizacionesServices
    {
        internal OperationResult saveRevitalizacion(ref Revitalizaciones revitalizacion)
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    connection.Open();
                    using (SqlTransaction transaction = connection.BeginTransaction("saveRevitalizacion"))
                    {
                        using (SqlCommand command = connection.CreateCommand())
                        {
                            command.Transaction = transaction;
                            command.CommandType = CommandType.StoredProcedure;
                            command.CommandText = "sp_saveRevitalizacion";

                            command.Parameters.Add(new SqlParameter("@idRevitalizacion", revitalizacion.idRevitalizacion));
                            command.Parameters.Add(new SqlParameter("@idProveedor", revitalizacion.proveedor.idProveedor));
                            command.Parameters.Add(new SqlParameter("@fechaInicio", revitalizacion.fechaSalida));
                            command.Parameters.Add(new SqlParameter("@fechaFin", revitalizacion.fechaEntrada == null ? null : revitalizacion.fechaEntrada));
                            command.Parameters.Add(new SqlParameter("@estatus", revitalizacion.estatus));
                            command.Parameters.Add(new SqlParameter("@usuario", revitalizacion.usuario));

                            SqlParameter spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                            SqlParameter spId = command.Parameters.Add(new SqlParameter("@id", SqlDbType.Int) { Direction = ParameterDirection.Output });
                            SqlParameter spMensaje = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                            command.ExecuteNonQuery();
                            if ((int)spValor.Value == 0)
                            {
                                revitalizacion.idRevitalizacion = (int)spId.Value;
                                foreach (DetalleRevitalizacion item in revitalizacion.listaDetalles)
                                {
                                    command.Parameters.Clear();
                                    command.CommandText = "sp_saveDetalleRevitalizacion";

                                    command.Parameters.Add(new SqlParameter("@idRevitalizacion", revitalizacion.idRevitalizacion));
                                    command.Parameters.Add(new SqlParameter("@idDetalleRevitalizacion", item.idDetalleRevitalizacion));
                                    command.Parameters.Add(new SqlParameter("@idSysLlanta", item.llanta.idLlanta));
                                    command.Parameters.Add(new SqlParameter("@idDiseño", item.diseño.idDiseño));
                                    command.Parameters.Add(new SqlParameter("@profundidadAct", item.profundidadAct));
                                    command.Parameters.Add(new SqlParameter("@profundidadNueva", item.profundidadNueva));

                                    spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                                    spId = command.Parameters.Add(new SqlParameter("@id", SqlDbType.Int) { Direction = ParameterDirection.Output });
                                    spMensaje = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                                    command.ExecuteNonQuery();
                                    if ((int)spValor.Value == 0)
                                    {
                                        item.idDetalleRevitalizacion = (int)spId.Value;
                                    }
                                    else
                                    {
                                        transaction.Rollback("saveRevitalizacion");
                                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = null };
                                    }                                    
                                }
                                transaction.Commit();
                                return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = revitalizacion };
                            }
                            else
                            {
                                transaction.Rollback("saveRevitalizacion");
                                return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = null };
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }

        internal OperationResult getRevitalizacionById(int idRevitalizacion)
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getRevitalizacionById";

                        command.Parameters.Add(new SqlParameter("@idRevitalizacion", idRevitalizacion));                        

                        SqlParameter spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });                        
                        SqlParameter spMensaje = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                        connection.Open();
                        Revitalizaciones revitalizacion = new Revitalizaciones();
                        using (SqlDataReader sqlReader = command.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                revitalizacion = new mapComunes().mapRevitalizacion(sqlReader);
                                if (revitalizacion == null)
                                {
                                    return new OperationResult { valor = 1, mensaje = "Error al mapear los resultados de la busqueda" };
                                }

                                revitalizacion.listaDetalles = new List<DetalleRevitalizacion>();
                                OperationResult resp = getDetalleRevitalizacionById(revitalizacion.idRevitalizacion);
                                if (resp.typeResult == ResultTypes.success)
                                {
                                    revitalizacion.listaDetalles = (List<DetalleRevitalizacion>)resp.result;
                                }
                                else
                                {
                                    return resp;
                                }

                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result =  revitalizacion };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }

        internal OperationResult getDetalleRevitalizacionById(int idRevitalizacion)
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getDetalleRevitalizacionById";

                        command.Parameters.Add(new SqlParameter("@idRevitalizacion", idRevitalizacion));

                        SqlParameter spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        SqlParameter spMensaje = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                        connection.Open();
                        List<DetalleRevitalizacion> lista = new List<DetalleRevitalizacion>();
                        using (SqlDataReader sqlReader = command.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                DetalleRevitalizacion detalle = new mapComunes().mapDetalleRevitalizacion(sqlReader);
                                if (detalle == null)
                                {
                                    return new OperationResult { valor = 1, mensaje = "Error al mapear los resultados de la busqueda" };
                                }
                                lista.Add(detalle);
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = lista };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }
    }
}
