﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using Core.BusinessLogic;
using System.Data;
using System.Data.SqlClient;
using Core.Utils;
namespace CoreFletera.BusinessLogic
{
    public class ClasificacionRutasServices
    {
        public OperationResult saveClasificacionRutas(ref ClasificacionRutas clasificacion)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    con.Open();
                    using (SqlTransaction tran = con.BeginTransaction())
                    {
                        
                        using (SqlCommand command = CrearSqlCommand.getCommand(con, "sp_saveClasificacionRuta", true))
                        {
                            command.Transaction = tran;
                            command.Parameters.Add(new SqlParameter("@idClasificacion", clasificacion.idClasificacion));
                            command.Parameters.Add(new SqlParameter("@idCliente", clasificacion.cliente.clave));
                            command.Parameters.Add(new SqlParameter("@clave", clasificacion.clasificacion));
                            command.Parameters.Add(new SqlParameter("@kmMenor", clasificacion.kmMenor));
                            command.Parameters.Add(new SqlParameter("@kmMayor", clasificacion.kmMayor));
                            command.Parameters.Add(new SqlParameter("@estatus", clasificacion.estatus));

                            command.ExecuteNonQuery();
                            if ((int)command.Parameters["@valor"].Value == 0)
                            {
                                clasificacion.idClasificacion = (int)command.Parameters["@id"].Value;
                                tran.Commit();
                            }
                            else
                            {
                                tran.Rollback();
                            }

                            return new OperationResult(command);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getClasificacionRutas(int idCliente = 0, int idClasificacion = 0)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comando = CrearSqlCommand.getCommand(con, "sp_getClasificacionRutas"))
                    {
                        comando.Parameters.Add(new SqlParameter("@idCliente", idCliente == 0 ? "%" : idCliente.ToString()));
                        comando.Parameters.Add(new SqlParameter("@idClasificacion", idClasificacion == 0 ? "%" : idClasificacion.ToString()));

                        con.Open();
                        List<ClasificacionRutas> lista = new List<ClasificacionRutas>();
                        using (SqlDataReader sqlReader = comando.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                ClasificacionRutas clasificacion = new mapComunes().mapClasificacion(sqlReader);
                                if (clasificacion == null)
                                {
                                    return new OperationResult { valor = 1, mensaje = "ERROR AL LEER EL RESULTADO DE LA BUSQUEDA" };
                                }
                                else
                                {
                                    lista.Add(clasificacion);
                                }
                            }
                        }
                        return new OperationResult(comando, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getClasificacionRutasByViaje(int idCliente, decimal km)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comando = CrearSqlCommand.getCommand(con, "sp_getClasificacionRutasByViaje"))
                    {
                        comando.Parameters.Add(new SqlParameter("@idCliente", idCliente));
                        comando.Parameters.Add(new SqlParameter("@km", km));

                        con.Open();
                        ClasificacionRutas clasif = new ClasificacionRutas();
                        using (SqlDataReader sqlReader = comando.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                clasif = new mapComunes().mapClasificacion(sqlReader);
                                if (clasif == null)
                                {
                                    return new OperationResult { valor = 1, mensaje = "ERROR AL LEER EL RESULTADO DE LA BUSQUEDA" };
                                }
                            }
                        }
                        return new OperationResult(comando, clasif);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
}
