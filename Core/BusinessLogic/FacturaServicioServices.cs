﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using Core.Utils;
using System.Data;
using System.Data.SqlClient;
namespace CoreFletera.BusinessLogic
{
    public class FacturaServicioServices
    {
        public OperationResult saveFacturaServicio(ref FacturaServicios facturaServicios)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    con.Open();
                    using (SqlTransaction tran = con.BeginTransaction())
                    {
                        using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_saveFacturaServicio", true))
                        {
                            comad.Transaction = tran;
                            comad.Parameters.Add(new SqlParameter("@folioFactura", facturaServicios.folioFactura));
                            comad.Parameters.Add(new SqlParameter("@idEmpresa", facturaServicios.empresa.clave));
                            comad.Parameters.Add(new SqlParameter("@idZonaOperativa", facturaServicios.zonaOperativa.idZonaOperativa));
                            comad.Parameters.Add(new SqlParameter("@idCliente", facturaServicios.cliente == null ? null : (int?)facturaServicios.cliente.clave));
                            comad.Parameters.Add(new SqlParameter("@tipoFaturaServicio", (int)facturaServicios.tipoFacturaServicio));
                            comad.Parameters.Add(new SqlParameter("@fechaCaptura", facturaServicios.fechaFactura));
                            comad.Parameters.Add(new SqlParameter("@usuario", facturaServicios.usuario));
                            comad.Parameters.Add(new SqlParameter("@subTotal", facturaServicios.subTotal));
                            comad.Parameters.Add(new SqlParameter("@importeIVA", facturaServicios.importeIVA));
                            comad.Parameters.Add(new SqlParameter("@importeRetencion", facturaServicios.importeRetencion));
                            comad.Parameters.Add(new SqlParameter("@importe", facturaServicios.importe));
                            comad.Parameters.Add(new SqlParameter("@observaciones", facturaServicios.observaciones));

                            comad.ExecuteNonQuery();
                            int id = 0;
                            if (CrearSqlCommand.validarCorrecto(comad, out id))
                            {
                                facturaServicios.idFacturaServicio = id;
                                foreach (var detalle in facturaServicios.listDetalles)
                                {
                                    comad.CommandText = "sp_saveDetalleFacturaServicio";
                                    comad.Parameters.Clear();

                                    foreach (var param in CrearSqlCommand.getPatametros(true))
                                    {
                                        comad.Parameters.Add(param);
                                    }

                                    comad.Parameters.Add(new SqlParameter("@idFacturaServicio", facturaServicios.idFacturaServicio));
                                    comad.Parameters.Add(new SqlParameter("@idDetalle", detalle.idDetalle));
                                    comad.Parameters.Add(new SqlParameter("@km", detalle.km));
                                    comad.Parameters.Add(new SqlParameter("@cantidad", detalle.cantidad));
                                    comad.Parameters.Add(new SqlParameter("@precio", detalle.precio));
                                    comad.Parameters.Add(new SqlParameter("@precioFijo", detalle.precioFijo));
                                    comad.Parameters.Add(new SqlParameter("@folioFactura", facturaServicios.folioFactura));
                                    comad.Parameters.Add(new SqlParameter("@tipoFaturaServicio", (int)facturaServicios.tipoFacturaServicio));
                                    comad.Parameters.Add(new SqlParameter("@fechaCaptura", facturaServicios.fechaFactura));
                                    comad.Parameters.Add(new SqlParameter("@subTotal", detalle.subTotal));
                                    comad.Parameters.Add(new SqlParameter("@importeIVA", detalle.importeIVA));
                                    comad.Parameters.Add(new SqlParameter("@importeRetencion", detalle.importeRetencion));
                                    comad.Parameters.Add(new SqlParameter("@importe", detalle.importe));

                                    comad.ExecuteNonQuery();
                                    if (CrearSqlCommand.validarCorrecto(comad, out id))
                                    {
                                        detalle.idDetalle = id;
                                    }
                                    else
                                    {
                                        tran.Rollback();
                                        return new OperationResult(comad);
                                    }
                                }
                            }
                            else
                            {
                                tran.Rollback();
                                return new OperationResult(comad);
                            }
                            tran.Commit();
                            return new OperationResult(comad, facturaServicios);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
}
