﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using Core.Utils;
using System.Data;
using System.Data.SqlClient;
using Core.BusinessLogic;

namespace CoreFletera.BusinessLogic
{
    public class SemaforoMttoServices
    {
        public OperationResult getSemaforoMtto(List<string> listaUnidades)
        {
            try
            {
                string xml = GenerateXML.generarListaCadena(listaUnidades);
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getSemaforoMtto"))
                    {
                        comad.Parameters.Add(new SqlParameter("@xml", xml));
                        con.Open();
                        List<SemaforoMtto> listaSemaforo = new List<SemaforoMtto>();
                        using (SqlDataReader sqlReader = comad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                SemaforoMtto semaforo = new SemaforoMtto
                                {
                                    claveUnidad = (string)sqlReader["idUnidad"],
                                    fechaUltimoMtto = (DateTime)sqlReader["fechaUltimoMtto"],
                                    ultimoKmMtto = (decimal)sqlReader["kmUltimoMtto"],
                                    fechaKmActual = DBNull.Value.Equals(sqlReader["fechaKmActual"]) ? null : (DateTime?)sqlReader["fechaKmActual"],
                                    mkmActual = DBNull.Value.Equals(sqlReader["mkmActual"]) ? null : (decimal?)sqlReader["mkmActual"],
                                    meta = DBNull.Value.Equals(sqlReader["meta"]) ? 0 : (int)sqlReader["meta"],
                                };

                                Servicio servicio = new mapComunes().mapServicio(sqlReader);
                                if (servicio == null) return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");

                                if (semaforo == null) return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");

                                semaforo.servicio = servicio;
                                listaSemaforo.Add(semaforo);
                            }
                        }
                        return new OperationResult(comad, listaSemaforo);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
}
