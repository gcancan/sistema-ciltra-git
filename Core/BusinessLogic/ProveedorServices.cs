﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using Core.Utils;
using System.Data;
using System.Data.SqlClient;
using Core.BusinessLogic;

namespace CoreFletera.BusinessLogic
{
    public class ProveedorServices
    {
        public OperationResult getAllProveedores()
        {
            try
            {
                using (SqlConnection connetion = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand command = connetion.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getAllProveedores";

                        SqlParameter spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        SqlParameter spMensaje = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        List<Proveedor> listaProveedores = new List<Proveedor>();
                        connetion.Open();
                        using (SqlDataReader sqlReader = command.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                Proveedor proveedor = new mapComunes().mapProveedor(sqlReader);
                                if (proveedor == null)
                                {
                                    return new OperationResult { valor = 1, mensaje = "ERROR AL MAPEAR LOS RESULTADOS DE LA BUSQUEDA(PROVEEDOR)" };
                                }
                                listaProveedores.Add(proveedor);
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = listaProveedores };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }

        public OperationResult getAllProveedoresBiIdMarca(int idMarca)
        {
            try
            {
                using (SqlConnection connetion = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand command = connetion.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getAllProveedoresBiIdMarca";

                        command.Parameters.Add(new SqlParameter("@idMarca", idMarca));

                        SqlParameter spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        SqlParameter spMensaje = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        List<Proveedor> listaProveedores = new List<Proveedor>();
                        connetion.Open();
                        using (SqlDataReader sqlReader = command.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                Proveedor proveedor = new mapComunes().mapProveedor(sqlReader);
                                if (proveedor == null)
                                {
                                    return new OperationResult { valor = 1, mensaje = "ERROR AL MAPEAR LOS RESULTADOS DE LA BUSQUEDA" };
                                }
                                listaProveedores.Add(proveedor);
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = listaProveedores };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }
        public OperationResult saveProveedor(ref ProveedorCombustible proveedor)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    con.Open();
                    using (SqlTransaction tran = con.BeginTransaction())
                    {
                        using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_saveProveedor", true))
                        {
                            comad.Transaction = tran;
                            comad.Parameters.Add(new SqlParameter("@idProveedor", proveedor.idProveedor));
                            comad.Parameters.Add(new SqlParameter("@nombre", proveedor.nombre));
                            comad.Parameters.Add(new SqlParameter("@precio", proveedor.precio));
                            comad.Parameters.Add(new SqlParameter("@estatus", proveedor.estatus));
                            comad.Parameters.Add(new SqlParameter("@RFC", proveedor.RFC));
                            comad.Parameters.Add(new SqlParameter("@direccion", proveedor.direccion));
                            comad.Parameters.Add(new SqlParameter("@telefono", proveedor.telefono));
                            comad.Parameters.Add(new SqlParameter("@idColonia", proveedor.colonia == null ? null : (int?)proveedor.colonia.idColonia));
                            comad.Parameters.Add(new SqlParameter("@vehiculo", proveedor.vehiculos));
                            comad.Parameters.Add(new SqlParameter("@llantasNuevas", proveedor.llantasNuevas));
                            comad.Parameters.Add(new SqlParameter("@renovadorLlantas", proveedor.renovadorLlantas));
                            comad.Parameters.Add(new SqlParameter("@reparadorLlantas", proveedor.reparadorLlantas));
                            comad.Parameters.Add(new SqlParameter("@diesel", proveedor.diesel));

                            comad.ExecuteNonQuery();
                            int id = 0;
                            if (CrearSqlCommand.validarCorrecto(comad, out id))
                            {
                                proveedor.idProveedor = id;
                            }
                            else
                            {
                                tran.Rollback();
                                return new OperationResult(comad);
                            }
                            tran.Commit();
                            return new OperationResult(comad, proveedor);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getAllProveedoresV2()
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getAllProveedoresV2"))
                    {
                        List<ProveedorCombustible> listaProveedores = new List<ProveedorCombustible>();
                        con.Open();
                        using (SqlDataReader sqlReader = comad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                ProveedorCombustible proveedor = new mapComunes().mapProveedorV2(sqlReader);
                                if (proveedor == null)
                                {
                                    return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA.");
                                }
                                listaProveedores.Add(proveedor);
                            }
                        }

                        return new OperationResult(comad, listaProveedores);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getAllProveedoresUnidades()
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getAllProveedoresUnidades"))
                    {
                        List<ProveedorCombustible> listaProveedores = new List<ProveedorCombustible>();
                        con.Open();
                        using (SqlDataReader sqlReader = comad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                ProveedorCombustible proveedor = new mapComunes().mapProveedorV2(sqlReader);
                                if (proveedor == null)
                                {
                                    return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA.");
                                }
                                listaProveedores.Add(proveedor);
                            }
                        }

                        return new OperationResult(comad, listaProveedores);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
}
