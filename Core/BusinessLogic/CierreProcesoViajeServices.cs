﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using Core.Utils;
using System.Data;
using System.Data.SqlClient;
using Core.Interfaces;
using Core.BusinessLogic;

namespace CoreFletera.BusinessLogic
{
    internal class CierreProcesoViajeServices
    {
        public OperationResult saveCierreProcesoViaje(ref CierreProcesoViajes cierreProceso, int idEmpresa, List<ReporteCartaPorteViajes> listViajes, List<ReporteCartaPorte> listaCartaPortes)
        {
            try
            {
                List<RegistroINTELISIS> listaRegistro = new List<RegistroINTELISIS>();
                List<RegistroCartaPortesINTELISIS> listaRegistroCp = new List<RegistroCartaPortesINTELISIS>();
                if (empresaSincronizaViajes(idEmpresa).typeResult == ResultTypes.success)
                {
                    var resp = new CartaPorteSvc().sincronizarViajes(listViajes, listaCartaPortes);
                    if (resp.typeResult != ResultTypes.success)
                    {
                        return resp;
                    }
                    else
                    {
                        listaRegistro = resp.result as List<RegistroINTELISIS>;
                        listaRegistroCp = resp.result2 as List<RegistroCartaPortesINTELISIS>;
                    }
                }

                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    con.Open();
                    using (SqlTransaction tran = con.BeginTransaction())
                    {
                        using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_saveCierreProcesoViajes", true))
                        {
                            comad.Transaction = tran;
                            comad.Parameters.Add(new SqlParameter("@fecha", cierreProceso.fecha));
                            comad.Parameters.Add(new SqlParameter("@idEmpresa", cierreProceso.idEmpresa));
                            comad.Parameters.Add(new SqlParameter("@idZonaOperativa", cierreProceso.idZonaOperativa));
                            comad.Parameters.Add(new SqlParameter("@idCliente", cierreProceso.idCliente));
                            comad.Parameters.Add(new SqlParameter("@usuario", cierreProceso.usuario));
                            comad.Parameters.Add(new SqlParameter("@noViajes", cierreProceso.noViaje));
                            comad.Parameters.Add(new SqlParameter("@km", cierreProceso.km));
                            comad.Parameters.Add(new SqlParameter("@toneladas", cierreProceso.toneladas));
                            comad.Parameters.Add(new SqlParameter("@dias", cierreProceso.dias));
                            comad.ExecuteNonQuery();
                            if (CrearSqlCommand.validarCorrecto(comad, out int id))
                            {
                                cierreProceso.idCierreProcesoViajes = id;
                                comad.CommandText = "sp_saveDetalleCierreProcesoViajes";

                                if (listaRegistro.Count > 0)
                                {
                                    foreach (var item in cierreProceso.listaDetalles)
                                    {
                                        item.registroCP = listaRegistroCp.Find(s => s.consecutivo == item.consecutivo);
                                    }
                                }

                                foreach (var det in cierreProceso.listaDetalles)
                                {
                                    det.idCierreProcesoViajes = cierreProceso.idCierreProcesoViajes;
                                    comad.Parameters.Clear();
                                    foreach (var param in CrearSqlCommand.getPatametros(true))
                                    {
                                        comad.Parameters.Add(param);
                                    }
                                    comad.Parameters.Add(new SqlParameter("@idCierreProcesoViajes", det.idCierreProcesoViajes));
                                    comad.Parameters.Add(new SqlParameter("@numGuiaId", det.numGuiaId));
                                    comad.Parameters.Add(new SqlParameter("@consecutivo", det.consecutivo));
                                    comad.Parameters.Add(new SqlParameter("@idOrigen", det.idOrigen));
                                    comad.Parameters.Add(new SqlParameter("@idDestino", det.idDestino));
                                    comad.Parameters.Add(new SqlParameter("@idProducto", det.idProducto));
                                    comad.Parameters.Add(new SqlParameter("@descargado", det.descargado));
                                    comad.Parameters.Add(new SqlParameter("@km", det.km));
                                    comad.Parameters.Add(new SqlParameter("@cveINTELISIS", det.registroCP == null ? null : (int?)det.registroCP.cveINTELISIS));
                                    comad.Parameters.Add(new SqlParameter("@renglon", det.registroCP == null ? null : (decimal?)det.registroCP.renglon));
                                    comad.Parameters.Add(new SqlParameter("@renglonId", det.registroCP == null ? null : (decimal?)det.registroCP.renglonId));

                                    comad.ExecuteNonQuery();
                                    if (CrearSqlCommand.validarCorrecto(comad, out int idDet))
                                    {
                                        det.idDetalleCierreProcesoViajes = idDet;
                                    }
                                    else
                                    {
                                        tran.Rollback();
                                        return new OperationResult(comad);
                                    }
                                }
                            }
                            else
                            {
                                tran.Rollback();
                                return new OperationResult(comad);
                            }
                            tran.Commit();
                            return new OperationResult(comad, cierreProceso);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        public OperationResult empresaSincronizaViajes(int idEmpresa)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_consultarEmpresaSincronizaViajes"))
                    {
                        comad.Parameters.Add(new SqlParameter("@idEmpresa", idEmpresa));
                        con.Open();
                        comad.ExecuteNonQuery();
                        return new OperationResult(comad);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
}
