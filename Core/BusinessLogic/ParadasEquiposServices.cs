﻿namespace CoreFletera.BusinessLogic
{
    using Core.Models;
    using Core.Utils;
    using CoreFletera.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;

    internal class ParadasEquiposServices
    {
        public OperationResult saveParadasEquipo(ref List<ParadaEquipo> listaParadas, UsuarioCSI usuarioCSI)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    connection.Open();
                    using (SqlTransaction transaction = connection.BeginTransaction())
                    {
                        using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_saveParadasEquipo", true))
                        {
                            command.Transaction = transaction;
                            foreach (ParadaEquipo equipo in listaParadas)
                            {
                                command.Parameters.Clear();
                                foreach (SqlParameter parameter in CrearSqlCommand.getPatametros(true))
                                {
                                    command.Parameters.Add(parameter);
                                }
                                command.Parameters.Add(new SqlParameter("@idParadaEquipo", equipo.idParadaEquipo));
                                command.Parameters.Add(new SqlParameter("@idUsuario", usuarioCSI.idUsuario));
                                command.Parameters.Add(new SqlParameter("@estatus", equipo.estatus));
                                command.Parameters.Add(new SqlParameter("@idUnidad", equipo.alias));
                                command.Parameters.Add(new SqlParameter("@fechaInicial", equipo.fechaInicial));
                                command.Parameters.Add(new SqlParameter("@fechaFinal", equipo.fechaFinal));
                                command.Parameters.Add(new SqlParameter("@duracion", equipo.duracion));
                                command.Parameters.Add(new SqlParameter("@geoCerca", equipo.geocerca));
                                command.Parameters.Add(new SqlParameter("@coordenadas", equipo.coordenadas));
                                command.Parameters.Add(new SqlParameter("@folioViaje", equipo.folioViaje));
                                int id = 0;
                                command.ExecuteNonQuery();
                                if (CrearSqlCommand.validarCorrecto(command, out id))
                                {
                                    equipo.idParadaEquipo = id;
                                }
                                else
                                {
                                    transaction.Rollback();
                                    return new OperationResult(command, null);
                                }
                            }
                            transaction.Commit();
                            result = new OperationResult(command, listaParadas);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult(exception);
            }
            return result;
        }
    }
}
