﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using Core.Utils;
using System.Data;
using System.Data.SqlClient;
using CoreFletera.BusinessLogic;
namespace Core.BusinessLogic
{
    public class EmpresaServices
    {
        public OperationResult getEmpresasALLorById(int id)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getEmpresasALLorById";
                        command.Parameters.Add(new SqlParameter("@id", id));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        connection.Open();
                        var i = command.ExecuteReader();
                        List<Empresa> listEmpresa = new List<Empresa>();

                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                Empresa empresa = new mapComunes().mapEmpresa(sqlReader);
                                listEmpresa.Add(empresa);
                                //listEmpresa.Add(new Empresa
                                //{
                                //    clave = (int)sqlReader["Clave"],
                                //    nombre = (string)sqlReader["Nombre"],
                                //    direccion = (string)sqlReader["Direccion"],                                    
                                //    telefono = (string)sqlReader["Telefono"],
                                //    fax = (string)sqlReader["Fax"],
                                //    estado = (string)sqlReader["estado"],
                                //    pais = (string)sqlReader["pais"],
                                //    municipio = (string)sqlReader["municipio"],
                                //    colonia = (string)sqlReader["colonia"],
                                //    cp = (string)sqlReader["cp"],
                                //    rfc = (string)sqlReader["rfc"]
                                //});
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)codigo.Value, result = listEmpresa };
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }
        public OperationResult sincronizarEmpresas(List<Empresa> listaEmpresas)
        {
            try
            {
                string xml = GenerateXML.getListaEmpresaSinc(listaEmpresas);
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    con.Open();
                    using (SqlTransaction tran = con.BeginTransaction())
                    {
                        using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_SincronizarEmpresas", false, true))
                        {
                            comad.Transaction = tran;
                            comad.Parameters.Add(new SqlParameter("@xml", xml));
                            comad.ExecuteNonQuery();
                            if (!CrearSqlCommand.validarCorrecto(comad))                            
                            {
                                tran.Rollback();
                                return new OperationResult(comad);
                            }
                            tran.Commit();
                            return new OperationResult(comad);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
}
