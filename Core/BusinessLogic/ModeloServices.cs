﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using Core.Utils;
using System.Data;
using System.Data.SqlClient;
using Core.BusinessLogic;

namespace CoreFletera.BusinessLogic
{
    public class ModeloServices
    {
        public OperationResult saveModelo(ref Modelo modelo)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    con.Open();
                    using (SqlTransaction tran = con.BeginTransaction())
                    {
                        using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_saveModelo", true))
                        {
                            comad.Transaction = tran;
                            comad.Parameters.Add(new SqlParameter("@idModelo", modelo.idModelo));
                            comad.Parameters.Add(new SqlParameter("@idMarca", modelo.marca.clave));
                            comad.Parameters.Add(new SqlParameter("@modelo", modelo.modelo));
                            comad.Parameters.Add(new SqlParameter("@activo", modelo.activo));
                            comad.ExecuteNonQuery();
                            int id = 0;
                            if (CrearSqlCommand.validarCorrecto(comad, out id))
                            {
                                modelo.idModelo = id;
                                tran.Commit();
                            }
                            else
                            {
                                tran.Rollback();
                            }
                            return new OperationResult(comad, modelo);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getAllModelos()
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getAllModelos"))
                    {
                        List<Modelo> lista = new List<Modelo>();
                        con.Open();
                        using (SqlDataReader reader = comad.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Modelo modelo = new mapComunes().mapModelo(reader);
                                lista.Add(modelo);
                            }
                        }
                        return new OperationResult(comad, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getAllModelosByMarca(int idMarca)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getAllModelosByMarca"))
                    {
                        comad.Parameters.Add(new SqlParameter("@idMarca", idMarca));
                        List<Modelo> lista = new List<Modelo>();
                        con.Open();
                        using (SqlDataReader reader = comad.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Modelo modelo = new mapComunes().mapModelo(reader);
                                lista.Add(modelo);
                            }
                        }
                        return new OperationResult(comad, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

    }
}
