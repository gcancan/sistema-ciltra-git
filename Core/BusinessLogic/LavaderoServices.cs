﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using Core.Utils;
using System.Data;
using System.Data.SqlClient;
namespace CoreFletera.BusinessLogic
{
    public class LavaderoServices
    {
        public OperationResult saveLavadero(ref Lavadero lavadero)
        {
            try
            {
                string xml = GenerateXML.generarListaInsumos(lavadero.listaInsumos, lavadero.ordenServicio);
                if (string.IsNullOrEmpty(xml))
                {
                    return new OperationResult { valor = 1, mensaje = "Error al enviar el XML" };
                }
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_saveLavadero";

                        if (lavadero.estatus == "TER")
                        {
                            using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                            {
                                con.Open();
                                using (SqlTransaction tran = con.BeginTransaction())
                                {
                                    using (SqlCommand comd = con.CreateCommand())
                                    {
                                        comd.Transaction = tran;
                                        foreach (var item in lavadero.listaInspecciones)
                                        {
                                            comd.Parameters.Clear();
                                            comd.CommandType = CommandType.StoredProcedure;
                                            comd.CommandText = "sp_getInspeccionesRealizadas";

                                            comd.Parameters.Add(new SqlParameter("@idInspeccion", item.idInspeccion));
                                            comd.Parameters.Add(new SqlParameter("@idOrden", lavadero.ordenServicio));

                                            var spValor_ = comd.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                                            var spId_ = comd.Parameters.Add(new SqlParameter("@id", SqlDbType.Int) { Direction = ParameterDirection.Output });
                                            var spMensaje_ = comd.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                                            comd.ExecuteNonQuery();

                                            if ((int)spValor_.Value == 0)
                                            {
                                                foreach (var pro in item.listaInsumos)
                                                {
                                                    int nuevoId = (int)spId_.Value;
                                                    comd.Parameters.Clear();
                                                    comd.CommandText = "sp_saveDetInspRealizadas";

                                                    comd.Parameters.Add(new SqlParameter("@idInspecRealizada", nuevoId));
                                                    comd.Parameters.Add(new SqlParameter("@idProducto", pro.idInsumo));
                                                    comd.Parameters.Add(new SqlParameter("@cantidad", pro.cantMaxima));
                                                    comd.Parameters.Add(new SqlParameter("@precio", pro.precio));

                                                    spValor_ = comd.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                                                    spId_ = comd.Parameters.Add(new SqlParameter("@id", SqlDbType.Int) { Direction = ParameterDirection.Output });
                                                    spMensaje_ = comd.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                                                    comd.ExecuteNonQuery();
                                                    if ((int)spValor_.Value != 0)
                                                    {
                                                        tran.Rollback();
                                                        return new OperationResult { valor = (int)spValor_.Value, mensaje = (string)spMensaje_.Value };
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                tran.Rollback();
                                                return new OperationResult { valor = (int)spValor_.Value, mensaje = (string)spMensaje_.Value };
                                            }
                                        }
                                        tran.Commit();
                                    }
                                }
                            }
                        }

                        command.Parameters.Add(new SqlParameter("@idOrdenServ", lavadero.ordenServicio));
                        command.Parameters.Add(new SqlParameter("@idActividad", lavadero.actividad.idActividad));
                        command.Parameters.Add(new SqlParameter("@fechaEntrada", lavadero.fechaEntrada));
                        command.Parameters.Add(new SqlParameter("@estatus", lavadero.estatus));
                        command.Parameters.Add(new SqlParameter("@idEmpreadoEntrega", lavadero.personalEntrega.clave));
                        command.Parameters.Add(new SqlParameter("@idLavadero", lavadero.lavador.clave));
                        command.Parameters.Add(new SqlParameter("@idLavadero2", lavadero.lavadorAux == null ? 0 : lavadero.lavadorAux.clave));
                        command.Parameters.Add(new SqlParameter("@idLavadero3", lavadero.lavadorAux2 == null ? 0 : lavadero.lavadorAux2.clave));
                        command.Parameters.Add(new SqlParameter("@usuarioRecepcion", lavadero.usuario));
                        command.Parameters.Add(new SqlParameter("@duracion", lavadero.actividad.duracion));
                        command.Parameters.Add(new SqlParameter("@costo", lavadero.costo));
                        command.Parameters.Add(new SqlParameter("@costoInsumos", lavadero.costoInsumos));
                        command.Parameters.Add(new SqlParameter("@xml", xml));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var spMensaje = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        connection.Open();
                        command.ExecuteNonQuery();

                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = (ex.Message + " Orden: " + lavadero.ordenServicio.ToString()) };
            }
        }
    }
}
