﻿namespace CoreFletera.BusinessLogic
{
    using Core.Models;
    using Core.Utils;
    using CoreFletera.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;

    public class ClasificacionProductosServices
    {
        public OperationResult getAllClasificacionProductos()
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getAllClasificacionProductos", false))
                    {
                        connection.Open();
                        List<ClasificacionProductos> list = new List<ClasificacionProductos>();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                ClasificacionProductos item = new ClasificacionProductos
                                {
                                    idClasificacionProducto = (int)reader["idClasificacionProducto"],
                                    descripcion = (string)reader["clasificacion"],
                                    diesel = (bool)reader["diesel"]
                                };
                                list.Add(item);
                            }
                        }
                        result = new OperationResult(command, list);
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult(exception);
            }
            return result;
        }

        public OperationResult saveClasificacionProductos(ref ClasificacionProductos clasificacion)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    connection.Open();
                    using (SqlTransaction transaction = connection.BeginTransaction())
                    {
                        using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_saveClasificacionProductos", true))
                        {
                            command.Transaction = transaction;
                            command.Parameters.Add(new SqlParameter("@idClasificacion", clasificacion.idClasificacionProducto));
                            command.Parameters.Add(new SqlParameter("@clasificacion", clasificacion.descripcion));
                            command.Parameters.Add(new SqlParameter("@diesel", clasificacion.diesel));
                            command.ExecuteNonQuery();
                            int id = 0;
                            if (CrearSqlCommand.validarCorrecto(command, out id))
                            {
                                clasificacion.idClasificacionProducto = id;
                                transaction.Commit();
                            }
                            else
                            {
                                transaction.Rollback();
                            }
                            result = new OperationResult(command, clasificacion);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult(exception);
            }
            return result;
        }
    }
}
