﻿using Core.Models;
using Core.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.BusinessLogic
{
    class PrivilegiosServices
    {
        public OperationResult getPrivilegiosALLorById(int id)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getPrivilegiosALLorById";
                        command.Parameters.Add(new SqlParameter("@id", id));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        connection.Open();
                        var i = command.ExecuteReader();
                        List<Privilegio> listPrivilegios = new List<Privilegio>();

                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                listPrivilegios.Add(new Privilegio
                                {
                                    idPrivilegio = (int)sqlReader["idPrivilegio"],
                                    clave = (string)sqlReader["Clave"],
                                    descripcion = (string)sqlReader["Descripcion"],
                                    nombre = (string)sqlReader["nombre"]
                                });
                            }
                        }

                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)codigo.Value, result = listPrivilegios };
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }

        internal OperationResult getAllPrivilegiosEspeiales()
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getAllPrivilegiosEspeiales";

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        connection.Open();
                        var i = command.ExecuteReader();
                        List<Privilegio> listPrivilegios = new List<Privilegio>();

                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                listPrivilegios.Add(new Privilegio
                                {
                                    idPrivilegio = (int)sqlReader["idPrivilegio"],
                                    clave = (string)sqlReader["Clave"],
                                    descripcion = (string)sqlReader["Descripcion"],
                                    nombre = "(PRIVILEGIO ESPECIAL)"
                                });
                            }
                        }

                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)codigo.Value, result = listPrivilegios };
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }

        public OperationResult consultarPrivilegio(string privilegio, int idUsuario)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_consultarPrivilegio";

                        command.Parameters.Add(new SqlParameter("@privilegio", privilegio));
                        command.Parameters.Add(new SqlParameter("@idUsuario", idUsuario));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        //var respuesta = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.Bit) { Direction = ParameterDirection.Output });
                        connection.Open();
                        var i = command.ExecuteNonQuery();

                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)codigo.Value};
                    }
                }
            }
            catch (Exception e)
            {
                return new OperationResult { result = null, valor = 1, mensaje = e.Message };
            }
        }

        public OperationResult getPrivilegiosByIdUsuario(int id)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getPrivilegiosByIdUsuario";
                        command.Parameters.Add(new SqlParameter("@id", id));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        connection.Open();
                        var i = command.ExecuteReader();
                        List<Privilegio> listPrivilegios = new List<Privilegio>();

                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                listPrivilegios.Add(new Privilegio
                                {
                                    idPrivilegio = (int)sqlReader["idPrivilegio"],
                                    clave = (string)sqlReader["Clave"],
                                    descripcion = (string)sqlReader["Descripcion"],
                                    nombre = (string)sqlReader["titulo"]
                                });
                            }
                        }

                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)codigo.Value, result = listPrivilegios };
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }

        public OperationResult quitarPrivilegio(int idUsuario, int idPrivilegio)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_quitarPrivilegio";
                        command.Parameters.Add(new SqlParameter("@idPrivilegio", idPrivilegio));
                        command.Parameters.Add(new SqlParameter("@idUsuario", idUsuario));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        connection.Open();
                        var i = command.ExecuteNonQuery();

                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)codigo.Value, result = null };
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }
    }
}
