﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Core.Models;
using CoreFletera.Models;
using Core.Utils;
using Core.BusinessLogic;
namespace CoreFletera.BusinessLogic
{
    public class LiquidacionPegasoServices
    {
        public OperationResult saveLiquidacionPegaso(ref LiquidacionPegaso liquidacion)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    con.Open();
                    using (SqlTransaction tran = con.BeginTransaction())
                    {
                        using (SqlCommand command = CrearSqlCommand.getCommand(con, "sp_saveLiquidacion", true))
                        {
                            command.Transaction = tran;
                            command.Parameters.Add(new SqlParameter("@idLiquidacion", liquidacion.idLiquidacion));
                            command.Parameters.Add(new SqlParameter("@fecha", liquidacion.fecha));
                            command.Parameters.Add(new SqlParameter("@idOperador", liquidacion.operador.clave));
                            command.Parameters.Add(new SqlParameter("@realiza", liquidacion.realiza));
                            command.Parameters.Add(new SqlParameter("@fechaInicio", liquidacion.fechaInicio));
                            command.Parameters.Add(new SqlParameter("@fechaFin", liquidacion.fechaFin));
                            command.Parameters.Add(new SqlParameter("@sumaModalidad", liquidacion.sumaModalidad));
                            command.Parameters.Add(new SqlParameter("@sueldoFijo", liquidacion.sueldoFijo));
                            command.Parameters.Add(new SqlParameter("@extraSegregacion", liquidacion.extraSegregacion));
                            command.Parameters.Add(new SqlParameter("@sumaExtras", liquidacion.sumaExtras));
                            command.Parameters.Add(new SqlParameter("@importePagado", liquidacion.importePagado));

                            command.ExecuteNonQuery();
                            if ((int)command.Parameters["@valor"].Value == 0)
                            {
                                liquidacion.idLiquidacion = (int)command.Parameters["@id"].Value;
                                command.CommandText = "sp_saveDetalleLiquidacion"; 
                                foreach (var item in liquidacion.listDetalles)
                                {
                                    command.Parameters.Clear();
                                    foreach (var param in CrearSqlCommand.getPatametros(true))
                                    {
                                        command.Parameters.Add(param);
                                    }
                                    command.Parameters.Add(new SqlParameter("@idLiquidacion", liquidacion.idLiquidacion));
                                    command.Parameters.Add(new SqlParameter("@idDetalleLiquidacion", item.idDetalleLiquidacion));
                                    command.Parameters.Add(new SqlParameter("@viaje", item.folioViaje));
                                    //command.Parameters.Add(new SqlParameter("@idPagoModalidad", item.pagoPorModalidad.idPagoPorModalidad));
                                    command.Parameters.Add(new SqlParameter("@precio", item.precio));
                                    command.Parameters.Add(new SqlParameter("@doble", item.doble));
                                    command.Parameters.Add(new SqlParameter("@viatico", item.viatico));
                                    command.Parameters.Add(new SqlParameter("@ruta", item.ruta));
                                    command.Parameters.Add(new SqlParameter("@importe", item.importe));

                                    command.ExecuteNonQuery();
                                    if ((int)command.Parameters["@valor"].Value == 0)
                                    {
                                        item.idDetalleLiquidacion = (int)command.Parameters["@id"].Value;
                                    }
                                    else
                                    {
                                        tran.Rollback();
                                        return new OperationResult(command);
                                    }
                                }
                                command.CommandText = "sp_savePagosExtraOperadores";
                                if (liquidacion.listaExtras != null)
                                {
                                    foreach (var item in liquidacion.listaExtras)
                                    {
                                        command.Parameters.Clear();
                                        foreach (var param in CrearSqlCommand.getPatametros(true))
                                        {
                                            command.Parameters.Add(param);
                                        }
                                        command.Parameters.Add(new SqlParameter("@idPago", item.idLiquidacion));
                                        command.Parameters.Add(new SqlParameter("@idLiquidacion", liquidacion.idLiquidacion));
                                        command.Parameters.Add(new SqlParameter("@idMotivo", (int)item.motivo));
                                        command.Parameters.Add(new SqlParameter("@motivoCtr", item.motivoStr));
                                        command.Parameters.Add(new SqlParameter("@importe", item.importe));
                                        command.ExecuteNonQuery();
                                        if ((int)command.Parameters["@valor"].Value == 0)
                                        {
                                            item.idPagosExtraOperadores = (int)command.Parameters["@id"].Value;
                                        }
                                        else
                                        {
                                            tran.Rollback();
                                            return new OperationResult(command);
                                        }
                                    }
                                }
                                
                                tran.Commit();
                                return new OperationResult(command);
                            }
                            else
                            {
                                tran.Rollback();
                                return new OperationResult(command);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        public OperationResult getLiquidacionById(int idLiquidacion)
        {
            try
            {
                using (SqlConnection con  = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comand = CrearSqlCommand.getCommand(con, "sp_getLiquidacionById"))
                    {
                        comand.Parameters.Add(new SqlParameter("@idLiquidacion", idLiquidacion));
                        LiquidacionPegaso liquicacion = new LiquidacionPegaso();
                        con.Open();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                liquicacion = new LiquidacionPegaso
                                {
                                    idLiquidacion = (int)sqlReader["idLiquidacion"],
                                    fecha = (DateTime)sqlReader["fecha"],
                                    fechaInicio = (DateTime)sqlReader["fechaInicio"],
                                    fechaFin = (DateTime)sqlReader["fechaFin"],
                                    realiza = (string)sqlReader["realiza"],
                                    sueldoFijo = (decimal)sqlReader["sueldoFijo"],
                                    sumaModalidad = (decimal)sqlReader["sumaModalidad"],
                                    extraSegregacion = (decimal)sqlReader["extraSegregacion"],
                                    importePagado = (decimal)sqlReader["importePagado"],
                                    sumaExtras = (decimal)sqlReader["sumaExtras"]
                                };

                                Personal operador = new mapComunes().mapPersonal(sqlReader);
                                if (operador == null)
                                {
                                    return new OperationResult { valor = 1, mensaje = "ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA" };
                                }
                                else
                                {
                                    liquicacion.operador = operador;
                                }
                                liquicacion.listaExtras = new List<PagosExtraOperadores>();
                                liquicacion.listDetalles = new List<DetalleLiquidacion_old>();
                                var r = getDetallesByIdLiquidacion(idLiquidacion);
                                liquicacion.listDetalles = (r.result as List<DetalleLiquidacion_old>);

                                var resp = getDetallesByIdLiquidacion((int)sqlReader["idLiquidacion"]);
                                if (resp.typeResult == ResultTypes.success)
                                {
                                    liquicacion.listDetalles = resp.result as List<DetalleLiquidacion_old>;
                                }
                                else
                                {
                                    return resp;
                                }

                                resp = getPagosExtrasByIdLiquidacion((int)sqlReader["idLiquidacion"]);
                                if (resp.typeResult == ResultTypes.success)
                                {
                                    liquicacion.listaExtras = resp.result as List<PagosExtraOperadores>;
                                }
                                else
                                {
                                    return resp;
                                }

                            }
                        }
                        return new OperationResult(comand, liquicacion);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        public OperationResult getDetallesByIdLiquidacion(int idLiquidacion)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comand = CrearSqlCommand.getCommand(con, "sp_getDetallesByIdLiquidacion"))
                    {
                        comand.Parameters.Add(new SqlParameter("@idLiquidacion", idLiquidacion));
                        List<DetalleLiquidacion_old> lista = new List<DetalleLiquidacion_old>();
                        con.Open();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                DetalleLiquidacion_old detalle = new DetalleLiquidacion_old
                                {
                                    idDetalleLiquidacion = (int)sqlReader["idLiquidacion"],
                                    precio = (decimal)sqlReader["precio"],
                                    doble = Convert.ToBoolean(sqlReader["doble"]),
                                    viatico = (decimal)sqlReader["viatico"]                                    
                                };
                                var respViaje = new Core.Interfaces.CartaPorteSvc().getCartaPorteById((int)sqlReader["viaje"]);
                                if (respViaje.typeResult == ResultTypes.success)
                                {
                                    detalle.viaje =((List<Viaje>) respViaje.result)[0];
                                }
                                else
                                {
                                    return respViaje;
                                }

                                //var respPago = new PagoPorModalidadServices().getPagoModalidadByidModalidad((int)sqlReader["idPagoModalidad"]);
                                //if (respPago.typeResult == ResultTypes.success)
                                //{
                                //    detalle.pagoPorModalidad = respPago.result as PagoPorModalidad;
                                //}
                                //else
                                //{
                                //    return respPago;
                                //}
                                lista.Add(detalle);
                            }
                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        public OperationResult getPagosExtrasByIdLiquidacion(int idLiquidacion)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comand = CrearSqlCommand.getCommand(con, "sp_getPagosExtrasByIdLiquidacion"))
                    {
                        comand.Parameters.Add(new SqlParameter("@idLiquidacion", idLiquidacion));
                        List<PagosExtraOperadores> lista = new List<PagosExtraOperadores>();
                        con.Open();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                PagosExtraOperadores detalle = new PagosExtraOperadores
                                {
                                    idPagosExtraOperadores = (int)sqlReader["idPagoExtraOperadores"],
                                    idLiquidacion = (int)sqlReader["idLiquidacion"],
                                    motivo = (MotivoExtra)sqlReader["motivo"],
                                    importe = (decimal)sqlReader["importe"]
                                };
                                
                                lista.Add(detalle);
                            }
                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        public OperationResult saveConfiguracionCliente(ref ConfiguracionClienteLiquidacion config)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    con.Open();
                    using (SqlTransaction tran = con.BeginTransaction())
                    {
                        using (SqlCommand comand = CrearSqlCommand.getCommand(con, "sp_saveConfigCliente"))
                        {
                            comand.Transaction = tran;
                            comand.Parameters.Add(new SqlParameter("@idCliente", config.cliente.clave));
                            comand.Parameters.Add(new SqlParameter("@diaInicio", config.diaInicio));
                            comand.Parameters.Add(new SqlParameter("@HoraInicio", config.horaInicio.ToString()));
                            comand.Parameters.Add(new SqlParameter("@diaFin", config.diaFin));
                            comand.Parameters.Add(new SqlParameter("@horaFin", config.horaFin.ToString()));
                            comand.Parameters.Add(new SqlParameter("@noViajes", config.noViajes));

                            comand.ExecuteNonQuery();
                            if ((int)comand.Parameters["@valor"].Value == 0)
                            {
                                tran.Commit();
                            }
                            else
                            {
                                tran.Rollback();
                            }
                            return new OperationResult(comand);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        public OperationResult getCongByIdCliente(int idCliente)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comand = CrearSqlCommand.getCommand(con, "sp_getCongByIdCliente"))
                    {
                        comand.Parameters.Add(new SqlParameter("@idCliente", idCliente));
                        ConfiguracionClienteLiquidacion config = new ConfiguracionClienteLiquidacion();
                        con.Open();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                config = new ConfiguracionClienteLiquidacion
                                {
                                    idConfig = (int)sqlReader["idConfig"],
                                    cliente = new mapComunes().mapCliente(sqlReader),
                                    diaInicio = (int)sqlReader["diaInicio"],
                                    horaInicio = (string)sqlReader["horaInicio"],
                                    diaFin = (int)sqlReader["diaFin"],
                                    horaFin = (string)sqlReader["horaFin"],
                                    noViajes = (int)sqlReader["noViajes"]
                                };                                
                            }                           
                        }
                        return new OperationResult(comand, config);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getAllConfigCliente()
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comand = CrearSqlCommand.getCommand(con, "sp_getAllConfigCliente"))
                    {
                        List< ConfiguracionClienteLiquidacion> lista = new List<ConfiguracionClienteLiquidacion>();
                        con.Open();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                ConfiguracionClienteLiquidacion config = new ConfiguracionClienteLiquidacion
                                {
                                    idConfig = (int)sqlReader["idConfig"],
                                    cliente = new mapComunes().mapCliente(sqlReader),
                                    diaInicio = (int)sqlReader["diaInicio"],
                                    horaInicio = (string)sqlReader["horaInicio"],
                                    diaFin = (int)sqlReader["diaFin"],
                                    horaFin = (string)sqlReader["horaFin"],
                                    noViajes = (int)sqlReader["noViajes"]
                                };
                                lista.Add(config);
                            }
                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
}
