﻿namespace CoreFletera.BusinessLogic
{
    using Core.BusinessLogic;
    using Core.Models;
    using Core.Utils;
    using CoreFletera.Models;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Runtime.InteropServices;

    public class LlantasServices
    {
        internal OperationResult asignarOrdenLlantera(int idOrden, Usuario usuario, Personal personalEntrega, Personal personalResp, Personal personalAyu1)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_asignarOrdenLlantera";
                        command.Parameters.Add(new SqlParameter("@idPersonalEntrega", personalEntrega.clave));
                        command.Parameters.Add(new SqlParameter("@usuario", usuario.nombreUsuario));
                        command.Parameters.Add(new SqlParameter("@idPersonalResp", personalResp.clave));
                        command.Parameters.Add(new SqlParameter("@idPersonalAyu1", (personalAyu1 == null) ? 0 : personalAyu1.clave));
                        command.Parameters.Add(new SqlParameter("@idOrden", idOrden));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        command.ExecuteNonQuery();
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 0,
                    mensaje = exception.Message
                };
            }
            return result;
        }

        internal OperationResult finalizarOrdenesTrabajo(List<DetalleLlantera> lista, int idOrdenSer)
        {
            OperationResult result;
            try
            {
                string str = GenerateXML.generarListaOrdenesTrabajo(lista);
                if (string.IsNullOrEmpty(str))
                {
                    result = new OperationResult
                    {
                        valor = 1,
                        mensaje = "ERROR AL CONSTRUIR EL XML"
                    };
                }
                else
                {
                    using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                    {
                        using (SqlCommand command = connection.CreateCommand())
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.CommandText = "sp_finalizarOrdenesTrabajo";
                            command.Parameters.Add(new SqlParameter("@xml", str));
                            command.Parameters.Add(new SqlParameter("@idOrdenSer", idOrdenSer));
                            SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                            {
                                Direction = ParameterDirection.Output
                            };
                            SqlParameter parameter = command.Parameters.Add(parameter1);
                            SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                            {
                                Direction = ParameterDirection.Output
                            };
                            SqlParameter parameter2 = command.Parameters.Add(parameter3);
                            connection.Open();
                            command.ExecuteNonQuery();
                            result = new OperationResult
                            {
                                valor = new int?((int)parameter.Value),
                                mensaje = (string)parameter2.Value
                            };
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 0,
                    mensaje = exception.Message
                };
            }
            return result;
        }

        public OperationResult getAllLLantasProducto()
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getAllLLantasProducto";
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        List<LLantasProducto> list = new List<LLantasProducto>();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                LLantasProducto item = new mapComunes().mapLLantaProducto(reader);
                                if (item == null)
                                {
                                    return new OperationResult
                                    {
                                        valor = 1,
                                        mensaje = "ERROR AL CONTRUIR LA BUSQUEDA"
                                    };
                                }
                                list.Add(item);
                            }
                        }
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = list
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            return result;
        }

        internal OperationResult getCondicionLlanta()
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getCondicionLlanta", false))
                    {
                        connection.Open();
                        List<CondicionLlanta> list = new List<CondicionLlanta>();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CondicionLlanta item = new CondicionLlanta
                                {
                                    idCondicion = (int)reader["idCondicion"],
                                    descripcion = (string)reader["Descripcion"]
                                };
                                list.Add(item);
                            }
                        }
                        result = new OperationResult(command, list);
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult(exception);
            }
            return result;
        }

        public OperationResult getDetallesServLlantera(int idOrdenServ)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getDetalleLanteraByIdOrden";
                        command.Parameters.Add(new SqlParameter("@idOrdenServ", idOrdenServ));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        List<DetalleLlantera> list = new List<DetalleLlantera>();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                DetalleLlantera item = new DetalleLlantera
                                {
                                    idOrdenServ = (int)reader["idOrdenSer"],
                                    idOrdenActividad = (int)reader["idOrdenActividad"],
                                    idOrdenTrabajo = (int)reader["idOrdenTrabajo"],
                                    posLlanta = (int)reader["PosLlanta"],
                                    descripcion = (string)reader["descripcion"],
                                    estatus = (string)reader["estatus"]
                                };
                                Llanta llanta = new mapComunes().mapLlanta(reader);
                                if (llanta == null)
                                {
                                    return new OperationResult
                                    {
                                        valor = 3,
                                        mensaje = "Ocurrio un error al mapear los datos de la llanta"
                                    };
                                }
                                item.llanta = llanta;
                                if (!DBNull.Value.Equals(reader["servicio_idServicio"]))
                                {
                                    Servicios servicios1 = new Servicios
                                    {
                                        idServicio = (int)reader["servicio_idServicio"]
                                    };
                                    TipoOrdenServ serv1 = new TipoOrdenServ
                                    {
                                        idTipoOrdServ = (int)reader["servicio_TipoOrdenServ_idTipoOrdServ"],
                                        nombreTioServ = (string)reader["servicio_TipoOrdenServ_nombreTipoOrdServ"]
                                    };
                                    servicios1.tipoOrden = serv1;
                                    servicios1.nombreServicio = (string)reader["servicio_nombreServicio"];
                                    servicios1.descripcion = (string)reader["servicio_descripcion"];
                                    servicios1.costo = (decimal)reader["servicio_costo"];
                                    item.servicio = servicios1;
                                }
                                else
                                {
                                    item.servicio = null;
                                }
                                if (!DBNull.Value.Equals(reader["actividad_idActividad"]))
                                {
                                    Actividad actividad1 = new Actividad
                                    {
                                        idActividad = (int)reader["actividad_idActividad"],
                                        nombre = (string)reader["actividad_nombre"],
                                        costo = (decimal)reader["actividad_costo"],
                                        duracion = Convert.ToDecimal(reader["actividad_duracion"])
                                    };
                                    item.actividad = actividad1;
                                }
                                else
                                {
                                    item.actividad = null;
                                }
                                list.Add(item);
                            }
                        }
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = list
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            return result;
        }

        internal OperationResult getFoliosLlantaByIdOrden(int idOrdenCompra, int idProductoLlanta)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getFoliosLlantaByIdOrden";
                        command.Parameters.Add(new SqlParameter("@idOrdenCompra", idOrdenCompra));
                        command.Parameters.Add(new SqlParameter("@idProductoLlanta", idProductoLlanta));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        List<string> list = new List<string>();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                list.Add((string)reader["llanta_clave"]);
                            }
                        }
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = list
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            return result;
        }

        internal OperationResult getLlantaByIdLlanta(int idLlanta)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getLlantaByIdLlanta";
                        command.Parameters.Add(new SqlParameter("@idLlanta", idLlanta));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        Llanta llanta = new Llanta();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                llanta = new mapComunes().mapLlanta(reader);
                                if (llanta == null)
                                {
                                    return new OperationResult
                                    {
                                        valor = 1,
                                        mensaje = "ERROR AL MAPEAR LOS RESULTADOS DE LA BUSQUEDA"
                                    };
                                }
                            }
                        }
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = llanta
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            return result;
        }

        public OperationResult getLLantaByPosAndUnidadTrans(int posicion, UnidadTransporte unidadTransporte)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getLlantasByPosAndUnidadTrans";
                        command.Parameters.Add(new SqlParameter("@posicion", posicion));
                        command.Parameters.Add(new SqlParameter("@idUnidadTrans", unidadTransporte.clave));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        Llanta llanta = new Llanta();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                llanta = new mapComunes().mapLlanta(reader);
                            }
                        }
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = llanta
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            return result;
        }

        public OperationResult getLlantasByIdEmpresa(List<string> listaEmpresa)
        {
            OperationResult result;
            try
            {
                string str = GenerateXML.generarListaCadena(listaEmpresa);
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getLlantasByIdEmpresa", false))
                    {
                        command.Parameters.Add(new SqlParameter("@xml", str));
                        List<Llanta> list = new List<Llanta>();
                        connection.Open();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Llanta item = new mapComunes().mapLlanta(reader);
                                if (item == null)
                                {
                                    return new OperationResult
                                    {
                                        valor = 1,
                                        mensaje = "Ocurrio un error al leer los resultados de la B\x00fasqueda."
                                    };
                                }
                                list.Add(item);
                            }
                        }
                        result = new OperationResult(command, list);
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult(exception);
            }
            return result;
        }

        internal OperationResult getLlantasByOrdenCompra(OrdenCompra ordenCompra)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getLlantasByOrdenCompra";
                        command.Parameters.Add(new SqlParameter("@ordenCompra", ordenCompra.CSERIEDOCUMENTO + ordenCompra.CFOLIO.ToString()));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        List<Llanta> list = new List<Llanta>();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Llanta item = new mapComunes().mapLlanta(reader);
                                if (item == null)
                                {
                                    return new OperationResult
                                    {
                                        valor = 1,
                                        mensaje = "ERROR AL MAPEAR LOS RESULTADOS DE LA BUSQUEDA"
                                    };
                                }
                                list.Add(item);
                            }
                        }
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = list
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            return result;
        }

        internal OperationResult getLlantasEnAlmacen(int idAlmacen = 0, string folioLlanta = "%")
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getLlantasEnAlmacen";
                        command.Parameters.Add(new SqlParameter("@idAlmacen", idAlmacen));
                        command.Parameters.Add(new SqlParameter("@folioLlanta", folioLlanta));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        List<Llanta> list = new List<Llanta>();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Llanta item = new mapComunes().mapLlanta(reader);
                                if (item == null)
                                {
                                    return new OperationResult
                                    {
                                        valor = 1,
                                        mensaje = "ERROR AL MAPEAR LOS RESULTADOS DE LA BUSQUEDA"
                                    };
                                }
                                list.Add(item);
                            }
                        }
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = list
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            return result;
        }

        internal OperationResult getLlantasParaRevitalizar(int idEmpresa, decimal profundidadInicial, decimal profundidadFinal)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getLlantasParaRevitalizar";
                        command.Parameters.Add(new SqlParameter("@idEmpresa", idEmpresa));
                        command.Parameters.Add(new SqlParameter("@profIni", profundidadInicial));
                        command.Parameters.Add(new SqlParameter("@profFinal", profundidadFinal));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        List<Llanta> list = new List<Llanta>();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Llanta item = new mapComunes().mapLlanta(reader);
                                if (list == null)
                                {
                                    return new OperationResult
                                    {
                                        valor = 1,
                                        mensaje = "ERROR AL MAPEAR LOS RESULTADOS DE LA BUSQUEDA"
                                    };
                                }
                                list.Add(item);
                            }
                        }
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = list
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            return result;
        }

        internal OperationResult getLlantasPendientesByIdOrdenCompra(int idOrdenCompra)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getLlantasPendientesByIdOrdenCompra";
                        command.Parameters.Add(new SqlParameter("@idOrdenCompra", idOrdenCompra));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        List<Llanta> list = new List<Llanta>();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Llanta item = new mapComunes().mapLlanta(reader);
                                if (item == null)
                                {
                                    return new OperationResult
                                    {
                                        valor = 1,
                                        mensaje = "ERROR AL LEER LOS DATOS DE LA BUSQUEDA"
                                    };
                                }
                                list.Add(item);
                            }
                        }
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = list
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            return result;
        }

        internal OperationResult getMedidasLlantas()
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getMedidasLlantas";
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        List<string> list = new List<string>();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                list.Add((string)reader["Medida"]);
                            }
                        }
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = list
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            return result;
        }

        public OperationResult getProductoLlantasByMedida(string medida)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getProductoLlantasByMedida";
                        command.Parameters.Add(new SqlParameter("@medida", medida));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        List<LLantasProducto> list = new List<LLantasProducto>();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                LLantasProducto item = new mapComunes().mapLLantaProducto(reader);
                                if (item == null)
                                {
                                    return new OperationResult
                                    {
                                        valor = 1,
                                        mensaje = "ERROR AL CONTRUIR LA BUSQUEDA"
                                    };
                                }
                                list.Add(item);
                            }
                        }
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = list
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            return result;
        }

        internal OperationResult getTiposLlantas()
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getTiposLlantas", false))
                    {
                        connection.Open();
                        List<TipoLlanta> list = new List<TipoLlanta>();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                TipoLlanta item = new TipoLlanta
                                {
                                    clave = (int)reader["idTipoLLanta"],
                                    nombre = (string)reader["Descripcion"]
                                };
                                list.Add(item);
                            }
                        }
                        result = new OperationResult(command, list);
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult(exception);
            }
            return result;
        }

        public OperationResult saveLlanta(ref Llanta llanta, string usuario)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    connection.Open();
                    using (SqlTransaction transaction = connection.BeginTransaction())
                    {
                        using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_saveLlanta", true))
                        {
                            command.Transaction = transaction;
                            command.Parameters.Add(new SqlParameter("@idLlanta", llanta.clave));
                            command.Parameters.Add(new SqlParameter("@idEmpresa", llanta.idEmpresa));
                            command.Parameters.Add(new SqlParameter("@idProductoLlanta", llanta.productoLLanta.idLLantasProducto));
                            command.Parameters.Add(new SqlParameter("@idTipoLLanta", llanta.tipoLlanta.clave));
                            command.Parameters.Add(new SqlParameter("@Factura", llanta.factura));
                            command.Parameters.Add(new SqlParameter("@Fecha", llanta.fechaFactura));
                            command.Parameters.Add(new SqlParameter("@Costo", llanta.costo));
                            command.Parameters.Add(new SqlParameter("@RFDI", llanta.rfid));
                            command.Parameters.Add(new SqlParameter("@Posicion", llanta.posicion));
                            command.Parameters.Add(new SqlParameter("@idtipoUbicacion", (int)llanta.TipoUbicacionLlanta));
                            command.Parameters.Add(new SqlParameter("@idUbicacion", llanta.unidadTransporte.clave));
                            command.Parameters.Add(new SqlParameter("@A\x00f1o", llanta.año));
                            command.Parameters.Add(new SqlParameter("@Mes", llanta.mes));
                            command.Parameters.Add(new SqlParameter("@Observaciones", llanta.observaciones));
                            command.Parameters.Add(new SqlParameter("@UltFecha", llanta.fecha));
                            command.Parameters.Add(new SqlParameter("@idCondicion", llanta.condicionLlanta.idCondicion));
                            command.Parameters.Add(new SqlParameter("@Activo", 1));
                            command.Parameters.Add(new SqlParameter("@ProfundidadAct", llanta.profundidadActual));
                            command.Parameters.Add(new SqlParameter("@idDisenioRev", (llanta.objDiseño == null) ? null : new int?(llanta.objDiseño.idDiseño)));
                            command.Parameters.Add(new SqlParameter("@DOT", llanta.DOT));
                            command.Parameters.Add(new SqlParameter("@usuario", usuario));
                            command.ExecuteNonQuery();
                            int id = 0;
                            if (CrearSqlCommand.validarCorrecto(command, out id))
                            {
                                llanta.idLlanta = id;
                                transaction.Commit();
                            }
                            else
                            {
                                transaction.Rollback();
                            }
                            result = new OperationResult(command, llanta);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult(exception);
            }
            return result;
        }

        internal OperationResult saveLlantasFromOrdenCompra(List<Llanta> listaLlantas, List<RelacionDetalleOrdenFactura> listaRelacion, string nombreUsuario, OrdenCompra ordenCompra)
        {
            OperationResult result;
            try
            {
                string str = GenerateXML.generateListLlantas(listaLlantas);
                string str2 = GenerateXML.generarListaRelacionDetalleFact(listaRelacion);
                if (string.IsNullOrEmpty(str) || string.IsNullOrEmpty(str2))
                {
                    result = new OperationResult
                    {
                        valor = 1,
                        mensaje = "OCURRIO UN ERROR AL FORMAR LOS ARCHIVOS XML"
                    };
                }
                else
                {
                    using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                    {
                        using (SqlCommand command = connection.CreateCommand())
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.CommandText = "sp_saveLlantasFromOrdenCompra";
                            command.Parameters.Add(new SqlParameter("@xmlLlantas", str));
                            command.Parameters.Add(new SqlParameter("@xmlRelacion", str2));
                            command.Parameters.Add(new SqlParameter("@nombreUsuario", nombreUsuario));
                            command.Parameters.Add(new SqlParameter("@compra", ordenCompra.idOrdenCompra));
                            SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                            {
                                Direction = ParameterDirection.Output
                            };
                            SqlParameter parameter = command.Parameters.Add(parameter1);
                            SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                            {
                                Direction = ParameterDirection.Output
                            };
                            SqlParameter parameter2 = command.Parameters.Add(parameter3);
                            connection.Open();
                            command.ExecuteNonQuery();
                            result = new OperationResult
                            {
                                valor = new int?((int)parameter.Value),
                                mensaje = (string)parameter2.Value
                            };
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            return result;
        }

        public OperationResult saveProductoLlanta(ref LLantasProducto lLantasProducto)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    connection.Open();
                    using (SqlTransaction transaction = connection.BeginTransaction())
                    {
                        using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_saveProductoLlanta", true))
                        {
                            command.Transaction = transaction;
                            command.Parameters.Add(new SqlParameter("@idProductoLlanta", lLantasProducto.idLLantasProducto));
                            command.Parameters.Add(new SqlParameter("@idMarca", lLantasProducto.marca.clave));
                            command.Parameters.Add(new SqlParameter("@idDise\x00f1o", lLantasProducto.diseño.idDiseño));
                            command.Parameters.Add(new SqlParameter("@medida", lLantasProducto.diseño.medida.medida));
                            command.Parameters.Add(new SqlParameter("@profundidad", lLantasProducto.profundidad));
                            command.Parameters.Add(new SqlParameter("@activo", lLantasProducto.activo));
                            command.ExecuteNonQuery();
                            int id = 0;
                            if (CrearSqlCommand.validarCorrecto(command, out id))
                            {
                                lLantasProducto.idLLantasProducto = id;
                                transaction.Commit();
                            }
                            else
                            {
                                transaction.Rollback();
                            }
                            result = new OperationResult(command, lLantasProducto);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult(exception);
            }
            return result;
        }
    }
}