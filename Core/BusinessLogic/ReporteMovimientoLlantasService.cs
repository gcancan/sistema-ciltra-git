﻿namespace CoreFletera.BusinessLogic
{
    using Core.Models;
    using Core.Utils;
    using CoreFletera.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;

    public class ReporteMovimientoLlantasService
    {
        public OperationResult getMovimientoLlantas(DateTime fechaInicio, DateTime fechaFin, List<string> listaLlantas)
        {
            int idLlanta = 0;
            string str = GenerateXML.generarListaCadena(listaLlantas);
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getMovimientoLlantas", false))
                    {
                        command.Parameters.Add(new SqlParameter("@fechaInicio", fechaInicio));
                        command.Parameters.Add(new SqlParameter("@fechaFin", fechaFin));
                        command.Parameters.Add(new SqlParameter("@xml", str));
                        List<ReporteMovimientoLlanta> list = new List<ReporteMovimientoLlanta>();
                        connection.Open();

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                idLlanta = (int)reader["idLlanta"];
                                if (idLlanta == 558)
                                {

                                }
                                ReporteMovimientoLlanta item = new ReporteMovimientoLlanta
                                {
                                    fecha = (DateTime)reader["Fecha"],
                                    tipoDocumento = (enumTipoDocumento)Convert.ToInt32(reader["idTipoDoc"]),
                                    idLlanta = (int)reader["idLlanta"],
                                    folioLlanta = (string)reader["folioLlanta"],
                                    tipoUbicacionActual = (string)reader["ubicacionActual"],
                                    descripcionActual = (string)reader["descripcionActual"],
                                    posicionActual = DBNull.Value.Equals(reader["posicionActual"]) ? 0 : ((int)reader["posicionActual"]),
                                    tipoUbicacionAnterior = (string)reader["origen"],
                                    descripcionAnterioir = (string)reader["origenDescripcion"],
                                    posicionAnterior = DBNull.Value.Equals(reader["posAnt"]) ? 0 : Convert.ToInt32(reader["posAnt"]),
                                    tipoUbicacionDestino = (string)reader["destino"],
                                    descripcionDestino = (string)reader["destinoDescripcion"],
                                    posicionDestino = DBNull.Value.Equals(reader["posDestino"]) ? 0 : ((int)reader["posDestino"]),
                                    idDocumento = Convert.ToString(reader["Documento"]),
                                    profundidad = (decimal)reader["ProfundidadAct"],
                                    usuario = (string)reader["Usuario"],
                                    km = DBNull.Value.Equals(reader["km"]) ? decimal.Zero : ((decimal)reader["km"])
                                };
                                list.Add(item);
                            }
                        }
                        return new OperationResult(command, list);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
}
