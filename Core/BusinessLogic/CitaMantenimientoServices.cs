﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using Core.Models;
using CoreFletera.Models;
using Core.Utils;
using Core.BusinessLogic;

namespace CoreFletera.BusinessLogic
{
    public class CitaMantenimientoServices
    {
        public OperationResult saveCitaMantenimiento(ref CitaMantenimiento citaMantenimiento)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    con.Open();
                    using (SqlTransaction tran = con.BeginTransaction())
                    {
                        using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_saveCitaMantenimiento", true))
                        {
                            comad.Transaction = tran;
                            comad.Parameters.Add(new SqlParameter("@idCitaMantenimiento", citaMantenimiento.idCitaMantenimiento));
                            comad.Parameters.Add(new SqlParameter("@fechaCaptura", citaMantenimiento.fechaCaptura));
                            comad.Parameters.Add(new SqlParameter("@idUnidad", citaMantenimiento.unidad.clave));
                            comad.Parameters.Add(new SqlParameter("@fechaCita", citaMantenimiento.fechaCita));
                            comad.Parameters.Add(new SqlParameter("@idServicio", citaMantenimiento.servicio.idServicio));
                            comad.Parameters.Add(new SqlParameter("@usuario", citaMantenimiento.usuario));
                            comad.Parameters.Add(new SqlParameter("@activo", citaMantenimiento.activo));
                            comad.ExecuteNonQuery();

                            if (CrearSqlCommand.validarCorrecto(comad, out int id))
                            {
                                citaMantenimiento.idCitaMantenimiento = id;
                                tran.Commit();
                            }
                            else
                            {
                                tran.Rollback();
                            }

                            return new OperationResult(comad, citaMantenimiento);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getAllCitasMatto()
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getAllCitasMtto"))
                    {
                        con.Open();
                        List<CitaMantenimiento> listaCitas = new List<CitaMantenimiento>();

                        using (SqlDataReader sqlReader = comad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                CitaMantenimiento cita = new mapComunes().mapCitaMatto(sqlReader);
                                if (cita == null) return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");

                                listaCitas.Add(cita);
                            }
                        }

                        return new OperationResult(comad, listaCitas);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getReporteCitasMatto(List<string> listaUnidades, DateTime fechaInicial, DateTime fechaFinal)
        {
            try
            {
                string xml = GenerateXML.generarListaCadena(listaUnidades);
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getReporteCitasMatto"))
                    {
                        comad.Parameters.Add(new SqlParameter("@xmlUnidades", xml));
                        comad.Parameters.Add(new SqlParameter("@fechaInicial", fechaInicial));
                        comad.Parameters.Add(new SqlParameter("@fechaFinal", fechaFinal));
                        con.Open();
                        List<ReporteAllCitasMtto> lista = new List<ReporteAllCitasMtto>();

                        using (SqlDataReader sqlReader = comad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                ReporteAllCitasMtto cita = new ReporteAllCitasMtto
                                {
                                    unidad = (string)sqlReader["unidad"],
                                    fecha = (DateTime)sqlReader["fecha"],
                                    tipo = (string)sqlReader["tipo"]
                                };
                                if (cita == null) return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");

                                lista.Add(cita);
                            }
                        }

                        return new OperationResult(comad, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
}
