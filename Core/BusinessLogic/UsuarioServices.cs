﻿using Core.Models;
using Core.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Interfaces;
using CoreFletera.BusinessLogic;
using CoreFletera.Models;

namespace Core.BusinessLogic
{
    public class UsuarioServices
    {
        public OperationResult getUserByUserPass(string usuario, string contrasena)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getUserByUserPass";
                        command.Parameters.Add(new SqlParameter("@usuario", usuario));
                        command.Parameters.Add(new SqlParameter("@contrasena", contrasena));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        connection.Open();
                        var i = command.ExecuteReader();
                        Usuario _usuario = new Usuario();

                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {

                                _usuario = (new Usuario
                                {
                                    idUsuario = (int)sqlReader["idUsuario"],
                                    nombreUsuario = (string)sqlReader["nombreUsuario"],
                                    contrasena = (string)sqlReader["contrasena"]
                                });

                                OperationResult serv = new OperationResult();
                                /////////////////////////////////////////////////////////////////////////////
                                serv = new OperadoresServices().getPersonalById_v2((int)sqlReader["idPersonal"]);//  mapUsuario((int)sqlReader["idPersonal"]);
                                if (serv.typeResult == ResultTypes.success)
                                {
                                    _usuario.personal = (Personal)serv.result;
                                }
                                else
                                {
                                    serv.mensaje += "Error al buscar el personal con clave " + ((int)sqlReader["idPersonal"]).ToString();
                                    return serv;
                                }
                                /////////////////////////////////////////////////////////////////////////////        
                                if (DBNull.Value.Equals(sqlReader["idCliente"]))
                                {
                                    _usuario.cliente = null;
                                }
                                else
                                {
                                    serv = mapCliente((int)sqlReader["idCliente"]);
                                    if (serv.typeResult == ResultTypes.success)
                                    {
                                        _usuario.cliente = (Cliente)serv.result;
                                    }
                                    else
                                    {
                                        serv.mensaje += "Error al buscar el cliente con clave " + ((int)sqlReader["idCliente"]).ToString();
                                        return serv;
                                    }
                                }

                                Empresa empresa = new mapComunes().mapEmpresa(sqlReader);
                                _usuario.empresa = empresa;

                                if (DBNull.Value.Equals(sqlReader["zonaO_idZonaOperativa"]))
                                {
                                    _usuario.zonaOperativa = null;
                                }
                                else
                                {
                                    ZonaOperativa zonaOperativa = new mapComunes().mapZonaOperativa(sqlReader);
                                    _usuario.zonaOperativa = zonaOperativa;
                                }
                                ///////////////////////////////////////////////////////////////////////////// 
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)codigo.Value, result = _usuario };
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }

        public OperationResult getAllUsuariosOrById_v2(int id)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(con, "sp_getAllUsuariosOrById_v2"))
                    {
                        command.Parameters.Add(new SqlParameter("@id", id));
                        con.Open();
                        List<Usuario> list = new List<Usuario>();
                        using (SqlDataReader sqlReader = command.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                Usuario user = new Usuario
                                {
                                    idUsuario = (int)sqlReader["idUsuario"],
                                    nombreUsuario = (string)sqlReader["nombreUsuario"],
                                    contrasena = (string)sqlReader["contrasena"],
                                    activo = (bool)sqlReader["usuario_activo"]
                                };

                                Personal personal = new mapComunes().mapPersonal(sqlReader);
                                if (personal != null)
                                {
                                    user.personal = personal;
                                }
                                else
                                {
                                    return new OperationResult() { valor = 2, mensaje = "Error al leer la informacion del usuario" };
                                }
                                if (DBNull.Value.Equals(sqlReader["cliente_idCliente"]))
                                {
                                    user.cliente = null;
                                }
                                else
                                {
                                    Cliente cliente = new mapComunes().mapCliente(sqlReader);
                                    if (personal != null)
                                    {
                                        user.cliente = cliente;
                                    }
                                    else
                                    {
                                        return new OperationResult() { valor = 2, mensaje = "Error al leer la informacion del usuario" };
                                    }
                                }

                                Empresa empresa = new mapComunes().mapEmpresa(sqlReader);
                                user.empresa = empresa;

                                if (DBNull.Value.Equals(sqlReader["zonaO_idZonaOperativa"]))
                                {
                                    user.zonaOperativa = null;
                                }
                                else
                                {
                                    ZonaOperativa zonaOperativa = new mapComunes().mapZonaOperativa(sqlReader);
                                    user.zonaOperativa = zonaOperativa;
                                }
                                list.Add(user);
                            }
                        }
                        return new OperationResult(command, list);

                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getAllUserOrById(int id, bool omitirPrivilegios = false)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getAllUsuariosOrById";
                        command.Parameters.Add(new SqlParameter("@id", id));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        connection.Open();
                        var i = command.ExecuteReader();
                        Usuario _usuario = new Usuario();
                        List<Usuario> list = new List<Usuario>();
                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {

                                _usuario = (new Usuario
                                {
                                    idUsuario = (int)sqlReader["idUsuario"],
                                    nombreUsuario = (string)sqlReader["nombreUsuario"],
                                    contrasena = (string)sqlReader["contrasena"]
                                });

                                OperationResult serv = new OperationResult();
                                /////////////////////////////////////////////////////////////////////////////
                                serv = mapUsuario((int)sqlReader["idPersonal"]);
                                if (serv.typeResult == ResultTypes.success)
                                {
                                    _usuario.personal = (Personal)serv.result;
                                }
                                else
                                {
                                    serv.mensaje += "Error al buscar el personal con clave " + ((int)sqlReader["idPersonal"]).ToString();
                                    return serv;
                                }
                                /////////////////////////////////////////////////////////////////////////////                               
                                serv = mapCliente((int)sqlReader["idCliente"], true);
                                if (serv.typeResult == ResultTypes.success)
                                {
                                    _usuario.cliente = (Cliente)serv.result;
                                }
                                else
                                {
                                    serv.mensaje += "Error al buscar el cliente con clave " + ((int)sqlReader["idCliente"]).ToString();
                                    return serv;
                                }
                                ///////////////////////////////////////////////////////////////////////////// 
                                if (omitirPrivilegios)
                                {
                                    _usuario.listPrivilegios = new List<Privilegio>();
                                }
                                else
                                {
                                    serv = mapPrivilegios((int)sqlReader["idUsuario"]);
                                    if (serv.typeResult == ResultTypes.success)
                                    {
                                        _usuario.listPrivilegios = new List<Privilegio>();
                                        _usuario.listPrivilegios = (List<Privilegio>)serv.result;
                                    }
                                    else
                                    {
                                        serv.mensaje += "Error al buscar los privilegios del usuario con clave " + ((int)sqlReader["idUsuario"]).ToString();
                                        return serv;
                                    }
                                }

                                ///////////////////////////////////////////////////////////////////////////// 
                                list.Add(_usuario);
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)codigo.Value, result = list };
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }

        public OperationResult saveUsuario(ref Usuario usuario)
        {
            try
            {
                var xml = GenerateXML.generateListPrivilegios(usuario.listPrivilegios);
                if (xml == null)
                {
                    return new OperationResult { valor = 1, mensaje = "Error al construir el XML" };
                }
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_saveUsuario";
                        command.Parameters.Add(new SqlParameter("@idUsario", usuario.idUsuario));
                        command.Parameters.Add(new SqlParameter("@nombreUsuario", usuario.nombreUsuario));
                        command.Parameters.Add(new SqlParameter("@contrasena", usuario.contrasena));
                        command.Parameters.Add(new SqlParameter("@idPersona", usuario.personal.clave));
                        command.Parameters.Add(new SqlParameter("@idEmpresa", usuario.empresa.clave));
                        command.Parameters.Add(new SqlParameter("@idZonaOperativa", usuario.zonaOperativa.idZonaOperativa));
                        command.Parameters.Add(new SqlParameter("@idCliente", usuario.cliente == null ? null : (int?)usuario.cliente.clave));
                        command.Parameters.Add(new SqlParameter("@XML", xml));
                        command.Parameters.Add(new SqlParameter("@activo", usuario.activo));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        var identificador = command.Parameters.Add(new SqlParameter("@identificador", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        connection.Open();
                        var i = command.ExecuteReader();

                        using (SqlDataReader sqlReader = i)
                        {
                            List<Privilegio> list = new List<Privilegio>();
                            while (sqlReader.Read())
                            {
                                list.Add(new Privilegio
                                {
                                    idPrivilegio = (int)sqlReader["idPrivilegio"],
                                    clave = (string)sqlReader["clave"],
                                    descripcion = (string)sqlReader["descripcion"],
                                    nombre = (string)sqlReader["nombre"]
                                });
                            }

                            usuario.listPrivilegios = new List<Privilegio>();
                            usuario.listPrivilegios = list;
                        }
                        usuario.idUsuario = (int)identificador.Value;
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)codigo.Value, result = usuario, identity = (int)identificador.Value };
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }

        private OperationResult mapCliente(int idCliente, bool omitirProductos = false)
        {
            try
            {
                OperationResult resp = new ClienteSvc().getClientesALLorById(0, idCliente, omitirProductos);
                if (resp.typeResult == ResultTypes.error)
                {
                    return new OperationResult { result = null, valor = 1, mensaje = resp.mensaje };
                }
                else if (resp.typeResult == ResultTypes.recordNotFound)
                {
                    return new OperationResult { result = null, valor = 3, mensaje = resp.mensaje };
                }
                else if (resp.typeResult == ResultTypes.success)
                {
                    List<Cliente> list = (List<Cliente>)resp.result;
                    return new OperationResult { result = list[0], valor = 0, mensaje = resp.mensaje };
                }
            }
            catch (Exception e)
            {
                return new OperationResult { result = null, valor = 1, mensaje = e.Message };
            }
            return new OperationResult();
        }

        private OperationResult mapUsuario(int idPersonal)
        {
            try
            {
                OperationResult resp = new OperadorSvc().getPersonalALLorById(idPersonal);
                if (resp.typeResult == ResultTypes.error)
                {
                    return new OperationResult { result = null, valor = 1, mensaje = resp.mensaje };
                }
                else if (resp.typeResult == ResultTypes.recordNotFound)
                {
                    return new OperationResult { result = null, valor = 0, mensaje = resp.mensaje };
                }
                else if (resp.typeResult == ResultTypes.success)
                {
                    List<Personal> list = (List<Personal>)resp.result;
                    return new OperationResult { result = list[0], valor = 0, mensaje = resp.mensaje };
                }
            }
            catch (Exception e)
            {
                return new OperationResult { result = null, valor = 1, mensaje = e.Message };
            }
            return new OperationResult();
        }

        private OperationResult mapPrivilegios(int idUsuario)
        {
            try
            {
                OperationResult resp = new PrivilegioSvc().getPrivilegiosByIdUsuario(idUsuario);
                if (resp.typeResult == ResultTypes.error)
                {
                    return new OperationResult { result = null, valor = 1, mensaje = resp.mensaje };
                }
                else if (resp.typeResult == ResultTypes.recordNotFound)
                {
                    return new OperationResult { result = null, valor = 0, mensaje = resp.mensaje };
                }
                else if (resp.typeResult == ResultTypes.success)
                {
                    List<Privilegio> list = (List<Privilegio>)resp.result;
                    return new OperationResult { result = list, valor = 0, mensaje = resp.mensaje };
                }
            }
            catch (Exception e)
            {
                return new OperationResult { result = null, valor = 1, mensaje = e.Message };
            }
            return new OperationResult();
        }
        public OperationResult getReporteUsuarios()
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getReporteUsuarios"))
                    {
                        List<ReporteUsuarios> listaReporte = new List<ReporteUsuarios>();
                        con.Open();
                        using (SqlDataReader sqlReader = comad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                ReporteUsuarios reporte = new mapComunes().mapReporteUsuarios(sqlReader);
                                if (reporte == null) return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");
                                listaReporte.Add(reporte);
                            }
                        }
                        return new OperationResult(comad, listaReporte);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getReportePrivilegios()
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getReportePrivilegios"))
                    {
                        List<ReportePrivilegios> listaReporte = new List<ReportePrivilegios>();
                        con.Open();
                        using (SqlDataReader sqlReader = comad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                ReportePrivilegios reporte = new mapComunes().mapReportePrivilegios(sqlReader);
                                if (reporte == null) return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");
                                listaReporte.Add(reporte);
                            }
                        }
                        return new OperationResult(comad, listaReporte);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        public OperationResult getReporteUsuariosPrivilegios()
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getReporteUsuariosPrivilegios"))
                    {
                        List<ReporteUsuarios> listaReporte = new List<ReporteUsuarios>();
                        con.Open();
                        using (SqlDataReader sqlReader = comad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                ReporteUsuarios reporte = new mapComunes().mapReporteUsuarios(sqlReader, true);
                                if (reporte == null) return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");
                                listaReporte.Add(reporte);
                            }
                        }
                        return new OperationResult(comad, listaReporte);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
}
