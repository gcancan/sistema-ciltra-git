﻿namespace CoreFletera.BusinessLogic
{
    using Core.Models;
    using Core.Utils;
    using CoreFletera.Models;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Runtime.InteropServices;

    public class ConsultasTaller
    {
        private string sqlGetProductosActividades = "\r\nBEGIN TRY\r\n    IF @existencia = 1\r\n        BEGIN\r\n            SELECT  CIDPRODUCTO ,\r\n                    CCODIGOPRODUCTO ,\r\n                    CNOMBREPRODUCTO ,\r\n                    CTIPOPRODUCTO ,\r\n                    CSTATUSPRODUCTO ,\r\n                    CIDUNIDADBASE ,\r\n                    CCODALTERN ,\r\n                    CPRECIO1 ,\r\n                    c.CCODIGOVALORCLASIFICACION ,\r\n                    existencia = CAST(ISNULL(( SELECT   SUM(mov.CUNIDADESCAPTURADAS)\r\n                                               FROM     dbo.admMovimientos AS mov\r\n                                                        JOIN dbo.admDocumentos\r\n                                                        AS doc ON doc.CIDDOCUMENTO = mov.CIDDOCUMENTO\r\n                                                        JOIN dbo.admDocumentosModelo\r\n                                                        AS m ON m.CIDDOCUMENTODE = doc.CIDDOCUMENTODE\r\n                                               WHERE    m.CAFECTAEXISTENCIA = 1\r\n                                                        AND mov.CIDPRODUCTO = p.CIDPRODUCTO\r\n                                                        AND mov.CIDALMACEN = 1\r\n                                             ), 0)\r\n                    - ISNULL(( SELECT   SUM(mov.CUNIDADESCAPTURADAS)\r\n                               FROM     dbo.admMovimientos AS mov\r\n                                        JOIN dbo.admDocumentos AS doc ON doc.CIDDOCUMENTO = mov.CIDDOCUMENTO\r\n                                        JOIN dbo.admDocumentosModelo AS m ON m.CIDDOCUMENTODE = doc.CIDDOCUMENTODE\r\n                               WHERE    m.CAFECTAEXISTENCIA = 2\r\n                                        AND mov.CIDPRODUCTO = p.CIDPRODUCTO\r\n                                        AND mov.CIDALMACEN = 1\r\n                             ), 0) AS INT)\r\n            FROM    dbo.admProductos AS p\r\n                    JOIN dbo.admClasificacionesValores AS c ON p.CIDVALORCLASIFICACION6 = c.CIDVALORCLASIFICACION\r\n            WHERE   c.CCODIGOVALORCLASIFICACION LIKE '%' + @area + '%'\r\n                    AND p.CIDPRODUCTO > 0\r\n                    AND p.CTIPOPRODUCTO = @tipo\r\n                    AND p.CNOMBREPRODUCTO LIKE @parametro\r\n                    AND CAST(ISNULL(( SELECT    SUM(mov.CUNIDADESCAPTURADAS)\r\n                                      FROM      dbo.admMovimientos AS mov\r\n                                                JOIN dbo.admDocumentos AS doc ON doc.CIDDOCUMENTO = mov.CIDDOCUMENTO\r\n                                                JOIN dbo.admDocumentosModelo\r\n                                                AS m ON m.CIDDOCUMENTODE = doc.CIDDOCUMENTODE\r\n                                      WHERE     m.CAFECTAEXISTENCIA = 1\r\n                                                AND mov.CIDPRODUCTO = p.CIDPRODUCTO\r\n                                                AND mov.CIDALMACEN = 1\r\n                                    ), 0)\r\n                    - ISNULL(( SELECT   SUM(mov.CUNIDADESCAPTURADAS)\r\n                               FROM     dbo.admMovimientos AS mov\r\n                                        JOIN dbo.admDocumentos AS doc ON doc.CIDDOCUMENTO = mov.CIDDOCUMENTO\r\n                                        JOIN dbo.admDocumentosModelo AS m ON m.CIDDOCUMENTODE = doc.CIDDOCUMENTODE\r\n                               WHERE    m.CAFECTAEXISTENCIA = 2\r\n                                        AND mov.CIDPRODUCTO = p.CIDPRODUCTO\r\n                                        AND mov.CIDALMACEN = 1\r\n                             ), 0) AS INT) > 0 \r\n                     ORDER BY p.CNOMBREPRODUCTO ASC;\r\n        END;\r\n    ELSE\r\n        BEGIN\r\n            SELECT  CIDPRODUCTO ,\r\n                    CCODIGOPRODUCTO ,\r\n                    CNOMBREPRODUCTO ,\r\n                    CTIPOPRODUCTO ,\r\n                    CSTATUSPRODUCTO ,\r\n                    CIDUNIDADBASE ,\r\n                    CCODALTERN ,\r\n                    CPRECIO1 ,\r\n                    c.CCODIGOVALORCLASIFICACION ,\r\n                    existencia = CAST(ISNULL(( SELECT   SUM(mov.CUNIDADESCAPTURADAS)\r\n                                               FROM     dbo.admMovimientos AS mov\r\n                                                        JOIN dbo.admDocumentos\r\n                                                        AS doc ON doc.CIDDOCUMENTO = mov.CIDDOCUMENTO\r\n                                                        JOIN dbo.admDocumentosModelo\r\n                                                        AS m ON m.CIDDOCUMENTODE = doc.CIDDOCUMENTODE\r\n                                               WHERE    m.CAFECTAEXISTENCIA = 1\r\n                                                        AND mov.CIDPRODUCTO = p.CIDPRODUCTO\r\n                                                        AND mov.CIDALMACEN = 1\r\n                                             ), 0)\r\n                    - ISNULL(( SELECT   SUM(mov.CUNIDADESCAPTURADAS)\r\n                               FROM     dbo.admMovimientos AS mov\r\n                                        JOIN dbo.admDocumentos AS doc ON doc.CIDDOCUMENTO = mov.CIDDOCUMENTO\r\n                                        JOIN dbo.admDocumentosModelo AS m ON m.CIDDOCUMENTODE = doc.CIDDOCUMENTODE\r\n                               WHERE    m.CAFECTAEXISTENCIA = 2\r\n                                        AND mov.CIDPRODUCTO = p.CIDPRODUCTO\r\n                                        AND mov.CIDALMACEN = 1\r\n                             ), 0) AS INT)\r\n            FROM    dbo.admProductos AS p\r\n                    JOIN dbo.admClasificacionesValores AS c ON p.CIDVALORCLASIFICACION6 = c.CIDVALORCLASIFICACION\r\n            WHERE   c.CCODIGOVALORCLASIFICACION LIKE '%' + @area + '%'\r\n                    AND p.CIDPRODUCTO > 0\r\n                    AND p.CTIPOPRODUCTO = @tipo\r\n                    AND p.CNOMBREPRODUCTO LIKE @parametro\r\n                    ORDER BY p.CNOMBREPRODUCTO ASC;\r\n        END;    \r\n    \r\n    IF @@ROWCOUNT > 0\r\n        BEGIN\r\n            SET @valor = 0;\r\n            SET @mensaje = 'ACTIVIDADES ENCONTRADAS';\r\n        END;\r\n    ELSE\r\n        BEGIN\r\n            SET @valor = 3;\r\n            SET @mensaje = 'ACTIVIDADES NO ENCONTRADAS';\r\n        END;\r\nEND TRY\r\nBEGIN CATCH\r\n    SET @area = 1;\r\n    SET @mensaje = ERROR_MESSAGE();\r\nEND CATCH;";

        public OperationResult consultarExistenByProducto(int idProducto)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactoryTaller(false))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText = "\r\nBEGIN TRY\r\n    --DECLARE @CIDPRODUCTO INT = ISNULL(( SELECT  CIDPRODUCTO\r\n    --                                    FROM    dbo.admProductos\r\n    --                                    WHERE   CCODIGOPRODUCTO = @idProducto\r\n    --                                  ), 0);\r\n    DECLARE @CIDPRODUCTO INT = @idProducto;\r\n    IF @CIDPRODUCTO = 0\r\n        BEGIN\r\n            SET @existencia = 0;\r\n            SET @valor = 3;\r\n            SET @mensaje = 'NO SE ENCONTRO EL PRODUCTO';\r\n        END;\r\n    ELSE\r\n        BEGIN\r\n            DECLARE @entradas DECIMAL(18, 4) = ( SELECT SUM(mov.CUNIDADESCAPTURADAS)\r\n                                                 FROM   dbo.admMovimientos AS mov\r\n                                                        JOIN dbo.admDocumentos\r\n                                                        AS doc ON doc.CIDDOCUMENTO = mov.CIDDOCUMENTO\r\n                                                        JOIN dbo.admDocumentosModelo\r\n                                                        AS m ON m.CIDDOCUMENTODE = doc.CIDDOCUMENTODE\r\n                                                 WHERE  m.CAFECTAEXISTENCIA = 1\r\n                                                        AND mov.CIDPRODUCTO = @CIDPRODUCTO\r\n                                                        AND mov.CIDALMACEN = 1\r\n                                               );\r\n            DECLARE @salidas DECIMAL(18, 4) = ( SELECT  SUM(mov.CUNIDADESCAPTURADAS)\r\n                                                FROM    dbo.admMovimientos AS mov\r\n                                                        JOIN dbo.admDocumentos\r\n                                                        AS doc ON doc.CIDDOCUMENTO = mov.CIDDOCUMENTO\r\n                                                        JOIN dbo.admDocumentosModelo\r\n                                                        AS m ON m.CIDDOCUMENTODE = doc.CIDDOCUMENTODE                                                        \r\n                                                WHERE   m.CAFECTAEXISTENCIA = 2\r\n                                                        AND mov.CIDPRODUCTO = @CIDPRODUCTO\r\n                                                        AND mov.CIDALMACEN = 1\r\n                                              );\r\n            SET @existencia = ISNULL(@entradas, 0) - ISNULL(@salidas, 0);\r\n            SET @valor = 0;\r\n            SET @mensaje = 'PRODUCTO Y EXISTENCIAS ENCONTRADAS';\r\n        END;\r\n    \r\nEND TRY\r\nBEGIN CATCH\r\n    SET @existencia = 0;\r\n    SET @valor = 1;\r\n    SET @mensaje = ERROR_MESSAGE();\r\nEND CATCH;";
                        command.Parameters.Add(new SqlParameter("@idProducto", idProducto.ToString()));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter4 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter4);
                        SqlParameter parameter5 = new SqlParameter("@existencia", SqlDbType.Decimal)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter3 = command.Parameters.Add(parameter5);
                        connection.Open();
                        command.ExecuteNonQuery();
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = (decimal)parameter3.Value
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            return result;
        }

        public OperationResult getActividadesCOM(AreaTrabajo area, string parametro = "%")
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactoryTaller(false))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, this.sqlGetProductosActividades, false))
                    {
                        command.CommandType = CommandType.Text;
                        string str = "%";
                        AreaTrabajo trabajo = area;
                        switch (trabajo)
                        {
                            case AreaTrabajo.TALLER:
                                str = "T";
                                break;

                            case AreaTrabajo.LAVADERO:
                                str = "V";
                                break;

                            case AreaTrabajo.LLANTAS:
                                str = "L";
                                break;

                            default:
                                if (trabajo == AreaTrabajo.TOTAS)
                                {
                                    str = "%";
                                }
                                break;
                        }
                        command.Parameters.Add(new SqlParameter("@area", str));
                        command.Parameters.Add(new SqlParameter("@tipo", 3));
                        command.Parameters.Add(new SqlParameter("@existencia", false));
                        command.Parameters.Add(new SqlParameter("@parametro", "%" + parametro + "%"));
                        List<Actividad> list = new List<Actividad>();
                        connection.Open();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Actividad item = new Actividad
                                {
                                    CIDPRODUCTO = (int)reader["CIDPRODUCTO"],
                                    CCODIGOPRODUCTO = (string)reader["CCODIGOPRODUCTO"],
                                    nombre = (string)reader["CNOMBREPRODUCTO"],
                                    costo = Convert.ToDecimal(reader["CPRECIO1"]),
                                    lavadero = ((string)reader["CCODIGOVALORCLASIFICACION"]).Contains("V"),
                                    llantera = ((string)reader["CCODIGOVALORCLASIFICACION"]).Contains("L"),
                                    taller = ((string)reader["CCODIGOVALORCLASIFICACION"]).Contains("T")
                                };
                                list.Add(item);
                            }
                        }
                        result = new OperationResult(command, list);
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult(exception);
            }
            return result;
        }

        public OperationResult getCantPendientesByIdProducto(int Idproducto)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getCantPendientesByIdProducto", false))
                    {
                        command.Parameters.Add(new SqlParameter("@idProducto", Idproducto));
                        SqlParameter parameter1 = new SqlParameter("@cantidad", SqlDbType.Decimal)
                        {
                            Direction = ParameterDirection.Output
                        };
                        command.Parameters.Add(parameter1);
                        connection.Open();
                        command.ExecuteNonQuery();
                        result = new OperationResult(command, (decimal)command.Parameters["@cantidad"].Value);
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult(exception);
            }
            return result;
        }

        public OperationResult getProductosCOM(AreaTrabajo area, string parametro = "%", bool soloExistencias = true)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactoryTaller(false))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, this.sqlGetProductosActividades, false))
                    {
                        command.CommandType = CommandType.Text;
                        string str = "%";
                        switch (area)
                        {
                            case AreaTrabajo.TALLER:
                                str = "T";
                                break;

                            case AreaTrabajo.LAVADERO:
                                str = "V";
                                break;

                            case AreaTrabajo.LLANTAS:
                                str = "L";
                                break;
                        }
                        command.Parameters.Add(new SqlParameter("@area", str));
                        command.Parameters.Add(new SqlParameter("@tipo", 1));
                        command.Parameters.Add(new SqlParameter("@existencia", soloExistencias));
                        command.Parameters.Add(new SqlParameter("@parametro", "%" + parametro + "%"));
                        List<ProductosCOM> list = new List<ProductosCOM>();
                        connection.Open();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                ProductosCOM item = new ProductosCOM
                                {
                                    idProducto = (int)reader["CIDPRODUCTO"],
                                    codigo = (string)reader["CCODIGOPRODUCTO"],
                                    nombre = (string)reader["CNOMBREPRODUCTO"],
                                    precio = Convert.ToDecimal(reader["CPRECIO1"]),
                                    lavadero = ((string)reader["CCODIGOVALORCLASIFICACION"]).Contains("V"),
                                    llantera = ((string)reader["CCODIGOVALORCLASIFICACION"]).Contains("L"),
                                    taller = ((string)reader["CCODIGOVALORCLASIFICACION"]).Contains("T"),
                                    existencia = (int)reader["existencia"]
                                };
                                list.Add(item);
                            }
                        }
                        result = new OperationResult(command, list);
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult(exception);
            }
            return result;
        }
    }
}
