﻿namespace CoreFletera.BusinessLogic
{
    using Core.Models;
    using Core.Utils;
    using CoreFletera.Models;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Runtime.InteropServices;

    public class BitacoraCargaCombustibleService
    {
        public OperationResult getBitacoraCargaCombustible(DateTime fechaInicial, DateTime fechaFinal, int idEmpresa, string tractor = "%", int idProveedor = 0)
        {
            OperationResult result;
            int num = 0;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getBitacoraCargaCombustible";
                        command.Parameters.Add(new SqlParameter("@fechaInicial", fechaInicial));
                        command.Parameters.Add(new SqlParameter("@fechaFinal", fechaFinal));
                        command.Parameters.Add(new SqlParameter("@idEmpresa", idEmpresa));
                        command.Parameters.Add(new SqlParameter("@tractor", tractor));
                        command.Parameters.Add(new SqlParameter("@idProveedor", idProveedor));
                        SqlParameter parameter1 = new SqlParameter("@valor", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter = command.Parameters.Add(parameter1);
                        SqlParameter parameter3 = new SqlParameter("@mensaje", SqlDbType.NVarChar, 0xff)
                        {
                            Direction = ParameterDirection.Output
                        };
                        SqlParameter parameter2 = command.Parameters.Add(parameter3);
                        connection.Open();
                        List<BitacoraCargaCombustible> list = new List<BitacoraCargaCombustible>();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                num = (int)reader["idCarga"];
                                if (num == 0x3b98)
                                {
                                }
                                BitacoraCargaCombustible item = new BitacoraCargaCombustible
                                {
                                    idCarga = (int)reader["idCarga"],
                                    idEntrada = DBNull.Value.Equals(reader["idEntrada"]) ? null: (int?)reader["idEntrada"],
                                    orden = (int)reader["orden"],
                                    fecha = (DateTime)reader["fecha"],
                                    fechaEntrada = DBNull.Value.Equals(reader["fechaEntrada"]) ? null : (DateTime?)reader["fechaEntrada"],
                                    unidad = (string)reader["unidad"],
                                    chofer = (string)reader["chofer"],
                                    proveedor = (string)reader["proveedor"],
                                    kmAnterior = (decimal)reader["kmAnterior"],
                                    kmCarga = (decimal)reader["kmCarga"],
                                    kmEntrada = DBNull.Value.Equals(reader["kmEntrada"]) ? 0:  (decimal)reader["kmEntrada"],
                                    kmCalculado = (decimal)reader["kmCalculado"],
                                    ltsCargados = (decimal)reader["ltsCargados"],
                                    ltsReinicio = (decimal)reader["ltsReinicio"],
                                    ltsManejo = (decimal)reader["ltsManejo"],
                                    despachador = (string)reader["despachador"],
                                    remolque1 = (string)reader["idRemolque1"],
                                    remolque2 = (string)reader["idRemolque2"],
                                    capacidad = (decimal)reader["capacidad"],
                                    kmRecorridos = (decimal)reader["kmRecorridos"],
                                    ltsExtra = (decimal)reader["ltsExtra"]
                                };
                                list.Add(item);
                            }
                        }
                        result = new OperationResult
                        {
                            valor = new int?((int)parameter.Value),
                            mensaje = (string)parameter2.Value,
                            result = list
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            return result;
        }
        public OperationResult getBitacoraCargaCombustibleByFiltros(DateTime fechaInicial, DateTime fechaFinal, int idEmpresa, int idZonaOperativa,
            List<string> listaProveedores, bool todosProveedores, List<string> listaUnidades, bool todasUnidades, List<string> listaOperadores, bool todosOperadores)
        {          
            try
            {
                string xmlUnidades = GenerateXML.generarListaCadena(listaUnidades);
                string xmlProveedores = GenerateXML.generarListaCadena(listaProveedores);
                string xmlOperadores = GenerateXML.generarListaCadena(listaOperadores);
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getCargasCombustibleByFiltros"))
                    {
                        
                        command.Parameters.Add(new SqlParameter("@fechaInicial", fechaInicial));
                        command.Parameters.Add(new SqlParameter("@fechaFinal", fechaFinal));
                        command.Parameters.Add(new SqlParameter("@idEmpresa", idEmpresa));
                        command.Parameters.Add(new SqlParameter("@idZonaOperativa", idZonaOperativa));
                        command.Parameters.Add(new SqlParameter("@xmlUnidades", xmlUnidades));
                        command.Parameters.Add(new SqlParameter("@todasUnidades", todasUnidades));
                        command.Parameters.Add(new SqlParameter("@xmlProveedores", xmlProveedores));
                        command.Parameters.Add(new SqlParameter("@todosProveedores", todosProveedores));
                        command.Parameters.Add(new SqlParameter("@xmlOperadores", xmlOperadores));
                        command.Parameters.Add(new SqlParameter("@todosOperadores", todosOperadores));
                        command.CommandTimeout = 1800;

                        connection.Open();
                        List<BitacoraCargaCombustible> list = new List<BitacoraCargaCombustible>();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {                               
                                BitacoraCargaCombustible item = new BitacoraCargaCombustible
                                {
                                    idCarga = (int)reader["idCarga"],
                                    idEntrada = DBNull.Value.Equals(reader["idEntrada"]) ? null : (int?)reader["idEntrada"],
                                    orden = (int)reader["orden"],
                                    fecha = (DateTime)reader["fecha"],
                                    fechaEntrada = DBNull.Value.Equals(reader["fechaEntrada"]) ? null : (DateTime?)reader["fechaEntrada"],
                                    unidad = (string)reader["unidad"],
                                    chofer = (string)reader["chofer"],
                                    proveedor = (string)reader["proveedor"],
                                    kmAnterior = (decimal)reader["kmAnterior"],
                                    kmCarga = (decimal)reader["kmCarga"],
                                    kmEntrada = DBNull.Value.Equals(reader["kmEntrada"]) ? 0 : (decimal)reader["kmEntrada"],
                                    kmCalculado = (decimal)reader["kmCalculado"],
                                    ltsCargados = (decimal)reader["ltsCargados"],
                                    ltsReinicio = (decimal)reader["ltsReinicio"],
                                    ltsManejo = (decimal)reader["ltsManejo"],
                                    despachador = (string)reader["despachador"],
                                    remolque1 = (string)reader["idRemolque1"],
                                    remolque2 = (string)reader["idRemolque2"],
                                    capacidad = (decimal)reader["capacidad"],
                                    kmRecorridos = (decimal)reader["kmRecorridos"],
                                    ltsExtra = (decimal)reader["ltsExtra"],
                                    ticket = (string)reader["ticket"],
                                    folioImpreso = (string)reader["folioImpreso"],
                                    facturaProveedor = (string)reader["facturaProveedor"],
                                    precioCombustible = (decimal)reader["precioCombustible"],
                                    importe = (decimal)reader["importe"],
                                };
                                list.Add(item);
                            }
                        }
                        return new OperationResult(command, list);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
}
