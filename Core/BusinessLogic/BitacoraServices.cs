﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using Core.Utils;
using CoreFletera.Models;
using System.Data.SqlClient;
using System.Data;

namespace CoreFletera.BusinessLogic
{
    public class BitacoraServices
    {
        public OperationResult saveBitacora(ref BitacoraEntradasSalidas bitacora)
        {
            try
            {
                string xml = GenerateXML.generarListaMotivos(bitacora.listaMotivos);
                if(string.IsNullOrEmpty(xml))
                {
                    return new OperationResult { valor = 2, mensaje = "Error al construir el xml" };
                }
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comand = con.CreateCommand())
                    {
                        comand.CommandType = CommandType.StoredProcedure;
                        comand.CommandText = "sp_saveBitacora";

                        comand.Parameters.Add(new SqlParameter("@idBitacora", bitacora.idBitacora));
                        comand.Parameters.Add(new SqlParameter("@idTractor", bitacora.tractor.clave));
                        comand.Parameters.Add(new SqlParameter("@idTolva1", bitacora.tolva1 == null ? null : bitacora.tolva1.clave));
                        comand.Parameters.Add(new SqlParameter("@idDolly", bitacora.dolly == null ? null : bitacora.dolly.clave));
                        comand.Parameters.Add(new SqlParameter("@idTolva2", bitacora.tolva2 == null ? null : bitacora.tolva2.clave));
                        comand.Parameters.Add(new SqlParameter("@idChofer", bitacora.chofer.clave));
                        comand.Parameters.Add(new SqlParameter("@km", bitacora.Kilometraje));
                        comand.Parameters.Add(new SqlParameter("@usuario", bitacora.usuario));
                        comand.Parameters.Add(new SqlParameter("@xml", xml));

                        var spValor = comand.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var identity = comand.Parameters.Add(new SqlParameter("@identity", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var spMensaje = comand.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                        con.Open();
                        comand.ExecuteNonQuery();

                        if ((int)spValor.Value == 0)
                        {
                            bitacora.idBitacora = (int)identity.Value;
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = bitacora };

                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message, result = null };
            }
        }

        public OperationResult getBitacoraByIdTractor(string idTractor)
        {
            try
            {
               
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comand = con.CreateCommand())
                    {
                        comand.CommandType = CommandType.StoredProcedure;
                        comand.CommandText = "sp_getBitacoraByIdTractor";

                        comand.Parameters.Add(new SqlParameter("@idTractor", idTractor));
                        

                        var spValor = comand.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var spMensaje = comand.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                        con.Open();
                        BitacoraEntradasSalidas bitacora = new BitacoraEntradasSalidas();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                bitacora.idBitacora = (int)sqlReader["idBitacora"];
                                bitacora.tractor = new UnidadTransporte { clave = (string)sqlReader["idTractor"] };

                                if (DBNull.Value.Equals(sqlReader["idTolva1"]))
                                {
                                    bitacora.tolva1 = null;
                                }
                                else
                                {
                                    bitacora.tolva1  = new UnidadTransporte { clave = (string)sqlReader["idTolva1"] };
                                }
                                

                                if (DBNull.Value.Equals(sqlReader["idDolly"]))
                                {
                                    bitacora.dolly = null;
                                }
                                else
                                {
                                    bitacora.dolly = new UnidadTransporte { clave = (string)sqlReader["idTractor"] };
                                }

                                if (DBNull.Value.Equals(sqlReader["idTolva2"]))
                                {
                                    bitacora.tolva2 = null;
                                }
                                else
                                {
                                    bitacora.tolva2 = new UnidadTransporte { clave = (string)sqlReader["idTolva2"] };
                                }

                                bitacora.chofer = new Personal { clave = (int)sqlReader["idChofer"] };
                                bitacora.Kilometraje = (decimal)sqlReader["km"];
                            }
                        }

                        
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = bitacora };

                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message, result = null };
            }
        }
    }
}
