﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using Core.Models;
using CoreFletera.Models;
using Core.Utils;
using Core.BusinessLogic;

namespace CoreFletera.BusinessLogic
{
    public class PagoPorModalidadServices
    {
        public OperationResult savePagoPorModalidad(ref PagoPorModalidad pagoXmodalidad)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    con.Open();
                    using (SqlTransaction tran = con.BeginTransaction())
                    {
                        using (SqlCommand command = CrearSqlCommand.getCommand(con, "sp_savePagoPorModalidad", true))
                        {
                            command.Transaction = tran;
                            command.Parameters.Add(new SqlParameter("@idPagoPorModalidad", pagoXmodalidad.idPagoPorModalidad));
                            command.Parameters.Add(new SqlParameter("@idClasificacion", pagoXmodalidad.clasificacion.idClasificacion));
                            command.Parameters.Add(new SqlParameter("@noViaje", pagoXmodalidad.noViaje));
                            command.Parameters.Add(new SqlParameter("@sencillo_24", pagoXmodalidad.sencillo_24));
                            command.Parameters.Add(new SqlParameter("@sencillo_30", pagoXmodalidad.sencillo_30));
                            command.Parameters.Add(new SqlParameter("@sencillo_35", pagoXmodalidad.sencillo_35));
                            command.Parameters.Add(new SqlParameter("@full", pagoXmodalidad.full));
                            command.Parameters.Add(new SqlParameter("@estatus", pagoXmodalidad.estatus));

                            command.ExecuteNonQuery();

                            if ((int)command.Parameters["@valor"].Value == 0)
                            {
                                pagoXmodalidad.idPagoPorModalidad = (int)command.Parameters["@id"].Value;
                                tran.Commit();
                            }
                            else
                            {
                                tran.Rollback();
                            }
                            return new OperationResult(command);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        public OperationResult getPagoModalidadByIdCliente(int idCliente)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comand = CrearSqlCommand.getCommand(con, "sp_getPagoModalidadByIdCliente"))
                    {
                        comand.Parameters.Add(new SqlParameter("@idCliente", idCliente));
                        con.Open();
                        List<PagoPorModalidad> listaPagos = new List<PagoPorModalidad>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                PagoPorModalidad pago = new mapComunes().mapPagoPorModalidad(sqlReader);
                                if (pago == null)
                                {
                                    return new OperationResult { valor = 1, mensaje = "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA" };
                                }
                                listaPagos.Add(pago);
                            }
                        }
                        return new OperationResult(comand, listaPagos);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        public OperationResult getPagoModalidadByNoViaje(int idClasificacion, int noViaje)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comand = CrearSqlCommand.getCommand(con, "sp_getPagoModalidadByNoViaje"))
                    {
                        comand.Parameters.Add(new SqlParameter("@idClasificacion", idClasificacion));
                        comand.Parameters.Add(new SqlParameter("@noViaje", noViaje));
                        con.Open();
                        PagoPorModalidad pago = new PagoPorModalidad();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                pago = new mapComunes().mapPagoPorModalidad(sqlReader);
                                if (pago == null)
                                {
                                    return new OperationResult { valor = 1, mensaje = "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA" };
                                }                                
                            }
                        }
                        return new OperationResult(comand, pago);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        public OperationResult getPagoModalidadByidModalidad(int idModalidad)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comand = CrearSqlCommand.getCommand(con, "sp_getPagoModalidadByidModalidad"))
                    {
                        comand.Parameters.Add(new SqlParameter("@idModalidad", idModalidad));
                        con.Open();
                        PagoPorModalidad pago = new PagoPorModalidad();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                pago = new mapComunes().mapPagoPorModalidad(sqlReader);
                                if (pago == null)
                                {
                                    return new OperationResult { valor = 1, mensaje = "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA" };
                                }
                            }
                        }
                        return new OperationResult(comand, pago);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
}