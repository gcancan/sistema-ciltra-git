﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using Core.Utils;
using System.Data;
using System.Data.SqlClient;
namespace CoreFletera.BusinessLogic
{
    public class KardexMttoPreventivoServices
    {
        public OperationResult getKardexMttoPreventivo(List<string> listaUnidades, List<string> listaServicios, DateTime fechaInicio, DateTime fechaFin)
        {
            try
            {
                var xml = GenerateXML.generarListaCadena(listaUnidades);
                var xmlUnidades = GenerateXML.generarListaCadena(listaServicios);
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getKardexMttoPreventivo"))
                    {
                        comad.Parameters.Add(new SqlParameter("@fechaInicio", fechaInicio));
                        comad.Parameters.Add(new SqlParameter("@fechaFin", fechaFin));
                        comad.Parameters.Add(new SqlParameter("@xml", xml));
                        comad.Parameters.Add(new SqlParameter("@xmlServicios", xmlUnidades));
                        con.Open();
                        List<KardexMttoPreventivo> lista = new List<KardexMttoPreventivo>();
                        using (SqlDataReader sqlReader = comad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                lista.Add(new KardexMttoPreventivo
                                {
                                    idKardex = (int)sqlReader["idInsersionKm"],
                                    idUnidad = (string)sqlReader["idUnidadTransporte"],
                                    fecha = (DateTime)sqlReader["fechaInsersion"],
                                    km = (decimal)sqlReader["km"],
                                    servicio = (string)sqlReader["NomServicio"],
                                    enumTipoInsersionKM = (enumTipoInsersionKM)sqlReader["tipoInsersion"]
                                });
                            }
                        }
                        return new OperationResult(comad, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getKardexMttoPreventivo(List<string> listaUnidades)
        {
            try
            {
                var xml = GenerateXML.generarListaCadena(listaUnidades);
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getKardexMttoPrev"))
                    {                
                        comad.Parameters.Add(new SqlParameter("@xml", xml));
                        con.Open();
                        List<KardexMttoPreventivo> lista = new List<KardexMttoPreventivo>();
                        using (SqlDataReader sqlReader = comad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                lista.Add(new KardexMttoPreventivo
                                {
                                    idKardex = (int)sqlReader["idInsersionKm"],
                                    idUnidad = (string)sqlReader["idUnidadTransporte"],
                                    fecha = (DateTime)sqlReader["fechaInsersion"],
                                    km = (decimal)sqlReader["km"],
                                    servicio = (string)sqlReader["NomServicio"],
                                    enumTipoInsersionKM = (enumTipoInsersionKM)sqlReader["tipoInsersion"]
                                });
                            }
                        }
                        return new OperationResult(comad, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getInsersionKm(List<string> listaUnidades, DateTime fechaInicio, DateTime fechaFin, enumTipoInsersionKM enumTipoInsersionKM)
        {
            try
            {
                var xml = GenerateXML.generarListaCadena(listaUnidades);
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_getInsersionKm"))
                    {
                        comad.Parameters.Add(new SqlParameter("@fechaInicio", fechaInicio));
                        comad.Parameters.Add(new SqlParameter("@fechaFin", fechaFin));
                        comad.Parameters.Add(new SqlParameter("@xml", xml));
                        comad.Parameters.Add(new SqlParameter("@idTipoInsersion", (int)enumTipoInsersionKM));
                        con.Open();
                        List<KardexMttoPreventivo> lista = new List<KardexMttoPreventivo>();
                        using (SqlDataReader sqlReader = comad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                lista.Add(new KardexMttoPreventivo
                                {
                                    idKardex = (int)sqlReader["idInsersionKm"],
                                    idUnidad = (string)sqlReader["idUnidadTransporte"],
                                    fecha = (DateTime)sqlReader["fechaInsersion"],
                                    km = (decimal)sqlReader["km"],
                                    servicio = (string)sqlReader["NomServicio"],
                                    enumTipoInsersionKM = (enumTipoInsersionKM)sqlReader["tipoInsersion"]
                                });
                            }
                        }
                        return new OperationResult(comad, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
}
