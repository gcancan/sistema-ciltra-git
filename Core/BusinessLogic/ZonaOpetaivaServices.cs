﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using Core.Utils;
using Core.BusinessLogic;
using System.Data;
using System.Data.SqlClient;
namespace CoreFletera.BusinessLogic
{
    public class ZonaOpetaivaServices
    {
        public OperationResult getAllZonasOperativas()
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand commad = CrearSqlCommand.getCommand(con, "sp_getAllZonasOperativas"))
                    {
                        List<ZonaOperativa> list = new List<ZonaOperativa>();
                        con.Open();
                        using (SqlDataReader sqlReader = commad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                ZonaOperativa zonaOperativa = new mapComunes().mapZonaOperativa(sqlReader);
                                if (zonaOperativa == null)
                                {
                                    return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");
                                }
                                else
                                {
                                    list.Add(zonaOperativa);
                                }
                            }
                        }
                        return new OperationResult(commad, list);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getZonaOperativaById(int idZonaOperativa)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand commad = CrearSqlCommand.getCommand(con, "sp_getZonaOperativaById"))
                    {
                        commad.Parameters.Add(new SqlParameter("@idZonaOperativa", idZonaOperativa));
                        ZonaOperativa zonaOperativa = new ZonaOperativa();
                        con.Open();
                        using (SqlDataReader sqlReader = commad.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                zonaOperativa = new mapComunes().mapZonaOperativa(sqlReader);
                                if (zonaOperativa == null)
                                {
                                    return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");
                                }
                            }
                        }
                        return new OperationResult(commad, zonaOperativa);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult sincronizarZonasOperativas(List<ZonaOperativa> listaZonas)
        {
            try
            {
                string xml = GenerateXML.mapListaZonasOperativasSinc(listaZonas);
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    con.Open();
                    using (SqlTransaction tran = con.BeginTransaction())
                    {
                        using (SqlCommand comad = CrearSqlCommand.getCommand(con, "sp_sincronizarZonasOperativas", false, true))
                        {
                            comad.Transaction = tran;
                            comad.Parameters.Add(new SqlParameter("@xml", xml));
                            comad.ExecuteNonQuery();
                            if (!CrearSqlCommand.validarCorrecto(comad))
                            {
                                tran.Rollback();
                                return new OperationResult(comad);
                            }
                            tran.Commit();
                            return new OperationResult(comad);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
}
