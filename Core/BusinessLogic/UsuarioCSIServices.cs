﻿namespace CoreFletera.BusinessLogic
{
    using Core.Models;
    using Core.Utils;
    using CoreFletera.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;

    public class UsuarioCSIServices
    {
        public OperationResult getAllUsuariosCSI()
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_getAllUsuarioCSI", false))
                    {
                        List<UsuarioCSI> list = new List<UsuarioCSI>();
                        connection.Open();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                UsuarioCSI item = new UsuarioCSI
                                {
                                    idUsuario = (int)reader["idUsuario"],
                                    alias = (string)reader["alias"],
                                    nombreUsuario = (string)reader["nombreUsuario"],
                                    contraseña = (string)reader["contrase\x00f1a"]
                                };
                                list.Add(item);
                            }
                        }
                        result = new OperationResult(command, list);
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult(exception);
            }
            return result;
        }

        public OperationResult saveUsuarioCSI(ref UsuarioCSI usuarioCSI)
        {
            OperationResult result;
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory(false, ""))
                {
                    connection.Open();
                    using (SqlTransaction transaction = connection.BeginTransaction())
                    {
                        using (SqlCommand command = CrearSqlCommand.getCommand(connection, "sp_saveUsuarioCSI", true))
                        {
                            command.Transaction = transaction;
                            command.Parameters.Add(new SqlParameter("@idUsuario", usuarioCSI.idUsuario));
                            command.Parameters.Add(new SqlParameter("@alias", usuarioCSI.alias));
                            command.Parameters.Add(new SqlParameter("@nombreUsuario", usuarioCSI.nombreUsuario));
                            command.Parameters.Add(new SqlParameter("@contrase\x00f1a", usuarioCSI.contraseña));
                            command.ExecuteNonQuery();
                            int id = 0;
                            if (CrearSqlCommand.validarCorrecto(command, out id))
                            {
                                usuarioCSI.idUsuario = id;
                                transaction.Commit();
                            }
                            else
                            {
                                transaction.Rollback();
                            }
                            result = new OperationResult(command, usuarioCSI);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult(exception);
            }
            return result;
        }
    }
}
