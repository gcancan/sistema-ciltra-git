﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using Core.Utils;
using System.Data;
using System.Data.SqlClient;
using Core.BusinessLogic;
namespace CoreFletera.BusinessLogic
{
    public class MantenimientoServices
    {
        public OperationResult getMantenimientoByFiltros(DateTime fechaInicio, DateTime fechaFin, List<string> listaUnidades, string tipoOrdenSer = "%", string tipoServicio = "%")
        {
            try
            {
                string xml = GenerateXML.generarListaUnidades(listaUnidades);
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand command = CrearSqlCommand.getCommand(con, "sp_getMantenimientoByFiltros"))
                    {
                        command.Parameters.Add(new SqlParameter("@fechaInicio", fechaInicio));
                        command.Parameters.Add(new SqlParameter("@fechaFin", fechaFin));
                        command.Parameters.Add(new SqlParameter("@xml", xml));
                        command.Parameters.Add(new SqlParameter("@tipoOrdenSer", tipoOrdenSer));
                        command.Parameters.Add(new SqlParameter("@tipoServicio", tipoServicio));
                        List<Mantenimiento> lisMatto = new List<Mantenimiento>();
                        con.Open();
                        using (SqlDataReader sqlReader = command.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                Mantenimiento matto = new mapComunes().mapMantenimiento(sqlReader);
                                if (matto == null)
                                {
                                    return new OperationResult() { valor = 1, mensaje = "OCURRIO UN ERROR AL MAPEAR LOS RESULTADOS DEL MANTENIMIENTO" };
                                }
                                lisMatto.Add(matto);
                            }
                        }

                        command.CommandText = "sp_getActividadesMantenimientoByFiltros";
                        List<DetalleMantenimiento> lisDetMatto = new List<DetalleMantenimiento>();
                        using (SqlDataReader sqlReader = command.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                DetalleMantenimiento detalle = new mapComunes().mapDetMantenimiento(sqlReader);
                                if (detalle == null)
                                {
                                    return new OperationResult() { valor = 1, mensaje = "OCURRIO UN ERROR AL MAPEAR LOS RESULTADOS DE LAS ACTIVIDADES DEL MANTENIMIENTO" };
                                }
                                lisDetMatto.Add(detalle);
                            }
                        }
                        //var sum = lisDetMatto.Where(s => s.idActividad == 34).Sum(s => s.idActividad);                        

                        command.CommandText = "sp_getInsumosMantenimientoByFiltros";
                        List<InsumosOrdenTrabajo> lisInsumos = new List<InsumosOrdenTrabajo>();
                        using (SqlDataReader sqlReader = command.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                InsumosOrdenTrabajo insumo = new mapComunes().mapInsumosOrdenTrabajo(sqlReader);
                                if (insumo == null)
                                {
                                    return new OperationResult() { valor = 1, mensaje = "OCURRIO UN ERROR AL MAPEAR LOS RESULTADOS DE LOS INSUMOS DEL MANTENIMIENTO" };
                                }
                                lisInsumos.Add(insumo);
                            }
                        }

                        foreach (var insumo in lisInsumos)
                        {
                            insumo.sumaGlobal = lisInsumos.Where(s => s.idProducto == insumo.idProducto).Sum(s => s.cantidad);
                        }

                        foreach (var item in lisDetMatto)
                        {
                            item.listaInsumos = lisInsumos.FindAll(s => s.idOrdenTrabajo == item.idOrdenTrabajo);
                        }
                        foreach (var MTTO in lisMatto)
                        {
                            MTTO.listaActividades = lisDetMatto.FindAll(s => s.idOrdenSer == MTTO.idOrdenSer);
                        }

                        return new OperationResult(command, lisMatto);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
}
