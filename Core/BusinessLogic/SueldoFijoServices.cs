﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.BusinessLogic;
using Core.Utils;
using System.Data;
using System.Data.SqlClient;
using Core.BusinessLogic;

namespace CoreFletera.BusinessLogic
{
    public class SueldoFijoServices
    {
        public OperationResult saveSueldoFijo(ref SueldoFijo sueldo)
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    con.Open();
                    using (SqlTransaction tran = con.BeginTransaction())
                    {
                        using (SqlCommand comand = CrearSqlCommand.getCommand(con, "sp_saveSueldosFijos", true))
                        {
                            comand.Transaction = tran;
                            comand.Parameters.Add(new SqlParameter("@idSueldoFijo", sueldo.idSueldoFijo));
                            comand.Parameters.Add(new SqlParameter("@idOperador", sueldo.operador.clave));
                            comand.Parameters.Add(new SqlParameter("@importe", sueldo.importe));
                            comand.Parameters.Add(new SqlParameter("@autoriza", sueldo.autoriza));
                            comand.Parameters.Add(new SqlParameter("@estatus", sueldo.estatus));
                            comand.ExecuteNonQuery();

                            if ((int)comand.Parameters["@valor"].Value == 0)
                            {
                                sueldo.idSueldoFijo = (int)comand.Parameters["@id"].Value;
                                tran.Commit();
                            }
                            else
                            {
                                tran.Rollback();
                            }
                            return new OperationResult(comand);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        public OperationResult getAllSueldoFijoOrByIdOperador(string idOperador = "")
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comand = CrearSqlCommand.getCommand(con, "sp_getAllSueldoFijoOrByIdOperador"))
                    {
                        comand.Parameters.Add(new SqlParameter("@idOperador", string.IsNullOrEmpty(idOperador) ? "%" : idOperador));
                        con.Open();
                        List<SueldoFijo> lista = new List<SueldoFijo>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                SueldoFijo sueldo = new mapComunes().mapSueldoFijo(sqlReader);
                                if (sueldo == null)
                                {
                                    return new OperationResult { valor = 1, mensaje = "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA" };
                                }
                                lista.Add(sueldo);
                            }
                        }
                        if ((int)comand.Parameters["@valor"].Value == 0)
                        {
                            if (string.IsNullOrEmpty(idOperador))
                            {
                                return new OperationResult(comand, lista);
                            }
                            else
                            {
                                return new OperationResult(comand, lista[0]);
                            }
                        }
                        else
                        {
                            return new OperationResult(comand, lista);
                        }
                        
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
}
