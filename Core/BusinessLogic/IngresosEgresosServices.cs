﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using Core.Utils;
using System.Data;
using System.Data.SqlClient;
namespace CoreFletera.BusinessLogic
{
    public class IngresosEgresosServices
    {
        public OperationResult getIngresos(string idUnidad, DateTime fechaInicial, DateTime fechaFinal)
        {
            try
            {
                using (SqlConnection conection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comand = conection.CreateCommand())
                    {
                        comand.CommandType = CommandType.StoredProcedure;
                        comand.CommandText = "sp_getIngresos";

                        comand.Parameters.Add(new SqlParameter("@idUnidad", idUnidad));
                        comand.Parameters.Add(new SqlParameter("@fechaInicial", fechaInicial));
                        comand.Parameters.Add(new SqlParameter("@fechaFinal", fechaFinal));

                        var spValor = comand.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var spMensaje = comand.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                        List<Ingresos> lisIngresos = new List<Ingresos>();
                        conection.Open();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                lisIngresos.Add(new Ingresos
                                {
                                    concepto = (string)sqlReader["concepto"],
                                    total = (decimal)sqlReader["total"]
                                });
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = lisIngresos };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message, result = null };
            }
        }

        public OperationResult getIngresosDetalles(string idUnidad, DateTime fechaInicial, DateTime fechaFinal)
        {
            try
            {
                using (SqlConnection conection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comand = conection.CreateCommand())
                    {
                        comand.CommandType = CommandType.StoredProcedure;
                        comand.CommandText = "sp_getIngresosDetallado";

                        comand.Parameters.Add(new SqlParameter("@idUnidad", idUnidad));
                        comand.Parameters.Add(new SqlParameter("@fechaInicial", fechaInicial));
                        comand.Parameters.Add(new SqlParameter("@fechaFinal", fechaFinal));

                        var spValor = comand.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var spMensaje = comand.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                        List<IngresosDetalle> lisIngresosDetalle = new List<IngresosDetalle>();
                        conection.Open();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                lisIngresosDetalle.Add(new IngresosDetalle
                                {
                                    fecha = (DateTime)sqlReader["fecha"],
                                    serie = (string)sqlReader["NoSerie"],
                                    movimiento = (int)sqlReader["IdMovimiento"],
                                    cliente = (string)sqlReader["cliente"],
                                    cantidad = (decimal)sqlReader["cantidad"],
                                    um = (string)sqlReader["um"],
                                    importe = (decimal)sqlReader["importe"],
                                    concepto = (string)sqlReader["concepto"]
                                });
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = lisIngresosDetalle };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message, result = null };
            }
        }

        public OperationResult getEgresos(string idUnidad, DateTime fechaInicial, DateTime fechaFinal)
        {
            try
            {
                using (SqlConnection conection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comand = conection.CreateCommand())
                    {
                        comand.CommandType = CommandType.StoredProcedure;
                        comand.CommandText = "sp_getEgresos";

                        comand.Parameters.Add(new SqlParameter("@idUnidad", idUnidad));
                        comand.Parameters.Add(new SqlParameter("@fechaInicial", fechaInicial));
                        comand.Parameters.Add(new SqlParameter("@fechaFinal", fechaFinal));

                        var spValor = comand.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var spMensaje = comand.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                        List<Egresos> lisEgresos = new List<Egresos>();
                        conection.Open();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                lisEgresos.Add(new Egresos
                                {
                                    concepto = (string)sqlReader["concepto"],
                                    total = (decimal)sqlReader["total"]
                                });
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = lisEgresos };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message, result = null };
            }
        }

        public OperationResult getEgresosDetallado(string idUnidad, DateTime fechaInicial, DateTime fechaFinal)
        {
            try
            {
                using (SqlConnection conection = BoundedContextFactory.ConnectionFactory())
                {
                    using (SqlCommand comand = conection.CreateCommand())
                    {
                        comand.CommandType = CommandType.StoredProcedure;
                        comand.CommandText = "sp_getEgresosDetallado";

                        comand.Parameters.Add(new SqlParameter("@idUnidad", idUnidad));
                        comand.Parameters.Add(new SqlParameter("@fechaInicial", fechaInicial));
                        comand.Parameters.Add(new SqlParameter("@fechaFinal", fechaFinal));

                        var spValor = comand.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var spMensaje = comand.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                        List<EgresosDetalle> lisEgresos = new List<EgresosDetalle>();
                        conection.Open();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                lisEgresos.Add(new EgresosDetalle
                                {
                                    fecha = (DateTime)sqlReader["fecha"],
                                    ordenSericio = (int)sqlReader["ordenServicio"],
                                    folio = (int)sqlReader["folio"],
                                    cadenaFolio = (string)sqlReader["cadenaFolio"],
                                    persona = (string)sqlReader["persona"],
                                    cantidad = (decimal)sqlReader["cantidad"],
                                    um = (string)sqlReader["um"],
                                    total = (decimal)sqlReader["total"],
                                    concepto = (string)sqlReader["concepto"],
                                });
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = lisEgresos };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message, result = null };
            }
        }

        public OperationResult getIngresosEgresos(DateTime fechaInicial, DateTime fechaFinal, int idEmpresa, List<string> listaUnidades)
        {
            try
            {
                using (SqlConnection conection = BoundedContextFactory.ConnectionFactory())
                {
                    string xml = GenerateXML.generarListaUnidades(listaUnidades);
                    if (xml == null)
                    {
                        return new OperationResult { valor = 1, mensaje = "Error en el XML" };
                    }
                    using (SqlCommand comand = conection.CreateCommand())
                    {
                        comand.CommandType = CommandType.StoredProcedure;
                        comand.CommandText = "sp_getIngresosEgresos";

                        comand.Parameters.Add(new SqlParameter("@fechaInicial", fechaInicial));
                        comand.Parameters.Add(new SqlParameter("@fechaFinal", fechaFinal));
                        comand.Parameters.Add(new SqlParameter("@xml", xml));
                        comand.Parameters.Add(new SqlParameter("@idEmpresa", idEmpresa));
                        comand.Parameters.Add(new SqlParameter("@lista", listaUnidades.Count == 0 ? 0 : 1));

                        var spValor = comand.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var spMensaje = comand.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                        List<IngresosEgresos> listIngresosEgresos = new List<IngresosEgresos>();
                        conection.Open();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                listIngresosEgresos.Add(new IngresosEgresos
                                {
                                    unidad = (string)sqlReader["idUnidadTrans"],
                                    fletes = (decimal)sqlReader["fletes"],
                                    combustible = (decimal)sqlReader["combustible"],
                                    mantenimiento = (decimal)sqlReader["mantenimiento"],
                                    llantas = (decimal)sqlReader["llantas"],
                                    lavadero = (decimal)sqlReader["lavadero"],
                                    utilidad = (decimal)sqlReader["utilidad"]
                                });
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = listIngresosEgresos };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message, result = null };
            }
        }
    }
}
