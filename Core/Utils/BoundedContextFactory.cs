﻿namespace Core.Utils
{
    using System;
    using System.Data.SqlClient;
    using System.Runtime.InteropServices;

    public static class BoundedContextFactory
    {
        public static SqlConnection ConnectionFactory(bool isComercial = false, string empresa = "") =>
            new SqlConnection(getConnectionString(isComercial, empresa));
        public static SqlConnection ConnectionFactoryINTELISIS() =>
            new SqlConnection(getConnectionStringINTELISIS());

        public static SqlConnection ConnectionFactoryCOMCONF() =>
            new SqlConnection(getConnectionStringCOMCONF());

        public static SqlConnection ConnectionFactoryCompacWAdmin()
        {
            SqlConnectionStringBuilder builder = new Util().getConectionStringComercial("");
            builder.InitialCatalog = "CompacWAdmin";
            return new SqlConnection(builder.ToString());
        }

        public static SqlConnection ConnectionFactoryTaller(bool isComercial = false) =>
            new SqlConnection(getConnectionStringTaller());

        private static string getConnectionString(bool isComercial, string empresa = "")
        {
            if (isComercial)
            {
                return new Util().getConectionStringComercial(empresa).ToString();
            }
            if (!isComercial)
            {
                return new Util().getConectionString().ToString();
            }
            return "";
        }

        private static string getConnectionStringINTELISIS()
        {
            return new Util().getConectionStringINTELISIS().ToString();           
        }

        private static string getConnectionStringCOMCONF() =>
            new Util().getConectionStringCOMCONF().ToString();

        private static string getConnectionStringTaller() =>
            new Util().getConectionStringTaller().ToString();
    }
}
