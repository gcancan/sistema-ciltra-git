﻿namespace Core.Utils
{
    using System;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.IO;
    using System.IO.Ports;
    using System.Net;
    using System.Net.Mail;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using System.Text;
    using System.Windows.Forms;

    public class Util
    {
        //[CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        //private object <Application>k__BackingField;

        private void createFileINI(string archivo)
        {
            try
            {
                using (FileStream stream = System.IO.File.Create(archivo))
                {
                    byte[] bytes = new UTF8Encoding(true).GetBytes("");
                    stream.Write(bytes, 0, bytes.Length);
                    stream.Flush();
                }
                this.Write("CONECTION_STRING", "SERVER_NAME", @"198.38.85.199\SQLEXPRESS", archivo);
                this.Write("CONECTION_STRING", "DB_NAME", "DBFletera", archivo);
                this.Write("CONECTION_STRING", "USER", "ciltra", archivo);
                this.Write("CONECTION_STRING", "PASSWORD", "2019!ciltra", archivo);
                this.Write("PUERTO COM", "COM", "COM3", archivo);
                this.Write("PUERTO COM", "BAUD_RADE", "9600", archivo);
                this.Write("PUERTO COM", "DATA_BITS", "8", archivo);
                this.Write("LAPSO TIEMPO", "TIEMPO", "15", archivo);
                this.Write("CONECTION_STRING_COMERCIAL", "SERVER_NAME", @"198.38.85.199\SQLEXPRESS", archivo);
                this.Write("CONECTION_STRING_COMERCIAL", "DB_NAME", "adPegaso", archivo);
                this.Write("CONECTION_STRING_COMERCIAL", "USER", "prueba", archivo);
                this.Write("CONECTION_STRING_COMERCIAL", "PASSWORD", "p54321", archivo);
                this.Write("CONECTION_STRING_COMERCIAL_ATLAS", "SERVER_NAME", @"192.168.99.5\COMERCIAL", archivo);
                this.Write("CONECTION_STRING_COMERCIAL_ATLAS", "DB_NAME", "adPrueba", archivo);
                this.Write("CONECTION_STRING_COMERCIAL_ATLAS", "USER", "sa", archivo);
                this.Write("CONECTION_STRING_COMERCIAL_ATLAS", "PASSWORD", "Comercial2017", archivo);

                this.Write("CONECTION_STRING_TALLER", "SERVER_NAME", @"192.168.99.5\COMERCIAL", archivo);
                this.Write("CONECTION_STRING_TALLER", "DB_NAME", "adPrueba", archivo);
                this.Write("CONECTION_STRING_TALLER", "USER", "sa", archivo);
                this.Write("CONECTION_STRING_TALLER", "PASSWORD", "Comercial2017", archivo);

                this.Write("CONECTION_COMERCIAL_CONF", "SERVER_NAME", @"192.168.99.5\COMERCIAL", archivo);
                this.Write("CONECTION_COMERCIAL_CONF", "DB_NAME", "CompacWAdmin", archivo);
                this.Write("CONECTION_COMERCIAL_CONF", "USER", "sa", archivo);
                this.Write("CONECTION_COMERCIAL_CONF", "PASSWORD", "Comercial2017", archivo);

                this.Write("RUTA_XML", "RUTA", System.Windows.Forms.Application.StartupPath + @"\43.xml", archivo);
                this.Write("CONFIGURACION_CORREO", "HOST", "smtp.outlook.office365.com", archivo);
                this.Write("CONFIGURACION_CORREO", "PUERTO", "587", archivo);
                this.Write("CONFIGURACION_CORREO", "USUARIO", "notificaciones@fleterapegaso.com", archivo);
                this.Write("CONFIGURACION_CORREO", "PASS", "Telmex.2016", archivo);
                this.Write("CONFIGURACION_CORREO", "SSL", "true", archivo);
                this.Write("CORREO_CSI", "CORREO", "monitoreo@csi.com.mx,supervisormerida@csi.com.mx,monitoristamas2@csi.com.mx", archivo);
                this.Write("PERMISOS_CORREO_CSI", "CSI", "true", archivo);
                this.Write("PERMISOS_CORREO", "RUTAS", "true", archivo);
                this.Write("IMPRIMIR", "ACTIVO", "true", archivo);
                this.Write("ANTENA", "ACTIVO", "false", archivo);
                this.Write("USUARIO", "ULTIMO_USUARIO", "", archivo);
                this.Write("ACTIVAR_EVENTO_TABLERO", "ACTIVO", "true", archivo);

                this.Write("CONECTION_STRING_INTELISIS", "SERVER_NAME", @"192.168.99.20", archivo);
                this.Write("CONECTION_STRING_INTELISIS", "DB_NAME", "PRUEBAS", archivo);
                this.Write("CONECTION_STRING_INTELISIS", "USER", "intelisis", archivo);
                this.Write("CONECTION_STRING_INTELISIS", "PASSWORD", "Ciltra2019$", archivo);
            }
            catch (Exception)
            {
            }
        }

        private void createNodo(eNodo nodo, string archivo)
        {
            switch (nodo)
            {
                case eNodo.CONECTION:
                    this.Write("CONECTION_STRING", "SERVER_NAME", @"198.38.85.199\SQLEXPRESS", archivo);
                    this.Write("CONECTION_STRING", "DB_NAME", "DBFletera", archivo);
                    this.Write("CONECTION_STRING", "USER", "ciltra", archivo);
                    this.Write("CONECTION_STRING", "PASSWORD", "2019!ciltra", archivo);
                    break;

                case eNodo.CONECTION_INTELISIS:
                    this.Write("CONECTION_STRING_INTELISIS", "SERVER_NAME", @"192.168.99.20", archivo);
                    this.Write("CONECTION_STRING_INTELISIS", "DB_NAME", "PRUEBAS", archivo);
                    this.Write("CONECTION_STRING_INTELISIS", "USER", "intelisis", archivo);
                    this.Write("CONECTION_STRING_INTELISIS", "PASSWORD", "Ciltra2019$", archivo);
                    break;

                case eNodo.CONECTION_COMERCIAL:
                    this.Write("CONECTION_STRING_COMERCIAL", "SERVER_NAME", @"198.38.85.199\SQLEXPRESS", archivo);
                    this.Write("CONECTION_STRING_COMERCIAL", "DB_NAME", "adPegaso", archivo);
                    this.Write("CONECTION_STRING_COMERCIAL", "USER", "prueba", archivo);
                    this.Write("CONECTION_STRING_COMERCIAL", "PASSWORD", "p54321", archivo);
                    break;

                case eNodo.PUERTOS:
                    this.Write("PUERTO COM", "COM", "COM3", archivo);
                    this.Write("PUERTO COM", "BAUD_RADE", "9600", archivo);
                    this.Write("PUERTO COM", "DATA_BITS", "8", archivo);
                    break;

                case eNodo.LAPSO_TIEMPO:
                    this.Write("LAPSO TIEMPO", "TIEMPO", "15", archivo);
                    break;

                case eNodo.RUTA_XML:
                    this.Write("RUTA_XML", "RUTA", System.Windows.Forms.Application.StartupPath + @"\43.xml", archivo);
                    break;

                case eNodo.CONFIGURACION_CORREO:
                    this.Write("CONFIGURACION_CORREO", "HOST", "smtp.outlook.office365.com", archivo);
                    this.Write("CONFIGURACION_CORREO", "PUERTO", "587", archivo);
                    this.Write("CONFIGURACION_CORREO", "USUARIO", "notificaciones@fleterapegaso.com", archivo);
                    this.Write("CONFIGURACION_CORREO", "PASS", "Telmex.2016", archivo);
                    this.Write("CONFIGURACION_CORREO", "SSL", "true", archivo);
                    break;

                case eNodo.ENVIO_CORREO_CSI:
                    this.Write("CORREO_CSI", "CORREO", "monitoreo@csi.com.mx,supervisormerida@csi.com.mx,monitoristamas2@csi.com.mx", archivo);
                    break;

                case eNodo.PERMISOS_CORREO_CSI:
                    this.Write("PERMISOS_CORREO_CSI", "CSI", "true", archivo);
                    break;

                case eNodo.PERMISOS_CORREO_GRANJAS:
                    this.Write("PERMISOS_CORREO", "RUTAS", "false", archivo);
                    break;

                case eNodo.ANTENA:
                    this.Write("ANTENA", "ACTIVO", "false", archivo);
                    break;

                case eNodo.PERMISO_IMPRIMIR:
                    this.Write("IMPRIMIR", "ACTIVO", "true", archivo);
                    break;

                case eNodo.CONECTION_TALLER:
                    this.Write("CONECTION_STRING_TALLER", "SERVER_NAME", @"198.38.85.199\SQLEXPRESS", archivo);
                    this.Write("CONECTION_STRING_TALLER", "DB_NAME", "adPrueba", archivo);
                    this.Write("CONECTION_STRING_TALLER", "USER", "prueba", archivo);
                    this.Write("CONECTION_STRING_TALLER", "PASSWORD", "p54321", archivo);
                    this.Write("CONECTION_STRING_COMERCIAL_ATLAS", "SERVER_NAME", @"198.38.85.199\SQLEXPRESS", archivo);
                    this.Write("CONECTION_STRING_COMERCIAL_ATLAS", "DB_NAME", "adPrueba", archivo);
                    this.Write("CONECTION_STRING_COMERCIAL_ATLAS", "USER", "prueba", archivo);
                    this.Write("CONECTION_STRING_COMERCIAL_ATLAS", "PASSWORD", "p54321", archivo);
                    break;

                case eNodo.CONECTION_COMERCIAL_ATLAS:
                    this.Write("CONECTION_STRING_COMERCIAL_ATLAS", "SERVER_NAME", @"192.168.99.5\COMERCIAL", archivo);
                    this.Write("CONECTION_STRING_COMERCIAL_ATLAS", "DB_NAME", "adPrueba", archivo);
                    this.Write("CONECTION_STRING_COMERCIAL_ATLAS", "USER", "sa", archivo);
                    this.Write("CONECTION_STRING_COMERCIAL_ATLAS", "PASSWORD", "Comercial2017", archivo);
                    this.Write("CONECTION_STRING_TALLER", "SERVER_NAME", @"192.168.99.5\COMERCIAL", archivo);
                    this.Write("CONECTION_STRING_TALLER", "DB_NAME", "adPrueba", archivo);
                    this.Write("CONECTION_STRING_TALLER", "USER", "sa", archivo);
                    this.Write("CONECTION_STRING_TALLER", "PASSWORD", "Comercial2017", archivo);
                    break;

                case eNodo.CONECTION_COMCONFIG:
                    this.Write("CONECTION_COMERCIAL_CONF", "SERVER_NAME", @"198.38.85.199\SQLEXPRESS", archivo);
                    this.Write("CONECTION_COMERCIAL_CONF", "DB_NAME", "CompacWAdmin", archivo);
                    this.Write("CONECTION_COMERCIAL_CONF", "USER", "prueba", archivo);
                    this.Write("CONECTION_COMERCIAL_CONF", "PASSWORD", "p54321", archivo);
                    break;
            }
        }

        public SqlConnectionStringBuilder getConectionString()
        {
            string path = System.Windows.Forms.Application.StartupPath + @"\config.ini";
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            try
            {
                if (System.IO.File.Exists(path))
                {
                    builder.DataSource = this.Read("CONECTION_STRING", "SERVER_NAME", path);
                    builder.InitialCatalog = this.Read("CONECTION_STRING", "DB_NAME", path);
                    builder.UserID = this.Read("CONECTION_STRING", "USER", path);
                    builder.Password = this.Read("CONECTION_STRING", "PASSWORD", path);
                    builder.ConnectTimeout = 3800;
                    if (builder.ConnectionString == "Data Source=;Initial Catalog=;User ID=;Password=")
                    {
                        this.createNodo(eNodo.CONECTION, path);
                        return this.getConectionString();
                    }
                    return builder;
                }
                this.createFileINI(path);
                builder = this.getConectionString();
            }
            catch (Exception)
            {
                this.createNodo(eNodo.CONECTION, path);
                return this.getConectionString();
            }
            return builder;
        }
        public SqlConnectionStringBuilder getConectionStringINTELISIS()
        {
            string path = System.Windows.Forms.Application.StartupPath + @"\config.ini";
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            try
            {
                if (System.IO.File.Exists(path))
                {
                    builder.DataSource = this.Read("CONECTION_STRING_INTELISIS", "SERVER_NAME", path);
                    builder.InitialCatalog = this.Read("CONECTION_STRING_INTELISIS", "DB_NAME", path);
                    builder.UserID = this.Read("CONECTION_STRING_INTELISIS", "USER", path);
                    builder.Password = this.Read("CONECTION_STRING_INTELISIS", "PASSWORD", path);
                    //builder.ConnectTimeout = 3800;
                    if (builder.ConnectionString == "Data Source=;Initial Catalog=;User ID=;Password=")
                    {
                        this.createNodo(eNodo.CONECTION_INTELISIS, path);
                        return this.getConectionString();
                    }
                    return builder;
                }
                this.createFileINI(path);
                builder = this.getConectionStringINTELISIS();
            }
            catch (Exception)
            {
                this.createNodo(eNodo.CONECTION_INTELISIS, path);
                return this.getConectionStringINTELISIS();
            }
            return builder;
        }

        public SqlConnectionStringBuilder getConectionStringCOMCONF()
        {
            string path = System.Windows.Forms.Application.StartupPath + @"\config.ini";
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            try
            {
                if (System.IO.File.Exists(path))
                {
                    builder.DataSource = this.Read("CONECTION_COMERCIAL_CONF", "SERVER_NAME", path);
                    builder.InitialCatalog = this.Read("CONECTION_COMERCIAL_CONF", "DB_NAME", path);
                    builder.UserID = this.Read("CONECTION_COMERCIAL_CONF", "USER", path);
                    builder.Password = this.Read("CONECTION_COMERCIAL_CONF", "PASSWORD", path);
                    if (builder.ConnectionString == "Data Source=;Initial Catalog=;User ID=;Password=")
                    {
                        this.createNodo(eNodo.CONECTION_COMCONFIG, path);
                        return this.getConectionString();
                    }
                    return builder;
                }
                this.createFileINI(path);
                builder = this.getConectionString();
            }
            catch (Exception)
            {
                this.createNodo(eNodo.CONECTION, path);
                return this.getConectionString();
            }
            return builder;
        }

        public SqlConnectionStringBuilder getConectionStringComercial(string empresa = "")
        {
            string path = System.Windows.Forms.Application.StartupPath + @"\config.ini";
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            try
            {
                if (System.IO.File.Exists(path))
                {
                    builder.DataSource = this.Read("CONECTION_STRING_COMERCIAL", "SERVER_NAME", path);
                    builder.InitialCatalog = string.IsNullOrEmpty(empresa) ? this.Read("CONECTION_STRING_COMERCIAL", "DB_NAME", path) : empresa;
                    builder.UserID = this.Read("CONECTION_STRING_COMERCIAL", "USER", path);
                    builder.Password = this.Read("CONECTION_STRING_COMERCIAL", "PASSWORD", path);
                    if (builder.ConnectionString == "Data Source=;Initial Catalog=;User ID=;Password=")
                    {
                        this.createNodo(eNodo.CONECTION_COMERCIAL, path);
                        return this.getConectionStringComercial("");
                    }
                    return builder;
                }
                this.createFileINI(path);
                builder = this.getConectionStringComercial("");
            }
            catch (Exception)
            {
                this.createNodo(eNodo.CONECTION_COMERCIAL, path);
                return this.getConectionStringComercial("");
            }
            return builder;
        }

        public SqlConnectionStringBuilder getConectionStringComercialATLAS(string empresa = "")
        {
            string path = System.Windows.Forms.Application.StartupPath + @"\config.ini";
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            try
            {
                if (System.IO.File.Exists(path))
                {
                    builder.DataSource = this.Read("CONECTION_STRING_TALLER", "SERVER_NAME", path);
                    builder.InitialCatalog = string.IsNullOrEmpty(empresa) ? this.Read("CONECTION_STRING_TALLER", "DB_NAME", path) : empresa;
                    builder.UserID = this.Read("CONECTION_STRING_TALLER", "USER", path);
                    builder.Password = this.Read("CONECTION_STRING_TALLER", "PASSWORD", path);
                    if (builder.ConnectionString == "Data Source=;Initial Catalog=;User ID=;Password=")
                    {
                        this.createNodo(eNodo.CONECTION_TALLER, path);
                        return this.getConectionStringComercialATLAS("");
                    }
                    return builder;
                }
                this.createFileINI(path);
                builder = this.getConectionStringComercialATLAS("");
            }
            catch (Exception)
            {
                this.createNodo(eNodo.CONECTION_COMERCIAL, path);
                return this.getConectionStringComercial("");
            }
            return builder;
        }

        public SqlConnectionStringBuilder getConectionStringTaller()
        {
            string path = System.Windows.Forms.Application.StartupPath + @"\config.ini";
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            try
            {
                if (System.IO.File.Exists(path))
                {
                    builder.DataSource = this.Read("CONECTION_STRING_TALLER", "SERVER_NAME", path);
                    builder.InitialCatalog = this.Read("CONECTION_STRING_TALLER", "DB_NAME", path);
                    builder.UserID = this.Read("CONECTION_STRING_TALLER", "USER", path);
                    builder.Password = this.Read("CONECTION_STRING_TALLER", "PASSWORD", path);
                    if (builder.ConnectionString == "Data Source=;Initial Catalog=;User ID=;Password=")
                    {
                        this.createNodo(eNodo.CONECTION_TALLER, path);
                        return this.getConectionStringTaller();
                    }
                    return builder;
                }
                this.createFileINI(path);
                builder = this.getConectionStringTaller();
            }
            catch (Exception)
            {
                this.createNodo(eNodo.CONECTION_TALLER, path);
                return this.getConectionStringTaller();
            }
            return builder;
        }

        public string getCorreoCSI()
        {
            try
            {
                string path = System.Windows.Forms.Application.StartupPath + @"\config.ini";
                if (System.IO.File.Exists(path))
                {
                    string str3 = this.Read("CORREO_CSI", "CORREO", path);
                    if (string.IsNullOrEmpty(str3))
                    {
                        this.createNodo(eNodo.ENVIO_CORREO_CSI, path);
                        return this.getCorreoCSI();
                    }
                    return str3;
                }
                this.createFileINI(path);
                this.getCorreoCSI();
            }
            catch (Exception)
            {
                return null;
            }
            return null;
        }

        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string Section, string Key, string Default, StringBuilder RetVal, int Size, string FilePath);
        public string getRutasImpresora(string configuracion)
        {
            try
            {
                string path = System.Windows.Forms.Application.StartupPath + @"\config.ini";
                if (System.IO.File.Exists(path))
                {
                    return this.Read("RUTAS_IMPRESORAS", configuracion.ToUpper(), path);
                }
                this.createFileINI(path);
                return this.getRutasImpresora(configuracion);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public string getRutaXML()
        {
            try
            {
                string path = System.Windows.Forms.Application.StartupPath + @"\config.ini";
                if (System.IO.File.Exists(path))
                {
                    if (string.IsNullOrEmpty(this.Read("RUTA_XML", "RUTA", path)))
                    {
                        this.createNodo(eNodo.RUTA_XML, path);
                        return "";
                    }
                    return Convert.ToString(this.Read("RUTA_XML", "RUTA", path));
                }
                this.createFileINI(path);
                this.getRutaXML();
            }
            catch (Exception)
            {
                return null;
            }
            return "";
        }

        public SerialPort getSerialPort()
        {
            string path = System.Windows.Forms.Application.StartupPath + @"\config.ini";
            SerialPort port = new SerialPort();
            try
            {
                if (System.IO.File.Exists(path))
                {
                    port.PortName = this.Read("PUERTO COM", "COM", path);
                    port.BaudRate = Convert.ToInt32(this.Read("PUERTO COM", "BAUD_RADE", path));
                    port.DataBits = Convert.ToInt32(this.Read("PUERTO COM", "DATA_BITS", path));
                    port.Parity = Parity.None;
                    port.StopBits = StopBits.One;
                    port.ReadTimeout = 300;
                    port.Close();
                    return port;
                }
                this.createFileINI(path);
                port = this.getSerialPort();
            }
            catch (Exception)
            {
                this.createNodo(eNodo.PUERTOS, path);
                this.getSerialPort();
            }
            return port;
        }

        public SmtpClient getSmtpClient()
        {
            string path = System.Windows.Forms.Application.StartupPath + @"\config.ini";
            try
            {
                if (System.IO.File.Exists(path))
                {
                    return new SmtpClient
                    {
                        Host = this.Read("CONFIGURACION_CORREO", "HOST", path),
                        Port = Convert.ToInt32(this.Read("CONFIGURACION_CORREO", "PUERTO", path)),
                        EnableSsl = Convert.ToBoolean(this.Read("CONFIGURACION_CORREO", "SSL", path)),
                        Credentials = new NetworkCredential(this.Read("CONFIGURACION_CORREO", "USUARIO", path), this.Read("CONFIGURACION_CORREO", "PASS", path))
                    };
                }
                this.createFileINI(path);
                this.getSmtpClient();
            }
            catch (Exception)
            {
                this.createNodo(eNodo.CONFIGURACION_CORREO, path);
                return this.getSmtpClient();
            }
            return null;
        }

        public SmtpClient getSmtpClientEnvioCorreos()
        {
            string path = System.Windows.Forms.Application.StartupPath + @"\config.ini";
            try
            {
                if (System.IO.File.Exists(path))
                {
                    return new SmtpClient
                    {
                        Host = "ciltra.com",//this.Read("CONFIGURACION_CORREO", "HOST", path),
                        Port = 587,//Convert.ToInt32(this.Read("CONFIGURACION_CORREO", "PUERTO", path)),
                        EnableSsl = true,// Convert.ToBoolean(this.Read("CONFIGURACION_CORREO", "SSL", path)),
                        Credentials = new NetworkCredential("notificaciones@ciltra.com", "Dzununcan2019!")//  new NetworkCredential(this.Read("CONFIGURACION_CORREO", "USUARIO", path), this.Read("CONFIGURACION_CORREO", "PASS", path))
                    };
                }
                this.createFileINI(path);
                this.getSmtpClient();
            }
            catch (Exception)
            {
                this.createNodo(eNodo.CONFIGURACION_CORREO, path);
                return this.getSmtpClient();
            }
            return null;
        }

        public int getTiempoEspera()
        {
            try
            {
                string path = System.Windows.Forms.Application.StartupPath + @"\config.ini";
                if (System.IO.File.Exists(path))
                {
                    string str3 = this.Read("LAPSO TIEMPO", "TIEMPO", path);
                    if (string.IsNullOrEmpty(str3))
                    {
                        this.createNodo(eNodo.LAPSO_TIEMPO, path);
                    }
                    return Convert.ToInt32(str3);
                }
                this.createFileINI(path);
                this.getTiempoEspera();
            }
            catch (Exception)
            {
                return 15;
            }
            return 15;
        }

        public string getUltimoUsuario()
        {
            try
            {
                string path = System.Windows.Forms.Application.StartupPath + @"\config.ini";
                if (System.IO.File.Exists(path))
                {
                    return this.Read("USUARIO", "ULTIMO_USUARIO", path);
                }
                this.createFileINI(path);
                this.getRutaXML();
            }
            catch (Exception)
            {
                return null;
            }
            return "";
        }

        public bool isActivoAntena()
        {
            try
            {
                string path = System.Windows.Forms.Application.StartupPath + @"\config.ini";
                if (System.IO.File.Exists(path))
                {
                    string str3 = this.Read("ANTENA", "ACTIVO", path);
                    if (string.IsNullOrEmpty(str3))
                    {
                        this.createNodo(eNodo.ANTENA, path);
                        return this.isActivoAntena();
                    }
                    return Convert.ToBoolean(str3.ToLower());
                }
                this.createFileINI(path);
                this.isActivoAntena();
            }
            catch (Exception)
            {
                return false;
            }
            return false;
        }

        public bool isActivoCorreoCSI()
        {
            try
            {
                string path = System.Windows.Forms.Application.StartupPath + @"\config.ini";
                if (System.IO.File.Exists(path))
                {
                    string str3 = this.Read("PERMISOS_CORREO_CSI", "CSI", path);
                    if (string.IsNullOrEmpty(str3))
                    {
                        this.createNodo(eNodo.PERMISOS_CORREO_CSI, path);
                        return this.isActivoCorreoCSI();
                    }
                    return Convert.ToBoolean(str3.ToLower());
                }
                this.createFileINI(path);
                return this.isActivoAntena();
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool isActivoCorreoGranjas()
        {
            try
            {
                string path = System.Windows.Forms.Application.StartupPath + @"\config.ini";
                if (System.IO.File.Exists(path))
                {
                    string str3 = this.Read("PERMISOS_CORREO", "RUTAS", path);
                    if (string.IsNullOrEmpty(str3))
                    {
                        this.createNodo(eNodo.PERMISOS_CORREO_GRANJAS, path);
                        return this.isActivoCorreoGranjas();
                    }
                    return Convert.ToBoolean(str3.ToLower());
                }
                this.createFileINI(path);
                return this.isActivoCorreoGranjas();
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool isActivoImpresion()
        {
            try
            {
                string path = System.Windows.Forms.Application.StartupPath + @"\config.ini";
                if (System.IO.File.Exists(path))
                {
                    string str3 = this.Read("IMPRIMIR", "ACTIVO", path);
                    if (string.IsNullOrEmpty(str3))
                    {
                        this.createNodo(eNodo.PERMISO_IMPRIMIR, path);
                        return this.isActivoImpresion();
                    }
                    return Convert.ToBoolean(str3.ToLower());
                }
                this.createFileINI(path);
                return this.isActivoImpresion();
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool isEventoTableroControlUnidades()
        {
            try
            {
                string path = System.Windows.Forms.Application.StartupPath + @"\config.ini";
                if (System.IO.File.Exists(path))
                {
                    string str3 = this.Read("ACTIVAR_EVENTO_TABLERO", "ACTIVO", path);
                    if (string.IsNullOrEmpty(str3))
                    {
                        this.createNodo(eNodo.PERMISO_IMPRIMIR, path);
                        return this.isEventoTableroControlUnidades();
                    }
                    return Convert.ToBoolean(str3.ToLower());
                }
                this.createFileINI(path);
                return this.isEventoTableroControlUnidades();
            }
            catch (Exception)
            {
                return false;
            }
        }

        public string Read(string Section, string Key, string ruta)
        {
            StringBuilder retVal = new StringBuilder(0xff);
            GetPrivateProfileString(Section, Key, "", retVal, 0xff, ruta);
            return retVal.ToString();
        }

        public string setRutasImpresora(string configuracion, string nombreImpresora)
        {
            try
            {
                string path = System.Windows.Forms.Application.StartupPath + @"\config.ini";
                if (System.IO.File.Exists(path))
                {
                    this.Write("RUTAS_IMPRESORAS", configuracion.ToUpper(), nombreImpresora, path);
                    return this.Read("RUTAS_IMPRESORAS", configuracion.ToUpper(), path);
                }
                this.createFileINI(path);
                return this.getRutasImpresora(configuracion);
            }
            catch (Exception)
            {
                return "";
            }
        }

        public string setRutaXML(string direccion)
        {
            try
            {
                string path = System.Windows.Forms.Application.StartupPath + @"\config.ini";
                if (System.IO.File.Exists(path))
                {
                    this.Write("RUTA_XML", "RUTA", direccion, path);
                    return Convert.ToString(this.Read("RUTA_XML", "RUTA", path));
                }
                this.createFileINI(path);
                this.getRutaXML();
            }
            catch (Exception)
            {
                return null;
            }
            return "";
        }

        public void setUltimoUsuario(string ultimoUsuario)
        {
            try
            {
                string path = System.Windows.Forms.Application.StartupPath + @"\config.ini";
                if (System.IO.File.Exists(path))
                {
                    this.Write("USUARIO", "ULTIMO_USUARIO", ultimoUsuario, path);
                }
                else
                {
                    this.createFileINI(path);
                }
            }
            catch (Exception)
            {
            }
        }

        public void Write(string Section, string Key, string Value, string ruta)
        {
            WritePrivateProfileString(Section, Key, Value, ruta);
        }

        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string Section, string Key, string Value, string FilePath);

        public object Application { get; private set; }

        private enum eNodo
        {
            CONECTION,
            CONECTION_COMERCIAL,
            CONECTION_INTELISIS,
            PUERTOS,
            LAPSO_TIEMPO,
            RUTA_XML,
            CONFIGURACION_CORREO,
            ENVIO_CORREO_CSI,
            PERMISOS_CORREO_CSI,
            PERMISOS_CORREO_GRANJAS,
            ANTENA,
            PERMISO_IMPRIMIR,
            CONECTION_TALLER,
            CONECTION_COMERCIAL_ATLAS,
            CONECTION_COMCONFIG
        }
    }
}
