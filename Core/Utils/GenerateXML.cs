﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using CoreFletera.Models;
using CoreFletera.BusinessLogic;
using Core.BusinessLogic;

namespace Core.Utils
{
    public class GenerateXML
    {
        internal static string generarListaCadena(List<string> listaCadenas)
        {
            try
            {
                XElement other = new XElement("listaCadenas");
                other.Add(from item in listaCadenas
                          where item != null
                          select new XElement("cadenas", new XElement("cadena", item)));
                return new XElement(other).ToString();
            }
            catch (Exception)
            {
                return null;
            }
        }

        internal static string generarDetallesPresupuestos(List<DetallePresupuesto> listaDetalles)
        {
            try
            {
                XElement itemsXML = new XElement("listaDetalles");
                itemsXML.Add(
                    from item in listaDetalles
                    where item != null
                    select new XElement("detalles",
                            new XElement("idDetallePresupuesto", item.idDetallePresupuesto),
                            new XElement("indicador", (int)item.indicador),
                            new XElement("idModalidad", item.modalidad == null ? null : (int?)item.modalidad.idModalidad),
                            new XElement("enero", item.enero),
                            new XElement("febrero", item.febrero),
                            new XElement("marzo", item.marzo),
                            new XElement("abril", item.abril),
                            new XElement("mayo", item.mayo),
                            new XElement("junio", item.junio),
                            new XElement("julio", item.julio),
                            new XElement("agosto", item.agosto),
                            new XElement("septiembre", item.septiembre),
                            new XElement("octubre", item.octubre),
                            new XElement("noviembre", item.noviembre),
                            new XElement("diciembre", item.diciembre)
                            )
                    );

                return itemsXML.ToString();
            }
            catch (Exception)
            {
                return null;
            }
        }

        internal static string generarListaPlazasSinc(List<Plaza> listaPlaza)
        {
            try
            {
                XElement itemsXML = new XElement("lista");
                itemsXML.Add(
                    from item in listaPlaza
                    where item != null
                    select new XElement("detalle",
                            new XElement("descripcion", item.descripcion),
                            new XElement("departamento", item.departamento),
                            new XElement("puesto", item.puesto),
                            new XElement("cveINTELISIS", item.cveINTELISIS),
                            new XElement("cveCliente", item.cveCliente),
                            new XElement("activo", item.activo)
                            )
                    );

                return itemsXML.ToString();
            }
            catch (Exception)
            {
                return null;
            }
        }

        internal static string getListaSucursalClientesSinc(List<SucursalCliente> listaSucursal)
        {
            try
            {
                XElement itemsXML = new XElement("listaSucCliente");
                itemsXML.Add(
                    from item in listaSucursal
                    where item != null
                    select new XElement("sucCliente",
                            new XElement("clienteINTELISIS", item.cveCliente),
                            new XElement("cveINTELISIS", item.cveINTELISIS),
                            new XElement("nombre", item.nombre),
                            new XElement("direccion", item.direccion),
                            new XElement("activo", item.activo)
                            )
                    );

                return itemsXML.ToString();
            }
            catch (Exception)
            {
                return null;
            }
        }

        internal static string generarListaCadena(List<classCadenas> listaCadenas)
        {
            try
            {
                XElement other = new XElement("listaCadenas");
                other.Add(from item in listaCadenas
                          where item != null
                          select new XElement("cadenas",
                          new XElement("fecha", item.fecha),
                          new XElement("idDepartamento", item.id)));
                return new XElement(other).ToString();
            }
            catch (Exception)
            {
                return null;
            }
        }
        public static string generateListaPagosApoyos(List<Personal> listaPersonal)
        {
            try
            {
                XElement itemsXML = new XElement("listaCadenas");
                itemsXML.Add(
                    from item in listaPersonal
                    where item != null
                    select new XElement("cadenas",
                            new XElement("idPersonal", item.clave),
                            new XElement("viajesMeta", item.apoyoPersonal.metaViaje),
                            new XElement("apoyo", item.apoyoPersonal.pagoApoyo),
                            new XElement("activo", item.apoyoPersonal.activo)
                            )
                    );

                return itemsXML.ToString();
            }
            catch (Exception e)
            {
                var s = e;
                return null;
            }
        }
        public static String generateListDetalles(List<CartaPorte> listaDetalles)
        {
            try
            {
                XElement itemsXML = new XElement("DetallesCartaPorte");
                itemsXML.Add(
                    from item in listaDetalles
                    where item != null
                    select new XElement("Detalles",
                            new XElement("idTarea", item.idTarea),
                            new XElement("NumGuiaId", item.numGuiaId),
                            new XElement("SerieGuia", "A"),
                            new XElement("Consecutivo", item.Consecutivo),
                            new XElement("NoRemision", item.remision),
                            new XElement("IdProdTraf", item.producto.clave),
                            new XElement("IdProdTraf", 0),
                            new XElement("IdPlazaTraf", item.zonaSelect.clave),
                            new XElement("VolDescarga", item.volumenDescarga),
                            new XElement("precio", item.precio),
                            new XElement("PorcIVA", item.PorcIVA),
                            new XElement("ImpIVA", item.ImporIVA),
                            new XElement("PorcRetencion", item.PorcRetencion),
                            new XElement("ImpRetencion", item.ImpRetencion),
                            new XElement("subTotal", item.importeReal),
                            new XElement("fCartaPorte", item.fCartaPorte),
                            new XElement("tara", item.tara),
                            new XElement("pesoBruto", item.pesoBruto),
                            new XElement("horaLlegada", item.horaLlegada),
                            new XElement("horaCaptura", item.horaCaptura),
                            new XElement("observaciones", item.observaciones),
                            new XElement("horaImpresion", item.horaImpresion),
                            new XElement("baja", item.baja ? 1 : 0),
                            new XElement("usuarioAjuste", item.usuarioAjuste),
                            new XElement("idOrigen", item.origen == null ? 0 : item.origen.idOrigen),
                            new XElement("remolque", item.remolque),
                            new XElement("viaje", item.viaje),
                            new XElement("precioOperador", item.precioOperador)
                            )
                    );

                return itemsXML.ToString();
            }
            catch (Exception e)
            {
                var s = e;
                return null;
            }
        }

        internal static string generarListaDeptosSin(List<Departamento> listaDepartamentos)
        {
            try
            {
                XElement itemsXML = new XElement("lista");
                itemsXML.Add(
                    from item in listaDepartamentos
                    where item != null
                    select new XElement("detalle",
                            new XElement("nombre", item.descripcion),
                            new XElement("activo", item.activo)
                            )
                    );

                return itemsXML.ToString();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal static string generarListasPuestosSinc(List<Puesto> listaPuestos)
        {
            try
            {
                XElement itemsXML = new XElement("lista");
                itemsXML.Add(
                    from item in listaPuestos
                    where item != null
                    select new XElement("detalle",
                            new XElement("nombre", item.nombrePuesto)
                            )
                    );

                return itemsXML.ToString();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal static string mapListaZonasOperativasSinc(List<ZonaOperativa> listaZonas)
        {
            try
            {
                XElement itemsXML = new XElement("listaZonas");
                itemsXML.Add(
                    from item in listaZonas
                    where item != null
                    select new XElement("zonas",
                            new XElement("cve", item.cveINTELISIS),
                            new XElement("nombre", item.nombre),
                            new XElement("prefijo", item.prefijo),
                            new XElement("activo", item.activo)
                            )
                    );

                return itemsXML.ToString();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal static string getListaEmpresaSinc(List<Empresa> listaEmpresas)
        {
            try
            {
                XElement itemsXML = new XElement("listaEmpresas");
                itemsXML.Add(
                    from item in listaEmpresas
                    where item != null
                    select new XElement("empresa",
                            new XElement("RazonSocial", item.nombre),
                            new XElement("Direccion", item.direccion),
                            new XElement("Telefono", item.telefono),
                            new XElement("Fax", item.fax),
                            new XElement("rfc", item.rfc),
                            new XElement("estado", item.estado),
                            new XElement("pais", item.pais),
                            new XElement("municipio", item.municipio),
                            new XElement("colonia", item.colonia),
                            new XElement("cp", item.cp),
                            new XElement("activo", item.activo),
                            new XElement("cveIntelisis", item.cveINTELISIS),
                            new XElement("noExterior", item.noExterior),
                            new XElement("noInterior", item.noInterior)
                            )
                    );

                return itemsXML.ToString();
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        internal static string getListaUENSinc(List<UEN> listaUEN)
        {
            try
            {
                XElement itemsXML = new XElement("listaUEN");
                itemsXML.Add(
                    from item in listaUEN
                    where item != null
                    select new XElement("UEN",
                            new XElement("cveINTELISIS", item.cveINTELISIS),
                            new XElement("nombre", item.nombre),
                            new XElement("activo", item.activo)
                            )
                    );
                return itemsXML.ToString();
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        internal static string getListaClientesSinc(List<Cliente> listaClientes)
        {
            try
            {
                XElement itemsXML = new XElement("listaClientes");
                itemsXML.Add(
                    from item in listaClientes
                    where item != null
                    select new XElement("clientes",
                            new XElement("nombre", item.nombre),
                            new XElement("domicilio", item.domicilio),
                            new XElement("rfc", item.rfc),
                            new XElement("activo", item.activo),
                            new XElement("estado", item.estado),
                            new XElement("pais", item.pais),
                            new XElement("municipio", item.municipio),
                            new XElement("colonia", item.colonia),
                            new XElement("cp", item.cp),
                            new XElement("telefono", item.telefono),
                            new XElement("alias", item.alias),
                            new XElement("cveINTELISIS", item.cveINTELISIS),
                            new XElement("noExt", item.direccionNumenro),
                            new XElement("noInt", item.direccionNumeroInt),
                            new XElement("entreCalles", item.entreCalles),
                            new XElement("delegacion", item.delegacion),
                            new XElement("fax", item.fax)
                            )
                    );

                return itemsXML.ToString();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal static string generarListaTickets(List<DetalleFacturaProveedorCombustible> listaDetalles)
        {
            try
            {
                XElement itemsXML = new XElement("listaDetalles");
                itemsXML.Add(
                    from item in listaDetalles
                    select new XElement("detalle",
                    new XElement("idCargaCombustible", item.idCargaCombustible),
                    new XElement("idUnidadTrans", item.unidadTrans),
                    new XElement("ticket", item.ticket),
                    new XElement("litros", item.litros),
                    new XElement("precio", item.precioNeto),
                    new XElement("importe", item.importe),
                    new XElement("IEPS", item.IEMPS),
                    new XElement("activo", item.activo),
                    new XElement("normal", item.normal),
                    new XElement("iva", item.iva),
                    new XElement("subTotal", item.subTotalIEPS)
                    ));
                string doc = new XElement(itemsXML).ToString();
                return doc;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal static string generarListaCargas(List<CargasCombustibleCOM> lista)
        {
            try
            {
                XElement element = new XElement("ListaCargas");
                element.Add(from item in lista
                            where item != null
                            select new XElement("cargas", new object[] { new XElement("idCargaCombustible", item.idCargaCombustible), new XElement("extra", item.extra) }));
                return element.ToString();
            }
            catch (Exception)
            {
                return null;
            }
        }
        internal static string generarListaIds(List<int> listaIds)
        {
            try
            {
                XElement itemsXML = new XElement("listaIds");
                itemsXML.Add(
                    from item in listaIds
                    where item != 0
                    select new XElement("ids",
                    new XElement("id", item)
                    ));
                string doc = new XElement(itemsXML).ToString();
                return doc;
            }
            catch (Exception)
            {
                return null;
            }
        }
        internal static string generarListaProveedores(List<Proveedor> listaProveedores)
        {
            try
            {
                XElement itemsXML = new XElement("listaProveedores");
                itemsXML.Add(
                    from item in listaProveedores
                    where item != null
                    select new XElement("Proveedor",
                        new XElement("CIDCLIENTEPROVEEDOR", item.CIDCLIENTEPROVEEDOR),
                        new XElement("CCODIGOCLIENTE", item.CCODIGOCLIENTE),
                        new XElement("CRAZONSOCIAL", item.razonSocial),
                        new XElement("CRFC", item.RFC),
                        new XElement("CEMAIL1", item.email),
                        new XElement("CESTATUS", item.CESTATUS)

                    ));
                string doc = new XElement(itemsXML).ToString();
                return doc;
            }
            catch (Exception)
            {
                return null;
            }
        }

        internal static string generarListaDetallesOrdenCompraLlanta(List<DetalleOrdenCompra> listaDetalles)
        {
            try
            {
                XElement itemsXML = new XElement("listaDetalles");
                itemsXML.Add(
                    from item in listaDetalles
                    where item != null
                    select new XElement("detalles",
                        new XElement("idDetalleCompraLlanta", item.idDetalleCompra),
                        new XElement("idProductoLlanta", item.llantaProducto == null ? null : (int?)item.llantaProducto.idLLantasProducto),
                        new XElement("idProductoInsumo", item.producto == null ? null : (int?)item.producto.idProducto),
                        new XElement("cantidad", item.cantidad),
                        new XElement("pendientes", item.pendientes),
                        new XElement("idUnidadTrans", item.unidadTransporte == null ? null : item.unidadTransporte.clave),
                        new XElement("factura", item.factura),
                        new XElement("costo", item.costo),
                        new XElement("importe", item.importe)
                    ));
                string doc = new XElement(itemsXML).ToString();
                return doc;
            }
            catch (Exception)
            {
                return null;
            }
        }

        internal static string generarListaTipoSinc(List<TipoUnidad> listaTiposUnidad)
        {
            try
            {
                XElement itemsXML = new XElement("lista");
                itemsXML.Add(
                    from item in listaTiposUnidad
                    where item != null
                    select new XElement("detalle",
                        new XElement("cveINTELISIS", item.cveINTELISIS),
                        new XElement("descripcion", item.descripcion),
                        new XElement("capacidad", item.TonelajeMax),
                        new XElement("clasificacion", item.clasificacion),
                        new XElement("bCombustible", item.bCombustible),
                        new XElement("activo", item.activo),
                        new XElement("noLlantas", item.NoLlantas),
                        new XElement("noEjes", item.NoEjes)
                    ));
                string doc = new XElement(itemsXML).ToString();
                return doc;
            }
            catch (Exception)
            {
                return null;
            }
        }

        internal static string generarListaFechas(List<DateTime> listaFechas)
        {
            try
            {
                XElement itemsXML = new XElement("listaFechas");
                itemsXML.Add(
                    from item in listaFechas
                    where item != null
                    select new XElement("fechas",
                        new XElement("fecha", item)
                    ));
                string doc = new XElement(itemsXML).ToString();
                return doc;
            }
            catch (Exception)
            {
                return null;
            }
        }

        internal static string generarListaTiposUnidad(List<GrupoTipoUnidades> listaTipoUnidades)
        {
            try
            {
                XElement itemsXML = new XElement("listTipoUnidades");
                itemsXML.Add(
                    from item in listaTipoUnidades
                    where item != null
                    select new XElement("tipoUnidad",
                        new XElement("grupo", item.grupo)
                    ));
                string doc = new XElement(itemsXML).ToString();
                return doc;
            }
            catch (Exception)
            {
                return null;
            }
        }

        internal static string generarListaInsumosInspeccion(List<ProductoInsumo> listaInsumos)
        {
            try
            {
                XElement itemsXML = new XElement("listInsumos");
                itemsXML.Add(
                    from item in listaInsumos
                    where item != null
                    select new XElement("insumos",
                        new XElement("idProducto", item.idInsumo),
                        new XElement("cMax", item.cMax)
                    ));
                string doc = new XElement(itemsXML).ToString();
                return doc;
            }
            catch (Exception)
            {
                return null;
            }
        }

        internal static string generarListaDetOrdenServ(List<DetOrdenServicio> listaOrdenServ)
        {
            try
            {
                XElement itemsXML = new XElement("listaOrdenServ");
                itemsXML.Add(
                    from item in listaOrdenServ
                    where item != null
                    select new XElement("ordenes",
                        new XElement("idOrdenTrabajo", item.idOrdenTrabajo),
                        new XElement("idOrdenSer", item.tipoOrdenServicio.idOrdenSer),
                        new XElement("NotaRecepcion", item.descripcion),
                        new XElement("idDivision", item.division.idDivision),
                        new XElement("idServicio", item.servicio == null ? 0 : item.servicio.idServicio),
                        new XElement("idActividad", item.actividad == null ? 0 : item.actividad.idActividad),
                        new XElement("duracion", item.duracion),
                        new XElement("llanta", item.llanta == null ? "" : item.llanta.clave),
                        new XElement("posicion", item.llanta == null ? 0 : item.pocision),
                        new XElement("costoActividad", item.costoActividad)
                    ));
                string doc = new XElement(itemsXML).ToString();
                return doc;
            }
            catch (Exception)
            {
                return null;
            }
        }
        internal static string generarListaActualizarCargas(List<Despacho> listaDespachos)
        {
            try
            {
                XElement itemsXML = new XElement("listaDespachos");
                itemsXML.Add(
                    from item in listaDespachos
                    where item != null
                    select new XElement("despachos",
                        new XElement("vale", item.noVale),
                        new XElement("isExtra", item.isExtra),
                        new XElement("precio", item.precio),
                        new XElement("factura", item.factura)
                    ));
                string doc = new XElement(itemsXML).ToString();
                return doc;
            }
            catch (Exception)
            {
                return null;
            }
        }

        internal static string generarListaInsumos(List<ProductosActividad> listaInsumos, int ordenServicio)
        {
            try
            {
                XElement itemsXML = new XElement("listProductos");
                itemsXML.Add(
                    from item in listaInsumos
                    where item != null
                    select new XElement("productos",
                        new XElement("idOrdenServ", ordenServicio),
                        new XElement("idActividad", item.idActicidad),
                        new XElement("idProducto", item.idProducto),
                        new XElement("cantidad", item.cantidad),
                        new XElement("costo", item.costo),
                        new XElement("existencia", item.existencia),
                        new XElement("extra", item.extra),
                        new XElement("cantCompra", item.cantCompra),
                        new XElement("cantSal", item.cantSal)
                    ));
                string doc = new XElement(itemsXML).ToString();
                return doc;
            }
            catch (Exception)
            {
                return null;
            }
        }

        internal static string generateListConsecutivos(List<CartaPorte> listCP)
        {
            try
            {
                XElement itemsXML = new XElement("listaConsecutivos");
                itemsXML.Add(
                    from item in listCP
                    where item != null
                    select new XElement("consecutivos",
                        new XElement("consecutivo", item.Consecutivo)
                    ));
                string doc = new XElement(itemsXML).ToString();
                return doc;
            }
            catch (Exception)
            {
                return null;
            }
        }

        internal static string generateListCargasExtea(List<CargaCombustibleExtra> listCombustibleExtra)
        {
            try
            {
                XElement itemsXML = new XElement("listaCargasExtra");
                itemsXML.Add(
                    from item in listCombustibleExtra
                    where item != null
                    select new XElement("cargaExtra",
                        new XElement("idCargaCombustibleExtra", item.idCargaCombustibleExtra)
                    ));
                string doc = new XElement(itemsXML).ToString();
                return doc;
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        internal static string generarListaMotivos(List<MotivoEntrada> listMotivo)
        {
            try
            {
                XElement itemsXML = new XElement("listaMotivos");
                itemsXML.Add(
                    from item in listMotivo
                    where item != null
                    select new XElement("motivo",
                        new XElement("idMotivo", item.idMotivo)
                    ));
                string doc = new XElement(itemsXML).ToString();
                return doc;
            }
            catch (Exception)
            {
                return null;
            }
        }

        internal static object generateListID(List<string> lista)
        {
            throw new NotImplementedException();
        }

        public static String generateListDetalles(List<CartaPorteBachoco> listaDetalles)
        {
            try
            {
                XElement itemsXML = new XElement("DetallesCartaPorte");
                itemsXML.Add(
                    from item in listaDetalles
                    where item != null
                    select new XElement("Detalles",
                            new XElement("idCartaPorte", item.idCartaPorte),
                            new XElement("idViaje", item.idViaje),
                            new XElement("idProducto", item.producto.clave),
                            new XElement("idDestino", item.destino.clave),
                            new XElement("pesoBruto", item.pesoBruto),
                            new XElement("volumenCarga", item.volumenCarga),
                            new XElement("precio", item.precio),
                            new XElement("iva", item.iva),
                            new XElement("importeIva", item.importeIva),
                            new XElement("retencion", item.retencion),
                            new XElement("importeRetencion", item.importeRetencion),
                            new XElement("subTotal", item.subTotal),
                            new XElement("remision", item.remision),
                            new XElement("horaLlegada", item.fechaLlegada),
                            new XElement("horaCaptura", item.fechaCaptura),
                            new XElement("horaImpresion", item.horaImpresion),
                            new XElement("observaciones", item.observaciones),
                            new XElement("fFolio", item.fFolio)
                            )
                    );

                return itemsXML.ToString();
            }
            catch (Exception e)
            {
                var s = e;
                return null;
            }
        }

        internal static string generarListaRelOrdenServCitas(List<RegistroCitaServicio> listaCitas)
        {
            try
            {
                XElement itemsXML = new XElement("lista");
                itemsXML.Add(
                    from item in listaCitas
                    where item != null
                    select new XElement("Detalles",
                            new XElement("id", item.idINTELISIS),
                            new XElement("idOrdenServ", item.idOrdenServicio),
                            new XElement("modulo", item.modulo)
                        )
                    );
                return itemsXML.ToString();
            }
            catch (Exception e)
            {
                var s = e;
                return null;
            }
        }

        internal static string generarListaCitasServicio(MasterOrdServ masterOrdServ, List<CabOrdenServicio> listaCabOrdenServicio)
        {
            try
            {
                XElement itemsXML = new XElement("lista");
                itemsXML.Add(
                    from item in listaCabOrdenServicio
                    where item != null
                    select new XElement("Detalles",
                            new XElement("idPadre", masterOrdServ.idPadreOrdSer.ToString()),
                            new XElement("idOrdenServ", item.idOrdenSer.ToString()),
                            new XElement("cveEmpresa", item.empresa.cveINTELISIS),
                            new XElement("fechaCaptura", item.fechaCaptura),
                            new XElement("cveUEN", item.cveUEN),
                            new XElement("cveCliente", item.cliente.cveINTELISIS),
                            new XElement("cvePersonal", item.cveEmpleadoAutoriza),
                            new XElement("cveSucursal", item.zonaOperativa.cveINTELISIS),
                            new XElement("sucCliente", item.cveSucCliente),
                            new XElement("idTipoOrden", (int)item.enumTipoOrdServ),
                            new XElement("idUnidad", item.unidad.clave),
                            new XElement("fallaReportada", (string)getCadenaOrdenTrabajo(item.listaDetOrdenServicio)),
                            new XElement("cveChofer", masterOrdServ.chofer.cveAgente)
                        )
                    );
                return itemsXML.ToString();
            }
            catch (Exception e)
            {
                var s = e;
                return null;
            }
        }
        internal static string generarListaInvCombustible(MasterOrdServ masterOrdServ, List<CabOrdenServicio> listaCabOrdenServicio)
        {
            try
            {
                XElement itemsXML = new XElement("lista");
                itemsXML.Add(
                    from item in listaCabOrdenServicio
                    where item != null
                    select new XElement("Detalles",
                            new XElement("cveEmpresa", masterOrdServ.empresa.cveINTELISIS),
                            new XElement("fechaCaptura", item.fechaCaptura),
                            new XElement("cveUEN", item.cveUEN),
                            new XElement("usuario", masterOrdServ.userCrea),
                            new XElement("idOrdenPadre", masterOrdServ.idPadreOrdSer),
                            new XElement("idOrdenServ", item.idOrdenSer),
                            new XElement("fallaReportada", getCadenaOrdenTrabajo(item.listaDetOrdenServicio)),
                            new XElement("cveSucursal", item.zonaOperativa.cveINTELISIS),
                            new XElement("cveUnidadTransporte", item.unidad.clave),
                            new XElement("cveAgenteOperador", masterOrdServ.chofer.cveAgente),
                            new XElement("agente", item.cveEmpleadoAutoriza),
                            new XElement("tipoCombustible", item.enumTipoOrdServ == enumTipoOrdServ.COMBUSTIBLE ? "NORMAL" : "EXTRA"),
                            new XElement("isCR", item.unidad.clave.Contains("CR"))
                        )
                    );
                return itemsXML.ToString();
            }
            catch (Exception e)
            {
                var s = e;
                return null;
            }
        }

        internal static string generarListaLlantas(MasterOrdServ masterOrdServ, List<CabOrdenServicio> listaCabOrdenServicio)
        {
            try
            {
                XElement itemsXML = new XElement("lista");
                itemsXML.Add(
                    from item in listaCabOrdenServicio
                    where item != null
                    select new XElement("Detalles",
                            new XElement("cveEmpresa", masterOrdServ.empresa.cveINTELISIS),
                            new XElement("cveSucursal", item.zonaOperativa.cveINTELISIS),
                            new XElement("fecha", item.fechaCaptura),
                            new XElement("usuario", masterOrdServ.userCrea),
                            new XElement("cveUEN", item.cveUEN),
                            new XElement("idOrdenPadre", masterOrdServ.idPadreOrdSer),
                            new XElement("idOrdenServ", item.idOrdenSer),
                            new XElement("cveAgente", item.cveEmpleadoAutoriza),
                            new XElement("cveArtTC", masterOrdServ.tractor.tipoUnidad.cveINTELISIS),
                            new XElement("cveIdTC", masterOrdServ.tractor.clave),
                            new XElement("cveArtRem1", masterOrdServ.tolva1 == null ? null : masterOrdServ.tolva1.tipoUnidad.cveINTELISIS),
                            new XElement("cveSerieRem1", masterOrdServ.tolva1 == null ? null : masterOrdServ.tolva1.clave),
                            new XElement("cveArtDl", masterOrdServ.dolly == null ? null : masterOrdServ.dolly.tipoUnidad.cveINTELISIS),
                            new XElement("cveSerieDl", masterOrdServ.dolly == null ? null : masterOrdServ.dolly.clave),
                            new XElement("cveArtRem2", masterOrdServ.tolva2 == null ? null : masterOrdServ.tolva2.tipoUnidad.cveINTELISIS),
                            new XElement("cveSerieRem2", masterOrdServ.tolva2 == null ? null : masterOrdServ.tolva2.clave)
                        )
                    );
                return itemsXML.ToString();
            }
            catch (Exception e)
            {
                var s = e;
                return null;
            }
        }

        private static string getCadenaOrdenTrabajo(List<DetOrdenServicio> listaDetOrdenServicio)
        {
            try
            {
                string cadena = string.Empty;
                foreach (var item in listaDetOrdenServicio)
                {
                    cadena += item.ordenTrabajo + Environment.NewLine;
                }
                return cadena;
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
        }

        internal static object generateListDetalles(List<DetallesPerfilReporte> listDetalles)
        {
            try
            {
                XElement itemsXML = new XElement("DetallesPerfilReporte");
                itemsXML.Add(
                    from item in listDetalles
                    where item != null
                    select new XElement("Detalles",
                            new XElement("nombreDetalle", item.nombreDetalle)
                            )
                    );

                return itemsXML.ToString();
            }
            catch (Exception e)
            {
                var s = e;
                return null;
            }
        }

        internal static string generarListaOrdenesTrabajo(List<DetalleLlantera> lista)
        {
            try
            {
                XElement itemsXML = new XElement("listaOrdenesTrabajos");
                itemsXML.Add(
                    from item in lista
                    where item != null
                    select new XElement("Ordenes",
                            new XElement("idOrdenActividad", item.idOrdenActividad)
                            )
                    );

                return itemsXML.ToString();
            }
            catch (Exception e)
            {
                var s = e;
                return null;
            }
        }

        internal static string generarListZonas(List<Zona> list)
        {
            try
            {
                XElement itemsXML = new XElement("listZonas");
                itemsXML.Add(
                    from item in list
                    where item != null
                    select new XElement("zona",
                            new XElement("idZona", item.clave)
                            )
                    );

                return itemsXML.ToString();
            }
            catch (Exception e)
            {
                var s = e;
                return null;
            }
        }

        internal static object generateListPrivilegios(List<Privilegio> listPrivilegios)
        {
            try
            {
                XElement itemsXML = new XElement("PrivilegiosUsuario");
                itemsXML.Add(
                    from item in listPrivilegios
                    where item != null
                    select new XElement("Privilegios",
                            new XElement("idPrivilegio", item.idPrivilegio)
                            )
                    );

                return itemsXML.ToString();
            }
            catch (Exception e)
            {
                var s = e;
                return null;
            }
        }

        internal static object generateListtareas(List<TareaProgramada> listTareas)
        {
            try
            {
                XElement itemsXML = new XElement("listTareas");
                itemsXML.Add(
                    from item in listTareas
                    where item != null
                    select new XElement("tareas",
                            new XElement("idTareaProgramada", item.idTareaProgramada),
                            new XElement("idProducto", item.idProducto),
                            new XElement("idCliente", item.idCliente),
                            new XElement("DescripcionProducto", item.DescripcionProducto),
                            new XElement("valorProgramado", item.valorProgramado),
                            new XElement("destino", item.destino),
                            new XElement("remision", item.remision),
                            new XElement("hoja", item.hoja),
                            new XElement("estatus", item.estatus),
                            new XElement("fecha", item.fecha)
                            )
                    );

                return itemsXML.ToString();
            }
            catch (Exception e)
            {
                var s = e;
                return null;
            }
        }

        internal static object generateListSalidas(List<SalidaEntrada> lista)
        {
            try
            {
                XElement itemsXML = new XElement("listSalidasEntradas");
                itemsXML.Add(
                    from item in lista
                    where item != null
                    select new XElement("salidasEntradas",
                            new XElement("idOperacion", item.idOperacion),
                            new XElement("idPadreOrdSer", item.masterOrdServ.idPadreOrdSer),
                            new XElement("idChofer", item.chofer.clave),
                            new XElement("idUnidadTrans", item.unidadTransporte.clave),
                            new XElement("fecha", item.fecha),
                            new XElement("fechaFin", item.fechaFin),
                            new XElement("tipoOperacion", ((int)item.tipoMoviemiento)),
                            new XElement("KM", item.km),
                            new XElement("idUsuario", item.usuario.idUsuario)
                            )
                    );

                return itemsXML.ToString();
            }
            catch (Exception e)
            {
                var s = e;
                return null;
            }
        }

        internal static string generateListLlantas(List<Llanta> lista)
        {
            try
            {
                XElement itemsXML = new XElement("listaLlantas");
                itemsXML.Add(
                    from item in lista
                    where item != null
                    select new XElement("llantas",
                            new XElement("idLlanta", item.idLlanta),
                            new XElement("NoSerie", item.noSerie),
                            new XElement("idEmpresa", item.idEmpresa),
                            //new XElement("idProductoLlanta", item.productoLLanta.idLLantasProducto),
                            new XElement("idTipoLLanta", 0),
                            new XElement("Factura", item.factura),
                            new XElement("FechaFactura", item.fechaFactura),
                            new XElement("Costo", item.costo),
                            new XElement("Año", item.año),
                            new XElement("Mes", item.mes)
                            )
                    );

                return itemsXML.ToString();
            }
            catch (Exception e)
            {
                var s = e;
                return null;
            }
        }

        internal static string generarListaRelacionDetalleFact(List<RelacionDetalleOrdenFactura> listaRelacion)
        {
            try
            {
                XElement itemsXML = new XElement("listaRelacion");
                itemsXML.Add(
                    from item in listaRelacion
                    where item != null
                    select new XElement("relacion",
                            new XElement("idOrden", item.ordenCompra.idOrdenCompra),
                            new XElement("idDetalleOrden", item.detalleCompra.idDetalleCompra),
                            new XElement("cantidad", item.cantidad),
                            new XElement("factura", item.detalleCompra.factura)
                            )
                    );

                return itemsXML.ToString();
            }
            catch (Exception e)
            {
                var s = e;
                return null;
            }
        }

        internal static string generarListaUnidades(List<string> listaUnidades)
        {
            try
            {
                if (listaUnidades.Count == 0)
                {
                    return string.Empty;
                }
                XElement itemsXML = new XElement("listaUnidades");
                itemsXML.Add(
                    from item in listaUnidades
                    where item != null
                    select new XElement("unidades",
                            new XElement("idUnidad", item)
                            )
                    );

                return itemsXML.ToString();
            }
            catch (Exception e)
            {
                var s = e;
                return null;
            }
        }

        internal static string generarListaRemisionesLiberadas(Viaje viaje)
        {
            try
            {
                XElement itemsXML = new XElement("RemisionesLiberadas");
                itemsXML.Add(
                    from item in viaje.listDetalles
                    where item != null
                    select new XElement("Detalles",
                            new XElement("remision", item.remision),
                            new XElement("idCliente", viaje.cliente.clave)
                            )
                    );

                return itemsXML.ToString();
            }
            catch (Exception e)
            {
                var s = e;
                return null;
            }
        }
        internal static string generarListaValesCarga2(List<ValeCarga> listaVales)
        {
            try
            {
                XElement itemsXML = new XElement("ListaValesCarga");
                itemsXML.Add(
                    from item in listaVales
                    where item != null
                    select new XElement("valeCarga",
                            new XElement("numGuiaId", item.numGuiaId),
                            new XElement("idCliente", item.cliente.clave)
                            )
                    );

                return itemsXML.ToString();
            }
            catch (Exception e)
            {
                var s = e;
                return null;
            }
        }

        internal static string generarListaValesCarga(List<ValeCarga> listVales)
        {
            try
            {
                XElement itemsXML = new XElement("ListaValesCarga");
                itemsXML.Add(
                    from item in listVales
                    where item != null
                    select new XElement("valeCarga",
                            new XElement("idValeCarga", item.idValeCarga),
                            new XElement("noRemision", item.remision),
                            new XElement("numGuiaId", item.numGuiaId),
                            new XElement("idProducto", item.producto == null ? 0 : item.producto.clave),
                            new XElement("idOrigen", item.origen == null ? 0 : item.origen.idOrigen),
                            new XElement("idDestino", item.destino == null ? 0 : item.destino.clave),
                            new XElement("volDescarga", item.volDescarga),
                            new XElement("cartaPorte", item.cartaPorte),
                            new XElement("precio", item.precio),
                            new XElement("idCliente", item.cliente.clave),
                            new XElement("vDescargado", item.vDescargado),
                            new XElement("fechaCarga", item.fechaCarga),
                            new XElement("fechaDescarga", item.fechaDescarga)
                            )
                    );

                return itemsXML.ToString();
            }
            catch (Exception e)
            {
                var s = e;
                return null;
            }
        }

        internal static string generarListaParadas(List<ParadaEquipo> listaParadas)
        {
            try
            {
                XElement itemsXML = new XElement("listaParadas");
                itemsXML.Add(
                    from item in listaParadas
                    where item != null
                    select new XElement("paradas",
                            new XElement("id", item.idParadaEquipo),
                            new XElement("tc", item.alias),
                            new XElement("fecha", item.fechaFinal),
                            new XElement("noViaje", item.folioViaje)
                            )
                    );

                return itemsXML.ToString();
            }
            catch (Exception e)
            {
                var s = e;
                return null;
            }
        }

        internal static string generarListaPersonalSinc(List<Personal> listaPersonal)
        {
            try
            {
                XElement itemsXML = new XElement("lista");
                itemsXML.Add(
                    from item in listaPersonal
                    where item != null
                    select new XElement("detalle",
                        new XElement("cveINTELISIS", item.cveINTELISIS),
                        new XElement("cveEmpresa", item.cveEmpresaINTELISIS),
                        new XElement("nombres", item.nombres),
                        new XElement("apellidoPat", item.apellidoPaterno),
                        new XElement("apellidoMat", item.apellidoMaterno),
                        new XElement("direccion", item.calle),
                        new XElement("numero", item.numero),
                        new XElement("numeroInt", item.numeroInt),
                        new XElement("colonia", item.coloniaStr),
                        new XElement("delegacion", item.delegacion.ToUpper()),
                        new XElement("poblacion", item.ciudad.ToUpper()),
                        new XElement("estado", item.estado.ToUpper()),
                        new XElement("pais", item.pais.ToUpper()),
                        new XElement("cp", item.cp),
                        new XElement("telefono", item.telefono),
                        new XElement("mail", item.correoElectronico),
                        new XElement("curp", item.documentacionPersonal.CURP),
                        new XElement("rfc", item.documentacionPersonal.RFC),
                        new XElement("imss", item.documentacionPersonal.numeroSeguroSocial),
                        new XElement("banco", item.documentacionPersonal.banco),
                        new XElement("cuenta", item.documentacionPersonal.cuenta),
                        new XElement("fechaNacimiento", item.fechaNacimiento),
                        new XElement("genero", item.genero.ToUpper()),
                        new XElement("estadoCivil", item.estadoCivil.ToUpper()),
                        new XElement("departamento", item.departamentoStr),
                        new XElement("puesto", item.puestoStr),
                        new XElement("plaza", item.cvePlaza),
                        new XElement("estatus", item.estatusBD),
                        new XElement("cveAgente", item.cveAgente)

                    ));
                string doc = new XElement(itemsXML).ToString();
                return doc;
            }
            catch (Exception)
            {
                return null;
            }
        }

        internal static string generarListaViajes(List<ReporteCartaPorteViajes> listViajes)
        {
            try
            {
                XElement itemsXML = new XElement("lista");
                itemsXML.Add(
                    from item in listViajes
                    where item != null
                    select new XElement("detalle",
                        new XElement("empresa", item.cveEmpresa),
                        new XElement("fecha", item.fechaInicio),
                        new XElement("UEN", item.cveUEN),
                        new XElement("cliente", item.cveCliente),
                        new XElement("sucCliente", item.cveSucCliente),
                        new XElement("cvePersonal", item.cvePersonal),
                        new XElement("hora", item.fechaInicio.ToString("HH:mm:ss")),
                        new XElement("importe", item.importe),
                        new XElement("importeImpuesto", item.impIva),
                        new XElement("zonaOperativa", item.cveSucursal),
                        new XElement("tc", item.tc),
                        new XElement("remolque1", item.rem1),
                        new XElement("dolly", string.IsNullOrEmpty(item.dolly) ? null : item.dolly),
                        new XElement("remolque2", string.IsNullOrEmpty(item.rem2) ? null : item.rem2),
                        new XElement("cveOperador", item.cveAgenteOperador),
                        new XElement("toneladas", item.toneladas),
                        new XElement("km", item.km),
                        new XElement("referencia", item.folio),
                        new XElement("tipoGranja", item.tipoGranja)
                    )); ;
                string doc = new XElement(itemsXML).ToString();
                return doc;
            }
            catch (Exception)
            {
                return null;
            }
        }

        internal static string generarListaCartaPorte(List<ReporteCartaPorte> listaCartaPortes)
        {
            try
            {
                XElement itemsXML = new XElement("lista");
                itemsXML.Add(
                    from item in listaCartaPortes
                    where item != null
                    select new XElement("detalle",
                        new XElement("folio", item.folio),
                        new XElement("cantidad", item.toneladas),
                        new XElement("sucCliente", item.cveSucCliente),
                        new XElement("precioVariable", item.precioVar),
                        new XElement("impIva", item.impIva),
                        new XElement("impRetencion", item.impRetencion),
                        new XElement("costo", item.subTotal),
                        new XElement("fecha", item.fechaInicio),
                        new XElement("cveChofer", item.cveOperador),
                        new XElement("cveSucursal", item.cveSucursal),
                        new XElement("cveUEN", item.cveUEN),
                        new XElement("consecutivo", item.cartaPorte),
                        new XElement("remision", item.orden),
                        new XElement("destino", item.destino),
                        new XElement("producto", item.producto),
                        new XElement("cantProgramada", item.toneladasDespachadas),
                        new XElement("cantDespachada", item.toneladasDespachadas),
                        new XElement("cantDescargada", item.toneladas),
                        new XElement("precioFijo", item.precioFijo),
                        new XElement("viaje", item.viaje),
                        new XElement("idDestino", item.idDestino),
                        new XElement("idProducto", item.idProducto),
                        new XElement("cobroKM", item.cobroXKm),
                        new XElement("full", item.modalidad == "FULL" ? true : false),
                        new XElement("id", item.id),
                        new XElement("capacidadIns", item.capacidadIns),
                        new XElement("tipoGranja", item.tipoGranja),
                        new XElement("cliente", item.idCliente),
                        new XElement("tractor", item.tc)
                    )); ;
                string doc = new XElement(itemsXML).ToString();
                return doc;
            }
            catch (Exception)
            {
                return null;
            }
        }

        internal static string generarListaUnidadesSinc(List<UnidadTransporte> listaUnidades)
        {
            try
            {
                XElement itemsXML = new XElement("lista");
                itemsXML.Add(
                    from item in listaUnidades
                    where item != null
                    select new XElement("detalle",
                        new XElement("cveUnidad", item.clave),
                        new XElement("descripcion", item.tipoUnidad.descripcion),
                        new XElement("cveTipoUnidad", item.tipoUnidad.cveINTELISIS),
                        new XElement("cveEmpresa", item.empresa.cveINTELISIS),
                        new XElement("estatus", item.estatus),
                        new XElement("fechaAdquisicion", item.fechaAdquisicion),
                        new XElement("placas", item.placas),
                        new XElement("factura", item.documento),
                        new XElement("anio", item.anio),
                        new XElement("serie", item.serie),
                        new XElement("cveINTELISIS", item.cveINTELISIS)
                    )); ;
                string doc = new XElement(itemsXML).ToString();
                return doc;
            }
            catch (Exception)
            {
                return null;
            }

        }

        internal static string generarListaReg(List<RegistroINTELISIS> listaRegistro)
        {
            try
            {
                XElement itemsXML = new XElement("lista");
                itemsXML.Add(
                    from item in listaRegistro
                    where item != null
                    select new XElement("detalle",
                        new XElement("folio", item.id),
                        new XElement("cantidad", item.fechaReg)
                    )); ;
                string doc = new XElement(itemsXML).ToString();
                return doc;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
