﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.ViewModel
{
    public class DetalleCartaPorteViewModel
    {
        public CartaPorte detalles = new CartaPorte();

        public List<Producto> listProducto { get; set; }
        public Producto productoSelect { get; set; }

        public List<Zona> listZonas { get; set; }
        public Zona zonaSelect { get; set; }

        

        public int remision { get; set; }

        public decimal volumenCarga { get; set; }

        private decimal _volCarga { get; set; }
        public decimal volumenDescarga
        {
            get
            {
                return _volCarga;
            }
            set
            {

                _volCarga = value;
                //this.OnPropertyChanged("volumenDescarga");
            }
        }

        public decimal impuesto { get; set; }
        public decimal retension { get; set; }


        public bool isFull { get; set; }

        private decimal _importeReal
        {
            get
            {
                if (zonaSelect == null && volumenDescarga < 0)
                {
                    return 0;
                }
                return volumenDescarga * (isFull ? zonaSelect.costoFull : zonaSelect.costoSencillo);
            }
            set
            {
                if (zonaSelect == null)
                {
                    _importeReal = 0;
                }
                _importeReal = volumenDescarga * (isFull ? zonaSelect.costoFull : zonaSelect.costoSencillo);
                //this.OnPropertyChanged("importeReal");
            }
        }
        public decimal importeReal
        {
            get
            {
                if (zonaSelect == null)
                {
                    return 0;
                }
                return volumenDescarga * (isFull ? zonaSelect.costoFull : zonaSelect.costoSencillo);
            }
            set
            {
                _importeReal = value;
                //this.OnPropertyChanged("importeReal");
            }
        }

        //public event PropertyChangedEventHandler PropertyChanged;
        //private void OnPropertyChanged(string propertyName)
        //{
        //    if (PropertyChanged != null)
        //    {
        //        PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        //        PropertyChanged(this, new PropertyChangedEventArgs("importeReal"));
        //    }
        //}
    }
}
