﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.BusinessLogic;
using CoreFletera.Models;

namespace CoreFletera.Interfaces
{
    public class TicketsCombustibleSvc : ITicketsCombustible
    {
        public OperationResult getTicketsCombustibleByProveedor(int idProveedor, int idEmpresa, int idTipoCombustible) =>
            new TicketsCombustibleServices().getTicketsCombustibleByProveedor(idProveedor, idEmpresa, idTipoCombustible);


        public OperationResult saveFaturaProveedorCombustible(ref FacturaProveedorCombustible factura) =>
                    new TicketsCombustibleServices().saveFaturaProveedorCombustible(ref factura);
        public OperationResult getFacturasProveedorDieselByFiltros(int idEmpresa, DateTime fechaInicial, DateTime fechaFinal, List<string> listaZonasOperativas, bool todasZonasOpe,
            List<string> listaProveedores, bool todosProveedores, int idTipoCombustible) =>
            new TicketsCombustibleServices().getFacturasProveedorDieselByFiltros(idEmpresa, fechaInicial, fechaFinal, listaZonasOperativas, todasZonasOpe, listaProveedores, todosProveedores,
                idTipoCombustible);

        public OperationResult getFacturasProveedorByFactura(int idProveedor, string strFactura) =>
        new TicketsCombustibleServices().getFacturasProveedorByFactura(idProveedor, strFactura);
    }
}
