﻿using Core.Models;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoreFletera.BusinessLogic;
namespace CoreFletera.Interfaces
{
    public class SucursalClienteSvc : ISucursalCliente
    {
        public OperationResult sincronizarSucursalCliente(List<SucursalCliente> listaSucursal) =>
            new SucursalClienteService().sincronizarSucursalCliente(listaSucursal);
    }
}
