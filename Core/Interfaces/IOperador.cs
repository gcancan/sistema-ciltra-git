﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Interfaces
{
    public interface IOperador
    {
        OperationResult getOperadoresALLorById(int id = 0, int idEmpresa = 0);
        OperationResult getPersonalALLorById(int id = 0);
        OperationResult getOperadoresByNombre(string nombre, int idEmpresa = 0, int idCliente = 0);
        OperationResult getLavadoresByNombre(string nombre, int idEmpresa = 0);
        OperationResult getLlanterosByNombre(string nombre, int idEmpresa = 0);
        OperationResult getMecanicosByNombre(string nombre, int idEmpresa = 0);
        OperationResult getPersonalByNombre(string nombre, int idEmpresa = 0);
        OperationResult getDespachadoresByNombre(string nombre = "%");
        OperationResult getOperadoresViaje(DateTime fechaInicio, DateTime fechaFin);
        OperationResult getOperadoresByIdCliente(int idCliente);
        OperationResult getOperadoresByNombreAndCliente(string nombre, int idCliente);
        OperationResult savePersonal(ref Personal personal);
        OperationResult getPersonal_v2();
        OperationResult getPersonalById_v2(int idPersonal);
        OperationResult getPersonalByDepartamentos(List<string> listaDeptos);
        OperationResult getPersonalByDepartamentosAndPuesto(List<string> listaDeptos, List<string> listaPuestos);
        OperationResult getPersonal_v2ByIdEmpresa(int idEmpresa);
        OperationResult getPersonalConApoyo();
        OperationResult getPersonal_v2ByClave(int idPersonal);
        OperationResult savePagosApoyos(List<Personal> listaPersonal);
        OperationResult getListaDatosIngreso(int idPersonal);
        OperationResult getContactoEmergencia(int idPersonal);
        OperationResult getDocumentosPersonalByIdPersonal(int idPersonal);
        OperationResult getLicenciasConducirByIdPersonal(int idPersonal);
        OperationResult sincronizarPersonal(List<Personal> listaPersonal, string usuario);
        OperationResult getPersonal_v2ByDepartamento(int idDepto);
        OperationResult getPersonal_v2ByIdPersonal(int idPersonal);
    }
}
