﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Interfaces
{
    public interface IProducto
    {
        OperationResult getALLProductosByIdORidFraccion(int idFraccion, int id = 0);
        OperationResult saveProducto(ref Producto producto);
        OperationResult deleteProductos(int clave);
        OperationResult getALLProductosByidFraccionEmpresa(int idEmpresa, int idFraccion);
        OperationResult getAllUnidadesMedida();
    }
}
