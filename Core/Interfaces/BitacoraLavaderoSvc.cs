﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.BusinessLogic;
namespace CoreFletera.Interfaces
{
    public class BitacoraLavaderoSvc : IBitacoraLavadero
    {
        public OperationResult getBitacoraLavadero(int idEmpresa, DateTime fechaInicio, DateTime fechaFin, List<string> listaTractores)
        {
            return new BitacoraLavaderoServices().getBitacoraLavadero(idEmpresa, fechaInicio, fechaFin, listaTractores);
        }
    }
}
