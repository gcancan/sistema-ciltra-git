﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.BusinessLogic;
namespace CoreFletera.Interfaces
{
    public class BitacoraCambioPreciosSvc : IBitacoraCambioPrecios
    {
        public OperationResult getBitacoraPrecios(DateTime fechaInicial, DateTime fechaFinal)
        {
            try
            {
                OperationResult resp = new BitacoraCambioPreciosService().getBitacoraPrecios(fechaInicial, fechaFinal);
                if (resp.typeResult == ResultTypes.success)
                {
                    List<BitacoraCambioPrecios> lista = new List<BitacoraCambioPrecios>();
                    lista = (List<BitacoraCambioPrecios>)resp.result;
                    foreach (BitacoraCambioPrecios bitacora in lista)
                    {
                        OperationResult respDet = new BitacoraCambioPreciosService().getDetalleBitacoraCambioPrecios(bitacora.idBitacoraCambioPrecios);
                        if (respDet.typeResult == ResultTypes.success)
                        {
                            bitacora.listaDetalles = (List<DetallesBitacoraCambioPrecios>)respDet.result;
                        }
                        else
                        {
                            return respDet;
                        }                            
                    }
                    return resp;
                }
                else
                {
                    return resp;
                }                
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }
    }
}
