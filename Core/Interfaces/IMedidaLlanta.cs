﻿namespace CoreFletera.Interfaces
{
    using Core.Models;
    using CoreFletera.Models;

    internal interface IMedidaLlanta
    {
        OperationResult getMedidasLlantas();
        OperationResult saveMedidaLlantas(ref MedidaLlanta medidaLlanta);
    }
}
