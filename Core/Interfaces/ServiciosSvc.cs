﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.BusinessLogic;
using CoreFletera.Models;

namespace CoreFletera.Interfaces
{
    public class ServiciosSvc : IServicios
    {
        public OperationResult getAllServicios() =>
            new ServiciosServices().getAllServicios();

        public OperationResult saveServicio(ref Servicio servicio)
        {
            try
            {
                return  new ServiciosServices().saveServicio(ref servicio);
                //if (resp.typeResult == ResultTypes.success)
                //{
                //    if (servicio.activo)
                //    {
                //        var r = new RelacionTiposOrdenServSvc().saveRelacioOrdenServ(
                //            new TipoOrdenServClass { idTipoOrdServ = (int)servicio.enumTipoOrdServ },
                //            CatTipoOrdenServ.SERVICIOS,
                //            servicio.idServicio
                //            );
                //        return r;
                //    }
                //    else
                //    {
                //        var r = new RelacionTiposOrdenServSvc().deleteRelacioOrdenServ(
                //            new TipoOrdenServClass { idTipoOrdServ = (int)servicio.enumTipoOrdServ },
                //            CatTipoOrdenServ.SERVICIOS,
                //            servicio.idServicio
                //            );
                //        return r;
                //    }
                //}
                //else
                //{
                //    return resp;
                //}
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

    }
}
