﻿namespace CoreFletera.Interfaces
{
    using Core.Models;
    using CoreFletera.BusinessLogic;
    using CoreFletera.Models;
    using System;

    public class GeoCercaSvc : IGeoCerca
    {
        public OperationResult getGeoCerca() =>
            new GeoCercaServices().getGeoCerca();
        public OperationResult getGeoCerca(string nombreGeoCerca, string strConnnestion) =>
            new GeoCercaServices().getGeoCerca(nombreGeoCerca, strConnnestion);
        public OperationResult saveGeoCerca(ref GeoCerca geoCerca) =>
            new GeoCercaServices().saveGeoCerca(ref geoCerca);
    }
}
