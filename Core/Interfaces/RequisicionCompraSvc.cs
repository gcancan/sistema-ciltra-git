﻿namespace CoreFletera.Interfaces{
    using Core.Models;
    using CoreFletera.BusinessLogic;
    using System;
    public class RequisicionCompraSvc : IRequisicionCompra
    {        public OperationResult getRequisicioCompraByFecha(DateTime fechaInicial, DateTime fechaFinal) =>
            new RequisicionCompraServices().getRequisicioCompraByFecha(fechaInicial, fechaFinal);
    }
}
