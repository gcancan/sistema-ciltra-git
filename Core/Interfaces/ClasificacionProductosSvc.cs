﻿namespace CoreFletera.Interfaces
{
    using Core.Models;
    using CoreFletera.BusinessLogic;
    using CoreFletera.Models;
    using System;

    public class ClasificacionProductosSvc : IClasificacionProductos
    {
        public OperationResult getAllClasificacionProductos() =>
            new ClasificacionProductosServices().getAllClasificacionProductos();

        public OperationResult saveClasificacionProductos(ref ClasificacionProductos clasificacion) =>
            new ClasificacionProductosServices().saveClasificacionProductos(ref clasificacion);
    }
}
