﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.BusinessLogic;
namespace CoreFletera.Interfaces
{
    public class MantenimientoSvc : IMantenimiento
    {
        public OperationResult getMantenimientoByFiltros(DateTime fechaInicio, DateTime fechaFin, List<string> listaUnidades, string tipoOrdenSer = "%", string tipoServicio = "%")
        {
            return new MantenimientoServices().getMantenimientoByFiltros(fechaInicio, fechaFin, listaUnidades, tipoOrdenSer, tipoServicio);
        }
    }
}
