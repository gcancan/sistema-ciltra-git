﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using Core.BusinessLogic;
namespace Core.Interfaces
{
    public class PerfilReporteSvc : IPerfilReporte
    {
        public OperationResult getPerfilesReportes(int idUsuario, enumPerfilRporte tipoReporte)
        {
            return new PerfilReporteServices().getPerfilesReportes(idUsuario, tipoReporte);
        }

        public OperationResult savePerfilReporte(ref PerfilReporte perfilReporte)
        {
            return new PerfilReporteServices().savePerfilReporte(ref perfilReporte);
        }
    }
}
