﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.BusinessLogic;
using CoreFletera.Models;

namespace CoreFletera.Interfaces
{
    public class ColoniaSvc : IColonia
    {
        public OperationResult saveColonia(ref Colonia colonia) =>
            new ColoniaServices().saveColonia(ref colonia);
    }
}
