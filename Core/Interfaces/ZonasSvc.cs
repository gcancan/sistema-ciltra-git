﻿namespace Core.Interfaces
{
    using Core.BusinessLogic;
    using Core.Models;
    using CoreFletera.Models;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    public class ZonasSvc : IZonas
    {
        public OperationResult borrarOrigenDestino(OrigenDestinos relacion) =>
            new ZonaServices().borrarOrigenDestino(relacion);

        public OperationResult borrarOrigenDestinoByRuta(int idOrigen, int idDestino, int idCliente) =>
            new ZonaServices().borrarOrigenDestinoByRuta(idOrigen, idDestino, idCliente);

        public OperationResult deleteRutasDestino(Zona zona) =>
            new ZonaServices().deleteRutasDestino(zona);

        public OperationResult getDestinosAtlante(int idEmpresa) =>
            new OrigenServices().getDestinosAtlante(idEmpresa);

        public OperationResult getDestinosByIdEmpresa(int idEmpresa) =>
            new ZonaServices().getDestinosByIdEmpresa(idEmpresa);

        public OperationResult getDestinosByOrigenAndIdCliente(int idCliente, int idOrigen) =>
            new ZonaServices().getDestinosByOrigenAndIdCliente(idCliente, idOrigen);

        public OperationResult getDestinosIncompletosAllById(int id = 0, string idCliente = "") =>
            new ZonaServices().getDestinosIncompletosAllById(id, idCliente);

        public OperationResult getOrigenByIdCliente(int clave) =>
            new ZonaServices().getOrigenByIdCliente(clave);
        public OperationResult getOrigenByIdEmpresa(int clave) =>
            new ZonaServices().getOrigenByIdEmpresa(clave);

        public OperationResult getPrecios(int idCliente, eTiposPrecios tipoPrecio, int idOrigen) =>
            new ZonaServices().getPrecios(idCliente, tipoPrecio, idOrigen);

        public OperationResult getRelacionesOrigenDestinosByIdCliente(int idCliente) =>
            new ZonaServices().getRelacionesOrigenDestinosByIdCliente(idCliente);

        public OperationResult getZonasALLorById(int id = 0, string idCliente = "")
        {
            try
            {
                return new ZonaServices().getZonasALLorById(id, idCliente);
            }
            catch (Exception exception)
            {
                return new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
        }

        public OperationResult getZonasByPrecioAndOrigen(int idCliente, eTiposPrecios tipoPrecio, int idOrigen, decimal precio) =>
            new ZonaServices().getZonasByPrecioAndOrigen(idCliente, tipoPrecio, idOrigen, precio);

        public OperationResult saveDestino_v2(ref Zona zona) =>
            new ZonaServices().saveDestino_v2(ref zona);

        public OperationResult saveNuevosPrecios(int idCliente, eTiposPrecios tipoPrecio, decimal precioAnterior, decimal precioNuevo, List<Zona> list, string usuario) =>
            new ZonaServices().saveNuevosPrecios(idCliente, tipoPrecio, precioAnterior, precioNuevo, list, usuario);

        public OperationResult saveOrigenDestino(ref OrigenDestinos relacion) =>
            new ZonaServices().saveOrigenDestino(ref relacion);

        public OperationResult saveRutasDestino(ref Zona zona) =>
            new ZonaServices().saveRutasDestino(ref zona);

        public OperationResult saveRutasDestinoAtlante(ref Zona destino) =>
            new ZonaServices().saveRutasDestinoAtlante(ref destino);

        public OperationResult saveRutasDestinoIncompletos(Zona zona) =>
            new ZonaServices().saveRutasDestinoIncompletos(zona);
        public OperationResult getZonasByEmpresaCliente(int idEmpresa, int idCliente) =>
            new ZonaServices().getZonasByEmpresaCliente(idEmpresa, idCliente);
        public OperationResult saveNewOrigenPegaso(ref OrigenPegaso origen) =>
            new ZonaServices().saveNewOrigenPegaso(ref origen);
        public OperationResult getAnticipos(int idDestino) =>
            new ZonaServices().getAnticipos(idDestino);
        public OperationResult getCasetasByDestino(int idDestino) =>
            new ZonaServices().getCasetasByDestino(idDestino);
        public OperationResult getAnticiposByNumViaje(int idViaje) =>
            new ZonaServices().getAnticiposByNumViaje(idViaje);
    }
}
