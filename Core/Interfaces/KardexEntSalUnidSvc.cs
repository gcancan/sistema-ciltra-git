﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.BusinessLogic;
namespace CoreFletera.Interfaces
{
    public class KardexEntSalUnidSvc : IKardexEntSalUnid
    {
        public OperationResult getKardexEntSalByFechas(DateTime fechaInicial, DateTime fechaFinal, string idUnidadTrans)
        {
            try
            {
                OperationResult resp = new KardexEntSalUnidService().getKardexEntSalByFechas(fechaInicial, fechaFinal, idUnidadTrans);
                if (resp.typeResult == ResultTypes.success)
                {
                    List<KardexEntSalUnid> listKardex = (List<KardexEntSalUnid>)resp.result;
                    foreach (KardexEntSalUnid kardex in listKardex)
                    {
                        OperationResult repDet = new KardexEntSalUnidService().getDetalleKardex(kardex);
                        if (repDet.typeResult == ResultTypes.success || repDet.typeResult == ResultTypes.recordNotFound)
                        {
                            kardex.listaDetalles = (List<string>)repDet.result;
                        }

                        else
                        {
                            return repDet;
                        }
                    }
                    return resp;
                }
                else
                {
                    return resp;
                }
            }
            catch (Exception EX)
            {
                return new OperationResult { valor = 1, mensaje = EX.Message };
            }
        }
    }
}
