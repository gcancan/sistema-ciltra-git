﻿namespace CoreFletera.Interfaces
{
    using Core.Models;
    using CoreFletera.BusinessLogic;
    using CoreFletera.Models;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;
    public class LlantaSvc : ILlanta
    {
        public OperationResult asignarOrdenLlantera(int idOrden, Usuario usuario, Personal personalEntrega, Personal personalResp, Personal personalAyu1) =>
            new LlantasServices().asignarOrdenLlantera(idOrden, usuario, personalEntrega, personalResp, personalAyu1);

        public OperationResult finalizarOrdenesTrabajo(List<DetalleLlantera> lista, int idOrdenSer) =>
            new LlantasServices().finalizarOrdenesTrabajo(lista, idOrdenSer);

        public OperationResult getAllLLantasProducto() =>
            new LlantasServices().getAllLLantasProducto();

        public OperationResult getCondicionLlanta() =>
            new LlantasServices().getCondicionLlanta();

        public OperationResult getDetallesServLlantera(int idOrdenServ) =>
            new LlantasServices().getDetallesServLlantera(idOrdenServ);

        public OperationResult getFoliosLlantaByIdOrden(int idOrdenCompra, int idProductoLlanta) =>
            new LlantasServices().getFoliosLlantaByIdOrden(idOrdenCompra, idProductoLlanta);

        public OperationResult getLLantaByPosAndUnidadTrans(int posicion, UnidadTransporte unidadTransporte) =>
            new LlantasServices().getLLantaByPosAndUnidadTrans(posicion, unidadTransporte);

        public OperationResult getLlantasByIdEmpresa(List<string> listaEmpresa) =>
            new LlantasServices().getLlantasByIdEmpresa(listaEmpresa);

        public OperationResult getLlantasByOrdenCompra(OrdenCompra ordenCompra) =>
            new LlantasServices().getLlantasByOrdenCompra(ordenCompra);

        public OperationResult getLlantasEnAlmacen(int idAlmacen = 0, string folioLlanta = "%") =>
            new LlantasServices().getLlantasEnAlmacen(idAlmacen, folioLlanta);

        public OperationResult getLlantasParaRevitalizar(int idEmpresa, decimal profundidadInicial, decimal profundidadFinal) =>
            new LlantasServices().getLlantasParaRevitalizar(idEmpresa, profundidadInicial, profundidadFinal);

        public OperationResult getLlantasPendientesByIdOrdenCompra(int idOrdenCompra) =>
            new LlantasServices().getLlantasPendientesByIdOrdenCompra(idOrdenCompra);

        public OperationResult getMedidasLlantas() =>
            new LlantasServices().getMedidasLlantas();

        public OperationResult getProductoLlantasByMedida(string medida) =>
            new LlantasServices().getProductoLlantasByMedida(medida);

        public OperationResult getTiposLlantas() =>
            new LlantasServices().getTiposLlantas();

        public OperationResult saveLlanta(ref Llanta llanta, string usuario) =>
            new LlantasServices().saveLlanta(ref llanta, usuario);

        public OperationResult saveLlantasFromOrdenCompra(List<Llanta> listaLlantas, List<RelacionDetalleOrdenFactura> listaRelacion, string nombreUsuario, OrdenCompra ordenCompra) =>
            new LlantasServices().saveLlantasFromOrdenCompra(listaLlantas, listaRelacion, nombreUsuario, ordenCompra);

        public OperationResult saveProductoLlanta(ref LLantasProducto lLantasProducto) =>
            new LlantasServices().saveProductoLlanta(ref lLantasProducto);
    }
}
