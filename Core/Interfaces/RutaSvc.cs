﻿namespace CoreFletera.Interfaces
{
    using Core.Models;
    using CoreFletera.BusinessLogic;
    using CoreFletera.Models;
    using System;

    public class RutaSvc : IRuta
    {
        public OperationResult getAllRutas() =>
            new RutasServices().getAllRutas();

        public OperationResult quitarRelacionRutaCasetas(int idRuta, int idCaseta) =>
            new RutasServices().quitarRelacionRutaCasetas(idRuta, idCaseta);

        public OperationResult saveRuta(ref Ruta ruta) =>
            new RutasServices().saveRuta(ref ruta);
    }
}
