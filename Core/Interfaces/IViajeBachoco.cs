﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Interfaces
{
    public interface IViajeBachoco
    {
        OperationResult saveViajeBachoco(ref ViajeBachoco viajeBachoco);
        OperationResult getViajeBachocoByIdCamion(string idCamion);
        OperationResult cerrarViajeBachoco(int idViaje);
        OperationResult getViajeBachocoByFiltros(DateTime fechaInicio, DateTime fechaFin, string idDestino, string idOperador, string idTractor, string estatus, int idcliente);
        OperationResult getViajeBachocoByFechas(DateTime fechaInicio, DateTime fechaFin);
        OperationResult finalizarViajeBachoco(int idViaje, eEstatus estatus, int idUsuario, string motivo = "");
    }
}
