﻿namespace Core.Interfaces
{
    using Core.BusinessLogic;
    using Core.Models;
    using System;

    public class RegistroKilometrajeSvc : IRegistroKilometraje
    {
        public OperationResult getUltimoKM(string idUnidadTrans) =>
            new RegistroKilometrajeServices().getUltimoKM(idUnidadTrans);

        public OperationResult getUltimoKM(string idUnidadTrans, DateTime fecha) =>
            new RegistroKilometrajeServices().getUltimoKM(idUnidadTrans, fecha);

        public OperationResult getUltimoKMVerificacion(string idUnidadTrans) =>
            new RegistroKilometrajeServices().getUltimoKMVerificacion(idUnidadTrans);

        public OperationResult getUltimoRegistroValido(string idUnidadTrans) =>
            new RegistroKilometrajeServices().getUltimoRegistroValido(idUnidadTrans);

        public OperationResult saveRegistokilometraje(ref RegistroKilometraje registroKM, decimal km) =>
            new RegistroKilometrajeServices().saveRegistokilometraje(ref registroKM, km);
    }
}
