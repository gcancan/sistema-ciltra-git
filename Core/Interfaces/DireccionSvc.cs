﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.BusinessLogic;
namespace CoreFletera.Interfaces
{
    public class DireccionSvc : IDireccion
    {
        public OperationResult getALLColoniasById(int id = 0) =>
            new DireccionServices().getALLColoniasById(id);

        public OperationResult getALLColoniasByNombre(string nombre) =>
            new DireccionServices().getALLColoniasByNombre(nombre);
    }
}
