﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.BusinessLogic;
namespace CoreFletera.Interfaces
{
    public class DetallesConsumoUnidadesSvc : IDetallesConsumoUnidades
    {
        public OperationResult getDetallesConsumoUnidades(int idEmpresa, int idCliente, DateTime fechaInicio, DateTime fechaFin, List<string> listaTractores, bool todos = false)
        {
            return new DetallesConsumoUnidadesServices().getDetallesConsumoUnidades(idEmpresa, idCliente, fechaInicio, fechaFin, listaTractores, todos);
        }
    }
}
