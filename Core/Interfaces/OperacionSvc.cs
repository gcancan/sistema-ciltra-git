﻿namespace Core.Interfaces
{
    using Core.BusinessLogic;
    using Core.Models;
    using CoreFletera.BusinessLogic;
    using CoreFletera.Models;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    public class OperacionSvc : IOperacion
    {
        public OperationResult agregarInsumosExtra(List<ProductosActividad> listProducto, int ordenServ) =>
            new OperacionServices().agregarInsumosExtra(listProducto, ordenServ);

        public OperationResult cancelaValeCombustible(int idVale) =>
            new OperacionServices().cancelaValeCombustible(idVale);

        public OperationResult cargaCombustibleByIdVale(int idVale) =>
            new OperacionServices().cargaCombustibleByIdVale(idVale);

        public OperationResult entregarOrdenServicio(int idOrdenServ, Personal personal, string usuario) =>
            new OperacionServices().entregarOrdenServicio(idOrdenServ, personal, usuario);

        public OperationResult getInsumosByNombre(string nombre = "", bool taller = true, bool lavadero = true, bool llantera = true) =>
            new OperacionServices().getInsumosByNombre(nombre, taller, lavadero, llantera);

        public OperationResult getOrdenServByFechas(DateTime fechaInicio, DateTime fechaFin, int tipoOrden = 0) =>
            new TipoOrdenServicioServices().getOrdenServByFechas(fechaInicio, fechaFin, tipoOrden);

        public OperationResult getOrdenServById(int idOrden) =>
            new TipoOrdenServicioServices().getOrdenServById(idOrden);

        public OperationResult getSalidaEntradaByCamion(string idCamion) =>
            new OperacionServices().getSalidaEntradaByCamion(idCamion);

        public OperationResult getSalidaEntradaByidORden(int idOrden) =>
            new OperacionServices().getSalidaEntradaByidORden(idOrden);

        public OperationResult getSalidasEntradasByIdOrdenServ(int idOrden) =>
            new OperacionServices().getSalidasEntradasByIdOrdenServ(idOrden);

        public OperationResult getTiposOrdenServicio(int idTipoOrdenServicio = 0) =>
            new OperacionServices().getTiposOrdenServicio(idTipoOrdenServicio);

        public OperationResult getUltimaOrdenServDiagByIdUnidadTrans(string unidadTransporte) =>
            new TipoOrdenServicioServices().getUltimaOrdenServDiagByIdUnidadTrans(unidadTransporte);

        public OperationResult saveCargaGasolina(ref CargaCombustible cargaGas, int idEmpresa, KardexCombustible kardexCombustible = null, bool isAjuste = false)
        {
            OperationResult result = new OperacionServices().saveCargaGasolina(ref cargaGas, idEmpresa, kardexCombustible);
            if (result.typeResult == ResultTypes.success)
            {
                new OperacionServices().ligarViajesConVale(cargaGas);
            }
            else
            {
                return result;
            }
            if ((result.typeResult == ResultTypes.success) && isAjuste)
            {
                OperationResult resultKardex = new KardexCombustibleServices().ajustarKardexCombustible(cargaGas.idCargaCombustible, cargaGas.fechaCombustible, cargaGas.ltCombustible, TipoOperacionDiesel.CARGA);
                if (resultKardex.typeResult == ResultTypes.success)
                {
                    if (cargaGas.listaAjustes != null)
                    {
                        var respAjuste = new RegistroAjustesServices().saveRegistrosAjustes(cargaGas.listaAjustes);
                        if (respAjuste.typeResult != ResultTypes.success)
                        {
                            return respAjuste;
                        }
                    }
                    return resultKardex;
                }
            }
            return result;
        }

        public OperationResult saveCargaGasolinaExterno(ref CargaCombustible cargaGas, int idEmpresa, Personal operador, UnidadTransporte tractor, UnidadTransporte remolque1, UnidadTransporte dolly, UnidadTransporte remolque2, ZonaOperativa zonaOperativa = null, string unidadCarga = "") =>
            new OperacionServices().saveCargaGasolinaExterno(ref cargaGas, idEmpresa, operador, tractor, remolque1, dolly, remolque2, zonaOperativa, unidadCarga);

        public OperationResult saveDetOrdenActividadExterno(List<OrdenTaller> listaOrdenes, Proveedor proveedor, string factura) =>
            new OperacionServices().saveDetOrdenActividadExterno(listaOrdenes, proveedor, factura);

        public OperationResult saveOperacion(List<string> lista, eTipoMovimiento tipoMovimiento) =>
            new OperacionServices().saveOperacion(lista, tipoMovimiento);

        public OperationResult savePreOrdenCompraDirecta(ref PreOrdenCompra preOrdenCompra)
        {
            try
            {
                foreach (DetallePreOrdenCompra compra in preOrdenCompra.listaDetalles)
                {
                    OperationResult result = new ProductosCOMServices().sincronizarProductoCOM(compra.producto);
                    if (result.typeResult == ResultTypes.error)
                    {
                        return result;
                    }
                }
                return new OperacionServices().savePreOrdenCompraDirecta(ref preOrdenCompra);
            }
            catch (Exception exception)
            {
                return new OperationResult(exception);
            }
        }

        public OperationResult savePreSalidaAlm(OrdenTaller orden, PreSalidaAlmacen preSalidaAlmacen, List<ProductosCOM> listaProducto, Personal persona, string usuario)
        {
            try
            {
                foreach (ProductosCOM scom in listaProducto)
                {
                    OperationResult result = new ProductosCOMServices().sincronizarProductoCOM(scom);
                    if (result.typeResult == ResultTypes.error)
                    {
                        return result;
                    }
                }
                return new OperacionServices().savePreSalidaAlm(orden, preSalidaAlmacen, listaProducto, persona, usuario);
            }
            catch (Exception exception)
            {
                return new OperationResult(exception);
            }
        }

        public OperationResult saveSalidaEntrada(List<SalidaEntrada> lista) =>
            new OperacionServices().saveSalidaEntrada(lista);

        public OperationResult saveSalidaEntrada(ref SalidaEntrada salidaEntrada, int idUsuario) =>
            new OperacionServices().saveSalidaEntrada(ref salidaEntrada, idUsuario);
        public OperationResult getCargasExtraByVale(int idVale) =>
            new OperacionServices().getCargasExtraByVale(idVale);
    }
}
