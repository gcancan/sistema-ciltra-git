﻿namespace Core.Interfaces
{
    using Core.BusinessLogic;
    using Core.Models;
    using CoreFletera.Models;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    public class CartaPorteSvc : ICartaPorte
    {
        private OperationResult acompletarDatos(List<Viaje> listViajes, bool detalle = true)
        {
            try
            {
                foreach (Viaje viaje in listViajes)
                {
                    if (0x13f2 == viaje.NumGuiaId)
                    {
                    }
                    OperationResult result = new UnidadTransporteSvc().getUnidadesALLorById(0, viaje.tractor.clave);
                    if (result.typeResult == ResultTypes.success)
                    {
                        viaje.tractor = ((List<UnidadTransporte>)result.result)[0];
                    }
                    else
                    {
                        return result;
                    }
                    if (viaje.remolque != null)
                    {
                        result = new UnidadTransporteSvc().getUnidadesALLorById(0, viaje.remolque.clave);
                        if (result.typeResult == ResultTypes.success)
                        {
                            viaje.remolque = ((List<UnidadTransporte>)result.result)[0];
                        }
                        else
                        {
                            return result;
                        }
                    }

                    if (viaje.dolly != null)
                    {
                        result = new UnidadTransporteSvc().getUnidadesALLorById(0, viaje.dolly.clave);
                        if (result.typeResult != ResultTypes.success)
                        {
                            return result;
                        }
                        viaje.dolly = ((List<UnidadTransporte>)result.result)[0];
                    }
                    if (viaje.remolque2 != null)
                    {
                        result = new UnidadTransporteSvc().getUnidadesALLorById(0, viaje.remolque2.clave);
                        if (result.typeResult != ResultTypes.success)
                        {
                            return result;
                        }
                        viaje.remolque2 = ((List<UnidadTransporte>)result.result)[0];
                    }
                    if (detalle)
                    {
                        if (viaje.IdEmpresa == 1)
                        {
                            result = new CartaPorteServices().getDetallesCartaPorteById(viaje.NumGuiaId, (viaje.cliente == null ? 0 : viaje.cliente.clave));
                        }
                        else if (viaje.IdEmpresa == 5)
                        {
                            result = new CartaPorteServices().getDetallesCartaPorteByIdGlobal(viaje.NumGuiaId, (viaje.cliente == null ? 0 : viaje.cliente.clave));
                        }
                        else
                        {
                            result = new CartaPorteServices().getDetallesCartaPorteByIdAtlante(viaje.NumGuiaId, viaje.cliente.clave);
                        }
                        if ((result.typeResult == ResultTypes.success) || (result.typeResult == ResultTypes.recordNotFound))
                        {
                            viaje.listDetalles = (List<CartaPorte>)result.result;
                        }
                        else
                        {
                            return result;
                        }
                    }
                }
                return new OperationResult
                {
                    valor = 0,
                    mensaje = "Busqueda exitosa",
                    result = listViajes
                };
            }
            catch (Exception exception)
            {
                return new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
        }

        private OperationResult acompletarDatos(List<Viaje> listViajes, string strConnection, bool detalle = true)
        {
            try
            {
                foreach (Viaje viaje in listViajes)
                {
                    OperationResult result = new OperationResult();
                    if (detalle)
                    {
                        if (viaje.IdEmpresa == 1)
                        {
                            result = new CartaPorteServices().getDetallesCartaPorteById(viaje.NumGuiaId, viaje.cliente.clave, strConnection);
                        }
                        else
                        {
                            result = new CartaPorteServices().getDetallesCartaPorteByIdAtlante(viaje.NumGuiaId, viaje.cliente.clave, strConnection);
                        }
                        if ((result.typeResult == ResultTypes.success) || (result.typeResult == ResultTypes.recordNotFound))
                        {
                            viaje.listDetalles = (List<CartaPorte>)result.result;
                        }
                        else
                        {
                            return result;
                        }
                    }
                }
                return new OperationResult
                {
                    valor = 0,
                    mensaje = "Busqueda exitosa",
                    result = listViajes
                };
            }
            catch (Exception exception)
            {
                return new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
        }

        public OperationResult activarViajeBachoco(int idViaje, eEstatus estatus)
        {
            try
            {
                OperationResult result = new CartaPorteServices().activarViajeBachoco(idViaje, estatus);
                if (result.typeResult == ResultTypes.success)
                {
                    List<Viaje> listViajes = new List<Viaje> {
                        (Viaje) result.result
                    };
                    return this.acompletarDatos(listViajes, true);
                }
                return result;
            }
            catch (Exception exception)
            {
                return new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
        }

        public OperationResult asignarCargaCombustibleViaje(int idVale, int idViaje) =>
            new CartaPorteServices().asignarCargaCombustibleViaje(idVale, idViaje);

        public OperationResult cambiarValesCarga(List<ValeCarga> listVales, int valeGasolina, int userFin, DateTime fechaInicio, DateTime fechaFin, bool finalizar = true) =>
            new CartaPorteServices().cambiarValesCarga(listVales, valeGasolina, userFin,  fechaInicio, fechaFin, finalizar);

        public OperationResult cancelarViajeBachoco(int numGuiaId, eEstatus estatus, string motivo) =>
            new CartaPorteServices().cancelarViajeBachoco(numGuiaId, estatus, motivo);

        public OperationResult cerrarCancelarCartaPorte(int idCartaPorte, TipoAccion tipoAccion, int idUsuario, string motivo = "", DateTime? fecha = new DateTime?()) =>
            new CartaPorteServices().cerrarCancelarCartaPorte(idCartaPorte, tipoAccion, idUsuario, motivo, fecha);

        public OperationResult checarCartaPorte(ref Viaje viaje, int idUsuario) =>
            new CartaPorteServices().checarCartaPorte(ref viaje, idUsuario);

        public OperationResult ConsultarViajesActivos(int idCliente) =>
            new CartaPorteServices().ConsultarViajesActivos(idCliente);

        public OperationResult ConsultarViajesFinalizados(int idCliente) =>
            new CartaPorteServices().ConsultarViajesFinalizados(idCliente);

        public OperationResult getCartaPorteByFecha(DateTime fechaInicio, DateTime fechaFin, string idCliente, int idEmpresa)
        {
            try
            {
                OperationResult result = new CartaPorteServices().getCartaPorteByFecha(fechaInicio, fechaFin, idCliente, idEmpresa);
                if (result.typeResult == ResultTypes.success)
                {
                    List<Viaje> listViajes = (List<Viaje>)result.result;
                    return this.acompletarDatos(listViajes, true);
                }
                return result;
            }
            catch (Exception exception)
            {
                return new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
        }

        public OperationResult getCartaPorteByfiltros(DateTime fechaInicio, DateTime fechaFin, string idDestino, string idOperador, string idTractor, string estatus, string idcliente, int idEmpresa, string tipoViaje = "%")
        {
            try
            {
                OperationResult result = new CartaPorteServices().getCartaPorteByfiltros(fechaInicio, fechaFin, idDestino, idOperador, idTractor, estatus, idcliente, idEmpresa, tipoViaje);
                if (result.typeResult == ResultTypes.success)
                {
                    List<Viaje> list = (List<Viaje>)result.result;
                    int num = 0;
                    foreach (Viaje viaje in list)
                    {
                        OperationResult result2 = new OperationResult();
                        if (viaje.IdEmpresa == 1)
                        {
                            result2 = new CartaPorteServices().getDetallesCartaPorteById(viaje.NumGuiaId, viaje.cliente.clave);
                        }
                        else
                        {
                            result2 = new CartaPorteServices().getDetallesCartaPorteByIdAtlante(viaje.NumGuiaId, viaje.cliente.clave);
                        }
                        if ((result2.typeResult == ResultTypes.success) || (result2.typeResult == ResultTypes.recordNotFound))
                        {
                            viaje.listDetalles = (List<CartaPorte>)result2.result;
                        }
                        else
                        {
                            return result2;
                        }
                        num++;
                    }
                }
                return result;
            }
            catch (Exception exception)
            {
                return new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
        }

        public OperationResult getCartaPorteById(int clave)
        {
            try
            {
                OperationResult result = new CartaPorteServices().getCartaPorteById(clave);
                if (result.typeResult == ResultTypes.success)
                {
                    List<Viaje> listViajes = (List<Viaje>)result.result;
                    return this.acompletarDatos(listViajes, true);
                }
                return result;
            }
            catch (Exception exception)
            {
                return new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
        }

        public OperationResult getCartaPorteById(int clave, string strConnection)
        {
            try
            {
                OperationResult result = new CartaPorteServices().getCartaPorteById(clave, strConnection);
                if (result.typeResult == ResultTypes.success)
                {
                    List<Viaje> listViajes = (List<Viaje>)result.result;
                    return this.acompletarDatos(listViajes, strConnection, true);
                }
                return result;
            }
            catch (Exception exception)
            {
                return new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
        }

        public OperationResult getCartaPorteByIdCamion(string idCamion)
        {
            try
            {
                OperationResult result = new CartaPorteServices().getCartaPorteByIdCamion(idCamion);
                if (result.typeResult == ResultTypes.success)
                {
                    List<Viaje> listViajes = (List<Viaje>)result.result;
                    return this.acompletarDatos(listViajes, true);
                }
                return result;
            }
            catch (Exception exception)
            {
                return new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
        }

        public OperationResult getCartaPortesFinalizados(DateTime fechaInicio, DateTime fechaFin, int idCliente = 0)
        {
            try
            {
                OperationResult result = new CartaPorteServices().getCartaPortesFinalizados(fechaInicio, fechaFin, idCliente);
                if (result.typeResult == ResultTypes.success)
                {
                    List<Viaje> listViajes = (List<Viaje>)result.result;
                    return this.acompletarDatos(listViajes, false);
                }
                return result;
            }
            catch (Exception exception)
            {
                return new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
        }

        public OperationResult getResumenCartaPortesByidCargaCombustible(int idCargaCombustible) =>
            new CartaPorteServices().getResumenCartaPortesByidCargaCombustible(idCargaCombustible);

        public OperationResult getUltimoViajeByIdTractor(string idTractor)
        {
            OperationResult result = new CartaPorteServices().getUltimoViajeByIdTractor(idTractor);
            if (result.typeResult == ResultTypes.success)
            {
                int clave = (int)result.result;
                return this.getCartaPorteById(clave);
            }
            return result;
        }

        public OperationResult getValesCargaByIdViaje(Viaje viaje) =>
            new CartaPorteServices().getValesCargaByIdViaje(viaje);

        public OperationResult getViajeByEventoCSI(DateTime fecha, string idTractor) =>
            new CartaPorteServices().getViajeByEventoCSI(fecha, idTractor);

        public OperationResult getViajesActivosByIdTractor(string idTractor) =>
            new CartaPorteServices().getViajesActivosByIdTractor(idTractor);

        public OperationResult getViajesOperador(int idOperador, DateTime fechaInicio, DateTime fechaFin) =>
            new CartaPorteServices().getViajesOperador(idOperador, fechaInicio, fechaFin);

        public OperationResult getViajesParaTimbrar()
        {
            try
            {
                OperationResult result = new CartaPorteServices().getViajesParaTimbrar();
                if (result.typeResult == ResultTypes.success)
                {
                    List<Viaje> list = (List<Viaje>)result.result;
                    int num = 0;
                    foreach (Viaje viaje in list)
                    {
                        OperationResult result2 = new OperationResult();
                        if (viaje.IdEmpresa == 1)
                        {
                            result2 = new CartaPorteServices().getDetallesCartaPorteById(viaje.NumGuiaId, viaje.cliente.clave);
                        }
                        else
                        {
                            result2 = new CartaPorteServices().getDetallesCartaPorteByIdAtlante(viaje.NumGuiaId, viaje.cliente.clave);
                        }
                        if ((result2.typeResult == ResultTypes.success) || (result2.typeResult == ResultTypes.recordNotFound))
                        {
                            viaje.listDetalles = (List<CartaPorte>)result2.result;
                        }
                        else
                        {
                            return result2;
                        }
                        num++;
                    }
                }
                return result;
            }
            catch (Exception exception)
            {
                return new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
        }

        public OperationResult saveAjusteViaje(ref Viaje viaje, int? idOperadorFinaliza = new int?()) =>
            new CartaPorteServices().saveAjusteViaje(ref viaje, idOperadorFinaliza);

        public OperationResult saveCartaPorte(Viaje cartaPorte, ref int ordenTrabajo, bool omitirRegraRemisiones = false, bool genSalida = false, bool genOrdenCom = false) =>
            new CartaPorteServices().saveCartaPorte(cartaPorte, ref ordenTrabajo, omitirRegraRemisiones, genSalida, genOrdenCom);

        public OperationResult saveCartaPorte_V2(ref Viaje cartaPorte, ref int ordenTrabajo, bool omitirRegraRemisiones = false, bool genSalida = false, bool genOrdenCom = false) =>
            new CartaPorteServices().saveCartaPorte_V2(ref cartaPorte, ref ordenTrabajo, omitirRegraRemisiones, genSalida, genOrdenCom);

        public OperationResult saveCartaPorteAtlante(Viaje cartaPorte, bool omitirRegraRemisiones = false) =>
            new CartaPorteServices().saveCartaPorteAtlante(cartaPorte, omitirRegraRemisiones);

        public OperationResult saveDetalleViaje(List<CartaPorte> listaDetalles, int numGuiaId) =>
            new CartaPorteServices().saveDetalleViaje(listaDetalles, numGuiaId);

        public OperationResult saveRemisionesLiberadas(Viaje viaje) =>
            new CartaPorteServices().saveRemisionesLiberadas(viaje);

        public OperationResult saveValesCarga(List<ValeCarga> listVales) =>
            new CartaPorteServices().saveValesCarga(listVales);

        public void upDateDetalles(CartaPorte items)
        {
            throw new NotImplementedException();
        }

        public OperationResult getDatosUltimoViajeByTC(string tc) =>
            new CartaPorteServices().getDatosUltimoViajeByTC(tc);

        public OperationResult getUltimaFechaEntradaPlanta(string tractor, DateTime? fecha = null) =>
            new CartaPorteServices().getUltimaFechaEntradaPlanta(tractor, fecha);
        public OperationResult ligarViajeVale(int idViaje, int idVale) =>
            new CartaPorteServices().ligarViajeVale(idViaje, idVale);
        public OperationResult getNoViajeOperador(DateTime fecha, int idOperador) =>
            new CartaPorteServices().getNoViajeOperador(fecha, idOperador);
        public OperationResult getViajeByEventoCSI(ref List<ParadaEquipo> listaParadas) =>
            new CartaPorteServices().getViajeByEventoCSI(ref listaParadas);
        public OperationResult bajarRemisiones(List<string> listaConsecutivos, int idUsuario, string motivo) =>
            new CartaPorteServices().bajarRemisiones(listaConsecutivos, idUsuario, motivo);
        public OperationResult sincronizarViajes(List<ReporteCartaPorteViajes> listViajes, List<ReporteCartaPorte> listaCartaPortes) =>
            new CartaPorteServices().sincronizarViajes(listViajes, listaCartaPortes);
        public OperationResult saveComentarioViajes(int numViaje, string usuario, string comentario, DateTime fecha) =>
            new CartaPorteServices().saveComentarioViajes(numViaje, usuario, comentario, fecha);
        public OperationResult getListaComentariosViajes(List<string> listaId) =>
            new CartaPorteServices().getListaComentariosViajes(listaId);
        public OperationResult getAnticipoMasterByFolioViaje(int folioViaje) =>
            new CartaPorteServices().getAnticipoMasterByFolioViaje(folioViaje);
    }
}
