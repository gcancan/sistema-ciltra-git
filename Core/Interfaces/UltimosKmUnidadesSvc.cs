﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.BusinessLogic;
namespace CoreFletera.Interfaces
{
    public class UltimosKmUnidadesSvc : IUltimosKmUnidades
    {
        public OperationResult getUltimosKmUnidades(int idEmpresa, List<string> listaZonas, List<string> listaOperaciones) =>
            new UltimosKmUnidadesServices().getUltimosKmUnidades(idEmpresa, listaZonas, listaOperaciones);
    }
}
