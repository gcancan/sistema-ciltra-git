﻿namespace CoreFletera.Interfaces
{
    using Core.Models;
    using CoreFletera.Models;

    internal interface IUsuarioCSI
    {
        OperationResult getAllUsuariosCSI();
        OperationResult saveUsuarioCSI(ref UsuarioCSI usuarioCSI);
    }
}
