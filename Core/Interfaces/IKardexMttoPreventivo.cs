﻿using Core.Models;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Interfaces
{
    interface IKardexMttoPreventivo
    {
        OperationResult getKardexMttoPreventivo(List<string> listaUnidades, List<string> listaServicios, DateTime fechaInicio, DateTime fechaFin);
        OperationResult getInsersionKm(List<string> listaUnidades, DateTime fechaInicio, DateTime fechaFin, enumTipoInsersionKM enumTipoInsersionKM);
        OperationResult getKardexMttoPreventivo(List<string> listaUnidades);
    }
}
