﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.BusinessLogic;
namespace CoreFletera.Interfaces
{
    public class IngresosEgresosSvc : IIngresosEgresos
    {
        public OperationResult getEgresos(string idUnidad, DateTime fechaInicial, DateTime fechaFinal)
        {
            return new IngresosEgresosServices().getEgresos(idUnidad, fechaInicial, fechaFinal);
        }

        public OperationResult getEgresosDetallado(string idUnidad, DateTime fechaInicial, DateTime fechaFinal)
        {
            return new IngresosEgresosServices().getEgresosDetallado(idUnidad, fechaInicial, fechaFinal);
        }

        public OperationResult getIngresos(string idUnidad, DateTime fechaInicial, DateTime fechaFinal)
        {
            return new IngresosEgresosServices().getIngresos(idUnidad, fechaInicial, fechaFinal);
        }

        public OperationResult getIngresosDetalles(string idUnidad, DateTime fechaInicial, DateTime fechaFinal)
        {
            return new IngresosEgresosServices().getIngresosDetalles(idUnidad, fechaInicial, fechaFinal);
        }

        public OperationResult getIngresosEgresos(DateTime fechaInicial, DateTime fechaFinal, int idEmpresa, List<string> listaUnidades)
        {
            return new IngresosEgresosServices().getIngresosEgresos(fechaInicial, fechaFinal, idEmpresa, listaUnidades);
        }
    }
}
