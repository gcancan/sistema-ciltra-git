﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.BusinessLogic;
using CoreFletera.Models;

namespace CoreFletera.Interfaces
{
    public class KardexMttoPreventivoSvc : IKardexMttoPreventivo
    {
        public OperationResult getInsersionKm(List<string> listaUnidades, DateTime fechaInicio, DateTime fechaFin, enumTipoInsersionKM enumTipoInsersionKM) =>
            new KardexMttoPreventivoServices().getInsersionKm(listaUnidades, fechaInicio, fechaFin, enumTipoInsersionKM);

        public OperationResult getKardexMttoPreventivo(List<string> listaUnidades, List<string> listaServicios, DateTime fechaInicio, DateTime fechaFin) =>
            new KardexMttoPreventivoServices().getKardexMttoPreventivo(listaUnidades, listaServicios, fechaInicio, fechaFin);
        public OperationResult getKardexMttoPreventivo(List<string> listaUnidades) =>
            new KardexMttoPreventivoServices().getKardexMttoPreventivo(listaUnidades);
    }
}
