﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.BusinessLogic;
namespace CoreFletera.Interfaces
{
    public class ProveedorSvc : IProveedor
    {
        public OperationResult getAllProveedores()
        {
            return new ProveedorServices().getAllProveedores();
        }

        public OperationResult getAllProveedoresBiIdMarca(int idMarca)
        {
            return new ProveedorServices().getAllProveedoresBiIdMarca(idMarca);
        }

        public OperationResult saveProveedor(ref ProveedorCombustible proveedor) =>
            new ProveedorServices().saveProveedor(ref proveedor);
        public OperationResult getAllProveedoresV2() =>
            new ProveedorServices().getAllProveedoresV2();

        public OperationResult getAllProveedoresUnidades() =>
            new ProveedorServices().getAllProveedoresUnidades();
    }
}
