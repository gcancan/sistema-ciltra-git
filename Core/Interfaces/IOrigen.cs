﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Interfaces
{
    interface IOrigen
    {
        OperationResult getOrigenesAll(int idCliente = 0, int idOrigen = 0);
        OperationResult saveOrigenCliente(ref Origen origen);
        OperationResult saveOrigenCliente_v2(ref Origen origen);
        OperationResult getOrigenesAll();
    }
}
