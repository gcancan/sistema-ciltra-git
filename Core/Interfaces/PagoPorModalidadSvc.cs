﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.BusinessLogic;
namespace CoreFletera.Interfaces
{
    public class PagoPorModalidadSvc : IPagoPorModalidad
    {
        public OperationResult getPagoModalidadByIdCliente(int idCliente)
        {
            return new PagoPorModalidadServices().getPagoModalidadByIdCliente(idCliente);
        }

        public OperationResult getPagoModalidadByNoViaje(int idClasificacion, int noViaje)
        {
            return new PagoPorModalidadServices().getPagoModalidadByNoViaje(idClasificacion, noViaje);
        }

        public OperationResult savePagoPorModalidad(ref PagoPorModalidad pagoXmodalidad)
        {
            return new PagoPorModalidadServices().savePagoPorModalidad(ref pagoXmodalidad);
        }
    }
}
