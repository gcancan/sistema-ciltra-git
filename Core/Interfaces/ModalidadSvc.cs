﻿using Core.Models;
using CoreFletera.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Interfaces
{
    public class ModalidadSvc : IModalidad
    {
        public OperationResult getAllModalidades() =>
            new ModalidadService().getAllModalidades();
    }
}
