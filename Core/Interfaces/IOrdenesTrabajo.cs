﻿using Core.Models;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Interfaces
{
    interface IOrdenesTrabajo
    {
        OperationResult getRelacionServicios(int idTipoOrden);
        OperationResult getTipoDeOrden(int idTipoOrdenServ);
        OperationResult getRelacionActividad(int idTipoOrden);
        OperationResult getDivisiones();
        OperationResult saveDetOrdenServ(List<DetOrdenServicio> list);
        OperationResult getAllServicios();
        OperationResult getAllActividades();
        OperationResult saveRelServAct(Servicio servicio, List<Actividad> listActividad, bool activo = true);
        OperationResult enviarTallerExterno(int idOrdenServicio);
    }
}
