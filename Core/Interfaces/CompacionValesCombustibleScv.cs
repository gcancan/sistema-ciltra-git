﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.BusinessLogic;
using CoreFletera.Models;

namespace CoreFletera.Interfaces
{
    public class CompacionValesCombustibleScv : ICompacionValesCombustible
    {
        public OperationResult getListaVales(string unidadTras, int mes, int año)
        {
            return new CompacionValesCombustibleServices().getListaVales(unidadTras, mes, año);
        }

        public OperationResult getListaValesLis(string unidadTras, string mes, int año)
        {
            return new CompacionValesCombustibleServices().getListaValesLis(unidadTras, mes, año);
        }

        public OperationResult saveValesLIS(List<ComparacionValesCombustible> lista)
        {
            return new CompacionValesCombustibleServices().saveValesLIS(lista);
        }
    }
}
