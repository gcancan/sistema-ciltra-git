﻿using Core.Models;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Interfaces
{
    interface IPresupuesto
    {
        OperationResult savePresupuesto(ref Presupuesto presupuesto);
        OperationResult getAllPresupuestos();
        OperationResult getDetallePresupuestosByClave(int clave);
        OperationResult getAllPresupuestosByClave(int clave);
        OperationResult bajarDetallePresupuesto(int idPresupuesto, int idModalidad, string usuario, string motivo);
        OperationResult getAllIndicadoresViajes(int año, List<string> listaIdClientes);
        OperationResult getPresupuestosByClientes(int año, List<string> listaIdClientes);
        OperationResult getAllIndicadoresCombustible(int año, List<string> listaZonaOpe, List<string> listaIdClientes);
    }
}
