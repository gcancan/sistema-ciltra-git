﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.BusinessLogic;
using CoreFletera.Models;
namespace CoreFletera.Interfaces
{
    public class BitacoraEntradaSalidasSvc : IBitacoraEntradaSalidas
    {
        public OperationResult getBitacoraEntradaSalidas(DateTime fechaInicial, DateTime fechaFinal, int idEmpresa)
        {
            //return new BitacoraEntradaSalidasService().getBitacoraEntradaSalidas(fechaInicial, fechaFinal, idEmpresa);
            try
            {
                OperationResult resp = new BitacoraEntradaSalidasService().getBitacoraEntradaSalidas(fechaInicial, fechaFinal, idEmpresa);
                if (resp.typeResult == ResultTypes.success)
                {
                    List<BitacoraEntradaSalidas> list = (List<BitacoraEntradaSalidas>)resp.result;
                    foreach (BitacoraEntradaSalidas item in list)
                    {
                        OperationResult listMotivos = new BitacoraEntradaSalidasService().getMotivosEntrada(item.folio, item.isOrden, item.unidad);
                        if (listMotivos.typeResult == ResultTypes.success)
                        {
                            item.listMotivos = (List<string>)listMotivos.result;
                        }
                        else if (listMotivos.typeResult == ResultTypes.error)
                        {
                            return listMotivos;
                        }
                    }
                    return resp;
                }
                else
                {
                    return resp;
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }
    }
}
