﻿namespace Core.Interfaces
{
    using Core.BusinessLogic;
    using Core.Models;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    public class ClienteSvc : ICliente
    {
        public OperationResult getClientesALLorById(int idEmpresa, int id = 0, bool omitirProductos = false)
        {
            try
            {
                OperationResult result = new ClienteServices().getClientesALLorById(idEmpresa, id);
                if (result.typeResult == ResultTypes.success)
                {
                    List<Cliente> list = (List<Cliente>)result.result;
                    if (!omitirProductos)
                    {
                        foreach (Cliente cliente in list)
                        {
                            OperationResult result2 = new FraccionSvc().getFraccionALLorById(cliente.clave);
                            if (result2.typeResult == ResultTypes.success)
                            {
                                cliente.fraccion = ((List<Fraccion>)result2.result)[0];
                            }
                            else
                            {
                                return result2;
                            }
                        }
                    }
                    result.result = list;
                }
                return result;
            }
            catch (Exception exception)
            {
                return new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
        }

        public OperationResult getClientesByRuta(int idOrigen, int idDestino, int? idClasificacion) =>
            new ClienteServices().getClientesByRuta(idOrigen, idDestino, idClasificacion);
        public OperationResult getClientesByZonasOperativas(List<string> listaId, int idEmpresa) =>
            new ClienteServices().getClientesByZonasOperativas(listaId, idEmpresa);

        public OperationResult saveCliente(ref Cliente cliente) =>
            new ClienteServices().saveCliente(ref cliente);
        public OperationResult getAllClienteByEmpresa(int idEmpresa) =>
            new ClienteServices().getAllClienteByEmpresa(idEmpresa);

        public OperationResult sincronizarClientes(int idEmpresa, List<Cliente> listaCliente) =>
            new ClienteServices().sincronizarClientes(idEmpresa, listaCliente);
    }
}
