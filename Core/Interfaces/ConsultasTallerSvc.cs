﻿namespace CoreFletera.Interfaces
{
    using Core.Models;
    using CoreFletera.BusinessLogic;
    using CoreFletera.Models;
    using System;
    using System.Runtime.InteropServices;

    public class ConsultasTallerSvc : IConsultasTaller
    {
        public OperationResult consultarExistenByProducto(int idProducto) =>
            new ConsultasTaller().consultarExistenByProducto(idProducto);

        public OperationResult getActividadesCOM(AreaTrabajo area, string parametro = "%") =>
            new ConsultasTaller().getActividadesCOM(area, parametro);

        public OperationResult getProductosCOM(AreaTrabajo area, string parametro = "%", bool soloExistencias = true)
        {
            OperationResult result = new ConsultasTaller().getProductosCOM(area, parametro, soloExistencias);
            if (result.typeResult == ResultTypes.success)
            {
            }
            return result;
        }
    }
}
