﻿using Core.Models;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Interfaces
{
    interface IInsersionKm
    {
        OperationResult saveInsersionKm(ref InsersionKm insersionKm);
        OperationResult getUltimaInsercionByUnidadAndTipo(enumTipoInsersionKM tipoInsersion, string idUnidad);
    }
}
