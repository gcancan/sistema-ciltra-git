﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.BusinessLogic;

namespace CoreFletera.Interfaces
{
    public class LavaderoSvc : ILavadero
    {
        public OperationResult saveLavadero(ref Lavadero lavadero)
        {
            foreach (var item in lavadero.listaInsumos)
            {
                ProductosCOM newProducto = new ProductosCOM
                {
                    idProducto = item.idProducto,
                    codigo = item.codigo,
                    nombre = item.nombre,
                    estatus = true,
                    precio = item.costo,
                    lavadero = item.lavadero,
                    llantera = item.llantera,
                    taller = item.taller
                };
                var resp = new ProductosCOMServices().sincronizarProductoCOM(newProducto);
                if (resp.typeResult == ResultTypes.error)
                {
                    return resp;
                }
            }
            return new LavaderoServices().saveLavadero(ref lavadero);
        }
    }
}
