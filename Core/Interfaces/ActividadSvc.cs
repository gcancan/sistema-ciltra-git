﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.BusinessLogic;

namespace CoreFletera.Interfaces
{
    public class ActividadSvc : IActividad
    {
        public OperationResult saveActividad(ref Actividad actividad)
        {
            foreach (var newProducto in actividad.listProductosCOM)
            {
                var resp = new ProductosCOMServices().sincronizarProductoCOM(newProducto);
                if (resp.typeResult == ResultTypes.error)
                {
                    return resp;
                }
            }

            var respAct = new ActividadServices().saveActividad(ref actividad);
            if (respAct.typeResult == ResultTypes.success)
            {
                if (actividad.taller)
                {
                    var respRelacion = new RelacionTiposOrdenServServices().saveRelacioOrdenServ(
                        new TipoOrdenServClass { idTipoOrdServ = 2, nomTipoOrdenSer = "TALLER" }, 
                        CatTipoOrdenServ.ACTIVIDADES, 
                        actividad.idActividad);
                    if (respRelacion.typeResult != ResultTypes.success)
                    {
                        return respRelacion;
                    }
                }
                if (actividad.lavadero)
                {
                    var respRelacion = new RelacionTiposOrdenServServices().saveRelacioOrdenServ(
                        new TipoOrdenServClass { idTipoOrdServ = 4, nomTipoOrdenSer = "LAVADERO" },
                        CatTipoOrdenServ.ACTIVIDADES,
                        actividad.idActividad);
                    if (respRelacion.typeResult != ResultTypes.success)
                    {
                        return respRelacion;
                    }
                }
                if (actividad.llantera)
                {
                    var respRelacion = new RelacionTiposOrdenServServices().saveRelacioOrdenServ(
                        new TipoOrdenServClass { idTipoOrdServ = 3, nomTipoOrdenSer = "LLANTAS" },
                        CatTipoOrdenServ.ACTIVIDADES,
                        actividad.idActividad);
                    if (respRelacion.typeResult != ResultTypes.success)
                    {
                        return respRelacion;
                    }
                }

                if (actividad.listProductosCOM.Count > 0)
                {
                    return new ActividadServices().saveRelActividadProducto(actividad);
                }
                else
                {
                    return respAct;
                }
            }
            else
            {
                return respAct;
            }
        }

        public OperationResult getProductosCOMByActividad(int CIDPRODUCTO)
        {
            return new ActividadServices().getProductosCOMByActividad(CIDPRODUCTO);
        }

        public OperationResult quitarRelActividadProducto(int CIDPRODUCTO, int idProducto)
        {
            return new ActividadServices().quitarRelActividadProducto(CIDPRODUCTO, idProducto);
        }

        public OperationResult getAllActividades(bool soloActivos = false) =>
            new ActividadServices().getAllActividades(soloActivos);

        public OperationResult getAllActividadesByTipoOrdenServ(enumTipoOrdServ enumTipoOrd) =>
            new ActividadServices().getAllActividadesByTipoOrdenServ(enumTipoOrd);
    }
}
