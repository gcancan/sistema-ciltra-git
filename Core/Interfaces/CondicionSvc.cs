﻿namespace CoreFletera.Interfaces
{
    using Core.Models;
    using CoreFletera.BusinessLogic;
    using CoreFletera.Models;
    using System;
    using System.Runtime.InteropServices;

    public class CondicionSvc : ICondicion
    {
        public OperationResult getCondicionAllOrById(int idCondicion = 0) =>
            new CondicionesServices().getCondicionAllOrById(idCondicion);

        public OperationResult saveCondicionLlantas(ref CondicionLlanta condicionLlanta) =>
            new CondicionesServices().saveCondicionLlantas(ref condicionLlanta);
    }
}
