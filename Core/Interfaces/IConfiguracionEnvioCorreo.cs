﻿using Core.Models;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Interfaces
{
    interface IConfiguracionEnvioCorreo
    {
        OperationResult saveCorreoElectronico(ref ConfiguracionEnvioCorreo envioCorreo);
        OperationResult getAllCorreosElectronicos();
        OperationResult getProcesosByIdCorreo(int idCorreo);
        OperationResult getCorreosByProceso(enumProcesosEnvioCorreo enumProcesos);
    }
}
