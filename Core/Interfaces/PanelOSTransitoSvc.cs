﻿namespace CoreFletera.Interfaces
{
    using Core.Models;
    using CoreFletera.BusinessLogic;
    using System;

    public class PanelOSTransitoSvc : IPanelOSTransito
    {
        public OperationResult getPanelOSTransitoByTipoOrden(int idTipoOrdenServ) =>
            new PanelOSTransitoServices().getPanelOSTransitoByTipoOrden(idTipoOrdenServ);
    }
}
