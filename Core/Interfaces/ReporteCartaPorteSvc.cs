﻿namespace CoreFletera.Interfaces
{
    using Core.Models;
    using CoreFletera.BusinessLogic;
    using System;
    using System.Collections.Generic;

    public class ReporteCartaPorteSvc : IReporteCartaPorte
    {
        public OperationResult getReporteCartaPorteV2(int idEmpresa, DateTime fechaInicio, DateTime fechaFin, List<string> listaTractores, List<string> listaOperadores,
            List<string> listaClientes, List<string> listaRutas, List<string> listaModalidades, List<string> listaEstatus, bool todosOperadores = true, bool todosTractores = true) =>
            new ReporteCartaPorteServices().getReporteCartaPorteV2(idEmpresa, fechaInicio, fechaFin, listaTractores, listaOperadores,
                listaClientes, listaRutas, listaModalidades, listaEstatus, todosOperadores, todosTractores);

        public OperationResult getViajesFacturar(int idEmpresa, DateTime fechaInicio, DateTime fechaFin, bool byFechaDespacho, int idCliente, int idZonaOpetativa,
            List<string> listaRutas, List<string> listaModalidades) =>
            new ReporteCartaPorteServices().getViajesFacturar(idEmpresa, fechaInicio, fechaFin, byFechaDespacho, idCliente, idZonaOpetativa,
                listaRutas, listaModalidades);

        public OperationResult getReporteCartaPorteV3(int idEmpresa, DateTime fechaInicio, DateTime fechaFin, List<string> listaTractores, List<string> listaOperadores,
            List<string> listaClientes, List<string> listaRutas, List<string> listaModalidades, List<string> listaEstatus, bool todosOperadores = true, bool todosTractores = true) =>
            new ReporteCartaPorteServices().getReporteCartaPorteV3(idEmpresa, fechaInicio, fechaFin, listaTractores, listaOperadores,
                listaClientes, listaRutas, listaModalidades, listaEstatus, todosOperadores, todosTractores);
        public OperationResult getReporteCartaPorteByOperadores(int idEmpresa, DateTime fechaInicio, DateTime fechaFin, List<string> listaOperadores) =>
            new ReporteCartaPorteServices().getReporteCartaPorteByOperadores(idEmpresa, fechaInicio, fechaFin, listaOperadores);

        public OperationResult getReporteCartaPorteByOperadoresAtlante(int idEmpresa, DateTime fechaInicio, DateTime fechaFin, List<string> listaOperadores) =>
            new ReporteCartaPorteServices().getReporteCartaPorteByOperadoresAtlante(idEmpresa, fechaInicio, fechaFin, listaOperadores);
    }
}
