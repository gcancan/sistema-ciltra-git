﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.BusinessLogic;
namespace CoreFletera.Interfaces
{
    public class MotivoCancelacionSvc : IMotivoCancelacion
    {
        public OperationResult getAllMotivosByProceso(enumProcesos enumProcesos) =>
            new MotivoCancelacionServices().getAllMotivosByProceso(enumProcesos);

        public OperationResult getAllMotivosCancelacion() =>
            new MotivoCancelacionServices().getAllMotivosCancelacion();

        public OperationResult getProcesosBiIdMOtivoCancelacion(int idMotivoCancelacion) =>
            new MotivoCancelacionServices().getProcesosBiIdMOtivoCancelacion(idMotivoCancelacion);

        public OperationResult saveMotivosCancelacion(ref MotivoCancelacion motivo) =>
            new MotivoCancelacionServices().saveMotivosCancelacion(ref motivo);
    }
}
