﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Interfaces
{
    interface IIngresosEgresos 
    {
        OperationResult getIngresos(string idUnidad, DateTime fechaInicial, DateTime fechaFinal);
        OperationResult getEgresos(string idUnidad, DateTime fechaInicial, DateTime fechaFinal);
        OperationResult getIngresosDetalles(string idUnidad, DateTime fechaInicial, DateTime fechaFinal);
        OperationResult getEgresosDetallado(string idUnidad, DateTime fechaInicial, DateTime fechaFinal);
        OperationResult getIngresosEgresos(DateTime fechaInicial, DateTime fechaFinal, int idEmpresa, List<string> listaUnidades);
    }
}
