﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using Core.BusinessLogic;
namespace Core.Interfaces
{
    public class UsuarioSvc : IUsuario
    {
        public OperationResult getAllUserOrById(int id = 0, bool omitirPrivilegios = false)
        {
            return new UsuarioServices().getAllUserOrById(id, omitirPrivilegios);
        }

        public OperationResult getAllUserOrById_v2(int id = 0)
        {
            return new UsuarioServices().getAllUsuariosOrById_v2(id);
        }

        public OperationResult getReportePrivilegios() =>
            new UsuarioServices().getReportePrivilegios();

        public OperationResult getReporteUsuarios() =>
            new UsuarioServices().getReporteUsuarios();

        public OperationResult getReporteUsuariosPrivilegios() =>
            new UsuarioServices().getReporteUsuariosPrivilegios();

        public OperationResult getUserByUserPass(string usuario, string contrasena)
        {
            return new UsuarioServices().getUserByUserPass(usuario, contrasena);
        }

        public OperationResult saveUsuario(ref Usuario usuario)
        {
            return new UsuarioServices().saveUsuario(ref usuario);
        }
    }
}
