﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.BusinessLogic;
using CoreFletera.Models;

namespace CoreFletera.Interfaces
{
    public class CitaMantenimientoSvc : ICitaMantenimiento
    {
        public OperationResult getAllCitasMatto() =>
            new CitaMantenimientoServices().getAllCitasMatto();

        public OperationResult saveCitaMantenimiento(ref CitaMantenimiento citaMantenimiento) =>
            new CitaMantenimientoServices().saveCitaMantenimiento(ref citaMantenimiento);
        public OperationResult getReporteCitasMatto(List<string> listaUnidades, DateTime fechaInicial, DateTime fechaFinal) =>
            new CitaMantenimientoServices().getReporteCitasMatto(listaUnidades, fechaInicial, fechaFinal);
    }
}
