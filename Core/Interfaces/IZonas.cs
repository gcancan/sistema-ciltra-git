﻿namespace Core.Interfaces
{
    using Core.Models;
    using CoreFletera.Models;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    public interface IZonas
    {
        OperationResult borrarOrigenDestino(OrigenDestinos relacion);
        OperationResult borrarOrigenDestinoByRuta(int idOrigen, int idDestino, int idCliente);
        OperationResult deleteRutasDestino(Zona zona);
        OperationResult getDestinosAtlante(int idEmpresa);
        OperationResult getDestinosByIdEmpresa(int idEmpresa);
        OperationResult getDestinosByOrigenAndIdCliente(int idCliente, int idOrigen);
        OperationResult getDestinosIncompletosAllById(int id = 0, string idCliente = "");
        OperationResult getOrigenByIdCliente(int clave);
        OperationResult getOrigenByIdEmpresa(int clave);
        OperationResult getPrecios(int idCliente, eTiposPrecios tipoPrecio, int idOrigen);
        OperationResult getRelacionesOrigenDestinosByIdCliente(int idCliente);
        OperationResult getZonasALLorById(int id = 0, string idCliente = "");
        OperationResult getZonasByPrecioAndOrigen(int idCliente, eTiposPrecios tipoPrecio, int idOrigen, decimal precio);
        OperationResult saveDestino_v2(ref Zona zona);
        OperationResult saveNuevosPrecios(int idCliente, eTiposPrecios tipoPrecio, decimal precioAnterior, decimal precioNuevo, List<Zona> list, string usuario);
        OperationResult saveOrigenDestino(ref OrigenDestinos relacion);
        OperationResult saveRutasDestino(ref Zona zona);
        OperationResult saveRutasDestinoAtlante(ref Zona destino);
        OperationResult saveRutasDestinoIncompletos(Zona zona);
        OperationResult getZonasByEmpresaCliente(int idEmpresa, int idCliente);
        OperationResult saveNewOrigenPegaso(ref OrigenPegaso origen);
        OperationResult getAnticipos(int idDestino);
        OperationResult getAnticiposByNumViaje(int idViaje);
        OperationResult getCasetasByDestino(int idDestino);
    }
}
