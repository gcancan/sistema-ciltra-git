﻿using Core.Models;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Interfaces
{
    interface ILiquidacionPegaso
    {
        OperationResult saveLiquidacionPegaso(ref LiquidacionPegaso liquidacion);
        OperationResult getLiquidacionById(int idLiquidacion);
        OperationResult saveConfiguracionCliente(ref ConfiguracionClienteLiquidacion config);
        OperationResult getCongByIdCliente(int idCliente);
        OperationResult getAllConfigCliente();
    }
}
