﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.BusinessLogic;
using CoreFletera.Models;

namespace CoreFletera.Interfaces
{
    public class PuestoSvc : IPuesto
    {
        public OperationResult getALLPuestosById(int id = 0) =>
            new PuestoServices().getALLPuestosById(id);

        public OperationResult savePuesto(ref Puesto puesto) =>
            new PuestoServices().savePuesto(ref puesto);

        public OperationResult sincronizarPuestos(List<Puesto> listaPuestos) =>
            new PuestoServices().sincronizarPuestos(listaPuestos);
    }
}
