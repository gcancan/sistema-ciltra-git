﻿using Core.Models;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Interfaces
{
    interface INivelesTanque
    {
        OperationResult getNivelTanqueByZonaOperativa(ZonaOperativa zonaOperativa);
        OperationResult saveNivelTanques(ref NivelesTanque nivelesTanque);
        OperationResult getResumenCargasInternas(DateTime fechaInicial, DateTime fechaFinal, int idZonaOperativa = 1);
        OperationResult getNivelTanqueByFiltros(ZonaOperativa zonaOperativa, DateTime fechaInicial, DateTime fechaFinal);
    }
}
