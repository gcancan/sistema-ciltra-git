﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using Core.BusinessLogic;
namespace Core.Interfaces
{
    public class EstadoSvc : IEstado
    {
        public OperationResult getEstadosAllorById(int id = 0)
        {
            return new EstadoServices().getEstadosAllorById(id);
        }

        public OperationResult saveEstado(ref Estado estado) =>
            new EstadoServices().saveEstado(ref estado);
    }
}
