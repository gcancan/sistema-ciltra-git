﻿using Core.Models;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Interfaces
{
    interface ITableroControlUnidades
    {
        OperationResult iniciarTrayectoGranja(ref TableroControlUnidades tablero);
        OperationResult getUltimoTableroActivoByTractor(string idTractor);
        OperationResult registrarEntradaGranja(int idTablero, int idDetalleTablero, DateTime fecha);
        OperationResult registrarSalidaGranja(int idTablero, int idDetalleTablero, DateTime fecha);
        OperationResult registrarEntradaGranja(int idTablero, int idDetalleTablero, DateTime fecha, string strConnection);
        OperationResult registrarSalidaGranja(int idTablero, int idDetalleTablero, DateTime fecha, string strConnection);
        OperationResult getTableroControlUnidad();
        OperationResult getTableroControlUnidadesByOperaciones(int idEmpresa, List<string> listIdZonas, List<string> listOperaciones);
        OperationResult getUltimoTableroActivoByTractor(string idTractor, string strConnection);
        OperationResult iniciarViajeTablero(int idTablero, int folioViaje, DateTime fecha, string strConnection);
        OperationResult terminarViajeTablero(int idTablero, int folioViaje, DateTime fecha, string strConnection);
        OperationResult regresarEventoEnPlanta(int idTablero, string strConnection);
        OperationResult entradaTallerTrayectoDestino(int idTablero, DateTime fecha, string strConnection);
        OperationResult salidaTallerTrayectoDestino(int idTablero, DateTime fecha, string strConnection);
        OperationResult entradaTallerTrayectoPlanta(int idTablero, DateTime fecha, string strConnection);
        OperationResult salidaTallerTrayectoPlanta(int idTablero, DateTime fecha, string strConnection);
        OperationResult saveComentarioTablero(ComentarioTablero comentario);
        OperationResult regresarAGranja(int idTablero, int idDetalleTablero, DateTime fecha, string strConnection);
    }
}
