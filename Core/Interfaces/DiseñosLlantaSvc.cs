﻿namespace CoreFletera.Interfaces
{
    using Core.Models;
    using CoreFletera.BusinessLogic;
    using CoreFletera.Models;
    using System;
    using System.Runtime.InteropServices;

    public class DiseñosLlantaSvc : IDiseñosLlanta
    {
        public OperationResult getDiseñosLlanta() =>
            new DiseñoLlantaServices().getDiseñosLlanta();

        public OperationResult getDiseñosLlantaAllOrById(int idDiseño = 0) =>
            new DiseñoLlantaServices().getDiseñosLlantaAllOrById(idDiseño);

        public OperationResult saveDiseñoLlanta(ref Diseño diseño) =>
            new DiseñoLlantaServices().saveDiseñoLlanta(ref diseño);
    }
}
