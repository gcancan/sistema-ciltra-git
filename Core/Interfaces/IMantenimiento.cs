﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Interfaces
{
    interface IMantenimiento
    {
        OperationResult getMantenimientoByFiltros(DateTime fechaInicio, DateTime fechaFin, List<string> listaUnidades, string tipoOrdenSer = "%", string tipoServicio = "%");
    }
}
