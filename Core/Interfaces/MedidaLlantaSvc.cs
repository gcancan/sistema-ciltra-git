﻿namespace CoreFletera.Interfaces
{
    using Core.Models;
    using CoreFletera.BusinessLogic;
    using CoreFletera.Models;
    using System;

    public class MedidaLlantaSvc : IMedidaLlanta
    {
        public OperationResult getMedidasLlantas() =>
            new MedidaLlantasServices().getMedidasLlantas();

        public OperationResult saveMedidaLlantas(ref MedidaLlanta medidaLlanta) =>
            new MedidaLlantasServices().saveMedidaLlantas(ref medidaLlanta);
    }
}
