﻿namespace Core.Interfaces
{
    using Core.BusinessLogic;
    using Core.Models;
    using CoreFletera.Interfaces;
    using CoreFletera.Models;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    public class CargaCombustibleSvc : ICargaCombustible
    {
        public OperationResult aplicarFolioSalidaAlmacen(List<CargasCombustibleCOM> lista, int lIdDocto) =>
            new CargaCombuctibleServices().aplicarFolioSalidaAlmacen(lista, lIdDocto);

        public OperationResult asignarValeExternoCartaPortes(List<CartaPorte> listDetalles, int idCargaCombustible) =>
            new CargaCombuctibleServices().asignarValeExternoCartaPortes(listDetalles, idCargaCombustible);

        public OperationResult findUltimaCargaByIdUnidad(string idUnidadTrans) =>
            new CargaCombuctibleServices().findUltimaCargaByIdUnidad(idUnidadTrans);

        public OperationResult getCargaCombustibleByIdUnidadTrans(string idUnidadTrans) =>
            new CargaCombuctibleServices().getCargaCombustibleByIdUnidadTrans(idUnidadTrans);

        public OperationResult getCargaCombustibleExternoByTicket(string ticket)
        {
            OperationResult result = new CargaCombuctibleServices().getCargaCombustibleExternoByTicket(ticket);
            if (result.typeResult == ResultTypes.success)
            {
                return new OperacionSvc().getSalidasEntradasByIdOrdenServ((int)result.result);
            }
            return result;
        }

        public OperationResult getCargaCombustibleExtra(int idCargaExtra)
        {
            try
            {
                OperationResult result = new CargaCombuctibleServices().getCargaCombustibleExtra(idCargaExtra);
                if (result.typeResult == ResultTypes.success)
                {
                    CargaCombustibleExtra extra = (CargaCombustibleExtra)result.result;
                    OperationResult result2 = new OperationResult();
                    result2 = new UnidadTransporteSvc().getUnidadesALLorById(0, extra.unidadTransporte.clave);
                    if (result2.typeResult == ResultTypes.success)
                    {
                        extra.unidadTransporte = ((List<UnidadTransporte>)result2.result)[0];
                    }
                    else
                    {
                        return result2;
                    }
                    result2 = new TipoCombustibleSvc().getTiposCombustibleAllOrById(extra.tipoCombustible.idTipoCombustible);
                    if (result2.typeResult == ResultTypes.success)
                    {
                        extra.tipoCombustible = (TipoCombustible)result2.result;
                    }
                    else
                    {
                        return result2;
                    }
                    if (extra.despachador.clave != 0)
                    {
                        result2 = new OperadorSvc().getPersonalALLorById(extra.despachador.clave);
                        if (result2.typeResult != ResultTypes.success)
                        {
                            return result2;
                        }
                        extra.despachador = ((List<Personal>)result2.result)[0];
                    }
                    else
                    {
                        extra.despachador = null;
                    }
                    result2 = new OperadorSvc().getPersonalALLorById(extra.chofer.clave);
                    if (result2.typeResult == ResultTypes.success)
                    {
                        extra.chofer = ((List<Personal>)result2.result)[0];
                    }
                    else
                    {
                        return result2;
                    }
                }
                return result;
            }
            catch (Exception exception)
            {
                return new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
        }
        public OperationResult getCargaCombustibleExtraLibresByUnidad(string idUnidad)
        {
            try
            {
                var resp = new CargaCombuctibleServices().getCargaCombustibleExtraLibresByUnidad(idUnidad);
                if (resp.typeResult == ResultTypes.success)
                {
                    foreach (var extra in resp.result as List<CargaCombustibleExtra>)
                    {
                        //CargaCombustibleExtra extra = (CargaCombustibleExtra)result.result;
                        OperationResult result2 = new OperationResult();
                        result2 = new UnidadTransporteSvc().getUnidadesALLorById(0, extra.unidadTransporte.clave);
                        if (result2.typeResult == ResultTypes.success)
                        {
                            extra.unidadTransporte = ((List<UnidadTransporte>)result2.result)[0];
                        }
                        else
                        {
                            return result2;
                        }
                        result2 = new TipoCombustibleSvc().getTiposCombustibleAllOrById(extra.tipoCombustible.idTipoCombustible);
                        if (result2.typeResult == ResultTypes.success)
                        {
                            extra.tipoCombustible = (TipoCombustible)result2.result;
                        }
                        else
                        {
                            return result2;
                        }
                        if (extra.despachador.clave != 0)
                        {
                            result2 = new OperadorSvc().getPersonalALLorById(extra.despachador.clave);
                            if (result2.typeResult != ResultTypes.success)
                            {
                                return result2;
                            }
                            extra.despachador = ((List<Personal>)result2.result)[0];
                        }
                        else
                        {
                            extra.despachador = null;
                        }
                        result2 = new OperadorSvc().getPersonalALLorById(extra.chofer.clave);
                        if (result2.typeResult == ResultTypes.success)
                        {
                            extra.chofer = ((List<Personal>)result2.result)[0];
                        }
                        else
                        {
                            return result2;
                        }
                    }
                    return resp;
                }
                else
                {
                    return resp;
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult getCargaCombustibleExtraByOrdenServ(int idOrdenServ)
        {
            try
            {
                OperationResult result = new CargaCombuctibleServices().getCargaCombustibleExtraByOrdenServ(idOrdenServ);
                if (result.typeResult == ResultTypes.success)
                {
                    CargaCombustibleExtra extra = (CargaCombustibleExtra)result.result;
                    OperationResult result2 = new OperationResult();
                    result2 = new UnidadTransporteSvc().getUnidadesALLorById(0, extra.unidadTransporte.clave);
                    if (result2.typeResult == ResultTypes.success)
                    {
                        extra.unidadTransporte = ((List<UnidadTransporte>)result2.result)[0];
                    }
                    else
                    {
                        return result2;
                    }
                    result2 = new TipoCombustibleSvc().getTiposCombustibleAllOrById(extra.tipoCombustible.idTipoCombustible);
                    if (result2.typeResult == ResultTypes.success)
                    {
                        extra.tipoCombustible = (TipoCombustible)result2.result;
                    }
                    else
                    {
                        return result2;
                    }
                    if (extra.despachador.clave != 0)
                    {
                        result2 = new OperadorSvc().getPersonalALLorById(extra.despachador.clave);
                        if (result2.typeResult != ResultTypes.success)
                        {
                            return result2;
                        }
                        extra.despachador = ((List<Personal>)result2.result)[0];
                    }
                    else
                    {
                        extra.despachador = null;
                    }
                    result2 = new OperadorSvc().getPersonalALLorById(extra.chofer.clave);
                    if (result2.typeResult == ResultTypes.success)
                    {
                        extra.chofer = ((List<Personal>)result2.result)[0];
                    }
                    else
                    {
                        return result2;
                    }
                }
                return result;
            }
            catch (Exception exception)
            {
                return new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
        }

        public OperationResult getCargasCom() =>
            new CargaCombuctibleServices().getCargasCom();

        public OperationResult getDespachosCombustibleByFiltros(DateTime fechaInicio, DateTime fechaFin, int idTanque, string idEmpresa = "%") =>
            new CargaCombuctibleServices().getDespachosCombustibleByFiltros(fechaInicio, fechaFin, idTanque, idEmpresa);

        public OperationResult getExistenciasDieselActual(int idTanque) =>
            new CargaCombuctibleServices().getExistenciasDieselActual(idTanque);

        public OperationResult getFechas() =>
            new CargaCombuctibleServices().getFechas();

        public OperationResult getLtsCombustibleByDia(DateTime fecha) =>
            new CargaCombuctibleServices().getLtsCombustibleByDia(fecha);

        public OperationResult getProveedor(int idProveedor = 0) =>
            new CargaCombuctibleServices().getProveedor(idProveedor);

        public OperationResult getTickets() =>
            new CargaCombuctibleServices().getTickets();

        public OperationResult getTicketsByFechas(DateTime fechaInicial, DateTime fechaFinal) =>
            new CargaCombuctibleServices().getTicketsByFechas(fechaInicial, fechaFinal);
        public OperationResult getTicketsByFechas(DateTime fechaInicial, DateTime fechaFinal, int? idEmpresa)=>
            new CargaCombuctibleServices().getTicketsByFechas(fechaInicial, fechaFinal, idEmpresa);

        public OperationResult getTipoOrdenCombustible(int id = 0) =>
            new CargaCombuctibleServices().getTipoOrdenCombustible(id);

        public OperationResult getTipoOrdenCombustibleExtra() =>
            new CargaCombuctibleServices().getTipoOrdenCombustibleExtra();

        public OperationResult insertFechaCargaCombustible(DateTime fecha) =>
            new CargaCombuctibleServices().insertFechaCargaCombustible(fecha);

        public OperationResult saveCargaCombustibleEXtra(ref CargaCombustibleExtra carga, ref KardexCombustible kardexCombustible) =>
            new CargaCombuctibleServices().saveCargaCombustibleEXtra(ref carga, ref kardexCombustible);

        public OperationResult saveTipoCombustibleEtra(ref TipoOrdenCombustible tipoOrden) =>
            new CargaCombuctibleServices().saveTipoCombustibleEtra(ref tipoOrden);
        public OperationResult integracionValesCombustible(int idVale, List<CargaCombustibleExtra> listaCargasExtra, List<CargaTicketDiesel> listaCargasTickets) =>
            new CargaCombuctibleServices().integracionValesCombustible(idVale, listaCargasExtra, listaCargasTickets);
        public OperationResult desLigarCargasExtra(int idValeExtra) =>
            new CargaCombuctibleServices().desLigarCargasExtra(idValeExtra);
        public OperationResult desLigarCargasTicket(int idVale, int idCargaTicket, List<CargaTicketDiesel> listaCargasTickets) =>
            new CargaCombuctibleServices().desLigarCargasTicket(idVale, idCargaTicket, listaCargasTickets);
        public OperationResult getUltimoCierreProcesoCombustible(int idTanque)=>
            new CargaCombuctibleServices().getUltimoCierreProcesoCombustible(idTanque);
        public OperationResult getValesCargaByViaje(string idUnidad, DateTime fecha) =>
            new CargaCombuctibleServices().getValesCargaByViaje(idUnidad, fecha);
    }
}
