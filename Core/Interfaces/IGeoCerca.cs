﻿namespace CoreFletera.Interfaces
{
    using Core.Models;
    using CoreFletera.Models;

    internal interface IGeoCerca
    {
        OperationResult getGeoCerca();
        OperationResult saveGeoCerca(ref GeoCerca geoCerca);
        OperationResult getGeoCerca(string nombreGeoCerca, string strConnnestion);
    }
}
