﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.BusinessLogic;
namespace CoreFletera.Interfaces
{
    public class FlotaSvc : IFlota
    {
        public OperationResult eliminarFlota(Flota flota) =>
            new FlotaServices().eliminarFlota(flota);
        public OperationResult getFlota(OperacionFletera operacionFletera) =>
            new FlotaServices().getFlota(operacionFletera);
        public OperationResult saveFlota(ref Flota flota) =>
            new FlotaServices().saveFlota(ref flota);
    }
}
