﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.BusinessLogic;
namespace CoreFletera.Interfaces
{
    public class CargaTicketDieselSvc : ICargaTicketDiesel
    {
        public OperationResult getCargaTicketsDieselByEmpresaZonaOperativa(int idEmpresa, int idZonaOperativa) =>
            new CargaTicketDieselServices().getCargaTicketsDieselByEmpresaZonaOperativa(idEmpresa, idZonaOperativa);

        public OperationResult saveCargaticketDiesel(ref CargaTicketDiesel carga) =>
            new CargaTicketDieselServices().saveCargaticketDiesel(ref carga);
        public OperationResult getCargaTicketsDieselByUnidad(string idUnidad) =>
            new CargaTicketDieselServices().getCargaTicketsDieselByUnidad(idUnidad);
    }
}
