﻿using Core.Models;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoreFletera.BusinessLogic;
namespace CoreFletera.Interfaces
{
    public class UENSvc : IUEN
    {
        public OperationResult sincronizarUEN(List<UEN> listaUEN) =>
            new UENService().sincronizarUEN(listaUEN);
    }
}
