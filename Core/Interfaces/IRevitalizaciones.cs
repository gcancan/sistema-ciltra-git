﻿using Core.Models;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Interfaces
{
    interface IRevitalizaciones
    {
        OperationResult saveRevitalizacion(ref Revitalizaciones revitalizacion);
        OperationResult getRevitalizacionById(int idRevitalizacion);
    }
}
