﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.BusinessLogic;
using CoreFletera.Models;

namespace CoreFletera.Interfaces
{
    public class TareasEnvioCorreoSvc : ITareasEnvioCorreo
    {
        public OperationResult getTareasEnviosCorreo() => new TareasEnvioCorreoServices().getTareasEnviosCorreo();
        public OperationResult getCumpleAñosPersonal() => new TareasEnvioCorreoServices().getCumpleAñosPersonal();
        public OperationResult saveTareaCorreosEnviados(TareasEnvioCorreo tarea) => new TareasEnvioCorreoServices().saveTareaCorreosEnviados(tarea);
        public OperationResult validarTareaDiaria(TareasEnvioCorreo tarea) => new TareasEnvioCorreoServices().validarTareaDiaria(tarea);
    }
}
