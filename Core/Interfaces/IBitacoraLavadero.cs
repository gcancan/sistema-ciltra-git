﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Interfaces
{
    interface IBitacoraLavadero
    {
        OperationResult getBitacoraLavadero(int idEmpresa, DateTime fechaInicio, DateTime fechaFin, List<string> listaTractores);
    }
}
