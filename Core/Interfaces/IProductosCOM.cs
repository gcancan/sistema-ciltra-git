﻿using Core.Models;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Interfaces
{
    interface IProductosCOM
    {
        OperationResult saveProductoCOM(ref ProductosCOM producto);
        OperationResult getInsumosCOM();
        OperationResult deleteProductoCOM(ProductosCOM producto);
        OperationResult getInsumosCOMbyIdOrdenActividad(int idOrdenActividad);
    }
}
