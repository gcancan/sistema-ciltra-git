﻿namespace Core.Interfaces
{
    using Core.Models;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    public interface IUnidadTransporte
    {
        OperationResult buscarRemolques(int idEmpresa = 0, string idUnidad = "%");
        OperationResult getDollys(int idEmpresa, string clave = "%", int idZonaOperativa = 0, int idCliente = 0);
        OperationResult getDollysSinFlota(int idEmpresa, string clave = "%");
        OperationResult getTolvas(int idEmpresa, string clave = "%", int idZonaOperativa = 0, int idCliente = 0);
        OperationResult getTolvasSinFlota(int idEmpresa, string clave = "%");
        OperationResult getTractores(int idEmpresa, string clave = "%", int idZonaOperativa = 0, int idCliente = 0);
        OperationResult getTractoresSinFlota(int idEmpresa, string clave = "%");
        OperationResult getUnidadBySerieGps(string strConnection, string serie);
        OperationResult getUnidadesALLorById(int idEmpresa, string id = "");
        OperationResult getUnidadesByGrupos(int idEmpresa, List<string> listaCadena);
        OperationResult getTractoresByOperacion(int idEmpresa, int idZonaOperativa, int idCliente);
        OperationResult getRemolquesByOperacion(int idEmpresa, int idZonaOperativa, int idCliente);
        OperationResult getDollysByOperacion(int idEmpresa, int idZonaOperativa, int idCliente);
        OperationResult saveUnidadTransporte(ref UnidadTransporte unidadTransporte);
        OperationResult getAllUnidadesTransV2();
        OperationResult getUnidadesCarganByEmpresaZonaOperativa(int idEmpresa, int idZonaOperativa);
        OperationResult sincronizarUnidadesTransporte(List<UnidadTransporte> listaUnidades);
    }
}
