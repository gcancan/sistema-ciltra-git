﻿using Core.Models;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Interfaces
{
    interface IEvaluacionDesempeño
    {
        OperationResult buscarDescripcionesEvaluacion();
        OperationResult saveEvaluacionDesempeño(ref EvaluacionDesempeño evaluacion);
        OperationResult getEvaluacionesByDepartamento(int idDepto);
        OperationResult getAlltEvaluaciones();
        OperationResult getEvaluacionesByIdPersonal(int idPersonal);
    }
}
