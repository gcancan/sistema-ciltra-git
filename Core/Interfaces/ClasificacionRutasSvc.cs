﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.BusinessLogic;
using CoreFletera.Models;

namespace CoreFletera.Interfaces
{
    public class ClasificacionRutasSvc : IClasificacionRutas
    {
        public OperationResult getClasificacionRutas(int idCliente = 0, int idClasificacion = 0)
        {
            return new ClasificacionRutasServices().getClasificacionRutas(idCliente, idClasificacion);
        }

        public OperationResult getClasificacionRutasByViaje(int idCliente, decimal km)
        {
            return new ClasificacionRutasServices().getClasificacionRutasByViaje(idCliente, km);
        }

        public OperationResult saveClasificacionRutas(ref ClasificacionRutas clasificacion)
        {
            return new ClasificacionRutasServices().saveClasificacionRutas(ref clasificacion);
        }
    }
}
