﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.BusinessLogic;

namespace CoreFletera.Interfaces
{
    public class BitacoraUbicacionesCSISvc : IBitacoraUbicacionesCSI
    {
        public OperationResult getBitacoraUbicacionCSI(DateTime fechaInicial, DateTime fechaFinal, int idEmpresa, List<string> listaZonas, List<string> listaOperaciones) =>
            new BitacoraUbicacionesCSIServices().getBitacoraUbicacionCSI(fechaInicial, fechaFinal, idEmpresa, listaZonas, listaOperaciones);
    }
}
