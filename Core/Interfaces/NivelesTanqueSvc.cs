﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.BusinessLogic;
namespace CoreFletera.Interfaces
{
    public class NivelesTanqueSvc : INivelesTanque
    {
        public OperationResult getNivelTanqueByFiltros(ZonaOperativa zonaOperativa, DateTime fechaInicial, DateTime fechaFinal) =>
            new NivelesTanqueServices().getNivelTanqueByFiltros(zonaOperativa, fechaInicial, fechaFinal);       
        public OperationResult getNivelTanqueByZonaOperativa(ZonaOperativa zonaOperativa)
        {
            return new NivelesTanqueServices().getNivelTanqueByZonaOperativa(zonaOperativa);
        }
        public OperationResult getResumenCargasInternas(DateTime fechaInicial, DateTime fechaFinal, int idZonaOperativa = 1)
        {
            return new NivelesTanqueServices().getResumenCargasInternas(fechaInicial, fechaFinal, idZonaOperativa);
        }
        public OperationResult saveNivelTanques(ref NivelesTanque nivelesTanque)
        {
            return new NivelesTanqueServices().saveNivelTanques(ref nivelesTanque);
        }
    }
}
