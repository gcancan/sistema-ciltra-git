﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.BusinessLogic;
namespace CoreFletera.Interfaces
{
    public class UbicacionUnidadesSvc : IUbicacionUnidades
    {
        public OperationResult saveUbicacionUnidades(List<UbicacionUnidades> listaUbicaciones)
        {
            return new UbicacionUnidadesServicios().saveUbicacionUnidades(listaUbicaciones);
        }
    }
}
