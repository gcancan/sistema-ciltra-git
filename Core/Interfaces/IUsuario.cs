﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Interfaces
{
    public interface IUsuario
    {
        OperationResult getUserByUserPass(string usuario, string contrasena);
        OperationResult saveUsuario(ref Usuario usuario);
        OperationResult getAllUserOrById(int id = 0, bool omitirPrivilegios = false);
        OperationResult getAllUserOrById_v2(int id = 0);
        OperationResult getReporteUsuarios();
        OperationResult getReportePrivilegios();
        OperationResult getReporteUsuariosPrivilegios();
    }
}
