﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.BusinessLogic;
namespace CoreFletera.Interfaces
{
    public class AnticipoSvc : IAnticipo
    {
        public OperationResult getTipoAnticipos() =>
            new AnticipoServices().getTipoAnticipos();
    }
}
