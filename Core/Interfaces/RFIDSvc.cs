﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using Core.BusinessLogic;
namespace Core.Interfaces
{
    public class RFIDSvc : IRFID
    {
        public OperationResult asignarRFIDOperador(ref Personal personal, string rfid)
        {
            return new RFIDServices().asignarRFIDOperador(ref personal, rfid);
        }

        public OperationResult asignarRFIDUnidades(ref UnidadTransporte unidadTrans, string rfid)
        {
            return new RFIDServices().asignarRFIDUnidades(ref unidadTrans, rfid);
        }

        public OperationResult findByRFID(string rfid, ref ResultadoBusquedaRFID tipo)
        {
            return new RFIDServices().findByRFID(rfid, ref tipo);
        }
    }
}
