﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using Core.BusinessLogic;
namespace Core.Interfaces
{
    public class DepartamentoSvc : IDepartamento
    {
        public OperationResult getALLDepartamentosById(int id = 0) =>
            new DepartamentoServices().getALLDepartamentosById(id);

        public OperationResult getDepartamentosALLorById(int id = 0)
        {
            return new DepartamentoServices().getDepartamentosALLorById(id);
        }

        public OperationResult saveDepartamento(ref Departamento departamento) =>
            new DepartamentoServices().saveDepartamento(ref departamento);

        public OperationResult sincronizarDepartamentos(List<Departamento> listaDepartamentos) =>
            new DepartamentoServices().sincronizarDepartamentos(listaDepartamentos);
    }
}
