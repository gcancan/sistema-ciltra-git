﻿using Core.Models;
using Core.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Interfaces
{
    public class ImpuestosServices
    {
        public OperationResult getImpuestosALLorById(int id)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getImpuestosALLorById";
                        command.Parameters.Add(new SqlParameter("@id", id));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        connection.Open();
                        var i = command.ExecuteReader();
                        List<Impuestos> listImpuestos = new List<Impuestos>();

                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                Impuestos impuesto = (new Impuestos
                                {
                                    clave_empresa = (int)sqlReader["Clave_empresa"],
                                    clave = (int)sqlReader["Clave"],
                                    descripcion = (string)sqlReader["Descripcion"],
                                    valor = (decimal)sqlReader["Valor"],
                                    activo = (bool)sqlReader["Activo"]
                                });                              

                                listImpuestos.Add(impuesto);
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)codigo.Value, result = listImpuestos };
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }
    }
}
