﻿namespace Core.Interfaces
{
    using Core.Models;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    public interface ICliente
    {
        OperationResult getClientesALLorById(int idEmpresa, int id = 0, bool omitirProductos = false);
        OperationResult getClientesByRuta(int idOrigen, int idDestino, int? idClasificacion);
        OperationResult getClientesByZonasOperativas(List<string> listaId, int idEmpresa);
        OperationResult saveCliente(ref Cliente cliente);
        OperationResult getAllClienteByEmpresa(int idEmpresa);
        OperationResult sincronizarClientes(int idEmpresa, List<Cliente> listaCliente);
    }
}
