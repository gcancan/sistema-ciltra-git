﻿namespace CoreFletera.Interfaces
{
    using Core.Models;
    using CoreFletera.Models;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    internal interface IKardexCombustible
    {
        OperationResult getKardexCombustible(int idTanque, DateTime fechaInicial, DateTime fechaFin);
        OperationResult getResumenKardexCombustible(DateTime fechaInicio, DateTime fechaFin, int idTanque, int idEmpresa = 0);
        OperationResult savekardexCombustible(ref KardexCombustible kardexCombustible);
        OperationResult getExisteciaDiarias(int idTanque, List<DateTime> listaFechas, int idEmpresa, bool todasEmpresas = false);
    }
}
