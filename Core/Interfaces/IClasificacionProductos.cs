﻿namespace CoreFletera.Interfaces
{
    using Core.Models;
    using CoreFletera.Models;

    internal interface IClasificacionProductos
    {
        OperationResult getAllClasificacionProductos();
        OperationResult saveClasificacionProductos(ref ClasificacionProductos clasificacion);
    }
}
