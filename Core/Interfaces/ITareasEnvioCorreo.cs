﻿using Core.Models;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Interfaces
{
    interface ITareasEnvioCorreo
    {
        OperationResult getTareasEnviosCorreo();
        OperationResult getCumpleAñosPersonal();
        OperationResult saveTareaCorreosEnviados(TareasEnvioCorreo tarea);
        OperationResult validarTareaDiaria(TareasEnvioCorreo tarea);
    }
}
