﻿using Core.Models;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Interfaces
{
    interface IOrdenCompra
    {
        OperationResult saveOrdenCompraLlantas(ref OrdenCompra orden);
        OperationResult saveOrdenCompraLlantas(List<OrdenCompra> ListaOrden);
        OperationResult getOrdenCompraByFolio(Empresa empresa, string folio);
        OperationResult getOrdenCompraByIdCotizacion(int idCotizacion);
    }
}
