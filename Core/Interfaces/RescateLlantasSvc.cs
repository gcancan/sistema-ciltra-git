﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.BusinessLogic;
namespace CoreFletera.Interfaces
{
    public class RescateLlantasSvc : IRescateLlantas
    {
        public OperationResult getMotivoRescateAllOrBy(int idMotivoRescate = 0)
        {
            return new RescateUnidadServices().getMotivoRescateAllOrBy(idMotivoRescate);
        }

        public OperationResult getRescateByid(int idRescate)
        {
            return new RescateUnidadServices().getRescateByid(idRescate);
        }

        public  OperationResult saveRescateLlantas(ref RescateLlantas rescate)
        {
            return new RescateUnidadServices().saveRescateLlantas(ref rescate);
        }
    }
}
