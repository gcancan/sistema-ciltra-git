﻿namespace CoreFletera.Interfaces
{
    using Core.Models;
    using CoreFletera.Models;
    using System.Collections.Generic;

    internal interface IParadasEquipos
    {
        OperationResult saveParadasEquipo(ref List<ParadaEquipo> listaParadas, UsuarioCSI usuarioCSI);
    }
}
