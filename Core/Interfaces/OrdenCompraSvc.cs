﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.BusinessLogic;
namespace CoreFletera.Interfaces
{
    public class OrdenCompraSvc : IOrdenCompra
    {
        public OperationResult getOrdenCompraByFolio(Empresa empresa, string folio)
        {
            return new OrdenCompraServices().getOrdenCompraByFolio(empresa, folio);
        }

        public OperationResult getOrdenCompraByIdCotizacion(int idCotizacion)
        {
            return new OrdenCompraServices().getOrdenCompraByIdCotizacion(idCotizacion);
        }

        public OperationResult saveOrdenCompraLlantas(List<OrdenCompra> ListaOrden)
        {
            List<OrdenCompra> lista = new List<OrdenCompra>();
            OperationResult resp = new OperationResult();
            foreach (var item in ListaOrden)
            {
                var orden = item;
                resp = new OrdenCompraServices().saveOrdenCompraLlantas(ref orden);
                if (resp.typeResult == ResultTypes.success)
                {
                    lista.Add(orden);
                }
            }
            resp.result = lista;
            return resp;
        }

        public OperationResult saveOrdenCompraLlantas(ref OrdenCompra orden)
        {
            return new OrdenCompraServices().saveOrdenCompraLlantas(ref orden);
        }
    }
}
