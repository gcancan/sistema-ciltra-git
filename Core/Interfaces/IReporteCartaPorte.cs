﻿namespace CoreFletera.Interfaces
{
    using Core.Models;
    using System;
    using System.Collections.Generic;

    internal interface IReporteCartaPorte
    {
        OperationResult getReporteCartaPorteV2(int idEmpresa, DateTime fechaInicio, DateTime fechaFin, List<string> listaTractores, List<string> listaOperadores, 
            List<string> listaClientes, List<string> listaRutas, List<string> listaModalidades, List<string> listaEstatus, bool todosOperadores = true, bool todosTractores = true);

        OperationResult getViajesFacturar(int idEmpresa, DateTime fechaInicio, DateTime fechaFin, bool byFechaDespacho, int idCliente, int idZonaOpetativa,
            List<string> listaRutas, List<string> listaModalidades);
        OperationResult getReporteCartaPorteByOperadores(int idEmpresa, DateTime fechaInicio, DateTime fechaFin, List<string> listaOperadores);
        OperationResult getReporteCartaPorteByOperadoresAtlante(int idEmpresa, DateTime fechaInicio, DateTime fechaFin, List<string> listaOperadores);
    }
}
