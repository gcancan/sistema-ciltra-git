﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using Core.BusinessLogic;
namespace Core.Interfaces
{
    public class FraccionSvc : IFraccion
    {
        public OperationResult getFraccionALLorById(int id=0)
        {
            try
            {
                var resultado =  new FraccionServices().getFraccionALLorById(id > 5 ? 6: id);
                if (resultado.typeResult == ResultTypes.success)
                {
                    List<Fraccion> listFraccion = (List<Fraccion>)resultado.result;                  
                    foreach (Fraccion item in listFraccion)
                    {
                        var resp = new ProductoServices().getALLProductosByIdORidFraccion(0, item.clave);
                        if (resp.typeResult == ResultTypes.success)
                        {
                            item.listProductos = (List<Producto>)resp.result;
                        }
                        else
                        {
                            return resp;
                        }
                    }                    
                    resultado.result = listFraccion;
                    return resultado;
                }
                else
                {
                    return resultado;
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
            
        }

        public OperationResult getFraccionALLorById(int id, int idClasificacion)
        {
            try
            {
                var resultado = new FraccionServices().getFraccionALLorById(id > 5 ? 6 : id);
                if (resultado.typeResult == ResultTypes.success)
                {
                    List<Fraccion> listFraccion = (List<Fraccion>)resultado.result;
                    foreach (Fraccion item in listFraccion)
                    {
                        var resp = new ProductoServices().getALLProductosByIdORidFraccionAndIdClasificacion(0, item.clave, idClasificacion);
                        if (resp.typeResult == ResultTypes.success)
                        {
                            item.listProductos = (List<Producto>)resp.result;
                        }
                        else
                        {
                            return resp;
                        }
                    }
                    resultado.result = listFraccion;
                    return resultado;
                }
                else
                {
                    return resultado;
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }

        }
    }
}
