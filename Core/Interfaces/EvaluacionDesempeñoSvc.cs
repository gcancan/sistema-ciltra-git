﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoreFletera.Models;
using CoreFletera.BusinessLogic;
namespace CoreFletera.Interfaces
{
    public class EvaluacionDesempeñoSvc : IEvaluacionDesempeño
    {
        public OperationResult buscarDescripcionesEvaluacion() =>
            new EvaluacionDesempeñoService().buscarDescripcionesEvaluacion();
        public OperationResult saveEvaluacionDesempeño(ref EvaluacionDesempeño evaluacion) =>
            new EvaluacionDesempeñoService().saveEvaluacionDesempeño(ref evaluacion);
        public OperationResult getEvaluacionesByDepartamento(int idDepto) =>
            new EvaluacionDesempeñoService().getEvaluacionesByDepartamento(idDepto);
        public OperationResult getAlltEvaluaciones() =>
            new EvaluacionDesempeñoService().getAlltEvaluaciones();
        public OperationResult getEvaluacionesByIdPersonal(int idPersonal) =>
            new EvaluacionDesempeñoService().getEvaluacionesByIdPersonal(idPersonal);
    }
}
