﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Interfaces
{
    interface IReporteCargaCombustible
    {
        OperationResult getReporteCombustibleByFiltros(int idEmpresa, DateTime fechaInicial, DateTime fechaFinal, List<string> listaZonas, bool todosZonas,
           List<string> listaProveedores, bool todosProveedores, List<string> listaUnidades, bool todasUnidades, List<string> listaOperadores, bool todosOperadores);
    }
}
