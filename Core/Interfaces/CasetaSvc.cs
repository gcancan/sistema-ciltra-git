﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.BusinessLogic;
using CoreFletera.Models;

namespace CoreFletera.Interfaces
{
    public class CasetaSvc : ICaseta
    {
        public OperationResult getCasetasByIdEstado(int idEstado = 0)
        {
            return new CasetaServices().getCasetasByIdEstado(idEstado);
        }

        public OperationResult getCasetasByIdRuta(int idRuta)
        {
            return new CasetaServices().getCasetasByIdRuta(idRuta);
        }

        public OperationResult saveCaseta(ref Caseta caseta)
        {
            return new CasetaServices().saveCaseta(ref caseta);
        }
    }
}
