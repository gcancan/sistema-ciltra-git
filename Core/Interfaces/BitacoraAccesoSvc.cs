﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.BusinessLogic;
namespace CoreFletera.Interfaces
{
    public class BitacoraAccesoSvc : IBitacoraAcceso
    {
        public OperationResult saveBitacoraAcceso(ref BitacoraAcceso bitacoraAcceso) =>
            new BitacoraAccesoSercices().saveBitacoraAcceso(ref bitacoraAcceso);
    }
}
