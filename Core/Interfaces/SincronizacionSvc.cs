﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.BusinessLogic;
using CoreFletera.Models;

namespace CoreFletera.Interfaces
{
    public class SincronizacionSvc : ISincronizacion
    {
        public OperationResult sincronizarProductos()
        {
            try
            {
                var respBusqueda = new SincronizacionServices().getConsultaUnidades();
                return respBusqueda;
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        public OperationResult sincronizarProveedores()
        {
            try
            {
                var resp = new SincronizacionServices().getConsultaProveedores();
                if (resp.typeResult == ResultTypes.success)
                {
                    OperationResult respSin = new SincronizacionServices().sincronizarProveedores(resp.result as List<Proveedor>);
                    return respSin;
                }
                else
                {
                    return resp;
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
}
