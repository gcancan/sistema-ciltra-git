﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.BusinessLogic;
namespace CoreFletera.Interfaces
{
    public class SueldoFijoSvc : ISueldoFijo
    {
        public OperationResult saveSueldoFijo(ref SueldoFijo sueldo)
        {
            return new SueldoFijoServices().saveSueldoFijo(ref sueldo);
        }

        public OperationResult getAllSueldoFijoOrByIdOperador(string idOperador = "")
        {
            return new SueldoFijoServices().getAllSueldoFijoOrByIdOperador(idOperador);
        }
    }
}
