﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Interfaces
{
    interface IRFID
    {
        OperationResult findByRFID(string rfid, ref ResultadoBusquedaRFID tipo);
        OperationResult asignarRFIDUnidades(ref UnidadTransporte unidadTrans, string rfid);
        OperationResult asignarRFIDOperador(ref Personal personal, string rfid);
    }
}
