﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.BusinessLogic;
namespace CoreFletera.Interfaces
{
    public class LiquidacionPegasoSvc : ILiquidacionPegaso
    {
        public OperationResult getAllConfigCliente()
        {
            return new LiquidacionPegasoServices().getAllConfigCliente();
        }

        public OperationResult getCongByIdCliente(int idCliente)
        {
            return new LiquidacionPegasoServices().getCongByIdCliente(idCliente);
        }

        public OperationResult getLiquidacionById(int idLiquidacion)
        {
            return new LiquidacionPegasoServices().getLiquidacionById(idLiquidacion);
        }

        public OperationResult saveConfiguracionCliente(ref ConfiguracionClienteLiquidacion config)
        {
            return new LiquidacionPegasoServices().saveConfiguracionCliente(ref config);
        }

        public OperationResult saveLiquidacionPegaso(ref LiquidacionPegaso liquidacion)
        {
            return new LiquidacionPegasoServices().saveLiquidacionPegaso(ref liquidacion);
        }
    }
}
