﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Interfaces
{
    interface IProveedor
    {
        OperationResult getAllProveedores();
        OperationResult getAllProveedoresBiIdMarca(int idMarca);
        OperationResult saveProveedor(ref ProveedorCombustible proveedor);
        OperationResult getAllProveedoresV2();
        OperationResult getAllProveedoresUnidades();
    }
}
