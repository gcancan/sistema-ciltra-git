﻿namespace Core.Interfaces
{
    using Core.BusinessLogic;
    using Core.Models;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.InteropServices;

    public class TipoUnidadSvc : ITipoUnidad
    {
        public OperationResult getAllTipoUnidades()
        {
            try
            {
                var resp = new TipoUnidadTransporteServices().getAllTipoUnidades();
                if (resp.typeResult == ResultTypes.success)
                {
                    List<TipoUnidad> listaTipoUnidades = resp.result as List<TipoUnidad>;
                    var respDescripcion = new TipoUnidadTransporteServices().getAllDescripcionLlantaByIdTipoUnidad();
                    if (respDescripcion.typeResult == ResultTypes.success)
                    {
                        List<DescripcionLlantas> listaDescripcion = respDescripcion.result as List<DescripcionLlantas>;
                        foreach (var item in listaTipoUnidades)
                        {
                            item.listaDescripcionLlantas = listaDescripcion.FindAll(s => s.idTipoUnidad == item.clave);
                            item.NoEjes = item.listaDescripcionLlantas.Count.ToString();
                            item.NoLlantas = item.listaDescripcionLlantas.Sum(s => s.cantLlantas).ToString();
                        }
                    }
                    else
                    {
                        return respDescripcion;
                    }

                    return resp;
                }
                else
                {
                    return resp;
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
            

        public OperationResult getGruposTiposUnidad() =>
            new TipoUnidadTransporteServices().getGruposTiposUnidad();

        public OperationResult getGruposTiposUnidad(int idEmpresa) =>
            new TipoUnidadTransporteServices().getGruposTiposUnidad(idEmpresa);

        public OperationResult getTiposUnidadesALLorById(int id = 0) =>
            new TipoUnidadTransporteServices().getTiposUnidadesALLorById(id);

        public OperationResult saveTipoUnidad(ref TipoUnidad tipoUnidad) =>
            new TipoUnidadTransporteServices().saveTipoUnidad(ref tipoUnidad);
        public OperationResult sincronizarTiposUnidad(List<TipoUnidad> listaTiposUnidad) =>
            new TipoUnidadTransporteServices().sincronizarTiposUnidad(listaTiposUnidad);
    }
}
