﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using Core.BusinessLogic;
namespace Core.Interfaces
{
    public class MotivosViajeFalsoSvc : IMotivosViajeFalso
    {
        public OperationResult getMotivosAllOrById(int idMotivo = 0)
        {
            return new MotivoViajeFalsoService().getMotivosAllOrById(idMotivo);
        }
    }
}
