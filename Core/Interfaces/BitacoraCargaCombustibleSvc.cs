﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.BusinessLogic;
namespace CoreFletera.Interfaces
{
    public class BitacoraCargaCombustibleSvc : IBitacoraCargaCombustible
    {
        public OperationResult getBitacoraCargaCombustible(DateTime fechaInicial, DateTime fechaFinal, int idEmpresa, string tractor = "%", int idProvvedor = 0)
        {
            return new BitacoraCargaCombustibleService().getBitacoraCargaCombustible(fechaInicial, fechaFinal, idEmpresa, tractor, idProvvedor);
        }
        public OperationResult getBitacoraCargaCombustibleByFiltros(DateTime fechaInicial, DateTime fechaFinal, int idEmpresa, int idZonaOperativa,
            List<string> listaProveedores, bool todosProveedores, List<string> listaUnidades, bool todasUnidades, List<string> listaOperadores, bool todosOperadores) =>
            new BitacoraCargaCombustibleService().getBitacoraCargaCombustibleByFiltros(fechaInicial, fechaFinal, idEmpresa, idZonaOperativa,
                listaProveedores, todosProveedores, listaUnidades, todasUnidades, listaOperadores, todosOperadores);
    }
}
