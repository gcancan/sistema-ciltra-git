﻿using Core.Models;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Interfaces
{
    interface ISolicitudOrdenCompra
    {
        OperationResult saveSolicitudOrdenCompra(ref SolicitudOrdenCompra solicitud, List<int> listaPreOC);
        OperationResult getSolicitudOrdComAllOrById(int idSolicitud = 0);
        OperationResult cancelarSolicitudOrdCom(int idSolicitud);
    }
}
