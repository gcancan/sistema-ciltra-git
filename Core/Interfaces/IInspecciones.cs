﻿using Core.Models;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Interfaces
{
    interface IInspecciones
    {
        OperationResult saveInspecciones(ref Inspecciones inspeccion);
        OperationResult getInspeccionesByIdTipoUniTrans(int idTipoUnidadTrans, int idTipoOrdenServicio, int idOrden);
        OperationResult getInsumosByInspeccion(int idInspeccion, int idOrden);
        OperationResult getInspeccionesByFiltros(int idTipoOrden, GrupoTipoUnidades grupo);
        OperationResult getInspeccionesRealizadas(int idOrden);
    }
}
