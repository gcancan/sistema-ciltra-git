﻿using Core.Models;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Interfaces
{
    interface ICitaMantenimiento
    {
        OperationResult saveCitaMantenimiento(ref CitaMantenimiento citaMantenimiento);
        OperationResult getAllCitasMatto();
        OperationResult getReporteCitasMatto(List<string> listaUnidades, DateTime fechaInicial, DateTime fechaFinal);
    }
}
