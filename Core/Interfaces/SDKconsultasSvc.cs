﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using Core.BusinessLogic;
namespace Core.Interfaces
{
    public class SDKconsultasSvc : ISDKconsultas
    {
        public OperationResult buscarDatosCliente(int id)
        {
            return new SDKconsultasServices().buscarDatosCliente(id);
        }

        public OperationResult buscarDatosEmpresa(int id)
        {
            return new SDKconsultasServices().buscarDatosEmpresa(id);
        }

        public OperationResult existeProductoComercial(string id, string empresa = "")
        {
            return new SDKconsultasServices().existeProductoComercial(id, empresa);
        }

        public OperationResult updateMovimiento(string idMovimiento, string remision, string Producto)
        {
            return new SDKconsultasServices().updateMovimiento(idMovimiento, remision, Producto);
        }
    }
}
