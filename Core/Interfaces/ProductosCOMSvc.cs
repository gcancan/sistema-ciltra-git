﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.BusinessLogic;

namespace CoreFletera.Interfaces
{
    public class ProductosCOMSvc : IProductosCOM
    {
        public OperationResult deleteProductoCOM(ProductosCOM producto)
        {
            return new ProductosCOMServices().deleteProductoCOM(producto);
        }
        public OperationResult getInsumosCOM()
        {
            return new ProductosCOMServices().getInsumosCOM();
        }
        public OperationResult getInsumosCOMbyIdOrdenActividad(int idOrdenActividad)
        {
            return new ProductosCOMServices().getInsumosCOMbyIdOrdenActividad(idOrdenActividad);
        }
        public OperationResult saveProductoCOM(ref ProductosCOM producto)
        {
            return new ProductosCOMServices().saveProductoCOM(ref producto);
        }
    }
}
