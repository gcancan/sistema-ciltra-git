﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.BusinessLogic;
using CoreFletera.Models;

namespace CoreFletera.Interfaces
{
    public class ModeloSvc : IModelo
    {
        public OperationResult getAllModelos() => new ModeloServices().getAllModelos();
        public OperationResult getAllModelosByMarca(int idMarca) => new ModeloServices().getAllModelosByMarca(idMarca);
        public OperationResult saveModelo(ref Modelo modelo) => new ModeloServices().saveModelo(ref modelo);
    }
}
