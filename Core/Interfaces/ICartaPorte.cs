﻿namespace Core.Interfaces
{
    using Core.Models;
    using CoreFletera.Models;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    public interface ICartaPorte
    {
        OperationResult activarViajeBachoco(int idViaje, eEstatus estatus);
        OperationResult asignarCargaCombustibleViaje(int idVale, int idViaje);
        OperationResult cambiarValesCarga(List<ValeCarga> listVales, int valeGasolina, int userFin, DateTime fechaInicio, DateTime fechaFin, bool finalizar = true);
        OperationResult cancelarViajeBachoco(int numGuiaId, eEstatus estatus, string motivo);
        OperationResult cerrarCancelarCartaPorte(int idCartaPorte, TipoAccion tipoAccion, int idUsuario, string motivo = "", DateTime? fecha = new DateTime?());
        OperationResult ConsultarViajesActivos(int idCliente);
        OperationResult ConsultarViajesFinalizados(int idCliente);
        OperationResult getCartaPorteByFecha(DateTime fechaInicio, DateTime fechaFin, string idCliente, int idEmpresa);
        OperationResult getCartaPorteByfiltros(DateTime fechaInicio, DateTime fechaFin, string idDestino, string idOperador, string idTractor, string estatus, string idcliente, int idEmpresa, string tipoViaje = "%");
        OperationResult getCartaPorteById(int clave);
        OperationResult getCartaPorteByIdCamion(string idCamion);
        OperationResult getCartaPortesFinalizados(DateTime fechaInicio, DateTime fechaFin, int idCliente = 0);
        OperationResult getResumenCartaPortesByidCargaCombustible(int idCargaCombustible);
        OperationResult getUltimoViajeByIdTractor(string idTractor);
        OperationResult getValesCargaByIdViaje(Viaje viaje);
        OperationResult getViajeByEventoCSI(DateTime fecha, string idTractor);
        OperationResult getViajesActivosByIdTractor(string idTractor);
        OperationResult getViajesOperador(int idOperador, DateTime fechaInicio, DateTime fechaFin);
        OperationResult saveAjusteViaje(ref Viaje viaje, int? idOperadorFinaliza = new int?());
        OperationResult saveCartaPorte(Viaje cartaPorte, ref int ordenTrabajo, bool omitirRegraRemisiones = false, bool genSalida = false, bool genOrdenCom = false);
        OperationResult saveCartaPorte_V2(ref Viaje cartaPorte, ref int ordenTrabajo, bool omitirRegraRemisiones = false, bool genSalida = false, bool genOrdenCom = false);
        OperationResult saveCartaPorteAtlante(Viaje cartaPorte, bool omitirRegraRemisiones = false);
        OperationResult saveDetalleViaje(List<CartaPorte> listaDetalles, int numGuiaId);
        OperationResult saveRemisionesLiberadas(Viaje viaje);
        OperationResult saveValesCarga(List<ValeCarga> listVales);
        OperationResult getDatosUltimoViajeByTC(string tc);
        OperationResult getUltimaFechaEntradaPlanta(string tractor, DateTime? fecha = null);
        OperationResult ligarViajeVale(int idViaje, int idVale);
        OperationResult getNoViajeOperador(DateTime fecha, int idOperador);
        OperationResult getViajeByEventoCSI(ref List<ParadaEquipo> listaParadas);
        OperationResult bajarRemisiones(List<string> listaConsecutivos, int idUsuario, string motivo);
        OperationResult sincronizarViajes(List<ReporteCartaPorteViajes> listViajes, List<ReporteCartaPorte> listaCartaPortes);
        OperationResult saveComentarioViajes(int numViaje, string usuario, string comentario, DateTime fecha);
        OperationResult getListaComentariosViajes(List<string> listaId);
        OperationResult getAnticipoMasterByFolioViaje(int folioViaje);
    }
}
