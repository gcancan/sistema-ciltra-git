﻿namespace CoreFletera.Interfaces
{
    using Core.Models;
    using CoreFletera.Models;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    internal interface ILlanta
    {
        OperationResult asignarOrdenLlantera(int idOrden, Usuario usuario, Personal personalEntrega, Personal personalResp, Personal personalAyu1);
        OperationResult finalizarOrdenesTrabajo(List<DetalleLlantera> lista, int idOrdenSer);
        OperationResult getAllLLantasProducto();
        OperationResult getCondicionLlanta();
        OperationResult getDetallesServLlantera(int idOrdenServ);
        OperationResult getFoliosLlantaByIdOrden(int idOrdenCompra, int idProductoLlanta);
        OperationResult getLLantaByPosAndUnidadTrans(int posicion, UnidadTransporte unidadTransporte);
        OperationResult getLlantasByIdEmpresa(List<string> listaEmpresa);
        OperationResult getLlantasByOrdenCompra(OrdenCompra ordenCompra);
        OperationResult getLlantasEnAlmacen(int idAlmacen = 0, string folioLlanta = "%");
        OperationResult getLlantasParaRevitalizar(int idEmpresa, decimal profundidadInicial, decimal profundidadFinal);
        OperationResult getLlantasPendientesByIdOrdenCompra(int idOrdenCompra);
        OperationResult getMedidasLlantas();
        OperationResult getProductoLlantasByMedida(string medida);
        OperationResult getTiposLlantas();
        OperationResult saveLlanta(ref Llanta llanta, string usuario);
        OperationResult saveLlantasFromOrdenCompra(List<Llanta> listaLlantas, List<RelacionDetalleOrdenFactura> listaRelacion, string nombreUsuario, OrdenCompra ordenCompra);
        OperationResult saveProductoLlanta(ref LLantasProducto lLantasProducto);
    }
}
