﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.BusinessLogic;
namespace CoreFletera.Interfaces
{
    public class FacturaServicioSvc : IFacturaServicio
    {
        public OperationResult saveFacturaServicio(ref FacturaServicios facturaServicios) => new FacturaServicioServices().saveFacturaServicio(ref facturaServicios);
    }
}
