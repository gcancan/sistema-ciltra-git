﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.BusinessLogic;

namespace CoreFletera.Interfaces
{
    public class TramoSvc : ITramo
    {
        public OperationResult saveTramo(ref Tramo tramo)
        {
            return new TramoServices().saveTramo(ref tramo);
        }
        public OperationResult getAllTramos()
        {
            return new TramoServices().getAllTramos();
        }
    }
}
