﻿using Core.Models;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Interfaces
{
    interface IMesContable
    {
        OperationResult getMesContable(int mes, int anio, int idEmpresa);
        OperationResult mesContableAbiertoByIdEmpresa(int idEmpresa);
        OperationResult abrirMesContable(ref MesContable mesContable);
        OperationResult cerrarMesContable(ref MesContable mesContable);
    }
}
