﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.BusinessLogic;
using CoreFletera.Models;

namespace CoreFletera.Interfaces
{
    public class DiagramasLLantasSvc : IDiagramaLLantas
    {
        public OperationResult getDiagramaByTipoUnidadTrans(TipoUnidad tipoUnidad)
        {
            OperationResult resp = new DiagramaLLantasServices().getDiagramaByTipoUnidadTrans(tipoUnidad);
            if (resp.typeResult == ResultTypes.success)
            {
                DiagramaLLantas diagrama = (DiagramaLLantas)resp.result;
                var r = new DiagramaLLantasServices().getDetalleDiagrama(diagrama.idClasificacionLLanta);
                if (r.typeResult == ResultTypes.success)
                {
                    diagrama.listaDetalles = (List<DetallesDiagrama>)r.result;
                }
                else
                {
                    return r;
                }
            }
            return resp;
        }
    }
}
