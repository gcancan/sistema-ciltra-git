﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using Core.BusinessLogic;
namespace Core.Interfaces
{
    public class PrivilegioSvc : IPrivilegio
    {
        public OperationResult consultarPrivilegio(string privilegio, int idUsuario)
        {
            return new PrivilegiosServices().consultarPrivilegio(privilegio, idUsuario);
        }

        public OperationResult getAllPrivilegiosEspeiales()
        {
            return new PrivilegiosServices().getAllPrivilegiosEspeiales();
        }

        public OperationResult getPrivilegiosALLorById(int id = 0)
        {
            return new PrivilegiosServices().getPrivilegiosALLorById(id);
        }

        public OperationResult getPrivilegiosByIdUsuario(int id)
        {
            return new PrivilegiosServices().getPrivilegiosByIdUsuario(id);
        }

        public OperationResult quitarPrivilegio(int idUsuario, int idPrivilegio)
        {
            return new PrivilegiosServices().quitarPrivilegio(idUsuario, idPrivilegio);
        }
    }
}
