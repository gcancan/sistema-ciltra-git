﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Interfaces
{
    interface IBitacoraCargaCombustible
    {
        OperationResult getBitacoraCargaCombustible(DateTime fechaInicial, DateTime fechaFinal, int idEmpresa, string tractor = "%", int idProvvedor = 0);
        OperationResult getBitacoraCargaCombustibleByFiltros(DateTime fechaInicial, DateTime fechaFinal, int idEmpresa, int idZonaOperativa,
            List<string> listaProveedores, bool todosProveedores, List<string> listaUnidades, bool todasUnidades, List<string> listaOperadores, bool todosOperadores);
    }
}
