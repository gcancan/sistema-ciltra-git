﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.BusinessLogic;
namespace CoreFletera.Interfaces
{
    public class BaseSvc : IBase
    {
        public OperationResult getAllBase()
        {
            return new BaseServices().getAllBase();
        }

        public OperationResult saveBase(ref Base bse)
        {
            return new BaseServices().saveBase(ref bse);
        }
    }
}
