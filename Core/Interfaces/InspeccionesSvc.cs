﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.BusinessLogic;
namespace CoreFletera.Interfaces
{
    public class InspeccionesSvc : IInspecciones
    {
        public OperationResult getInspeccionesByFiltros(int idTipoOrden, GrupoTipoUnidades grupo)
        {
            try
            {
                var resp = new InspeccionesServices().getInspeccionesByFiltros(idTipoOrden, grupo);
                if (resp.typeResult == ResultTypes.success)
                {
                    var lista = (List<Inspecciones>)resp.result;
                    foreach (var item in lista)
                    {
                        var respB = new InspeccionesServices().getInsumosByInspeccion(item.idInspeccion, 0);
                        if (respB.typeResult == ResultTypes.success)
                        {
                            item.listaInsumos = (List<ProductoInsumo>)respB.result;
                        }
                        else if (respB.typeResult == ResultTypes.recordNotFound)
                        {
                            item.listaInsumos = new List<ProductoInsumo>();
                        }
                        else
                        {
                            return respB;
                        }
                    }
                }
                return resp;
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }

        public OperationResult getInspeccionesByIdTipoUniTrans(int idTipoUnidadTrans, int idTipoOrdenServicio, int idOrden)
        {
            //return new InspeccionesServices().getInspeccionesByIdTipoUniTrans(idTipoUnidadTrans);
            try
            {
                var resp = new InspeccionesServices().getInspeccionesByIdTipoUniTrans(idTipoUnidadTrans, idTipoOrdenServicio, idOrden);
                if (resp.typeResult == ResultTypes.success)
                {
                    var lista = (List<Inspecciones>)resp.result;
                    foreach (var item in lista)
                    {
                        var respB = new InspeccionesServices().getInsumosByInspeccion(item.idInspeccion, idOrden);
                        if (respB.typeResult == ResultTypes.success)
                        {
                            item.listaInsumos = (List<ProductoInsumo>)respB.result;
                        }
                        else if (respB.typeResult == ResultTypes.recordNotFound)
                        {
                            item.listaInsumos = new List<ProductoInsumo>();
                        }
                        else
                        {
                            return respB;
                        }
                    }
                }
                return resp;
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }

        public OperationResult getInspeccionesRealizadas(int idOrden)
        {
            try
            {
                var resp = new InspeccionesServices().getInspeccionesRealizadas(idOrden);
                if (resp.typeResult == ResultTypes.success)
                {
                    var lista = (List<Inspecciones>)resp.result;
                    foreach (var item in lista)
                    {
                        var respB = new InspeccionesServices().getInsumosByInspeccion(item.idInspeccion, idOrden);
                        if (respB.typeResult == ResultTypes.success)
                        {
                            item.listaInsumos = (List<ProductoInsumo>)respB.result;
                        }
                        else if (respB.typeResult == ResultTypes.recordNotFound)
                        {
                            item.listaInsumos = new List<ProductoInsumo>();
                        }
                        else
                        {
                            return respB;
                        }
                    }
                }
                return resp;
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }

        public OperationResult getInsumosByInspeccion(int idInspeccion, int idOrden)
        {
            return new InspeccionesServices().getInsumosByInspeccion(idInspeccion, idOrden);
        }

        public OperationResult saveInspecciones(ref Inspecciones inspeccion)
        {
            return new InspeccionesServices().saveInspecciones(ref inspeccion);
        }
    }
}
