﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoreFletera.Models;
namespace CoreFletera.Interfaces
{
    interface IClasificacionRutas
    {
        OperationResult getClasificacionRutas(int idCliente = 0, int idClasificacion = 0);
        OperationResult saveClasificacionRutas(ref ClasificacionRutas clasificacion);
        OperationResult getClasificacionRutasByViaje(int idCliente, decimal km);
    }
}
