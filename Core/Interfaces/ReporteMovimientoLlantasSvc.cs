﻿namespace CoreFletera.Interfaces
{
    using Core.Models;
    using CoreFletera.BusinessLogic;
    using System;
    using System.Collections.Generic;

    public class ReporteMovimientoLlantasSvc : IReporteMovimientoLlantas
    {
        public OperationResult getMovimientoLlantas(DateTime fechaInicio, DateTime fechaFin, List<string> listaLlantas) =>
            new ReporteMovimientoLlantasService().getMovimientoLlantas(fechaInicio, fechaFin, listaLlantas);
    }
}
