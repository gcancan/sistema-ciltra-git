﻿namespace CoreFletera.Interfaces
{
    using Core.Models;
    using CoreFletera.Models;
    using System;
    using System.Runtime.InteropServices;

    internal interface IDiseñosLlanta
    {
        OperationResult getDiseñosLlanta();
        OperationResult getDiseñosLlantaAllOrById(int idDiseño = 0);
        OperationResult saveDiseñoLlanta(ref Diseño diseño);
    }
}
