﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using Core.BusinessLogic;
using CoreFletera.Models;

namespace Core.Interfaces
{
    public class MasterOrdServSvc : IMasterOrdServ
    {
        public OperationResult cancelarOrdenServicio(TipoOrdenServicio orden, string motivo, string usuario)
        {
            return new MasterOrdServService().cancelarOrdenServicio(orden, motivo, usuario);
        }
        public OperationResult cancelarOrdenServicio_v2(TipoOrdenServicio orden, string motivo, string usuario)
            => new MasterOrdServService().cancelarOrdenServicio_v2(orden, motivo, usuario);
        public OperationResult cancelarOrdenTrabajo(OrdenTaller orden, string motivo, string usuario)
        {
            return new MasterOrdServService().cancelarOrdenTrabajo(orden, motivo, usuario);
        }

        public OperationResult cancelarOrdenTrabajo_v2(OrdenTaller orden, string motivo, string usuario)
            => new MasterOrdServService().cancelarOrdenTrabajo_v2(orden, motivo, usuario);

        public OperationResult getActividadesByOrdenSer(int folio)
        {
            //return new MasterOrdServService().getActividadesByOrdenSer(folio);
            try
            {
                OperationResult resp = new MasterOrdServService().getActividadesByOrdenSer(folio);
                if (resp.typeResult == ResultTypes.success)
                {
                    List<Actividad> listActividades = (List<Actividad>)resp.result;
                    foreach (var actividad in listActividades)
                    {
                        OperationResult respPro = new MasterOrdServService().getProductosByIdActividad(actividad.idActividad, folio);
                        if (respPro.typeResult == ResultTypes.success)
                        {
                            actividad.listProductos = (List<ProductosActividad>)respPro.result;
                        }
                        else if (respPro.typeResult == ResultTypes.recordNotFound)
                        {
                            actividad.listProductos = new List<ProductosActividad>();
                        }
                        else                       
                        {
                            return respPro;
                        }
                    }
                }
                return resp;

            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }

        public OperationResult getMasterOrdServ(int folio)
        {
            return new MasterOrdServService().getMasterOrdServ(folio);
        }

        public OperationResult getMasterOrdServByCamion(string idTractor)
        {
            return new MasterOrdServService().getMasterOrdServByCamion(idTractor);
        }

        public OperationResult getTrabajosByOrdenSer(int folio)
        {
            return new MasterOrdServService().getTrabajosByOrdenSer(folio);
        }
    }
}
