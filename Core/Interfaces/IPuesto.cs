﻿using Core.Models;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Interfaces
{
    interface IPuesto
    {
        OperationResult getALLPuestosById(int id = 0);
        OperationResult savePuesto(ref Puesto puesto);
        OperationResult sincronizarPuestos(List<Puesto> listaPuestos);
    }
}
