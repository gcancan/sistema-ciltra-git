﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Interfaces
{
    public interface IPerfilReporte
    {
        OperationResult savePerfilReporte(ref PerfilReporte perfilReporte);
        OperationResult getPerfilesReportes(int idUsuario, enumPerfilRporte tipoReporte);
    }
}
