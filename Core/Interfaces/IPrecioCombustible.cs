﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
namespace CoreFletera.Interfaces
{
    interface IPrecioCombustible
    {
        OperationResult savePrecioCombustible(ref PrecioCombustible precioCombustible);
    }
}
