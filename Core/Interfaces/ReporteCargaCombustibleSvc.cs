﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.BusinessLogic;
namespace CoreFletera.Interfaces
{
    public class ReporteCargaCombustibleSvc : IReporteCargaCombustible
    {
        public OperationResult getReporteCombustibleByFiltros(int idEmpresa, DateTime fechaInicial, DateTime fechaFinal, List<string> listaZonas, bool todosZonas, List<string> listaProveedores, bool todosProveedores, List<string> listaUnidades, bool todasUnidades, List<string> listaOperadores, bool todosOperadores)
        {
            return new ReporteCargaCombustibleServices().getReporteCombustibleByFiltros(idEmpresa, fechaInicial, fechaFinal, listaZonas,
                todosZonas, listaProveedores, todosProveedores, listaUnidades, todasUnidades,
                listaOperadores, todosOperadores);
        }
    }
}
