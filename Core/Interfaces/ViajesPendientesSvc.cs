﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.BusinessLogic;
namespace CoreFletera.Interfaces
{
    public class ViajesPendientesSvc : IViajesPendientes
    {
        public OperationResult saveViajesCargasPendientes(ref Viaje viaje, ref CargaCombustible carga)
        {
            return new ViajesPendientesServices().saveViajesCargasPendientes(ref viaje, ref carga);
        }
    }
}
