﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.BusinessLogic;
using CoreFletera.Models;

namespace CoreFletera.Interfaces
{
    public class OrdenesTrabajoSvc : IOrdenesTrabajo
    {
        public OperationResult enviarTallerExterno(int idOrdenServicio)
        {
            return new OrdenesTrabajoServices().enviarTallerExterno(idOrdenServicio);
        }

        public OperationResult getAllActividades()
        {
            return new OrdenesTrabajoServices().getAllActividad();
        }

        public OperationResult getAllServicios()
        {
            try
            {
                var resp = new OrdenesTrabajoServices().getAllServicios();
                if (resp.typeResult == ResultTypes.success)
                {
                    List<Servicio> lista = (List<Servicio>)resp.result;
                    foreach (var item in lista)
                    {
                        var respAct = new OrdenesTrabajoServices().getListaActividadesByIdServicio(item.idServicio);
                        if (respAct.typeResult == ResultTypes.success)
                        {
                            item.listaActividades = (List<Actividad>)respAct.result;
                        }
                        else if (respAct.typeResult == ResultTypes.recordNotFound)
                        {
                            item.listaActividades = new List<Actividad>();
                        }
                        else
                        {
                            return respAct;
                        }
                    }

                }
                return resp;
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }

        public OperationResult getDivisiones()
        {
            return new OrdenesTrabajoServices().getDivisiones();
        }

        public OperationResult getRelacionActividad(int idTipoOrden)
        {
            return new OrdenesTrabajoServices().getRelacionActividad(idTipoOrden);
        }

        public OperationResult getRelacionServicios(int idTipoOrden)
        {
            //return new OrdenesTrabajoServices().getRelacionServicios(idTipoOrden);
            try
            {
                var resp = new OrdenesTrabajoServices().getRelacionServicios(idTipoOrden);
                if (resp.typeResult == ResultTypes.success)
                {
                    List<Servicio> lista = (List<Servicio>)resp.result;
                    foreach (var item in lista)
                    {
                        var respAct = new OrdenesTrabajoServices().getListaActividadesByIdServicio(item.idServicio);
                        if (respAct.typeResult == ResultTypes.success)
                        {
                            item.listaActividades = (List<Actividad>)respAct.result;
                        }
                        else if (respAct.typeResult == ResultTypes.recordNotFound)
                        {
                            item.listaActividades = new List<Actividad>();
                        }
                        else
                        {
                            return respAct;
                        }
                    }

                }
                return resp;
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }

        public OperationResult getTipoDeOrden(int idTipoOrdenServ)
        {
            return new OrdenesTrabajoServices().getTipoDeOrden(idTipoOrdenServ);
        }

        public OperationResult saveDetOrdenServ(List<DetOrdenServicio> list)
        {
            return new OrdenesTrabajoServices().saveDetOrdenServ(list);
        }

        public OperationResult saveRelServAct(Servicio servicio, List<Actividad> listActividad, bool activo = true)
        {
            return new OrdenesTrabajoServices().saveRelServAct(servicio, listActividad, activo);
        }
    }
}
