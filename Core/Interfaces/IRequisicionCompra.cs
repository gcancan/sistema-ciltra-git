﻿namespace CoreFletera.Interfaces
{
    using Core.Models;
    using System;
    internal interface IRequisicionCompra
    {        OperationResult getRequisicioCompraByFecha(DateTime fechaInicial, DateTime fechaFinal);
    }
}
