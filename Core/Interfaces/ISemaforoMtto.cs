﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Interfaces
{
    interface ISemaforoMtto
    {
        OperationResult getSemaforoMtto(List<string> listaUnidades);
    }
}
