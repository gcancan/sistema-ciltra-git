﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.BusinessLogic;
namespace CoreFletera.Interfaces
{
    public class PaisSvc : IPais
    {
        public OperationResult getAllPais() =>
            new PaisServices().getAllPais();

        public OperationResult savePais(ref Pais pais) =>
            new PaisServices().savePais(ref pais);
    }
}
