﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using Core.BusinessLogic;
namespace Core.Interfaces
{
    public class OperadorSvc : IOperador
    {
        public OperationResult getPersonalALLorById(int id = 0)
        {
            return new OperadoresServices().getPersonalALLorById(id);
        }
        public OperationResult getOperadoresALLorById(int id = 0, int idEmpresa = 0)
        {
            return new OperadoresServices().getOperadoresALLorById(id, idEmpresa);
        }
        public OperationResult getOperadoresByNombre(string nombre, int idEmpresa = 0, int idCliente = 0)
        {
            return new OperadoresServices().getOperadoresByNombre(nombre, idEmpresa, idCliente);
        }
        public OperationResult getDespachadoresByNombre(string nombre = "%")
        {
            return new OperadoresServices().getDespachadoresByNombre(nombre);
        }
        public OperationResult getPersonalByNombre(string nombre, int idEmpresa = 0)
        {
            return new OperadoresServices().getPersonalByNombre(nombre, idEmpresa);
        }
        public OperationResult getLavadoresByNombre(string nombre, int idEmpresa = 0)
        {
            return new OperadoresServices().getLavadoresByNombre(nombre, idEmpresa);
        }
        public OperationResult getLlanterosByNombre(string nombre, int idEmpresa = 0)
        {
            return new OperadoresServices().getLlanterosByNombre(nombre, idEmpresa);
        }
        public OperationResult getOperadoresViaje(DateTime fechaInicio, DateTime fechaFin)
        {
            return new OperadoresServices().getOperadoresViaje(fechaInicio, fechaFin);
        }
        public OperationResult getOperadoresByIdCliente(int idCliente) => 
            new OperadoresServices().getOperadoresByIdCliente(idCliente);

        public OperationResult getOperadoresByNombreAndCliente(string nombre, int idCliente) =>
            new OperadoresServices().getOperadoresByNombreAndCliente(nombre, idCliente);

        public OperationResult getMecanicosByNombre(string nombre, int idEmpresa = 0) =>
            new OperadoresServices().getMecanicosByNombre(nombre, idEmpresa);

        public OperationResult savePersonal(ref Personal personal) =>
            new OperadoresServices().savePersonal(ref personal);

        public OperationResult getPersonal_v2() =>
            new OperadoresServices().getPersonal_v2();
        public OperationResult getPersonalById_v2(int idPersonal) =>
            new OperadoresServices().getPersonalById_v2(idPersonal);

        public OperationResult getPersonal_v2ByIdEmpresa(int idEmpresa) =>
            new OperadoresServices().getPersonal_v2ByIdEmpresa(idEmpresa);
        public OperationResult getPersonal_v2ByDepartamento(int idDepto) =>
            new OperadoresServices().getPersonal_v2ByDepartamento(idDepto);
        public OperationResult getPersonal_v2Activos() =>
            new OperadoresServices().getPersonal_v2Activos();
        public OperationResult getPersonalByDepartamentos(List<string> listaDeptos) =>
            new OperadoresServices().getPersonalByDepartamentos(listaDeptos);
        public OperationResult getPersonalByDepartamentosAndPuesto(List<string> listaDeptos, List<string> listaPuestos)=>
            new OperadoresServices().getPersonalByDepartamentosAndPuesto(listaDeptos, listaPuestos);
        public OperationResult getPersonalConApoyo() =>
            new OperadoresServices().getPersonalConApoyo();
        public OperationResult getPersonal_v2ByClave(int idPersonal) =>
            new OperadoresServices().getPersonal_v2ByClave(idPersonal);
        public OperationResult savePagosApoyos(List<Personal> listaPersonal)=>
            new OperadoresServices().savePagosApoyos(listaPersonal);
        public OperationResult getListaDatosIngreso(int idPersonal) =>
            new OperadoresServices().getListaDatosIngreso(idPersonal);
        public OperationResult getContactoEmergencia(int idPersonal) =>
            new OperadoresServices().getContactoEmergencia(idPersonal);
        public OperationResult getDocumentosPersonalByIdPersonal(int idPersonal) =>
            new OperadoresServices().getDocumentosPersonalByIdPersonal(idPersonal);
        public OperationResult getLicenciasConducirByIdPersonal(int idPersonal) =>
            new OperadoresServices().getLicenciasConducirByIdPersonal(idPersonal);
        public OperationResult sincronizarPersonal(List<Personal> listaPersonal, string usuario) =>
            new OperadoresServices().sincronizarPersonal(listaPersonal, usuario);
        public OperationResult getPersonal_v2ByIdPersonal(int idPersonal)=>
            new OperadoresServices().getPersonal_v2ByIdPersonal(idPersonal);
    }
}
