﻿using Core.Models;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Interfaces
{
    interface IMasterOrdServ
    {
        OperationResult getMasterOrdServ(int folio);
        OperationResult getMasterOrdServByCamion(string idTractor);
        OperationResult getActividadesByOrdenSer(int folio);
        OperationResult getTrabajosByOrdenSer(int folio);
        OperationResult cancelarOrdenTrabajo(OrdenTaller orden, string motivo, string usuario);
        OperationResult cancelarOrdenTrabajo_v2(OrdenTaller orden, string motivo, string usuario);
        OperationResult cancelarOrdenServicio(TipoOrdenServicio orden, string motivo, string usuario);
        OperationResult cancelarOrdenServicio_v2(TipoOrdenServicio orden, string motivo, string usuario);
    }
}
