﻿namespace CoreFletera.Interfaces
{
    using Core.Models;
    using CoreFletera.BusinessLogic;
    using System;
    using System.Collections.Generic;

    public class InventarioLlantasSvc : IInventarioLlantas
    {
        public OperationResult getInventarioLlantas(List<string> listaUnidades) =>
            new InventarioLlantasServices().getInventarioLlantas(listaUnidades);
    }
}
