﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using Core.BusinessLogic;
namespace Core.Interfaces
{
    public class SucursalSvc : ISucursal
    {
        public OperationResult getSucursalALLorById(int id = 0)
        {
            return new SucursalesServices().getSucursalALLorById(id);
        }
    }
}
