﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.BusinessLogic;
using Core.Interfaces;

namespace CoreFletera.Interfaces
{
    public class SolicitudOrdenServicioSvc : ISolicitudOrdenServicio
    {
        public OperationResult actualizarCabOrdenServicio(ref CabOrdenServicio cabOrdenServicio) =>
            new SolicitudOrdenServicioServices().actualizarCabOrdenServicio(ref cabOrdenServicio);

        public OperationResult cancelarActividadOrdenServicio(ActividadOrdenServicio actividadOrdenServicio) =>
            new SolicitudOrdenServicioServices().cancelarActividadOrdenServicio(actividadOrdenServicio);

        public OperationResult getCapOrdenServicioByFolioOrden(int folio) =>
            new SolicitudOrdenServicioServices().getCapOrdenServicioByFolioOrden(folio);

        public OperationResult getMasterOrdenServicioByFolio(int folio)
        {
            var resp = new SolicitudOrdenServicioServices().getMasterOrdenServicioByFolio(folio);
            if (resp.typeResult == ResultTypes.success)
            {
                MasterOrdServ masterOrdServ = resp.result as MasterOrdServ;
                foreach (var cab in masterOrdServ.listaCabOrdenServicio)
                {
                    if (cab.enumTipoOrdServ == enumTipoOrdServ.COMBUSTIBLE_EXTRA)
                    {
                        var respCargaExtra = new CargaCombustibleSvc().getCargaCombustibleExtraByOrdenServ(cab.idOrdenSer);
                        if (respCargaExtra.typeResult == ResultTypes.success)
                        {
                            cab.cargaCombustibleExtra = respCargaExtra.result as CargaCombustibleExtra;
                        }
                    }
                }
            }
            return resp;
        }
            

        public OperationResult saveSolicitudOrdenServicio(ref MasterOrdServ masterOrdServ, ref List<CargaCombustibleExtra> listaCargaExtra, bool sincronizarCombustible = false)
        {
            var resp = new SolicitudOrdenServicioServices().saveSolicitudOrdenServicio(ref masterOrdServ, ref listaCargaExtra, sincronizarCombustible);
            if (resp.typeResult == ResultTypes.success)
            {
                if (masterOrdServ.cargaCombustible != null)
                {
                    CargaCombustible cargaGas = masterOrdServ.cargaCombustible;
                    OperationResult result2 = new KardexCombustibleServices().ajustarKardexCombustible(cargaGas.idCargaCombustible, cargaGas.fechaCombustible, cargaGas.ltCombustible, TipoOperacionDiesel.CARGA);
                    if (result2.typeResult > ResultTypes.success)
                    {
                        return result2;
                    }
                    else
                    {
                        return result2;
                    }
                }
                else
                {
                    return resp;
                }
            }
            else
            {
                return resp;
            }
        }           

        public OperationResult saveActividadesPendientes(ref List<ActividadPendiente> listActPend) =>
            new SolicitudOrdenServicioServices().saveActividadesPendientes(ref listActPend);
        public OperationResult getActividadesPendientes(int idEmpresa, DateTime fechaInicial, DateTime fechaFinal, List<string> listaIds) =>
            new SolicitudOrdenServicioServices().getActividadesPendientes(idEmpresa, fechaInicial, fechaFinal, listaIds); 
        public OperationResult bajaActividadesPendientes(List<string> listId, string usuario, string motivoCancelacion, int idMotivo) =>
            new SolicitudOrdenServicioServices().bajaActividadesPendientes(listId, usuario, motivoCancelacion, idMotivo);
        public OperationResult getActividadesPendientes(UnidadTransporte unidad) =>
            new SolicitudOrdenServicioServices().getActividadesPendientes(unidad);

        public OperationResult getActividadesPendientes(List<string> listaIds) =>
            new SolicitudOrdenServicioServices().getActividadesPendientes(listaIds);

        public OperationResult getReporteOrdenesServicio(int idEmpresa, DateTime fechaInicio, DateTime fechaFinal, List<string> listaClientes, List<string> listaUnidades,
            List<string> listaTipoOrden, List<string> listaTipoOrdenServ, List<string> listaEstatus)
        {
            return new SolicitudOrdenServicioServices().getReporteOrdenesServicio(idEmpresa, fechaInicio, fechaFinal, listaClientes, listaUnidades,
                listaTipoOrden, listaTipoOrdenServ, listaEstatus);
        }
    }
}
