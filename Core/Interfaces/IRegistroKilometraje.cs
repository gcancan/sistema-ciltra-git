﻿namespace Core.Interfaces
{
    using Core.Models;
    using System;

    internal interface IRegistroKilometraje
    {
        OperationResult getUltimoKM(string idUnidadTrans);
        OperationResult getUltimoKM(string idUnidadTrans, DateTime fecha);
        OperationResult getUltimoKMVerificacion(string idUnidadTrans);
        OperationResult getUltimoRegistroValido(string idUnidadTrans);
        OperationResult saveRegistokilometraje(ref RegistroKilometraje registroKM, decimal km);
    }
}
