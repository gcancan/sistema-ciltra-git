﻿using Core.Models;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Interfaces
{
    interface ITicketsCombustible
    {
        OperationResult getTicketsCombustibleByProveedor(int idProveedor, int idEmpresa, int idTipoCombustible);
        OperationResult saveFaturaProveedorCombustible(ref FacturaProveedorCombustible factura);
        OperationResult getFacturasProveedorDieselByFiltros(int idEmpresa, DateTime fechaInicial, DateTime fechaFinal, List<string> listaZonasOperativas, bool todasZonasOpe,
            List<string> listaProveedores, bool todosProveedores, int idTipoCombustible);
        OperationResult getFacturasProveedorByFactura(int idProveedor, string strFactura);
    }
}
