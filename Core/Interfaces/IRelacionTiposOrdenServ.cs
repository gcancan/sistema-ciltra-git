﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
namespace CoreFletera.Interfaces
{
    interface IRelacionTiposOrdenServ
    {
        OperationResult getTiposOrdenes();
        OperationResult getAllActividades();
        OperationResult getAllServicios();
        OperationResult getAllTipoDeOrden();
        OperationResult saveRelacioOrdenServ(TipoOrdenServClass tipoOrden, CatTipoOrdenServ catalogo, int idRelacion);
        OperationResult deleteRelacioOrdenServ(TipoOrdenServClass tipoOrden, CatTipoOrdenServ catalogo, int idRelacion);
    }
}
