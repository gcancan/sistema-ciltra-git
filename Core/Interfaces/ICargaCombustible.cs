﻿namespace Core.Interfaces
{
    using Core.Models;
    using CoreFletera.Models;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    internal interface ICargaCombustible
    {
        OperationResult aplicarFolioSalidaAlmacen(List<CargasCombustibleCOM> lista, int lIdDocto);
        OperationResult findUltimaCargaByIdUnidad(string idUnidadTrans);
        OperationResult getCargaCombustibleByIdUnidadTrans(string idUnidadTrans);
        OperationResult getCargaCombustibleExternoByTicket(string ticket);
        OperationResult getCargaCombustibleExtra(int idCargaExtra);
        OperationResult getCargaCombustibleExtraByOrdenServ(int idOrdenServ);
        OperationResult getCargasCom();
        OperationResult getDespachosCombustibleByFiltros(DateTime fechaInicio, DateTime fechaFin, int idTanque, string idEmpresa = "%");
        OperationResult getExistenciasDieselActual(int idTanque);
        OperationResult getFechas();
        OperationResult getLtsCombustibleByDia(DateTime fecha);
        OperationResult getTickets();
        OperationResult getTicketsByFechas(DateTime fechaInicial, DateTime fechaFinal);
        OperationResult getTipoOrdenCombustible(int id = 0);
        OperationResult getTipoOrdenCombustibleExtra();
        OperationResult insertFechaCargaCombustible(DateTime fecha);
        OperationResult saveCargaCombustibleEXtra(ref CargaCombustibleExtra carga, ref KardexCombustible kardexCombustible);
        OperationResult saveTipoCombustibleEtra(ref TipoOrdenCombustible tipoOrden);
        OperationResult getCargaCombustibleExtraLibresByUnidad(string idUnidad);
        OperationResult integracionValesCombustible(int idVale, List<CargaCombustibleExtra> listaCargasExtra, List<CargaTicketDiesel> listaCargasTickets);
        OperationResult desLigarCargasExtra(int idValeExtra);
        OperationResult desLigarCargasTicket(int idVale, int idCargaTicket, List<CargaTicketDiesel> listaCargasTickets);
        OperationResult getTicketsByFechas(DateTime fechaInicial, DateTime fechaFinal, int? idEmpresa);
        OperationResult getUltimoCierreProcesoCombustible(int idTanque);
        OperationResult getValesCargaByViaje(string idUnidad, DateTime fecha);
    }
}
