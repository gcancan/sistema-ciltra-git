﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.BusinessLogic;
namespace CoreFletera.Interfaces
{
    public class TanqueCombustibleSvc : ITanqueCombustible
    {
        public OperationResult getTanqueByZonaOperativa(ZonaOperativa zonaOperativa)
        {
            return new TanqueCombustibleServices().getTanqueByZonaOperativa(zonaOperativa);
        }
    }
}
