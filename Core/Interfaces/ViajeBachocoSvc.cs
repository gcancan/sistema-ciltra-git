﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using Core.BusinessLogic;
namespace Core.Interfaces
{
    public class ViajeBachocoSvc : IViajeBachoco
    {
        public OperationResult cerrarViajeBachoco(int idViaje)
        {
            return new ViajesBachocoServices().cerrarViajeBachoco(idViaje);
        }

        public OperationResult finalizarViajeBachoco(int idViaje, eEstatus estatus, int idUsuario, string motivo = "")
        {
            return new ViajesBachocoServices().finalizarViajeBachoco(idViaje, estatus, idUsuario, motivo);
        }

        public OperationResult getViajeBachocoByFechas(DateTime fechaInicio, DateTime fechaFin)
        {
            return new ViajesBachocoServices().getViajeBachocoByFechas(fechaInicio, fechaFin);
        }

        public OperationResult getViajeBachocoByFiltros(DateTime fechaInicio, DateTime fechaFin, string idDestino, string idOperador, string idTractor, string estatus, int idcliente)
        {
            return new ViajesBachocoServices().getViajeBachocoByFiltros(fechaInicio, fechaFin, idDestino, idOperador, idTractor, estatus, idcliente);
        }

        public OperationResult getViajeBachocoByIdCamion(string idCamion)
        {
            return new ViajesBachocoServices().getViajeBachocoByIdCamion(idCamion);
        }

        public OperationResult saveViajeBachoco(ref ViajeBachoco viajeBachoco)
        {
            return new ViajesBachocoServices().saveViajeBachoco(ref viajeBachoco);
        }
    }
}
