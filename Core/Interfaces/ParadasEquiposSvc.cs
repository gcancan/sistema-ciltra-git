﻿namespace CoreFletera.Interfaces
{
    using Core.Models;
    using CoreFletera.BusinessLogic;
    using CoreFletera.Models;
    using System;
    using System.Collections.Generic;

    public class ParadasEquiposSvc : IParadasEquipos
    {
        public OperationResult saveParadasEquipo(ref List<ParadaEquipo> listaParadas, UsuarioCSI usuarioCSI) =>
            new ParadasEquiposServices().saveParadasEquipo(ref listaParadas, usuarioCSI);
    }
}
