﻿namespace CoreFletera.Interfaces
{
    using Core.Models;
    using CoreFletera.BusinessLogic;
    using CoreFletera.Models;
    using System;
    public class UsuarioCSISvc : IUsuarioCSI
    {
        public OperationResult getAllUsuariosCSI() =>
            new UsuarioCSIServices().getAllUsuariosCSI();
        public OperationResult saveUsuarioCSI(ref UsuarioCSI usuarioCSI) =>
            new UsuarioCSIServices().saveUsuarioCSI(ref usuarioCSI);
    }
}
