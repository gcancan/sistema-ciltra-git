﻿using Core.Models;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Interfaces
{
    interface IPagoPorModalidad
    {
        OperationResult savePagoPorModalidad(ref PagoPorModalidad pagoXmodalidad);
        OperationResult getPagoModalidadByIdCliente(int idCliente);
        OperationResult getPagoModalidadByNoViaje(int idClasificacion, int noViaje);
    }
}
