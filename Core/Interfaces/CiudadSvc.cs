﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.BusinessLogic;
using CoreFletera.Models;

namespace CoreFletera.Interfaces
{
    public class CiudadSvc : ICiudad
    {
        public OperationResult getAllCiudad() =>
            new CiudadServices().getAllCiudad();

        public OperationResult saveCiudad(ref Ciudad ciudad) =>
            new CiudadServices().saveCiudad(ref ciudad);
    }
}
