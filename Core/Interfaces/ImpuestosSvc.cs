﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using Core.BusinessLogic;
namespace Core.Interfaces
{
    public class ImpuestosSvc : IImpuestos
    {
        public OperationResult getImpuestosALLorById(int id = 0)
        {
            return new ImpuestosServices().getImpuestosALLorById(id);
        }
    }
}
