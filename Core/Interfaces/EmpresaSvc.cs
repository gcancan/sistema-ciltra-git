﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using Core.BusinessLogic;
namespace Core.Interfaces
{
    public class EmpresaSvc : IEmpresa
    {
        public OperationResult getEmpresasALLorById(int id = 0)
        {
            return new EmpresaServices().getEmpresasALLorById(id);
        }
        public OperationResult sincronizarEmpresas(List<Empresa> listaEmpresas) =>
            new EmpresaServices().sincronizarEmpresas(listaEmpresas);
    }
}
