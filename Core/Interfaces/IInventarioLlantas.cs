﻿namespace CoreFletera.Interfaces
{
    using Core.Models;
    using System.Collections.Generic;

    internal interface IInventarioLlantas
    {
        OperationResult getInventarioLlantas(List<string> listaUnidades);
    }
}
