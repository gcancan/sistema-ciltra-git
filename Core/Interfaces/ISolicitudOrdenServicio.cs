﻿using Core.Models;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Interfaces
{
    interface ISolicitudOrdenServicio
    {
        OperationResult saveSolicitudOrdenServicio(ref MasterOrdServ masterOrdServ, ref List<CargaCombustibleExtra> listaCargaExtra, bool sincronizarCombustible = false);
        OperationResult getMasterOrdenServicioByFolio(int folio);
        OperationResult getCapOrdenServicioByFolioOrden(int folio);
        OperationResult actualizarCabOrdenServicio(ref CabOrdenServicio cabOrdenServicio);
        OperationResult cancelarActividadOrdenServicio(ActividadOrdenServicio actividadOrdenServicio);
        OperationResult saveActividadesPendientes(ref List<ActividadPendiente> listActPend);
        OperationResult getActividadesPendientes(UnidadTransporte unidad);
        OperationResult getActividadesPendientes(int idEmpresa, DateTime fechaInicial, DateTime fechaFinal, List<string> listaIds);
        OperationResult getActividadesPendientes(List<string> listaIds);
        OperationResult bajaActividadesPendientes(List<string> listId, string usuario, string motivoCancelacion, int idMotivo);
        OperationResult getReporteOrdenesServicio(int idEmpresa, DateTime fechaInicio, DateTime fechaFinal,
           List<string> listaClientes, List<string> listaUnidades, List<string> listaTipoOrden, List<string> listaTipoOrdenServ, List<string> listaEstatus);
    }
}
