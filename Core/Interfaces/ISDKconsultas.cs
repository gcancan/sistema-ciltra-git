﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Interfaces
{
    interface ISDKconsultas
    {
        OperationResult existeProductoComercial(string id, string empresa = "");
        OperationResult updateMovimiento(string idMovimiento, string remision, string Producto);
        OperationResult buscarDatosCliente(int id);
        OperationResult buscarDatosEmpresa(int id);
    }
}
