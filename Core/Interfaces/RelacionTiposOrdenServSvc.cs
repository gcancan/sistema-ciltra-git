﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.BusinessLogic;
using CoreFletera.Models;

namespace CoreFletera.Interfaces
{
    public class RelacionTiposOrdenServSvc : IRelacionTiposOrdenServ
    {
        public OperationResult deleteRelacioOrdenServ(TipoOrdenServClass tipoOrden, CatTipoOrdenServ catalogo, int idRelacion)
        {
            return new RelacionTiposOrdenServServices().deleteRelacioOrdenServ(tipoOrden, catalogo, idRelacion);
        }

        public OperationResult getAllActividades()
        {
            return new RelacionTiposOrdenServServices().getAllActividades();
        }

        public OperationResult getAllServicios()
        {
            return new RelacionTiposOrdenServServices().getAllServicios();
        }

        public OperationResult getAllTipoDeOrden()
        {
            return new RelacionTiposOrdenServServices().getAllTipoDeOrden();
        }

        public OperationResult getTiposOrdenes()
        {
            return new RelacionTiposOrdenServServices().getTiposOrdenes();
        }

        public OperationResult saveRelacioOrdenServ(TipoOrdenServClass tipoOrden, CatTipoOrdenServ catalogo, int idRelacion)
        {
            return new RelacionTiposOrdenServServices().saveRelacioOrdenServ(tipoOrden, catalogo, idRelacion);
        }
    }
}
