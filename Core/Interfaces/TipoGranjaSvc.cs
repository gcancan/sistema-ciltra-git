﻿using Core.Models;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoreFletera.BusinessLogic;
namespace CoreFletera.Interfaces
{
    public class TipoGranjaSvc : ITipoGranja
    {
        public OperationResult getAllTiposGranjas() =>
            new TipoGranjaServives().getAllTiposGranjas();

        public OperationResult saveTipoGranja(ref TipoGranja tipoGranja, string usuario) =>
            new TipoGranjaServives().saveTipoGranja(ref tipoGranja, usuario);
    }
}
