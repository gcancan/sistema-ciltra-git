﻿namespace CoreFletera.Interfaces
{
    using Core.Models;
    using System;
    using System.Collections.Generic;

    internal interface IReporteMovimientoLlantas
    {
        OperationResult getMovimientoLlantas(DateTime fechaInicio, DateTime fechaFin, List<string> listaLlantas);
    }
}
