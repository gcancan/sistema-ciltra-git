﻿namespace CoreFletera.Interfaces
{
    using Core.Models;
    using CoreFletera.BusinessLogic;
    using System;
    using System.Collections.Generic;
    using CoreFletera.Models;
    public class MovimientoInternoLlantaSvc : IMovimientoInternoLlanta
    {      

        public OperationResult guardarMovimientoInterno(List<MovimientoInternoLlanta> listaMovimientos)
        {
            return new MovimientoInternoLlantaServices().guardarMovimientoInterno(listaMovimientos);
        }
    }
}
