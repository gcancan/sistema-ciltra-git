﻿namespace CoreFletera.Interfaces
{
    using Core.Models;
    using CoreFletera.Models;
    using System;

    internal interface IRuta
    {
        OperationResult getAllRutas();
        OperationResult quitarRelacionRutaCasetas(int idRuta, int idCaseta);
        OperationResult saveRuta(ref Ruta ruta);
    }
}
