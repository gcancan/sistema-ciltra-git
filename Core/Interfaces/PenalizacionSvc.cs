﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using Core.BusinessLogic;

namespace Core.Interfaces
{
    public class PenalizacionSvc : IPenalizacion
    {
        public OperationResult getPenalizacionesByFechas(DateTime fechaInicio, DateTime fechaFin, int idCliente = 0)
        {
            //return new PenalizacionService().getPenalizacionesByFechas(fechaInicio, fechaFin, idCliente);
            var resp = new PenalizacionService().getPenalizacionesByFechas(fechaInicio, fechaFin, idCliente);
            if (resp.typeResult == ResultTypes.success)
            {
                List<Penalizacion> lista = (List<Penalizacion>)resp.result;
                foreach (Penalizacion item in lista)
                {
                    var respDet = new PenalizacionService().getDetallesPensalizaciones(item.idViaje);
                    if (respDet.typeResult == ResultTypes.success)
                    {
                        item.listaDetalles = (List<DetallePenalizacion>)respDet.result;
                    }
                    else
                    {
                        return respDet;
                    }
                }
            }
            return resp;
        }
    }
}
