﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.BusinessLogic;
namespace CoreFletera.Interfaces
{
    public class TipoCombustibleSvc : ITipoCombustible
    {
        public OperationResult saveTipoCombustible(ref TipoCombustible tipoCombustible)
        {
            return new TipoCombustibleServices().saveTipoCombustible(ref tipoCombustible);
        }

        public OperationResult getTiposCombustibleAllOrById(int idTipoCombustible = 0)
        {
            return new TipoCombustibleServices().getTiposCombustibleAllOrById(idTipoCombustible);
        }
    }
}
