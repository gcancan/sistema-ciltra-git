﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.BusinessLogic;
namespace CoreFletera.Interfaces
{
    public class SolicitudOrdenCompraSvc : ISolicitudOrdenCompra
    {
        public OperationResult cancelarSolicitudOrdCom(int idSolicitud)
        {
            return new SolicitudOrdenCompraServices().cancelarSolicitudOrdCom(idSolicitud);
        }

        public OperationResult getSolicitudOrdComAllOrById(int idSolicitud = 0)
        {
            return new SolicitudOrdenCompraServices().getSolicitudOrdComAllOrById(idSolicitud);
        }

        public OperationResult saveSolicitudOrdenCompra(ref SolicitudOrdenCompra solicitud, List<int> listaPreOC)
        {
            return new SolicitudOrdenCompraServices().saveSolicitudOrdenCompra(ref solicitud, listaPreOC);
        }
    }
}
