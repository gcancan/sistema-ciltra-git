﻿namespace Core.Interfaces
{
    using Core.BusinessLogic;
    using Core.Models;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    public class UnidadTransporteSvc : IUnidadTransporte
    {
        public OperationResult buscarRemolques(int idEmpresa = 0, string idUnidad = "%") =>
            new UnidadTransporteServices().buscarRemolques(idEmpresa, idUnidad);

        public OperationResult getAllUnidadesTransV2() =>
            new UnidadTransporteServices().getAllUnidadesTransV2();

        public OperationResult getDollys(int idEmpresa, string clave = "%", int idZonaOperativa = 0, int idCliente = 0) =>
            new UnidadTransporteServices().getDollys(idEmpresa, clave, idZonaOperativa, idCliente);

        public OperationResult getDollysByOperacion(int idEmpresa, int idZonaOperativa, int idCliente) =>
            new UnidadTransporteServices().getDollysByOperacion(idEmpresa, idZonaOperativa, idCliente);

        public OperationResult getDollysSinFlota(int idEmpresa, string clave = "%") =>
            new UnidadTransporteServices().getDollysSinFlota(idEmpresa, clave);

        public OperationResult getRemolquesByOperacion(int idEmpresa, int idZonaOperativa, int idCliente) =>
            new UnidadTransporteServices().getRemolquesByOperacion(idEmpresa, idZonaOperativa, idCliente);

        public OperationResult getTolvas(int idEmpresa, string clave = "%", int idZonaOperativa = 0, int idCliente = 0) =>
            new UnidadTransporteServices().getTolvas(idEmpresa, clave, idZonaOperativa, idCliente);
        public OperationResult getTolvasSinFlota(int idEmpresa, string clave = "%") =>
            new UnidadTransporteServices().getTolvasSinFlota(idEmpresa, clave);

        public OperationResult getTractores(int idEmpresa, string clave = "%", int idZonaOperativa = 0, int idCliente = 0) =>
            new UnidadTransporteServices().getTractores(idEmpresa, clave, idZonaOperativa, idCliente);

        public OperationResult getTractoresByOperacion(int idEmpresa, int idZonaOperativa, int idCliente) =>
            new UnidadTransporteServices().getTractoresByOperacion(idEmpresa, idZonaOperativa, idCliente);

        public OperationResult getTractoresSinFlota(int idEmpresa, string clave = "%") =>
            new UnidadTransporteServices().getTractoresSinFlota(idEmpresa, clave);

        public OperationResult getUnidadBySerieGps(string strConnection, string serie) =>
            new UnidadTransporteServices().getUnidadBySerieGps(strConnection, serie);

        public OperationResult getUnidadesALLorById(int idEmpresa, string id = "") =>
            new UnidadTransporteServices().getUnidadesALLorById(idEmpresa, id);

        public OperationResult getUnidadesByGrupos(int idEmpresa, List<string> listaCadena) =>
            new UnidadTransporteServices().getUnidadesByGrupos(idEmpresa, listaCadena);

        public OperationResult saveUnidadTransporte(ref UnidadTransporte unidadTransporte) =>
            new UnidadTransporteServices().saveUnidadTransporte(ref unidadTransporte);
        public OperationResult getUnidadesCarganByEmpresaZonaOperativa(int idEmpresa, int idZonaOperativa) =>
            new UnidadTransporteServices().getUnidadesCarganByEmpresaZonaOperativa(idEmpresa, idZonaOperativa);

        public OperationResult sincronizarUnidadesTransporte(List<UnidadTransporte> listaUnidades) =>
            new UnidadTransporteServices().sincronizarUnidadesTransporte(listaUnidades);
    }
}
