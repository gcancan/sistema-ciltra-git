﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.BusinessLogic;
namespace CoreFletera.Interfaces
{
    public class CierreProcesoViajeSvc : ICierreProcesoViaje
    {
        public OperationResult saveCierreProcesoViaje(ref CierreProcesoViajes cierreProceso, int idEmpresa, List<ReporteCartaPorteViajes> listViajes, List<ReporteCartaPorte> listaCartaPortes) =>
            new CierreProcesoViajeServices().saveCierreProcesoViaje(ref cierreProceso, idEmpresa, listViajes, listaCartaPortes);
    }
}
