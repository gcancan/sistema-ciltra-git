﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.BusinessLogic;

namespace CoreFletera.Interfaces
{
    public class PrecioCombustibleSvc : IPrecioCombustible
    {
        public OperationResult savePrecioCombustible(ref PrecioCombustible precioCombustible)
        {
            return new PrecioCombustibleServices().savePrecioCombustible(ref precioCombustible);
        }
    }
}
