﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.BusinessLogic;
namespace CoreFletera.Interfaces
{
    public class ParametrosSvc : IParametros
    {
        public OperationResult getParametrosSistema()
        {
            return new ParametrosServices().getParametrosSistema();
        }
    }
}
