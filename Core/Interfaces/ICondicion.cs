﻿namespace CoreFletera.Interfaces
{
    using Core.Models;
    using CoreFletera.Models;
    using System;
    using System.Runtime.InteropServices;

    internal interface ICondicion
    {
        OperationResult getCondicionAllOrById(int idCondicion = 0);
        OperationResult saveCondicionLlantas(ref CondicionLlanta condicionLlanta);
    }
}
