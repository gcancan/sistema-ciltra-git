﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using Core.BusinessLogic;
namespace Core.Interfaces
{
    public class TareasProgramadasSvc : ITareasProgramadas
    {
        public OperationResult getTareasProgramadas(int idCliente, int id = 0)
        {
            return new TareasProgramadasServices().getTareasProgramadas(idCliente, id);
        }

        public OperationResult saveTareasProgramadas(List<TareaProgramada> listTareas)
        {
            return new TareasProgramadasServices().saveTareasProgramadas(listTareas);
        }
    }
}
