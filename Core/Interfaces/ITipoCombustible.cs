﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoreFletera.Models;
using Core.Models;

namespace CoreFletera.Interfaces
{
    interface ITipoCombustible
    {
        OperationResult saveTipoCombustible(ref TipoCombustible tipoCombustible);
        OperationResult getTiposCombustibleAllOrById(int idTipoCombustible = 0);
    }
}
