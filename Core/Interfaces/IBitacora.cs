﻿using Core.Models;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Interfaces
{
    public interface IBitacora
    {
        OperationResult saveBitacora(ref BitacoraEntradasSalidas bitacora);
        OperationResult getBitacoraByIdTractor(string idTractor);
    }
}
