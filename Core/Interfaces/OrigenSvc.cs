﻿namespace Core.Interfaces
{
    using Core.BusinessLogic;
    using Core.Models;
    using System;
    using System.Runtime.InteropServices;

    public class OrigenSvc : IOrigen
    {
        public OperationResult getOrigenesAll() =>
            new OrigenServices().getOrigenesAll();

        public OperationResult getOrigenesAll(int idCliente = 0, int idOrigen = 0) =>
            new OrigenServices().getOrigenesAll(idCliente, idOrigen);

        public OperationResult saveOrigenCliente(ref Origen origen) =>
            new OrigenServices().saveOrigenCliente(ref origen);

        public OperationResult saveOrigenCliente_v2(ref Origen origen) =>
            new OrigenServices().saveOrigenCliente_v2(ref origen);
    }
}
