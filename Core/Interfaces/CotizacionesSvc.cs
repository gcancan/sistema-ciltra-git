﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.BusinessLogic;
namespace CoreFletera.Interfaces
{
    public class CotizacionesSvc : ICotizaciones
    {
        public OperationResult getCotizacionAllOrById(int idCotizacion = 0, bool detalle = true)
        {
            return new CotizacionesServices().getCotizacionAllOrById(idCotizacion, detalle);
        }

        public OperationResult getCotizacionAllOrByIdSolicitud(int idSolicitud)
        {
            return new CotizacionesServices().getCotizacionAllOrByIdSolicitud(idSolicitud);
        }

        public OperationResult getDetPreOrdCompraBy(int idPreOrd)
        {
            return new CotizacionesServices().getDetPreOrdCompraBy(idPreOrd);
        }

        public OperationResult getPreOrdCompraByEmpresa(int idEmpresa)
        {
            return new CotizacionesServices().getPreOrdCompraByEmpresa(idEmpresa);
        }
        public OperationResult getPreOrdCompraByFiltros(DateTime fechaInicio, DateTime fechaFin, int idEmpresa, string tipoOrden = "%")
        {
            return new CotizacionesServices().getPreOrdCompraByFiltros(fechaInicio, fechaFin, idEmpresa, tipoOrden);
        }

        public OperationResult saveCotizacion(ref CotizacionSolicitudOrdCompra cotizacion)
        {
            return new CotizacionesServices().saveCotizacion(ref cotizacion);
        }
    }
}
