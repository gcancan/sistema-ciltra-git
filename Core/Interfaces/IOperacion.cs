﻿namespace Core.Interfaces
{
    using Core.Models;
    using CoreFletera.Models;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    public interface IOperacion
    {
        OperationResult agregarInsumosExtra(List<ProductosActividad> listProducto, int ordenServ);
        OperationResult cargaCombustibleByIdVale(int idVale);
        OperationResult entregarOrdenServicio(int idOrdenServ, Personal personal, string usuario);
        OperationResult getInsumosByNombre(string nombre = "", bool taller = true, bool lavadero = true, bool llantera = true);
        OperationResult getOrdenServByFechas(DateTime fechaInicio, DateTime fechaFin, int tipoOrden = 0);
        OperationResult getOrdenServById(int idOrden);
        OperationResult getSalidaEntradaByCamion(string idCamion);
        OperationResult getSalidaEntradaByidORden(int idOrden);
        OperationResult getSalidasEntradasByIdOrdenServ(int idOrden);
        OperationResult getTiposOrdenServicio(int idTipoOrdenServicio = 0);
        OperationResult getUltimaOrdenServDiagByIdUnidadTrans(string unidadTransporte);
        OperationResult saveCargaGasolina(ref CargaCombustible cargaGas, int idEmpresa, KardexCombustible kardexCombustible = null, bool isAjuste = false);
        OperationResult saveCargaGasolinaExterno(ref CargaCombustible cargaGas, int idEmpresa, Personal operador, UnidadTransporte tractor, 
            UnidadTransporte remolque1, UnidadTransporte dolly, UnidadTransporte remolque2, ZonaOperativa zonaOperativa = null, string unidadCarga = "");
        OperationResult saveDetOrdenActividadExterno(List<OrdenTaller> listaOrdenes, Proveedor proveedor, string factura);
        OperationResult saveOperacion(List<string> lista, eTipoMovimiento tipoMovimiento);
        OperationResult savePreOrdenCompraDirecta(ref PreOrdenCompra preOrdenCompra);
        OperationResult savePreSalidaAlm(OrdenTaller orden, PreSalidaAlmacen preSalidaAlmacen, List<ProductosCOM> listaProducto, Personal persona, string usuario);
        OperationResult saveSalidaEntrada(List<SalidaEntrada> lista);
        OperationResult saveSalidaEntrada(ref SalidaEntrada salidaEntrada, int idUsuario);
        OperationResult cancelaValeCombustible(int idVale);
        OperationResult getCargasExtraByVale(int idVale);
    }
}
