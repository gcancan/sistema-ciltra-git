﻿namespace CoreFletera.Interfaces
{
    using Core.Models;
    using System;

    internal interface IPanelOSTransito
    {
        OperationResult getPanelOSTransitoByTipoOrden(int idTipoOrdenServ);
    }
}
