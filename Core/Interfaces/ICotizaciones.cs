﻿using Core.Models;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Interfaces
{
    interface ICotizaciones
    {
        OperationResult saveCotizacion(ref CotizacionSolicitudOrdCompra cotizacion);
        OperationResult getCotizacionAllOrById(int idCotizacion = 0, bool detalle = true);
        OperationResult getPreOrdCompraByEmpresa(int idEmpresa);
        OperationResult getDetPreOrdCompraBy(int idPreOrd);
        OperationResult getCotizacionAllOrByIdSolicitud(int idSolicitud);
        OperationResult getPreOrdCompraByFiltros(DateTime fechaInicio, DateTime fechaFin, int idEmpresa, string tipoOrden = "%");
    }
}
