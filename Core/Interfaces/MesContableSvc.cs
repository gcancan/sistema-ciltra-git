﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.BusinessLogic;
using CoreFletera.Models;

namespace CoreFletera.Interfaces
{
    public class MesContableSvc : IMesContable
    {
        public OperationResult abrirMesContable(ref MesContable mesContable)
        {
            return new MesContableServices().abrirMesContable(ref mesContable);
        }

        public OperationResult cerrarMesContable(ref MesContable mesContable)
        {
            return new MesContableServices().cerrarMesContable(ref mesContable);
        }

        public OperationResult getMesContable(int mes, int anio, int idEmpresa)
        {
            return new MesContableServices().getMesContable(mes, anio, idEmpresa);
        }

        public OperationResult mesContableAbiertoByIdEmpresa(int idEmpresa)
        {
            return new MesContableServices().mesContableAbiertoByIdEmpresa(idEmpresa);
        }
    }
}
