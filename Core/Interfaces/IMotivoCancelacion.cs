﻿using Core.Models;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Interfaces
{
    interface IMotivoCancelacion
    {
        OperationResult saveMotivosCancelacion(ref MotivoCancelacion motivo);
        OperationResult getAllMotivosCancelacion();
        OperationResult getProcesosBiIdMOtivoCancelacion(int idMotivoCancelacion);
        OperationResult getAllMotivosByProceso(enumProcesos enumProcesos);
    }
}
