﻿using Core.Models;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Interfaces
{
    interface IConsultasTaller
    {
        OperationResult consultarExistenByProducto(int idProducto);
        OperationResult getProductosCOM(AreaTrabajo area, string parametro = "%", bool soloExistencias = true);
        OperationResult getActividadesCOM(AreaTrabajo area, string parametro = "%");
    }
}
