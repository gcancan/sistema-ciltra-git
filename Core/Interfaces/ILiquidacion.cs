﻿using Core.Models;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Interfaces
{
    interface ILiquidacion 
    {
        OperationResult saveLiquidacion(ref Liquidacion liquidacion);
        OperationResult getLiquidacionesByFolio(int folio);
        OperationResult bajarConceptoLiquidacion(int idConceptoLiquidacion);
        OperationResult getAllLiquidaciones();
        OperationResult getInhabiles();
        OperationResult validarDepartamentoEnLiquidacion(List<string> listaIdDeptos);
        OperationResult getAllLiquidacionesByIdDetpto(int idDepartamento);
        OperationResult validarDepartamentoFechasEnLiquidacion(List<classCadenas> lista);
    }
}
