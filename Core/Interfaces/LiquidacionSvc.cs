﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.BusinessLogic;
namespace CoreFletera.Interfaces
{
    public class LiquidacionSvc : ILiquidacion
    {
        public OperationResult bajarConceptoLiquidacion(int idConceptoLiquidacion) =>
            new LiquidacionServices().bajarConceptoLiquidacion(idConceptoLiquidacion);

        public OperationResult getAllLiquidaciones() =>
            new LiquidacionServices().getAllLiquidaciones();

        public OperationResult getInhabiles() =>
            new LiquidacionServices().getInhabiles();

        public OperationResult getLiquidacionesByFolio(int folio) =>
            new LiquidacionServices().getLiquidacionesByFolio(folio);

        public OperationResult saveLiquidacion(ref Liquidacion liquidacion) =>
            new LiquidacionServices().saveLiquidacion(ref liquidacion);
        public OperationResult validarDepartamentoEnLiquidacion(List<string> listaIdDeptos) =>
            new LiquidacionServices().validarDepartamentoEnLiquidacion(listaIdDeptos);
        public OperationResult getAllLiquidacionesByIdDetpto(int idDepartamento) =>
            new LiquidacionServices().getAllLiquidacionesByIdDetpto(idDepartamento);
        public OperationResult validarDepartamentoFechasEnLiquidacion(List<classCadenas> lista) =>
            new LiquidacionServices().validarDepartamentoFechasEnLiquidacion(lista);
    }
}
