﻿namespace Core.Interfaces
{
    using Core.BusinessLogic;
    using Core.Models;
    using System;
    using System.Runtime.InteropServices;

    public class ProductoSvc : IProducto
    {
        public OperationResult deleteProductos(int clave) =>
            new ProductoServices().deleteProductos(clave);

        public OperationResult getALLProductosByidFraccionEmpresa(int idEmpresa, int idFraccion) =>
            new ProductoServices().getALLProductosByidFraccionEmpresa(idEmpresa, idFraccion);

        public OperationResult getALLProductosByIdORidFraccion(int idFraccion, int id = 0) =>
            new ProductoServices().getALLProductosByIdORidFraccion(id, idFraccion);

        public OperationResult saveProducto(ref Producto producto) =>
            new ProductoServices().saveProducto(ref producto);
        public OperationResult getAllUnidadesMedida() =>
            new ProductoServices().getAllUnidadesMedida();
    }
}
