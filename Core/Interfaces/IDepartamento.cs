﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
namespace Core.Interfaces
{
    public interface IDepartamento
    {
        OperationResult getDepartamentosALLorById(int id = 0);
        OperationResult getALLDepartamentosById(int id = 0);
        OperationResult saveDepartamento(ref Departamento departamento);
        OperationResult sincronizarDepartamentos(List<Departamento> listaDepartamentos);
    }
}
