﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.BusinessLogic;
using Core.Interfaces;

namespace CoreFletera.Interfaces
{
    public class BitacoraSvc : IBitacora
    {
        public OperationResult getBitacoraByIdTractor(string idTractor)
        {
            try
            {
                OperationResult resp = new BitacoraServices().getBitacoraByIdTractor(idTractor);
                if (resp.typeResult == ResultTypes.success)
                {
                    BitacoraEntradasSalidas bitacora = (BitacoraEntradasSalidas)resp.result;
                    OperationResult resultado = new UnidadTransporteSvc().getUnidadesALLorById(0, bitacora.tractor.clave);
                    if (resultado.typeResult == ResultTypes.success)
                    {
                        bitacora.tractor = ((List<UnidadTransporte>)resultado.result)[0];
                    }
                    else
                    {
                        return resultado;
                    }
                    if (bitacora.tolva1 != null)
                    {
                        resultado = new UnidadTransporteSvc().getUnidadesALLorById(0, bitacora.tolva1.clave);
                        if (resultado.typeResult == ResultTypes.success)
                        {
                            bitacora.tolva1 = ((List<UnidadTransporte>)resultado.result)[0];
                        }
                        else
                        {
                            return resultado;
                        }
                    }
                    
                    if (bitacora.dolly != null)
                    {
                        resultado = new UnidadTransporteSvc().getUnidadesALLorById(0, bitacora.dolly.clave);
                        if (resultado.typeResult == ResultTypes.success)
                        {
                            bitacora.dolly = ((List<UnidadTransporte>)resultado.result)[0];
                        }
                        else
                        {
                            return resultado;
                        }
                    }

                    if (bitacora.tolva2 != null)
                    {
                        resultado = new UnidadTransporteSvc().getUnidadesALLorById(0, bitacora.tolva2.clave);
                        if (resultado.typeResult == ResultTypes.success)
                        {
                            bitacora.tolva2 = ((List<UnidadTransporte>)resultado.result)[0];
                        }
                        else
                        {
                            return resultado;
                        }
                    }

                    resultado = new OperadorSvc().getOperadoresALLorById(bitacora.chofer.clave);
                    if (resultado.typeResult == ResultTypes.success)
                    {
                        bitacora.chofer = ((List<Personal>)resultado.result)[0];
                    }
                    else
                    {
                        return resultado;
                    }

                    resp.result = bitacora;
                    return resp;
                }
                return resp;
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }

        public OperationResult saveBitacora(ref BitacoraEntradasSalidas bitacora)
        {
            return new BitacoraServices().saveBitacora(ref bitacora);
        }
    }
}
