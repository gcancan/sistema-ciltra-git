﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Interfaces
{
    public interface IPrivilegio
    {
        OperationResult getPrivilegiosALLorById(int id = 0);
        OperationResult getAllPrivilegiosEspeiales();
        OperationResult getPrivilegiosByIdUsuario(int id);
        OperationResult quitarPrivilegio(int idUsuario, int idPrivilegio);
        OperationResult consultarPrivilegio(string privilegio, int idUsuario);
    }
}
