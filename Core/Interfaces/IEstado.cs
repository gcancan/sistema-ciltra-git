﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Interfaces
{
    interface IEstado
    {
        OperationResult getEstadosAllorById(int id = 0);
        OperationResult saveEstado(ref Estado estado);
    }
}
