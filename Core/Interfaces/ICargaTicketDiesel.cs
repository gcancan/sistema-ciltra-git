﻿using Core.Models;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Interfaces
{
    interface ICargaTicketDiesel
    {
        OperationResult saveCargaticketDiesel(ref CargaTicketDiesel carga);
        OperationResult getCargaTicketsDieselByEmpresaZonaOperativa(int idEmpresa, int idZonaOperativa);
        OperationResult getCargaTicketsDieselByUnidad(string idUnidad);
    }
}
