﻿namespace CoreFletera.Interfaces
{
    using Core.Models;
    using System.Collections.Generic;
    using CoreFletera.Models;
    internal interface IMovimientoInternoLlanta
    {
        OperationResult guardarMovimientoInterno(List<MovimientoInternoLlanta> listaMovimientos);
    }
}
