﻿using Core.Models;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Interfaces
{
    interface IActividad
    {
        OperationResult saveActividad(ref Actividad actividad);
        OperationResult getProductosCOMByActividad(int CIDPRODUCTO);
        OperationResult quitarRelActividadProducto(int CIDPRODUCTO, int idProducto);
        OperationResult getAllActividades(bool soloActivos = false);
        OperationResult getAllActividadesByTipoOrdenServ(enumTipoOrdServ enumTipoOrd);
    }
}
