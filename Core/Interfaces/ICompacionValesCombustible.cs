﻿using Core.Models;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Interfaces
{
    interface ICompacionValesCombustible
    {
        OperationResult getListaValesLis(string unidadTras, string mes, int año);
        OperationResult getListaVales(string unidadTras, int mes, int año);
        OperationResult saveValesLIS(List<ComparacionValesCombustible> lista);
    }
}
