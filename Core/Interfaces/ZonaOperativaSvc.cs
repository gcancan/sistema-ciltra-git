﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.BusinessLogic;
using CoreFletera.Models;

namespace CoreFletera.Interfaces
{
    public class ZonaOperativaSvc : IZonaOperativa
    {
        public OperationResult getAllZonasOperativas()
        {
            return new ZonaOpetaivaServices().getAllZonasOperativas();
        }

        public OperationResult sincronizarZonasOperativas(List<ZonaOperativa> listaZonas) =>
            new ZonaOpetaivaServices().sincronizarZonasOperativas(listaZonas);
    }
}
