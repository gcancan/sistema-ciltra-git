﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.BusinessLogic;
namespace CoreFletera.Interfaces
{
    public class RevitalizacionesSvc : IRevitalizaciones
    {
        public OperationResult getRevitalizacionById(int idRevitalizacion)
        {
            return new RevitalizacionesServices().getRevitalizacionById(idRevitalizacion);
        }

        public OperationResult saveRevitalizacion(ref Revitalizaciones revitalizacion)
        {
            return new RevitalizacionesServices().saveRevitalizacion(ref revitalizacion);
        }
    }
}
