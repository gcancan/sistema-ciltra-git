﻿namespace Core.Interfaces
{
    using Core.Models;
    using System;
    using System.Runtime.InteropServices;
    public interface IMarcas
    {
        OperationResult getMarcasALLorById(int id = 0);
        OperationResult getMarcasLlantas();
        OperationResult getMarcasUnidades();
        OperationResult saveMarcaLlantas(ref Marca marca);
    }
}
