﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.BusinessLogic;
using CoreFletera.Models;

namespace CoreFletera.Interfaces
{
    public class ConfiguracionEnvioCorreoSvc : IConfiguracionEnvioCorreo
    {
        public OperationResult getAllCorreosElectronicos() =>
            new ConfiguracionEnvioCorreosServices().getAllCorreosElectronicos();

        public OperationResult getCorreosByProceso(enumProcesosEnvioCorreo enumProcesos) =>
            new ConfiguracionEnvioCorreosServices().getCorreosByProceso(enumProcesos);

        public OperationResult getProcesosByIdCorreo(int idCorreo) =>
            new ConfiguracionEnvioCorreosServices().getProcesosByIdCorreo(idCorreo);

        public OperationResult saveCorreoElectronico(ref ConfiguracionEnvioCorreo envioCorreo) =>
            new ConfiguracionEnvioCorreosServices().saveCorreoElectronico(ref envioCorreo);
    }
}
