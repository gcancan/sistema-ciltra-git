﻿namespace CoreFletera.Interfaces
{
    using Core.Interfaces;
    using Core.Models;
    using CoreFletera.BusinessLogic;
    using CoreFletera.Models;
    using System;
    using System.Collections.Generic;

    public class TableroControlUnidadesSvc : ITableroControlUnidades
    {
        public OperationResult getTableroControlUnidad()
        {
            try
            {
                OperationResult result = new TableroControlUnidadesServices().getTableroControlUnidad();
                if (result.typeResult == ResultTypes.success)
                {
                    List<TableroControlUnidades> list = result.result as List<TableroControlUnidades>;
                    foreach (TableroControlUnidades unidades in list)
                    {
                        OperationResult result2 = new CartaPorteSvc().getCartaPorteById(unidades.numGuiaId);
                        if (result2.typeResult == ResultTypes.success)
                        {
                            unidades.viaje = (result2.result as List<Viaje>)[0];
                            this.ligarCartaPortesConTablero(unidades);
                        }
                        else
                        {
                            return result2;
                        }
                    }
                    return result;
                }
                return result;
            }
            catch (Exception exception)
            {
                return new OperationResult(exception);
            }
        }
        public OperationResult getTableroControlUnidadesByOperaciones(int idEmpresa, List<string> listIdZonas, List<string> listOperaciones)
        {
            try
            {
                OperationResult result = new TableroControlUnidadesServices().getTableroControlUnidadesByOperaciones(idEmpresa, listIdZonas, listOperaciones);
                if (result.typeResult == ResultTypes.success)
                {
                    List<TableroControlUnidades> list = result.result as List<TableroControlUnidades>;
                    foreach (TableroControlUnidades unidades in list)
                    {
                        if (unidades.numGuiaId == 106963)
                        {

                        }
                        OperationResult result2 = new CartaPorteSvc().getCartaPorteById(unidades.numGuiaId);
                        if (result2.typeResult == ResultTypes.success)
                        {
                            unidades.viaje = (result2.result as List<Viaje>)[0];
                            this.ligarCartaPortesConTablero(unidades);
                        }
                        else if (result2.typeResult == ResultTypes.error)
                        {
                            return result2;
                        }
                    }
                    return result;
                }
                return result;
            }
            catch (Exception exception)
            {
                return new OperationResult(exception);
            }
        }

        public OperationResult getUltimoTableroActivoByTractor(string idTractor)
        {
            OperationResult result = new TableroControlUnidadesServices().getUltimoTableroActivoByTractor(idTractor);
            if (result.typeResult == ResultTypes.success)
            {
                TableroControlUnidades tablero = result.result as TableroControlUnidades;
                OperationResult result2 = new CartaPorteSvc().getCartaPorteById(tablero.numGuiaId);
                if (result2.typeResult == ResultTypes.success)
                {
                    tablero.viaje = (result2.result as List<Viaje>)[0];
                }
                this.ligarCartaPortesConTablero(tablero);
                return result;
            }
            return result;
        }

        public OperationResult getUltimoTableroActivoByTractor(string idTractor, string strConnection)
        {
            OperationResult result = new TableroControlUnidadesServices().getUltimoTableroActivoByTractor(idTractor, strConnection);
           
            if (result.typeResult == ResultTypes.success)
            {
                TableroControlUnidades tablero = result.result as TableroControlUnidades;
                
                OperationResult result2 = new CartaPorteSvc().getCartaPorteById(tablero.numGuiaId, strConnection);
                //return result;
                if (result2.typeResult == ResultTypes.success)
                {
                    tablero.viaje = (result2.result as List<Viaje>)[0];
                }
                this.ligarCartaPortesConTablero(tablero);
                return result;
            }
            return result;
        }

        public OperationResult iniciarTrayectoGranja(ref TableroControlUnidades tablero) =>
            new TableroControlUnidadesServices().iniciarTrayectoGranja(ref tablero);

        private void ligarCartaPortesConTablero(TableroControlUnidades tablero)
        {
            Viaje viaje = tablero.viaje;
            foreach (DetalleTableroControlUnidades detalle in tablero.listaDetalles)
            {
                detalle.cartaPorte = viaje.listDetalles.Find(s => s.Consecutivo == detalle.consecutivo);
            }
        }

        public OperationResult registrarEntradaGranja(int idTablero, int idDetalleTablero, DateTime fecha) =>
            new TableroControlUnidadesServices().registrarEntradaGranja(idTablero, idDetalleTablero, fecha);

        public OperationResult registrarEntradaGranja(int idTablero, int idDetalleTablero, DateTime fecha, string strConnection) =>
            new TableroControlUnidadesServices().registrarEntradaGranja(idTablero, idDetalleTablero, fecha, strConnection);

        public OperationResult registrarSalidaGranja(int idTablero, int idDetalleTablero, DateTime fecha) =>
            new TableroControlUnidadesServices().registrarSalidaGranja(idTablero, idDetalleTablero, fecha);

        public OperationResult registrarSalidaGranja(int idTablero, int idDetalleTablero, DateTime fecha, string strConnection) =>
            new TableroControlUnidadesServices().registrarSalidaGranja(idTablero, idDetalleTablero, fecha, strConnection);

        public OperationResult saveRegistroCSI(string strConnection, ref RegistroCSI registroCSI) =>
            new TableroControlUnidadesServices().saveRegistroCSI(strConnection, ref registroCSI);

        public OperationResult iniciarViajeTablero(int idTablero, int folioViaje, DateTime fecha, string strConnection) =>
            new TableroControlUnidadesServices().iniciarViajeTablero(idTablero, folioViaje, fecha, strConnection);

        public OperationResult terminarViajeTablero(int idTablero, int folioViaje, DateTime fecha, string strConnection) =>
            new TableroControlUnidadesServices().terminarViajeTablero(idTablero, folioViaje, fecha, strConnection);

        public OperationResult regresarEventoEnPlanta(int idTablero, string strConnection) =>
            new TableroControlUnidadesServices().regresarEventoEnPlanta(idTablero, strConnection);

        public OperationResult entradaTallerTrayectoDestino(int idTablero, DateTime fecha, string strConnection) =>
            new TableroControlUnidadesServices().entradaTallerTrayectoDestino(idTablero, fecha, strConnection);

        public OperationResult salidaTallerTrayectoDestino(int idTablero, DateTime fecha, string strConnection) =>
            new TableroControlUnidadesServices().salidaTallerTrayectoDestino(idTablero, fecha, strConnection);

        public OperationResult entradaTallerTrayectoPlanta(int idTablero, DateTime fecha, string strConnection) =>
            new TableroControlUnidadesServices().entradaTallerTrayectoPlanta(idTablero, fecha, strConnection);

        public OperationResult salidaTallerTrayectoPlanta(int idTablero, DateTime fecha, string strConnection) =>
            new TableroControlUnidadesServices().salidaTallerTrayectoPlanta(idTablero, fecha, strConnection);

        public OperationResult saveComentarioTablero(ComentarioTablero comentario) =>
            new TableroControlUnidadesServices().saveComentarioTablero(comentario);

        public OperationResult regresarAGranja(int idTablero, int idDetalleTablero, DateTime fecha, string strConnection) =>
            new TableroControlUnidadesServices().regresarAGranja(idTablero, idDetalleTablero, fecha, strConnection);
    }
}
