﻿using Core.Models;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Interfaces
{
    interface IRescateLlantas
    {
        OperationResult saveRescateLlantas(ref RescateLlantas rescate);
        OperationResult getRescateByid(int idRescate);
        OperationResult getMotivoRescateAllOrBy(int idMotivoRescate=0);
    }
}
