﻿namespace Core.Interfaces
{
    using Core.Models;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    public interface ITipoUnidad
    {
        OperationResult getGruposTiposUnidad();
        OperationResult getGruposTiposUnidad(int idEmpresa);
        OperationResult getTiposUnidadesALLorById(int id = 0);
        OperationResult saveTipoUnidad(ref TipoUnidad tipoUnidad);
        OperationResult getAllTipoUnidades();
        OperationResult sincronizarTiposUnidad(List<TipoUnidad> listaTiposUnidad);
    }
}
