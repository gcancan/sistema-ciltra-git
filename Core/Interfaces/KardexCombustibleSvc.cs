﻿namespace CoreFletera.Interfaces
{
    using Core.Models;
    using CoreFletera.BusinessLogic;
    using CoreFletera.Models;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    public class KardexCombustibleSvc : IKardexCombustible
    {
        public OperationResult getKardexCombustible(int idTanque, DateTime fechaInicial, DateTime fechaFin) =>
            new KardexCombustibleServices().getKardexCombustible(idTanque, fechaInicial, fechaFin);

        public OperationResult getResumenKardexCombustible(DateTime fechaInicio, DateTime fechaFin, int idTanque, int idEmpresa = 0) =>
            new KardexCombustibleServices().getResumenKardexCombustible(fechaInicio, fechaFin, idTanque, idEmpresa);

        public OperationResult savekardexCombustible(ref KardexCombustible kardexCombustible) =>
            new KardexCombustibleServices().savekardexCombustible(ref kardexCombustible, false);
        public OperationResult getExisteciaDiarias(int idTanque, List<DateTime> listaFechas, int idEmpresa, bool todasEmpresas = false)=>
            new KardexCombustibleServices().getExisteciaDiarias(idTanque, listaFechas, idEmpresa, todasEmpresas);
    }
}
