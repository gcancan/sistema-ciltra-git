﻿namespace CoreFletera.Interfaces
{
    using Core.Models;
    using CoreFletera.Models;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    internal interface ISuministroCombustible
    {
        OperationResult getDetalleSuministroCombustible(DateTime fechaInicio, DateTime fechaFin, int idTanque, int idEmpresa = 0);
        OperationResult getNivelesIniciales(int idTanque, DateTime fecha);
        OperationResult saveSuministroCombustible(ref SuministroCombustible suministro);
        OperationResult saveNivelesIniciales(DateTime fecha, List<NivelInicial> listaNiveles, int idTanque, List<Despacho> listaDespachos, CierreProcesoCombustible cierreProceso = null);
        OperationResult getNivelesIniciales(int idTanque);
        OperationResult getSuministrosCombustible(int idTanque);
        OperationResult actualizarSuministro(SuministroCombustible suministro);
    }
}
