﻿using Core.Models;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Interfaces
{
    interface ICaseta
    {
        OperationResult getCasetasByIdEstado(int idEstado = 0);
        OperationResult saveCaseta(ref Caseta caseta);
        OperationResult getCasetasByIdRuta(int idRuta);
    }
}
