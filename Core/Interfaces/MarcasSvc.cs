﻿namespace Core.Interfaces
{
    using Core.BusinessLogic;
    using Core.Models;
    using System;
    using System.Runtime.InteropServices;

    public class MarcasSvc : IMarcas
    {
        public OperationResult getMarcasALLorById(int id = 0) =>
            new MarcasServices().getMarcasALLorById(id);

        public OperationResult getMarcasLlantas() =>
            new MarcasServices().getMarcasLlantas();

        public OperationResult getMarcasUnidades() =>
            new MarcasServices().getMarcasUnidades();

        public OperationResult saveMarcaLlantas(ref Marca marca) =>
            new MarcasServices().saveMarcaLlantas(ref marca);
    }
}
