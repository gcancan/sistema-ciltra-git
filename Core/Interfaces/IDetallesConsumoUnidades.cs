﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Interfaces
{
    interface IDetallesConsumoUnidades
    {
        OperationResult getDetallesConsumoUnidades(int idEmpresa, int idCliente, DateTime fechaInicio, DateTime fechaFin, List<string> listaTractores, bool todos = false);
    }
}
