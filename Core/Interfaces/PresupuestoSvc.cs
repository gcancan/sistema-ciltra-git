﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.BusinessLogic;
using CoreFletera.Models;

namespace CoreFletera.Interfaces
{
    public class PresupuestoSvc : IPresupuesto
    {
        public OperationResult getAllPresupuestos() =>
            new PresupuestoService().getAllPresupuestos();

        public OperationResult getAllPresupuestosByClave(int clave) =>
            new PresupuestoService().getAllPresupuestosByClave(clave);

        public OperationResult getDetallePresupuestosByClave(int clave) =>
            new PresupuestoService().getDetallePresupuestosByClave(clave);

        public OperationResult savePresupuesto(ref Presupuesto presupuesto) =>
            new PresupuestoService().savePresupuesto(ref presupuesto);

        public OperationResult bajarDetallePresupuesto(int idPresupuesto, int idModalidad, string usuario, string motivo) =>
            new PresupuestoService().bajarDetallePresupuesto(idPresupuesto, idModalidad, usuario, motivo);

        public OperationResult getAllIndicadoresViajes(int año, List<string> listaIdClientes) =>
            new PresupuestoService().getAllIndicadoresViajes(año, listaIdClientes);

        public OperationResult getPresupuestosByClientes(int año, List<string> listaIdClientes) =>
            new PresupuestoService().getPresupuestosByClientes(año, listaIdClientes);
        public OperationResult getAllIndicadoresCombustible(int año, List<string> listaZonaOpe, List<string> listaIdClientes) =>
            new PresupuestoService().getAllIndicadoresCombustible(año, listaZonaOpe, listaIdClientes);
    }
}
