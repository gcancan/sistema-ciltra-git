﻿namespace CoreFletera.Interfaces
{
    using Core.Models;
    using CoreFletera.BusinessLogic;
    using CoreFletera.Models;
    using System;
    using System.Runtime.InteropServices;
    using System.Collections.Generic;

    public class SuministroCombustibleSvc : ISuministroCombustible
    {
        public OperationResult getDetalleSuministroCombustible(DateTime fechaInicio, DateTime fechaFin, int idTanque, int idEmpresa = 0) =>
            new SuministroCombustibleServices().getDetalleSuministroCombustible(fechaInicio, fechaFin, idTanque, idEmpresa);

        public OperationResult getNivelesIniciales(int idTanque, DateTime fecha) =>
            new SuministroCombustibleServices().getNivelesIniciales(idTanque, fecha);

        public OperationResult getNivelesIniciales(int idTanque) =>
            new SuministroCombustibleServices().getNivelesIniciales(idTanque);

        public OperationResult getSuministrosCombustible(int idTanque) =>
            new SuministroCombustibleServices().getSuministrosCombustible(idTanque);

        public OperationResult saveNivelesIniciales(DateTime fecha, List<NivelInicial> listaNiveles, int idTanque, List<Despacho> listaDespachos, CierreProcesoCombustible cierreProceso = null) =>
            new SuministroCombustibleServices().saveNivelesIniciales(fecha, listaNiveles, idTanque, listaDespachos, cierreProceso);        

        public OperationResult saveSuministroCombustible(ref SuministroCombustible suministro) =>
            new SuministroCombustibleServices().saveSuministroCombustible(ref suministro);
        public OperationResult actualizarSuministro(SuministroCombustible suministro)=>
            new SuministroCombustibleServices().actualizarSuministro(suministro);
    }
}
