﻿using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class TipoOrdenServicio
    {
        public int idTipoOrdenServicio { get; set; }
        public string TipoServicio { get; set; }
        public int idOrdenSer { get; set; }
        public UnidadTransporte unidadTrans { get; set; } 
        public string estatus { get; set; }
        public Personal personaEntrega { get; set; }
        public DateTime fechaCaptura { get; set; }
        public string usuarioCrea { get; set; }
        public string chofer { get; set; }
        public int idChofer { get; set; }
        public TipoOrden tipoOrden { get; set; }
        public bool externo { get; set; }
        public Proveedor proveedor { get; set; }
        public string factura { get; set; }
    }
}
