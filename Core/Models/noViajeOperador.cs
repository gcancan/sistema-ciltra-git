﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class NoViajeOperador
    {
        public int noViaje { get; set; }
        public DateTime fechaFin { get; set; }
        public int horas { get; set; }
    }
}
