﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
namespace CoreFletera.Models
{
    public class CargaTicketDiesel
    {
        public int clave { get; set; }
        public Empresa empresa { get; set; }
        public ZonaOperativa zonaOperativa { get; set; }
        public UnidadTransporte tractor { get; set; }
        public ProveedorCombustible proveedor { get; set; }
        public DateTime? fecha { get; set; } 
        public string ticket { get; set; }
        public decimal precio { get; set; }
        public decimal litros { get; set; }
        public decimal importe => precio * litros;
        public string folioImpreso { get; set; }
        public string observaciones { get; set; }
        public CargaCombustible carga { get; set; }
        public Personal operador { get; set; }
    }
}
