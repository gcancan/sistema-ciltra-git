﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class DetallesConsumoUnidades
    {
        public string claveUnidad { get; set; }
        public decimal kmRecorridosSen { get; set; }
        public decimal kmRecorridosFull { get; set; }
        public decimal kmRecorridos { get; set; }
        public decimal ltsConsumidosSen { get; set; }
        public decimal ltsConsumidosFull { get; set; }
        public decimal ltsConsumidos { get; set; }
        public decimal viajesSen { get; set; }
        public decimal viajesFull { get; set; }
        public decimal viajes { get; set; }
        public decimal casetasSen { get; set; }
        public decimal casetasFull { get; set; }
        public decimal casetas { get; set; }
        public decimal dieselSen { get; set; }
        public decimal dieselFull { get; set; }
        public decimal diesel { get; set; }
        public decimal operadoresSen { get; set; }
        public decimal operadoresFull { get; set; }
        public decimal operadores { get; set; }
        public decimal lavadero { get; set; }
        public decimal llantas { get; set; }
        public decimal mtto { get; set; }
    }
}
