﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Models;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class OrdenTaller
    {
        public int idOrdenServ { get; set; }
        public int idOrdenTrabajo { get; set; }
        public int idOrdenActividad { get; set; }
        public Servicio servicio { get; set; }
        public Actividad actividad { get; set; }
        public string descripcionOS { get; set; }
        public string descripcionA { get; set; }
        public string preSalidas { get; set; }
        public string estatus { get; set; }
        public bool cancelado { get; set; }
        public Personal personal1 { get; set; }
        public Personal personal2 { get; set; }
        public Personal personal3 { get; set; }
        public string clave_llanta { get; set; }
        public string descripcion
        {
            get
            {
                if (actividad != null)
                {
                    return actividad.nombre;
                }
                else
                {
                    return descripcionOS;
                }
            }
        }

        public string descripcionActividad
        {
            get
            {
                if (actividad != null)
                {
                    return actividad.nombre + ( string.IsNullOrEmpty(descripcionA) ? string.Empty : ("(" + descripcionA + ")"));
                }
                else
                {
                    if (descripcionOS != descripcionA)
                    {
                        return descripcionOS + (string.IsNullOrEmpty(descripcionA) ? string.Empty : ("(" + descripcionA + ")"));
                    }
                    else
                    {
                        return descripcionOS;
                    }                    
                }
            }
        }

        public string tipoDescripcion
        {
            get
            {
                if (servicio != null)
                {
                    return "SERVICIO";
                }
                else if (actividad != null)
                {
                    return "ACTIVIDAD";
                }
                else
                {
                    return "FALLA REPORTADA";
                }
            }
        }
        public List<ProductosCOM> listaProductosCOM { get; set; }
        public decimal costoActividad { get; set; }
    }
}
