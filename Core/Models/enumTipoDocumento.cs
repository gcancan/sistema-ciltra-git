﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public enum enumTipoDocumento
    {
        Orden_Compra = 1,
        Orden_Servicio = 2,
        Orden_Revitalizado = 3,
        Orden_Baja = 4,
        Rescate = 5,
        Inventario = 6,
        Servicio_Interno = 7,
        Orden_Trabajo = 8,
        Revicion = 9
    }
}
