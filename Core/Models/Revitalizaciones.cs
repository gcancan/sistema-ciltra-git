﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class Revitalizaciones
    {
        public int idRevitalizacion { get; set; }
        public Proveedor proveedor { get; set; }
        public DateTime fechaSalida { get; set; }
        public DateTime ? fechaEntrada { get; set; }
        public string estatus { get; set; }
        public string usuario { get; set; }
        public List<DetalleRevitalizacion> listaDetalles { get; set; }
    }

    public class DetalleRevitalizacion
    {
        public int idDetalleRevitalizacion { get; set; }
        public Llanta llanta { get; set; }
        public Diseño diseño { get; set; }
        public decimal profundidadAct { get; set; }
        public decimal profundidadNueva { get; set; }
    }
}
