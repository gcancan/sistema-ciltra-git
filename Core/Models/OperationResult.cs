﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    [Serializable]
    public class OperationResult
    {
        private SqlCommand command;
        public OperationResult()
        {
            valor = null;
            mensaje = string.Empty;
            identity = null;
            result = null;
            unidadTrans = null;
        }
        public OperationResult(Exception ex)
        {
            valor = 1;
            mensaje = ex.Message;
            result = null;
            unidadTrans = null;
        }
        public OperationResult(ResultTypes resultTypes, string mensaje, object result = null)
        {
            valor = (int)resultTypes;
            this.mensaje = mensaje;
            this.result = result;
            unidadTrans = null;
        }
        public OperationResult(SqlCommand command, object result = null)
        {
            valor = (int)command.Parameters["@valor"].Value;
            mensaje = (string)command.Parameters["@mensaje"].Value;

            if (command.Parameters.Contains("@nuevos")) noNuevos = (int)command.Parameters["@nuevos"].Value;

            if (command.Parameters.Contains("@actualizados")) noActualizados = (int)command.Parameters["@actualizados"].Value;

            this.result = result ;
            unidadTrans = null;
        }
        public OperationResult(SqlCommand command, object result, object result2)
        {
            valor = (int)command.Parameters["@valor"].Value;
            mensaje = (string)command.Parameters["@mensaje"].Value;

            if (command.Parameters.Contains("@nuevos")) noNuevos = (int)command.Parameters["@nuevos"].Value;

            if (command.Parameters.Contains("@actualizados")) noActualizados = (int)command.Parameters["@actualizados"].Value;

            this.result = result;
            this.result2 = result2;
            unidadTrans = null;
        }
        public OperationResult(SqlCommand command, UnidadTransporte unidad)
        {
            valor = (int)command.Parameters["@valor"].Value;
            mensaje = (string)command.Parameters["@mensaje"].Value;
            this.result = result;
            unidadTrans = unidad;
        }
        public int? valor { get; set; }
        public int noNuevos { get; set; }
        public int noActualizados { get; set; }
        public string mensaje { get; set; }
        public ResultTypes typeResult
        {
            get
            {
                if (valor == null)
                {
                    return ResultTypes.warning;
                }
                return (ResultTypes)valor;
            }
        }
        public int? identity { get; set; }
        public object result { get; set; }
        public object result2 { get; set; }
        //public static explicit operator OperationResult(Task<OperationResult> v)
        //{
        //    //throw new NotImplementedException();
        //}
        public UnidadTransporte unidadTrans { get; set; }
    }
}
