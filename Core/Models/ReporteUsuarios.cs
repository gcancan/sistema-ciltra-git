﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class ReporteUsuarios
    {
        public int idUsuario { get; set; }
        public string nombreUsuario { get; set; }
        public string contraseña { get; set; }
        public string personal { get; set; }
        public string empresa { get; set; }
        public string zonaOperativa { get; set; }
        public string cliente { get; set; }
        public string estatus { get; set; }
        public ReportePrivilegios privilegios { get; set; }
    }
    public class ReportePrivilegios
    {
        public int idPrivilegio { get; set; }
        public string clave { get; set; }
        public string titulo { get; set; }
        public string descripcion { get; set; }
        public string estatus { get; set; }
    }
}
