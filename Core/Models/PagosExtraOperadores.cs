﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class PagosExtraOperadores
    {
        public int idPagosExtraOperadores { get; set; }
        public int idLiquidacion { get; set; }
        public string _motivoStr { get; set; }
        public string motivoStr
        {
            get
            {
                switch (motivo)
                {
                    case MotivoExtra.RESCATE:
                        return MotivoExtra.RESCATE.ToString();
                    case MotivoExtra.VIAJES_AL_60:
                        return MotivoExtra.VIAJES_AL_60.ToString();
                    case MotivoExtra.OTRO:
                        return _motivoStr;
                    default:
                        return "";
                }
            }
            set { _motivoStr = value; }
        }
        public MotivoExtra motivo { get; set; }
        public decimal importe { get; set; }
    }
    public enum MotivoExtra
    {
        RESCATE = 0,
        VIAJES_AL_60 = 1,
        OTRO = 1000
    }
}
