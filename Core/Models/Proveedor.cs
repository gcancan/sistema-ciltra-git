﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class Proveedor
    {
        public int idProveedor { get; set; }
        public string razonSocial { get; set; }
        public string RFC { get; set; }
        public string direccion { get; set; }
        public string telefono { get; set; }
        public int idColonia { get; set; }
        public string email { get; set; }
        public string tipoPersona { get; set; }
        public decimal ultimoCosto { get; set; }
        public int CIDCLIENTEPROVEEDOR { get; set; }
        public string CCODIGOCLIENTE { get; set; }
        public int CESTATUS { get; set; }
        public Colonia colonia { get; set; }
    }
}
