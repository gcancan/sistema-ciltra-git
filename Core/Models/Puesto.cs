﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class Puesto
    {
        public int idPuesto { get; set; }
        public string nombrePuesto { get; set; }
        public bool activo { get; set; }
        public string activoStr => activo ? "ACTIVO" : "BAJA";
    }
}
