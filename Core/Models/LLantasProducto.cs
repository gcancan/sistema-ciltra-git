﻿namespace CoreFletera.Models
{
    using Core.Models;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class LLantasProducto
    {       
        public int idLLantasProducto { get; set; }

        public Marca marca { get; set; }

        public Diseño diseño { get; set; }

        public string medida { get; set; }

        public decimal profundidad { get; set; }

        public bool activo { get; set; }

        public int DOT { get; set; }

        public string descripcion
        {
            get
            {
                string[] textArray1 = new string[] { this.marca.descripcion, " ", this.profundidad.ToString(), " mm ", this.diseño.descripcion };
                return string.Concat(textArray1);
            }
        }

        public string descripcionSinDiseño =>
            (this.marca.descripcion + " " + this.profundidad.ToString() + " mm ");

        public string medidaMarcaDiseño =>
            $"{((this.diseño.medida == null) ? "[Sin Medida]" : this.diseño.medida.medida)} {this.marca.descripcion} {this.diseño.descripcion}";
    }
}
