﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
namespace CoreFletera.Models
{
    public class OperacionFletera
    {
        public Empresa empresa { get; set; }
        public ZonaOperativa zonaOperativa { get; set;}
        public Cliente cliente { get; set; }
        public string nombre
        {
            get
            {
                if (cliente == null)
                {
                    return zonaOperativa.nombre;
                }
                return string.Format("({0})-{1}", zonaOperativa.nombre, cliente.nombre);
            }
        }
    }
    public class Flota
    {
        public int idFlota { get; set; }
        public Empresa empresa { get; set; }
        public ZonaOperativa zonaOperativa { get; set; }
        public Cliente cliente { get; set; }
        public UnidadTransporte tractor { get; set; }
        public UnidadTransporte remolque1 { get; set; }
        public UnidadTransporte dolly { get; set; }
        public UnidadTransporte remolque2 { get; set; }

        public Flota mapFlota(OperacionFletera operacionFletera)
        {
            empresa = operacionFletera.empresa;
            zonaOperativa = operacionFletera.zonaOperativa;
            cliente = operacionFletera.cliente;
            return this;
        }
    }
}
