﻿namespace Core.Models
{
    using CoreFletera.Models;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class Zona
    {
        public int clave_empresa { get; set; }

        public int? claveCliente { get; set; }

        public string descCliente
        {
            get
            {
                if (this.cliente == null)
                {
                    return this.descripcion;
                }
                return $"{this.descripcion} - ({this.cliente.nombre})";
            }
        }

        public OrigenPegaso origenPegaso { get; set; }
        public string origenPegasoDescripcion =>
            origenPegaso == null ? descripcion : string.Format("{0} - {1}", origenPegaso.nombre, descripcion);
        public Base baseDestino { get; set; }

        public Cliente cliente { get; set; }

        public int clave { get; set; }

        public string descripcion { get; set; }

        public string descripcionConKm =>
            $"{this.descripcion} {this.km.ToString("N2")} Km";

        public bool activo { get; set; }

        public decimal costoSencillo { get; set; }

        public decimal costoFull { get; set; }

        public decimal km { get; set; }

        public string claveZona { get; set; }

        public decimal costoSencillo35 { get; set; }

        public string claveExtra { get; set; }

        public string correo { get; set; }

        public Estado estado { get; set; }

        public bool viaje { get; set; }
        public string viajeStr => viaje ? "SI" : "NO";

        private bool _seleccionado { get; set; }
        public bool seleccionado
        {
            get =>
                this._seleccionado;
            set
            {
                _seleccionado = value;
            }
        }

        public decimal viajeSencillo { get; set; }

        public decimal viajeFull { get; set; }

        public bool incluyeCaseta { get; set; }

        public decimal precioCasetas { get; set; }

        public decimal sencillo_24 { get; set; }

        public decimal sencillo_30 { get; set; }

        public decimal sencillo_35 { get; set; }

        public decimal full { get; set; }

        public decimal precioFijoSencillo { get; set; }
        public decimal precioFijoSencillo35 { get; set; }
        public decimal precioFijoFull { get; set; }

        public bool estatus { get; set; }

        public ClasificacionPagoChofer clasificacionPagoChofer { get; set; }

        public string geoCerca { get; set; }

        public string baseNombre =>
            ((this.baseDestino != null) ? string.Format("{1}-{0}", this.baseDestino.nombreBase, this.descripcion) : this.descripcion);

        public string baseNombreConKm =>
            ((this.baseDestino != null) ? string.Format("{1}- BASE {0} {2} km", this.baseDestino.nombreBase, this.descripcion, this.km.ToString("N1")) : $"{this.descripcion} {this.km.ToString("N1")} km");

        public ClasificacionProductos clasificacion { get; set; }
        public string tiempoIda { get; set; }
        public string tiempoGranja { get; set; }
        public string tiempoRetorno { get; set; }
        public List<Caseta> listaCasetas { get; set; }
        public List<Anticipo> listaAnticipo { get; set; }
        public decimal sumaAnticipo5Ejes => listaAnticipo != null ? listaAnticipo.Sum(s => s.sencillo2Ejes) : 0;
        public decimal sumaAnticipo6Ejes => listaAnticipo != null ? listaAnticipo.Sum(s => s.sencillo3Ejes) : 0;
        public decimal sumaAnticipo9Ejes => listaAnticipo != null ? listaAnticipo.Sum(s => s.full) : 0;

        public decimal sumaCaseta3Ejes => listaCasetas != null ? listaCasetas.Sum(s => s.costo3Ejes) : 0;
        public decimal sumaCaseta5Ejes => listaCasetas != null ? listaCasetas.Sum(s => s.costo5Ejes) : 0;
        public decimal sumaCaseta6Ejes => listaCasetas != null ? listaCasetas.Sum(s => s.costo6Ejes) : 0;
        public decimal sumaCaseta9Ejes => listaCasetas != null ? listaCasetas.Sum(s => s.costo9Ejes) : 0;
        public TipoGranja categoria { get; set; }
        public TipoGranja categoria2 { get; set; }
    }
    public enum ClasificacionPagoChofer
    {
        VIAJE,
        TONELADA,
        DIA,
        PORCENTAJE
    }

}
