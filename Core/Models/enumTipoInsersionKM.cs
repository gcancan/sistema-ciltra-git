﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public enum enumTipoInsersionKM
    {
        CAMBIO_HODÓMETRO = 0,
        REGISTRO_KM_PREVENTIVO = 1,
        REGISTRO_KM_TALLER_PEGASO = 2
    }
}
