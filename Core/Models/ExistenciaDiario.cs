﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class ExistenciaDiario
    {
        public Empresa empresa { get; set; }
        public DateTime fecha { get; set; }
        public decimal inicial { get; set; }
        public decimal entradas { get; set; }
        public decimal salidas { get; set; }
        public decimal final { get; set; }
    }
    public class GrupoExistenciaDiario
    {
        public Empresa empresa { get; set; }
        public List<ExistenciaDiario> listaExistencias { get; set; }
    }
    public class GrupoDiasExistenciaDiario
    {
        public DateTime fecha { get; set; }
        public List<ExistenciaDiario> listaExistencias { get; set; }
    }
}
