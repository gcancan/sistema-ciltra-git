﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class SemaforoMtto
    {
        public string claveUnidad { get; set; }
        public DateTime fechaUltimoMtto { get; set; }
        public decimal ultimoKmMtto { get; set; }
        public Servicio servicio { get; set; }
        public DateTime? fechaKmActual { get; set; }
        public decimal? mkmActual { get; set; }
        public decimal proxKmMtto => ultimoKmMtto + meta;
        public decimal? kmRecorridos => fechaKmActual == null ? null : (mkmActual - ultimoKmMtto);
        public decimal? faltanKm => mkmActual == null ? null : (((proxKmMtto - mkmActual) <= 0) ? 0 : (proxKmMtto - mkmActual));
        public DateTime fechaProxMtto => fechaUltimoMtto.AddDays(60);
        public TimeSpan diasFaltantes => fechaProxMtto - DateTime.Now;
        public int diasFalt => diasFaltantes.Days <= 0 ? 0 : diasFaltantes.Days;
        private int? _meta { get; set; }
        public int meta
        {
            get
            {
                if (_meta == null)
                {
                    return 24000;
                }
                if (_meta.Value <= 0)
                {
                    return 24000;
                }
                return _meta.Value;
            }
            set
            {
                _meta = value;
            }
        }
        private int valor1 => meta - 3000; // NARANJA
        private int valor2 => meta - 8000; // AMARILLO
        //private int valor3 => meta - 6000; 

        public ColorSemaforo getColorSemaforo
        {
            get
            {
                if (kmRecorridos <= 0)
                {
                    return ColorSemaforo.NINGUNO;
                }
                if (kmRecorridos <= valor2) return ColorSemaforo.VERDE;                

                if ((kmRecorridos > valor2) && (kmRecorridos <= valor1)) return ColorSemaforo.AMARILLO;

                if ((kmRecorridos > valor1) && (kmRecorridos <= meta)) return ColorSemaforo.NARANJA;

                if ((kmRecorridos > meta)) return ColorSemaforo.ROJO;

                return ColorSemaforo.NINGUNO;
            }
        }
        public List<Servicio> listaServicios { get; set; }
        public Servicio proximoServicio
        {
            get
            {
                switch (servicio.idServicio)
                {
                    case 1:
                        return listaServicios.Find(s => s.idServicio == 23);
                    case 23:
                        return listaServicios.Find(s => s.idServicio == 2);
                    case 2:
                        return listaServicios.Find(s => s.idServicio == 24);
                    case 24:
                        return listaServicios.Find(s => s.idServicio == 13);
                    case 13:
                        return listaServicios.Find(s => s.idServicio == 1);
                    default:
                        return null;
                }
            }
        }


    }
    public enum ColorSemaforo
    {
        NINGUNO = 0,
        VERDE = 1,
        AMARILLO = 2,
        NARANJA = 3,
        ROJO = 4
    }
}
