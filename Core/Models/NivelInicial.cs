﻿namespace CoreFletera.Models
{
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class NivelInicial
    { 
        public int idTanque { get; set; }
        public DateTime fecha { get; set; }
        public decimal cantidad { get; set; }
        private decimal? _cantidadFinal { get; set; }
        public decimal cantidadFinal
        {
            get
            {
                if (_cantidadFinal == null)
                {
                    return cantidad;
                }
                else
                {
                   return _cantidadFinal.Value;
                }
            }
            set
            {
                _cantidadFinal = value;
            }
        }
        public decimal precio { get; set; }
        public bool usado { get; set; }
        public string factura { get; set; }
    }
}
