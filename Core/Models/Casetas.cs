﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class Caseta
    {
        public int idCaseta { get; set; }
        public string via { get; set; }
        public string nombreCaseta { get; set; }
        public string header
        {
            get
            {
                return string.IsNullOrEmpty(nombreCaseta) ? via : nombreCaseta;
            }
        }
        public Estado estado { get; set; }
        public DateTime? vigencia { get; set; }
        public decimal costo3Ejes { get; set; }
        public decimal costo5Ejes { get; set; }
        public decimal costo6Ejes { get; set; }
        public decimal costo9Ejes { get; set; }
        public bool estatus { get; set; }
        public string paginaFacturacion { get; set; }
    }
}
