﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public enum eTipoMovimiento
    {
        SALIDA = 0,
        ENTRADA = 1
    }
}
