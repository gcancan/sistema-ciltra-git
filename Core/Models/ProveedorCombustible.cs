﻿namespace Core.Models
{
    using CoreFletera.Models;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class ProveedorCombustible
    {        
        public int idProveedor { get; set; }
        public string nombre { get; set; }
        public string RFC { get; set; }
        public string direccion { get; set; }
        public string telefono { get; set; }
        public Colonia colonia { get; set; }
        public decimal precio { get; set; }
        public bool vehiculos { get; set; }
        public bool llantasNuevas { get; set; }
        public bool renovadorLlantas { get; set; }
        public bool reparadorLlantas { get; set; }
        public bool diesel { get; set; }
        public bool estatus { get; set; }
        public string strEstatus =>
            estatus ? "ACTIVO" : "BAJA";
    }
}
