﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
namespace CoreFletera.Models
{
    public class LiquidacionPegaso
    {
        public int idLiquidacion { get; set; }
        public DateTime fecha { get; set; }
        public Personal operador { get; set; }
        public string realiza { get; set; }
        public DateTime fechaInicio { get; set; }
        public DateTime fechaFin { get; set; }

        public string ragoFechas
        {
            get
            {
                return fechaInicio.ToShortDateString() + " - " + fechaFin.ToShortDateString();
            }
        }
        private decimal? _sumaModalidad { get; set; }
        public decimal sumaModalidad
        {
            get
            {
                if (_sumaModalidad != null)
                {
                    return (int)_sumaModalidad;
                }
                else
                {
                    return getSumaModalidad();
                }
            }
            set
            {
                _sumaModalidad = value;
            }
        }

        public SueldoFijo sueldoFijoObj { get; set; }

        private decimal? _sueldoFijo { get; set; }
        public decimal sueldoFijo
        {
            get
            {
                if (_sueldoFijo != null)
                {
                    return (int)_sueldoFijo;
                }
                if (sueldoFijoObj != null)
                {
                    return sueldoFijoObj.importe;
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                _sueldoFijo = value;
            }
        }
        public decimal? _extraSegregacion { get; set; }
        public decimal extraSegregacion
        {
            get
            {
                if (_extraSegregacion != null)
                {
                    return (decimal)_extraSegregacion;
                }
                else
                {
                    if (sueldoFijo >= sumaModalidad)
                    {
                        return sueldoFijo - sumaModalidad;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
            set
            {
                _extraSegregacion = value;
            }
        }
        private decimal? _sumaExtras { get; set; }
        public decimal sumaExtras
        {
            get
            {
                if (_sumaExtras != null)
                {
                    return (decimal)_sumaExtras;
                }
                else
                {
                    return getSumaExtras();
                }
            }
            set
            {
                _sumaExtras = value;
            }
        }
        public decimal? _importePagado { get; set; }
        public decimal importePagado
        {
            get
            {
                if (_importePagado != null)
                {
                    return (decimal)_importePagado;
                }
                else
                {
                    return calcularPago();
                }
            }
            set
            {
                _importePagado = value;
            }
        }  
        public List<DetalleLiquidacion_old> listDetalles { get; set; }
        public List<PagosExtraOperadores> listaExtras { get; set; }

        private decimal getSumaModalidad()
        {
            decimal valor = 0;
            if (listDetalles != null)
            {
                foreach (var item in listDetalles)
                {
                    valor += item.importe;
                }
            }
            return valor;
        }
        private decimal getSumaExtras()
        {
            decimal valor = 0;
            if (listaExtras != null)
            {
                foreach (var item in listaExtras)
                {
                    valor += item.importe;
                }
            }
            return valor;
        }
        private decimal calcularPago()
        {
            decimal pago = 0;
            pago = sueldoFijo >= sumaModalidad ? sueldoFijo : sumaModalidad;
            pago += sumaExtras;
            return pago; 
        }
    }
}
