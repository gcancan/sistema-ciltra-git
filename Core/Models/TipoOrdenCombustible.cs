﻿namespace CoreFletera.Models
{
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class TipoOrdenCombustible
    {        
        public int idTipoOrdenCombustible { get; set; }

        public string nombreTipoOrdenCombustible { get; set; }

        public bool requiereViaje { get; set; }

        public string tipoUnidad { get; set; }

        public bool operativo { get; set; }

        public bool activo { get; set; }
    }
}
