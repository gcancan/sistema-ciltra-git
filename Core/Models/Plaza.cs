﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class Plaza
    {
        public int idPlaza { get; set; }
        public string descripcion { get; set; }
        public string departamento { get; set; }
        public string puesto { get; set; }
        public string cveINTELISIS { get; set; }
        public string cveCliente { get; set; }
        public string nombrePersonal { get; set; }
        public bool activo { get; set; }
        public string estatus => activo ? "ACTIVO" : "BAJA";
    }
}
