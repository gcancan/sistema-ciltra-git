﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class SalidaEntrada
    {
        public int idOperacion { get; set; }
        public Personal chofer { get; set; }       
        public MasterOrdServ masterOrdServ { get; set; }
        public eTipoMovimiento tipoMoviemiento { get; set; }
        public DateTime ? fecha { get; set; }
        public DateTime ? fechaFin { get; set; }
        public List<MotivoEntrada> listMotivo { get; set; }
        public decimal km { get; set; }        
        public UnidadTransporte unidadTransporte { get; set; }
        public Usuario usuario { get; set; }
    }
    
}
