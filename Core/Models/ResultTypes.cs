﻿namespace Core.Models
{
    public enum ResultTypes
    {
        success = 0,
        error = 1,
        warning = 2,
        recordNotFound = 3
    }
}