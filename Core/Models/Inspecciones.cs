﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
namespace CoreFletera.Models
{
    public class Inspecciones
    {
        public int idInspeccion { get; set; }
        public string claveDiagrama { get; set; }
        public TiposDeOrdenServicio tipoOrdenServicio { get; set; }
        public string nombreInspeccion { get; set; }
        public List<GrupoTipoUnidades> listaTipoUnidades { get; set; }
        public List<ProductoInsumo> listaInsumos { get; set; }
        public decimal dias { get; set; }
        public decimal diasPasados { get; set; }
        public bool estatus { get; set; }
        public bool checado { get; set; }
        public GrupoTipoUnidades grupo {get;set;}
        public bool opcional { get; set; }
    }
}
