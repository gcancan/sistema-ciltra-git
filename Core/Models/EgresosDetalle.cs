﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class EgresosDetalle
    {
        public DateTime fecha { get; set; }
        public int ordenSericio { get; set; }
        public int folio { get; set; }
        public string cadenaFolio { get; set; }
        public string persona { get; set; }
        public decimal cantidad { get; set; }
        public string um { get; set; }
        public decimal total { get; set; }
        public string concepto { get; set; }
    }
}
