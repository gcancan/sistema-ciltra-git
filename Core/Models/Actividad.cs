﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class Actividad
    {
        public int idActividad { get; set; }
        public string nombre { get; set; }
        public decimal costo { get; set; }
        public decimal duracion { get; set; }
        public List<ProductosActividad> listProductos = new List<ProductosActividad>();
        public List<ProductosCOM> listProductosCOM = new List<ProductosCOM>();
        public string estatus { get; set; }
        public Personal personal1 { get; set; }
        public Personal personal2 { get; set; }
        public Personal personal3 { get; set; }
        public int idDivicion { get; set; }
        public Division division { get; set; }
        public int CIDPRODUCTO { get; set; }
        public string CCODIGOPRODUCTO { get; set; }
        public bool lavadero { get; set; }
        public bool llantera { get; set; }
        public bool taller { get; set; }
        public bool activo { get; set; }
        public string estatusStr =>
            activo ? "ACTIVO" : "BAJA";
        public string estatusCompleto
        {
            get
            {
                if (string.IsNullOrEmpty(estatus))
                {
                    return string.Empty;
                }
                else if (estatus.ToUpper() == "PRE")
                {
                    return "SIN ASIGNAR";
                }
                else if (estatus.ToUpper() == "ASIG")
                {
                    return "EN PROCESO";
                }
                else if (estatus.ToUpper() == "DIAG")
                {
                    return "EN PROCESO";
                }
                else if (estatus.ToUpper() == "TER")
                {
                    return "TERMINADO";
                }
                else if (estatus.ToUpper() == "ENT")
                {
                    return "ENTREGADO";
                }
                else if (estatus.ToUpper() == "DIAG")
                {
                    return "CANCELADO";
                }
                else
                {
                    return string.Empty;
                }
            }
        }
    }
    public class ProductosActividad
    {
        public int idActicidad { get; set; }
        public int idProducto { get; set; }
        public string codigo { get; set; }
        public string nombre { get; set; }
        public decimal cantidad { get; set; }
        public decimal costo { get; set; }
        public string um { get; set; }
        public decimal cantMin { get; set; }
        public decimal cantMax { get; set; }
        public decimal existencia { get; set; }
        public decimal extra { get; set; }
        public decimal importe { get { return cantidad * costo; } }
        public decimal cantCompra
        {
            get
            {
                if (existencia > cantidad)
                {
                    return 0;
                }
                else
                {
                    return Math.Abs(existencia - cantidad);
                }
            }
        }
        public decimal cantSal
        {
            get
            {
                if (existencia > cantidad)
                {
                    return cantidad;
                }
                else
                {
                    return 0;
                }
            }
        }
        public bool lavadero { get; set; }
        public bool taller { get; set; }
        public bool llantera { get; set; }
    }
}
