﻿namespace CoreFletera.Interfaces
{
    using Core.Models;
    using CoreFletera.BusinessLogic;
    using CoreFletera.Models;
    using System;

    public class RevisionLlantaSvc : IRevisionLlanta
    {
        public OperationResult saveRevisionLlanta(ref RevisionLlantas revisionLlantas) =>
            new RevisionLlantasServices().saveRevisionLlanta(ref revisionLlantas);
    }
}
