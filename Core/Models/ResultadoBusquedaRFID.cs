﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public enum ResultadoBusquedaRFID
    {
        UNIDAD_TRANSPORTE = 0,
        OPERADOR = 1,
        NINGUNO = 3
    }
}
