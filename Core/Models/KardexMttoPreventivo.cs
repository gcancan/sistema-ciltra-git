﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class KardexMttoPreventivo
    {
        public int idKardex { get; set; }
        public string idUnidad { get; set; }
        public DateTime fecha { get; set; }
        public decimal km { get; set; }
        public string servicio { get; set; }
        public enumTipoInsersionKM enumTipoInsersionKM { get; set; }
    }
}
