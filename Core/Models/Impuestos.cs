﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    [Serializable]
    public class Impuestos
    {
        public int clave_empresa { get; set; }
        public int clave { get; set; }
        public string descripcion { get; set; }
        public decimal valor { get; set; }
        public bool activo { get; set; }
    }
}
