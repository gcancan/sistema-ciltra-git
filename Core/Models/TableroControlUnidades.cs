﻿using Core.Interfaces;
using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace CoreFletera.Models
{
    public class TableroControlUnidades
    {
        //public TableroControlUnidades()
        //{
        //    idTableroControlUnidades = 0;
        //    viaje = null;
        //    _tractor = null;
        //    _remolque1 = null;
        //    _remolque2 = null;
        //    _operador = null;
        //    fechaEntradaPlanta = null;
        //    fechaLlegadaDestino = null;
        //    fechaSalidaDestino = null;
        //    fechaEntradaPlanta = null;
        //    listaDetalles = new List<DetalleTableroControlUnidades>();
        //}
        public int idTableroControlUnidades { get; set; }
        public int numGuiaId { get; set; }
        public int UltimoNumGuiaId { get; set; }
        public int? _numGuiaId
        {
            get
            {
                return fechaSalidaPlanta != null ? null : (int?)numGuiaId;
            }
        }
        public string _operadorStr => fechaSalidaPlanta != null ? string.Empty : operador.nombre;
        public string _strProductoActual => fechaSalidaPlanta != null ? string.Empty : productoActual;
        public string _granjas => fechaSalidaPlanta != null ? string.Empty : granjas;
        public string estatusEvento => fechaSalidaPlanta != null ? "DISPONIBLE/CARGANDO" : "LISTO PARA SALIR";
        public Viaje viaje { get; set; }
        public Viaje ultimoViaje { get; set; }
        private UnidadTransporte _tractor { get; set; }
        public UnidadTransporte tractor
        {
            get
            {
                if (_tractor != null)
                {
                    return _tractor;
                }
                else
                {
                    return viaje == null ? null : viaje.tractor;
                }
            }
            set
            {
                _tractor = value;
            }
        }
        private UnidadTransporte _remolque1 { get; set; }
        public UnidadTransporte remolque1
        {
            get
            {
                if (_remolque1 != null)
                {
                    return _remolque1;
                }
                else
                {
                    //if (ubicacion == UbicacionTablero.PLANTA && fechaSalidaPlanta == null)
                    //{
                    //    if (ultimoViaje == null)
                    //    {
                    //        OperationResult result2 = new CartaPorteSvc().getCartaPorteById(UltimoNumGuiaId);
                    //        if (result2.typeResult == ResultTypes.success)
                    //        {
                    //            ultimoViaje = (result2.result as List<Viaje>)[0];
                    //        }
                    //    }
                    //    return ultimoViaje.remolque;
                    //}
                    return viaje == null ? null : viaje.remolque;
                }
            }
            set
            {
                _remolque1 = value;
            }
        }
        private UnidadTransporte _remolque2 { get; set; }
        public UnidadTransporte remolque2
        {
            get
            {
                if (_remolque2 != null)
                {
                    return _remolque2;
                }
                else
                {
                    //if (ubicacion == UbicacionTablero.PLANTA && fechaSalidaPlanta == null)
                    //{
                    //    if (ultimoViaje == null)
                    //    {
                    //        OperationResult result2 = new CartaPorteSvc().getCartaPorteById(UltimoNumGuiaId);
                    //        if (result2.typeResult == ResultTypes.success)
                    //        {
                    //            ultimoViaje = (result2.result as List<Viaje>)[0];
                    //        }
                    //    }
                    //    return ultimoViaje.remolque2;
                    //}
                    return viaje == null ? null : viaje.remolque2;
                }
            }
            set
            {
                _remolque2 = value;
            }
        }
        private Personal _operador { get; set; }
        public Personal operador
        {
            get
            {
                if (_operador != null)
                {
                    return _operador;
                }
                else
                {
                    //if (ubicacion == UbicacionTablero.PLANTA && fechaSalidaPlanta == null)
                    //{
                    //    if (ultimoViaje == null)
                    //    {
                    //        OperationResult result2 = new CartaPorteSvc().getCartaPorteById(UltimoNumGuiaId);
                    //        if (result2.typeResult == ResultTypes.success)
                    //        {
                    //            ultimoViaje = (result2.result as List<Viaje>)[0];
                    //        }
                    //    }
                    //    return ultimoViaje.operador;
                    //}
                    return viaje == null ? null : viaje.operador;
                }
            }
            set
            {
                _operador = value;
            }
        }
        public DateTime? fechaPrimeraEntradaPlanta { get; set; }
        public DateTime? fechaSalidaPlanta { get; set; }
        public string strFechaSalidaPlanta
        {
            get
            {
                return fechaSalidaPlanta == null ? string.Empty : fechaSalidaPlanta.Value.ToString("dd/MM HH:mm");
            }
        }
        public DateTime? fechaLlegadaDestino { get; set; }
        public string strFechaLlegadaDestino
        {
            get => fechaLlegadaDestino == null ? string.Empty : fechaLlegadaDestino.Value.ToString("dd/MM HH:mm");
        }
        public DateTime? fechaSalidaDestino { get; set; }
        public string strFechaSalidaDestino
        {
            get => fechaSalidaDestino == null ? string.Empty : fechaSalidaDestino.Value.ToString("dd/MM HH:mm");
        }
        public DateTime? fechaEntradaPlanta { get; set; }
        public DateTime? fechaUltimaEntradaPlanta { get; set; }

        public string granjas
        {
            get
            {
                if (listaDetalles.Count == 0)
                {
                    return this.taller ? "TALLER" : "TALLER EXTERNO";
                }
                string cadena = string.Empty;
                foreach (var item in listaDetalles)
                {
                    if (!cadena.Contains(item.cartaPorte.zonaSelect.descripcion))
                    {
                        cadena += item.cartaPorte.zonaSelect.descripcion + ", ";
                    }
                }
                return cadena.Trim().Trim(',');
            }
        }
        public DateTime _fechaUltimaEntradaPlanta
        {
            get
            {
                if (tractor.clave == "TC360")
                {

                }
                if (fechaSalidaPlanta == null)
                {
                    if (fechaEntradaPlanta != null)
                    {
                        return fechaEntradaPlanta.Value;
                    }
                    if (fechaUltimaEntradaPlanta == null)
                    {
                        return DateTime.Now;
                    }
                    return fechaUltimaEntradaPlanta.Value;
                }
                else
                {
                    return fechaEntradaPlanta.Value;
                }
            }
        }
        public DateTime? fechaActual { get; set; }
        public List<DetalleTableroControlUnidades> listaDetalles { get; set; }
        private DateTime _fechaGranja { get; set; }
        public string tiempoTabulado { get; set; }
        public string strtiempoTabulado
        {
            get
            {
                string cadena = string.Empty;
                string[] cadenaArray = tiempoTabulado.Split(':');
                return string.Format("{0}:{1}", cadenaArray[0], cadenaArray[1]);
            }
        }
        public UbicacionTablero ubicacion
        {
            get
            {
                if (tractor == null)
                {
                    
                }
                if (tractor.clave == "TC366")
                {

                }
                var id = idTableroControlUnidades;

                if (eventosTaller)
                {
                    plantaActual = this.taller ? "TALLER" : "TALLER EXTERNO";
                    productoActual = "N/A";
                    tiempoTabulado = "00:45:00";
                    if (fechaSalidaPlanta == null)
                    {
                        return UbicacionTablero.PLANTA;
                    }
                    else if (fechaEntradaPlanta != null)
                    {
                        return UbicacionTablero.PLANTA;
                    }
                    else if (fechaSalidaPlanta != null && fechaEntradaTallerTrayectoDestino == null)
                    {
                        return UbicacionTablero.TRAYECTO_TALLER;
                    }
                    else if (fechaEntradaTallerTrayectoDestino != null && fechaSalidaTallerTrayectoDestino == null)
                    {
                        return UbicacionTablero.TALLER;
                    }
                    else if (fechaEntradaTallerTrayectoDestino != null && fechaSalidaTallerTrayectoDestino != null)
                    {
                        return UbicacionTablero.RETORNO_PLANTA;
                    }
                    else
                    {
                        return UbicacionTablero.DESCONOCIDO;
                    }
                }
                else
                {
                    DetalleTableroControlUnidades detalle = new DetalleTableroControlUnidades();
                    if (fechaSalidaPlanta == null)
                    {
                        detalle = (from det in listaDetalles orderby det.fechaLLegada descending select det).First();
                        plantaActual = detalle.cartaPorte.zonaSelect.descripcion;
                        productoActual = detalle.cartaPorte.producto.descripcion;
                        return UbicacionTablero.PLANTA;
                    }
                    else if (fechaEntradaPlanta != null)
                    {

                        detalle = (from det in listaDetalles orderby det.fechaSalida descending select det).First();
                        plantaActual = detalle.cartaPorte.zonaSelect.descripcion;
                        productoActual = detalle.cartaPorte.producto.descripcion;

                        return UbicacionTablero.PLANTA;
                    }
                    else if (fechaSalidaPlanta != null && fechaLlegadaDestino == null)
                    {
                        detalle = (from det in listaDetalles orderby det.cartaPorte.zonaSelect.km descending select det).First();
                        this.tiempoTabulado = detalle.cartaPorte.zonaSelect.tiempoIda;
                        plantaActual = detalle.cartaPorte.zonaSelect.descripcion;
                        productoActual = detalle.cartaPorte.producto.descripcion;
                        return UbicacionTablero.TRAYECTO_GRANJA;
                    }
                    else if (fechaSalidaDestino != null && fechaEntradaPlanta == null)
                    {
                        if (listaDetalles.Count == 0)
                        {
                            plantaActual = string.Empty;
                            productoActual = string.Empty;
                            tiempoTabulado = "00:45:00";
                        }
                        else
                        {
                            detalle = (from det in listaDetalles orderby det.fechaSalida descending select det).First();
                            this.tiempoTabulado = detalle.cartaPorte.zonaSelect.tiempoRetorno;
                            plantaActual = detalle.cartaPorte.zonaSelect.descripcion;
                            productoActual = detalle.cartaPorte.producto.descripcion;
                        }
                        return UbicacionTablero.RETORNO_PLANTA;
                    }
                    else if (fechaLlegadaDestino != null && fechaSalidaDestino == null)
                    {
                        if (listaDetalles.Count == 0)
                        {
                            plantaActual = string.Empty;
                            productoActual = string.Empty;
                            tiempoTabulado = "00:45:00";
                        }
                        else
                        {
                            detalle = listaDetalles.Find(s => s.fechaLLegada != null && s.fechaSalida == null);
                            if (detalle == null)
                            {
                                detalle = (from det in listaDetalles orderby det.fechaSalida descending select det).First();
                            }
                            this.tiempoTabulado = detalle.cartaPorte.zonaSelect.tiempoGranja;
                            plantaActual = detalle.cartaPorte.zonaSelect.descripcion;
                            productoActual = detalle.cartaPorte.producto.descripcion;
                            _fechaGranja = detalle.fechaLLegada.Value;
                        }

                        return UbicacionTablero.GRANJA;
                    }
                    else
                    {
                        return UbicacionTablero.DESCONOCIDO;
                    }
                }
            }
        }
        public string ubicacionEstatusTaller
        {
            get
            {
                switch (ubicacion)
                {
                    
                    case UbicacionTablero.RETORNO_PLANTA:
                        return "TRAYECTO";
                    case UbicacionTablero.TALLER:
                        return "TALLER";
                    case UbicacionTablero.TRAYECTO_TALLER:
                        return "TRAYECTO";
                    default:
                        return string.Empty;
                }
            }
        }

        public TimeSpan estadoAcumuladoSpan
        {
            get
            {
                if (tractor.clave == "TC360")
                {

                }
                if (fechaActual == null)
                {
                    return new TimeSpan(0, 0, 0, 0);
                }
                else
                {

                    switch (ubicacion)
                    {
                        case UbicacionTablero.PLANTA:
                            return ((DateTime)fechaActual).Subtract((DateTime)_fechaUltimaEntradaPlanta);

                        case UbicacionTablero.TRAYECTO_GRANJA:
                            var id = idTableroControlUnidades;
                            return ((DateTime)fechaActual).Subtract((DateTime)fechaSalidaPlanta.Value);

                        case UbicacionTablero.GRANJA:

                            return fechaActual.Value.Subtract(_fechaGranja);

                        case UbicacionTablero.RETORNO_PLANTA:
                            if (eventosTaller)
                            {
                                return ((DateTime)fechaActual).Subtract((DateTime)fechaSalidaTallerTrayectoDestino.Value);
                            }
                            else
                            {
                                return ((DateTime)fechaActual).Subtract((DateTime)fechaSalidaDestino);
                            }


                        case UbicacionTablero.TALLER:
                            return fechaActual.Value.Subtract(fechaEntradaTallerTrayectoDestino.Value);

                        case UbicacionTablero.DESCONOCIDO:
                            return new TimeSpan(0, 0, 0, 0);
                        case UbicacionTablero.TRAYECTO_TALLER:
                            return fechaActual.Value.Subtract(fechaInicioEstadoTALLER);
                        default:
                            return new TimeSpan(0, 0, 0, 0);
                    }
                }
            }
        }

        public string horas
        {
            get
            {
                if (tractor.clave == "TC359")
                {

                }
                if (estadoAcumuladoSpan.Days > 0)
                {
                    return string.Format("{0} días {1:00}:{2:00}", estadoAcumuladoSpan.Days, estadoAcumuladoSpan.Hours, estadoAcumuladoSpan.Minutes);
                }
                else
                {
                    return string.Format("{0:00}:{1:00}", estadoAcumuladoSpan.Hours, estadoAcumuladoSpan.Minutes);
                }

            }
        }

        public TimeSpan estadiaEnTallerTrayectoDestino { get; set; }
        public string tiempoTallerTrayectoDestino
        {
            get
            {
                if (fechaSalidaTallerTrayectoDestino == null)
                {
                    return string.Empty;
                }
                else
                {
                    estadiaEnTallerTrayectoDestino = fechaSalidaTallerTrayectoDestino.Value.Subtract(fechaEntradaTallerTrayectoDestino.Value);
                    if (estadiaEnTallerTrayectoDestino.Days > 0)
                    {
                        return string.Format("{0} días {1:00}:{2:00}", estadiaEnTallerTrayectoDestino.Days, estadiaEnTallerTrayectoDestino.Hours, estadiaEnTallerTrayectoDestino.Minutes);
                    }
                    else
                    {
                        return string.Format("{0:00}:{1:00}", estadiaEnTallerTrayectoDestino.Hours, estadiaEnTallerTrayectoDestino.Minutes);
                    }
                }
            }
        }

        public TimeSpan estadiaEnTallerTrayectoPlanta { get; set; }
        public string tiempoTallerTrayectoPlanta
        {
            get
            {
                if (fechaSalidaTallerTrayectoPlanta == null)
                {
                    return string.Empty;
                }
                else
                {
                    estadiaEnTallerTrayectoPlanta = fechaSalidaTallerTrayectoPlanta.Value.Subtract(fechaEntradaTallerTrayectoPlanta.Value);
                    if (estadiaEnTallerTrayectoPlanta.Days > 0)
                    {
                        return string.Format("{0} días {1:00}:{2:00}", estadiaEnTallerTrayectoPlanta.Days, estadiaEnTallerTrayectoPlanta.Hours, estadiaEnTallerTrayectoPlanta.Minutes);
                    }
                    else
                    {
                        return string.Format("{0:00}:{1:00}", estadiaEnTallerTrayectoPlanta.Hours, estadiaEnTallerTrayectoPlanta.Minutes);
                    }
                }
            }
        }
        public string plantaActual { get; set; }
        public string productoActual { get; set; }
        public DateTime tiempoEstimadaLlegadaDestino
        {
            get
            {
                //var deta = (from det in listaDetalles orderby det.cartaPorte.zonaSelect.km descending select det).First().cartaPorte.zonaSelect.tiempoIda;
                //var tiempoIda = listaDetalles[0].cartaPorte.zonaSelect.tiempoIda;
                var tiempoIda = (from det in listaDetalles orderby det.cartaPorte.zonaSelect.km descending select det).First().cartaPorte.zonaSelect.tiempoIda;
                int minutos = 45;
                if (!string.IsNullOrEmpty(tiempoIda))
                {
                    string[] arr = tiempoIda.Split(':');
                    minutos = (int)new TimeSpan(Convert.ToInt32(arr[0]), Convert.ToInt32(arr[1]), Convert.ToInt32(arr[2])).TotalMinutes;
                }
                if (!string.IsNullOrEmpty(tiempoTallerTrayectoDestino))
                {
                    minutos += (int)estadiaEnTallerTrayectoDestino.TotalMinutes;
                }
                return fechaSalidaPlanta.Value.AddMinutes(minutos);
            }
        }
        public string strTiempoEstimadaLlegadaDestino
        {
            get => tiempoEstimadaLlegadaDestino.ToString("dd/MM HH:mm");
        }
        public DateTime tiempoEstimadaLlegadaPlanta //revisando // aprovado.
        {
            get
            {
                if (tractor.clave == "TC500")
                {

                }
                var detalle = (from det in listaDetalles orderby det.fechaSalida descending select det).First();
                var tiempoRetorno = detalle.cartaPorte.zonaSelect.tiempoRetorno;
                int minutos = 45;
                if (!string.IsNullOrEmpty(tiempoRetorno))
                {
                    string[] arr = tiempoRetorno.Split(':');
                    minutos = (int)new TimeSpan(Convert.ToInt32(arr[0]), Convert.ToInt32(arr[1]), Convert.ToInt32(arr[2])).TotalMinutes;
                }
                if (!string.IsNullOrEmpty(tiempoTallerTrayectoPlanta))
                {
                    minutos += (int)estadiaEnTallerTrayectoPlanta.TotalMinutes;
                }
                return fechaSalidaDestino.Value.AddMinutes(minutos);
            }
        }
        public string strTiempoEstimadaLlegadaPlanta
        {
            get => tiempoEstimadaLlegadaPlanta.ToString("dd/MM HH:mm");
        }
        public string diferenciaLlegadaPlanta //en Proceso // GRCC
        {
            get
            {
                if (tractor.clave == "TC500")
                {

                }
                var diferencia = fechaActual.Value.Subtract(tiempoEstimadaLlegadaPlanta);
                if (diferencia.Minutes < 0)
                {
                    return "00:00";
                }
                if (diferencia.Days > 0)
                {
                    return string.Format("{0} días {1:00}:{2:00}", diferencia.Days, diferencia.Hours, diferencia.Minutes);
                }
                else
                {
                    return string.Format("{0:00}:{1:00}", diferencia.Hours, diferencia.Minutes);
                }
            }
        }
        public string diferenciaLlegadaGranja
        {
            get
            {
                if (listaDetalles.Count == 0)
                {
                    return "00:00:00";
                }
                var tiempoIda = listaDetalles[0].cartaPorte.zonaSelect.tiempoIda;
                int minutos = 45;
                if (!string.IsNullOrEmpty(tiempoIda))
                {
                    string[] arr = tiempoIda.Split(':');
                    minutos = (int)new TimeSpan(Convert.ToInt32(arr[0]), Convert.ToInt32(arr[1]), Convert.ToInt32(arr[2])).TotalMinutes;
                }
                if (!string.IsNullOrEmpty(tiempoTallerTrayectoDestino))
                {
                    minutos += (int)estadiaEnTallerTrayectoDestino.TotalMinutes;
                }
                var diferencia = estadoAcumuladoSpan.Subtract(new TimeSpan(0, minutos, 0));
                if (diferencia.Minutes < 0)
                {
                    return "00:00";
                }
                if (diferencia.Days > 0)
                {
                    return string.Format("{0} días {1:00}:{2:00}", diferencia.Days, diferencia.Hours, diferencia.Minutes);
                }
                else
                {
                    return string.Format("{0:00}:{1:00}", diferencia.Hours, diferencia.Minutes);
                }
            }
        }

        public string diferenciaEngranja
        {
            get
            {
                var detalle = listaDetalles.Find(s => s.fechaLLegada != null && s.fechaSalida == null);
                if (detalle == null)
                {
                    detalle = (from det in listaDetalles orderby det.fechaSalida descending select det).First();
                }

                var tiempoEnGranja = listaDetalles[0].cartaPorte.zonaSelect.tiempoGranja;
                int minutos = 45;
                if (!string.IsNullOrEmpty(tiempoEnGranja))
                {
                    string[] arr = tiempoEnGranja.Split(':');
                    minutos = (int)new TimeSpan(Convert.ToInt32(arr[0]), Convert.ToInt32(arr[1]), Convert.ToInt32(arr[2])).TotalMinutes;
                }
                var diferencia = estadoAcumuladoSpan.Subtract(new TimeSpan(0, minutos, 0));
                if (diferencia.Minutes < 0)
                {
                    return "00:00";
                }
                if (diferencia.Days > 0)
                {
                    return string.Format("{0} días {1:00}:{2:00}", diferencia.Days, diferencia.Hours, diferencia.Minutes);
                }
                else
                {
                    return string.Format("{0:00}:{1:00}", diferencia.Hours, diferencia.Minutes);
                }
            }
        }
        public DateTime? fechaOS { get; set; }

        public DateTime? fechaEntradaTallerTrayectoDestino { get; set; }
        public DateTime? fechaSalidaTallerTrayectoDestino { get; set; }
        public DateTime? fechaEntradaTallerTrayectoPlanta { get; set; }
        public DateTime? fechaSalidaTallerTrayectoPlanta { get; set; }
        public bool enTallerTrayectoDestino
        {
            get
            {
                if (_enTallerTrayectoDestinor != null)
                {
                    return _enTallerTrayectoDestinor.Value;
                }
                if (fechaEntradaTallerTrayectoDestino != null && fechaSalidaTallerTrayectoDestino == null)
                    return true;
                else
                    return false;
            }
            set
            {
                _enTallerTrayectoDestinor = value;
            }

        }
        private bool? _enTallerTrayectoDestinor { get; set; }

        public bool enTallerTrayectoPlanta
        {
            get
            {
                if (_enTallerTrayectoPlanta != null)
                {
                    return _enTallerTrayectoPlanta.Value;
                }
                if (fechaEntradaTallerTrayectoPlanta != null && fechaSalidaTallerTrayectoPlanta == null)
                    return true;
                else
                    return false;
            }
            set
            {
                _enTallerTrayectoPlanta = value;
            }

        }
        private bool? _enTallerTrayectoPlanta { get; set; }

        public bool taller { get; set; }
        public bool tallerExterno { get; set; }
        public DateTime fechaInicioEstadoTALLER
        {
            get
            {
                if (tractor.clave == "TC511")
                {

                }
                if (ubicacion == UbicacionTablero.TRAYECTO_TALLER)
                {
                    return fechaSalidaPlanta.Value;
                }
                else if (ubicacion == UbicacionTablero.TALLER)
                {
                    return fechaEntradaTallerTrayectoDestino.Value;
                }
                else if (ubicacion == UbicacionTablero.RETORNO_PLANTA)
                {
                    return fechaSalidaTallerTrayectoDestino.Value;
                }
                else
                {
                    return DateTime.Now;
                }
            }
        }
        public bool eventosTaller
        {
            get
                => taller || tallerExterno;
        }
        public string geoCerca
        {
            get; set;
        }
        public string operacion { get; set; }
        public List<ComentarioTablero> listaComentarios { get; set; }
        public Visibility visibleComentario
        {
            get => listaComentarios.Count > 0 ? Visibility.Visible : Visibility.Hidden;

        }
        public int contadorComentarios { get => listaComentarios.Count; }
    }
    public class ComentarioTablero
    {
        public int idTablero { get; set; }
        public string usuario { get; set; }
        public string comentario { get; set; }
        public DateTime fecha { get; set; }
    }
    public class DetalleTableroControlUnidades
    {
        public int idDetalleTableroControlUnidades { get; set; }
        public int idTableroControlUnidades { get; set; }
        public int consecutivo { get; set; }
        public CartaPorte cartaPorte { get; set; }
        public int tiempo { get; set; }
        public DateTime? fechaLLegada { get; set; }
        public DateTime? fechaSalida { get; set; }

    }
    public enum UbicacionTablero
    {
        PLANTA = 0,
        TRAYECTO_GRANJA = 1,
        GRANJA = 2,
        RETORNO_PLANTA = 3,
        TALLER = 4,
        TRAYECTO_TALLER = 5,
        DESCONOCIDO = 1000
    }
}
