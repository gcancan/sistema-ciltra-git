﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
namespace CoreFletera.Models
{
    public class Lavadero
    {
        public int idLavadero { get; set; }
        public int ordenServicio { get; set; }
        public Actividad actividad { get; set; }
        public DateTime fechaEntrada { get; set; }
        public DateTime ? fechaSalida { get; set; }
        public string estatus { get; set; }
        public decimal costo { get; set; }
        public decimal costoInsumos { get; set; }
        public Personal lavador { get; set; }
        public Personal lavadorAux { get; set; }
        public Personal lavadorAux2 { get; set; }
        public List<ProductosActividad> listaInsumos { get; set; }      
        public string usuario { get; set; }
        public Personal personalEntrega { get; set; }
        public List<Inspecciones> listaInspecciones { get; set; }
    }
}
