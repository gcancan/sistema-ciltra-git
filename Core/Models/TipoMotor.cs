﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    [Serializable]
    public class TipoMotor
    {
        public int diTipoMotor { get; set; }
        public string tipoMotor { get; set; }
        public decimal porcTolerancia { get; set; }
    }
}
