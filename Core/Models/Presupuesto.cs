﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class Presupuesto
    {
        public int idPresupuesto { get; set; }
        public Empresa empresa { get; set; }
        public ZonaOperativa sucursal { get; set; }
        public Cliente cliente { get; set; } 
        public int anio { get; set; }
        public string usuario { get; set; }
        public DateTime fechaRegistro { get; set; }
        public List<DetallePresupuesto> listaDetalles { get; set; }
        public bool estatus { get; set; }
        public string usuarioBaja { get; set; }
        public DateTime? fechaBaja { get; set; }
    }
    public class DetallePresupuesto
    {
        public int idDetallePresupuesto { get; set; }
        public int idPresupuesto { get; set; }
        public Indicador indicador { get; set; }
        public Modalidades modalidad { get; set; }
        public decimal enero
        {
            get
            {
                if (mes <= 1)
                {
                    return mes == 1 ? _enero.Value : 0;
                }
                else
                {
                    return _enero.Value;
                }
            }
            set
            {
                _enero = value;
            }
        }
        public decimal febrero
        {
            get
            {
                if (mes <= 2)
                {
                    return mes == 2 ? _febrero.Value : 0;
                }
                else
                {
                    return _febrero.Value;
                }
            }
            set
            {
                _febrero = value;
            }
        }
        public decimal marzo
        {
            get
            {
                if (mes <= 3)
                {
                    return mes == 3 ? _marzo.Value : 0;
                }
                else
                {
                    return _marzo.Value;
                }
            }
            set
            {
                _marzo = value;
            }
        }
        public decimal abril
        {
            get
            {
                if (mes <= 4)
                {
                    return mes == 4 ? _abril.Value : 0;
                }
                else
                {
                    return _abril.Value;
                }
            }
            set
            {
                _abril = value;
            }
        }
        public decimal mayo
        {
            get
            {
                if (mes <= 5)
                {
                    return mes == 5 ? _mayo.Value : 0;
                }
                else
                {
                    return _mayo.Value;
                }
            }
            set
            {
                _mayo = value;
            }
        }
        public decimal junio
        {
            get
            {
                if (mes <= 6)
                {
                    return mes==6 ? _junio.Value : 0 ;
                }
                else
                {
                    return _junio.Value;
                }
            }
            set
            {
                _junio = value;
            }
        }
        public decimal julio
        {
            get
            {
                if (mes<=7)
                {
                    return mes == 7 ? _julio.Value : 0;
                }
                else
                {
                    return _julio.Value;
                }
            }
            set
            {
                _julio = value;
            }
        }
        public decimal agosto
        {
            get
            {
                if (mes <= 8)
                {
                    return mes == 8 ? _agosto.Value : 0;
                }
                else
                {
                    return _agosto.Value;
                }
            }
            set
            {
                _agosto = value;
            }
        }
        public decimal septiembre
        {
            get
            {
                if (mes <= 9)
                {
                    return mes == 9 ? _septiembre.Value : 0;
                }
                else
                {
                    return _septiembre.Value;
                }
            }
            set
            {
                _septiembre = value;
            }
        }
        public decimal octubre
        {
            get
            {
                if (mes <= 10)
                {
                    return mes == 10 ? _octubre.Value : 0;
                }
                else
                {
                    return _octubre.Value;
                }
            }
            set
            {
                _octubre = value;
            }
        }
        public decimal noviembre
        {
            get
            {
                if (mes <= 11)
                {
                    return mes == 11 ? _noviembre.Value : 0;
                }
                else
                {
                    return _noviembre.Value;
                }
            }
            set
            {
                _noviembre = value;
            }
        }
        public decimal diciembre
        {
            get
            {
                if (mes <= 12)
                {
                    return mes == 12 ? _diciembre.Value : 0;
                }
                else
                {
                    return _diciembre.Value;
                }
            }
            set
            {
                _diciembre = value;
            }
        }
        public decimal sumaAnual =>
            enero + febrero + marzo + abril + mayo + junio + julio + agosto + septiembre + octubre + noviembre + diciembre;
        #region privados
        private int mes => DateTime.Now.Month;
        private decimal? _enero { get; set; }
        private decimal? _febrero { get; set; }
        private decimal? _marzo { get; set; }
        private decimal? _abril { get; set; }
        private decimal? _mayo { get; set; }
        private decimal? _junio { get; set; }
        private decimal? _julio { get; set; }
        private decimal? _agosto { get; set; }
        private decimal? _septiembre { get; set; }
        private decimal? _octubre { get; set; }
        private decimal? _noviembre { get; set; }
        private decimal? _diciembre { get; set; }
        #endregion

    }
    public enum Indicador

    {
        TONELADAS = 0,
        VIAJES = 1,
        KILOMETROS = 2,
        LITROS = 3,
        VENTAS = 4,
        RENDIMIENTO = 5,
        PRECIO_KM = 6,
        DIAS_NO_OPERATIVOS = 999
    }

    
}
