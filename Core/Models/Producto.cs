﻿namespace Core.Models
{
    using CoreFletera.Models;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class Producto
    {   
        public int clave_empresa { get; set; }
        public int clave_fraccion { get; set; }
        public int clave { get; set; }
        public string descripcion { get; set; }
        public string embalaje { get; set; }
        public string clave_unidad_de_medida { get; set; }
        public string clave_externo { get; set; }
        public bool combustible { get; set; }
        public ClasificacionProductos clasificacion { get; set; }
    }
    public class UnidadMedida
    {
        public int idUnidadMedida { get; set; }
        public string medida { get; set; }
        public bool activo { get; set; }
    }
}
