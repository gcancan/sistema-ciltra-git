﻿namespace CoreFletera.Models
{
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class ClasificacionProductos
    {        
        public int idClasificacionProducto { get; set; }

        public string descripcion { get; set; }

        public bool diesel { get; set; }
    }
}
