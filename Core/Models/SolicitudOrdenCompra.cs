﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class SolicitudOrdenCompra
    {
        public int idSolicitudOrdenCompra { get; set; }
        public Empresa empresa { get; set; }
        public DateTime fecha { get; set; }
        public string usuario { get; set; }
        public bool isLlantas { get; set; }
        public string obsevaciones {get;set;}
        public string estatus { get; set; }
        public List<DetalleSolicitudOrdenCompra> listaDetalles { get; set; }
    }

    public class DetalleSolicitudOrdenCompra
    {
        public int idDetalleSolitudOrdenCompra { get; set; }
        public string medidaLlanta { get; set; }
        public LLantasProducto productoLlanta { get; set; }
        public ProductosCOM producto { get; set; }
        public UnidadTransporte unidadTrans { get; set; }
        public int cantidad { get; set; }
    }
}
