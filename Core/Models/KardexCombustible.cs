﻿namespace CoreFletera.Models
{
    using Core.Models;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class KardexCombustible
    {
        public int idInventarioDiesel { get; set; }
        public DateTime? fecha { get; set; }
        public TanqueCombustible tanque { get; set; }
        public Empresa empresa { get; set; }
        public UnidadTransporte unidadTransporte { get; set; }
        public TipoOperacionDiesel tipoOperacion { get; set; }
        public eTipoMovimiento tipoMovimiento { get; set; }
        public int? idOperacion { get; set; }
        public decimal cantidadInicial { get; set; }
        public decimal cantidad { get; set; }
        public decimal cantidadFinal { get; set; }
        public Personal personal { get; set; }
        public string observaciones { get; set; }
        public decimal? cantEntrada
        {
            get
            {
                if (this.tipoMovimiento == eTipoMovimiento.ENTRADA)
                {
                    return new decimal?(this.cantidad);
                }
                return null;
            }
        }
        public decimal? cantSalida
        {
            get
            {
                if (this.tipoMovimiento == eTipoMovimiento.SALIDA)
                {
                    return new decimal?(this.cantidad);
                }
                return null;
            }
        }
    }
}
