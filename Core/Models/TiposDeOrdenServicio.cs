﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class TiposDeOrdenServicio
    {
        public int idTipOrdServ { get; set; }
        public string NomTipOrdServ { get; set; }  
    }
}
