﻿using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class MasterOrdServ
    {
        public int idPadreOrdSer { get; set; }
        public Empresa empresa { get; set; }
        public ZonaOperativa zonaOperativa { get; set; }
        public Cliente cliente { get; set; }
        public UnidadTransporte tractor { get; set; }
        public UnidadTransporte tolva1 { get; set; }
        public UnidadTransporte dolly { get; set; }
        public UnidadTransporte tolva2 { get; set; }
        public Personal chofer { get; set; }
        public DateTime fechaCreacion { get; set; }
        public string userCrea { get; set; }
        public DateTime? fechaModificacion { get; set; }
        public string userModifica { get; set; }
        public DateTime? fechaCancela { get; set; }
        public string userCancela { get; set; }
        public string motivoCancela { get; set; }
        public string estatus { get; set; }
        public List<CabOrdenServicio> listaCabOrdenServicio { get; set; }
        public List<TipoOrdenServicio> listTiposOrdenServicio { get; set; }
        public CargaCombustible cargaCombustible { get; set; }
        public decimal km { get; set; }
    }
}
