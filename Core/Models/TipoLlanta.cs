﻿namespace CoreFletera.Models
{
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class TipoLlanta
    {
        public int clave { get; set; }
        public string nombre { get; set; }
    }
}
