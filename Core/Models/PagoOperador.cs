﻿namespace CoreFletera.Models
{
    using Core.Models;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class PagoOperador
    {  
        public decimal precioSencillo { get; set; }
        public decimal precioFull { get; set; }
        public ClasificacionPagoChofer tipoPago { get; set; }
    }
}
