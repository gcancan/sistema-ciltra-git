﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class NivelesTanque
    {
        public int idNivel { get; set; }
        public TanqueCombustible tanqueCombustible { get; set; }
        public DateTime fecha { get; set; }
        public int idPersonaAbre { get; set; }
        public string personaAbre { get; set; }
        public DateTime fechaInicio { get; set; }
        public DateTime? fechaFinal { get; set; }
        public int? idPersonaCierra { get; set; }
        public string personaCierra { get; set; }
        public List<DetalleNivelesTanque> listaDetalles { get; set; }
        public decimal? despachado =>
            listaDetalles.Sum(s => s.nivelFinal - s.nivelInicial);
        public bool cerrado
        {
            get
            {
                return fechaFinal != null;
            }
        }
        public decimal totalDespacho
        {
            get
            {
                if (listaDetalles == null)
                {
                    return 0;
                }
                else
                {
                    return listaDetalles.Sum(s => s.totalDespachado);
                }
            }
        }
        public decimal ltsFisicos { get; set; }
    }
    public class DetalleNivelesTanque
    {
        public int idDetalleNivel { get; set; }
        public int idNivel { get; set; }
        public BombasCombustible bombasCombustible { get; set; }
        public decimal nivelInicial { get; set; }
        public decimal? nivelFinal { get; set; }
        private decimal _totalDespachado { get; set; }
        public decimal totalDespachado
        {
            get
            {
                if (nivelFinal == null)
                {
                    return 0;
                }
                else
                {
                    return nivelFinal.Value - nivelInicial;
                }
            }
            set { _totalDespachado = value; }
        }
    }
}
