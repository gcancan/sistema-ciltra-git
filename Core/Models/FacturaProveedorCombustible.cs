﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class FacturaProveedorCombustible
    {
        public int idFacturaProveedorCombustible { get; set; }
        public Empresa empresa { get; set; }
        public ZonaOperativa zonaOperativa { get; set; }
        public ProveedorCombustible proveedor { get; set; }
        public string factura { get; set; }
        public string folioFiscal { get; set; }
        public DateTime fechaFactura { get; set; }
        public TipoCombustible tipoCombustible { get; set; }
        public decimal litros => listaDetalles.Sum(s => s.litros);
        public decimal subTotalIEPS => listaDetalles.Sum(s => s.subTotalIEPS);
        public decimal subTotal => listaDetalles.Sum(s => s.subTotal);
        public decimal iva => listaDetalles.Sum(s => s.iva);
        public decimal importe => listaDetalles.Sum(s => s.importe);
        public decimal IEPS => listaDetalles.Sum(s => s.IEMPS);
        public string usuario { get; set; }
        public DateTime fechaCaptura { get; set; }
        public List<DetalleFacturaProveedorCombustible> listaDetalles { get; set; }
    }
    public class DetalleFacturaProveedorCombustible
    {
        public int idDetallaFacturaProveedorCombustible { get; set; }
        public int idFacturaProveedorCombustible { get; set; }
        public int idCargaCombustible { get; set; }
        public string unidadTrans { get; set; }
        public string ticket { get; set; }
        public decimal litros { get; set; }
        public decimal precioNeto { get; set; }
        public decimal precioSinIEPS => (importeSinIEPS / litros);
        public decimal precioSubTotal => (importeSinIEPS / 1.16m) / litros;
        private decimal importeSinIEPS => importe - IEMPS;
        public decimal subTotalIEPS { get; set; }
        public decimal subTotal => precioSubTotal * litros;
        public decimal iva { get; set; }
        public decimal importe { get; set; }
        public decimal IEMPS { get; set; }
        private bool? _activo { get; set; }
        public bool activo
        {
            get => _activo == null ? true : _activo.Value;
            set => _activo = value;
        }
        public bool normal { get; set; }
        public string folioImpreso { get; set; }
        public int idOrdenServ { get; set; }
        public string viajes { get; set; }
        public DateTime fecha { get; set; }
        public string operador { get; set; }
        public int? idCliente { get; set; }
        public string cliente { get; set; }
    }
}
