﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class Clasificacion
    {
        public int idClasificacion { get; set; }
        public string clasificacion { get; set; }
    }
}
