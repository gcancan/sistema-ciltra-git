﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class CierreProcesoCombustible
    {
        public int idCierreProcesosCombustible { get; set; }
        public ZonaOperativa zonaOperativa { get; set; }
        public TanqueCombustible tanque { get; set; }
        public string usuario { get; set; }
        public DateTime fechaInicio { get; set; }
        public DateTime fechaFin { get; set; }
    }
}
