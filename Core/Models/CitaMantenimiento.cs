﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
namespace CoreFletera.Models
{
    public class CitaMantenimiento
    {
        public int idCitaMantenimiento { get; set; }
        public DateTime fechaCaptura { get; set; }
        public UnidadTransporte unidad { get; set; }
        public DateTime fechaCita { get; set; }
        public Servicio servicio { get; set; }
        public string usuario { get; set; }
        private bool? _activo { get; set; }
        public bool activo
        {
            get => _activo == null ? true : _activo.Value;
            set => _activo = value;
        }
    }
    public class ReporteAllCitasMtto
    {
        public string unidad { get; set; }
        public DateTime fecha { get; set; }
        public string tipo { get; set; }
        public TipoReporteAllCitasMtto tipoReporteAllCitasMtto
        {
            get
            {
                if (tipo == "P")
                {
                    return TipoReporteAllCitasMtto.PROGRAMADO;
                }
                else
                {
                    return TipoReporteAllCitasMtto.SERVICIO;
                }
            }
        }
    }
    public enum TipoReporteAllCitasMtto
    {
        PROGRAMADO = 0,
        SERVICIO = 1
    }
}
