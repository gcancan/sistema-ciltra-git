using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
namespace CoreFletera.Models
{
    public class CargaCombustibleExtra
    { 
		public int idCargaCombustibleExtra {get;set;}
		public TipoOrdenCombustible tipoOrdenCombustible {get;set;}
		public UnidadTransporte unidadTransporte {get;set;}
		public DateTime fecha {get;set;}
		public string userCrea {get;set;}
		public TipoCombustible tipoCombustible {get;set;}
		public DateTime ? fechaCombustible {get;set;}
		public decimal? ltCombustible {get;set;}
		public decimal? precioCombustible {get;set;}
		public decimal? importe {get;set;}
		public Personal despachador {get;set;}
		public int ? idCargaCombustible {get;set;}
		public Personal chofer {get; set;}
		public int ? numViaje {get;set;}
        public bool externo { get; set; }
        public string ticket { get; set; }
        public string folioImpreso { get; set; }
        public ProveedorCombustible proveedor { get; set; }
        public int idEmpleadoAutoriza { get; set; }
        public MasterOrdServ masterOrdServ { get; set; }
        public int idOrdenServ { get; set; }
        public DetOrdenServicio detOrden { get; set; }
    }
}