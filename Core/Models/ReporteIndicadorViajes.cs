﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class ReporteIndicadorViajes
    {
        public int idCliente { get; set; }
        public string cliente { get; set; }
        public string modalidad { get; set; }
        public decimal subModalidad { get; set; }
        public string desModalidad => string.Format("{0} {1}", modalidad, subModalidad.ToString("N0"));
        public decimal eneroTon { get; set; }
        public decimal febreroTon { get; set; }
        public decimal marzoTon { get; set; }
        public decimal abrilTon { get; set; }
        public decimal mayoTon { get; set; }
        public decimal junioTon { get; set; }
        public decimal julioTon { get; set; }
        public decimal agostoTon { get; set; }
        public decimal septiembreTon { get; set; }
        public decimal octubreTon { get; set; }
        public decimal noviembreTon { get; set; }
        public decimal diciembreTon { get; set; }
        public int sumaTotalToneladas => Convert.ToInt32(eneroTon + febreroTon + marzoTon + abrilTon + mayoTon + junioTon + julioTon + agostoTon + septiembreTon + octubreTon + noviembreTon + diciembreTon);
        /////////////////////////////////////////////////
        public decimal eneroVen { get; set; }
        public decimal febreroVen { get; set; }
        public decimal marzoVen { get; set; }
        public decimal abrilVen { get; set; }
        public decimal mayoVen { get; set; }
        public decimal junioVen { get; set; }
        public decimal julioVen { get; set; }
        public decimal agostoVen { get; set; }
        public decimal septiembreVen { get; set; }
        public decimal octubreVen { get; set; }
        public decimal noviembreVen { get; set; }
        public decimal diciembreVen { get; set; }
        public int sumaTotalVentas => Convert.ToInt32(eneroVen + febreroVen + marzoVen + abrilVen + mayoVen + junioVen + julioVen + agostoVen + septiembreVen + octubreVen + noviembreVen + diciembreVen);
        /////////////////////////////////////////////////
        public int eneroViaje => Convert.ToInt32(eneroTon / subModalidad);
        public int febreroViaje => Convert.ToInt32(febreroTon / subModalidad);
        public int marzoViaje => Convert.ToInt32(marzoTon / subModalidad);
        public int abrilViaje => Convert.ToInt32(abrilTon / subModalidad);
        public int mayoViaje => Convert.ToInt32(mayoTon / subModalidad);
        public int junioViaje => Convert.ToInt32(junioTon / subModalidad);
        public int julioViaje => Convert.ToInt32(julioTon / subModalidad);
        public int agostoViaje => Convert.ToInt32(agostoTon / subModalidad);
        public int septiembreViaje => Convert.ToInt32(septiembreTon / subModalidad);
        public int octubreViaje => Convert.ToInt32(octubreTon / subModalidad);
        public int noviembreViaje => Convert.ToInt32(noviembreTon / subModalidad);
        public int diciembreViaje => Convert.ToInt32(diciembreTon / subModalidad);
        public int sumaTotalViajes => Convert.ToInt32(eneroViaje + febreroViaje + marzoViaje + abrilViaje + mayoViaje + junioViaje + julioViaje + agostoViaje + septiembreViaje + octubreViaje + noviembreViaje + diciembreViaje);
        /////////////////////////////////////////////////
        public decimal eneroPrecioKm { get; set; }
        public decimal febreroPrecioKm { get; set; }
        public decimal marzoPrecioKm { get; set; }
        public decimal abrilPrecioKm { get; set; }
        public decimal mayoPrecioKm { get; set; }
        public decimal junioPrecioKm { get; set; }
        public decimal julioPrecioKm { get; set; }
        public decimal agostoPrecioKm { get; set; }
        public decimal septiembrePrecioKm { get; set; }
        public decimal octubrePrecioKm { get; set; }
        public decimal noviembrePrecioKm { get; set; }
        public decimal diciembrePrecioKm { get; set; }
        public int sumaTotalPrecioKm => Convert.ToInt32(eneroPrecioKm + febreroPrecioKm + marzoPrecioKm + abrilPrecioKm + mayoPrecioKm + junioPrecioKm + julioPrecioKm + agostoPrecioKm + septiembrePrecioKm + octubrePrecioKm + noviembrePrecioKm + diciembrePrecioKm);
    }
    public class ReporteIndicadorCombustible
    {
        public int idCliente { get; set; }
        public string cliente { get; set; }
        public string modalidad { get; set; }
        public decimal subModalidad { get; set; }
        public string desModalidad => string.Format("{0} {1}", modalidad, subModalidad.ToString("N0"));
        /////////////////////////////////////////////////
        public decimal eneroKm { get; set; }
        public decimal febreroKm { get; set; }
        public decimal marzoKm { get; set; }
        public decimal abrilKm { get; set; }
        public decimal mayoKm { get; set; }
        public decimal junioKm { get; set; }
        public decimal julioKm { get; set; }
        public decimal agostoKm { get; set; }
        public decimal septiembreKm { get; set; }
        public decimal octubreKm { get; set; }
        public decimal noviembreKm { get; set; }
        public decimal diciembreKm { get; set; }
        public decimal sumaTotalKm => Convert.ToInt32(eneroKm + febreroKm + marzoKm + abrilKm + mayoKm + junioKm + julioKm + agostoKm + septiembreKm + octubreKm + noviembreKm + diciembreKm);
        /////////////////////////////////////////////////
        public decimal eneroLitro { get; set; }
        public decimal febreroLitro { get; set; }
        public decimal marzoLitro { get; set; }
        public decimal abrilLitro { get; set; }
        public decimal mayoLitro { get; set; }
        public decimal junioLitro { get; set; }
        public decimal julioLitro { get; set; }
        public decimal agostoLitro { get; set; }
        public decimal septiembreLitro { get; set; }
        public decimal octubreLitro { get; set; }
        public decimal noviembreLitro { get; set; }
        public decimal diciembreLitro { get; set; }
        public decimal sumaTotalLitros => Convert.ToInt32(eneroLitro + febreroLitro + marzoLitro + abrilLitro + mayoLitro + junioLitro + julioLitro + agostoLitro + septiembreLitro + octubreLitro + noviembreLitro + diciembreLitro);
        /////////////////////////////////////////////////
        public decimal eneroRendimiento => eneroLitro <= 0 ? 0 : (eneroKm / eneroLitro);
        public decimal febreroRendimiento => febreroLitro <= 0 ? 0 : (febreroKm / febreroLitro);
        public decimal marzoRendimiento => marzoLitro <= 0 ? 0 : (marzoKm / marzoLitro);
        public decimal abrilRendimiento => abrilLitro <= 0 ? 0 : (abrilKm / abrilLitro);
        public decimal mayoRendimiento => mayoLitro <= 0 ? 0 : (mayoKm / mayoLitro);
        public decimal junioRendimiento => junioLitro <= 0 ? 0 : (junioKm / junioLitro);
        public decimal julioRendimiento => julioLitro <= 0 ? 0 : (julioKm / julioLitro);
        public decimal agostoRendimiento => agostoLitro <= 0 ? 0 : (agostoKm / agostoLitro);
        public decimal septiembreRendimiento => septiembreLitro <= 0 ? 0 : (septiembreKm / septiembreLitro);
        public decimal octubreRendimiento => octubreLitro <= 0 ? 0 : (octubreKm / octubreLitro);
        public decimal noviembreRendimiento => noviembreLitro <= 0 ? 0 : (noviembreKm / noviembreLitro);
        public decimal diciembreRendimiento => diciembreLitro <= 0 ? 0 : (diciembreKm / diciembreLitro);
        public decimal sumaTotalRendimientos => sumaTotalLitros <= 0 ? 0 : sumaTotalKm / sumaTotalLitros;// Convert.ToDecimal(eneroRendimiento + febreroRendimiento + marzoRendimiento + abrilRendimiento + mayoRendimiento + junioRendimiento + julioRendimiento + agostoRendimiento + septiembreRendimiento + octubreRendimiento + noviembreRendimiento + diciembreRendimiento);


    }
}
