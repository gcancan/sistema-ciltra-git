﻿namespace CoreFletera.Models
{
    using Core.Models;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class OrigenDestinoClientes
    {       
        public int idOrigenDestinoClientes { get; set; }

        public int idOrigen { get; set; }

        public string origen { get; set; }

        public decimal kmOrigen { get; set; }

        public int idDestino { get; set; }

        public string destino { get; set; }

        public decimal kmDestino { get; set; }

        public decimal precioSencillo { get; set; }

        public decimal precioFull { get; set; }

        public decimal km { get; set; }

        public bool viaje { get; set; }

        public decimal viajeSencillo { get; set; }

        public decimal viajeFull { get; set; }

        public bool incluyeCaseta { get; set; }

        public decimal precioCasetas { get; set; }

        public decimal sumaKM =>
            ((this.kmOrigen + this.km) + this.kmDestino);

        public ClasificacionPagoChofer tipoPago { get; set; }

        public ClasificacionProductos clasificacion { get; set; }
    }
}
