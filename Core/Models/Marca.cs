﻿namespace Core.Models
{
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class Marca
    {        
        public int clave { get; set; }
        public string descripcion { get; set; }
        public bool llantas { get; set; }
        public bool uniTrans { get; set; }
        public bool activo { get; set; }
    }
}
