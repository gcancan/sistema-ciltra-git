﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class BitacoraUbicacionesCSI
    {
        public DateTime fechaInicio { get; set; }
        public DateTime fechaFin { get; set; }
        public string tituloFecha =>
            //string.Format("{0} {1}-{2}", fechaInicio.ToString("dd/MM/yyyy"), fechaInicio.ToString("HH:mm"), fechaFin.ToString("HH:mm"));
            string.Format("{0} {1}", fechaInicio.ToString("dd/MM/yyyy"), fechaInicio.ToString("HH:mm"));
        public List<ResumenDetallesRegistrosCSI> listaResumen { get; set; }
        public int cantEnPlanta =>
            (from s in listaResumen where s.tipoUbicacionBitacora == TipoUbicacionBitacora.PLANTA group s by new { s.tipoUbicacionBitacora, s.idTractor }).Count();
        public int cantEnBase =>
            (from s in listaResumen where s.tipoUbicacionBitacora == TipoUbicacionBitacora.BASE group s by new { s.tipoUbicacionBitacora, s.idTractor }).Count();
        public int cantEnGranja =>
            (from s in listaResumen where s.tipoUbicacionBitacora == TipoUbicacionBitacora.GRANJA group s by new { s.tipoUbicacionBitacora, s.idTractor }).Count();
        public int cantEnTrayecto =>
            (from s in listaResumen where s.tipoUbicacionBitacora == TipoUbicacionBitacora.TRAYECTO group s by new { s.tipoUbicacionBitacora, s.idTractor }).Count();
        public int cantEnTallerExterno =>
            (from s in listaResumen where s.tipoUbicacionBitacora == TipoUbicacionBitacora.TALLER_EXTERNO group s by new { s.tipoUbicacionBitacora, s.idTractor }).Count();
        public int sumaUbicaciones =>
            (cantEnPlanta + cantEnBase + cantEnGranja + cantEnTrayecto + cantEnTallerExterno);
    }
    public class ResumenDetallesRegistrosCSI
    {
        public string idTractor { get; set; }
        public TipoUbicacionBitacora tipoUbicacionBitacora { get; set; }
        public List<DetallesRegistosCSI> listaDetalles { get; set; }
    }
    public class DetallesRegistosCSI
    {
        public string idTractor { get; set; }
        public DateTime fecha { get; set; }
        public GeoCerca GeoCerca { get; set; }
        public enumClasificacionGeoCerca clasificacionGeoCerca =>
            GeoCerca.clasificacion;
        public TipoRegistroCSI tipoRegistroCSI { get; set; }
        public TipoUbicacionBitacora tipoUbicacionBitacora
        {
            get
            {
                switch (clasificacionGeoCerca)
                {
                    case enumClasificacionGeoCerca.PLANTA:
                        switch (tipoRegistroCSI)
                        {
                            case TipoRegistroCSI.ENTRADA:
                                return TipoUbicacionBitacora.PLANTA;
                            case TipoRegistroCSI.SALIDA:
                                return TipoUbicacionBitacora.TRAYECTO;
                            default:
                                return TipoUbicacionBitacora.DESCONOCIDO;
                        }
                    case enumClasificacionGeoCerca.GRANJA:
                        switch (tipoRegistroCSI)
                        {
                            case TipoRegistroCSI.ENTRADA:
                                return TipoUbicacionBitacora.GRANJA;
                            case TipoRegistroCSI.SALIDA:
                                return TipoUbicacionBitacora.TRAYECTO;
                            default:
                                return TipoUbicacionBitacora.DESCONOCIDO;
                        }
                    case enumClasificacionGeoCerca.TALLER_ATLANTE:

                        switch (tipoRegistroCSI)
                        {
                            case TipoRegistroCSI.ENTRADA:
                                return TipoUbicacionBitacora.BASE;
                            case TipoRegistroCSI.SALIDA:
                                return TipoUbicacionBitacora.TRAYECTO;
                            default:
                                return TipoUbicacionBitacora.DESCONOCIDO;
                        }
                    case enumClasificacionGeoCerca.TALLER_EXTERNO:

                        switch (tipoRegistroCSI)
                        {
                            case TipoRegistroCSI.ENTRADA:
                                return TipoUbicacionBitacora.TALLER_EXTERNO;
                            case TipoRegistroCSI.SALIDA:
                                return TipoUbicacionBitacora.TRAYECTO;
                            default:
                                return TipoUbicacionBitacora.DESCONOCIDO;
                        }
                    default:
                        return TipoUbicacionBitacora.DESCONOCIDO;
                }
            }
        }
    }
    public enum TipoRegistroCSI
    {
        ENTRADA = 1,
        SALIDA = 0
    }
    public enum TipoUbicacionBitacora
    {
        PLANTA = 0,
        BASE = 1,
        GRANJA = 2,
        TRAYECTO = 3,
        TALLER_EXTERNO = 4,
        DESCONOCIDO = 1000
    }
}
