﻿namespace Core.Models
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class CartaPorte
    {

        public void limpiarImporte()
        {
            this._importeReal = null;
            this._precioOperador = null;
            this._km = null;
            this._precioFijo = null;
        }
        public string grupoCartaPorte;
        public decimal tara { get; set; }
        public Cliente cliente { get; set; }
        public DateTime? horaImpresion { get; set; }

        public DateTime? horaCaptura { get; set; }

        public DateTime? horaLlegada { get; set; }

        public string observaciones { get; set; }

        public List<Producto> listProducto { get; set; }

        public Producto producto { get; set; }
        public string unidadMedida { get; set; }

        public string grupoProducto { get; set; }

        public List<Zona> listZonas { get; set; }

        public Zona zonaSelect { get; set; }

        public Origen origen { get; set; }

        public string grupoZonas { get; set; }

        public string remision { get; set; }

        public string grupoRemisiones { get; set; }

        public decimal volumenCarga { get; set; }

        public bool omitePesoBruto { get; set; }

        private decimal volCarga { get; set; }
        /// <summary>
        /// ESTE ES EL VOLUMEN DESPACHADO DE LA REMISIÓN.
        /// </summary>
        public decimal volumenDescarga
        {
            get
            {
                if (!this.omitePesoBruto && (this.pesoBruto > decimal.Zero))
                {
                    return (this.pesoBruto - this.tara);
                }
                return this.volCarga;
            }
            set
            {
                volCarga = value;
            }
        }

        private decimal? _vDescarga { get; set; }
        /// <summary>
        /// ESTE ES EL VOLUMEN DESCARGADO DE LA REMISIÓN.
        /// </summary>
        public decimal vDescarga
        {
            get
            {
                if (this._vDescarga.HasValue)
                {
                    return this._vDescarga.Value;
                }
                return this.volumenDescarga;
            }
            set
            {
                _vDescarga = value;
            }
        }
        /// <summary>
        /// VOLUMEN SIN DESCARGAR (DESPACHADO - DESCARGADO)
        /// </summary>
        public decimal volSinDescargar => volumenDescarga - vDescarga;
        public bool viaje { get; set; }

        public decimal importeReal
        {
            get
            {
                if (this._importeReal != null)
                {
                    return this._importeReal.Value;
                }
                if (this.zonaSelect == null)
                {
                    return decimal.Zero;
                }
                if (this.zonaSelect.viaje)
                {
                    if (precio == 0)
                    {
                        return precioFijo;
                    }
                    return this.precio;
                }
                if (cobroXkm)
                {
                    return (this.km * this.precio) + precioFijo;
                }
                return (this.volumenDescarga * this.precio);
            }
            set
            {
                _importeReal = value;
            }
        }

        private decimal? _importeReal { get; set; }

        public decimal grupoImportes { get; set; }

        public int numGuiaId { get; set; }

        public string serieGuia { get; set; }

        public string idUnidadProd { get; set; }

        public decimal PorcIVA { get; set; }

        public decimal ImporIVA
        {
            get
            {
                if (this.PorcIVA > decimal.Zero)
                {
                    return (this.importeReal * (this.PorcIVA / 100M));
                }
                return decimal.Zero;
            }
        }

        public decimal PorcRetencion { get; set; }

        public decimal ImpRetencion
        {
            get
            {
                if (this.PorcRetencion > decimal.Zero)
                {
                    return (this.importeReal * (this.PorcRetencion / 100M));
                }
                return decimal.Zero;
            }
        }

        public int Consecutivo { get; set; }

        public int idTarea { get; set; }

        public TipoPrecio tipoPrecio { get; set; }

        public TipoPrecio tipoPrecioChofer { get; set; }

        private decimal? _precio { get; set; }

        public decimal precio
        {
            get
            {
                if (this.zonaSelect != null)
                {
                    if (this._precio.HasValue)
                    {
                        return Convert.ToDecimal(this._precio);
                    }
                    if (cliente != null)
                    {
                        if (cliente.clave == 237)
                        {
                            precio = 0;
                        }
                    }
                    if (this.tipoPrecio == TipoPrecio.SENCILLO)
                    {
                        if (this.zonaSelect.incluyeCaseta)
                        {
                            return (this.zonaSelect.costoSencillo - this.zonaSelect.precioCasetas);
                        }
                        return this.zonaSelect.costoSencillo;
                    }
                    if (this.tipoPrecio == TipoPrecio.SENCILLO35)
                    {
                        if (this.zonaSelect.incluyeCaseta)
                        {
                            return (this.zonaSelect.costoSencillo35 - this.zonaSelect.precioCasetas);
                        }
                        return this.zonaSelect.costoSencillo35;
                    }
                    if (this.tipoPrecio == TipoPrecio.FULL)
                    {
                        if (this.zonaSelect.incluyeCaseta)
                        {
                            return (this.zonaSelect.costoFull - this.zonaSelect.precioCasetas);
                        }
                        return this.zonaSelect.costoFull;
                    }
                    return decimal.Zero;
                }
                return decimal.Zero;
            }
            set
            {
                _precio = value;
            }
        }

        private decimal? _precioFijo { get; set; }

        public decimal precioFijo
        {
            get
            {
                if (this.zonaSelect != null)
                {
                    if (this._precioFijo != null)
                    {
                        return Convert.ToDecimal(this._precioFijo);
                    }
                    if (this.tipoPrecio == TipoPrecio.SENCILLO)
                    {
                        return this.zonaSelect.precioFijoSencillo;
                    }
                    if (this.tipoPrecio == TipoPrecio.SENCILLO35)
                    {
                        return this.zonaSelect.precioFijoFull;
                    }
                    if (this.tipoPrecio == TipoPrecio.FULL)
                    {
                        return this.zonaSelect.precioFijoFull;
                    }
                    return decimal.Zero;
                }
                return decimal.Zero;                
            }
            set
            {
                _precioFijo = value;
            }
        }

        private decimal? _km { get; set; }

        public decimal km
        {
            get
            {
                if (this.zonaSelect != null)
                {
                    if (this._km != null)
                    {
                        return this._km.Value;
                    }
                    return this.zonaSelect.km;
                }
                return decimal.Zero;
            }
            set
            {
                _km = value;
            }
        }

        private decimal? _precioCaseta { get; set; }

        public decimal precioCaseta
        {
            get
            {
                if (this._precioCaseta.HasValue)
                {
                    return this._precioCaseta.Value;
                }
                return this.zonaSelect.precioCasetas;
            }
            set
            {
                _precioCaseta = value;
            }
        }

        private bool? _incluyeCaseta { get; set; }

        public bool incluyeCaseta
        {
            get
            {
                if (this._incluyeCaseta.HasValue)
                {
                    return this._incluyeCaseta.Value;
                }
                return this.zonaSelect.incluyeCaseta;
            }
            set
            {
                _incluyeCaseta = value;
            }
        }

        private decimal? _precioOperador { get; set; }

        public decimal precioOperador
        {
            get
            {
                if (this.zonaSelect == null)
                {
                    return decimal.Zero;
                }
                decimal viajeSencillo = 0M;
                if (!this._precioOperador.HasValue)
                {
                    switch (this.tipoPrecioChofer)
                    {
                        case TipoPrecio.SENCILLO:
                            viajeSencillo = this.zonaSelect.viajeSencillo;
                            goto Label_00B9;

                        case TipoPrecio.SENCILLO35:
                            viajeSencillo = this.zonaSelect.sencillo_35;
                            goto Label_00B9;

                        case TipoPrecio.FULL:
                            viajeSencillo = this.zonaSelect.full;
                            goto Label_00B9;

                        case TipoPrecio.SENCILLO24:
                            viajeSencillo = this.zonaSelect.sencillo_24;
                            goto Label_00B9;

                        case TipoPrecio.SENCILLO30:
                            viajeSencillo = this.zonaSelect.sencillo_30;
                            goto Label_00B9;
                    }
                    viajeSencillo = 0M;
                }
                else
                {
                    viajeSencillo = this._precioOperador.Value;
                }
                Label_00B9:
                switch (this.zonaSelect.clasificacionPagoChofer)
                {
                    case ClasificacionPagoChofer.VIAJE:
                        return viajeSencillo;

                    case ClasificacionPagoChofer.TONELADA:
                        return (viajeSencillo * this.volumenDescarga);

                    case ClasificacionPagoChofer.DIA:
                        return viajeSencillo;

                    case ClasificacionPagoChofer.PORCENTAJE:
                        return (this.importeReal * (viajeSencillo / 100M));
                }
                return decimal.Zero;
            }
            set
            {
                _precioOperador = value;
            }
        }

        public bool abilitado
        {
            get
            {
                if (this.idTarea > 0)
                {
                    return false;
                }
                return true;
            }
        }

        public int fCartaPorte { get; set; }

        public decimal pesoBruto { get; set; }

        public bool baja { get; set; }

        public int? usuarioAjuste { get; set; }

        public int remolque
        {
            get; set;
        }

        public int idCargaGasolina { get; set; }

        public bool timbrado { get; set; }

        public int ordenServicio { get; set; }

        public string ticket { get; set; }

        public bool viajeFalso { get; set; }

        public bool cobroXkm { get; set; }
        public bool isReEnvio { get; set; }
    }
}
