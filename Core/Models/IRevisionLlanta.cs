﻿namespace CoreFletera.Interfaces
{
    using Core.Models;
    using CoreFletera.Models;
    internal interface IRevisionLlanta
    {
        OperationResult saveRevisionLlanta(ref RevisionLlantas revisionLlantas);
    }
}
