﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class ProductosCOM
    {
        public int idProducto { get; set; }
        public string codigo { get; set; }
        public string nombre { get; set; }
        public decimal precio { get; set; }
        public bool taller { get; set; }
        public bool lavadero { get; set; }
        public bool llantera { get; set; }
        public bool estatus { get; set; }
        public decimal cantidad { get; set; }
        public decimal importe { get { return (cantidad * precio); } }
        public decimal existencia { get; set; }
        public decimal pendientes { get; set; }
        public decimal disponibles { get { return existencia - pendientes; } }
        public string nombreExistencia
        {
            get
            {
                return existencia <= 0 ? nombre : string.Format("{0} ({1})", nombre, existencia.ToString());
            }
        }
        public decimal cantPreSalida
        {
            get
            {
                if (existencia >= cantidad)
                {
                    return cantidad;
                }
                else
                {
                    return existencia;
                }
            }
        }
        public decimal cantPreOC
        {
            get
            {
                if (existencia >= cantidad)
                {
                    return 0;
                }
                else
                {
                    return cantidad - existencia;
                }
            }
        }

    }

    public enum AreaTrabajo
    {
        TALLER = 0,
        LAVADERO = 1,
        LLANTAS = 2,
        TOTAS = 10
    }
}
