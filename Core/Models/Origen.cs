﻿using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    [Serializable]
    public class Origen 
    {     
        public int idOrigen { get; set; }
        public int idCliente { get; set; }
        public string nombreOrigen { get; set; }
        public string estado { get; set; }
        public string clasificacion { get; set; }
        public List<Zona> listDestinos { get; set; }
        public Base baseOrigen { get; set; }
        public decimal km { get; set; }
        public bool activo { get; set; }

        public string baseNombre
        {
            get
            {
                return baseOrigen != null ? string.Format("{0}-{1}", baseOrigen.nombreBase, nombreOrigen) : nombreOrigen;
            }
        }
        public string baseNombreConKm
        {
            get
            {
                return baseOrigen != null ? 
                    (string.Format("{0}-{1} {2} km", baseOrigen.nombreBase, nombreOrigen, km.ToString("N1"))) : 
                    (string.Format("{0} {1} km", nombreOrigen, km.ToString("N1")));
            }
        }
    }
}
