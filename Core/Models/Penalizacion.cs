﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class Penalizacion
    {
        public int idPenalizacion { get; set; }
        public int idViaje { get; set; }
        public string tractor { get; set; }
        public string remolque1 { get; set; }
        public string remolque2 { get; set; }
        public string operador { get; set; }
        public decimal capacidadViaje { get; set; }
        public decimal sumaCargas { get; set; }
        public decimal penalizacion { get; set; }
        public decimal cargaMinima { get; set; }
        public decimal diferencia { get; set; }
        public decimal precio { get; set; }
        public decimal importe { get; set; }
        public DateTime fecha { get; internal set; }

        public decimal peso
        {
            get
            {
                return capacidadViaje - sumaCargas;
            }
        }
        public decimal pesoPermitido
        {
            get
            {
                return capacidadViaje - cargaMinima;
            }
        }
        public decimal tnsFacturar
        {
            get
            {
                return peso - pesoPermitido;
            }
        }

        public List<DetallePenalizacion> listaDetalles { get; set; }
        public string listaRemisiones
        {
            get
            {
                if (listaDetalles.Count == 0 || listaDetalles == null)
                {
                    return string.Empty;
                }
                else
                {
                    var cadena = string.Empty;
                    foreach (var item in listaDetalles)
                    {
                        cadena += item.remision + ", ";
                    }
                    return cadena.Trim().Trim(',');
                }
            }
        }

        public string listaDestinos
        {
            get
            {
                if (listaDetalles.Count == 0 || listaDetalles == null)
                {
                    return string.Empty;
                }
                else
                {
                    var cadena = string.Empty;
                    foreach (var item in listaDetalles)
                    {
                        if (!cadena.Contains(item.destino))
                        {
                            cadena += item.destino + ", ";
                        }
                        
                    }
                    return cadena.Trim().Trim(',').Trim();
                }
            }
        }
        public string listaProductos
        {
            get
            {
                if (listaDetalles.Count == 0 || listaDetalles == null)
                {
                    return string.Empty;
                }
                else
                {
                    var cadena = string.Empty;
                    foreach (var item in listaDetalles)
                    {
                        if (!cadena.Contains(item.producto))
                        {
                            cadena += item.producto + ", ";
                        }

                    }
                    return cadena.Trim().Trim(',').Trim();
                }
            }
        }
    }

    public class DetallePenalizacion
    {
        public string remision { get; set; }
        public string destino { get; set; }
        public string producto { get; set; }
    }
}
