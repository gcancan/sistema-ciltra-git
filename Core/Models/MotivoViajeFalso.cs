﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class MotivoViajeFalso
    {
        public int idMotivo { get; set; }
        public string motivo { get; set; }
        public eImputable imputable { get; set; }
    }
}
