﻿namespace CoreFletera.Models
{
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class ReporteSuministrosCombustible
    {
        public int folio { get; set; }
        public DateTime fecha { get; set; }
        public string empresa { get; set; }
        public string tanque { get; set; }
        public string factura { get; set; }
        public string noPipa { get; set; }
        public string placasPipa { get; set; }
        public decimal ltsTotales { get; set; }
        public decimal precio { get; set; }
        public decimal importe { get; set; }
    }
}
