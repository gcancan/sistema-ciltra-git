﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class ReporteOrdenesServicio
    {
        public string clave { get; set; }
        public int folioPadre { get; set; }
        public int ordenServ { get; set; }
        public string tipoOrden { get; set; }
        public string tipoOrdenServicio { get; set; }
        public DateTime fechaCaptura { get; set; }
        public string usuarioCaptura { get; set; }
        public DateTime? fechaRecepcion { get; set; }
        public string usuarioRecepcion { get; set; }
        public DateTime? fechaTerminacion { get; set; }
        public string usuarioTermina { get; set; }
    }
}
