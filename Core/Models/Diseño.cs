﻿namespace CoreFletera.Models
{
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class Diseño
    {        
        public int idDiseño { get; set; }

        public string descripcion { get; set; }

        public MedidaLlanta medida { get; set; }

        public int profundidad { get; set; }

        public TipoLlanta tipoLlanta { get; set; }

        public bool activo { get; set; }

        public string nombreMostrar =>
            $"{this.descripcion} {((this.medida == null) ? string.Empty : this.medida.medida)} {this.profundidad} {((this.tipoLlanta == null) ? string.Empty : this.tipoLlanta.nombre)}";
    }
}
