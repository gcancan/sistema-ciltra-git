﻿namespace CoreFletera.Models
{
    using Core.Models;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class Ruta
    {        
        public int idRuta { get; set; }
        public Origen origen { get; set; }
        public Zona destino { get; set; }
        public bool cobroPorViaje { get; set; }
        public decimal km { get; set; }
        public TarifaRuta tarifaRuta { get; set; }
        public PagoOperador pagoOperador { get; set; }
        public CasetaRuta casetas { get; set; }
        public bool activo { get; set; }
        public List<Caseta> listCaseta { get; set; }
        public decimal kmRedondo =>
            ((this.origen.km + this.km) + this.destino.km);
        public ClasificacionProductos clasificacion { get; set; }
    }
}
