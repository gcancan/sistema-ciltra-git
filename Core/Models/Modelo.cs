﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class Modelo
    {
        public int idModelo { get; set; }
        public Marca marca { get; set; }
        public string modelo { get; set; }
        public bool activo { get; set; }
        public string activoStr => activo ? "ACTIVO" : "BAJA";
    }
}
