﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public enum enumTipoServicioFacturacion
    {
        VIAJES = 0,
        LAVADO_ESPECIALIZADO = 1,
        TRASPASOS = 2
    }
}
