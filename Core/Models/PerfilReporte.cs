﻿namespace Core.Models
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class PerfilReporte
    {
        public void cargarlista(List<string> listString)
        {
            this.listDetalles = new List<DetallesPerfilReporte>();
            foreach (string str in listString)
            {
                DetallesPerfilReporte item = new DetallesPerfilReporte
                {
                    idDetallePerfilReporte = 0,
                    idPerfilReporte = 0,
                    nombreDetalle = str
                };
                this.listDetalles.Add(item);
            }
        }
        public int idPerfilReporte { get; set; }
        public string nombrePerfil { get; set; }
        public Usuario usuraio { get; set; }
        public bool activo { get; set; }
        public enumPerfilRporte tipoReporte { get; set; }
        public List<DetallesPerfilReporte> listDetalles { get; set; }
    }
    public enum enumPerfilRporte
    {
        COBRANZA,
        VIAJES,
        VIAJES_V2,
        COMBUSTIBLE
    }
}
