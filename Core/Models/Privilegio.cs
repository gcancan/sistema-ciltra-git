﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class Privilegio
    {
        public int idPrivilegio { get; set; }
        public string clave { get; set; }
        public string nombre { get; set; }
        public string descripcion { get; set; }
    }
}
