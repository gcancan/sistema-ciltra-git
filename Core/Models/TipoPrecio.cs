﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public enum TipoPrecio
    {       
        SENCILLO = 0,
        SENCILLO35 = 1,
        FULL = 2,
        SENCILLO24=3,
        SENCILLO30 = 4
    }
    public enum TipoPrecioCasetas
    {
        DOS_EJES = 2,
        TRES_EJES = 3,
        CINCO_EJES = 5,
        SIES_EJES = 6,
        NUEVE_EJES = 9,
        CERO_EJES = 0,
        FULL = 20
    }
}
