﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class EvaluacionDesempeño
    {
        public int idEvaluacionDesempeño { get; set; }
        public DateTime fecha { get; set; }
        public Personal personalEvalua { get; set; }
        public Personal personalEvaluada { get; set; }
        public int conDelCalf { get; set; }
        public List<DetalleEvaluacionDesempeño> listaDetalles { get; set; }
        public int contDefiniciones => listaDetalles.Count;
        public decimal resultado => listaDetalles.Sum(s=> s.resultado);
        public decimal resultadoP2 => resultado/100;
        public string factoresMejora { get; set; }
        public string observaciones { get; set; }
        public string compromiso { get; set; }
        public string comentarios { get; set; }
        public string estatus { get; set; }
        public string usuarioCierra { get; set; }
        public DateTime? fechaCierra { get; set; }
    }
    public class DetalleEvaluacionDesempeño
    {
        public int idDetalleEvaluacionDesempeño { get; set; }
        public int idEvaluacionDesempeño { get; set; }
        public DescripcionEvaluacion descripcionEvaluacion { get; set; }
        public int valoracion { get; set; }
        public decimal porcentaje { get; set; }
        public int contCalf { get; set; }        
        public decimal resultado => contCalf <= 0 ? 0 : (valoracion * porcentaje) / contCalf;
    }
}
