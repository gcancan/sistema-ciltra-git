﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public enum enumProcesosEnvioCorreo
    {
        SALIDA_VIAJES = 0,
        CANCELACION_VIAJES = 1,
        ALTA_PERSONAL = 2,
        BAJA_PERSONAL = 3,
        FORMATO_ANTICIPOS = 4,
        VIAJE_EN_FALSO = 5
    }
}
