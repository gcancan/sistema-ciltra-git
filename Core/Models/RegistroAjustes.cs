﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class RegistroAjustes
    {
        public string nombreTabla { get; set; }
        public string nombreColumna { get; set; }
        public string idTabla { get; set; }
        public string valorAnterior { get; set; }
        public string valorNuevo { get; set; }
        public string usuario { get; set; }
    }
}
