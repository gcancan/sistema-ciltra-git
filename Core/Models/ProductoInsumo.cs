﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class ProductoInsumo
    {
        public ProductoInsumo()
        {
            idInsumo = 0;
            clave = string.Empty;
            nombre = string.Empty;
            tipoProducto = 0;
            precio = 0m;
            cantMaxima = 0m;
            cMax = 0m;
            taller = false;
            lavadero = false;
            llantera = false;
        }

        public ProductoInsumo(ProductosCOM pro)
        {
            idInsumo = pro.idProducto;
            clave = pro.codigo;
            nombre = pro.nombre;
            tipoProducto = 0;
            precio = pro.precio;
            cantMaxima = 0m;
            cMax = 0m;
            taller = pro.taller;
            lavadero = pro.lavadero;
            llantera = pro.llantera;
        }
        public int idInsumo { get; set; }
        public string clave { get; set; }
        public string nombre { get; set; }
        public int tipoProducto { get; set; }
        public decimal precio { get; set; }
        public decimal cantMaxima { get; set; }
        public decimal cMax { get; set; }
        public bool lavadero { get; set; }
        public bool taller { get; set; }
        public bool llantera { get; set; }
    }
}
