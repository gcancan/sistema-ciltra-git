﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class PreSalidaAlmacen
    {
        public int idPreSalida { get; set; }
        public int idOrdenTrabajo { get; set; }
        public DateTime fecha { get; set; }
        public int idEmpleadoSolicita { get; set; }
        public string estatus { get; set; }
        private decimal? _total = null;
        public decimal total
        {
            get
            {
                if (_total != null)
                {
                    return (decimal)_total;
                }
                else
                {
                    decimal t = 0;
                    if (listaProductos != null)
                    {
                        foreach (var item in listaProductos)
                        {
                            t += item.precio;
                        }
                        return t;
                    }
                    else
                    {
                        return 0;
                    }
                    
                }
            }
            set { _total = value; }
        }
        public int CIDDOCUMENTO_ENT { get; set; }
        public string CSERIEDOCUMENTO_ENT { get; set; }
        public double CFOLIO_ENT { get; set; }
        public int CIDDOCUMENTO_SAL { get; set; }
        public string CSERIEDOCUMENTO_SAL { get; set; }
        public double CFOLIO_SAL { get; set; }
        public List<ProductosCOM> listaProductos { get; set; }
    }
}
