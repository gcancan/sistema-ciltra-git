﻿namespace CoreFletera.Models
{
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class ParadaEquipo
    {
        public int idParadaEquipo { get; set; }
        public string estatus { get; set; }
        public string geocerca { get; set; }
        public string coordenadas { get; set; }
        public string coordenadasFinales { get; set; }
        public DateTime fechaInicial { get; set; }
        public DateTime fechaFinal { get; set; }
        public TimeSpan duracion =>
            ((TimeSpan)(this.fechaFinal - this.fechaInicial));
        public string aliasSerie { get; set; }
        public string alias { get; set; }
        public string CategoriaGeocerca { get; set; }
        public string Estado { get; set; }
        public int? folioViaje { get; set; }
    }
}
