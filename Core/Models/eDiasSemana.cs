﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public enum eDiasSemana
    {
        LUNES = 1,
        MARTES = 2,
        MIERCOLES = 3,
        JUEVES = 4,
        VIERNES = 5,
        SABADO = 6,
        DOMINGO = 7
    }
}
