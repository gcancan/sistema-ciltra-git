﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class OrdenCompra
    {
        public int idOrdenCompra { get; set; }
        public int idCotizacion { get; set; }
        public int CIDDOCUMENTO { get; set; }
        public string CSERIEDOCUMENTO { get; set; }
        public double CFOLIO { get; set; }
        public string folioFactura { get; set; }
        public Empresa empresa { get; set; }
        public DateTime fecha { get; set; }
        public List<DetalleOrdenCompra> listaDetalles { get; set; }
        public string estatus { get; set; }
        public string usuario { get; set; }
        public Proveedor proveedor { get; set; }
        public bool isLLanta { get; set; }
        public string strFolio { get
            {
                return CSERIEDOCUMENTO + CFOLIO;
            }
        }
    }
    public class DetalleOrdenCompra
    {
        public int idDetalleCompra { get; set; }
        public LLantasProducto llantaProducto { get; set; }
        public ProductosCOM producto { get; set; }
        public decimal cantidad { get; set; }
        public decimal pendientes { get; set; }
        public decimal costo { get; set; }
        public decimal importe { get; set; }
        public UnidadTransporte unidadTransporte { get; set; }
        public string factura { get; set; }
        public DateTime fechaFactura { get; set; }
    }
}
