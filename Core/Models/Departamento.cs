﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class Departamento
    {
        public int clave_empresa { get; set; }
        public int clave { get; set; }
        public string descripcion { get; set; }
        public bool es_almacen { get; set; }
        public bool activo { get; set; }
        public string activoStr => activo ? "ACTIVO" : "BAJA";
    }
}
