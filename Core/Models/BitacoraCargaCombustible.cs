﻿namespace CoreFletera.Models
{
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class BitacoraCargaCombustible
    {
        public int idCarga { get; set; }
        public int? idEntrada { get; set; }
        public int orden { get; set; }
        public DateTime fecha { get; set; }
        public DateTime? fechaEntrada { get; set; }
        public TimeSpan diferenciaFechas =>
            ((TimeSpan)(this.fecha - this.fechaEntrada));

        public string unidad { get; set; }
        public string proveedor { get; set; }
        public string chofer { get; set; }
        public decimal kmAnterior { get; set; }
        public decimal kmCarga { get; set; }
        public decimal kmEntrada { get; set; }
        public decimal kmCalculado { get; set; }
        public decimal kmDifCargaEntrada =>
            (this.kmCarga - this.kmEntrada);

        public decimal kmDifCargaCaluculado =>
            (this.kmCarga - this.kmCalculado);

        public decimal kmDiferencia =>
            (this.kmCarga - this.kmAnterior);
        public decimal kmRecorridos { get; set; }
        public decimal rendimiento
        {
            get
            {
                try
                {
                    return (this.kmRecorridos / this.ltsReinicio);
                }
                catch (Exception)
                {
                    return decimal.Zero;
                }
            }
        }
        public decimal rendimientoFisico
        {
            get
            {
                try
                {
                    return (this.kmDiferencia / this.sumaLitros);
                }
                catch (Exception)
                {
                    return decimal.Zero;
                }
            }
        }
        public decimal rendimientoManejo
        {
            get
            {
                try
                {
                    return (this.kmDiferencia / this.ltsManejo);
                }
                catch (Exception)
                {
                    return decimal.Zero;
                }
            }
        }
        public decimal ltsCargados { get; set; }
        public decimal ltsExtra { get; set; }
        public decimal sumaLitros =>
            (this.ltsCargados + this.ltsExtra);
        public decimal ltsReinicio { get; set; }
        public decimal ltsManejo { get; set; }
        public decimal relleno =>
            (this.sumaLitros - this.ltsReinicio);
        public string despachador { get; set; }
        public string remolque1 { get; set; }
        public string remolque2 { get; set; }
        public decimal capacidad { get; set; }
        public string ticket { get; set; }
        public string folioImpreso { get; set; }
        public decimal precioCombustible { get; set; }
        public decimal importe { get; set; }
        public string facturaProveedor { get; set; }
    }
}
