﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class OrigenPegaso
    {
        public int idOrigen { get; set; }
        public string nombre { get; set; }
        public int ? idCliente { get; set; }
        public int idEmpresa { get; set; }
    }
}
