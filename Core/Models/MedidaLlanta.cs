﻿namespace CoreFletera.Models
{
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class MedidaLlanta
    {        
        public int idMedidaLlanta { get; set; }
        public string medida { get; set; }
    }
}
