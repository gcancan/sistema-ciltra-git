﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class ComentarioViaje
    {
        public int idComentario { get; set; }
        public int idViaje { get; set; }
        public string usuario { get; set; }
        public string comentario { get; set; }
        public DateTime fecha { get; set; }
    }
}
