﻿namespace CoreFletera.Models
{
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class Despacho
    {
        public Despacho(Despacho despachoOLD)
        {
            this.fecha = despachoOLD.fecha;
            this.kmECM = despachoOLD.kmECM;
            this.ltsECM = despachoOLD.ltsECM;
            this.despachado = despachoOLD.despachado;
            this.noVale = despachoOLD.noVale;
            this.cliente = despachoOLD.cliente;
            this.unidad = despachoOLD.unidad;
            this.km = despachoOLD.km;
            this.operador = despachoOLD.operador;
            this.precio = despachoOLD.precio;
            this.factura = despachoOLD.factura;
            this.isExtra = despachoOLD.isExtra;
        }
        public Despacho()
        {
            this.fecha = DateTime.Now;
            this.kmECM = decimal.Zero;
            this.ltsECM = decimal.Zero;
            this.despachado = decimal.Zero;
            this.noVale = 0;
            this.cliente = string.Empty;
            this.unidad = string.Empty;
            this.km = decimal.Zero;
            this.operador = string.Empty;
            this.precio = decimal.Zero;
            this.factura = string.Empty;
            this.isExtra = false;
        }
        public DateTime fecha { get; set; }
        public decimal kmECM { get; set; }
        public decimal ltsECM { get; set; }
        public decimal despachado { get; set; }
        public int noVale { get; set; }
        public string cliente { get; set; }
        public string unidad { get; set; }
        public decimal relleno
        {
            get
            { return (this.despachado - this.ltsECM); }
        }
        public decimal km { get; set; }
        public string operador { get; set; }
        public decimal precio { get; set; }
        public string factura { get; set; }
        public bool isExtra { get; set; }
    }
}
