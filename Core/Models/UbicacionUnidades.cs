﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class UbicacionUnidades
    {
        public UnidadTransporte unidadTrans { get; set; }
        public eTipoMovimiento tipoMovimiento { get; set; }
        public EstatusUbicacion ubicacionActual { get; set; }
        public string descripcion { get; set; }
        public int folio { get; set; }
        public EstatusUbicacion ubicacionNew { get; set; }
    }
}
