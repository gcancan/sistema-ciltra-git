﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class TanqueCombustible
    {
        public int idTanqueCombustible { get; set; }
        public ZonaOperativa zonaOperativa { get; set; }
        public string nombre { get; set; }
        public bool activo { get; set; }
        public List<BombasCombustible> listaBombas { get; set; }
    }
    public class BombasCombustible
    {
        public int idBombaCombustible { get; set; }
        public int idTanque { get; set; }
        public int numero { get; set; }
        public bool activo { get; set; }
    }
}
