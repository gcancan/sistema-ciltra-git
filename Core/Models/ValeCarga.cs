﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
namespace CoreFletera.Models
{
    public class ValeCarga
    {
        public int idValeCarga { get; set; }
        public string remision { get; set; }
        public int numGuiaId { get; set; }
        public Producto producto { get; set; }
        public Origen origen { get; set; }
        public Zona destino { get; set; }
        public decimal precio { get; set; }
        public decimal volDescarga { get; set; }
        public decimal vDescargado { get; set; }
        public DateTime ? fechaCarga { get; set; }
        public DateTime ? fechaDescarga { get; set; }
        public int cartaPorte { get; set; }
        //public int remision2 { get; set; }
        public bool validado { get; set; }
        public Cliente cliente { get; set; }
        public decimal importe
        {
            get
            {
                if (destino == null)
                {
                    return 0;
                }
                else
                {
                    if (destino.viaje)
                    {
                        return precio;
                    }
                    else
                    {
                        return (volDescarga * precio);
                    }
                }
            }
         }
        public decimal ImpIVA
        {
            get
            {
                if (cliente.impuesto.valor > 0)
                {
                    return importe * (cliente.impuesto.valor / 100);
                }
                else
                {
                    return 0;
                }
            }
        }

        public decimal ImpRetencion
        {
            get
            {
                if (cliente.impuestoFlete.valor > 0)
                {
                    return importe * (cliente.impuestoFlete.valor / 100);
                }
                else
                {
                    return 0;
                }

            }
        }
        TipoPrecio tipoPrecioChofer { get; set; }
        public decimal precioOperador
        {
            get
            {
                if (destino != null)
                {
                    decimal valor = 0;

                    switch (tipoPrecioChofer)
                    {
                        case TipoPrecio.SENCILLO:
                            valor = destino.viajeSencillo;
                            break;
                        case TipoPrecio.SENCILLO35:
                            valor = destino.sencillo_35;
                            break;
                        case TipoPrecio.FULL:
                            valor = destino.full;
                            break;
                        case TipoPrecio.SENCILLO24:
                            valor = destino.sencillo_24;
                            break;
                        case TipoPrecio.SENCILLO30:
                            valor = destino.sencillo_30;
                            break;
                        default:
                            valor = 0m;
                            break;
                    }


                    switch (destino.clasificacionPagoChofer)
                    {
                        case ClasificacionPagoChofer.VIAJE:
                            return valor;
                        case ClasificacionPagoChofer.TONELADA:
                            return valor * volDescarga;
                        case ClasificacionPagoChofer.DIA:
                            return valor;
                        case ClasificacionPagoChofer.PORCENTAJE:
                            return (volDescarga * precio) * (valor / 100);
                        default:
                            return 0;
                    }
                }
                else
                {
                    return 0m;
                }
            }
        }
    }
}
