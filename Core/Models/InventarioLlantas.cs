﻿namespace CoreFletera.Models
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class InventarioLlantas
    {
        public int idEmpresa { get; set; }
        public string empresa { get; set; }
        public int idTipoUniTrans { get; set; }
        public string tipoUnidad { get; set; }
        public string clave { get; set; }
        public int noLlantas { get; set; }
        public int inv { get; set; }
        public int sinInv =>
            (this.noLlantas - this.inv);

        public List<DetalleInventarioLlantas> listaDetalles { get; set; }

        public estatusInventario estatus
        {
            get
            {
                if (this.sinInv == 0)
                {
                    return estatusInventario.COMPLETO;
                }
                if (this.noLlantas == this.sinInv)
                {
                    return estatusInventario.VACIO;
                }
                return estatusInventario.INCOMPLETO;
            }
        }
    }
    public enum estatusInventario
    {
        COMPLETO,
        INCOMPLETO,
        VACIO
    }

    public class DetalleInventarioLlantas
    {
        public int idLlanta { get; set; }
        public string folioLlanta { get; set; }
        public string clave { get; set; }
        public int posision { get; set; }
        public int profunidad { get; set; }
        public estatusDetalleInventario estatus
        {
            get
            {
                if (this.profunidad <= 4)
                {
                    return estatusDetalleInventario.ROJO;
                }
                if ((this.profunidad > 4) && (this.profunidad <= 9))
                {
                    return estatusDetalleInventario.AMARILLO;
                }
                return estatusDetalleInventario.VERDE;
            }
        }
    }
    public enum estatusDetalleInventario
    {
        VERDE,
        AMARILLO,
        ROJO
    }

}
