﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class TicketsCombustible : INotifyPropertyChanged
    {
        public string ticket { get; set; }
        public DateTime fecha { get; set; }
        public int vale { get; set; }
        public string unidad { get; set; }
        public string folioImpreso { get; set; }
        public int idOrdenServ { get; set; }
        public decimal litros { get; set; }
        public decimal precio { get; set; }        
        public decimal subTotal => (importe - IEMPS) / 1.16m;
        public decimal iva => subTotal * .16m;
        public decimal subTotalConIEPS => importe - iva;
        public decimal importe { get; set; }
        public decimal? valorIEPS { get; set; }
        public decimal IEMPS
        {
            get
            {
                try
                {
                    return litros * (valorIEPS == null ? .3521m : valorIEPS.Value);
                    if (subTotalConIEPS <= decimal.Zero)
                    {
                        return decimal.Zero;
                    }
                    else
                    {
                        decimal v1 = importe - subTotalConIEPS;
                        decimal v2 = v1 / 0.16m;
                        decimal v3 = v2 / litros;
                        decimal v4 = v3 * 1.16m;
                        decimal v5 = precio - v4;
                        return (v5 * litros);

                    }
                }
                catch (Exception ex)
                {
                    return decimal.Zero;
                }
            }
        }
        public bool isSeleccionado
        {
            get
            {
                if (_isSeleccionado == null)
                {
                    return false;
                }
                return _isSeleccionado.Value;
            }
            set
            {
                _isSeleccionado = value;
                OnPropertyChanged();
            }
        }
        public bool normal { get; set; }
        private bool? _isSeleccionado { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string name = "")
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(name));
        }
        public int folioViaje { get; set; }
        public string operador { get; set; }
    }
}
