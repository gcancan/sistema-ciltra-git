﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class ViajeBachoco
    {
        public int idViaje { get; set; }
        public Cliente cliente { get; set; }
        public UnidadTransporte tractor { get; set; }
        public UnidadTransporte tolva1 { get; set; }
        public UnidadTransporte dolly { get; set; }
        public UnidadTransporte tolva2 { get; set; }
        public DateTime fechaViaje { get; set; }
        public DateTime fechaReal { get; set; }
        public Personal chofer { get; set; }
        public Usuario usuarioBascula { get; set; }
        public DateTime? fechaFin { get; set; }
        public List<CartaPorteBachoco> listaCartaPortes { get; set; }
        public decimal tara { get; set; }
        public eEstatus estatus { get; set; }
        
    }
}
