﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public enum enumDominiosCorreos
    {
        
        [enumDesDominiosCorreos(@"@ciltra.com")]
        CILTRA = 0,
        [enumDesDominiosCorreos(@"@fleterapegaso.com")]
        PEGASO = 1,
        [enumDesDominiosCorreos(@"@fleteraatlante.com")]
        ATLANTE = 2,
        [enumDesDominiosCorreos(@"@fleteraglobal.com")]
        GLOBAL = 3,
        [enumDesDominiosCorreos(@"@csi.com.mx")]
        CSI = 4,
        [enumDesDominiosCorreos(@"@gmail.com")]
        GMAIL = 5,
        [enumDesDominiosCorreos(@"@hotmail.com")]
        HOTMAIL = 6,
        [enumDesDominiosCorreos(@"@")]
        OTRO = 9999
    }
    public class enumDesDominiosCorreos : Attribute
    {
        private string _dominio;
        public string dominio => this._dominio;
        public enumDesDominiosCorreos(string dominio)
        {
            _dominio = dominio;
        }
        public static object objEnum<T>(T _enum)
        {
            FieldInfo info = typeof(T).GetField(_enum.ToString(), BindingFlags.Public |
                                                                  BindingFlags.Static);
            foreach (object obj in info.GetCustomAttributes(false))
            {
                if (obj.GetType() == typeof(enumDesDominiosCorreos))
                {
                    return obj;
                }
            }
            return null;
        }
    }
}
