﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class Base
    {
        public int idBase { get; set; }
        public string nombreBase { get; set; }
        public Estado estado { get; set; }
        public bool activo { get; set; }
    }
}
