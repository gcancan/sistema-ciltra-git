﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class RegistroKilometraje
    {
        public int idRegistroKilometraje { get; set; }
        public string idUnidadTrans { get; set; }
        public decimal kmInicial { get; set; }
        public decimal kmFinal { get; set; }
        public DateTime ? fecha { get; set; }
        public int idUsuario { get; set; }
        public string nombreTabla { get; set; }
        public int idTabla { get; set; }
        public bool validado { get; set; }
    }
}
