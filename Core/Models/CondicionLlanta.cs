﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class CondicionLlanta
    {
        public int idCondicion { get; set; }
        public string descripcion { get; set; }
        public string clave { get; set; }
    }
}
