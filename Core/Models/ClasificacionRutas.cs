﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class ClasificacionRutas
    {
        public int idClasificacion { get; set; }
        public char clasificacion { get; set; }
        public Cliente cliente { get; set; }
        public decimal kmMenor { get; set; }
        public decimal kmMayor { get; set; }
        /// <summary>
        /// 1 = activo
        /// 0 = baja
        /// </summary>
        public bool estatus { get; set; }
    }
}
