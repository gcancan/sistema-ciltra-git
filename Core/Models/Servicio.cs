﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
namespace CoreFletera.Models
{
    public class Servicio
    {
        public List<Actividad> listaActividades { get; set; }

        public int idServicio { get; set; }
        public string nombre { get; set; }
        public string descipcion { get; set; }
        public TipoOrden tipoOrden { get; set; }
        public decimal km { get; set; }
        public decimal dias { get; set; }
        public decimal costo { get; set; }
        public int idDivicion { get; set; }
        public enumTipoOrdServ enumTipoOrdServ { get; set; }
        public bool activo { get; set; }
        public string estatusStr =>
            activo ? "ACTIVO" : "BAJA";
        public bool taller { get; set; }
        public bool lavadero { get; set; }
        public bool llantera { get; set; }

    }
    public class TipoOrden
    {
        public int idTipoOrden { get; set; }
        public string nombreTipoOrden { get; set; }
        public string nombre { get { return nombreTipoOrden; } }
    }
    public enum enumTipoOrden
    {
        PREVENTIVO = 1,
        CORRECTIVO = 2
    }
    public enum enumTipoOrdServ
    {
        COMBUSTIBLE = 1,
        TALLER = 2,
        LLANTAS = 3,
        LAVADERO = 4,
        RESGUARDO = 5,
        COMBUSTIBLE_EXTRA = 6
    }
    public enum enumTipoServicio
    {
        PREVENTIVO = 1,
        CORRECTIVO = 2,
        RECARGA = 3,
        RESGUARDO = 4
    }
}
