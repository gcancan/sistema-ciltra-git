﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class MotivosRescate
    {
        public int idMotivoRescate { get; set; }
        public string descripcionMotivo { get; set; }
        public int idAlmacen { get; set; }
        public bool activoMotivo { get; set; }
    }
}
