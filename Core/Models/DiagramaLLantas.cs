﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class DiagramaLLantas
    {
        public int idClasificacionLLanta { get; set; }
        public string nombre { get; set; }
        public int numEjes { get; set; }
        public int numLLantas { get; set; }
        public List<DetallesDiagrama> listaDetalles { get; set; }
    }
    public class DetallesDiagrama
    {
        public int eje { get; set; }
        public int llantas { get; set; }
    }
}
