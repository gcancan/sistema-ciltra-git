﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class ResumenCargasInternas
    {
        public string empresa { get; set; }
        public string unidadTransporte { get; set; }
        public int clave { get; set; }
        public decimal lts { get; set; }
        public bool extra { get; set; }
    }
}
