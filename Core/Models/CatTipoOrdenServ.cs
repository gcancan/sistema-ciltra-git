﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public enum CatTipoOrdenServ
    {
        [EnumDesCatalogo("Catalogo Actividades", "CatActividades", "idActividad")]
        ACTIVIDADES = 0,
        [EnumDesCatalogo("Catalogo Servicios", "CatServicios", "idServicio")]
        SERVICIOS = 1,
        [EnumDesCatalogo("Catalogo Tipos Servicios", "CatTipoServicio", "idTipoServicio")]
        TIPO_SERVICIOS = 2
    }

    public class EnumDesCatalogo : Attribute
    {
        private string _descripcion;
        private string _catalogo;
        private string _idRelacion;
        public string descripcion { get { return this._descripcion; } }
        public string catalogo { get { return this._catalogo; } }
        public string idRelacion { get { return this._idRelacion; } }
        // Constructor
        public EnumDesCatalogo(string descripcion, string catalogo, string idRelacion)
        {
            _descripcion = descripcion;
            _catalogo = catalogo;
            _idRelacion = idRelacion;
        }

        public static object objEnum<T>(T _enum)
        {
            FieldInfo info = typeof(T).GetField(_enum.ToString(), BindingFlags.Public |
                                                                  BindingFlags.Static);
            foreach (object obj in info.GetCustomAttributes(false))
            {
                if (obj.GetType() == typeof(EnumDesCatalogo))
                {
                    return obj;
                }
            }
            return null;
        }
    }
}
