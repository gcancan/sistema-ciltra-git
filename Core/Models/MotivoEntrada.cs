﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class MotivoEntrada
    {
        public int idMotivo { get; set; }
        public string motivo { get; set; }
        public string descripcion { get; set; }
    }
}
