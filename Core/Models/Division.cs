﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class Division
    {
        public int idDivision { get; set; }
        public string nombre { get; set; }
    }
}
