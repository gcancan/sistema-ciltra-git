﻿namespace CoreFletera.Models
{
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class Almacen
    {        
        public int idAlmacen { get; set; }
        public string nombreAlmacen { get; set; }
    }
}
