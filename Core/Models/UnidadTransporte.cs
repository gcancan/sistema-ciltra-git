﻿using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    [Serializable]
    public class UnidadTransporte
    {
        public string serie { get; set; }
        public int? cveINTELISIS { get; set; }
        public string clave { get; set; }
        public int mttoPrev { get; set; }
        public Empresa empresa { get; set; }
        public int ? clave_empresa
        {
            get
            {
                if (empresa != null)
                {
                    return empresa.clave;
                }
                return null;
            }
        }
        public Marca marca { get; set; }
        public Modelo modelo { get; set; }
        public int anio { get; set; }
        public int ? clave_marca_de_unidad
        {
            get
            {
                if (marca != null)
                {
                    return marca.clave;
                }
                return null;
            }
        }

        public TipoUnidad tipoUnidad { get; set; }
        public int ? clave_tipo_de_unidad
        {
            get
            {
                if (tipoUnidad != null)
                {
                    return tipoUnidad.clave;
                }
                return null;
            }
        }

        //public string modelo { get; set; }
       
        //public string tipo_de_combustible { get; set; }
        //public string numero_de_placa { get; set; }
        public string estatus { get; set; }
        public string rfid { get; set; }
        public TipoMotor tipoMotor { get; set; }
        public EstatusUbicacion ubicacion { get; set; }
        public DateTime? fechaAdquisicion { get; set; }
        public enumTipoAdquisicion tipoAdquisicion { get; set; }
        public int odometro { get; set; }
        public string placas { get; set; }
        public ProveedorCombustible proveedor { get; set; }
        public string documento { get; set; }
        public int capacidadTanque { get; set; }
        public TipoAccionUnidad tipoAccion { get; set; }
        public string strEstatus =>
            estatus == "ACT" ? "ACTIVO" : "BAJA"; 
        public List<RegistroAjustes> listaAjustes { get; set; }
        public string VIN { get; set; }
    }
    public enum TipoAccionUnidad
    {
        NUEVO = 0,
        EDITAR = 1
    }
    public enum enumTipoAdquisicion
    {
        COMPRA = 0,
        RENTA = 1,
        A_PRUEBA = 2,
        ND = 999
    }
}
