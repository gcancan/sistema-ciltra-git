﻿namespace CoreFletera.Models
{
    using Core.Models;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class GeoCerca
    {
        public int idGeoCerca { get; set; }
        public string nombre { get; set; }
        public Zona zona { get; set; }
        public bool granja { get; set; }
        public bool planta { get; set; }
        public bool taller { get; set; }
        public enumClasificacionGeoCerca clasificacion { get; set; }
        public string nameClasificacion =>
            clasificacion.ToString();
        public bool activo { get; set; }
    }
    public enum enumClasificacionGeoCerca
    {
        PLANTA = 1,
        GRANJA = 2,
        CAMINO_A_GRANJA = 3,
        ENTRADA_A_GRANJA = 4,
        TALLER_ATLANTE = 5,
        TALLER_EXTERNO = 6,
        CORRALON = 7,
        PUNTOS_DE_QUIEBRE = 8,
        ZONA_SIN_COBERTURA = 9,
        ZONA_DE_ROBO = 10,
        LAVADERO_EXTERNO = 11,
        PARADA_AUTORIZADA = 12,
        FITOSANITARIAS = 13,
        BASCULA = 14,
        CASETA = 15,
        CENTRO_DE_DESINFECCIÓN = 16,
        MANIOBRAS = 17,
        BASE = 18,
        CURVA_PELIGROSA = 19,
        GASOLINERA_EXTERNA = 20,
        POBLADO = 21,
        PUNTO_DE_DESCANSO = 22,
        PUNTO_DE_INTERES = 23,
        ZONA_DE_VIGILANCIA = 24
    }

}
