﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class Tramo
    {
        public int idTramo { get; set; }
        public Zona destino { get; set; }
        public Origen origen { get; set; }
        public decimal km { get; set; }
    }
}
