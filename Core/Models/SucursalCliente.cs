﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class SucursalCliente
    {
        public int idSucursalCliente { get; set; }
        public int cveINTELISIS { get; set; }
        public string nombre { get; set; }
        public string cveCliente { get; set; }
        public string nombreCliente { get; set; }
        public string direccion { get; set; }
        public bool activo { get; set; }
        public string estatus => activo ? "ACTIVO" : "BAJA";
        
    }
}
