﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class BitacoraEntradasSalidas
    {
        public int idBitacora { get; set; }
        public DateTime fechaEntrada { get; set; }
        public DateTime fechaSalida { get; set; }
        public UnidadTransporte tractor { get; set; }
        public UnidadTransporte tolva1 { get; set; }
        public UnidadTransporte dolly { get; set; }
        public UnidadTransporte tolva2 { get; set; }
        public Personal chofer { get; set; }
        public decimal Kilometraje { get; set; }
        public List<MotivoEntrada> listaMotivos { get; set; }
        public string usuario { get; set; }
        public eTipoMovimiento tipoMovimiento { get; set; }
    }
}
