﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class ComparacionValesCombustible
    {
        public int id { get; set; }
        public string mes { get; set; }
        public int año { get; set; }
        public string unidadTransporte { get; set; }
        public string vale { get; set; }
        public decimal carga { get; set; }
        public bool mach { get; set; }
        public string valeMach { get; set; }
    }
}
