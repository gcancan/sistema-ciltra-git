﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class CartaPorteBachoco
    {
        public int idCartaPorte { get; set; }
        public int idViaje { get; set; }
        public List<Producto> listaProductos { get; set; }
        public Producto producto { get; set; }
        public List<Zona> listaZonas { get; set; }
        public Zona destino { get; set; }
        public decimal pesoBruto { get; set; }
        public decimal tara { get; set; }

        private decimal _volumenCarga { get; set; }
        public decimal volumenCarga
        {
            get
            {
                if (_volumenCarga != 0m)
                {
                    return _volumenCarga;
                }
                else
                {
                    return pesoBruto - tara;
                }
            }
            set
            {
                _volumenCarga = value;
            }
            //get; set;
        }
        public TipoPrecio tipoPrecio { get; set; }

        private decimal _precio { get; set; }
        public decimal precio
        {
            get
            {
                if (_precio != 0)
                {
                    return _precio;
                }
                else
                {
                    if (destino == null)
                    {
                        return 0m;
                    }
                    switch (tipoPrecio)
                    {
                        case TipoPrecio.SENCILLO:
                            return destino.costoSencillo;
                        case TipoPrecio.SENCILLO35:
                            return destino.costoSencillo35;
                        case TipoPrecio.FULL:
                            return destino.costoFull; ;
                        default:
                            return 0m;
                    }
                }
            }
            set
            {
                _precio = value;
            }
            //get; set;
        }

        public decimal subTotal
        {
            get
            {
                return volumenCarga * precio;
            }
            //get; set;
        }

        public decimal iva { get; set; }
        public decimal importeIva
        {
            get
            {
                return subTotal * (iva / 100);
            }
            //get; set;
        }
        public decimal retencion { get; set; }
        public decimal importeRetencion
        {
            get
            {
                return subTotal * (retencion / 100);
            }
            //get; set;
        }
        
        public string remision { get; set; }
        public DateTime fechaLlegada { get; set; }
        public DateTime fechaCaptura { get; set; }
        public DateTime horaImpresion { get; set; }
        public string observaciones { get; set; }
        public int fFolio { get; set; }
    }
}
