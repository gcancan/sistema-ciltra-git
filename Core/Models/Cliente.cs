﻿namespace Core.Models
{
    using CoreFletera.Models;
    using System;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class Cliente : INotifyPropertyChanged
    {
        public int clave_empresa { get; set; }
        public string nombre { get; set; }
        public string alias { get; set; }
        public int clave { get; set; }
        public string domicilio { get; set; }
        public string rfc { get; set; }
        public Empresa empresa { get; set; }
        public Impuestos impuesto { get; set; }
        public Impuestos impuestoFlete { get; set; }
        public Fraccion fraccion { get; set; }
        public Colonia objColonia { get; set; } 
        public decimal penalizacion { get; set; }
        public string estado { get; set; }
        public string pais { get; set; }
        public string municipio { get; set; }
        public string colonia { get; set; }
        public string cp { get; set; }
        public string telefono { get; set; }
        public bool omitirRemision { get; set; }
        public bool cobroXkm { get; set; }
        public bool activo { get; set; }
        public string estatus =>
            activo ? "ACTIVO" : "BAJA";
        public string ubicacion =>
            objColonia == null ? 
            string.Format("{0}, {1}, {2}, {3}", pais, estado, municipio, colonia) :
            (string.Format("{0}, {1}, {2}, {3}", objColonia.ciudad.estado.pais.nombrePais, objColonia.ciudad.estado.nombre, objColonia.ciudad.nombre, objColonia.nombre));
        public string cveINTELISIS { get; set; }
        public string direccionNumenro { get; set; }
        public string direccionNumeroInt { get; set; }
        public string entreCalles { get; set; }
        public string delegacion { get; set; }
        public string fax { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        private bool? _isSeleccionado { get; set; }
        public bool isSeleccionado
        {
            get
            {
                if (_isSeleccionado == null)
                {
                    return false;
                }
                return _isSeleccionado.Value;
            }
            set
            {
                _isSeleccionado = value;
                OnPropertyChanged();
            }
        }
        protected void OnPropertyChanged([CallerMemberName] string name = "")
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(name));
        }
    }
}
