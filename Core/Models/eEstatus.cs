﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public enum eEstatus
    {
        INICIADO = 0,
        ACTIVO = 1,
        FINALIZADO = 2,
        CANCELADO = 3,
        EXTERNO = 4,
        FACTURADO = 5
    }
}
