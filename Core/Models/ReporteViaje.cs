﻿namespace CoreFletera.Models
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq;
    using System.Runtime.CompilerServices;

    public class ReporteViaje
    {
        public int folio { get; set; }
        
        public string empresa { get; set; }
        public string cliente { get; set; }
        public DateTime fechaInicio { get; set; }
        public string strFechaInicio =>
            DateTime.ParseExact(this.fechaInicio.ToString("dd/MM/yyyy"), "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString();
        public string horaInicio =>
            this.fechaInicio.ToString("HH:mm");
        public DateTime? fechaFin { get; set; }
        public string strFechaFin =>
            (!this.fechaFin.HasValue ? string.Empty : this.fechaFin.Value.ToString("dd/MM/yyyy"));

        public string horaFin =>
            (!this.fechaFin.HasValue ? string.Empty : this.fechaFin.Value.ToString("HH:mm"));
        public DateTime? fechaPrograma { get; set; }
        public string fechaProramadastr =>
            (!this.fechaPrograma.HasValue ? string.Empty : this.fechaPrograma.Value.ToString("dd/MM/yyyy"));
        public string tc { get; set; }
        public string rem1 { get; set; }
        public string rem2 { get; set; }
        public string operador { get; set; }
        public string operadorFin { get; set; }
        public string operadorAyuda { get; set; }
        public string operadorCap { get; set; }
        public string estatus { get; set; }
        public string modalidad { get; set; }
        public decimal capacidadIns { get; set; }
        public bool cobroXKm { get; set; }
        public string usuario { get; set; }
        public string usuarioFin { get; set; }
        public string usuarioAjusta { get; set; }
        public string duracion { get; set; }
        public decimal utilizado { get; set; }
        public DateTime? fechaSalidaPlantaCSI { get; set; }
        public DateTime? fechaEntradaPlantaCSI { get; set; }
        public string duracionDespachoSalida { get; set; }
        public string duracionCierreEntrada { get; set; }
        public string tiempoTab { get; set; }
        public string diferenciaTiempo { get; set; }
        public string strvFalso { get; set; }
        public List<ReporteCartaPorte> listaCp { get; set; }
        public List<DetalleReporteCartaPorte> getListaDetalles()
        {
            List<DetalleReporteCartaPorte> lista = new List<DetalleReporteCartaPorte>();

            foreach (var item in listaCp)
            {
                lista.Add(new DetalleReporteCartaPorte
                {
                    cartaPorte = item.cartaPorte.ToString(),
                    //capacidadIns = item.capacidadIns,
                    cobroXKm = item.cobroXKm,
                    destino = item.destino,
                    idDestino = item.idDestino,
                    idOrigen = item.idOrigen,
                    impIva = item.impIva,
                    impRetencion = item.impRetencion,
                    importe = item.importe,
                    km = item.km,
                    orden = item.orden,
                    origen = item.origen,
                    pagoOperador = item.pagoOperador,
                    pagoOperadorCap = item.pagoOperadorCap,
                    precioFijo = item.precioFijo,
                    precioVar = item.precioVar,
                    producto = item.producto,
                    subTotal = item.subTotal,
                    subTotalVar = item.subTotalVar,
                    toneladas = item.toneladas,
                    toneladasDespachadas = item.toneladasDespachadas,
                    vale = item.vale,
                    //vFalso = item.vFalso,
                    viaje = item.viaje
                });
            }

            return lista;
        }

        public DetalleReporteCartaPorte agruparLista()
        {
            List<DetalleReporteCartaPorte> lista = getListaDetalles();

            
                DetalleReporteCartaPorte detalle = new DetalleReporteCartaPorte
                {
                    cartaPorte = agruparCP(lista),
                    //capacidadIns = item.capacidadIns,
                    cobroXKm = (from s in lista select s.cobroXKm).First() ,
                    destino = agruparDestinos(lista),
                    //idDestino = item.idDestino,
                    //idOrigen = item.idOrigen,
                    impIva = lista.Sum(s=> s.impIva),
                    impRetencion = lista.Sum(s => s.impRetencion),
                    importe = lista.Sum(s => s.importe),
                    km = (from s in lista orderby s.km descending select s.km).First(),
                    orden = agruparOrdenes(lista),
                    origen = agruparOrigenes(lista),
                    pagoOperador = (from s in lista orderby s.km descending select s.pagoOperador).First(),
                    pagoOperadorCap = (from s in lista orderby s.km descending select s.pagoOperadorCap).First(),
                    precioFijo = (from s in lista orderby s.km descending select s.precioFijo).First(),
                    precioVar = (from s in lista orderby s.km descending select s.precioVar).First(),
                    producto = agruparProductos(lista),
                    subTotal = lista.Sum(s => s.subTotal),
                    subTotalVar = lista.Sum(s => s.subTotalVar),
                    toneladas = lista.Sum(s => s.toneladas),
                    toneladasDespachadas = lista.Sum(s => s.toneladasDespachadas),
                    vale = (from s in lista orderby s.km descending select s.vale).First(),
                    //vFalso = item.vFalso,
                    viaje = (from s in lista orderby s.km descending select s.viaje).First()
                };
            

            return detalle;
        }

        private string agruparCP(List<DetalleReporteCartaPorte> lista)
        {
            string cadena = string.Empty;
            foreach (var item in lista)
            {
                cadena += item.cartaPorte + ", ";
            }
            return cadena.Trim().Trim(',');
        }
        private string agruparDestinos(List<DetalleReporteCartaPorte> lista)
        {
            string cadena = string.Empty;
            foreach (var item in lista)
            {
                if (!cadena.Contains(item.destino))
                {
                    cadena += item.destino + ", ";
                }
            }
            return cadena.Trim().Trim(',');
        }
        private string agruparOrdenes(List<DetalleReporteCartaPorte> lista)
        {
            string cadena = string.Empty;
            foreach (var item in lista)
            {
                cadena += item.orden + ", ";
            }
            return cadena.Trim().Trim(',');
        }
        private string agruparOrigenes(List<DetalleReporteCartaPorte> lista)
        {
            string cadena = string.Empty;
            foreach (var item in lista)
            {
                if (!cadena.Contains(item.origen))
                {
                    cadena += item.origen + ", ";
                }
            }
            return cadena.Trim().Trim(',');
        }
        private string agruparProductos(List<DetalleReporteCartaPorte> lista)
        {
            string cadena = string.Empty;
            foreach (var item in lista)
            {
                if (!cadena.Contains(item.producto))
                {
                    cadena += item.producto + ", ";
                }
            }
            return cadena.Trim().Trim(',');
        }
    }
    public class DetalleReporteCartaPorte
    {
        public string cartaPorte { get; set; }
        public decimal km { get; set; }
        public int? idOrigen { get; set; }
        public string origen { get; set; }
        public int idDestino { get; set; }
        public string destino { get; set; }
        public string orden { get; set; }
        public string producto { get; set; }
        //public decimal capacidadIns { get; set; }
        public decimal toneladas { get; set; }
        public decimal toneladasDespachadas { get; set; }
        //public decimal utilizado =>
        //    (this.toneladasDespachadas / this.capacidadIns);
        public decimal precioVar { get; set; }
        public decimal subTotalVar { get; set; }
        public decimal precioFijo { get; set; }
        public decimal subTotal { get; set; }
        public bool cobroXKm { get; set; }
        public bool viaje { get; set; }
        public decimal impIva { get; set; }
        public decimal impRetencion { get; set; }
        public decimal importe { get; set; }
        //public bool vFalso { get; set; }
        public decimal pagoOperador { get; set; }
        public decimal pagoOperadorCap { get; set; }
        public int vale { get; set; }
    }
}
