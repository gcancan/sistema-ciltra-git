﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class ConfiguracionClienteLiquidacion
    {
        public int idConfig { get; set; }
        public Cliente cliente { get; set; }
        public int? diaInicio { get; set; }

        public string inicio
        {
            get
            {
                if (diaInicio != null)
                {
                    return string.Format("{0} {1}", ((eDiasSemana)(int)diaInicio).ToString(), horaInicio);
                }
                return string.Empty;
            }
        }
        public string horaInicio { get; set; }
        public int? diaFin { get; set; }
        public string fin
        {
            get
            {
                if (diaFin != null)
                {
                    return string.Format("{0} {1}", ((eDiasSemana)(int)diaFin).ToString(), horaFin);
                }
                return string.Empty;
            }
        }
        public string horaFin { get; set; }
        public int noViajes { get; set; }
    }
}
