﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public enum eTiposPrecios
    {
        SENCILLO = 0,
        SENCILLO_35 = 1,
        FULL = 2,
        FIJO_SENCILLO = 3,
        FIJO_FULL = 4
    }
}
