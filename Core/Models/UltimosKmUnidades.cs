﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class UltimosKmUnidades
    {
        public decimal km { get; set; }
        public string unidad { get; set; }
        public string empresa { get; set; }
        public string zonaOperativa { get; set; }
        public string operacion { get; set; }
        public DateTime fecha { get; set; }
        public int vale { get; set; }
    }
}
