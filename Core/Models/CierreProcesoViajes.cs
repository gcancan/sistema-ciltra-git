﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.BusinessLogic;
using Core.Models;
namespace CoreFletera.Models
{
    public class CierreProcesoViajes
    {
        public int idCierreProcesoViajes { get; set; }
        public DateTime fecha { get; set; }
        public int idEmpresa { get; set; }
        public int? idZonaOperativa { get; set; }
        public int? idCliente { get; set; }
        public string usuario { get; set; }
        public int noViaje { get; set; }
        public decimal km { get; set; }
        public decimal toneladas { get; set; }
        public int dias { get; set; }
        public List<DetalleCierreProcesoViajes> listaDetalles { get; set; }
    }
    public class DetalleCierreProcesoViajes
    {
        public int idDetalleCierreProcesoViajes { get; set; }
        public int idCierreProcesoViajes { get; set; }
        public int numGuiaId { get; set; }
        public int consecutivo { get; set; }
        public int? idOrigen { get; set; }
        public int idDestino { get; set; }
        public int idProducto { get; set; }
        public decimal descargado { get; set; }
        public decimal km { get; set; }
        public int? cveINTELISIS { get; set; }
        public RegistroCartaPortesINTELISIS registroCP { get; set; }
    }
}
