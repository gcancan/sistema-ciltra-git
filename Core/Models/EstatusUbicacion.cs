﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    [Serializable]
    public enum EstatusUbicacion
    {
        RUTA = 0,
        BASE = 1,
        TALLER = 2,
        LAVADERO = 3,
        LLANTAS = 4,
        TALLER_EX = 5,
        GASOLINERA = 6,
        DESCONOCIDO = 10
    }
}
