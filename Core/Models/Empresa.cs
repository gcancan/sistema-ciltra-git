﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    [Serializable]
    public class Empresa
    {
        public int clave { get; set; }
        public string nombre { get; set; }
        public string direccion { get; set; }
        public string telefono { get; set; }
        public string fax { get; set; }
        public string estado { get; set; }
        public string pais { get; set; }
        public string municipio { get; set; }
        public string colonia { get; set; }
        public string cp { get; set; }
        public string rfc { get; set; }
        public string nombreComercial { get; set; }
        public string cveINTELISIS { get; set; }
        public string noExterior { get; set; }
        public string noInterior { get; set; }
        public bool activo { get; set; }
        public string estatus => activo ? "ACTIVO" : "BAJA";
    }
}
