﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class CotizacionSolicitudOrdCompra
    {
        public int idCotizacionSolicitudOrdCompra { get; set; }        
        public Empresa empresa { get; set; }
        public SolicitudOrdenCompra solicitud { get; set; }
        public DateTime fecha { get; set; }
        public string usuarioCotiza { get; set; }
        public DateTime? fechaAutorizacion { get; set; }
        public string usuarioAprueba { get; set; }
        public string estatus { get; set; }
        public bool isLlantas { get; set; }
        public List<DetalleCotizacionSolicitudOrdCompra> listDetalle { get; set; }
    }

    public class DetalleCotizacionSolicitudOrdCompra
    {
        public int idDetalleCotizacionSolicitudOrdCompra { get; set; }
        public string medidaLlanta { get; set; }
        public LLantasProducto llantaProducto { get; set; }
        public ProductosCOM producto { get; set; }
        public UnidadTransporte unidadTransporte { get; set; }
        public int cantidad { get; set; }
        public Proveedor proveedor { get; set; }
        public decimal costo { get; set; }
        public decimal importe { get; set; }
        public List<DetalleCotizaciones> listDetalles { get; set; }        
    }

    public class DetalleCotizaciones
    {
        public int idDetalle { get; set; }
        public Proveedor proveedor { get; set; }
        public decimal costo { get; set; }
        public int diasCredito { get; set; }
        public int diasEntrega { get; set; }
        public decimal importe { get; set; }
        public bool elegido { get; set; }
    }
}
