﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class MotivoCancelacion
    {
        public int idMotivoCancelacion { get; set; }
        public string descripcion { get; set; }
        public List<enumProcesos> listaProcesos { get; set; }
        public bool requiereObservacion { get; set; }
        public bool activo { get; set; }
        public string estatusStr =>
            activo ? "ACTIVO" : "BAJA";
        public string usuario { get; set; }
    }
    public enum enumProcesos
    {
        VIAJES = 0,
        SOLICITUD_ORDEN_SERVICIO = 1,
        CARGA_DE_COMBUSTIBLE = 2,
        FACTURA = 3,
        ACTIVIDAD_PENDIENTE = 4,
        ORDENES_DE_TRABAJO = 5
    }
}
