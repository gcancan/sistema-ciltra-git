﻿namespace CoreFletera.Models
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class ResumenKardexCombustible
    {
        public int idEmpresa { get; set; }
        public string empresa { get; set; }
        public DateTime dia { get; set; }
        public decimal cantInicial { get; set; }
        public decimal entradas { get; set; }
        public decimal salidas { get; set; }
        public decimal cantFinal { get; set; }
    }
    public class GrupoResumenKardexCombustible
    {
        public string empresa { get; set; }
        public List<ResumenKardexCombustible> lista { get; set; }
    }

}
