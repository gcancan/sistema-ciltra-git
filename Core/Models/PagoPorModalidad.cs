﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class PagoPorModalidad
    {
        public int idPagoPorModalidad { get; set; }
        public ClasificacionRutas clasificacion { get; set; }
        public int ? noViaje { get; set; }
        public string modalidad
        {
            get
            {
                if (noViaje != null && clasificacion != null)
                {
                    return clasificacion.clasificacion + noViaje.ToString();
                }
                return string.Empty;
            }
        }

        public decimal sencillo_24 { get; set; }
        public decimal sencillo_30 { get; set; }
        public decimal sencillo_35 { get; set; }
        public decimal full { get; set; }
        public bool estatus { get; set; }
    }
}
