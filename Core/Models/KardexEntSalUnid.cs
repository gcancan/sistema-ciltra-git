﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class KardexEntSalUnid
    {
        public int idKardexEntSalUnid { get; set; }
        public string idUnidadTrans { get; set; }
        public DateTime fecha { get; set; }
        public eTipoMovimiento tipoMovimiento { get; set; }
        public string lugar { get; set; }
        public string descripcion { get; set; }
        public int folio { get; set; }
        public List<string> listaDetalles { get; set; }
    }
}
