﻿
namespace CoreFletera.Models
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Linq;
    using System.ComponentModel;
    using Core.Models;
    using System.Windows;

    public class ReporteCartaPorte 
    {
        public int id { get; set; }
        public string cveEmpresa { get; set; }
        public int cveSucursal { get; set; }
        public int cveUEN { get; set; }
        public string cveCliente { get; set; }
        public string cveSucCliente { get; set; }
        public string cvePersonal { get; set; }
        public string cveOperador { get; set; }
        public string cveAgenteOperador { get; set; }
        public int folio { get; set; }
        public int cartaPorte { get; set; }
        public int idEmpresa { get; set; }
        public string empresa { get; set; }
        public int idCliente { get; set; }
        public string cliente { get; set; }
        public int idOperador { get; set; }
        public int idOperadorFin { get; set; }
        public string encabezado =>
            $"VIAJE {this.folio}         ({this.fechaInicio.ToString("dd/MM/yy HH:mm")})       ({this.empresa})       ({this.cliente})   ";
        public string operador { get; set; }
        public string operadorFin { get; set; }
        public string operadorAyuda { get; set; }
        public string operadorCap { get; set; }
        string _estatus { get; set; }
        public string estatus
        {
            get
            {
                if (idEmpresa == 2)
                {
                    if (_estatus == "FINALIZADO" && vale == 0)
                    {
                        return "PRE-FINALIZADO";
                    }
                    return _estatus;
                }
                else
                    return _estatus;
            }
            set
            { _estatus = value; }
        }
        public string modalidad { get; set; }
        public string tc { get; set; }
        public string rem1 { get; set; }
        public string dolly { get; set; }
        public string rem2 { get; set; }
        public DateTime? fechaInicioCarga { get; set; }
        public DateTime? fechaCarga { get; set; }
        public DateTime? fechaDescarga { get; set; }
        public string tiempoCarga
        {
            get
            {
                try
                {
                    TimeSpan duracion;

                    DateTime? fInicio = fechaInicioCarga;
                    DateTime? fFin = fechaInicio;

                    if (!fInicio.HasValue)
                    {
                        return "N/D";
                    }
                    else
                    {
                        duracion = (fFin.Value - fInicio.Value);
                        return string.Format("{0:000}:{1:00}:{2:00}", duracion.TotalHours.ToString().Split('.')[0], duracion.Minutes.ToString(), duracion.Seconds.ToString());
                    }
                }
                catch (Exception ex)
                {
                    return "N/D";
                }
            }
        }
        public DateTime fechaInicio { get; set; }
        public string strFechaInicio =>
            this.fechaInicio.ToString("dd/MM/yyyy");
        public string horaInicio =>
            this.fechaInicio.ToString("HH:mm");
        public DateTime? fechaFin { get; set; }
        public string strFechaFin =>
            (!this.fechaFin.HasValue ? string.Empty : this.fechaFin.Value.ToString("dd/MM/yyyy"));

        public string horaFin =>
            (!this.fechaFin.HasValue ? string.Empty : this.fechaFin.Value.ToString("HH:mm"));
        public DateTime? fechaPrograma { get; set; }
        public string fechaProramadastr =>
            (!this.fechaPrograma.HasValue ? string.Empty : this.fechaPrograma.Value.ToString("dd/MM/yyyy"));
        public decimal _km { get; set; }
        public decimal km
        {
            get
            {
                try
                {
                    if (idEmpresa == 1)
                    {
                        return _km;
                    }
                    else if (idEmpresa == 2)
                    {
                        if (listaGrupo.Count == 1)
                        {
                            return listaGrupo[0].kmRedondo;
                        }
                        else
                        {
                            var i = listaGrupo.IndexOf(listaGrupo.Find(s => s.cartaPorte == cartaPorte));
                            var maxIndex = listaGrupo.Count - 1;
                            if (i == 0)
                            {
                                return listaGrupo[i].kmOrigen + (listaGrupo[i].kmOrigenDestino * 2);
                            }
                            else if (i == maxIndex)
                            {
                                return listaGrupo[i].kmOrigenDestino + listaGrupo[i].kmDestino;
                            }
                            else
                            {
                                return (listaGrupo[i].kmOrigenDestino * 2);
                            }
                        }
                    }
                    return _km;
                }
                catch (Exception ex)
                {
                    return _km;
                }
            }
            set
            {
                _km = value;
            }
        }
        public decimal kmTotal
        {
            get
            {
                if (idEmpresa == 1)
                {
                    return listaGrupo.Sum(s => s.kmRedondo);
                }
                else if (idEmpresa == 2)
                {
                    if (listaGrupo.Count == 1)
                    {
                        return listaGrupo[0].kmRedondo;
                    }
                    else
                    {
                        decimal kmTotal = 0;
                        var maxIndex = listaGrupo.Count - 1;
                        for (int i = 0; i < listaGrupo.Count; i++)
                        {
                            if (i == 0)
                            {
                                kmTotal += listaGrupo[i].kmOrigen + (listaGrupo[i].kmOrigenDestino * 2);
                            }
                            else if (i == maxIndex)
                            {
                                kmTotal += listaGrupo[i].kmOrigenDestino + listaGrupo[i].kmDestino;
                            }
                            else
                            {
                                kmTotal += (listaGrupo[i].kmOrigenDestino * 2);
                            }
                        }
                        return kmTotal;
                    }
                }
                return listaGrupo.Sum(s => s.km);
            }
        }
        public int? idOrigen { get; set; }
        public string origen { get; set; }
        public int idDestino { get; set; }
        public string destino { get; set; }
        public string orden { get; set; }
        public string producto { get; set; }
        public int idProducto { get; set; }
        public decimal capacidadIns { get; set; }
        public decimal toneladas { get; set; }
        public decimal toneladasDespachadas { get; set; }
        public decimal utilizado
        {
            get {
                try
                {
                    return (this.totalDespachados / this.capacidadIns);
                }
                catch (Exception ex)
                {
                    return 0;
                }
            }
        }
    
            
        public decimal totalDespachados { get; set; }
        public decimal precioVar { get; set; }
        public decimal subTotalVar { get; set; }
        public decimal precioFijo { get; set; }
        public decimal subTotal { get; set; }
        public decimal importe { get => subTotal + (impIva - impRetencion); }
        public bool cobroXKm { get; set; }
        public bool viaje { get; set; }
        public decimal impIva { get; set; }
        public decimal impRetencion { get; set; }
        public bool vFalso { get; set; }
        public EstatusProducto estatusProducto { get; set; }
        public string strvFalso { get => vFalso ? "SI" : "NO"; }
        public decimal pagoOperador { get; set; }
        public decimal pagoOperadorCap { get; set; }
        public int vale { get; set; }
        public decimal kmECM { get; set; }
        public decimal kmFisico { get; set; }
        public decimal _ltsVale { get; set; }
        public decimal porcLitro => ((km * 100) / kmTotal) / 100;
        public decimal ltsVale
        {
            get
            {
                if (vale == 36517)
                {

                }
                if (sumatoriaKm <= 0)
                {
                    return _ltsVale;
                }
                return (_ltsVale * km) / sumatoriaKm;
            }
        }
        public decimal sumatoriaKm { get; set; }
        public string usuario { get; set; }
        public string usuarioFin { get; set; }
        public string usuarioAjusta { get; set; }
        public string duracion
        {
            get
            {
                TimeSpan duracion;

                DateTime fInicio = fechaSalidaPlantaCSI.HasValue ? fechaSalidaPlantaCSI.Value : fechaInicio;
                DateTime? fFin = fechaEntradaPlantaCSI.HasValue ? fechaEntradaPlantaCSI : fechaFin;

                if (!fFin.HasValue)
                {
                    duracion = (TimeSpan)(DateTime.Now - fInicio);
                }
                else
                {
                    duracion = (fFin.Value - fInicio);
                }
                return string.Format("{0:000}:{1:00}:{2:00}", duracion.TotalHours.ToString().Split('.')[0], duracion.Minutes.ToString(), duracion.Seconds.ToString());
            }
        }
        public DateTime? fechaSalidaPlantaCSI { get; set; }
        public DateTime? fechaEntradaPlantaCSI { get; set; }
        public string duracionDespachoSalida
        {
            get
            {
                try
                {
                    TimeSpan duracion;

                    DateTime fInicio = fechaInicio;
                    DateTime? fFin = fechaSalidaPlantaCSI;

                    if (!fFin.HasValue)
                    {
                        return "N/D";
                    }
                    else
                    {
                        duracion = (fFin.Value - fInicio);
                        return string.Format("{0:000}:{1:00}:{2:00}", duracion.TotalHours.ToString().Split('.')[0], duracion.Minutes.ToString(), duracion.Seconds.ToString());
                    }
                }
                catch (Exception ex)
                {
                    return "N/D";
                }
            }
            
        }
        public string duracionCierreEntrada
        {
            get
            {
                try
                {
                    TimeSpan duracion;

                    DateTime? fInicio = fechaEntradaPlantaCSI;
                    DateTime? fFin = fechaFin;

                    if (!fFin.HasValue || !fFin.HasValue)
                    {
                        return "N/D";
                    }
                    else
                    {
                        duracion = (fFin.Value - fInicio.Value);
                        return string.Format("{0:000}:{1:00}:{2:00}", duracion.TotalHours.ToString().Split('.')[0], duracion.Minutes.ToString(), duracion.Seconds.ToString());
                    }
                }
                catch (Exception ex)
                {
                    return "N/D";
                }
            }
        }
        public string tiempoIda { get; set; }
        public string tiempoGranja { get; set; }
        public string tiempoRetorno { get; set; }
        public string tiempoTab
        {
            get
            {
                TimeSpan tIda;
                if (!TimeSpan.TryParse(tiempoIda, out tIda))
                {
                    tIda = new TimeSpan(0, 0, 0);
                }
                TimeSpan tGranja;
                if (!TimeSpan.TryParse(tiempoGranja, out tGranja))
                {
                    tGranja = new TimeSpan(0, 0, 0);
                }
                TimeSpan tRetorno;
                if (!TimeSpan.TryParse(tiempoRetorno, out tRetorno))
                {
                    tRetorno = new TimeSpan(0, 0, 0);
                }
                TimeSpan tabulado = tIda + tGranja + tRetorno;
                return tabulado.ToString();
            }
        }
        public string tiempoTabuladoCarga
        {
            get
            {
                if (tiempoCarga == "N/D")
                {
                    return tiempoCarga;
                }
                TimeSpan timeDuracion = new TimeSpan(0, 0, 0);
                TimeSpan timeTabulado = new TimeSpan(0, 0, 0);
                if (modalidad == "SENCILLO")
                {
                    timeTabulado = new TimeSpan(1, 0, 0);
                }
                else
                {
                    timeTabulado = new TimeSpan(1, 30, 0);
                }
                TimeSpan.TryParse(tiempoCarga, out timeDuracion);
                //TimeSpan.TryParse(timeTabulado, out timeTabulado);
                TimeSpan diferencia = timeTabulado.Subtract(timeDuracion); // timeDuracion.Subtract(timeTabulado);
                return diferencia.ToString();
            }
        }
        public string diferenciaTiempo
        {
            get
            {
                TimeSpan timeDuracion = new TimeSpan(0, 0, 0);
                TimeSpan timeTabulado = new TimeSpan(0, 0, 0);
                TimeSpan.TryParse(duracion, out timeDuracion);
                TimeSpan.TryParse(tiempoTab, out timeTabulado);

                TimeSpan diferencia = timeDuracion.Subtract(timeTabulado);
                return diferencia.ToString();

                //if (timeDuracion > timeTabulado)
                //{
                //    TimeSpan diferencia = timeDuracion.Subtract(timeTabulado);
                //    return diferencia.ToString();
                //}
                //return "00:00:00";
            }
        }
        public string zona { get; set; }
        public bool isReEnviar { get; set; }
        public string strReEnvio
        {
            get => isReEnviar ? "SI" : "NO";
        }
        public string imputable { get; set; }
        public string usuarioFactura { get; set; }
        public string noFactura { get; set; }
        public int ordenServ { get; set; }
        public string ticket { get; set; }
        public decimal precioLts1 { get; set; }
        public decimal precioLts2 { get; set; }
        public decimal precioLts
        {
            get
            {
                if (vale == 34545)
                {

                }
                if (!string.IsNullOrEmpty(ticket))
                {
                    return precioLts1;
                }
                else
                {
                    if (precioLts2 > 0)
                    {
                        return (precioLts1 + precioLts2) / 2;
                    }
                    else
                    {
                        return precioLts1;
                    }
                }
            }
        }
        public string facturaDiesel { get; set; }
        public decimal importeLts { get; set; }
        public string folioImpreso { get; set; }
        public string proveedorCombustible { get; set; }
        public decimal kmOrigen { get; set; }
        public decimal kmOrigenDestino { get; set; }
        public decimal kmDestino { get; set; }
        public decimal kmRedondo => kmOrigen + kmOrigenDestino + kmDestino;
        public List<GrupoRuta> listaGrupo { get; set; }
        public int noDestinos => (from n in listaGrupo select n.idDestino).Distinct().Count();
        public int noViajesOperadores { get; set; }     
        public int cveTipoGranja { get; set; }
        public string tipoGranja { get; set; }
        public int cveTipoGranja2 { get; set; }
        public string tipoGranja2 { get; set; }
    }
    public class GrupoRuta
    {
        public int folio { get; set; }
        public int cartaPorte { get; set; }
        public int? idOrigen { get; set; }
        public int idDestino { get; set; }
        public decimal km { get; set; }
        public decimal kmOrigen { get; set; }
        public decimal kmOrigenDestino { get; set; }
        public decimal kmDestino { get; set; }
        public decimal kmRedondo => kmOrigen + kmOrigenDestino + kmDestino;        
        
    }
    public class ReporteCartaPorteViajes : INotifyPropertyChanged
    {
        public List<ComentarioViaje> listaComentarios { get; set; }
        public Visibility visibleComentario =>        
             listaComentarios == null ? Visibility.Hidden : (listaComentarios.Count > 0 ? Visibility.Visible : Visibility.Hidden);
        public int contadorComentarios { get => listaComentarios.Count; }
        public string cveEmpresa => (from s in lista orderby s.km descending select s.cveEmpresa).First();
        public int cveUEN => (from s in lista orderby s.km descending select s.cveUEN).First();
        public string cveCliente => (from s in lista orderby s.km descending select s.cveCliente).First();
        public string cveSucCliente => (from s in lista orderby s.km descending select s.cveSucCliente).First();
        public int cveSucursal => (from s in lista orderby s.km descending select s.cveSucursal).First();
        public string cvePersonal => (from s in lista orderby s.km descending select s.cvePersonal).First();
        public string cveOperador => (from s in lista orderby s.km descending select s.cveOperador).First();
        public string cveAgenteOperador => (from s in lista orderby s.km descending select s.cveAgenteOperador).First();
        public int folio => (from s in lista orderby s.km descending select s.folio).First();
        public bool isSeleccionado
        {
            get
            {
                if (_isSeleccionado == null)
                {
                    return true;
                }
                return _isSeleccionado.Value;
            }
            set
            {
                _isSeleccionado = value;
                OnPropertyChanged();
            }
        }
        private bool? _isSeleccionado { get; set; }
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string name = "")
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(name));
        }
        public List<ReporteCartaPorte> lista { get; set; }
        public string cartaPorte
        {
            get
            {
                string cadena = string.Empty;
                foreach (var item in lista)
                {
                    cadena += item.cartaPorte.ToString() + ", ";
                }
                return cadena.Trim().Trim(',');
            }
        }
        public int idEmpresa => (from s in lista select s.idEmpresa).First();
        public string empresa => (from s in lista orderby s.km descending select s.empresa).First();
        public string cliente => (from s in lista orderby s.km descending select s.cliente).First();
        public int idCliente => (from s in lista orderby s.km descending select s.idCliente).First();
        public int idOperador => (from s in lista orderby s.km descending select s.idOperador).First();
        public int idOperadorFin => (from s in lista orderby s.km descending select s.idOperadorFin).First();
        public string encabezado =>
            $"VIAJE {this.folio}         ({this.fechaInicio.ToString("dd/MM/yy HH:mm")})       ({this.empresa})       ({this.cliente})   ";
        public string operador => (from s in lista orderby s.km descending select s.operador).First();
        public string operadorFin => (from s in lista orderby s.km descending select s.operadorFin).First();
        public string operadorAyuda => (from s in lista orderby s.km descending select s.operadorAyuda).First();
        public string operadorCap => (from s in lista orderby s.km descending select s.operadorCap).First();
        public string estatus => (from s in lista orderby s.km descending select s.estatus).First();
        public string modalidad => (from s in lista orderby s.km descending select s.modalidad).First();
        public string tc => (from s in lista orderby s.km descending select s.tc).First();
        public string rem1 => (from s in lista orderby s.km descending select s.rem1).First();
        public string dolly => (from s in lista orderby s.km descending select s.dolly).First();
        public string rem2 => (from s in lista orderby s.km descending select s.rem2).First();
        public DateTime? fechaInicioCarga { get; set; }
        public DateTime? fechaCarga => (from s in lista orderby s.fechaCarga descending select s.fechaCarga).First();
        public DateTime? fechaDescarga => (from s in lista orderby s.fechaDescarga descending select s.fechaDescarga).First();
        public string tiempoCarga => (from s in lista orderby s.km descending select s.tiempoCarga).First();
        public DateTime fechaInicio { get; set; }
        public string strFechaInicio =>
            this.fechaInicio.ToString("dd/MM/yyyy");
        public string horaInicio =>
            this.fechaInicio.ToString("HH:mm");
        public DateTime? fechaFin { get; set; }
        public string strFechaFin =>
            (!this.fechaFin.HasValue ? string.Empty : this.fechaFin.Value.ToString("dd/MM/yyyy"));

        public string horaFin =>
            (!this.fechaFin.HasValue ? string.Empty : this.fechaFin.Value.ToString("HH:mm"));
        public DateTime? fechaPrograma { get; set; }
        public string fechaProramadastr =>
            (!this.fechaPrograma.HasValue ? string.Empty : this.fechaPrograma.Value.ToString("dd/MM/yyyy"));
        public decimal km
        {
            get
            {
                if (idEmpresa == 1)
                {
                    return (from s in lista orderby s.km descending select s.km).First();
                }
                else if (idEmpresa == 2)
                {
                    return lista.Sum(s => s.km);
                }
                return (from s in lista orderby s.km descending select s.km).First();
            }
        }

        //public int? idOrigen { get; set; }
        public string origen
        {
            get
            {
                string cadena = string.Empty;
                foreach (var item in lista)
                {
                    if (!cadena.Contains(item.origen))
                    {
                        cadena += item.origen + ", ";
                    }
                }
                return cadena.Trim().Trim(',');
            }
        }
        //public int idDestino { get; set; }
        public string destino
        {
            get
            {
                string cadena = string.Empty;
                foreach (var item in lista)
                {
                    if (!cadena.Contains(item.destino))
                    {
                        cadena += item.destino + ", ";
                    }
                }
                return cadena.Trim().Trim(',');
            }
        }
        public string orden
        {
            get
            {
                string cadena = string.Empty;
                foreach (var item in lista)
                {
                    if (!cadena.Contains(item.orden))
                    {
                        cadena += item.orden + ", ";
                    }
                }
                return cadena.Trim().Trim(',');
            }
        }
        public string producto
        {
            get
            {
                string cadena = string.Empty;
                foreach (var item in lista)
                {
                    if (!cadena.Contains(item.producto))
                    {
                        cadena += item.producto + ", ";
                    }
                }
                return cadena.Trim().Trim(',');
            }
        }
        public decimal capacidadIns => (from s in lista orderby s.km descending select s.capacidadIns).First();
        public decimal toneladas => lista.Sum(s => s.toneladas);
        public decimal toneladasDespachadas => lista.Sum(s => s.toneladasDespachadas);
        public decimal utilizado => (from s in lista orderby s.km descending select s.utilizado).First();
        public decimal precioVar => (from s in lista orderby s.km descending select s.precioVar).First();
        public decimal subTotalVar => lista.Sum(s => s.subTotalVar);
        public decimal precioFijo => (from s in lista orderby s.km descending select s.precioFijo).First();
        public decimal subTotal => lista.Sum(s => s.subTotal);
        public decimal importe => lista.Sum(s => s.importe);
        public decimal impIva => lista.Sum(s => s.impIva);
        public decimal impRetencion => lista.Sum(s => s.impRetencion);
        public string strvFalso => (from s in lista orderby s.km descending select s.strvFalso).First();
        public EstatusProducto estatusProducto => (from s in lista select s.estatusProducto).First();
        public decimal pagoOperador => (from s in lista orderby s.km descending select s.pagoOperador).First();
        public decimal pagoOperadorCap => (from s in lista orderby s.km descending select s.pagoOperadorCap).First();
        public int vale => (from s in lista orderby s.km descending select s.vale).First();
        public decimal kmECM => (from s in lista orderby s.km descending select s.kmECM).First();
        public decimal ltsVale => lista.Sum(s => s.ltsVale);
        public string usuario => (from s in lista orderby s.km descending select s.usuario).First();
        public string usuarioFin => (from s in lista orderby s.km descending select s.usuarioFin).First();
        public string usuarioAjusta => (from s in lista orderby s.km descending select s.usuarioAjusta).First();
        public string duracion => (from s in lista orderby s.km descending select s.duracion).First();
        public DateTime? fechaSalidaPlantaCSI => (from s in lista orderby s.km descending select s.fechaSalidaPlantaCSI).First();
        public DateTime? fechaEntradaPlantaCSI => (from s in lista orderby s.km descending select s.fechaEntradaPlantaCSI).First();
        public string duracionDespachoSalida => (from s in lista orderby s.km descending select s.duracionDespachoSalida).First();
        public string duracionCierreEntrada => (from s in lista orderby s.km descending select s.duracionCierreEntrada).First();
        public string tiempoTab => (from s in lista orderby s.km descending select s.tiempoTab).First();
        public string tiempoTabuladoCarga => (from s in lista orderby s.km descending select s.tiempoTabuladoCarga).First();
        public string diferenciaTiempo => (from s in lista orderby s.km descending select s.diferenciaTiempo).First();
        public string zona => (from s in lista orderby s.km descending select s.zona).First();
        public bool isReEnviar { get; set; }
        public string strReEnvio
        {
            get => isReEnviar ? "SI" : "NO";
        }
        public string imputable { get; set; }
        public string usuarioFactura => (from s in lista select s.usuarioFactura).First();
        public string noFactura => (from s in lista select s.noFactura).First();
        public int ordenServ => (from s in lista select s.ordenServ).First();
        public string ticket => (from s in lista select s.ticket).First();
        public decimal precioLts => (from s in lista select s.precioLts).First();
        public decimal importeLts => (from s in lista select s.importeLts).First();
        public string folioImpreso => (from s in lista select s.folioImpreso).First();
        public string facturaDiesel => (from s in lista select s.facturaDiesel).First();
        public string proveedorCombustible => (from s in lista select s.proveedorCombustible).First();
        public decimal kmFisico => (from s in lista select s.kmFisico).First();
        public int noDestinos => (from s in lista select s.noDestinos).First();
        public int noViajesOperadores => (from s in lista select s.noViajesOperadores).First();
        public int cveTipoGranja => (from s in lista select s.cveTipoGranja).First();
        public string tipoGranja => (from s in lista select s.tipoGranja).First();
        public int cveTipoGranja2 => (from s in lista select s.cveTipoGranja2).First();
        public string tipoGranja2 => (from s in lista select s.tipoGranja2).First();
    }
}
