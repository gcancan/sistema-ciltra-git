﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class Anticipo
    {
        public int idAnticipo { get; set; }
        public int idDestino { get; set; }
        public TipoAnticipo tipoAnticipo { get; set; }
        public decimal sencillo2Ejes { get; set; }
        public decimal sencillo3Ejes { get; set; }
        public decimal full { get; set; }
        public decimal costo { get; set; }
        public string observaciones { get; set; }

    }
    public class TipoAnticipo
    {
        public int idTipoAnticipo { get; set; }
        public string anticipo { get; set; }
        public bool activo { get; set; }
        public bool obs { get; set; }
    }

    public class AnticipoMaster
    {
        public int idAnticipoMaster { get; set; }
        public int folioViaje { get; set; }
        public DateTime fechaPago { get; set; }
        public string usuario { get; set; }
        public List<DetalleAnticipoMaster> listaDetalles { get; set; }
        public int cveINTELISIS { get; set; }
        public decimal importe => listaDetalles.Sum(s => s.importe);
    }
    public class DetalleAnticipoMaster
    {
        public int idDetalleAnticipoMaster { get; set; }
        public int idAnticipoMaster { get; set; }
        public TipoAnticipo tipoAnticipo { get; set; }
        public decimal importe { get; set; }       
    }
}
