﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public enum eImputable
    {
        CLIENTE = 0,
        PEGASO = 1,
        CASO_ESPECIAL = 2
    }
}
