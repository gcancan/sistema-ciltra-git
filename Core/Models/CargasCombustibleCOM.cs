﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class CargasCombustibleCOM
    {
        public int idCargaCombustible { get; set; }
        public DateTime fecha { get; set; }
        public string empresa { get; set; }
        public decimal lts { get; set; }
        public int extra { get; set; }
    }
}
