﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
namespace CoreFletera.Models
{
    public class DetalleLlantera
    {
        public int idOrdenServ { get; set; }
        public int idOrdenActividad { get; set; }
        public int idOrdenTrabajo { get; set; }
        public Servicios servicio { get; set; }
        public Actividad actividad { get; set; }
        public int posLlanta { get; set; }
        public Llanta llanta { get; set; }
        private string _descripcion { get; set; }
        public string estatus { get; set; }
        public string descripcion
        {
            get
            {
                if (actividad != null)
                {
                    return actividad.nombre;
                }
                else if (servicio != null)
                {
                    return servicio.nombreServicio;
                }
                return _descripcion;
            }
            set
            {
                _descripcion = value;
            }
        }
    }
    public class Servicios
    {
        internal List<Actividad> listaActividades { get; set; }

        public int idServicio { get; set; }
        public TipoOrdenServ tipoOrden {get;set; }
        public string nombreServicio { get; set; }
        public string descripcion { get; set; }
        public decimal costo { get; set; }
    }
    public class TipoOrdenServ
    {
        public int idTipoOrdServ { get; set; }
        public string nombreTioServ { get; set; }
    }
}
