﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class InsersionKm
    {
        public int idInsersionKm { get; set; }
        public UnidadTransporte unidadTransporte { get; set; }
        public DateTime fechaCaptura { get; set; }
        public string usuarioCaptura { get; set; }
        public DateTime fechaInsersion { get; set; }
        public enumTipoInsersionKM enumTipoInsersion { get; set; }
        public Servicio servicio { get; set; }
        public decimal km { get; set; }
    }
}
