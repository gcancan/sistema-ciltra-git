﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class DatosFacturaXML
    {
        public string noCertificado { get; set; }
        public string sello { get; set; }
        public string selloSAT { get; set; }
        public string selloCFD { get; set; }
        public string UUID { get; set; }
        public string noCertificadoSAT { get; set; }
    }
}
