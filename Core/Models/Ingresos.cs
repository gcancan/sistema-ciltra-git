﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class Ingresos
    {
        public string concepto { get; set; }
        public decimal total { get; set; }
    }
}
