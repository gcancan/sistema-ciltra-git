﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class Tickets
    {
        public DateTime fecha { get; set; }
        public string unidadTransporte { get; set; }
        public int ordenServicio { get; set; }
        public string ticket { get; set; }
        public decimal lts { get; set; }
        public string folioImpreso { get; set; }
    }
}
