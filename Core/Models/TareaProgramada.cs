﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class TareaProgramada
    {
        public int idTareaProgramada { get; set; }
        public int idProducto { get; set; }
        public int idCliente { get; set; }
        public string DescripcionProducto { get; set; }
        public decimal valorProgramado { get; set; }
        public string destino { get; set; }
        public int remision { get; set; }
        public string hoja { get; set; }
        public bool estatus { get; set; }
        public DateTime fecha { get; set; }
    }
}
