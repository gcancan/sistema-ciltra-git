﻿namespace CoreFletera.Models
{
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class RegistroCSI
    {        
        public int idRegistroCSI { get; set; }
        public string serie { get; set; }
        public DateTime fechaCSI { get; set; }
        public string geoCerca { get; set; }
        public bool isEntrada { get; set; }
        public bool isPlanta { get; set; }
        public bool isTaller { get; set; }
    }
}
