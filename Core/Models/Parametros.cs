﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class Parametros
    {
        public string idConceptoEntAlm { get; set; }
        public string idConceptoSalAlm { get; set; }
        public string idAlmacenEnt { get; set; }
        public string idAlmacenSal { get; set; }
    }
}
