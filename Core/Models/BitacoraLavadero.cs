﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class BitacoraLavadero
    {
        public int idPadreOrdSer { get; set; }
        public int idOrdenServ { get; set; }
        public string unidadTrans { get; set; }
        public DateTime fechaSolicitud { get; set; }
        public DateTime ? fechaRecepcion { get; set; }
        public DateTime ? fechaTerminado { get; set; }
        public string personal { get; set; }
        public decimal costoInsumos { get; set; }
        public decimal costoInspecciones { get; set; }
        public string estatus { get; set; }
        public string usuario { get; set; }
        public decimal importe { get { return costoInsumos + costoInspecciones; } }
    }
}
