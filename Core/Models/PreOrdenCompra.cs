﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class PreOrdenCompra
    {
        public int idPreOrdenCompra { get; set; }
        public int ordenServicio { get; set; }
        public string NomTipOrdServ { get; set; }
        public string unidadTransporte { get; set; }
        public int? idOrdenTrabajo { get; set; }
        public int? idOrdenCompra { get; set; }
        public int? idCotizacion { get; set; }
        public DateTime fechaPreOrden { get; set; }
        public string usuario { get; set; }
        public string estatus { get; set; }
        public string usuarioAutorizaOC { get; set; }
        public DateTime? fechaAutoriza { get; set; }
        public string observaciones { get; set; }
        public DateTime? fechaModifica { get; set; }
        public string usuarioModifica { get; set; }
        public decimal subTotal { get; set; }
        public decimal IVA { get; set; }
        public DateTime? fechaCancela { get; set; }
        public string usuarioCancela { get; set; }
        public string motioCancelacion { get; set; }
        public List<DetallePreOrdenCompra> listaDetalles { get; set; }
    }

    public class DetallePreOrdenCompra
    {
        public int idDetallePreOrdCom { get; set; }
        public ProductosCOM producto { get; set; }
        public decimal cantidadSolicitada { get; set; }
        public decimal cantidad { get; set; }

        private decimal? _faltante { get; set; }
        public decimal faltante
        {
            get
            {
                return _faltante != null ? (decimal)_faltante : ((cantidadSolicitada - cantidad) < 0 ? 0 : (cantidadSolicitada - cantidad));
            }
            set
            {
                _faltante = value;
            }
        }
        public decimal precio { get; set; }
        public decimal porcIVA { get; set; }
    }
}
