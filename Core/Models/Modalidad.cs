﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class Modalidades
    {
        public int idModalidad { get; set; }
        public string modalidad { get; set; }
        public int subModalidad { get; set; }
        public string unidadMedida { get; set; }
        public string descripcion => string.Format("{0} {1}", subModalidad.ToString(), unidadMedida);
    }
}
