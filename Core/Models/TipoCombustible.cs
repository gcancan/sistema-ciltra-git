﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class TipoCombustible
    {
        public int idTipoCombustible { get; set; }
        public string tipoCombustible { get; set; }
        public string usuario { get; set; }
        public decimal ultimoPrecio { get; set; }
    }
}
