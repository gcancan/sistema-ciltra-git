﻿using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class Personal
    {
        public int clave { get; set; }
        public string cveINTELISIS { get; set; }
        public string cveEmpresaINTELISIS { get; set; }
        public string cveAgente { get; set; }
        public string tipo { get; set; }
        public string nombres { get; set; }
        public string apellidoPaterno { get; set; }
        public string apellidoMaterno { get; set; }
        private string _nombre { get; set; }
        public string nombre
        {
            get
            {
                if (string.IsNullOrEmpty(apellidoMaterno))
                {
                    return string.IsNullOrEmpty(_nombre) ? (string.Format("{0} {1}", apellidoPaterno, nombres).Trim()) : _nombre;
                }
                else
                {
                  return string.IsNullOrEmpty(_nombre) ? (string.Format("{0} {1} {2}", apellidoPaterno, apellidoMaterno, nombres).Trim()) : _nombre;
                }
            } 
            set => _nombre = value;
        }        
        public DateTime? fechaNacimiento { get; set; }
        public string tipoSangre { get; set; }
        public DateTime? fechaIngreso { get; set; }
        public string tipo_Contrato { get; set; }

        public Empresa empresa { get; set; }
        public int ? clave_empresa
        {
            get
            {
                if (empresa != null)
                {
                    return empresa.clave;
                }
                return null;
            }
        }

        public Departamento departamento { get; set; }
        public string departamentoStr { get; set; }
        public Puesto puesto { get; set; }
        public string puestoStr { get; set; }
        public string cvePlaza { get; set; }
        public int? clave_departamento
        {
            get
            {
                if (departamento!=null)
                {
                    return departamento.clave;
                }
                return null;
            }
        }

        public CategoriaEmpleado categoriaEmpleado { get; set; }
        public int ? clave_categoria
        {
            get
            {
                if (categoriaEmpleado!=null)
                {
                    return categoriaEmpleado.clave;
                }
                return null;
            }
        }
        public Colonia colonia { get; set; }
        public string ciudad { get; set; }
        public string delegacion { get; set; }
        public string coloniaStr { get; set; }
        public string direccion { get; set; }
        public string calle { get; set; }
        public string estado { get; set; }
        public string pais { get; set; }
        public string numero { get; set; }
        public string numeroInt { get; set; }
        public string cruce1 { get; set; }
        public string cruce2 { get; set; }
        public int cp { get; set; }
        public string telefono { get; set; }
        public string telefonoCelular { get; set; }
        public string correoElectronico { get; set; }
        public string genero { get; set; }
        public string estadoCivil { get; set; }
        public string tallaCamisa { get; set; }
        public string tallaZapatos { get; set; }
        public string rfid { get; set; }
        public string estatus { get; set; }
        public string estatusBD =>
            (estatus == "ACTIVO" ? "ACT" : "BAJ");
        public string usuario { get; set; }
        public bool? activo { get; set; }
        public List<RegistroAjustes> listaAjustes { get; set; }
        public decimal pagoFijo { get; set; }
        public ApoyoPersonal apoyoPersonal { get; set; }
        public ContactoEmergencia contactoEmergencia { get; set; }
        public List<DatosIngresoPersonal> listaIngresos { get; set; }
        DatosIngresoPersonal _ingreso { get; set; }
        public DatosIngresoPersonal ingreso
        {
            get
            {
                if (_ingreso != null)
                {
                    return _ingreso;
                }
                else
                {
                    if (listaIngresos == null) return null;
                    return listaIngresos.Find(s => s.tipoIngreso == TipoIngreso.INGRESO);
                }
            }
            set
            {
                _ingreso = value;
            }
        }

        DatosIngresoPersonal _reingreso { get; set; }
        public DatosIngresoPersonal reingreso
        {
            get
            {
                if (_reingreso != null)
                {
                    return _reingreso;
                }
                else
                {
                    if (listaIngresos == null) return null;
                    return listaIngresos.Find(s => s.tipoIngreso == TipoIngreso.REINGRESO);
                }
            }
            set
            {
                _reingreso = value;
            }
        }

        DatosIngresoPersonal _cambioPuesto { get; set; }
        public DatosIngresoPersonal cambioPuesto
        {
            get
            {
                if (_cambioPuesto != null)
                {
                    return _cambioPuesto;
                }
                else
                {
                    if (listaIngresos == null) return null;
                    return listaIngresos.Find(s => s.tipoIngreso == TipoIngreso.CAMBIO_PUESTO);
                }
            }
            set
            {
                _cambioPuesto = value;
            }
        }
        public DocumentacionPersonal documentacionPersonal { get; set; }
        public event PropertyChangedEventHandler PropertyChanged;
        private bool? _isSeleccionado { get; set; }
        public bool isSeleccionado
        {
            get
            {
                if (_isSeleccionado == null)
                {
                    return false;
                }
                return _isSeleccionado.Value;
            }
            set
            {
                _isSeleccionado = value;
                OnPropertyChanged();
            }
        }
        protected void OnPropertyChanged([CallerMemberName] string name = "")
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(name));
        }
        public byte[] foto { get; set; }
    }   
    public class ApoyoPersonal : INotifyPropertyChanged
    {
        public int idApoyo { get; set; }
        public int idPersonal { get; set; }
        private int? _metaViaje { get; set; }
        public int metaViaje
        {
            get
            {
                if (_metaViaje == null)
                {
                    return 0;
                }
                return _metaViaje.Value;
            }
            set
            {
                _metaViaje = value;
                OnPropertyChanged();
            }
        }
        private decimal? _pagoApoyo { get; set; }
        public decimal pagoApoyo
        {
            get
            {
                if (_pagoApoyo == null)
                {
                    return 0;
                }
                return _pagoApoyo.Value;
            }
            set
            {
                _pagoApoyo = value;
                OnPropertyChanged();
            }
        }
        private bool? _activo { get; set; }
        public bool activo
        {
            get
            {
                if (_activo == null)
                {
                    return true;
                }
                return _activo.Value;
            }
            set
            {
                _activo = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string name = "")
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(name));
        }
    }
    public class ContactoEmergencia
    {
        public int idContactoEmergencia { get; set; }
        public int idPersonal { get; set; }
        public string parentezco { get; set; }
        public string nombres { get; set; }
        public string apellidoPaterno { get; set; }
        public string apellidoMaterno { get; set; }
        public string telefonoDomicilio { get; set; }
        public string telefonoCelular { get; set; }
        public string usuario { get; set; }
    }
    public class DatosIngresoPersonal
    {
        public int idDatoIngreso { get; set; }
        public int idPersonal { get; set; }
        public DateTime fechaInicio { get; set; }
        public DateTime fechaFinCapacitacion { get; set; }
        public DateTime? fechaBaja { get; set; }
        public string motivoBaja { get; set; }
        public string usuarioRegistra { get; set; }
        public string usuarioBaja { get; set; }
        public Puesto puesto { get; set; }
        public Departamento departamento { get; set; }
        public decimal salarioDiario { get; set; }
        public decimal salarioHora { get; set; }
        public int maximoHorasExtra { get; set; }
        public string observaciones { get; set; }
        public TipoIngreso tipoIngreso { get; set; }
    }
    public enum TipoIngreso
    {
        INGRESO = 0,
        REINGRESO = 1,
        CAMBIO_PUESTO = 2
    }
    public class DocumentacionPersonal
    {
        public int idDocumentacionPersonal { get; set; }
        public int idPersonal { get; set; }
        public string numeroSeguroSocial { get; set; }
        public string CURP { get; set; }
        public string INE { get; set; }
        public string RFC { get; set; }
        public List<LicenciaConducir> listaLicencias { get; set; }
        private LicenciaConducir _licenciaEstatal { get; set; }
        public LicenciaConducir licenciaEstatal 
        {
            get
            {
                if (_licenciaEstatal != null) return _licenciaEstatal;

                if (listaLicencias == null) return null;

                return listaLicencias.Find(s => s.clasificacionLicencia == ClasificacionLicencia.ESTATAL);
            }
            set
            {
                _licenciaEstatal = value;
            }
        }
        private LicenciaConducir _licenciaFederal { get; set; }
        public LicenciaConducir licenciaFederal
        {
            get
            {
                if (_licenciaFederal != null) return _licenciaFederal;

                if (listaLicencias == null) return null;

                return listaLicencias.Find(s => s.clasificacionLicencia == ClasificacionLicencia.FEDERAL);
            }
            set
            {
                _licenciaFederal = value;
            }
        }
        public string banco { get; set; }
        public string cuenta { get; set; }
        public string clabe { get; set; }
        public string observaciones { get; set; }
        public string noExamenPsicofisico { get; set; }
        public DateTime? fechaVencimientoExamen { get; set; }
        public string usuario { get; set; }

    }
    public class LicenciaConducir
    {
        public int idLicencia { get; set; }
        public int idPersonal { get; set; }
        public string noLicencia { get; set; }
        public string tipoLicencia { get; set; }
        public DateTime vencimiento { get; set; }
        public ClasificacionLicencia clasificacionLicencia { get; set; }
        public string usuario { get; set; }
    }
    public enum ClasificacionLicencia
    {
        ESTATAL = 0,
        FEDERAL = 1
    }
}
