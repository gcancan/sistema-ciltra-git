﻿namespace CoreFletera.Models
{
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.Serialization;

    [DataContract]
    public class Respuesta
    {        
        public string estatus { get; set; }
        public int code { get; set; }
        public int pag { get; set; }
        public int totPag { get; set; }
    }
    [DataContract]
    public class RespuestaJson
    {
        public bool valido { get; set; }
        public string mensaje { get; set; }
        public int CodEstado { get; set; }
    }
}
