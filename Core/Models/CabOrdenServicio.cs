﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class CabOrdenServicio
    {
        public int idOrdenSer { get; set; }
        public int? idOrdenSerOrigen { get; set; }
        public int? cveUEN { get; set; }
        public int? cveSucCliente { get; set; }
        public Empresa empresa { get; set; }
        public ZonaOperativa zonaOperativa { get; set; }
        public Cliente cliente { get; set; }
        public UnidadTransporte unidad { get; set; }
        public enumTipoOrden enumTipoOrden { get; set; }
        public enumTipoOrdServ enumTipoOrdServ { get; set; }
        public enumTipoServicio enumtipoServicio { get; set; }
        public DateTime? fechaRecepcion { get; set; }
        public int? idEmpleadoEntrega { get; set; }
        public string usuarioRecepcion { get; set; }
        public string km { get; set; }
        public string estatus { get; set; }
        public DateTime? fechaDiagnostico { get; set; }
        public DateTime? fechaAsignado { get; set; }
        public DateTime? fechaTerminado { get; set; }
        public DateTime? fechaEntregado { get; set; }
        public string usuarioEntrega { get; set; }
        public int? idEmpleadoRecibe { get; set; }
        public string notaFinal { get; set; }
        public string usuarioCancela { get; set; }
        public DateTime? fechaCancela { get; set; }
        public int idEmpleadoAutoriza { get; set; }
        public string cveEmpleadoAutoriza { get; set; }
        public DateTime fechaCaptura { get; set; }
        public bool externo { get; set; }
        public string usuarioAsigna { get; set; }
        public string usuarioTermina { get; set; }
        public List<DetOrdenServicio> listaDetOrdenServicio { get; set; }
        public List<int> listaIdCabOS { get; set; }
        public int idPadreOrdSer { get; set; }
        public List<ActividadOrdenServicio> listaActividades { get; set; }
        public string observaciones { get; set; }
        public CargaCombustibleExtra cargaCombustibleExtra { get; set; } 
    }
    public enum enumActividadOrdenServ
    {
        SI = 0,
        NO = 1,
        PENDIENTE = 2
    }
    public class ActividadOrdenServicio
    {
        public int idActividadOrdenServicio { get; set; }
        public int idOrdenTrabajo { get; set; }
        public int idActividadPendiente { get; set; }
        public DateTime fecha { get; set; }
        public string usuario { get; set; }
        public int idPadreOrdenServicio { get; set; }
        public int idOrdenServicio { get; set; }
        public enumTipoOrdServ enumTipoOrdServ { get; set; }
        public Actividad actividad { get; set; }
        public Servicio servicio { get; set; }
        public int? posicion { get; set; }
        public string descripcion
        {
            get
            {
                if (actividad != null)
                {
                    return actividad.nombre;
                }
                else if (servicio != null)
                {
                    return servicio.nombre;
                }
                return string.Empty;
            }
        }
        public int? idResponsable { get; set; }
        public int? idAuxiliar1 { get; set; }
        public int? idAuxiliar2 { get; set; }
        public bool activo { get; set; }
        private bool? _pendiente { get; set; }
        public bool pendiente
        {
            get => _pendiente == null ? false : _pendiente.Value;
            set { _pendiente = value; }
        }
        public DateTime? fechaCancelacion { get; set; }
        public string motivoCancelacion { get; set; }
        public MotivoCancelacion motivoCancelacionObj { get; set; }
        public string usuarioCancela { get; set; }
        public enumActividadOrdenServ enumActividadOrdenServ { get; set; }
    }

    public class ActividadPendiente
    {
        public int idActividadPendiente { get; set; }
        public UnidadTransporte unidad { get; set; }
        public int idOrdenServicio { get; set; }
        public DateTime fecha { get; set; }
        public enumTipoOrden enumTipoOrden { get; set; }
        public enumTipoOrdServ enumTipoOrdServ { get; set; }
        private enumTipoServicio _enumTipoServicio { get; set; }
        public enumTipoServicio enumTipoServicio
        {
            get
            {
                switch (enumTipoOrdServ)
                {

                    case enumTipoOrdServ.TALLER:
                        if (enumTipoOrden == enumTipoOrden.PREVENTIVO)
                        {
                            return enumTipoServicio.PREVENTIVO;
                        }
                        else
                        {
                            return enumTipoServicio.CORRECTIVO;
                        }
                    case enumTipoOrdServ.LLANTAS:
                        return enumTipoServicio.CORRECTIVO;
                    case enumTipoOrdServ.LAVADERO:
                        return enumTipoServicio.PREVENTIVO;
                    default:
                        return enumTipoServicio.CORRECTIVO;
                }
            }
            set { _enumTipoServicio = value; }
        }
        public Actividad actividad { get; set; }
        public Servicio servicio { get; set; }
        public string descripcion =>
            actividad == null ? servicio.nombre : actividad.nombre;
        public int posicion { get; set; }
        public string usuario { get; set; }
        public bool estatus { get; set; }
    }

}
