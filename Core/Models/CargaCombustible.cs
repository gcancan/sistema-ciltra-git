﻿namespace Core.Models
{
    using CoreFletera.Models;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class CargaCombustible
    {
        public TanqueCombustible tanqueCombustible { get; set; }
        public UnidadTransporte unidadTransporte { get; set; }
        public ZonaOperativa zonaOperativa { get; set; }
        public Empresa empresa { get; set; }
        public DateTime? fechaEntrada { get; set; }
        public MasterOrdServ masterOrdServ { get; set; }
        public int idCargaCombustible { get; set; }
        public int idSalidaEntrada { get; set; }
        public bool isCR { get; set; }
        public int idChofer { get; set; }
        public int idOrdenServ { get; set; }
        public DateTime? fechaCombustible { get; set; }
        public decimal kmDespachador { get; set; }
        public decimal kmRecorridos { get; set; }
        public decimal precioCombustible { get; set; }
        public decimal ltCombustible { get; set; }
        public decimal ltCombustiblePTO { get; set; }
        public decimal ltCombustibleEST { get; set; }
        public decimal ltCombustibleUtilizado { get; set; }
        public decimal relleno => ltCombustibleUtilizado == 0 ? 0 : ((ltCombustible + ltsExtra) - ltCombustibleUtilizado);
        public decimal sumaLitrosExtra { get; set; }
        public decimal sumasTotalesCompleto =>
            ((this.ltCombustible + this.ltsExtra) + this.sumaLitrosExtra);
        public decimal ltsManejo =>
            (this.ltCombustibleUtilizado - (this.ltCombustibleEST + this.ltCombustiblePTO));
        public string tiempoTotal { get; set; }
        public string tiempoPTO { get; set; }
        public string tiempoEST { get; set; }
        public string tiempoManejo
        {
            get
            {
                char[] separator = new char[] { ':' };
                string[] strArray = this.tiempoTotal.Split(separator);
                TimeSpan span = new TimeSpan(Convert.ToInt32(strArray[0]), Convert.ToInt32(strArray[1]), Convert.ToInt32(strArray[2]));
                char[] chArray2 = new char[] { ':' };
                strArray = this.tiempoPTO.Split(chArray2);
                TimeSpan span2 = new TimeSpan(Convert.ToInt32(strArray[0]), Convert.ToInt32(strArray[1]), Convert.ToInt32(strArray[2]));
                char[] chArray3 = new char[] { ':' };
                strArray = this.tiempoEST.Split(chArray3);
                TimeSpan span3 = new TimeSpan(Convert.ToInt32(strArray[0]), Convert.ToInt32(strArray[1]), Convert.ToInt32(strArray[2]));
                TimeSpan span4 = span - (span2 + span3);
                return span4.ToString();
            }
        }
        public decimal segundosTotal
        {
            get
            {
                char[] separator = new char[] { ':' };
                string[] strArray = this.tiempoTotal.Split(separator);
                TimeSpan span = new TimeSpan(Convert.ToInt32(strArray[0]), Convert.ToInt32(strArray[1]), Convert.ToInt32(strArray[2]));
                return Convert.ToDecimal(span.TotalSeconds);
            }
        }
        public decimal segundosManejo
        {
            get
            {
                if (TimeSpan.TryParse(this.tiempoManejo, out TimeSpan span))
                {
                    return Convert.ToDecimal(span.TotalSeconds);
                }
                return decimal.Zero;
            }
        }
        public decimal segundosPTO
        {
            get
            {
                if (TimeSpan.TryParse(this.tiempoPTO, out TimeSpan span))
                {
                    return Convert.ToDecimal(span.TotalSeconds);
                }
                return decimal.Zero;
            }
        }
        public decimal segundosEST
        {
            get
            {
                if (TimeSpan.TryParse(this.tiempoEST, out TimeSpan span))
                {
                    return Convert.ToDecimal(span.TotalSeconds);
                }
                return decimal.Zero;
            }
        }
        public int? sello1 { get; set; }
        public int? sello2 { get; set; }
        public int? selloReserva { get; set; }
        public Personal despachador { get; set; }
        public decimal kmInicial { get; set; }
        public decimal kmFinal { get; set; }
        public List<CartaPorte> listCP { get; set; }
        public List<Viaje> listViajes { get; set; }
        public string usuarioCrea { get; set; }
        public List<CargaCombustibleExtra> listCombustibleExtra { get; set; }
        public decimal ltsSumaTotales =>
            (this.ltCombustible + this.ltsExtra);
        public bool isExterno { get; set; }
        public string ticket { get; set; }
        public string folioImpreso { get; set; }
        public string observacioses { get; set; }
        public ProveedorCombustible proveedor { get; set; }
        public ProveedorCombustible proveedorExtra { get; set; }
        public string ticketExtra { get; set; }
        public decimal ltsExtra { get; set; }
        public string folioImpresoExtra { get; set; }
        public decimal importeExtra
        {
            get
            {
                if (this.proveedorExtra != null)
                {
                    return (this.ltsExtra * this.proveedorExtra.precio);
                }
                return decimal.Zero;
            }
        }
        private bool? _ecm { get; set; }
        public bool ecm
        {
            get
            {
                if (_ecm == null)
                {
                    return true;
                }
                return _ecm.Value;
            }
            set
            {
                _ecm = value;
            }
        }
        public int? idCliente { get; set; }
        public List<RegistroAjustes> listaAjustes { get; set; }
        public string facturaPemex { get; set; }
        public string facturaProveedor { get; set; }
        public List<TicketsExterno> listaTickets { get; set; }
        public List<CargaTicketDiesel> listaCargaTickets { get; set; }
        private bool? _isHubodometo { get; set; }
        public bool isHubodometo
        {
            get
            {
                return _isHubodometo ?? true;
            }
            set
            {
                _isHubodometo = value;
            }
        }
    }

    public class TicketsExterno
    {
        public int idTicketsExterno { get; set; }
        public int idCargaCombustible { get; set; }
        public Empresa empresa { get; set; }
        public ZonaOperativa zonaOperativa { get; set; }
        public Cliente cliente { get; set; }        
        public ProveedorCombustible Proveedor { get; set; }
        public string folioImpreso { get; set; }
        public string ticket { get; set; }
        public decimal litros { get; set; }
        public decimal precio { get; set; }
        public decimal IEPS { get; set; }
        public decimal importe => litros * precio;
        public bool isExtra { get; set; }        
    }

}
