﻿namespace CoreFletera.Models
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class RevisionLlantas
    {
        public int idRevisionLlanta { get; set; }
        public DateTime fechaRevision { get; set; }
        public string usuario { get; set; }
        public int? idOrdenServ { get; set; }
        public string idUnidadTrans { get; set; }
        public int idEmpresa { get; set; }
        public decimal km { get; set; }
        public List<DetalleRevisionLlantas> listaDetalles { get; set; }
    }

    public class DetalleRevisionLlantas
    {
        public int idDetalleRevisionLlanta { get; set; }
        public int idRevisionLlanta { get; set; }
        public Llanta llanta { get; set; }
        public decimal profundidadAnterior { get; set; }
        public decimal profundidadNueva { get; set; }
        public decimal kmAnterior { get; set; }
        public decimal kmNuevo { get; set; }
    }

}
