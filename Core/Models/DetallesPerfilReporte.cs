﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class DetallesPerfilReporte
    {
        public int idDetallePerfilReporte { get; set; }
        public int idPerfilReporte { get; set; }
        public string nombreDetalle { get; set; }
    }
}