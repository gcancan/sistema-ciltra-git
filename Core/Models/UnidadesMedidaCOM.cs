﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class UnidadesMedidaCOM
    {
        public int CIDUNIDAD { get; set; }
        public string CNOMBREUNIDAD { get; set; }
        public string CABREVIATURA { get; set; }
        public string CDESPLIEGUE { get; set; }
        public string CCLAVEINT { get; set; }
        public string CCLAVESAT { get; set; }
    }
}
