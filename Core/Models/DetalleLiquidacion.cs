﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
namespace CoreFletera.Models
{
    public class DetalleLiquidacion_old
    {
        private bool _liquidar = true;
        public bool liquidar
        {
            get { return _liquidar; }
            set { _liquidar = value; }
        }
        public int idDetalleLiquidacion { get; set; }
        public Viaje viaje { get; set; }
        public int? folioViaje
        {
            get
            {
                if (viaje == null)
                {
                    return null;
                }
                else
                {
                    return viaje.NumGuiaId;
                }
            }
        }
        public PagoPorModalidad pagoPorModalidad { get; set; }

        public string mod
        {
            get
            {
                if (pagoPorModalidad == null)
                {
                    return "";
                }
                return pagoPorModalidad.modalidad;
            }
        }
        private decimal? _precio { get; set; }
        public decimal precio
        {
            get
            {
                if (_precio != null)
                {
                    return (decimal)_precio;
                }
                else
                {
                    switch (tipoRuta)
                    {
                        case eModalidadRutas.SENCILLO_24_TON:
                            //return pagoPorModalidad.sencillo_24;
                            return viaje.listDetalles[0].zonaSelect.sencillo_24;
                        case eModalidadRutas.SENCILLO_30_TON:
                            //return pagoPorModalidad.sencillo_30;
                            return viaje.listDetalles[0].zonaSelect.sencillo_30;
                        case eModalidadRutas.SENCILLO_35_TON:
                            //return pagoPorModalidad.sencillo_35;
                            return viaje.listDetalles[0].zonaSelect.sencillo_35;
                        case eModalidadRutas.FULL:
                            //return pagoPorModalidad.full;
                            return viaje.listDetalles[0].zonaSelect.full;
                        default:
                            return 0;
                    }
                }
            }
            set
            {
                _precio = value;
            }
        }
        public eModalidadRutas tipoRuta
        {
            get
            {
                return getTipoRuta(viaje);
            }
        }

        public decimal viatico { get; set; }

        private bool? _doble { get; set; }
        public bool doble
        {
            get
            {
                if (_doble != null)
                {
                    return (bool)_doble;
                }
                else
                {
                    return isDoble(viaje);
                }
            }
            set
            {
                _doble = value;
            }
        }

        public string ruta
        {
            get
            {
                return viaje.listDetalles[0].zonaSelect.descripcion;
            }
        }

        private decimal? _importe { get; set; }
        public decimal importe
        {
            get
            {
                if (_importe != null)
                {
                    return (decimal)_importe;
                }
                return (doble ? (precio * 2) : precio) + viatico;
            }
        }

        public ConfiguracionClienteLiquidacion config { get; set; }
        private bool isDoble(Viaje viaje)
        {
            if (config == null)
            {
                return false;
            }

            //bool doble = false;

            int dx = (int)config.diaInicio;
            int dy = (int)config.diaFin;
            int dia = (int)viaje.FechaHoraViaje.DayOfWeek;
            TimeSpan hora = viaje.FechaHoraViaje.TimeOfDay;
            TimeSpan hx = new TimeSpan();
            TimeSpan hy = new TimeSpan();
            TimeSpan.TryParse(config.horaInicio, out hx);
            TimeSpan.TryParse(config.horaFin, out hy);

            dia = dia == 0 ? 7 : dia;

            if (dx <= dy) //caso 1
            {
                if (dia >= dx && dia <= dy)
                {
                    if (dx == dy)
                    {
                        return (hora >= hx && hora <= hy);
                    }
                    else if (dia == dx)
                    {
                        return (hora > hx);
                    }
                    else if (dia == dy)
                    {
                        return (hora < hy);
                    }
                    else
                    {
                        return true;
                    }
                }
                else
                    return false;
            }
            else if (dx > dy) //caso 2
            {
                if (dia == dx)
                {
                    return (hora > hx);
                }
                else if (dia == dy)
                {
                    return (hora < hy);
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
            //return doble;
        }

        private eModalidadRutas getTipoRuta(Viaje viaje)
        {
            if (viaje.remolque2 != null)
            {
                return eModalidadRutas.FULL;
            }
            if (viaje.remolque.tipoUnidad.TonelajeMax <= 24)
            {
                return eModalidadRutas.SENCILLO_24_TON;
            }
            if (viaje.remolque.tipoUnidad.TonelajeMax <= 30)
            {
                return eModalidadRutas.SENCILLO_30_TON;
            }
            if (viaje.remolque.tipoUnidad.TonelajeMax <= 35)
            {
                return eModalidadRutas.SENCILLO_35_TON;
            }
            else
                return eModalidadRutas.NINGUNO;
        }
    }


}
