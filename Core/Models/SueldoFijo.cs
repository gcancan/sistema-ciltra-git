﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
namespace CoreFletera.Models
{
    public class SueldoFijo
    {
        public int idSueldoFijo { get; set; }
        public Personal operador { get; set; }
        public decimal importe { get; set; }
        public string autoriza { get; set; }
        public bool estatus { get; set; }
    }
}
