﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
namespace CoreFletera.Models
{
    public class Mantenimiento
    {
        public int idOrdenSer { get; set; }
        public Empresa empresa { get; set; }
        public UnidadTransporte unidadTransporte { get; set; }
        public TipoOrdenServ tipoOrdenServ { get; set; }
        public TipoServicio tipoServicio { get; set; }
        public DateTime fechaRec { get; set; }
        public DateTime fechaAsig { get; set; }
        public DateTime fechaTer { get; set; }
        public bool externo { get; set; }
        public List<DetalleMantenimiento> listaActividades { get; set; }
    }

    public class TipoServicio
    {
        public int idTipoServicio { get; set; }
        public string nombreServicio { get; set; }
    }

    public class DetalleMantenimiento
    {
        public int idOrdenSer { get; set; }
        public int idOrdenTrabajo { get; set; }
        public int idOrdenActividad { get; set; }
        public string idUnidadTransporte { get; set; }
        public int idActividad { get; set; }
        public string actividad { get; set; }
        public DateTime fechaTer { get; set; }
        public string responsable { get; set; }
        public string aux1 { get; set; }
        public string aux2 { get; set; }
        public decimal costoManoObra { get; set; }
        public List<InsumosOrdenTrabajo> listaInsumos { get; set; }
    }

    public class InsumosOrdenTrabajo
    {
        public int idPreSalida { get; set; }
        public int idOrdenSer { get; set; }
        public string idUnidadTransporte { get; set; }
        public int idOrdenTrabajo { get; set; }
        public DateTime fechaSolicitud { get; set; }
        public DateTime? fechaEntrega { get; set; }
        public int idProducto { get; set; }
        public string nombreProducto { get; set; }
        public decimal cantidad { get; set; }
        public string estatus { get; set; }
        public decimal sumaGlobal { get; set; }
        public string encabezado
        {
            get
            {
                return string.Format("{0} {1}", nombreProducto, sumaGlobal.ToString("N2"));
            }
        }
    }
}
