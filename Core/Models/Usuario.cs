﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoreFletera.Models;
namespace Core.Models
{
    public class Usuario
    {
        public int idUsuario { get; set; }
        public string nombreUsuario { get; set; }
        public string contrasena { get; set; }
        public Personal personal { get; set; }
            private Empresa _empresa { get; set; }
        public Empresa empresa
        { get
            {
                if (_empresa == null)
                {
                    return personal.empresa;
                }
                else
                {
                    return _empresa;
                }
            }
            set
            {
                _empresa = value;
            }
        }
        public Cliente cliente { get; set; }
        public List<Privilegio> listPrivilegios { get; set; }
        public ZonaOperativa zonaOperativa { get; set; }
        public bool activo { get; set; }
        public string strActivo =>
            activo ? "ACTIVO" : "BAJA";
    }
}
