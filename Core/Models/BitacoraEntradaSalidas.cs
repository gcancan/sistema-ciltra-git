﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class BitacoraEntradaSalidas
    {
        public int idBitacora { get; set; }
        public int folio { get; set; }
        public DateTime fechaEntrada { get; set; }
        public DateTime ? fechaSalida { get; set; }
        public string unidad { get; set; }
        public string chofer { get; set; }
        public string guardia { get; set; }
        public bool isOrden { get; set; }

        public List<string> listMotivos { get; set; }
    }
}
