﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class BitacoraAcceso
    {
        public int idBitacoraAcceso { get; set; }
        public DateTime fecha { get; set; }
        public string acceso { get; set; }
        public string usuario { get; set; }
        public string contraseña { get; set; }
        public string nombreEquipo { get; set; }
    }
}
