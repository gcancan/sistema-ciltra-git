﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class MesContable
    {
        public int idMesContable { get; set; }
        public int idEmpresa { get; set; }
        public int mes { get; set; }
        public int anio { get; set; }
        public DateTime fechaInicio { get; set; }
        public DateTime ? fechaFin { get; set; }
    }
}
