﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class ReporteCargasDiesel
    {
        public string proveedorExtra { get; set; }
        public int vale { get; set; }
        public int idEmpresa { get; set; }
        public string empresa { get; set; }
        public DateTime fecha { get; set; }
        public string fechaCarga => fecha.ToString("dd/MM/yy");
        public string horaCarga => fecha.ToString("HH:mm");
        public string operación { get; set; }
        public string unidad { get; set; }
        public decimal litros { get; set; }
        public decimal litrosCargados => litros + litrosExtra;
        public decimal relleno => litrosCargados - litrosECM;
        public string rem1 { get; set; }
        public string dolly { get; set; }
        public string rem2 { get; set; }
        public decimal capacidad { get; set; }
        public int idOperador { get; set; }
        public string operador { get; set; }
        public decimal kmInicial
        {
            get
            {
                if (fechaUltimaInsersion == null)
                {
                    return kmCarga;
                }
                if (fechaUltimaInsersion > fechaUltimaCarga)
                {
                    return kmInsercion;
                }
                return kmCarga;
            }
        }
        public decimal hrInicial { get; set; }
        public decimal kmFinal { get; set; }
        public decimal hrFinal { get; set; }
        public DateTime fechaUltimaCarga { get; set; }
        public DateTime? fechaUltimaInsersion { get; set; }
        public decimal kmCarga { get; set; }
        public decimal kmInsercion { get; set; }
        public decimal tripFisico => kmFinal - kmInicial;
        public decimal hrOperativas => hrFinal - hrInicial;
        public decimal rendimientoLHr => isCR ? (hrOperativas <= 0 ? 0 : litrosCargados / hrOperativas) : 0;
        public decimal tripECM { get; set; }
        public decimal rendimientoECM => isCR ? (litrosECM <= 0 ? 0 : litrosECM / tripECM) : (litrosECM <= 0 ? 0 : tripECM / litrosECM);
        public decimal rendiemientoFisico => isCR ? (tripFisico <= 0 ? 0 : (litrosCargados / tripFisico)) : (tripFisico / litrosCargados);
        public decimal rendimientoManejo
        {
            get
            {
                try
                {
                    return isCR ? 0 : (litrosECM - litrosPTO - litrosEST) <= 0 ? 0 : tripFisico / (litrosECM - litrosPTO - litrosEST);
                }
                catch (Exception)
                {
                    return 0;
                }
            }
        }

        public string factura { get; set; }
        public string facturaProveedor { get; set; }
        public decimal litrosECM { get; set; }
        public decimal litrosPTO { get; set; }
        public decimal litrosEST { get; set; }
        public string despachador { get; set; }
        public decimal precio { get; set; }
        public decimal precio2 { get; set; }
        private decimal _varIEPS { get; set; }
        public decimal varIEPS
        {
            get
            {
                if (_varIEPS <= 0)
                {
                    return .3521m;
                }
                return _varIEPS;
            }
            set
            {
                _varIEPS = value;
            }
        }
        public decimal IEPS => importe > 0 ? litrosCargados * varIEPS : 0;
        public decimal subTotal => importe > 0 ? (importe - IEPS) / 1.16m : 0;
        public decimal iva => importe > 0 ? subTotal * .16m : 0;
        public decimal subTotalConIEPS => importe > 0 ? importe - iva : 0;
        public decimal importe { get; set; }
        public decimal importeLocal => litros * precio;
        public decimal IEPSLocal => importeLocal > 0 ? (litros * varIEPS) : 0;
        public decimal importeSinIEPSLocal => importeLocal - IEPSLocal;
        public decimal presioSinIEPSLocal => importeSinIEPSLocal / litros;
        public decimal presioSinIVALocal => (importeSinIEPSLocal / 1.16m) / litros;
        public decimal litrosExtra { get; set; }
        public decimal importeExtra => precio2 * litrosExtra;
        public decimal IEPSExtra => importeExtra > 0 ? (litrosExtra * varIEPS) : 0;
        public decimal importeSinIEPSExtra => importeExtra - IEPSExtra;
        public decimal precioSinIEPSExtra => importeExtra <= 0 ? 0 : importeSinIEPSExtra / litrosExtra;
        public decimal presioSinIVAExtra => importeExtra <= 0 ? 0 : (importeSinIEPSExtra / 1.16m) / litrosExtra;
        public string ticket { get; set; }
        public string folioImpreso { get; set; }
        public int idOrdenServ { get; set; }
        public int idProveedor { get; set; }
        public string proveedor { get; set; }
        public string viajes { get; set; }
        public DateTime? fechaConciliacion { get; set; }
        public string usuarioConcilio { get; set; }
        public string tipoCarga { get; set; }
        public string tipoCargaExtra { get; set; }
        public bool isCR { get; set; }
    }
}
