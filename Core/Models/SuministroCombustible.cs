﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class SuministroCombustible
    {
        public int idSuministroCombustible { get; set; }
        public TanqueCombustible tanqueCombustible { get; set; }
        public DateTime fecha { get; set; }
        public decimal ltsCombustible { get; set; }
        public Usuario usuario { get; set; }
        public List<DetalleSuminstroCombustible> listaDetalles { get; set; }
        public string factura { get; set; }
        public decimal importe { get; set; }
        public string noPipa { get; set; }
        public string placasPipa { get; set; }
        public string operador { get; set; }
        public Personal PersonalRecibe { get; set; }
        public DateTime fechaRecibe { get; set; }
    }
    public class DetalleSuminstroCombustible
    {
        public int idDetalleSuministroCombustible { get; set; }
        public int idSuministroCombustible { get; set; }
        public Empresa empresa { get; set; }
        public decimal ltsCombustible { get; set; }
    }
}
