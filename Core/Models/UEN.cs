﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class UEN
    {
        public int idUEN { get; set; }
        public string nombre { get; set; }
        public bool activo { get; set; }
        public int cveINTELISIS { get; set; }
        public string estatus => activo ? "ALTA" : "BAJA";
    }
}
