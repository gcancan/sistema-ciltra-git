﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class DescripcionEvaluacion
    {
        public int idDescripcionEvaluacion { get; set; }
        public FactorCalificacion factorCalificacion { get; set; }
        public string descripcion { get; set; }
        public decimal porcentaje { get; set; }
        public bool activo { get; set; }
    }
    public class FactorCalificacion
    {
        public int idFactorCalificacion { get; set; }
        public string factorCalificacion { get; set; }
        public bool activo { get; set; }
    }
}
