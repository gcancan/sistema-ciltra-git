﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class TipoGranja
    {
        public int idTipoGranja { get; set; }
        public string tipoGranja { get; set; }
        public bool activo { get; set; }
        public string activoStr => activo ? "ACTIVO" : "BAJA";
    }
}
