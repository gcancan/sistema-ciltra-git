﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class CumpleañosPersonal
    {
        public int idPersonal { get; set; }
        public string nombre { get; set; }
        public string empresa { get; set; }
        public string departamento { get; set; }
        public string puesto { get; set; }
        public DateTime fechaNacimiento { get; set; }
        public int dia { get; set; }
        public int años { get; set; }
    }
}
