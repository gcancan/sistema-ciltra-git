﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    [Serializable]
    public class TipoUnidad
    {
        public int clave { get; set; }
        public string cveINTELISIS { get; set; }
        public string descripcion { get; set; }
        public string NoLlantas { get; set; }
        public string NoEjes { get; set; }
        public decimal TonelajeMax { get; set; }
        public string clasificacion { get; set; }
        public bool bCombustible { get; set; }
        public List<DescripcionLlantas> listaDescripcionLlantas { get; set; }
        public int cantRefacciones { get; set; }
        public bool activo { get; set; }
        public string activoStr => activo ? "ACTIVO" : "BAJA";
        public string conbustibleStr => bCombustible ? "SI" : "NO";
    }
    public class DescripcionLlantas
    {
        public int idDescripcionLlantas { get; set; }
        public int idTipoUnidad { get; set; }
        public int noEje { get; set; }
        public enumTipoEjeLlanta tipoEjeLlanta { get; set; }
        public int cantLlantas { get; set; }
    }
    public enum enumTipoEjeLlanta
    {
        DIRECCION = 0,
        TRACCION = 1,
        ARRASTRE = 2
    }
    public enum enumClasificacionTipoUnidad
    {
        TRACTOR = 0,
        DOLLY = 1,
        REMOLQUE = 2,
        OTRO = 3
    }
}
