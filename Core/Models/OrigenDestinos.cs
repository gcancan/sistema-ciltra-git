﻿namespace Core.Models
{
    using CoreFletera.Models;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class OrigenDestinos
    {       
        public int idOrigenDestino { get; set; }

        public Origen origen { get; set; }

        public Zona destino { get; set; }

        public decimal precioSencillo { get; set; }

        public decimal precioFull { get; set; }

        public decimal km { get; set; }

        public bool viaje { get; set; }

        public int idCliente { get; set; }

        public decimal viajeSencillo { get; set; }

        public decimal viajeFull { get; set; }

        public bool incluyeCaseta { get; set; }

        public decimal precioCasetas { get; set; }

        public ClasificacionPagoChofer tipoPago { get; set; }

        public ClasificacionProductos clasificacion { get; set; }
    }
}
