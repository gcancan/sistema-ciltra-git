﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class RelacionDetalleOrdenFactura
    {
        public OrdenCompra ordenCompra { get; set; }
        public DetalleOrdenCompra detalleCompra { get; set; }
        public int cantidad { get; set; }
    }
}
