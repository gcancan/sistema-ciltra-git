﻿namespace CoreFletera.Models
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class Paradas
    {   
        public CoreFletera.Models.Respuesta Respuesta { get; set; }
        public CoreFletera.Models.RespuestaJson RespuestaJson { get; set; }
        public List<CoreFletera.Models.Equipos> Equipos { get; set; }
    }
    public class PNA
    {
        public bool Valido { get; set; }
        public string Mensaje { get; set; }
        public int CodEstado { get; set; }
        public List<Vehiculos> Vehiculos { get; set; }
    }
    public class Vehiculos
    {
        public string Alias { get; set; }
        public DateTime? FechaInicial { get; set; }
        public DateTime? FechaFinal { get; set; }
        public TimeSpan? DuracionTotalPNA { get; set; }
        public List<Detalles> Detalles { get; set; }
        

    }
    public class Detalles
    {
        public TimeSpan DuracionPNA { get; set; }
        public DateTime FechaFinal { get; set; }
        public DateTime FechaInicial { get; set; }
        public decimal Latitud { get; set; }
        //public decimal LatitudFinal { get; set; }
        public decimal Longitud { get; set; }
        //public decimal LongitudFinal { get; set; }
        public string GeoCerca { get; set; }
        public string CategoriaGeocerca { get; set; }
        public string Estado { get; set; }
    }
}
