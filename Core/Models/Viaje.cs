﻿namespace Core.Models
{
    using CoreFletera.Models;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class Viaje : ICloneable
    {        
        public object Clone() =>
            base.MemberwiseClone();
        public string cveUsuario { get; set; }
        public int NumGuiaId { get; set; }
        public string SerieGuia { get; set; }
        public int NumViaje { get; set; }
        public Cliente cliente { get; set; }
        public int IdEmpresa { get; set; }
        public DateTime FechaHoraViaje { get; set; }
        public DateTime? FechaInicioCarga { get; set; }
        public DateTime? FechaSalida { get; set; }
        public DateTime? FechaPrograma { get; set; }
        public DateTime? fechaBachoco { get; set; }
        public Personal operador { get; set; }
        public Personal operadorCapacitacion { get; set; }
        public UnidadTransporte tractor { get; set; }
        public UnidadTransporte remolque { get; set; }
        public UnidadTransporte dolly { get; set; }
        public UnidadTransporte remolque2 { get; set; }
        public DateTime? FechaHoraFin { get; set; }
        public string EstatusGuia { get; set; }
        public int UserViaje { get; set; }
        public int? UserFinViaje { get; set; }
        public char tipoViaje { get; set; }
        public List<CartaPorte> listDetalles { get; set; }
        public bool isBachoco { get; set; }
        public decimal tara { get; set; }
        public decimal tara2 { get; set; }
        public decimal km { get; set; }
        public int? idUsuarioCheco { get; set; }
        public DateTime fechaChecado { get; set; }
        public bool isExterno { get; set; }
        public decimal importe
        {
            get
            {
                if (this.listDetalles.Count > 0)
                {
                    decimal num = 0M;
                    foreach (CartaPorte porte in this.listDetalles)
                    {
                        num += porte.importeReal;
                    }
                    return num;
                }
                return decimal.Zero;
            }
        }
        public decimal casetas
        {
            get
            {
                if (this.listDetalles.Count > 0)
                {
                    decimal num = 0M;
                    foreach (CartaPorte porte in this.listDetalles)
                    {
                        num += porte.precioCaseta;
                    }
                    return num;
                }
                return decimal.Zero;
            }
        }
        public string usuarioCambia { get; set; }
        public bool viajeFalso { get; set; }
        public bool cobroXKm { get; set; }
        public Personal ayudante { get; set; }
        public Personal OperadorFinViaje { get; set; }
        public bool noAjustar { get; set; }
        public bool taller { get; set; }
        public bool tallerExterno { get; set; }
        public int? imputable { get; set; }
        public ZonaOperativa zonaOperativa { get; set; }
        public int noViaje { get; set; }
        public bool editoNoViaje { get; set; }
        public string obsViajeFalso { get; set; }
        public EstatusProducto estatusProducto { get; set; }
        public string motivoViajeFalso { get; set; }
        public int? idMotivoViajeFalso { get; set; }
        public List<Anticipo> listaAnticipos { get; set; }
        public decimal pagoOperadores { get; set; }
        public decimal pagoCasetas { get; set; }
        public List<AnticipoMaster> listaAnticiposMaster { get; set; }
    }
    public enum EstatusProducto
    {
        NINGUNO = 0,
        REPROCESO = 1,
        REDESTINO = 2,
        REDESTINO_MISMA_GRANJA= 3
    }
}
