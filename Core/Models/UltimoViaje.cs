﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class UltimoViaje
    {
        public string idRemolque { get; set; }
        public string idDolly { get; set; }
        public string idRemolque2 { get; set; }
        public int idOperador { get; set; }
    }
}
