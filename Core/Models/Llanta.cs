﻿namespace CoreFletera.Models
{
    using Core.Models;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class Llanta
    {     
        public string getUbicacion()
        {
            if (!string.IsNullOrEmpty(this.Lugar))
            {
                switch (this.Lugar)
                {
                    case "UNIDAD TRANSPORTE":
                        {
                            object[] objArray1 = new object[] { this.unidadTransporte.tipoUnidad.descripcion, " ", this.unidadTransporte.clave, " POS ", this.posicion };
                            return string.Concat(objArray1);
                        }
                    case "ALMACEN":
                        return this.almacen.nombreAlmacen;

                    case "PROVEEDOR":
                        return this.proveedor.razonSocial;

                    case "PILA DESHECHOS":
                        return this.almacen.nombreAlmacen;
                }
            }
            return "";
        }

        public int idLlanta { get; set; }

        public string clave { get; set; }

        public string noSerie { get; set; }

        public int idEmpresa { get; set; }

        public int DOT { get; set; }

        public string diseño { get; set; }

        public string medida { get; set; }

        public decimal profundidad { get; set; }

        public decimal km { get; set; }

        public string factura { get; set; }

        public DateTime fechaFactura { get; set; }

        public DateTime fecha { get; set; }

        public decimal costo { get; set; }

        public string rfid { get; set; }

        public int posicion { get; set; }

        public int año { get; set; }

        public int mes { get; set; }

        public string estatus { get; set; }

        public string observaciones { get; set; }

        public Marca marca { get; set; }

        public EnumTipoUbicacionLlanta TipoUbicacionLlanta { get; set; }

        public TipoLlanta tipoLlanta { get; set; }

        public UnidadTransporte unidadTransporte { get; set; }

        public Almacen almacen { get; set; }

        public Proveedor proveedor { get; set; }

        public LLantasProducto productoLLanta { get; set; }

        public string Lugar { get; set; }

        public Diseño objDiseño { get; set; }

        public bool revitalizada { get; set; }

        public CondicionLlanta condicionLlanta { get; set; }

        public int idDetalleCompra { get; set; }

        public decimal profundidadActual { get; set; }
    }
}
