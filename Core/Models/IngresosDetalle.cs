﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class IngresosDetalle
    {
        public DateTime fecha { get; set; }
        public string serie { get; set; }
        public int movimiento { get; set; }
        public string cliente { get; set; }
        public decimal cantidad { get; set; }
        public string um { get; set; }
        public decimal importe { get; set; }
        public string concepto { get; set; }
    }
}
