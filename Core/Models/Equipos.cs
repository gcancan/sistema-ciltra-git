﻿namespace CoreFletera.Models
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class Equipos
    {        
        public string alias { get; set; }

        public string serie { get; set; }

        public List<ParadaEquipo> paradas { get; set; }
    }
}
