﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class PrivilegioUsuario
    {
        public int idPrivilegioUsuario { get; set; }
        public Privilegio privilegio { get; set; }
        public Usuario usuario { get; set; }
    }
}
