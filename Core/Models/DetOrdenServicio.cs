﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
namespace CoreFletera.Models
{
    public class DetOrdenServicio
    {
        public int idOrdenServicio { get; set; }
        public int idActividadPendiente { get; set; }
        public int idOrdenActividad { get; set; }
        public int idOrdenTrabajo { get; set; }
        public UnidadTransporte unidad { get; set; }
        public decimal duracion
        {
            get
            {
                if (_duracion == null)
                {
                    return _duracion.Value;
                }
                if (actividad != null)
                {
                    return actividad.duracion;
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                _duracion = value;
            }
        }
        public decimal? _duracion { get; set; }
        public decimal costoActividad
        {
            get
            {
                if (actividad != null)
                {
                    return actividad.costo;
                }
                else
                {
                    return 0;
                }
            }
        }
        public TipoOrden tipoOrden { get; set; }
        public enumTipoOrden enumTipoOrden { get; set; }
        public enumTipoOrdServ enumTipoOrdServ { get; set; }
        public enumTipoServicio enumTipoServicio { get; set; }
        public TipoOrdenServicio tipoOrdenServicio { get; set; }
        public Servicio servicio { get; set; }
        public Actividad actividad { get; set; }
        public string descripcion { get; set; }
        public string tipoOrdenTrabajo
        {
            get
            {
                if (actividad != null)
                {
                    return "ACTIVIDAD";
                }
                if (servicio != null)
                {
                    return "SERVICIO";
                }
                return "FALLA REPORTADA";
            }
        }
        public Division division { get; set; }
        public Llanta llanta { get; set; }
        private int? _pocision { get; set; }
        public int pocision
        {
            get
            {
                if (_pocision != null)
                {
                    return _pocision.Value;
                }
                if (llanta == null)
                {
                    return 0;
                }
                return llanta.posicion;
            }
            set
            {
                _pocision = value;
            }
        }
        public string ordenTrabajo
        {
            get
            {
                if (actividad != null)
                {
                    return actividad.nombre;
                }
                else if (servicio != null)
                {
                    return servicio.nombre;
                }
                else
                {
                    return descripcion;
                }
            }
        }
        private string _estatus { get; set; }
        public string estatus
        {
            get
            {
                if (string.IsNullOrEmpty(_estatus))
                {
                    return tipoOrdenServicio.estatus;
                }
                else
                {
                    return _estatus;
                }
            }
            set
            {
                _estatus = value;
            }
        }
        public Personal responsable { get; set; }
        public Personal auxiliar1 { get; set; }
        public Personal auxiliar2 { get; set; }
        public List<Actividad> listaActividades { get; set; }
        public OrdenTaller ordenTaller { get; set; }
        public string descripcionDetalle
        {
            get
            {
                if (servicio != null)
                {
                    return servicio.descipcion;
                }
                if (actividad != null)
                {
                    return actividad.nombre;
                }
                return descripcion;
            }
        }
        public string descripcionDetallePendientes
        {
            get
            {
                if (servicio != null)
                {
                    return servicio.descipcion + (idActividadPendiente!= 0 ? "(SERVICIO PENDIENTE)" : String.Empty);
                }
                if (actividad != null)
                {
                    return actividad.nombre + (idActividadPendiente != 0 ? "(ACTIVIDAD PENDIENTE)" : String.Empty);
                }
                return descripcion + (idActividadPendiente != 0 ? "(ACTIVIDAD PENDIENTE)" : String.Empty);
            }
        }
        public DateTime? fechaDiagnostico { get; set; }
        public string usuarioDiagnostico { get; set; }
        public DateTime? fechaAsignacion { get; set; }
        public string usuarioAsignacion { get; set; }
        public DateTime? fechaPausado { get; set; }
        public string notaPausa { get; set; }
        public DateTime? fechaTerminado { get; set; }
        public string usuarioTerminado { get; set; }
        public int idPadreOrdSer { get; set; }
        public string notaDiagnostico { get; set; }
        public CargaCombustibleExtra cargaCombustibleExtra { get; set; }
    }
}
