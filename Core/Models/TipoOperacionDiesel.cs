﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public enum TipoOperacionDiesel
    {
        AJUSTE = 0,
        CARGA = 1,
        ROSEO = 2,
        ANTICIPO = 3,
        SUMINISTRO = 4,
        POR_CIERRE_NIVEL = 5
    }
}
