﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace CoreFletera.Models
{
    public class Liquidacion
    {
        public int idLiquidacion { get; set; }
        public DateTime fecha { get; set; }
        public DateTime fechaInicio { get; set; }
        public DateTime fechaFin { get; set; }
        public string usuarioInicio { get; set; }
        public string usuarioFin { get; set; }
        public List<DetalleLiquidacion> listaDetalleLiquidacion { get; set; }
        public List<DetallePersonaLiquidacion> listaDetallePersonalLiquidacion { get; set; }
        private decimal? _total { get; set; }
        public decimal? total
        {
            get
            {
                if (_total == null)
                {
                    return listaDetallePersonalLiquidacion.Sum(s => s.totalPago);
                }
                return _total.Value;
            }
            set
            {
                _total = value;
            }
        }
        public bool liquidado { get; set; }
        public bool activo { get; set; }
        public string strDeptos
        {
            get
            {
                string str = string.Empty;
                if (listaDetalleLiquidacion == null)
                {
                    return str;
                }
                
                foreach (var item in listaDetalleLiquidacion)
                {
                    str += item.departamento.descripcion + ", ";
                }
                return str.Trim().Trim(',');
            }
        }
        public string strDeptosEnter
        {
            get
            {
                string str = string.Empty;
                if (listaDetalleLiquidacion == null)
                {
                    return str;
                }

                foreach (var item in listaDetalleLiquidacion)
                {
                    str += item.departamento.descripcion + Environment.NewLine;
                }
                return str;
            }
        }
    }
    public class DetalleLiquidacion
    {
        public int idDetalleLiquidacion { get; set; }
        public int idLiquidacion { get; set; }
        public Departamento departamento { get; set; }
    }
    public class ViajesOperador
    {
        public int folioViaje { get; set; }
        public decimal pagoViaje { get; set; }
    }
    public class DetallePersonaLiquidacion : INotifyPropertyChanged
    {
        public int idDetallePersonaLiquidacion { get; set; }
        public int idLiquidacion { get; set; }
        public Personal personal { get; set; }
        public string mombreDpto =>
            personal.departamento.descripcion;
        public bool activo
        {
            get
            {
                return _activo == null ? true : _activo.Value;
            }
            set
            {
                _activo = value;
            }
        }
        private bool? _activo { get; set; }
        private List<ConceptoLiquidacion> _listaConceptoLiquidacion { get; set; }
        public List<ConceptoLiquidacion> listaConceptoLiquidacion { get; set; }
        private decimal? _totalPago { get; set; }
        public decimal totalDescuento =>
                        listaConceptoLiquidacion.Where(s => s.tipoConceptoLiquidacion == TipoConceptoLiquidacion.DESCUENTO).Sum(s => s.total);
        public decimal totalExtras =>
                        listaConceptoLiquidacion.Where(s => s.tipoConceptoLiquidacion == TipoConceptoLiquidacion.EXTRAS).Sum(s => s.total);
        public decimal totalHorasExtras =>
                        listaConceptoLiquidacion.Where(s => s.tipoConceptoLiquidacion == TipoConceptoLiquidacion.HORAS_EXTRAS).Sum(s => s.total);
        public decimal totalViajesBalanceados => listaConceptoLiquidacion.Where(
                        s => s.tipoConceptoLiquidacion == TipoConceptoLiquidacion.VIAJES_ALIMENTOS_BALANCEADOS ).Sum(s=> s.total);
        public decimal totalViajes => totalViajesBalanceados + totalViajesSecos;

        public decimal totalViajesSecos => listaConceptoLiquidacion.Where(
                        s=> s.tipoConceptoLiquidacion == TipoConceptoLiquidacion.VIAJES_SECOS_REFRIGERADOS).Sum(s => s.total);
        public decimal totalViajesAtlante => listaConceptoLiquidacion.Where(
                        s => s.tipoConceptoLiquidacion == TipoConceptoLiquidacion.VIAJES_ATLANTE).Sum(s => s.total);
        public decimal sumaTotalViajes => totalViajesBalanceados + totalViajesSecos + totalViajesAtlante;
        public decimal totalPagoFijo =>
                        listaConceptoLiquidacion.Where(s => s.tipoConceptoLiquidacion == TipoConceptoLiquidacion.PAGO_FIJO).Sum(s => s.total);
        public decimal totalPagoApoyo=>
                        listaConceptoLiquidacion.Where(s => s.tipoConceptoLiquidacion == TipoConceptoLiquidacion.PAGO_APOYO).Sum(s => s.total);
        public bool sePagaViaje =>
                        sumaTotalViajes > (pagoAp);
        private decimal pagoAp => totalPagoApoyo >= totalPagoFijo ? totalPagoApoyo : totalPagoFijo;
        public decimal sumaPAgo
        {
            get
            {
                try
                {
                    if (_totalPago != null)
                    {
                        return _totalPago.Value;
                    }
                    decimal sumaPositivo;

                    sumaPositivo = sePagaViaje ? sumaTotalViajes : (pagoAp);

                    return (sumaPositivo + totalExtras + totalHorasExtras);
                }
                catch (Exception)
                {
                    return 0;
                }
                finally
                {
                    //OnPropertyChanged("totalPago");
                }
            }
        }
        public decimal totalPago
        {
            get
            {
                try
                {
                    if (_totalPago != null)
                    {
                        return _totalPago.Value;
                    }
                    decimal sumaPositivo;

                    

                    sumaPositivo = sePagaViaje ? sumaTotalViajes : (pagoAp);
                    
                    return (sumaPositivo + totalExtras+ totalHorasExtras) - totalDescuento;
                }
                catch (Exception)
                {

                    return 0;
                }
                finally
                {
                    //OnPropertyChanged("totalPago");
                }
            }
            set
            {
                _totalPago = value;
                OnPropertyChanged();
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string name = "")
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(name));
        }
    }
    public class ConceptoLiquidacion : INotifyPropertyChanged
    {
        private DetallePersonaLiquidacion _detalleHeader { get; set; }
        public DetallePersonaLiquidacion detalleHeader
        {
            get
            {
                if (_detalleHeader != null)
                {
                    return _detalleHeader;
                }
                return new DetallePersonaLiquidacion();
            }
            set
            {
                _detalleHeader = value;
                OnPropertyChanged("detalleHeader");
                OnPropertyChanged("total");                
                OnPropertyChangedHeader("totalPago");
            }
        }
        public List<DateTime> listaDiasInhabil { get; set; }
        public int idConceptoLiquidacion { get; set; }
        public int idLiquidacion { get; set; }
        public int idDetallePersonaLiquidacion { get; set; }
        public TipoConceptoLiquidacion tipoConceptoLiquidacion { get; set; }
        public bool suma { get; set; }
        public int? idCliente { get; set; }
        public int? folioViaje { get; set; }
        public int? cartaPorte { get; set; }
        public int? noViaje { get; set; }
        public string tractor { get; set; }
        public string modalidad { get; set; }
        public string origenes { get; set; }
        public string destinos { get; set; }
        public string remision { get; set; }
        public int? noDestinos { get; set; }
        public int? noViajeOperador { get; set; }
        public DateTime? fechaInicial { get; set; }
        public DateTime? fechaFinal { get; set; }
        public DateTime? fechaCarga { get; set; }
        public DateTime? fechaDescarga { get; set; }
        private decimal? _cantidad { get; set; }
        public decimal? cantidad
        {
            get
            {
                return _cantidad;
            }
            set
            {
                _cantidad = value;
                OnPropertyChanged();
                OnPropertyChanged("total");
                OnPropertyChangedHeader("totalPago");
            }
        }
        public bool doble { get; set; }
        public List<TipoIncidencia> listaTiposIndicencias { get; set; }        
        public List<MotivoDescuentos> listaMotivosDescuento { get; set; }
        private MotivosDoble? _motivosDoble { get; set; }
        private bool validarInhabil(DateTime? date = null)
        {
            DateTime? fecha = date == null ? fechaInicial : date;
            return  listaDiasInhabil.Exists(s => s.Day == fecha.Value.Day && s.Month == fecha.Value.Month && s.Year == fecha.Value.Year);
        }
        
        public MotivosDoble motivosDoble
        {
            get
            {
                if (_motivosDoble != null)
                {
                    return _motivosDoble.Value;
                }
                if (tipoConceptoLiquidacion == TipoConceptoLiquidacion.VIAJES_ALIMENTOS_BALANCEADOS)
                {
                    if (validarInhabil()) return MotivosDoble.INHABIL_DE_LEY;

                    if (idCliente == 1)
                    {
                        if (fechaInicial.Value.DayOfWeek == DayOfWeek.Saturday)
                        {
                            if (fechaInicial.Value.Hour >= 12)
                            {
                                return MotivosDoble.FIN_DE_SEMANA;
                            }
                        }
                        if (fechaInicial.Value.DayOfWeek == DayOfWeek.Sunday)
                        {
                            if (fechaInicial.Value.Hour < 22)
                            {
                                return MotivosDoble.FIN_DE_SEMANA;
                            }
                            if (fechaInicial.Value.Hour >= 22)
                            {
                                if (validarInhabil(fechaInicial.Value.AddDays(1)))
                                {
                                    return MotivosDoble.INHABIL_DE_LEY;
                                }
                            }

                        }
                    }
                    if (idCliente == 3)
                    {
                        if (validarInhabil()) return MotivosDoble.INHABIL_DE_LEY;

                        if (noDestinos >= 3)
                        {
                            return MotivosDoble.TRES_GRANJAS;
                        }
                       // if (noViajeOperador >= 5)
                       // {
                       //    return MotivosDoble.CINCO_VIAJES;
                       // }
                    }
                }
                return MotivosDoble.NINGUNO;
            }
            set
            {
                _motivosDoble = value;
                OnPropertyChanged();
            }
        }
        public string strMotivoDoble
        {
            get
            {
                switch (motivosDoble)
                {
                    case MotivosDoble.NINGUNO:
                        return string.Empty;                    
                    default:
                        return motivosDoble.ToString().Replace('_', ' ');
                }
            }
        }
        public decimal cantidadDoble => doble ? cantidad.Value : 0;
        public decimal total
        {
            get
            {
                try
                {   
                    if (tipoConceptoLiquidacion == TipoConceptoLiquidacion.VIAJES_ALIMENTOS_BALANCEADOS)
                    {
                        if (motivosDoble != MotivosDoble.NINGUNO)
                        {
                            return cantidad.Value * 2;
                        }
                    }
                    if ((tipoConceptoLiquidacion == TipoConceptoLiquidacion.PAGO_FIJO) || (tipoConceptoLiquidacion == TipoConceptoLiquidacion.PAGO_APOYO))
                    {                        
                        if (tipoIncidencia == TipoIncidencia.FALTA)
                        {
                            return 0;
                        }
                        if (tipoIncidencia == TipoIncidencia.ASISTENCIA)
                        {
                            return cantidad.Value;
                        }
                    }
                    if (tipoConceptoLiquidacion == TipoConceptoLiquidacion.DESCUENTO)
                    {
                        return cantidad.Value;
                    }
                    if (tipoConceptoLiquidacion == TipoConceptoLiquidacion.EXTRAS)
                    {
                        return cantidad.Value;
                    }
                    if (tipoConceptoLiquidacion == TipoConceptoLiquidacion.HORAS_EXTRAS)
                    {
                        return cantidad.Value * noHoras;
                    }
                    return cantidad.Value;
                }
                catch (Exception ex)
                {
                    return 0;
                }
                finally
                {
                   
                }
            }
            set
            {                
                _total = value;
                OnPropertyChanged("total");
                OnPropertyChangedHeader("totalPago");
            }
        }
        private decimal? _total { get; set; }
        public DateTime? fecha
        {
            get
            {
                if (_fecha == null)
                {
                    return null;
                }
                return _fecha.Value;
            }
            set
            {
                _fecha = value;
            }
        }
        public DateTime? _fecha { get; set; }
        private TipoIncidencia? _tipoIncidencia { get; set; }
        public TipoIncidencia tipoIncidencia
        {
            get
            {
                try
                {
                    if (_tipoIncidencia != null)
                    {
                        return _tipoIncidencia.Value;
                    }
                    return TipoIncidencia.NINGUNA;
                }
                catch (Exception ex)
                {
                    return TipoIncidencia.NINGUNA;
                }
            }
            set
            {
                _tipoIncidencia = value;
                OnPropertyChanged();
                OnPropertyChanged("total");
                OnPropertyChangedHeader("totalPago");
            }
        }

        private MotivoDescuentos? _motivoDescuentos { get; set; }
        public MotivoDescuentos motivoDescuentos
        {
            get
            {
                try
                {
                    if (_motivoDescuentos != MotivoDescuentos.ZONA_DE_ROBO)
                    {
                        viajesOperadorDecuento = null;
                    }
                    return _motivoDescuentos.Value;
                }
                catch (Exception ex)
                {
                    return MotivoDescuentos.NINGUNO;
                }
            }
            set
            {
                _motivoDescuentos = value;
                OnPropertyChanged();
                OnPropertyChanged("viajesOperadorDecuento");
                OnPropertyChanged("motrarCampoViaje"); 
                OnPropertyChanged("habilitarCantidad");
                
            }
        }

        public List<ViajesOperador> listaViajesOperador { get; set; }
        public int? folioViajeDescuento =>
            motivoDescuentos == MotivoDescuentos.ZONA_DE_ROBO ? (int?)viajesOperadorDecuento.folioViaje : null;
        private ViajesOperador _viajesOperadorDecuento { get; set; }
        public ViajesOperador viajesOperadorDecuento
        {
            get
            {
                if (_viajesOperadorDecuento == null)
                {
                    
                }
                return _viajesOperadorDecuento;
            }
            set
            {
                _viajesOperadorDecuento = value;
                if (_viajesOperadorDecuento == null)
                {
                    if (cantidad == null)
                    {
                        cantidad = 0;
                    }
                    
                }
                else
                {
                    cantidad = _viajesOperadorDecuento.pagoViaje;
                }
                OnPropertyChanged("viajesOperadorDecuento");
                //OnPropertyChanged("motrarCampoViaje");
                //OnPropertyChanged("habilitarCantidad");
                //OnPropertyChanged("total");
                //OnPropertyChangedHeader("totalPago");
            }
        }
        public int? idViajeDescuento { get; set; }
        public Visibility motrarCampoViaje
        {
            get
            {
                if (motivoDescuentos == MotivoDescuentos.ZONA_DE_ROBO)
                {
                    return Visibility.Visible;
                }
                return Visibility.Hidden;
            }
        }
        public bool habilitarCantidad
        {
            get
            {
                if (motivoDescuentos == MotivoDescuentos.ZONA_DE_ROBO)
                {
                    return false;
                }
                return true;
            }
        }
        public string observaciones { get; set; }
        public string usuario { get; set; }
        public int noHoras
        {
            get
            {
                if (_noHoras == null)
                {
                    return 0;
                }
                return _noHoras.Value;
            }
            set
            {
                _noHoras = value;
                OnPropertyChanged();
                OnPropertyChanged("total");
                OnPropertyChangedHeader("totalPago");
            }
        }
        public int? _noHoras { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string name = "")
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(name));
        }
        protected void OnPropertyChangedHeader([CallerMemberName] string name = "")
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(detalleHeader, new PropertyChangedEventArgs(name));
        }
        public bool activo
        {
            get
            {
                return _activo == null ? true : _activo.Value;
            }
            set
            {
                _activo = true;
            }
        }
        public bool? _activo { get; set; }
    }
    public enum MotivosDoble
    {
        NINGUNO = 0,
        INHABIL_DE_LEY = 1,
        TRES_GRANJAS = 2,
        CINCO_VIAJES = 3,
        FIN_DE_SEMANA = 4
    }
    public enum TipoConceptoLiquidacion
    {
        VIAJES_ALIMENTOS_BALANCEADOS = 0,
        VIAJES_SECOS_REFRIGERADOS = 1,
        PAGO_FIJO = 2,
        DESCUENTO = 3,
        EXTRAS = 4,
        HORAS_EXTRAS = 5,
        PAGO_APOYO = 6,
        VIAJES_ATLANTE = 7
    }
    public enum TipoIncidencia
    {
        NINGUNA = 0,
        ASISTENCIA = 1,
        FALTA = 2
    }
    public enum MotivoDescuentos
    {
        NINGUNO = 0,
        OTRO = 1,
        DAÑO_O_PERDIDA = 2,
        ZONA_DE_ROBO = 3,
        COMBUSTIBLE = 4
    }
}
