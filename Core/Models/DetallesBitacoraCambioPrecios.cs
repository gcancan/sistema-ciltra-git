﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class DetallesBitacoraCambioPrecios
    {
        public int idDetalleBitacoraCambioPrecios { get; set; }
        public int idBitacoraCambioPrecios { get; set; }
        public string destino { get; set; }
    }
}
