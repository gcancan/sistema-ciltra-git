﻿namespace CoreFletera.Models
{
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class RequisicionCompra
    {
        public int? idOrdenServ { get; set; }
        public string idOrdenServ_str =>
            (!this.idOrdenServ.HasValue ? "N/A" : this.idOrdenServ.ToString());

        public DateTime? fechaOrdenServ { get; set; }
        private string _idUnidadTrans { get; set; }
        public string idUnidadTrans
        {
            get
            {
                return (string.IsNullOrEmpty(this._idUnidadTrans) ? "N/A" : this._idUnidadTrans);
            }

            set
            {
                this._idUnidadTrans = value;
            }

        }

        public int idPreOC { get; set; }
        public DateTime fechaPreOC { get; set; }
        public int? idSolicitud { get; set; }
        public DateTime? FechaSolicitud { get; set; }
        public int? idCotizacion { get; set; }
        public DateTime? fechaCotizacion { get; set; }
        public int? idCompra { get; set; }
        public string ordenCompra { get; set; }
        public DateTime? fechaOrdenCompra { get; set; }
        public ComplementoRequisicionCompra complemento { get; set; }
    }

    public class ComplementoRequisicionCompra
    {
        public int idCompra { get; set; }
        public decimal importeCompra { get; set; }
        public string avisoEntrada { get; set; }
        public DateTime? fechaAvisoEntrada { get; set; }
        public string folioFacProv { get; set; }
        public DateTime? fechaFolioFacProv { get; set; }
        public decimal? importe { get; set; }
        public decimal? saldo { get; set; }
        public EstatusPago estatusPago
        {
            get
            {
                if (this.saldo.HasValue)
                {
                    if (this.saldo.Value == decimal.Zero)
                    {
                        return EstatusPago.PAGADO;
                    }
                    if (this.importe.Value == this.saldo.Value)
                    {
                        return EstatusPago.NO_PAGADO;
                    }
                    decimal? saldo = this.saldo;
                    decimal? importe = this.importe;
                    if ((saldo.GetValueOrDefault() < importe.GetValueOrDefault()) ? (saldo.HasValue & importe.HasValue) : false)
                    {
                        return EstatusPago.SALDO_PENDIENTE;
                    }
                }
                return EstatusPago.NO_DISPONIBLE;
            }
        }
    }
    public enum EstatusPago
    {
        NO_DISPONIBLE,
        PAGADO,
        NO_PAGADO,
        SALDO_PENDIENTE
    }

}
