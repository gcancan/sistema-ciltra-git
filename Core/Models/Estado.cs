﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class Estado
    {
        public int idEstado { get; set; }
        public string nombre { get; set; }
        public Pais pais { get; set; }
    }
}
