﻿namespace CoreFletera.Models
{
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class TarifaRuta
    {      
        public decimal precioSencillo { get; set; }

        public decimal precioFull { get; set; }
    }
}
