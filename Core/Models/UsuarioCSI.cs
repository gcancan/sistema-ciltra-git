﻿namespace CoreFletera.Models
{
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class UsuarioCSI
    {        
        public int idUsuario { get; set; }
        public string alias { get; set; }
        public string nombreUsuario { get; set; }
        public string contraseña { get; set; }
    }
}
