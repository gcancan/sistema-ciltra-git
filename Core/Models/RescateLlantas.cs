﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class RescateLlantas
    {
        public int idRescateLlanta { get; set; }
        public UnidadTransporte unidadTransporte { get; set; }
        public Personal chofer { get; set; }
        public Personal Llantero { get; set; }
        public DateTime fechaSolicitud { get; set; }
        public DateTime fechaSalida { get; set; }
        public DateTime? fechaEntrada { get; set; }
        public string unicacion { get; set; }
        public string estatus { get; set; }
        public string usuario { get; set; }
        public List<DetalleRescateLlanta> listaDetalles  { get; set; }
    }

    public class DetalleRescateLlanta
    {
        public int idDetalleRescateLlanta { get; set; }
        public Llanta llantaLleva { get; set; }
        public Llanta llantaTrae { get; set; }
        public int posicion { get; set; }
        public decimal profundidad { get; set; }
        public CondicionLlanta condicion { get; set; }
        public MotivosRescate motivoRescate { get; set; }
    }
}
