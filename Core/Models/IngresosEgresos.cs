﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class IngresosEgresos
    {
        public string unidad { get; set; }
        public decimal fletes { get; set; }
        public decimal combustible { get; set; }
        public decimal mantenimiento { get; set; }
        public decimal llantas { get; set; }
        public decimal lavadero { get; set; }
        public decimal utilidad { get; set; }
    }
}
