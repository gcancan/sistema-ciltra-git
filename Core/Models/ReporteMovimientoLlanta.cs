﻿namespace CoreFletera.Models
{
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class ReporteMovimientoLlanta
    {        
        public int idLlanta { get; set; }
        public string folioLlanta { get; set; }
        public DateTime fecha { get; set; }
        public enumTipoDocumento tipoDocumento { get; set; }
        public string tipoUbicacionActual { get; set; }
        public string descripcionActual { get; set; }
        public int posicionActual { get; set; }
        public string tipoUbicacionAnterior { get; set; }
        public string descripcionAnterioir { get; set; }
        public int posicionAnterior { get; set; }
        public string tipoUbicacionDestino { get; set; }
        public string descripcionDestino { get; set; }
        public int posicionDestino { get; set; }
        public decimal profundidad { get; set; }
        public string idDocumento { get; set; }
        public string usuario { get; set; }
        public decimal km { get; set; }
    }
}
