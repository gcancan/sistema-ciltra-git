﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class TareasEnvioCorreo : INotifyPropertyChanged
    {
        public int idTareasEnvioCorreo { get; set; }
        public DateTime fechaCreacion { get; set; }
        public DateTime fechaEjecucion { get; set; }
        public string descripcion { get; set; }
        public string titulo { get; set; }
        public string Texto { get; set; }
        public bool diario { get; set; }
        public bool activo { get; set; }
        public List<DetalleTareasEnvioCorreo> listaDetalles { get; set; }
        public string fechaStr
        {
            get
            {
                //if (diario)
                //{
                //    return DateTime.Now.ToString("dd/MM/yyyy");
                //}
                return fechaEjecucion.ToString("dd/MM/yyyy");
            }
        }
        public string horaStr => fechaEjecucion.ToString("HH:mm");
        private EstatusActividad _estatusActividad { get; set; }
        public EstatusActividad estatusActividad
        {
            get
            {
                return _estatusActividad;
            }
            set
            {
                _estatusActividad = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string name = "")
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(name));
        }
    }
    public enum EstatusActividad
    {
        ACTIVO = 0,
        EN_CURSO = 1,
        TERMINADO = 2
    }
    public class DetalleTareasEnvioCorreo
    {
        public int idDetalleTareasEnvioCorreo { get; set; }
        public int idTareasEnvioCorreo { get; set; }
        public ConfiguracionEnvioCorreo correoElectronico { get; set; } 
    }
}
