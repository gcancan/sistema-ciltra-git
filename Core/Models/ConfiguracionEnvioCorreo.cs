﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class ConfiguracionEnvioCorreo
    {
        public int idConfiguracionEnvioCorreo { get; set; }
        public string direccion { get; set; }
        public string dominio { get; set; }
        public string dirCorreo => string.Format("{0}{1}", direccion, dominio);
        public enumDominiosCorreos enumDominios { get; set; }
        public string direccionCorreo => string.Format(@"{0}{1}", direccion, dominio);
        public bool activo { get; set; }
        public string estatusStr => activo ? "ACTIVO" : "BAJA";
        public string nomDes { get; set; }
        public List<enumProcesosEnvioCorreo> listaProcesosEnvioCorreo { get; set; }
    }
}
