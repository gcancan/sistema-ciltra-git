﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public enum EnumTipoUbicacionLlanta
    {
        ALMACEN = 1,
        PROVEEDOR = 2,
        UNIDAD_TRANSPORTE = 3,
        PILA_DESECHOS = 4,
        SIN_UBICACION = 5
    }

    public enum EnumAlmacenLlantas
    {
        NINGUNA = 0,
        NUEVAS = 1,
        USADAS = 2,
        REVITALAZIDAS = 3,
        DESECHOS = 4,
        RESCATE = 5
    }
}
