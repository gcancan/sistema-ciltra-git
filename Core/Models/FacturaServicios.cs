﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
namespace CoreFletera.Models
{
    public class FacturaServicios
    {
        public int idFacturaServicio { get; set; }
        public string folioFactura { get; set; }
        public Empresa empresa { get; set; }
        public ZonaOperativa zonaOperativa { get; set; }
        public Cliente cliente { get; set; }
        public enumTipoServicioFacturacion tipoFacturaServicio { get; set; }
        public DateTime fechaFactura { get; set; }
        public string usuario { get; set; }
        public DateTime? fechaCancelacion { get; set; }
        public string usuarioCancelacion { get; set; }
        public decimal subTotal { get; set; }
        public decimal importeIVA { get; set; }
        public decimal importeRetencion { get; set; }
        public decimal importe { get; set; }
        public string observaciones { get; set; }
        public List<DetalleFacturaServicio> listDetalles { get; set; }
    }
    public class DetalleFacturaServicio
    {
        public int idDetalleFacturaServicio { get; set; }
        public int idFacturaServicio { get; set; }
        public int idDetalle { get; set; }
        public decimal km { get; set; }
        public decimal cantidad { get; set; }
        public decimal precio { get; set; }
        public decimal precioFijo { get; set; }
        public decimal subTotal { get; set; }
        public decimal importeIVA { get; set; }
        public decimal importeRetencion { get; set; }
        public decimal importe { get; set; }
    }
}
