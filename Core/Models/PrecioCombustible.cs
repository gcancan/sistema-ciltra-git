﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class PrecioCombustible
    {
        public decimal precio { get; set; }
        public int idPrecioCombustible { get; set; }
        public TipoCombustible tipoCombustible { get; set; }
        public string usuario { get; set; }
        public DateTime fecha { get; set; }
    }
}
