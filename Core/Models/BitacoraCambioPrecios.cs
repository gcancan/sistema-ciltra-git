﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class BitacoraCambioPrecios
    {
        public int idBitacoraCambioPrecios { get; set; }
        public DateTime fecha { get; set; }
        public string usuario { get; set; }
        public string cliente { get; set; }
        public eTiposPrecios tipoPrecio { get; set; }
        public decimal precioAnterior { get; set; }
        public decimal precioNuevo { get; set; }
        public List<DetallesBitacoraCambioPrecios> listaDetalles { get; set; }
    }
}
