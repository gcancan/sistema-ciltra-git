﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
namespace CoreFletera.Models
{
    public class Direccion
    {
        public Colonia colonia { get; set; }
    }
    public class Colonia
    {
        public int idColonia { get; set; }
        public string nombre { get; set; }
        public string cp { get; set; }
        public Ciudad ciudad { get; set; }
    }
    public class Ciudad
    {
        public int idCiudad { get; set; }
        public string nombre { get; set; }
        public Estado estado { get; set; }
    }

}
