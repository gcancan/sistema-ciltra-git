﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public enum eModalidadRutas
    {
        SENCILLO_24_TON = 0,
        SENCILLO_30_TON = 1,
        SENCILLO_35_TON=2,
        FULL = 3,
        NINGUNO = 1000
    }
}
