﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class ResumenCargaCombustible
    {
        public int idValeCarga { get; set; }
        public int idOrdenServ { get; set; }
        public int idOrdenPadre { get; set; }
        public string idUnidad { get; set; }
        public DateTime fechaCarga { get; set; }
        public decimal litros { get; set; }
    }
}
