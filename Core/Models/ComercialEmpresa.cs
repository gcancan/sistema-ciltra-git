﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class ComercialEmpresa
    {
        public string telefono { get; set; }

        public string nombre { get; set; }
        public string rfc { get; set; }
        public string calle { get; set; }
        public string estado { get; set; }
        public string pais { get; set; }
        public string municipio { get; set; }
        public string colonia { get; set; }
        public string cp { get; set; }
    }
}
