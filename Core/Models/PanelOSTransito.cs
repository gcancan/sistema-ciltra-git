﻿namespace CoreFletera.Models
{
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class PanelOSTransito
    {        
        public DateTime fechaCaptura { get; set; }
        public string unidadTrans { get; set; }
        public string chofer { get; set; }
        public int idPadre { get; set; }
        public int idOrdenServ { get; set; }
        public bool patios { get; set; }
        public string empresa { get; set; }
    }
}
