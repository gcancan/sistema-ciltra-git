﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFletera.Models
{
    public class ResumenCartaPorte
    {
        public int numGuiaId { get; set; }
        public int consecutivo { get; set; }
        public int idEmpresa { get; set; }
        public int idCliente { get; set; }
        public string origen { get; set; }
        public string destino { get; set; }
        public string producto { get; set; }
        public decimal km { get; set; }
        public int idCargaCombustible { get; set; }
        public string header
        {
            get
            {
                if (string.IsNullOrEmpty(origen))
                {
                    return destino;
                }
                else
                {
                    return string.Format("{0} - {1}", origen, destino);
                }
            }
        }
    }
}
