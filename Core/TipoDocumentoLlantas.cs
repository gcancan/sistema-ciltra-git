﻿namespace CoreFletera.Models
{
    using System;

    public enum TipoDocumentoLlantas
    {
        ORDEN_COMPRA = 1,
        ORDEN_SERVICIO = 2,
        ORDEN_REVITALIZADO = 3,
        ORDEN_BAJA = 4,
        RESCATE = 5,
        INVENTARIO = 6,
        SERVICIO_INTERNO = 7,
        ORDEN_TRABAJO = 8,
        VERIFICACION = 9
    }
}
