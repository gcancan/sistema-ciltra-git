﻿namespace CoreFletera.Models
{
    using Core.Models;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class MovimientoInternoLlanta
    {
        public Llanta llanta { get; set; }
        public DateTime fecha { get; set; }
        public enumTipoMovimientoLlanta tipoMovimiento { get; set; }
        public EnumTipoUbicacionLlanta tipoUbicacionAnt { get; set; }
        public EnumAlmacenLlantas almacenLlantasOrigen { get; set; }
        public string idUbicacionAnt { get; set; }
        public int? posicionAnt { get; set; }
        public decimal profundidadAnterior { get; set; }
        public decimal kmAnterior { get; set; }
        public EnumTipoUbicacionLlanta tipoUbicacionActual { get; set; }
        public EnumAlmacenLlantas almacenLlantasDestino { get; set; }
        public string idUbicacionActual { get; set; }
        public int? posicionActual { get; set; }
        public decimal profundidadActual { get; set; }
        public decimal kmActual { get; set; }
        public TipoDocumentoLlantas tipoDocumento { get; set; }

        public Usuario usuario { get; set; }

        public string idDocumento { get; set; }

        public CondicionLlanta condicionLlanta { get; set; }
    }
}
