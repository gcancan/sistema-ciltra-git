﻿namespace SincronizadorBaseDatos
{
    partial class SincronizadorForms
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_Proveedores = new MetroFramework.Controls.MetroButton();
            this.btnProductos = new MetroFramework.Controls.MetroButton();
            this.SuspendLayout();
            // 
            // btn_Proveedores
            // 
            this.btn_Proveedores.Location = new System.Drawing.Point(65, 74);
            this.btn_Proveedores.Name = "btn_Proveedores";
            this.btn_Proveedores.Size = new System.Drawing.Size(182, 37);
            this.btn_Proveedores.TabIndex = 0;
            this.btn_Proveedores.Text = "Proveedores";
            this.btn_Proveedores.UseSelectable = true;
            this.btn_Proveedores.Click += new System.EventHandler(this.btn_Proveedores_Click);
            // 
            // btnProductos
            // 
            this.btnProductos.Location = new System.Drawing.Point(65, 130);
            this.btnProductos.Name = "btnProductos";
            this.btnProductos.Size = new System.Drawing.Size(182, 39);
            this.btnProductos.TabIndex = 1;
            this.btnProductos.Text = "Productos";
            this.btnProductos.UseSelectable = true;
            this.btnProductos.Click += new System.EventHandler(this.btnProductos_Click);
            // 
            // SincronizadorForms
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(727, 261);
            this.Controls.Add(this.btnProductos);
            this.Controls.Add(this.btn_Proveedores);
            this.Name = "SincronizadorForms";
            this.Text = "SincronizadorForms";
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroButton btn_Proveedores;
        private MetroFramework.Controls.MetroButton btnProductos;
    }
}