﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.Interfaces;
using Core.Interfaces;
namespace SincronizadorBaseDatos
{
    public partial class SincronizadorForms : MetroFramework.Forms.MetroForm
    {
        public SincronizadorForms()
        {
            InitializeComponent();
        }

        private void btn_Proveedores_Click(object sender, EventArgs e)
        {
            sincronizarProveedores();
        }

        private void sincronizarProveedores()
        {
            new SincronizacionSvc().sincronizarProveedores();
        }

        private void btnProductos_Click(object sender, EventArgs e)
        {
            sincronizarProductos();
        }

        private void sincronizarProductos()
        {
            new SincronizacionSvc().sincronizarProductos();
        }
    }
}
