﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
//using Core.Models;
using Core.Interfaces;
using Core.Models;

namespace WpfClient
{
    /// <summary>
    /// Interaction logic for selectTipoTransporte.xaml
    /// </summary>
    public partial class selectTipoTransporte : Window
    {
        public selectTipoTransporte()
        {
            InitializeComponent();
        }

        public TipoTransporte selectTipoTransporteFind()
        {
            bool? dResult = ShowDialog();
            if (dResult.Value && lvlTiposTransportes != null && lvlTiposTransportes.SelectedItem != null)
            {
                return (TipoTransporte)((ListViewItem)lvlTiposTransportes.SelectedItem).Content;
            }

            return null;
        }


        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            buscar();
        }

        public void buscar()
        {
            OperationResult resp = new TiposTransporteSvc().findTiposTransportes(txtFind.Text.Trim());
            if (resp.typeResult == ResultTypes.error)
            {
                MessageBox.Show(string.Format(resp.mensaje), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            if (resp.typeResult == ResultTypes.warning)
            {
                MessageBox.Show(string.Format(resp.mensaje), "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            if (resp.typeResult == ResultTypes.recordNotFound)
            {
                MessageBox.Show(string.Format(resp.mensaje), "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            if (resp.typeResult == ResultTypes.success)
            {
                //MessageBox.Show(string.Format(resp.mensaje), "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);

                List<TipoTransporte> listT = (List<TipoTransporte>)(resp.result);
                foreach (var item in listT)
                {
                    ListViewItem lvl = new ListViewItem();
                    lvl.Content = item;
                    lvl.MouseDoubleClick += Lvl_MouseDoubleClick;
                    lvlTiposTransportes.Items.Add(lvl);
                }
            }
        }

        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DialogResult = true;
        }
    }
}
