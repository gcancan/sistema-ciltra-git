﻿
using System.Windows;


namespace WpfClient
{
    /// <summary>
    /// Interaction logic for viewFlotantes.xaml
    /// </summary>
    public partial class viewFlotantes : Window
    {
        public viewFlotantes(IView iv)
        {
            InitializeComponent();

            contenedorFlotante.Content = iv;


        }
    }
}
