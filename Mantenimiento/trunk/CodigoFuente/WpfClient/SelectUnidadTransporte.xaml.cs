﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
//using Core.Models;
using Core.Interfaces;
using Core.Models;

namespace WpfClient
{
    /// <summary>
    /// Interaction logic for SelectUnidadTransporte.xaml
    /// </summary>
    public partial class SelectUnidadTransporte : Window
    {
        //List<UnidadTransporte> lvlList = new List<UnidadTransporte>();
        string parametro = "";
        string _idEmpresa = "%";
        public SelectUnidadTransporte()
        {
            InitializeComponent();
        }

        public SelectUnidadTransporte(string param)
        {            
            InitializeComponent();
            parametro = param;
        }
        public SelectUnidadTransporte(string param, string idEmpresa)
        {
            InitializeComponent();
            parametro = param;
            _idEmpresa = idEmpresa;
        }

        public UnidadTransporte findAllUnidadesTransporte()
        {
            
            bool? dResult = ShowDialog();
            if (dResult.Value && lvlUnidades != null && lvlUnidades.SelectedItem != null)
            {
                return (UnidadTransporte)((ListViewItem)lvlUnidades.SelectedItem).Content;
            }
            return null;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            findUnidades();
            txtFind.Text = parametro;
        }

        private void findUnidades()
        {
            try
            {
                Cursor = Cursors.Wait;
                OperationResult resp = new UnidadTransporteSvc().findAllUnidadesTransporte(_idEmpresa);
                if (resp.typeResult == ResultTypes.error)
                {
                    MessageBox.Show(string.Format(resp.mensaje), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    controles.IsEnabled = false;
                }
                if (resp.typeResult == ResultTypes.recordNotFound)
                {
                    MessageBox.Show(string.Format(resp.mensaje), "Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
                    controles.IsEnabled = false;
                }
                if (resp.typeResult == ResultTypes.warning)
                {
                    MessageBox.Show(string.Format(resp.mensaje), "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                    controles.IsEnabled = false;
                }
                if (resp.typeResult == ResultTypes.success)
                {
                    //lvlList = ((List<UnidadTransporte>)resp.result);
                    mapList(((List<UnidadTransporte>)resp.result));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format(ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                controles.IsEnabled = false;
            }
            finally { Cursor = Cursors.Arrow; }
            
        }
                

        private void mapList(List<UnidadTransporte> list)
        {
            lvlUnidades.Items.Clear();
            List<ListViewItem> lvlList = new List<ListViewItem>();
            foreach (UnidadTransporte item in list)
            {
                ListViewItem lvl = new ListViewItem();
                lvl.Content = item;
                lvl.MouseDoubleClick += Lvl_MouseDoubleClick;
                lvlList.Add(lvl);
                //lvlUnidades.Items.Add(lvl);
            }
            lvlUnidades.ItemsSource = lvlList;
            CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(lvlUnidades.ItemsSource);
            view.Filter = UserFilter;            
        }

        private bool UserFilter(object item)
        {
            if (String.IsNullOrEmpty(txtFind.Text))
                return true;
            else
                //return ((item as UnidadTransporte).descripcionUni.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0);
            return ((UnidadTransporte)(item as ListViewItem).Content).tipoUnidad.nomTipoUniTrans.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0 ||
                    ((UnidadTransporte)(item as ListViewItem).Content).id_UnidadTrans.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0 ||
                    ((UnidadTransporte)(item as ListViewItem).Content).empresa.razonSocial.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0 ||
                    ((UnidadTransporte)(item as ListViewItem).Content).marca.nombreMarca.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0;

        }

        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DialogResult = true;
        }

        private void txtFind_TextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(lvlUnidades.ItemsSource).Refresh();          
        }       
    }
}
