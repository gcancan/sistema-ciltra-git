﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
//using Core.Models;
using Core.Interfaces;
using System.Net.Http;
using System.Net.Http.Headers;
using Core.Models;
//using System.Web.Script.Serialization;
//using Newtonsoft.Json;

namespace WpfClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private IView pantalla { get; set; }
        public IView pant { get; set; }
        private bool isNewWindow { get; set; }
        Dictionary<string, Window> _childs;
        private bool isDialog { get; set; }
        int _winId;
        public Usuario usuario;
        public bool esTaller;
        public int clave = 0;

        public MainWindow()
        {
            InitializeComponent();
        }

        public MainWindow(Usuario usuario)
        {
            this.usuario = usuario;
            InitializeComponent();
        }

        public MainWindow(Usuario usuario, bool esTaller)
        {
            this.usuario = usuario;
            this.esTaller = esTaller;
            InitializeComponent();
        }

        public MainWindow(Usuario usuario, bool esTaller, int clave)
        {
            this.usuario = usuario;
            this.esTaller = esTaller;
            this.clave = clave;
            InitializeComponent();
        }

        private void mainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            enableToolBarCommands(ToolbarCommands.none);

            //var mi1 = new MenuItem { Header = "Sistema" };

            //mainMenu.Items.Add(mi1);

            //var mi2 = new MenuItem();

            //mi2 = new MenuItem { Header = "Cerrar sistema" };
            ////mi2.Click += miCerrarSistema_Click;

            //mi1.Items.Add(mi2);

            //List<MenusAplicacion> menus = new List<MenusAplicacion>();
            //using (var client = new HttpClient())
            //{
            //    client.BaseAddress = new Uri("http://localhost:56543/");
            //    client.DefaultRequestHeaders.Accept.Clear();
            //    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            //    // New code:

            //    HttpResponseMessage response = await client.GetAsync("api/UnidadTransporte/buscarMen/");
            //    if (response.IsSuccessStatusCode)
            //    {
            //        string product = await response.Content.ReadAsStringAsync();
            //        //var s = new Newtonsoft.Json.JsonConverterJsonConvert();
            //        OperationResult resp = Newtonsoft.Json.JsonConvert.DeserializeObject<OperationResult>(product);
            //        menus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<MenusAplicacion>>(resp.result.ToString());

            //        if (resp.typeResult == ResultTypes.error)
            //        {
            //            MessageBox.Show(string.Format(resp.mensaje), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            //            return;
            //        }
            //        if (resp.typeResult == ResultTypes.recordNotFound)
            //        {
            //            MessageBox.Show(string.Format(resp.mensaje), "Advertencia", MessageBoxButton.OK, MessageBoxImage.Warning);
            //            return;
            //        }
            //        if (resp.typeResult == ResultTypes.success)
            //        {
            //            foreach (MenusAplicacion ma in menus)
            //            {
            //                if (ma.padre == null)
            //                {
            //                    MenuItem mi = new MenuItem();

            //                    mi.Header = ma.nombre;
            //                    mi.Name = ma.clave;
            //                    mi.Tag = ma;

            //                    generateMenuRaiz(mi, menus);

            //                    mainMenu.Items.Add(mi);
            //                }

            //            }
            //        }

            //                   JsonConverter jconvert = new JsonConverter().CanRead(product);


            //                   JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            //                   List<MenusAplicacion> routes_list = json_serializer.Deserialize<List<MenusAplicacion>>(product);
            //                   //var routes_list2 = json_serializer.ConvertToType<List<MenusAplicacion>>(product);
            ////DeserializeObject(product);

            //                   //Console.WriteLine("{0}\t${1}\t{2}", product.Name, product.Price, product.Category);
            //}
            //}


            //OperationResult find = new MenusAplicacionSvc().getAllMenus();




            //var cat = new MenuItem { Header = "Catalogos" };
            //mainMenu.Items.Add(cat);
            //var cat2 = new MenuItem { Header = "Unidad de Transporte" };
            //cat2.Name = "EDIT_UNIDAD_TRANSPORTE";
            //cat2.Click += Cat2_Click;
            //cat.Items.Add(cat2);

        }
        private void generateMenuRaiz(MenuItem raiz, List<MenusAplicacion> list)
        {
            foreach (MenusAplicacion ma in list)
            {
                if (ma.idPadre == ((MenusAplicacion)raiz.Tag).idMenuAplicacion)
                {
                    MenuItem mi = new MenuItem();

                    mi.Name = ma.clave;
                    mi.Header = ma.nombre;
                    mi.Tag = ma;

                    generateMenuRaiz(mi, list);

                    if (mi.Items.Count == 0)
                        mi.Click += menu_Click;

                    raiz.Items.Add(mi);
                }
            }
        }

        private void menu_Click(object sender, RoutedEventArgs e)
        {
            if (pant != null)
            {
                if (Keyboard.IsKeyDown(Key.LeftShift) && pant.bandeja == false)
                {
                    try
                    {
                        MainWindow mw = new MainWindow();
                        mw.isNewWindow = true;
                        mw.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                        mw.SizeToContent = SizeToContent.WidthAndHeight;
                        //mw.Owner = this;
                        //mw.datosSesion = datosSesion;
                        mw.createView(((MenuItem)sender).Name);

                        mw.Show();
                    }
                    catch (Exception)
                    {
                        createView(((MenuItem)sender).Name);
                    }
                }
                else
                {

                    //if (isEditable)
                    //{
                    //    if (MessageBox.Show("¿Desea salir de esta pantalla sin guardar los cambios?", Title, MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes)
                    //    {
                    //        return;
                    //    }
                    //}

                    //if (MessageBox.Show("¿Desea salir de esta pantalla sin guardar los cambios?", Title, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    //{
                    if (!Keyboard.IsKeyDown(Key.LeftShift))
                        createView(((MenuItem)sender).Name);
                    else
                    {
                        try
                        {
                            MainWindow mw = new MainWindow();
                            mw.isNewWindow = true;
                            mw.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                            mw.SizeToContent = SizeToContent.WidthAndHeight;

                            //mw.datosSesion = datosSesion;
                            mw.createView(((MenuItem)sender).Name);

                            mw.Show();
                        }
                        catch (Exception)
                        {
                            createView(((MenuItem)sender).Name);
                        }
                    }
                    pant = null;
                    //} //fin del if keyboard.iskeydown
                }
            }
            else
            {
                if (!Keyboard.IsKeyDown(Key.LeftShift))
                    createView(((MenuItem)sender).Name);
                else
                {
                    try
                    {
                        var mw = new MainWindow
                        {
                            isNewWindow = true,
                            WindowStartupLocation = WindowStartupLocation.CenterScreen,
                            SizeToContent = SizeToContent.WidthAndHeight,
                            //datosSesion = datosSesion
                        };
                        //mw.Owner = this;
                        mw.createView(((MenuItem)sender).Name);

                        mw.Show();
                    }
                    catch (Exception)
                    {
                        createView(((MenuItem)sender).Name);
                    }
                }
            }
        }

        public void createView(string clave)
        {
            Cursor = Cursors.Wait;

            IView view = ViewFactory.createView(clave);

            if (view == null)
            {
                Cursor = Cursors.Arrow;
                return;
            }

            view.mainWindow = this;
            //view.datosSesion = datosSesion;
            if (view.bandeja == false)
            {
                viewContainer vc = new viewContainer(view);
                scrollContainer.Content = vc;
                pantalla = view;
                pant = view;
                //
                //viewContainer.Children.Clear();
                //viewContainer.Children.Add((ViewBase)view);

                ((ViewBase)view).SetResourceReference(StyleProperty, "EditView");

                //enableToolBarCommands(ToolbarCommands.cerrar);
            }
            else
            {
                var vf = new viewFlotantes(view)
                {
                    Name = "winfloat" + _winId
                };
                _childs.Add(vf.Name, vf);
                vf.Closing += Vf_Closing; ;
                //vf.Owner = this;
                Cursor = Cursors.Arrow;
                vf.Show();
                _winId += 1;
            }
            Cursor = Cursors.Arrow;
        }

        private void Vf_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            _childs.Remove(((Window)sender).Name);
        }

        //public IView activeView
        //{
        //    get
        //    {
        //        if (viewContainer.Children.Count > 0)
        //            return (IView)viewContainer.Children[0];
        //        return null;
        //    }
        //}

        public void enableToolBarCommands(ToolbarCommands commands)
        {
            btnNuevo.Visibility = (commands & ToolbarCommands.nuevo) == ToolbarCommands.nuevo ? Visibility.Visible : Visibility.Collapsed;
            btnGuardar.Visibility = (commands & ToolbarCommands.guardar) == ToolbarCommands.guardar ? Visibility.Visible : Visibility.Collapsed;
            btnEliminar.Visibility = (commands & ToolbarCommands.eliminar) == ToolbarCommands.eliminar ? Visibility.Visible : Visibility.Collapsed;
            btnBuscar.Visibility = (commands & ToolbarCommands.buscar) == ToolbarCommands.buscar ? Visibility.Visible : Visibility.Collapsed;
            btnEditar.Visibility = (commands & ToolbarCommands.editar) == ToolbarCommands.editar ? Visibility.Visible : Visibility.Collapsed;
            cerrarButton.Visibility = (commands & ToolbarCommands.cerrar) == ToolbarCommands.cerrar ? Visibility.Visible : Visibility.Collapsed;
            reporteButton.Visibility = (commands & ToolbarCommands.imprimir) == ToolbarCommands.imprimir ? Visibility.Visible : Visibility.Collapsed;

            //if (activeView != null)
            //cerrarButton.Visibility = Visibility.Visible;

            //if (activeView != null && isDialog)
            //{
            //    guardarButton.Visibility = Visibility.Visible;
            //    cerrarButton.Visibility = Visibility.Visible;
            //    nuevoButton.Visibility = Visibility.Collapsed;
            //    eliminarButton.Visibility = Visibility.Collapsed;
            //    copiarButton.Visibility = Visibility.Collapsed;
            //    buscarButton.Visibility = Visibility.Collapsed;
            //    editarButton.Visibility = Visibility.Collapsed;
            //}
        }
        //public IView activeView
        //{
        //    get
        //    {
        //        if (viewContainer.Children.Count > 0)
        //            return (IView)viewContainer.Children[0];
        //        return null;
        //    }
        //}

        public void visualizarToolBar(Visibility x)
        {
            toolBar1.Visibility = x;
        }

        private void cerrarButton_Click(object sender, RoutedEventArgs e)
        {
            _cerrar();
            //visualizarToolBar(Visibility.Collapsed);          
        }

        private void _cerrar()
        {
            this.Close();
            //if (MessageBox.Show("¿Desea salir de esta pantalla sin guardar los cambios?", Title, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            //{
            //pant = null;
            //Cursor = Cursors.Wait;
            ////IResult result = validarEjecucionCmd();            

            //pantalla.cerrar();

            //viewContainer vc = new viewContainer(null);
            //scrollContainer.Content = vc;
            //pantalla = null;
            ////if (viewContainer.Children != null)
            ////    if (viewContainer.Children.Count > 0)
            ////        viewContainer.Children.Clear();
            //enableToolBarCommands(ToolbarCommands.none);
            //Cursor = Cursors.Arrow;

            //}
        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            _guardar();
        }

        private void _guardar()
        {
            IView vw = pantalla;
            var result = vw.guardar();
            if (result.valor != null)
            {
                if (result.typeResult == ResultTypes.error)
                {
                    MessageBox.Show(string.Format(result.mensaje), "Ocurrio un Error", MessageBoxButton.OK, MessageBoxImage.Error);

                }
                else if (result.typeResult == ResultTypes.warning)
                {
                    MessageBox.Show(string.Format(result.mensaje), "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                else if (result.typeResult == ResultTypes.recordNotFound)
                {
                    MessageBox.Show(string.Format(result.mensaje), "No se encontraron registros", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                else if (result.typeResult == ResultTypes.success)
                {
                    MessageBox.Show(string.Format(result.mensaje), "Registro Guardado", MessageBoxButton.OK, MessageBoxImage.Information);
                    scrollContainer.IsEnabled = false;
                    mainWindow.enableToolBarCommands(
                    ToolbarCommands.eliminar |
                    ToolbarCommands.editar |
                    ToolbarCommands.nuevo |
                    ToolbarCommands.cerrar
                );
                }
            }
            else
            {
                MessageBox.Show(string.Format("Obejeto Vacio"), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }



        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            _eliminar();
        }

        private void _eliminar()
        {
            IView vw = pantalla;
            var result = vw.eliminar();

            if (result.typeResult == ResultTypes.success)
            {
                MessageBox.Show(string.Format(result.mensaje), "Registro Borrado", MessageBoxButton.OK, MessageBoxImage.Information);
                scrollContainer.IsEnabled = true;
            }
            else if (result.typeResult == ResultTypes.warning)
            {
                MessageBox.Show(string.Format(result.mensaje), "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else if (result.typeResult == ResultTypes.recordNotFound)
            {
                MessageBox.Show(string.Format(result.mensaje), "No se encontraron registros", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else
            {
                MessageBox.Show(string.Format(result.mensaje), "Ocurrio un Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnNuevo_Click(object sender, RoutedEventArgs e)
        {
            _nuevo();
        }

        private void _nuevo()
        {

            IView vw = pantalla;
            mainWindow.enableToolBarCommands(
                ToolbarCommands.buscar |
                ToolbarCommands.guardar |
                ToolbarCommands.nuevo |
                ToolbarCommands.cerrar
            );
            vw.nuevo();
            scrollContainer.IsEnabled = true;

        }

        private void btnEditar_Click(object sender, RoutedEventArgs e)
        {
            _editar();
        }

        private void _editar()
        {
            IView vw = pantalla;
            vw.editar();
            scrollContainer.IsEnabled = true;
        }

        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            _buscar();
        }

        private void _buscar()
        {
            IView vw = pantalla;            
            scrollContainer.IsEnabled = false;
            vw.buscar();
        }

        private void _imprimir()
        {
            IView vw = pantalla;
            vw.imprimir();
        }

        private void reporteButton_Click(object sender, RoutedEventArgs e)
        {
            _imprimir();
        }
    }
}