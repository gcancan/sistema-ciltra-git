﻿//using Core.Models;
using Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace WpfClient
{
    public class ViewBase : UserControl, IView
    {
        public MainWindow mainWindow { get; set; }

        public Boolean bandeja { get; set; }

        public virtual void nuevo()
        {
            mainWindow.enableToolBarCommands(
                ToolbarCommands.buscar |
                ToolbarCommands.guardar |
                ToolbarCommands.nuevo |
                ToolbarCommands.cerrar
            );

            IsEnabled = true;
        }
        

        public virtual void cerrar()
        {

        }

        public virtual OperationResult guardar()
        {
            mainWindow.enableToolBarCommands(
                ToolbarCommands.eliminar |
                ToolbarCommands.editar |
                ToolbarCommands.nuevo |
                ToolbarCommands.cerrar |
                ToolbarCommands.imprimir);
            return new OperationResult { mensaje ="Se implemento codigo de guardado", valor = (int)ResultTypes.success};
        }

        public virtual OperationResult eliminar()
        {
            return new OperationResult { mensaje = "Se implemento codigo para eliminar", valor = (int)ResultTypes.success };
        }

        public virtual void editar()
        {
            IsEnabled = true;
            mainWindow.enableToolBarCommands(
                //ToolbarCommands.buscar |              
                //ToolbarCommands.eliminar |
                ToolbarCommands.guardar |
                ToolbarCommands.nuevo |
                ToolbarCommands.cerrar
            );           
        }

        public virtual void buscar()
        {
            mainWindow.enableToolBarCommands(
                ToolbarCommands.eliminar |
                ToolbarCommands.editar |
                ToolbarCommands.nuevo |
                ToolbarCommands.cerrar |
                ToolbarCommands.imprimir);
        }

        public virtual void imprimir()
        {
            mainWindow.enableToolBarCommands(
                ToolbarCommands.eliminar |
                ToolbarCommands.editar |
                ToolbarCommands.nuevo |
                ToolbarCommands.cerrar |
                ToolbarCommands.imprimir);
        }

             
        protected delegate T ProcessDelegate<out T>(object[] arguments);
        protected delegate void ProcessCompleted<in T>(object[] arguments, T processResult);
        public static bool isBusy { get; private set; }

        protected void tryExecuteAsync<T>(object[] arguments, ProcessDelegate<T> processDelegate, ProcessCompleted<T> completed)
        {
            processDelegate.BeginInvoke(arguments,
                                        result =>
                                        {
                                            var p = (ProcessDelegate<T>)((AsyncResult)result).AsyncDelegate;
                                            var iResult = p.EndInvoke(result);

                                            Dispatcher.Invoke((Action)(() => completed.Invoke(arguments, iResult)));

                                        },
                                        null
                );
        }

        public static void executeLongProcess(Delegate metodoAInvocar, Delegate operacionCompletada)
        {
            var bgw = new BackgroundWorker();

            bgw.DoWork += delegate
            {
                isBusy = true;
                metodoAInvocar.DynamicInvoke();
            };
            bgw.RunWorkerCompleted += delegate
            {
                isBusy = false;
                operacionCompletada.DynamicInvoke();
            };

            bgw.RunWorkerAsync();
        }

        
    }
}
