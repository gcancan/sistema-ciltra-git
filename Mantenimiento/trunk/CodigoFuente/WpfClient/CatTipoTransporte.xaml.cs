﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
//using Core.Models;
using Core.Interfaces;
using Core.Models;

namespace WpfClient
{
    /// <summary>
    /// Interaction logic for CatTipoTransporte.xaml
    /// </summary>
    public partial class CatTipoTransporte : ViewBase
    {
        public CatTipoTransporte()
        {
            InitializeComponent();
        }

        public override OperationResult guardar()
        {
            if (validar())
            {
                TipoTransporte tipoTransporte = mapForm();
                if (tipoTransporte != null)
                {
                    OperationResult resp = new TiposTransporteSvc().saveTipoTransporte(tipoTransporte);
                    if (resp.typeResult == ResultTypes.success)
                    {

                        mapForm((TipoTransporte)resp.result);
                        base.guardar();
                    }
                    return resp;
                }
                else
                {
                    return new OperationResult { result = null, mensaje = "Ocurrio un error al mapear el objeto", valor = 1 };
                }
            }
            else
            {
                return new OperationResult { result = null, mensaje = "Se requiere Acompletar todos los campos", valor = 2 };
            }
        }

        private bool validar()
        {
            if (string.IsNullOrEmpty(txtNombre.Text.Trim()))
            {
                return false;
            }
            return true;
        }

        private TipoTransporte mapForm()
        {
            try
            {
                return new TipoTransporte
                {
                    nombre = txtNombre.Text.Trim(),
                    idTipoTransporte = (string.IsNullOrEmpty(txtClave.Text) ? 0 : Convert.ToInt32(txtClave.Text))
                };
            }
            catch (Exception)
            {
                return null;
            }
        }


        public override OperationResult eliminar()
        {
            try
            {
                if (validar())
                {
                    TipoTransporte tipoTransporte = mapForm();
                    if (tipoTransporte != null)
                    {
                        OperationResult resp = new TiposTransporteSvc().deleteTipoTransporte(tipoTransporte);
                        if (resp.typeResult == ResultTypes.success)
                        {
                            mapForm(null);
                            base.eliminar();
                        }
                        return resp;
                    }
                    else
                    {
                        return new OperationResult { result = null, mensaje = "Ocurrio un error al mapear el objeto", valor = 1 };
                    }
                }
                else
                {
                    return new OperationResult { result = null, mensaje = "Se requiere Acompletar todos los campos", valor = 2 };

                }
            }
            catch (Exception e)
            {
                return new OperationResult { result = null, mensaje = e.Message, valor = 1 };
            }
        }

        public override void buscar()
        {
            selectTipoTransporte select = new selectTipoTransporte();
            var resp = select.selectTipoTransporteFind();
            mapForm(resp);
            base.buscar();
        }


        private void mapForm(TipoTransporte resp)
        {
            if (resp != null)
            {
                txtClave.Text = resp.idTipoTransporte.ToString();
                txtNombre.Text = resp.nombre;
                //gridControles.IsEnabled = false;
            }
            else
            {
                txtClave.Clear();
                txtNombre.Clear();
                //gridControles.IsEnabled = true;
            }
        }
        public override void nuevo()
        {
            mapForm(null);
            base.nuevo();
        }
        public override void editar()
        {
            base.editar();
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            base.nuevo();
        }
    }
}
