﻿#pragma checksum "..\..\..\Modulos Mantenimiento\addAsignacionTrabajo.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "938C100C283E8D88415C055DBD99B08420F76031C0BBD090AA9DF567F2A3E8DB"
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using WpfClient.Modulos_Mantenimiento;


namespace WpfClient.Modulos_Mantenimiento {
    
    
    /// <summary>
    /// addAsignacionTrabajo
    /// </summary>
    public partial class addAsignacionTrabajo : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 11 "..\..\..\Modulos Mantenimiento\addAsignacionTrabajo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid controles;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\..\Modulos Mantenimiento\addAsignacionTrabajo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtDescripcion;
        
        #line default
        #line hidden
        
        
        #line 26 "..\..\..\Modulos Mantenimiento\addAsignacionTrabajo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbxDivicion;
        
        #line default
        #line hidden
        
        
        #line 31 "..\..\..\Modulos Mantenimiento\addAsignacionTrabajo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbxFamilia;
        
        #line default
        #line hidden
        
        
        #line 36 "..\..\..\Modulos Mantenimiento\addAsignacionTrabajo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbxMecanico;
        
        #line default
        #line hidden
        
        
        #line 41 "..\..\..\Modulos Mantenimiento\addAsignacionTrabajo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnAceptar;
        
        #line default
        #line hidden
        
        
        #line 42 "..\..\..\Modulos Mantenimiento\addAsignacionTrabajo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnCancelar;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/WpfClient;component/modulos%20mantenimiento/addasignaciontrabajo.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Modulos Mantenimiento\addAsignacionTrabajo.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 10 "..\..\..\Modulos Mantenimiento\addAsignacionTrabajo.xaml"
            ((WpfClient.Modulos_Mantenimiento.addAsignacionTrabajo)(target)).Loaded += new System.Windows.RoutedEventHandler(this.Window_Loaded);
            
            #line default
            #line hidden
            return;
            case 2:
            this.controles = ((System.Windows.Controls.Grid)(target));
            return;
            case 3:
            this.txtDescripcion = ((System.Windows.Controls.TextBox)(target));
            return;
            case 4:
            this.cbxDivicion = ((System.Windows.Controls.ComboBox)(target));
            
            #line 26 "..\..\..\Modulos Mantenimiento\addAsignacionTrabajo.xaml"
            this.cbxDivicion.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.cbxDivicion_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 5:
            this.cbxFamilia = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 6:
            this.cbxMecanico = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 7:
            this.btnAceptar = ((System.Windows.Controls.Button)(target));
            
            #line 41 "..\..\..\Modulos Mantenimiento\addAsignacionTrabajo.xaml"
            this.btnAceptar.Click += new System.Windows.RoutedEventHandler(this.btnAceptar_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.btnCancelar = ((System.Windows.Controls.Button)(target));
            
            #line 42 "..\..\..\Modulos Mantenimiento\addAsignacionTrabajo.xaml"
            this.btnCancelar.Click += new System.Windows.RoutedEventHandler(this.btnCancelar_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

