﻿namespace WpfClient
{
    public enum Menus
    {
        MENU_CATALOGOS_TIPOS_UNIDADES = 0,
        MENU_CATALOGOS_SERVICIOS = 1,
        MENU_MOVIMIENTOS_RECEPCION=2,
        MENU_MOVIMIENTOS_DIAGNOSTICO = 3
    }
}