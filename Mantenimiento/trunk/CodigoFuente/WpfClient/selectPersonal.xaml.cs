﻿using Core.Interfaces;
using Core.Models;
//using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfClient
{
    /// <summary>
    /// Interaction logic for selectPersonal.xaml
    /// </summary>
    public partial class selectPersonal : Window
    {
        string parametro = "";
        //private List<Personal> lvlList;

        public selectPersonal()
        {
            InitializeComponent();
        }
        public selectPersonal(string param)
        {
            InitializeComponent();
            parametro = param;
        }

        public Personal findPersonal()
        {
            bool? dResult = ShowDialog();
            if (dResult.Value && lvlPersonal != null && lvlPersonal.SelectedItem != null)
            {
                return (Personal)((ListViewItem)lvlPersonal.SelectedItem).Content;
            }
            else
            {
                return null;
            }
        }

        private void mapList(List<Personal> list)
        {
            lvlPersonal.Items.Clear();
            List<ListViewItem> lvlList = new List<ListViewItem>();
            foreach (Personal item in list)
            {
                ListViewItem lvl = new ListViewItem();
                lvl.Content = item;
                lvl.MouseDoubleClick += Lvl_MouseDoubleClick;
                lvlList.Add(lvl);
                //lvlUnidades.Items.Add(lvl);
            }
            lvlPersonal.ItemsSource = lvlList;
            CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(lvlPersonal.ItemsSource);
            view.Filter = UserFilter;
        }

        private bool UserFilter(object item)
        {
            if (String.IsNullOrEmpty(txtFind.Text))
                return true;
            else
                //return ((item as UnidadTransporte).descripcionUni.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0);
                return ((Personal)(item as ListViewItem).Content).idPersonal.ToString().IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0 ||
                        ((Personal)(item as ListViewItem).Content).nombreCompleto.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0 ||
                        ((Personal)(item as ListViewItem).Content).empresa.razonSocial.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0;

        }

        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DialogResult = true;
        }

        private void txtFind_TextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(lvlPersonal.ItemsSource).Refresh();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            findAllPersonal();
            txtFind.Text = parametro;
        }

        private void findAllPersonal()
        {
            try
            {
                Cursor = Cursors.Wait;
                OperationResult resp = new PersonalSvc().findAllPersonal();
                if (resp.typeResult == ResultTypes.error)
                {
                    MessageBox.Show(string.Format(resp.mensaje), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    controles.IsEnabled = false;
                }
                if (resp.typeResult == ResultTypes.recordNotFound)
                {
                    MessageBox.Show(string.Format(resp.mensaje), "Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
                    controles.IsEnabled = false;
                }
                if (resp.typeResult == ResultTypes.warning)
                {
                    MessageBox.Show(string.Format(resp.mensaje), "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                    controles.IsEnabled = false;
                }
                if (resp.typeResult == ResultTypes.success)
                {
                    //lvlList = ((List<Personal>)resp.result);
                    mapList(((List<Personal>)resp.result));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format(ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                controles.IsEnabled = false;
            }
            finally { Cursor = Cursors.Arrow; }

        }
    }
}
