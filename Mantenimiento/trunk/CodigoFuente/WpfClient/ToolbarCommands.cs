﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfClient
{
    [Flags]
    public enum ToolbarCommands
    {
        nuevo = 1,
        guardar = 2,
        editar = 4,
       
        buscar = 16,
        eliminar = 32,
        cerrar = 64,
        imprimir = 100,
        none = 128
    }
}
