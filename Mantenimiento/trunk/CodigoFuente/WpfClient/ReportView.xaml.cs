﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Xps.Packaging;
//using Core.Models;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using ap = System.Windows.Forms;
using System.IO.Packaging;
using Core.Interfaces;
using System;

namespace WpfClient
{
    /// <summary>
    /// Interaction logic for ReportView.xaml
    /// </summary>
    public partial class ReportView : Window
    {
        string rutaExcel = "";
        string rutaWord = "";
        private MainWindow mainWindow;
        //private Cliente cliente;

        public ReportView()
        {
            InitializeComponent();
        }

        public ReportView(MainWindow mainWindow)
        {
            InitializeComponent();
            this.mainWindow = mainWindow;
            //this.cliente = cliente;
        }

        //public void crearArchivo(Viaje cartaPorte)
        //{
        //    Cursor = Cursors.Wait;
        //    try
        //    {
        //        btnEXCEL.Visibility = Visibility.Collapsed;
        //        string path = System.Windows.Forms.Application.StartupPath;
        //        if (!Directory.Exists(path + "\\reportes\\"))
        //        {
        //            DirectoryInfo dir = Directory.CreateDirectory(path + "\\reportes\\");
        //        }
        //        string archivo = "";
        //        //archivo = string.Format(@"{0}{1}", path + "\\reportes\\", cartaPorte.NumGuiaId);
        //        archivo = string.Format(@"{0}{1}", path + "\\reportes\\", "Ejemplo");

        //        if (File.Exists(archivo + ".doc"))
        //        {
        //            File.Delete(archivo + ".doc");
        //            MiDocumentoCartaPorte(cartaPorte, archivo);
        //        }
        //        else
        //        {
        //            MiDocumentoCartaPorte(cartaPorte, archivo);
        //        }

        //        imprimir(archivo);
        //        Cursor = Cursors.Arrow;
        //        ShowDialog();
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message);
        //    }
        //    finally { Cursor = Cursors.Arrow; }
        //}

        //internal bool ReporteKardexEntSal(DateTime fechaInicial, DateTime fechaFinal, List<KardexEntSalUnid> lista, UnidadTransporte unidad, bool isMes, Empresa empresa)
        //{
        //    #region inicio
        //    string archivo = "";
        //    try
        //    {
        //        string path = System.Windows.Forms.Application.StartupPath;
        //        if (!Directory.Exists(path + "\\reportes\\"))
        //        {
        //            DirectoryInfo dir = Directory.CreateDirectory(path + "\\reportes\\");
        //        }

        //        //archivo = string.Format(@"{0}{1}", path + "\\reportes\\", cartaPorte.NumGuiaId);
        //        archivo = string.Format(@"{0}{1}", path + "\\reportes\\", "reporte");

        //        if (File.Exists(archivo + ".xlsx"))
        //        {
        //            File.Delete(archivo + ".xlsx");
        //        }
        //        if (File.Exists(archivo + ".xps"))
        //        {
        //            File.Delete(archivo + ".xps");
        //        }
        //        //".xps"
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message);
        //        return false;
        //    }
        //    #endregion
        //    Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
        //    Excel.Workbook xlWorkBook = xlApp.Workbooks.Add();
        //    //Microsoft.Office.Interop.Excel.Worksheet excelSheet;
        //    Microsoft.Office.Interop.Excel.Range excelCellrange;
        //    try
        //    {

        //        if (xlApp == null)
        //        {
        //            MessageBox.Show("Excel is not properly installed!!");
        //            return false;
        //        }
        //        else
        //        {
        //            Excel.Worksheet xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
        //            xlWorkSheet.PageSetup.LeftMargin = 10;
        //            xlWorkSheet.PageSetup.RightMargin = 10;
        //            xlWorkSheet.PageSetup.Orientation = Microsoft.Office.Interop.Excel.XlPageOrientation.xlLandscape;

        //            xlWorkSheet.Name = "kardex de unidades";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[1, 6]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 14;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            xlWorkSheet.Cells[1, 1] = empresa.nombre;

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[2, 1], xlWorkSheet.Cells[2, 6]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 12;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            xlWorkSheet.Cells[2, 1] = "KARDEX MOVIMIENTOS DE LA UNIDAD " + unidad.clave;


        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[3, 1], xlWorkSheet.Cells[3, 6]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Italic = true;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            if (isMes)
        //            {
        //                xlWorkSheet.Cells[3, 1] = ((Mes)fechaInicial.Month).ToString() + " del " + fechaInicial.Year;
        //            }
        //            else
        //            {
        //                xlWorkSheet.Cells[3, 1] = "Del " + fechaInicial.ToString("dd MMMM yyyy") + " al " + fechaFinal.ToString("dd MMMM yyyy");
        //            }

        //            xlWorkSheet.Cells[4, 1] = "FECHA";
        //            xlWorkSheet.Cells[4, 2] = "ENTRADA";
        //            xlWorkSheet.Cells[4, 3] = "SALIDA";
        //            xlWorkSheet.Cells[4, 4] = "TIPO OPERACIÓN";
        //            xlWorkSheet.Cells[4, 5] = "DETALLES";
        //            xlWorkSheet.Cells[4, 6] = "FOLIO";
        //            int i = 4;
        //            foreach (KardexEntSalUnid kardex in lista)
        //            {
        //                i++;
        //                xlWorkSheet.Cells[i, 1] = kardex.fecha.ToString("dd MMMM yyyy HH:mm:ss tt");
        //                if (kardex.tipoMovimiento == eTipoMovimiento.ENTRADA)
        //                {
        //                    xlWorkSheet.Cells[i, 2] = kardex.lugar;
        //                }
        //                else if (kardex.tipoMovimiento == eTipoMovimiento.SALIDA)
        //                {
        //                    xlWorkSheet.Cells[i, 3] = kardex.lugar;
        //                }
        //                xlWorkSheet.Cells[i, 4] = kardex.descripcion;
        //                xlWorkSheet.Cells[i, 5] = (string)detallesKardex(kardex.listaDetalles);
        //                xlWorkSheet.Cells[i, 6] = kardex.folio;
        //            }
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[4, 1], xlWorkSheet.Cells[i, 6]];
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.Worksheet.ListObjects.AddEx(
        //                                                        SourceType: Excel.XlListObjectSourceType.xlSrcRange,
        //                                                        Source: excelCellrange,
        //                                                        XlListObjectHasHeaders: Excel.XlYesNoGuess.xlYes);
        //            excelCellrange.Font.Size = 9;
        //            excelCellrange.EntireColumn.AutoFit();
        //            xlWorkBook.SaveAs(archivo + ".xlsx");
        //            rutaExcel = archivo + ".xlsx";

        //            bool resp = convertirExcelToSps(archivo);
        //            if (resp)
        //            {
        //                imprimirReporte(archivo + ".xps");
        //            }
        //            return resp;
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        MessageBox.Show(e.Message);
        //        xlWorkBook.Close();
        //        xlApp.Quit();
        //        return false;
        //    }
        //    finally
        //    {
        //        xlWorkBook.Close();
        //        xlApp.Quit();
        //        GC.WaitForPendingFinalizers();
        //        GC.Collect();
        //        GC.WaitForPendingFinalizers();
        //        GC.Collect();
        //    }
        //}

        //private string detallesKardex(List<string> listaDetalles)
        //{
        //    try
        //    {
        //        string cadena = "";
        //        foreach (var item in listaDetalles)
        //        {
        //            cadena += item + ", ";
        //        }
        //        return cadena.Trim().Trim(',');
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //    }
        //}

        //internal bool ReporteBitacoraCargaCombustible(List<BitacoraCargaCombustible> listBitacora, DateTime fechaInicial, DateTime fechaFinal, bool isMes, Empresa empresaSelected)
        //{
        //    #region inicio
        //    string archivo = "";
        //    try
        //    {
        //        string path = System.Windows.Forms.Application.StartupPath;
        //        if (!Directory.Exists(path + "\\reportes\\"))
        //        {
        //            DirectoryInfo dir = Directory.CreateDirectory(path + "\\reportes\\");
        //        }

        //        //archivo = string.Format(@"{0}{1}", path + "\\reportes\\", cartaPorte.NumGuiaId);
        //        archivo = string.Format(@"{0}{1}", path + "\\reportes\\", "reporte");

        //        if (File.Exists(archivo + ".xlsx"))
        //        {
        //            File.Delete(archivo + ".xlsx");
        //        }
        //        if (File.Exists(archivo + ".xps"))
        //        {
        //            File.Delete(archivo + ".xps");
        //        }
        //        //".xps"
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message);
        //        return false;
        //    }
        //    #endregion
        //    Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
        //    Excel.Workbook xlWorkBook = xlApp.Workbooks.Add();
        //    //Microsoft.Office.Interop.Excel.Worksheet excelSheet;
        //    Microsoft.Office.Interop.Excel.Range excelCellrange;
        //    try
        //    {

        //        if (xlApp == null)
        //        {
        //            MessageBox.Show("Excel is not properly installed!!");
        //            return false;
        //        }
        //        else
        //        {
        //            Excel.Worksheet xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
        //            xlWorkSheet.PageSetup.LeftMargin = 10;
        //            xlWorkSheet.PageSetup.RightMargin = 10;
        //            xlWorkSheet.PageSetup.Orientation = Microsoft.Office.Interop.Excel.XlPageOrientation.xlLandscape;

        //            xlWorkSheet.Name = "Bitacora de cambio de precios";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[1, 12]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 14;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            xlWorkSheet.Cells[1, 1] = empresaSelected.nombre;

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[2, 1], xlWorkSheet.Cells[2, 12]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 12;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            xlWorkSheet.Cells[2, 1] = "BITACORA DE CARGA DE COMBUSTIBLE";


        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[3, 1], xlWorkSheet.Cells[3, 12]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Italic = true;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            if (isMes)
        //            {
        //                xlWorkSheet.Cells[3, 1] = ((Mes)fechaInicial.Month).ToString() + " del " + fechaInicial.Year;
        //            }
        //            else
        //            {
        //                xlWorkSheet.Cells[3, 1] = "Del " + fechaInicial.ToString("dd MMMM yyyy") + " al " + fechaFinal.ToString("dd MMMM yyyy");
        //            }

        //            xlWorkSheet.Cells[4, 1] = "FECHA DE CARGA";
        //            xlWorkSheet.Cells[4, 2] = "FECHA DE ENTRADA";
        //            xlWorkSheet.Cells[4, 3] = "TIEMPO";
        //            xlWorkSheet.Cells[4, 4] = "UNIDAD";
        //            xlWorkSheet.Cells[4, 5] = "CHOFER";
        //            xlWorkSheet.Cells[4, 6] = "KM AL CARGAR";
        //            xlWorkSheet.Cells[4, 7] = "KM AL ENTRAR";
        //            xlWorkSheet.Cells[4, 8] = "KM CALCULADO";
        //            xlWorkSheet.Cells[4, 9] = "DIFERENCIA KM" + Environment.NewLine + "CARGA-ENTRADA";
        //            xlWorkSheet.Cells[4, 10] = "DIFERENCIA KM" + Environment.NewLine + "CARGA-CALCULADO";
        //            xlWorkSheet.Cells[4, 11] = "LTS";
        //            xlWorkSheet.Cells[4, 12] = "DESPACHADOR";
        //            int i = 4;
        //            foreach (BitacoraCargaCombustible bitacora in listBitacora)
        //            {
        //                i++;
        //                xlWorkSheet.Cells[i, 1] = bitacora.fecha.ToString("dd/MMMM/yy HH:mm:ss");
        //                xlWorkSheet.Cells[i, 2] = bitacora.fechaEntrada.ToString("dd/MMMM/yy HH:mm:ss");
        //                xlWorkSheet.Cells[i, 3] = ((bitacora.diferenciaFechas.Days * 24) + bitacora.diferenciaFechas.Hours).ToString() + ":" + bitacora.diferenciaFechas.Minutes.ToString() + ":" + bitacora.diferenciaFechas.Seconds.ToString();
        //                xlWorkSheet.Cells[i, 4] = bitacora.unidad;
        //                xlWorkSheet.Cells[i, 5] = bitacora.chofer;
        //                xlWorkSheet.Cells[i, 6] = bitacora.kmCarga;
        //                xlWorkSheet.Cells[i, 7] = bitacora.kmEntrada;
        //                xlWorkSheet.Cells[i, 8] = bitacora.kmCalculado;
        //                xlWorkSheet.Cells[i, 9] = bitacora.kmDifCargaEntrada;
        //                xlWorkSheet.Cells[i, 10] = bitacora.kmDifCargaCaluculado;
        //                xlWorkSheet.Cells[i, 11] = bitacora.ltsCargados;
        //                xlWorkSheet.Cells[i, 12] = bitacora.despachador;
        //            }
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[4, 1], xlWorkSheet.Cells[i, 12]];
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.Worksheet.ListObjects.AddEx(
        //                                                        SourceType: Excel.XlListObjectSourceType.xlSrcRange,
        //                                                        Source: excelCellrange,
        //                                                        XlListObjectHasHeaders: Excel.XlYesNoGuess.xlYes);
        //            excelCellrange.Font.Size = 8;
        //            excelCellrange.EntireColumn.AutoFit();
        //            xlWorkBook.SaveAs(archivo + ".xlsx");
        //            rutaExcel = archivo + ".xlsx";

        //            xlWorkBook.Close();
        //            xlApp.Quit();
        //            ap.SaveFileDialog rutaDestino = new ap.SaveFileDialog();
        //            rutaDestino.Title = "Guardar Como";
        //            rutaDestino.Filter = "Libro de excel(*.xlsx)|*.xlsx";
        //            if (rutaDestino.ShowDialog() == ap.DialogResult.OK)
        //            {
        //                if (exportarExcel(rutaDestino.FileName))
        //                {
        //                    MessageBox.Show("Se exporto correctamente el reporte", "Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
        //                    return true;
        //                }
        //                else
        //                {
        //                    return false;
        //                }
        //            }
        //            else
        //            {
        //                return false;
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        MessageBox.Show(e.Message);
        //        xlWorkBook.Close();
        //        xlApp.Quit();
        //        return false;
        //    }
        //    finally
        //    {

        //        GC.WaitForPendingFinalizers();
        //        GC.Collect();
        //        GC.WaitForPendingFinalizers();
        //        GC.Collect();
        //    }
        //}

        //internal bool reporteIngresosEgresos(List<IngresosEgresos> lista, Empresa empresa, DateTime fechaInicial, DateTime fechaFinal, bool isMes)
        //{
        //    #region inicio
        //    string archivo = "";
        //    try
        //    {
        //        string path = System.Windows.Forms.Application.StartupPath;
        //        if (!Directory.Exists(path + "\\reportes\\"))
        //        {
        //            DirectoryInfo dir = Directory.CreateDirectory(path + "\\reportes\\");
        //        }

        //        //archivo = string.Format(@"{0}{1}", path + "\\reportes\\", cartaPorte.NumGuiaId);
        //        archivo = string.Format(@"{0}{1}", path + "\\reportes\\", "reporte");

        //        if (File.Exists(archivo + ".xlsx"))
        //        {
        //            File.Delete(archivo + ".xlsx");
        //        }
        //        if (File.Exists(archivo + ".xps"))
        //        {
        //            File.Delete(archivo + ".xps");
        //        }
        //        //".xps"
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message);
        //        return false;
        //    }
        //    #endregion
        //    Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
        //    Excel.Workbook xlWorkBook = xlApp.Workbooks.Add();
        //    //Microsoft.Office.Interop.Excel.Worksheet excelSheet;
        //    Microsoft.Office.Interop.Excel.Range excelCellrange;
        //    try
        //    {

        //        if (xlApp == null)
        //        {
        //            MessageBox.Show("Excel is not properly installed!!");
        //            return false;
        //        }
        //        else
        //        {
        //            Excel.Worksheet xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
        //            xlWorkSheet.PageSetup.LeftMargin = 10;
        //            xlWorkSheet.PageSetup.RightMargin = 10;
        //            xlWorkSheet.PageSetup.Orientation = Microsoft.Office.Interop.Excel.XlPageOrientation.xlLandscape;

        //            xlWorkSheet.Name = "Bitacora de cambio de precios";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[1, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 14;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            xlWorkSheet.Cells[1, 1] = empresa.nombre;

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[2, 1], xlWorkSheet.Cells[2, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 12;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            xlWorkSheet.Cells[2, 1] = "RESUMEN DE RESULTADOS (INGRESOS/EGRESOS)";


        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[3, 1], xlWorkSheet.Cells[3, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Italic = true;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            if (isMes)
        //            {
        //                xlWorkSheet.Cells[3, 1] = ((Mes)fechaInicial.Month).ToString() + " del " + fechaInicial.Year;
        //            }
        //            else
        //            {
        //                xlWorkSheet.Cells[3, 1] = "Del " + fechaInicial.ToString("dd MMMM yyyy") + " al " + fechaFinal.ToString("dd MMMM yyyy");
        //            }

        //            xlWorkSheet.Cells[4, 1] = "UNIDAD";
        //            xlWorkSheet.Cells[4, 2] = "FLETES";
        //            xlWorkSheet.Cells[4, 3] = "COMBUSTIBLE";
        //            xlWorkSheet.Cells[4, 4] = "MANTENIMIENTO";
        //            xlWorkSheet.Cells[4, 5] = "LLANTAS";
        //            xlWorkSheet.Cells[4, 6] = "LAVADERO";
        //            xlWorkSheet.Cells[4, 7] = "UTILIDAD";
        //            int i = 4;
        //            foreach (IngresosEgresos ingEgre in lista)
        //            {
        //                i++;
        //                xlWorkSheet.Cells[i, 1] = ingEgre.unidad;
        //                xlWorkSheet.Cells[i, 2] = ingEgre.fletes.ToString("C4");
        //                xlWorkSheet.Cells[i, 3] = ingEgre.combustible.ToString("C4");
        //                xlWorkSheet.Cells[i, 4] = ingEgre.mantenimiento.ToString("C4");
        //                xlWorkSheet.Cells[i, 5] = ingEgre.llantas.ToString("C4");
        //                xlWorkSheet.Cells[i, 6] = ingEgre.lavadero.ToString("C4");
        //                xlWorkSheet.Cells[i, 7] = ingEgre.utilidad.ToString("C4");
        //            }
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[4, 1], xlWorkSheet.Cells[i, 7]];
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.Worksheet.ListObjects.AddEx(
        //                                                        SourceType: Excel.XlListObjectSourceType.xlSrcRange,
        //                                                        Source: excelCellrange,
        //                                                        XlListObjectHasHeaders: Excel.XlYesNoGuess.xlYes);
        //            excelCellrange.Font.Size = 9;
        //            excelCellrange.EntireColumn.AutoFit();
        //            xlWorkBook.SaveAs(archivo + ".xlsx");
        //            rutaExcel = archivo + ".xlsx";

        //            bool resp = convertirExcelToSps(archivo);
        //            if (resp)
        //            {
        //                imprimirReporte(archivo + ".xps");
        //            }
        //            return resp;
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        MessageBox.Show(e.Message);
        //        xlWorkBook.Close();
        //        xlApp.Quit();
        //        return false;
        //    }
        //    finally
        //    {
        //        xlWorkBook.Close();
        //        xlApp.Quit();
        //        GC.WaitForPendingFinalizers();
        //        GC.Collect();
        //        GC.WaitForPendingFinalizers();
        //        GC.Collect();
        //    }
        //}

        //internal bool ReporteBitacoraPrecios(List<BitacoraCambioPrecios> listBitacora, bool isMes, DateTime fechaInicio, DateTime fechaFin)
        //{
        //    #region inicio
        //    string archivo = "";
        //    try
        //    {
        //        string path = System.Windows.Forms.Application.StartupPath;
        //        if (!Directory.Exists(path + "\\reportes\\"))
        //        {
        //            DirectoryInfo dir = Directory.CreateDirectory(path + "\\reportes\\");
        //        }

        //        //archivo = string.Format(@"{0}{1}", path + "\\reportes\\", cartaPorte.NumGuiaId);
        //        archivo = string.Format(@"{0}{1}", path + "\\reportes\\", "reporte");

        //        if (File.Exists(archivo + ".xlsx"))
        //        {
        //            File.Delete(archivo + ".xlsx");
        //        }
        //        if (File.Exists(archivo + ".xps"))
        //        {
        //            File.Delete(archivo + ".xps");
        //        }
        //        //".xps"
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message);
        //        return false;
        //    }
        //    #endregion
        //    Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
        //    Excel.Workbook xlWorkBook = xlApp.Workbooks.Add();
        //    //Microsoft.Office.Interop.Excel.Worksheet excelSheet;
        //    Microsoft.Office.Interop.Excel.Range excelCellrange;
        //    try
        //    {

        //        if (xlApp == null)
        //        {
        //            MessageBox.Show("Excel is not properly installed!!");
        //            return false;
        //        }
        //        else
        //        {
        //            Excel.Worksheet xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
        //            xlWorkSheet.PageSetup.LeftMargin = 10;
        //            xlWorkSheet.PageSetup.RightMargin = 10;
        //            xlWorkSheet.PageSetup.Orientation = Microsoft.Office.Interop.Excel.XlPageOrientation.xlLandscape;

        //            xlWorkSheet.Name = "Bitacora de cambio de precios";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[1, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 14;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            xlWorkSheet.Cells[1, 1] = "BITACORA DE CAMBIO DE PRECIOS";


        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[2, 1], xlWorkSheet.Cells[2, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Italic = true;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            if (isMes)
        //            {
        //                xlWorkSheet.Cells[2, 1] = ((Mes)fechaInicio.Month).ToString() + " del " + fechaInicio.Year;
        //            }
        //            else
        //            {
        //                xlWorkSheet.Cells[2, 1] = "Del " + fechaInicio.ToString("dd MMMM yyyy") + " al " + fechaFin.ToString("dd MMMM yyyy");
        //            }

        //            xlWorkSheet.Cells[4, 1] = "FECHA";
        //            xlWorkSheet.Cells[4, 2] = "CLIENTE";
        //            xlWorkSheet.Cells[4, 3] = "DESTINO";
        //            xlWorkSheet.Cells[4, 4] = "TIPO PRECIO";
        //            xlWorkSheet.Cells[4, 5] = "PRECIO ANTERIOR";
        //            xlWorkSheet.Cells[4, 6] = "PRECIO NUEVO";
        //            xlWorkSheet.Cells[4, 7] = "USUARIO";
        //            int i = 4;
        //            foreach (BitacoraCambioPrecios bitacora in listBitacora)
        //            {
        //                foreach (DetallesBitacoraCambioPrecios detelle in bitacora.listaDetalles)
        //                {
        //                    i++;
        //                    xlWorkSheet.Cells[i, 1] = bitacora.fecha.ToString("dd/MMMM/yyyy");
        //                    xlWorkSheet.Cells[i, 2] = bitacora.cliente;
        //                    xlWorkSheet.Cells[i, 3] = detelle.destino;
        //                    xlWorkSheet.Cells[i, 4] = bitacora.tipoPrecio.ToString();
        //                    xlWorkSheet.Cells[i, 5] = bitacora.precioAnterior.ToString("C4");
        //                    xlWorkSheet.Cells[i, 6] = bitacora.precioNuevo.ToString("C4");
        //                    xlWorkSheet.Cells[i, 7] = bitacora.usuario;
        //                }

        //            }
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[4, 1], xlWorkSheet.Cells[i, 7]];
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.Worksheet.ListObjects.AddEx(
        //                                                        SourceType: Excel.XlListObjectSourceType.xlSrcRange,
        //                                                        Source: excelCellrange,
        //                                                        XlListObjectHasHeaders: Excel.XlYesNoGuess.xlYes);
        //            excelCellrange.Font.Size = 8;
        //            excelCellrange.EntireColumn.AutoFit();
        //            xlWorkBook.SaveAs(archivo + ".xlsx");
        //            rutaExcel = archivo + ".xlsx";

        //            bool resp = convertirExcelToSps(archivo);
        //            if (resp)
        //            {
        //                imprimirReporte(archivo + ".xps");
        //            }
        //            return resp;
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        MessageBox.Show(e.Message);
        //        return false;
        //    }
        //    finally
        //    {
        //        xlWorkBook.Close();
        //        xlApp.Quit();
        //        GC.WaitForPendingFinalizers();
        //        GC.Collect();
        //        GC.WaitForPendingFinalizers();
        //        GC.Collect();
        //    }
        //}

        //internal bool ReporteBitacoraSalidasEntradas(DateTime fechaInicio, DateTime fechaFin, List<BitacoraEntradaSalidas> lisBitacora, bool isMes)
        //{
        //    #region inicio
        //    string archivo = "";
        //    try
        //    {
        //        string path = System.Windows.Forms.Application.StartupPath;
        //        if (!Directory.Exists(path + "\\reportes\\"))
        //        {
        //            DirectoryInfo dir = Directory.CreateDirectory(path + "\\reportes\\");
        //        }

        //        //archivo = string.Format(@"{0}{1}", path + "\\reportes\\", cartaPorte.NumGuiaId);
        //        archivo = string.Format(@"{0}{1}", path + "\\reportes\\", "reporte");

        //        if (File.Exists(archivo + ".xlsx"))
        //        {
        //            File.Delete(archivo + ".xlsx");
        //        }
        //        if (File.Exists(archivo + ".xps"))
        //        {
        //            File.Delete(archivo + ".xps");
        //        }
        //        //".xps"
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message);
        //        return false;
        //    }
        //    #endregion
        //    Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
        //    Excel.Workbook xlWorkBook = xlApp.Workbooks.Add();
        //    //Microsoft.Office.Interop.Excel.Worksheet excelSheet;
        //    Microsoft.Office.Interop.Excel.Range excelCellrange;
        //    try
        //    {

        //        if (xlApp == null)
        //        {
        //            MessageBox.Show("Excel is not properly installed!!");
        //            return false;
        //        }
        //        else
        //        {
        //            Excel.Worksheet xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
        //            xlWorkSheet.PageSetup.LeftMargin = 10;
        //            xlWorkSheet.PageSetup.RightMargin = 10;
        //            xlWorkSheet.PageSetup.Orientation = Microsoft.Office.Interop.Excel.XlPageOrientation.xlLandscape;

        //            xlWorkSheet.Name = "Bitacora de cambio de precios";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[1, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 14;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            xlWorkSheet.Cells[1, 1] = "BITACORA DE ENTRADAS/SALIDAS A BASE";


        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[2, 1], xlWorkSheet.Cells[2, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Italic = true;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            if (isMes)
        //            {
        //                xlWorkSheet.Cells[2, 1] = ((Mes)fechaInicio.Month).ToString() + " del " + fechaInicio.Year;
        //            }
        //            else
        //            {
        //                xlWorkSheet.Cells[2, 1] = "Del " + fechaInicio.ToString("dd MMMM yyyy") + " al " + fechaFin.ToString("dd MMMM yyyy");
        //            }

        //            xlWorkSheet.Cells[4, 1] = "FECHA ENTRADA";
        //            xlWorkSheet.Cells[4, 2] = "UNIDAD TRANS";
        //            xlWorkSheet.Cells[4, 3] = "FECHA SALIDA";
        //            xlWorkSheet.Cells[4, 4] = "MOTIVOS";
        //            xlWorkSheet.Cells[4, 5] = "FOLIO";
        //            xlWorkSheet.Cells[4, 6] = "CHOFER";
        //            xlWorkSheet.Cells[4, 7] = "GUARDIA";
        //            int i = 4;
        //            foreach (BitacoraEntradaSalidas bitacora in lisBitacora)
        //            {
        //                i++;
        //                xlWorkSheet.Cells[i, 1] = bitacora.fechaEntrada.ToString("dd/MMMM/yyyy HH:mm:ss");
        //                xlWorkSheet.Cells[i, 2] = bitacora.unidad;
        //                xlWorkSheet.Cells[i, 3] = bitacora.fechaSalida == null ? "" : Convert.ToDateTime(bitacora.fechaSalida).ToString("dd/MMMM/yyyy HH:mm:ss");
        //                xlWorkSheet.Cells[i, 4] = concadenarMotivos(bitacora.listMotivos);
        //                xlWorkSheet.Cells[i, 5] = bitacora.isOrden ? bitacora.folio.ToString() : "";
        //                xlWorkSheet.Cells[i, 6] = bitacora.chofer;
        //                xlWorkSheet.Cells[i, 7] = bitacora.guardia;
        //            }
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[4, 1], xlWorkSheet.Cells[i, 7]];
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.Worksheet.ListObjects.AddEx(
        //                                                        SourceType: Excel.XlListObjectSourceType.xlSrcRange,
        //                                                        Source: excelCellrange,
        //                                                        XlListObjectHasHeaders: Excel.XlYesNoGuess.xlYes);
        //            excelCellrange.Font.Size = 8;
        //            excelCellrange.EntireColumn.AutoFit();
        //            xlWorkBook.SaveAs(archivo + ".xlsx");
        //            rutaExcel = archivo + ".xlsx";

        //            bool resp = convertirExcelToSps(archivo);
        //            if (resp)
        //            {
        //                imprimirReporte(archivo + ".xps");
        //            }
        //            return resp;
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        MessageBox.Show(e.Message);
        //        return false;
        //    }
        //    finally
        //    {
        //        xlWorkBook.Close();
        //        xlApp.Quit();
        //        GC.WaitForPendingFinalizers();
        //        GC.Collect();
        //        GC.WaitForPendingFinalizers();
        //        GC.Collect();
        //    }
        //}

        //private string concadenarMotivos(List<string> listMotivos)
        //{
        //    try
        //    {
        //        string cadenaMotivos = "";
        //        foreach (var item in listMotivos)
        //        {
        //            cadenaMotivos += ", " + item;
        //        }
        //        return cadenaMotivos.Trim(',').Trim();
        //    }
        //    catch (Exception ex)
        //    {
        //        return "";
        //    }
        //}

        //public bool imprimirValesCarga(List<ValeCarga> listValesCarga, Viaje viaje)
        //{
        //    #region inicio
        //    string archivo = "";
        //    try
        //    {
        //        string path = System.Windows.Forms.Application.StartupPath;
        //        if (!Directory.Exists(path + "\\reportes\\"))
        //        {
        //            DirectoryInfo dir = Directory.CreateDirectory(path + "\\reportes\\");
        //        }

        //        //archivo = string.Format(@"{0}{1}", path + "\\reportes\\", cartaPorte.NumGuiaId);
        //        archivo = string.Format(@"{0}{1}", path + "\\reportes\\", "reporte");

        //        if (File.Exists(archivo + ".xlsx"))
        //        {
        //            File.Delete(archivo + ".xlsx");
        //        }
        //        if (File.Exists(archivo + ".xps"))
        //        {
        //            File.Delete(archivo + ".xps");
        //        }
        //        //".xps"
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message);
        //        return false;
        //    }
        //    #endregion
        //    Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
        //    Excel.Workbook xlWorkBook = xlApp.Workbooks.Add();
        //    //Microsoft.Office.Interop.Excel.Worksheet excelSheet;
        //    Microsoft.Office.Interop.Excel.Range excelCellrange;
        //    try
        //    {

        //        if (xlApp == null)
        //        {
        //            MessageBox.Show("Excel is not properly installed!!");
        //            return false;
        //        }
        //        else
        //        {
        //            Excel.Worksheet xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
        //            xlWorkSheet.PageSetup.TopMargin = 25;
        //            xlWorkSheet.PageSetup.BottomMargin = 12;
        //            xlWorkSheet.PageSetup.LeftMargin = 5;
        //            xlWorkSheet.PageSetup.RightMargin = 5;
        //            xlWorkSheet.PageSetup.HeaderMargin = 0;
        //            xlWorkSheet.PageSetup.FooterMargin = 0;
        //            //xlWorkSheet.PageSetup.Orientation = Microsoft.Office.Interop.Excel.XlPageOrientation.xlLandscape;

        //            xlWorkSheet.Name = "Bitacora de cambio de precios";

        //            int i = 2;

        //            foreach (var item in listValesCarga)
        //            {
        //                int PrimerInicio = i;
        //                #region ENCABEZADO
        //                excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 1], xlWorkSheet.Cells[i, 4]];
        //                excelCellrange.Merge(true);
        //                excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //                excelCellrange.Font.FontStyle = "Bold";
        //                xlWorkSheet.Cells[i, 1] = "VALE DE CARGA";
        //                i++;
        //                excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 1], xlWorkSheet.Cells[i, 3]];
        //                excelCellrange.Merge(true);
        //                excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //                excelCellrange.Font.FontStyle = "Bold";
        //                xlWorkSheet.Cells[i, 1] = "TRANSPORTADO POR FLETERA ATLANTE S.A DE C.V ";

        //                excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 4], xlWorkSheet.Cells[i, 4]];
        //                excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //                excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Red);
        //                excelCellrange.Font.FontStyle = "Bold";
        //                xlWorkSheet.Cells[i, 4] = item.idValeCarga;
        //                i++;
        //                #endregion

        //                #region 1
        //                int INICIO = i;
        //                excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 1], xlWorkSheet.Cells[i, 2]];
        //                excelCellrange.ColumnWidth = 20;
        //                xlWorkSheet.Cells[i, 1] = "FECHA:";
        //                xlWorkSheet.Cells[i, 2] = item.cartaPorte != 0 ? viaje.FechaHoraViaje.ToString("dd/MMMM/yyyy") : "___________________________";

        //                excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 3], xlWorkSheet.Cells[i, 4]];
        //                excelCellrange.ColumnWidth = 25;
        //                xlWorkSheet.Cells[i, 3] = "No. VIAJES:";
        //                xlWorkSheet.Cells[i, 4] = "__________";
        //                i++;
        //                excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 1], xlWorkSheet.Cells[i, 2]];
        //                excelCellrange.ColumnWidth = 20;
        //                xlWorkSheet.Cells[i, 1] = "CARGA EN:";
        //                xlWorkSheet.Cells[i, 2] = item.cartaPorte != 0 ? item.origen.nombreOrigen : "__________";

        //                excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 3], xlWorkSheet.Cells[i, 4]];
        //                excelCellrange.ColumnWidth = 25;
        //                xlWorkSheet.Cells[i, 3] = "FOLIO DE BANCO DE MATERIAL:";
        //                xlWorkSheet.Cells[i, 4] = "__________";
        //                i++;
        //                excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 1], xlWorkSheet.Cells[i, 2]];
        //                excelCellrange.ColumnWidth = 20;
        //                xlWorkSheet.Cells[i, 1] = "UNIDADES:";
        //                xlWorkSheet.Cells[i, 2] = (viaje.tractor.clave + "/") + (viaje.remolque.clave) 
        //                    + (viaje.dolly != null ? "/" +viaje.dolly.clave : "")
        //                    + (viaje.remolque2 != null ? "/" + viaje.remolque2.clave : "");

        //                excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 3], xlWorkSheet.Cells[i, 4]];
        //                excelCellrange.ColumnWidth = 25;
        //                xlWorkSheet.Cells[i, 3] = "NOMBRE CHOFER:";

        //                excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 4], xlWorkSheet.Cells[i, 4]];
        //                excelCellrange.ColumnWidth = 35;
        //                xlWorkSheet.Cells[i, 4] = viaje.operador.nombre;
        //                i++;


        //                excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 1], xlWorkSheet.Cells[i, 2]];
        //                excelCellrange.ColumnWidth = 20;
        //                xlWorkSheet.Cells[i, 1] = "AUTORIZA CLIENTE:";

        //                excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 2], xlWorkSheet.Cells[i, 4]];
        //                excelCellrange.Merge(true);
        //                xlWorkSheet.Cells[i, 2] = viaje.cliente.nombre;

        //                excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[INICIO, 1], xlWorkSheet.Cells[i, 4]];
        //                excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;

        //                excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[INICIO, 1], xlWorkSheet.Cells[i, 1]];
        //                excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //                excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[INICIO, 3], xlWorkSheet.Cells[i, 3]];
        //                excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //                i++;
        //                i++;
        //                #endregion

        //                #region detalles
        //                excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 1], xlWorkSheet.Cells[i, 3]];
        //                excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //                xlWorkSheet.Cells[i, 1] = "CANTIDAD:";
        //                xlWorkSheet.Cells[i, 2] = "DESCRIPCION:";
        //                xlWorkSheet.Cells[i, 3] = "DESTINO:";
        //                i++;

        //                xlWorkSheet.Cells[i, 1] = item.cartaPorte != 0 ? item.volDescarga.ToString() : "";
        //                xlWorkSheet.Cells[i, 2] = item.cartaPorte != 0 ? item.producto.descripcion : "";
        //                xlWorkSheet.Cells[i, 3] = item.cartaPorte != 0 ? item.destino.descripcion : "";
        //                excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 1], xlWorkSheet.Cells[i, 3]];
        //                excelCellrange.Font.Bold = true;
        //                excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //                Excel.Borders border2 = excelCellrange.Borders;
        //                border2.LineStyle = Excel.XlLineStyle.xlContinuous;
        //                border2.Weight = 2d;
        //                i++;
        //                i++;
        //                #endregion

        //                #region firmas y sellos
        //                excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 1], xlWorkSheet.Cells[i, 3]];
        //                excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //                xlWorkSheet.Cells[i, 1] = "RECIBE MATERIAL:";
        //                xlWorkSheet.Cells[i, 2] = "SELLO DESTINO:";
        //                xlWorkSheet.Cells[i, 3] = "LUGAR DE TIRO:";
        //                i += 4;

        //                xlWorkSheet.Cells[i, 1] = "__________________";

        //                excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i - 4, 2], xlWorkSheet.Cells[i, 2]];
        //                excelCellrange.Cells.BorderAround2(Excel.XlLineStyle.xlContinuous);

        //                xlWorkSheet.Cells[i - 2, 3] = "______________________________";
        //                xlWorkSheet.Cells[i, 3] = "______________________________";
        //                i++;
        //                xlWorkSheet.Cells[i, 1] = "NOMBRE Y FIRMA";
        //                xlWorkSheet.Cells[i, 2] = "HORA DE DESCARGA";
        //                #endregion
        //                excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[PrimerInicio, 1], xlWorkSheet.Cells[i, 4]];
        //                excelCellrange.Font.Size = 10;
        //                excelCellrange.RowHeight = 15;
        //                excelCellrange.Cells.BorderAround2(Excel.XlLineStyle.xlContinuous);
        //                i += 2;
        //            }
        //            xlWorkBook.SaveAs(archivo + ".xlsx");
        //            ap.PrintDialog printDialog1 = new ap.PrintDialog();
        //            printDialog1.PrinterSettings.Copies = 2;
        //            ap.DialogResult result = printDialog1.ShowDialog();                    
        //            var ss = printDialog1.PrinterSettings.PrinterName;
        //            xlWorkSheet.PrintOutEx(1, 10, printDialog1.PrinterSettings.Copies, false, ss); //linea para imprimir
        //            rutaExcel = archivo + ".xlsx";
        //            return true;
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        MessageBox.Show(e.Message);
        //        return false;
        //    }
        //    finally
        //    {
        //        xlWorkBook.Close();
        //        xlApp.Quit();
        //        GC.WaitForPendingFinalizers();
        //        GC.Collect();
        //        GC.WaitForPendingFinalizers();
        //        GC.Collect();
        //    }
        //}

        //public void imprimir(string archivo)
        //{
        //    try
        //    {
        //        string path = ConvertWordtoXps(archivo);
        //        using (XpsDocument xpsDocument = new XpsDocument(path, System.IO.FileAccess.ReadWrite))
        //        {
        //            documentViewer.Document = xpsDocument.GetFixedDocumentSequence();
        //        }

        //        File.Delete(path);
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message);
        //    }
        //}

        //public bool imprimirReporte(string ruta)
        //{
        //    try
        //    {
        //        //XpsDocument xpsDocument = new XpsDocument(ruta, System.IO.FileAccess.ReadWrite);
        //        //documentViewer.Document = this.ConvertPptxDocToXPSDoc(ruta, this.newXPSDocumentName).GetFixedDocumentSequence();
        //        //using (XpsDocument xpsDocument = new XpsDocument(ruta, FileAccess.Read, CompressionOption.NotCompressed))
        //        //{
        //        XpsDocument xpsDocument = new XpsDocument(ruta, FileAccess.Read, CompressionOption.NotCompressed);
        //        var documento = xpsDocument.GetFixedDocumentSequence();
        //        //documentViewer.Document = new DocumentViewer();

        //        documentViewer.Document = documento;
        //        xpsDocument.Close();
        //        return true;
        //        //}
        //    }
        //    catch (Exception e)
        //    {
        //        MessageBox.Show(e.Message);
        //        return false;
        //    }
        //    finally
        //    {
        //        GC.WaitForPendingFinalizers();
        //        GC.Collect();
        //        GC.WaitForPendingFinalizers();
        //        GC.Collect();
        //    }
        //}

        //internal bool documentoReportePenalizaciones(List<Penalizacion> list, DateTime fechaInicio, DateTime fechaFin)
        //{
        //    string archivo = "";
        //    try
        //    {
        //        string path = System.Windows.Forms.Application.StartupPath;
        //        if (!Directory.Exists(path + "\\reportes\\"))
        //        {
        //            DirectoryInfo dir = Directory.CreateDirectory(path + "\\reportes\\");
        //        }

        //        //archivo = string.Format(@"{0}{1}", path + "\\reportes\\", cartaPorte.NumGuiaId);
        //        archivo = string.Format(@"{0}{1}", path + "\\reportes\\", "reporte");

        //        if (File.Exists(archivo + ".xlsx"))
        //        {
        //            File.Delete(archivo + ".xlsx");
        //        }
        //        if (File.Exists(archivo + ".xps"))
        //        {
        //            File.Delete(archivo + ".xps");
        //        }
        //        //".xps"
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message);
        //        return false;
        //    }

        //    Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
        //    Excel.Workbook xlWorkBook = xlApp.Workbooks.Add();
        //    //Microsoft.Office.Interop.Excel.Worksheet excelSheet;
        //    Microsoft.Office.Interop.Excel.Range excelCellrange;
        //    try
        //    {

        //        if (xlApp == null)
        //        {
        //            MessageBox.Show("Excel is not properly installed!!");
        //            return false;
        //        }
        //        else
        //        {

        //            Excel.Worksheet xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
        //            xlWorkSheet.PageSetup.Orientation = Microsoft.Office.Interop.Excel.XlPageOrientation.xlLandscape;
        //            xlWorkSheet.Name = "Penalizaciones";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[1, 10]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 8;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            //xlWorkSheet.Cells[1, 1] = cliente.nombre;

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[2, 1], xlWorkSheet.Cells[2, 10]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            xlWorkSheet.Cells[2, 1] = "Listado de Penalizaciones por Fechas del " + fechaInicio.ToString("dd/MM/yyyy") + " al " + fechaFin.ToString("dd/MM/yyyy");

        //            xlWorkSheet.Cells[3, 1] = "Folio Viaje";
        //            xlWorkSheet.Cells[3, 2] = "Fecha";
        //            xlWorkSheet.Cells[3, 3] = "Capacidad Max";
        //            xlWorkSheet.Cells[3, 4] = "Capacidad Min";
        //            xlWorkSheet.Cells[3, 5] = "Porcentaje";
        //            xlWorkSheet.Cells[3, 6] = "Total";
        //            xlWorkSheet.Cells[3, 7] = "Diferencia";
        //            xlWorkSheet.Cells[3, 8] = "Precio";
        //            xlWorkSheet.Cells[3, 9] = "Importe";

        //            //decimal sumaToneladas = 0m;
        //            //decimal sumaIva = 0m;
        //            //decimal sumaRetencion = 0m;
        //            //decimal sumaImporte = 0m;
        //            //decimal sumaKM = 0m;
        //            //decimal total = 0m;
        //            int i = 4;
        //            foreach (Penalizacion penalizacion in list)
        //            {
        //                //foreach (CartaPorte cartaPorte in viaje.listDetalles)
        //                //{
        //                //sumaToneladas += cartaPorte.volumenDescarga;
        //                //sumaIva += cartaPorte.ImporIVA;
        //                //sumaRetencion += cartaPorte.ImpRetencion;
        //                //sumaKM += cartaPorte.zonaSelect.km;
        //                //sumaImporte += cartaPorte.importeReal;
        //                //total += cartaPorte.importeReal + (cartaPorte.ImporIVA - cartaPorte.ImpRetencion);
        //                //}
        //                xlWorkSheet.Cells[i, 1] = penalizacion.idViaje;
        //                xlWorkSheet.Cells[i, 2] = penalizacion.fecha.ToString("dd/MM/yyyy HH:mm:00");
        //                xlWorkSheet.Cells[i, 3] = penalizacion.cargaMinima;
        //                xlWorkSheet.Cells[i, 4] = penalizacion.cargaMinima;
        //                xlWorkSheet.Cells[i, 5] = penalizacion.penalizacion;
        //                xlWorkSheet.Cells[i, 6] = penalizacion.sumaCargas;
        //                xlWorkSheet.Cells[i, 7] = penalizacion.diferencia;
        //                xlWorkSheet.Cells[i, 8] = penalizacion.precio;
        //                xlWorkSheet.Cells[i, 9] = penalizacion.importe;
        //                //foreach (var detalles in viaje.listDetalles)
        //                //{
        //                //    xlWorkSheet.Cells[i, 9] = detalles.zonaSelect.descripcion;
        //                //    xlWorkSheet.Cells[i, 10] = detalles.zonaSelect.km;
        //                //    xlWorkSheet.Cells[i, 11] = detalles.volumenDescarga;
        //                //    xlWorkSheet.Cells[i, 12] = detalles.ImporIVA;
        //                //    xlWorkSheet.Cells[i, 13] = detalles.ImpRetencion;
        //                //    xlWorkSheet.Cells[i, 14] = detalles.importeReal;
        //                //    xlWorkSheet.Cells[i, 15] = detalles.importeReal + (detalles.ImporIVA - detalles.ImpRetencion);
        //                //    i++;
        //                //}


        //                //sumaToneladas = 0m;
        //                //sumaIva = 0m;
        //                //sumaRetencion = 0m;
        //                //sumaImporte = 0m;
        //                //sumaKM = 0m;
        //                //total = 0m;
        //                i++;
        //            }

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 10], xlWorkSheet.Cells[i, 9]];
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Red);
        //            excelCellrange.Font.FontStyle = "Bold";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[3, 1], xlWorkSheet.Cells[3, 9]];
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Blue);
        //            excelCellrange.Font.FontStyle = "Bold";

        //            excelCellrange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Silver);

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[3, 1], xlWorkSheet.Cells[i + 1, 9]];
        //            excelCellrange.Font.Size = 5;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.EntireColumn.AutoFit();

        //            xlWorkBook.SaveAs(archivo + ".xlsx");
        //            rutaExcel = archivo + ".xlsx";

        //            bool resp = convertirExcelToSps(archivo);
        //            if (resp)
        //            {
        //                imprimirReporte(archivo + ".xps");
        //            }
        //            return resp;
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        MessageBox.Show(e.Message);
        //        return false;
        //    }
        //    finally
        //    {
        //        xlWorkBook.Close();
        //        xlApp.Quit();
        //        GC.WaitForPendingFinalizers();
        //        GC.Collect();
        //        GC.WaitForPendingFinalizers();
        //        GC.Collect();
        //    }
        //}

        //class grupoL
        //{
        //    public string concepto { get; set; }
        //    public List<EgresosDetalle> lista { get; set; }
        //}
        //List<grupoL> grupo = new List<grupoL>();
        //List<grupoL> agrupar(List<EgresosDetalle> list)
        //{
        //    foreach (var item in list)
        //    {
        //        //if (grupo.Count == 0)
        //        //{
        //        //    grupo.Add(new grupoL { concepto = item.concepto, lista = new List<EgresosDetalle> { item } });
        //        //    continue;
        //        //}
        //        bool encontro = false;
        //        foreach (var item2 in grupo)
        //        {
        //            if (item2.concepto == item.concepto)
        //            {
        //                item2.lista.Add(item);
        //                encontro = true;
        //            }
        //        }
        //        if (!encontro)
        //        {
        //            grupo.Add(new grupoL { concepto = item.concepto, lista = new List<EgresosDetalle> { item } });
        //        }

        //    }
        //    return grupo;
        //}
        //internal bool reporteIngresosEgresosDetallado(UnidadTransporte unidad, List<IngresosDetalle> listIngresos, List<EgresosDetalle> listEgresos, DateTime fechaInicio, DateTime fechaFin, bool isMes)
        //{
        //    #region inicio
        //    string archivo = "";
        //    try
        //    {
        //        string path = System.Windows.Forms.Application.StartupPath;
        //        if (!Directory.Exists(path + "\\reportes\\"))
        //        {
        //            DirectoryInfo dir = Directory.CreateDirectory(path + "\\reportes\\");
        //        }

        //        //archivo = string.Format(@"{0}{1}", path + "\\reportes\\", cartaPorte.NumGuiaId);
        //        archivo = string.Format(@"{0}{1}", path + "\\reportes\\", "reporte");

        //        if (File.Exists(archivo + ".xlsx"))
        //        {
        //            File.Delete(archivo + ".xlsx");
        //        }
        //        if (File.Exists(archivo + ".xps"))
        //        {
        //            File.Delete(archivo + ".xps");
        //        }
        //        //".xps"
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message);
        //        return false;
        //    }
        //    #endregion
        //    Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
        //    Excel.Workbook xlWorkBook = xlApp.Workbooks.Add();
        //    //Microsoft.Office.Interop.Excel.Worksheet excelSheet;
        //    Microsoft.Office.Interop.Excel.Range excelCellrange;
        //    try
        //    {

        //        if (xlApp == null)
        //        {
        //            MessageBox.Show("Excel is not properly installed!!");
        //            return false;
        //        }
        //        else
        //        {
        //            Excel.Worksheet xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
        //            xlWorkSheet.PageSetup.LeftMargin = 10;
        //            xlWorkSheet.PageSetup.RightMargin = 10;
        //            //xlWorkSheet.PageSetup.Orientation = Microsoft.Office.Interop.Excel.XlPageOrientation.xlLandscape;

        //            xlWorkSheet.Name = "IngresosEgresos";

        //            int fondo = 0;
        //            if (unidad.empresa.clave == 1)
        //            {
        //                fondo = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkGreen);
        //            }
        //            else
        //            {
        //                fondo = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.SteelBlue);
        //            }

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[1, 5]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 14;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            xlWorkSheet.Cells[1, 1] = unidad.clave + " " + unidad.tipoUnidad.descripcion;


        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[2, 1], xlWorkSheet.Cells[2, 5]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Italic = true;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            if (isMes)
        //            {
        //                xlWorkSheet.Cells[2, 1] = ((Mes)fechaInicio.Month).ToString() + " del " + fechaInicio.Year;
        //            }
        //            else
        //            {
        //                xlWorkSheet.Cells[2, 1] = "Del " + fechaInicio.ToString("dd MMMM yyyy") + " al " + fechaFin.ToString("dd MMMM yyyy");
        //            }

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[3, 1], xlWorkSheet.Cells[3, 5]];
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
        //            excelCellrange.Interior.Color = fondo;
        //            excelCellrange.Merge(true);
        //            xlWorkSheet.Cells[3, 1] = "INGRESOS";

        //            xlWorkSheet.Cells[4, 1] = "FECHA";
        //            xlWorkSheet.Cells[4, 2] = "FACTURA";
        //            xlWorkSheet.Cells[4, 3] = "CLIENTE";
        //            xlWorkSheet.Cells[4, 4] = "CANTIDAD";
        //            xlWorkSheet.Cells[4, 5] = "IMPORTE";
        //            int i = 5;
        //            decimal sumaIngresos = 0m;
        //            foreach (IngresosDetalle item in listIngresos)
        //            {

        //                xlWorkSheet.Cells[i, 1] = item.fecha.ToString("dd/MMM/yyy");
        //                xlWorkSheet.Cells[i, 2] = item.serie + item.movimiento.ToString();
        //                xlWorkSheet.Cells[i, 3] = item.cliente;
        //                xlWorkSheet.Cells[i, 4] = item.cantidad.ToString() + " " + item.um;
        //                xlWorkSheet.Cells[i, 5] = item.importe.ToString("C4");
        //                sumaIngresos += item.importe;
        //                i++;
        //            }

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[4, 1], xlWorkSheet.Cells[i - 1, 5]];
        //            //excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.EntireColumn.AutoFit();
        //            excelCellrange.Worksheet.ListObjects.AddEx(
        //                                                        SourceType: Excel.XlListObjectSourceType.xlSrcRange,
        //                                                        Source: excelCellrange,
        //                                                        XlListObjectHasHeaders: Excel.XlYesNoGuess.xlYes);
        //            i++;
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 4], xlWorkSheet.Cells[i, 5]];
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 9;
        //            excelCellrange.Font.Italic = true;
        //            xlWorkSheet.Cells[i, 4] = "Total:";
        //            xlWorkSheet.Cells[i, 5] = sumaIngresos.ToString("C4");

        //            i += 2;
        //            int iEgresos = i;
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 1], xlWorkSheet.Cells[i, 5]];
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
        //            excelCellrange.Interior.Color = fondo;
        //            excelCellrange.Merge(true);
        //            xlWorkSheet.Cells[i, 1] = "EGRESOS";
        //            i++;

        //            decimal sumaEgresosTOTAL = 0m;
        //            var list = agrupar(listEgresos);

        //            foreach (grupoL item in list)
        //            {
        //                excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 1], xlWorkSheet.Cells[i, 5]];
        //                excelCellrange.Font.FontStyle = "Bold";
        //                excelCellrange.Font.Size = 10;
        //                xlWorkSheet.Cells[i, 1] = item.concepto;

        //                i++;
        //                int indice = i;
        //                decimal sumaEgresos = 0m;
        //                xlWorkSheet.Cells[i, 1] = "FECHA";
        //                xlWorkSheet.Cells[i, 2] = "No ORDEN";
        //                xlWorkSheet.Cells[i, 3] = "PERSONAL";
        //                xlWorkSheet.Cells[i, 4] = "CANTIDAD";
        //                xlWorkSheet.Cells[i, 5] = "IMPORTE";
        //                i++;
        //                foreach (EgresosDetalle detalle in item.lista)
        //                {
        //                    xlWorkSheet.Cells[i, 1] = detalle.fecha.ToString("dd/MMM/yyy");
        //                    xlWorkSheet.Cells[i, 2] = detalle.cadenaFolio;
        //                    xlWorkSheet.Cells[i, 3] = detalle.persona;
        //                    xlWorkSheet.Cells[i, 4] = detalle.cantidad.ToString() + " " + detalle.um;
        //                    xlWorkSheet.Cells[i, 5] = detalle.total.ToString("C4");
        //                    sumaEgresos += detalle.total;
        //                    i++;
        //                }

        //                excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[indice, 1], xlWorkSheet.Cells[i - 1, 5]];
        //                //excelCellrange.Font.Size = 7;
        //                excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //                excelCellrange.EntireColumn.AutoFit();
        //                excelCellrange.Worksheet.ListObjects.AddEx(
        //                                                            SourceType: Excel.XlListObjectSourceType.xlSrcRange,
        //                                                            Source: excelCellrange,
        //                                                            XlListObjectHasHeaders: Excel.XlYesNoGuess.xlYes);

        //                i++;
        //                excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 1], xlWorkSheet.Cells[i, 5]];
        //                excelCellrange.Font.FontStyle = "Bold";
        //                excelCellrange.Font.Size = 9;
        //                excelCellrange.Font.Italic = true;
        //                xlWorkSheet.Cells[i, 4] = "Total:";
        //                xlWorkSheet.Cells[i, 5] = sumaEgresos.ToString("C4");
        //                sumaEgresosTOTAL += sumaEgresos;
        //                i++;
        //            }
        //            i++;
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 1], xlWorkSheet.Cells[i, 5]];
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 9;
        //            excelCellrange.Font.Italic = true;
        //            xlWorkSheet.Cells[i, 4] = "Total Egresos:";
        //            xlWorkSheet.Cells[i, 5] = sumaEgresosTOTAL.ToString("C4");

        //            i += 2;

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 1], xlWorkSheet.Cells[i, 2]];
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 9;
        //            excelCellrange.Font.Italic = true;
        //            excelCellrange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.WhiteSmoke);
        //            xlWorkSheet.Cells[i, 1] = "UTILIDADES:";
        //            xlWorkSheet.Cells[i, 2] = (sumaIngresos - sumaEgresosTOTAL).ToString("C4");

        //            //excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[3, 1], xlWorkSheet.Cells[i, 2]];
        //            //excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            //excelCellrange.ColumnWidth = 20;
        //            //excelCellrange.Worksheet.ListObjects.AddEx(
        //            //                                            SourceType: Excel.XlListObjectSourceType.xlSrcRange,
        //            //                                            Source: excelCellrange,
        //            //                                            XlListObjectHasHeaders: Excel.XlYesNoGuess.xlYes);
        //            //excelCellrange.Font.Size = 8;
        //            ////excelCellrange.EntireColumn.AutoFit();

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[3, 1], xlWorkSheet.Cells[3, 2]];
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[iEgresos, 1], xlWorkSheet.Cells[iEgresos, 2]];
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

        //            xlWorkBook.SaveAs(archivo + ".xlsx");
        //            rutaExcel = archivo + ".xlsx";

        //            bool resp = convertirExcelToSps(archivo);
        //            if (resp)
        //            {
        //                imprimirReporte(archivo + ".xps");
        //            }
        //            return resp;
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        MessageBox.Show(e.Message);
        //        return false;
        //    }
        //    finally
        //    {
        //        xlWorkBook.Close();
        //        xlApp.Quit();
        //        GC.WaitForPendingFinalizers();
        //        GC.Collect();
        //        GC.WaitForPendingFinalizers();
        //        GC.Collect();
        //    }
        //}

        //internal bool reporteIngresosEgresos(UnidadTransporte unidad, List<Ingresos> listIngresos, List<Egresos> listEgresos, DateTime fechaInicio, DateTime fechaFin, bool isMes)
        //{
        //    #region inicio
        //    string archivo = "";
        //    try
        //    {
        //        string path = System.Windows.Forms.Application.StartupPath;
        //        if (!Directory.Exists(path + "\\reportes\\"))
        //        {
        //            DirectoryInfo dir = Directory.CreateDirectory(path + "\\reportes\\");
        //        }

        //        //archivo = string.Format(@"{0}{1}", path + "\\reportes\\", cartaPorte.NumGuiaId);
        //        archivo = string.Format(@"{0}{1}", path + "\\reportes\\", "reporte");

        //        if (File.Exists(archivo + ".xlsx"))
        //        {
        //            File.Delete(archivo + ".xlsx");
        //        }
        //        if (File.Exists(archivo + ".xps"))
        //        {
        //            File.Delete(archivo + ".xps");
        //        }
        //        //".xps"
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message);
        //        return false;
        //    }
        //    #endregion
        //    Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
        //    Excel.Workbook xlWorkBook = xlApp.Workbooks.Add();
        //    //Microsoft.Office.Interop.Excel.Worksheet excelSheet;
        //    Microsoft.Office.Interop.Excel.Range excelCellrange;
        //    try
        //    {

        //        if (xlApp == null)
        //        {
        //            MessageBox.Show("Excel is not properly installed!!");
        //            return false;
        //        }
        //        else
        //        {
        //            Excel.Worksheet xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
        //            //xlWorkSheet.PageSetup.LeftMargin = 10;
        //            //xlWorkSheet.PageSetup.RightMargin = 10;
        //            //xlWorkSheet.PageSetup.Orientation = Microsoft.Office.Interop.Excel.XlPageOrientation.xlLandscape;

        //            xlWorkSheet.Name = "IngresosEgresos";

        //            int fondo = 0;
        //            if (unidad.empresa.clave == 1)
        //            {
        //                fondo = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkGreen);
        //            }
        //            else
        //            {
        //                fondo = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.SteelBlue);
        //            }

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[1, 2]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 14;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            xlWorkSheet.Cells[1, 1] = unidad.clave + " " + unidad.tipoUnidad.descripcion;


        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[2, 1], xlWorkSheet.Cells[2, 2]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Italic = true;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            if (isMes)
        //            {
        //                xlWorkSheet.Cells[2, 1] = ((Mes)fechaInicio.Month).ToString() + " del " + fechaInicio.Year;
        //            }
        //            else
        //            {
        //                xlWorkSheet.Cells[2, 1] = "Del " + fechaInicio.ToString("dd MMMM yyyy") + " al " + fechaFin.ToString("dd MMMM yyyy");
        //            }

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[3, 1], xlWorkSheet.Cells[3, 2]];
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
        //            excelCellrange.Interior.Color = fondo;
        //            excelCellrange.Merge(true);
        //            xlWorkSheet.Cells[3, 1] = "INGRESOS";
        //            int i = 4;
        //            decimal sumaIngresos = 0m;
        //            foreach (Ingresos item in listIngresos)
        //            {
        //                if (item.total == 0)
        //                {
        //                    continue;
        //                }
        //                xlWorkSheet.Cells[i, 1] = item.concepto;
        //                xlWorkSheet.Cells[i, 2] = item.total.ToString("C4");
        //                sumaIngresos += item.total;
        //                i++;
        //            }

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 1], xlWorkSheet.Cells[i, 2]];
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 9;
        //            excelCellrange.Font.Italic = true;
        //            xlWorkSheet.Cells[i, 1] = "Total:";
        //            xlWorkSheet.Cells[i, 2] = sumaIngresos.ToString("C4");

        //            i += 2;
        //            int iEgresos = i;
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 1], xlWorkSheet.Cells[i, 2]];
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
        //            excelCellrange.Interior.Color = fondo;
        //            excelCellrange.Merge(true);
        //            xlWorkSheet.Cells[i, 1] = "EGRESOS";
        //            i++;
        //            decimal sumaEgresos = 0m;
        //            foreach (Egresos item in listEgresos)
        //            {
        //                xlWorkSheet.Cells[i, 1] = item.concepto;
        //                xlWorkSheet.Cells[i, 2] = item.total.ToString("C4");
        //                sumaEgresos += item.total;
        //                i++;
        //            }

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 1], xlWorkSheet.Cells[i, 2]];
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 9;
        //            excelCellrange.Font.Italic = true;
        //            xlWorkSheet.Cells[i, 1] = "Total:";
        //            xlWorkSheet.Cells[i, 2] = sumaEgresos.ToString("C4");
        //            i += 2;


        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 1], xlWorkSheet.Cells[i, 2]];
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 9;
        //            excelCellrange.Font.Italic = true;
        //            excelCellrange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.WhiteSmoke);
        //            xlWorkSheet.Cells[i, 1] = "UTILIDADES:";
        //            xlWorkSheet.Cells[i, 2] = (sumaIngresos - sumaEgresos).ToString("C4");

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[3, 1], xlWorkSheet.Cells[i, 2]];
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            excelCellrange.ColumnWidth = 20;
        //            //excelCellrange.Worksheet.ListObjects.AddEx(
        //            //                                            SourceType: Excel.XlListObjectSourceType.xlSrcRange,
        //            //                                            Source: excelCellrange,
        //            //                                            XlListObjectHasHeaders: Excel.XlYesNoGuess.xlYes);
        //            //excelCellrange.Font.Size = 8;
        //            ////excelCellrange.EntireColumn.AutoFit();

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[3, 1], xlWorkSheet.Cells[3, 2]];
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[iEgresos, 1], xlWorkSheet.Cells[iEgresos, 2]];
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

        //            xlWorkBook.SaveAs(archivo + ".xlsx");
        //            rutaExcel = archivo + ".xlsx";

        //            bool resp = convertirExcelToSps(archivo);
        //            if (resp)
        //            {
        //                imprimirReporte(archivo + ".xps");
        //            }
        //            return resp;
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        MessageBox.Show(e.Message);
        //        return false;
        //    }
        //    finally
        //    {
        //        xlWorkBook.Close();
        //        xlApp.Quit();
        //        GC.WaitForPendingFinalizers();
        //        GC.Collect();
        //        GC.WaitForPendingFinalizers();
        //        GC.Collect();
        //    }
        //}

        //private string ConvertWordtoXps(string wordDocName)
        //{

        //    Microsoft.Office.Interop.Word.Application wordApp = new Microsoft.Office.Interop.Word.Application();
        //    wordApp.Documents.Open(wordDocName, ConfirmConversions: false, ReadOnly: true);
        //    string xpsFileName = String.Concat((System.Windows.Forms.Application.StartupPath + "\\reportes\\"), System.IO.Path.GetFileNameWithoutExtension(wordDocName), ".xps");

        //    try
        //    {
        //        if (File.Exists(xpsFileName))
        //        {
        //            File.Delete(xpsFileName);
        //        }
        //        wordApp.ActiveDocument.SaveAs(xpsFileName, FileFormat: Microsoft.Office.Interop.Word.WdSaveFormat.wdFormatXPS);
        //        wordApp.ActiveDocument.Close();

        //        return xpsFileName;
        //    }
        //    catch (Exception e)
        //    {
        //        MessageBox.Show(e.Message);
        //    }
        //    finally
        //    {
        //        wordApp.Quit();

        //        //((Microsoft.Office.Interop.Word._Application)wordApp).Quit(SaveChanges: false, OriginalFormat: nullObject, RouteDocument: nullObject);
        //    }
        //    return null;
        //}

        //private void MiDocumentoCartaPorte(Viaje cartaPorte, string archivo)
        //{
        //    //Objeto del Tipo Word Application 
        //    Microsoft.Office.Interop.Word.Application objWordApplication;
        //    //Objeto del Tipo Word Document 
        //    Microsoft.Office.Interop.Word.Document objWordDocument;
        //    // Objeto para interactuar con el Interop 
        //    Object oMissing = System.Reflection.Missing.Value;
        //    //Creamos una instancia de una Aplicación Word. 
        //    objWordApplication = new Microsoft.Office.Interop.Word.Application();
        //    //A la aplicación Word, le añadimos un documento. 
        //    objWordDocument = objWordApplication.Documents.Add(ref oMissing, ref oMissing,
        //                                                     ref oMissing, ref oMissing);
        //    try
        //    {
        //        //objWordDocument.Password = "SoftNG";
        //        //Activamos el documento recien creado, de forma que podamos escribir en el 
        //        objWordDocument.Activate();
        //        //Empezamos a escribir



        //        object oEndOfDoc = "\\endofdoc";
        //        Microsoft.Office.Interop.Word.Table oTable;
        //        Microsoft.Office.Interop.Word.Range wrdRng = objWordDocument.Bookmarks.get_Item(ref oEndOfDoc).Range;
        //        oTable = objWordDocument.Tables.Add(wrdRng, cartaPorte.listDetalles.Count + 14, 5, ref oMissing, ref oMissing);


        //        //oTable.Cell(1, 1).Range.InlineShapes.AddPicture(@"e:\SisCompras\WpfClient\Recursos\logo_ciltra_transparente1.png");
        //        oTable.Cell(1, 1).Range.Text = "CARTA PORTE.";
        //        oTable.Cell(1, 1).Range.Bold = 2;
        //        oTable.Cell(1, 1).Range.Font.Size = 14;
        //        oTable.Cell(1, 1).Merge(oTable.Cell(1, 5));
        //        oTable.Cell(1, 1).Range.ParagraphFormat.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphCenter;

        //        oTable.Cell(2, 2).Range.Text = "Fecha de salida: " + cartaPorte.FechaHoraViaje.ToString("dd/MMMM/yy hh:mm:ss tt");
        //        oTable.Cell(2, 2).Range.Font.Size = 8;
        //        oTable.Cell(2, 2).Merge(oTable.Cell(2, 5));
        //        oTable.Cell(2, 2).Range.ParagraphFormat.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphRight;

        //        oTable.Cell(3, 1).Range.Font.Size = 14; //Tamaño de la Fuente 
        //        oTable.Cell(3, 1).Range.Font.Name = "Time New Roman";
        //        oTable.Cell(3, 1).Range.Font.Bold = 1; // Negrita 
        //        oTable.Cell(3, 1).Range.Font.Color = Microsoft.Office.Interop.Word.WdColor.wdColorRed;
        //        oTable.Cell(3, 1).Range.Text = (string.Format("FOLIO: {0}", cartaPorte.NumGuiaId));
        //        oTable.Cell(3, 1).Merge(oTable.Cell(3, 5));

        //        //Bar-Code 39
        //        oTable.Cell(4, 1).Range.Font.Size = 40; //Tamaño de la Fuente 
        //        oTable.Cell(4, 1).Range.Font.Name = "Bar-Code 39";
        //        oTable.Cell(4, 1).Range.Font.Color = Microsoft.Office.Interop.Word.WdColor.wdColorBlack;
        //        oTable.Cell(4, 1).Range.Text = (string.Format(cartaPorte.NumGuiaId.ToString()));
        //        oTable.Cell(4, 1).Merge(oTable.Cell(4, 5));

        //        oTable.Cell(5, 1).Range.Text = string.Format("CLIENTE: {0} " + Environment.NewLine + "NÚMERO DE VIAJE: {1}",
        //            cartaPorte.cliente.nombre.ToUpper()
        //            , cartaPorte.NumViaje.ToString()
        //            );
        //        oTable.Cell(5, 1).Range.Bold = 0;
        //        oTable.Cell(5, 1).Range.Font.Size = 14;
        //        oTable.Cell(5, 1).Range.ParagraphFormat.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphJustify;
        //        oTable.Cell(5, 1).Merge(oTable.Cell(5, 5));

        //        oTable.Cell(6, 1).Range.Text = "OPERADOR: " + cartaPorte.operador.nombre.ToUpper();
        //        oTable.Cell(6, 1).Range.Bold = 1;
        //        oTable.Cell(6, 1).Range.Font.Size = 12;
        //        oTable.Cell(6, 1).Range.ParagraphFormat.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphCenter;
        //        oTable.Cell(6, 1).Merge(oTable.Cell(6, 5));

        //        oTable.Cell(7, 1).Range.Text = "UNIDADES.";
        //        oTable.Cell(7, 1).Range.Bold = 1;
        //        oTable.Cell(7, 1).Range.Font.Size = 12;
        //        oTable.Cell(7, 1).Range.ParagraphFormat.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphJustify;
        //        oTable.Cell(7, 1).Merge(oTable.Cell(7, 5));

        //        oTable.Cell(8, 1).Range.Text = "TRACTOR: " + cartaPorte.tractor.clave + " - " + cartaPorte.tractor.tipoUnidad.descripcion;
        //        oTable.Cell(8, 1).Range.Bold = 0;
        //        oTable.Cell(8, 1).Range.Font.Size = 8;
        //        oTable.Cell(8, 1).Range.ParagraphFormat.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphCenter;
        //        oTable.Cell(8, 1).Merge(oTable.Cell(8, 5));

        //        oTable.Cell(9, 1).Range.Text = "REMOLQUE 1: " + cartaPorte.remolque.clave + " - " + cartaPorte.remolque.tipoUnidad.descripcion;
        //        oTable.Cell(9, 1).Range.Bold = 0;
        //        oTable.Cell(9, 1).Range.Font.Size = 8;
        //        oTable.Cell(9, 1).Range.ParagraphFormat.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphCenter;
        //        oTable.Cell(9, 1).Merge(oTable.Cell(9, 5));

        //        if (cartaPorte.dolly != null)
        //        {
        //            oTable.Cell(10, 1).Range.Text = "DOLLY: " + cartaPorte.dolly.clave + " - " + cartaPorte.dolly.tipoUnidad.descripcion;
        //            oTable.Cell(10, 1).Range.Bold = 0;
        //            oTable.Cell(10, 1).Range.Font.Size = 8;
        //            oTable.Cell(10, 1).Range.ParagraphFormat.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphCenter;
        //            oTable.Cell(10, 1).Merge(oTable.Cell(10, 5));
        //        }

        //        if (cartaPorte.remolque2 != null)
        //        {
        //            oTable.Cell(11, 1).Range.Text = "REMOLQUE 2: " + cartaPorte.remolque2.clave + " - " + cartaPorte.remolque2.tipoUnidad.descripcion;
        //            oTable.Cell(11, 1).Range.Bold = 0;
        //            oTable.Cell(11, 1).Range.Font.Size = 8;
        //            oTable.Cell(11, 1).Range.ParagraphFormat.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphCenter;
        //            oTable.Cell(11, 1).Merge(oTable.Cell(11, 5));
        //        }

        //        oTable.Cell(12, 1).Range.Text = "DETALLES DE LOS PRODUCTOS";
        //        oTable.Cell(12, 1).Range.Bold = 1;
        //        oTable.Cell(12, 1).Range.Font.Size = 10;
        //        oTable.Cell(12, 1).Merge(oTable.Cell(12, 5));
        //        oTable.Cell(12, 1).Range.ParagraphFormat.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphCenter;

        //        /////////////////

        //        oTable.Cell(13, 1).Range.Text = "REMISIÓN";
        //        oTable.Cell(13, 1).Range.Bold = 1;
        //        oTable.Cell(13, 1).Range.Font.Size = 9;
        //        oTable.Cell(13, 1).Range.ParagraphFormat.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphCenter;

        //        oTable.Cell(13, 2).Range.Text = "DESCRIPCIÓN";
        //        oTable.Cell(13, 2).Range.Bold = 1;
        //        oTable.Cell(13, 2).Range.Font.Size = 9;
        //        oTable.Cell(13, 2).Range.ParagraphFormat.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphCenter;

        //        oTable.Cell(13, 3).Range.Text = "DESTINO";
        //        oTable.Cell(13, 3).Range.Bold = 1;
        //        oTable.Cell(13, 3).Range.Font.Size = 9;
        //        oTable.Cell(13, 3).Range.ParagraphFormat.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphCenter;

        //        oTable.Cell(13, 4).Range.Text = "V. PRO - V. DES";
        //        oTable.Cell(13, 4).Range.Bold = 1;
        //        oTable.Cell(13, 4).Range.Font.Size = 9;
        //        oTable.Cell(13, 4).Range.ParagraphFormat.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphCenter;

        //        oTable.Cell(13, 5).Range.Text = "IMPORTE";
        //        oTable.Cell(13, 5).Range.Bold = 1;
        //        oTable.Cell(13, 5).Range.Font.Size = 9;
        //        oTable.Cell(13, 5).Range.ParagraphFormat.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphCenter;


        //        //////////////////
        //        int i = 14;
        //        //int j = 1;
        //        foreach (var item in cartaPorte.listDetalles)
        //        {
        //            oTable.Cell(i, 1).Range.Text = item.remision.ToString();
        //            oTable.Cell(i, 1).Range.Bold = 0;
        //            oTable.Cell(i, 1).Range.Font.Size = 9;
        //            oTable.Cell(i, 1).Range.ParagraphFormat.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphCenter;

        //            oTable.Cell(i, 2).Range.Text = item.producto.descripcion;
        //            oTable.Cell(i, 2).Range.Bold = 0;
        //            oTable.Cell(i, 2).Range.Font.Size = 9;
        //            oTable.Cell(i, 2).Range.ParagraphFormat.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphCenter;

        //            oTable.Cell(i, 3).Range.Text = item.zonaSelect.descripcion;
        //            oTable.Cell(i, 3).Range.Bold = 0;
        //            oTable.Cell(i, 3).Range.Font.Size = 9;
        //            oTable.Cell(i, 3).Range.ParagraphFormat.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphCenter;

        //            oTable.Cell(i, 4).Range.Text = item.volumenCarga.ToString() + " - " + item.volumenDescarga.ToString();
        //            oTable.Cell(i, 4).Range.Bold = 0;
        //            oTable.Cell(i, 4).Range.Font.Size = 9;
        //            oTable.Cell(i, 4).Range.ParagraphFormat.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphCenter;

        //            oTable.Cell(i, 5).Range.Text = "$ 1" /* item.importeReal.ToString()*/;
        //            oTable.Cell(i, 5).Range.Bold = 0;
        //            oTable.Cell(i, 5).Range.Font.Size = 9;
        //            oTable.Cell(i, 5).Range.ParagraphFormat.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphCenter;
        //            i++;
        //            //j++;
        //        }

        //        rutaWord = archivo;
        //        objWordDocument.SaveAs(archivo, FileFormat: Microsoft.Office.Interop.Word.WdSaveFormat.wdFormatDocument);

        //    }
        //    catch (Exception e)
        //    {
        //        var ex = e.Message;
        //    }
        //    finally
        //    {
        //        objWordDocument.Close();
        //        objWordApplication.Quit();
        //        GC.WaitForPendingFinalizers();
        //        GC.Collect();
        //        GC.WaitForPendingFinalizers();
        //        GC.Collect();
        //    }
        //}

        //internal bool imprimirCatalogoRutas(Cliente cliente)
        //{
        //    #region inicio
        //    string archivo = "";
        //    try
        //    {
        //        string path = System.Windows.Forms.Application.StartupPath;
        //        if (!Directory.Exists(path + "\\reportes\\"))
        //        {
        //            DirectoryInfo dir = Directory.CreateDirectory(path + "\\reportes\\");
        //        }

        //        //archivo = string.Format(@"{0}{1}", path + "\\reportes\\", cartaPorte.NumGuiaId);
        //        archivo = string.Format(@"{0}{1}", path + "\\reportes\\", "reporte");

        //        if (File.Exists(archivo + ".xlsx"))
        //        {
        //            File.Delete(archivo + ".xlsx");
        //        }
        //        if (File.Exists(archivo + ".xps"))
        //        {
        //            File.Delete(archivo + ".xps");
        //        }
        //        //".xps"
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message);
        //        return false;
        //    }
        //    #endregion
        //    Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
        //    Excel.Workbook xlWorkBook = xlApp.Workbooks.Add();
        //    //Microsoft.Office.Interop.Excel.Worksheet excelSheet;
        //    Microsoft.Office.Interop.Excel.Range excelCellrange;
        //    try
        //    {

        //        if (xlApp == null)
        //        {
        //            MessageBox.Show("Excel is not properly installed!!");
        //            return false;
        //        }
        //        else
        //        {
        //            Excel.Worksheet xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
        //            xlWorkSheet.PageSetup.LeftMargin = 10;
        //            xlWorkSheet.PageSetup.RightMargin = 10;
        //            xlWorkSheet.PageSetup.Orientation = Microsoft.Office.Interop.Excel.XlPageOrientation.xlLandscape;

        //            xlWorkSheet.Name = "Rutas Cliente";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[1, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 10;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            xlWorkSheet.Cells[1, 1] = "CATÁLOGO DE RUTAS POR CLIENTE";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[2, 1], xlWorkSheet.Cells[2, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 8;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            xlWorkSheet.Cells[2, 1] = cliente.nombre;

        //            xlWorkSheet.Cells[3, 1] = "Clave";
        //            xlWorkSheet.Cells[3, 2] = "Descripción";
        //            xlWorkSheet.Cells[3, 3] = "KM";
        //            xlWorkSheet.Cells[3, 4] = "Precio Sencillo";
        //            xlWorkSheet.Cells[3, 5] = "Precio Sencillo 35";
        //            xlWorkSheet.Cells[3, 6] = "Full";
        //            xlWorkSheet.Cells[3, 7] = "Clave Cliente";
        //            xlWorkSheet.Cells[3, 8] = "Estado";

        //            OperationResult respGet = new ZonasSvc().getZonasALLorById(0, cliente.clave.ToString());
        //            if (respGet.typeResult != ResultTypes.success)
        //            {
        //                ImprimirMensaje.imprimir(respGet);
        //                return false;
        //            }
        //            List<Zona> listZonas = new List<Zona>();
        //            listZonas = (List<Zona>)respGet.result;

        //            int i = 4;
        //            foreach (Zona ruta in listZonas)
        //            {
        //                try
        //                {
        //                    xlWorkSheet.Cells[i, 1] = ruta.clave;
        //                    xlWorkSheet.Cells[i, 2] = ruta.descripcion;
        //                    xlWorkSheet.Cells[i, 3] = ruta.km;
        //                    xlWorkSheet.Cells[i, 4] = ruta.costoSencillo.ToString("C4");
        //                    xlWorkSheet.Cells[i, 5] = ruta.costoSencillo35.ToString("C4");
        //                    xlWorkSheet.Cells[i, 6] = ruta.costoFull.ToString("C4");
        //                    xlWorkSheet.Cells[i, 7] = ruta.claveExtra;
        //                    xlWorkSheet.Cells[i, 8] = ruta.estado.nombre;
        //                    i++;
        //                }
        //                catch (Exception ex)
        //                {
        //                    MessageBox.Show(ex.Message);
        //                    return false;
        //                }

        //            }
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[3, 1], xlWorkSheet.Cells[i - 1, 8]];
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
        //            excelCellrange.Worksheet.ListObjects.AddEx(
        //                                                        SourceType: Excel.XlListObjectSourceType.xlSrcRange,
        //                                                        Source: excelCellrange,
        //                                                        XlListObjectHasHeaders: Excel.XlYesNoGuess.xlYes);
        //            excelCellrange.Font.Size = 8;
        //            //excelCellrange.EntireColumn.AutoFit();

        //            xlWorkBook.SaveAs(archivo + ".xlsx");
        //            rutaExcel = archivo + ".xlsx";

        //            bool resp = convertirExcelToSps(archivo);
        //            if (resp)
        //            {
        //                imprimirReporte(archivo + ".xps");
        //            }
        //            return resp;
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        MessageBox.Show(e.Message);
        //        return false;
        //    }
        //    finally
        //    {
        //        xlWorkBook.Close();
        //        xlApp.Quit();
        //        GC.WaitForPendingFinalizers();
        //        GC.Collect();
        //        GC.WaitForPendingFinalizers();
        //        GC.Collect();
        //    }
        //}

        //internal bool imprimirCatalogoProductos(Cliente cliente)
        //{
        //    #region inicio
        //    string archivo = "";
        //    try
        //    {
        //        string path = System.Windows.Forms.Application.StartupPath;
        //        if (!Directory.Exists(path + "\\reportes\\"))
        //        {
        //            DirectoryInfo dir = Directory.CreateDirectory(path + "\\reportes\\");
        //        }

        //        //archivo = string.Format(@"{0}{1}", path + "\\reportes\\", cartaPorte.NumGuiaId);
        //        archivo = string.Format(@"{0}{1}", path + "\\reportes\\", "reporte");

        //        if (File.Exists(archivo + ".xlsx"))
        //        {
        //            File.Delete(archivo + ".xlsx");
        //        }
        //        if (File.Exists(archivo + ".xps"))
        //        {
        //            File.Delete(archivo + ".xps");
        //        }
        //        //".xps"
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message);
        //        return false;
        //    }
        //    #endregion
        //    Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
        //    Excel.Workbook xlWorkBook = xlApp.Workbooks.Add();
        //    //Microsoft.Office.Interop.Excel.Worksheet excelSheet;
        //    Microsoft.Office.Interop.Excel.Range excelCellrange;
        //    try
        //    {

        //        if (xlApp == null)
        //        {
        //            MessageBox.Show("Excel is not properly installed!!");
        //            return false;
        //        }
        //        else
        //        {
        //            Excel.Worksheet xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
        //            //xlWorkSheet.PageSetup.LeftMargin = 10;
        //            //xlWorkSheet.PageSetup.RightMargin = 10;
        //            //xlWorkSheet.PageSetup.Orientation = Microsoft.Office.Interop.Excel.XlPageOrientation.xlLandscape;    

        //            xlWorkSheet.Name = "Productos Cliente";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[1, 3]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 10;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            xlWorkSheet.Cells[1, 1] = "CATÁLOGO DE PRODUCTOS POR CLIENTE";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[2, 1], xlWorkSheet.Cells[2, 3]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 8;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            xlWorkSheet.Cells[2, 1] = cliente.nombre;

        //            xlWorkSheet.Cells[3, 1] = "Clave";
        //            xlWorkSheet.Cells[3, 2] = "Descripción";
        //            xlWorkSheet.Cells[3, 3] = "Clave Cliente";


        //            OperationResult respGet = new ProductoSvc().getALLProductosByIdORidFraccion(cliente.fraccion.clave);
        //            if (respGet.typeResult != ResultTypes.success)
        //            {
        //                ImprimirMensaje.imprimir(respGet);
        //                return false;
        //            }
        //            List<Producto> listProducto = new List<Producto>();
        //            listProducto = (List<Producto>)respGet.result;

        //            int i = 4;
        //            foreach (Producto ruta in listProducto)
        //            {
        //                try
        //                {
        //                    xlWorkSheet.Cells[i, 1] = ruta.clave;
        //                    xlWorkSheet.Cells[i, 2] = ruta.descripcion;
        //                    xlWorkSheet.Cells[i, 3] = ruta.clave_externo;

        //                    i++;
        //                }
        //                catch (Exception ex)
        //                {
        //                    MessageBox.Show(ex.Message);
        //                    return false;
        //                }
        //            }
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[3, 1], xlWorkSheet.Cells[i - 1, 3]];
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
        //            excelCellrange.Worksheet.ListObjects.AddEx(
        //                                                        SourceType: Excel.XlListObjectSourceType.xlSrcRange,
        //                                                        Source: excelCellrange,
        //                                                        XlListObjectHasHeaders: Excel.XlYesNoGuess.xlYes);
        //            excelCellrange.Font.Size = 10;
        //            excelCellrange.EntireColumn.AutoFit();

        //            xlWorkBook.SaveAs(archivo + ".xlsx");
        //            rutaExcel = archivo + ".xlsx";

        //            bool resp = convertirExcelToSps(archivo);
        //            if (resp)
        //            {
        //                imprimirReporte(archivo + ".xps");
        //            }
        //            return resp;
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        MessageBox.Show(e.Message);
        //        return false;
        //    }
        //    finally
        //    {
        //        xlWorkBook.Close();
        //        xlApp.Quit();
        //        GC.WaitForPendingFinalizers();
        //        GC.Collect();
        //        GC.WaitForPendingFinalizers();
        //        GC.Collect();
        //    }
        //}

        public bool createValeGasolina(int idAnticipo, decimal ltCombustible, string nombreChofer,
            string nombreDespachador, DateTime fechaCombustible,
            string claveTractor, string claveRemolque1 = "",
            string claveDolly = "", string claveRemolque2 = "")
        {
            #region crear
            string archivo = "";
            try
            {

                string path = System.Windows.Forms.Application.StartupPath;
                if (!Directory.Exists(path + "\\reportes\\"))
                {
                    DirectoryInfo dir = Directory.CreateDirectory(path + "\\reportes\\");
                }

                //archivo = string.Format(@"{0}{1}", path + "\\reportes\\", cartaPorte.NumGuiaId);
                archivo = string.Format(@"{0}{1}", path + "\\reportes\\", "reporte");

                if (File.Exists(archivo + ".xlsx"))
                {
                    File.Delete(archivo + ".xlsx");
                }
                if (File.Exists(archivo + ".xps"))
                {
                    File.Delete(archivo + ".xps");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
            #endregion

            #region crearVale
            Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
            Excel.Workbook xlWorkBook = xlApp.Workbooks.Add();
            //Microsoft.Office.Interop.Excel.Worksheet excelSheet;
            Microsoft.Office.Interop.Excel.Range excelCellrange;

            try
            {

                if (xlApp == null)
                {
                    MessageBox.Show("Excel is not properly installed!!");
                    return false;
                }
                else
                {

                    Excel.Worksheet xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
                    //xlWorkSheet.PageSetup.Orientation = Microsoft.Office.Interop.Excel.XlPageOrientation.xlLandscape;

                    xlWorkSheet.Name = "ValeGasolina";

                    #region 1 tabla encabezado
                    excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[3, 1], xlWorkSheet.Cells[3, 4]];
                    excelCellrange.MergeCells = true;
                    excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                    xlWorkSheet.Cells[3, 1] = "DESCRIPCIÓN";

                    excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[3, 5], xlWorkSheet.Cells[3, 5]];
                    excelCellrange.MergeCells = true;
                    excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                    xlWorkSheet.Cells[3, 5] = "LITROS";
                    ////////////////////////////////////////////////
                    excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[4, 1], xlWorkSheet.Cells[4, 1]];
                    excelCellrange.MergeCells = true;
                    excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
                    xlWorkSheet.Cells[4, 1] = "CONSUMO";

                    excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[4, 2], xlWorkSheet.Cells[4, 4]];
                    excelCellrange.MergeCells = true;
                    excelCellrange.Font.Size = 8;
                    xlWorkSheet.Cells[4, 2] = "(EN VIAJES SEGUN PAPELETAS O SEGUN EL E-CHECK)";
                    //////////////////////////////////////////////////////
                    excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[5, 1], xlWorkSheet.Cells[5, 1]];
                    excelCellrange.MergeCells = true;
                    excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
                    xlWorkSheet.Cells[5, 1] = "RELLENO";

                    excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[5, 5], xlWorkSheet.Cells[5, 5]];
                    excelCellrange.Font.Bold = true;
                    xlWorkSheet.Cells[5, 5] = ltCombustible; ///

                    excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[5, 2], xlWorkSheet.Cells[5, 4]];
                    excelCellrange.MergeCells = true;
                    excelCellrange.Font.Size = 8;
                    xlWorkSheet.Cells[5, 2] = "(EXCEDENTE SOBRE VIAJES O DESPUES DEL E-CHECK)";
                    //////////////////////////////////////////////////////
                    excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[6, 1], xlWorkSheet.Cells[6, 4]];
                    excelCellrange.MergeCells = true;
                    excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
                    xlWorkSheet.Cells[6, 1] = "USO INTERNO* PARA  ( ) LAVADO CAJA  ( ) USO TALLER....";

                    //////////////////////////////////////////////////////

                    excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[7, 1], xlWorkSheet.Cells[7, 4]];
                    excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
                    excelCellrange.MergeCells = true;
                    xlWorkSheet.Cells[7, 1] = "OTROS* _____________________________________________";

                    //////////////////////////////////////////////////////

                    excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[8, 1], xlWorkSheet.Cells[8, 2]];
                    excelCellrange.MergeCells = true;
                    excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
                    excelCellrange.VerticalAlignment = Excel.XlVAlign.xlVAlignTop;
                    excelCellrange.Font.Size = 7;
                    xlWorkSheet.Cells[8, 1] = "*Describir e indicar quien autoriza";

                    excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[8, 3], xlWorkSheet.Cells[8, 4]];
                    excelCellrange.MergeCells = true;
                    excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
                    xlWorkSheet.Cells[8, 3] = "TOTAL DESPACHADO";

                    excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[8, 5], xlWorkSheet.Cells[8, 5]];
                    excelCellrange.Font.Bold = true;
                    xlWorkSheet.Cells[8, 5] = ltCombustible;
                    //////////////////////////////////////////////////////

                    excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[9, 1], xlWorkSheet.Cells[9, 5]];
                    excelCellrange.MergeCells = true;
                    excelCellrange.Font.Italic = true;
                    excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
                    //xlWorkSheet.Cells[9, 1] = "SON " + new ConvertNumToString().getLetras(ltCombustible.ToString());
                    //////////////////////////////////////////////////////

                    excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[3, 1], xlWorkSheet.Cells[3, 5]];
                    Excel.Borders border = excelCellrange.Borders;
                    border.LineStyle = Excel.XlLineStyle.xlContinuous;
                    border.Weight = 2d;

                    excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[3, 5], xlWorkSheet.Cells[8, 5]];
                    Excel.Borders border2 = excelCellrange.Borders;
                    border2.LineStyle = Excel.XlLineStyle.xlContinuous;
                    border2.Weight = 2d;
                    #endregion

                    #region 2 tabla
                    excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[10, 1], xlWorkSheet.Cells[10, 1]];
                    xlWorkSheet.Cells[10, 1] = "FOLIO";

                    excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[10, 2], xlWorkSheet.Cells[10, 3]];
                    excelCellrange.MergeCells = true;
                    xlWorkSheet.Cells[10, 2] = "RUTA DE VIAJES";
                    int i = 11;
                    //foreach (var item in salEnt.listCP)
                    //{
                    //    excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 1], xlWorkSheet.Cells[i, 1]];
                    //    xlWorkSheet.Cells[i, 1] = item.Consecutivo;

                    //    excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 2], xlWorkSheet.Cells[i, 3]];
                    //    excelCellrange.MergeCells = true;
                    //    xlWorkSheet.Cells[i, 2] = item.zonaSelect.descripcion;

                    //    excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[10, 4], xlWorkSheet.Cells[10, 4]];
                    //    xlWorkSheet.Cells[10, 4] = item.zonaSelect.km;
                    //    i++;
                    //}
                    //excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[11, 2], xlWorkSheet.Cells[11, 3]];
                    //excelCellrange.MergeCells = true;
                    //excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[12, 2], xlWorkSheet.Cells[12, 3]];
                    //excelCellrange.MergeCells = true;
                    //excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[13, 2], xlWorkSheet.Cells[13, 3]];
                    //excelCellrange.MergeCells = true;
                    //excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[14, 2], xlWorkSheet.Cells[14, 3]];
                    //excelCellrange.MergeCells = true;
                    //excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[15, 2], xlWorkSheet.Cells[15, 3]];
                    //excelCellrange.MergeCells = true;
                    //excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[16, 2], xlWorkSheet.Cells[16, 3]];
                    //excelCellrange.MergeCells = true;

                    excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[10, 4], xlWorkSheet.Cells[10, 4]];
                    xlWorkSheet.Cells[10, 4] = "KMS";

                    excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[10, 5], xlWorkSheet.Cells[10, 5]];
                    xlWorkSheet.Cells[10, 5] = "LITROS";

                    excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[10, 1], xlWorkSheet.Cells[10, 5]];
                    excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

                    excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[10, 1], xlWorkSheet.Cells[i - 1, 5]];
                    Excel.Borders bordert = excelCellrange.Borders;
                    bordert.LineStyle = Excel.XlLineStyle.xlContinuous;
                    bordert.Weight = 2d;
                    i++;
                    #endregion

                    #region 3 KM/LTS
                    excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 1], xlWorkSheet.Cells[i, 2]];
                    excelCellrange.MergeCells = true;
                    excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
                    xlWorkSheet.Cells[i, 1] = "SI ES MOTOR ELECTRICO";

                    excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 3], xlWorkSheet.Cells[i, 3]];
                    excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                    excelCellrange.Font.Size = 8;
                    xlWorkSheet.Cells[i, 3] = "Total Inicial";

                    excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 4], xlWorkSheet.Cells[i, 4]];
                    excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                    excelCellrange.Font.Size = 8;
                    xlWorkSheet.Cells[i, 4] = "Total Final";

                    excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 5], xlWorkSheet.Cells[i, 5]];
                    excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                    excelCellrange.Font.Size = 8;
                    xlWorkSheet.Cells[i, 5] = "TRIP";
                    i++;
                    ///////////////////////////////////////////////////////////////
                    excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 1], xlWorkSheet.Cells[i, 1]];
                    excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
                    xlWorkSheet.Cells[i, 1] = "( ) N-14";

                    excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 2], xlWorkSheet.Cells[i, 2]];
                    excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
                    xlWorkSheet.Cells[i, 2] = "( ) Detroit Diesel      KMS";


                    xlWorkSheet.Cells[i, 3] = 0;
                    xlWorkSheet.Cells[i, 4] = 0;
                    xlWorkSheet.Cells[i, 5] = 0;

                    excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 3], xlWorkSheet.Cells[i, 5]];//
                    excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                    excelCellrange.Font.Bold = true;
                    i++;
                    excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 1], xlWorkSheet.Cells[i, 1]];
                    excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
                    xlWorkSheet.Cells[i, 1] = "( ) OTRO";

                    excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 2], xlWorkSheet.Cells[i, 2]];
                    excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
                    xlWorkSheet.Cells[i, 2] = "____________________ LITROS";

                    xlWorkSheet.Cells[i, 5] = 0;

                    excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i - 1, 3], xlWorkSheet.Cells[19, 5]];
                    Excel.Borders border3 = excelCellrange.Borders;
                    border3.LineStyle = Excel.XlLineStyle.xlContinuous;
                    border3.Weight = 2d;

                    i++;
                    #endregion

                    #region transportes
                    excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 2], xlWorkSheet.Cells[i, 5]];
                    excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                    excelCellrange.Font.Size = 8;
                    xlWorkSheet.Cells[i, 2] = "Tractor";
                    xlWorkSheet.Cells[i, 3] = "Remolque 1";
                    xlWorkSheet.Cells[i, 4] = "Dolly";
                    xlWorkSheet.Cells[i, 5] = "Remolque 2";
                    i++;
                    excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 1], xlWorkSheet.Cells[i, 1]];
                    excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
                    excelCellrange.VerticalAlignment = Excel.XlVAlign.xlVAlignTop;
                    excelCellrange.Font.Size = 8;
                    xlWorkSheet.Cells[i, 1] = "Números" + Environment.NewLine + "Economicos";
                    i++;
                    excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 2], xlWorkSheet.Cells[i, 5]];
                    excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                    excelCellrange.Font.Bold = true;
                    xlWorkSheet.Cells[i - 1, 2] = claveTractor ;
                    xlWorkSheet.Cells[i - 1, 3] = claveRemolque1;
                    xlWorkSheet.Cells[i - 1, 4] = claveDolly;
                    xlWorkSheet.Cells[i - 1, 5] = claveRemolque2;
                    xlWorkSheet.Cells[i, 5] = 0;

                    excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i - 1, 2], xlWorkSheet.Cells[i - 1, 5]];
                    Excel.Borders border4 = excelCellrange.Borders;
                    border4.LineStyle = Excel.XlLineStyle.xlContinuous;
                    border3.Weight = 2d;
                    #endregion

                    #region firmas
                    excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 2], xlWorkSheet.Cells[i, 3]];
                    excelCellrange.MergeCells = true;
                    excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                    xlWorkSheet.Cells[i, 2] = "OPERADOR";

                    excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 4], xlWorkSheet.Cells[i, 5]];
                    excelCellrange.MergeCells = true;
                    excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                    xlWorkSheet.Cells[i, 4] = "GASOLINERO";


                    excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 2], xlWorkSheet.Cells[i, 3]];
                    excelCellrange.MergeCells = true;
                    excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                    i += 3;
                    xlWorkSheet.Cells[i, 2] = "Firmas";

                    //excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[22, 4], xlWorkSheet.Cells[22, 5]];
                    //excelCellrange.MergeCells = true;
                    //excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                    //xlWorkSheet.Cells[25, 4] = "____________________";
                    i++;
                    excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 2], xlWorkSheet.Cells[i, 3]];
                    excelCellrange.MergeCells = true;
                    excelCellrange.Font.Bold = true;
                    excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                    xlWorkSheet.Cells[i, 2] = nombreChofer ;

                    excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 4], xlWorkSheet.Cells[i, 5]];
                    excelCellrange.MergeCells = true;
                    excelCellrange.Font.Bold = true;
                    excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                    xlWorkSheet.Cells[i, 4] = nombreDespachador;
                    i++;
                    #endregion

                    #region datos
                    excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 1], xlWorkSheet.Cells[i, 1]];
                    excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
                    xlWorkSheet.Cells[i, 1] = "FECHA";

                    excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 2], xlWorkSheet.Cells[i, 2]];
                    excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
                    excelCellrange.Font.Bold = true;
                    DateTime fecha = (DateTime)fechaCombustible;
                    xlWorkSheet.Cells[i, 2] = fecha.ToString("dd MMMM yyyy");

                    excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 3], xlWorkSheet.Cells[i, 3]];
                    excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
                    xlWorkSheet.Cells[i, 3] = "HORA";

                    excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 4], xlWorkSheet.Cells[i, 4]];
                    excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
                    excelCellrange.Font.Bold = true;
                    xlWorkSheet.Cells[i, 4] = fecha.ToString("HH:mm:ss");

                    excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 5], xlWorkSheet.Cells[i, 5]];
                    excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                    excelCellrange.Font.Bold = true;
                    excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Red);
                    excelCellrange.Font.Size = 20;

                    xlWorkSheet.Cells[i, 5] = "VALE No. " + idAnticipo.ToString();
                    i++;
                    #endregion

                    #region observaciones  
                    excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 1], xlWorkSheet.Cells[i, 5]];
                    excelCellrange.MergeCells = true;
                    xlWorkSheet.Cells[i, 1] = @"OBSERVACIONES Y/O AUTORIZACIONES:";

                    i++;
                    excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 1], xlWorkSheet.Cells[i, 5]];
                    excelCellrange.MergeCells = true;
                    xlWorkSheet.Cells[i, 1] = @"______________________________________________________________";
                    i++;
                    excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 1], xlWorkSheet.Cells[i, 5]];
                    excelCellrange.MergeCells = true;
                    xlWorkSheet.Cells[i, 1] = @"______________________________________________________________";
                    i++;
                    excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 1], xlWorkSheet.Cells[i, 5]];
                    excelCellrange.MergeCells = true;
                    xlWorkSheet.Cells[i, 1] = @"______________________________________________________________";
                    i++;
                    #endregion

                    #region sellos
                    excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 1], xlWorkSheet.Cells[i, 1]];
                    excelCellrange.MergeCells = true;
                    xlWorkSheet.Cells[i, 1] = @"SELLOS";

                    excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 2], xlWorkSheet.Cells[i, 5]];
                    excelCellrange.MergeCells = true;
                    excelCellrange.Font.Bold = true;
                    //xlWorkSheet.Cells[i, 2] = salEnt.sello1.ToString() + " - " + salEnt.sello2.ToString();
                    #endregion

                    #region GUARDAR/IMPRIMIR
                    excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[100, 100]];
                    excelCellrange.EntireColumn.AutoFit();
                    excelCellrange.EntireRow.AutoFit();

                    xlWorkBook.SaveAs(archivo + ".xlsx");

                    bool resp = convertirExcelToSps(archivo);
                    if (resp)
                    {
                        //imprimirReporte(archivo + ".xps");
                    }
                    ap.PrintDialog printDialog1 = new ap.PrintDialog();
                    printDialog1.PrinterSettings.Copies = 2;
                    ap.DialogResult result = printDialog1.ShowDialog();
                    var ss = printDialog1.PrinterSettings.PrinterName;
                    xlWorkSheet.PrintOutEx(1, 1, printDialog1.PrinterSettings.Copies, false, ss); //linea para imprimir
                    rutaExcel = archivo + ".xlsx";

                    return true;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                xlWorkBook.Close();
                xlApp.Quit();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            #endregion

        }

        //public bool cartaPorteFalso(ComercialCliente cliente, ComercialEmpresa empresa, DatosFacturaXML datosFactura, CartaPorte cartaPorte)
        //{
        //    string archivo = "";
        //    try
        //    {
        //        string path = System.Windows.Forms.Application.StartupPath;
        //        if (!Directory.Exists(path + "\\reportes\\"))
        //        {
        //            DirectoryInfo dir = Directory.CreateDirectory(path + "\\reportes\\");
        //        }

        //        //archivo = string.Format(@"{0}{1}", path + "\\reportes\\", cartaPorte.NumGuiaId);
        //        archivo = string.Format(@"{0}{1}", path + "\\reportes\\", "reporte");

        //        if (File.Exists(archivo + ".xlsx"))
        //        {
        //            File.Delete(archivo + ".xlsx");
        //        }
        //        if (File.Exists(archivo + ".xps"))
        //        {
        //            File.Delete(archivo + ".xps");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message);
        //        return false;
        //    }

        //    Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
        //    Excel.Workbook xlWorkBook = xlApp.Workbooks.Add();
        //    //Microsoft.Office.Interop.Excel.Worksheet excelSheet;
        //    Microsoft.Office.Interop.Excel.Range excelCellrange;

        //    try
        //    {

        //        if (xlApp == null)
        //        {
        //            MessageBox.Show("Excel is not properly installed!!");
        //            return false;
        //        }
        //        else
        //        {

        //            Excel.Worksheet xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
        //            //xlWorkSheet.PageSetup.Orientation = Microsoft.Office.Interop.Excel.XlPageOrientation.xlLandscape;

        //            xlWorkSheet.Name = "CartaPorte";

        //            //excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[6, 4]];
        //            //excelCellrange.Merge(true);

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[6, 4]];
        //            excelCellrange.MergeCells = true;

        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.Font.Size = 8;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            //oTable.Cell(1, 1).Range.InlineShapes.AddPicture(@"e:\SisCompras\WpfClient\Recursos\logo_ciltra_transparente1.png");
        //            //xlWorkSheet.Cells[1, 1] = "lugar para la imagen";

        //            xlWorkSheet.Shapes.AddPicture(System.Windows.Forms.Application.StartupPath + @"\\logoPegaso.png",
        //                Microsoft.Office.Core.MsoTriState.msoFalse,
        //                Microsoft.Office.Core.MsoTriState.msoCTrue,
        //                float.Parse(excelCellrange.Left.ToString()), float.Parse(excelCellrange.Top.ToString()),
        //                150, 50
        //                );

        //            #region primera tabla
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[1, 5], xlWorkSheet.Cells[1, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 28;
        //            excelCellrange.Font.Name = "Free 3 of 9 Extended";
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            //excelCellrange.Font.FontStyle = "Bold";
        //            xlWorkSheet.Cells[1, 5] = "*" + cartaPorte.numGuiaId.ToString() + "-" + cartaPorte.grupoCartaPorte.Trim('-') + "*";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[2, 5], xlWorkSheet.Cells[2, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 8;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Silver);
        //            excelCellrange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkGreen);
        //            xlWorkSheet.Cells[2, 5] = "CARTA PORTE";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[3, 6], xlWorkSheet.Cells[3, 6]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            //excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
        //            xlWorkSheet.Cells[3, 6] = "Viaje:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[4, 6], xlWorkSheet.Cells[4, 6]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            //excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
        //            xlWorkSheet.Cells[4, 6] = "Carta Portes:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[5, 6], xlWorkSheet.Cells[5, 6]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            //excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
        //            xlWorkSheet.Cells[5, 6] = "Fecha:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[6, 6], xlWorkSheet.Cells[6, 6]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            //excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
        //            xlWorkSheet.Cells[6, 6] = "Hora:";
        //            /*----------------------------------------------------------------------------------------------------*/

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[3, 7], xlWorkSheet.Cells[3, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 8;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
        //            xlWorkSheet.Cells[3, 7] = cartaPorte.numGuiaId; //ACA Iria el folio.

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[4, 7], xlWorkSheet.Cells[4, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
        //            xlWorkSheet.Cells[4, 7] = cartaPorte.grupoCartaPorte.Trim('-'); //ACA Iria el folio.

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[5, 7], xlWorkSheet.Cells[5, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            //excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
        //            xlWorkSheet.Cells[5, 7] = DateTime.Now.ToString("dd/MMM/yyyy"); // aca la fecha

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[6, 7], xlWorkSheet.Cells[6, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            //excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
        //            xlWorkSheet.Cells[6, 7] = DateTime.Now.ToString("HH:mm:ss"); // aca la fecha hora
        //            #endregion

        //            #region datos empresa

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[7, 1], xlWorkSheet.Cells[7, 4]];
        //            excelCellrange.Merge(true);
        //            xlWorkSheet.Cells[7, 1] = empresa.nombre.ToUpper(); // aca la empresa.

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[8, 1], xlWorkSheet.Cells[8, 4]];
        //            excelCellrange.Merge(true);
        //            xlWorkSheet.Cells[8, 1] = empresa.rfc.ToUpper(); // aca la RFC.

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[9, 1], xlWorkSheet.Cells[9, 4]];
        //            excelCellrange.Merge(true);
        //            xlWorkSheet.Cells[9, 1] = "No Aplica";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[7, 5], xlWorkSheet.Cells[7, 7]];
        //            excelCellrange.Merge(true);
        //            xlWorkSheet.Cells[7, 5] = empresa.calle.ToUpper(); // aca la direccion empresa.

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[8, 5], xlWorkSheet.Cells[8, 5]];
        //            excelCellrange.Merge(true);
        //            xlWorkSheet.Cells[8, 5] = empresa.municipio.ToUpper(); // ciudad empresa
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[8, 5], xlWorkSheet.Cells[8, 5]];
        //            excelCellrange.Merge(true);
        //            xlWorkSheet.Cells[8, 7] = empresa.cp; // CP empresa

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[8, 5], xlWorkSheet.Cells[8, 5]];
        //            excelCellrange.Merge(true);
        //            xlWorkSheet.Cells[9, 5] = empresa.municipio.ToUpper(); // ciudad empresa
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[8, 5], xlWorkSheet.Cells[8, 5]];
        //            excelCellrange.Merge(true);
        //            xlWorkSheet.Cells[9, 7] = empresa.estado.ToUpper(); // estado empresa

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[9, 5], xlWorkSheet.Cells[9, 5]];
        //            excelCellrange.Merge(true);
        //            xlWorkSheet.Cells[10, 5] = empresa.pais.ToUpper(); // pais empresa

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[7, 1], xlWorkSheet.Cells[10, 7]];
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
        //            excelCellrange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkGreen);
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);

        //            //Excel.Borders border = excelCellrange.Borders;
        //            //border.LineStyle = Excel.XlLineStyle.xlContinuous;
        //            //border.Weight = 2d;

        //            #endregion

        //            #region origen

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[11, 1], xlWorkSheet.Cells[11, 1]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkGreen);
        //            xlWorkSheet.Cells[11, 1] = "ORIGEN:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[11, 2], xlWorkSheet.Cells[11, 2]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Silver);
        //            xlWorkSheet.Cells[11, 2] = string.Format("Ciudad: {0}", cliente.municipio.ToUpper()); // ciudad origen

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[11, 5], xlWorkSheet.Cells[11, 5]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Silver);
        //            xlWorkSheet.Cells[11, 5] = string.Format("Estado: {0}", cliente.estado.ToUpper()); // estado origen

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[11, 1], xlWorkSheet.Cells[11, 5]];
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
        //            #endregion

        //            #region remitente

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[12, 1], xlWorkSheet.Cells[12, 1]];
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkGreen);
        //            xlWorkSheet.Cells[12, 1] = "REMITENTE:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[12, 2], xlWorkSheet.Cells[12, 2]];
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gray);
        //            xlWorkSheet.Cells[12, 2] = cliente.nombre.ToUpper(); // NOMBRE CLIENTE

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[13, 1], xlWorkSheet.Cells[13, 1]];
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkGreen);
        //            xlWorkSheet.Cells[13, 1] = "RFC:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[13, 2], xlWorkSheet.Cells[13, 2]];
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gray);
        //            xlWorkSheet.Cells[13, 2] = cliente.rfc.ToUpper(); // RFC CLIENTE

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[14, 1], xlWorkSheet.Cells[14, 1]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            xlWorkSheet.Cells[14, 1] = "Domicilio:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[14, 2], xlWorkSheet.Cells[14, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            xlWorkSheet.Cells[14, 2] = cliente.calle.ToUpper(); // aca la direccion del cliente

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[15, 1], xlWorkSheet.Cells[15, 1]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            xlWorkSheet.Cells[15, 1] = "Telefono:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[15, 2], xlWorkSheet.Cells[15, 2]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            xlWorkSheet.Cells[15, 2] = cliente.telefono.ToUpper(); // aca el telefono del cliente

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[15, 4], xlWorkSheet.Cells[15, 4]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            xlWorkSheet.Cells[15, 4] = "Colonia:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[15, 5], xlWorkSheet.Cells[15, 5]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            xlWorkSheet.Cells[15, 5] = cliente.colonia.ToUpper(); // aca el telefono del cliente

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[15, 6], xlWorkSheet.Cells[15, 6]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            xlWorkSheet.Cells[15, 6] = "CP:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[15, 7], xlWorkSheet.Cells[15, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            xlWorkSheet.Cells[15, 7] = cliente.cp; // aca el CP del cliente

        //            /**/

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[16, 1], xlWorkSheet.Cells[16, 1]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            xlWorkSheet.Cells[16, 1] = "Ciudad:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[16, 2], xlWorkSheet.Cells[16, 2]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            xlWorkSheet.Cells[16, 2] = cliente.municipio.ToUpper(); // aca el telefono del cliente

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[16, 4], xlWorkSheet.Cells[16, 4]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            xlWorkSheet.Cells[16, 4] = "Estado:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[16, 5], xlWorkSheet.Cells[16, 5]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            xlWorkSheet.Cells[16, 5] = cliente.estado.ToUpper();

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[16, 6], xlWorkSheet.Cells[16, 6]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            xlWorkSheet.Cells[16, 6] = "Pais:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[16, 7], xlWorkSheet.Cells[16, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            xlWorkSheet.Cells[16, 7] = cliente.pais.ToUpper();


        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[14, 1], xlWorkSheet.Cells[16, 7]];
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gray);
        //            #endregion

        //            #region DESTINO
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[17, 1], xlWorkSheet.Cells[17, 1]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkGreen);
        //            xlWorkSheet.Cells[17, 1] = "DESTINO:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[17, 2], xlWorkSheet.Cells[17, 2]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            xlWorkSheet.Cells[17, 2] = string.Format("Ciudad: {0}", empresa.municipio.ToUpper()); // ciudad origen

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[17, 5], xlWorkSheet.Cells[17, 5]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            xlWorkSheet.Cells[17, 5] = string.Format("Estado: {0}", empresa.estado.ToUpper()); // estado origen

        //            #endregion

        //            #region destinatario

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[18, 1], xlWorkSheet.Cells[18, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkGreen);
        //            xlWorkSheet.Cells[18, 1] = "DESTINATARIO";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[19, 1], xlWorkSheet.Cells[19, 1]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            xlWorkSheet.Cells[19, 1] = "Domicilio:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[19, 2], xlWorkSheet.Cells[19, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            xlWorkSheet.Cells[19, 2] = empresa.calle.ToUpper();

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[20, 1], xlWorkSheet.Cells[20, 1]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            xlWorkSheet.Cells[20, 1] = "Telefono:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[20, 2], xlWorkSheet.Cells[20, 2]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            xlWorkSheet.Cells[20, 2] = empresa.telefono.ToUpper(); // aca el telefono del cliente

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[22, 4], xlWorkSheet.Cells[20, 4]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            xlWorkSheet.Cells[20, 4] = "Colonia:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[20, 5], xlWorkSheet.Cells[20, 5]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            xlWorkSheet.Cells[20, 5] = empresa.colonia.ToUpper(); // aca el telefono de la empresa

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[20, 6], xlWorkSheet.Cells[20, 6]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            xlWorkSheet.Cells[20, 6] = "CP:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[20, 7], xlWorkSheet.Cells[20, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            xlWorkSheet.Cells[20, 7] = empresa.cp; // aca el CP de la empresa

        //            ///*-------------------------------------------------------------------------------*/

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[21, 1], xlWorkSheet.Cells[21, 1]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            xlWorkSheet.Cells[21, 1] = "Ciudad:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[21, 2], xlWorkSheet.Cells[21, 2]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            xlWorkSheet.Cells[21, 2] = empresa.municipio.ToUpper(); // aca la ciudad del cliente

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[21, 4], xlWorkSheet.Cells[21, 4]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            xlWorkSheet.Cells[21, 4] = empresa.estado.ToUpper();

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[21, 5], xlWorkSheet.Cells[21, 5]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            xlWorkSheet.Cells[21, 5] = empresa.estado.ToUpper(); // aca el estado de la empresa

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[21, 6], xlWorkSheet.Cells[21, 6]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            xlWorkSheet.Cells[21, 6] = "País:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[21, 7], xlWorkSheet.Cells[21, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            xlWorkSheet.Cells[21, 7] = empresa.pais.ToUpper(); // aca el pais del cliente

        //            #endregion

        //            #region tabla detalles

        //            #region titulo tabla detalles

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[22, 1], xlWorkSheet.Cells[22, 1]];
        //            xlWorkSheet.Cells[22, 1] = "Cantidad.";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[22, 2], xlWorkSheet.Cells[22, 2]];
        //            xlWorkSheet.Cells[22, 2] = "Unidad.";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[22, 3], xlWorkSheet.Cells[22, 4]];
        //            excelCellrange.Merge(true);
        //            xlWorkSheet.Cells[22, 3] = "Concepto / Descripción.";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[22, 5], xlWorkSheet.Cells[22, 7]];
        //            excelCellrange.Merge(true);
        //            xlWorkSheet.Cells[22, 5] = "Producto.";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[22, 1], xlWorkSheet.Cells[22, 7]];
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkGreen);
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
        //            #endregion

        //            #region datos tabla detalle

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[23, 1], xlWorkSheet.Cells[23, 1]];
        //            excelCellrange.NumberFormat = "@";
        //            xlWorkSheet.Cells[23, 1] = cartaPorte.volumenDescarga.ToString();

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[23, 2], xlWorkSheet.Cells[23, 2]];
        //            xlWorkSheet.Cells[23, 2] = "Servicio";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[23, 3], xlWorkSheet.Cells[23, 4]];
        //            excelCellrange.Merge(true);
        //            xlWorkSheet.Cells[23, 3] = cartaPorte.zonaSelect.descripcion;

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[23, 5], xlWorkSheet.Cells[23, 7]];
        //            excelCellrange.Merge(true);
        //            xlWorkSheet.Cells[23, 5] = cartaPorte.producto.descripcion;

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[23, 1], xlWorkSheet.Cells[23, 7]];
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.Font.FontStyle = "Bold";


        //            #endregion


        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[22, 1], xlWorkSheet.Cells[23, 7]];
        //            Excel.Borders border = excelCellrange.Borders;
        //            border.LineStyle = Excel.XlLineStyle.xlContinuous;
        //            border.Weight = 2d;
        //            #endregion


        //            #region valores 
        //            /*----------------------------------------------------------------------------------------*/
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[24, 6], xlWorkSheet.Cells[24, 6]];
        //            xlWorkSheet.Cells[24, 6] = "Subtotal:";
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[24, 7], xlWorkSheet.Cells[24, 7]];
        //            excelCellrange.NumberFormat = "@";
        //            xlWorkSheet.Cells[24, 7] = "0.00";
        //            /*----------------------------------------------------------------------------------------*/
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[25, 6], xlWorkSheet.Cells[25, 6]];
        //            xlWorkSheet.Cells[25, 6] = "Descuentos:";
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[25, 7], xlWorkSheet.Cells[25, 7]];
        //            excelCellrange.NumberFormat = "@";
        //            xlWorkSheet.Cells[25, 7] = "0.00";
        //            /*----------------------------------------------------------------------------------------*/
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[26, 6], xlWorkSheet.Cells[26, 6]];
        //            xlWorkSheet.Cells[26, 6] = "I.E.P.S.:";
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[26, 7], xlWorkSheet.Cells[26, 7]];
        //            excelCellrange.NumberFormat = "@";
        //            xlWorkSheet.Cells[26, 7] = "0.00";
        //            /*----------------------------------------------------------------------------------------*/
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[27, 6], xlWorkSheet.Cells[27, 6]];
        //            xlWorkSheet.Cells[27, 6] = "I.V.A.:";
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[27, 7], xlWorkSheet.Cells[27, 7]];
        //            excelCellrange.NumberFormat = "@";
        //            xlWorkSheet.Cells[27, 7] = "0.00";
        //            /*----------------------------------------------------------------------------------------*/
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[28, 6], xlWorkSheet.Cells[28, 6]];
        //            xlWorkSheet.Cells[28, 6] = "Retención I.S.R.:";
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[28, 7], xlWorkSheet.Cells[28, 7]];
        //            excelCellrange.NumberFormat = "@";
        //            xlWorkSheet.Cells[28, 7] = "0.00";
        //            /*----------------------------------------------------------------------------------------*/
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[29, 6], xlWorkSheet.Cells[29, 6]];
        //            xlWorkSheet.Cells[29, 6] = "Retención I.V.A.:";
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[29, 7], xlWorkSheet.Cells[29, 7]];
        //            excelCellrange.NumberFormat = "@";
        //            xlWorkSheet.Cells[29, 7] = "0.00".ToString();
        //            /*----------------------------------------------------------------------------------------*/
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[30, 6], xlWorkSheet.Cells[30, 6]];
        //            xlWorkSheet.Cells[30, 6] = "Total";
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[30, 7], xlWorkSheet.Cells[30, 7]];
        //            excelCellrange.NumberFormat = "@";
        //            xlWorkSheet.Cells[30, 7] = "0.00";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[24, 6], xlWorkSheet.Cells[30, 6]];
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkGreen);

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[24, 7], xlWorkSheet.Cells[30, 7]];
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gray);

        //            #endregion

        //            #region tabla importe letra

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[26, 1], xlWorkSheet.Cells[26, 5]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkGreen);
        //            xlWorkSheet.Cells[26, 1] = "Importe con letra";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[27, 1], xlWorkSheet.Cells[27, 5]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            xlWorkSheet.Cells[27, 1] = "cero Pesos 00/100 M.N.";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[26, 1], xlWorkSheet.Cells[27, 5]];
        //            Excel.Borders border2 = excelCellrange.Borders;
        //            excelCellrange.Font.Size = 7;
        //            border2.LineStyle = Excel.XlLineStyle.xlContinuous;
        //            border2.Weight = 2d;

        //            #region letras
        //            #endregion
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[28, 1], xlWorkSheet.Cells[28, 5]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
        //            xlWorkSheet.Cells[28, 1] = "Lugar de expedición";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[29, 1], xlWorkSheet.Cells[29, 5]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
        //            xlWorkSheet.Cells[29, 1] = string.Format("{0}, {1}, {2}, {3}, {4}",
        //                empresa.calle.ToUpper(), empresa.municipio.ToUpper(),
        //                empresa.cp, empresa.estado.ToUpper(), empresa.pais.ToUpper());
        //            // "187 Diagonal x 94 y 96 424, DZUNUNCAN, 97315, MERIDA, MERIDA, YUCATAN, MEXICO";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[30, 1], xlWorkSheet.Cells[30, 5]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 6;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
        //            xlWorkSheet.Cells[30, 1] = "Método de pago:      No identificado";

        //            #endregion                    

        //            #region firamas

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[31, 1], xlWorkSheet.Cells[33, 2]];
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.VerticalAlignment = Excel.XlVAlign.xlVAlignTop;
        //            excelCellrange.MergeCells = true;

        //            border2 = excelCellrange.Borders;
        //            excelCellrange.Font.Size = 7;
        //            border2.LineStyle = Excel.XlLineStyle.xlContinuous;
        //            border2.Weight = 2d;
        //            xlWorkSheet.Cells[31, 1] = "Documento";
        //            /*--------------------------------------------------------------------------------------------*/
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[31, 3], xlWorkSheet.Cells[33, 5]];
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.VerticalAlignment = Excel.XlVAlign.xlVAlignTop;
        //            excelCellrange.MergeCells = true;

        //            border2 = excelCellrange.Borders;
        //            excelCellrange.Font.Size = 7;
        //            border2.LineStyle = Excel.XlLineStyle.xlContinuous;
        //            border2.Weight = 2d;
        //            xlWorkSheet.Cells[31, 3] = "Recibí de conformidad:" +
        //                Environment.NewLine +
        //                Environment.NewLine +
        //                Environment.NewLine + "Firma del destinatario:";

        //            /*--------------------------------------------------------------------------------------------*/
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[31, 6], xlWorkSheet.Cells[33, 7]];
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.VerticalAlignment = Excel.XlVAlign.xlVAlignTop;
        //            excelCellrange.MergeCells = true;

        //            border2 = excelCellrange.Borders;
        //            excelCellrange.Font.Size = 7;
        //            border2.LineStyle = Excel.XlLineStyle.xlContinuous;
        //            border2.Weight = 2d;
        //            xlWorkSheet.Cells[31, 6] = "Observaciones:";

        //            #endregion

        //            #region imagenQR

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[35, 1], xlWorkSheet.Cells[40, 2]];
        //            excelCellrange.MergeCells = true;


        //            var qrEncoder = new Gma.QrCodeNet.Encoding.QrEncoder(Gma.QrCodeNet.Encoding.ErrorCorrectionLevel.H);
        //            var qrCode = qrEncoder.Encode(string.Format("re={0}&rr={1}&0000000000.000000&id={2}"
        //                , empresa.rfc.ToUpper()
        //                , cliente.rfc.ToUpper()
        //                , datosFactura.UUID));

        //            var renderer = new Gma.QrCodeNet.Encoding.Windows.Render.GraphicsRenderer(
        //                new Gma.QrCodeNet.Encoding.Windows.Render.FixedModuleSize
        //                (5, Gma.QrCodeNet.Encoding.Windows.Render.QuietZoneModules.Two), System.Drawing.Brushes.Black, System.Drawing.Brushes.White);
        //            using (var stream = new FileStream("qrcode.png", FileMode.Create))
        //                renderer.WriteToStream(qrCode.Matrix, System.Drawing.Imaging.ImageFormat.Png, stream);

        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
        //            excelCellrange.Font.Size = 8;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            //oTable.Cell(1, 1).Range.InlineShapes.AddPicture(@"e:\SisCompras\WpfClient\Recursos\logo_ciltra_transparente1.png");
        //            //xlWorkSheet.Cells[1, 1] = "lugar para la imagen";

        //            if (File.Exists(System.Windows.Forms.Application.StartupPath + @"\\qrcode.png"))
        //            {

        //            }
        //            xlWorkSheet.Shapes.AddPicture(System.Windows.Forms.Application.StartupPath + @"\\qrcode.png",
        //                Microsoft.Office.Core.MsoTriState.msoFalse,
        //                Microsoft.Office.Core.MsoTriState.msoCTrue,
        //                float.Parse(excelCellrange.Left.ToString()), float.Parse(excelCellrange.Top.ToString()),
        //                float.Parse((excelCellrange.Width).ToString()), float.Parse((excelCellrange.Height).ToString()));
        //            #endregion

        //            #region tabla emitido

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[35, 3], xlWorkSheet.Cells[35, 7]];
        //            excelCellrange.MergeCells = true;
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Silver);
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkGreen);
        //            xlWorkSheet.Cells[35, 3] = "Este documento es una representación impresa de un CFDI";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[36, 3], xlWorkSheet.Cells[36, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.VerticalAlignment = Excel.XlVAlign.xlVAlignTop;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gray);
        //            xlWorkSheet.Cells[36, 3] = "Efectos fiscales al pago.  PAGO EN UNA SOLA EXHIBICION";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[35, 3], xlWorkSheet.Cells[36, 7]];
        //            Excel.Borders bordersSombreado = excelCellrange.Borders;
        //            bordersSombreado[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
        //            bordersSombreado[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;
        //            bordersSombreado[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
        //            bordersSombreado[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;

        //            #endregion

        //            #region datosFactura

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[37, 3], xlWorkSheet.Cells[37, 4]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkGreen);
        //            xlWorkSheet.Cells[37, 3] = "Serie del Certificado del emisor:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[38, 3], xlWorkSheet.Cells[38, 4]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkGreen);
        //            xlWorkSheet.Cells[38, 3] = "Folio Fiscal:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[39, 3], xlWorkSheet.Cells[39, 4]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkGreen);
        //            xlWorkSheet.Cells[39, 3] = "No. de serie del Certificado del SAT:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[40, 3], xlWorkSheet.Cells[40, 4]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkGreen);
        //            xlWorkSheet.Cells[40, 3] = "Fecha y hora de certificación:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[37, 5], xlWorkSheet.Cells[37, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
        //            excelCellrange.NumberFormat = "@";
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gray);
        //            //excelCellrange.te
        //            xlWorkSheet.Cells[37, 5] = datosFactura.noCertificado;

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[38, 5], xlWorkSheet.Cells[38, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
        //            excelCellrange.NumberFormat = "@";
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gray);
        //            xlWorkSheet.Cells[38, 5] = datosFactura.UUID;

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[39, 5], xlWorkSheet.Cells[39, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gray);
        //            excelCellrange.NumberFormat = "@";
        //            xlWorkSheet.Cells[39, 5] = datosFactura.noCertificadoSAT;

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[40, 5], xlWorkSheet.Cells[40, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gray);
        //            xlWorkSheet.Cells[40, 5] = DateTime.Now.ToString("dd-MM-yy HH:mm:ss tt");

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[37, 3], xlWorkSheet.Cells[40, 7]];
        //            border = excelCellrange.Borders;
        //            border.LineStyle = Excel.XlLineStyle.xlContinuous;
        //            border.Weight = 2d;

        //            #endregion

        //            #region tabla final
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[41, 1], xlWorkSheet.Cells[41, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkGreen);
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
        //            xlWorkSheet.Cells[41, 1] = "Sello digital del CFDI:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[42, 1], xlWorkSheet.Cells[42, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 5;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
        //            xlWorkSheet.Cells[42, 1] = datosFactura.sello;

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[43, 1], xlWorkSheet.Cells[43, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkGreen);
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
        //            xlWorkSheet.Cells[43, 1] = "Sello del SAT:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[44, 1], xlWorkSheet.Cells[44, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 5;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
        //            xlWorkSheet.Cells[44, 1] = datosFactura.selloSAT;

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[45, 1], xlWorkSheet.Cells[45, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 6;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkGreen);
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
        //            xlWorkSheet.Cells[45, 1] = "Cadena original del complemento del certificación digital del SAT:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[46, 1], xlWorkSheet.Cells[46, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 5;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
        //            excelCellrange.EntireRow.AutoFit();
        //            xlWorkSheet.Cells[46, 1] = string.Format("||1.0|{0}|{1}|{2}|{3}|| "
        //                , datosFactura.UUID
        //                , DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")
        //                , datosFactura.selloCFD,
        //                datosFactura.selloSAT);

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[41, 1], xlWorkSheet.Cells[45, 7]];
        //            border = excelCellrange.Borders;
        //            border.LineStyle = Excel.XlLineStyle.xlContinuous;
        //            border.Weight = 2d;
        //            #endregion

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[100, 100]];
        //            excelCellrange.EntireColumn.AutoFit();
        //            var x = excelCellrange.Height;
        //            excelCellrange.EntireRow.AutoFit();

        //            xlWorkBook.SaveAs(archivo + ".xlsx");
        //            xlWorkSheet.PrintOutEx(1, 2, 3, false); //linea para imprimir
        //            rutaExcel = archivo + ".xlsx";
        //            //bool resp = convertirExcelToSps(archivo);
        //            //if (resp)
        //            //{
        //            //    imprimirReporte(archivo + ".xps");
        //            //}
        //            return true;
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        MessageBox.Show(e.Message);
        //        return false;
        //    }
        //    finally
        //    {
        //        xlWorkBook.Close();
        //        xlApp.Quit();
        //        GC.WaitForPendingFinalizers();
        //        GC.Collect();
        //        GC.WaitForPendingFinalizers();
        //        GC.Collect();
        //    }
        //}

        //public bool cartaPorteFalso(ComercialCliente cliente, ComercialEmpresa empresa, DatosFacturaXML datosFactura, List<CartaPorte> cartaPorte, Viaje viaje)
        //{
        //    string archivo = "";
        //    try
        //    {
        //        string path = System.Windows.Forms.Application.StartupPath;
        //        if (!Directory.Exists(path + "\\reportes\\"))
        //        {
        //            DirectoryInfo dir = Directory.CreateDirectory(path + "\\reportes\\");
        //        }

        //        //archivo = string.Format(@"{0}{1}", path + "\\reportes\\", cartaPorte.NumGuiaId);
        //        archivo = string.Format(@"{0}{1}", path + "\\reportes\\", "reporte");

        //        if (File.Exists(archivo + ".xlsx"))
        //        {
        //            File.Delete(archivo + ".xlsx");
        //        }
        //        if (File.Exists(archivo + ".xps"))
        //        {
        //            File.Delete(archivo + ".xps");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message);
        //        return false;
        //    }

        //    Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
        //    Excel.Workbook xlWorkBook = xlApp.Workbooks.Add();
        //    //Microsoft.Office.Interop.Excel.Worksheet excelSheet;
        //    Microsoft.Office.Interop.Excel.Range excelCellrange;

        //    try
        //    {

        //        if (xlApp == null)
        //        {
        //            MessageBox.Show("Excel is not properly installed!!");
        //            return false;
        //        }
        //        else
        //        {

        //            Excel.Worksheet xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
        //            xlWorkSheet.PageSetup.LeftMargin = 25;
        //            xlWorkSheet.PageSetup.RightMargin = 25;
        //            xlWorkSheet.PageSetup.TopMargin = 15;
        //            xlWorkSheet.PageSetup.BottomMargin = 5;
        //            //xlWorkSheet.PageSetup.Orientation = Microsoft.Office.Interop.Excel.XlPageOrientation.xlLandscape;

        //            xlWorkSheet.Name = "CartaPorte";

        //            //excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[6, 4]];
        //            //excelCellrange.Merge(true);

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[6, 4]];
        //            excelCellrange.MergeCells = true;

        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.Font.Size = 8;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            //oTable.Cell(1, 1).Range.InlineShapes.AddPicture(@"e:\SisCompras\WpfClient\Recursos\logo_ciltra_transparente1.png");
        //            //xlWorkSheet.Cells[1, 1] = "lugar para la imagen";

        //            if (File.Exists(System.Windows.Forms.Application.StartupPath + "\\logoPegaso.png"))
        //            {
        //                xlWorkSheet.Shapes.AddPicture(System.Windows.Forms.Application.StartupPath + "\\logoPegaso.png",
        //                Microsoft.Office.Core.MsoTriState.msoFalse,
        //                Microsoft.Office.Core.MsoTriState.msoCTrue,
        //                float.Parse(excelCellrange.Left.ToString()), float.Parse(excelCellrange.Top.ToString()),
        //                150, 50
        //                );
        //            }

        //            #region primera tabla
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[1, 5], xlWorkSheet.Cells[1, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 28;
        //            excelCellrange.Font.Name = "Free 3 of 9 Extended";
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            //excelCellrang= e.Font.FontStyle = "Bold";
        //            string listRem = string.Empty;
        //            foreach (var item in cartaPorte)
        //            {
        //                listRem = listRem + item.grupoCartaPorte;
        //            }
        //            xlWorkSheet.Cells[1, 5] = "*" + cartaPorte[0].numGuiaId.ToString() + "-" + listRem.Trim('-') + "*";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[2, 5], xlWorkSheet.Cells[2, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 8;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Silver);
        //            excelCellrange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkGreen);
        //            xlWorkSheet.Cells[2, 5] = "CARTA PORTE";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[3, 5], xlWorkSheet.Cells[3, 5]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            //excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
        //            xlWorkSheet.Cells[3, 5] = "Viaje:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[4, 5], xlWorkSheet.Cells[4, 5]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            //excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
        //            xlWorkSheet.Cells[4, 5] = "Carta Portes:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[5, 5], xlWorkSheet.Cells[5, 5]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            //excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
        //            xlWorkSheet.Cells[5, 5] = "Fecha:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[6, 5], xlWorkSheet.Cells[6, 5]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            //excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
        //            xlWorkSheet.Cells[6, 5] = "Hora:";
        //            /*----------------------------------------------------------------------------------------------------*/

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[3, 6], xlWorkSheet.Cells[3, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 8;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
        //            xlWorkSheet.Cells[3, 6] = cartaPorte[0].numGuiaId; //ACA Iria el folio.

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[4, 6], xlWorkSheet.Cells[4, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
        //            listRem = string.Empty;
        //            foreach (var item in cartaPorte)
        //            {
        //                listRem = listRem + item.grupoCartaPorte;
        //            }
        //            xlWorkSheet.Cells[4, 6] = listRem.Trim('-'); //ACA Iria el folio.

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[5, 6], xlWorkSheet.Cells[5, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            //excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
        //            xlWorkSheet.Cells[5, 6] = DateTime.Now.ToString("dd/MMM/yyyy"); // aca la fecha

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[6, 6], xlWorkSheet.Cells[6, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            //excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
        //            xlWorkSheet.Cells[6, 6] = DateTime.Now.ToString("HH:mm:ss"); // aca la fecha hora
        //            #endregion

        //            #region datos empresa

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[7, 1], xlWorkSheet.Cells[7, 4]];
        //            excelCellrange.Merge(true);
        //            xlWorkSheet.Cells[7, 1] = empresa.nombre.ToUpper(); // aca la empresa.

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[8, 1], xlWorkSheet.Cells[8, 4]];
        //            excelCellrange.Merge(true);
        //            xlWorkSheet.Cells[8, 1] = empresa.rfc.ToUpper(); // aca la RFC.

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[9, 1], xlWorkSheet.Cells[9, 4]];
        //            excelCellrange.Merge(true);
        //            xlWorkSheet.Cells[9, 1] = "No Aplica";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[7, 5], xlWorkSheet.Cells[7, 7]];
        //            excelCellrange.Merge(true);
        //            xlWorkSheet.Cells[7, 5] = empresa.calle.ToUpper(); // aca la direccion empresa.

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[8, 5], xlWorkSheet.Cells[8, 5]];
        //            excelCellrange.Merge(true);
        //            xlWorkSheet.Cells[8, 5] = empresa.municipio.ToUpper(); // ciudad empresa
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[8, 5], xlWorkSheet.Cells[8, 5]];
        //            excelCellrange.Merge(true);
        //            xlWorkSheet.Cells[8, 7] = empresa.cp; // CP empresa

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[8, 5], xlWorkSheet.Cells[8, 5]];
        //            excelCellrange.Merge(true);
        //            xlWorkSheet.Cells[9, 5] = empresa.municipio.ToUpper(); // ciudad empresa
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[8, 5], xlWorkSheet.Cells[8, 5]];
        //            excelCellrange.Merge(true);
        //            xlWorkSheet.Cells[9, 7] = empresa.estado.ToUpper(); // estado empresa

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[9, 5], xlWorkSheet.Cells[9, 5]];
        //            excelCellrange.Merge(true);
        //            xlWorkSheet.Cells[10, 5] = empresa.pais.ToUpper(); // pais empresa

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[7, 1], xlWorkSheet.Cells[10, 7]];
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
        //            excelCellrange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkGreen);
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);

        //            //Excel.Borders border = excelCellrange.Borders;
        //            //border.LineStyle = Excel.XlLineStyle.xlContinuous;
        //            //border.Weight = 2d;

        //            #endregion

        //            #region origen

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[11, 1], xlWorkSheet.Cells[11, 1]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkGreen);
        //            xlWorkSheet.Cells[11, 1] = "ORIGEN:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[11, 2], xlWorkSheet.Cells[11, 2]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Silver);
        //            xlWorkSheet.Cells[11, 2] = string.Format("Ciudad: {0}", cliente.municipio.ToUpper()); // ciudad origen

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[11, 5], xlWorkSheet.Cells[11, 5]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Silver);
        //            xlWorkSheet.Cells[11, 5] = string.Format("Estado: {0}", cliente.estado.ToUpper()); // estado origen

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[11, 1], xlWorkSheet.Cells[11, 5]];
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
        //            #endregion

        //            #region remitente

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[12, 1], xlWorkSheet.Cells[12, 1]];
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkGreen);
        //            xlWorkSheet.Cells[12, 1] = "REMITENTE:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[12, 2], xlWorkSheet.Cells[12, 2]];
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gray);
        //            xlWorkSheet.Cells[12, 2] = cliente.nombre.ToUpper(); // NOMBRE CLIENTE

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[13, 1], xlWorkSheet.Cells[13, 1]];
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkGreen);
        //            xlWorkSheet.Cells[13, 1] = "RFC:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[13, 2], xlWorkSheet.Cells[13, 2]];
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gray);
        //            xlWorkSheet.Cells[13, 2] = cliente.rfc.ToUpper(); // RFC CLIENTE

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[14, 1], xlWorkSheet.Cells[14, 1]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            xlWorkSheet.Cells[14, 1] = "Domicilio:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[14, 2], xlWorkSheet.Cells[14, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 6;
        //            xlWorkSheet.Cells[14, 2] = cliente.calle.ToUpper(); // aca la direccion del cliente

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[15, 1], xlWorkSheet.Cells[15, 1]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            xlWorkSheet.Cells[15, 1] = "Telefono:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[15, 2], xlWorkSheet.Cells[15, 2]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            xlWorkSheet.Cells[15, 2] = cliente.telefono.ToUpper(); // aca el telefono del cliente

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[15, 4], xlWorkSheet.Cells[15, 4]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            xlWorkSheet.Cells[15, 4] = "Colonia:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[15, 5], xlWorkSheet.Cells[15, 5]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            xlWorkSheet.Cells[15, 5] = cliente.colonia.ToUpper(); // aca el telefono del cliente

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[15, 6], xlWorkSheet.Cells[15, 6]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            xlWorkSheet.Cells[15, 6] = "CP:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[15, 7], xlWorkSheet.Cells[15, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            xlWorkSheet.Cells[15, 7] = cliente.cp; // aca el CP del cliente

        //            /**/

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[16, 1], xlWorkSheet.Cells[16, 1]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            xlWorkSheet.Cells[16, 1] = "Ciudad:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[16, 2], xlWorkSheet.Cells[16, 2]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            xlWorkSheet.Cells[16, 2] = cliente.municipio.ToUpper(); // aca el telefono del cliente

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[16, 4], xlWorkSheet.Cells[16, 4]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            xlWorkSheet.Cells[16, 4] = "Estado:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[16, 5], xlWorkSheet.Cells[16, 5]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            xlWorkSheet.Cells[16, 5] = cliente.estado.ToUpper();

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[16, 6], xlWorkSheet.Cells[16, 6]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            xlWorkSheet.Cells[16, 6] = "Pais:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[16, 7], xlWorkSheet.Cells[16, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            xlWorkSheet.Cells[16, 7] = cliente.pais.ToUpper();


        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[14, 1], xlWorkSheet.Cells[16, 7]];
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gray);
        //            #endregion

        //            #region DESTINO
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[17, 1], xlWorkSheet.Cells[17, 1]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkGreen);
        //            xlWorkSheet.Cells[17, 1] = "DESTINO:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[17, 2], xlWorkSheet.Cells[17, 2]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            xlWorkSheet.Cells[17, 2] = string.Format("Ciudad: {0}", empresa.municipio.ToUpper()); // ciudad origen

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[17, 5], xlWorkSheet.Cells[17, 5]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            xlWorkSheet.Cells[17, 5] = string.Format("Estado: {0}", empresa.estado.ToUpper()); // estado origen

        //            #endregion

        //            #region destinatario

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[18, 1], xlWorkSheet.Cells[18, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkGreen);
        //            xlWorkSheet.Cells[18, 1] = "DESTINATARIO";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[19, 1], xlWorkSheet.Cells[19, 1]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            xlWorkSheet.Cells[19, 1] = "Domicilio:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[19, 2], xlWorkSheet.Cells[19, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            xlWorkSheet.Cells[19, 2] = empresa.calle.ToUpper();

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[20, 1], xlWorkSheet.Cells[20, 1]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            xlWorkSheet.Cells[20, 1] = "Telefono:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[20, 2], xlWorkSheet.Cells[20, 2]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            xlWorkSheet.Cells[20, 2] = empresa.telefono.ToUpper(); // aca el telefono del cliente

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[22, 4], xlWorkSheet.Cells[20, 4]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            xlWorkSheet.Cells[20, 4] = "Colonia:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[20, 5], xlWorkSheet.Cells[20, 5]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            xlWorkSheet.Cells[20, 5] = empresa.colonia.ToUpper(); // aca el telefono de la empresa

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[20, 6], xlWorkSheet.Cells[20, 6]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            xlWorkSheet.Cells[20, 6] = "CP:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[20, 7], xlWorkSheet.Cells[20, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            xlWorkSheet.Cells[20, 7] = empresa.cp; // aca el CP de la empresa

        //            ///*-------------------------------------------------------------------------------*/

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[21, 1], xlWorkSheet.Cells[21, 1]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            xlWorkSheet.Cells[21, 1] = "Ciudad:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[21, 2], xlWorkSheet.Cells[21, 2]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            xlWorkSheet.Cells[21, 2] = empresa.municipio.ToUpper(); // aca la ciudad del cliente

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[21, 4], xlWorkSheet.Cells[21, 4]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            xlWorkSheet.Cells[21, 4] = empresa.estado.ToUpper();

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[21, 5], xlWorkSheet.Cells[21, 5]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            xlWorkSheet.Cells[21, 5] = empresa.estado.ToUpper(); // aca el estado de la empresa

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[21, 6], xlWorkSheet.Cells[21, 6]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            xlWorkSheet.Cells[21, 6] = "País:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[21, 7], xlWorkSheet.Cells[21, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            xlWorkSheet.Cells[21, 7] = empresa.pais.ToUpper(); // aca el pais del cliente

        //            #endregion

        //            #region tabla detalles

        //            #region titulo tabla detalles

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[22, 1], xlWorkSheet.Cells[22, 1]];
        //            xlWorkSheet.Cells[22, 1] = "Cantidad. (Tons)";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[22, 2], xlWorkSheet.Cells[22, 2]];
        //            xlWorkSheet.Cells[22, 2] = "Unidad.";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[22, 3], xlWorkSheet.Cells[22, 4]];
        //            excelCellrange.Merge(true);
        //            xlWorkSheet.Cells[22, 3] = "Concepto / Descripción.";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[22, 5], xlWorkSheet.Cells[22, 7]];
        //            excelCellrange.Merge(true);
        //            xlWorkSheet.Cells[22, 5] = "Producto.";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[22, 1], xlWorkSheet.Cells[22, 7]];
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkGreen);
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
        //            #endregion

        //            #region datos tabla detalle
        //            int i = 23;
        //            foreach (var item in cartaPorte)
        //            {
        //                excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 1], xlWorkSheet.Cells[i, 1]];
        //                excelCellrange.NumberFormat = "@";
        //                xlWorkSheet.Cells[i, 1] = item.volumenDescarga.ToString();

        //                excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 2], xlWorkSheet.Cells[i, 2]];
        //                xlWorkSheet.Cells[i, 2] = "Servicio";

        //                excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 3], xlWorkSheet.Cells[i, 4]];
        //                excelCellrange.Merge(true);
        //                xlWorkSheet.Cells[i, 3] = item.zonaSelect.descripcion;

        //                excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 5], xlWorkSheet.Cells[i, 7]];
        //                excelCellrange.Merge(true);
        //                xlWorkSheet.Cells[i, 5] = item.producto.descripcion;
        //                i++;
        //            }

        //            i--;
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[23, 1], xlWorkSheet.Cells[(i), 7]];
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.Font.FontStyle = "Bold";


        //            #endregion


        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[22, 1], xlWorkSheet.Cells[(i), 7]];
        //            Excel.Borders border = excelCellrange.Borders;
        //            border.LineStyle = Excel.XlLineStyle.xlContinuous;
        //            border.Weight = 2d;
        //            #endregion

        //            //i++; //23

        //            #region valores 
        //            /*----------------------------------------------------------------------------------------*/
        //            /**/
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 1), 1], xlWorkSheet.Cells[(i + 2), 1]];
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 8;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            xlWorkSheet.Cells[(i + 1), 1] = "UNIDADES:";
        //            xlWorkSheet.Cells[(i + 2), 1] = "OPERADOR:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 1), 2], xlWorkSheet.Cells[(i + 1), 5]];
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 8;
        //            excelCellrange.Cells.Merge(true);
        //            xlWorkSheet.Cells[(i + 1), 2] = (viaje.tractor.clave + "/") + (viaje.remolque.clave)
        //                    + (viaje.dolly != null ? "/" + viaje.dolly.clave : "")
        //                    + (viaje.remolque2 != null ? "/" + viaje.remolque2.clave : "");

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 2), 2], xlWorkSheet.Cells[(i + 2), 5]];
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 8;
        //            excelCellrange.Cells.Merge(true);
        //            xlWorkSheet.Cells[(i + 2), 2] = viaje.operador.nombre;
        //            /**/
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 1), 6], xlWorkSheet.Cells[(i + 1), 6]]; //24
        //            xlWorkSheet.Cells[(i + 1), 6] = "Subtotal:";
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 1), 7], xlWorkSheet.Cells[(i + 1), 7]];
        //            excelCellrange.NumberFormat = "@";
        //            xlWorkSheet.Cells[(i + 1), 7] = "0.00";
        //            /*----------------------------------------------------------------------------------------*/
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 2), 6], xlWorkSheet.Cells[(i + 2), 6]]; //25
        //            xlWorkSheet.Cells[(i + 2), 6] = "Descuentos:";
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 2), 7], xlWorkSheet.Cells[(i + 2), 7]];
        //            excelCellrange.NumberFormat = "@";
        //            xlWorkSheet.Cells[(i + 2), 7] = "0.00";
        //            /*----------------------------------------------------------------------------------------*/
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 3), 6], xlWorkSheet.Cells[(i + 3), 6]]; //26
        //            xlWorkSheet.Cells[(i + 3), 6] = "I.E.P.S.:";
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 3), 7], xlWorkSheet.Cells[(i + 3), 7]];
        //            excelCellrange.NumberFormat = "@";
        //            xlWorkSheet.Cells[(i + 3), 7] = "0.00";
        //            /*----------------------------------------------------------------------------------------*/
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 4), 6], xlWorkSheet.Cells[(i + 4), 6]]; //27
        //            xlWorkSheet.Cells[(i + 4), 6] = "I.V.A.:";
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 4), 7], xlWorkSheet.Cells[(i + 4), 7]];
        //            excelCellrange.NumberFormat = "@";
        //            xlWorkSheet.Cells[(i + 4), 7] = "0.00";
        //            /*----------------------------------------------------------------------------------------*/
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 5), 6], xlWorkSheet.Cells[(i + 5), 6]]; //28
        //            xlWorkSheet.Cells[(i + 5), 6] = "Retención I.S.R.:";
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 5), 7], xlWorkSheet.Cells[(i + 5), 7]];
        //            excelCellrange.NumberFormat = "@";
        //            xlWorkSheet.Cells[(i + 5), 7] = "0.00";
        //            /*----------------------------------------------------------------------------------------*/
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 6), 6], xlWorkSheet.Cells[(i + 6), 6]]; //29
        //            xlWorkSheet.Cells[(i + 6), 6] = "Retención I.V.A.:";
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 6), 7], xlWorkSheet.Cells[(i + 6), 7]];
        //            excelCellrange.NumberFormat = "@";
        //            xlWorkSheet.Cells[(i + 6), 7] = "0.00".ToString();
        //            /*----------------------------------------------------------------------------------------*/
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 7), 6], xlWorkSheet.Cells[(i + 7), 6]]; //30
        //            xlWorkSheet.Cells[(i + 7), 6] = "Total";
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 7), 7], xlWorkSheet.Cells[(i + 7), 7]];
        //            excelCellrange.NumberFormat = "@";
        //            xlWorkSheet.Cells[(i + 7), 7] = "0.00";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 1), 6], xlWorkSheet.Cells[(i + 7), 6]];
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkGreen);

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 1), 7], xlWorkSheet.Cells[(i + 7), 7]];
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gray);

        //            #endregion

        //            #region tabla importe letra

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 3), 1], xlWorkSheet.Cells[(i + 3), 5]]; //26
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkGreen);
        //            xlWorkSheet.Cells[(i + 3), 1] = "Importe con letra";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 4), 1], xlWorkSheet.Cells[(i + 4), 5]]; //27
        //            excelCellrange.Merge(true);
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            xlWorkSheet.Cells[(i + 4), 1] = "cero Pesos 00/100 M.N.";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 3), 1], xlWorkSheet.Cells[(i + 4), 5]]; //26 / 27
        //            Excel.Borders border2 = excelCellrange.Borders;
        //            excelCellrange.Font.Size = 7;
        //            border2.LineStyle = Excel.XlLineStyle.xlContinuous;
        //            border2.Weight = 2d;

        //            #region letras
        //            #endregion
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 5), 1], xlWorkSheet.Cells[(i + 5), 5]]; //28
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
        //            xlWorkSheet.Cells[(i + 5), 1] = "Lugar de expedición";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 6), 1], xlWorkSheet.Cells[(i + 6), 5]]; //29
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
        //            xlWorkSheet.Cells[(i + 6), 1] = string.Format("{0}, {1}, {2}, {3}, {4}",
        //                empresa.calle.ToUpper(), empresa.municipio.ToUpper(),
        //                empresa.cp, empresa.estado.ToUpper(), empresa.pais.ToUpper());
        //            // "187 Diagonal x 94 y 96 424, DZUNUNCAN, 97315, MERIDA, MERIDA, YUCATAN, MEXICO";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 7), 1], xlWorkSheet.Cells[(i + 7), 5]]; //30
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 6;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
        //            xlWorkSheet.Cells[(i + 7), 1] = "Método de pago:      No identificado";

        //            #endregion                    

        //            #region firamas

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 8), 1], xlWorkSheet.Cells[(i + 10), 2]]; //31/33
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.VerticalAlignment = Excel.XlVAlign.xlVAlignTop;
        //            excelCellrange.MergeCells = true;

        //            border2 = excelCellrange.Borders;
        //            excelCellrange.Font.Size = 7;
        //            border2.LineStyle = Excel.XlLineStyle.xlContinuous;
        //            border2.Weight = 2d;
        //            xlWorkSheet.Cells[(i + 8), 1] = "Documento"; //31
        //            /*--------------------------------------------------------------------------------------------*/
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 8), 3], xlWorkSheet.Cells[(i + 10), 5]]; //31/33
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.VerticalAlignment = Excel.XlVAlign.xlVAlignTop;
        //            excelCellrange.MergeCells = true;

        //            border2 = excelCellrange.Borders;
        //            excelCellrange.Font.Size = 7;
        //            border2.LineStyle = Excel.XlLineStyle.xlContinuous;
        //            border2.Weight = 2d;
        //            xlWorkSheet.Cells[(i + 8), 3] = "Recibí de conformidad:" + //31
        //                Environment.NewLine +
        //                Environment.NewLine +
        //                Environment.NewLine + "Firma del destinatario:";

        //            /*--------------------------------------------------------------------------------------------*/
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 8), 6], xlWorkSheet.Cells[(i + 10), 7]]; //31/33
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.VerticalAlignment = Excel.XlVAlign.xlVAlignTop;
        //            excelCellrange.MergeCells = true;

        //            border2 = excelCellrange.Borders;
        //            excelCellrange.Font.Size = 7;
        //            border2.LineStyle = Excel.XlLineStyle.xlContinuous;
        //            border2.Weight = 2d;
        //            xlWorkSheet.Cells[(i + 8), 6] = "Observaciones:"; //31

        //            #endregion

        //            i += 11; //igualada a 35
        //            #region imagenQR

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 1], xlWorkSheet.Cells[(i + 5), 2]]; //35/40
        //            excelCellrange.MergeCells = true;


        //            var qrEncoder = new Gma.QrCodeNet.Encoding.QrEncoder(Gma.QrCodeNet.Encoding.ErrorCorrectionLevel.H);
        //            var qrCode = qrEncoder.Encode(string.Format("re={0}&rr={1}&0000000000.000000&id={2}"
        //                , empresa.rfc.ToUpper()
        //                , cliente.rfc.ToUpper()
        //                , datosFactura.UUID));

        //            var renderer = new Gma.QrCodeNet.Encoding.Windows.Render.GraphicsRenderer(
        //                new Gma.QrCodeNet.Encoding.Windows.Render.FixedModuleSize
        //                (5, Gma.QrCodeNet.Encoding.Windows.Render.QuietZoneModules.Two), System.Drawing.Brushes.Black, System.Drawing.Brushes.White);
        //            using (var stream = new FileStream("qrcode.png", FileMode.Create))
        //                renderer.WriteToStream(qrCode.Matrix, System.Drawing.Imaging.ImageFormat.Png, stream);

        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
        //            excelCellrange.Font.Size = 8;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            //oTable.Cell(1, 1).Range.InlineShapes.AddPicture(@"e:\SisCompras\WpfClient\Recursos\logo_ciltra_transparente1.png");
        //            //xlWorkSheet.Cells[1, 1] = "lugar para la imagen";

        //            if (File.Exists(System.Windows.Forms.Application.StartupPath + @"\\qrcode.png"))
        //            {

        //            }
        //            xlWorkSheet.Shapes.AddPicture(System.Windows.Forms.Application.StartupPath + @"\\qrcode.png",
        //                Microsoft.Office.Core.MsoTriState.msoFalse,
        //                Microsoft.Office.Core.MsoTriState.msoCTrue,
        //                float.Parse(excelCellrange.Left.ToString()), float.Parse(excelCellrange.Top.ToString()),
        //                float.Parse((excelCellrange.Width).ToString()), float.Parse((excelCellrange.Height).ToString()));
        //            #endregion

        //            #region tabla emitido

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 3], xlWorkSheet.Cells[i, 7]]; //35
        //            excelCellrange.MergeCells = true;
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Silver);
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkGreen);
        //            xlWorkSheet.Cells[i, 3] = "Este documento es una representación impresa de un CFDI";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 1), 3], xlWorkSheet.Cells[(i + 1), 7]]; //36
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.VerticalAlignment = Excel.XlVAlign.xlVAlignTop;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gray);
        //            xlWorkSheet.Cells[(i + 1), 3] = "Efectos fiscales al pago.  PAGO EN UNA SOLA EXHIBICION";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 3], xlWorkSheet.Cells[(i + 1), 7]]; //35/36
        //            Excel.Borders bordersSombreado = excelCellrange.Borders;
        //            bordersSombreado[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
        //            bordersSombreado[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;
        //            bordersSombreado[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
        //            bordersSombreado[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;

        //            #endregion

        //            #region datosFactura

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 2), 3], xlWorkSheet.Cells[(i + 2), 4]]; //37
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkGreen);
        //            xlWorkSheet.Cells[(i + 2), 3] = "Serie del Certificado del emisor:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 3), 3], xlWorkSheet.Cells[(i + 3), 4]]; //38
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkGreen);
        //            xlWorkSheet.Cells[(i + 3), 3] = "Folio Fiscal:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 4), 3], xlWorkSheet.Cells[(i + 4), 4]]; //39
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkGreen);
        //            xlWorkSheet.Cells[(i + 4), 3] = "No. de serie del Certificado del SAT:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 5), 3], xlWorkSheet.Cells[(i + 5), 4]]; //40
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkGreen);
        //            xlWorkSheet.Cells[(i + 5), 3] = "Fecha y hora de certificación:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 2), 5], xlWorkSheet.Cells[(i + 2), 7]]; //37
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
        //            excelCellrange.NumberFormat = "@";
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gray);
        //            //excelCellrange.te
        //            xlWorkSheet.Cells[(i + 2), 5] = datosFactura.noCertificado;

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 3), 5], xlWorkSheet.Cells[(i + 3), 7]]; //38
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
        //            excelCellrange.NumberFormat = "@";
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gray);
        //            xlWorkSheet.Cells[(i + 3), 5] = datosFactura.UUID;

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 4), 5], xlWorkSheet.Cells[(i + 4), 7]]; //39
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gray);
        //            excelCellrange.NumberFormat = "@";
        //            xlWorkSheet.Cells[(i + 4), 5] = datosFactura.noCertificadoSAT;

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 5), 5], xlWorkSheet.Cells[(i + 5), 7]]; // 40
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gray);
        //            xlWorkSheet.Cells[(i + 5), 5] = DateTime.Now.ToString("dd-MM-yy HH:mm:ss tt");

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 2), 3], xlWorkSheet.Cells[(i + 5), 7]]; //37/40
        //            border = excelCellrange.Borders;
        //            border.LineStyle = Excel.XlLineStyle.xlContinuous;
        //            border.Weight = 2d;

        //            #endregion

        //            i += 5; //igua a 40
        //            #region tabla final
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 1), 1], xlWorkSheet.Cells[(i + 1), 7]]; //41
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkGreen);
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
        //            xlWorkSheet.Cells[(i + 1), 1] = "Sello digital del CFDI:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 2), 1], xlWorkSheet.Cells[(i + 2), 7]]; //42
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 5;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
        //            xlWorkSheet.Cells[(i + 2), 1] = datosFactura.sello;

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 3), 1], xlWorkSheet.Cells[(i + 3), 7]]; //43
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkGreen);
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
        //            xlWorkSheet.Cells[(i + 3), 1] = "Sello del SAT:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 4), 1], xlWorkSheet.Cells[(i + 4), 7]]; //44
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 5;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
        //            xlWorkSheet.Cells[(i + 4), 1] = datosFactura.selloSAT;

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 5), 1], xlWorkSheet.Cells[(i + 5), 7]]; //45
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 6;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkGreen);
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
        //            xlWorkSheet.Cells[(i + 5), 1] = "Cadena original del complemento del certificación digital del SAT:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 6), 1], xlWorkSheet.Cells[(i + 6), 7]]; //46
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 5;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
        //            excelCellrange.EntireRow.AutoFit();
        //            xlWorkSheet.Cells[(i + 6), 1] = string.Format("||1.0|{0}|{1}|{2}|{3}|| "
        //                , datosFactura.UUID
        //                , DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")
        //                , datosFactura.selloCFD,
        //                datosFactura.selloSAT);

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 1), 1], xlWorkSheet.Cells[(i + 6), 7]]; //41/45
        //            border = excelCellrange.Borders;
        //            border.LineStyle = Excel.XlLineStyle.xlContinuous;
        //            border.Weight = 2d;
        //            #endregion

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[100, 100]];
        //            excelCellrange.EntireColumn.AutoFit();
        //            var x = excelCellrange.Height;
        //            excelCellrange.EntireRow.AutoFit();

        //            xlWorkBook.SaveAs(archivo + ".xlsx");
        //            xlWorkSheet.PrintOutEx(1, 2, 1, false); //linea para imprimir
        //            rutaExcel = archivo + ".xlsx";
        //            //bool resp = convertirExcelToSps(archivo);
        //            //if (resp)
        //            //{
        //            //    imprimirReporte(archivo + ".xps");
        //            //}
        //            return true;
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        MessageBox.Show(e.Message);
        //        return false;
        //    }
        //    finally
        //    {
        //        xlWorkBook.Close();
        //        xlApp.Quit();
        //        GC.WaitForPendingFinalizers();
        //        GC.Collect();
        //        GC.WaitForPendingFinalizers();
        //        GC.Collect();
        //    }
        //}

        //public bool cartaPorteFalsoAtlante(ComercialCliente cliente, ComercialEmpresa empresa, DatosFacturaXML datosFactura, List<CartaPorte> cartaPorte, Viaje viaje)
        //{
        //    string archivo = "";
        //    try
        //    {
        //        string path = System.Windows.Forms.Application.StartupPath;
        //        if (!Directory.Exists(path + "\\reportes\\"))
        //        {
        //            DirectoryInfo dir = Directory.CreateDirectory(path + "\\reportes\\");
        //        }

        //        //archivo = string.Format(@"{0}{1}", path + "\\reportes\\", cartaPorte.NumGuiaId);
        //        archivo = string.Format(@"{0}{1}", path + "\\reportes\\", "reporte");

        //        if (File.Exists(archivo + ".xlsx"))
        //        {
        //            File.Delete(archivo + ".xlsx");
        //        }
        //        if (File.Exists(archivo + ".xps"))
        //        {
        //            File.Delete(archivo + ".xps");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message);
        //        return false;
        //    }

        //    Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
        //    Excel.Workbook xlWorkBook = xlApp.Workbooks.Add();
        //    //Microsoft.Office.Interop.Excel.Worksheet excelSheet;
        //    Microsoft.Office.Interop.Excel.Range excelCellrange;

        //    try
        //    {

        //        if (xlApp == null)
        //        {
        //            MessageBox.Show("Excel is not properly installed!!");
        //            return false;
        //        }
        //        else
        //        {

        //            Excel.Worksheet xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
        //            xlWorkSheet.PageSetup.LeftMargin = 25;
        //            xlWorkSheet.PageSetup.RightMargin = 25;
        //            xlWorkSheet.PageSetup.TopMargin = 15;
        //            xlWorkSheet.PageSetup.BottomMargin = 5;
        //            //xlWorkSheet.PageSetup.Orientation = Microsoft.Office.Interop.Excel.XlPageOrientation.xlLandscape;

        //            xlWorkSheet.Name = "CartaPorte";

        //            //excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[6, 4]];
        //            //excelCellrange.Merge(true);

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[6, 4]];
        //            excelCellrange.MergeCells = true;

        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.Font.Size = 8;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            //oTable.Cell(1, 1).Range.InlineShapes.AddPicture(@"e:\SisCompras\WpfClient\Recursos\logo_ciltra_transparente1.png");
        //            //xlWorkSheet.Cells[1, 1] = "lugar para la imagen";


        //            if (File.Exists(System.Windows.Forms.Application.StartupPath + "\\atlanteLogo.jpg"))
        //            {
        //                xlWorkSheet.Shapes.AddPicture(System.Windows.Forms.Application.StartupPath + "\\atlanteLogo.jpg",
        //                Microsoft.Office.Core.MsoTriState.msoFalse,
        //                Microsoft.Office.Core.MsoTriState.msoCTrue,
        //                float.Parse(excelCellrange.Left.ToString()), float.Parse(excelCellrange.Top.ToString()),
        //                150, 50
        //                );
        //            }


        //            #region primera tabla
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[1, 5], xlWorkSheet.Cells[1, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 28;
        //            excelCellrange.Font.Name = "Free 3 of 9 Extended";
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            //excelCellrang= e.Font.FontStyle = "Bold";
        //            string listRem = string.Empty;
        //            foreach (var item in cartaPorte)
        //            {
        //                listRem = listRem + item.grupoCartaPorte;
        //            }
        //            xlWorkSheet.Cells[1, 5] = "*" + cartaPorte[0].numGuiaId.ToString() + "-" + listRem.Trim('-') + "*";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[2, 5], xlWorkSheet.Cells[2, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 8;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Silver);
        //            excelCellrange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.SteelBlue);
        //            xlWorkSheet.Cells[2, 5] = "CARTA PORTE";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[3, 5], xlWorkSheet.Cells[3, 5]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            //excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
        //            xlWorkSheet.Cells[3, 5] = "Viaje:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[4, 5], xlWorkSheet.Cells[4, 5]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            //excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
        //            xlWorkSheet.Cells[4, 5] = "Carta Portes:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[5, 5], xlWorkSheet.Cells[5, 5]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            //excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
        //            xlWorkSheet.Cells[5, 5] = "Fecha:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[6, 5], xlWorkSheet.Cells[6, 5]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            //excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
        //            xlWorkSheet.Cells[6, 5] = "Hora:";
        //            /*----------------------------------------------------------------------------------------------------*/

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[3, 6], xlWorkSheet.Cells[3, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 8;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
        //            xlWorkSheet.Cells[3, 6] = cartaPorte[0].numGuiaId; //ACA Iria el folio.

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[4, 6], xlWorkSheet.Cells[4, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
        //            listRem = string.Empty;
        //            foreach (var item in cartaPorte)
        //            {
        //                listRem = listRem + item.grupoCartaPorte;
        //            }
        //            xlWorkSheet.Cells[4, 6] = listRem.Trim('-'); //ACA Iria el folio.

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[5, 6], xlWorkSheet.Cells[5, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            //excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
        //            xlWorkSheet.Cells[5, 6] = DateTime.Now.ToString("dd/MMM/yyyy"); // aca la fecha

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[6, 6], xlWorkSheet.Cells[6, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            //excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
        //            xlWorkSheet.Cells[6, 6] = DateTime.Now.ToString("HH:mm:ss"); // aca la fecha hora
        //            #endregion

        //            #region datos empresa

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[7, 1], xlWorkSheet.Cells[7, 4]];
        //            excelCellrange.Merge(true);
        //            xlWorkSheet.Cells[7, 1] = empresa.nombre.ToUpper(); // aca la empresa.

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[8, 1], xlWorkSheet.Cells[8, 4]];
        //            excelCellrange.Merge(true);
        //            xlWorkSheet.Cells[8, 1] = empresa.rfc.ToUpper(); // aca la RFC.

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[9, 1], xlWorkSheet.Cells[9, 4]];
        //            excelCellrange.Merge(true);
        //            xlWorkSheet.Cells[9, 1] = "No Aplica";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[7, 5], xlWorkSheet.Cells[7, 7]];
        //            excelCellrange.Merge(true);
        //            xlWorkSheet.Cells[7, 5] = empresa.calle.ToUpper(); // aca la direccion empresa.

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[8, 5], xlWorkSheet.Cells[8, 5]];
        //            excelCellrange.Merge(true);
        //            xlWorkSheet.Cells[8, 5] = empresa.municipio.ToUpper(); // ciudad empresa
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[8, 5], xlWorkSheet.Cells[8, 5]];
        //            excelCellrange.Merge(true);
        //            xlWorkSheet.Cells[8, 7] = empresa.cp; // CP empresa

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[8, 5], xlWorkSheet.Cells[8, 5]];
        //            excelCellrange.Merge(true);
        //            xlWorkSheet.Cells[9, 5] = empresa.municipio.ToUpper(); // ciudad empresa
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[8, 5], xlWorkSheet.Cells[8, 5]];
        //            excelCellrange.Merge(true);
        //            xlWorkSheet.Cells[9, 7] = empresa.estado.ToUpper(); // estado empresa

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[9, 5], xlWorkSheet.Cells[9, 5]];
        //            excelCellrange.Merge(true);
        //            xlWorkSheet.Cells[10, 5] = empresa.pais.ToUpper(); // pais empresa

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[7, 1], xlWorkSheet.Cells[10, 7]];
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
        //            excelCellrange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.SteelBlue);
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);

        //            //Excel.Borders border = excelCellrange.Borders;
        //            //border.LineStyle = Excel.XlLineStyle.xlContinuous;
        //            //border.Weight = 2d;

        //            #endregion

        //            #region origen

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[11, 1], xlWorkSheet.Cells[11, 1]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.SteelBlue);
        //            xlWorkSheet.Cells[11, 1] = "ORIGEN:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[11, 2], xlWorkSheet.Cells[11, 2]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Silver);
        //            xlWorkSheet.Cells[11, 2] = string.Format("Ciudad: {0}", cliente.municipio.ToUpper()); // ciudad origen

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[11, 5], xlWorkSheet.Cells[11, 5]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Silver);
        //            xlWorkSheet.Cells[11, 5] = string.Format("Estado: {0}", cliente.estado.ToUpper()); // estado origen

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[11, 1], xlWorkSheet.Cells[11, 5]];
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
        //            #endregion

        //            #region remitente

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[12, 1], xlWorkSheet.Cells[12, 1]];
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.SteelBlue);
        //            xlWorkSheet.Cells[12, 1] = "REMITENTE:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[12, 2], xlWorkSheet.Cells[12, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gray);
        //            xlWorkSheet.Cells[12, 2] = cliente.nombre.ToUpper(); // NOMBRE CLIENTE

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[13, 1], xlWorkSheet.Cells[13, 1]];
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.SteelBlue);
        //            xlWorkSheet.Cells[13, 1] = "RFC:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[13, 2], xlWorkSheet.Cells[13, 2]];
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gray);
        //            xlWorkSheet.Cells[13, 2] = cliente.rfc.ToUpper(); // RFC CLIENTE

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[14, 1], xlWorkSheet.Cells[14, 1]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            xlWorkSheet.Cells[14, 1] = "Domicilio:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[14, 2], xlWorkSheet.Cells[14, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 6;
        //            xlWorkSheet.Cells[14, 2] = cliente.calle.ToUpper(); // aca la direccion del cliente

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[15, 1], xlWorkSheet.Cells[15, 1]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            xlWorkSheet.Cells[15, 1] = "Telefono:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[15, 2], xlWorkSheet.Cells[15, 2]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            xlWorkSheet.Cells[15, 2] = cliente.telefono.ToUpper(); // aca el telefono del cliente

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[15, 4], xlWorkSheet.Cells[15, 4]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            xlWorkSheet.Cells[15, 4] = "Colonia:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[15, 5], xlWorkSheet.Cells[15, 5]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            xlWorkSheet.Cells[15, 5] = cliente.colonia.ToUpper(); // aca el telefono del cliente

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[15, 6], xlWorkSheet.Cells[15, 6]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            xlWorkSheet.Cells[15, 6] = "CP:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[15, 7], xlWorkSheet.Cells[15, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            xlWorkSheet.Cells[15, 7] = cliente.cp; // aca el CP del cliente

        //            /**/

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[16, 1], xlWorkSheet.Cells[16, 1]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            xlWorkSheet.Cells[16, 1] = "Ciudad:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[16, 2], xlWorkSheet.Cells[16, 2]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            xlWorkSheet.Cells[16, 2] = cliente.municipio.ToUpper(); // aca el telefono del cliente

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[16, 4], xlWorkSheet.Cells[16, 4]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            xlWorkSheet.Cells[16, 4] = "Estado:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[16, 5], xlWorkSheet.Cells[16, 5]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            xlWorkSheet.Cells[16, 5] = cliente.estado.ToUpper();

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[16, 6], xlWorkSheet.Cells[16, 6]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            xlWorkSheet.Cells[16, 6] = "Pais:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[16, 7], xlWorkSheet.Cells[16, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            xlWorkSheet.Cells[16, 7] = cliente.pais.ToUpper();


        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[14, 1], xlWorkSheet.Cells[16, 7]];
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gray);
        //            #endregion

        //            #region DESTINO
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[17, 1], xlWorkSheet.Cells[17, 1]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.SteelBlue);
        //            xlWorkSheet.Cells[17, 1] = "DESTINO:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[17, 2], xlWorkSheet.Cells[17, 2]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            xlWorkSheet.Cells[17, 2] = string.Format("Ciudad: {0}", empresa.municipio.ToUpper()); // ciudad origen

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[17, 5], xlWorkSheet.Cells[17, 5]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            xlWorkSheet.Cells[17, 5] = string.Format("Estado: {0}", empresa.estado.ToUpper()); // estado origen

        //            #endregion

        //            #region destinatario

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[18, 1], xlWorkSheet.Cells[18, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.SteelBlue);
        //            xlWorkSheet.Cells[18, 1] = "DESTINATARIO";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[19, 1], xlWorkSheet.Cells[19, 1]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            xlWorkSheet.Cells[19, 1] = "Domicilio:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[19, 2], xlWorkSheet.Cells[19, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            xlWorkSheet.Cells[19, 2] = empresa.calle.ToUpper();

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[20, 1], xlWorkSheet.Cells[20, 1]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            xlWorkSheet.Cells[20, 1] = "Telefono:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[20, 2], xlWorkSheet.Cells[20, 2]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            xlWorkSheet.Cells[20, 2] = empresa.telefono.ToUpper(); // aca el telefono del cliente

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[22, 4], xlWorkSheet.Cells[20, 4]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            xlWorkSheet.Cells[20, 4] = "Colonia:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[20, 5], xlWorkSheet.Cells[20, 5]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            xlWorkSheet.Cells[20, 5] = empresa.colonia.ToUpper(); // aca el telefono de la empresa

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[20, 6], xlWorkSheet.Cells[20, 6]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            xlWorkSheet.Cells[20, 6] = "CP:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[20, 7], xlWorkSheet.Cells[20, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            xlWorkSheet.Cells[20, 7] = empresa.cp; // aca el CP de la empresa

        //            ///*-------------------------------------------------------------------------------*/

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[21, 1], xlWorkSheet.Cells[21, 1]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            xlWorkSheet.Cells[21, 1] = "Ciudad:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[21, 2], xlWorkSheet.Cells[21, 2]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            xlWorkSheet.Cells[21, 2] = empresa.municipio.ToUpper(); // aca la ciudad del cliente

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[21, 4], xlWorkSheet.Cells[21, 4]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            xlWorkSheet.Cells[21, 4] = empresa.estado.ToUpper();

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[21, 5], xlWorkSheet.Cells[21, 5]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            xlWorkSheet.Cells[21, 5] = empresa.estado.ToUpper(); // aca el estado de la empresa

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[21, 6], xlWorkSheet.Cells[21, 6]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            xlWorkSheet.Cells[21, 6] = "País:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[21, 7], xlWorkSheet.Cells[21, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            xlWorkSheet.Cells[21, 7] = empresa.pais.ToUpper(); // aca el pais del cliente

        //            #endregion

        //            #region tabla detalles

        //            #region titulo tabla detalles

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[22, 1], xlWorkSheet.Cells[22, 1]];
        //            xlWorkSheet.Cells[22, 1] = "Cantidad.";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[22, 2], xlWorkSheet.Cells[22, 2]];
        //            xlWorkSheet.Cells[22, 2] = "Origen";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[22, 3], xlWorkSheet.Cells[22, 4]];
        //            excelCellrange.Merge(true);
        //            xlWorkSheet.Cells[22, 3] = "Destino.";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[22, 5], xlWorkSheet.Cells[22, 7]];
        //            excelCellrange.Merge(true);
        //            xlWorkSheet.Cells[22, 5] = "Producto.";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[22, 1], xlWorkSheet.Cells[22, 7]];
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.SteelBlue);
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
        //            #endregion

        //            #region datos tabla detalle
        //            int i = 23;
        //            foreach (var item in cartaPorte)
        //            {
        //                excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 1], xlWorkSheet.Cells[i, 1]];
        //                excelCellrange.NumberFormat = "@";
        //                xlWorkSheet.Cells[i, 1] = item.volumenDescarga.ToString() + " " + item.producto.clave_unidad_de_medida;

        //                excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 2], xlWorkSheet.Cells[i, 2]];
        //                xlWorkSheet.Cells[i, 2] = item.origen.nombreOrigen;

        //                excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 3], xlWorkSheet.Cells[i, 4]];
        //                excelCellrange.Merge(true);
        //                xlWorkSheet.Cells[i, 3] = item.zonaSelect.descripcion;

        //                excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 5], xlWorkSheet.Cells[i, 7]];
        //                excelCellrange.Merge(true);
        //                xlWorkSheet.Cells[i, 5] = item.producto.descripcion;
        //                i++;
        //            }

        //            i--;
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[23, 1], xlWorkSheet.Cells[(i), 7]];
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.Font.FontStyle = "Bold";


        //            #endregion


        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[22, 1], xlWorkSheet.Cells[(i), 7]];
        //            Excel.Borders border = excelCellrange.Borders;
        //            border.LineStyle = Excel.XlLineStyle.xlContinuous;
        //            border.Weight = 2d;
        //            #endregion

        //            //i++; //23

        //            #region valores 
        //            /*----------------------------------------------------------------------------------------*/
        //            /**/
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 1), 1], xlWorkSheet.Cells[(i + 2), 1]]; 
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 8;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            xlWorkSheet.Cells[(i + 1), 1] = "UNIDADES:";
        //            xlWorkSheet.Cells[(i + 2), 1] = "OPERADOR:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 1), 2], xlWorkSheet.Cells[(i + 1), 5]]; 
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 8;
        //            excelCellrange.Cells.Merge(true);
        //            xlWorkSheet.Cells[(i + 1), 2] = (viaje.tractor.clave + "/") + (viaje.remolque.clave)
        //                    + (viaje.dolly != null ? "/" + viaje.dolly.clave : "")
        //                    + (viaje.remolque2 != null ? "/" + viaje.remolque2.clave : "");

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 2), 2], xlWorkSheet.Cells[(i + 2), 5]];
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 8;
        //            excelCellrange.Cells.Merge(true);
        //            xlWorkSheet.Cells[(i + 2), 2] = viaje.operador.nombre;
        //            /**/

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 1), 6], xlWorkSheet.Cells[(i + 1), 6]]; //24
        //            xlWorkSheet.Cells[(i + 1), 6] = "Subtotal:";
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 1), 7], xlWorkSheet.Cells[(i + 1), 7]];
        //            excelCellrange.NumberFormat = "@";
        //            xlWorkSheet.Cells[(i + 1), 7] = "0.00";
        //            /*----------------------------------------------------------------------------------------*/
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 2), 6], xlWorkSheet.Cells[(i + 2), 6]]; //25
        //            xlWorkSheet.Cells[(i + 2), 6] = "Descuentos:";
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 2), 7], xlWorkSheet.Cells[(i + 2), 7]];
        //            excelCellrange.NumberFormat = "@";
        //            xlWorkSheet.Cells[(i + 2), 7] = "0.00";
        //            /*----------------------------------------------------------------------------------------*/
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 3), 6], xlWorkSheet.Cells[(i + 3), 6]]; //26
        //            xlWorkSheet.Cells[(i + 3), 6] = "I.E.P.S.:";
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 3), 7], xlWorkSheet.Cells[(i + 3), 7]];
        //            excelCellrange.NumberFormat = "@";
        //            xlWorkSheet.Cells[(i + 3), 7] = "0.00";
        //            /*----------------------------------------------------------------------------------------*/
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 4), 6], xlWorkSheet.Cells[(i + 4), 6]]; //27
        //            xlWorkSheet.Cells[(i + 4), 6] = "I.V.A.:";
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 4), 7], xlWorkSheet.Cells[(i + 4), 7]];
        //            excelCellrange.NumberFormat = "@";
        //            xlWorkSheet.Cells[(i + 4), 7] = "0.00";
        //            /*----------------------------------------------------------------------------------------*/
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 5), 6], xlWorkSheet.Cells[(i + 5), 6]]; //28
        //            xlWorkSheet.Cells[(i + 5), 6] = "Retención I.S.R.:";
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 5), 7], xlWorkSheet.Cells[(i + 5), 7]];
        //            excelCellrange.NumberFormat = "@";
        //            xlWorkSheet.Cells[(i + 5), 7] = "0.00";
        //            /*----------------------------------------------------------------------------------------*/
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 6), 6], xlWorkSheet.Cells[(i + 6), 6]]; //29
        //            xlWorkSheet.Cells[(i + 6), 6] = "Retención I.V.A.:";
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 6), 7], xlWorkSheet.Cells[(i + 6), 7]];
        //            excelCellrange.NumberFormat = "@";
        //            xlWorkSheet.Cells[(i + 6), 7] = "0.00".ToString();
        //            /*----------------------------------------------------------------------------------------*/
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 7), 6], xlWorkSheet.Cells[(i + 7), 6]]; //30
        //            xlWorkSheet.Cells[(i + 7), 6] = "Total";
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 7), 7], xlWorkSheet.Cells[(i + 7), 7]];
        //            excelCellrange.NumberFormat = "@";
        //            xlWorkSheet.Cells[(i + 7), 7] = "0.00";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 1), 6], xlWorkSheet.Cells[(i + 7), 6]];
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.SteelBlue);

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 1), 7], xlWorkSheet.Cells[(i + 7), 7]];
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gray);

        //            #endregion

        //            #region tabla importe letra

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 3), 1], xlWorkSheet.Cells[(i + 3), 5]]; //26
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.SteelBlue);
        //            xlWorkSheet.Cells[(i + 3), 1] = "Importe con letra";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 4), 1], xlWorkSheet.Cells[(i + 4), 5]]; //27
        //            excelCellrange.Merge(true);
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            xlWorkSheet.Cells[(i + 4), 1] = "cero Pesos 00/100 M.N.";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 3), 1], xlWorkSheet.Cells[(i + 4), 5]]; //26 / 27
        //            Excel.Borders border2 = excelCellrange.Borders;
        //            excelCellrange.Font.Size = 7;
        //            border2.LineStyle = Excel.XlLineStyle.xlContinuous;
        //            border2.Weight = 2d;

        //            #region letras
        //            #endregion
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 5), 1], xlWorkSheet.Cells[(i + 5), 5]]; //28
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
        //            xlWorkSheet.Cells[(i + 5), 1] = "Lugar de expedición";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 6), 1], xlWorkSheet.Cells[(i + 6), 5]]; //29
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
        //            xlWorkSheet.Cells[(i + 6), 1] = string.Format("{0}, {1}, {2}, {3}, {4}",
        //                empresa.calle.ToUpper(), empresa.municipio.ToUpper(),
        //                empresa.cp, empresa.estado.ToUpper(), empresa.pais.ToUpper());
        //            // "187 Diagonal x 94 y 96 424, DZUNUNCAN, 97315, MERIDA, MERIDA, YUCATAN, MEXICO";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 7), 1], xlWorkSheet.Cells[(i + 7), 5]]; //30
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 6;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
        //            xlWorkSheet.Cells[(i + 7), 1] = "Método de pago:      No identificado";

        //            #endregion                    

        //            #region firamas

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 8), 1], xlWorkSheet.Cells[(i + 10), 2]]; //31/33
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.VerticalAlignment = Excel.XlVAlign.xlVAlignTop;
        //            excelCellrange.MergeCells = true;

        //            border2 = excelCellrange.Borders;
        //            excelCellrange.Font.Size = 7;
        //            border2.LineStyle = Excel.XlLineStyle.xlContinuous;
        //            border2.Weight = 2d;
        //            xlWorkSheet.Cells[(i + 8), 1] = "Documento"; //31
        //            /*--------------------------------------------------------------------------------------------*/
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 8), 3], xlWorkSheet.Cells[(i + 10), 5]]; //31/33
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.VerticalAlignment = Excel.XlVAlign.xlVAlignTop;
        //            excelCellrange.MergeCells = true;

        //            border2 = excelCellrange.Borders;
        //            excelCellrange.Font.Size = 7;
        //            border2.LineStyle = Excel.XlLineStyle.xlContinuous;
        //            border2.Weight = 2d;
        //            xlWorkSheet.Cells[(i + 8), 3] = "Recibí de conformidad:" + //31
        //                Environment.NewLine +
        //                Environment.NewLine +
        //                Environment.NewLine + "Firma del destinatario:";

        //            /*--------------------------------------------------------------------------------------------*/
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 8), 6], xlWorkSheet.Cells[(i + 10), 7]]; //31/33
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.VerticalAlignment = Excel.XlVAlign.xlVAlignTop;
        //            excelCellrange.MergeCells = true;

        //            border2 = excelCellrange.Borders;
        //            excelCellrange.Font.Size = 7;
        //            border2.LineStyle = Excel.XlLineStyle.xlContinuous;
        //            border2.Weight = 2d;
        //            xlWorkSheet.Cells[(i + 8), 6] = "Observaciones:"; //31

        //            #endregion

        //            i += 11; //igualada a 35
        //            #region imagenQR

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 1], xlWorkSheet.Cells[(i + 5), 2]]; //35/40
        //            excelCellrange.MergeCells = true;


        //            var qrEncoder = new Gma.QrCodeNet.Encoding.QrEncoder(Gma.QrCodeNet.Encoding.ErrorCorrectionLevel.H);
        //            var qrCode = qrEncoder.Encode(string.Format("re={0}&rr={1}&0000000000.000000&id={2}"
        //                , empresa.rfc.ToUpper()
        //                , cliente.rfc.ToUpper()
        //                , datosFactura.UUID));

        //            var renderer = new Gma.QrCodeNet.Encoding.Windows.Render.GraphicsRenderer(
        //                new Gma.QrCodeNet.Encoding.Windows.Render.FixedModuleSize
        //                (5, Gma.QrCodeNet.Encoding.Windows.Render.QuietZoneModules.Two), System.Drawing.Brushes.Black, System.Drawing.Brushes.White);
        //            using (var stream = new FileStream("qrcode.png", FileMode.Create))
        //                renderer.WriteToStream(qrCode.Matrix, System.Drawing.Imaging.ImageFormat.Png, stream);

        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
        //            excelCellrange.Font.Size = 8;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            //oTable.Cell(1, 1).Range.InlineShapes.AddPicture(@"e:\SisCompras\WpfClient\Recursos\logo_ciltra_transparente1.png");
        //            //xlWorkSheet.Cells[1, 1] = "lugar para la imagen";

        //            if (File.Exists(System.Windows.Forms.Application.StartupPath + @"\\qrcode.png"))
        //            {

        //            }
        //            xlWorkSheet.Shapes.AddPicture(System.Windows.Forms.Application.StartupPath + @"\\qrcode.png",
        //                Microsoft.Office.Core.MsoTriState.msoFalse,
        //                Microsoft.Office.Core.MsoTriState.msoCTrue,
        //                float.Parse(excelCellrange.Left.ToString()), float.Parse(excelCellrange.Top.ToString()),
        //                float.Parse((excelCellrange.Width).ToString()), float.Parse((excelCellrange.Height).ToString()));
        //            #endregion

        //            #region tabla emitido

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 3], xlWorkSheet.Cells[i, 7]]; //35
        //            excelCellrange.MergeCells = true;
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Silver);
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.SteelBlue);
        //            xlWorkSheet.Cells[i, 3] = "Este documento es una representación impresa de un CFDI";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 1), 3], xlWorkSheet.Cells[(i + 1), 7]]; //36
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.VerticalAlignment = Excel.XlVAlign.xlVAlignTop;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gray);
        //            xlWorkSheet.Cells[(i + 1), 3] = "Efectos fiscales al pago.  PAGO EN UNA SOLA EXHIBICION";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 3], xlWorkSheet.Cells[(i + 1), 7]]; //35/36
        //            Excel.Borders bordersSombreado = excelCellrange.Borders;
        //            bordersSombreado[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
        //            bordersSombreado[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;
        //            bordersSombreado[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
        //            bordersSombreado[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;

        //            #endregion

        //            #region datosFactura

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 2), 3], xlWorkSheet.Cells[(i + 2), 4]]; //37
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.SteelBlue);
        //            xlWorkSheet.Cells[(i + 2), 3] = "Serie del Certificado del emisor:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 3), 3], xlWorkSheet.Cells[(i + 3), 4]]; //38
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.SteelBlue);
        //            xlWorkSheet.Cells[(i + 3), 3] = "Folio Fiscal:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 4), 3], xlWorkSheet.Cells[(i + 4), 4]]; //39
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.SteelBlue);
        //            xlWorkSheet.Cells[(i + 4), 3] = "No. de serie del Certificado del SAT:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 5), 3], xlWorkSheet.Cells[(i + 5), 4]]; //40
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.SteelBlue);
        //            xlWorkSheet.Cells[(i + 5), 3] = "Fecha y hora de certificación:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 2), 5], xlWorkSheet.Cells[(i + 2), 7]]; //37
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
        //            excelCellrange.NumberFormat = "@";
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gray);
        //            //excelCellrange.te
        //            xlWorkSheet.Cells[(i + 2), 5] = datosFactura.noCertificado;

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 3), 5], xlWorkSheet.Cells[(i + 3), 7]]; //38
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
        //            excelCellrange.NumberFormat = "@";
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gray);
        //            xlWorkSheet.Cells[(i + 3), 5] = datosFactura.UUID;

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 4), 5], xlWorkSheet.Cells[(i + 4), 7]]; //39
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gray);
        //            excelCellrange.NumberFormat = "@";
        //            xlWorkSheet.Cells[(i + 4), 5] = datosFactura.noCertificadoSAT;

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 5), 5], xlWorkSheet.Cells[(i + 5), 7]]; // 40
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gray);
        //            xlWorkSheet.Cells[(i + 5), 5] = DateTime.Now.ToString("dd-MM-yy HH:mm:ss tt");

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 2), 3], xlWorkSheet.Cells[(i + 5), 7]]; //37/40
        //            border = excelCellrange.Borders;
        //            border.LineStyle = Excel.XlLineStyle.xlContinuous;
        //            border.Weight = 2d;

        //            #endregion

        //            i += 5; //igua a 40
        //            #region tabla final
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 1), 1], xlWorkSheet.Cells[(i + 1), 7]]; //41
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.SteelBlue);
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
        //            xlWorkSheet.Cells[(i + 1), 1] = "Sello digital del CFDI:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 2), 1], xlWorkSheet.Cells[(i + 2), 7]]; //42
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 5;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
        //            xlWorkSheet.Cells[(i + 2), 1] = datosFactura.sello;

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 3), 1], xlWorkSheet.Cells[(i + 3), 7]]; //43
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.SteelBlue);
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
        //            xlWorkSheet.Cells[(i + 3), 1] = "Sello del SAT:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 4), 1], xlWorkSheet.Cells[(i + 4), 7]]; //44
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 5;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
        //            xlWorkSheet.Cells[(i + 4), 1] = datosFactura.selloSAT;

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 5), 1], xlWorkSheet.Cells[(i + 5), 7]]; //45
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 6;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.SteelBlue);
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
        //            xlWorkSheet.Cells[(i + 5), 1] = "Cadena original del complemento del certificación digital del SAT:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 6), 1], xlWorkSheet.Cells[(i + 6), 7]]; //46
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 5;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
        //            excelCellrange.EntireRow.AutoFit();
        //            xlWorkSheet.Cells[(i + 6), 1] = string.Format("||1.0|{0}|{1}|{2}|{3}|| "
        //                , datosFactura.UUID
        //                , DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")
        //                , datosFactura.selloCFD,
        //                datosFactura.selloSAT);

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[(i + 1), 1], xlWorkSheet.Cells[(i + 6), 7]]; //41/45
        //            border = excelCellrange.Borders;
        //            border.LineStyle = Excel.XlLineStyle.xlContinuous;
        //            border.Weight = 2d;
        //            #endregion

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[100, 100]];
        //            excelCellrange.EntireColumn.AutoFit();
        //            var x = excelCellrange.Height;
        //            excelCellrange.EntireRow.AutoFit();
        //            xlWorkBook.SaveAs(archivo + ".xlsx");
        //            ap.PrintDialog printDialog1 = new ap.PrintDialog();
        //            printDialog1.PrinterSettings.Copies = 1;
        //            ap.DialogResult result = printDialog1.ShowDialog();
        //            var ss = printDialog1.PrinterSettings.PrinterName;
        //            xlWorkSheet.PrintOutEx(1, 3, printDialog1.PrinterSettings.Copies, false, ss); //linea para imprimir
        //            rutaExcel = archivo + ".xlsx";

        //            return true;
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        MessageBox.Show(e.Message);
        //        return false;
        //    }
        //    finally
        //    {
        //        xlWorkBook.Close();
        //        xlApp.Quit();
        //        GC.WaitForPendingFinalizers();
        //        GC.Collect();
        //        GC.WaitForPendingFinalizers();
        //        GC.Collect();
        //    }
        //}

        //public bool cartaPorteFalsoAtlante(ComercialCliente cliente, ComercialEmpresa empresa, DatosFacturaXML datosFactura, CartaPorte cartaPorte)
        //{
        //    string archivo = "";
        //    try
        //    {
        //        string path = System.Windows.Forms.Application.StartupPath;
        //        if (!Directory.Exists(path + "\\reportes\\"))
        //        {
        //            DirectoryInfo dir = Directory.CreateDirectory(path + "\\reportes\\");
        //        }

        //        //archivo = string.Format(@"{0}{1}", path + "\\reportes\\", cartaPorte.NumGuiaId);
        //        archivo = string.Format(@"{0}{1}", path + "\\reportes\\", "reporte");

        //        if (File.Exists(archivo + ".xlsx"))
        //        {
        //            File.Delete(archivo + ".xlsx");
        //        }
        //        if (File.Exists(archivo + ".xps"))
        //        {
        //            File.Delete(archivo + ".xps");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message);
        //        return false;
        //    }

        //    Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
        //    Excel.Workbook xlWorkBook = xlApp.Workbooks.Add();
        //    //Microsoft.Office.Interop.Excel.Worksheet excelSheet;
        //    Microsoft.Office.Interop.Excel.Range excelCellrange;

        //    try
        //    {

        //        if (xlApp == null)
        //        {
        //            MessageBox.Show("Excel is not properly installed!!");
        //            return false;
        //        }
        //        else
        //        {

        //            Excel.Worksheet xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
        //            //xlWorkSheet.PageSetup.Orientation = Microsoft.Office.Interop.Excel.XlPageOrientation.xlLandscape;

        //            xlWorkSheet.Name = "CartaPorte";

        //            //excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[6, 4]];
        //            //excelCellrange.Merge(true);

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[6, 4]];
        //            excelCellrange.MergeCells = true;

        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.Font.Size = 8;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            //oTable.Cell(1, 1).Range.InlineShapes.AddPicture(@"e:\SisCompras\WpfClient\Recursos\logo_ciltra_transparente1.png");
        //            //xlWorkSheet.Cells[1, 1] = "lugar para la imagen";

        //            xlWorkSheet.Shapes.AddPicture(System.Windows.Forms.Application.StartupPath + @"\\atlanteLogo.jpg",
        //                Microsoft.Office.Core.MsoTriState.msoFalse,
        //                Microsoft.Office.Core.MsoTriState.msoCTrue,
        //                float.Parse(excelCellrange.Left.ToString()), float.Parse(excelCellrange.Top.ToString()),
        //                150, 50
        //                );

        //            #region primera tabla
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[1, 5], xlWorkSheet.Cells[1, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 28;
        //            excelCellrange.Font.Name = "Free 3 of 9 Extended";
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            //excelCellrange.Font.FontStyle = "Bold";
        //            xlWorkSheet.Cells[1, 5] = "*" + cartaPorte.numGuiaId.ToString() + "-" + cartaPorte.grupoCartaPorte.Trim('-') + "*";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[2, 5], xlWorkSheet.Cells[2, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 8;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Silver);
        //            excelCellrange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.SteelBlue);
        //            xlWorkSheet.Cells[2, 5] = "CARTA PORTE";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[3, 6], xlWorkSheet.Cells[3, 6]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            //excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
        //            xlWorkSheet.Cells[3, 6] = "Viaje:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[4, 6], xlWorkSheet.Cells[4, 6]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            //excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
        //            xlWorkSheet.Cells[4, 6] = "Carta Portes:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[5, 6], xlWorkSheet.Cells[5, 6]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            //excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
        //            xlWorkSheet.Cells[5, 6] = "Fecha:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[6, 6], xlWorkSheet.Cells[6, 6]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            //excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
        //            xlWorkSheet.Cells[6, 6] = "Hora:";
        //            /*----------------------------------------------------------------------------------------------------*/

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[3, 7], xlWorkSheet.Cells[3, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 8;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
        //            xlWorkSheet.Cells[3, 7] = cartaPorte.numGuiaId; //ACA Iria el folio.

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[4, 7], xlWorkSheet.Cells[4, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
        //            xlWorkSheet.Cells[4, 7] = cartaPorte.grupoCartaPorte.Trim('-'); //ACA Iria el folio.

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[5, 7], xlWorkSheet.Cells[5, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            //excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
        //            xlWorkSheet.Cells[5, 7] = DateTime.Now.ToString("dd/MMM/yyyy"); // aca la fecha

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[6, 7], xlWorkSheet.Cells[6, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            //excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
        //            xlWorkSheet.Cells[6, 7] = DateTime.Now.ToString("HH:mm:ss"); // aca la fecha hora
        //            #endregion

        //            #region datos empresa

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[7, 1], xlWorkSheet.Cells[7, 4]];
        //            excelCellrange.Merge(true);
        //            xlWorkSheet.Cells[7, 1] = empresa.nombre.ToUpper(); // aca la empresa.

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[8, 1], xlWorkSheet.Cells[8, 4]];
        //            excelCellrange.Merge(true);
        //            xlWorkSheet.Cells[8, 1] = empresa.rfc.ToUpper(); // aca la RFC.

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[9, 1], xlWorkSheet.Cells[9, 4]];
        //            excelCellrange.Merge(true);
        //            xlWorkSheet.Cells[9, 1] = "No Aplica";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[7, 5], xlWorkSheet.Cells[7, 7]];
        //            excelCellrange.Merge(true);
        //            xlWorkSheet.Cells[7, 5] = empresa.calle.ToUpper(); // aca la direccion empresa.

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[8, 5], xlWorkSheet.Cells[8, 5]];
        //            excelCellrange.Merge(true);
        //            xlWorkSheet.Cells[8, 5] = empresa.municipio.ToUpper(); // ciudad empresa
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[8, 5], xlWorkSheet.Cells[8, 5]];
        //            excelCellrange.Merge(true);
        //            xlWorkSheet.Cells[8, 7] = empresa.cp; // CP empresa

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[8, 5], xlWorkSheet.Cells[8, 5]];
        //            excelCellrange.Merge(true);
        //            xlWorkSheet.Cells[9, 5] = empresa.municipio.ToUpper(); // ciudad empresa
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[8, 5], xlWorkSheet.Cells[8, 5]];
        //            excelCellrange.Merge(true);
        //            xlWorkSheet.Cells[9, 7] = empresa.estado.ToUpper(); // estado empresa

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[9, 5], xlWorkSheet.Cells[9, 5]];
        //            excelCellrange.Merge(true);
        //            xlWorkSheet.Cells[10, 5] = empresa.pais.ToUpper(); // pais empresa

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[7, 1], xlWorkSheet.Cells[10, 7]];
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
        //            excelCellrange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.SteelBlue);
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);

        //            //Excel.Borders border = excelCellrange.Borders;
        //            //border.LineStyle = Excel.XlLineStyle.xlContinuous;
        //            //border.Weight = 2d;

        //            #endregion

        //            #region origen

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[11, 1], xlWorkSheet.Cells[11, 1]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.SteelBlue);
        //            xlWorkSheet.Cells[11, 1] = "ORIGEN:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[11, 2], xlWorkSheet.Cells[11, 2]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Silver);
        //            xlWorkSheet.Cells[11, 2] = string.Format("Ciudad: {0}", cliente.municipio.ToUpper()); // ciudad origen

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[11, 5], xlWorkSheet.Cells[11, 5]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Silver);
        //            xlWorkSheet.Cells[11, 5] = string.Format("Estado: {0}", cliente.estado.ToUpper()); // estado origen

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[11, 1], xlWorkSheet.Cells[11, 5]];
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
        //            #endregion

        //            #region remitente

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[12, 1], xlWorkSheet.Cells[12, 1]];
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.SteelBlue);
        //            xlWorkSheet.Cells[12, 1] = "REMITENTE:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[12, 2], xlWorkSheet.Cells[12, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gray);
        //            xlWorkSheet.Cells[12, 2] = cliente.nombre.ToUpper(); // NOMBRE CLIENTE

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[13, 1], xlWorkSheet.Cells[13, 1]];
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.SteelBlue);
        //            xlWorkSheet.Cells[13, 1] = "RFC:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[13, 2], xlWorkSheet.Cells[13, 2]];
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gray);
        //            xlWorkSheet.Cells[13, 2] = cliente.rfc.ToUpper(); // RFC CLIENTE

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[14, 1], xlWorkSheet.Cells[14, 1]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            xlWorkSheet.Cells[14, 1] = "Domicilio:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[14, 2], xlWorkSheet.Cells[14, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            xlWorkSheet.Cells[14, 2] = cliente.calle.ToUpper(); // aca la direccion del cliente

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[15, 1], xlWorkSheet.Cells[15, 1]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            xlWorkSheet.Cells[15, 1] = "Telefono:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[15, 2], xlWorkSheet.Cells[15, 2]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            xlWorkSheet.Cells[15, 2] = cliente.telefono.ToUpper(); // aca el telefono del cliente

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[15, 4], xlWorkSheet.Cells[15, 4]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            xlWorkSheet.Cells[15, 4] = "Colonia:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[15, 5], xlWorkSheet.Cells[15, 5]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            xlWorkSheet.Cells[15, 5] = cliente.colonia.ToUpper(); // aca el telefono del cliente

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[15, 6], xlWorkSheet.Cells[15, 6]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            xlWorkSheet.Cells[15, 6] = "CP:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[15, 7], xlWorkSheet.Cells[15, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            xlWorkSheet.Cells[15, 7] = cliente.cp; // aca el CP del cliente

        //            /**/

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[16, 1], xlWorkSheet.Cells[16, 1]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            xlWorkSheet.Cells[16, 1] = "Ciudad:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[16, 2], xlWorkSheet.Cells[16, 2]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            xlWorkSheet.Cells[16, 2] = cliente.municipio.ToUpper(); // aca el telefono del cliente

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[16, 4], xlWorkSheet.Cells[16, 4]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            xlWorkSheet.Cells[16, 4] = "Estado:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[16, 5], xlWorkSheet.Cells[16, 5]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            xlWorkSheet.Cells[16, 5] = cliente.estado.ToUpper();

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[16, 6], xlWorkSheet.Cells[16, 6]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            xlWorkSheet.Cells[16, 6] = "Pais:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[16, 7], xlWorkSheet.Cells[16, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            xlWorkSheet.Cells[16, 7] = cliente.pais.ToUpper();


        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[14, 1], xlWorkSheet.Cells[16, 7]];
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gray);
        //            #endregion

        //            #region DESTINO
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[17, 1], xlWorkSheet.Cells[17, 1]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.SteelBlue);
        //            xlWorkSheet.Cells[17, 1] = "DESTINO:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[17, 2], xlWorkSheet.Cells[17, 2]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            xlWorkSheet.Cells[17, 2] = string.Format("Ciudad: {0}", empresa.municipio.ToUpper()); // ciudad origen

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[17, 5], xlWorkSheet.Cells[17, 5]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            xlWorkSheet.Cells[17, 5] = string.Format("Estado: {0}", empresa.estado.ToUpper()); // estado origen

        //            #endregion

        //            #region destinatario

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[18, 1], xlWorkSheet.Cells[18, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.SteelBlue);
        //            xlWorkSheet.Cells[18, 1] = "DESTINATARIO";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[19, 1], xlWorkSheet.Cells[19, 1]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            xlWorkSheet.Cells[19, 1] = "Domicilio:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[19, 2], xlWorkSheet.Cells[19, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            xlWorkSheet.Cells[19, 2] = empresa.calle.ToUpper();

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[20, 1], xlWorkSheet.Cells[20, 1]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            xlWorkSheet.Cells[20, 1] = "Telefono:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[20, 2], xlWorkSheet.Cells[20, 2]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            xlWorkSheet.Cells[20, 2] = empresa.telefono.ToUpper(); // aca el telefono del cliente

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[22, 4], xlWorkSheet.Cells[20, 4]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            xlWorkSheet.Cells[20, 4] = "Colonia:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[20, 5], xlWorkSheet.Cells[20, 5]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            xlWorkSheet.Cells[20, 5] = empresa.colonia.ToUpper(); // aca el telefono de la empresa

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[20, 6], xlWorkSheet.Cells[20, 6]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            xlWorkSheet.Cells[20, 6] = "CP:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[20, 7], xlWorkSheet.Cells[20, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            xlWorkSheet.Cells[20, 7] = empresa.cp; // aca el CP de la empresa

        //            ///*-------------------------------------------------------------------------------*/

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[21, 1], xlWorkSheet.Cells[21, 1]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            xlWorkSheet.Cells[21, 1] = "Ciudad:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[21, 2], xlWorkSheet.Cells[21, 2]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            xlWorkSheet.Cells[21, 2] = empresa.municipio.ToUpper(); // aca la ciudad del cliente

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[21, 4], xlWorkSheet.Cells[21, 4]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            xlWorkSheet.Cells[21, 4] = empresa.estado.ToUpper();

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[21, 5], xlWorkSheet.Cells[21, 5]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            xlWorkSheet.Cells[21, 5] = empresa.estado.ToUpper(); // aca el estado de la empresa

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[21, 6], xlWorkSheet.Cells[21, 6]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            xlWorkSheet.Cells[21, 6] = "País:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[21, 7], xlWorkSheet.Cells[21, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            xlWorkSheet.Cells[21, 7] = empresa.pais.ToUpper(); // aca el pais del cliente

        //            #endregion

        //            #region tabla detalles

        //            #region titulo tabla detalles

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[22, 1], xlWorkSheet.Cells[22, 1]];
        //            xlWorkSheet.Cells[22, 1] = "Cantidad.";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[22, 2], xlWorkSheet.Cells[22, 2]];
        //            xlWorkSheet.Cells[22, 2] = "Unidad.";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[22, 3], xlWorkSheet.Cells[22, 4]];
        //            excelCellrange.Merge(true);
        //            xlWorkSheet.Cells[22, 3] = "Concepto / Descripción.";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[22, 5], xlWorkSheet.Cells[22, 7]];
        //            excelCellrange.Merge(true);
        //            xlWorkSheet.Cells[22, 5] = "Producto.";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[22, 1], xlWorkSheet.Cells[22, 7]];
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.SteelBlue);
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
        //            #endregion

        //            #region datos tabla detalle

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[23, 1], xlWorkSheet.Cells[23, 1]];
        //            excelCellrange.NumberFormat = "@";
        //            xlWorkSheet.Cells[23, 1] = cartaPorte.volumenDescarga.ToString();

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[23, 2], xlWorkSheet.Cells[23, 2]];
        //            xlWorkSheet.Cells[23, 2] = "Servicio";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[23, 3], xlWorkSheet.Cells[23, 4]];
        //            excelCellrange.Merge(true);
        //            xlWorkSheet.Cells[23, 3] = cartaPorte.zonaSelect.descripcion;

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[23, 5], xlWorkSheet.Cells[23, 7]];
        //            excelCellrange.Merge(true);
        //            xlWorkSheet.Cells[23, 5] = cartaPorte.producto.descripcion;

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[23, 1], xlWorkSheet.Cells[23, 7]];
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.Font.FontStyle = "Bold";


        //            #endregion


        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[22, 1], xlWorkSheet.Cells[23, 7]];
        //            Excel.Borders border = excelCellrange.Borders;
        //            border.LineStyle = Excel.XlLineStyle.xlContinuous;
        //            border.Weight = 2d;
        //            #endregion


        //            #region valores 
        //            /*----------------------------------------------------------------------------------------*/
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[24, 6], xlWorkSheet.Cells[24, 6]];
        //            xlWorkSheet.Cells[24, 6] = "Subtotal:";
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[24, 7], xlWorkSheet.Cells[24, 7]];
        //            excelCellrange.NumberFormat = "@";
        //            xlWorkSheet.Cells[24, 7] = "0.00";
        //            /*----------------------------------------------------------------------------------------*/
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[25, 6], xlWorkSheet.Cells[25, 6]];
        //            xlWorkSheet.Cells[25, 6] = "Descuentos:";
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[25, 7], xlWorkSheet.Cells[25, 7]];
        //            excelCellrange.NumberFormat = "@";
        //            xlWorkSheet.Cells[25, 7] = "0.00";
        //            /*----------------------------------------------------------------------------------------*/
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[26, 6], xlWorkSheet.Cells[26, 6]];
        //            xlWorkSheet.Cells[26, 6] = "I.E.P.S.:";
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[26, 7], xlWorkSheet.Cells[26, 7]];
        //            excelCellrange.NumberFormat = "@";
        //            xlWorkSheet.Cells[26, 7] = "0.00";
        //            /*----------------------------------------------------------------------------------------*/
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[27, 6], xlWorkSheet.Cells[27, 6]];
        //            xlWorkSheet.Cells[27, 6] = "I.V.A.:";
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[27, 7], xlWorkSheet.Cells[27, 7]];
        //            excelCellrange.NumberFormat = "@";
        //            xlWorkSheet.Cells[27, 7] = "0.00";
        //            /*----------------------------------------------------------------------------------------*/
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[28, 6], xlWorkSheet.Cells[28, 6]];
        //            xlWorkSheet.Cells[28, 6] = "Retención I.S.R.:";
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[28, 7], xlWorkSheet.Cells[28, 7]];
        //            excelCellrange.NumberFormat = "@";
        //            xlWorkSheet.Cells[28, 7] = "0.00";
        //            /*----------------------------------------------------------------------------------------*/
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[29, 6], xlWorkSheet.Cells[29, 6]];
        //            xlWorkSheet.Cells[29, 6] = "Retención I.V.A.:";
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[29, 7], xlWorkSheet.Cells[29, 7]];
        //            excelCellrange.NumberFormat = "@";
        //            xlWorkSheet.Cells[29, 7] = "0.00".ToString();
        //            /*----------------------------------------------------------------------------------------*/
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[30, 6], xlWorkSheet.Cells[30, 6]];
        //            xlWorkSheet.Cells[30, 6] = "Total";
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[30, 7], xlWorkSheet.Cells[30, 7]];
        //            excelCellrange.NumberFormat = "@";
        //            xlWorkSheet.Cells[30, 7] = "0.00";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[24, 6], xlWorkSheet.Cells[30, 6]];
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.SteelBlue);

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[24, 7], xlWorkSheet.Cells[30, 7]];
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gray);

        //            #endregion

        //            #region tabla importe letra

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[26, 1], xlWorkSheet.Cells[26, 5]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.SteelBlue);
        //            xlWorkSheet.Cells[26, 1] = "Importe con letra";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[27, 1], xlWorkSheet.Cells[27, 5]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            xlWorkSheet.Cells[27, 1] = "cero Pesos 00/100 M.N.";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[26, 1], xlWorkSheet.Cells[27, 5]];
        //            Excel.Borders border2 = excelCellrange.Borders;
        //            excelCellrange.Font.Size = 7;
        //            border2.LineStyle = Excel.XlLineStyle.xlContinuous;
        //            border2.Weight = 2d;

        //            #region letras
        //            #endregion
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[28, 1], xlWorkSheet.Cells[28, 5]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
        //            xlWorkSheet.Cells[28, 1] = "Lugar de expedición";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[29, 1], xlWorkSheet.Cells[29, 5]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
        //            xlWorkSheet.Cells[29, 1] = string.Format("{0}, {1}, {2}, {3}, {4}",
        //                empresa.calle.ToUpper(), empresa.municipio.ToUpper(),
        //                empresa.cp, empresa.estado.ToUpper(), empresa.pais.ToUpper());
        //            // "187 Diagonal x 94 y 96 424, DZUNUNCAN, 97315, MERIDA, MERIDA, YUCATAN, MEXICO";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[30, 1], xlWorkSheet.Cells[30, 5]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 6;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
        //            xlWorkSheet.Cells[30, 1] = "Método de pago:      No identificado";

        //            #endregion                    

        //            #region firamas

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[31, 1], xlWorkSheet.Cells[33, 2]];
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.VerticalAlignment = Excel.XlVAlign.xlVAlignTop;
        //            excelCellrange.MergeCells = true;

        //            border2 = excelCellrange.Borders;
        //            excelCellrange.Font.Size = 7;
        //            border2.LineStyle = Excel.XlLineStyle.xlContinuous;
        //            border2.Weight = 2d;
        //            xlWorkSheet.Cells[31, 1] = "Documento";
        //            /*--------------------------------------------------------------------------------------------*/
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[31, 3], xlWorkSheet.Cells[33, 5]];
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.VerticalAlignment = Excel.XlVAlign.xlVAlignTop;
        //            excelCellrange.MergeCells = true;

        //            border2 = excelCellrange.Borders;
        //            excelCellrange.Font.Size = 7;
        //            border2.LineStyle = Excel.XlLineStyle.xlContinuous;
        //            border2.Weight = 2d;
        //            xlWorkSheet.Cells[31, 3] = "Recibí de conformidad:" +
        //                Environment.NewLine +
        //                Environment.NewLine +
        //                Environment.NewLine + "Firma del destinatario:";

        //            /*--------------------------------------------------------------------------------------------*/
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[31, 6], xlWorkSheet.Cells[33, 7]];
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.VerticalAlignment = Excel.XlVAlign.xlVAlignTop;
        //            excelCellrange.MergeCells = true;

        //            border2 = excelCellrange.Borders;
        //            excelCellrange.Font.Size = 7;
        //            border2.LineStyle = Excel.XlLineStyle.xlContinuous;
        //            border2.Weight = 2d;
        //            xlWorkSheet.Cells[31, 6] = "Observaciones:";

        //            #endregion

        //            #region imagenQR

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[35, 1], xlWorkSheet.Cells[40, 2]];
        //            excelCellrange.MergeCells = true;


        //            var qrEncoder = new Gma.QrCodeNet.Encoding.QrEncoder(Gma.QrCodeNet.Encoding.ErrorCorrectionLevel.H);
        //            var qrCode = qrEncoder.Encode(string.Format("re={0}&rr={1}&0000000000.000000&id={2}"
        //                , empresa.rfc.ToUpper()
        //                , cliente.rfc.ToUpper()
        //                , datosFactura.UUID));

        //            var renderer = new Gma.QrCodeNet.Encoding.Windows.Render.GraphicsRenderer(
        //                new Gma.QrCodeNet.Encoding.Windows.Render.FixedModuleSize
        //                (5, Gma.QrCodeNet.Encoding.Windows.Render.QuietZoneModules.Two), System.Drawing.Brushes.Black, System.Drawing.Brushes.White);
        //            using (var stream = new FileStream("qrcode.png", FileMode.Create))
        //                renderer.WriteToStream(qrCode.Matrix, System.Drawing.Imaging.ImageFormat.Png, stream);

        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
        //            excelCellrange.Font.Size = 8;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            //oTable.Cell(1, 1).Range.InlineShapes.AddPicture(@"e:\SisCompras\WpfClient\Recursos\logo_ciltra_transparente1.png");
        //            //xlWorkSheet.Cells[1, 1] = "lugar para la imagen";

        //            if (File.Exists(System.Windows.Forms.Application.StartupPath + @"\\qrcode.png"))
        //            {

        //            }
        //            xlWorkSheet.Shapes.AddPicture(System.Windows.Forms.Application.StartupPath + @"\\qrcode.png",
        //                Microsoft.Office.Core.MsoTriState.msoFalse,
        //                Microsoft.Office.Core.MsoTriState.msoCTrue,
        //                float.Parse(excelCellrange.Left.ToString()), float.Parse(excelCellrange.Top.ToString()),
        //                float.Parse((excelCellrange.Width).ToString()), float.Parse((excelCellrange.Height).ToString()));
        //            #endregion

        //            #region tabla emitido

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[35, 3], xlWorkSheet.Cells[35, 7]];
        //            excelCellrange.MergeCells = true;
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Silver);
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.SteelBlue);
        //            xlWorkSheet.Cells[35, 3] = "Este documento es una representación impresa de un CFDI";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[36, 3], xlWorkSheet.Cells[36, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.VerticalAlignment = Excel.XlVAlign.xlVAlignTop;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gray);
        //            xlWorkSheet.Cells[36, 3] = "Efectos fiscales al pago.  PAGO EN UNA SOLA EXHIBICION";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[35, 3], xlWorkSheet.Cells[36, 7]];
        //            Excel.Borders bordersSombreado = excelCellrange.Borders;
        //            bordersSombreado[Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Excel.XlLineStyle.xlContinuous;
        //            bordersSombreado[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;
        //            bordersSombreado[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
        //            bordersSombreado[Excel.XlBordersIndex.xlEdgeRight].LineStyle = Excel.XlLineStyle.xlContinuous;

        //            #endregion

        //            #region datosFactura

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[37, 3], xlWorkSheet.Cells[37, 4]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.SteelBlue);
        //            xlWorkSheet.Cells[37, 3] = "Serie del Certificado del emisor:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[38, 3], xlWorkSheet.Cells[38, 4]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.SteelBlue);
        //            xlWorkSheet.Cells[38, 3] = "Folio Fiscal:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[39, 3], xlWorkSheet.Cells[39, 4]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.SteelBlue);
        //            xlWorkSheet.Cells[39, 3] = "No. de serie del Certificado del SAT:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[40, 3], xlWorkSheet.Cells[40, 4]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.SteelBlue);
        //            xlWorkSheet.Cells[40, 3] = "Fecha y hora de certificación:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[37, 5], xlWorkSheet.Cells[37, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
        //            excelCellrange.NumberFormat = "@";
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gray);
        //            //excelCellrange.te
        //            xlWorkSheet.Cells[37, 5] = datosFactura.noCertificado;

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[38, 5], xlWorkSheet.Cells[38, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
        //            excelCellrange.NumberFormat = "@";
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gray);
        //            xlWorkSheet.Cells[38, 5] = datosFactura.UUID;

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[39, 5], xlWorkSheet.Cells[39, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gray);
        //            excelCellrange.NumberFormat = "@";
        //            xlWorkSheet.Cells[39, 5] = datosFactura.noCertificadoSAT;

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[40, 5], xlWorkSheet.Cells[40, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gray);
        //            xlWorkSheet.Cells[40, 5] = DateTime.Now.ToString("dd-MM-yy HH:mm:ss tt");

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[37, 3], xlWorkSheet.Cells[40, 7]];
        //            border = excelCellrange.Borders;
        //            border.LineStyle = Excel.XlLineStyle.xlContinuous;
        //            border.Weight = 2d;

        //            #endregion

        //            #region tabla final
        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[41, 1], xlWorkSheet.Cells[41, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.SteelBlue);
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
        //            xlWorkSheet.Cells[41, 1] = "Sello digital del CFDI:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[42, 1], xlWorkSheet.Cells[42, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 5;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
        //            xlWorkSheet.Cells[42, 1] = datosFactura.sello;

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[43, 1], xlWorkSheet.Cells[43, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.SteelBlue);
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
        //            xlWorkSheet.Cells[43, 1] = "Sello del SAT:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[44, 1], xlWorkSheet.Cells[44, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 5;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
        //            xlWorkSheet.Cells[44, 1] = datosFactura.selloSAT;

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[45, 1], xlWorkSheet.Cells[45, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 6;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.SteelBlue);
        //            excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
        //            xlWorkSheet.Cells[45, 1] = "Cadena original del complemento del certificación digital del SAT:";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[46, 1], xlWorkSheet.Cells[46, 7]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 5;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
        //            excelCellrange.EntireRow.AutoFit();
        //            xlWorkSheet.Cells[46, 1] = string.Format("||1.0|{0}|{1}|{2}|{3}|| "
        //                , datosFactura.UUID
        //                , DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")
        //                , datosFactura.selloCFD,
        //                datosFactura.selloSAT);

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[41, 1], xlWorkSheet.Cells[46, 7]];
        //            border = excelCellrange.Borders;
        //            border.LineStyle = Excel.XlLineStyle.xlContinuous;
        //            border.Weight = 2d;
        //            #endregion

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[40, 7]];
        //            excelCellrange.EntireColumn.AutoFit();
        //            var x = excelCellrange.Height;
        //            excelCellrange.EntireRow.AutoFit();

        //            xlWorkBook.SaveAs(archivo + ".xlsx");
        //            ap.PrintDialog printDialog1 = new ap.PrintDialog();
        //            printDialog1.PrinterSettings.Copies = 1;
        //            ap.DialogResult result = printDialog1.ShowDialog();
        //            var ss = printDialog1.PrinterSettings.PrinterName;
        //            xlWorkSheet.PrintOutEx(1, 1, printDialog1.PrinterSettings.Copies, false, ss); //linea para imprimir

        //            return true;
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        MessageBox.Show(e.Message);
        //        return false;
        //    }
        //    finally
        //    {
        //        xlWorkBook.Close();
        //        xlApp.Quit();
        //        GC.WaitForPendingFinalizers();
        //        GC.Collect();
        //        GC.WaitForPendingFinalizers();
        //        GC.Collect();
        //    }
        //}

        //public bool documentoReporteViajesPersonalizadosDetallado(List<Viaje> listViaje,
        //    bool folio, bool noViaje, bool chofer, bool tc, bool tolva1, bool tolva2, bool fechaViaje,
        //    bool fechaFin, bool km, bool toneladas, bool iva, bool retencion, bool importe, bool _total,
        //    bool tipoViaje, bool destino, DateTime fechaInicio, DateTime _fechaFin, Cliente cliente, bool zona, bool isCliente)
        //{
        //    string archivo = "";
        //    try
        //    {
        //        string path = System.Windows.Forms.Application.StartupPath;
        //        if (!Directory.Exists(path + "\\reportes\\"))
        //        {
        //            DirectoryInfo dir = Directory.CreateDirectory(path + "\\reportes\\");
        //        }

        //        //archivo = string.Format(@"{0}{1}", path + "\\reportes\\", cartaPorte.NumGuiaId);
        //        archivo = string.Format(@"{0}{1}", path + "\\reportes\\", "reporte");

        //        if (File.Exists(archivo + ".xlsx"))
        //        {
        //            File.Delete(archivo + ".xlsx");
        //        }
        //        if (File.Exists(archivo + ".xps"))
        //        {
        //            File.Delete(archivo + ".xps");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message);
        //    }

        //    Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
        //    Excel.Workbook xlWorkBook = xlApp.Workbooks.Add();
        //    //Microsoft.Office.Interop.Excel.Worksheet excelSheet;
        //    Microsoft.Office.Interop.Excel.Range excelCellrange;
        //    try
        //    {

        //        if (xlApp == null)
        //        {
        //            MessageBox.Show("Excel is not properly installed!!");
        //            return false;
        //        }
        //        else
        //        {

        //            Excel.Worksheet xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
        //            xlWorkSheet.PageSetup.Orientation = Microsoft.Office.Interop.Excel.XlPageOrientation.xlLandscape;
        //            xlWorkSheet.PageSetup.LeftMargin = 2;
        //            xlWorkSheet.PageSetup.RightMargin = 2;
        //            xlWorkSheet.PageSetup.TopMargin = 15;
        //            xlWorkSheet.PageSetup.BottomMargin = 5;
        //            int indice = 0;
        //            int indiceToneladas = 0;
        //            int indiceIva = 0;
        //            int indiceImporte = 0;
        //            int indiceRetencion = 0;
        //            int indiceTotal = 0;
        //            int indiceKM = 0;
        //            int indiceDestino = 0;

        //            xlWorkSheet.Name = "Viajes";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[1, 10]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 8;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            xlWorkSheet.Cells[1, 1] = cliente.nombre;

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[2, 1], xlWorkSheet.Cells[2, 10]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            xlWorkSheet.Cells[2, 1] = "Listado de Cobranza Fechas del " + fechaInicio.ToString("dd/MM/yyyy") + " al " + _fechaFin.ToString("dd/MM/yyyy");

        //            if (folio)
        //            {
        //                indice++;
        //                xlWorkSheet.Cells[3, indice] = "Folio";
        //            }
        //            if (noViaje)
        //            {
        //                indice++;
        //                xlWorkSheet.Cells[3, indice] = "No. Viaje";
        //            }
        //            if (chofer)
        //            {
        //                indice++;
        //                xlWorkSheet.Cells[3, indice] = "Nombre";
        //            }
        //            if (tc)
        //            {
        //                indice++;
        //                xlWorkSheet.Cells[3, indice] = "TC";
        //            }
        //            if (fechaViaje)
        //            {
        //                indice++;
        //                xlWorkSheet.Cells[3, indice] = "Fecha Viaje";
        //            }
        //            if (fechaFin)
        //            {
        //                indice++;
        //                xlWorkSheet.Cells[3, indice] = "Fecha Fin";
        //            }
        //            if (tolva1)
        //            {
        //                indice++;
        //                xlWorkSheet.Cells[3, indice] = "Tolva 1";
        //            }
        //            if (tolva2)
        //            {
        //                indice++;
        //                xlWorkSheet.Cells[3, indice] = "Tolva 2";
        //            }
        //            if (destino)
        //            {
        //                indice++;
        //                xlWorkSheet.Cells[3, indice] = "Destino";
        //                indiceDestino = indice;
        //            }
        //            if (km)
        //            {
        //                indice++;
        //                xlWorkSheet.Cells[3, indice] = "KM";
        //                indiceKM = indice;
        //            }
        //            if (toneladas)
        //            {
        //                indice++;
        //                xlWorkSheet.Cells[3, indice] = "Toneladas";
        //                indiceToneladas = indice;
        //            }
        //            if (iva)
        //            {
        //                indice++;
        //                xlWorkSheet.Cells[3, indice] = "IVA";
        //                indiceIva = indice;
        //            }
        //            if (retencion)
        //            {
        //                indice++;
        //                xlWorkSheet.Cells[3, indice] = "Retencion";
        //                indiceRetencion = indice;
        //            }
        //            if (importe)
        //            {
        //                indice++;
        //                xlWorkSheet.Cells[3, indice] = "Importe";
        //                indiceImporte = indice;
        //            }
        //            if (_total)
        //            {
        //                indice++;
        //                xlWorkSheet.Cells[3, indice] = "Total";
        //                indiceTotal = indice;
        //            }
        //            if (tipoViaje)
        //            {
        //                indice++;
        //                xlWorkSheet.Cells[3, indice] = "TipoViaje";
        //                //indiceRetencion = indice;
        //            }
        //            if (zona)
        //            {
        //                indice++;
        //                xlWorkSheet.Cells[3, indice] = "Zona";
        //            }

        //            decimal sumaToneladas = 0m;
        //            decimal sumaIva = 0m;
        //            decimal sumaRetencion = 0m;
        //            decimal sumaImporte = 0m;
        //            decimal total = 0m;
        //            decimal sumaKM = 0m;
        //            int i = 4;
        //            indice = 0;


        //            foreach (Viaje viajes in listViaje)
        //            {
        //                try
        //                {
        //                    indice = 0;
        //                    foreach (CartaPorte cartaPorte in viajes.listDetalles)
        //                    {
        //                        sumaToneladas += cartaPorte.volumenDescarga;
        //                        sumaIva += cartaPorte.ImporIVA;
        //                        sumaRetencion += cartaPorte.ImpRetencion;
        //                        sumaKM += cartaPorte.zonaSelect.km;
        //                        sumaImporte += cartaPorte.importeReal;
        //                        total += cartaPorte.importeReal + (cartaPorte.ImporIVA - cartaPorte.ImpRetencion);
        //                    }
        //                    if (folio)
        //                    {
        //                        indice++;
        //                        xlWorkSheet.Cells[i, indice] = viajes.NumGuiaId.ToString() + viajes.SerieGuia;
        //                    }
        //                    if (noViaje)
        //                    {
        //                        indice++;
        //                        xlWorkSheet.Cells[i, indice] = viajes.NumViaje;
        //                    }
        //                    if (chofer)
        //                    {
        //                        indice++;
        //                        xlWorkSheet.Cells[i, indice] = viajes.operador.nombre;
        //                    }
        //                    if (tc)
        //                    {
        //                        indice++;
        //                        xlWorkSheet.Cells[i, indice] = viajes.tractor.clave;
        //                    }
        //                    if (fechaViaje)
        //                    {
        //                        indice++;
        //                        xlWorkSheet.Cells[i, indice] = viajes.FechaHoraViaje.ToString("dd/MM/yyyy HH:mm:00");
        //                    }
        //                    if (fechaFin)
        //                    {
        //                        indice++;
        //                        xlWorkSheet.Cells[i, indice] = viajes.FechaHoraFin == null ? "" : Convert.ToDateTime(viajes.FechaHoraFin).ToString("dd/MM/yyyy HH:mm:00");
        //                    }
        //                    if (tolva1)
        //                    {
        //                        indice++;
        //                        xlWorkSheet.Cells[i, indice] = viajes.remolque.clave;
        //                    }
        //                    if (tolva2)
        //                    {
        //                        indice++;
        //                        xlWorkSheet.Cells[i, indice] = viajes.remolque2 == null ? "" : viajes.remolque2.clave;
        //                    }

        //                    var y = i;
        //                    var x = indice + 1;
        //                    if (destino || km || toneladas || iva || retencion || importe || _total)
        //                    {
        //                        foreach (var item in viajes.listDetalles)
        //                        {
        //                            x = indice + 1;
        //                            if (destino)
        //                            {
        //                                x++;
        //                                xlWorkSheet.Cells[i, indiceDestino] = item.zonaSelect.descripcion;
        //                            }
        //                            if (km)
        //                            {
        //                                x++;
        //                                xlWorkSheet.Cells[i, indiceKM] = item.zonaSelect.km;
        //                            }
        //                            if (toneladas)
        //                            {
        //                                x++;
        //                                xlWorkSheet.Cells[i, indiceToneladas] = item.volumenDescarga;
        //                                //indiceToneladas = indice;
        //                            }
        //                            if (iva)
        //                            {
        //                                x++;
        //                                xlWorkSheet.Cells[i, indiceIva] = item.ImporIVA.ToString("C3");
        //                                //indiceIva = indice;
        //                            }
        //                            if (retencion)
        //                            {
        //                                x++;
        //                                xlWorkSheet.Cells[i, indiceRetencion] = item.ImpRetencion.ToString("C3");
        //                                //indiceRetencion = indice;
        //                            }
        //                            if (importe)
        //                            {
        //                                x++;
        //                                xlWorkSheet.Cells[i, indiceImporte] = item.importeReal.ToString("C3");
        //                                //indiceImporte = indice;
        //                            }
        //                            if (_total)
        //                            {
        //                                x++;
        //                                xlWorkSheet.Cells[i, indiceTotal] = (item.importeReal + (item.ImporIVA - item.ImpRetencion)).ToString("C3");
        //                                //indiceTotal = indice;
        //                            }
        //                            i++;

        //                        }
        //                    }
        //                    indice = x;
        //                    if (tipoViaje)
        //                    {
        //                        //indice++;
        //                        xlWorkSheet.Cells[y, indice] = !isCliente ? (isFull(viajes)) : (viajes.tipoViaje == 'F' ? "FULL" : "SENCILLO");
        //                    }
        //                    if (zona)
        //                    {
        //                        indice++;
        //                        xlWorkSheet.Cells[y, indice] = obtenerZonaStr(viajes.listDetalles);
        //                    }

        //                    sumaToneladas = 0m;
        //                    sumaIva = 0m;
        //                    sumaRetencion = 0m;
        //                    sumaKM = 0m;
        //                    sumaImporte = 0m;
        //                    total = 0m;


        //                }
        //                catch (Exception ex)
        //                {
        //                    MessageBox.Show(ex.Message);
        //                    return false;
        //                }
        //            }

        //            //foreach (Viaje viajes in listViaje)
        //            //{
        //            //    foreach (CartaPorte cartaPorte in viajes.listDetalles)
        //            //    {
        //            //        sumaToneladas += cartaPorte.volumenDescarga;
        //            //        sumaIva += cartaPorte.ImporIVA;
        //            //        sumaRetencion += cartaPorte.ImpRetencion;
        //            //        sumaKM += cartaPorte.zonaSelect.km;
        //            //        sumaImporte += cartaPorte.importeReal;
        //            //        total += cartaPorte.importeReal + (cartaPorte.ImporIVA - cartaPorte.ImpRetencion);
        //            //    }
        //            //}

        //            //if (km)
        //            //{
        //            //    xlWorkSheet.Cells[i, indiceKM] = sumaKM;
        //            //}

        //            //if (toneladas)
        //            //{
        //            //    xlWorkSheet.Cells[i, indiceToneladas] = sumaToneladas;
        //            //}

        //            //if (importe)
        //            //{
        //            //    xlWorkSheet.Cells[i, indiceImporte] = sumaImporte;
        //            //}
        //            //if (iva)
        //            //{
        //            //    xlWorkSheet.Cells[i, indiceIva] = sumaIva;
        //            //}
        //            //if (retencion)
        //            //{
        //            //    xlWorkSheet.Cells[i, indiceRetencion] = sumaRetencion;
        //            //}
        //            //if (_total)
        //            //{
        //            //    xlWorkSheet.Cells[i, indiceTotal] = total;
        //            //}

        //            //excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 1], xlWorkSheet.Cells[i, 20]];
        //            //excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Red);
        //            //excelCellrange.Font.FontStyle = "Bold";

        //            //excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[3, 1], xlWorkSheet.Cells[3, indice]];
        //            //excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Blue);
        //            //excelCellrange.Font.FontStyle = "Bold";
        //            //excelCellrange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Silver);

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[3, 1], xlWorkSheet.Cells[i - 1, indice]];
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.Worksheet.ListObjects.AddEx(
        //                                                        SourceType: Excel.XlListObjectSourceType.xlSrcRange,
        //                                                        Source: excelCellrange,
        //                                                        XlListObjectHasHeaders: Excel.XlYesNoGuess.xlYes);
        //            excelCellrange.Font.Size = 6;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.EntireColumn.AutoFit();

        //            //string ar = selectedPath + "\\" + DateTime.Now.ToString("ddMMyy HHmmss");
        //            xlWorkBook.SaveAs(archivo + ".xlsx");
        //            rutaExcel = archivo + ".xlsx";
        //            bool resp = convertirExcelToSps(archivo);
        //            if (resp)
        //            {
        //                imprimirReporte(archivo + ".xps");
        //            }
        //            return resp;
        //        }

        //    }
        //    catch (Exception e)
        //    {
        //        MessageBox.Show(e.Message);
        //        return false;
        //    }
        //    finally
        //    {
        //        xlWorkBook.Close();
        //        xlApp.Quit();
        //        GC.WaitForPendingFinalizers();
        //        GC.Collect();
        //        GC.WaitForPendingFinalizers();
        //        GC.Collect();
        //    }
        //}

        //private decimal obtenerZona(List<CartaPorte> listDetalles)
        //{
        //    decimal mayor = 0m;
        //    foreach (var item in listDetalles)
        //    {
        //        if (item.zonaSelect.km > mayor)
        //        {
        //            mayor = item.zonaSelect.km;
        //        }
        //    }
        //    return mayor;
        //}

        //private string obtenerZonaStr(List<CartaPorte> listDetalles)
        //{
        //    decimal mayor = 0m;
        //    string zona = "";
        //    foreach (var item in listDetalles)
        //    {
        //        if (item.zonaSelect.km > mayor)
        //        {
        //            mayor = item.zonaSelect.km;
        //            zona = item.zonaSelect.claveZona;
        //        }
        //    }
        //    return zona;
        //}

        //public bool documentoReporteViajesPersonalizado(List<Viaje> listViaje,
        //    bool folio, bool noViaje, bool chofer, bool tc, bool tolva1, bool tolva2, bool fechaViaje,
        //    bool fechaFin, bool km, bool toneladas, bool iva, bool retencion, bool importe, bool _total,
        //    bool tipoViaje, DateTime fechaInicio, DateTime _fechaFin, Cliente cliente, bool zona, bool isCliente)
        //{
        //    string archivo = "";
        //    try
        //    {
        //        string path = System.Windows.Forms.Application.StartupPath;
        //        if (!Directory.Exists(path + "\\reportes\\"))
        //        {
        //            DirectoryInfo dir = Directory.CreateDirectory(path + "\\reportes\\");
        //        }

        //        //archivo = string.Format(@"{0}{1}", path + "\\reportes\\", cartaPorte.NumGuiaId);
        //        archivo = string.Format(@"{0}{1}", path + "\\reportes\\", "reporte");

        //        if (File.Exists(archivo + ".xlsx"))
        //        {
        //            File.Delete(archivo + ".xlsx");
        //        }
        //        if (File.Exists(archivo + ".xps"))
        //        {
        //            File.Delete(archivo + ".xps");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message);
        //    }

        //    Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
        //    Excel.Workbook xlWorkBook = xlApp.Workbooks.Add();
        //    //Microsoft.Office.Interop.Excel.Worksheet excelSheet;
        //    Microsoft.Office.Interop.Excel.Range excelCellrange;
        //    try
        //    {

        //        if (xlApp == null)
        //        {
        //            MessageBox.Show("Excel is not properly installed!!");
        //            return false;
        //        }
        //        else
        //        {

        //            Excel.Worksheet xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
        //            xlWorkSheet.PageSetup.LeftMargin = 2;
        //            xlWorkSheet.PageSetup.RightMargin = 2;
        //            xlWorkSheet.PageSetup.TopMargin = 15;
        //            xlWorkSheet.PageSetup.BottomMargin = 5;
        //            xlWorkSheet.PageSetup.Orientation = Microsoft.Office.Interop.Excel.XlPageOrientation.xlLandscape;
        //            int indice = 0;
        //            int indiceToneladas = 0;
        //            int indiceIva = 0;
        //            int indiceImporte = 0;
        //            int indiceRetencion = 0;
        //            int indiceTotal = 0;
        //            int indiceKM = 0;
        //            int indiceDestino = 0;

        //            xlWorkSheet.Name = "Viajes";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[1, 10]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 8;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            xlWorkSheet.Cells[1, 1] = cliente.nombre;

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[2, 1], xlWorkSheet.Cells[2, 10]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            xlWorkSheet.Cells[2, 1] = "Listado de Cobranza Fechas del " + fechaInicio.ToString("dd/MM/yyyy") + " al " + _fechaFin.ToString("dd/MM/yyyy");

        //            if (folio)
        //            {
        //                indice++;
        //                xlWorkSheet.Cells[3, indice] = "Folio";
        //            }
        //            if (noViaje)
        //            {
        //                indice++;
        //                xlWorkSheet.Cells[3, indice] = "No. Viaje";
        //            }
        //            if (chofer)
        //            {
        //                indice++;
        //                xlWorkSheet.Cells[3, indice] = "Nombre";
        //            }
        //            if (tc)
        //            {
        //                indice++;
        //                xlWorkSheet.Cells[3, indice] = "TC";
        //            }
        //            if (fechaViaje)
        //            {
        //                indice++;
        //                xlWorkSheet.Cells[3, indice] = "Fecha Viaje";
        //            }
        //            if (fechaFin)
        //            {
        //                indice++;
        //                xlWorkSheet.Cells[3, indice] = "Fecha Fin";
        //            }
        //            if (tolva1)
        //            {
        //                indice++;
        //                xlWorkSheet.Cells[3, indice] = "Tolva 1";
        //            }
        //            if (tolva2)
        //            {
        //                indice++;
        //                xlWorkSheet.Cells[3, indice] = "Tolva 2";
        //            }
        //            if (km)
        //            {
        //                indice++;
        //                xlWorkSheet.Cells[3, indice] = "KM";
        //                indiceKM = indice;
        //            }
        //            if (toneladas)
        //            {
        //                indice++;
        //                xlWorkSheet.Cells[3, indice] = "Toneladas";
        //                indiceToneladas = indice;
        //            }
        //            if (iva)
        //            {
        //                indice++;
        //                xlWorkSheet.Cells[3, indice] = "IVA";
        //                indiceIva = indice;
        //            }
        //            if (retencion)
        //            {
        //                indice++;
        //                xlWorkSheet.Cells[3, indice] = "Retencion";
        //                indiceRetencion = indice;
        //            }
        //            if (importe)
        //            {
        //                indice++;
        //                xlWorkSheet.Cells[3, indice] = "Importe";
        //                indiceImporte = indice;
        //            }
        //            if (_total)
        //            {
        //                indice++;
        //                xlWorkSheet.Cells[3, indice] = "Total";
        //                indiceTotal = indice;
        //            }
        //            if (tipoViaje)
        //            {
        //                indice++;
        //                xlWorkSheet.Cells[3, indice] = "Tipo Viaje";
        //                //indiceRetencion = indice;
        //            }
        //            if (zona)
        //            {
        //                indice++;
        //                xlWorkSheet.Cells[3, indice] = "Zona";
        //                //indiceRetencion = indice;
        //            }
        //            decimal sumaToneladas = 0m;
        //            decimal sumaIva = 0m;
        //            decimal sumaRetencion = 0m;
        //            decimal sumaImporte = 0m;
        //            decimal total = 0m;
        //            decimal sumaKM = 0m;
        //            int i = 4;
        //            indice = 0;
        //            foreach (Viaje viajes in listViaje)
        //            {
        //                try
        //                {
        //                    indice = 0;
        //                    foreach (CartaPorte cartaPorte in viajes.listDetalles)
        //                    {
        //                        sumaToneladas += cartaPorte.volumenDescarga;
        //                        sumaIva += cartaPorte.ImporIVA;
        //                        sumaRetencion += cartaPorte.ImpRetencion;
        //                        sumaKM += cartaPorte.zonaSelect.km;
        //                        sumaImporte += cartaPorte.importeReal;
        //                        total += cartaPorte.importeReal + (cartaPorte.ImporIVA - cartaPorte.ImpRetencion);
        //                    }
        //                    if (folio)
        //                    {
        //                        indice++;
        //                        xlWorkSheet.Cells[i, indice] = viajes.NumGuiaId.ToString() + viajes.SerieGuia;
        //                    }
        //                    if (noViaje)
        //                    {
        //                        indice++;
        //                        xlWorkSheet.Cells[i, indice] = viajes.NumViaje;
        //                    }
        //                    if (chofer)
        //                    {
        //                        indice++;
        //                        xlWorkSheet.Cells[i, indice] = viajes.operador.nombre;
        //                    }
        //                    if (tc)
        //                    {
        //                        indice++;
        //                        xlWorkSheet.Cells[i, indice] = viajes.tractor.clave;
        //                    }
        //                    if (fechaViaje)
        //                    {
        //                        indice++;
        //                        xlWorkSheet.Cells[i, indice] = viajes.FechaHoraViaje.ToString("dd/MM/yyyy HH:mm:00");
        //                    }
        //                    if (fechaFin)
        //                    {
        //                        indice++;
        //                        xlWorkSheet.Cells[i, indice] = viajes.FechaHoraFin == null ? "" : Convert.ToDateTime(viajes.FechaHoraFin).ToString("dd/MM/yyyy HH:mm:00");
        //                    }
        //                    if (tolva1)
        //                    {
        //                        indice++;
        //                        xlWorkSheet.Cells[i, indice] = viajes.remolque.clave;
        //                    }
        //                    if (tolva2)
        //                    {
        //                        indice++;
        //                        xlWorkSheet.Cells[i, indice] = viajes.remolque2 == null ? "" : viajes.remolque2.clave;
        //                    }
        //                    if (km)
        //                    {
        //                        indice++;
        //                        xlWorkSheet.Cells[i, indiceKM] = obtenerZona(viajes.listDetalles);
        //                    }
        //                    if (toneladas)
        //                    {
        //                        indice++;
        //                        xlWorkSheet.Cells[i, indiceToneladas] = sumaToneladas;
        //                        //indiceToneladas = indice;
        //                    }
        //                    if (iva)
        //                    {
        //                        indice++;
        //                        xlWorkSheet.Cells[i, indiceIva] = sumaIva.ToString("C3");
        //                        //indiceIva = indice;
        //                    }
        //                    if (retencion)
        //                    {
        //                        indice++;
        //                        xlWorkSheet.Cells[i, indiceRetencion] = sumaRetencion.ToString("C3");
        //                        //indiceRetencion = indice;
        //                    }
        //                    if (importe)
        //                    {
        //                        indice++;
        //                        xlWorkSheet.Cells[i, indiceImporte] = sumaImporte.ToString("C3");
        //                        //indiceImporte = indice;
        //                    }
        //                    if (_total)
        //                    {
        //                        indice++;
        //                        xlWorkSheet.Cells[i, indiceTotal] = total.ToString("C3");
        //                        //indiceTotal = indice;
        //                    }
        //                    if (tipoViaje)
        //                    {
        //                        indice++;
        //                        xlWorkSheet.Cells[i, indice] = !isCliente ? (isFull(viajes)) : (viajes.tipoViaje == 'F' ? "FULL" : "SENCILLO");
        //                    }
        //                    if (zona)
        //                    {
        //                        indice++;
        //                        xlWorkSheet.Cells[i, indice] = obtenerZonaStr(viajes.listDetalles);
        //                        //indiceRetencion = indice;
        //                    }
        //                    sumaToneladas = 0m;
        //                    sumaIva = 0m;
        //                    sumaRetencion = 0m;
        //                    sumaKM = 0m;
        //                    sumaImporte = 0m;
        //                    total = 0m;

        //                    i++;
        //                }
        //                catch (Exception ex)
        //                {
        //                    MessageBox.Show(ex.Message);
        //                    return false;
        //                }
        //            }

        //            //foreach (Viaje viajes in listViaje)
        //            //{
        //            //    foreach (CartaPorte cartaPorte in viajes.listDetalles)
        //            //    {
        //            //        sumaToneladas += cartaPorte.volumenDescarga;
        //            //        sumaIva += cartaPorte.ImporIVA;
        //            //        sumaRetencion += cartaPorte.ImpRetencion;
        //            //        sumaKM += cartaPorte.zonaSelect.km;
        //            //        sumaImporte += cartaPorte.importeReal;
        //            //        total += cartaPorte.importeReal + (cartaPorte.ImporIVA - cartaPorte.ImpRetencion);
        //            //    }
        //            //}

        //            //if (km)
        //            //{
        //            //    xlWorkSheet.Cells[i, indiceKM] = sumaKM;
        //            //}

        //            //if (toneladas)
        //            //{
        //            //    xlWorkSheet.Cells[i, indiceToneladas] = sumaToneladas;
        //            //}

        //            //if (importe)
        //            //{
        //            //    xlWorkSheet.Cells[i, indiceImporte] = sumaImporte;
        //            //}
        //            //if (iva)
        //            //{
        //            //    xlWorkSheet.Cells[i, indiceIva] = sumaIva;
        //            //}
        //            //if (retencion)
        //            //{
        //            //    xlWorkSheet.Cells[i, indiceRetencion] = sumaRetencion;
        //            //}
        //            //if (_total)
        //            //{
        //            //    xlWorkSheet.Cells[i, indiceTotal] = total;
        //            //}

        //            //excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 1], xlWorkSheet.Cells[i, 17]];
        //            //excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Red);
        //            //excelCellrange.Font.FontStyle = "Bold";

        //            //excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[3, 1], xlWorkSheet.Cells[3, indice]];
        //            //excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Blue);
        //            //excelCellrange.Font.FontStyle = "Bold";
        //            //excelCellrange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Silver);

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[3, 1], xlWorkSheet.Cells[i - 1, indice]];
        //            excelCellrange.Font.Size = 6;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.Worksheet.ListObjects.AddEx(
        //                                                        SourceType: Excel.XlListObjectSourceType.xlSrcRange,
        //                                                        Source: excelCellrange,
        //                                                        XlListObjectHasHeaders: Excel.XlYesNoGuess.xlYes);
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.EntireColumn.AutoFit();

        //            //string ar = selectedPath + "\\" + DateTime.Now.ToString("ddMMyy HHmmss");
        //            xlWorkBook.SaveAs(archivo + ".xlsx");
        //            rutaExcel = archivo + ".xlsx";
        //            bool resp = convertirExcelToSps(archivo);
        //            if (resp)
        //            {
        //                imprimirReporte(archivo + ".xps");
        //            }
        //            return resp;
        //        }

        //    }
        //    catch (Exception e)
        //    {
        //        MessageBox.Show(e.Message);
        //        return false;
        //    }
        //    finally
        //    {
        //        xlWorkBook.Close();
        //        xlApp.Quit();
        //        GC.WaitForPendingFinalizers();
        //        GC.Collect();
        //        GC.WaitForPendingFinalizers();
        //        GC.Collect();
        //    }
        //}

        //private string isFull(Viaje viajes)
        //{
        //    UnidadTransporte x = viajes.remolque;


        //    if (viajes.dolly != null || viajes.remolque2 != null)
        //    {
        //        return "FULL";
        //    }

        //    return "SENCILLO";
        //}
        //private string isFullBachoco(Viaje viajes)
        //{
        //    UnidadTransporte x = viajes.remolque;
        //    if (viajes.dolly != null || viajes.remolque2 != null)
        //    {
        //        return "FULL";
        //    }
        //    else if (x.tipoUnidad.descripcion == "TOLVA 35 TON")
        //    {
        //        return "FULL";
        //    }
        //    return "SENCILLO";
        //}

        //public bool documentoReporte(List<Viaje> listCartaPorte,
        //    bool fecha, bool hora, bool orden, bool operador, bool tc, bool tolva1, bool tolva2,
        //    bool destino, bool km, bool producto, bool toneladas, bool precio, bool importe, bool iva,
        //    bool retencion, bool _total, Usuario usuario, DateTime fechaInicio, DateTime fechaFin, Cliente cliente)
        //{
        //    string archivo = "";
        //    try
        //    {
        //        string path = System.Windows.Forms.Application.StartupPath;
        //        if (!Directory.Exists(path + "\\reportes\\"))
        //        {
        //            DirectoryInfo dir = Directory.CreateDirectory(path + "\\reportes\\");
        //        }

        //        //archivo = string.Format(@"{0}{1}", path + "\\reportes\\", cartaPorte.NumGuiaId);
        //        archivo = string.Format(@"{0}{1}", path + "\\reportes\\", "reporte");

        //        if (File.Exists(archivo + ".xlsx"))
        //        {
        //            File.Delete(archivo + ".xlsx");
        //        }
        //        if (File.Exists(archivo + ".xps"))
        //        {
        //            File.Delete(archivo + ".xps");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message);
        //    }

        //    Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
        //    Excel.Workbook xlWorkBook = xlApp.Workbooks.Add();
        //    //Microsoft.Office.Interop.Excel.Worksheet excelSheet;
        //    Microsoft.Office.Interop.Excel.Range excelCellrange;
        //    try
        //    {

        //        if (xlApp == null)
        //        {
        //            MessageBox.Show("Excel is not properly installed!!");
        //            return false;
        //        }
        //        else
        //        {

        //            Excel.Worksheet xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
        //            xlWorkSheet.PageSetup.LeftMargin = 2;
        //            xlWorkSheet.PageSetup.RightMargin = 2;
        //            xlWorkSheet.PageSetup.TopMargin = 15;
        //            xlWorkSheet.PageSetup.BottomMargin = 5;
        //            xlWorkSheet.PageSetup.Orientation = Microsoft.Office.Interop.Excel.XlPageOrientation.xlLandscape;
        //            int indice = 0;
        //            int indiceToneladas = 0;
        //            int indiceIva = 0;
        //            int indiceImporte = 0;
        //            int indiceRetencion = 0;
        //            int indiceTotal = 0;
        //            int indiceKM = 0;

        //            xlWorkSheet.Name = "Cobranza";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[1, 10]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 8;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            xlWorkSheet.Cells[1, 1] = cliente.nombre;

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[2, 1], xlWorkSheet.Cells[2, 10]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            xlWorkSheet.Cells[2, 1] = "Listado de Cobranza Fechas del " + fechaInicio.ToString("dd/MM/yyyy") + " al " + fechaFin.ToString("dd/MM/yyyy");

        //            if (fecha)
        //            {
        //                indice++;
        //                xlWorkSheet.Cells[3, indice] = "Fecha";
        //            }
        //            if (hora)
        //            {
        //                indice++;
        //                xlWorkSheet.Cells[3, indice] = "Hora";
        //            }
        //            if (orden)
        //            {
        //                indice++;
        //                xlWorkSheet.Cells[3, indice] = "Orden";
        //            }
        //            if (operador)
        //            {
        //                indice++;
        //                xlWorkSheet.Cells[3, indice] = "Operador";
        //            }
        //            if (tc)
        //            {
        //                indice++;
        //                xlWorkSheet.Cells[3, indice] = "TC";
        //            }
        //            if (tolva1)
        //            {
        //                indice++;
        //                xlWorkSheet.Cells[3, indice] = "Tolva 1";
        //            }
        //            if (tolva2)
        //            {
        //                indice++;
        //                xlWorkSheet.Cells[3, indice] = "Tolva 2";
        //            }
        //            if (destino)
        //            {
        //                indice++;
        //                xlWorkSheet.Cells[3, indice] = "Destino";
        //            }
        //            if (km)
        //            {
        //                indice++;
        //                xlWorkSheet.Cells[3, indice] = "KM";
        //                indiceKM = indice;
        //            }
        //            if (producto)
        //            {
        //                indice++;
        //                xlWorkSheet.Cells[3, indice] = "Producto";
        //            }
        //            if (toneladas)
        //            {
        //                indice++;
        //                xlWorkSheet.Cells[3, indice] = "Tons";
        //                indiceToneladas = indice;
        //            }
        //            if (precio)
        //            {
        //                indice++;
        //                xlWorkSheet.Cells[3, indice] = "Precio";
        //            }
        //            if (importe)
        //            {
        //                indice++;
        //                xlWorkSheet.Cells[3, indice] = "Importe";
        //                indiceImporte = indice;
        //            }
        //            if (iva)
        //            {
        //                indice++;
        //                xlWorkSheet.Cells[3, indice] = "IVA";
        //                indiceIva = indice;
        //            }
        //            if (retencion)
        //            {
        //                indice++;
        //                xlWorkSheet.Cells[3, indice] = "Retención";
        //                indiceRetencion = indice;
        //            }
        //            if (_total)
        //            {
        //                indice++;
        //                xlWorkSheet.Cells[3, indice] = "Total";
        //                indiceTotal = indice;
        //            }

        //            decimal sumaToneladas = 0m;
        //            decimal sumaIva = 0m;
        //            decimal sumaRetencion = 0m;
        //            decimal sumaImporte = 0m;
        //            decimal total = 0m;
        //            decimal sumaKm = 0m;
        //            int i = 4;
        //            indice = 0;


        //            foreach (var cartaPorte in listCartaPorte)
        //            {
        //                foreach (var detalles in cartaPorte.listDetalles)
        //                {
        //                    try
        //                    {
        //                        indice = 0;
        //                        if (fecha)
        //                        {
        //                            indice++;
        //                            xlWorkSheet.Cells[i, indice] = cartaPorte.FechaHoraViaje.ToString("MM/dd/yyyy");
        //                        }
        //                        if (hora)
        //                        {
        //                            indice++;
        //                            xlWorkSheet.Cells[i, indice] = cartaPorte.FechaHoraViaje.ToString("HH:mm:ss");
        //                        }
        //                        if (orden)
        //                        {
        //                            indice++;
        //                            xlWorkSheet.Cells[i, indice] = detalles.remision.ToString();
        //                        }
        //                        if (operador)
        //                        {
        //                            indice++;
        //                            xlWorkSheet.Cells[i, indice] = cartaPorte.operador == null ? "(Ninguno)" : cartaPorte.operador.nombre;
        //                        }
        //                        if (tc)
        //                        {
        //                            indice++;
        //                            xlWorkSheet.Cells[i, indice] = cartaPorte.tractor.clave;
        //                        }
        //                        if (tolva1)
        //                        {
        //                            indice++;
        //                            xlWorkSheet.Cells[i, indice] = cartaPorte.remolque.clave;
        //                        }
        //                        if (tolva2)
        //                        {
        //                            indice++;
        //                            xlWorkSheet.Cells[i, indice] = cartaPorte.remolque2 == null ? "" : cartaPorte.remolque2.clave;
        //                        }
        //                        if (destino)
        //                        {
        //                            indice++;
        //                            xlWorkSheet.Cells[i, indice] = detalles.zonaSelect.descripcion;
        //                        }
        //                        if (km)
        //                        {
        //                            indice++;
        //                            xlWorkSheet.Cells[i, indice] = detalles.zonaSelect.km;
        //                            sumaKm += detalles.zonaSelect.km;
        //                        }
        //                        if (producto)
        //                        {
        //                            indice++;
        //                            xlWorkSheet.Cells[i, indice] = detalles.producto.descripcion;
        //                        }
        //                        if (toneladas)
        //                        {
        //                            indice++;
        //                            xlWorkSheet.Cells[i, indice] = detalles.volumenDescarga;
        //                            sumaToneladas += detalles.volumenDescarga;
        //                        }
        //                        if (precio)
        //                        {
        //                            indice++;
        //                            xlWorkSheet.Cells[i, indice] = detalles.precio; /*detalles.isFull ? detalles.zonaSelect.costoFull : detalles.zonaSelect.costoSencillo;*/
        //                        }
        //                        if (importe)
        //                        {
        //                            indice++;
        //                            xlWorkSheet.Cells[i, indice] = detalles.importeReal;
        //                            sumaImporte += detalles.importeReal;
        //                        }
        //                        if (iva)
        //                        {
        //                            indice++;
        //                            xlWorkSheet.Cells[i, indice] = detalles.ImporIVA;
        //                            sumaIva += detalles.ImporIVA;
        //                        }
        //                        if (retencion)
        //                        {
        //                            indice++;
        //                            xlWorkSheet.Cells[i, indice] = detalles.ImpRetencion;
        //                            sumaRetencion += detalles.ImpRetencion;
        //                        }
        //                        if (_total)
        //                        {
        //                            indice++;
        //                            xlWorkSheet.Cells[i, indice] = (detalles.importeReal + detalles.ImporIVA) - detalles.ImpRetencion;
        //                            total += (detalles.importeReal + detalles.ImporIVA) - detalles.ImpRetencion;
        //                        }

        //                        i++;
        //                    }
        //                    catch (Exception ex)
        //                    {
        //                        MessageBox.Show(ex.Message);
        //                        return false;
        //                    }

        //                }
        //            }

        //if (km)
        //{
        //    xlWorkSheet.Cells[i, indiceKM] = sumaKm;
        //}
        //if (toneladas)
        //{
        //    xlWorkSheet.Cells[i, indiceToneladas] = sumaToneladas;
        //}

        //if (importe)
        //{
        //    xlWorkSheet.Cells[i, indiceImporte] = sumaImporte;
        //}
        //if (iva)
        //{
        //    xlWorkSheet.Cells[i, indiceIva] = sumaIva;
        //}
        //if (retencion)
        //{
        //    xlWorkSheet.Cells[i, indiceRetencion] = sumaRetencion;
        //}
        //if (_total)
        //{
        //    xlWorkSheet.Cells[i, indiceTotal] = total;
        //}

        //xlWorkSheet.Cells[i, 10] = sumaToneladas;
        //xlWorkSheet.Cells[i, 12] = sumaImporte;
        //xlWorkSheet.Cells[i, 13] = sumaIva;
        //xlWorkSheet.Cells[i, 14] = sumaRetencion;
        //xlWorkSheet.Cells[i, 15] = total;

        //excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 1], xlWorkSheet.Cells[i, 15]];
        //excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Red);
        //excelCellrange.Font.FontStyle = "Bold";

        //excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[3, 1], xlWorkSheet.Cells[3, indice]];
        //excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Blue);
        //excelCellrange.Font.FontStyle = "Bold";
        //excelCellrange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Silver);

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[3, 1], xlWorkSheet.Cells[i - 1, indice]];
        //            excelCellrange.Font.Size = 6;
        //            excelCellrange.Worksheet.ListObjects.AddEx(
        //                                                        SourceType: Excel.XlListObjectSourceType.xlSrcRange,
        //                                                        Source: excelCellrange,
        //                                                        XlListObjectHasHeaders: Excel.XlYesNoGuess.xlYes);
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.EntireColumn.AutoFit();

        //            //string ar = selectedPath + "\\" + DateTime.Now.ToString("ddMMyy HHmmss");
        //            xlWorkBook.SaveAs(archivo + ".xlsx");
        //            rutaExcel = archivo + ".xlsx";
        //            bool resp = convertirExcelToSps(archivo);
        //            if (resp)
        //            {
        //                imprimirReporte(archivo + ".xps");
        //            }
        //            return resp;
        //        }

        //    }
        //    catch (Exception e)
        //    {
        //        MessageBox.Show(e.Message);
        //        return false;
        //    }
        //    finally
        //    {
        //        xlWorkBook.Close();
        //        xlApp.Quit();
        //        GC.WaitForPendingFinalizers();
        //        GC.Collect();
        //        GC.WaitForPendingFinalizers();
        //        GC.Collect();
        //    }
        //}


        //public bool documentoReporte(List<Viaje> listCartaPorte, DateTime fechaInicio, DateTime fechaFin, Cliente cliente)
        //{
        //    string archivo = "";
        //    try
        //    {
        //        string path = System.Windows.Forms.Application.StartupPath;
        //        if (!Directory.Exists(path + "\\reportes\\"))
        //        {
        //            DirectoryInfo dir = Directory.CreateDirectory(path + "\\reportes\\");
        //        }

        //        //archivo = string.Format(@"{0}{1}", path + "\\reportes\\", cartaPorte.NumGuiaId);
        //        archivo = string.Format(@"{0}{1}", path + "\\reportes\\", "reporte");

        //        if (File.Exists(archivo + ".xlsx"))
        //        {
        //            File.Delete(archivo + ".xlsx");
        //        }
        //        if (File.Exists(archivo + ".xps"))
        //        {
        //            File.Delete(archivo + ".xps");
        //        }
        //        //".xps"
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message);
        //    }

        //    Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
        //    Excel.Workbook xlWorkBook = xlApp.Workbooks.Add();
        //    //Microsoft.Office.Interop.Excel.Worksheet excelSheet;
        //    Microsoft.Office.Interop.Excel.Range excelCellrange;
        //    try
        //    {

        //        if (xlApp == null)
        //        {
        //            MessageBox.Show("Excel is not properly installed!!");
        //            return false;
        //        }
        //        else
        //        {

        //            Excel.Worksheet xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
        //            xlWorkSheet.PageSetup.LeftMargin = 2;
        //            xlWorkSheet.PageSetup.RightMargin = 2;
        //            xlWorkSheet.PageSetup.TopMargin = 15;
        //            xlWorkSheet.PageSetup.BottomMargin = 5;
        //            xlWorkSheet.PageSetup.Orientation = Microsoft.Office.Interop.Excel.XlPageOrientation.xlLandscape;
        //            //xlWorkSheet.Name = "Reporte de Salidas";

        //            xlWorkSheet.Name = "Cobranza";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[1, 10]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 8;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            xlWorkSheet.Cells[1, 1] = cliente.nombre;

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[2, 1], xlWorkSheet.Cells[2, 10]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            xlWorkSheet.Cells[2, 1] = "Listado de Cobranza Fechas del " + fechaInicio.ToString("dd/MM/yyyy") + " al " + fechaFin.ToString("dd/MM/yyyy");

        //            xlWorkSheet.Cells[3, 1] = "No Viaje";
        //            xlWorkSheet.Cells[3, 2] = "Carta Porte";
        //            xlWorkSheet.Cells[3, 3] = "Fecha Viaje";
        //            xlWorkSheet.Cells[3, 4] = "Fecha Fin";
        //            xlWorkSheet.Cells[3, 5] = "Duración Viaje";
        //            xlWorkSheet.Cells[3, 6] = "Orden";
        //            xlWorkSheet.Cells[3, 7] = "Operador";
        //            xlWorkSheet.Cells[3, 8] = "TC";
        //            xlWorkSheet.Cells[3, 9] = "Tolva 1";
        //            xlWorkSheet.Cells[3, 10] = "Tolva 2";
        //            xlWorkSheet.Cells[3, 11] = "Destino";
        //            xlWorkSheet.Cells[3, 12] = "KM";
        //            xlWorkSheet.Cells[3, 13] = "Producto";
        //            xlWorkSheet.Cells[3, 14] = "Tons";
        //            xlWorkSheet.Cells[3, 15] = "Precio";
        //            xlWorkSheet.Cells[3, 16] = "Importe";
        //            xlWorkSheet.Cells[3, 17] = "IVA";
        //            xlWorkSheet.Cells[3, 18] = "Retención";
        //            xlWorkSheet.Cells[3, 19] = "Total";
        //            xlWorkSheet.Cells[3, 20] = "Estado";
        //            decimal sumaToneladas = 0m;
        //            decimal sumaIva = 0m;
        //            decimal sumaRetencion = 0m;
        //            decimal sumaImporte = 0m;
        //            decimal total = 0m;
        //            decimal sumaKM = 0m;
        //            int i = 4;
        //            foreach (var cartaPorte in listCartaPorte)
        //            {
        //                foreach (var detalles in cartaPorte.listDetalles)
        //                {
        //                    try
        //                    {
        //                        xlWorkSheet.Cells[i, 1] = cartaPorte.NumGuiaId.ToString();
        //                        xlWorkSheet.Cells[i, 2] = detalles.Consecutivo.ToString();
        //                        xlWorkSheet.Cells[i, 3] = cartaPorte.FechaHoraViaje.ToString("MM/dd/yyyy HH:mm:ss");
        //                        xlWorkSheet.Cells[i, 4] = cartaPorte.FechaHoraFin == null ? "" : ((DateTime)cartaPorte.FechaHoraFin).ToString("MM/dd/yyyy HH:mm:ss");

        //                        if (cartaPorte.FechaHoraFin != null)
        //                        {
        //                            TimeSpan ts = (DateTime)cartaPorte.FechaHoraFin - cartaPorte.FechaHoraViaje;
        //                            var tiempo = ts.Hours.ToString() + ":" + ts.Minutes.ToString() + ":" + ts.Seconds.ToString();
        //                            xlWorkSheet.Cells[i, 5] = tiempo;
        //                        }

        //                        xlWorkSheet.Cells[i, 6] = detalles.remision.ToString();
        //                        xlWorkSheet.Cells[i, 7] = cartaPorte.operador == null ? "(Ninguno)" : cartaPorte.operador.nombre;
        //                        xlWorkSheet.Cells[i, 8] = cartaPorte.tractor.clave;
        //                        xlWorkSheet.Cells[i, 9] = cartaPorte.remolque.clave;
        //                        xlWorkSheet.Cells[i, 10] = cartaPorte.remolque2 == null ? "" : cartaPorte.remolque2.clave;
        //                        xlWorkSheet.Cells[i, 11] = detalles.zonaSelect.descripcion;
        //                        xlWorkSheet.Cells[i, 12] = detalles.zonaSelect.km;
        //                        sumaKM += detalles.zonaSelect.km;
        //                        xlWorkSheet.Cells[i, 13] = detalles.producto.descripcion;
        //                        xlWorkSheet.Cells[i, 14] = detalles.volumenDescarga;
        //                        sumaToneladas += detalles.volumenDescarga;
        //                        xlWorkSheet.Cells[i, 15] = detalles.precio.ToString("C3"); // detalles.isFull ? detalles.zonaSelect.costoFull : detalles.zonaSelect.costoSencillo;
        //                        xlWorkSheet.Cells[i, 16] = detalles.importeReal.ToString("C3");
        //                        sumaImporte += detalles.importeReal;
        //                        xlWorkSheet.Cells[i, 17] = detalles.ImporIVA.ToString("C3");
        //                        sumaIva += detalles.ImporIVA;
        //                        xlWorkSheet.Cells[i, 18] = detalles.ImpRetencion.ToString("C3");
        //                        sumaRetencion += detalles.ImpRetencion;
        //                        xlWorkSheet.Cells[i, 19] = ((detalles.importeReal + detalles.ImporIVA) - detalles.ImpRetencion).ToString("C3");
        //                        total += (detalles.importeReal + detalles.ImporIVA) - detalles.ImpRetencion;
        //                        xlWorkSheet.Cells[i, 20] = detalles.zonaSelect.estado.nombre;
        //                        i++;
        //                    }
        //                    catch (Exception ex)
        //                    {
        //                        MessageBox.Show(ex.Message);
        //                        return false;
        //                    }

        //                }
        //            }
        //            //xlWorkSheet.Cells[i, 9] = sumaKM;
        //            //xlWorkSheet.Cells[i, 11] = sumaToneladas;
        //            //xlWorkSheet.Cells[i, 13] = sumaImporte;
        //            //xlWorkSheet.Cells[i, 14] = sumaIva;
        //            //xlWorkSheet.Cells[i, 15] = sumaRetencion;
        //            //xlWorkSheet.Cells[i, 16] = total;
        //            //excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 10], xlWorkSheet.Cells[i, 20]];
        //            //excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Red);
        //            //excelCellrange.Font.FontStyle = "Bold";

        //            //excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[3, 1], xlWorkSheet.Cells[3, 20]];
        //            //excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Blue);
        //            //excelCellrange.Font.FontStyle = "Bold";
        //            //excelCellrange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Silver);

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[3, 1], xlWorkSheet.Cells[i - 1, 20]];
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.Worksheet.ListObjects.AddEx(
        //                                                        SourceType: Excel.XlListObjectSourceType.xlSrcRange,
        //                                                        Source: excelCellrange,
        //                                                        XlListObjectHasHeaders: Excel.XlYesNoGuess.xlYes);
        //            excelCellrange.Font.Size = 5;
        //            excelCellrange.EntireColumn.AutoFit();

        //            xlWorkBook.SaveAs(archivo + ".xlsx");
        //            rutaExcel = archivo + ".xlsx";

        //            bool resp = convertirExcelToSps(archivo);
        //            if (resp)
        //            {
        //                imprimirReporte(archivo + ".xps");
        //            }
        //            return resp;
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        MessageBox.Show(e.Message);
        //        return false;
        //    }
        //    finally
        //    {
        //        xlWorkBook.Close();
        //        xlApp.Quit();
        //        GC.WaitForPendingFinalizers();
        //        GC.Collect();
        //        GC.WaitForPendingFinalizers();
        //        GC.Collect();
        //    }
        //}

        //public bool documentoReporteViajesBachocoMensual(List<Viaje> ListViajes, DateTime fechaInicio, DateTime fechaFin, Cliente cliente)
        //{
        //    string archivo = "";
        //    try
        //    {
        //        string path = System.Windows.Forms.Application.StartupPath;
        //        if (!Directory.Exists(path + "\\reportes\\"))
        //        {
        //            DirectoryInfo dir = Directory.CreateDirectory(path + "\\reportes\\");
        //        }

        //        //archivo = string.Format(@"{0}{1}", path + "\\reportes\\", cartaPorte.NumGuiaId);
        //        archivo = string.Format(@"{0}{1}", path + "\\reportes\\", "reporte");

        //        if (File.Exists(archivo + ".xlsx"))
        //        {
        //            File.Delete(archivo + ".xlsx");
        //        }
        //        if (File.Exists(archivo + ".xps"))
        //        {
        //            File.Delete(archivo + ".xps");
        //        }
        //        //".xps"
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message);
        //    }

        //    Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
        //    Excel.Workbook xlWorkBook = xlApp.Workbooks.Add();
        //    //Microsoft.Office.Interop.Excel.Worksheet excelSheet;
        //    Microsoft.Office.Interop.Excel.Range excelCellrange;
        //    try
        //    {

        //        if (xlApp == null)
        //        {
        //            MessageBox.Show("Excel is not properly installed!!");
        //            return false;
        //        }
        //        else
        //        {

        //            Excel.Worksheet xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
        //            xlWorkSheet.PageSetup.LeftMargin = 2;
        //            xlWorkSheet.PageSetup.RightMargin = 2;
        //            xlWorkSheet.PageSetup.TopMargin = 15;
        //            xlWorkSheet.PageSetup.BottomMargin = 5;
        //            xlWorkSheet.PageSetup.Orientation = Microsoft.Office.Interop.Excel.XlPageOrientation.xlLandscape;
        //            //xlWorkSheet.Name = "Reporte de Salidas";
        //            xlWorkSheet.Name = "Cobranza";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[1, 10]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 8;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            xlWorkSheet.Cells[1, 1] = "TRANSPORTE DIVISIÓN";//cliente.nombre;

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[2, 1], xlWorkSheet.Cells[2, 10]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            xlWorkSheet.Cells[2, 1] = "REGISTRO DIARIO DE VIAJES - línea de entrega (Pollo Procesado, Pollo Vivo a Clientes, etc.)";

        //            xlWorkSheet.Cells[4, 1] = "CENTRO";
        //            xlWorkSheet.Cells[4, 2] = "CONTROL DE VIAJE (FOLIO)";
        //            xlWorkSheet.Cells[4, 3] = "FECHA DE VIAJE";
        //            xlWorkSheet.Cells[4, 4] = "LINEA DE ENTREGA";
        //            xlWorkSheet.Cells[4, 5] = "TRACTOR";
        //            xlWorkSheet.Cells[4, 6] = "REMOLQUE";
        //            xlWorkSheet.Cells[4, 7] = "OPERADOR";
        //            xlWorkSheet.Cells[4, 8] = "TIPO DE VIAJE";
        //            xlWorkSheet.Cells[4, 9] = "EQUIPO";
        //            xlWorkSheet.Cells[4, 10] = "ORIGEN";
        //            xlWorkSheet.Cells[4, 11] = "DESTINO";
        //            xlWorkSheet.Cells[4, 12] = "KMS";
        //            xlWorkSheet.Cells[4, 13] = "CENTRO DE VENTAS";
        //            xlWorkSheet.Cells[4, 14] = "CENTRO DE COSTO";
        //            xlWorkSheet.Cells[4, 15] = "KGS ORIGEN";
        //            xlWorkSheet.Cells[4, 16] = "KGS DESTINO";
        //            xlWorkSheet.Cells[4, 17] = "KGS MERMA";
        //            xlWorkSheet.Cells[4, 18] = "CANTIDAD AVES";
        //            xlWorkSheet.Cells[4, 19] = "REMISION-ORDEN";
        //            xlWorkSheet.Cells[4, 20] = "COSTO FLETE";
        //            xlWorkSheet.Cells[4, 21] = "FACTURA";
        //            xlWorkSheet.Cells[4, 22] = "TRANSPORTISTA";
        //            xlWorkSheet.Cells[4, 23] = "DÍA";
        //            xlWorkSheet.Cells[4, 24] = "HORA SALIDA";
        //            xlWorkSheet.Cells[4, 25] = "DÍA SALIDA COMPROMISO";
        //            xlWorkSheet.Cells[4, 26] = "HR SALIDA COMPROMISO";
        //            xlWorkSheet.Cells[4, 27] = "DÍA LLEGADA REAL";
        //            xlWorkSheet.Cells[4, 28] = "HORA LLEGADA REAL";
        //            xlWorkSheet.Cells[4, 29] = "DIA";
        //            xlWorkSheet.Cells[4, 30] = "HORA";
        //            xlWorkSheet.Cells[4, 31] = "DÍA SALIDA DESTINO";
        //            xlWorkSheet.Cells[4, 32] = "HR SALIDA DESTINO";
        //            xlWorkSheet.Cells[4, 33] = "DEMORA";
        //            xlWorkSheet.Cells[4, 34] = "OBSERVACIONES";
        //            xlWorkSheet.Cells[4, 35] = "TARIFA";
        //            //decimal sumaToneladas = 0m;
        //            //decimal sumaIva = 0m;
        //            //decimal sumaRetencion = 0m;
        //            //decimal sumaImporte = 0m;
        //            //decimal total = 0m;
        //            //decimal sumaKM = 0m;
        //            int i = 5;
        //            foreach (Viaje viaje in ListViajes)
        //            {
        //                AgruparCartaPortes list = new AgruparCartaPortes(viaje.listDetalles, true);
        //                var listNew = list._listaAgrupada;

        //                foreach (var detalles in listNew)
        //                {
        //                    try
        //                    {
        //                        xlWorkSheet.Cells[i, 1] = "P. Alim., Merida 1 Bachoco";
        //                        xlWorkSheet.Cells[i, 2] = detalles.grupoRemisiones.Trim().Trim(',');
        //                        xlWorkSheet.Cells[i, 3] = viaje.fechaBachoco == null ? viaje.FechaHoraViaje.ToString("dd/MM/yyyy") : Convert.ToDateTime(viaje.fechaBachoco).ToString("dd/MM/yyyy");
        //                        xlWorkSheet.Cells[i, 4] = "PLANTA DE ALIMENTOS (TRANSPORTES)";
        //                        xlWorkSheet.Cells[i, 5] = viaje.tractor.clave;
        //                        xlWorkSheet.Cells[i, 6] = viaje.remolque.clave;
        //                        xlWorkSheet.Cells[i, 7] = viaje.operador.nombre;
        //                        xlWorkSheet.Cells[i, 8] = isFullBachoco(viaje);
        //                        xlWorkSheet.Cells[i, 9] = "EXTERNO";
        //                        xlWorkSheet.Cells[i, 10] = "PLANTA DE ALIMENTOS 1";
        //                        xlWorkSheet.Cells[i, 11] = detalles.grupoZonas.Trim().Trim(','); ;
        //                        xlWorkSheet.Cells[i, 12] = detalles.zonaSelect.km;
        //                        xlWorkSheet.Cells[i, 13] = "";
        //                        xlWorkSheet.Cells[i, 14] = "463060";
        //                        xlWorkSheet.Cells[i, 15] = (detalles.volumenDescarga * 1000).ToString("N", new System.Globalization.CultureInfo("es-MX"));
        //                        xlWorkSheet.Cells[i, 16] = (detalles.volumenDescarga * 1000).ToString("N", new System.Globalization.CultureInfo("es-MX"));
        //                        xlWorkSheet.Cells[i, 17] = "0";
        //                        xlWorkSheet.Cells[i, 18] = "N/A";
        //                        xlWorkSheet.Cells[i, 19] = detalles.grupoRemisiones.Trim().Trim(',');
        //                        xlWorkSheet.Cells[i, 20] = detalles.grupoImportes.ToString("C3"); // (detalles.precio).ToString("N", new System.Globalization.CultureInfo("es-MX"));
        //                        xlWorkSheet.Cells[i, 21] = "";
        //                        xlWorkSheet.Cells[i, 22] = "FLETERA PEGASO";
        //                        xlWorkSheet.Cells[i, 23] = viaje.FechaHoraFin == null ? viaje.FechaHoraViaje.ToString("dd/MM/yyyy") : Convert.ToDateTime(viaje.fechaBachoco).ToString("dd/MM/yyyy");// Convert.ToDateTime(viaje.FechaHoraViaje).ToString("dd/MM/yyyy");
        //                        xlWorkSheet.Cells[i, 24] = viaje.FechaHoraFin == null ? viaje.FechaHoraViaje.ToString("HH:mm:00") : Convert.ToDateTime(viaje.fechaBachoco).ToString("HH:mm:00"); // Convert.ToDateTime(viaje.FechaHoraViaje).ToString("HH:mm:00");
        //                        xlWorkSheet.Cells[i, 25] = Convert.ToDateTime(viaje.FechaHoraViaje).ToString("dd/MM/yyyy");
        //                        xlWorkSheet.Cells[i, 26] = Convert.ToDateTime(viaje.FechaHoraViaje).ToString("HH:mm:00");
        //                        xlWorkSheet.Cells[i, 27] = "";// Convert.ToDateTime(viaje.FechaHoraFin).ToString("dd/MM/yyyy");
        //                        xlWorkSheet.Cells[i, 28] = "";//Convert.ToDateTime(viaje.FechaHoraFin).ToString("HH:mm:00");
        //                        xlWorkSheet.Cells[i, 29] = "";
        //                        xlWorkSheet.Cells[i, 30] = "";
        //                        xlWorkSheet.Cells[i, 31] = "";
        //                        xlWorkSheet.Cells[i, 32] = "";
        //                        xlWorkSheet.Cells[i, 33] = "";
        //                        xlWorkSheet.Cells[i, 34] = detalles.observaciones;
        //                        xlWorkSheet.Cells[i, 35] = "";

        //                        i++;
        //                    }
        //                    catch (Exception ex)
        //                    {
        //                        MessageBox.Show(ex.Message);
        //                        return false;
        //                    }
        //                }
        //            }

        //            //excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 10], xlWorkSheet.Cells[i, 35]];
        //            //excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Red);
        //            //excelCellrange.Font.FontStyle = "Bold";

        //            //excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[4, 1], xlWorkSheet.Cells[4, 35]];
        //            //excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Blue);
        //            //excelCellrange.Font.FontStyle = "Bold";
        //            //excelCellrange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Silver);

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[4, 1], xlWorkSheet.Cells[i - 1, 35]];
        //            excelCellrange.Font.Size = 8;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.Worksheet.ListObjects.AddEx(
        //                                                        SourceType: Excel.XlListObjectSourceType.xlSrcRange,
        //                                                        Source: excelCellrange,
        //                                                        XlListObjectHasHeaders: Excel.XlYesNoGuess.xlYes);
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.EntireColumn.AutoFit();

        //            xlWorkBook.SaveAs(archivo + ".xlsx");
        //            rutaExcel = archivo + ".xlsx";

        //            //bool resp = convertirExcelToSps(archivo);
        //            //if (resp)
        //            //{
        //            //    imprimirReporte(archivo + ".xps");
        //            //}
        //            xlWorkBook.Close();
        //            xlApp.Quit();
        //            ap.FolderBrowserDialog rutaDestino = new ap.FolderBrowserDialog();

        //            if (rutaDestino.ShowDialog() == ap.DialogResult.OK)
        //            {
        //                if (exportarExcelMensual(rutaDestino.SelectedPath + "\\" + fechaInicio.ToString("ddMMMM")))
        //                {
        //                    MessageBox.Show("Se exporto correctamente el reporte", "Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
        //                }
        //                return true;
        //            }
        //            else
        //            {
        //                return false;
        //            }

        //        }

        //    }
        //    catch (Exception e)
        //    {
        //        MessageBox.Show(e.Message);
        //        xlWorkBook.Close();
        //        xlApp.Quit();
        //        return false;
        //    }
        //    finally
        //    {
        //        //
        //        GC.WaitForPendingFinalizers();
        //        GC.Collect();
        //        GC.WaitForPendingFinalizers();
        //        GC.Collect();
        //    }
        //}
        //public bool documentoReporteViajes(List<Viaje> listViajes, DateTime fechaInicio, DateTime fechaFin, Cliente cliente)
        //{
        //    string archivo = "";
        //    try
        //    {
        //        string path = System.Windows.Forms.Application.StartupPath;
        //        if (!Directory.Exists(path + "\\reportes\\"))
        //        {
        //            DirectoryInfo dir = Directory.CreateDirectory(path + "\\reportes\\");
        //        }

        //        //archivo = string.Format(@"{0}{1}", path + "\\reportes\\", cartaPorte.NumGuiaId);
        //        archivo = string.Format(@"{0}{1}", path + "\\reportes\\", "reporte");

        //        if (File.Exists(archivo + ".xlsx"))
        //        {
        //            File.Delete(archivo + ".xlsx");
        //        }
        //        if (File.Exists(archivo + ".xps"))
        //        {
        //            File.Delete(archivo + ".xps");
        //        }
        //        //".xps"
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message);
        //        return false;
        //    }

        //    Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
        //    Excel.Workbook xlWorkBook = xlApp.Workbooks.Add();
        //    //Microsoft.Office.Interop.Excel.Worksheet excelSheet;
        //    Microsoft.Office.Interop.Excel.Range excelCellrange;
        //    try
        //    {

        //        if (xlApp == null)
        //        {
        //            MessageBox.Show("Excel is not properly installed!!");
        //            return false;
        //        }
        //        else
        //        {
        //            Excel.Worksheet xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
        //            xlWorkSheet.PageSetup.LeftMargin = 2;
        //            xlWorkSheet.PageSetup.RightMargin = 2;
        //            xlWorkSheet.PageSetup.TopMargin = 15;
        //            xlWorkSheet.PageSetup.BottomMargin = 5;
        //            xlWorkSheet.PageSetup.Orientation = Microsoft.Office.Interop.Excel.XlPageOrientation.xlLandscape;
        //            xlWorkSheet.Name = "Viajes";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[1, 10]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 8;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            xlWorkSheet.Cells[1, 1] = cliente.nombre;

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[2, 1], xlWorkSheet.Cells[2, 10]];
        //            excelCellrange.Merge(true);
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            xlWorkSheet.Cells[2, 1] = "Listado de Cobranza Fechas del " + fechaInicio.ToString("dd/MM/yyyy") + " al " + fechaFin.ToString("dd/MM/yyyy");

        //            //xlWorkSheet.Cells[3, 1] = "Folio";
        //            xlWorkSheet.Cells[3, 1] = "No.Viaje";
        //            xlWorkSheet.Cells[3, 2] = "Nombre";
        //            xlWorkSheet.Cells[3, 3] = "TC";
        //            xlWorkSheet.Cells[3, 4] = "Fecha Viaje";
        //            xlWorkSheet.Cells[3, 5] = "Fin Viaje";
        //            xlWorkSheet.Cells[3, 6] = "Tolva1";
        //            xlWorkSheet.Cells[3, 7] = "Tolva2";
        //            xlWorkSheet.Cells[3, 8] = "Destino";
        //            xlWorkSheet.Cells[3, 9] = "KM";
        //            xlWorkSheet.Cells[3, 10] = "Tons";
        //            //xlWorkSheet.Cells[3, 12] = "IVA";
        //            //xlWorkSheet.Cells[3, 13] = "Retención";
        //            //xlWorkSheet.Cells[3, 14] = "Importe";
        //            //xlWorkSheet.Cells[3, 15] = "Total";
        //            xlWorkSheet.Cells[3, 11] = "Tipo V";
        //            xlWorkSheet.Cells[3, 12] = "Zona";
        //            decimal sumaToneladas = 0m;
        //            decimal sumaIva = 0m;
        //            decimal sumaRetencion = 0m;
        //            decimal sumaImporte = 0m;
        //            decimal sumaKM = 0m;
        //            decimal total = 0m;
        //            int i = 4;
        //            foreach (Viaje viaje in listViajes)
        //            {
        //                foreach (CartaPorte cartaPorte in viaje.listDetalles)
        //                {
        //                    sumaToneladas += cartaPorte.volumenDescarga;
        //                    sumaIva += cartaPorte.ImporIVA;
        //                    sumaRetencion += cartaPorte.ImpRetencion;
        //                    sumaKM += cartaPorte.zonaSelect.km;
        //                    sumaImporte += cartaPorte.importeReal;
        //                    total += cartaPorte.importeReal + (cartaPorte.ImporIVA - cartaPorte.ImpRetencion);
        //                }
        //                //xlWorkSheet.Cells[i, 1] = viaje.NumGuiaId.ToString();
        //                xlWorkSheet.Cells[i, 1] = viaje.NumViaje.ToString();
        //                xlWorkSheet.Cells[i, 2] = viaje.operador == null ? "(ninguno)" : viaje.operador.nombre;
        //                xlWorkSheet.Cells[i, 3] = viaje.tractor.clave;
        //                xlWorkSheet.Cells[i, 4] = viaje.FechaHoraViaje.ToString("dd/MM/yyyy HH:mm:00");
        //                xlWorkSheet.Cells[i, 5] = viaje.FechaHoraFin == null ? "" : Convert.ToDateTime(viaje.FechaHoraFin).ToString("dd/MM/yyyy HH:mm:00");
        //                xlWorkSheet.Cells[i, 6] = viaje.remolque.clave;
        //                xlWorkSheet.Cells[i, 7] = viaje.remolque2 == null ? "" : viaje.remolque2.clave;
        //                xlWorkSheet.Cells[i, 10] = obtenerZona(viaje.listDetalles);
        //                xlWorkSheet.Cells[i, 11] = isFull(viaje);
        //                xlWorkSheet.Cells[i, 12] = obtenerZonaStr(viaje.listDetalles);
        //                foreach (var detalles in viaje.listDetalles)
        //                {
        //                    xlWorkSheet.Cells[i, 8] = detalles.zonaSelect.descripcion;

        //                    xlWorkSheet.Cells[i, 10] = detalles.volumenDescarga;
        //                    //xlWorkSheet.Cells[i, 12] = detalles.ImporIVA.ToString("C3");
        //                    //xlWorkSheet.Cells[i, 13] = detalles.ImpRetencion.ToString("C3");
        //                    //xlWorkSheet.Cells[i, 14] = detalles.importeReal.ToString("C3");
        //                    //xlWorkSheet.Cells[i, 15] = (detalles.importeReal + (detalles.ImporIVA - detalles.ImpRetencion)).ToString("C3");
        //                    i++;
        //                }

        //                sumaToneladas = 0m;
        //                sumaIva = 0m;
        //                sumaRetencion = 0m;
        //                sumaImporte = 0m;
        //                sumaKM = 0m;
        //                total = 0m;

        //            }

        //            foreach (Viaje viajes in listViajes)
        //            {
        //                foreach (CartaPorte cartaPorte in viajes.listDetalles)
        //                {
        //                    sumaToneladas += cartaPorte.volumenDescarga;
        //                    sumaIva += cartaPorte.ImporIVA;
        //                    sumaRetencion += cartaPorte.ImpRetencion;
        //                    //sumaKM += cartaPorte.zonaSelect.km;
        //                    sumaImporte += cartaPorte.importeReal;
        //                    total += cartaPorte.importeReal + (cartaPorte.ImporIVA - cartaPorte.ImpRetencion);
        //                }
        //            }
        //            //xlWorkSheet.Cells[i, 10] = sumaKM;

        //            //xlWorkSheet.Cells[i, 11] = sumaToneladas;
        //            //xlWorkSheet.Cells[i, 12] = sumaIva;
        //            //xlWorkSheet.Cells[i, 13] = sumaRetencion;
        //            //xlWorkSheet.Cells[i, 14] = sumaImporte;
        //            //xlWorkSheet.Cells[i, 15] = total;

        //            //excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[i, 10], xlWorkSheet.Cells[i, 17]];
        //            //excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Red);
        //            //excelCellrange.Font.FontStyle = "Bold";

        //            //excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[3, 1], xlWorkSheet.Cells[3, 12]];
        //            //excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Blue);
        //            //excelCellrange.Font.FontStyle = "Bold";

        //            //excelCellrange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Silver);

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[3, 1], xlWorkSheet.Cells[i - 1, 1]];
        //            excelCellrange.NumberFormat = "@";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[3, 1], xlWorkSheet.Cells[i - 1, 12]];
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.Worksheet.ListObjects.AddEx(
        //                                                        SourceType: Excel.XlListObjectSourceType.xlSrcRange,
        //                                                        Source: excelCellrange,
        //                                                        XlListObjectHasHeaders: Excel.XlYesNoGuess.xlYes);
        //            excelCellrange.Font.Size = 6;
        //            excelCellrange.EntireColumn.AutoFit();

        //            xlWorkBook.SaveAs(archivo + ".xlsx");
        //            rutaExcel = archivo + ".xlsx";

        //            bool resp = convertirExcelToSps(archivo);
        //            if (resp)
        //            {
        //                imprimirReporte(archivo + ".xps");
        //            }
        //            return resp;
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        MessageBox.Show(e.Message);
        //        return false;
        //    }
        //    finally
        //    {
        //        xlWorkBook.Close();
        //        xlApp.Quit();
        //        GC.WaitForPendingFinalizers();
        //        GC.Collect();
        //        GC.WaitForPendingFinalizers();
        //        GC.Collect();
        //    }
        //}

        //public bool documentoReporteViajesBachoco(List<Viaje> listViajes, DateTime fechaInicio, DateTime fechaFin, Cliente cliente)
        //{
        //    string archivo = "";
        //    try
        //    {
        //        string path = System.Windows.Forms.Application.StartupPath;
        //        if (!Directory.Exists(path + "\\reportes\\"))
        //        {
        //            DirectoryInfo dir = Directory.CreateDirectory(path + "\\reportes\\");
        //        }

        //        //archivo = string.Format(@"{0}{1}", path + "\\reportes\\", cartaPorte.NumGuiaId);
        //        archivo = string.Format(@"{0}{1}", path + "\\reportes\\", "reporte");

        //        if (File.Exists(archivo + ".xlsx"))
        //        {
        //            File.Delete(archivo + ".xlsx");
        //        }
        //        if (File.Exists(archivo + ".xps"))
        //        {
        //            File.Delete(archivo + ".xps");
        //        }
        //        //".xps"
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message);
        //        return false;
        //    }

        //    Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
        //    Excel.Workbook xlWorkBook = xlApp.Workbooks.Add();
        //    //Microsoft.Office.Interop.Excel.Worksheet excelSheet;
        //    Microsoft.Office.Interop.Excel.Range excelCellrange;
        //    try
        //    {

        //        if (xlApp == null)
        //        {
        //            MessageBox.Show("Excel is not properly installed!!");
        //            return false;
        //        }
        //        else
        //        {
        //            Excel.Worksheet xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
        //            xlWorkSheet.PageSetup.Orientation = Microsoft.Office.Interop.Excel.XlPageOrientation.xlLandscape;
        //            xlWorkSheet.PageSetup.LeftMargin = 2;
        //            xlWorkSheet.PageSetup.RightMargin = 2;
        //            xlWorkSheet.PageSetup.TopMargin = 15;
        //            xlWorkSheet.PageSetup.BottomMargin = 5;
        //            xlWorkSheet.Name = "CP";

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[1, 10]];
        //            excelCellrange.MergeCells = true;
        //            excelCellrange.Font.Size = 8;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            xlWorkSheet.Cells[1, 1] = cliente.nombre;

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[2, 1], xlWorkSheet.Cells[2, 10]];
        //            excelCellrange.MergeCells = true;
        //            excelCellrange.Font.Size = 7;
        //            excelCellrange.Font.FontStyle = "Bold";
        //            xlWorkSheet.Cells[2, 1] = "Listado de Cobranza Fechas del " + fechaInicio.ToString("dd/MM/yyyy") + " al " + fechaFin.ToString("dd/MM/yyyy");

        //            xlWorkSheet.Cells[3, 1] = "Fecha";
        //            xlWorkSheet.Cells[3, 2] = "Codigo";
        //            xlWorkSheet.Cells[3, 3] = "Alimento";
        //            xlWorkSheet.Cells[3, 4] = "Unidad";
        //            xlWorkSheet.Cells[3, 5] = "Remolque";
        //            xlWorkSheet.Cells[3, 6] = "Chofer";
        //            xlWorkSheet.Cells[3, 7] = "Granja";
        //            xlWorkSheet.Cells[3, 8] = "Folio Sistema";
        //            xlWorkSheet.Cells[3, 9] = "Bruto";
        //            xlWorkSheet.Cells[3, 10] = "Tara";
        //            xlWorkSheet.Cells[3, 11] = "Neto";
        //            xlWorkSheet.Cells[3, 12] = "Basculista";
        //            xlWorkSheet.Cells[3, 13] = "Tipo de Unidad";
        //            xlWorkSheet.Cells[3, 14] = "INCIDENCIAS DE BASCULA";
        //            int i = 4;
        //            List<List<object>> lista = new List<List<object>>();
        //            foreach (Viaje viaje in listViajes)
        //            {
        //                if (viaje.EstatusGuia != "CANCELADO")
        //                {
        //                    continue;
        //                }
        //                List<object> l = new List<object>();

        //                int inicio = i;
        //                int fin = i;
        //                l.Add(inicio);
        //                foreach (CartaPorte cartaPorte in viaje.listDetalles)
        //                {
        //                    if (cartaPorte.numGuiaId == 10611)
        //                    {

        //                    }
        //                    xlWorkSheet.Cells[i, 1] = viaje.FechaHoraViaje.ToString("dd-MMM");
        //                    xlWorkSheet.Cells[i, 2] = cartaPorte.producto.clave_externo;
        //                    xlWorkSheet.Cells[i, 3] = cartaPorte.producto.descripcion;
        //                    xlWorkSheet.Cells[i, 4] = viaje.tractor.clave;
        //                    xlWorkSheet.Cells[i, 5] = viaje.remolque.clave;
        //                    xlWorkSheet.Cells[i, 6] = viaje.operador.nombre;
        //                    xlWorkSheet.Cells[i, 7] = cartaPorte.zonaSelect.descripcion;
        //                    xlWorkSheet.Cells[i, 8] = cartaPorte.remision;
        //                    xlWorkSheet.Cells[i, 9] = Convert.ToDecimal((cartaPorte.pesoBruto * 1000)).ToString("N", new System.Globalization.CultureInfo("es-MX"));
        //                    xlWorkSheet.Cells[i, 10] = (cartaPorte.tara * 1000).ToString("N", new System.Globalization.CultureInfo("es-MX"));
        //                    xlWorkSheet.Cells[i, 11] = (cartaPorte.volumenDescarga * 1000).ToString("N", new System.Globalization.CultureInfo("es-MX"));
        //                    xlWorkSheet.Cells[i, 12] = "HBA";
        //                    //xlWorkSheet.Cells[i, 13] = validarIsFull(viaje) ? "Full" : "Sencillo";// viaje.tipoViaje;
        //                    xlWorkSheet.Cells[i, 14] = cartaPorte.observaciones;
        //                    i++;
        //                    fin = i;
        //                }
        //                l.Add(fin - 1);
        //                l.Add(validarIsFull(viaje) ? "Full" : "Sencillo");
        //                lista.Add(l);

        //            }

        //            foreach (var item in lista)
        //            {
        //                excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[item[0], 13], xlWorkSheet.Cells[item[1], 13]];
        //                excelCellrange.MergeCells = true;
        //                xlWorkSheet.Cells[item[0], 13] = item[2];
        //            }

        //            //excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[3, 1], xlWorkSheet.Cells[3, 14]];
        //            //excelCellrange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Blue);
        //            //excelCellrange.Font.FontStyle = "Bold";

        //            //excelCellrange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Silver);

        //            excelCellrange = xlWorkSheet.Range[xlWorkSheet.Cells[3, 1], xlWorkSheet.Cells[i - 1, 14]];
        //            excelCellrange.Font.Size = 6;
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.Worksheet.ListObjects.AddEx(
        //                                                        SourceType: Excel.XlListObjectSourceType.xlSrcRange,
        //                                                        Source: excelCellrange,
        //                                                        XlListObjectHasHeaders: Excel.XlYesNoGuess.xlYes);
        //            excelCellrange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //            excelCellrange.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
        //            excelCellrange.EntireColumn.AutoFit();

        //            xlWorkBook.SaveAs(archivo + ".xlsx");
        //            rutaExcel = archivo + ".xlsx";

        //            bool resp = convertirExcelToSps(archivo);
        //            if (resp)
        //            {
        //                imprimirReporte(archivo + ".xps");
        //            }
        //            return resp;
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        MessageBox.Show(e.Message);
        //        return false;
        //    }
        //    finally
        //    {
        //        xlWorkBook.Close();
        //        xlApp.Quit();
        //        GC.WaitForPendingFinalizers();
        //        GC.Collect();
        //        GC.WaitForPendingFinalizers();
        //        GC.Collect();
        //    }
        //}

        //private bool validarIsFull(Viaje cartaPorte)
        //{
        //    if (cartaPorte.remolque.tipoUnidad.descripcion == "TOLVA 35 TON")
        //    {
        //        return true;
        //    }
        //    if (cartaPorte.dolly != null || cartaPorte.remolque2 != null)
        //    {
        //        return true;
        //    }
        //    return false;
        //}

        public bool convertirExcelToSps(string archivo)
        {
            var excelApp = new Microsoft.Office.Interop.Excel.Application();
            excelApp.DisplayAlerts = false;

            excelApp.Workbooks.Add(archivo + ".xlsx");
            Microsoft.Office.Interop.Excel.Workbook wb = excelApp.ActiveWorkbook;

            try
            {

                wb.ExportAsFixedFormat(Excel.XlFixedFormatType.xlTypeXPS, archivo);

                return true;
            }
            catch (Exception e)
            {
                MessageBox.Show("Exception: " + e.Message);
                return false;
            }
            finally
            {
                wb.Close();
                excelApp.Quit();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
        }

        private void btnEXCEL_Click(object sender, RoutedEventArgs e)
        {
            ap.SaveFileDialog rutaDestino = new ap.SaveFileDialog();
            rutaDestino.Title = "Guardar Como";
            rutaDestino.Filter = "Libro de excel(*.xlsx)|*.xlsx";
            if (rutaDestino.ShowDialog() == ap.DialogResult.OK)
            {
                if (exportarExcel(rutaDestino.FileName))
                {
                    MessageBox.Show("Se exporto correctamente el reporte", "Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
        }

        public bool exportarExcel(string pathNew)
        {
            string path = rutaExcel;

            Excel.Application oXL = new Microsoft.Office.Interop.Excel.Application();
            Excel.Workbook mWorkBook = oXL.Workbooks.Add();
            try
            {
                mWorkBook = oXL.Workbooks.Open(path, 0, false, 5, "", "", false, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "", true, false, 0, true, false, false);
                mWorkBook.SaveAs(pathNew);
                //mWorkBook.Visible = true;
                mWorkBook.Close();
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
            finally
            {
                mWorkBook = null;
                oXL.Quit();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
        }

        //public bool exportarExcelMensual(string pathNew)
        //{
        //    string path = rutaExcel;

        //    Excel.Application oXL = new Microsoft.Office.Interop.Excel.Application();
        //    Excel.Workbook mWorkBook = oXL.Workbooks.Add();
        //    try
        //    {
        //        mWorkBook = oXL.Workbooks.Open(path, 0, false, 5, "", "", false, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "", true, false, 0, true, false, false);
        //        mWorkBook.SaveAs(pathNew + ".xlsx");
        //        //mWorkBook.Visible = true;
        //        mWorkBook.Close();
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message);
        //        return false;
        //    }
        //    finally
        //    {
        //        //mWorkBook.Close();
        //        mWorkBook = null;
        //        oXL.Quit();
        //        GC.WaitForPendingFinalizers();
        //        GC.Collect();
        //        GC.WaitForPendingFinalizers();
        //        GC.Collect();
        //    }
        //}

        private void btnPDF_Click(object sender, RoutedEventArgs e)
        {
            //ap.FolderBrowserDialog rutaDestino = new ap.FolderBrowserDialog();
            ap.SaveFileDialog rutaDestino = new ap.SaveFileDialog();
            rutaDestino.Title = "Guardar Como";
            rutaDestino.Filter = "Libro de PDF(*.pdf)|*.pdf";

            if (rutaDestino.ShowDialog() == ap.DialogResult.OK)
            {
                if (exportarExcelToPDF(rutaDestino.FileName))
                {
                    MessageBox.Show("Se exporto correctamente el reporte", "Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
        }

        private bool exportarExcelToPDF(string selectedPath)
        {
            string path = rutaExcel;
            Excel.Application oXL = new Microsoft.Office.Interop.Excel.Application();
            Excel.Workbook mWorkBook = oXL.Workbooks.Add();
            oXL = new Microsoft.Office.Interop.Excel.Application();
            try
            {
                mWorkBook = oXL.Workbooks.Open(rutaExcel, 0, false, 5, "", "", false, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "", true, false, 0, true, false, false);
                mWorkBook.ExportAsFixedFormat(Microsoft.Office.Interop.Excel.XlFixedFormatType.xlTypePDF, selectedPath);
                //mWorkBook.Visible = true;

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
            finally
            {
                mWorkBook.Close();
                //mWorkBook = null;
                oXL.Quit();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
        }
    }
}
