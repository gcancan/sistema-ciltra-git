﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
//using Core.Models;
using Core.Interfaces;
using Core.Models;

namespace WpfClient.Modulos_Mantenimiento
{
    /// <summary>
    /// Interaction logic for Diagnosticos.xaml
    /// </summary>
    public partial class DiagnosticosV1 : ViewBase
    {
        //List<Personal> lll = new List<Personal>();
        public DiagnosticosV1()
        {
            InitializeComponent();
        }

        private void btnBuscarRecepciones_Click(object sender, RoutedEventArgs e)
        {
            buscarRecepciones();
        }
        private void txtRecepcionFolio_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                buscarRecepciones();
            }            
        }
        private void buscarRecepciones()
        {
            selectRecepcion select = new selectRecepcion(txtRecepcionFolio.Text.Trim());
            var recepcion = select.findAllRecepciones(0);
            if (recepcion != null)
            {
                mapFormRecepcioner(recepcion);
            }
        }

        private void mapFormRecepcioner(Recepcion recepcion)
        {
            if (recepcion != null)
            {
                txtRecepcionFolio.Tag = recepcion;
                txtRecepcionFolio.Text = recepcion.idTipoOrden.ToString();
                txtRecepcionTipoOrden.Text = recepcion.tipoOrden.nomTipoOrden;
                txtRecepcionFecha.Text = recepcion.FechaRecepcion.ToString();
                txtRecepcionEstatus.Text = recepcion.Estatus == "REC" ? "RECEPCION" : "DIAGNOSTICO";

                txtRecepcionClaveUnidadTransporte.Text = recepcion.id_UnidadTrans;
                txtRecepcionTipoUnidadTransporte.Text = recepcion.unidadTransporte.tipoUnidad.nomTipoUniTrans;
                txtRecepcionMarcaUnidadTransporte.Text = recepcion.unidadTransporte.marca.nombreMarca;
                txtRecepcionEmpresa.Text = recepcion.empresa.razonSocial;
            }
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            mapServicios();
            mapActividades();
            chkTodos.IsChecked = true;
            //var r = new PersonalSvc().findAllPersonal();
            //lll = (List<Personal>)r.result;

            base.nuevo();
        }       

        private void mapServicios()
        {
            try
            {
                OperationResult servicio = new ServicioSvc().findServicio();
                if (servicio.typeResult == ResultTypes.error)
                {
                    MessageBox.Show(servicio.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                if (servicio.typeResult == ResultTypes.warning)
                {
                    MessageBox.Show(servicio.mensaje, "Advertencia", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                if (servicio.typeResult == ResultTypes.recordNotFound)
                {
                    MessageBox.Show(servicio.mensaje, "Advertencia", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                if (servicio.typeResult == ResultTypes.success)
                {
                    List<Servicio> list = (List<Servicio>)servicio.result;
                    foreach (var item in list)
                    {
                        cbxServicios.Items.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void mapActividades()
        {
            try
            {
                OperationResult servicio = new ServicioSvc().findDetallesServicio(0); //el parametro 0 trae todos las actividades
                if (servicio.typeResult == ResultTypes.error)
                {
                    MessageBox.Show(servicio.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                if (servicio.typeResult == ResultTypes.warning)
                {
                    MessageBox.Show(servicio.mensaje, "Advertencia", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                if (servicio.typeResult == ResultTypes.recordNotFound)
                {
                    MessageBox.Show(servicio.mensaje, "Advertencia", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                if (servicio.typeResult == ResultTypes.success)
                {
                    List<Actividad> list = (List<Actividad>)servicio.result;
                    foreach (var item in list)
                    {
                        cbxActividades.Items.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }         

        private bool existeList(Actividad actividad)
        {
            if (lvlActividades.Items.Count < 1)
            {
                return false;
            }
            else
            {
                foreach (ListViewItem item in lvlActividades.Items)
                {
                    if (((Actividad)item.Content).idActividad == actividad.idActividad)
                    {
                        MessageBox.Show(string.Format("La actividad {0} ya existe en la lista.", actividad.nombreAct), "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return true;
                    }
                }
                return false;
            }
        }

        private void addlvlActividades(Actividad actividad)
        {
            ListViewItem lvl = new ListViewItem();
            //actividad.listPersonal = lll;
            lvl.Content = actividad;
            lvl.Selected += Lvl_Selected;
            lvl.MouseDoubleClick += Lvl_MouseDoubleClick;
            lvlActividades.Items.Add(lvl);

            if (chkTodos.IsChecked.Value)
            {
                motrarTodosProductos();
            }
            //foreach (var item in actividad.listProductos)
            //{
            //    ListViewItem lvlp = new ListViewItem();
            //    lvlp.Content = item;
            //    //lvlp.MouseDoubleClick += Lvl_MouseDoubleClick;
            //    lvlProductos.Items.Add(lvlp);
            //}
        }

        private void Lvl_Selected(object sender, RoutedEventArgs e)
        {
            if (sender !=  null)
            {
                if (chkTodos.IsChecked==false)
                {
                    mostrarPorActividad();
                }
                
            }
        }

        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            
            lvlProductos.Items.Clear();
            if (((Actividad)((ListViewItem)lvlActividades.SelectedItem).Content).idServicio != 0)
            {
                List<ListViewItem> lvl = new List<ListViewItem>();
                foreach (ListViewItem item in lvlActividades.Items)
                {
                    Actividad actividad = (Actividad)((ListViewItem)item).Content;
                    if (actividad.idServicio == ((Actividad)((ListViewItem)lvlActividades.SelectedItem).Content).idServicio)
                    {
                        lvl.Add(item);
                    }
                }
                foreach (var item in lvl)
                {
                    lvlActividades.Items.Remove(item);
                }
            }
            else
            {
                lvlActividades.Items.Remove(sender);
            }
            if (chkTodos.IsChecked.Value)
            {
                //List<ListViewItem> lvl = new List<ListViewItem>();
                //foreach (ListViewItem item in lvlProductos.Items)
                //{
                //    Producto producto = (Producto)((ListViewItem)item).Content;
                //    if (producto.idActividad == ((Actividad)((ListViewItem)lvlActividades.SelectedItem).Content).idActividad)
                //    {
                //        lvl.Add(item);
                //    }
                //}
                //foreach (var item in lvl)
                //{
                //    lvlProductos.Items.Remove(item);
                //}
                
                motrarTodosProductos();
            }
            
        }

               
        private Personal buscarPersonas()
        {
            try
            {
                var select = new selectPersonal().findPersonal();
                if (select != null)
                {
                    return(select);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }       

        private void btnBuscarPersona_Click(object sender, RoutedEventArgs e)
        {
            Personal personal = buscarPersonas();
            if (personal != null)
            {
                txtPersonal.Tag = personal;
                txtPersonal.Text = personal.nombreCompleto;
            }
        }

        private void btnAddServicioActividad_Click(object sender, RoutedEventArgs e)
        {
            if (rbnActividades.IsChecked.Value)
            {
                if (cbxActividades.SelectedItem != null)
                {
                    var item = ((Actividad)cbxActividades.SelectedItem);
                    if (!existeList(item))
                    {
                        if (txtPersonal.Tag != null)
                        {
                            item.idServicio = 0;
                            item.nombreServicio = "SERV GRAL";
                            item.personalAsignado = (Personal)txtPersonal.Tag;
                            addlvlActividades(item);
                            txtPersonal.Tag = null;
                            txtPersonal.Clear();
                            cbxActividades.SelectedItem = null;
                        }
                        else
                        {
                            MessageBox.Show("Se requiere elegir el personal para asignar esta actividad", "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                        }
                    }
                }
            }
            else
            {
                if (cbxServicios.SelectedItem != null)
                {
                    var listServicios = ((Servicio)cbxServicios.SelectedItem).listActividades;
                    foreach (var item in listServicios)
                    {
                        if (!existeList(item))
                        {
                            if (txtPersonal.Tag != null)
                            {
                                item.nombreServicio = ((Servicio)cbxServicios.SelectedItem).NomServicio;
                                item.idServicio = (int)((Servicio)cbxServicios.SelectedItem).idServicio;
                                item.personalAsignado = (Personal)txtPersonal.Tag;
                                addlvlActividades(item);
                                
                            }
                            else
                            {
                                MessageBox.Show("Se requiere elegir el personal para asignar esta actividad", "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                                return;
                            }

                        }
                    }
                    txtPersonal.Tag = null;
                    txtPersonal.Clear();
                    cbxServicios.SelectedItem = null;
                }
            }
            sumar();
        }

        private void sumar()
        {
            decimal sumCantidad = 0.00m;
            decimal sumAlmacen = 0.00m;
            decimal sumCompra = 0.00m;
            List<Actividad> listActividad = new List<Actividad>();
            foreach (ListViewItem item in lvlActividades.Items)
            {
                listActividad.Add((Actividad)item.Content);
            }
            foreach (Actividad item in listActividad)
            {
                foreach (Producto pro in item.listProductos)
                {
                    sumCantidad += pro.cantidad;
                    sumAlmacen += pro.cantidadOrdenAlmacen;
                    sumCompra += pro.cantidadOrdenCompra;
                }
            }
            txtsumCantidad.Text = sumCantidad.ToString();
            txtsumOrdenAlmacen.Text = sumAlmacen.ToString();
            txtsumOrdenCompra.Text = sumCompra.ToString();
        }

        private void chkTodos_Checked(object sender, RoutedEventArgs e)
        {
            motrarTodosProductos(); 
        }

        private void motrarTodosProductos()
        {
            if (lvlActividades.Items.Count > 0)
            {
                lvlProductos.Items.Clear();
                List<Actividad> listActividad = new List<Actividad>();
                foreach (ListViewItem item in lvlActividades.Items)
                {
                    listActividad.Add((Actividad)item.Content);
                }
                foreach (Actividad item in listActividad)
                {
                    foreach (Producto pro in item.listProductos)
                    {
                        ListViewItem lvl = new ListViewItem();
                        lvl.Content = pro;
                        lvlProductos.Items.Add(pro);
                    }
                }
            }
        }

        public void mostrarPorActividad()
        {
            lvlProductos.Items.Clear();
            if (lvlActividades.SelectedItem != null)
            {
                var item = (Actividad)((ListViewItem)lvlActividades.SelectedItem).Content;
                foreach (Producto pro in item.listProductos)
                {
                    ListViewItem lvl = new ListViewItem();
                    lvl.Content = pro;
                    lvlProductos.Items.Add(pro);
                }
            }
        }

        private void chkTodos_Unchecked(object sender, RoutedEventArgs e)
        {
            mostrarPorActividad();
        }

        public override OperationResult guardar()
        {
            List<Diagnostico> listDiagnostico = mapForm();
            if (listDiagnostico != null)
            {
                var resp = new DiagnosticoSvc().saveRecepcion(listDiagnostico);
                return resp;
            }
            else
            {
                return new OperationResult { valor = 1, mensaje= "Error al construir la informacion del dianostico"};
            }
            //return base.guardar();
        }

        private List<Diagnostico> mapForm()
        {
            if (lvlActividades.Items.Count  < 1)
            {
                return null;
            }
            List<Diagnostico> listDiagnostico = new List<Diagnostico>();
            try
            {
                foreach (ListViewItem item in lvlActividades.Items)
                {
                    Actividad actividad = (Actividad)item.Content;
                    listDiagnostico.Add(new Diagnostico
                    {
                        recepcion = (Recepcion)(txtRecepcionFolio.Tag),
                        idServicio = actividad.idServicio,
                        idOrdenActividad = txtRecepcionFolio.Tag == null ? 0 : ((Recepcion)(txtRecepcionFolio.Tag)).idOrdenSer,
                        actividad = actividad,
                        CostoProductos = mapCostos(actividad),
                        Estatus = "DIA",
                        FechaDiag = DateTime.Now,
                        FechaAsig = DateTime.Now,
                        FechaPausado = DateTime.Now,
                        FechaTerminado = DateTime.Now,
                        id_proveedor = 0,
                        UsuarioAsig = actividad.personalAsignado.idPersonal.ToString(),
                        UsuarioDiag = mainWindow.usuario.sUserId,
                        NoPersonal = 0,
                        NotaPausado = "",
                        UsuarioTerminado = ""
                    });                    
                }
                return listDiagnostico;
            }
            catch (Exception)
            {

                return null;
            }
        }

        private decimal mapCostos(Actividad actividad)
        {
            decimal suma = 0.0m;
            foreach (var item in actividad.listProductos)
            {
                suma += (item.costo * item.cantidadOrdenAlmacen);
            }
            return suma;
        }
    }
}
