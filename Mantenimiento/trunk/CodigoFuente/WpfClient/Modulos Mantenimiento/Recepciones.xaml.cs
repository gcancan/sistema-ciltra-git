﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Interfaces;
//using Core.Models;
using Microsoft.VisualBasic;
using Core.Models;

namespace WpfClient.Modulos_Mantenimiento
{
    /// <summary>
    /// Interaction logic for Recepciones.xaml
    /// </summary>
    public partial class Recepciones : ViewBase
    {
        List<DetalleRecepcion> listBaja = new List<DetalleRecepcion>();
        DetalleRecepcion editDetalle = new DetalleRecepcion();
        public Recepciones()
        {
            InitializeComponent();
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
           
            txtRecive.Text = mainWindow.usuario.sNomUser;
            txtRecive.Tag = mainWindow.usuario;
            //getAllFamilias(); /// se comenta ya que no sera el filto por familias sera por las diviciones.
            getAllDiviciones();
            getAllTiposOrden();
            nuevo();

            if (!mainWindow.esTaller)
            {
                stpEntrega.Visibility = Visibility.Collapsed;
                mainWindow.Title = "Solicitud de Orden de Servicio.";
                //stpKilometros.Visibility = Visibility.Collapsed;
            }
            else
            {
                mainWindow.Title = "Recepción de Orden de Servicio.";
            }

            if (mainWindow.clave != 0)
            {
                txtID.Text = mainWindow.clave.ToString();
                buscarRecepcion();
            }
        }

        private void getAllDiviciones()
        {
            try
            {
                var resp = new DivicionSvc().getAllDiviciones();
                List<Divicion> list = (List<Divicion>)resp.result;
                if (resp.typeResult == ResultTypes.error)
                {
                    MessageBox.Show(string.Format(resp.mensaje), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                if (resp.typeResult == ResultTypes.recordNotFound)
                {
                    MessageBox.Show(string.Format(resp.mensaje), "Advertencia", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                if (resp.typeResult == ResultTypes.success)
                {
                    mapDiviciones((List<Divicion>)resp.result);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format(ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void mapDiviciones(List<Divicion> result)
        {
            foreach (Divicion item in result)
            {
                cbxTipoFamilia.Items.Add(item);
            }
        }

        private void getAllTiposOrden()
        {
            try
            {
                OperationResult resp = new TipoOrdenSvc().getAllTiposOrden();
                if (resp.typeResult == ResultTypes.error)
                {
                    MessageBox.Show(string.Format(resp.mensaje), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                if (resp.typeResult == ResultTypes.recordNotFound)
                {
                    MessageBox.Show(string.Format(resp.mensaje), "Advertencia", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                if (resp.typeResult == ResultTypes.success)
                {
                    mapTiposOrden((List<TipoOrden>)resp.result);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format(ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void mapTiposOrden(List<TipoOrden> list)
        {
            foreach (TipoOrden item in list)
            {
                cbxTipoOrden.Items.Add(item);
            }
        }

        private void getAllFamilias()
        {
            try
            {
                OperationResult resp = new FamiliaSvc().selectAllFamilias();
                if (resp.typeResult == ResultTypes.error)
                {
                    MessageBox.Show(string.Format(resp.mensaje), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                if (resp.typeResult == ResultTypes.recordNotFound)
                {
                    MessageBox.Show(string.Format(resp.mensaje), "Advertencia", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                if (resp.typeResult == ResultTypes.success)
                {
                    mapFamilia((List<Familia>)resp.result);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format(ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void mapFamilia(List<Familia> list)
        {
            foreach (Familia item in list)
            {
                cbxTipoFamilia.Items.Add(item);
            }
        }

        public override void nuevo()
        {

            //return;

            mapFormComplete(null);
            cbxTipoOrden.SelectedItem = null;
            {
                //gBoxUnidad.IsEnabled = true;
                //gBoxPersonal.IsEnabled = false;
                //gBoxSintomas.IsEnabled = false;
            }
            cbxTipoOrden.SelectedIndex = 1;
            base.nuevo();
            txtfecha.Text = DateTime.Now.ToShortDateString();
            txtUnidadTransporte.Focus();

        }

        private void btnBuscarUnidadTransporte_Click(object sender, RoutedEventArgs e)
        {
            buscarUnidadesTransporte();
        }
        private void txtUnidadTransporte_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.F3)
            {
                buscarUnidadesTransporte();
            }
            if (e.Key == Key.Enter)
            {
                finUnudadById();
            }
        }

        private void finUnudadById()
        {
            var resp = new UnidadTransporteSvc().findAllUnidadesTransporteByIdAndIdEmpresa(txtUnidadTransporte.Text.Trim(), mainWindow.usuario.personal.idEmpresa.ToString());
            if (resp.typeResult == ResultTypes.error)
            {
                MessageBox.Show(string.Format(resp.mensaje), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            if (resp.typeResult == ResultTypes.recordNotFound)
            {
                MessageBox.Show(string.Format(resp.mensaje), "Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            if (resp.typeResult == ResultTypes.warning)
            {
                MessageBox.Show(string.Format(resp.mensaje), "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            if (resp.typeResult == ResultTypes.success)
            {
                List<UnidadTransporte> listUnidad = (List<UnidadTransporte>)resp.result;
                if (listUnidad.Count == 1)
                {
                    mapForm(listUnidad[0]);
                }
                else
                {
                    buscarUnidadesTransporte();
                }
            }
        }

        private void buscarUnidadesTransporte()
        {
            try
            {
                SelectUnidadTransporte select = new SelectUnidadTransporte(txtUnidadTransporte.Text.Trim(), mainWindow.usuario.personal.idEmpresa.ToString());
                var unidadTransporte = select.findAllUnidadesTransporte();
                if (unidadTransporte != null)
                {
                    mapForm(unidadTransporte);
                }
                else
                {
                    txtUnidadTransporte.Clear();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mapForm(UnidadTransporte unidadTransporte)
        {
            if (unidadTransporte != null)
            {

                txtUnidadTransporte.Tag = unidadTransporte;
                txtUnidadTransporte.Text = unidadTransporte.id_UnidadTrans;
                //gBoxUnidad.IsEnabled = false;
                txtTipoUnidad.Text = unidadTransporte.descripcionUni;
                txtMarca.Text = unidadTransporte.marca.nombreMarca;
                //btnBuscarUnidadTransporte.IsEnabled = false;

                txtEmpresa.Tag = unidadTransporte.empresa;
                txtEmpresa.Text = unidadTransporte.empresa.razonSocial;
                //txtEmpresaDireccion.Text = unidadTransporte.empresa.direccion;

                //gBoxPersonal.IsEnabled = true;

                if (mainWindow.esTaller)
                {
                    txtEntrega.Focus();
                }
                else
                {
                    txtAutoriza.Focus();
                }                
            }
            else
            {
                txtUnidadTransporte.Tag = null;
                txtUnidadTransporte.Clear();
                //txtUnidadTransporte.IsEnabled = true;
                txtTipoUnidad.Clear();
                txtMarca.Clear();
                //btnBuscarUnidadTransporte.IsEnabled = true;

                txtEmpresa.Tag = null;
                txtEmpresa.Clear();
                //txtEmpresaDireccion.Clear();
            }

        }

        private void btnBuscarPersona_Click(object sender, RoutedEventArgs e)
        {
            buscarPersonal();
        }
        private void buscarPersonal()
        {
            try
            {
                var select = new selectPersonal().findPersonal();
                if (select != null)
                {
                    mapForm(select);
                }
                else
                {
                    txtEntrega.Clear();
                    txtEntrega.Tag = null;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnBuscarPersonaAutoriza_Click(object sender, RoutedEventArgs e)
        {
            buscarPersonaAutiza();
        }

        private void buscarPersonaAutiza()
        {
            try
            {
                var select = new selectPersonal().findPersonal();
                if (select != null)
                {
                    mapFormAutoriza(select);
                }
                else
                {
                    txtAutoriza.Clear();
                    txtAutoriza.Tag = null;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mapFormAutoriza(Personal select)
        {
            if (select != null)
            {
                txtAutoriza.Tag = select;
                txtAutoriza.Text = select.nombreCompleto;
                txtKilometraje.Focus();
            }
            else
            {
                txtAutoriza.Tag = null;
                txtAutoriza.Clear();

            }
        }

        private void mapForm(Personal select)
        {
            if (select != null)
            {
                txtEntrega.Tag = select;
                txtEntrega.Text = select.nombreCompleto;
                txtAutoriza.Focus();

            }
            else
            {
                txtEntrega.Tag = null;
                txtEntrega.Clear();
                //txtEntrega.IsEnabled = true;
            }

        }

        private void btn_add_Click(object sender, RoutedEventArgs e)
        {
            addList();
            txtDescripcion.Focus();
        }
        private void cbxTipoOrden_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                addList();
            }

        }
        private void addList()
        {
            if (lvlSintomas.SelectedItem == null)
            {
                if (!string.IsNullOrEmpty(txtDescripcion.Text.Trim()) && cbxTipoFamilia.SelectedItem != null)
                {
                    ListViewItem lvl = new ListViewItem();
                    lvl.MouseDoubleClick += Lvl_MouseDoubleClick;
                    lvl.Selected += Lvl_Selected;
                    lvl.Content = new DetalleRecepcion
                    {
                        descripcion = txtDescripcion.Text.Trim(),
                        divicion = ((Divicion)cbxTipoFamilia.SelectedItem),
                        isCancelado = false,
                        motivoCancelacion = ""
                    };
                    if (!validarExixte(lvl)) { lvlSintomas.Items.Add(lvl); }
                    txtDescripcion.Clear();
                    cbxTipoFamilia.SelectedItem = null;
                    txtDescripcion.Focus();
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(txtDescripcion.Text.Trim()) && cbxTipoFamilia.SelectedItem != null)
                {
                    ListViewItem lvl = new ListViewItem();
                    lvl.MouseDoubleClick += Lvl_MouseDoubleClick;
                    lvl.Selected += Lvl_Selected;
                    editDetalle.descripcion = txtDescripcion.Text.Trim();
                    editDetalle.divicion = (Divicion)cbxTipoFamilia.SelectedItem;
                    lvl.Content = editDetalle;
                    if (!validarExixte(lvl))
                    {
                        lvlSintomas.Items[lvlSintomas.SelectedIndex] = lvl;
                    }
                    txtDescripcion.Clear();
                    cbxTipoFamilia.SelectedItem = null;
                    txtDescripcion.Focus();
                }
            }
        }

        private void Lvl_Selected(object sender, RoutedEventArgs e)
        {
            if (sender != null)
            {
                editDetalle = ((DetalleRecepcion)((ListViewItem)sender).Content);
                txtDescripcion.Text = editDetalle.descripcion;

                int i = 0;
                foreach (Divicion item in cbxTipoFamilia.Items)
                {
                    if (editDetalle.idDivicion == item.idDivicion)
                    {
                        cbxTipoFamilia.SelectedIndex = i;
                        break;
                    }
                    else
                    {
                        i++;
                    }
                }

            }
        }

        private bool validarExixte(ListViewItem lvl)
        {
            if (lvlSintomas.Items.Count <= 0)
            {
                return false;
            }
            else
            {
                foreach (ListViewItem item in lvlSintomas.Items)
                {
                    if (((DetalleRecepcion)lvl.Content).descripcion == ((DetalleRecepcion)item.Content).descripcion)
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            quitarLista();
        }

        private void btn_delete_Click(object sender, RoutedEventArgs e)
        {
            if (lvlSintomas.SelectedItem != null)
            {
                quitarLista();
            }
        }
        public void quitarLista()
        {
            if (txtID.Tag != null)
            {
                string motivo = Interaction.InputBox("Escribir el motivo de cancelación", "Motivo de Baja");
                if (!string.IsNullOrEmpty(motivo.Trim()))
                {
                    DetalleRecepcion det = (DetalleRecepcion)((ListViewItem)lvlSintomas.SelectedItem).Content;
                    listBaja.Add(new DetalleRecepcion
                    {
                        idDetalleRecepcion = det.idDetalleRecepcion,
                        divicion = det.divicion,
                        isCancelado = true,
                        motivoCancelacion = motivo,
                        descripcion = det.descripcion
                    });
                    lvlSintomas.Items.Remove(lvlSintomas.SelectedItem);
                }
            }
            else
            {
                lvlSintomas.Items.Remove(lvlSintomas.SelectedItem);
            }
        }

        public override void imprimir()
        {
            try
            {
                Cursor = Cursors.Wait;
                impirimirDocumento();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
            
        }

        private void impirimirDocumento()
        {
            try
            {
                var recepcion = (Recepcion)txtID.Tag;
                ReportView rv = new ReportView();
                //rv.crearArchivo(TipoDocumento.OREDEN_SERVICIO_RECEPCION, recepcion);
                rv.ShowDialog();
                rv.Close();
                base.imprimir();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public override OperationResult guardar()
        {
            try
            {
                Cursor = Cursors.Wait;
                if (!validar())
                {
                    return new OperationResult { mensaje = "Falatan valores por capturar", valor = 2 };
                }
                Recepcion recepcion = mapForm();
                if (recepcion != null)
                {
                    OperationResult result = new RecepcionSvc().saveRecepcion(recepcion);
                    if (result.typeResult == ResultTypes.success)
                    {
                        mapFormComplete((Recepcion)result.result);
                        impirimirDocumento();
                    }
                    return result;
                }
                else
                {
                    return new OperationResult { valor = 1, mensaje = "Error al construir el objeto", result = null };
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message, result = null };
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
            //return new OperationResult();
        }

        private bool validar()
        {
            if (mainWindow.esTaller)
            {
                if (txtEntrega.Tag == null)
                {
                    return false;
                }
                
                if (txtAutoriza.Tag == null)
                {
                    return false;
                }

            }
            if (txtKilometraje.Text.Trim() == "")
            {
                return false;
            }
            if (txtUnidadTransporte.Tag == null)
            {
                return false;
            }
            if (lvlSintomas.Items.Count < 1)
            {
                return false;
            }
            if (cbxTipoOrden.SelectedItem == null)
            {
                return false;
            }
            return true;
        }

        private void mapFormComplete(Recepcion recepcion)
        {
            listBaja = new List<DetalleRecepcion>();
            if (recepcion != null)
            {
                txtID.Tag = recepcion;
                txtID.Text = recepcion.idOrdenSer.ToString();
                mapForm(recepcion.unidadTransporte);
                mapForm(recepcion.personal);
                mapForm(recepcion.detalles);
                mapFormAutoriza(recepcion.personalAutoriza);
                txtKilometraje.Text = recepcion.Kilometraje.ToString();
                //cbxTipoOrden.SelectedItem = recepcion.tipoOrden;

                int i = 0;
                foreach (var item in cbxTipoOrden.Items)
                {
                    if (((TipoOrden)item).nomTipoOrden == recepcion.tipoOrden.nomTipoOrden)
                    {
                        cbxTipoOrden.SelectedIndex = i;
                        break;
                    }
                    else
                    {
                        i++;
                    }
                }
            }
            else
            {
                txtID.Tag = null;
                txtID.Clear();
                txtKilometraje.Clear();
                cbxTipoOrden.SelectedItem = null;
                UnidadTransporte t = null;
                Personal p = null;
                List<DetalleRecepcion> d = null;
                mapForm(t);
                mapForm(p);
                mapForm(d);
                mapFormAutoriza(null);
            }

        }

        private void mapForm(List<DetalleRecepcion> detalles)
        {
            lvlSintomas.Items.Clear();
            if (detalles != null)
            {
                foreach (var item in detalles)
                {
                    if (item.isCancelado == false)
                    {
                        ListViewItem lvl = new ListViewItem();
                        lvl.Content = item;
                        lvl.Selected += Lvl_Selected;

                        lvl.MouseDoubleClick += Lvl_MouseDoubleClick;
                        lvlSintomas.Items.Add(lvl);
                    }
                    else
                    {
                        listBaja.Add(item);
                    }

                }
            }
        }

        private Recepcion mapForm()
        {
            DateTime? fechaNull = null;
            try
            {
                return new Recepcion
                {
                    idOrdenSer = txtID.Tag == null ? 0 : Convert.ToInt32(txtID.Text),
                    empresa = (Empresa)txtEmpresa.Tag,
                    personal = mainWindow.esTaller == false ? new Personal() : (Personal)txtEntrega.Tag,
                    personalAutoriza = (Personal)txtAutoriza.Tag,
                    fechaCaptura = txtID.Tag != null ? ((Recepcion)txtID.Tag).fechaCaptura : DateTime.Now,
                    FechaRecepcion = mainWindow.esTaller == true ? DateTime.Now : fechaNull,
                    Estatus = mainWindow.esTaller == true ? "REC" : "PRE",
                    Kilometraje = Convert.ToDecimal(txtKilometraje.Text), // mainWindow.esTaller == false ? 0 : Convert.ToDecimal(txtKilometraje.Text),
                    tipoOrden = (TipoOrden)cbxTipoOrden.SelectedItem,
                    unidadTransporte = (UnidadTransporte)txtUnidadTransporte.Tag,
                    usuario = (Usuario)txtRecive.Tag,
                    detalles = getList()
                };
            }
            catch (Exception E)
            {
                MessageBox.Show(E.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }
        }

        private List<DetalleRecepcion> getList()
        {
            List<DetalleRecepcion> list = new List<DetalleRecepcion>();
            if (txtID.Tag != null)
            {
                foreach (var item in listBaja)
                {
                    list.Add(item);
                }
            }
            foreach (var item in lvlSintomas.Items)
            {
                list.Add((DetalleRecepcion)((ListViewItem)item).Content);
            }
            return list;
        }

        public override OperationResult eliminar()
        {
            try
            {
                Recepcion recepcion = mapForm();
                if (recepcion != null)
                {
                    string motivo = Interaction.InputBox("Escribir el motivo de la baja", "Motivo de Baja");
                    if (!string.IsNullOrEmpty(motivo.Trim()))
                    {
                        recepcion.motivoBaja = motivo;
                        OperationResult result = new RecepcionSvc().deleteRecepcion(recepcion);
                        if (result.typeResult == ResultTypes.success)
                        {
                            nuevo();
                        }
                        return result;
                    }
                    else
                    {
                        return new OperationResult { valor = 2, mensaje = "No se capturo el motivo de la baja", result = null };
                    }

                }
                else
                {
                    return new OperationResult { valor = 1, mensaje = "Error al construir el objeto", result = null };
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message, result = null };
            }
        }

        public override void editar()
        {
            //{
            //    gBoxUnidad.IsEnabled = true;
            //    gBoxPersonal.IsEnabled = true;
            //    gBoxSintomas.IsEnabled = true;
            //}
            base.editar();
        }
        public override void buscar()
        {
            selectRecepcion select = new selectRecepcion(txtID.Text.Trim());
            Recepcion recepcion = select.findAllRecepciones(0);
            if (recepcion != null)
            {
                mapFormComplete(recepcion);
                txtUnidadTransporte.Focus();
                base.guardar();
            }
           

        }

        private void txtID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.F3)
            {
                buscar();
            }
            else if (e.Key == Key.Enter)
            {
                buscarRecepcion();
            }
        }

        private void buscarRecepcion()
        {
            int id = 0;
            try
            {
                id = Convert.ToInt32(txtID.Text.Trim());
            }
            catch (Exception)
            {
                MessageBox.Show(string.Format("El folio no es correcto."), "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            var resp = new RecepcionSvc().getRecepcionesById(0, id);
            if (resp.typeResult == ResultTypes.error)
            {
                MessageBox.Show(string.Format(resp.mensaje), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            if (resp.typeResult == ResultTypes.recordNotFound)
            {
                MessageBox.Show(string.Format(resp.mensaje), "Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            if (resp.typeResult == ResultTypes.warning)
            {
                MessageBox.Show(string.Format(resp.mensaje), "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            if (resp.typeResult == ResultTypes.success)
            {
                List<Recepcion> listUnidad = (List<Recepcion>)resp.result;
                if (listUnidad.Count == 1)
                {
                    mapFormComplete(listUnidad[0]);
                    base.guardar();
                    mainWindow.scrollContainer.IsEnabled = false;
                    txtUnidadTransporte.Focus();
                }
                else
                {
                    buscar();
                }
                
                
            }
        }

        private void txtKilometraje_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                txtDescripcion.Focus();
            }
        }

        private void txtEntrega_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.F3)
            {
                buscarPersonal();
            }
        }

        private void txtAutoriza_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.F3)
            {
                buscarPersonaAutiza();
            }
        }
    }
}
