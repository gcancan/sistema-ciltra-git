﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
//using Core.Models;
using Core.Interfaces;
using Core.Models;

namespace WpfClient.Modulos_Mantenimiento
{
    /// <summary>
    /// Interaction logic for Diagnosticos.xaml
    /// </summary>
    public partial class Diagnosticos : ViewBase
    {
        public HeaderDiagnostico _headerDiagnostico = new HeaderDiagnostico();
        
        public Diagnosticos()
        {
            InitializeComponent();
        }
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            mainWindow.Title = "Asignación de Ordenes de Trabajo.";
            getAllDiviciones();
            base.nuevo();

            if (mainWindow.clave != 0)
            {
                txtRecepcionFolio.Text = mainWindow.clave.ToString();
                buscarRecepcionesById();
            }
        }

        private void getAllDiviciones()
        {
            OperationResult resp = new DivicionSvc().getAllDiviciones();
            if (resp.typeResult == ResultTypes.error)
            {
                MessageBox.Show(resp.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                mainWindow.scrollContainer.IsEnabled = false;
            }
            if (resp.typeResult == ResultTypes.warning)
            {
                MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                mainWindow.scrollContainer.IsEnabled = false;
            }
            if (resp.typeResult == ResultTypes.recordNotFound)
            {
                MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                mainWindow.scrollContainer.IsEnabled = false;
            }
            if (resp.typeResult == ResultTypes.success)
            {
                List<Divicion> list = (List<Divicion>)resp.result;
                mapComboDiviciones(list);
            }
        }

        private void mapComboDiviciones(List<Divicion> list)
        {
            foreach (var item in list)
            {
                cbxDivicion.Items.Add(item);
            }
        }

        private void btnBuscarRecepciones_Click(object sender, RoutedEventArgs e)
        {
            buscarRecepciones();
        }
        private void txtRecepcionFolio_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.F3)
            {
                buscarRecepciones();
            }
            else if (e.Key == Key.Enter)
            {
                buscarRecepcionesById();
            }
        }

        private void buscarRecepcionesById()
        {
            int id = 0;
            try
            {
                id = Convert.ToInt32(txtRecepcionFolio.Text.Trim());
            }
            catch (Exception)
            {
                MessageBox.Show(string.Format("El folio no es correcto."), "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            var resp = new RecepcionSvc().getRecepcionesById(0, id);
            if (resp.typeResult == ResultTypes.error)
            {
                MessageBox.Show(string.Format(resp.mensaje), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            if (resp.typeResult == ResultTypes.recordNotFound)
            {
                MessageBox.Show(string.Format(resp.mensaje), "Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            if (resp.typeResult == ResultTypes.warning)
            {
                MessageBox.Show(string.Format(resp.mensaje), "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            if (resp.typeResult == ResultTypes.success)
            {
                List<Recepcion> listUnidad = (List<Recepcion>)resp.result;
                if (listUnidad.Count == 1)
                {
                    mapFormRecepcioner(listUnidad[0]);
                    base.nuevo();
                    //mainWindow.scrollContainer.IsEnabled = false;
                }
                else
                {
                    buscarRecepciones();
                }

            }
        }

        private void buscarRecepciones()
        {
            selectRecepcion select = new selectRecepcion(txtRecepcionFolio.Text.Trim());
            var recepcion = select.findAllRecepciones(1);
            if (recepcion != null)
            {
                mapFormRecepcioner(recepcion);
            }
        }

        private void mapFormRecepcioner(Recepcion recepcion)
        {
            if (recepcion != null)
            {
                txtRecepcionFolio.Tag = recepcion;
                txtRecepcionFolio.Text = recepcion.idOrdenSer.ToString();
                txtRecepcionTipoOrden.Text = recepcion.tipoOrden.nomTipoOrden;
                txtRecepcionFecha.Text = recepcion.FechaRecepcion.ToString();
                txtRecepcionEstatus.Text = recepcion.Estatus == "REC" ? "RECEPCION" : "ASIGNADOS";

                txtRecepcionClaveUnidadTransporte.Text = recepcion.id_UnidadTrans;
                txtRecepcionTipoUnidadTransporte.Text = recepcion.unidadTransporte.tipoUnidad.nomTipoUniTrans;
                txtRecepcionMarcaUnidadTransporte.Text = recepcion.unidadTransporte.marca.nombreMarca;
                if (recepcion.empresa == null)
                {
                    recepcion.empresa = recepcion.unidadTransporte.empresa;
                }
                txtRecepcionEmpresa.Text = recepcion.empresa.razonSocial;

                lvlDetalles.Items.Clear();
                mapList(recepcion.detalles);
                lvlDetalles.SelectedIndex = 0;
                cbxMecanico.Focus();
            }
            else
            {
                txtRecepcionFolio.Tag = null;
                txtRecepcionFolio.Clear();
                txtRecepcionTipoOrden.Clear();
                txtRecepcionFecha.Clear();
                txtRecepcionEstatus.Clear();

                txtRecepcionClaveUnidadTransporte.Clear();
                txtRecepcionTipoUnidadTransporte.Clear();
                txtRecepcionMarcaUnidadTransporte.Clear();
                txtRecepcionEmpresa.Clear();

                lvlDetalles.Items.Clear();
                //mapList(recepcion.detalles);
            }
        }

        private void mapList(List<DetalleRecepcion> detalles)
        {
            lvlDetalles.Items.Clear();
            foreach (var item in detalles)
            {
                if (!item.isCancelado)
                {
                    item._divicion = item.divicion;
                    item._familia = item.familia;
                    ListViewItem lvl = new ListViewItem();
                    lvl.Content = item;
                    lvl.Selected += Lvl_Selected;

                    lvlDetalles.Items.Add(lvl);
                }
            }
        }

        private void Lvl_Selected(object sender, RoutedEventArgs e)
        {
            var x = ((DetalleRecepcion)((ListViewItem)sender).Content);
            txtDescripcion.Text = x.descripcion;
            int i = 0;
            foreach (var item in cbxDivicion.Items)
            {
                if (x._divicion.idDivicion == ((Divicion)item).idDivicion)
                {
                    cbxDivicion.SelectedIndex = i;
                    break;
                }
                else { i++; }
            }
            mapComboFamilias(x._divicion.listFamilia);
            mapComboPersonas(x._divicion.listPersonal);
            
        }

        private void cbxDivicion_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbxDivicion.SelectedItem != null && lvlDetalles.SelectedItem != null)
            {
                Divicion divicion = ((Divicion)cbxDivicion.SelectedItem);
                mapComboFamilias(divicion.listFamilia);
                mapComboPersonas(divicion.listPersonal);
            }
        }

        private void mapComboPersonas(List<Personal> listPersonal)
        {
            cbxMecanico.Items.Clear();
            foreach (var item in listPersonal)
            {
                cbxMecanico.Items.Add(item);
            }
        }

        private void mapComboFamilias(List<Familia> listFamilia)
        {
            cbxFamilia.Items.Clear();
            foreach (var item in listFamilia)
            {
                cbxFamilia.Items.Add(item);
            }
            cbxFamilia.SelectedIndex = 0;
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            if (lvlDetalles.SelectedItem != null)
            {
                ListViewItem lvl = new ListViewItem();

                DetalleRecepcion det = new DetalleRecepcion();
                det = ((DetalleRecepcion)((ListViewItem)lvlDetalles.SelectedItem).Content);
                det._divicion = ((Divicion)cbxDivicion.SelectedItem);
                det._familia = ((Familia)cbxFamilia.SelectedItem);
                det._personal = ((Personal)cbxMecanico.SelectedItem);
                lvl.Content = det;
                lvl.Selected += Lvl_Selected;
                lvlDetalles.Items[lvlDetalles.SelectedIndex] = lvl;

                cbxDivicion.SelectedItem = null;
                cbxFamilia.Items.Clear();
                cbxMecanico.Items.Clear();
                txtDescripcion.Clear();
            }
        }

        public override OperationResult guardar()
        {
            try
            {
                Cursor = Cursors.Wait;
                if (validar())
                {
                    HeaderDiagnostico diagnostico = mapForm();
                    if (diagnostico != null)
                    {
                        OperationResult resp = new DiagnosticoSvc().saveAsignacion(diagnostico);
                        if (resp.typeResult == ResultTypes.success)
                        {
                            List<DetalleRecepcion> list = new List<DetalleRecepcion>();
                            HeaderDiagnostico head = (HeaderDiagnostico)resp.result;
                            _headerDiagnostico = head;
                            foreach (DetalleRecepcion item in head.listDetallesRecepcion)
                            {
                                item._divicion = item.divicion;
                                item._familia = item.familia;
                                list.Add(item);
                            }

                            imprimirDocumento();
                            mapList(list);
                            return resp;
                        }
                        else
                        {
                            return new OperationResult { valor = 1, mensaje = resp.mensaje };
                        }
                    }
                    else
                    {
                        return new OperationResult { valor = 1, mensaje = "Error al construir el objeto" };
                    }
                }
                else
                {
                    return new OperationResult { valor = 1, mensaje = "No se cumplen las validaciones" };
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { mensaje = ex.Message, valor = 1, result = null };
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
           
        }

        private bool validar()
        {
            if (txtRecepcionFolio.Tag == null)
            {
                return false;
            }
            return true;
        }

        private HeaderDiagnostico mapForm()
        {
            try
            {
                List<Diagnostico> listDiag = new List<Diagnostico>();
                List<DetalleRecepcion> listDet = new List<DetalleRecepcion>();

                foreach (var item in lvlDetalles.Items)
                {
                    DetalleRecepcion detalles = ((DetalleRecepcion)((ListViewItem)item).Content);
                    Diagnostico diagnostico = new Diagnostico
                    {
                        idOrdenTrabajo = detalles.idDetalleRecepcion,
                        actividad = null,
                        Estatus = "ASIG",
                        FechaAsig = DateTime.Now,
                        recepcion = (Recepcion)txtRecepcionFolio.Tag,
                        idServicio = 0,
                        UsuarioAsig = mainWindow.usuario.sUserId,
                        CostoProductos = 0,
                        UsuarioDiag = mainWindow.usuario.sUserId
                                                                                                                
                    };
                    listDiag.Add(diagnostico);

                    detalles.familia = detalles._familia;
                    detalles.divicion = detalles._divicion;
                    listDet.Add(detalles);
                }

                HeaderDiagnostico header = new HeaderDiagnostico
                {
                    recepcion = (Recepcion)txtRecepcionFolio.Tag,
                    idOrdenServ = ((Recepcion)txtRecepcionFolio.Tag).idOrdenSer,
                    listDiagnostico = listDiag,
                    listDetallesRecepcion = listDet
                };

                return header;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public override void buscar()
        {
            try
            {
                selectRecepcion select = new selectRecepcion();                
                _headerDiagnostico = select.findAllAsignaciones();
                if (_headerDiagnostico != null)
                {                    
                    mapFormRecepcioner(_headerDiagnostico.recepcion);
                    base.buscar();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }            
        }

        public override void imprimir()
        {
            try
            {
                //HeaderDiagnostico headerDiagnostico = (HeaderDiagnostico)mapForm();

                //_headerDiagnostico = _headerDiagnostico.listDetallesRecepcion == null ? mapForm() : _headerDiagnostico;

                //_headerDiagnostico.fecha = _headerDiagnostico.fecha == null ? DateTime.Now : _headerDiagnostico.fecha;
                //_headerDiagnostico.persona = _headerDiagnostico.persona == null ? "Default" : _headerDiagnostico.persona;
                imprimirDocumento();
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void imprimirDocumento()
        {
            try
            {
                ReportView rv = new ReportView();
                //rv.crearArchivo(TipoDocumento.ASIGNACION_DE_TRABAJO, null, _headerDiagnostico);
                rv.ShowDialog();
                rv.Close();
                base.imprimir();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        public override OperationResult eliminar()
        {
            try
            {
                HeaderDiagnostico headerDiagnostico = (HeaderDiagnostico)mapForm();
                OperationResult resp = new DiagnosticoSvc().deleteAsignacion(headerDiagnostico);
                if (resp.typeResult == ResultTypes.success)
                {
                    mapFormRecepcioner(null);
                    base.nuevo();
                }
                return resp;
            }
            catch (Exception ex)
            {
                return new OperationResult { result= null, mensaje = ex.Message, valor = 1};
            }
        }

        private void btnAddNew_Click(object sender, RoutedEventArgs e)
        {
            if (txtRecepcionFolio.Tag != null)
            {
                addAsignacionTrabajo add = new addAsignacionTrabajo();
                var det = add.newDetalle(((Recepcion)txtRecepcionFolio.Tag).idOrdenSer);
                if (det != null)
                {
                    det.idRecepcion = ((Recepcion)txtRecepcionFolio.Tag).idOrdenSer;
                    ListViewItem lvl = new ListViewItem();
                    lvl.Content = det;
                    lvl.Selected += Lvl_Selected;
                    lvlDetalles.Items.Add(lvl);
                }
            }
            
        }

        public override void nuevo()
        {
            mapFormRecepcioner(null);
            base.nuevo();
            txtRecepcionFolio.Focus();
        }
    }
}
