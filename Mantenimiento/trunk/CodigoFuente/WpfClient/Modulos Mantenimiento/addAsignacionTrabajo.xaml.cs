﻿using Core.Interfaces;
using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfClient.Modulos_Mantenimiento
{
    /// <summary>
    /// Interaction logic for addAsignacionTrabajo.xaml
    /// </summary>
    public partial class addAsignacionTrabajo : Window
    {
        public DetalleRecepcion _detalle = null;
        int _idOrdenSer = 0;
        public addAsignacionTrabajo()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            getAllDiviciones();
        }

        public DetalleRecepcion newDetalle(int idOrdenSer)
        {
            _idOrdenSer = idOrdenSer;
            bool? sResul = ShowDialog();
            if (sResul.Value && _detalle != null)
            {
                return _detalle;
            }
            return null;
        }

        private void getAllDiviciones()
        {
            OperationResult resp = new DivicionSvc().getAllDiviciones();
            if (resp.typeResult == ResultTypes.error)
            {
                MessageBox.Show(resp.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                controles.IsEnabled = false;
            }
            if (resp.typeResult == ResultTypes.warning)
            {
                MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                controles.IsEnabled = false;
            }
            if (resp.typeResult == ResultTypes.recordNotFound)
            {
                MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                controles.IsEnabled = false;
            }
            if (resp.typeResult == ResultTypes.success)
            {
                List<Divicion> list = (List<Divicion>)resp.result;
                mapComboDiviciones(list);
            }
        }

        private void mapComboDiviciones(List<Divicion> list)
        {
            foreach (var item in list)
            {
                cbxDivicion.Items.Add(item);
            }
        }

        private void cbxDivicion_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbxDivicion.SelectedItem != null)
            {
                Divicion divicion = ((Divicion)cbxDivicion.SelectedItem);
                mapComboFamilias(divicion.listFamilia);
                mapComboPersonas(divicion.listPersonal);
            }
        }
        private void mapComboPersonas(List<Personal> listPersonal)
        {
            cbxMecanico.Items.Clear();
            foreach (var item in listPersonal)
            {
                cbxMecanico.Items.Add(item);
            }
        }

        private void mapComboFamilias(List<Familia> listFamilia)
        {
            cbxFamilia.Items.Clear();
            foreach (var item in listFamilia)
            {
                cbxFamilia.Items.Add(item);
            }
            cbxFamilia.SelectedIndex = 0;
        }

        private void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            if (validar())
            {
                List<DetalleRecepcion> detalle = new List<DetalleRecepcion>();
                detalle.Add(new DetalleRecepcion
                {
                    idRecepcion = _idOrdenSer,
                    descripcion = txtDescripcion.Text.Trim(),
                    familia = (Familia)cbxFamilia.SelectedItem,
                    _familia = (Familia)cbxFamilia.SelectedItem,
                    divicion = (Divicion)cbxDivicion.SelectedItem,
                    _divicion = (Divicion)cbxDivicion.SelectedItem,
                    _personal = (Personal)cbxMecanico.SelectedItem,
                    isCancelado = false,
                    motivoCancelacion = "",
                    idDetalleRecepcion = 0
                });

                OperationResult resp = new RecepcionSvc().insertDetallesRecepcion(detalle);
                if (resp.typeResult == ResultTypes.success)
                {
                    DetalleRecepcion det = (DetalleRecepcion)resp.result;
                    _detalle = det;
                    DialogResult = true;
                }

                
            }
        }

        private bool validar()
        {
            if (string.IsNullOrEmpty(txtDescripcion.Text.Trim()))
            {
                MessageBox.Show("Es necesaria una descripción de la falla.", "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            if (cbxDivicion.SelectedItem == null)
            {
                MessageBox.Show("Es necesario seleccionar una división.", "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            if (cbxFamilia.SelectedItem == null)
            {
                MessageBox.Show("Es necesario elegir una familia.", "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            if (cbxMecanico.SelectedItem == null)
            {
                MessageBox.Show("Es necesario asignar un mecanico", "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            return true;
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}
