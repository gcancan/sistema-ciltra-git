﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using System.Windows;

namespace WpfClient
{
    public partial class ViewFactory
    {
        public static IView createView(string claveMenu)
        {
            if (!Enum.IsDefined(typeof(Menus), claveMenu))
            {
                //MessageBox.Show("No se ha agregado el menú " + claveMenu + " a la enumeración Menus.", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return null;
                //throw new Exception("No se ha agregado el menú " + claveMenu + " a la enumeración Menus.");
            }

            IView view = null;

            switch ((Menus)Enum.Parse(typeof(Menus), claveMenu))
            {
                case Menus.MENU_CATALOGOS_TIPOS_UNIDADES:
                    view = new CatTipoTransporte();
                    break;
                //case Menus.MENU_CATALOGOS_SERVICIOS:
                //    view = new CatServicios();
                //    break;i
                case Menus.MENU_MOVIMIENTOS_RECEPCION:
                    view = new Modulos_Mantenimiento.Recepciones();
                    break;
                case Menus.MENU_MOVIMIENTOS_DIAGNOSTICO:
                    view = new Modulos_Mantenimiento.Diagnosticos();
                    break; 
                default:
                    return null;
            }

            return view;
        }
    }
}
