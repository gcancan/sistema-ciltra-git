﻿using Core.Interfaces;
using Core.Models;
//using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfClient
{
    /// <summary>
    /// Interaction logic for selectRecepcion.xaml
    /// </summary>
    public partial class selectRecepcion : Window
    {
        string parametro = "";
        int _tipoBusqueda = 0;
        bool _asignacion = false;
        List<HeaderDiagnostico> _listHead = new List<HeaderDiagnostico>();
        public selectRecepcion()
        {
            InitializeComponent();
        }
        public selectRecepcion(string txtFind)
        {
            InitializeComponent();
            parametro = txtFind;
        }

        public Recepcion findAllRecepciones(int tipoBusqueda)
        {
            _tipoBusqueda = tipoBusqueda;
            bool? sResul = ShowDialog();
            if (sResul.Value && lvlRecepciones.Items != null && lvlRecepciones.SelectedItem != null)
            {
                return (Recepcion)((ListViewItem)lvlRecepciones.SelectedItem).Content;
            }
            return null;
        }

        public HeaderDiagnostico findAllAsignaciones()
        {
            _asignacion = true;
            bool? sResul = ShowDialog();
            if (sResul.Value && lvlRecepciones.Items != null && lvlRecepciones.SelectedItem != null)
            {
                Recepcion rep = (Recepcion)((ListViewItem)lvlRecepciones.SelectedItem).Content;
                foreach (var item in _listHead)
                {
                    if (rep.idOrdenSer == item.idOrdenServ)
                    {
                        return item;
                    }
                }
                
            }
            return null;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            
            findAllRecepcionesInicio();
            txtFind.Text = parametro;
        }

        private void findAllRecepcionesInicio()
        {
            try
            {
                Cursor = Cursors.Wait;
                if (_asignacion == false )
                {
                    OperationResult resp = new RecepcionSvc().getAllrecepciones(_tipoBusqueda);
                    if (resp.typeResult == ResultTypes.error)
                    {
                        MessageBox.Show(string.Format(resp.mensaje), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        controles.IsEnabled = false;
                    }
                    if (resp.typeResult == ResultTypes.recordNotFound)
                    {
                        MessageBox.Show(string.Format(resp.mensaje), "Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
                        controles.IsEnabled = false;
                    }
                    if (resp.typeResult == ResultTypes.warning)
                    {
                        MessageBox.Show(string.Format(resp.mensaje), "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                        controles.IsEnabled = false;
                    }
                    if (resp.typeResult == ResultTypes.success)
                    {
                        List<Recepcion> recep = (List<Recepcion>)resp.result;
                        foreach (var item in recep)
                        {
                            item.empresa = item.unidadTransporte.empresa;
                        }
                        mapList(recep);
                    }
                }
                else
                {
                    OperationResult resp = new DiagnosticoSvc().getAllAsignaciones();
                    if (resp.typeResult == ResultTypes.error)
                    {
                        MessageBox.Show(string.Format(resp.mensaje), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        controles.IsEnabled = false;
                    }
                    if (resp.typeResult == ResultTypes.recordNotFound)
                    {
                        MessageBox.Show(string.Format(resp.mensaje), "Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
                        controles.IsEnabled = false;
                    }
                    if (resp.typeResult == ResultTypes.warning)
                    {
                        MessageBox.Show(string.Format(resp.mensaje), "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                        controles.IsEnabled = false;
                    }
                    if (resp.typeResult == ResultTypes.success)
                    {
                        List<HeaderDiagnostico> recep = (List<HeaderDiagnostico>)resp.result;
                        _listHead = recep;
                        List<Recepcion> list = new List<Recepcion>();
                        foreach (var item in recep)
                        {
                            item.recepcion.empresa = item.recepcion.unidadTransporte.empresa;
                            list.Add(item.recepcion);
                        }
                        mapList(list);
                    }
                }
               
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format(ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                controles.IsEnabled = false;
            }
            finally { Cursor = Cursors.Arrow; }
        }

        private void mapList(List<Recepcion> list)
        {
            lvlRecepciones.Items.Clear();
            List<ListViewItem> lvlList = new List<ListViewItem>();
            foreach (Recepcion item in list)
            {
                ListViewItem lvl = new ListViewItem();
                lvl.Content = item;
                lvl.MouseDoubleClick += Lvl_MouseDoubleClick;
                lvlList.Add(lvl);
                //lvlUnidades.Items.Add(lvl);
            }
            lvlRecepciones.ItemsSource = lvlList;
            CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(lvlRecepciones.ItemsSource);
            view.Filter = UserFilter;
        }

        private bool UserFilter(object item)
        {
            if (String.IsNullOrEmpty(txtFind.Text))
                return true;
            else
                //return ((item as UnidadTransporte).descripcionUni.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0);
                return ((Recepcion)(item as ListViewItem).Content).idOrdenSer.ToString().IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0 ||
                        ((Recepcion)(item as ListViewItem).Content).tipoOrden.nomTipoOrden.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0 ||
                        ((Recepcion)(item as ListViewItem).Content).empresa.razonSocial.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0 ||
                        ((Recepcion)(item as ListViewItem).Content).personal.nombreCompleto.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0 ||
                        ((Recepcion)(item as ListViewItem).Content).unidadTransporte.id_UnidadTrans.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0 ||
                        ((Recepcion)(item as ListViewItem).Content).FechaRecepcion.ToString().IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0;

        }

        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DialogResult = true;
        }

        private void txtFind_TextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(lvlRecepciones.ItemsSource).Refresh();
        }        
    }
}

