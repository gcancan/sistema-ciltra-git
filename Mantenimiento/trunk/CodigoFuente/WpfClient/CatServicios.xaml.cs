﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Models;
using Core.Interfaces;


namespace WpfClient
{
    /// <summary>
    /// Interaction logic for CatServicios.xaml
    /// </summary>
    public partial class CatServicios : ViewBase
    {
        public CatServicios()
        {
            InitializeComponent();
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            
            nuevo();
            lvlServicios.SelectedItem = lvlServicios.SelectedIndex= 0;
            //var f = new Repo
            //frmReporteador
        }

        private void fintAllServicios()
        {
            try
            {
                OperationResult resp = new ServicioSvc().findServicio();

                List<Servicio> listService = ((List<Servicio>)resp.result);

                lvlServicios.Items.Clear();
                foreach (var item in listService)
                {
                    addLista(item);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        public void addLista(Servicio servicio)
        {
            ListViewItem lvl = new ListViewItem();
            lvl.Content = servicio;
            lvl.Selected += Lvl_Selected;
            lvlServicios.Items.Add(lvl);
        }

        private void Lvl_Selected(object sender, RoutedEventArgs e)
        {
            try
            {
                if (lvlServicios.SelectedItem != null)
                {
                    ListViewItem lvl = new ListViewItem();
                    lvl = (ListViewItem)lvlServicios.SelectedItem;
                    mapForm(((Servicio)lvl.Content));
                    base.guardar();
                    controles.IsEnabled = false;
                }
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }
            
        }

        public override void nuevo()
        {
            fintAllServicios();
            mapForm(null);
            base.nuevo();
        }
        private void mapForm(Servicio servicio)
        {
            if (servicio != null)
            {
                txtId.Text = servicio.idServicio.ToString();
                txtId.Tag = servicio;
                txtClave.Text = servicio.clave;
                txtNombre.Text = servicio.nombre;
                txtKmHr.Text = servicio.km_Hr.ToString();
                txtCosto.Text = servicio.costo.ToString();
                txtDescripcion.Text = servicio.descripcion;
            }
            else
            {
                txtId.Clear();
                txtId.Tag = null;
                txtClave.Clear();
                txtNombre.Clear();
                txtKmHr.Clear();
                txtCosto.Clear();
                txtDescripcion.Clear();
                controles.IsEnabled = true;             
            }
        }

        private Servicio mapForm()
        {
            try
            {
                txtId.Tag = new Servicio
                {
                    idServicio = string.IsNullOrEmpty(txtId.Text.Trim()) ? 0 : Convert.ToInt32(txtId.Text),
                    clave = txtClave.Text,
                    nombre = txtNombre.Text,
                    descripcion = txtDescripcion.Text,
                    costo = Convert.ToDecimal(txtCosto.Text),
                    km_Hr = Convert.ToDecimal(txtKmHr.Text)
                };
                return ((Servicio)txtId.Tag);
            }
            catch (Exception e)
            {
                return null;
            }
        }


        public override OperationResult guardar()
        {
            try
            {
                if (validar())
                {
                    Servicio servicio = mapForm();
                    if (servicio != null)
                    {
                        OperationResult resp = new ServicioSvc().saveServicio(servicio);
                        if (resp.typeResult == ResultTypes.success)
                        {
                            mapForm((Servicio)resp.result);
                            addLista((Servicio)resp.result);
                            
                        }
                        return resp;
                    }
                    else
                    {
                        return new OperationResult { result = null, valor = 1, mensaje = "Error al construir el objeto Servicios" };
                    }
                }
                else
                {
                    return new OperationResult { result = null, valor = 2, mensaje = "Falta información por completar y/o valores incorrectos" };
                }

            }
            catch (Exception e)
            {
                return new OperationResult { result = null, valor = 1, mensaje = e.Message };
            }
        }

        private bool validar()
        {
            try
            {
                if (string.IsNullOrEmpty(txtClave.Text.Trim()))
                {
                    return false;
                }
                if (string.IsNullOrEmpty(txtNombre.Text.Trim()))
                {
                    return false;
                }
                if (string.IsNullOrEmpty(txtCosto.Text.Trim()))
                {
                    return false;
                }
                if (string.IsNullOrEmpty(txtKmHr.Text.Trim()))
                {
                    return false;
                }
                if (string.IsNullOrEmpty(txtDescripcion.Text.Trim()))
                {
                    return false;
                }
                decimal d;
                if (!decimal.TryParse(txtCosto.Text.Trim(), out d))
                {
                    return false;
                }
                if (!decimal.TryParse(txtKmHr.Text.Trim(), out d))
                {
                    return false;
                }

                return true;
            }
            catch (Exception)
            {
                MessageBox.Show(string.Format("Ocurrio un error durante las validaciones"), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }

        }

        public override OperationResult eliminar()
        {
            try
            {
                Servicio servicio = ((Servicio)txtId.Tag);
                OperationResult resp = new ServicioSvc().deleteServicio(servicio);
                if (resp.typeResult == ResultTypes.success)
                {
                    nuevo();
                }
                return resp;
            }
            catch (Exception e)
            {
                return new OperationResult { valor = 1, mensaje = e.Message, result = null };
            }
        }

        public override void editar()
        {
            controles.IsEnabled = true;
            base.editar();     
        }

        private void lvlServicios_Selected(object sender, RoutedEventArgs e)
        {

        }
    }
}
