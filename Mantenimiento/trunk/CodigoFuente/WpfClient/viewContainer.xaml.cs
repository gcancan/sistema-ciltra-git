﻿using System.Windows.Controls;

namespace WpfClient
{
    /// <summary>
    /// Interaction logic for viewContainer.xaml
    /// </summary>
    public partial class viewContainer : UserControl
    {
        public viewContainer(IView vb)
        {
            InitializeComponent();

            contedorPantalla.Content = vb;
            //MainWindow mw = new MainWindow();
            //mw.enableToolBarCommands(ToolbarCommands.cerrar);
        }
    }
}
