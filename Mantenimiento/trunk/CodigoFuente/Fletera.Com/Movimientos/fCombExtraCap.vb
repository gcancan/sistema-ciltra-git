'Fecha: 15 / Febrero / 2016
'ROGER GALA PACHECO
'************************************************************************************************************************************
'*  Fecha Modificacion :   15 / Febrero / 2016                                                                                       
'*  
'************************************************************************************************************************************
Imports System.Configuration
Imports Core.Models
Imports System.Drawing.Printing
Imports System.Drawing.Text
Imports Nombre = Core
Imports WpfClient

Public Class fCombExtraCap

    Inherits System.Windows.Forms.Form
    Private _Tag As String
    Dim bEsIdentidad As Boolean = True
    Dim OpcionForma As TipoOpcionForma
    Dim TablaBd As String = "COMBEXTRACAP"


    '*******************************************VALIDOS
    Dim MyTablaTipoOrdServ As DataTable = New DataTable(TablaBd)
    Dim TipOrdServ As TipoServClass
    Dim Servicio As ServicioClass
    Dim vNumOrdServ As Integer = 0
    Dim vOrdenTrabajo As Boolean = False
    Dim vTipoOrdServ As Integer = 0
    Dim vidUnidadTrans As String = ""
    Dim vNomTipOrdServ As String = ""
    Dim vTipoServ As Integer = 0

    Dim UniTrans As UnidadesTranspClass
    Dim TipoUniTrans As TipoUniTransClass
    Dim Clave As Integer
    Dim MyTablaDetalle As DataTable = New DataTable(TablaBd)
    Dim vidPadreOrdSer As Integer = 0
    Dim ArrayOrdenSer() As Integer
    Dim ArrayOrdenSerTipo() As Boolean
    Dim indiceOS As Integer = 0
    Dim ArrayOrdenTrab() As Integer
    Dim indiceOT As Integer = 0
    Dim ArraySql() As String
    Dim indice As Integer = 0

    Dim ImpRep As New ImprimeRemision
    Private prtSettings As PrinterSettings
    Private prtDoc As PrintDocument

    Dim MyPaperSize As New Printing.PaperSize("MediaCarta", 827, 584.5)
    'Dim MyPaperSize As New Printing.PaperSize()

    Dim printFont As New System.Drawing.Font("Courier New", 7)
    Dim NumRegistros As Integer
    Private lineaActual As Integer = 0
    Dim ContPaginas As Integer = 0
    Dim MyTablaImprime As DataTable = New DataTable("IMPRIME")
    Dim DsRep As DataSet
    Dim PATH_FONTS As String = ""
    Dim Empresa As EmpresaClass

    Dim PadreOS As MasterOrdServClass
    Dim vUltKm As Integer
    Dim MaximoKmdeMas As Integer = 10000

    Dim vValorNomAct As String
    Dim vValorNomDivision As String
    Dim vValorDivision As Integer

    'Dim tablaTipOS As DataTable
    '*******************************************VALIDOS


    Private OrdenTrabajo As OrdenTrabajoClass
    Private ULTOT As OrdenTrabajoClass
    Private Personal As PersonalClass
    Private PersonalPuesto As PuestosClass
    Private FamiliaOT As FamiliasClass

    Private OrdenServ As OrdenServClass
    Private TipOrd As TipoOrdenClass
    Private User As UsuarioClass
    Private Emp As EmpresaClass
    Private DetOS As detOrdenServicioClass
    Private Marca As MarcaClass

    Dim MyTablaHisOS As DataTable = New DataTable(TablaBd)
    Dim MyTablaHisOT As DataTable = New DataTable(TablaBd)
    Dim MyTablaRefac As DataTable = New DataTable(TablaBd)

    Dim MyTablaComboResp As DataTable = New DataTable(TablaBd)
    Dim MyTablaComboAyu1 As DataTable = New DataTable(TablaBd)
    Dim MyTablaComboAyu2 As DataTable = New DataTable(TablaBd)

    Dim MyTablaPersOcupado As DataTable = New DataTable(TablaBd)

    Private ProdCom As ProductosComClass
    Private UnidadCom As UnidadesComClass

    Private ObjSql As New ToolSQLs

    Dim Salida As Boolean
    Dim myDataColumn As DataColumn
    Dim myDataRow As DataRow

    Dim CantidadGrid As Double = 0
    Dim BandNoExisteProdEnGrid As Boolean = False

    Dim ObjCom As New FuncionesComClass

    Dim vCIDPRODUCTO As Integer
    Dim vCIDUNIDAD As Integer

    Dim vIdAlmacen As Integer = 1
    Dim IdOrdenServ As Integer

    Dim CadCam As String = ""
    Dim StrSql As String = ""


    Dim NoIdPreOC As Integer = 0
    Dim NoIdPreSal As Integer = 0

    Dim objNumSig As New CapaNegocio.Tablas

    Dim ContPersonal As Integer = 0
    Dim CostoManoObra As Double = 0
    Dim CostoRef As Double = 0

    Dim vLugOcupado As Integer = 0

    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents txtNoViaje As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents dtpFechaCapturaHora As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpFechaCap As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label10 As System.Windows.Forms.Label
    '
    Private _NoServ As Integer
    Private _con As String
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents cmbChofer As System.Windows.Forms.ComboBox
    Private _Usuario As String
    Private _IdUsuario As Integer
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtNomEmpresa As System.Windows.Forms.TextBox
    Friend WithEvents btnMenuReImprimir As System.Windows.Forms.ToolStripButton
    Friend WithEvents ConMnuOrdSer As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents ModificarToolStripMenuOrdSer As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CancelarToolStripMenuOrdSer As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ImprimirToolStripMenuOrdSer As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnMnuCancelar As System.Windows.Forms.ToolStripButton
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtEstatus As System.Windows.Forms.TextBox
    Friend WithEvents cmbOpciones As ComboBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Private _cveEmpresa As Integer
    Friend WithEvents cmdBuscaClave As Button
    Friend WithEvents txtidVCExtra As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents dtpHoraComb As DateTimePicker
    Friend WithEvents dtpFechaComb As DateTimePicker
    Friend WithEvents Label6 As Label
    Friend WithEvents txtltCombustible As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents cmbDespachador As ComboBox
    Friend WithEvents Label7 As Label
    Private TiposComb As CatTipoOsCombClass
    Private tabla As DataTable
    Dim TipoUnidad As String = ""

    Dim NomTipoUnidad As String = ""
    Friend WithEvents txtUniTrans As TextBox
    '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    'ULTIMAS VARIABLES
    Dim CombExt As CombustibleExtraClass
    Friend WithEvents Timer1 As Timer
    Dim UniTrasp As UnidadesTranspClass
    Dim PrecioCombustible As Decimal

    '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents btnMnuOk As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuLimpiar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuSalir As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripMenu As System.Windows.Forms.ToolStrip
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(fCombExtraCap))
        Me.ToolStripMenu = New System.Windows.Forms.ToolStrip()
        Me.btnMnuOk = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuCancelar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuLimpiar = New System.Windows.Forms.ToolStripButton()
        Me.btnMenuReImprimir = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuSalir = New System.Windows.Forms.ToolStripButton()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.txtUniTrans = New System.Windows.Forms.TextBox()
        Me.cmdBuscaClave = New System.Windows.Forms.Button()
        Me.txtidVCExtra = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cmbOpciones = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtEstatus = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtNomEmpresa = New System.Windows.Forms.TextBox()
        Me.cmbChofer = New System.Windows.Forms.ComboBox()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.dtpFechaCapturaHora = New System.Windows.Forms.DateTimePicker()
        Me.dtpFechaCap = New System.Windows.Forms.DateTimePicker()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtNoViaje = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.ConMnuOrdSer = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ModificarToolStripMenuOrdSer = New System.Windows.Forms.ToolStripMenuItem()
        Me.CancelarToolStripMenuOrdSer = New System.Windows.Forms.ToolStripMenuItem()
        Me.ImprimirToolStripMenuOrdSer = New System.Windows.Forms.ToolStripMenuItem()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.cmbDespachador = New System.Windows.Forms.ComboBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.dtpHoraComb = New System.Windows.Forms.DateTimePicker()
        Me.dtpFechaComb = New System.Windows.Forms.DateTimePicker()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtltCombustible = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.ToolStripMenu.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.ConMnuOrdSer.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'ToolStripMenu
        '
        Me.ToolStripMenu.AutoSize = False
        Me.ToolStripMenu.BackColor = System.Drawing.Color.Silver
        Me.ToolStripMenu.Dock = System.Windows.Forms.DockStyle.None
        Me.ToolStripMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnMnuOk, Me.btnMnuCancelar, Me.btnMnuLimpiar, Me.btnMenuReImprimir, Me.btnMnuSalir})
        Me.ToolStripMenu.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow
        Me.ToolStripMenu.Location = New System.Drawing.Point(3, 0)
        Me.ToolStripMenu.Name = "ToolStripMenu"
        Me.ToolStripMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.ToolStripMenu.Size = New System.Drawing.Size(891, 42)
        Me.ToolStripMenu.TabIndex = 1
        Me.ToolStripMenu.Text = "ToolStrip1"
        '
        'btnMnuOk
        '
        Me.btnMnuOk.ForeColor = System.Drawing.Color.Red
        Me.btnMnuOk.Image = Global.Fletera.My.Resources.Resources.Save1
        Me.btnMnuOk.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuOk.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuOk.Name = "btnMnuOk"
        Me.btnMnuOk.Size = New System.Drawing.Size(56, 39)
        Me.btnMnuOk.Text = "&GRABAR"
        Me.btnMnuOk.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuCancelar
        '
        Me.btnMnuCancelar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuCancelar.Image = Global.Fletera.My.Resources.Resources.signos_f_059
        Me.btnMnuCancelar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuCancelar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuCancelar.Name = "btnMnuCancelar"
        Me.btnMnuCancelar.Size = New System.Drawing.Size(71, 39)
        Me.btnMnuCancelar.Text = "&CANCELAR"
        Me.btnMnuCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnMnuCancelar.Visible = False
        '
        'btnMnuLimpiar
        '
        Me.btnMnuLimpiar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuLimpiar.Image = Global.Fletera.My.Resources.Resources.signos_f_059
        Me.btnMnuLimpiar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuLimpiar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuLimpiar.Name = "btnMnuLimpiar"
        Me.btnMnuLimpiar.Size = New System.Drawing.Size(56, 39)
        Me.btnMnuLimpiar.Text = "&LIMPIAR"
        Me.btnMnuLimpiar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMenuReImprimir
        '
        Me.btnMenuReImprimir.ForeColor = System.Drawing.Color.Red
        Me.btnMenuReImprimir.Image = Global.Fletera.My.Resources.Resources.impresora
        Me.btnMenuReImprimir.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMenuReImprimir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMenuReImprimir.Name = "btnMenuReImprimir"
        Me.btnMenuReImprimir.Size = New System.Drawing.Size(63, 39)
        Me.btnMenuReImprimir.Text = "&IMPRIMIR"
        Me.btnMenuReImprimir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuSalir
        '
        Me.btnMnuSalir.ForeColor = System.Drawing.Color.Red
        Me.btnMnuSalir.Image = Global.Fletera.My.Resources.Resources.Salida2
        Me.btnMnuSalir.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuSalir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuSalir.Name = "btnMnuSalir"
        Me.btnMnuSalir.Size = New System.Drawing.Size(41, 39)
        Me.btnMnuSalir.Text = "&SALIR"
        Me.btnMnuSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.txtUniTrans)
        Me.GroupBox4.Controls.Add(Me.cmdBuscaClave)
        Me.GroupBox4.Controls.Add(Me.txtidVCExtra)
        Me.GroupBox4.Controls.Add(Me.Label4)
        Me.GroupBox4.Controls.Add(Me.Label3)
        Me.GroupBox4.Controls.Add(Me.cmbOpciones)
        Me.GroupBox4.Controls.Add(Me.Label2)
        Me.GroupBox4.Controls.Add(Me.Label1)
        Me.GroupBox4.Controls.Add(Me.txtEstatus)
        Me.GroupBox4.Controls.Add(Me.Label9)
        Me.GroupBox4.Controls.Add(Me.txtNomEmpresa)
        Me.GroupBox4.Controls.Add(Me.cmbChofer)
        Me.GroupBox4.Controls.Add(Me.Label37)
        Me.GroupBox4.Controls.Add(Me.dtpFechaCapturaHora)
        Me.GroupBox4.Controls.Add(Me.dtpFechaCap)
        Me.GroupBox4.Controls.Add(Me.Label10)
        Me.GroupBox4.Controls.Add(Me.txtNoViaje)
        Me.GroupBox4.Controls.Add(Me.Label13)
        Me.GroupBox4.Location = New System.Drawing.Point(3, 45)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(854, 126)
        Me.GroupBox4.TabIndex = 75
        Me.GroupBox4.TabStop = False
        '
        'txtUniTrans
        '
        Me.txtUniTrans.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtUniTrans.ForeColor = System.Drawing.Color.Red
        Me.txtUniTrans.Location = New System.Drawing.Point(129, 91)
        Me.txtUniTrans.Name = "txtUniTrans"
        Me.txtUniTrans.Size = New System.Drawing.Size(69, 20)
        Me.txtUniTrans.TabIndex = 7
        Me.txtUniTrans.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cmdBuscaClave
        '
        Me.cmdBuscaClave.Image = CType(resources.GetObject("cmdBuscaClave.Image"), System.Drawing.Image)
        Me.cmdBuscaClave.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdBuscaClave.Location = New System.Drawing.Point(204, 17)
        Me.cmdBuscaClave.Name = "cmdBuscaClave"
        Me.cmdBuscaClave.Size = New System.Drawing.Size(32, 32)
        Me.cmdBuscaClave.TabIndex = 3
        Me.cmdBuscaClave.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.cmdBuscaClave.Visible = False
        '
        'txtidVCExtra
        '
        Me.txtidVCExtra.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtidVCExtra.ForeColor = System.Drawing.Color.Red
        Me.txtidVCExtra.Location = New System.Drawing.Point(129, 20)
        Me.txtidVCExtra.Name = "txtidVCExtra"
        Me.txtidVCExtra.Size = New System.Drawing.Size(69, 20)
        Me.txtidVCExtra.TabIndex = 2
        Me.txtidVCExtra.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label4.Location = New System.Drawing.Point(6, 23)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(118, 13)
        Me.Label4.TabIndex = 119
        Me.Label4.Text = "Vale Combustible Extra:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label3.Location = New System.Drawing.Point(10, 99)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(113, 13)
        Me.Label3.TabIndex = 116
        Me.Label3.Text = "Unidad de Transporte:"
        '
        'cmbOpciones
        '
        Me.cmbOpciones.FormattingEnabled = True
        Me.cmbOpciones.Location = New System.Drawing.Point(129, 55)
        Me.cmbOpciones.Name = "cmbOpciones"
        Me.cmbOpciones.Size = New System.Drawing.Size(254, 21)
        Me.cmbOpciones.TabIndex = 4
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label2.Location = New System.Drawing.Point(9, 58)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(104, 13)
        Me.Label2.TabIndex = 106
        Me.Label2.Text = "Opcion Combustible:"
        '
        'Label1
        '
        Me.Label1.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label1.Location = New System.Drawing.Point(654, 19)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(54, 20)
        Me.Label1.TabIndex = 103
        Me.Label1.Text = "Estatus:"
        '
        'txtEstatus
        '
        Me.txtEstatus.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtEstatus.Enabled = False
        Me.txtEstatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEstatus.Location = New System.Drawing.Point(714, 16)
        Me.txtEstatus.Name = "txtEstatus"
        Me.txtEstatus.ReadOnly = True
        Me.txtEstatus.Size = New System.Drawing.Size(94, 20)
        Me.txtEstatus.TabIndex = 102
        '
        'Label9
        '
        Me.Label9.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label9.Location = New System.Drawing.Point(538, 58)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(54, 20)
        Me.Label9.TabIndex = 100
        Me.Label9.Text = "Empresa:"
        '
        'txtNomEmpresa
        '
        Me.txtNomEmpresa.Enabled = False
        Me.txtNomEmpresa.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNomEmpresa.Location = New System.Drawing.Point(598, 55)
        Me.txtNomEmpresa.Name = "txtNomEmpresa"
        Me.txtNomEmpresa.ReadOnly = True
        Me.txtNomEmpresa.Size = New System.Drawing.Size(210, 20)
        Me.txtNomEmpresa.TabIndex = 6
        '
        'cmbChofer
        '
        Me.cmbChofer.FormattingEnabled = True
        Me.cmbChofer.Location = New System.Drawing.Point(554, 91)
        Me.cmbChofer.Name = "cmbChofer"
        Me.cmbChofer.Size = New System.Drawing.Size(254, 21)
        Me.cmbChofer.TabIndex = 8
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label37.Location = New System.Drawing.Point(469, 94)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(41, 13)
        Me.Label37.TabIndex = 90
        Me.Label37.Text = "Chofer:"
        '
        'dtpFechaCapturaHora
        '
        Me.dtpFechaCapturaHora.Enabled = False
        Me.dtpFechaCapturaHora.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtpFechaCapturaHora.Location = New System.Drawing.Point(548, 20)
        Me.dtpFechaCapturaHora.Name = "dtpFechaCapturaHora"
        Me.dtpFechaCapturaHora.Size = New System.Drawing.Size(83, 20)
        Me.dtpFechaCapturaHora.TabIndex = 88
        '
        'dtpFechaCap
        '
        Me.dtpFechaCap.Enabled = False
        Me.dtpFechaCap.Location = New System.Drawing.Point(317, 20)
        Me.dtpFechaCap.Name = "dtpFechaCap"
        Me.dtpFechaCap.Size = New System.Drawing.Size(225, 20)
        Me.dtpFechaCap.TabIndex = 87
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label10.Location = New System.Drawing.Point(271, 20)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(40, 13)
        Me.Label10.TabIndex = 89
        Me.Label10.Text = "Fecha:"
        '
        'txtNoViaje
        '
        Me.txtNoViaje.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNoViaje.ForeColor = System.Drawing.Color.Red
        Me.txtNoViaje.Location = New System.Drawing.Point(457, 56)
        Me.txtNoViaje.Name = "txtNoViaje"
        Me.txtNoViaje.Size = New System.Drawing.Size(69, 20)
        Me.txtNoViaje.TabIndex = 5
        Me.txtNoViaje.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label13.Location = New System.Drawing.Point(392, 59)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(59, 13)
        Me.Label13.TabIndex = 78
        Me.Label13.Text = "No. VIAJE:"
        '
        'ConMnuOrdSer
        '
        Me.ConMnuOrdSer.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ModificarToolStripMenuOrdSer, Me.CancelarToolStripMenuOrdSer, Me.ImprimirToolStripMenuOrdSer})
        Me.ConMnuOrdSer.Name = "ConMnuOrdSer"
        Me.ConMnuOrdSer.Size = New System.Drawing.Size(126, 70)
        '
        'ModificarToolStripMenuOrdSer
        '
        Me.ModificarToolStripMenuOrdSer.Name = "ModificarToolStripMenuOrdSer"
        Me.ModificarToolStripMenuOrdSer.Size = New System.Drawing.Size(125, 22)
        Me.ModificarToolStripMenuOrdSer.Text = "Modificar"
        '
        'CancelarToolStripMenuOrdSer
        '
        Me.CancelarToolStripMenuOrdSer.Name = "CancelarToolStripMenuOrdSer"
        Me.CancelarToolStripMenuOrdSer.Size = New System.Drawing.Size(125, 22)
        Me.CancelarToolStripMenuOrdSer.Text = "Cancelar"
        '
        'ImprimirToolStripMenuOrdSer
        '
        Me.ImprimirToolStripMenuOrdSer.Name = "ImprimirToolStripMenuOrdSer"
        Me.ImprimirToolStripMenuOrdSer.Size = New System.Drawing.Size(125, 22)
        Me.ImprimirToolStripMenuOrdSer.Text = "Imprimir"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cmbDespachador)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.dtpHoraComb)
        Me.GroupBox1.Controls.Add(Me.dtpFechaComb)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.txtltCombustible)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Location = New System.Drawing.Point(3, 177)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(854, 152)
        Me.GroupBox1.TabIndex = 76
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Combustible"
        '
        'cmbDespachador
        '
        Me.cmbDespachador.FormattingEnabled = True
        Me.cmbDespachador.Location = New System.Drawing.Point(95, 61)
        Me.cmbDespachador.Name = "cmbDespachador"
        Me.cmbDespachador.Size = New System.Drawing.Size(254, 21)
        Me.cmbDespachador.TabIndex = 10
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label7.Location = New System.Drawing.Point(10, 64)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(74, 13)
        Me.Label7.TabIndex = 94
        Me.Label7.Text = "Despachador:"
        '
        'dtpHoraComb
        '
        Me.dtpHoraComb.Enabled = False
        Me.dtpHoraComb.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtpHoraComb.Location = New System.Drawing.Point(761, 19)
        Me.dtpHoraComb.Name = "dtpHoraComb"
        Me.dtpHoraComb.Size = New System.Drawing.Size(83, 20)
        Me.dtpHoraComb.TabIndex = 12
        '
        'dtpFechaComb
        '
        Me.dtpFechaComb.Enabled = False
        Me.dtpFechaComb.Location = New System.Drawing.Point(530, 19)
        Me.dtpFechaComb.Name = "dtpFechaComb"
        Me.dtpFechaComb.Size = New System.Drawing.Size(225, 20)
        Me.dtpFechaComb.TabIndex = 11
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label6.Location = New System.Drawing.Point(484, 25)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(40, 13)
        Me.Label6.TabIndex = 92
        Me.Label6.Text = "Fecha:"
        '
        'txtltCombustible
        '
        Me.txtltCombustible.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtltCombustible.ForeColor = System.Drawing.Color.Red
        Me.txtltCombustible.Location = New System.Drawing.Point(95, 25)
        Me.txtltCombustible.Name = "txtltCombustible"
        Me.txtltCombustible.Size = New System.Drawing.Size(96, 20)
        Me.txtltCombustible.TabIndex = 9
        Me.txtltCombustible.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label5.Location = New System.Drawing.Point(9, 28)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(52, 13)
        Me.Label5.TabIndex = 80
        Me.Label5.Text = "Cantidad:"
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        '
        'fCombExtraCap
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(873, 341)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.ToolStripMenu)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "fCombExtraCap"
        Me.Text = "Captura de Vale de Combustible Externo"
        Me.ToolStripMenu.ResumeLayout(False)
        Me.ToolStripMenu.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.ConMnuOrdSer.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Sub New(ByVal vTag As String, Optional ByVal vNoServ As Integer = 0)
        InitializeComponent()
        _Tag = vTag
        _NoServ = vNoServ
    End Sub

    '21/SEP/2016
    Public Sub New(con As String, nomUsuario As String, cveEmpresa As Integer, IdUsuario As Integer)
        _con = con
        BD = New CapaDatos.UtilSQL(con, "Juan")
        InitializeComponent()
        _Usuario = nomUsuario
        _cveEmpresa = cveEmpresa
        _IdUsuario = IdUsuario

    End Sub
    'Public Sub New(ByVal vNoServ As Integer)
    '    InitializeComponent()
    '    _NoServ = vNoServ
    'End Sub
    '28/SEP/2016
    'Private Function UltimoMaster() As Integer

    '    Dim Obj As New CapaNegocio.Tablas
    '    Dim Ultimo As Integer = 0
    '    Ultimo = objNumSig.NumSigEnTablas(Inicio.CONSTR, "idPadreOrdSer", "PARAMETROS", "MasterOrdServ", " WHERE IDPARAMETRO = 1", " ORDER BY idPadreOrdSer DESC")
    '    'Ultimo = Obj.NumSigEnTablasSinUpdate(Inicio.CONSTR, "Consecutivo", "detPromociones", " WHERE IdPromocion = " & Promocion)

    '    Return Ultimo
    'End Function

    'Private Function UltimoOrdenTrab() As Integer

    '    Dim Obj As New CapaNegocio.Tablas
    '    Dim Ultimo As Integer = 0
    '    Ultimo = objNumSig.NumSigEnTablas(Inicio.CONSTR, "idOrdenTrabajo", "PARAMETROS", "DetRecepcionOS", " WHERE IDPARAMETRO = 1", " ORDER BY idOrdenTrabajo DESC")
    '    'Ultimo = Obj.NumSigEnTablasSinUpdate(Inicio.CONSTR, "Consecutivo", "detPromociones", " WHERE IdPromocion = " & Promocion)

    '    Return Ultimo
    'End Function

    'Private Function UltimoOrdServ() As Integer
    '    Dim Obj As New CapaNegocio.Tablas
    '    Dim Ultimo As Integer = 0
    '    Ultimo = objNumSig.NumSigEnTablas(Inicio.CONSTR, "idOrdenSer", "PARAMETROS", "CabOrdenServicio", " WHERE IDPARAMETRO = 1", " ORDER BY idOrdenSer DESC")
    '    Return Ultimo
    'End Function


    '21/SEP/2016
    'Private Sub LlenaCheckBox()
    '    TipOrdServ = New TipoServClass(0)
    '    MyTablaTipoOrdServ = TipOrdServ.TablaTiposOrdenServicio("")
    '    If Not MyTablaTipoOrdServ Is Nothing Then
    '        If MyTablaTipoOrdServ.Rows.Count > 0 Then
    '            chkBoxTipoOrdServ.Items.Clear()
    '            chkBoxTipoOrdServ.DisplayMember = "nomTipOrdServ"
    '            For i = 0 To MyTablaTipoOrdServ.Rows.Count - 1

    '                Dim tipoOrden As TiposOrdenServicio = New TiposOrdenServicio()
    '                tipoOrden.idTipoOrden = MyTablaTipoOrdServ.DefaultView.Item(i).Item("idTipOrdServ")
    '                tipoOrden.nomTipOrdServ = MyTablaTipoOrdServ.DefaultView.Item(i).Item("NomTipOrdServ")
    '                tipoOrden.bOrdenTrab = MyTablaTipoOrdServ.DefaultView.Item(i).Item("bOrdenTrab")
    '                chkBoxTipoOrdServ.Items.Add(tipoOrden)

    '                'chkBoxTipoOrdServ.Items.Add(MyTablaTipoOrdServ.DefaultView.Item(i).Item("NomTipOrdServ"))
    '            Next

    '        End If
    '    End If

    'End Sub

    Public Function AutoCompletarFolios(ByVal Control As TextBox, Optional ByVal FiltroSinWhere As String = "") As AutoCompleteStringCollection
        Dim Coleccion As New AutoCompleteStringCollection

        PadreOS = New MasterOrdServClass(0)

        Dim dt As DataTable = PadreOS.tbPadreOS()

        For Each row As DataRow In dt.Rows
            Coleccion.Add(Convert.ToString(row("idPadreOrdSer")))
        Next
        'Ajustamos el control TextBox o ComboBox para recibir los datos de la siguiente manera.
        With Control

            .AutoCompleteCustomSource = Coleccion
            .AutoCompleteMode = AutoCompleteMode.Suggest
            .AutoCompleteSource = AutoCompleteSource.CustomSource
        End With

        ' Aca le devuelvo los datos a recuperados de la Base
        Return Coleccion
    End Function

    Public Function AutoCompletarUnidadTransp(ByVal Control As TextBox, ByVal Activo As Boolean, ByVal idEmpresa As Integer, ByVal Tipo As String, Optional ByVal FiltroSinWhere As String = "") As AutoCompleteStringCollection
        Dim Coleccion As New AutoCompleteStringCollection

        UniTrans = New UnidadesTranspClass(0)

        Dim dt As DataTable = UniTrans.TablaUnidadxTipo(Activo, idEmpresa, Tipo, FiltroSinWhere)
        'Recorrer y cargar los items para el Autocompletado
        For Each row As DataRow In dt.Rows
            Coleccion.Add(Convert.ToString(row("idUnidadTrans")))
        Next
        'Ajustamos el control TextBox o ComboBox para recibir los datos de la siguiente manera.
        With Control

            .AutoCompleteCustomSource = Coleccion
            .AutoCompleteMode = AutoCompleteMode.Suggest
            .AutoCompleteSource = AutoCompleteSource.CustomSource
        End With

        ' Aca le devuelvo los datos a recuperados de la Base
        Return Coleccion
    End Function

    Public Function AutoCompletarPersonal(ByVal Control As ComboBox, ByVal TipoCon As TipotablasPer, ByVal Empresa As Integer) As AutoCompleteStringCollection

        Dim Coleccion As New AutoCompleteStringCollection
        Dim dt As DataTable

        Personal = New PersonalClass(0)
        If TipoCon = TipotablasPer.tpChoferes Then
            dt = Personal.TablaChoferesCombo(True, Empresa)
        ElseIf TipoCon = TipotablasPer.tpSupervisores Then
            dt = Personal.TablaAutorizaSolServ(True, Empresa, 2, 1)
        ElseIf TipoCon = TipotablasPer.tpDespachadores Then
            dt = Personal.TablaAutorizaSolServ(True, Empresa, "42", "")
        End If

        'Recorrer y cargar los items para el Autocompletado
        For Each row As DataRow In dt.Rows
            Coleccion.Add(Convert.ToString(row("NombreCompleto")))
        Next
        'Ajustamos el control TextBox o ComboBox para recibir los datos de la siguiente manera.
        With Control
            .DataSource = dt
            .DisplayMember = "NombreCompleto"
            .ValueMember = "idPersonal"


            .AutoCompleteCustomSource = Coleccion
            .AutoCompleteMode = AutoCompleteMode.Suggest
            .AutoCompleteSource = AutoCompleteSource.CustomSource
        End With

        ' Aca le devuelvo los datos a recuperados de la Base
        Return Coleccion

    End Function

    Private Sub frmDiagnosticoTaller_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'LimpiaCampos()

        Salida = True
        If Inicio.CONSTR = "" Then
            Inicio.CONSTR = _con
        End If

        ComboTipos()
        'LlenaCheckBox()
        'LlenaCombos()
        'Autocompletar()
        Cancelar()
        'CreaTablaProdRefacciones()
        CreaTablaDetalle()

        'Salida = False
        If _NoServ > 0 Then
            'txtOrdenTrabajo.Text = _NoServ
            'txtOrdenTrabajo_Leave(sender, e)
        End If


        'AutoCompletarUnidadesTransp(cmbUnidades, True, 0, NomTipoUnidad)
        'AutoCompletarUnidadTransp(txtTractor, True, _cveEmpresa, "tractor")
        'AutoCompletarUnidadTransp(txtRemolque1, True, _cveEmpresa, "Tolva")
        'AutoCompletarUnidadTransp(txtDolly, True, _cveEmpresa, "Dolly")
        'AutoCompletarUnidadTransp(txtRemolque2, True, _cveEmpresa, "Tolva")

        'AutoCompletarFolios(txtNoViaje)

        'txtNomChofer.AutoCompleteCustomSource = Autocompletar()
        'txtNomChofer.AutoCompleteMode = AutoCompleteMode.Suggest
        'txtNomChofer.AutoCompleteSource = AutoCompleteSource.CustomSource

        'ActivaBotones(False, TipoOpcActivaBoton.tOpcInicializa)
        CreaTablaImprime()

        'Empresa = New EmpresaClass(_cveEmpresa)
        'If Empresa.Existe Then
        '    txtNomEmpresa.Text = Empresa.RazonSocial
        'End If


        Me.Show()
        txtNoViaje.Focus()
        txtNoViaje.SelectAll()
        'txtTractor.Focus()



    End Sub

    'Private Sub LlenaCombos()
    '    Try
    '        Personal = New PersonalClass(0)

    '        MyTablaComboResp = Personal.TablaPersonalCombo(" per.iddepto in (6) and per.idpuesto in (10,11,12,13,14,15,16,17,20,21,22,23,24,25) ")
    '        If Not MyTablaComboResp Is Nothing Then
    '            If MyTablaComboResp.Rows.Count > 0 Then
    '                cmbResponsable.DataSource = MyTablaComboResp
    '                cmbResponsable.ValueMember = "idpersonal"
    '                cmbResponsable.DisplayMember = "nombrecompleto"

    '                cmbPuestoResp.DataSource = MyTablaComboResp
    '                cmbPuestoResp.ValueMember = "idpuesto"
    '                cmbPuestoResp.DisplayMember = "Nompuesto"

    '                'DsComboMovimiento = DsCombo
    '            End If
    '        End If

    '        Personal = New PersonalClass(0)
    '        MyTablaComboAyu1 = Personal.TablaPersonalCombo(" per.iddepto in (6) and per.idpuesto in (10,11,12,13,14,15,16,17,20,21,22,23,24,25) ")
    '        If Not MyTablaComboAyu1 Is Nothing Then
    '            If MyTablaComboAyu1.Rows.Count > 0 Then
    '                cmbayudante1.DataSource = MyTablaComboAyu1
    '                cmbayudante1.ValueMember = "idpersonal"
    '                cmbayudante1.DisplayMember = "nombrecompleto"

    '                cmbPuestoAyu1.DataSource = MyTablaComboAyu1
    '                cmbPuestoAyu1.ValueMember = "idpuesto"
    '                cmbPuestoAyu1.DisplayMember = "Nompuesto"
    '            End If
    '        End If

    '        Personal = New PersonalClass(0)
    '        MyTablaComboAyu2 = Personal.TablaPersonalCombo(" per.iddepto in (6) and per.idpuesto in (10,11,12,13,14,15,16,17,20,21,22,23,24,25) ")
    '        If Not MyTablaComboAyu2 Is Nothing Then
    '            If MyTablaComboAyu2.Rows.Count > 0 Then
    '                cmbayudante2.DataSource = MyTablaComboAyu2
    '                cmbayudante2.ValueMember = "idpersonal"
    '                cmbayudante2.DisplayMember = "nombrecompleto"

    '                cmbPuestoAyu2.DataSource = MyTablaComboAyu2
    '                cmbPuestoAyu2.ValueMember = "idpuesto"
    '                cmbPuestoAyu2.DisplayMember = "Nompuesto"
    '            End If
    '        End If

    '    Catch ex As Exception
    '        MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
    '    End Try
    'End Sub


    Private Sub CreaTablaResultados()
        Try
            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Int64")
            myDataColumn.ColumnName = "trefCIDPRODUCTO"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRefac.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Int64")
            myDataColumn.ColumnName = "trefCIDUNIDADBASE"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRefac.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.String")
            myDataColumn.ColumnName = "trefCCODIGOP01_PRO"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRefac.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.String")
            myDataColumn.ColumnName = "trefCNOMBREP01"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRefac.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Double")
            myDataColumn.ColumnName = "trefCOSTO"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRefac.Columns.Add(myDataColumn)


            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.String")
            myDataColumn.ColumnName = "trefCCLAVEINT"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRefac.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Double")
            myDataColumn.ColumnName = "trefExistencia"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRefac.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Double")
            myDataColumn.ColumnName = "trefCantidad"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRefac.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Boolean")
            myDataColumn.ColumnName = "trefBComprar"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRefac.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Double")
            myDataColumn.ColumnName = "trefCantCompra"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRefac.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Double")
            myDataColumn.ColumnName = "trefCantSalida"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRefac.Columns.Add(myDataColumn)

        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try

    End Sub
    Private Sub LimpiaCampos()
        txtidVCExtra.Text = ""
        dtpFechaCap.Value = Now
        dtpFechaCapturaHora.Value = Now
        txtNomEmpresa.Text = ""
        txtNoViaje.Text = ""
        txtEstatus.Text = ""
        txtltCombustible.Text = ""
        txtUniTrans.Text = ""
        'txtMotivoCancela.Text = ""

        If cmbChofer.Items.Count > 0 Then
            cmbChofer.SelectedIndex = 0
        End If
        If cmbDespachador.Items.Count > 0 Then
            cmbDespachador.SelectedIndex = 0
        End If
        'If cmbAutoriza.Items.Count > 0 Then
        '    cmbAutoriza.SelectedIndex = 0
        'End If

        'AutoCompletarPersonal(cmbChofer, TipotablasPer.tpChoferes, 0)

        'grdOrdenServCab.DataSource = Nothing
        'grdOrdenServCab.Rows.Clear()
        'grdOrdenServDet.DataSource = Nothing
        'grdOrdenServDet.Rows.Clear()


        ' txtTractor.Focus()

    End Sub

    Private Sub txtOrdenTrabajo_KeyDown(sender As Object, e As KeyEventArgs)
        If e.KeyCode = Keys.F3 Then
            cmdBuscaClave_Click(sender, e)
        End If
    End Sub

    'Private Sub txtOrdenTrabajo_KeyPress(sender As Object, e As KeyPressEventArgs)
    '    If Asc(e.KeyChar) = 13 Then
    '        If txtOrdenTrabajo.Text.Trim <> "" Then
    '            CargaForm(txtOrdenTrabajo.Text, "OrdenTrabajo")


    '        End If
    '        e.Handled = True
    '    End If
    'End Sub

    'Private Sub txtOrdenTrabajo_Leave(sender As Object, e As EventArgs)
    '    If Salida Then Exit Sub
    '    If txtOrdenTrabajo.Text.Length = 0 Then
    '        txtOrdenTrabajo.Focus()
    '        txtOrdenTrabajo.SelectAll()
    '    Else
    '        If txtOrdenTrabajo.Enabled Then
    '            CargaForm(txtOrdenTrabajo.Text, "OrdenTrabajo")

    '        End If
    '    End If
    'End Sub

    'Private Sub txtOrdenTrabajo_MouseDown(sender As Object, e As MouseEventArgs)
    '    txtOrdenTrabajo.Focus()
    '    txtOrdenTrabajo.SelectAll()
    'End Sub

    Private Sub cmdBuscaClave_Click(sender As Object, e As EventArgs) Handles cmdBuscaClave.Click
        txtidVCExtra.Text = DespliegaGrid(UCase(TablaBd), Inicio.CONSTR, "idVCExtra")
        Salida = False
        txtidVCExtra_Leave(sender, e)
        Salida = True
        'txtOrdenTrabajo.Text = DespliegaGrid(UCase(TablaBd), Inicio.CONSTR, "")
        'txtidEstado_Leave(sender, e)
    End Sub

    Private Sub CargaForm(ByVal idClave As String, ByVal NomTabla As String, Optional ByVal ValorExtra1 As String = "",
                          Optional ByVal txtControl As ComboBox = Nothing, Optional ByVal FiltroSinWhere As String = "",
                          Optional ByVal bRequerido As Boolean = False)

        Try
            Select Case UCase(NomTabla)

                Case UCase("cargaCombustibleExtra")
                    CombExt = New CombustibleExtraClass(Val(idClave))
                    If CombExt.Existe Then
                        dtpFechaCap.Value = CombExt.FechaCrea
                        dtpFechaCapturaHora.Value = CombExt.FechaCrea
                        txtUniTrans.Text = CombExt.idUnidadTrans
                        cmbOpciones.SelectedValue = CombExt.idTipoOSComb
                        If CombExt.NumViaje = 0 Then
                            txtNoViaje.Text = ""
                        Else
                            txtNoViaje.Text = CombExt.NumViaje
                        End If
                        UniTrasp = New UnidadesTranspClass(CombExt.idUnidadTrans)
                        If UniTrasp.Existe Then
                            Dim Empresa As New EmpresaClass(UniTrasp.idEmpresa)
                            If Empresa.Existe Then
                                txtNomEmpresa.Text = UCase(Empresa.RazonSocial)
                            End If
                        End If

                        cmbChofer.SelectedValue = CombExt.idChofer

                        If CombExt.ltCombustible = 0 And CombExt.idDespachador = 0 Then
                            PrecioCombustible = CombExt.UltPrecioCombustible()

                            If PrecioCombustible > 0 Then
                                ActivaCampos(True, TipoOpcActivaCampos.tOpcINICIALIZA)

                                ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)
                                txtltCombustible.Focus()

                            Else
                                'Mensaje
                            End If

                        Else
                            txtltCombustible.Text = CombExt.ltCombustible
                            cmbDespachador.SelectedValue = CombExt.idDespachador
                            Timer1.Stop()
                            dtpFechaComb.Value = CombExt.fechaCombustible
                            dtpHoraComb.Value = CombExt.fechaCombustible

                            ActivaCampos(True, TipoOpcActivaCampos.tOpcDESHABTODOS)

                            ActivaBotones(False, TipoOpcActivaBoton.tOpcEspecial)

                        End If


                        'cmbUnidades.SelectedValue = CombExt.idUnidadTrans






                    Else

                        'No existe

                    End If

                Case UCase("VIAJE")
                    'CHECAR EL VIAJE
                    Dim vObj = New Nombre.Interfaces.CartaPorteSvc().getCartaPorteById(Convert.ToInt32(idClave))
                    If vObj.valor = 0 Then
                        'Dim listObj As List(Of UnidadesTranspClass) = New List(Of UnidadesTranspClass)()
                        'Dim Lista As List(Of Nombre.Models.Viaje) = New List(Of Nombre.Models.Viaje)
                        '        listObj.Add(CType(obj, TiposOrdenServicio))
                        Dim Viaje As Nombre.Models.Viaje
                        Viaje = CType(vObj.result, List(Of Nombre.Models.Viaje))(0)

                        If Viaje.EstatusGuia = "ACTIVO" Then
                            'chofer
                            cmbChofer.SelectedValue = Viaje.operador.clave

                            Dim Empresa As New EmpresaClass(Viaje.IdEmpresa)
                            If Empresa.Existe Then
                                txtNomEmpresa.Text = UCase(Empresa.RazonSocial)

                            End If
                            txtEstatus.Text = Viaje.EstatusGuia


                            'unidades
                            Dim listObj As List(Of UnidadesTranspClass) = New List(Of UnidadesTranspClass)()
                            For Each lsdet In Viaje.listDetalles
                                If TipoUnidad = "RE" Then
                                    If lsdet.producto.combustible Then
                                        UniTrans = New UnidadesTranspClass(Viaje.remolque.clave)
                                        listObj.Add(UniTrans)
                                        If Viaje.remolque2 IsNot Nothing Then
                                            UniTrans = New UnidadesTranspClass(Viaje.remolque2.ToString())
                                            listObj.Add(UniTrans)
                                        End If
                                    Else
                                        MsgBox("En el No. Viaje : " & idClave & " Los productos no requieren de Combustible" & vbCrLf & "Verifique los Productos del Viaje ")

                                    End If
                                ElseIf TipoUnidad = "TC" Then
                                    UniTrans = New UnidadesTranspClass(Viaje.tractor.clave)

                                    listObj.Add(UniTrans)
                                End If
                            Next
                            'LlenaComboUnidaesxViaje(listObj)
                            ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)


                            'cmbUnidades.Enabled = True
                            'cmbUnidades.Focus()

                        Else
                            MsgBox("El No. Viaje : " & idClave & " No esta activo")
                        End If
                    ElseIf vObj.valor = 3 Then
                        MsgBox("El No. Viaje : " & idClave & " No Existe")
                    End If



                Case UCase("FOLIOOS")
                    If Val(idClave) > 0 Then
                        PadreOS = New MasterOrdServClass(Val(idClave))
                        If PadreOS.Existe Then
                            'txtTractor.Text = PadreOS.idTractor
                            'txtRemolque1.Text = PadreOS.idTolva1
                            'txtDolly.Text = PadreOS.idDolly
                            'txtRemolque2.Text = PadreOS.idTolva2
                            dtpFechaCap.Value = PadreOS.FechaCreacion
                            dtpFechaCapturaHora.Value = PadreOS.FechaCreacion
                            txtEstatus.Text = PadreOS.Estatus
                            'txtMotivoCancela.Text = PadreOS.MotivoCancela
                            'txtKilometraje.Text = PadreOS.Kilometraje
                            'cmbAutoriza.SelectedValue = PadreOS.idEmpleadoAutoriza

                            Dim tablaTipOS As DataTable = PadreOS.tbTipoOrdSerxPadre(Val(idClave))
                            'If tablaTipOS.Rows.Count > 0 Then
                            '    Dim list = chkBoxTipoOrdServ.Items

                            '    Dim listObj As List(Of TiposOrdenServicio) = New List(Of TiposOrdenServicio)()
                            '    For Each obj In list
                            '        listObj.Add(CType(obj, TiposOrdenServicio))
                            '    Next

                            '    Dim i As Integer = 0
                            '    For Each lobj In listObj
                            '        For Each row As DataRow In tablaTipOS.Rows
                            '            If lobj.idTipoOrden = row("idTipOrdServ") Then
                            '                chkBoxTipoOrdServ.SetItemChecked(i, True)
                            '            End If
                            '        Next
                            '        i = i + 1
                            '    Next
                            'End If
                            'cmbAutoriza.SelectedValue = PadreO  
                            'Aqui se Consulta las Ordenes de Servicio
                            Dim TablaOrdSer As DataTable = PadreOS.tbOrdSerxPadre(Val(idClave))
                            If TablaOrdSer.Rows.Count > 0 Then
                                For i = 0 To TablaOrdSer.Rows.Count - 1
                                    '.ToString()
                                    'idTipoServicio
                                    'InsertaRegGridCab(TablaOrdSer.DefaultView.Item(i).Item("CLAVE"), TablaOrdSer.DefaultView.Item(i).Item("idTipOrdServ"),
                                    'TablaOrdSer.DefaultView.Item(i).Item("NomTipOrdServ").ToString(), TablaOrdSer.DefaultView.Item(i).Item("idUnidadTrans").ToString(),
                                    'TablaOrdSer.DefaultView.Item(i).Item("idTipoUnidad"), TablaOrdSer.DefaultView.Item(i).Item("nomTipoUniTras").ToString(),
                                    'TablaOrdSer.DefaultView.Item(i).Item("Kilometraje"), TablaOrdSer.DefaultView.Item(i).Item("idEmpleadoAutoriza"),
                                    'TablaOrdSer.DefaultView.Item(i).Item("bOrdenTrab"), TablaOrdSer.DefaultView.Item(i).Item("idTipoServicio"), False, False)


                                Next
                            End If

                            'Dim vValorServ As Integer

                            Dim TablaDetOrdSer As DataTable = PadreOS.tbDetOrdSerxPadre(Val(idClave))
                            If TablaDetOrdSer.Rows.Count > 0 Then
                                For i = 0 To TablaDetOrdSer.Rows.Count - 1
                                    If TablaDetOrdSer.DefaultView.Item(i).Item("IDSERVICIO") = 0 Then
                                        vValorNomAct = TablaDetOrdSer.DefaultView.Item(i).Item("NOTARECEPCION")
                                        vValorNomDivision = TablaDetOrdSer.DefaultView.Item(i).Item("NOMDIVISIONOT")
                                        vValorDivision = TablaDetOrdSer.DefaultView.Item(i).Item("IDDIVISIONOT")
                                    Else
                                        vValorNomAct = TablaDetOrdSer.DefaultView.Item(i).Item("NOMBREACT")
                                        vValorNomDivision = TablaDetOrdSer.DefaultView.Item(i).Item("NOMDIVISION")
                                        vValorDivision = TablaDetOrdSer.DefaultView.Item(i).Item("IDDIVISION")

                                    End If

                                    'InsertaRegGridDet(TablaDetOrdSer.DefaultView.Item(i).Item("CLAVE"),
                                    'TablaDetOrdSer.DefaultView.Item(i).Item("IDSERVICIO"),
                                    'TablaDetOrdSer.DefaultView.Item(i).Item("NOMSERVICIO"),
                                    'TablaDetOrdSer.DefaultView.Item(i).Item("IDACTIVIDAD"),
                                    'vValorNomAct,
                                    'vValorDivision,
                                    'vValorNomDivision,
                                    'TablaDetOrdSer.DefaultView.Item(i).Item("DURACIONACTHR"),
                                    'TablaDetOrdSer.DefaultView.Item(i).Item("IDORDENSER"),
                                    'TablaDetOrdSer.DefaultView.Item(i).Item("IDORDENTRABAJO"), False,
                                    'TablaDetOrdSer.DefaultView.Item(i).Item("idOrdenActividad"))

                                Next
                            End If
                            'Clave = grdOrdenServCab.Item("oscCLAVE", grdOrdenServCab.CurrentCell.RowIndex).Value
                            'FiltraDetallexClave(Clave)

                        Else

                        End If

                    Else
                        If bRequerido Or Val(idClave) > 0 Then
                            'txtidPadreOrdSer.Text = DespliegaGrid(UCase("UNITRANSTIPO"), Inicio.CONSTR, "", FiltroSinWhere)
                            txtNoViaje.Text = DespliegaGrid(UCase("FolioOS"), Inicio.CONSTR, "idPadreOrdSer")
                            If txtNoViaje.Text <> "" Then
                                CargaForm(txtNoViaje.Text, "FOLIOOS")
                            End If
                        End If
                    End If
                Case UCase("UNITRANS")

                    If Val(idClave) > 0 Then
                        UniTrans = New UnidadesTranspClass(Val(idClave), ValorExtra1)
                    Else
                        UniTrans = New UnidadesTranspClass(idClave)
                    End If

                    If UniTrans.Existe Then
                        txtControl.Text = UniTrans.iUnidadTrans

                        TipoUniTrans = New TipoUniTransClass(UniTrans.idTipoUnidad)

                        If TipoUniTrans.Existe Then
                            If UCase(TipoUniTrans.Clasificacion) = UCase("Tractor") Then
                                vUltKm = UniTrans.UltKilomValidado(UniTrans.iUnidadTrans)
                            End If

                        End If

                        Empresa = New EmpresaClass(UniTrans.idEmpresa)
                        If Empresa.Existe Then
                            txtNomEmpresa.Text = Empresa.RazonSocial
                        End If

                        AutoCompletarPersonal(cmbChofer, TipotablasPer.tpChoferes, UniTrans.idEmpresa)

                    Else
                        'txtControl.Text = DespliegaGrid(UCase("UNITRANSTIPO"), Inicio.CONSTR, "", "tip.clasificacion = 'tractor' AND UNI.ESTATUS = 'ACT' AND UNI.IDEMPRESA = " & _Usuario.personal.idEmpresa & " AND UNI.IDUNIDADTRANS LIKE '%" & Val(idClave) & "%'", "TRACTOR")
                        If bRequerido Or Val(idClave) > 0 Then
                            txtControl.Text = DespliegaGrid(UCase("UNITRANSTIPO"), Inicio.CONSTR, "", FiltroSinWhere)
                            If txtControl.Text <> "" Then
                                CargaForm(txtControl.Text, "UNITRANS", ValorExtra1, txtControl)
                            End If
                        End If
                        'MsgBox("La Unidad de Transporte : " & idClave & " No Existe")
                        'txtControl.Focus()
                        'txtControl.SelectAll()
                    End If


                    'Case UCase("UNIPROD_COM")
                    '    UnidadCom = New UnidadesComClass(idClave, "CCLAVEINT", TipoDato.TdCadena)
                    '    If UnidadCom.Existe Then
                    '        txtNomUniProd.Text = UnidadCom.CNOMBREUNIDAD
                    '        vCIDUNIDAD = UnidadCom.CIDUNIDAD
                    '    End If
                    'Case UCase("PROD_COM")
                    '    ProdCom = New ProductosComClass(idClave, "")
                    '    If ProdCom.Existe Then
                    '        If ProdCom.CSTATUSPRODUCTO = 1 Then
                    '            vCIDPRODUCTO = ProdCom.CIDPRODUCTO
                    '            txtNomProducto.Text = ProdCom.CNOMBREPRODUCTO
                    '            UnidadCom = New UnidadesComClass(ProdCom.CIDUNIDADBASE, "CIDUNIDAD", TipoDato.TdNumerico)
                    '            If UnidadCom.Existe Then
                    '                txtIdUnidadProd.Text = UnidadCom.CCLAVEINT
                    '            End If

                    '        Else
                    '            'Desactivado
                    '        End If
                    '    End If
                Case UCase("OrdenServicio")
                    OrdenServ = New OrdenServClass(idClave)
                    With OrdenServ
                        txtNoViaje.Text = idClave
                        If .Existe Then
                            txtNoViaje.Text = .idOrdenSer
                            'dtpFechaOrden.Value = OrdenServ.FechaRecepcion
                            'dtpFechaOrdenhr.Value = OrdenServ.FechaRecepcion

                            Emp = New EmpresaClass(OrdenServ.IdEmpresa)
                            If Emp.Existe Then
                                txtNomEmpresa.Text = Emp.RazonSocial
                            End If

                            TipOrd = New TipoOrdenClass(OrdenServ.idTipoOrden)
                            'If TipOrd.Existe Then
                            '    txtTipoOrden.Text = TipOrd.NomTipoOrden
                            'End If
                            'txtEstatusOrden.Text = OrdenServ.Estatus

                            '10/ENE/2017
                            'txtKilometraje.Text = OrdenServ.Kilometraje

                            'RECEPCIONO
                            'User = New UsuarioClass(OrdenServ.UsuarioRecepcion)
                            'If User.Existe Then
                            '    txtRecepciono.Text = User.NombreUsuario
                            'End If
                            'ENTREGO
                            'Personal = New PersonalClass(OrdenServ.idEmpleadoEntrega)
                            'If Personal.Existe Then
                            '    txtEntrego.Text = Personal.NombreCompleto
                            'End If
                            'AUTORIZO
                            'Personal = New PersonalClass(OrdenServ.idEmpleadoAutoriza)
                            'If Personal.Existe Then
                            '    txtAutorizo.Text = Personal.NombreCompleto
                            'End If
                        End If
                    End With
                Case UCase("OrdenTrabajo")
                    OrdenTrabajo = New OrdenTrabajoClass(idClave)
                    With OrdenTrabajo
                        'ORDEN DE TRABAJO
                        'txtOrdenTrabajo.Text = idClave
                        If .Existe Then
                            'MECANICO RESPONSABLE
                            'Personal = New PersonalClass(.idPersonalResp)
                            'cmbResponsable.SelectedValue = .idPersonalResp
                            'If Personal.Existe Then
                            '    'txtNombreMecanico.Text = Personal.NombreCompleto

                            '    PersonalPuesto = New PuestosClass(Personal.idPuesto)
                            '    If PersonalPuesto.Existe Then
                            '        'txtPuestoMec.Text = PersonalPuesto.NomPuesto
                            '    End If
                            'End If
                            ''AYUDANTE 1
                            'If .idPersonalAyu1 > 0 Then
                            '    chkAyu1.Checked = True
                            '    Personal = New PersonalClass(.idPersonalAyu1)
                            '    cmbayudante1.SelectedValue = .idPersonalAyu1
                            'Else
                            '    chkAyu1.Checked = False
                            'End If

                            ''AYUDANTE 2
                            'If .idPersonalAyu2 > 0 Then
                            '    chkAyu2.Checked = True
                            '    Personal = New PersonalClass(.idPersonalAyu2)
                            '    cmbayudante2.SelectedValue = .idPersonalAyu2
                            'Else
                            '    chkAyu2.Checked = False
                            'End If


                            'DETALLE ORDEN DE SERVICIO
                            DetOS = New detOrdenServicioClass(idClave)
                            If DetOS.Existe Then
                                dtpFechaCap.Value = DetOS.FechaAsig
                                dtpFechaCapturaHora.Value = DetOS.FechaAsig
                                User = New UsuarioClass(DetOS.UsuarioAsig)
                                'If User.Existe Then
                                '    txtUsuarioAsig.Text = User.NombreUsuario
                                'End If
                            End If

                            'ORDEN DE SERVICIO
                            OrdenServ = New OrdenServClass(.idOrdenSer)

                            txtNoViaje.Text = .idOrdenSer
                            'dtpFechaOrden.Value = OrdenServ.FechaRecepcion
                            'dtpFechaOrdenhr.Value = OrdenServ.FechaRecepcion

                            Emp = New EmpresaClass(OrdenServ.IdEmpresa)
                            If Emp.Existe Then
                                txtNomEmpresa.Text = Emp.RazonSocial
                            End If

                            'TipOrd = New TipoOrdenClass(OrdenServ.idTipoOrden)
                            'If TipOrd.Existe Then
                            '    txtTipoOrden.Text = TipOrd.NomTipoOrden
                            'End If
                            'txtEstatusOrden.Text = OrdenServ.Estatus

                            '10/ENE/2017
                            'txtKilometraje.Text = OrdenServ.Kilometraje

                            'RECEPCIONO
                            'User = New UsuarioClass(OrdenServ.UsuarioRecepcion)
                            'If User.Existe Then
                            '    txtRecepciono.Text = User.NombreUsuario
                            'End If
                            'ENTREGO
                            'Personal = New PersonalClass(OrdenServ.idEmpleadoEntrega)
                            'If Personal.Existe Then
                            '    txtEntrego.Text = Personal.NombreCompleto
                            'End If
                            'AUTORIZO
                            'Personal = New PersonalClass(OrdenServ.idEmpleadoAutoriza)
                            'If Personal.Existe Then
                            '    txtAutorizo.Text = Personal.NombreCompleto
                            'End If
                            'ORDEN DE SERVICIO

                            'UNIDAD DE TRANSPORTE
                            'txtIdUnidad.Text = OrdenServ.idUnidadTrans
                            'UniTrans = New UnidadesTranspClass(OrdenServ.idUnidadTrans)
                            'If UniTrans.Existe Then
                            '    txtNomUnidad.Text = UniTrans.DescripcionUni
                            '    Marca = New MarcaClass(UniTrans.idMarca)
                            '    If Marca.Existe Then
                            '        txtMarcaUni.Text = Marca.NOMBREMARCA
                            '    End If
                            '    'ULTIMA ORDEN DE SERVICIO

                            '    If UniTrans.UltOT > 0 Then
                            '        txtUltOrdenTrabajo.Text = UniTrans.UltOT
                            '        ULTOT = New OrdenTrabajoClass(UniTrans.UltOT)
                            '        If ULTOT.Existe Then
                            '            txtUltimaOrdenServ.Text = ULTOT.idOrdenSer
                            '            Personal = New PersonalClass(ULTOT.idPersonalResp)
                            '            If Personal.Existe Then
                            '                txtUltimoMecanico.Text = Personal.NombreCompleto
                            '            End If

                            '            txtUltFalla.Text = ULTOT.NotaRecepcion
                            '            FamiliaOT = New FamiliasClass(ULTOT.idFamilia)
                            '            If FamiliaOT.Existe Then
                            '                txtUltFamilia.Text = FamiliaOT.NomFamilia
                            '                txtUltDivision.Text = FamiliaOT.NomDivision
                            '            End If
                            '        End If
                            '    Else
                            '        txtUltOrdenTrabajo.Text = ""
                            '    End If
                            'End If


                            'Historial
                            'CargaOrdenesSerHis(OrdenServ.idUnidadTrans)

                            'IdOrdenServ = grdOrdenServCab.Item("hosIdServicio", grdOrdenServCab.CurrentCell.RowIndex).Value
                            'CargaOrdenesTrabHis(txtIdUnidad.Text, IdOrdenServ)

                            'VALIDACIONES
                            If OrdenServ.Estatus = "ASIG" Then
                                ActivaCampos(False, TipoOpcActivaCampos.tOpcINICIALIZA)
                                ActivaBotones(True, TipoOpcActivaBoton.tOpcInicializa)

                                'txtDuracion.Focus()
                                'txtDuracion.SelectAll()
                            ElseIf OrdenServ.Estatus = "DIA" Then
                                MsgBox("La Orden de Trabajo: " & idClave & " ya Esta en Proceso, No puede Modificarse")
                                ActivaCampos(False, TipoOpcActivaCampos.tOpcINICIALIZA)

                                ActivaBotones(False, TipoOpcActivaBoton.tOpcInicializa)
                                btnMnuLimpiar.Enabled = True
                                'txtOrdenTrabajo.Focus()
                                'txtOrdenTrabajo.SelectAll()

                            ElseIf OrdenServ.Estatus = "REC" Then
                                MsgBox("La Orden de Trabajo: " & idClave & " No esta Asignada, No puede Modificarse")
                                'txtOrdenTrabajo.Focus()
                                'txtOrdenTrabajo.SelectAll()

                            End If
                        Else
                            'No existe 
                            MsgBox("La Orden de Trabajo: " & idClave & " No Existe !!!")
                            'txtOrdenTrabajo.Focus()
                            'txtOrdenTrabajo.SelectAll()
                        End If

                    End With
                    'Case UCase(TablaBd2)
                    '    'aqui()
                    '    TipoServ = New TipoServClass(idClave)
                    '    With TipoServ
                    '        txtidTipoUnidad.Text = .idTipoServicio
                    '        If .Existe Then
                    '            txtnomTipoUniTras.Text = .NomTipoServicio
                    '            txtDescripcionUni.Focus()
                    '            txtDescripcionUni.SelectAll()
                    '        Else
                    '            MsgBox("El Tipo de Servicio con id: " & txtidTipoUnidad.Text & " No Existe")
                    '            txtidTipoUnidad.Focus()
                    '            txtidTipoUnidad.SelectAll()
                    '        End If
                    '    End With

            End Select



        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try

    End Sub

    Private Sub txtOrdenTrabajo_TextChanged(sender As Object, e As EventArgs)

    End Sub

    Private Sub btnMnuSalir_Click(sender As Object, e As EventArgs) Handles btnMnuSalir.Click
        Me.Close()
    End Sub

    Private Sub btnMnuCancelar_Click(sender As Object, e As EventArgs) Handles btnMnuLimpiar.Click

        Cancelar()
        'txtTractor.Focus()
    End Sub

    'Private Sub CargaOrdenesSerHis(ByVal vidUnidadTrans As String)
    '    Try
    '        MyTablaHisOS.Rows.Clear()
    '        MyTablaHisOS = DetOS.CargaOrdSerxUni(vidUnidadTrans)
    '        grdOrdenServCab.DataSource = Nothing
    '        grdOrdenServCab.Rows.Clear()

    '        If MyTablaHisOS.Rows.Count > 0 Then
    '            For i = 0 To MyTablaHisOS.DefaultView.Count - 1
    '                grdOrdenServCab.Rows.Add()
    '                grdOrdenServCab.Item("hosConsecutivo", i).Value = i + 1
    '                grdOrdenServCab.Item("hosIdServicio", i).Value = MyTablaHisOS.DefaultView.Item(i).Item("idOrdenSer")
    '                grdOrdenServCab.Item("hosFechaRecepcion", i).Value = MyTablaHisOS.DefaultView.Item(i).Item("FechaRecepcion")
    '                grdOrdenServCab.Item("hosTipoOrden", i).Value = MyTablaHisOS.DefaultView.Item(i).Item("idTipoOrden")
    '                grdOrdenServCab.Item("hosKm", i).Value = MyTablaHisOS.DefaultView.Item(i).Item("Kilometraje")
    '                grdOrdenServCab.Item("hosAutorizo", i).Value = MyTablaHisOS.DefaultView.Item(i).Item("idEmpleadoAutoriza")
    '                grdOrdenServCab.Item("hosEntrego", i).Value = MyTablaHisOS.DefaultView.Item(i).Item("idEmpleadoEntrega")
    '                grdOrdenServCab.Item("hosRecepciono", i).Value = MyTablaHisOS.DefaultView.Item(i).Item("UsuarioRecepcion")
    '                grdOrdenServCab.Item("hosFinalizo", i).Value = MyTablaHisOS.DefaultView.Item(i).Item("UsuarioEntrega")
    '            Next
    '            grdOrdenServCab.PerformLayout()
    '        End If

    '    Catch ex As Exception

    '    End Try
    'End Sub
    'Private Sub CargaOrdenesTrabHis(ByVal vidUnidadTrans As String, Optional ByVal vidOrdenSer As Integer = 0)
    '    Try
    '        MyTablaHisOT.Rows.Clear()
    '        'grdOrdenServHis.DataSource = DetOS.CargaOrdSerxUni(OrdenServ.idUnidadTrans)
    '        MyTablaHisOT = DetOS.CargaOrdTrabxUni(vidUnidadTrans, vidOrdenSer)
    '        grdOrdenServDet.DataSource = Nothing
    '        grdOrdenServDet.Rows.Clear()

    '        If MyTablaHisOT.Rows.Count > 0 Then
    '            For i = 0 To MyTablaHisOT.DefaultView.Count - 1
    '                grdOrdenServDet.Rows.Add()
    '                'grdOrdenServHis.Item("hosIdServicio", i).Value = Trim(MyTabla.DefaultView.Item(i).Item("idOrdenSer").ToString)
    '                grdOrdenServDet.Item("hotIdOrdenSer", i).Value = MyTablaHisOT.DefaultView.Item(i).Item("idOrdenSer")
    '                grdOrdenServDet.Item("hotidOrdenTrabajo", i).Value = MyTablaHisOT.DefaultView.Item(i).Item("idOrdenTrabajo")
    '                grdOrdenServDet.Item("hotFechaFin", i).Value = MyTablaHisOT.DefaultView.Item(i).Item("FechaTerminado")
    '                grdOrdenServDet.Item("hotAsigno", i).Value = MyTablaHisOT.DefaultView.Item(i).Item("UsuarioAsig")
    '                grdOrdenServDet.Item("hotMecanico", i).Value = MyTablaHisOT.DefaultView.Item(i).Item("idPersonalResp")
    '                grdOrdenServDet.Item("hotDuracion", i).Value = MyTablaHisOT.DefaultView.Item(i).Item("DuracionActHr")
    '            Next
    '            grdOrdenServDet.PerformLayout()
    '        End If

    '    Catch ex As Exception

    '    End Try
    'End Sub

    Private Sub grdOrdenServHis_CellContentClick(sender As Object, e As DataGridViewCellEventArgs)

    End Sub

    'Private Sub grdOrdenServHis_Click(sender As Object, e As EventArgs)
    '    IdOrdenServ = grdOrdenServCab.Item("hosIdServicio", grdOrdenServCab.CurrentCell.RowIndex).Value

    'End Sub
    Private Sub EncuentraDetalle(ByVal idOrdenSer As Integer)

        If MyTablaHisOT.DefaultView.Count > 0 Then
            With MyTablaHisOT
                .DefaultView.RowFilter = "idOrdenSer = " & idOrdenSer
                If .DefaultView.Count > 0 Then
                    'grdOrdenTrabajoHis.DataSource = .DefaultView
                End If
            End With
        End If

    End Sub


    'Private Sub cmdBuscaProd_Click(sender As Object, e As EventArgs)
    '    Salida = True
    '    txtIdProducto.Text = DespliegaGrid(UCase("PROD_COM"), Inicio.CONSTR_COM, "CCODIGOPRODUCTO")
    '    If txtIdProducto.Text <> "" Then CargaForm(txtIdProducto.Text, UCase("PROD_COM"))
    '    CargaForm(txtIdUnidadProd.Text, UCase("UNIPROD_COM"))

    '    txtCantidad.Focus()
    '    txtCantidad.SelectAll()
    '    Salida = False
    'End Sub

    'Private Sub txtIdProducto_EnabledChanged(sender As Object, e As EventArgs)
    '    cmdBuscaProd.Enabled = txtIdProducto.Enabled
    'End Sub

    'Private Sub txtIdProducto_KeyDown(sender As Object, e As KeyEventArgs)
    '    If e.KeyCode = Keys.F3 Then
    '        cmdBuscaProd_Click(sender, e)
    '    End If

    'End Sub

    'Private Sub txtIdProducto_KeyPress(sender As Object, e As KeyPressEventArgs)
    '    If txtIdProducto.Text.Length = 0 Then
    '        txtIdProducto.Focus()
    '        txtIdProducto.SelectAll()
    '    Else
    '        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
    '            Salida = True
    '            CargaForm(txtIdProducto.Text, UCase("PROD_COM"))
    '            CargaForm(txtIdUnidadProd.Text, UCase("UNIPROD_COM"))

    '            txtCantidad.Focus()
    '            txtCantidad.SelectAll()
    '            Salida = False
    '        End If
    '    End If
    'End Sub

    'Private Sub txtIdProducto_Leave(sender As Object, e As EventArgs)
    '    If Salida Then Exit Sub
    '    If txtIdProducto.Text.Length = 0 Then
    '        txtIdProducto.Focus()
    '        txtIdProducto.SelectAll()
    '    Else
    '        If txtIdProducto.Enabled Then
    '            CargaForm(txtIdProducto.Text, UCase("PROD_COM"))
    '            CargaForm(txtIdUnidadProd.Text, UCase("UNIPROD_COM"))

    '            txtCantidad.Focus()
    '            txtCantidad.SelectAll()
    '        End If
    '    End If
    'End Sub


    'Private Sub cmdBuscaUniProd_Click(sender As Object, e As EventArgs)
    '    Salida = True
    '    txtIdProducto.Text = DespliegaGrid(UCase("UNIPROD_COM"), Inicio.CONSTR_COM, "CCODIGOPRODUCTO")
    '    If txtIdProducto.Text <> "" Then CargaForm(txtIdProducto.Text, UCase("PROD_COM"))
    '    txtCantidad.Focus()
    '    txtCantidad.SelectAll()
    '    Salida = False
    'End Sub

    'Private Sub txtIdUnidadProd_EnabledChanged(sender As Object, e As EventArgs)
    '    cmdBuscaUniProd.Enabled = txtIdUnidadProd.Enabled
    'End Sub

    'Private Sub txtIdUnidadProd_KeyDown(sender As Object, e As KeyEventArgs)
    '    If e.KeyCode = Keys.F3 Then
    '        cmdBuscaUniProd_Click(sender, e)
    '    End If

    'End Sub

    'Private Sub txtIdUnidadProd_KeyPress(sender As Object, e As KeyPressEventArgs)
    '    If txtIdUnidadProd.Text.Length = 0 Then
    '        txtIdUnidadProd.Focus()
    '        txtIdUnidadProd.SelectAll()
    '    Else
    '        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
    '            Salida = True
    '            CargaForm(txtIdUnidadProd.Text, UCase("UNIPROD_COM"))

    '            txtCantidad.Focus()
    '            txtCantidad.SelectAll()
    '            Salida = False
    '        End If
    '    End If

    'End Sub

    'Private Sub txtIdUnidadProd_Leave(sender As Object, e As EventArgs)
    '    If Salida Then Exit Sub
    '    If txtIdUnidadProd.Text.Length = 0 Then
    '        txtIdUnidadProd.Focus()
    '        txtIdUnidadProd.SelectAll()
    '    Else
    '        If txtIdProducto.Enabled Then
    '            CargaForm(txtIdUnidadProd.Text, UCase("UNIPROD_COM"))
    '            txtCantidad.Focus()
    '            txtCantidad.SelectAll()
    '        End If
    '    End If
    'End Sub

    'Private Sub txtCantidad_KeyPress(sender As Object, e As KeyPressEventArgs)
    '    If txtCantidad.Text.Length = 0 Then
    '        txtCantidad.Focus()
    '        txtCantidad.SelectAll()
    '    Else
    '        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
    '            Salida = True
    '            'CargaForm(txtIdUnidadProd.Text, UCase("UNIPROD_COM"))
    '            'Agrega a Grid
    '            GrabaProductos()
    '            BorraCamposProd()

    '            txtIdProducto.Focus()
    '            txtIdProducto.SelectAll()
    '            Salida = False
    '        End If
    '    End If
    'End Sub

    'Private Sub GrabaProductos()
    '    GrabaGridProductos()
    'End Sub

    'Private Sub GrabaGridProductos()
    '    Dim BandGraba As Boolean = False
    '    Dim vExistencia As Double = 0
    '    Dim vCosto As Double = 0
    '    Dim CantCompra As Double = 0
    '    Dim CantSalida As Double = 0
    '    Try
    '        If txtIdProducto.Text <> "" Then
    '            If (txtCantidad.Text <> CantidadGrid) Or txtCantidad.Text = 0 Then
    '                BandGraba = True
    '            End If

    '            If BandGraba Then
    '                If grdProductosOT.Rows.Count = 0 Then
    '                    MyTablaRefac.Clear()
    '                    BandNoExisteProdEnGrid = True
    '                End If
    '                'Determinar si el producto existe en el grid, si no existe hay que agregarlo
    '                If BandNoExisteProdEnGrid Then
    '                    Salida = True
    '                    myDataRow = MyTablaRefac.NewRow()
    '                    '
    '                    myDataRow("trefCIDPRODUCTO") = vCIDPRODUCTO
    '                    myDataRow("trefCIDUNIDADBASE") = vCIDUNIDAD

    '                    myDataRow("trefCCODIGOP01_PRO") = txtIdProducto.Text
    '                    myDataRow("trefCNOMBREP01") = txtNomProducto.Text

    '                    'Calcula Existencia
    '                    vExistencia = ObjCom.Existencia(vCIDPRODUCTO, Now, vIdAlmacen)
    '                    myDataRow("trefExistencia") = vExistencia

    '                    myDataRow("trefCantidad") = Val(txtCantidad.Text)
    '                    myDataRow("trefCCLAVEINT") = txtIdUnidadProd.Text
    '                    myDataRow("trefBComprar") = IIf(vExistencia < Val(txtCantidad.Text), True, False)

    '                    'Calcula Costo
    '                    vCosto = ObjCom.ChecaCosto(vCIDPRODUCTO, Now, vIdAlmacen)
    '                    myDataRow("trefCOSTO") = vCosto

    '                    'Cantidades
    '                    'Por Comprar y Salidas
    '                    If vExistencia >= Val(txtCantidad.Text) Then
    '                        CantCompra = 0
    '                        CantSalida = Val(txtCantidad.Text)
    '                    Else
    '                        CantCompra = Val(txtCantidad.Text) - vExistencia
    '                        CantSalida = vExistencia
    '                    End If
    '                    myDataRow("trefCantCompra") = CantCompra
    '                    myDataRow("trefCantSalida") = CantSalida

    '                    MyTablaRefac.Rows.Add(myDataRow)

    '                    'Grabamos Producto
    '                    GrabaProductoLOC(txtIdProducto.Text)
    '                    GrabaUnidadLOC(vCIDUNIDAD)


    '                    InsertaRegGrid()

    '                    BandNoExisteProdEnGrid = False
    '                    Salida = False
    '                Else
    '                    For i = 0 To MyTablaRefac.DefaultView.Count - 1
    '                        BandNoExisteProdEnGrid = True
    '                        If Trim(txtIdProducto.Text) = Trim(MyTablaRefac.DefaultView.Item(i).Item("trefCCODIGOP01_PRO").ToString) And Trim(txtIdUnidadProd.Text) = Trim(grdProductosOT.Item("grefCCLAVEINT", i).Value) Then
    '                            MyTablaRefac.DefaultView.Item(i).Item("trefCIDPRODUCTO") = vCIDPRODUCTO
    '                            MyTablaRefac.DefaultView.Item(i).Item("trefCIDUNIDADBASE") = vCIDUNIDAD

    '                            MyTablaRefac.DefaultView.Item(i).Item("trefCCODIGOP01_PRO") = txtIdProducto.Text
    '                            MyTablaRefac.DefaultView.Item(i).Item("trefCNOMBREP01") = txtNomProducto.Text
    '                            'Calcula Existencia
    '                            vExistencia = ObjCom.Existencia(vCIDPRODUCTO, Now, vIdAlmacen)
    '                            MyTablaRefac.DefaultView.Item(i).Item("trefExistencia") = vExistencia

    '                            MyTablaRefac.DefaultView.Item(i).Item("trefCantidad") = Val(txtCantidad.Text)
    '                            MyTablaRefac.DefaultView.Item(i).Item("trefCCLAVEINT") = txtIdUnidadProd.Text

    '                            MyTablaRefac.DefaultView.Item(i).Item("trefBComprar") = IIf(vExistencia < Val(txtCantidad.Text), True, False)

    '                            'Calcula Costo
    '                            vCosto = ObjCom.ChecaCosto(vCIDPRODUCTO, Now, vIdAlmacen)
    '                            MyTablaRefac.DefaultView.Item(i).Item("trefCOSTO") = vCosto

    '                            If vExistencia >= Val(txtCantidad.Text) Then
    '                                CantCompra = 0
    '                                CantSalida = Val(txtCantidad.Text)
    '                            Else
    '                                CantCompra = Val(txtCantidad.Text) - vExistencia
    '                                CantSalida = vExistencia
    '                            End If
    '                            MyTablaRefac.DefaultView.Item(i).Item("trefCantCompra") = CantCompra
    '                            MyTablaRefac.DefaultView.Item(i).Item("trefCantSalida") = CantSalida

    '                            'Grabamos Producto
    '                            GrabaProductoLOC(txtIdProducto.Text)
    '                            GrabaUnidadLOC(vCIDUNIDAD)

    '                            BandNoExisteProdEnGrid = False
    '                            Exit For
    '                        End If
    '                    Next
    '                    If BandNoExisteProdEnGrid Then
    '                        Salida = True
    '                        myDataRow = MyTablaRefac.NewRow()
    '                        myDataRow("trefCIDPRODUCTO") = vCIDPRODUCTO
    '                        myDataRow("trefCIDUNIDADBASE") = vCIDUNIDAD

    '                        myDataRow("trefCCODIGOP01_PRO") = txtIdProducto.Text
    '                        myDataRow("trefCNOMBREP01") = txtNomProducto.Text
    '                        'Calcula Existencia
    '                        vExistencia = ObjCom.Existencia(vCIDPRODUCTO, Now, vIdAlmacen)
    '                        myDataRow("trefExistencia") = vExistencia

    '                        myDataRow("trefCantidad") = Val(txtCantidad.Text)
    '                        myDataRow("trefCCLAVEINT") = txtIdUnidadProd.Text

    '                        myDataRow("trefBComprar") = IIf(vExistencia < Val(txtCantidad.Text), True, False)

    '                        'Calcula Costo
    '                        vCosto = ObjCom.ChecaCosto(vCIDPRODUCTO, Now, vIdAlmacen)
    '                        myDataRow("trefCOSTO") = vCosto

    '                        If vExistencia >= Val(txtCantidad.Text) Then
    '                            CantCompra = 0
    '                            CantSalida = Val(txtCantidad.Text)
    '                        Else
    '                            CantCompra = Val(txtCantidad.Text) - vExistencia
    '                            CantSalida = vExistencia
    '                        End If
    '                        myDataRow("trefCantCompra") = CantCompra
    '                        myDataRow("trefCantSalida") = CantSalida



    '                        MyTablaRefac.Rows.Add(myDataRow)

    '                        'Grabamos Producto
    '                        GrabaProductoLOC(txtIdProducto.Text)
    '                        GrabaUnidadLOC(vCIDUNIDAD)
    '                    End If
    '                    InsertaRegGrid()
    '                End If
    '            End If
    '        Else
    '            txtIdProducto.Enabled = True
    '        End If
    '        'ActualizaTotales()
    '    Catch ex As Exception
    '        MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, Me.Text)
    '    End Try
    'End Sub

    'Private Function InsertaRegGrid() As Boolean
    '    Try

    '        grdProductosOT.Rows.Clear()

    '        For i = 0 To MyTablaRefac.DefaultView.Count - 1
    '            grdProductosOT.Rows.Add()
    '            'Consecutivo
    '            grdProductosOT.Item("grefConsecutivo", i).Value = i + 1
    '            grdProductosOT.Item("grefCCODIGOP01_PRO", i).Value = Trim(MyTablaRefac.DefaultView.Item(i).Item("trefCCODIGOP01_PRO").ToString)
    '            grdProductosOT.Item("grefCNOMBREP01", i).Value = Trim(MyTablaRefac.DefaultView.Item(i).Item("trefCNOMBREP01").ToString)
    '            grdProductosOT.Item("grefExistencia", i).Value = CDbl(MyTablaRefac.DefaultView.Item(i).Item("trefExistencia"))
    '            grdProductosOT.Item("grefCantidad", i).Value = CDbl(MyTablaRefac.DefaultView.Item(i).Item("trefCantidad"))
    '            grdProductosOT.Item("grefCCLAVEINT", i).Value = Trim(MyTablaRefac.DefaultView.Item(i).Item("trefCCLAVEINT").ToString)
    '            grdProductosOT.Item("grefBComprar", i).Value = MyTablaRefac.DefaultView.Item(i).Item("trefBComprar")
    '            grdProductosOT.Item("grefCOSTO", i).Value = CDbl(MyTablaRefac.DefaultView.Item(i).Item("trefCOSTO"))

    '            grdProductosOT.Item("grefCIDPRODUCTO", i).Value = CInt(MyTablaRefac.DefaultView.Item(i).Item("trefCIDPRODUCTO"))
    '            grdProductosOT.Item("grefCIDUNIDADBASE", i).Value = CInt(MyTablaRefac.DefaultView.Item(i).Item("trefCIDUNIDADBASE"))

    '            grdProductosOT.Item("grefCantCompra", i).Value = CDbl(MyTablaRefac.DefaultView.Item(i).Item("trefCantCompra"))
    '            grdProductosOT.Item("grefCantSalida", i).Value = CDbl(MyTablaRefac.DefaultView.Item(i).Item("trefCantSalida"))
    '        Next
    '        'grdVentas.Columns("Precio").DefaultCellStyle.Format = "c"
    '        grdProductosOT.PerformLayout()
    '        'grdVentas.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
    '        Return True
    '    Catch ex As Exception
    '        Return False
    '        MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, Me.Text)
    '    End Try

    'End Function

    'Private Sub GrabaProductoLOC(ByVal vCCODIGOPRODUCTO As String)
    '    'Grabamos Producto LOCAL
    '    ProdCom = New ProductosComClass(vCCODIGOPRODUCTO, "")
    '    If ProdCom.Existe Then
    '        'Existe en Comercial
    '        ProdCom.Guardar(False, vCCODIGOPRODUCTO, txtNomProducto.Text, ProdCom.CTIPOPRODUCTO, ProdCom.CSTATUSPRODUCTO, ProdCom.CIDUNIDADBASE, ProdCom.CCODALTERN)

    '    End If

    'End Sub

    'Private Sub GrabaUnidadLOC(ByVal vCIDUNIDAD As Integer)
    '    'Grabamos Producto LOCAL
    '    'UnidadCom = New UnidadesComClass(vCCODIGOPRODUC)
    '    UnidadCom = New UnidadesComClass(vCIDUNIDAD, "CIDUNIDAD", TipoDato.TdNumerico)
    '    If UnidadCom.Existe Then
    '        'Existe en Comercial
    '        UnidadCom.Guardar(False, txtNomUniProd.Text, UnidadCom.CABREVIATURA, UnidadCom.CDESPLIEGUE, UnidadCom.CCLAVEINT)

    '    End If

    'End Sub

    'Private Sub BorraCamposProd()
    '    txtIdProducto.Text = ""
    '    txtNomProducto.Text = ""
    '    txtIdUnidadProd.Text = ""
    '    txtNomUniProd.Text = ""
    '    txtCantidad.Text = ""

    'End Sub

    'Private Sub txtDuracion_KeyPress(sender As Object, e As KeyPressEventArgs)
    '    If txtDuracion.Text.Length = 0 Then
    '        txtDuracion.Focus()
    '        txtDuracion.SelectAll()
    '    Else
    '        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
    '            Salida = True
    '            tabPrinc.SelectedIndex = 2
    '            txtIdProducto.Focus()
    '            txtIdProducto.SelectAll()
    '            Salida = False
    '        End If
    '    End If
    'End Sub

    Private Sub Cancelar()
        Timer1.Start()
        LimpiaCampos()
        ActivaBotones(False, TipoOpcActivaBoton.tOpcInicializa)

        'ActivaCampos(False, TipoOpcActivaCampos.tOpcDESHABTODOS)

        ActivaCampos(False, TipoOpcActivaCampos.tOpcDESHABTODOS)



        ToolStripMenu.Enabled = True
        'GroupBox2.Enabled = True
        'tabPrinc.SelectedIndex = 0
        'DsComboMovimiento.Tables("MGW10006").DefaultView.RowFilter = Nothing
        'chkAyu1.Checked = False
        'chkAyu2.Checked = False
        'ActivaAyudantes(chkAyu1.Checked, TipoNumAyudante.Ayudante1)
        'ActivaAyudantes(chkAyu2.Checked, TipoNumAyudante.Ayudante2)
        indice = 0
        indiceOS = 0
        indiceOT = 0



        'grdOrdenServCab.DataSource = Nothing
        'grdOrdenServCab.Rows.Clear()



        'grdOrdenServDet.DataSource = Nothing
        'grdOrdenServDet.Rows.Clear()


        MyTablaDetalle.Clear()

        AutoCompletarPersonal(cmbChofer, TipotablasPer.tpChoferes, 0)
        AutoCompletarPersonal(cmbDespachador, TipotablasPer.tpDespachadores, 0)

        txtidVCExtra.Focus()

        Salida = True
        'txtOrdenTrabajo.Enabled = True
        'txtOrdenTrabajo.Focus()
        'Salida = False


    End Sub

    Private Sub ActivaBotones(ByVal Valor As Boolean, Optional ByVal vOpcion As TipoOpcActivaBoton = TipoOpcActivaBoton.tOpcInicializa)
        'Si es Verdadero Se activan Ok,Cancelar y Buscar y Terminar Se desactiva
        If vOpcion = TipoOpcActivaBoton.tOpcConsulta Then

            btnMnuOk.Enabled = False
            btnMnuLimpiar.Enabled = True
            btnMnuCancelar.Enabled = False
            btnMenuReImprimir.Enabled = False
            btnMnuSalir.Enabled = False
        ElseIf vOpcion = TipoOpcActivaBoton.tOpcInicializa Then
            'btnMnuOk.Enabled = Valor
            'btnMnuLimpiar.Enabled = Valor
            'btnMnuCancelar.Enabled = Valor
            'btnMnuSalir.Enabled = Not Valor

            btnMnuOk.Enabled = Valor
            btnMnuLimpiar.Enabled = Valor
            btnMnuCancelar.Enabled = Valor
            btnMenuReImprimir.Enabled = Valor
            btnMnuSalir.Enabled = Not Valor


        ElseIf vOpcion = TipoOpcActivaBoton.tOpcInsertar Then
            btnMnuOk.Enabled = True
            btnMenuReImprimir.Enabled = False
            btnMnuLimpiar.Enabled = True
            btnMnuCancelar.Enabled = False
            btnMnuSalir.Enabled = False

        ElseIf vOpcion = TipoOpcActivaBoton.tOpcEspecial Then
            btnMnuOk.Enabled = False
            btnMnuLimpiar.Enabled = True
            btnMnuCancelar.Enabled = True
            btnMenuReImprimir.Enabled = True
            btnMnuSalir.Enabled = True

        End If
    End Sub

    Private Sub ActivaCampos(ByVal valor As Boolean, ByVal vOpcion As TipoOpcActivaCampos)
        txtEstatus.Enabled = False
        dtpFechaCap.Enabled = False
        dtpFechaCapturaHora.Enabled = False
        txtNoViaje.Enabled = False
        txtUniTrans.Enabled = False
        cmbChofer.Enabled = False
        cmbOpciones.Enabled = False

        If vOpcion = TipoOpcActivaCampos.tOpcDESHABTODOS Then
            'txtNoViaje.Enabled = False
            txtidVCExtra.Enabled = True
            txtltCombustible.Enabled = False
            cmbDespachador.Enabled = False
        ElseIf vOpcion = TipoOpcActivaCampos.tOpcINICIALIZA Then
            txtidVCExtra.Enabled = False
            txtltCombustible.Enabled = True
            cmbDespachador.Enabled = True

            'cmbChofer.Enabled = True
        ElseIf vOpcion = TipoOpcActivaCampos.tOpcOTRO Then
            'SIN VIAJE
            txtNoViaje.Enabled = False
            txtUniTrans.Enabled = True
            cmbChofer.Enabled = True
        ElseIf vOpcion = TipoOpcActivaCampos.tOpcDEPENDEVALOR Then
            'CON VIAJE
            txtNoViaje.Enabled = True
            txtUniTrans.Enabled = False
            cmbChofer.Enabled = False
        End If
    End Sub
    'Private Sub ActivaCamposProductos(ByVal valor As Boolean)
    '    txtIdProducto.Enabled = valor
    '    txtNomProducto.Enabled = valor
    '    txtIdUnidadProd.Enabled = valor
    '    txtNomUniProd.Enabled = valor
    '    txtCantidad.Enabled = valor
    'End Sub

    Private Sub txtIdProducto_TextChanged(sender As Object, e As EventArgs)

    End Sub

    Private Sub txtIdUnidadProd_TextChanged(sender As Object, e As EventArgs)

    End Sub

    'Private Sub btnMnuOk_Click(sender As Object, e As EventArgs) Handles btnMnuOk.Click
    '    Dim bCabPreOC As Boolean = False
    '    Dim bCabPreSal As Boolean = False
    '    Dim objsql As New ToolSQLs
    '    Dim vIdCLAVE As Integer
    '    Dim vidOrdTrab As Integer
    '    Dim vNewidOrdTrab As Integer
    '    Dim vidOrdServ As Integer
    '    Dim vUnidadTrans As String
    '    Dim vidTipOrdServ As Integer
    '    Dim vidTipServ As Integer
    '    Dim vbOrdenTrabajo As Boolean

    '    Dim vidServicio As Integer
    '    Dim vidActividad As Integer
    '    Dim vDurHora As Integer
    '    Dim vidDivision As Integer
    '    Dim vNotaRecepcion As String
    '    Dim vidOrdenActividad As Integer

    '    Dim Mensaje As String = ""

    '    Try
    '        Salida = True
    '        indice = 0
    '        indiceOS = 0
    '        indiceOT = 0
    '        vidOrdTrab = 0
    '        If Not Validar() Then Exit Sub

    '        If Val(txtidPadreOrdSer.Text) > 0 Then
    '            Mensaje = "!! Esta seguro que desea Modificar las Ordenes de Servicio con FOLIO: " & Val(txtidPadreOrdSer.Text) & "? !!"
    '        Else
    '            Mensaje = "!! Esta seguro que desea Grabar las Ordenes de Servicio: ? !!"
    '        End If

    '        If MsgBox(Mensaje, MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then

    '            If Val(txtidPadreOrdSer.Text) > 0 Then
    '                AgregaTODOSDET()

    '                'Actualiza Fecha de Ordenes de Servicio
    '                For c = 0 To grdOrdenServCab.Rows.Count - 1
    '                    vIdCLAVE = grdOrdenServCab.Item("oscCLAVE", c).Value
    '                    vbOrdenTrabajo = grdOrdenServCab.Item("oscbOrdenTrabajo", c).Value

    '                    ReDim Preserve ArrayOrdenSer(indiceOS)
    '                    ArrayOrdenSer(indiceOS) = vIdCLAVE

    '                    ReDim Preserve ArrayOrdenSerTipo(indiceOS)
    '                    ArrayOrdenSerTipo(indiceOS) = vbOrdenTrabajo
    '                    indiceOS += 1

    '                    StrSql = objsql.ActualizaCampoTabla("CabOrdenServicio", "idOrdenSer", TipoDato.TdNumerico, vIdCLAVE,
    '                    "fechaCaptura", TipoDato.TdFechaHora, FormatFecHora(Now, True))
    '                    ReDim Preserve ArraySql(indice)
    '                    ArraySql(indice) = StrSql
    '                    indice += 1

    '                    'Detalle de Orden Servicio
    '                    'Borrar todas
    '                    For d = 0 To grdOrdenServDet.Rows.Count - 1
    '                        If vIdCLAVE = grdOrdenServDet.Item("osdCLAVE", d).Value Then
    '                            If grdOrdenServDet.Item("osdModificado", d).Value Then

    '                                vidServicio = grdOrdenServDet.Item("osdidServicio", d).Value
    '                                vidActividad = grdOrdenServDet.Item("osdidActividad", d).Value
    '                                vDurHora = grdOrdenServDet.Item("osdDuracionActHr", d).Value
    '                                vidDivision = grdOrdenServDet.Item("osdidDivision", d).Value
    '                                vNotaRecepcion = grdOrdenServDet.Item("osdNomServicio", d).Value
    '                                vidOrdenActividad = grdOrdenServDet.Item("osdidOrdenActividad", d).Value
    '                                vidOrdTrab = grdOrdenServDet.Item("osdidOrdenTrabajo", d).Value

    '                                'Cancelar el Actual
    '                                'StrSql = objsql.ActualizaCampoTabla("DetOrdenServicio", "idOrdenActividad", TipoDato.TdNumerico, vidOrdenActividad,
    '                                '"Estatus", TipoDato.TdCadena, "CAN")

    '                                StrSql = "DELETE dbo.DetOrdenServicio WHERE idOrdenActividad = " & vidOrdenActividad
    '                                ReDim Preserve ArraySql(indice)
    '                                ArraySql(indice) = StrSql
    '                                indice += 1

    '                                If vidOrdTrab > 0 Then
    '                                    'Cancelamos actual
    '                                    StrSql = objsql.ActualizaCampoTabla("DetRecepcionOS", "idOrdenTrabajo", TipoDato.TdNumerico, vidOrdTrab,
    '                                    "isCancelado", TipoDato.TdBoolean, 1)
    '                                    ReDim Preserve ArraySql(indice)
    '                                    ArraySql(indice) = StrSql
    '                                    indice += 1

    '                                    StrSql = objsql.ActualizaCampoTabla("DetRecepcionOS", "idOrdenTrabajo", TipoDato.TdNumerico, vidOrdTrab,
    '                                    "motivoCancelacion", TipoDato.TdCadena, "DESDE SISTEMA")
    '                                    ReDim Preserve ArraySql(indice)
    '                                    ArraySql(indice) = StrSql
    '                                    indice += 1
    '                                    'Generamos nuevo
    '                                    If d = 0 Or vNewidOrdTrab = 0 Then
    '                                        vNewidOrdTrab = UltimoOrdenTrab()
    '                                    Else
    '                                        vNewidOrdTrab = vidOrdTrab + 1
    '                                    End If

    '                                    ReDim Preserve ArrayOrdenTrab(indiceOT)
    '                                    ArrayOrdenTrab(indiceOT) = vNewidOrdTrab
    '                                    indiceOT += 1

    '                                    StrSql = objsql.InsertaOrdenTrabajo(vNewidOrdTrab, vidOrdServ, 0, vNotaRecepcion, 0, vidDivision, 0, 0, 0)
    '                                    ReDim Preserve ArraySql(indice)
    '                                    ArraySql(indice) = StrSql
    '                                    indice += 1
    '                                End If

    '                                StrSql = objsql.InsertaOrdenServicioDET(vIdCLAVE, vidServicio,
    '                                vidActividad, vDurHora, 0, 0, "PRE", 0, 0, vNewidOrdTrab, vidDivision)
    '                                ReDim Preserve ArraySql(indice)
    '                                ArraySql(indice) = StrSql
    '                                indice += 1
    '                                'aqui()
    '                            End If
    '                        End If


    '                        'If vIdCLAVE = grdOrdenServDet.Item("osdCLAVE", d).Value Then

    '                        '    'Trabajos
    '                        '    If vbOrdenTrabajo Then
    '                        '        If d = 0 Or vidOrdTrab = 0 Then
    '                        '            vidOrdTrab = UltimoOrdenTrab()
    '                        '        Else
    '                        '            vidOrdTrab = vidOrdTrab + 1
    '                        '        End If

    '                        '        ReDim Preserve ArrayOrdenTrab(indiceOT)
    '                        '        ArrayOrdenTrab(indiceOT) = vidOrdTrab
    '                        '        indiceOT += 1

    '                        '        StrSql = objsql.InsertaOrdenTrabajo(vidOrdTrab, vidOrdServ, 0, vNotaRecepcion, 0, vidDivision, 0, 0, 0)
    '                        '        ReDim Preserve ArraySql(indice)
    '                        '        ArraySql(indice) = StrSql
    '                        '        indice += 1
    '                        '    Else
    '                        '        vidOrdTrab = 0
    '                        '    End If
    '                        '    'Detalle - checar los ceros
    '                        '    StrSql = objsql.InsertaOrdenServicioDET(vidOrdServ, vidServicio,
    '                        '    vidActividad, vDurHora, 0, 0, "PRE", 0, 0, vidOrdTrab, vidDivision)
    '                        '    ReDim Preserve ArraySql(indice)
    '                        '    ArraySql(indice) = StrSql
    '                        '    indice += 1

    '                        'End If
    '                    Next

    '                Next
    '                'Se actualiza Parametros
    '                'StrSql = objsql.ActualizaCampoTabla("Parametros", "idOrdenSer", TipoDato.TdNumerico, vIdCLAVE,
    '                '"IdParametro", TipoDato.TdNumerico, "1")
    '                'ReDim Preserve ArraySql(indice)
    '                'ArraySql(indice) = StrSql
    '                'indice += 1

    '                StrSql = objsql.ActualizaCampoTabla("Parametros", "idOrdenTrabajo", TipoDato.TdNumerico, vidOrdTrab,
    '                "IdParametro", TipoDato.TdNumerico, "1")
    '                ReDim Preserve ArraySql(indice)
    '                ArraySql(indice) = StrSql
    '                indice += 1

    '            Else
    '                '1.- Grabar MasterOrdServ y Obtener Consecutivo para poder continuar
    '                '2- Recorrer Grid de Cabeceros para insertar Ordenes de servicio
    '                AgregaTODOSDET()
    '                vidPadreOrdSer = UltimoMaster()
    '                If vidPadreOrdSer > 0 Then
    '                    'Insertar Padre
    '                    StrSql = objsql.InsertaPadre(vidPadreOrdSer, txtTractor.Text, txtRemolque1.Text, txtDolly.Text,
    '                    txtRemolque2.Text, cmbChofer.SelectedValue, FormatFecHora(dtpFechaCap.Value, True), _Usuario, "CAP")
    '                    ReDim Preserve ArraySql(indice)
    '                    ArraySql(indice) = StrSql
    '                    indice += 1

    '                    'Ordenes de Servicio
    '                    For c = 0 To grdOrdenServCab.Rows.Count - 1
    '                        vIdCLAVE = grdOrdenServCab.Item("oscCLAVE", c).Value
    '                        vUnidadTrans = grdOrdenServCab.Item("oscidUnidadTrans", c).Value
    '                        vidTipOrdServ = grdOrdenServCab.Item("oscTipoOrdenServ", c).Value
    '                        vidTipServ = grdOrdenServCab.Item("oscTipoServ", c).Value
    '                        vbOrdenTrabajo = grdOrdenServCab.Item("oscbOrdenTrabajo", c).Value

    '                        If c = 0 Then
    '                            vidOrdServ = UltimoOrdServ()
    '                        Else
    '                            vidOrdServ = vidOrdServ + 1
    '                        End If

    '                        If vidOrdServ > 0 Then
    '                            ReDim Preserve ArrayOrdenSer(indiceOS)
    '                            ArrayOrdenSer(indiceOS) = vidOrdServ

    '                            ReDim Preserve ArrayOrdenSerTipo(indiceOS)
    '                            ArrayOrdenSerTipo(indiceOS) = vbOrdenTrabajo
    '                            indiceOS += 1


    '                            'Insertamos Cabecero
    '                            'StrSql = objsql.InsertaOrdenServicio(vidOrdServ, _cveEmpresa, vUnidadTrans, vidTipOrdServ,
    '                            'Val(txtKilometraje.Text), "PRE", cmbAutoriza.SelectedValue,
    '                            'FormatFecHora(dtpFechaCap.Value, True), vidTipOrdServ, vidTipServ, vidPadreOrdSer)

    '                            '10/ENE/2017
    '                            StrSql = objsql.InsertaOrdenServicio(vidOrdServ, _cveEmpresa, vUnidadTrans, vidTipOrdServ,
    '                            0, "PRE", cmbAutoriza.SelectedValue,
    '                            FormatFecHora(dtpFechaCap.Value, True), vidTipOrdServ, vidTipServ, vidPadreOrdSer)


    '                            ReDim Preserve ArraySql(indice)
    '                            ArraySql(indice) = StrSql
    '                            indice += 1

    '                            For d = 0 To grdOrdenServDet.Rows.Count - 1
    '                                vidServicio = grdOrdenServDet.Item("osdidServicio", d).Value
    '                                vidActividad = grdOrdenServDet.Item("osdidActividad", d).Value
    '                                vDurHora = grdOrdenServDet.Item("osdDuracionActHr", d).Value
    '                                vidDivision = grdOrdenServDet.Item("osdidDivision", d).Value
    '                                vNotaRecepcion = grdOrdenServDet.Item("osdNomServicio", d).Value

    '                                If vIdCLAVE = grdOrdenServDet.Item("osdCLAVE", d).Value Then

    '                                    'Trabajos
    '                                    If vbOrdenTrabajo Then
    '                                        If d = 0 Or vidOrdTrab = 0 Then
    '                                            vidOrdTrab = UltimoOrdenTrab()
    '                                        Else
    '                                            vidOrdTrab = vidOrdTrab + 1
    '                                        End If

    '                                        ReDim Preserve ArrayOrdenTrab(indiceOT)
    '                                        ArrayOrdenTrab(indiceOT) = vidOrdTrab
    '                                        indiceOT += 1

    '                                        StrSql = objsql.InsertaOrdenTrabajo(vidOrdTrab, vidOrdServ, 0, vNotaRecepcion, 0, vidDivision, 0, 0, 0)
    '                                        ReDim Preserve ArraySql(indice)
    '                                        ArraySql(indice) = StrSql
    '                                        indice += 1
    '                                    Else
    '                                        vidOrdTrab = 0
    '                                    End If
    '                                    'Detalle - checar los ceros
    '                                    StrSql = objsql.InsertaOrdenServicioDET(vidOrdServ, vidServicio,
    '                                    vidActividad, vDurHora, 0, 0, "PRE", 0, 0, vidOrdTrab, vidDivision)
    '                                    ReDim Preserve ArraySql(indice)
    '                                    ArraySql(indice) = StrSql
    '                                    indice += 1

    '                                    'Else
    '                                    'If vidServicio > 0 Or vidActividad > 0 Then
    '                                    '    StrSql = objsql.InsertaOrdenServicioDET(vidOrdServ, vidServicio,
    '                                    '    vidActividad, vDurHora, 0, 0, "PRE", 0, 0, vidOrdTrab)
    '                                    '    ReDim Preserve ArraySql(indice)
    '                                    '    ArraySql(indice) = StrSql
    '                                    '    indice += 1
    '                                    'End If
    '                                End If
    '                            Next

    '                        End If
    '                    Next

    '                    'Se actualiza Parametros
    '                    StrSql = objsql.ActualizaCampoTabla("Parametros", "idOrdenSer", TipoDato.TdNumerico, vidOrdServ,
    '                    "IdParametro", TipoDato.TdNumerico, "1")
    '                    ReDim Preserve ArraySql(indice)
    '                    ArraySql(indice) = StrSql
    '                    indice += 1

    '                    StrSql = objsql.ActualizaCampoTabla("Parametros", "idOrdenTrabajo", TipoDato.TdNumerico, vidOrdTrab,
    '                    "IdParametro", TipoDato.TdNumerico, "1")
    '                    ReDim Preserve ArraySql(indice)
    '                    ArraySql(indice) = StrSql
    '                    indice += 1

    '                End If
    '            End If




    '            If indice > 0 Then
    '                Dim obj As New CapaNegocio.Tablas
    '                Dim Respuesta As String
    '                Dim sOrdenes As String = ""
    '                Dim idOS As Integer = 0
    '                Dim TipidOS As Boolean = False

    '                Respuesta = obj.EjecutarSql(Inicio.CONSTR, "SQL", ArraySql, Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, indice)
    '                FEspera.Close()
    '                If Respuesta = "EFECTUADO" Then
    '                    For Each Res In ArrayOrdenSer
    '                        sOrdenes = sOrdenes & Res & ","
    '                        'ImprimeOrdenServicio(270, True)
    '                    Next
    '                    sOrdenes = Mid(sOrdenes, 1, sOrdenes.Length - 1)
    '                    MsgBox("Se crearon la(s) Orden de Servicio(s) :" & sOrdenes, MsgBoxStyle.Exclamation, Me.Text)

    '                    For imp = 0 To ArrayOrdenSer.Length - 1
    '                        idOS = ArrayOrdenSer(imp)
    '                        TipidOS = ArrayOrdenSerTipo(imp)
    '                        ImprimeOrdenServicio(idOS, TipidOS, True, True)
    '                    Next

    '                    'MsgBox("Orden de Trabajo Actualizado Satisfactoriamente" & vbCrLf & _
    '                    'IIf(NoIdPreOC > 0, "Se Creo la El Folio de Pre Orden de Compra # " & NoIdPreOC, "") & vbCrLf & _
    '                    'IIf(NoIdPreSal > 0, "Se Creo la El Folio de Pre Salida de ALmacen # " & NoIdPreSal, ""), MsgBoxStyle.Exclamation, Me.Text)

    '                    'If OpcionForma = TipoOpcionForma.tOpcModificar Then
    '                    '    MsgBox("Orden de Trabajo Actualizado Satisfactoriamente" & vbCrLf & _
    '                    '    IIf(NoIdPreOC > 0, "Se Creo la El Folio de Pre Orden de Compra # " & NoIdPreOC, "") & vbCrLf & _
    '                    '    IIf(NoIdPreSal > 0, "Se Creo la El Folio de Pre Salida de ALmacen # " & NoIdPreSal, ""), MsgBoxStyle.Exclamation, Me.Text)
    '                    '    'ElseIf OpcionForma = TipoOpcionForma.tOpcEliminar Then
    '                    '    '    MsgBox("Grupo eliminado con exito!!!", MsgBoxStyle.Exclamation, Me.Text)
    '                    '    'ElseIf OpcionForma = TipoOpcionForma.tOpcInsertar Then
    '                    '    'MsgBox("Actualizacion efectuada satisfactoriamente", MsgBoxStyle.Exclamation, Me.Text)
    '                    'End If
    '                ElseIf Respuesta = "NOEFECTUADO" Then

    '                ElseIf Respuesta = "ERROR" Then
    '                    indice = 0
    '                    SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")
    '                End If
    '            Else
    '                MsgBox("No se Encontraron Registros que Grabar", MsgBoxStyle.Exclamation, Me.Text)
    '            End If
    '            Cancelar()
    '        End If
    '        Salida = False
    '    Catch ex As Exception
    '        MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
    '    End Try
    'End Sub

#Region "IMPRESION"
    Private Sub ImprimeOrdenServicio(ByVal idOrdenSer As Integer, ByVal bOrdenTrab As Boolean, ByVal BandPreview As Boolean,
    ByVal vbanHorizontal As Boolean)
        Dim objSql As New ToolSQLs


        ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 10)
        ImpRep.FuenteImprimeCampos3 = New System.Drawing.Font("Courier New", 10, FontStyle.Bold)

        'PATH_FONTS = "C:\Windows\Fonts\"
        'Dim pfc As PrivateFontCollection = New PrivateFontCollection()
        ''FRE3OF9X_0
        ''pfc.AddFontFile(PATH_FONTS & "BARCOD39.TTF")
        'pfc.AddFontFile(PATH_FONTS & "FREE3OF9_2.TTF")
        'Dim FontFam As FontFamily = pfc.Families(0)
        'ImpRep.FuenteImprimeCampos2 = New Font(FontFam, 40)

        ImpRep.FuenteImprimeCampos2 = New Font("Free 3 of 9 Extended", 40)

        'BARCOD39
        ImpRep.Constr = Inicio.CONSTR
        ImpRep.TipoConexion = TipoConexion.TcSQL
        ImpRep.NomTabla = "ORDSERV"


        If bOrdenTrab Then
            ImpRep.StrSql = objSql.ImpOrdServConOT(idOrdenSer)
        Else
            ImpRep.StrSql = objSql.ImpOrdServSinOT(idOrdenSer)
        End If

        NumRegistros = ImpRep.LlenaDataSet
        If NumRegistros > 0 Then
            ArmaTablaImpresion(bOrdenTrab)
            ImpRep.TablaImprime = MyTablaImprime

            If prtSettings Is Nothing Then
                prtSettings = New PrinterSettings
            End If

            If prtDoc Is Nothing Then
                prtDoc = New PrintDocument
                AddHandler prtDoc.PrintPage, AddressOf prt_PrintPage
            End If

            'If ExisteColaImp = "" Then
            'ojoooo ExisteColaImp = LlenaDatos("1", "ColaImpresion", "IdParametro", TipoDato.TdNumerico, , )

            'End If

            If ExisteColaImp = KEY_RCORRECTO Then
                If StrColaImpRecibo <> "" Then
                    prtSettings.PrinterName = StrColaImpRecibo
                    MsgBox(StrColaImpRecibo, MsgBoxStyle.Information, Me.Text)
                End If
            End If

            'If StrColaImpresion = "" Then
            '    'MsgBox("No Hay Cola de Impresi�n, la Hoja de Surtido No se mando a Imprimir", MsgBoxStyle.Information, "Impresion de surtido")
            '    Exit Sub
            'Else
            '    ImpGlobal.PrinterSettings.PrinterName = strCola
            'End If

            prtDoc.PrinterSettings = prtSettings

            prtDoc.DefaultPageSettings.Landscape = vbanHorizontal
            prtDoc.DefaultPageSettings.PaperSize = MyPaperSize


            If BandPreview Then
                Dim prtPrev As New PrintPreviewDialog
                prtPrev.Document = prtDoc
                prtPrev.Text = "Previsualizar datos de " & Me.Text
                prtPrev.WindowState = FormWindowState.Maximized
                prtPrev.Document.DefaultPageSettings.Landscape = vbanHorizontal
                'prtPrev.PrintPreviewControl.Zoom = 1
                If vbanHorizontal Then
                    prtPrev.PrintPreviewControl.Zoom = 1
                Else
                    prtPrev.PrintPreviewControl.Zoom = 1.5
                End If

                'prtPrev.MdiParent = Me
                prtPrev.ShowDialog()
            Else
                prtDoc.Print()
            End If



        End If


    End Sub

    Private Sub prt_PrintPage(ByVal sender As Object, ByVal e As PrintPageEventArgs)
        ' Este evento se produce cada vez que se va a imprimir una p�gina
        Try
            'lineaActual = 0

            Dim yPos As Single = e.MarginBounds.Top
            'Margen Izquierdo
            Dim leftMargin As Single = e.MarginBounds.Left
            'Fuentes
            'Altura de la Linea
            Dim lineHeight As Single = printFont.GetHeight(e.Graphics)

            'Dim vFuenteImprimeCampos = New System.Drawing.Font("Courier New", 7)
            'Dim vFuenteImprimeCampos2 = New System.Drawing.Font("Courier New", 7)

            If _cveEmpresa = 1 Then

                'Dim bmp As New Bitmap(My.Resources.LogoPegaso)
                'ImpRep.Imagen1 = New Bitmap(bmp, bmp.Width / 20, bmp.Height / 20)

                'ImpRep.PosicionImagen1 = New Point(e.MarginBounds.Left - 10, yPos - 30)
                ImpRep.PosicionImagen1 = New Point(e.MarginBounds.Left - 70, yPos - 30)


            End If

            Do
                yPos += lineHeight
                'Aqui se imprime el dato

                yPos = ImpRep.ImprimeLineaRecibo(e, lineaActual)

                lineaActual += 1

            Loop Until yPos >= e.MarginBounds.Bottom - 20 _
                OrElse lineaActual >= NumRegistros


            ContPaginas += 1
            'DS.Tables(vNomTabla).DefaultView.Count



            If lineaActual < NumRegistros Then
                e.HasMorePages = True
            Else
                ContPaginas = 0
                e.HasMorePages = False
            End If
        Catch ex As Exception
            MsgBox(ex.Message, Me.Text)
        End Try

    End Sub

    Private Sub ArmaTablaImpresion(ByVal bOrdenTrab As Boolean)
        Dim campo As Integer
        Dim Descripcion As String
        Dim Valor As String
        Dim PosValorX, PosValorY, PosDescX, PosDescY As Single
        Dim NumAletra As New NumALetras
        Dim EnLetras As String = ""
        Dim Total As Double = 0

        Dim Y1 As Single = 100

        'Horizontal
        'Dim X1 As Single = 20
        'Dim X1A As Single = 120
        'Dim X1B As Single = 200
        'Dim X2 As Single = 320
        'Dim X3 As Single = 440
        'Dim X3A As Single = 540
        'Dim X4 As Single = 620

        'Vertical
        Dim X1 As Single = 20
        Dim X1A As Single = 120
        Dim X1B As Single = 200
        Dim X2 As Single = 160
        Dim X3 As Single = 220
        Dim X3A As Single = 260
        Dim X4 As Single = 400

        Dim ValPad As Integer = 12

        DsRep = New DataSet
        'MyTablaImprime = New DataTable("IMPRIME")
        If MyTablaImprime.Rows.Count > 0 Then
            MyTablaImprime.Rows.Clear()
        End If


        DsRep = ImpRep.LlenaDsImprime(ImpRep.NomTabla)
        If DsRep.Tables(ImpRep.NomTabla).DefaultView.Count > 0 Then

            For i As Integer = 0 To DsRep.Tables(ImpRep.NomTabla).DefaultView.Count - 1

                'LINEA 1
                campo += 1
                'Descripcion = "FECHA DE PAGO:"
                'Valor = CDate(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("FechaTrans"))
                Descripcion = ""
                Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("RazonSocial"))
                PosDescX = X2 : PosDescY = Y1 : PosValorX = X2 : PosValorY = PosDescY
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

                'CODIGO DE BARRAS ORDEN SERVICIO
                campo += 1
                Descripcion = ""
                'PosDescY = PosDescY + 15 : PosValorY = PosDescY
                PosDescX = X4 : PosValorX = X4
                'Valor = FormarBarCode(8)
                Valor = FormarBarCode(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idOrdenSer"))
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 2)

                'LINEA 2
                campo += 1
                PosDescX = X2
                PosDescY = PosDescY + 15
                'PosValorX = PosDescX + 100
                PosValorX = X2 + 35
                PosValorY = PosDescY
                Descripcion = ""
                'Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idOrdenSer")
                Valor = "ORDEN DE SERVICIO"
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

                'LINEA 3
                campo += 1
                Descripcion = ""
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                PosDescX = X2 : PosValorX = X2 + 40
                Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("NomTipOrdServ"))
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)

                'LINEA 4
                campo += 1
                Descripcion = "ORD.SER."
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                PosDescX = X4 : PosValorX = X4 + 80
                'Valor = FormarBarCode(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idOrdenSer"))
                Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idOrdenSer")
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)



                'LINEA 5
                campo += 1
                Descripcion = "FECHA:"
                Valor = CDate(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("fechaCaptura")).ToShortDateString
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                PosDescX = X4 : PosValorX = PosDescX + 50
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

                'campo += 1
                'PosDescY = PosDescY + 30 : PosValorY = PosDescY
                'Descripcion = "DIA: "
                'Valor = CDate(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("fechaCaptura")).Day
                'PosDescX = X1 : PosValorX = PosDescX + 35
                'InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

                'campo += 1
                'Descripcion = "MES:"
                'Valor = CDate(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("fechaCaptura")).Month
                'PosDescX = X2 : PosValorX = PosDescX + 35
                'InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

                'campo += 1
                'Descripcion = "A�O:"
                'Valor = CDate(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("fechaCaptura")).Year
                'PosDescX = X3 : PosValorX = PosDescX + 35
                'InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

                'LINEA 5.1
                campo += 1
                Descripcion = "HORA:"
                Valor = CDate(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("fechaCaptura")).ToShortTimeString
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                PosDescX = X4 : PosValorX = PosDescX + 40
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)


                'LINEA 6
                campo += 1
                PosDescY = PosDescY + 30 : PosValorY = PosDescY
                Descripcion = "TRACTOR: "
                Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idTractor"))
                PosDescX = X1 : PosValorX = PosDescX + 70
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

                campo += 1
                Descripcion = "REMOLQUE 1:"
                Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idTolva1"))
                PosDescX = X2 : PosValorX = PosDescX + 95
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)


                'LINEA 7
                campo += 1
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                Descripcion = "DOLLY: "
                Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idDolly"))
                PosDescX = X1 : PosValorX = PosDescX + 50
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

                campo += 1
                Descripcion = "REMOLQUE 2:"
                Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idTolva2"))
                PosDescX = X2 : PosValorX = PosDescX + 95
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

                'LINEA 8
                'campo += 1
                'PosDescY = PosDescY + 15 : PosValorY = PosDescY
                'Descripcion = "KILOMETRAJE: "
                'Valor = Format(CInt(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("Kilometraje")), "##,##0")
                'PosDescX = X1 : PosValorX = PosDescX + 110
                'InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)


                'LINEA 9
                campo += 1
                PosDescY = PosDescY + 30 : PosValorY = PosDescY
                Descripcion = ""
                If bOrdenTrab Then
                    Valor = "REPARACIONES SOLICITADAS"
                Else
                    Valor = "DESCRIPCION DEL SERVICIO"
                End If
                PosDescX = X2 : PosValorX = PosDescX + 25
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)
                PosDescY = PosDescY + 15 : PosValorY = PosDescY

                'LINEA 10
                'DETALLE
                For j As Integer = 0 To DsRep.Tables(ImpRep.NomTabla).DefaultView.Count - 1
                    If bOrdenTrab Then
                        If DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NomServicio") <> "" Then
                            campo += 1
                            PosDescY = PosDescY + 15 : PosValorY = PosDescY
                            Descripcion = ""
                            Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NomServicio")
                            PosDescX = X1 : PosValorX = PosDescX + 25
                            InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                        Else
                            If DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NombreAct") <> "" Then
                                campo += 1
                                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                                Descripcion = ""
                                Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NombreAct")
                                PosDescX = X1 : PosValorX = PosDescX + 25
                                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                            Else
                                'salto de linea
                                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                            End If
                        End If

                        campo += 1
                        'PosDescY = PosDescY + 30 : PosValorY = PosDescY
                        Descripcion = ""
                        Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NotaRecepcion")
                        PosDescX = X2 : PosValorX = PosDescX + 25
                        InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                    Else
                        campo += 1
                        PosDescY = PosDescY + 15 : PosValorY = PosDescY
                        Descripcion = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("idUnidadTrans")
                        Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NomServicio")
                        PosDescX = X2 - 35 : PosValorX = X2 + 25
                        InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                        'PosDescY = PosDescY + 15

                    End If
                Next




                'LINEA 11
                campo += 1
                PosDescY = PosDescY + 290 : PosValorY = PosDescY
                Descripcion = ""
                Valor = "___________________"
                PosDescX = X1 : PosValorX = PosDescX + 25
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                'PosDescY = PosDescY + 15

                campo += 1
                Descripcion = ""
                Valor = "___________________"
                PosDescX = X3A : PosValorX = PosDescX + 25
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                'PosDescY = PosDescY + 15

                'LINEA 12
                campo += 1
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                Descripcion = ""
                Valor = " ( OPERADOR ) "
                PosDescX = X1 : PosValorX = PosDescX + 35
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                'PosDescY = PosDescY + 15

                campo += 1
                Descripcion = ""
                Valor = " ( AUTORIZ� ) "
                PosDescX = X3A : PosValorX = PosDescX + 35
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                'PosDescY = PosDescY + 15

                'LINEA 13
                campo += 1
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                Descripcion = ""
                Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("NomOperador"))
                PosDescX = X1 : PosValorX = PosDescX + 25
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                'PosDescY = PosDescY + 15

                campo += 1
                Descripcion = ""
                Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("NomAutoriza"))
                PosDescX = X3A : PosValorX = PosDescX + 25
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                'PosDescY = PosDescY + 15

                'LINEA 14
                'CODIGO DE BARRAS ORDEN SERVICIO
                campo += 1
                Descripcion = ""
                'PosDescY = PosDescY + 15 : PosValorY = PosDescY
                PosDescY = PosDescY + 75 : PosValorY = PosDescY
                PosDescX = X4 : PosValorX = X4
                Valor = FormarBarCode(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idPadreOrdSer"))
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 2)

                'LINEA 15
                campo += 1
                Descripcion = "FOLIO: "
                PosDescY = PosDescY + 45 : PosValorY = PosDescY
                PosDescX = X4 : PosValorX = X4 + 60
                Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idPadreOrdSer")
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)

                'LINEA 14
                'If idContrato = 0 Then
                '    '4 LINEAS
                '    PosDescY = PosDescY + 60
                'End If

                'campo += 1
                'Descripcion = "RECIBO NO."
                'PosDescX = X1 : PosDescY = PosDescY + 15 : PosValorX = PosDescX + 100 : PosValorY = PosDescY
                'Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("NUMRECIBO")
                'InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY)
            Next
        End If
    End Sub

    Private Function FormarBarCode(ByVal Codigo As String) As String

        Dim Barcode As String = vbEmpty
        'Barcode = Format("*{0}*", Codigo)
        If Len(Codigo) = 1 Then
            Barcode = "*00" & Codigo & "*"
        ElseIf Len(Codigo) = 2 Then
            Barcode = "*0" & Codigo & "*"
        ElseIf Len(Codigo) = 3 Then
            Barcode = "*" & Codigo & "*"
        End If

        Return Barcode
    End Function


    Private Sub CreaTablaImprime()
        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Int32")
        myDataColumn.ColumnName = "Campo"
        myDataColumn.ReadOnly = False
        myDataColumn.Unique = False
        MyTablaImprime.Columns.Add(myDataColumn)


        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.String")
        myDataColumn.ColumnName = "Descripcion"
        myDataColumn.ReadOnly = False
        myDataColumn.Unique = False
        MyTablaImprime.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.String")
        myDataColumn.ColumnName = "Valor"
        myDataColumn.ReadOnly = False
        myDataColumn.Unique = False
        MyTablaImprime.Columns.Add(myDataColumn)


        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Single")
        myDataColumn.ColumnName = "PosDescX"
        myDataColumn.ReadOnly = False
        myDataColumn.Unique = False
        MyTablaImprime.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Single")
        myDataColumn.ColumnName = "PosDescY"
        myDataColumn.ReadOnly = False
        myDataColumn.Unique = False
        MyTablaImprime.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Single")
        myDataColumn.ColumnName = "PosValX"
        myDataColumn.ReadOnly = False
        myDataColumn.Unique = False
        MyTablaImprime.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Single")
        myDataColumn.ColumnName = "PosValY"
        myDataColumn.ReadOnly = False
        myDataColumn.Unique = False
        MyTablaImprime.Columns.Add(myDataColumn)


        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Single")
        myDataColumn.ColumnName = "Fuente"
        myDataColumn.ReadOnly = False
        myDataColumn.Unique = False
        MyTablaImprime.Columns.Add(myDataColumn)



    End Sub
    Private Sub InsertaRegistro(ByVal Campo As Integer, ByVal Descripcion As String, ByVal Valor As String, ByVal PosDescX As Single,
                           ByVal PosDescY As Single, ByVal PosValX As Single, ByVal PosValY As Single, ByVal vFuente As String)

        myDataRow = MyTablaImprime.NewRow()
        myDataRow("Campo") = Campo
        myDataRow("Descripcion") = Descripcion
        myDataRow("Valor") = Valor
        myDataRow("PosDescX") = PosDescX
        myDataRow("PosDescY") = PosDescY
        myDataRow("PosValX") = PosValX
        myDataRow("PosValY") = PosValY
        myDataRow("Fuente") = vFuente
        MyTablaImprime.Rows.Add(myDataRow)

    End Sub
#End Region


    Private Function Validar() As Boolean
        Validar = True



        '10/ENE/2017
        If Not Inicio.ValidandoCampo(Trim(txtltCombustible.Text), TipoDato.TdNumerico) Then
            MsgBox("La Cantidad no puede ser Cero o Vac�o", vbInformation, "Aviso" & Me.Text)
            If txtltCombustible.Enabled Then
                txtltCombustible.Focus()
                txtltCombustible.SelectAll()
            End If
            Validar = False
        ElseIf cmbDespachador.Text = "" Then
            MsgBox("El Despachador no puede ser Vac�o", vbInformation, "Aviso" & Me.Text)
            cmbDespachador.Focus()
            Validar = False
        End If
        'Else
        'If txtTractor.Text = "" Then
        '    MsgBox("El Tractor no puede ser vac�o", vbInformation, "Aviso" & Me.Text)
        '    txtTractor.Focus()

        '    Validar = False

        '    'ElseIf chkAyu1.Checked Then
        '    '    If cmbayudante1.SelectedValue = 0 Then
        '    '        MsgBox("El Ayudante no puede ser vacio", vbInformation, "Aviso" & Me.Text)
        '    '        If cmbayudante1.Enabled Then
        '    '            cmbayudante1.Focus()
        '    '        End If
        '    '        Validar = False

        '    '    End If
        '    'ElseIf chkAyu2.Checked And Not chkAyu1.Checked Then
        '    '    MsgBox("No puede Seleccionar al Ayudante 2 Hasta que seleccione el Ayudante 1", vbInformation, "Aviso" & Me.Text)
        '    '    chkAyu2.Checked = False
        '    '    If chkAyu1.Enabled Then
        '    '        chkAyu1.Focus()
        '    '    End If

        '    'ElseIf chkAyu2.Checked Then
        '    '    If cmbayudante2.SelectedValue = 0 Then
        '    '        MsgBox("El Ayudante no puede ser vacio", vbInformation, "Aviso" & Me.Text)
        '    '        If cmbayudante2.Enabled Then
        '    '            cmbayudante2.Focus()
        '    '        End If
        '    '        Validar = False

        '    '    End If
        'End If
    End Function

    'Private Function CalculaNoEmpleados() As Integer
    '    If txt Then
    'End Function


    Private Sub txtCantidad_TextChanged(sender As Object, e As EventArgs)

    End Sub

    'Private Function ChecaPersonalOcupadoForma(ByVal vLugarOrigen As Integer) As Integer
    '    Dim LugarOcupado As Integer = 0
    '    'Origen = 1 -> cmbResponsable 
    '    'Origen = 2 -> cmbayudante1 
    '    'Origen = 3 -> cmbayudante2
    '    If vLugarOrigen = 1 Then
    '        If cmbayudante1.SelectedValue = cmbResponsable.SelectedValue Then
    '            LugarOcupado = 2
    '        ElseIf cmbayudante2.SelectedValue = cmbResponsable.SelectedValue Then
    '            LugarOcupado = 3
    '        End If
    '    ElseIf vLugarOrigen = 2 Then
    '        If cmbResponsable.SelectedValue = cmbayudante1.SelectedValue Then
    '            LugarOcupado = 1
    '        ElseIf cmbayudante2.SelectedValue = cmbayudante1.SelectedValue Then
    '            LugarOcupado = 3
    '        End If
    '    ElseIf vLugarOrigen = 3 Then
    '        If cmbResponsable.SelectedValue = cmbayudante2.SelectedValue Then
    '            LugarOcupado = 1
    '        ElseIf cmbayudante1.SelectedValue = cmbayudante2.SelectedValue Then
    '            LugarOcupado = 2
    '        End If

    '    End If
    '    Return LugarOcupado
    'End Function

    'Private Function ChecaPersonalOcupado(ByVal idPersonal As Integer, ByVal NomPersonal As String) As Boolean
    '    Dim Resp As Boolean = False
    '    Personal = New PersonalClass(cmbResponsable.SelectedValue)
    '    If Personal.Existe Then
    '        'aqui()
    '        MyTablaPersOcupado = Personal.TablaMecanicoOCupado("DIAG")
    '        If MyTablaPersOcupado.Rows.Count > 0 Then
    '            Resp = True
    '            With MyTablaPersOcupado.Rows(0)
    '                MsgBox("EL Personal: " & NomPersonal & " tiene Asignado actualmente la Orden de Trabajo: " & .Item("idOrdenTrabajo") & vbCrLf & "de la Orden de Servicio: " & .Item("idordenSer") & "Asigne a Otro Personal", MsgBoxStyle.Information, Me.Text)
    '            End With
    '        End If
    '    End If
    '    Return Resp
    'End Function

    'Private Sub cmbResponsable_SelectedIndexChanged(sender As Object, e As EventArgs)
    '    If Salida Then Exit Sub
    '    If cmbResponsable.SelectedValue > 0 Then
    '        vLugOcupado = ChecaPersonalOcupadoForma(1)
    '        If vLugOcupado > 0 Then
    '            MsgBox("El Personal: " & cmbResponsable.Text & " ya esta Asignado para el" & NombreLugarPuesto(vLugOcupado), MsgBoxStyle.Information, Me.Text)
    '            cmbResponsable.SelectedValue = 0
    '            cmbResponsable.Focus()
    '        Else
    '            If ChecaPersonalOcupado(cmbResponsable.SelectedValue, cmbResponsable.Text) Then
    '                'txtNombreMecanico.Text = cmbResponsable.Text
    '                'txtPuestoMec.Text = cmbPuestoResp.Text
    '            End If

    '        End If
    '    Else

    '    End If



    'End Sub

    'Private Sub chkAyu1_CheckedChanged(sender As Object, e As EventArgs)
    '    ActivaAyudantes(chkAyu1.Checked, TipoNumAyudante.Ayudante1)
    'End Sub

    'Private Sub ActivaAyudantes(ByVal Valor As Boolean, ByVal Ayudante As TipoNumAyudante)
    '    If Ayudante = TipoNumAyudante.Ayudante1 Then
    '        cmbayudante1.Enabled = Valor
    '        cmbPuestoAyu1.Enabled = Valor

    '    ElseIf Ayudante = TipoNumAyudante.Ayudante2 Then
    '        cmbayudante2.Enabled = Valor
    '        cmbPuestoAyu2.Enabled = Valor

    '    End If
    'End Sub

    'Private Sub chkAyu2_CheckedChanged(sender As Object, e As EventArgs)
    '    ActivaAyudantes(chkAyu2.Checked, TipoNumAyudante.Ayudante2)
    'End Sub


    'Private Function ContadordePersonal() As Integer
    '    Dim cont As Integer = 0
    '    If cmbPuestoResp.SelectedValue > 0 Then
    '        cont += 1
    '    End If
    '    If chkAyu1.Checked And cmbayudante1.SelectedValue > 0 Then
    '        cont += 1
    '    End If
    '    If chkAyu2.Checked And cmbayudante2.SelectedValue > 0 Then
    '        cont += 1
    '    End If

    '    Return cont
    'End Function

    'Private Function CalculaCostoManoObra(ByVal vNoHoras As Double) As Double
    '    Dim cmo As Double = 0
    '    If cmbResponsable.SelectedValue > 0 Then
    '        Personal = New PersonalClass(cmbResponsable.SelectedValue)
    '        If Personal.Existe Then
    '            cmo = vNoHoras * Personal.PrecioHora
    '        End If
    '    End If
    '    If chkAyu1.Checked And cmbayudante1.SelectedValue > 0 Then
    '        Personal = New PersonalClass(cmbayudante1.SelectedValue)
    '        If Personal.Existe Then
    '            cmo = cmo + (vNoHoras * Personal.PrecioHora)
    '        End If
    '    End If
    '    If chkAyu2.Checked And cmbayudante2.SelectedValue > 0 Then
    '        Personal = New PersonalClass(cmbayudante2.SelectedValue)
    '        If Personal.Existe Then
    '            cmo = cmo + (vNoHoras * Personal.PrecioHora)
    '        End If
    '    End If

    '    Return cmo
    'End Function

    Private Function CalculaCostoRefacciones() As Double
        Dim cre As Double = 0
        If MyTablaRefac.Rows.Count > 0 Then
            For i = 0 To MyTablaRefac.Rows.Count - 1
                cre = cre + (MyTablaRefac.DefaultView.Item(i).Item("trefCantSalida") * MyTablaRefac.DefaultView.Item(i).Item("trefCOSTO"))
            Next
        End If
        Return cre
    End Function

    Private Function NombreLugarPuesto(ByVal vLugar As Integer) As String
        Dim Resp As String = ""
        If vLugar = 1 Then
            Resp = " Responsable "
        ElseIf vLugar = 2 Then
            Resp = " Ayudante 1 "
        ElseIf vLugar = 3 Then
            Resp = " Ayudante 2 "
        End If
        Return Resp
    End Function


    '    Private Sub txtTractor_KeyDown(sender As Object, e As KeyEventArgs)
    '        If e.KeyCode = Keys.F3 Then
    '            CargaForm(txtTractor.Text, "UNITRANS", "TC", txtTractor,
    '" tip.clasificacion = 'tractor' AND UNI.ESTATUS = 'ACT' AND UNI.IDEMPRESA = " & _cveEmpresa & " AND UNI.IDUNIDADTRANS LIKE '%" & Val(txtTractor.Text) & "%'", True)

    '            'txtTractor.Text = DespliegaGrid(UCase("UNITRANSTIPO"), Inicio.CONSTR, "", FiltroSinWhere)
    '            'txtTractor_Leave(sender, e)
    '        End If
    '    End Sub

    'Private Sub cmbayudante1_SelectedIndexChanged(sender As Object, e As EventArgs)
    '    If Salida Then Exit Sub
    '    If cmbayudante1.SelectedValue > 0 Then
    '        vLugOcupado = ChecaPersonalOcupadoForma(2)
    '        If vLugOcupado > 0 Then
    '            MsgBox("El Personal: " & cmbayudante1.Text & " ya esta Asignado para el " & NombreLugarPuesto(vLugOcupado))
    '            cmbayudante1.SelectedValue = 0
    '            cmbayudante1.Focus()
    '        Else
    '            If ChecaPersonalOcupado(cmbayudante1.SelectedValue, cmbayudante1.Text) Then
    '                'txtPuestoMec.Text = cmbayudante1.Text
    '            End If

    '        End If
    '    Else

    '    End If
    'End Sub

    'Private Sub cmbayudante2_SelectedIndexChanged(sender As Object, e As EventArgs)
    '    'If Salida Then Exit Sub
    '    'If cmbayudante2.SelectedValue > 0 Then
    '    '    If ChecaPersonalOcupado(cmbayudante2.SelectedValue, cmbayudante2.Text) Then

    '    '    End If
    '    'Else

    '    'End If
    '    If Salida Then Exit Sub
    '    If cmbayudante2.SelectedValue > 0 Then
    '        vLugOcupado = ChecaPersonalOcupadoForma(3)
    '        If vLugOcupado > 0 Then
    '            MsgBox("El Personal: " & cmbayudante2.Text & " ya esta Asignado para el " & NombreLugarPuesto(vLugOcupado))
    '            cmbayudante2.SelectedValue = 0
    '            cmbayudante2.Focus()
    '        Else
    '            If ChecaPersonalOcupado(cmbayudante2.SelectedValue, cmbayudante2.Text) Then
    '                'txtPuestoMec.Text = cmbayudante2.Text
    '            End If

    '        End If
    '    Else

    '    End If
    'End Sub

    'Private Sub cmbPuestoResp_SelectedIndexChanged(sender As Object, e As EventArgs)
    '    If Salida Then Exit Sub
    '    If cmbResponsable.SelectedValue > 0 Then
    '        vLugOcupado = ChecaPersonalOcupadoForma(1)
    '        If vLugOcupado > 0 Then
    '            MsgBox("El Personal: " & cmbResponsable.Text & " ya esta Asignado para el" & NombreLugarPuesto(vLugOcupado), MsgBoxStyle.Information, Me.Text)
    '            cmbResponsable.SelectedValue = 0
    '            cmbResponsable.Focus()
    '        Else
    '            If ChecaPersonalOcupado(cmbResponsable.SelectedValue, cmbResponsable.Text) Then
    '                'txtNombreMecanico.Text = cmbResponsable.Text
    '                'txtPuestoMec.Text = cmbPuestoResp.Text
    '            End If

    '        End If
    '    Else

    '    End If
    'End Sub

    'Private Sub cmbPuestoAyu1_SelectedIndexChanged(sender As Object, e As EventArgs)
    '    If Salida Then Exit Sub
    '    If cmbayudante1.SelectedValue > 0 Then
    '        vLugOcupado = ChecaPersonalOcupadoForma(2)
    '        If vLugOcupado > 0 Then
    '            MsgBox("El Personal: " & cmbayudante1.Text & " ya esta Asignado para el " & NombreLugarPuesto(vLugOcupado))
    '            cmbayudante1.SelectedValue = 0
    '            cmbayudante1.Focus()
    '        Else
    '            If ChecaPersonalOcupado(cmbayudante1.SelectedValue, cmbayudante1.Text) Then
    '                'txtPuestoMec.Text = cmbayudante1.Text
    '            End If

    '        End If
    '    Else

    '    End If
    'End Sub

    'Private Sub cmbPuestoAyu2_SelectedIndexChanged(sender As Object, e As EventArgs)
    '    If Salida Then Exit Sub
    '    If cmbayudante2.SelectedValue > 0 Then
    '        vLugOcupado = ChecaPersonalOcupadoForma(3)
    '        If vLugOcupado > 0 Then
    '            MsgBox("El Personal: " & cmbayudante2.Text & " ya esta Asignado para el " & NombreLugarPuesto(vLugOcupado))
    '            cmbayudante2.SelectedValue = 0
    '            cmbayudante2.Focus()
    '        Else
    '            If ChecaPersonalOcupado(cmbayudante2.SelectedValue, cmbayudante2.Text) Then
    '                'txtPuestoMec.Text = cmbayudante2.Text
    '            End If

    '        End If
    '    Else

    '    End If
    'End Sub

    'Private Sub txtTractor_KeyUp(sender As Object, e As KeyEventArgs)
    '    If txtTractor.Text.Length = 0 Then
    '        txtTractor.Focus()
    '        txtTractor.SelectAll()
    '    Else
    '        If e.KeyValue = Keys.Enter Then
    '            Salida = True
    '            CargaForm(txtTractor.Text, "UNITRANS", "TC", txtTractor,
    '            " tip.clasificacion = 'tractor' AND UNI.ESTATUS = 'ACT' AND UNI.IDEMPRESA = " & _cveEmpresa & " AND UNI.IDUNIDADTRANS LIKE '%" & Val(txtTractor.Text) & "%'", True)
    '            ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)
    '            chkBoxTipoOrdServ.Enabled = True
    '            txtRemolque1.Focus()
    '            txtRemolque1.SelectAll()
    '            Salida = False
    '        Else
    '            Salida = False
    '        End If
    '    End If
    'End Sub

    'Private Sub txtTractor_Leave(sender As Object, e As EventArgs)
    '    If Salida Then Exit Sub
    '    'If txtTractor.Text.Length = 0 Then
    '    '    txtTractor.Focus()
    '    '    txtTractor.SelectAll()
    '    'Else
    '    If txtTractor.Enabled Then
    '        CargaForm(txtTractor.Text, "UNITRANS", "TC", txtTractor,
    '        " tip.clasificacion = 'tractor' AND UNI.ESTATUS = 'ACT' AND UNI.IDEMPRESA = " & _cveEmpresa & " AND UNI.IDUNIDADTRANS LIKE '%" & Val(txtTractor.Text) & "%'", True)
    '        ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)
    '        chkBoxTipoOrdServ.Enabled = True
    '        txtRemolque1.Focus()
    '        txtRemolque1.SelectAll()
    '    End If
    '    'End If

    'End Sub


    'Private Sub txtRemolque1_KeyUp(sender As Object, e As KeyEventArgs)

    '    If e.KeyValue = Keys.Enter Then
    '        Salida = True
    '        'CargaForm(txtRemolque1.Text, "UNITRANS", "TV", txtRemolque1,
    '        '" tip.clasificacion = 'Tolva' AND UNI.ESTATUS = 'ACT' AND UNI.IDEMPRESA = " & _cveEmpresa & " AND UNI.IDUNIDADTRANS LIKE '%" & Val(txtRemolque1.Text) & "%'")

    '        CargaForm(txtRemolque1.Text, "UNITRANS", "", txtRemolque1,
    '        " tip.clasificacion = 'Tolva' AND UNI.ESTATUS = 'ACT' AND UNI.IDEMPRESA = " & _cveEmpresa & " AND UNI.IDUNIDADTRANS LIKE '%" & Val(txtRemolque1.Text) & "%'")

    '        txtDolly.Focus()
    '        txtDolly.SelectAll()
    '        Salida = False
    '    Else
    '        Salida = False
    '    End If


    'End Sub

    'Private Sub txtRemolque1_Leave(sender As Object, e As EventArgs)
    '    If Salida Then Exit Sub

    '    If txtRemolque1.Enabled Then
    '        CargaForm(txtRemolque1.Text, "UNITRANS", "", txtRemolque1,
    '        " tip.clasificacion = 'Tolva' AND UNI.ESTATUS = 'ACT' AND UNI.IDEMPRESA = " & _cveEmpresa & " AND UNI.IDUNIDADTRANS LIKE '%" & Val(txtRemolque1.Text) & "%'")

    '        txtDolly.Focus()
    '        txtDolly.SelectAll()
    '    End If


    'End Sub

    'Private Sub txtDolly_KeyUp(sender As Object, e As KeyEventArgs)

    '    If e.KeyValue = Keys.Enter Then
    '        Salida = True
    '        CargaForm(txtDolly.Text, "UNITRANS", "DL", txtDolly,
    '        " tip.clasificacion = 'Dolly' AND UNI.ESTATUS = 'ACT' AND UNI.IDEMPRESA = " & _cveEmpresa & " AND UNI.IDUNIDADTRANS LIKE '%" & Val(txtDolly.Text) & "%'")
    '        txtRemolque2.Focus()
    '        txtRemolque2.SelectAll()
    '        Salida = False
    '    Else
    '        Salida = False
    '    End If


    'End Sub

    'Private Sub txtDolly_Leave(sender As Object, e As EventArgs)
    '    If Salida Then Exit Sub

    '    If txtDolly.Enabled Then
    '        CargaForm(txtDolly.Text, "UNITRANS", "DL", txtDolly,
    '        " tip.clasificacion = 'Dolly' AND UNI.ESTATUS = 'ACT' AND UNI.IDEMPRESA = " & _cveEmpresa & " AND UNI.IDUNIDADTRANS LIKE '%" & Val(txtDolly.Text) & "%'")
    '        txtRemolque2.Focus()
    '        txtRemolque2.SelectAll()
    '    End If


    'End Sub

    Private Sub txtDolly_TextChanged(sender As Object, e As EventArgs)

    End Sub

    'Private Sub txtRemolque2_KeyUp(sender As Object, e As KeyEventArgs)

    '    If e.KeyValue = Keys.Enter Then
    '        Salida = True
    '        CargaForm(txtRemolque2.Text, "UNITRANS", "", txtRemolque2,
    '        " tip.clasificacion = 'Tolva' AND UNI.ESTATUS = 'ACT' AND UNI.IDEMPRESA = " & _cveEmpresa & " AND UNI.IDUNIDADTRANS LIKE '%" & Val(txtRemolque2.Text) & "%'")
    '        cmbChofer.Focus()
    '        Salida = False
    '    Else
    '        Salida = False
    '    End If


    'End Sub

    'Private Sub txtRemolque2_Leave(sender As Object, e As EventArgs)
    '    If Salida Then Exit Sub

    '    If txtRemolque2.Enabled Then
    '        CargaForm(txtRemolque2.Text, "UNITRANS", "", txtRemolque2,
    '        " tip.clasificacion = 'Tolva' AND UNI.ESTATUS = 'ACT' AND UNI.IDEMPRESA = " & _cveEmpresa & " AND UNI.IDUNIDADTRANS LIKE '%" & Val(txtRemolque2.Text) & "%'")

    '        cmbChofer.Focus()
    '    End If


    'End Sub

    Private Sub txtRemolque2_TextChanged(sender As Object, e As EventArgs)

    End Sub

    Private Sub cmbChofer_KeyUp(sender As Object, e As KeyEventArgs) Handles cmbChofer.KeyUp
        If e.KeyValue = Keys.Enter Then
            Salida = True
            btnMnuOk_Click(sender, e)
            'cmbAutori
            Salida = False
        End If
    End Sub



    Private Sub cmbAutoriza_KeyUp(sender As Object, e As KeyEventArgs)
        If e.KeyValue = Keys.Enter Then
            Salida = True
            'chkBoxTipoOrdServ.Focus()

            Salida = False
        End If

    End Sub

    'Private Function IniciaOrdenTrabajo(ByVal IdTipo As Integer, ByVal nomTipOrdServ As String, ByVal NumOrdServ As Integer,
    '                                    ByVal bModifica As Boolean, ByVal TipoServ As Integer, ByVal IdUnidadTrans As String) As DataTable

    '    'Dim list As List(Of UnidadesTranspClass)
    '    'Dim list = cmbTipoServ.SelectedItem


    '    Dim listObj As List(Of UnidadesTranspClass) = New List(Of UnidadesTranspClass)()
    '    If bModifica Then
    '        UniTrans = New UnidadesTranspClass(IdUnidadTrans)
    '        listObj.Add(UniTrans)
    '    Else
    '        UniTrans = New UnidadesTranspClass(txtTractor.Text)
    '        listObj.Add(UniTrans)
    '        If txtRemolque1.Text <> "" Then
    '            UniTrans = New UnidadesTranspClass(txtRemolque1.Text)
    '            listObj.Add(UniTrans)
    '        End If
    '        If txtDolly.Text <> "" Then
    '            UniTrans = New UnidadesTranspClass(txtDolly.Text)
    '            listObj.Add(UniTrans)
    '        End If
    '        If txtRemolque2.Text <> "" Then
    '            UniTrans = New UnidadesTranspClass(txtRemolque2.Text)
    '            listObj.Add(UniTrans)
    '        End If

    '    End If



    '    Dim frm As New fOrdenTrab(_con, _Usuario, _cveEmpresa, IdTipo, nomTipOrdServ, NumOrdServ, listObj, bModifica, TipoServ)
    '    If frm.ShowDialog = Windows.Forms.DialogResult.OK Then
    '        'Hace algo
    '        If frm.MyTablaRes.Rows.Count > 0 Then
    '            If grdOrdenServCab.Rows.Count > 0 Then
    '                Clave = grdOrdenServCab.Item("oscCLAVE", grdOrdenServCab.CurrentCell.RowIndex).Value
    '                FiltraDetallexClave(Clave)
    '            End If

    '        End If
    '        frm.Dispose()
    '        Return frm.MyTablaRes
    '    Else
    '        frm.Dispose()
    '        Return Nothing
    '    End If

    'End Function

    'Private Sub btnAgregaOS_Click(sender As Object, e As EventArgs)
    '    'Agregar del listado las diferentes ordenes de servicio.
    '    'Empezaremos con Combustible que es con un servicio Fijo.
    '    'Dim s As String = ""
    '    If chkBoxTipoOrdServ.CheckedItems.Count > 0 Then
    '        vNumOrdServ = 0

    '        'Tipos de Orden de Servicio Palomiados
    '        Dim list = chkBoxTipoOrdServ.CheckedItems

    '        Dim listObj As List(Of TiposOrdenServicio) = New List(Of TiposOrdenServicio)()
    '        For Each obj In list
    '            listObj.Add(CType(obj, TiposOrdenServicio))
    '        Next

    '        Dim ObjTipOrdServ As New TiposOrdenServicio()
    '        Dim vIdTipo As Integer = 0
    '        Dim myTabla As DataTable
    '        Dim vCatalogo As String
    '        'Dim vNomCampo As String
    '        Dim vValorId As Integer
    '        Dim tablaRes As DataTable
    '        Dim vUnidadTrans As String = ""
    '        Dim vNotaRecepcion As String = ""
    '        Dim vIdDivision As Integer = 0
    '        Dim vNomDivision As String = ""
    '        Dim vidServicio As Integer = 0
    '        Dim vidActividad As Integer = 0
    '        Dim vDuracionAct As Integer = 0
    '        Dim vNomServicio As String = ""

    '        For Each lobj In listObj
    '            vIdTipo = lobj.idTipoOrden
    '            If lobj.bOrdenTrab Then
    '                'Si tiene que abrir la Forma del Detalle
    '                vNumOrdServ += 1
    '                tablaRes = New DataTable()
    '                tablaRes = IniciaOrdenTrabajo(vIdTipo, lobj.nomTipOrdServ, vNumOrdServ, False, 0, "")
    '                If Not tablaRes Is Nothing Then
    '                    If tablaRes.Rows.Count > 0 Then
    '                        vUnidadTrans = ""
    '                        Dim dv As New DataView(tablaRes)
    '                        dv.Sort = "totIdUnidadTransp"
    '                        Dim DTOrdenado As DataTable = dv.ToTable
    '                        For i = 0 To DTOrdenado.Rows.Count - 1
    '                            If vUnidadTrans = "" And i = 0 Then
    '                                vUnidadTrans = DTOrdenado.DefaultView.Item(i).Item("totIdUnidadTransp")
    '                            End If

    '                            If vUnidadTrans = DTOrdenado.DefaultView.Item(i).Item("totIdUnidadTransp") Then
    '                                'vUnidadTrans = DTOrdenado.DefaultView.Item(i).Item("totIdUnidadTransp")
    '                                vNotaRecepcion = DTOrdenado.DefaultView.Item(i).Item("totNotaRecepcion")
    '                                vIdDivision = DTOrdenado.DefaultView.Item(i).Item("totidDivision")
    '                                vNomDivision = DTOrdenado.DefaultView.Item(i).Item("totNomDivision")
    '                                vidServicio = DTOrdenado.DefaultView.Item(i).Item("totidServicio")
    '                                If vidServicio > 0 Then
    '                                    Servicio = New ServicioClass(vidServicio)
    '                                    If Servicio.Existe Then
    '                                        vNomServicio = Servicio.NomServicio
    '                                    Else
    '                                        vNomServicio = ""
    '                                    End If
    '                                Else
    '                                    vNomServicio = ""
    '                                End If

    '                                vidActividad = DTOrdenado.DefaultView.Item(i).Item("totidActividad")
    '                                vDuracionAct = DTOrdenado.DefaultView.Item(i).Item("totDuracionActHr")
    '                                vTipoServ = DTOrdenado.DefaultView.Item(i).Item("totTipoServ")

    '                                InsertaRegGridDet(vNumOrdServ, vidServicio, vNomServicio, vidActividad, vNotaRecepcion, vIdDivision,
    '                                                  vNomDivision, vDuracionAct, 0, 0, False, 0)

    '                                If i = DTOrdenado.Rows.Count - 1 Then
    '                                    UniTrans = New UnidadesTranspClass(vUnidadTrans)
    '                                    TipoUniTrans = New TipoUniTransClass(UniTrans.idTipoUnidad)

    '                                    '10/ENE/2017 - KILOMETRAJE CERO 
    '                                    InsertaRegGridCab(vNumOrdServ, vIdTipo, lobj.nomTipOrdServ, vUnidadTrans, UniTrans.idTipoUnidad,
    '                                    TipoUniTrans.nomTipoUniTras, 0, cmbAutoriza.SelectedValue, lobj.bOrdenTrab, vTipoServ, False, False)

    '                                End If

    '                            Else
    '                                'CAMBIO DE UNIDAD
    '                                'Insertamos Cabecero Anterior

    '                                '10/ENE/2017 -> KILOMETRAJE 0
    '                                InsertaRegGridCab(vNumOrdServ, vIdTipo, lobj.nomTipOrdServ, txtTractor.Text, UniTrans.idTipoUnidad,
    '                                 TipoUniTrans.nomTipoUniTras, 0, cmbAutoriza.SelectedValue, lobj.bOrdenTrab, vTipoServ, False, False)

    '                                vNumOrdServ += 1
    '                                vUnidadTrans = DTOrdenado.DefaultView.Item(i).Item("totIdUnidadTransp")
    '                                vNotaRecepcion = DTOrdenado.DefaultView.Item(i).Item("totNotaRecepcion")
    '                                vIdDivision = DTOrdenado.DefaultView.Item(i).Item("totidDivision")
    '                                vNomDivision = DTOrdenado.DefaultView.Item(i).Item("totNomDivision")
    '                                vidServicio = DTOrdenado.DefaultView.Item(i).Item("totidServicio")
    '                                vidActividad = DTOrdenado.DefaultView.Item(i).Item("totidActividad")
    '                                vDuracionAct = DTOrdenado.DefaultView.Item(i).Item("totDuracionActHr")
    '                                vTipoServ = DTOrdenado.DefaultView.Item(i).Item("totTipoServ")

    '                                InsertaRegGridDet(vNumOrdServ, vidServicio, vNotaRecepcion, vidActividad, "", vIdDivision,
    '                                                  vNomDivision, vDuracionAct, 0, 0, False, 0)

    '                                If i = DTOrdenado.Rows.Count - 1 Then
    '                                    UniTrans = New UnidadesTranspClass(vUnidadTrans)
    '                                    TipoUniTrans = New TipoUniTransClass(UniTrans.idTipoUnidad)

    '                                    '10/ENE/2017 -> KILOMETRAJE 0
    '                                    InsertaRegGridCab(vNumOrdServ, vIdTipo, lobj.nomTipOrdServ, vUnidadTrans, UniTrans.idTipoUnidad,
    '                                    TipoUniTrans.nomTipoUniTras, 0, cmbAutoriza.SelectedValue, lobj.bOrdenTrab, vTipoServ, False, False)

    '                                End If
    '                            End If
    '                        Next

    '                    End If
    '                End If

    '            Else
    '                'Checamos Si Existen Relaciones Automaticas para Este Tipo de Orden de Servicio
    '                myTabla = New DataTable
    '                myTabla = ObjTipOrdServ.TablaRelTipOrdServ("rel.idTipOrdServ = " & vIdTipo)
    '                If myTabla.Rows.Count > 0 Then
    '                    For i = 0 To myTabla.Rows.Count - 1
    '                        vCatalogo = myTabla.DefaultView.Item(i).Item("NomCatalogo").ToString
    '                        'vNomCampo = myTabla.DefaultView.Item(i).Item("NombreId").ToString
    '                        vValorId = myTabla.DefaultView.Item(i).Item("IdRelacion")
    '                        Select Case UCase(vCatalogo)
    '                            Case UCase("catservicios")
    '                                Servicio = New ServicioClass(vValorId)
    '                                If Servicio.Existe Then
    '                                    'Recorrer todas las unidades de TRansporte
    '                                    For Each txtCTR As Control In Me.GroupBox4.Controls
    '                                        If TypeOf txtCTR Is TextBox Then
    '                                            If txtCTR.Tag >= 1 And txtCTR.Tag <= 4 Then
    '                                                'Agregar Orden de Servicio CAB y DET
    '                                                If txtCTR.Text <> "" Then
    '                                                    UniTrans = New UnidadesTranspClass(txtCTR.Text)
    '                                                    TipoUniTrans = New TipoUniTransClass(UniTrans.idTipoUnidad)
    '                                                    If TipoUniTrans.bCombustible Then
    '                                                        vNumOrdServ += 1

    '                                                        '10/ENE/2017 -> KILOMETRAJE 0
    '                                                        InsertaRegGridCab(vNumOrdServ, vIdTipo, lobj.nomTipOrdServ, UniTrans.iUnidadTrans, UniTrans.idTipoUnidad,
    '                                                        TipoUniTrans.nomTipoUniTras, 0, cmbAutoriza.SelectedValue, lobj.bOrdenTrab, Servicio.idTipoServicio, False, False)

    '                                                        InsertaRegGridDet(vNumOrdServ, vValorId, Servicio.NomServicio, 0, "", 0, "", 0, 0, 0, False, 0)
    '                                                    End If
    '                                                End If
    '                                            End If
    '                                        End If
    '                                    Next
    '                                End If
    '                        End Select
    '                    Next
    '                Else
    '                    'No tiene RElaciones
    '                End If
    '            End If


    '        Next
    '    End If
    'End Sub
    'Private Function InsertaRegGridCab(ByVal ClaveFicticia As Integer, ByVal IdTipoOrdServ As Integer, ByVal NomTipoOrdServ As String,
    '                                   ByVal idUnidadTrans As String, ByVal idTipoUnidadTrans As Integer, ByVal nomTipoUniTras As String,
    '                                   ByVal Km As Integer, ByVal idEmpleadoAutoriza As Integer, ByVal bOrdenTrab As Boolean,
    '                                   ByVal idTipoServ As Integer, ByVal bModificado As Boolean, ByVal bCancelado As Boolean) As Boolean
    '    Try

    '        grdOrdenServCab.Rows.Add(ClaveFicticia, ClaveFicticia, 0, dtpFechaCap.Value, IdTipoOrdServ, NomTipoOrdServ,
    '        idUnidadTrans, idTipoUnidadTrans, nomTipoUniTras, Km, idEmpleadoAutoriza, bOrdenTrab, idTipoServ, IIf(bModificado, 1, 0), IIf(bCancelado, 1, 0))

    '        grdOrdenServCab.PerformLayout()
    '        Return True

    '    Catch ex As Exception
    '        Return False
    '        MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, Me.Text)
    '    End Try

    'End Function

    'Private Function InsertaRegGridDet(ByVal ClaveFicticia As Integer, ByVal idServicio As Integer,
    'ByVal NomServicio As String, ByVal idActividad As Integer, ByVal NombreAct As String,
    'ByVal idDivision As Integer, ByVal NomDivision As String, ByVal DuracionActHr As Integer,
    'ByVal IdOrdenSer As Integer, ByVal idOrdenTrabajo As Integer, ByVal bModificado As Boolean, ByVal idOrdenActividad As Integer)
    '    Try

    '        grdOrdenServDet.Rows.Add(ClaveFicticia, ClaveFicticia, idServicio, NomServicio, idActividad, NombreAct, idOrdenTrabajo,
    '                                 idDivision, NomDivision, DuracionActHr, IIf(bModificado, 1, 0), idOrdenActividad)

    '        myDataRow = MyTablaDetalle.NewRow()
    '        myDataRow("tosdCLAVE") = ClaveFicticia
    '        myDataRow("tosdIdOrdenSer") = IdOrdenSer
    '        myDataRow("tosdidServicio") = idServicio
    '        myDataRow("tosdNomServicio") = NomServicio
    '        myDataRow("tosdidActividad") = idActividad
    '        myDataRow("tosdNombreAct") = NombreAct
    '        myDataRow("tosdidOrdenTrabajo") = idOrdenTrabajo
    '        myDataRow("tosdidDivision") = idDivision
    '        myDataRow("tosdNomDivision") = NomDivision
    '        myDataRow("tosdDuracionActHr") = DuracionActHr

    '        myDataRow("tosdbModificado") = bModificado
    '        myDataRow("tosdidOrdenActividad") = idOrdenActividad


    '        MyTablaDetalle.Rows.Add(myDataRow)

    '        grdOrdenServDet.PerformLayout()
    '        Return True

    '    Catch ex As Exception
    '        Return False
    '        MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, Me.Text)
    '    End Try

    'End Function



    '10/ENE/2017
    'Private Sub txtKilometraje_KeyUp(sender As Object, e As KeyEventArgs)
    '    If txtKilometraje.Text.Length = 0 Then
    '        txtKilometraje.Focus()
    '        txtKilometraje.SelectAll()
    '    Else
    '        If e.KeyValue = Keys.Enter Then
    '            Salida = True
    '            If Val(txtKilometraje.Text) > vUltKm And Val(txtKilometraje.Text) <= (vUltKm + MaximoKmdeMas) Then
    '                chkBoxTipoOrdServ.Focus()
    '            Else
    '                MsgBox("El Kilometraje no es Valido", MsgBoxStyle.Exclamation, Me.Text)
    '                txtKilometraje.Focus()
    '                txtKilometraje.SelectAll()
    '            End If
    '            Salida = False
    '        End If
    '    End If
    'End Sub

    '10/ENE/2017
    'Private Sub txtKilometraje_Leave(sender As Object, e As EventArgs)
    '    If Salida Then Exit Sub
    '    If txtKilometraje.Text.Length = 0 Then
    '        txtKilometraje.Focus()
    '        txtKilometraje.SelectAll()
    '    Else
    '        If txtKilometraje.Enabled Then
    '            If Val(txtKilometraje.Text) > vUltKm And Val(txtKilometraje.Text) <= (vUltKm + MaximoKmdeMas) Then
    '                chkBoxTipoOrdServ.Focus()
    '            Else
    '                MsgBox("El Kilometraje no es Valido", MsgBoxStyle.Exclamation, Me.Text)
    '                txtKilometraje.Focus()
    '                txtKilometraje.SelectAll()
    '            End If
    '        End If
    '    End If

    'End Sub


    'Private Sub grdOrdenServCab_Click(sender As Object, e As EventArgs)
    '    Clave = grdOrdenServCab.Item("oscCLAVE", grdOrdenServCab.CurrentCell.RowIndex).Value
    '    FiltraDetallexClave(Clave)
    'End Sub

    Private Sub CreaTablaDetalle()
        Try
            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Int64")
            myDataColumn.ColumnName = "tosdCLAVE"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaDetalle.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Int64")
            myDataColumn.ColumnName = "tosdIdOrdenSer"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaDetalle.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Int64")
            myDataColumn.ColumnName = "tosdidServicio"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaDetalle.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.String")
            myDataColumn.ColumnName = "tosdNomServicio"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaDetalle.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Int64")
            myDataColumn.ColumnName = "tosdidActividad"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaDetalle.Columns.Add(myDataColumn)


            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.String")
            myDataColumn.ColumnName = "tosdNombreAct"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaDetalle.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Int64")
            myDataColumn.ColumnName = "tosdidOrdenTrabajo"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaDetalle.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Int64")
            myDataColumn.ColumnName = "tosdidDivision"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaDetalle.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.String")
            myDataColumn.ColumnName = "tosdNomDivision"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaDetalle.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Int64")
            myDataColumn.ColumnName = "tosdDuracionActHr"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaDetalle.Columns.Add(myDataColumn)


            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Boolean")
            myDataColumn.ColumnName = "tosdbModificado"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaDetalle.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Int64")
            myDataColumn.ColumnName = "tosdidOrdenActividad"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaDetalle.Columns.Add(myDataColumn)

        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try

    End Sub

    'Private Sub FiltraDetallexClave(ByVal Clave As Integer)
    '    MyTablaDetalle.DefaultView.RowFilter = Nothing
    '    MyTablaDetalle.DefaultView.RowFilter = "tosdCLAVE = " & Clave

    '    grdOrdenServDet.Rows.Clear()
    '    For i = 0 To MyTablaDetalle.DefaultView.Count - 1
    '        grdOrdenServDet.Rows.Add()
    '        grdOrdenServDet.Item("osdCLAVE", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdCLAVE")
    '        grdOrdenServDet.Item("osdIdOrdenSer", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdIdOrdenSer")
    '        grdOrdenServDet.Item("osdidServicio", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdidServicio")
    '        grdOrdenServDet.Item("osdNomServicio", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdNomServicio")
    '        grdOrdenServDet.Item("osdidActividad", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdidActividad")
    '        grdOrdenServDet.Item("osdNombreAct", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdNombreAct")
    '        grdOrdenServDet.Item("osdidOrdenTrabajo", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdidOrdenTrabajo")
    '        grdOrdenServDet.Item("osdidDivision", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdidDivision")
    '        grdOrdenServDet.Item("osdNomDivision", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdNomDivision")

    '        grdOrdenServDet.Item("osdDuracionActHr", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdDuracionActHr")
    '        grdOrdenServDet.Item("osdModificado", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdbModificado")
    '        grdOrdenServDet.Item("osdidOrdenActividad", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdidOrdenActividad")


    '        'grdOrdenServDet.DataSource = MyTablaDetalle
    '    Next
    '    grdOrdenServDet.PerformLayout()
    '    'MyTablaDetalle.DefaultView.RowFilter = Nothing
    'End Sub

    'Private Sub AgregaTODOSDET()
    '    MyTablaDetalle.DefaultView.RowFilter = Nothing

    '    grdOrdenServDet.Rows.Clear()
    '    For i = 0 To MyTablaDetalle.DefaultView.Count - 1
    '        grdOrdenServDet.Rows.Add()
    '        grdOrdenServDet.Item("osdCLAVE", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdCLAVE")
    '        grdOrdenServDet.Item("osdIdOrdenSer", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdIdOrdenSer")
    '        grdOrdenServDet.Item("osdidServicio", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdidServicio")
    '        grdOrdenServDet.Item("osdNomServicio", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdNomServicio")
    '        grdOrdenServDet.Item("osdidActividad", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdidActividad")
    '        grdOrdenServDet.Item("osdNombreAct", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdNombreAct")
    '        grdOrdenServDet.Item("osdidOrdenTrabajo", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdidOrdenTrabajo")
    '        grdOrdenServDet.Item("osdidDivision", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdidDivision")
    '        grdOrdenServDet.Item("osdNomDivision", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdNomDivision")

    '        grdOrdenServDet.Item("osdDuracionActHr", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdDuracionActHr")
    '        grdOrdenServDet.Item("osdModificado", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdbModificado")
    '        grdOrdenServDet.Item("osdidOrdenActividad", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdidOrdenActividad")

    '        'grdOrdenServDet.DataSource = MyTablaDetalle
    '    Next
    '    grdOrdenServDet.PerformLayout()
    '    'MyTablaDetalle.DefaultView.RowFilter = Nothing
    'End Sub

    'Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
    '    'ImprimeOrdenServicio(271, False, True, True)




    '    ImprimeOrdenServicio(287, False, True, True)
    '    'ImprimeOrdenServicio(288, True, True, True)
    '    'ImprimeOrdenServicio(289, True, True, True)


    '    'ImprimeOrdenServicio(272, True, True, True)

    '    'ImprimeOrdenServicio(284, False, True, True)
    'End Sub

    'Private Sub txtidPadreOrdSer_KeyDown(sender As Object, e As KeyEventArgs) Handles txtidPadreOrdSer.KeyDown
    '    If e.KeyCode = Keys.F3 Then
    '        txtidPadreOrdSer_Leave(sender, e)
    '    End If

    'End Sub


    'Private Sub txtidPadreOrdSer_KeyUp(sender As Object, e As KeyEventArgs) Handles txtidPadreOrdSer.KeyUp
    '    If e.KeyValue = Keys.Enter Then
    '        Salida = True
    '        CargaForm(txtidPadreOrdSer.Text, "FolioOS", , , , True)
    '        If txtEstatus.Text = "CAP" Then
    '            'ActivaBotones(True, TipoOpcActivaBoton.tOpcInicializa)
    '            ActivaBotones(True, TipoOpcActivaBoton.tOpcEditar)
    '            ActivaCampos(False, TipoOpcActivaCampos.tOpcDEPENDEVALOR)
    '            chkBoxTipoOrdServ.Enabled = False
    '            chkBoxTipoOrdServ.Focus()
    '        Else
    '            ActivaBotones(True, TipoOpcActivaBoton.tOpcConsulta)
    '            ActivaCampos(False, TipoOpcActivaCampos.tOpcDEPENDEVALOR)
    '            chkBoxTipoOrdServ.Enabled = False
    '            grdOrdenServCab.Focus()
    '        End If


    '        Salida = False
    '    Else
    '        Salida = False
    '    End If
    'End Sub

    'Private Sub txtidPadreOrdSer_Leave(sender As Object, e As EventArgs) Handles txtidPadreOrdSer.Leave
    '    If Salida Then Exit Sub
    '    'CargaForm(txtidPadreOrdSer.Text, "FolioOS", , , , True)
    '    'ActivaBotones(True, TipoOpcActivaBoton.tOpcInicializa)
    '    'ActivaCampos(False, TipoOpcActivaCampos.tOpcDEPENDEVALOR)
    '    'chkBoxTipoOrdServ.Focus()
    '    Salida = True
    '    CargaForm(txtidPadreOrdSer.Text, "FolioOS", , , , True)
    '    If txtEstatus.Text = "CAP" Then
    '        'ActivaBotones(True, TipoOpcActivaBoton.tOpcInicializa)
    '        ActivaBotones(True, TipoOpcActivaBoton.tOpcEditar)
    '        ActivaCampos(False, TipoOpcActivaCampos.tOpcDEPENDEVALOR)
    '        chkBoxTipoOrdServ.Enabled = False
    '        chkBoxTipoOrdServ.Focus()
    '    Else
    '        ActivaBotones(True, TipoOpcActivaBoton.tOpcConsulta)
    '        ActivaCampos(False, TipoOpcActivaCampos.tOpcDEPENDEVALOR)
    '        chkBoxTipoOrdServ.Enabled = False
    '        grdOrdenServCab.Focus()
    '    End If
    'End Sub


    Private Sub Button1_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub btnMenuReImprimir_Click(sender As Object, e As EventArgs) Handles btnMenuReImprimir.Click
        Dim Reporte As New ReportView()
        Reporte.createValeGasolina(txtidVCExtra.Text, txtltCombustible.Text, cmbChofer.Text, cmbDespachador.Text, dtpFechaComb.Value, txtUniTrans.Text)
        'If Val(txtidPadreOrdSer.Text) > 0 Then
        '    For i = 0 To grdOrdenServCab.Rows.Count - 1
        '        ImprimeOrdenServicio(grdOrdenServCab.Item("oscCLAVE", i).Value, grdOrdenServCab.Item("oscbOrdenTrabajo", i).Value, True, True)
        '    Next
        'Else
        '    MsgBox("tiene que Existir el numero de FOLIO", MsgBoxStyle.Exclamation, Me.Text)
        'End If
    End Sub

    'Private Sub ModificarToolStripMenuOrdSer_Click(sender As Object, e As EventArgs) Handles ModificarToolStripMenuOrdSer.Click
    '    Try
    '        Dim tablaRes As DataTable
    '        'Dim TablaNewMyTablaDetalle As DataTable
    '        Dim vUnidadTrans As String = ""
    '        Dim vNotaRecepcion As String = ""
    '        Dim vIdDivision As Integer = 0
    '        Dim vNomDivision As String = ""
    '        Dim vidServicio As Integer = 0
    '        Dim vidActividad As Integer = 0
    '        Dim vDuracionAct As Integer = 0
    '        Dim vNomServicio As String = ""
    '        Dim vIdTipo As Integer = 0
    '        Dim vidOrdenActividad As Integer = 0
    '        Dim vIdOrdenTrabajo As Integer = 0

    '        vNumOrdServ = grdOrdenServCab.Item("oscCLAVE", grdOrdenServCab.CurrentCell.RowIndex).Value
    '        vOrdenTrabajo = grdOrdenServCab.Item("oscbOrdenTrabajo", grdOrdenServCab.CurrentCell.RowIndex).Value
    '        vNomTipOrdServ = grdOrdenServCab.Item("oscNomTipOrdServ", grdOrdenServCab.CurrentCell.RowIndex).Value
    '        vTipoOrdServ = grdOrdenServCab.Item("oscTipoOrdenServ", grdOrdenServCab.CurrentCell.RowIndex).Value
    '        vidUnidadTrans = grdOrdenServCab.Item("oscidUnidadTrans", grdOrdenServCab.CurrentCell.RowIndex).Value
    '        vTipoServ = grdOrdenServCab.Item("oscTipoServ", grdOrdenServCab.CurrentCell.RowIndex).Value

    '        If vOrdenTrabajo Then
    '            'Preparar para Abrir
    '            tablaRes = New DataTable()
    '            tablaRes = IniciaOrdenTrabajo(vTipoOrdServ, vNomTipOrdServ, vNumOrdServ, True, vTipoServ, vidUnidadTrans)
    '            If Not tablaRes Is Nothing Then
    '                If tablaRes.Rows.Count > 0 Then
    '                    vUnidadTrans = ""
    '                    'Borramos en el grid lo actual
    '                    'For z = 0 To grdOrdenServDet.Rows.Count - 1
    '                    '    If grdOrdenServDet.Item("osdIdOrdenSer", z).Value = vNumOrdServ Then
    '                    '        grdOrdenServDet.Rows.RemoveAt(z)
    '                    '    End If
    '                    'Next
    '                    'TablaNewMyTablaDetalle = New DataTable
    '                    'TablaNewMyTablaDetalle = MyTablaDetalle
    '                    Dim TablaNewMyTablaDetalle As DataTable = MyTablaDetalle.Copy

    '                    For z = MyTablaDetalle.Rows.Count - 1 To 0 Step -1
    '                        If MyTablaDetalle.Rows.Item(z).Item("tosdIdOrdenSer") = vNumOrdServ Then
    '                            'grdOrdenServDet.Rows.RemoveAt(z)
    '                            'TablaNewMyTablaDetalle.DefaultView.Delete(z)
    '                            TablaNewMyTablaDetalle.Rows.RemoveAt(z)
    '                            'z = z + 1
    '                        End If
    '                    Next
    '                    MyTablaDetalle.Clear()
    '                    MyTablaDetalle = TablaNewMyTablaDetalle

    '                    grdOrdenServDet.Rows.Clear()

    '                    'MyTablaDetalle.Rows.Add(myDataRow)

    '                    For i = 0 To tablaRes.Rows.Count - 1
    '                        If vUnidadTrans = "" And i = 0 Then
    '                            vUnidadTrans = tablaRes.DefaultView.Item(i).Item("totIdUnidadTransp")
    '                        End If
    '                        If vUnidadTrans = tablaRes.DefaultView.Item(i).Item("totIdUnidadTransp") Then
    '                            vNotaRecepcion = tablaRes.DefaultView.Item(i).Item("totNotaRecepcion")
    '                            vIdDivision = tablaRes.DefaultView.Item(i).Item("totidDivision")
    '                            vNomDivision = tablaRes.DefaultView.Item(i).Item("totNomDivision")
    '                            vidServicio = tablaRes.DefaultView.Item(i).Item("totidServicio")
    '                            vIdOrdenTrabajo = tablaRes.DefaultView.Item(i).Item("totidOrdenTrabajo")


    '                            If vidServicio > 0 Then
    '                                Servicio = New ServicioClass(vidServicio)
    '                                If Servicio.Existe Then
    '                                    vNomServicio = Servicio.NomServicio
    '                                Else
    '                                    vNomServicio = ""
    '                                End If
    '                            Else
    '                                vNomServicio = ""
    '                            End If

    '                            vidActividad = tablaRes.DefaultView.Item(i).Item("totidActividad")
    '                            vDuracionAct = tablaRes.DefaultView.Item(i).Item("totDuracionActHr")
    '                            vTipoServ = tablaRes.DefaultView.Item(i).Item("totTipoServ")

    '                            InsertaRegGridDet(vNumOrdServ, vidServicio, vNomServicio, vidActividad, vNotaRecepcion, vIdDivision, vNomDivision, vDuracionAct, vNumOrdServ, vIdOrdenTrabajo, True, tablaRes.DefaultView.Item(i).Item("totidOrdenActividad"))

    '                            If i = tablaRes.Rows.Count - 1 Then
    '                                UniTrans = New UnidadesTranspClass(vUnidadTrans)
    '                                TipoUniTrans = New TipoUniTransClass(UniTrans.idTipoUnidad)

    '                                '10/ENE/2017 -> KILOMETRAJE CERO
    '                                InsertaRegGridCab(vNumOrdServ, vIdTipo, vTipoServ, vUnidadTrans, UniTrans.idTipoUnidad,
    '                                TipoUniTrans.nomTipoUniTras, 0, cmbAutoriza.SelectedValue, vOrdenTrabajo, vTipoServ, True, False)

    '                            End If

    '                        Else
    '                            'CAMBIO DE UNIDAD
    '                            'Insertamos Cabecero Anterior
    '                            '10/ENE/2017 -> KILOMETRAJE 0
    '                            InsertaRegGridCab(vNumOrdServ, vIdTipo, vNomTipOrdServ, "", UniTrans.idTipoUnidad,
    '                             TipoUniTrans.nomTipoUniTras, 0, cmbAutoriza.SelectedValue, vOrdenTrabajo, vTipoServ, True, False)

    '                            vNumOrdServ += 1
    '                            vUnidadTrans = tablaRes.DefaultView.Item(i).Item("totIdUnidadTransp")
    '                            vNotaRecepcion = tablaRes.DefaultView.Item(i).Item("totNotaRecepcion")
    '                            vIdDivision = tablaRes.DefaultView.Item(i).Item("totidDivision")
    '                            vNomDivision = tablaRes.DefaultView.Item(i).Item("totNomDivision")
    '                            vidServicio = tablaRes.DefaultView.Item(i).Item("totidServicio")
    '                            vidActividad = tablaRes.DefaultView.Item(i).Item("totidActividad")
    '                            vDuracionAct = tablaRes.DefaultView.Item(i).Item("totDuracionActHr")
    '                            vTipoServ = tablaRes.DefaultView.Item(i).Item("totTipoServ")

    '                            InsertaRegGridDet(vNumOrdServ, vidServicio, vNomServicio, vidActividad, vNotaRecepcion, vIdDivision, vNomDivision, vDuracionAct, vNumOrdServ, vIdOrdenTrabajo, True, tablaRes.DefaultView.Item(i).Item("totidOrdenActividad"))
    '                            If i = tablaRes.Rows.Count - 1 Then
    '                                UniTrans = New UnidadesTranspClass(vUnidadTrans)
    '                                TipoUniTrans = New TipoUniTransClass(UniTrans.idTipoUnidad)

    '                                '10/ENE/2017 -> KILOMETRAJE 0
    '                                InsertaRegGridCab(vNumOrdServ, vIdTipo, vNomTipOrdServ, vUnidadTrans, UniTrans.idTipoUnidad,
    '                                TipoUniTrans.nomTipoUniTras, 0, cmbAutoriza.SelectedValue, vOrdenTrabajo, vTipoServ, True, False)

    '                            End If
    '                        End If
    '                    Next

    '                End If
    '            End If
    '        End If
    '    Catch ex As Exception
    '        MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
    '    End Try
    'End Sub

    Private Sub ImprimirToolStripMenuOrdSer_Click(sender As Object, e As EventArgs) Handles ImprimirToolStripMenuOrdSer.Click
        Try
            'vNumOrdServ = grdOrdenServCab.Item("oscCLAVE", grdOrdenServCab.CurrentCell.RowIndex).Value
            'vOrdenTrabajo = grdOrdenServCab.Item("oscbOrdenTrabajo", grdOrdenServCab.CurrentCell.RowIndex).Value
            ImprimeOrdenServicio(vNumOrdServ, vOrdenTrabajo, True, True)
        Catch ex As Exception

        End Try
    End Sub


    Private Sub ConMnuOrdSer_Opening(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles ConMnuOrdSer.Opening
        'With grdOrdenServCab.Rows(grdOrdenServCab.CurrentRow.Index)
        '    ImprimirToolStripMenuOrdSer.Enabled = True
        '    CancelarToolStripMenuOrdSer.Enabled = True
        '    If .Cells("oscbOrdenTrabajo").Value Then
        '        ModificarToolStripMenuOrdSer.Enabled = True
        '    Else
        '        ModificarToolStripMenuOrdSer.Enabled = False
        '    End If
        'End With
    End Sub


    Private Sub ToolStripButton1_Click(sender As Object, e As EventArgs) Handles btnMnuCancelar.Click

        Dim fCanc As New FCancelaSolServ(txtNoViaje.Text, txtNomEmpresa.Text, dtpFechaCap.Value, _Usuario)
        fCanc.WindowState = FormWindowState.Normal
        fCanc.Show()
        Cancelar()
        'txtTractor.Focus()

    End Sub

#Region "CODIGO"
    Private Sub ComboTipos()
        Dim obj As New CatTipoOsCombClass(0)
        tabla = New DataTable

        tabla = obj.TablaTiposComb("")
        If tabla.Rows.Count > 0 Then
            cmbOpciones.DataSource = tabla
            cmbOpciones.ValueMember = "IDTIPOOSCOMB"
            cmbOpciones.DisplayMember = "NOMTIPOOSCOMB"
        End If
    End Sub

    Private Sub cmbOpciones_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbOpciones.SelectedIndexChanged
        'If cmbOpciones.SelectedValue IsNot Nothing Then
        '    ValidarOpcion(cmbOpciones.SelectedValue)
        'End If
    End Sub

    Private Sub cmbOpciones_KeyUp(sender As Object, e As KeyEventArgs) Handles cmbOpciones.KeyUp
        If e.KeyValue = Keys.Enter Then
            ValidarOpcion(cmbOpciones.SelectedValue)
        End If
    End Sub

    Private Sub ValidarOpcion(ByVal idTipoOSComb As Integer)
        TipoUnidad = ""
        txtNomEmpresa.Text = ""
        If idTipoOSComb > 0 Then
            TiposComb = New CatTipoOsCombClass(idTipoOSComb)
            If TiposComb.Existe Then
                TipoUnidad = TiposComb.TipoUnidad
                If TiposComb.RequiereViaje Then

                    ActivaCampos(True, TipoOpcActivaCampos.tOpcDEPENDEVALOR)
                    txtNoViaje.Focus()
                Else
                    If TiposComb.TipoUnidad = "TC" Then
                        NomTipoUnidad = "tractor"
                    ElseIf TiposComb.TipoUnidad = "" Then

                    End If
                    'AutoCompletarUnidadesTransp(cmbUnidades, True, 0, NomTipoUnidad)

                    ActivaCampos(True, TipoOpcActivaCampos.tOpcOTRO)
                    'cmbUnidades.Focus()
                End If
            End If
        End If
    End Sub

    Private Sub txtNoViaje_KeyDown(sender As Object, e As KeyEventArgs) Handles txtNoViaje.KeyDown

    End Sub

    Private Sub txtNoViaje_Leave(sender As Object, e As EventArgs) Handles txtNoViaje.Leave
        If Salida Then Exit Sub
        If txtNoViaje.Text.Length = 0 Then
            txtNoViaje.Focus()
            txtNoViaje.SelectAll()
        Else
            If txtNoViaje.Enabled Then
                CargaForm(txtNoViaje.Text, UCase("VIAJE"))
                'cmbUnidades.Focus()
            End If
        End If
    End Sub

    Private Sub txtNoViaje_MouseDown(sender As Object, e As MouseEventArgs) Handles txtNoViaje.MouseDown
        txtNoViaje.Focus()
        txtNoViaje.SelectAll()

    End Sub

    Private Sub txtNoViaje_KeyUp(sender As Object, e As KeyEventArgs) Handles txtNoViaje.KeyUp
        If txtNoViaje.Text.Length = 0 Then
            txtNoViaje.Focus()
            txtNoViaje.SelectAll()
        Else
            If e.KeyValue = Keys.Enter Then
                Salida = True
                CargaForm(txtNoViaje.Text, UCase("VIAJE"))

                'cmbUnidades.Focus()
                Salida = False
            End If
        End If
    End Sub

    'Private Sub LlenaComboUnidaesxViaje(ByVal Unidades As List(Of UnidadesTranspClass))
    '    cmbUnidades.DataSource = Unidades
    '    cmbUnidades.ValueMember = "iUnidadTrans"
    '    cmbUnidades.DisplayMember = "iUnidadTrans"
    'End Sub

    Public Function AutoCompletarUnidadesTransp(ByVal Control As ComboBox, ByVal Activo As Boolean, ByVal idEmpresa As Integer,
    ByVal Tipo As String, Optional ByVal FiltroSinWhere As String = "") As AutoCompleteStringCollection
        Dim Coleccion As New AutoCompleteStringCollection

        UniTrans = New UnidadesTranspClass(0)

        Dim dt As DataTable = UniTrans.TablaUnidadxTipo(Activo, idEmpresa, Tipo, FiltroSinWhere)
        'Recorrer y cargar los items para el Autocompletado
        For Each row As DataRow In dt.Rows
            Coleccion.Add(Convert.ToString(row("idUnidadTrans")))
        Next
        'Ajustamos el control TextBox o ComboBox para recibir los datos de la siguiente manera.
        With Control
            .DataSource = dt
            .DisplayMember = "idUnidadTrans"
            .ValueMember = "idUnidadTrans"

            .AutoCompleteCustomSource = Coleccion
            .AutoCompleteMode = AutoCompleteMode.Suggest
            .AutoCompleteSource = AutoCompleteSource.CustomSource
        End With

        ' Aca le devuelvo los datos a recuperados de la Base
        Return Coleccion
    End Function

    Private Sub btnMnuOk_Click(sender As Object, e As EventArgs) Handles btnMnuOk.Click
        Dim Mensaje As String = ""
        Dim objsql As New ToolSQLs
        Try
            If Not Validar() Then Exit Sub
            Mensaje = "!! Esta seguro que desea Capturar el Vale de Combustible Extra: " & txtidVCExtra.Text & " para la unidad de transporte: " & txtUniTrans.Text & " ? !!"
            If MsgBox(Mensaje, MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then

                'StrSql = objsql.InsertaCombustibleExtra(cmbOpciones.SelectedValue, txtUniTrans.Text, Now, _Usuario, 1, cmbChofer.SelectedValue)
                StrSql = objsql.CapturaCombustibleExtra(txtidVCExtra.Text, Now, txtltCombustible.Text, PrecioCombustible,
                (Val(txtltCombustible.Text) * PrecioCombustible), cmbDespachador.SelectedValue)
                ReDim Preserve ArraySql(indice)
                ArraySql(indice) = StrSql
                indice += 1

                If indice > 0 Then
                    Dim obj As New CapaNegocio.Tablas
                    Dim Respuesta As String
                    'Dim Indentity As String
                    'Dim Respuesta2 As String

                    Respuesta = obj.EjecutarSql(Inicio.CONSTR, "SQL", ArraySql, Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, indice)
                    'FEspera.Close()
                    'Respuesta2 = Respuesta.Substring(0, 9)

                    If Respuesta = "EFECTUADO" Then
                        'Indentity = Respuesta.Substring(10, Respuesta.Length - 10)
                        MsgBox("Se Capturo satisfactoriamente el Vale de Combustible Extra = " & txtidVCExtra.Text, MsgBoxStyle.Exclamation, Me.Text)

                        Dim Reporte As New ReportView()
                        Reporte.createValeGasolina(txtidVCExtra.Text, txtltCombustible.Text, cmbChofer.Text, cmbDespachador.Text, dtpFechaComb.Value, txtUniTrans.Text)


                        'Dim Reporte As New ReportView()
                        'Reporte.c

                        'Imprimir
                    ElseIf Respuesta = "NOEFECTUADO" Then
                    ElseIf Respuesta = "ERROR" Then
                        indice = 0
                        SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")
                    End If
                Else
                    MsgBox("No se Encontraron Registros que Grabar", MsgBoxStyle.Exclamation, Me.Text)
                End If
                Cancelar()
            End If
        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try
    End Sub

    Private Sub cmbUnidades_SelectedIndexChanged(sender As Object, e As EventArgs)

    End Sub

    Private Sub cmbUnidades_KeyUp(sender As Object, e As KeyEventArgs)
        If e.KeyValue = Keys.Enter Then
            Salida = True
            'CargaForm(cmbUnidades.SelectedValue, "UNITRANS", NomTipoUnidad, cmbUnidades,
            '" tip.clasificacion = '" & NomTipoUnidad & "' AND UNI.ESTATUS = 'ACT' AND UNI.IDEMPRESA = " & _cveEmpresa & " AND UNI.IDUNIDADTRANS LIKE '%" & Val(txtTractor.Text) & "%'", True)

            'CargaForm(cmbUnidades.SelectedValue, "UNITRANS", NomTipoUnidad, cmbUnidades,
            '" tip.clasificacion = '" & NomTipoUnidad & "' AND UNI.ESTATUS = 'ACT' AND UNI.IDUNIDADTRANS LIKE '%" & Val(cmbUnidades.SelectedValue) & "%'", True)

            'Actualiza Choferes de la empresa



            ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)
            cmbChofer.Focus()
            Salida = False
        Else
            Salida = False
        End If

    End Sub




#End Region
#Region "CODIGO NUEVO"
    Private Sub txtidVCExtra_TextChanged(sender As Object, e As EventArgs) Handles txtidVCExtra.TextChanged

    End Sub

    Private Sub txtidVCExtra_KeyDown(sender As Object, e As KeyEventArgs) Handles txtidVCExtra.KeyDown
        If e.KeyCode = Keys.F3 Then
            cmdBuscaClave_Click(sender, e)
        End If
    End Sub

    Private Sub txtidVCExtra_KeyUp(sender As Object, e As KeyEventArgs) Handles txtidVCExtra.KeyUp
        If txtidVCExtra.Text.Length = 0 Then
            txtidVCExtra.Focus()
            txtidVCExtra.SelectAll()
        Else
            If e.KeyValue = Keys.Enter Then
                Salida = True
                CargaForm(txtidVCExtra.Text, "cargaCombustibleExtra")

                '
                Salida = False
            Else
                Salida = False
            End If
        End If
    End Sub

    Private Sub txtidVCExtra_Leave(sender As Object, e As EventArgs) Handles txtidVCExtra.Leave
        If Salida Then Exit Sub
        If txtidVCExtra.Text.Length = 0 Then
            txtidVCExtra.Focus()
            txtidVCExtra.SelectAll()
        Else
            If txtidVCExtra.Enabled Then
                CargaForm(txtidVCExtra.Text, "cargaCombustibleExtra")

                'ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)

            End If
        End If
    End Sub

    Private Sub txtidVCExtra_MouseDown(sender As Object, e As MouseEventArgs) Handles txtidVCExtra.MouseDown
        txtidVCExtra.Focus()
        txtidVCExtra.SelectAll()

    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        dtpFechaComb.Value = Now
        dtpHoraComb.Value = Now
    End Sub

    Private Sub cmbDespachador_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbDespachador.SelectedIndexChanged

    End Sub

    Private Sub cmbDespachador_KeyUp(sender As Object, e As KeyEventArgs) Handles cmbDespachador.KeyUp
        If e.KeyValue = Keys.Enter Then
            Salida = True
            btnMnuOk_Click(sender, e)
            'cmbAutori
            Salida = False
        End If
    End Sub

    Private Sub txtltCombustible_TextChanged(sender As Object, e As EventArgs) Handles txtltCombustible.TextChanged

    End Sub

    Private Sub txtltCombustible_KeyUp(sender As Object, e As KeyEventArgs) Handles txtltCombustible.KeyUp
        If e.KeyValue = Keys.Enter Then
            Salida = True
            cmbDespachador.Focus()
            Salida = False
        End If
    End Sub


#End Region'"CODIGO NUEVO"
End Class
