﻿Public Class fPreSalidaPen
    'VARIABLES RECIBE
    Private _con As String
    Private _sqlConCom As String

    Private _Usuario As String
    Private _IdUsuario As Integer
    Private _NombreUsuario As String


    'VARIABLES GENERALES
    Dim strsql As String
    Dim PreSal As PreSalidaClass
    Dim tdCab As New DataTable
    Dim tdDet As New DataTable
    Dim IdPreSalida As Integer
    Dim _conConfigl As String

    Dim tdCabPreOC As New DataTable
    Dim IdPreOC As Integer
    Dim tdDetPreOC As New DataTable

    Public Sub New(con As String, nomUsuario As String, IdUsuario As Integer,
    ByVal vNombreUsuario As String, sqlConComercial As String, conConfigl As String)
        _con = con
        _sqlConCom = sqlConComercial
        BD = New CapaDatos.UtilSQL(con, "Juan")
        BDCOM = New CapaDatos.UtilSQL(_sqlConCom, "Juan")

        _Usuario = nomUsuario
        _IdUsuario = IdUsuario
        Inicio.CONSTR = _con
        'Inicio.CONSTR_COM = _sqlConCom

        _NombreUsuario = vNombreUsuario
        _conConfigl = conConfigl


        InitializeComponent()

    End Sub
    Private Sub fPreSalidaPen_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        formatosTodosGrids()
        CargaCabecero()
        CargaCabPreOC()
    End Sub

    Private Sub formatosTodosGrids()
        FormatoGrids(grdCabecero)
        FormatoGrids(grdDetalle)
        FormatoGrids(grdCabPreOC)
        FormatoGrids(grdDetPreOC)
    End Sub

    Private Sub CargaCabecero()
        PreSal = New PreSalidaClass(0)
        tdCab = PreSal.tbPreSalidas(" cab.estatus = 'PEN' ", "cab.IdPreSalida DESC")
        If tdCab.Rows.Count > 0 Then
            grdCabecero.DataSource = tdCab
            FormatoGridCabecero()

            IdPreSalida = grdCabecero.Item("IdPreSalida", 0).Value
            CargaDetalle(IdPreSalida)


        End If

    End Sub
    Private Sub CargaCabPreOC()
        PreSal = New PreSalidaClass(0)
        tdCabPreOC = PreSal.tbPreSalidasPREOC(" ISNULL(c.idOC,0) > 0 AND c.Estatus = 'COM' ", " c.IdPreOC DESC")
        If tdCabPreOC.Rows.Count > 0 Then
            grdCabPreOC.DataSource = Nothing
            grdCabPreOC.DataSource = tdCabPreOC
            FormatoGridCabeceroPreOC()
            IdPreOC = grdCabPreOC.Item("IdPreOC", 0).Value
            CargaDetallePreOC(IdPreOC)


        End If

    End Sub

    Private Sub FormatoGrids(ByVal grid As DataGridView)
        grid.RowsDefaultCellStyle.BackColor = Color.AliceBlue
        grid.AlternatingRowsDefaultCellStyle.BackColor = Color.LightGreen
        grid.DefaultCellStyle.Font = New Font("Berlin Sans FB", 12)
        grid.ColumnHeadersDefaultCellStyle.Font = New Font("Berlin Sans FB", 12)
        grid.ColumnHeadersDefaultCellStyle.BackColor = Color.Yellow
        grid.ColumnHeadersDefaultCellStyle.ForeColor = Color.Red
        grid.EnableHeadersVisualStyles = False
        grid.SelectionMode = DataGridViewSelectionMode.FullRowSelect

        'grdDetalle.RowsDefaultCellStyle.BackColor = Color.Bisque
        'grdDetalle.AlternatingRowsDefaultCellStyle.BackColor = Color.Beige
        'grdDetalle.DefaultCellStyle.Font = New Font("Berlin Sans FB", 12)
        'grdDetalle.ColumnHeadersDefaultCellStyle.Font = New Font("Berlin Sans FB", 12)
        'grdDetalle.ColumnHeadersDefaultCellStyle.BackColor = Color.Yellow
        'grdDetalle.ColumnHeadersDefaultCellStyle.ForeColor = Color.Red
        'grdDetalle.EnableHeadersVisualStyles = False
        'grdDetalle.SelectionMode = DataGridViewSelectionMode.FullRowSelect
    End Sub
    Private Sub FormatoGridCabecero()
        grdCabecero.Columns("IdEmpleadoEnc").Visible = False
        grdCabecero.Columns("UserSolicitud").Visible = False


        grdCabecero.Columns("IdPreSalida").HeaderText = "Folio"
        grdCabecero.Columns("idOrdenTrabajo").HeaderText = "O. Trabajo"
        grdCabecero.Columns("FecSolicitud").HeaderText = "Fecha"
        grdCabecero.Columns("NombreCompleto").HeaderText = "Encargado"
        grdCabecero.Columns("NotaRecepcion").HeaderText = "Actividad"
        grdCabecero.Columns("NomSolicita").HeaderText = "Solicita"


        grdCabecero.Columns("IdPreSalida").AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader
        grdCabecero.Columns("idOrdenTrabajo").AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader

        grdCabecero.Columns("FecSolicitud").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        grdCabecero.Columns("NombreCompleto").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        grdCabecero.Columns("NotaRecepcion").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        grdCabecero.Columns("NomSolicita").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill


        'grdCabecero.Columns["ID"].Visible = False;
        '    
        '    grdCabecero.Columns["CIDPRODUCTO"].Visible = False;
        '    grdCabecero.Columns["CIDUNIDA01"].Visible = False;
        '    grdCabecero.Columns["Costo"].Visible = False;
        '    grdCabecero.Columns["PorcIVA"].Visible = False;
        '    //gridVenta.Columns["ImpDescto"].Visible = false;
        '    grdCabecero.Columns["Cancelado"].Visible = False;
        '    grdCabecero.Columns["ConsecCancela"].Visible = False;





        '//gridVenta.Columns["PorcIVA"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;
        '//gridVenta.Columns["PorcIVA"].DefaultCellStyle.Format = "P2";

        'gridVenta.Columns["Precio"].DefaultCellStyle.Format = "C2";
        'gridVenta.Columns["Precio"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight;
        'gridVenta.Columns["Total"].DefaultCellStyle.Format = "C2";
        'gridVenta.Columns["Total"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight;
        'gridVenta.Columns["ImpDescto"].DefaultCellStyle.Format = "C2";
        'gridVenta.Columns["ImpDescto"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight;

        'gridVenta.Columns["PorcDescto"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;
        '//gridVenta.Columns["PorcDescto"].DefaultCellStyle.Format = "P";


        'gridVenta.Columns["Cantidad"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;


        'gridVenta.Columns["Consecutivo"].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
        'gridVenta.Columns["CNOMBREPRODUCTO"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        'gridVenta.Columns["CNOMBREUNIDAD"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
        'gridVenta.Columns["Precio"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        'gridVenta.Columns["ImpDescto"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        'gridVenta.Columns["Total"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;


    End Sub

    Private Sub FormatoGridCabeceroPreOC()
        grdCabPreOC.Columns("idPersonalResp").Visible = False
        'grdCabPreOC.Columns("UserSolicitud").Visible = False

        grdCabPreOC.Columns("IdPreOC").HeaderText = "Folio"
        grdCabPreOC.Columns("idOrdenTrabajo").HeaderText = "O. Trabajo"
        grdCabPreOC.Columns("idOrdenSer").HeaderText = "O. Servicio"
        grdCabPreOC.Columns("FechaPreOc").HeaderText = "Fecha"
        grdCabPreOC.Columns("NomPersonalResp").HeaderText = "Encargado"
        grdCabPreOC.Columns("NotaRecepcion").HeaderText = "Actividad"
        grdCabPreOC.Columns("NomSolicita").HeaderText = "Solicita"


        grdCabPreOC.Columns("IdPreOC").AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        grdCabPreOC.Columns("idOrdenTrabajo").AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        grdCabPreOC.Columns("idOrdenSer").AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells

        grdCabPreOC.Columns("FechaPreOc").AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        grdCabPreOC.Columns("NomPersonalResp").AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        grdCabPreOC.Columns("NotaRecepcion").AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        grdCabPreOC.Columns("NomSolicita").AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells

    End Sub

    Private Sub btnMnuOk_Click(sender As Object, e As EventArgs) Handles btnMnuOk.Click
        formatosTodosGrids()
        CargaCabecero()
    End Sub


    Private Sub grdCabecero_Click(sender As Object, e As EventArgs) Handles grdCabecero.Click
        'Salida = True
        IdPreSalida = grdCabecero.Item("IdPreSalida", grdCabecero.CurrentCell.RowIndex).Value
        CargaDetalle(IdPreSalida)

        'FiltraDetallexClave(Clave)
        'Salida = False
    End Sub

    Private Sub CargaDetalle(ByVal IdPreSalida As Integer)
        PreSal = New PreSalidaClass(0)
        tdDet = PreSal.tbDetPreSalidaxId(IdPreSalida)
        grdDetalle.DataSource = Nothing
        If tdDet.Rows.Count > 0 Then
            grdDetalle.DataSource = tdDet
            FormatoGridDetalle()
        End If
    End Sub

    Private Sub CargaDetallePreOC(ByVal IdPreOC As Integer)
        PreSal = New PreSalidaClass(0)
        tdDetPreOC = PreSal.tbDetPreSalidaPreOCxId(IdPreOC)
        grdDetPreOC.DataSource = Nothing
        If tdDetPreOC.Rows.Count > 0 Then
            grdDetPreOC.DataSource = tdDetPreOC
            FormatoGridDetallePreOC()
        End If
    End Sub

    Private Sub FormatoGridDetalle()
        grdDetalle.Columns("IdPreSalida").Visible = False
        grdDetalle.Columns("Entregar").Visible = False
        grdDetalle.Columns("CantidadEnt").Visible = False
        grdDetalle.Columns("Costo").Visible = False
        grdDetalle.Columns("idUnidadProd").Visible = False



        grdDetalle.Columns("Cantidad").HeaderText = "Cantidad"
        grdDetalle.Columns("idProducto").HeaderText = "Producto"
        grdDetalle.Columns("CNOMBREPRODUCTO").HeaderText = "Descripción"
        'grdDetalle.Columns("NombreCompleto").HeaderText = "Encargado"
        'grdDetalle.Columns("NotaRecepcion").HeaderText = "Actividad"

        'grdDetalle.Columns("IdPreSalida").AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader
        'grdDetalle.Columns("idOrdenTrabajo").AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader

        grdDetalle.Columns("Cantidad").AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader
        grdDetalle.Columns("idProducto").AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader
        grdDetalle.Columns("CNOMBREPRODUCTO").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill


        'grdCabecero.Columns["ID"].Visible = False;
        '    
        '    grdCabecero.Columns["CIDPRODUCTO"].Visible = False;
        '    grdCabecero.Columns["CIDUNIDA01"].Visible = False;
        '    grdCabecero.Columns["Costo"].Visible = False;
        '    grdCabecero.Columns["PorcIVA"].Visible = False;
        '    //gridVenta.Columns["ImpDescto"].Visible = false;
        '    grdCabecero.Columns["Cancelado"].Visible = False;
        '    grdCabecero.Columns["ConsecCancela"].Visible = False;





        '//gridVenta.Columns["PorcIVA"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;
        '//gridVenta.Columns["PorcIVA"].DefaultCellStyle.Format = "P2";

        'gridVenta.Columns["Precio"].DefaultCellStyle.Format = "C2";
        'gridVenta.Columns["Precio"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight;
        'gridVenta.Columns["Total"].DefaultCellStyle.Format = "C2";
        'gridVenta.Columns["Total"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight;
        'gridVenta.Columns["ImpDescto"].DefaultCellStyle.Format = "C2";
        'gridVenta.Columns["ImpDescto"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight;

        'gridVenta.Columns["PorcDescto"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;
        '//gridVenta.Columns["PorcDescto"].DefaultCellStyle.Format = "P";


        'gridVenta.Columns["Cantidad"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;


        'gridVenta.Columns["Consecutivo"].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
        'gridVenta.Columns["CNOMBREPRODUCTO"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        'gridVenta.Columns["CNOMBREUNIDAD"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
        'gridVenta.Columns["Precio"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        'gridVenta.Columns["ImpDescto"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        'gridVenta.Columns["Total"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;


    End Sub

    Private Sub grdCabecero_DoubleClick(sender As Object, e As EventArgs) Handles grdCabecero.DoubleClick
        IdPreSalida = grdCabecero.Item("IdPreSalida", grdCabecero.CurrentCell.RowIndex).Value
        Dim frm As New fEntregaInsumosOS(_con, _Usuario, _IdUsuario, IdPreSalida, _NombreUsuario,
        _conConfigl, _sqlConCom, True)
        frm.StartPosition = FormStartPosition.CenterParent
        frm.ShowDialog()
        CargaCabecero()
    End Sub

    Private Sub btnMnuSalir_Click(sender As Object, e As EventArgs) Handles btnMnuSalir.Click
        Close()

    End Sub

    Private Sub FormatoGridDetallePreOC()
        grdDetPreOC.Columns("IdPreOC").Visible = False
        'grdDetalle.Columns("Entregar").Visible = False
        'grdDetalle.Columns("CantidadEnt").Visible = False


        grdDetPreOC.Columns("Cantidad").HeaderText = "Cantidad"
        grdDetPreOC.Columns("idProducto").HeaderText = "Producto"
        grdDetPreOC.Columns("CCODIGOPRODUCTO").HeaderText = "Codigo"
        grdDetPreOC.Columns("CNOMBREPRODUCTO").HeaderText = "Descripción"

        grdDetPreOC.Columns("Cantidad").AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        grdDetPreOC.Columns("idProducto").AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        grdDetPreOC.Columns("CCODIGOPRODUCTO").AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        grdDetPreOC.Columns("CNOMBREPRODUCTO").AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells


        'grdCabecero.Columns["ID"].Visible = False;
        '    
        '    grdCabecero.Columns["CIDPRODUCTO"].Visible = False;
        '    grdCabecero.Columns["CIDUNIDA01"].Visible = False;
        '    grdCabecero.Columns["Costo"].Visible = False;
        '    grdCabecero.Columns["PorcIVA"].Visible = False;
        '    //gridVenta.Columns["ImpDescto"].Visible = false;
        '    grdCabecero.Columns["Cancelado"].Visible = False;
        '    grdCabecero.Columns["ConsecCancela"].Visible = False;





        '//gridVenta.Columns["PorcIVA"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;
        '//gridVenta.Columns["PorcIVA"].DefaultCellStyle.Format = "P2";

        'gridVenta.Columns["Precio"].DefaultCellStyle.Format = "C2";
        'gridVenta.Columns["Precio"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight;
        'gridVenta.Columns["Total"].DefaultCellStyle.Format = "C2";
        'gridVenta.Columns["Total"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight;
        'gridVenta.Columns["ImpDescto"].DefaultCellStyle.Format = "C2";
        'gridVenta.Columns["ImpDescto"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight;

        'gridVenta.Columns["PorcDescto"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;
        '//gridVenta.Columns["PorcDescto"].DefaultCellStyle.Format = "P";


        'gridVenta.Columns["Cantidad"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;


        'gridVenta.Columns["Consecutivo"].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
        'gridVenta.Columns["CNOMBREPRODUCTO"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        'gridVenta.Columns["CNOMBREUNIDAD"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
        'gridVenta.Columns["Precio"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        'gridVenta.Columns["ImpDescto"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        'gridVenta.Columns["Total"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;


    End Sub

    Private Sub grdDetPreOC_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles grdDetPreOC.CellContentClick

    End Sub

    Private Sub grdCabecero_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles grdCabecero.CellContentClick

    End Sub

    Private Sub grdCabPreOC_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles grdCabPreOC.CellContentClick

    End Sub

    Private Sub grdCabPreOC_Click(sender As Object, e As EventArgs) Handles grdCabPreOC.Click
        IdPreOC = grdCabPreOC.Item("IdPreOC", grdCabPreOC.CurrentCell.RowIndex).Value
        CargaDetallePreOC(IdPreOC)
    End Sub

    Private Sub grdCabPreOC_DoubleClick(sender As Object, e As EventArgs) Handles grdCabPreOC.DoubleClick
        IdPreOC = grdCabPreOC.Item("IdPreOC", grdCabPreOC.CurrentCell.RowIndex).Value
        Dim frm As New fEntregaInsumosOS(_con, _Usuario, _IdUsuario, IdPreOC, _NombreUsuario,
        _conConfigl, _sqlConCom, False)
        frm.StartPosition = FormStartPosition.CenterParent
        frm.ShowDialog()
        CargaCabPreOC()
    End Sub

    Private Sub grdCabecero_ColumnHeaderMouseDoubleClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles grdCabecero.ColumnHeaderMouseDoubleClick

    End Sub

    Private Sub grdCabecero_ColumnHeadersHeightChanged(sender As Object, e As EventArgs) Handles grdCabecero.ColumnHeadersHeightChanged

    End Sub
End Class