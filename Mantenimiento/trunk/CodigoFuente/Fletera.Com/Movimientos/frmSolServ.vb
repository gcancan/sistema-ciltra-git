'Fecha: 15 / Febrero / 2016
'ROGER GALA PACHECO
'************************************************************************************************************************************
'*  Fecha Modificacion :   15 / Febrero / 2016                                                                                       
'*  
'************************************************************************************************************************************
Imports System.Configuration
Imports Core.Models
Imports System.Drawing.Printing
Imports System.Drawing.Text



Public Class frmSolServ

    Inherits System.Windows.Forms.Form
    Private _Tag As String
    Dim bEsIdentidad As Boolean = True
    Dim OpcionForma As TipoOpcionForma
    Dim TablaBd As String = "Diagnostico"


    '*******************************************VALIDOS
    Dim MyTablaTipoOrdServ As DataTable = New DataTable(TablaBd)
    Dim TipOrdServ As TipoServClass
    Dim Servicio As ServicioClass
    Dim vNumOrdServ As Integer = 0
    Dim vOrdenTrabajo As Boolean = False
    Dim vTipoOrdServ As Integer = 0
    Dim vidUnidadTrans As String = ""
    Dim vNomTipOrdServ As String = ""
    Dim vTipoServ As Integer = 0

    Dim UniTrans As UnidadesTranspClass
    Dim UniTransOp As UnidadesTranspClass
    Dim TipoUniTrans As TipoUniTransClass
    Dim Clave As Integer
    Dim MyTablaDetalle As DataTable = New DataTable(TablaBd)
    Dim vidPadreOrdSer As Integer = 0
    Dim ArrayOrdenSer() As Integer
    Dim ArrayOrdenSerTipo() As Boolean
    Dim indiceOS As Integer = 0
    Dim ArrayOrdenTrab() As Integer
    Dim indiceOT As Integer = 0
    Dim ArraySql() As String
    Dim indice As Integer = 0

    Dim ImpRep As New ImprimeRemision
    Private prtSettings As PrinterSettings
    Private prtDoc As PrintDocument

    Dim MyPaperSize As New Printing.PaperSize("MediaCarta", 827, 584.5)
    'Dim MyPaperSize As New Printing.PaperSize("MediaCarta", 827, 584.5)
    'Dim MyPaperSize As New Printing.PaperSize()

    Dim printFont As New System.Drawing.Font("Courier New", 7)
    Dim NumRegistros As Integer
    Private lineaActual As Integer = 0
    Dim ContPaginas As Integer = 0
    Dim MyTablaImprime As DataTable = New DataTable("IMPRIME")
    Dim DsRep As DataSet
    Dim PATH_FONTS As String = ""
    Dim Empresa As EmpresaClass

    Dim PadreOS As MasterOrdServClass
    Dim vUltKm As Integer
    Dim MaximoKmdeMas As Integer = 10000

    Dim vValorNomAct As String
    Dim vValorNomDivision As String
    Dim vValorDivision As Integer

    'Dim tablaTipOS As DataTable
    '*******************************************VALIDOS


    Private OrdenTrabajo As OrdenTrabajoClass
    Private ULTOT As OrdenTrabajoClass
    Private Personal As PersonalClass
    Private PersonalPuesto As PuestosClass
    Private FamiliaOT As FamiliasClass

    Private OrdenServ As OrdenServClass
    Private TipOrd As TipoOrdenClass
    Private User As UsuarioClass
    Private Emp As EmpresaClass
    Private DetOS As detOrdenServicioClass
    Private Marca As MarcaClass

    Dim MyTablaHisOS As DataTable = New DataTable(TablaBd)
    Dim MyTablaHisOT As DataTable = New DataTable(TablaBd)
    Dim MyTablaRefac As DataTable = New DataTable(TablaBd)

    Dim MyTablaComboResp As DataTable = New DataTable(TablaBd)
    Dim MyTablaComboAyu1 As DataTable = New DataTable(TablaBd)
    Dim MyTablaComboAyu2 As DataTable = New DataTable(TablaBd)

    Dim MyTablaPersOcupado As DataTable = New DataTable(TablaBd)

    Private ProdCom As ProductosComClass
    Private UnidadCom As UnidadesComClass

    Private ObjSql As New ToolSQLs

    Dim Salida As Boolean
    Dim myDataColumn As DataColumn
    Dim myDataRow As DataRow

    Dim CantidadGrid As Double = 0
    Dim BandNoExisteProdEnGrid As Boolean = False

    Dim ObjCom As New FuncionesComClass

    Dim vCIDPRODUCTO As Integer
    Dim vCIDUNIDAD As Integer

    Dim vIdAlmacen As Integer = 1
    Dim IdOrdenServ As Integer

    Dim CadCam As String = ""
    Dim StrSql As String = ""


    Dim NoIdPreOC As Integer = 0
    Dim NoIdPreSal As Integer = 0

    Dim objNumSig As New CapaNegocio.Tablas

    Dim ContPersonal As Integer = 0
    Dim CostoManoObra As Double = 0
    Dim CostoRef As Double = 0

    Dim vLugOcupado As Integer = 0

    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents txtidPadreOrdSer As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents dtpFechaCapturaHora As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpFechaCap As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtRemolque2 As System.Windows.Forms.TextBox
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents txtDolly As System.Windows.Forms.TextBox
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents txtRemolque1 As System.Windows.Forms.TextBox
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents txtTractor As System.Windows.Forms.TextBox
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents chkBoxTipoOrdServ As System.Windows.Forms.CheckedListBox
    '
    'Dim CampoLLave As String = "idHerramienta"
    'Dim TablaBd2 As String = "CatFamilia"
    'Dim TablaBd3 As String = "CatTipoHerramienta"
    'Dim TablaBd4 As String = "CatMarcas"

    'Private Herramienta As HerramientaClass
    'Private Familia As FamiliasClass
    'Private TipoHerramienta As TipoHerramClass
    'Private Marca As MarcaClass

    Private _NoServ As Integer
    Private _con As String
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents cmbChofer As System.Windows.Forms.ComboBox
    Friend WithEvents GrupoCaptura As System.Windows.Forms.GroupBox
    Friend WithEvents cmbAutoriza As System.Windows.Forms.ComboBox
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Private _Usuario As String
    Private _IdUsuario As Integer
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents btnAgregaOS As System.Windows.Forms.Button
    Friend WithEvents grdOrdenServDet As System.Windows.Forms.DataGridView
    Friend WithEvents grdOrdenServCab As System.Windows.Forms.DataGridView
    Friend WithEvents btnMenuReImprimir As System.Windows.Forms.ToolStripButton
    Friend WithEvents ConMnuOrdSer As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents ModificarToolStripMenuOrdSer As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CancelarToolStripMenuOrdSer As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ImprimirToolStripMenuOrdSer As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnMnuCancelar As System.Windows.Forms.ToolStripButton
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtEstatus As System.Windows.Forms.TextBox
    Friend WithEvents txtMotivoCancela As System.Windows.Forms.TextBox
    Private _cveEmpresa As Integer

    Dim idLlanta As String = ""
    Friend WithEvents osdCLAVE As DataGridViewTextBoxColumn
    Friend WithEvents osdIdOrdenSer As DataGridViewTextBoxColumn
    Friend WithEvents osdidServicio As DataGridViewTextBoxColumn
    Friend WithEvents osdNomServicio As DataGridViewTextBoxColumn
    Friend WithEvents osdidActividad As DataGridViewTextBoxColumn
    Friend WithEvents osdNombreAct As DataGridViewTextBoxColumn
    Friend WithEvents osdidOrdenTrabajo As DataGridViewTextBoxColumn
    Friend WithEvents osdidDivision As DataGridViewTextBoxColumn
    Friend WithEvents osdNomDivision As DataGridViewTextBoxColumn
    Friend WithEvents osdDuracionActHr As DataGridViewTextBoxColumn
    Friend WithEvents osdModificado As DataGridViewCheckBoxColumn
    Friend WithEvents osdidOrdenActividad As DataGridViewTextBoxColumn
    Friend WithEvents osdidLlanta As DataGridViewTextBoxColumn
    Friend WithEvents osdPosicion As DataGridViewTextBoxColumn
    Dim PosLLanta As Integer = 0
    Dim BandResultado As Boolean = False
    Dim UltElemento As String = ""
    Dim usuario As UsuarioClass
    Friend WithEvents cmbIdEmpresa As ComboBox
    Friend WithEvents oscCLAVE As DataGridViewTextBoxColumn
    Friend WithEvents oscConsecutivo As DataGridViewTextBoxColumn
    Friend WithEvents oscIdOrdenSer As DataGridViewTextBoxColumn
    Friend WithEvents oscfechaCaptura As DataGridViewTextBoxColumn
    Friend WithEvents oscTipoOrdenServ As DataGridViewTextBoxColumn
    Friend WithEvents oscNomTipOrdServ As DataGridViewTextBoxColumn
    Friend WithEvents oscidUnidadTrans As DataGridViewTextBoxColumn
    Friend WithEvents oscidTipoUnidadTrans As DataGridViewTextBoxColumn
    Friend WithEvents oscnomTipoUniTras As DataGridViewTextBoxColumn
    Friend WithEvents oscKm As DataGridViewTextBoxColumn
    Friend WithEvents oscAutorizo As DataGridViewTextBoxColumn
    Friend WithEvents oscbOrdenTrabajo As DataGridViewCheckBoxColumn
    Friend WithEvents oscTipoServ As DataGridViewTextBoxColumn
    Friend WithEvents oscModificado As DataGridViewCheckBoxColumn
    Friend WithEvents oscCancelado As DataGridViewCheckBoxColumn
    Private _MultiEmpresa As Boolean

    Dim DtFaltantes As New DataTable


#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents btnMnuOk As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuLimpiar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuSalir As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripMenu As System.Windows.Forms.ToolStrip
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSolServ))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.ToolStripMenu = New System.Windows.Forms.ToolStrip()
        Me.btnMnuOk = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuCancelar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuLimpiar = New System.Windows.Forms.ToolStripButton()
        Me.btnMenuReImprimir = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuSalir = New System.Windows.Forms.ToolStripButton()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.cmbIdEmpresa = New System.Windows.Forms.ComboBox()
        Me.txtMotivoCancela = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtEstatus = New System.Windows.Forms.TextBox()
        Me.btnAgregaOS = New System.Windows.Forms.Button()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.cmbAutoriza = New System.Windows.Forms.ComboBox()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.cmbChofer = New System.Windows.Forms.ComboBox()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.chkBoxTipoOrdServ = New System.Windows.Forms.CheckedListBox()
        Me.dtpFechaCapturaHora = New System.Windows.Forms.DateTimePicker()
        Me.dtpFechaCap = New System.Windows.Forms.DateTimePicker()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtRemolque2 = New System.Windows.Forms.TextBox()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.txtDolly = New System.Windows.Forms.TextBox()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.txtRemolque1 = New System.Windows.Forms.TextBox()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.txtTractor = New System.Windows.Forms.TextBox()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.txtidPadreOrdSer = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.GrupoCaptura = New System.Windows.Forms.GroupBox()
        Me.grdOrdenServDet = New System.Windows.Forms.DataGridView()
        Me.osdCLAVE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.osdIdOrdenSer = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.osdidServicio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.osdNomServicio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.osdidActividad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.osdNombreAct = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.osdidOrdenTrabajo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.osdidDivision = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.osdNomDivision = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.osdDuracionActHr = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.osdModificado = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.osdidOrdenActividad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.osdidLlanta = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.osdPosicion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.grdOrdenServCab = New System.Windows.Forms.DataGridView()
        Me.oscCLAVE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.oscConsecutivo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.oscIdOrdenSer = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.oscfechaCaptura = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.oscTipoOrdenServ = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.oscNomTipOrdServ = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.oscidUnidadTrans = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.oscidTipoUnidadTrans = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.oscnomTipoUniTras = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.oscKm = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.oscAutorizo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.oscbOrdenTrabajo = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.oscTipoServ = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.oscModificado = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.oscCancelado = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.ConMnuOrdSer = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ModificarToolStripMenuOrdSer = New System.Windows.Forms.ToolStripMenuItem()
        Me.CancelarToolStripMenuOrdSer = New System.Windows.Forms.ToolStripMenuItem()
        Me.ImprimirToolStripMenuOrdSer = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenu.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GrupoCaptura.SuspendLayout()
        CType(Me.grdOrdenServDet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdOrdenServCab, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ConMnuOrdSer.SuspendLayout()
        Me.SuspendLayout()
        '
        'ToolStripMenu
        '
        Me.ToolStripMenu.AutoSize = False
        Me.ToolStripMenu.BackColor = System.Drawing.Color.Silver
        Me.ToolStripMenu.Dock = System.Windows.Forms.DockStyle.None
        Me.ToolStripMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnMnuOk, Me.btnMnuCancelar, Me.btnMnuLimpiar, Me.btnMenuReImprimir, Me.btnMnuSalir})
        Me.ToolStripMenu.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow
        Me.ToolStripMenu.Location = New System.Drawing.Point(3, 0)
        Me.ToolStripMenu.Name = "ToolStripMenu"
        Me.ToolStripMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.ToolStripMenu.Size = New System.Drawing.Size(891, 42)
        Me.ToolStripMenu.TabIndex = 1
        Me.ToolStripMenu.Text = "ToolStrip1"
        '
        'btnMnuOk
        '
        Me.btnMnuOk.ForeColor = System.Drawing.Color.Red
        Me.btnMnuOk.Image = Global.Fletera.My.Resources.Resources.Save1
        Me.btnMnuOk.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuOk.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuOk.Name = "btnMnuOk"
        Me.btnMnuOk.Size = New System.Drawing.Size(56, 39)
        Me.btnMnuOk.Text = "&GRABAR"
        Me.btnMnuOk.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuCancelar
        '
        Me.btnMnuCancelar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuCancelar.Image = Global.Fletera.My.Resources.Resources.deleteCancelar
        Me.btnMnuCancelar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuCancelar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuCancelar.Name = "btnMnuCancelar"
        Me.btnMnuCancelar.Size = New System.Drawing.Size(71, 39)
        Me.btnMnuCancelar.Text = "&CANCELAR"
        Me.btnMnuCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuLimpiar
        '
        Me.btnMnuLimpiar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuLimpiar.Image = Global.Fletera.My.Resources.Resources.signos_f_059
        Me.btnMnuLimpiar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuLimpiar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuLimpiar.Name = "btnMnuLimpiar"
        Me.btnMnuLimpiar.Size = New System.Drawing.Size(56, 39)
        Me.btnMnuLimpiar.Text = "&LIMPIAR"
        Me.btnMnuLimpiar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMenuReImprimir
        '
        Me.btnMenuReImprimir.ForeColor = System.Drawing.Color.Red
        Me.btnMenuReImprimir.Image = Global.Fletera.My.Resources.Resources.impresora
        Me.btnMenuReImprimir.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMenuReImprimir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMenuReImprimir.Name = "btnMenuReImprimir"
        Me.btnMenuReImprimir.Size = New System.Drawing.Size(63, 39)
        Me.btnMenuReImprimir.Text = "&IMPRIMIR"
        Me.btnMenuReImprimir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuSalir
        '
        Me.btnMnuSalir.ForeColor = System.Drawing.Color.Red
        Me.btnMnuSalir.Image = Global.Fletera.My.Resources.Resources.Salida2
        Me.btnMnuSalir.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuSalir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuSalir.Name = "btnMnuSalir"
        Me.btnMnuSalir.Size = New System.Drawing.Size(41, 39)
        Me.btnMnuSalir.Text = "&SALIR"
        Me.btnMnuSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.cmbIdEmpresa)
        Me.GroupBox4.Controls.Add(Me.txtMotivoCancela)
        Me.GroupBox4.Controls.Add(Me.Label1)
        Me.GroupBox4.Controls.Add(Me.txtEstatus)
        Me.GroupBox4.Controls.Add(Me.btnAgregaOS)
        Me.GroupBox4.Controls.Add(Me.Label9)
        Me.GroupBox4.Controls.Add(Me.cmbAutoriza)
        Me.GroupBox4.Controls.Add(Me.Label38)
        Me.GroupBox4.Controls.Add(Me.cmbChofer)
        Me.GroupBox4.Controls.Add(Me.Label37)
        Me.GroupBox4.Controls.Add(Me.Button4)
        Me.GroupBox4.Controls.Add(Me.Button3)
        Me.GroupBox4.Controls.Add(Me.Button2)
        Me.GroupBox4.Controls.Add(Me.Button1)
        Me.GroupBox4.Controls.Add(Me.chkBoxTipoOrdServ)
        Me.GroupBox4.Controls.Add(Me.dtpFechaCapturaHora)
        Me.GroupBox4.Controls.Add(Me.dtpFechaCap)
        Me.GroupBox4.Controls.Add(Me.Label10)
        Me.GroupBox4.Controls.Add(Me.txtRemolque2)
        Me.GroupBox4.Controls.Add(Me.Label36)
        Me.GroupBox4.Controls.Add(Me.txtDolly)
        Me.GroupBox4.Controls.Add(Me.Label35)
        Me.GroupBox4.Controls.Add(Me.txtRemolque1)
        Me.GroupBox4.Controls.Add(Me.Label34)
        Me.GroupBox4.Controls.Add(Me.txtTractor)
        Me.GroupBox4.Controls.Add(Me.Label33)
        Me.GroupBox4.Controls.Add(Me.txtidPadreOrdSer)
        Me.GroupBox4.Controls.Add(Me.Label13)
        Me.GroupBox4.Controls.Add(Me.Label32)
        Me.GroupBox4.Location = New System.Drawing.Point(3, 45)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(854, 171)
        Me.GroupBox4.TabIndex = 75
        Me.GroupBox4.TabStop = False
        '
        'cmbIdEmpresa
        '
        Me.cmbIdEmpresa.FormattingEnabled = True
        Me.cmbIdEmpresa.Location = New System.Drawing.Point(232, 12)
        Me.cmbIdEmpresa.Name = "cmbIdEmpresa"
        Me.cmbIdEmpresa.Size = New System.Drawing.Size(210, 21)
        Me.cmbIdEmpresa.TabIndex = 105
        '
        'txtMotivoCancela
        '
        Me.txtMotivoCancela.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMotivoCancela.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMotivoCancela.Location = New System.Drawing.Point(456, 100)
        Me.txtMotivoCancela.MaxLength = 250
        Me.txtMotivoCancela.Multiline = True
        Me.txtMotivoCancela.Name = "txtMotivoCancela"
        Me.txtMotivoCancela.ReadOnly = True
        Me.txtMotivoCancela.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtMotivoCancela.Size = New System.Drawing.Size(374, 25)
        Me.txtMotivoCancela.TabIndex = 104
        '
        'Label1
        '
        Me.Label1.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label1.Location = New System.Drawing.Point(676, 76)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(54, 20)
        Me.Label1.TabIndex = 103
        Me.Label1.Text = "Estatus:"
        '
        'txtEstatus
        '
        Me.txtEstatus.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtEstatus.Enabled = False
        Me.txtEstatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEstatus.Location = New System.Drawing.Point(736, 73)
        Me.txtEstatus.Name = "txtEstatus"
        Me.txtEstatus.ReadOnly = True
        Me.txtEstatus.Size = New System.Drawing.Size(94, 20)
        Me.txtEstatus.TabIndex = 102
        '
        'btnAgregaOS
        '
        Me.btnAgregaOS.Image = CType(resources.GetObject("btnAgregaOS.Image"), System.Drawing.Image)
        Me.btnAgregaOS.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnAgregaOS.Location = New System.Drawing.Point(393, 122)
        Me.btnAgregaOS.Name = "btnAgregaOS"
        Me.btnAgregaOS.Size = New System.Drawing.Size(39, 42)
        Me.btnAgregaOS.TabIndex = 14
        Me.btnAgregaOS.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'Label9
        '
        Me.Label9.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label9.Location = New System.Drawing.Point(172, 15)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(54, 20)
        Me.Label9.TabIndex = 100
        Me.Label9.Text = "Empresa:"
        '
        'cmbAutoriza
        '
        Me.cmbAutoriza.FormattingEnabled = True
        Me.cmbAutoriza.Location = New System.Drawing.Point(411, 73)
        Me.cmbAutoriza.Name = "cmbAutoriza"
        Me.cmbAutoriza.Size = New System.Drawing.Size(254, 21)
        Me.cmbAutoriza.TabIndex = 11
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label38.Location = New System.Drawing.Point(357, 76)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(48, 13)
        Me.Label38.TabIndex = 95
        Me.Label38.Text = "Autoriza:"
        '
        'cmbChofer
        '
        Me.cmbChofer.FormattingEnabled = True
        Me.cmbChofer.Location = New System.Drawing.Point(95, 73)
        Me.cmbChofer.Name = "cmbChofer"
        Me.cmbChofer.Size = New System.Drawing.Size(254, 21)
        Me.cmbChofer.TabIndex = 10
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label37.Location = New System.Drawing.Point(10, 76)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(41, 13)
        Me.Label37.TabIndex = 90
        Me.Label37.Text = "Chofer:"
        '
        'Button4
        '
        Me.Button4.Image = CType(resources.GetObject("Button4.Image"), System.Drawing.Image)
        Me.Button4.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button4.Location = New System.Drawing.Point(703, 35)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(32, 32)
        Me.Button4.TabIndex = 9
        Me.Button4.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.Button4.Visible = False
        '
        'Button3
        '
        Me.Button3.Image = CType(resources.GetObject("Button3.Image"), System.Drawing.Image)
        Me.Button3.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button3.Location = New System.Drawing.Point(518, 35)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(32, 32)
        Me.Button3.TabIndex = 7
        Me.Button3.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.Button3.Visible = False
        '
        'Button2
        '
        Me.Button2.Image = CType(resources.GetObject("Button2.Image"), System.Drawing.Image)
        Me.Button2.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button2.Location = New System.Drawing.Point(355, 35)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(32, 32)
        Me.Button2.TabIndex = 5
        Me.Button2.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.Button2.Visible = False
        '
        'Button1
        '
        Me.Button1.Image = CType(resources.GetObject("Button1.Image"), System.Drawing.Image)
        Me.Button1.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button1.Location = New System.Drawing.Point(170, 35)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(32, 32)
        Me.Button1.TabIndex = 3
        Me.Button1.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.Button1.Visible = False
        '
        'chkBoxTipoOrdServ
        '
        Me.chkBoxTipoOrdServ.FormattingEnabled = True
        Me.chkBoxTipoOrdServ.HorizontalScrollbar = True
        Me.chkBoxTipoOrdServ.Location = New System.Drawing.Point(95, 100)
        Me.chkBoxTipoOrdServ.MultiColumn = True
        Me.chkBoxTipoOrdServ.Name = "chkBoxTipoOrdServ"
        Me.chkBoxTipoOrdServ.Size = New System.Drawing.Size(292, 64)
        Me.chkBoxTipoOrdServ.TabIndex = 13
        '
        'dtpFechaCapturaHora
        '
        Me.dtpFechaCapturaHora.Enabled = False
        Me.dtpFechaCapturaHora.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtpFechaCapturaHora.Location = New System.Drawing.Point(765, 9)
        Me.dtpFechaCapturaHora.Name = "dtpFechaCapturaHora"
        Me.dtpFechaCapturaHora.Size = New System.Drawing.Size(83, 20)
        Me.dtpFechaCapturaHora.TabIndex = 88
        '
        'dtpFechaCap
        '
        Me.dtpFechaCap.Enabled = False
        Me.dtpFechaCap.Location = New System.Drawing.Point(534, 9)
        Me.dtpFechaCap.Name = "dtpFechaCap"
        Me.dtpFechaCap.Size = New System.Drawing.Size(225, 20)
        Me.dtpFechaCap.TabIndex = 87
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label10.Location = New System.Drawing.Point(488, 9)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(40, 13)
        Me.Label10.TabIndex = 89
        Me.Label10.Text = "Fecha:"
        '
        'txtRemolque2
        '
        Me.txtRemolque2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRemolque2.Location = New System.Drawing.Point(628, 47)
        Me.txtRemolque2.MaxLength = 10
        Me.txtRemolque2.Name = "txtRemolque2"
        Me.txtRemolque2.Size = New System.Drawing.Size(69, 20)
        Me.txtRemolque2.TabIndex = 8
        Me.txtRemolque2.Tag = "4"
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label36.Location = New System.Drawing.Point(558, 50)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(64, 13)
        Me.Label36.TabIndex = 85
        Me.Label36.Text = "Remolque2:"
        '
        'txtDolly
        '
        Me.txtDolly.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDolly.Location = New System.Drawing.Point(438, 47)
        Me.txtDolly.MaxLength = 10
        Me.txtDolly.Name = "txtDolly"
        Me.txtDolly.Size = New System.Drawing.Size(69, 20)
        Me.txtDolly.TabIndex = 6
        Me.txtDolly.Tag = "3"
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label35.Location = New System.Drawing.Point(399, 47)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(33, 13)
        Me.Label35.TabIndex = 83
        Me.Label35.Text = "Dolly:"
        '
        'txtRemolque1
        '
        Me.txtRemolque1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRemolque1.Location = New System.Drawing.Point(280, 47)
        Me.txtRemolque1.MaxLength = 10
        Me.txtRemolque1.Name = "txtRemolque1"
        Me.txtRemolque1.Size = New System.Drawing.Size(69, 20)
        Me.txtRemolque1.TabIndex = 4
        Me.txtRemolque1.Tag = "2"
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label34.Location = New System.Drawing.Point(213, 50)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(64, 13)
        Me.Label34.TabIndex = 81
        Me.Label34.Text = "Remolque1:"
        '
        'txtTractor
        '
        Me.txtTractor.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTractor.Location = New System.Drawing.Point(95, 47)
        Me.txtTractor.MaxLength = 10
        Me.txtTractor.Name = "txtTractor"
        Me.txtTractor.Size = New System.Drawing.Size(69, 20)
        Me.txtTractor.TabIndex = 2
        Me.txtTractor.Tag = "1"
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label33.Location = New System.Drawing.Point(10, 47)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(44, 13)
        Me.Label33.TabIndex = 79
        Me.Label33.Text = "Tractor:"
        '
        'txtidPadreOrdSer
        '
        Me.txtidPadreOrdSer.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtidPadreOrdSer.ForeColor = System.Drawing.Color.Red
        Me.txtidPadreOrdSer.Location = New System.Drawing.Point(97, 13)
        Me.txtidPadreOrdSer.Name = "txtidPadreOrdSer"
        Me.txtidPadreOrdSer.Size = New System.Drawing.Size(69, 20)
        Me.txtidPadreOrdSer.TabIndex = 1
        Me.txtidPadreOrdSer.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label13.Location = New System.Drawing.Point(11, 16)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(41, 13)
        Me.Label13.TabIndex = 78
        Me.Label13.Text = "FOLIO:"
        '
        'Label32
        '
        Me.Label32.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label32.Location = New System.Drawing.Point(4, 111)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(79, 41)
        Me.Label32.TabIndex = 75
        Me.Label32.Text = "Tipo de Orden Servicio"
        '
        'GrupoCaptura
        '
        Me.GrupoCaptura.Controls.Add(Me.grdOrdenServDet)
        Me.GrupoCaptura.Controls.Add(Me.grdOrdenServCab)
        Me.GrupoCaptura.Location = New System.Drawing.Point(8, 222)
        Me.GrupoCaptura.Name = "GrupoCaptura"
        Me.GrupoCaptura.Size = New System.Drawing.Size(845, 336)
        Me.GrupoCaptura.TabIndex = 3
        Me.GrupoCaptura.TabStop = False
        Me.GrupoCaptura.Text = "Ordenes de Servicio"
        '
        'grdOrdenServDet
        '
        Me.grdOrdenServDet.AllowUserToAddRows = False
        Me.grdOrdenServDet.AllowUserToResizeColumns = False
        Me.grdOrdenServDet.AllowUserToResizeRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Yellow
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black
        Me.grdOrdenServDet.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.grdOrdenServDet.BackgroundColor = System.Drawing.Color.White
        Me.grdOrdenServDet.ColumnHeadersHeight = 34
        Me.grdOrdenServDet.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.grdOrdenServDet.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.osdCLAVE, Me.osdIdOrdenSer, Me.osdidServicio, Me.osdNomServicio, Me.osdidActividad, Me.osdNombreAct, Me.osdidOrdenTrabajo, Me.osdidDivision, Me.osdNomDivision, Me.osdDuracionActHr, Me.osdModificado, Me.osdidOrdenActividad, Me.osdidLlanta, Me.osdPosicion})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Yellow
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.grdOrdenServDet.DefaultCellStyle = DataGridViewCellStyle2
        Me.grdOrdenServDet.Location = New System.Drawing.Point(9, 149)
        Me.grdOrdenServDet.Name = "grdOrdenServDet"
        Me.grdOrdenServDet.RowHeadersWidth = 26
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.Yellow
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black
        Me.grdOrdenServDet.RowsDefaultCellStyle = DataGridViewCellStyle3
        Me.grdOrdenServDet.Size = New System.Drawing.Size(816, 177)
        Me.grdOrdenServDet.TabIndex = 35
        '
        'osdCLAVE
        '
        Me.osdCLAVE.HeaderText = "osdCLAVE"
        Me.osdCLAVE.Name = "osdCLAVE"
        Me.osdCLAVE.Visible = False
        '
        'osdIdOrdenSer
        '
        Me.osdIdOrdenSer.HeaderText = "osdIdOrdenSer"
        Me.osdIdOrdenSer.Name = "osdIdOrdenSer"
        Me.osdIdOrdenSer.Visible = False
        '
        'osdidServicio
        '
        Me.osdidServicio.HeaderText = "osdidServicio"
        Me.osdidServicio.Name = "osdidServicio"
        Me.osdidServicio.Visible = False
        '
        'osdNomServicio
        '
        Me.osdNomServicio.HeaderText = "Servicio"
        Me.osdNomServicio.Name = "osdNomServicio"
        Me.osdNomServicio.Width = 200
        '
        'osdidActividad
        '
        Me.osdidActividad.HeaderText = "osdidActividad"
        Me.osdidActividad.Name = "osdidActividad"
        Me.osdidActividad.Visible = False
        '
        'osdNombreAct
        '
        Me.osdNombreAct.HeaderText = "Actividad"
        Me.osdNombreAct.Name = "osdNombreAct"
        Me.osdNombreAct.Width = 300
        '
        'osdidOrdenTrabajo
        '
        Me.osdidOrdenTrabajo.HeaderText = "O. Trabajo"
        Me.osdidOrdenTrabajo.Name = "osdidOrdenTrabajo"
        '
        'osdidDivision
        '
        Me.osdidDivision.HeaderText = "osdidDivision"
        Me.osdidDivision.Name = "osdidDivision"
        Me.osdidDivision.Visible = False
        '
        'osdNomDivision
        '
        Me.osdNomDivision.HeaderText = "Division"
        Me.osdNomDivision.Name = "osdNomDivision"
        Me.osdNomDivision.Width = 200
        '
        'osdDuracionActHr
        '
        Me.osdDuracionActHr.HeaderText = "osdDuracionActHr"
        Me.osdDuracionActHr.Name = "osdDuracionActHr"
        Me.osdDuracionActHr.Visible = False
        '
        'osdModificado
        '
        Me.osdModificado.HeaderText = "osdModificado"
        Me.osdModificado.Name = "osdModificado"
        Me.osdModificado.Visible = False
        '
        'osdidOrdenActividad
        '
        Me.osdidOrdenActividad.HeaderText = "osdidOrdenActividad"
        Me.osdidOrdenActividad.Name = "osdidOrdenActividad"
        Me.osdidOrdenActividad.Visible = False
        '
        'osdidLlanta
        '
        Me.osdidLlanta.HeaderText = "idLlanta"
        Me.osdidLlanta.Name = "osdidLlanta"
        '
        'osdPosicion
        '
        Me.osdPosicion.HeaderText = "Posicion"
        Me.osdPosicion.Name = "osdPosicion"
        '
        'grdOrdenServCab
        '
        Me.grdOrdenServCab.AllowUserToAddRows = False
        Me.grdOrdenServCab.AllowUserToResizeColumns = False
        Me.grdOrdenServCab.AllowUserToResizeRows = False
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.Yellow
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black
        Me.grdOrdenServCab.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle4
        Me.grdOrdenServCab.BackgroundColor = System.Drawing.Color.White
        Me.grdOrdenServCab.ColumnHeadersHeight = 34
        Me.grdOrdenServCab.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.grdOrdenServCab.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.oscCLAVE, Me.oscConsecutivo, Me.oscIdOrdenSer, Me.oscfechaCaptura, Me.oscTipoOrdenServ, Me.oscNomTipOrdServ, Me.oscidUnidadTrans, Me.oscidTipoUnidadTrans, Me.oscnomTipoUniTras, Me.oscKm, Me.oscAutorizo, Me.oscbOrdenTrabajo, Me.oscTipoServ, Me.oscModificado, Me.oscCancelado})
        Me.grdOrdenServCab.ContextMenuStrip = Me.ConMnuOrdSer
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.Yellow
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.grdOrdenServCab.DefaultCellStyle = DataGridViewCellStyle8
        Me.grdOrdenServCab.Location = New System.Drawing.Point(9, 19)
        Me.grdOrdenServCab.Name = "grdOrdenServCab"
        Me.grdOrdenServCab.RowHeadersWidth = 26
        DataGridViewCellStyle9.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.Yellow
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.Black
        Me.grdOrdenServCab.RowsDefaultCellStyle = DataGridViewCellStyle9
        Me.grdOrdenServCab.Size = New System.Drawing.Size(816, 124)
        Me.grdOrdenServCab.TabIndex = 34
        '
        'oscCLAVE
        '
        Me.oscCLAVE.HeaderText = "oscCLAVE"
        Me.oscCLAVE.Name = "oscCLAVE"
        Me.oscCLAVE.Visible = False
        '
        'oscConsecutivo
        '
        Me.oscConsecutivo.HeaderText = "#"
        Me.oscConsecutivo.Name = "oscConsecutivo"
        Me.oscConsecutivo.Width = 60
        '
        'oscIdOrdenSer
        '
        Me.oscIdOrdenSer.HeaderText = "Orden Servicio"
        Me.oscIdOrdenSer.Name = "oscIdOrdenSer"
        Me.oscIdOrdenSer.Visible = False
        '
        'oscfechaCaptura
        '
        Me.oscfechaCaptura.HeaderText = "Fecha"
        Me.oscfechaCaptura.Name = "oscfechaCaptura"
        Me.oscfechaCaptura.Width = 150
        '
        'oscTipoOrdenServ
        '
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.oscTipoOrdenServ.DefaultCellStyle = DataGridViewCellStyle5
        Me.oscTipoOrdenServ.HeaderText = "Tipo"
        Me.oscTipoOrdenServ.Name = "oscTipoOrdenServ"
        Me.oscTipoOrdenServ.Visible = False
        Me.oscTipoOrdenServ.Width = 60
        '
        'oscNomTipOrdServ
        '
        Me.oscNomTipOrdServ.HeaderText = "Nombre Tipo"
        Me.oscNomTipOrdServ.Name = "oscNomTipOrdServ"
        '
        'oscidUnidadTrans
        '
        Me.oscidUnidadTrans.HeaderText = "Unidad Transporte"
        Me.oscidUnidadTrans.Name = "oscidUnidadTrans"
        '
        'oscidTipoUnidadTrans
        '
        Me.oscidTipoUnidadTrans.HeaderText = "idTipoUnidad"
        Me.oscidTipoUnidadTrans.Name = "oscidTipoUnidadTrans"
        Me.oscidTipoUnidadTrans.Visible = False
        '
        'oscnomTipoUniTras
        '
        Me.oscnomTipoUniTras.HeaderText = "Tipo Unidad"
        Me.oscnomTipoUniTras.Name = "oscnomTipoUniTras"
        '
        'oscKm
        '
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.oscKm.DefaultCellStyle = DataGridViewCellStyle6
        Me.oscKm.HeaderText = "Kilometraje"
        Me.oscKm.Name = "oscKm"
        Me.oscKm.Width = 60
        '
        'oscAutorizo
        '
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.oscAutorizo.DefaultCellStyle = DataGridViewCellStyle7
        Me.oscAutorizo.HeaderText = "Autoriz�"
        Me.oscAutorizo.Name = "oscAutorizo"
        Me.oscAutorizo.Visible = False
        Me.oscAutorizo.Width = 60
        '
        'oscbOrdenTrabajo
        '
        Me.oscbOrdenTrabajo.HeaderText = "oscbOrdenTrabajo"
        Me.oscbOrdenTrabajo.Name = "oscbOrdenTrabajo"
        Me.oscbOrdenTrabajo.Visible = False
        '
        'oscTipoServ
        '
        Me.oscTipoServ.HeaderText = "oscTipoServ"
        Me.oscTipoServ.Name = "oscTipoServ"
        Me.oscTipoServ.Visible = False
        '
        'oscModificado
        '
        Me.oscModificado.HeaderText = "oscModificado"
        Me.oscModificado.Name = "oscModificado"
        Me.oscModificado.Visible = False
        '
        'oscCancelado
        '
        Me.oscCancelado.HeaderText = "oscCancelado"
        Me.oscCancelado.Name = "oscCancelado"
        Me.oscCancelado.Visible = False
        '
        'ConMnuOrdSer
        '
        Me.ConMnuOrdSer.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ModificarToolStripMenuOrdSer, Me.CancelarToolStripMenuOrdSer, Me.ImprimirToolStripMenuOrdSer})
        Me.ConMnuOrdSer.Name = "ConMnuOrdSer"
        Me.ConMnuOrdSer.Size = New System.Drawing.Size(126, 70)
        '
        'ModificarToolStripMenuOrdSer
        '
        Me.ModificarToolStripMenuOrdSer.Name = "ModificarToolStripMenuOrdSer"
        Me.ModificarToolStripMenuOrdSer.Size = New System.Drawing.Size(125, 22)
        Me.ModificarToolStripMenuOrdSer.Text = "Modificar"
        '
        'CancelarToolStripMenuOrdSer
        '
        Me.CancelarToolStripMenuOrdSer.Name = "CancelarToolStripMenuOrdSer"
        Me.CancelarToolStripMenuOrdSer.Size = New System.Drawing.Size(125, 22)
        Me.CancelarToolStripMenuOrdSer.Text = "Cancelar"
        '
        'ImprimirToolStripMenuOrdSer
        '
        Me.ImprimirToolStripMenuOrdSer.Name = "ImprimirToolStripMenuOrdSer"
        Me.ImprimirToolStripMenuOrdSer.Size = New System.Drawing.Size(125, 22)
        Me.ImprimirToolStripMenuOrdSer.Text = "Imprimir"
        '
        'frmSolServ
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(873, 556)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.ToolStripMenu)
        Me.Controls.Add(Me.GrupoCaptura)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmSolServ"
        Me.Text = "Solicitud de Orden de Servicio"
        Me.ToolStripMenu.ResumeLayout(False)
        Me.ToolStripMenu.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GrupoCaptura.ResumeLayout(False)
        CType(Me.grdOrdenServDet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdOrdenServCab, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ConMnuOrdSer.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Sub New(ByVal vTag As String, Optional ByVal vNoServ As Integer = 0)
        InitializeComponent()
        _Tag = vTag
        _NoServ = vNoServ
    End Sub

    '21/SEP/2016
    Public Sub New(con As String, nomUsuario As String, cveEmpresa As Integer, IdUsuario As Integer)
        _con = con
        BD = New CapaDatos.UtilSQL(con, "Juan")
        InitializeComponent()
        _Usuario = nomUsuario
        _cveEmpresa = cveEmpresa
        _IdUsuario = IdUsuario
        Inicio.CONSTR = _con

        'VErifica Permiso Usuario
        usuario = New UsuarioClass("0")
        _MultiEmpresa = usuario.VerificaPermisoEspecial(_Usuario, "ISADMIN")


    End Sub
    'Public Sub New(ByVal vNoServ As Integer)
    '    InitializeComponent()
    '    _NoServ = vNoServ
    'End Sub
    '28/SEP/2016
    Private Function UltimoMaster() As Integer

        Dim Obj As New CapaNegocio.Tablas
        Dim Ultimo As Integer = 0
        Ultimo = objNumSig.NumSigEnTablas(Inicio.CONSTR, "idPadreOrdSer", "PARAMETROS", "MasterOrdServ", " WHERE IDPARAMETRO = 1", " ORDER BY idPadreOrdSer DESC")
        'Ultimo = Obj.NumSigEnTablasSinUpdate(Inicio.CONSTR, "Consecutivo", "detPromociones", " WHERE IdPromocion = " & Promocion)

        Return Ultimo
    End Function

    Private Function UltimoOrdenTrab() As Integer

        Dim Obj As New CapaNegocio.Tablas
        Dim Ultimo As Integer = 0
        Ultimo = objNumSig.NumSigEnTablas(Inicio.CONSTR, "idOrdenTrabajo", "PARAMETROS", "DetRecepcionOS", " WHERE IDPARAMETRO = 1", " ORDER BY idOrdenTrabajo DESC")
        'Ultimo = Obj.NumSigEnTablasSinUpdate(Inicio.CONSTR, "Consecutivo", "detPromociones", " WHERE IdPromocion = " & Promocion)

        Return Ultimo
    End Function

    Private Function UltimoOrdServ() As Integer
        Dim Obj As New CapaNegocio.Tablas
        Dim Ultimo As Integer = 0
        Ultimo = objNumSig.NumSigEnTablas(Inicio.CONSTR, "idOrdenSer", "PARAMETROS", "CabOrdenServicio", " WHERE IDPARAMETRO = 1", " ORDER BY idOrdenSer DESC")
        Return Ultimo
    End Function


    '21/SEP/2016
    Private Sub LlenaCheckBox()
        TipOrdServ = New TipoServClass(0)
        MyTablaTipoOrdServ = TipOrdServ.TablaTiposOrdenServicio("")
        If Not MyTablaTipoOrdServ Is Nothing Then
            If MyTablaTipoOrdServ.Rows.Count > 0 Then
                chkBoxTipoOrdServ.Items.Clear()
                chkBoxTipoOrdServ.DisplayMember = "nomTipOrdServ"
                For i = 0 To MyTablaTipoOrdServ.Rows.Count - 1

                    Dim tipoOrden As TiposOrdenServicio = New TiposOrdenServicio(0)
                    tipoOrden.idTipoOrden = MyTablaTipoOrdServ.DefaultView.Item(i).Item("idTipOrdServ")
                    tipoOrden.nomTipOrdServ = MyTablaTipoOrdServ.DefaultView.Item(i).Item("NomTipOrdServ")
                    tipoOrden.bOrdenTrab = MyTablaTipoOrdServ.DefaultView.Item(i).Item("bOrdenTrab")
                    chkBoxTipoOrdServ.Items.Add(tipoOrden)

                    'chkBoxTipoOrdServ.Items.Add(MyTablaTipoOrdServ.DefaultView.Item(i).Item("NomTipOrdServ"))
                Next

            End If
        End If

    End Sub
    'Public Function Autocompletar() As AutoCompleteStringCollection
    '    Personal = New PersonalClass(0)


    '    Dim dt As DataTable = Personal.TablaChoferesCombo(True, 1)
    '    Dim coleccion As New AutoCompleteStringCollection()
    '    'Recorrer y cargar los items para el Autocompletado
    '    For Each row As DataRow In dt.Rows
    '        coleccion.Add(Convert.ToString(row("NombreCompleto")))
    '    Next

    '    Return coleccion
    'End Function
    Public Function AutoCompletarFolios(ByVal Control As TextBox, Optional ByVal FiltroSinWhere As String = "") As AutoCompleteStringCollection
        Dim Coleccion As New AutoCompleteStringCollection

        PadreOS = New MasterOrdServClass(0)

        Dim dt As DataTable = PadreOS.tbPadreOS()

        For Each row As DataRow In dt.Rows
            Coleccion.Add(Convert.ToString(row("idPadreOrdSer")))
        Next
        'Ajustamos el control TextBox o ComboBox para recibir los datos de la siguiente manera.
        With Control

            .AutoCompleteCustomSource = Coleccion
            .AutoCompleteMode = AutoCompleteMode.Suggest
            .AutoCompleteSource = AutoCompleteSource.CustomSource
        End With

        ' Aca le devuelvo los datos a recuperados de la Base
        Return Coleccion
    End Function

    Public Function AutoCompletarUnidadTransp(ByVal Control As TextBox, ByVal Activo As Boolean,
    ByVal idEmpresa As Integer,
    ByVal Tipo As String, Optional ByVal FiltroSinWhere As String = "") As AutoCompleteStringCollection
        Dim Coleccion As New AutoCompleteStringCollection

        UniTrans = New UnidadesTranspClass(0)

        Dim dt As DataTable = UniTrans.TablaUnidadxTipo(Activo, idEmpresa, Tipo, FiltroSinWhere)
        'Recorrer y cargar los items para el Autocompletado
        For Each row As DataRow In dt.Rows
            Coleccion.Add(Convert.ToString(row("idUnidadTrans")))
        Next
        'Ajustamos el control TextBox o ComboBox para recibir los datos de la siguiente manera.
        With Control

            .AutoCompleteCustomSource = Coleccion
            .AutoCompleteMode = AutoCompleteMode.Suggest
            .AutoCompleteSource = AutoCompleteSource.CustomSource
        End With

        ' Aca le devuelvo los datos a recuperados de la Base
        Return Coleccion
    End Function

    Public Function AutoCompletarPersonal(ByVal Control As ComboBox, ByVal TipoCon As TipotablasPer,
    ByVal Empresa As Integer) As AutoCompleteStringCollection

        Dim Coleccion As New AutoCompleteStringCollection
        Dim dt As DataTable

        Personal = New PersonalClass(0)
        If TipoCon = TipotablasPer.tpChoferes Then
            dt = Personal.TablaChoferesCombo(True, Empresa)
        ElseIf TipoCon = TipotablasPer.tpSupervisores Then
            If _cveEmpresa = 1 Then
                dt = Personal.TablaAutorizaSolServ(True, Empresa, "2", "1")
            ElseIf _cveEmpresa = 2 Then
                dt = Personal.TablaAutorizaSolServ(True, Empresa, "1,2,3,5", "")
            End If

        End If

        'Recorrer y cargar los items para el Autocompletado
        For Each row As DataRow In dt.Rows
            Coleccion.Add(Convert.ToString(row("NombreCompleto")))
        Next
        'Ajustamos el control TextBox o ComboBox para recibir los datos de la siguiente manera.
        With Control
            .DataSource = dt
            .DisplayMember = "NombreCompleto"
            .ValueMember = "idPersonal"


            .AutoCompleteCustomSource = Coleccion
            .AutoCompleteMode = AutoCompleteMode.Suggest
            .AutoCompleteSource = AutoCompleteSource.CustomSource
        End With

        ' Aca le devuelvo los datos a recuperados de la Base
        Return Coleccion

    End Function

    Private Sub frmDiagnosticoTaller_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'LimpiaCampos()

        Salida = True
        If Inicio.CONSTR = "" Then
            Inicio.CONSTR = _con
        End If

        LlenaCheckBox()
        'LlenaCombos()
        'Autocompletar()
        Cancelar()
        'CreaTablaProdRefacciones()
        CreaTablaDetalle()

        'Salida = False
        If _NoServ > 0 Then
            'txtOrdenTrabajo.Text = _NoServ
            'txtOrdenTrabajo_Leave(sender, e)
        End If

        AutoCompletarTodos(_cveEmpresa)

        'txtNomChofer.AutoCompleteCustomSource = Autocompletar()
        'txtNomChofer.AutoCompleteMode = AutoCompleteMode.Suggest
        'txtNomChofer.AutoCompleteSource = AutoCompleteSource.CustomSource

        'ActivaBotones(False, TipoOpcActivaBoton.tOpcInicializa)
        CreaTablaImprime()

        If _MultiEmpresa Then
            AutoCompletarFolios(txtidPadreOrdSer)
            cmbIdEmpresa.Enabled = True
        Else
            cmbIdEmpresa.Enabled = False
        End If
        cmbIdEmpresa.SelectedValue = _cveEmpresa


        'Empresa = New EmpresaClass(_cveEmpresa)
        'If Empresa.Existe Then
        '    txtNomEmpresa.Text = Empresa.RazonSocial
        '    cmbIdEmpresa.
        'End If


        Me.Show()
        'txtidPadreOrdSer.Focus()
        'txtidPadreOrdSer.SelectAll()
        'txtTractor.Focus()

        cmbIdEmpresa.Focus()





    End Sub

    'Private Sub LlenaCombos()
    '    Try
    '        Personal = New PersonalClass(0)

    '        MyTablaComboResp = Personal.TablaPersonalCombo(" per.iddepto in (6) and per.idpuesto in (10,11,12,13,14,15,16,17,20,21,22,23,24,25) ")
    '        If Not MyTablaComboResp Is Nothing Then
    '            If MyTablaComboResp.Rows.Count > 0 Then
    '                cmbResponsable.DataSource = MyTablaComboResp
    '                cmbResponsable.ValueMember = "idpersonal"
    '                cmbResponsable.DisplayMember = "nombrecompleto"

    '                cmbPuestoResp.DataSource = MyTablaComboResp
    '                cmbPuestoResp.ValueMember = "idpuesto"
    '                cmbPuestoResp.DisplayMember = "Nompuesto"

    '                'DsComboMovimiento = DsCombo
    '            End If
    '        End If

    '        Personal = New PersonalClass(0)
    '        MyTablaComboAyu1 = Personal.TablaPersonalCombo(" per.iddepto in (6) and per.idpuesto in (10,11,12,13,14,15,16,17,20,21,22,23,24,25) ")
    '        If Not MyTablaComboAyu1 Is Nothing Then
    '            If MyTablaComboAyu1.Rows.Count > 0 Then
    '                cmbayudante1.DataSource = MyTablaComboAyu1
    '                cmbayudante1.ValueMember = "idpersonal"
    '                cmbayudante1.DisplayMember = "nombrecompleto"

    '                cmbPuestoAyu1.DataSource = MyTablaComboAyu1
    '                cmbPuestoAyu1.ValueMember = "idpuesto"
    '                cmbPuestoAyu1.DisplayMember = "Nompuesto"
    '            End If
    '        End If

    '        Personal = New PersonalClass(0)
    '        MyTablaComboAyu2 = Personal.TablaPersonalCombo(" per.iddepto in (6) and per.idpuesto in (10,11,12,13,14,15,16,17,20,21,22,23,24,25) ")
    '        If Not MyTablaComboAyu2 Is Nothing Then
    '            If MyTablaComboAyu2.Rows.Count > 0 Then
    '                cmbayudante2.DataSource = MyTablaComboAyu2
    '                cmbayudante2.ValueMember = "idpersonal"
    '                cmbayudante2.DisplayMember = "nombrecompleto"

    '                cmbPuestoAyu2.DataSource = MyTablaComboAyu2
    '                cmbPuestoAyu2.ValueMember = "idpuesto"
    '                cmbPuestoAyu2.DisplayMember = "Nompuesto"
    '            End If
    '        End If

    '    Catch ex As Exception
    '        MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
    '    End Try
    'End Sub


    Private Sub CreaTablaResultados()
        Try
            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Int64")
            myDataColumn.ColumnName = "trefCIDPRODUCTO"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRefac.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Int64")
            myDataColumn.ColumnName = "trefCIDUNIDADBASE"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRefac.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.String")
            myDataColumn.ColumnName = "trefCCODIGOP01_PRO"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRefac.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.String")
            myDataColumn.ColumnName = "trefCNOMBREP01"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRefac.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Double")
            myDataColumn.ColumnName = "trefCOSTO"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRefac.Columns.Add(myDataColumn)


            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.String")
            myDataColumn.ColumnName = "trefCCLAVEINT"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRefac.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Double")
            myDataColumn.ColumnName = "trefExistencia"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRefac.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Double")
            myDataColumn.ColumnName = "trefCantidad"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRefac.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Boolean")
            myDataColumn.ColumnName = "trefBComprar"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRefac.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Double")
            myDataColumn.ColumnName = "trefCantCompra"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRefac.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Double")
            myDataColumn.ColumnName = "trefCantSalida"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRefac.Columns.Add(myDataColumn)

        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try

    End Sub
    Private Sub LimpiaCampos()
        dtpFechaCap.Value = Now
        dtpFechaCapturaHora.Value = Now
        'txtNomEmpresa.Text = ""
        cmbIdEmpresa.SelectedValue = 0

        txtidPadreOrdSer.Text = ""
        'txtKilometraje.Text = ""

        txtTractor.Text = ""
        txtRemolque1.Text = ""
        txtDolly.Text = ""
        txtRemolque2.Text = ""

        txtEstatus.Text = ""
        txtMotivoCancela.Text = ""

        If cmbChofer.Items.Count > 0 Then
            cmbChofer.SelectedIndex = 0
        End If
        If cmbAutoriza.Items.Count > 0 Then
            cmbAutoriza.SelectedIndex = 0
        End If

        For i As Integer = 1 To chkBoxTipoOrdServ.Items.Count - 1

            chkBoxTipoOrdServ.SetItemChecked(i, False)

        Next

        grdOrdenServCab.DataSource = Nothing
        grdOrdenServCab.Rows.Clear()
        grdOrdenServDet.DataSource = Nothing
        grdOrdenServDet.Rows.Clear()
        'grdRefaccionesHis.DataSource = Nothing
        'grdRefaccionesHis.Rows.Clear()

        'BorraCamposProd()

        'grdProductosOT.DataSource = Nothing
        'grdProductosOT.Rows.Clear()

        txtTractor.Focus()

    End Sub

    Private Sub txtOrdenTrabajo_KeyDown(sender As Object, e As KeyEventArgs)
        If e.KeyCode = Keys.F3 Then
            cmdBuscaClave_Click(sender, e)
        End If
    End Sub

    'Private Sub txtOrdenTrabajo_KeyPress(sender As Object, e As KeyPressEventArgs)
    '    If Asc(e.KeyChar) = 13 Then
    '        If txtOrdenTrabajo.Text.Trim <> "" Then
    '            CargaForm(txtOrdenTrabajo.Text, "OrdenTrabajo")


    '        End If
    '        e.Handled = True
    '    End If
    'End Sub

    'Private Sub txtOrdenTrabajo_Leave(sender As Object, e As EventArgs)
    '    If Salida Then Exit Sub
    '    If txtOrdenTrabajo.Text.Length = 0 Then
    '        txtOrdenTrabajo.Focus()
    '        txtOrdenTrabajo.SelectAll()
    '    Else
    '        If txtOrdenTrabajo.Enabled Then
    '            CargaForm(txtOrdenTrabajo.Text, "OrdenTrabajo")

    '        End If
    '    End If
    'End Sub

    'Private Sub txtOrdenTrabajo_MouseDown(sender As Object, e As MouseEventArgs)
    '    txtOrdenTrabajo.Focus()
    '    txtOrdenTrabajo.SelectAll()
    'End Sub

    Private Sub cmdBuscaClave_Click(sender As Object, e As EventArgs)
        'txtOrdenTrabajo.Text = DespliegaGrid(UCase(TablaBd), Inicio.CONSTR, "")
        'txtidEstado_Leave(sender, e)
    End Sub

    Private Sub CargaForm(ByVal idClave As String, ByVal NomTabla As String, Optional ByVal ValorExtra1 As String = "",
                          Optional ByVal txtControl As TextBox = Nothing, Optional ByVal FiltroSinWhere As String = "",
                          Optional ByVal bRequerido As Boolean = False)

        Try
            Select Case UCase(NomTabla)
                Case UCase("FOLIOOS")
                    If Val(idClave) > 0 Then
                        PadreOS = New MasterOrdServClass(Val(idClave))
                        If PadreOS.Existe Then
                            txtTractor.Text = PadreOS.idTractor
                            txtRemolque1.Text = PadreOS.idTolva1
                            txtDolly.Text = PadreOS.idDolly
                            txtRemolque2.Text = PadreOS.idTolva2
                            dtpFechaCap.Value = PadreOS.FechaCreacion
                            dtpFechaCapturaHora.Value = PadreOS.FechaCreacion
                            txtEstatus.Text = PadreOS.Estatus
                            txtMotivoCancela.Text = PadreOS.MotivoCancela
                            'txtKilometraje.Text = PadreOS.Kilometraje
                            cmbAutoriza.SelectedValue = PadreOS.idEmpleadoAutoriza

                            Dim tablaTipOS As DataTable = PadreOS.tbTipoOrdSerxPadre(Val(idClave))
                            If tablaTipOS.Rows.Count > 0 Then
                                Dim list = chkBoxTipoOrdServ.Items

                                Dim listObj As List(Of TiposOrdenServicio) = New List(Of TiposOrdenServicio)()
                                For Each obj In list
                                    listObj.Add(CType(obj, TiposOrdenServicio))
                                Next

                                Dim i As Integer = 0
                                For Each lobj In listObj
                                    For Each row As DataRow In tablaTipOS.Rows
                                        If lobj.idTipoOrden = row("idTipOrdServ") Then
                                            chkBoxTipoOrdServ.SetItemChecked(i, True)
                                        End If
                                    Next
                                    i = i + 1
                                Next
                            End If
                            'cmbAutoriza.SelectedValue = PadreO  
                            'Aqui se Consulta las Ordenes de Servicio
                            Dim TablaOrdSer As DataTable = PadreOS.tbOrdSerxPadre(Val(idClave))
                            If TablaOrdSer.Rows.Count > 0 Then
                                For i = 0 To TablaOrdSer.Rows.Count - 1
                                    '.ToString()
                                    'idTipoServicio
                                    InsertaRegGridCab(TablaOrdSer.DefaultView.Item(i).Item("CLAVE"), TablaOrdSer.DefaultView.Item(i).Item("idTipOrdServ"),
                                    TablaOrdSer.DefaultView.Item(i).Item("NomTipOrdServ").ToString(), TablaOrdSer.DefaultView.Item(i).Item("idUnidadTrans").ToString(),
                                    TablaOrdSer.DefaultView.Item(i).Item("idTipoUnidad"), TablaOrdSer.DefaultView.Item(i).Item("nomTipoUniTras").ToString(),
                                    TablaOrdSer.DefaultView.Item(i).Item("Kilometraje"), TablaOrdSer.DefaultView.Item(i).Item("idEmpleadoAutoriza"),
                                    TablaOrdSer.DefaultView.Item(i).Item("bOrdenTrab"), TablaOrdSer.DefaultView.Item(i).Item("idTipoServicio"), False, False)

                                    '                InsertaRegGridCab(vNumOrdServ, vIdTipo, lobj.nomTipOrdServ, txtTractor.Text, UniTrans.idTipoUnidad,
                                    'TipoUniTrans.nomTipoUniTras, Val(txtKilometraje.Text), cmbAutoriza.SelectedValue, lobj.bOrdenTrab)

                                Next
                            End If

                            'Dim vValorServ As Integer

                            Dim TablaDetOrdSer As DataTable = PadreOS.tbDetOrdSerxPadre(Val(idClave))
                            If TablaDetOrdSer.Rows.Count > 0 Then
                                For i = 0 To TablaDetOrdSer.Rows.Count - 1

                                    If TablaDetOrdSer.DefaultView.Item(i).Item("IDACTIVIDAD") = 0 Then
                                        If TablaDetOrdSer.DefaultView.Item(i).Item("IDSERVICIO") = 0 Then
                                            vValorNomAct = TablaDetOrdSer.DefaultView.Item(i).Item("NOTARECEPCION")
                                            vValorNomDivision = TablaDetOrdSer.DefaultView.Item(i).Item("NOMDIVISIONOT")
                                            vValorDivision = TablaDetOrdSer.DefaultView.Item(i).Item("IDDIVISIONOT")
                                        Else
                                            vValorNomAct = ""
                                            'vValorNomAct = TablaDetOrdSer.DefaultView.Item(i).Item("NOMSERVICIO")
                                            vValorNomDivision = TablaDetOrdSer.DefaultView.Item(i).Item("NOMDIVISION")
                                            vValorDivision = TablaDetOrdSer.DefaultView.Item(i).Item("IDDIVISION")
                                        End If
                                    Else
                                        vValorNomAct = TablaDetOrdSer.DefaultView.Item(i).Item("NOMBREACT")
                                        vValorNomDivision = TablaDetOrdSer.DefaultView.Item(i).Item("NOMDIVISION")
                                        vValorDivision = TablaDetOrdSer.DefaultView.Item(i).Item("IDDIVISION")
                                    End If

                                    '7/jun/2017
                                    'If TablaDetOrdSer.DefaultView.Item(i).Item("IDSERVICIO") = 0 Then
                                    '    vValorNomAct = TablaDetOrdSer.DefaultView.Item(i).Item("NOTARECEPCION")
                                    '    vValorNomDivision = TablaDetOrdSer.DefaultView.Item(i).Item("NOMDIVISIONOT")
                                    '    vValorDivision = TablaDetOrdSer.DefaultView.Item(i).Item("IDDIVISIONOT")
                                    'Else
                                    '    vValorNomAct = TablaDetOrdSer.DefaultView.Item(i).Item("NOMBREACT")
                                    '    vValorNomDivision = TablaDetOrdSer.DefaultView.Item(i).Item("NOMDIVISION")
                                    '    vValorDivision = TablaDetOrdSer.DefaultView.Item(i).Item("IDDIVISION")
                                    'End If

                                    InsertaRegGridDet(TablaDetOrdSer.DefaultView.Item(i).Item("CLAVE"),
                                    TablaDetOrdSer.DefaultView.Item(i).Item("IDSERVICIO"),
                                    TablaDetOrdSer.DefaultView.Item(i).Item("NOMSERVICIO"),
                                    TablaDetOrdSer.DefaultView.Item(i).Item("IDACTIVIDAD"),
                                    vValorNomAct,
                                    vValorDivision,
                                    vValorNomDivision,
                                    TablaDetOrdSer.DefaultView.Item(i).Item("DURACIONACTHR"),
                                    TablaDetOrdSer.DefaultView.Item(i).Item("IDORDENSER"),
                                    TablaDetOrdSer.DefaultView.Item(i).Item("IDORDENTRABAJO"), False,
                                    TablaDetOrdSer.DefaultView.Item(i).Item("idOrdenActividad"),
                                    TablaDetOrdSer.DefaultView.Item(i).Item("idLlanta"),
                                    TablaDetOrdSer.DefaultView.Item(i).Item("Posicion"))


                                Next
                            End If
                            Clave = grdOrdenServCab.Item("oscCLAVE", grdOrdenServCab.CurrentCell.RowIndex).Value
                            FiltraDetallexClave(Clave)

                        Else

                        End If

                    Else
                        If bRequerido Or Val(idClave) > 0 Then
                            'txtidPadreOrdSer.Text = DespliegaGrid(UCase("UNITRANSTIPO"), Inicio.CONSTR, "", FiltroSinWhere)
                            txtidPadreOrdSer.Text = DespliegaGrid(UCase("FolioOS"), Inicio.CONSTR, "idPadreOrdSer")
                            If txtidPadreOrdSer.Text <> "" Then
                                CargaForm(txtidPadreOrdSer.Text, "FOLIOOS")
                            End If
                        End If
                    End If
                Case UCase("UNITRANS")

                    If Val(idClave) > 0 Then
                        UniTrans = New UnidadesTranspClass(Val(idClave), ValorExtra1)
                    Else
                        UniTrans = New UnidadesTranspClass(idClave)
                    End If

                    If UniTrans.Existe Then
                        txtControl.Text = UniTrans.iUnidadTrans

                        TipoUniTrans = New TipoUniTransClass(UniTrans.idTipoUnidad)

                        If TipoUniTrans.Existe Then
                            If UCase(TipoUniTrans.Clasificacion) = UCase("Tractor") Then
                                vUltKm = UniTrans.UltKilomValidado(UniTrans.iUnidadTrans)
                                UniTransOp = New UnidadesTranspClass("0")
                                'Ultimp Operador
                                UltElemento = UniTransOp.ULtimoElementoViaje(UniTrans.iUnidadTrans, OpcTipoElementoViaje.Operador)
                                If Val(UltElemento) > 0 Then
                                    cmbChofer.SelectedValue = Val(UltElemento)
                                End If
                                'Ultimo Remolque 1
                                UltElemento = UniTransOp.ULtimoElementoViaje(UniTrans.iUnidadTrans, OpcTipoElementoViaje.Remolque1)
                                If UltElemento <> "" Then
                                    txtRemolque1.Text = UltElemento
                                End If
                                'Ultimo Dolly
                                UltElemento = UniTransOp.ULtimoElementoViaje(UniTrans.iUnidadTrans, OpcTipoElementoViaje.Dolly)
                                If UltElemento <> "" Then
                                    txtDolly.Text = UltElemento
                                End If
                                'Ultimo Remolque 2
                                UltElemento = UniTransOp.ULtimoElementoViaje(UniTrans.iUnidadTrans, OpcTipoElementoViaje.Remolque2)
                                If UltElemento <> "" Then
                                    txtRemolque2.Text = UltElemento
                                End If

                            End If
                        End If
                        BandResultado = True
                    Else
                        'txtControl.Text = DespliegaGrid(UCase("UNITRANSTIPO"), Inicio.CONSTR, "", "tip.TipoUso = 'tractor' AND UNI.ESTATUS = 'ACT' AND UNI.IDEMPRESA = " & _Usuario.personal.idEmpresa & " AND UNI.IDUNIDADTRANS LIKE '%" & Val(idClave) & "%'", "TRACTOR")
                        If bRequerido Or Val(idClave) > 0 Then
                            txtControl.Text = DespliegaGrid(UCase("UNITRANSTIPO"), Inicio.CONSTR, "", FiltroSinWhere)
                            If txtControl.Text <> "" Then
                                CargaForm(txtControl.Text, "UNITRANS", ValorExtra1, txtControl)
                            End If
                        Else
                            If txtControl.Text <> "" Then
                                MsgBox("La Unidad de Transporte : " & idClave & " No Existe")
                                BandResultado = False
                                'txtControl.SelectAll()
                            End If
                        End If
                        'MsgBox("La Unidad de Transporte : " & idClave & " No Existe")
                        'txtControl.Focus()
                        'txtControl.SelectAll()
                    End If


                    'Case UCase("UNIPROD_COM")
                    '    UnidadCom = New UnidadesComClass(idClave, "CCLAVEINT", TipoDato.TdCadena)
                    '    If UnidadCom.Existe Then
                    '        txtNomUniProd.Text = UnidadCom.CNOMBREUNIDAD
                    '        vCIDUNIDAD = UnidadCom.CIDUNIDAD
                    '    End If
                    'Case UCase("PROD_COM")
                    '    ProdCom = New ProductosComClass(idClave, "")
                    '    If ProdCom.Existe Then
                    '        If ProdCom.CSTATUSPRODUCTO = 1 Then
                    '            vCIDPRODUCTO = ProdCom.CIDPRODUCTO
                    '            txtNomProducto.Text = ProdCom.CNOMBREPRODUCTO
                    '            UnidadCom = New UnidadesComClass(ProdCom.CIDUNIDADBASE, "CIDUNIDAD", TipoDato.TdNumerico)
                    '            If UnidadCom.Existe Then
                    '                txtIdUnidadProd.Text = UnidadCom.CCLAVEINT
                    '            End If

                    '        Else
                    '            'Desactivado
                    '        End If
                    '    End If
                Case UCase("OrdenServicio")
                    OrdenServ = New OrdenServClass(idClave)
                    With OrdenServ
                        txtidPadreOrdSer.Text = idClave
                        If .Existe Then
                            txtidPadreOrdSer.Text = .idOrdenSer
                            'dtpFechaOrden.Value = OrdenServ.FechaRecepcion
                            'dtpFechaOrdenhr.Value = OrdenServ.FechaRecepcion

                            cmbIdEmpresa.SelectedValue = OrdenServ.IdEmpresa
                            'Emp = New EmpresaClass(OrdenServ.IdEmpresa)
                            'If Emp.Existe Then
                            '    txtNomEmpresa.Text = Emp.RazonSocial
                            'End If

                            TipOrd = New TipoOrdenClass(OrdenServ.idTipoOrden)
                            'If TipOrd.Existe Then
                            '    txtTipoOrden.Text = TipOrd.NomTipoOrden
                            'End If
                            'txtEstatusOrden.Text = OrdenServ.Estatus

                            '10/ENE/2017
                            'txtKilometraje.Text = OrdenServ.Kilometraje

                            'RECEPCIONO
                            'User = New UsuarioClass(OrdenServ.UsuarioRecepcion)
                            'If User.Existe Then
                            '    txtRecepciono.Text = User.NombreUsuario
                            'End If
                            'ENTREGO
                            'Personal = New PersonalClass(OrdenServ.idEmpleadoEntrega)
                            'If Personal.Existe Then
                            '    txtEntrego.Text = Personal.NombreCompleto
                            'End If
                            'AUTORIZO
                            'Personal = New PersonalClass(OrdenServ.idEmpleadoAutoriza)
                            'If Personal.Existe Then
                            '    txtAutorizo.Text = Personal.NombreCompleto
                            'End If
                        End If
                    End With
                Case UCase("OrdenTrabajo")
                    OrdenTrabajo = New OrdenTrabajoClass(idClave)
                    With OrdenTrabajo
                        'ORDEN DE TRABAJO
                        'txtOrdenTrabajo.Text = idClave
                        If .Existe Then
                            'MECANICO RESPONSABLE
                            'Personal = New PersonalClass(.idPersonalResp)
                            'cmbResponsable.SelectedValue = .idPersonalResp
                            'If Personal.Existe Then
                            '    'txtNombreMecanico.Text = Personal.NombreCompleto

                            '    PersonalPuesto = New PuestosClass(Personal.idPuesto)
                            '    If PersonalPuesto.Existe Then
                            '        'txtPuestoMec.Text = PersonalPuesto.NomPuesto
                            '    End If
                            'End If
                            ''AYUDANTE 1
                            'If .idPersonalAyu1 > 0 Then
                            '    chkAyu1.Checked = True
                            '    Personal = New PersonalClass(.idPersonalAyu1)
                            '    cmbayudante1.SelectedValue = .idPersonalAyu1
                            'Else
                            '    chkAyu1.Checked = False
                            'End If

                            ''AYUDANTE 2
                            'If .idPersonalAyu2 > 0 Then
                            '    chkAyu2.Checked = True
                            '    Personal = New PersonalClass(.idPersonalAyu2)
                            '    cmbayudante2.SelectedValue = .idPersonalAyu2
                            'Else
                            '    chkAyu2.Checked = False
                            'End If


                            'DETALLE ORDEN DE SERVICIO
                            DetOS = New detOrdenServicioClass(idClave)
                            If DetOS.Existe Then
                                dtpFechaCap.Value = DetOS.FechaAsig
                                dtpFechaCapturaHora.Value = DetOS.FechaAsig
                                User = New UsuarioClass(DetOS.UsuarioAsig)
                                'If User.Existe Then
                                '    txtUsuarioAsig.Text = User.NombreUsuario
                                'End If
                            End If

                            'ORDEN DE SERVICIO
                            OrdenServ = New OrdenServClass(.idOrdenSer)

                            txtidPadreOrdSer.Text = .idOrdenSer
                            'dtpFechaOrden.Value = OrdenServ.FechaRecepcion
                            'dtpFechaOrdenhr.Value = OrdenServ.FechaRecepcion

                            cmbIdEmpresa.SelectedValue = OrdenServ.IdEmpresa
                            'Emp = New EmpresaClass(OrdenServ.IdEmpresa)
                            'If Emp.Existe Then
                            '    txtNomEmpresa.Text = Emp.RazonSocial
                            'End If

                            'TipOrd = New TipoOrdenClass(OrdenServ.idTipoOrden)
                            'If TipOrd.Existe Then
                            '    txtTipoOrden.Text = TipOrd.NomTipoOrden
                            'End If
                            'txtEstatusOrden.Text = OrdenServ.Estatus

                            '10/ENE/2017
                            'txtKilometraje.Text = OrdenServ.Kilometraje

                            'RECEPCIONO
                            'User = New UsuarioClass(OrdenServ.UsuarioRecepcion)
                            'If User.Existe Then
                            '    txtRecepciono.Text = User.NombreUsuario
                            'End If
                            'ENTREGO
                            'Personal = New PersonalClass(OrdenServ.idEmpleadoEntrega)
                            'If Personal.Existe Then
                            '    txtEntrego.Text = Personal.NombreCompleto
                            'End If
                            'AUTORIZO
                            'Personal = New PersonalClass(OrdenServ.idEmpleadoAutoriza)
                            'If Personal.Existe Then
                            '    txtAutorizo.Text = Personal.NombreCompleto
                            'End If
                            'ORDEN DE SERVICIO

                            'UNIDAD DE TRANSPORTE
                            'txtIdUnidad.Text = OrdenServ.idUnidadTrans
                            'UniTrans = New UnidadesTranspClass(OrdenServ.idUnidadTrans)
                            'If UniTrans.Existe Then
                            '    txtNomUnidad.Text = UniTrans.DescripcionUni
                            '    Marca = New MarcaClass(UniTrans.idMarca)
                            '    If Marca.Existe Then
                            '        txtMarcaUni.Text = Marca.NOMBREMARCA
                            '    End If
                            '    'ULTIMA ORDEN DE SERVICIO

                            '    If UniTrans.UltOT > 0 Then
                            '        txtUltOrdenTrabajo.Text = UniTrans.UltOT
                            '        ULTOT = New OrdenTrabajoClass(UniTrans.UltOT)
                            '        If ULTOT.Existe Then
                            '            txtUltimaOrdenServ.Text = ULTOT.idOrdenSer
                            '            Personal = New PersonalClass(ULTOT.idPersonalResp)
                            '            If Personal.Existe Then
                            '                txtUltimoMecanico.Text = Personal.NombreCompleto
                            '            End If

                            '            txtUltFalla.Text = ULTOT.NotaRecepcion
                            '            FamiliaOT = New FamiliasClass(ULTOT.idFamilia)
                            '            If FamiliaOT.Existe Then
                            '                txtUltFamilia.Text = FamiliaOT.NomFamilia
                            '                txtUltDivision.Text = FamiliaOT.NomDivision
                            '            End If
                            '        End If
                            '    Else
                            '        txtUltOrdenTrabajo.Text = ""
                            '    End If
                            'End If


                            'Historial
                            CargaOrdenesSerHis(OrdenServ.idUnidadTrans)

                            IdOrdenServ = grdOrdenServCab.Item("hosIdServicio", grdOrdenServCab.CurrentCell.RowIndex).Value
                            'CargaOrdenesTrabHis(txtIdUnidad.Text, IdOrdenServ)

                            'VALIDACIONES
                            If OrdenServ.Estatus = "ASIG" Then
                                ActivaCampos(False, TipoOpcActivaCampos.tOpcINICIALIZA)
                                ActivaBotones(True, TipoOpcActivaBoton.tOpcInicializa)

                                'txtDuracion.Focus()
                                'txtDuracion.SelectAll()
                            ElseIf OrdenServ.Estatus = "DIA" Then
                                MsgBox("La Orden de Trabajo: " & idClave & " ya Esta en Proceso, No puede Modificarse")
                                ActivaCampos(False, TipoOpcActivaCampos.tOpcINICIALIZA)

                                ActivaBotones(False, TipoOpcActivaBoton.tOpcInicializa)
                                btnMnuLimpiar.Enabled = True
                                'txtOrdenTrabajo.Focus()
                                'txtOrdenTrabajo.SelectAll()

                            ElseIf OrdenServ.Estatus = "REC" Then
                                MsgBox("La Orden de Trabajo: " & idClave & " No esta Asignada, No puede Modificarse")
                                'txtOrdenTrabajo.Focus()
                                'txtOrdenTrabajo.SelectAll()

                            End If
                        Else
                            'No existe 
                            MsgBox("La Orden de Trabajo: " & idClave & " No Existe !!!")
                            'txtOrdenTrabajo.Focus()
                            'txtOrdenTrabajo.SelectAll()
                        End If

                    End With
                    'Case UCase(TablaBd2)
                    '    'aqui()
                    '    TipoServ = New TipoServClass(idClave)
                    '    With TipoServ
                    '        txtidTipoUnidad.Text = .idTipoServicio
                    '        If .Existe Then
                    '            txtnomTipoUniTras.Text = .NomTipoServicio
                    '            txtDescripcionUni.Focus()
                    '            txtDescripcionUni.SelectAll()
                    '        Else
                    '            MsgBox("El Tipo de Servicio con id: " & txtidTipoUnidad.Text & " No Existe")
                    '            txtidTipoUnidad.Focus()
                    '            txtidTipoUnidad.SelectAll()
                    '        End If
                    '    End With

            End Select



        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try

    End Sub

    Private Sub txtOrdenTrabajo_TextChanged(sender As Object, e As EventArgs)

    End Sub

    Private Sub btnMnuSalir_Click(sender As Object, e As EventArgs) Handles btnMnuSalir.Click
        Me.Close()
    End Sub

    Private Sub btnMnuCancelar_Click(sender As Object, e As EventArgs) Handles btnMnuLimpiar.Click
        Cancelar()
        If _MultiEmpresa Then
            cmbIdEmpresa.Focus()
        Else
            txtTractor.Focus()
        End If

    End Sub

    Private Sub CargaOrdenesSerHis(ByVal vidUnidadTrans As String)
        Try
            MyTablaHisOS.Rows.Clear()
            'grdOrdenServHis.DataSource = DetOS.CargaOrdSerxUni(OrdenServ.idUnidadTrans)
            MyTablaHisOS = DetOS.CargaOrdSerxUni(vidUnidadTrans)
            grdOrdenServCab.DataSource = Nothing
            grdOrdenServCab.Rows.Clear()

            If MyTablaHisOS.Rows.Count > 0 Then
                For i = 0 To MyTablaHisOS.DefaultView.Count - 1
                    grdOrdenServCab.Rows.Add()
                    grdOrdenServCab.Item("hosConsecutivo", i).Value = i + 1
                    'grdOrdenServHis.Item("hosIdServicio", i).Value = Trim(MyTabla.DefaultView.Item(i).Item("idOrdenSer").ToString)
                    grdOrdenServCab.Item("hosIdServicio", i).Value = MyTablaHisOS.DefaultView.Item(i).Item("idOrdenSer")
                    grdOrdenServCab.Item("hosFechaRecepcion", i).Value = MyTablaHisOS.DefaultView.Item(i).Item("FechaRecepcion")
                    grdOrdenServCab.Item("hosTipoOrden", i).Value = MyTablaHisOS.DefaultView.Item(i).Item("idTipoOrden")
                    grdOrdenServCab.Item("hosKm", i).Value = MyTablaHisOS.DefaultView.Item(i).Item("Kilometraje")
                    grdOrdenServCab.Item("hosAutorizo", i).Value = MyTablaHisOS.DefaultView.Item(i).Item("idEmpleadoAutoriza")
                    grdOrdenServCab.Item("hosEntrego", i).Value = MyTablaHisOS.DefaultView.Item(i).Item("idEmpleadoEntrega")
                    grdOrdenServCab.Item("hosRecepciono", i).Value = MyTablaHisOS.DefaultView.Item(i).Item("UsuarioRecepcion")
                    grdOrdenServCab.Item("hosFinalizo", i).Value = MyTablaHisOS.DefaultView.Item(i).Item("UsuarioEntrega")
                Next
                grdOrdenServCab.PerformLayout()
            End If

        Catch ex As Exception

        End Try
    End Sub
    Private Sub CargaOrdenesTrabHis(ByVal vidUnidadTrans As String, Optional ByVal vidOrdenSer As Integer = 0)
        Try
            MyTablaHisOT.Rows.Clear()
            'grdOrdenServHis.DataSource = DetOS.CargaOrdSerxUni(OrdenServ.idUnidadTrans)
            MyTablaHisOT = DetOS.CargaOrdTrabxUni(vidUnidadTrans, vidOrdenSer)
            grdOrdenServDet.DataSource = Nothing
            grdOrdenServDet.Rows.Clear()

            If MyTablaHisOT.Rows.Count > 0 Then
                For i = 0 To MyTablaHisOT.DefaultView.Count - 1
                    grdOrdenServDet.Rows.Add()
                    'grdOrdenServHis.Item("hosIdServicio", i).Value = Trim(MyTabla.DefaultView.Item(i).Item("idOrdenSer").ToString)
                    grdOrdenServDet.Item("hotIdOrdenSer", i).Value = MyTablaHisOT.DefaultView.Item(i).Item("idOrdenSer")
                    grdOrdenServDet.Item("hotidOrdenTrabajo", i).Value = MyTablaHisOT.DefaultView.Item(i).Item("idOrdenTrabajo")
                    grdOrdenServDet.Item("hotFechaFin", i).Value = MyTablaHisOT.DefaultView.Item(i).Item("FechaTerminado")
                    grdOrdenServDet.Item("hotAsigno", i).Value = MyTablaHisOT.DefaultView.Item(i).Item("UsuarioAsig")
                    grdOrdenServDet.Item("hotMecanico", i).Value = MyTablaHisOT.DefaultView.Item(i).Item("idPersonalResp")
                    grdOrdenServDet.Item("hotDuracion", i).Value = MyTablaHisOT.DefaultView.Item(i).Item("DuracionActHr")
                Next
                grdOrdenServDet.PerformLayout()
            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub grdOrdenServHis_CellContentClick(sender As Object, e As DataGridViewCellEventArgs)

    End Sub

    Private Sub grdOrdenServHis_Click(sender As Object, e As EventArgs)
        'DataTabla.DefaultView.RowFilter = Nothing
        'EncuentraDetalle(grdOrdenServHis.Item("hosIdServicio", grdOrdenServHis.CurrentCell.RowIndex).Value)
        IdOrdenServ = grdOrdenServCab.Item("hosIdServicio", grdOrdenServCab.CurrentCell.RowIndex).Value
        'CargaOrdenesTrabHis(txtIdUnidad.Text, IdOrdenServ)
    End Sub
    Private Sub EncuentraDetalle(ByVal idOrdenSer As Integer)

        If MyTablaHisOT.DefaultView.Count > 0 Then
            With MyTablaHisOT
                .DefaultView.RowFilter = "idOrdenSer = " & idOrdenSer
                If .DefaultView.Count > 0 Then
                    'grdOrdenTrabajoHis.DataSource = .DefaultView
                End If
            End With
        End If

    End Sub


    'Private Sub cmdBuscaProd_Click(sender As Object, e As EventArgs)
    '    Salida = True
    '    txtIdProducto.Text = DespliegaGrid(UCase("PROD_COM"), Inicio.CONSTR_COM, "CCODIGOPRODUCTO")
    '    If txtIdProducto.Text <> "" Then CargaForm(txtIdProducto.Text, UCase("PROD_COM"))
    '    CargaForm(txtIdUnidadProd.Text, UCase("UNIPROD_COM"))

    '    txtCantidad.Focus()
    '    txtCantidad.SelectAll()
    '    Salida = False
    'End Sub

    'Private Sub txtIdProducto_EnabledChanged(sender As Object, e As EventArgs)
    '    cmdBuscaProd.Enabled = txtIdProducto.Enabled
    'End Sub

    'Private Sub txtIdProducto_KeyDown(sender As Object, e As KeyEventArgs)
    '    If e.KeyCode = Keys.F3 Then
    '        cmdBuscaProd_Click(sender, e)
    '    End If

    'End Sub

    'Private Sub txtIdProducto_KeyPress(sender As Object, e As KeyPressEventArgs)
    '    If txtIdProducto.Text.Length = 0 Then
    '        txtIdProducto.Focus()
    '        txtIdProducto.SelectAll()
    '    Else
    '        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
    '            Salida = True
    '            CargaForm(txtIdProducto.Text, UCase("PROD_COM"))
    '            CargaForm(txtIdUnidadProd.Text, UCase("UNIPROD_COM"))

    '            txtCantidad.Focus()
    '            txtCantidad.SelectAll()
    '            Salida = False
    '        End If
    '    End If
    'End Sub

    'Private Sub txtIdProducto_Leave(sender As Object, e As EventArgs)
    '    If Salida Then Exit Sub
    '    If txtIdProducto.Text.Length = 0 Then
    '        txtIdProducto.Focus()
    '        txtIdProducto.SelectAll()
    '    Else
    '        If txtIdProducto.Enabled Then
    '            CargaForm(txtIdProducto.Text, UCase("PROD_COM"))
    '            CargaForm(txtIdUnidadProd.Text, UCase("UNIPROD_COM"))

    '            txtCantidad.Focus()
    '            txtCantidad.SelectAll()
    '        End If
    '    End If
    'End Sub


    'Private Sub cmdBuscaUniProd_Click(sender As Object, e As EventArgs)
    '    Salida = True
    '    txtIdProducto.Text = DespliegaGrid(UCase("UNIPROD_COM"), Inicio.CONSTR_COM, "CCODIGOPRODUCTO")
    '    If txtIdProducto.Text <> "" Then CargaForm(txtIdProducto.Text, UCase("PROD_COM"))
    '    txtCantidad.Focus()
    '    txtCantidad.SelectAll()
    '    Salida = False
    'End Sub

    'Private Sub txtIdUnidadProd_EnabledChanged(sender As Object, e As EventArgs)
    '    cmdBuscaUniProd.Enabled = txtIdUnidadProd.Enabled
    'End Sub

    'Private Sub txtIdUnidadProd_KeyDown(sender As Object, e As KeyEventArgs)
    '    If e.KeyCode = Keys.F3 Then
    '        cmdBuscaUniProd_Click(sender, e)
    '    End If

    'End Sub

    'Private Sub txtIdUnidadProd_KeyPress(sender As Object, e As KeyPressEventArgs)
    '    If txtIdUnidadProd.Text.Length = 0 Then
    '        txtIdUnidadProd.Focus()
    '        txtIdUnidadProd.SelectAll()
    '    Else
    '        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
    '            Salida = True
    '            CargaForm(txtIdUnidadProd.Text, UCase("UNIPROD_COM"))

    '            txtCantidad.Focus()
    '            txtCantidad.SelectAll()
    '            Salida = False
    '        End If
    '    End If

    'End Sub

    'Private Sub txtIdUnidadProd_Leave(sender As Object, e As EventArgs)
    '    If Salida Then Exit Sub
    '    If txtIdUnidadProd.Text.Length = 0 Then
    '        txtIdUnidadProd.Focus()
    '        txtIdUnidadProd.SelectAll()
    '    Else
    '        If txtIdProducto.Enabled Then
    '            CargaForm(txtIdUnidadProd.Text, UCase("UNIPROD_COM"))
    '            txtCantidad.Focus()
    '            txtCantidad.SelectAll()
    '        End If
    '    End If
    'End Sub

    'Private Sub txtCantidad_KeyPress(sender As Object, e As KeyPressEventArgs)
    '    If txtCantidad.Text.Length = 0 Then
    '        txtCantidad.Focus()
    '        txtCantidad.SelectAll()
    '    Else
    '        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
    '            Salida = True
    '            'CargaForm(txtIdUnidadProd.Text, UCase("UNIPROD_COM"))
    '            'Agrega a Grid
    '            GrabaProductos()
    '            BorraCamposProd()

    '            txtIdProducto.Focus()
    '            txtIdProducto.SelectAll()
    '            Salida = False
    '        End If
    '    End If
    'End Sub

    'Private Sub GrabaProductos()
    '    GrabaGridProductos()
    'End Sub

    'Private Sub GrabaGridProductos()
    '    Dim BandGraba As Boolean = False
    '    Dim vExistencia As Double = 0
    '    Dim vCosto As Double = 0
    '    Dim CantCompra As Double = 0
    '    Dim CantSalida As Double = 0
    '    Try
    '        If txtIdProducto.Text <> "" Then
    '            If (txtCantidad.Text <> CantidadGrid) Or txtCantidad.Text = 0 Then
    '                BandGraba = True
    '            End If

    '            If BandGraba Then
    '                If grdProductosOT.Rows.Count = 0 Then
    '                    MyTablaRefac.Clear()
    '                    BandNoExisteProdEnGrid = True
    '                End If
    '                'Determinar si el producto existe en el grid, si no existe hay que agregarlo
    '                If BandNoExisteProdEnGrid Then
    '                    Salida = True
    '                    myDataRow = MyTablaRefac.NewRow()
    '                    '
    '                    myDataRow("trefCIDPRODUCTO") = vCIDPRODUCTO
    '                    myDataRow("trefCIDUNIDADBASE") = vCIDUNIDAD

    '                    myDataRow("trefCCODIGOP01_PRO") = txtIdProducto.Text
    '                    myDataRow("trefCNOMBREP01") = txtNomProducto.Text

    '                    'Calcula Existencia
    '                    vExistencia = ObjCom.Existencia(vCIDPRODUCTO, Now, vIdAlmacen)
    '                    myDataRow("trefExistencia") = vExistencia

    '                    myDataRow("trefCantidad") = Val(txtCantidad.Text)
    '                    myDataRow("trefCCLAVEINT") = txtIdUnidadProd.Text
    '                    myDataRow("trefBComprar") = IIf(vExistencia < Val(txtCantidad.Text), True, False)

    '                    'Calcula Costo
    '                    vCosto = ObjCom.ChecaCosto(vCIDPRODUCTO, Now, vIdAlmacen)
    '                    myDataRow("trefCOSTO") = vCosto

    '                    'Cantidades
    '                    'Por Comprar y Salidas
    '                    If vExistencia >= Val(txtCantidad.Text) Then
    '                        CantCompra = 0
    '                        CantSalida = Val(txtCantidad.Text)
    '                    Else
    '                        CantCompra = Val(txtCantidad.Text) - vExistencia
    '                        CantSalida = vExistencia
    '                    End If
    '                    myDataRow("trefCantCompra") = CantCompra
    '                    myDataRow("trefCantSalida") = CantSalida

    '                    MyTablaRefac.Rows.Add(myDataRow)

    '                    'Grabamos Producto
    '                    GrabaProductoLOC(txtIdProducto.Text)
    '                    GrabaUnidadLOC(vCIDUNIDAD)


    '                    InsertaRegGrid()

    '                    BandNoExisteProdEnGrid = False
    '                    Salida = False
    '                Else
    '                    For i = 0 To MyTablaRefac.DefaultView.Count - 1
    '                        BandNoExisteProdEnGrid = True
    '                        If Trim(txtIdProducto.Text) = Trim(MyTablaRefac.DefaultView.Item(i).Item("trefCCODIGOP01_PRO").ToString) And Trim(txtIdUnidadProd.Text) = Trim(grdProductosOT.Item("grefCCLAVEINT", i).Value) Then
    '                            MyTablaRefac.DefaultView.Item(i).Item("trefCIDPRODUCTO") = vCIDPRODUCTO
    '                            MyTablaRefac.DefaultView.Item(i).Item("trefCIDUNIDADBASE") = vCIDUNIDAD

    '                            MyTablaRefac.DefaultView.Item(i).Item("trefCCODIGOP01_PRO") = txtIdProducto.Text
    '                            MyTablaRefac.DefaultView.Item(i).Item("trefCNOMBREP01") = txtNomProducto.Text
    '                            'Calcula Existencia
    '                            vExistencia = ObjCom.Existencia(vCIDPRODUCTO, Now, vIdAlmacen)
    '                            MyTablaRefac.DefaultView.Item(i).Item("trefExistencia") = vExistencia

    '                            MyTablaRefac.DefaultView.Item(i).Item("trefCantidad") = Val(txtCantidad.Text)
    '                            MyTablaRefac.DefaultView.Item(i).Item("trefCCLAVEINT") = txtIdUnidadProd.Text

    '                            MyTablaRefac.DefaultView.Item(i).Item("trefBComprar") = IIf(vExistencia < Val(txtCantidad.Text), True, False)

    '                            'Calcula Costo
    '                            vCosto = ObjCom.ChecaCosto(vCIDPRODUCTO, Now, vIdAlmacen)
    '                            MyTablaRefac.DefaultView.Item(i).Item("trefCOSTO") = vCosto

    '                            If vExistencia >= Val(txtCantidad.Text) Then
    '                                CantCompra = 0
    '                                CantSalida = Val(txtCantidad.Text)
    '                            Else
    '                                CantCompra = Val(txtCantidad.Text) - vExistencia
    '                                CantSalida = vExistencia
    '                            End If
    '                            MyTablaRefac.DefaultView.Item(i).Item("trefCantCompra") = CantCompra
    '                            MyTablaRefac.DefaultView.Item(i).Item("trefCantSalida") = CantSalida

    '                            'Grabamos Producto
    '                            GrabaProductoLOC(txtIdProducto.Text)
    '                            GrabaUnidadLOC(vCIDUNIDAD)

    '                            BandNoExisteProdEnGrid = False
    '                            Exit For
    '                        End If
    '                    Next
    '                    If BandNoExisteProdEnGrid Then
    '                        Salida = True
    '                        myDataRow = MyTablaRefac.NewRow()
    '                        myDataRow("trefCIDPRODUCTO") = vCIDPRODUCTO
    '                        myDataRow("trefCIDUNIDADBASE") = vCIDUNIDAD

    '                        myDataRow("trefCCODIGOP01_PRO") = txtIdProducto.Text
    '                        myDataRow("trefCNOMBREP01") = txtNomProducto.Text
    '                        'Calcula Existencia
    '                        vExistencia = ObjCom.Existencia(vCIDPRODUCTO, Now, vIdAlmacen)
    '                        myDataRow("trefExistencia") = vExistencia

    '                        myDataRow("trefCantidad") = Val(txtCantidad.Text)
    '                        myDataRow("trefCCLAVEINT") = txtIdUnidadProd.Text

    '                        myDataRow("trefBComprar") = IIf(vExistencia < Val(txtCantidad.Text), True, False)

    '                        'Calcula Costo
    '                        vCosto = ObjCom.ChecaCosto(vCIDPRODUCTO, Now, vIdAlmacen)
    '                        myDataRow("trefCOSTO") = vCosto

    '                        If vExistencia >= Val(txtCantidad.Text) Then
    '                            CantCompra = 0
    '                            CantSalida = Val(txtCantidad.Text)
    '                        Else
    '                            CantCompra = Val(txtCantidad.Text) - vExistencia
    '                            CantSalida = vExistencia
    '                        End If
    '                        myDataRow("trefCantCompra") = CantCompra
    '                        myDataRow("trefCantSalida") = CantSalida



    '                        MyTablaRefac.Rows.Add(myDataRow)

    '                        'Grabamos Producto
    '                        GrabaProductoLOC(txtIdProducto.Text)
    '                        GrabaUnidadLOC(vCIDUNIDAD)
    '                    End If
    '                    InsertaRegGrid()
    '                End If
    '            End If
    '        Else
    '            txtIdProducto.Enabled = True
    '        End If
    '        'ActualizaTotales()
    '    Catch ex As Exception
    '        MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, Me.Text)
    '    End Try
    'End Sub

    'Private Function InsertaRegGrid() As Boolean
    '    Try

    '        grdProductosOT.Rows.Clear()

    '        For i = 0 To MyTablaRefac.DefaultView.Count - 1
    '            grdProductosOT.Rows.Add()
    '            'Consecutivo
    '            grdProductosOT.Item("grefConsecutivo", i).Value = i + 1
    '            grdProductosOT.Item("grefCCODIGOP01_PRO", i).Value = Trim(MyTablaRefac.DefaultView.Item(i).Item("trefCCODIGOP01_PRO").ToString)
    '            grdProductosOT.Item("grefCNOMBREP01", i).Value = Trim(MyTablaRefac.DefaultView.Item(i).Item("trefCNOMBREP01").ToString)
    '            grdProductosOT.Item("grefExistencia", i).Value = CDbl(MyTablaRefac.DefaultView.Item(i).Item("trefExistencia"))
    '            grdProductosOT.Item("grefCantidad", i).Value = CDbl(MyTablaRefac.DefaultView.Item(i).Item("trefCantidad"))
    '            grdProductosOT.Item("grefCCLAVEINT", i).Value = Trim(MyTablaRefac.DefaultView.Item(i).Item("trefCCLAVEINT").ToString)
    '            grdProductosOT.Item("grefBComprar", i).Value = MyTablaRefac.DefaultView.Item(i).Item("trefBComprar")
    '            grdProductosOT.Item("grefCOSTO", i).Value = CDbl(MyTablaRefac.DefaultView.Item(i).Item("trefCOSTO"))

    '            grdProductosOT.Item("grefCIDPRODUCTO", i).Value = CInt(MyTablaRefac.DefaultView.Item(i).Item("trefCIDPRODUCTO"))
    '            grdProductosOT.Item("grefCIDUNIDADBASE", i).Value = CInt(MyTablaRefac.DefaultView.Item(i).Item("trefCIDUNIDADBASE"))

    '            grdProductosOT.Item("grefCantCompra", i).Value = CDbl(MyTablaRefac.DefaultView.Item(i).Item("trefCantCompra"))
    '            grdProductosOT.Item("grefCantSalida", i).Value = CDbl(MyTablaRefac.DefaultView.Item(i).Item("trefCantSalida"))
    '        Next
    '        'grdVentas.Columns("Precio").DefaultCellStyle.Format = "c"
    '        grdProductosOT.PerformLayout()
    '        'grdVentas.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
    '        Return True
    '    Catch ex As Exception
    '        Return False
    '        MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, Me.Text)
    '    End Try

    'End Function

    'Private Sub GrabaProductoLOC(ByVal vCCODIGOPRODUCTO As String)
    '    'Grabamos Producto LOCAL
    '    ProdCom = New ProductosComClass(vCCODIGOPRODUCTO, "")
    '    If ProdCom.Existe Then
    '        'Existe en Comercial
    '        ProdCom.Guardar(False, vCCODIGOPRODUCTO, txtNomProducto.Text, ProdCom.CTIPOPRODUCTO, ProdCom.CSTATUSPRODUCTO, ProdCom.CIDUNIDADBASE, ProdCom.CCODALTERN)

    '    End If

    'End Sub

    'Private Sub GrabaUnidadLOC(ByVal vCIDUNIDAD As Integer)
    '    'Grabamos Producto LOCAL
    '    'UnidadCom = New UnidadesComClass(vCCODIGOPRODUC)
    '    UnidadCom = New UnidadesComClass(vCIDUNIDAD, "CIDUNIDAD", TipoDato.TdNumerico)
    '    If UnidadCom.Existe Then
    '        'Existe en Comercial
    '        UnidadCom.Guardar(False, txtNomUniProd.Text, UnidadCom.CABREVIATURA, UnidadCom.CDESPLIEGUE, UnidadCom.CCLAVEINT)

    '    End If

    'End Sub

    'Private Sub BorraCamposProd()
    '    txtIdProducto.Text = ""
    '    txtNomProducto.Text = ""
    '    txtIdUnidadProd.Text = ""
    '    txtNomUniProd.Text = ""
    '    txtCantidad.Text = ""

    'End Sub

    'Private Sub txtDuracion_KeyPress(sender As Object, e As KeyPressEventArgs)
    '    If txtDuracion.Text.Length = 0 Then
    '        txtDuracion.Focus()
    '        txtDuracion.SelectAll()
    '    Else
    '        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
    '            Salida = True
    '            tabPrinc.SelectedIndex = 2
    '            txtIdProducto.Focus()
    '            txtIdProducto.SelectAll()
    '            Salida = False
    '        End If
    '    End If
    'End Sub

    Private Sub Cancelar()

        LimpiaCampos()
        ActivaBotones(False, TipoOpcActivaBoton.tOpcInicializa)

        'ActivaCampos(False, TipoOpcActivaCampos.tOpcDESHABTODOS)

        ActivaCampos(False, TipoOpcActivaCampos.tOpcINICIALIZA)



        ToolStripMenu.Enabled = True
        'GroupBox2.Enabled = True
        'tabPrinc.SelectedIndex = 0
        'DsComboMovimiento.Tables("MGW10006").DefaultView.RowFilter = Nothing
        'chkAyu1.Checked = False
        'chkAyu2.Checked = False
        'ActivaAyudantes(chkAyu1.Checked, TipoNumAyudante.Ayudante1)
        'ActivaAyudantes(chkAyu2.Checked, TipoNumAyudante.Ayudante2)
        indice = 0
        indiceOS = 0
        indiceOT = 0



        grdOrdenServCab.DataSource = Nothing
        grdOrdenServCab.Rows.Clear()



        grdOrdenServDet.DataSource = Nothing
        grdOrdenServDet.Rows.Clear()


        MyTablaDetalle.Clear()

        If _MultiEmpresa Then
            cmbIdEmpresa.Enabled = True
        Else
            cmbIdEmpresa.Enabled = False
        End If
        cmbIdEmpresa.SelectedValue = _cveEmpresa


        Salida = True
        'txtOrdenTrabajo.Enabled = True
        'txtOrdenTrabajo.Focus()
        'Salida = False


    End Sub

    Private Sub ActivaBotones(ByVal Valor As Boolean, Optional ByVal vOpcion As TipoOpcActivaBoton = TipoOpcActivaBoton.tOpcInicializa)
        'Si es Verdadero Se activan Ok,Cancelar y Buscar y Terminar Se desactiva
        If vOpcion = TipoOpcActivaBoton.tOpcConsulta Then

            btnMnuOk.Enabled = False
            btnMnuLimpiar.Enabled = True
            btnMnuCancelar.Enabled = False
            btnMenuReImprimir.Enabled = True
            btnMnuSalir.Enabled = False
        ElseIf vOpcion = TipoOpcActivaBoton.tOpcInicializa Then
            'btnMnuOk.Enabled = Valor
            'btnMnuLimpiar.Enabled = Valor
            'btnMnuCancelar.Enabled = Valor
            'btnMnuSalir.Enabled = Not Valor

            btnMnuOk.Enabled = Valor
            btnMnuLimpiar.Enabled = Valor
            btnMnuCancelar.Enabled = Valor
            btnMenuReImprimir.Enabled = Valor
            btnMnuSalir.Enabled = Not Valor


        ElseIf vOpcion = TipoOpcActivaBoton.tOpcInsertar Then
            btnMnuOk.Enabled = True
            btnMenuReImprimir.Enabled = False
            btnMnuLimpiar.Enabled = True
            btnMnuCancelar.Enabled = False
            btnMnuSalir.Enabled = False

        ElseIf vOpcion = TipoOpcActivaBoton.tOpcEditar Then
            btnMnuOk.Enabled = Not Valor
            btnMnuLimpiar.Enabled = Valor
            btnMnuCancelar.Enabled = Valor
            btnMenuReImprimir.Enabled = Valor
            btnMnuSalir.Enabled = Not Valor

        End If
    End Sub

    Private Sub ActivaCampos(ByVal valor As Boolean, ByVal vOpcion As TipoOpcActivaCampos)
        txtEstatus.Enabled = False
        txtMotivoCancela.Enabled = False
        If vOpcion = TipoOpcActivaCampos.tOpcDESHABTODOS Then
            dtpFechaCap.Enabled = False
            dtpFechaCapturaHora.Enabled = False

        ElseIf vOpcion = TipoOpcActivaCampos.tOpcINICIALIZA Then

            dtpFechaCap.Enabled = False
            dtpFechaCapturaHora.Enabled = False

            txtTractor.Enabled = True
            txtRemolque1.Enabled = True
            txtDolly.Enabled = True
            txtRemolque2.Enabled = True
            cmbChofer.Enabled = True
            'cmbAutoriza.Enabled = True





        ElseIf vOpcion = TipoOpcActivaCampos.tOpcDEPENDEVALOR Then
            txtTractor.Enabled = valor
            txtRemolque1.Enabled = valor
            txtDolly.Enabled = valor
            txtRemolque2.Enabled = valor
            cmbChofer.Enabled = valor
            'cmbAutoriza.Enabled = valor
            '10/ENE/2017
            'txtKilometraje.Enabled = valor

            '6 / ENERO / 2017
            'txtTractor.Enabled = True
            'txtRemolque1.Enabled = True
            'txtDolly.Enabled = True
            'txtRemolque2.Enabled = True
            'cmbChofer.Enabled = True
            'cmbAutoriza.Enabled = valor

            '10/ENE/2017
            'txtKilometraje.Enabled = valor
        End If
    End Sub
    'Private Sub ActivaCamposProductos(ByVal valor As Boolean)
    '    txtIdProducto.Enabled = valor
    '    txtNomProducto.Enabled = valor
    '    txtIdUnidadProd.Enabled = valor
    '    txtNomUniProd.Enabled = valor
    '    txtCantidad.Enabled = valor
    'End Sub

    Private Sub txtIdProducto_TextChanged(sender As Object, e As EventArgs)

    End Sub

    Private Sub txtIdUnidadProd_TextChanged(sender As Object, e As EventArgs)

    End Sub

    Private Sub btnMnuOk_Click(sender As Object, e As EventArgs) Handles btnMnuOk.Click
        Dim bCabPreOC As Boolean = False
        Dim bCabPreSal As Boolean = False
        Dim objsql As New ToolSQLs
        Dim vIdCLAVE As Integer
        Dim vidOrdTrab As Integer
        Dim vNewidOrdTrab As Integer
        Dim vidOrdServ As Integer
        Dim vUnidadTrans As String
        Dim vidTipOrdServ As Integer
        Dim vidTipServ As Integer
        Dim vbOrdenTrabajo As Boolean

        Dim vidServicio As Integer
        Dim vidActividad As Integer
        Dim vDurHora As Integer
        Dim vidDivision As Integer
        Dim vNotaRecepcion As String
        Dim vidOrdenActividad As Integer

        Dim Mensaje As String = ""

        Try
            Salida = True
            indice = 0
            indiceOS = 0
            indiceOT = 0
            vidOrdTrab = 0
            If Not Validar() Then Exit Sub

            If Val(txtidPadreOrdSer.Text) > 0 Then
                Mensaje = "!! Esta seguro que desea Modificar las Ordenes de Servicio con FOLIO: " & Val(txtidPadreOrdSer.Text) & "? !!"
            Else
                Mensaje = "!! Esta seguro que desea Grabar las Ordenes de Servicio: ? !!"
            End If

            If MsgBox(Mensaje, MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then

                If Val(txtidPadreOrdSer.Text) > 0 Then
                    AgregaTODOSDET()

                    'Actualiza Fecha de Ordenes de Servicio
                    For c = 0 To grdOrdenServCab.Rows.Count - 1
                        vIdCLAVE = grdOrdenServCab.Item("oscCLAVE", c).Value
                        vbOrdenTrabajo = grdOrdenServCab.Item("oscbOrdenTrabajo", c).Value

                        ReDim Preserve ArrayOrdenSer(indiceOS)
                        ArrayOrdenSer(indiceOS) = vIdCLAVE

                        ReDim Preserve ArrayOrdenSerTipo(indiceOS)
                        ArrayOrdenSerTipo(indiceOS) = vbOrdenTrabajo
                        indiceOS += 1

                        StrSql = objsql.ActualizaCampoTabla("CabOrdenServicio", "idOrdenSer", TipoDato.TdNumerico, vIdCLAVE,
                        "fechaCaptura", TipoDato.TdFechaHora, FormatFecHora(Now, True, True))
                        ReDim Preserve ArraySql(indice)
                        ArraySql(indice) = StrSql
                        indice += 1

                        'Detalle de Orden Servicio
                        'Borrar todas
                        For d = 0 To grdOrdenServDet.Rows.Count - 1
                            If vIdCLAVE = grdOrdenServDet.Item("osdCLAVE", d).Value Then
                                If grdOrdenServDet.Item("osdModificado", d).Value Then

                                    vidServicio = grdOrdenServDet.Item("osdidServicio", d).Value
                                    vidActividad = grdOrdenServDet.Item("osdidActividad", d).Value
                                    vDurHora = grdOrdenServDet.Item("osdDuracionActHr", d).Value
                                    vidDivision = grdOrdenServDet.Item("osdidDivision", d).Value
                                    vNotaRecepcion = grdOrdenServDet.Item("osdNomServicio", d).Value
                                    vidOrdenActividad = grdOrdenServDet.Item("osdidOrdenActividad", d).Value
                                    vidOrdTrab = grdOrdenServDet.Item("osdidOrdenTrabajo", d).Value

                                    idLlanta = grdOrdenServDet.Item("osdidLlanta", d).Value
                                    PosLLanta = grdOrdenServDet.Item("osdPosicion", d).Value
                                    'Cancelar el Actual
                                    'StrSql = objsql.ActualizaCampoTabla("DetOrdenServicio", "idOrdenActividad", TipoDato.TdNumerico, vidOrdenActividad,
                                    '"Estatus", TipoDato.TdCadena, "CAN")

                                    StrSql = "DELETE dbo.DetOrdenServicio WHERE idOrdenActividad = " & vidOrdenActividad
                                    ReDim Preserve ArraySql(indice)
                                    ArraySql(indice) = StrSql
                                    indice += 1

                                    If vidOrdTrab > 0 Then
                                        'Cancelamos actual
                                        StrSql = objsql.ActualizaCampoTabla("DetRecepcionOS", "idOrdenTrabajo", TipoDato.TdNumerico, vidOrdTrab,
                                        "isCancelado", TipoDato.TdBoolean, 1)
                                        ReDim Preserve ArraySql(indice)
                                        ArraySql(indice) = StrSql
                                        indice += 1

                                        StrSql = objsql.ActualizaCampoTabla("DetRecepcionOS", "idOrdenTrabajo", TipoDato.TdNumerico, vidOrdTrab,
                                        "motivoCancelacion", TipoDato.TdCadena, "DESDE SISTEMA")
                                        ReDim Preserve ArraySql(indice)
                                        ArraySql(indice) = StrSql
                                        indice += 1
                                        'Generamos nuevo
                                        If d = 0 Or vNewidOrdTrab = 0 Then
                                            vNewidOrdTrab = UltimoOrdenTrab()
                                        Else
                                            vNewidOrdTrab = vidOrdTrab + 1
                                        End If

                                        ReDim Preserve ArrayOrdenTrab(indiceOT)
                                        ArrayOrdenTrab(indiceOT) = vNewidOrdTrab
                                        indiceOT += 1

                                        StrSql = objsql.InsertaOrdenTrabajo(vNewidOrdTrab, vidOrdServ, 0, vNotaRecepcion, 0, vidDivision, 0, 0, 0)
                                        ReDim Preserve ArraySql(indice)
                                        ArraySql(indice) = StrSql
                                        indice += 1
                                    End If

                                    StrSql = objsql.InsertaOrdenServicioDET(vIdCLAVE, vidServicio,
                                    vidActividad, vDurHora, 0, 0, "PRE", 0, 0, vNewidOrdTrab, vidDivision, idLlanta, PosLLanta)
                                    ReDim Preserve ArraySql(indice)
                                    ArraySql(indice) = StrSql
                                    indice += 1
                                    'aqui()
                                End If
                            End If


                            'If vIdCLAVE = grdOrdenServDet.Item("osdCLAVE", d).Value Then

                            '    'Trabajos
                            '    If vbOrdenTrabajo Then
                            '        If d = 0 Or vidOrdTrab = 0 Then
                            '            vidOrdTrab = UltimoOrdenTrab()
                            '        Else
                            '            vidOrdTrab = vidOrdTrab + 1
                            '        End If

                            '        ReDim Preserve ArrayOrdenTrab(indiceOT)
                            '        ArrayOrdenTrab(indiceOT) = vidOrdTrab
                            '        indiceOT += 1

                            '        StrSql = objsql.InsertaOrdenTrabajo(vidOrdTrab, vidOrdServ, 0, vNotaRecepcion, 0, vidDivision, 0, 0, 0)
                            '        ReDim Preserve ArraySql(indice)
                            '        ArraySql(indice) = StrSql
                            '        indice += 1
                            '    Else
                            '        vidOrdTrab = 0
                            '    End If
                            '    'Detalle - checar los ceros
                            '    StrSql = objsql.InsertaOrdenServicioDET(vidOrdServ, vidServicio,
                            '    vidActividad, vDurHora, 0, 0, "PRE", 0, 0, vidOrdTrab, vidDivision)
                            '    ReDim Preserve ArraySql(indice)
                            '    ArraySql(indice) = StrSql
                            '    indice += 1

                            'End If
                        Next

                    Next
                    'Se actualiza Parametros
                    'StrSql = objsql.ActualizaCampoTabla("Parametros", "idOrdenSer", TipoDato.TdNumerico, vIdCLAVE,
                    '"IdParametro", TipoDato.TdNumerico, "1")
                    'ReDim Preserve ArraySql(indice)
                    'ArraySql(indice) = StrSql
                    'indice += 1

                    StrSql = objsql.ActualizaCampoTabla("Parametros", "idOrdenTrabajo", TipoDato.TdNumerico, vidOrdTrab,
                    "IdParametro", TipoDato.TdNumerico, "1")
                    ReDim Preserve ArraySql(indice)
                    ArraySql(indice) = StrSql
                    indice += 1

                Else
                    '1.- Grabar MasterOrdServ y Obtener Consecutivo para poder continuar
                    '2- Recorrer Grid de Cabeceros para insertar Ordenes de servicio
                    AgregaTODOSDET()
                    vidPadreOrdSer = UltimoMaster()
                    If vidPadreOrdSer > 0 Then
                        'Insertar Padre
                        StrSql = objsql.InsertaPadre(vidPadreOrdSer, txtTractor.Text, txtRemolque1.Text, txtDolly.Text,
                        txtRemolque2.Text, cmbChofer.SelectedValue, FormatFecHora(dtpFechaCap.Value, True, True), _Usuario, "CAP")
                        ReDim Preserve ArraySql(indice)
                        ArraySql(indice) = StrSql
                        indice += 1

                        'Ordenes de Servicio
                        For c = 0 To grdOrdenServCab.Rows.Count - 1
                            vIdCLAVE = grdOrdenServCab.Item("oscCLAVE", c).Value
                            vUnidadTrans = grdOrdenServCab.Item("oscidUnidadTrans", c).Value
                            vidTipOrdServ = grdOrdenServCab.Item("oscTipoOrdenServ", c).Value
                            vidTipServ = grdOrdenServCab.Item("oscTipoServ", c).Value
                            vbOrdenTrabajo = grdOrdenServCab.Item("oscbOrdenTrabajo", c).Value

                            If c = 0 Then
                                vidOrdServ = UltimoOrdServ()
                            Else
                                vidOrdServ = vidOrdServ + 1
                            End If

                            If vidOrdServ > 0 Then
                                ReDim Preserve ArrayOrdenSer(indiceOS)
                                ArrayOrdenSer(indiceOS) = vidOrdServ

                                ReDim Preserve ArrayOrdenSerTipo(indiceOS)
                                ArrayOrdenSerTipo(indiceOS) = vbOrdenTrabajo
                                indiceOS += 1


                                'Insertamos Cabecero
                                'StrSql = objsql.InsertaOrdenServicio(vidOrdServ, _cveEmpresa, vUnidadTrans, vidTipOrdServ,
                                'Val(txtKilometraje.Text), "PRE", cmbAutoriza.SelectedValue,
                                'FormatFecHora(dtpFechaCap.Value, True), vidTipOrdServ, vidTipServ, vidPadreOrdSer)

                                '10/ENE/2017
                                StrSql = objsql.InsertaOrdenServicio(vidOrdServ, _cveEmpresa, vUnidadTrans, vidTipOrdServ,
                                0, "PRE", cmbAutoriza.SelectedValue,
                                FormatFecHora(dtpFechaCap.Value, True, True), vidTipOrdServ, vidTipServ, vidPadreOrdSer)


                                ReDim Preserve ArraySql(indice)
                                ArraySql(indice) = StrSql
                                indice += 1

                                For d = 0 To grdOrdenServDet.Rows.Count - 1
                                    vidServicio = grdOrdenServDet.Item("osdidServicio", d).Value
                                    vidActividad = grdOrdenServDet.Item("osdidActividad", d).Value
                                    vDurHora = grdOrdenServDet.Item("osdDuracionActHr", d).Value
                                    vidDivision = grdOrdenServDet.Item("osdidDivision", d).Value
                                    vNotaRecepcion = grdOrdenServDet.Item("osdNombreAct", d).Value

                                    idLlanta = grdOrdenServDet.Item("osdidLlanta", d).Value
                                    PosLLanta = grdOrdenServDet.Item("osdPosicion", d).Value

                                    If vIdCLAVE = grdOrdenServDet.Item("osdCLAVE", d).Value Then

                                        'Trabajos
                                        If vbOrdenTrabajo Then
                                            If d = 0 Or vidOrdTrab = 0 Then
                                                vidOrdTrab = UltimoOrdenTrab()
                                            Else
                                                vidOrdTrab = vidOrdTrab + 1
                                            End If

                                            ReDim Preserve ArrayOrdenTrab(indiceOT)
                                            ArrayOrdenTrab(indiceOT) = vidOrdTrab
                                            indiceOT += 1

                                            StrSql = objsql.InsertaOrdenTrabajo(vidOrdTrab, vidOrdServ, 0, vNotaRecepcion, 0, vidDivision, 0, 0, 0)
                                            ReDim Preserve ArraySql(indice)
                                            ArraySql(indice) = StrSql
                                            indice += 1
                                        Else
                                            vidOrdTrab = 0
                                        End If
                                        'Detalle - checar los ceros
                                        StrSql = objsql.InsertaOrdenServicioDET(vidOrdServ, vidServicio,
                                        vidActividad, vDurHora, 0, 0, "PRE", 0, 0, vidOrdTrab, vidDivision, idLlanta, PosLLanta)
                                        ReDim Preserve ArraySql(indice)
                                        ArraySql(indice) = StrSql
                                        indice += 1

                                        'Else
                                        'If vidServicio > 0 Or vidActividad > 0 Then
                                        '    StrSql = objsql.InsertaOrdenServicioDET(vidOrdServ, vidServicio,
                                        '    vidActividad, vDurHora, 0, 0, "PRE", 0, 0, vidOrdTrab)
                                        '    ReDim Preserve ArraySql(indice)
                                        '    ArraySql(indice) = StrSql
                                        '    indice += 1
                                        'End If
                                    End If
                                Next

                            End If
                        Next

                        'Se actualiza Parametros
                        StrSql = objsql.ActualizaCampoTabla("Parametros", "idOrdenSer", TipoDato.TdNumerico, vidOrdServ,
                        "IdParametro", TipoDato.TdNumerico, "1")
                        ReDim Preserve ArraySql(indice)
                        ArraySql(indice) = StrSql
                        indice += 1

                        StrSql = objsql.ActualizaCampoTabla("Parametros", "idOrdenTrabajo", TipoDato.TdNumerico, vidOrdTrab,
                        "IdParametro", TipoDato.TdNumerico, "1")
                        ReDim Preserve ArraySql(indice)
                        ArraySql(indice) = StrSql
                        indice += 1

                    End If
                End If




                If indice > 0 Then
                    Dim obj As New CapaNegocio.Tablas
                    Dim Respuesta As String
                    Dim sOrdenes As String = ""
                    Dim idOS As Integer = 0
                    Dim TipidOS As Boolean = False

                    Respuesta = obj.EjecutarSql(Inicio.CONSTR, "SQL", ArraySql, Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, indice)
                    FEspera.Close()
                    If Respuesta = "EFECTUADO" Then
                        For Each Res In ArrayOrdenSer
                            sOrdenes = sOrdenes & Res & ","
                            'ImprimeOrdenServicio(270, True)
                        Next
                        sOrdenes = Mid(sOrdenes, 1, sOrdenes.Length - 1)
                        MsgBox("Se crearon la(s) Orden de Servicio(s) :" & sOrdenes, MsgBoxStyle.Exclamation, Me.Text)

                        If SetupThePrinting() Then
                            For imp = 0 To ArrayOrdenSer.Length - 1
                                idOS = ArrayOrdenSer(imp)
                                TipidOS = ArrayOrdenSerTipo(imp)
                                ImprimeOrdenServicio(idOS, TipidOS, True, False)
                            Next
                        End If

                        'MsgBox("Orden de Trabajo Actualizado Satisfactoriamente" & vbCrLf & _
                        'IIf(NoIdPreOC > 0, "Se Creo la El Folio de Pre Orden de Compra # " & NoIdPreOC, "") & vbCrLf & _
                        'IIf(NoIdPreSal > 0, "Se Creo la El Folio de Pre Salida de ALmacen # " & NoIdPreSal, ""), MsgBoxStyle.Exclamation, Me.Text)

                        'If OpcionForma = TipoOpcionForma.tOpcModificar Then
                        '    MsgBox("Orden de Trabajo Actualizado Satisfactoriamente" & vbCrLf & _
                        '    IIf(NoIdPreOC > 0, "Se Creo la El Folio de Pre Orden de Compra # " & NoIdPreOC, "") & vbCrLf & _
                        '    IIf(NoIdPreSal > 0, "Se Creo la El Folio de Pre Salida de ALmacen # " & NoIdPreSal, ""), MsgBoxStyle.Exclamation, Me.Text)
                        '    'ElseIf OpcionForma = TipoOpcionForma.tOpcEliminar Then
                        '    '    MsgBox("Grupo eliminado con exito!!!", MsgBoxStyle.Exclamation, Me.Text)
                        '    'ElseIf OpcionForma = TipoOpcionForma.tOpcInsertar Then
                        '    'MsgBox("Actualizacion efectuada satisfactoriamente", MsgBoxStyle.Exclamation, Me.Text)
                        'End If
                    ElseIf Respuesta = "NOEFECTUADO" Then

                    ElseIf Respuesta = "ERROR" Then
                        indice = 0
                        SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")
                    End If
                Else
                    MsgBox("No se Encontraron Registros que Grabar", MsgBoxStyle.Exclamation, Me.Text)
                End If
                Cancelar()
            End If
            Salida = False
        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try
    End Sub

#Region "IMPRESION"
    Public Sub ImprimeOrdenServicio(ByVal idOrdenSer As Integer, ByVal bOrdenTrab As Boolean,
    ByVal BandPreview As Boolean,
    ByVal vbanHorizontal As Boolean, Optional ByVal nombreImpresora As String = "")
        Dim objSql As New ToolSQLs


        ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 10)
        ImpRep.FuenteImprimeCampos2 = New Font("Free 3 of 9 Extended", 40)
        ImpRep.FuenteImprimeCampos3 = New System.Drawing.Font("Courier New", 10, FontStyle.Bold)
        ImpRep.FuenteImprimeCampos4 = New System.Drawing.Font("Courier New", 7, FontStyle.Bold)
        ImpRep.FuenteImprimeCampos5 = New System.Drawing.Font("Courier New", 10, FontStyle.Underline)

        'PATH_FONTS = "C:\Windows\Fonts\"
        'Dim pfc As PrivateFontCollection = New PrivateFontCollection()
        ''FRE3OF9X_0
        ''pfc.AddFontFile(PATH_FONTS & "BARCOD39.TTF")
        'pfc.AddFontFile(PATH_FONTS & "FREE3OF9_2.TTF")
        'Dim FontFam As FontFamily = pfc.Families(0)
        'ImpRep.FuenteImprimeCampos2 = New Font(FontFam, 40)



        'BARCOD39
        ImpRep.Constr = Inicio.CONSTR
        ImpRep.TipoConexion = TipoConexion.TcSQL
        ImpRep.NomTabla = "ORDSERV"


        If bOrdenTrab Then
            ImpRep.StrSql = objSql.ImpOrdServConOT(idOrdenSer)
        Else
            ImpRep.StrSql = objSql.ImpOrdServSinOT(idOrdenSer)
        End If

        NumRegistros = ImpRep.LlenaDataSet
        If NumRegistros > 0 Then
            ArmaTablaImpresionTicket(bOrdenTrab)
            ArmaTablaImpresion(bOrdenTrab)

            ImpRep.TablaImprime = MyTablaImprime

            'If prtSettings Is Nothing Then
            '    prtSettings = New PrinterSettings
            'End If

            If prtDoc Is Nothing Then
                prtDoc = New PrintDocument
                AddHandler prtDoc.PrintPage, AddressOf prt_PrintPage
            End If

            'If ExisteColaImp = "" Then
            'ojoooo ExisteColaImp = LlenaDatos("1", "ColaImpresion", "IdParametro", TipoDato.TdNumerico, , )

            'End If

            If ExisteColaImp = KEY_RCORRECTO Then
                If StrColaImpRecibo <> "" Then
                    prtSettings.PrinterName = StrColaImpRecibo
                    MsgBox(StrColaImpRecibo, MsgBoxStyle.Information, Me.Text)
                End If
            End If

            'If StrColaImpresion = "" Then
            '    'MsgBox("No Hay Cola de Impresi�n, la Hoja de Surtido No se mando a Imprimir", MsgBoxStyle.Information, "Impresion de surtido")
            '    Exit Sub
            'Else
            '    ImpGlobal.PrinterSettings.PrinterName = strCola
            'End If

            If prtSettings Is Nothing Then
                prtSettings = New PrinterSettings
            End If

            prtDoc.PrinterSettings = prtSettings
            prtDoc.DefaultPageSettings = prtSettings.DefaultPageSettings

            prtDoc.DefaultPageSettings.Landscape = vbanHorizontal
            prtDoc.DefaultPageSettings.PaperSize = MyPaperSize


            If BandPreview Then
                Dim prtPrev As New PrintPreviewDialog
                prtPrev.Document = prtDoc
                prtPrev.Text = "Previsualizar datos de " & Me.Text
                prtPrev.WindowState = FormWindowState.Maximized
                prtPrev.Document.DefaultPageSettings.Landscape = vbanHorizontal
                'prtPrev.PrintPreviewControl.Zoom = 1
                If vbanHorizontal Then
                    prtPrev.PrintPreviewControl.Zoom = 1
                Else
                    prtPrev.PrintPreviewControl.Zoom = 1
                End If

                'prtPrev.MdiParent = Me
                prtPrev.ShowDialog()
            Else
                'prtDoc.Print()
                'Dim printDialog1 = New PrintDialog()
                'printDialog1.PrinterSettings.Copies = 1
                'Dim result = printDialog1.ShowDialog()

                If nombreImpresora <> "" Then
                    prtDoc.PrinterSettings.PrinterName = nombreImpresora
                End If
                '
                prtDoc.Print()
            End If



        End If


    End Sub

    Private Sub prt_PrintPage(ByVal sender As Object, ByVal e As PrintPageEventArgs)
        ' Este evento se produce cada vez que se va a imprimir una p�gina
        Try
            'lineaActual = 0

            Dim yPos As Single = e.MarginBounds.Top
            'Margen Izquierdo
            Dim leftMargin As Single = e.MarginBounds.Left
            'Fuentes
            'Altura de la Linea
            Dim lineHeight As Single = printFont.GetHeight(e.Graphics)

            'Dim vFuenteImprimeCampos = New System.Drawing.Font("Courier New", 7)
            'Dim vFuenteImprimeCampos2 = New System.Drawing.Font("Courier New", 7)

            'If _cveEmpresa = 1 Then

            'PARA TICKETS ..... O J O
            'Dim bmp As New Bitmap(My.Resources.logo_ciltra_transparente1)
            'ImpRep.Imagen1 = New Bitmap(bmp, bmp.Width / 1, bmp.Height / 1)
            'ImpRep.PosicionImagen1 = New Point(40, 0)
            Dim bmp As New Bitmap(My.Resources.logo_ciltra_transparente1)
            ImpRep.Imagen1 = New Bitmap(bmp, bmp.Width / 1, bmp.Height / 1)
            ImpRep.PosicionImagen1 = New Point(40, 0)


            'End If

            Do
                yPos += lineHeight
                'Aqui se imprime el dato

                yPos = ImpRep.ImprimeLineaRecibo(e, lineaActual)

                lineaActual += 1

            Loop Until yPos >= e.MarginBounds.Bottom - 20 _
                OrElse lineaActual >= NumRegistros


            ContPaginas += 1
            'DS.Tables(vNomTabla).DefaultView.Count



            If lineaActual < NumRegistros Then
                e.HasMorePages = True
            Else
                ContPaginas = 0
                e.HasMorePages = False
            End If
        Catch ex As Exception
            MsgBox(ex.Message, Me.Text)
        End Try

    End Sub


    '8/ENE/2018
    'Private Sub ArmaTablaImpresion(ByVal bOrdenTrab As Boolean)
    '    Dim campo As Integer
    '    Dim Descripcion As String
    '    Dim Valor As String
    '    Dim PosValorX, PosValorY, PosDescX, PosDescY As Single
    '    Dim NumAletra As New NumALetras
    '    Dim EnLetras As String = ""
    '    Dim Total As Double = 0

    '    Dim Y1 As Single = 150

    '    'Horizontal
    '    'Dim X1 As Single = 20
    '    'Dim X1A As Single = 120
    '    'Dim X1B As Single = 200
    '    'Dim X2 As Single = 320
    '    'Dim X3 As Single = 440
    '    'Dim X3A As Single = 540
    '    'Dim X4 As Single = 620

    '    'Vertical
    '    'Dim X1 As Single = 20
    '    'Dim X1A As Single = 120
    '    'Dim X1B As Single = 200
    '    'Dim X2 As Single = 160
    '    'Dim X3 As Single = 220
    '    'Dim X3A As Single = 260
    '    'Dim X4 As Single = 400

    '    Dim X1 As Single = 20
    '    Dim X1A As Single = 80
    '    Dim X2 As Single = 150

    '    Dim X1B As Single = X1 + 25


    '    Dim ValPad As Integer = 12

    '    DsRep = New DataSet
    '    'MyTablaImprime = New DataTable("IMPRIME")
    '    If MyTablaImprime.Rows.Count > 0 Then
    '        MyTablaImprime.Rows.Clear()
    '    End If


    '    DsRep = ImpRep.LlenaDsImprime(ImpRep.NomTabla)
    '    If DsRep.Tables(ImpRep.NomTabla).DefaultView.Count > 0 Then

    '        For i As Integer = 0 To DsRep.Tables(ImpRep.NomTabla).DefaultView.Count - 1

    '            'LINEA 1
    '            campo += 1
    '            'Descripcion = "FECHA DE PAGO:"
    '            'Valor = CDate(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("FechaTrans"))
    '            Descripcion = ""
    '            Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("RazonSocial"))
    '            PosDescX = X2 : PosDescY = Y1 : PosValorX = X2 : PosValorY = PosDescY
    '            InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

    '            'CODIGO DE BARRAS ORDEN SERVICIO
    '            campo += 1
    '            Descripcion = ""
    '            'PosDescX = X4 : PosValorX = X4
    '            PosDescY = PosDescY + 30
    '            PosDescX = X2 : PosValorX = X2
    '            PosValorY = PosDescY
    '            Valor = FormarBarCode(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idOrdenSer"))
    '            InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 2)

    '            'LINEA 2
    '            campo += 1
    '            PosDescX = X2
    '            PosDescY = PosDescY + 60
    '            'PosValorX = PosDescX + 100
    '            PosValorX = X2 + 35
    '            PosValorY = PosDescY
    '            Descripcion = ""
    '            'Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idOrdenSer")
    '            Valor = "ORDEN DE SERVICIO"
    '            InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

    '            'LINEA 3
    '            campo += 1
    '            Descripcion = ""
    '            PosDescY = PosDescY + 15 : PosValorY = PosDescY
    '            PosDescX = X2 : PosValorX = X2 + 40
    '            Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("NomTipOrdServ"))
    '            InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)

    '            'LINEA 4
    '            campo += 1
    '            Descripcion = "ORD.SER."
    '            PosDescY = PosDescY + 15 : PosValorY = PosDescY
    '            'PosDescX = X4 : PosValorX = X4 + 80
    '            PosDescX = X2 : PosValorX = X2 + 80

    '            Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idOrdenSer")
    '            InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)



    '            'LINEA 5
    '            campo += 1
    '            Descripcion = "FECHA:"
    '            Valor = CDate(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("fechaCaptura")).ToShortDateString
    '            PosDescY = PosDescY + 15 : PosValorY = PosDescY
    '            'PosDescX = X4 : PosValorX = PosDescX + 50
    '            PosDescX = X2 : PosValorX = PosDescX + 50
    '            InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

    '            'campo += 1
    '            'PosDescY = PosDescY + 30 : PosValorY = PosDescY
    '            'Descripcion = "DIA: "
    '            'Valor = CDate(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("fechaCaptura")).Day
    '            'PosDescX = X1 : PosValorX = PosDescX + 35
    '            'InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

    '            'campo += 1
    '            'Descripcion = "MES:"
    '            'Valor = CDate(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("fechaCaptura")).Month
    '            'PosDescX = X2 : PosValorX = PosDescX + 35
    '            'InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

    '            'campo += 1
    '            'Descripcion = "A�O:"
    '            'Valor = CDate(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("fechaCaptura")).Year
    '            'PosDescX = X3 : PosValorX = PosDescX + 35
    '            'InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

    '            'LINEA 5.1
    '            campo += 1
    '            Descripcion = "HORA:"
    '            Valor = CDate(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("fechaCaptura")).ToShortTimeString
    '            PosDescY = PosDescY + 15 : PosValorY = PosDescY
    '            'PosDescX = X4 : PosValorX = PosDescX + 40
    '            PosDescX = X2 : PosValorX = PosDescX + 40
    '            InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)


    '            'LINEA 6
    '            campo += 1
    '            PosDescY = PosDescY + 30 : PosValorY = PosDescY
    '            Descripcion = "TRACTOR: "
    '            Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idTractor"))
    '            PosDescX = X1 : PosValorX = PosDescX + 70
    '            If DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idTractor") = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idUnidadTrans") Then
    '                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
    '            Else
    '                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)
    '            End If


    '            campo += 1
    '            Descripcion = "REMOLQUE 1:"
    '            Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idTolva1"))
    '            PosDescX = X2 : PosValorX = PosDescX + 95
    '            If DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idTolva1") = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idUnidadTrans") Then
    '                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
    '            Else
    '                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)
    '            End If



    '            'LINEA 7
    '            campo += 1
    '            PosDescY = PosDescY + 15 : PosValorY = PosDescY
    '            Descripcion = "DOLLY: "
    '            Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idDolly"))
    '            PosDescX = X1 : PosValorX = PosDescX + 50
    '            If DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idDolly") = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idUnidadTrans") Then
    '                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
    '            Else
    '                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)
    '            End If


    '            campo += 1
    '            Descripcion = "REMOLQUE 2:"
    '            Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idTolva2"))
    '            PosDescX = X2 : PosValorX = PosDescX + 95
    '            If DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idTolva2") = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idUnidadTrans") Then
    '                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
    '            Else
    '                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)
    '            End If


    '            'LINEA 8
    '            'campo += 1
    '            'PosDescY = PosDescY + 15 : PosValorY = PosDescY
    '            'Descripcion = "KILOMETRAJE: "
    '            'Valor = Format(CInt(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("Kilometraje")), "##,##0")
    '            'PosDescX = X1 : PosValorX = PosDescX + 110
    '            'InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)


    '            'LINEA 9
    '            campo += 1
    '            PosDescY = PosDescY + 30 : PosValorY = PosDescY
    '            Descripcion = ""
    '            If bOrdenTrab Then
    '                Valor = "REPARACIONES SOLICITADAS"
    '            Else
    '                Valor = "DESCRIPCION DEL SERVICIO"
    '            End If
    '            PosDescX = X2 : PosValorX = PosDescX + 25
    '            InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)
    '            PosDescY = PosDescY + 15 : PosValorY = PosDescY

    '            'LINEA 10
    '            'DETALLE
    '            For j As Integer = 0 To DsRep.Tables(ImpRep.NomTabla).DefaultView.Count - 1
    '                If bOrdenTrab Then
    '                    If DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NomServicio") <> "" Then
    '                        campo += 1
    '                        PosDescY = PosDescY + 15 : PosValorY = PosDescY
    '                        Descripcion = ""
    '                        Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NomServicio")
    '                        PosDescX = X1 : PosValorX = PosDescX + 25
    '                        InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
    '                    Else
    '                        '12/JULIO/2017
    '                        'If DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NombreAct") <> "" Then
    '                        '    campo += 1
    '                        '    PosDescY = PosDescY + 15 : PosValorY = PosDescY
    '                        '    Descripcion = ""
    '                        '    Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NombreAct")
    '                        '    PosDescX = X1 : PosValorX = PosDescX + 25
    '                        '    InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
    '                        'Else
    '                        '    'salto de linea
    '                        '    PosDescY = PosDescY + 15 : PosValorY = PosDescY
    '                        'End If
    '                    End If

    '                    campo += 1
    '                    'PosDescY = PosDescY + 30 : PosValorY = PosDescY
    '                    Descripcion = ""
    '                    Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NotaRecepcion")
    '                    PosDescY = PosDescY + 15 : PosValorY = PosDescY
    '                    PosDescX = X2 : PosValorX = PosDescX + 25
    '                    InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
    '                Else
    '                    campo += 1
    '                    PosDescY = PosDescY + 15 : PosValorY = PosDescY
    '                    Descripcion = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("idUnidadTrans")
    '                    Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NomServicio")
    '                    PosDescX = X2 - 35 : PosValorX = X2 + 25
    '                    InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
    '                    'PosDescY = PosDescY + 15

    '                End If
    '            Next




    '            'LINEA 11
    '            campo += 1
    '            PosDescY = PosDescY + 290 : PosValorY = PosDescY
    '            Descripcion = ""
    '            Valor = "___________________"
    '            PosDescX = X1 : PosValorX = PosDescX + 25
    '            InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
    '            'PosDescY = PosDescY + 15

    '            campo += 1
    '            Descripcion = ""
    '            Valor = "___________________"
    '            'PosDescX = X3A : PosValorX = PosDescX + 25
    '            PosDescX = X1 : PosValorX = PosDescX + 25
    '            InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
    '            'PosDescY = PosDescY + 15

    '            'LINEA 12
    '            campo += 1
    '            PosDescY = PosDescY + 15 : PosValorY = PosDescY
    '            Descripcion = ""
    '            Valor = " ( OPERADOR ) "
    '            PosDescX = X1 : PosValorX = PosDescX + 35
    '            InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
    '            'PosDescY = PosDescY + 15

    '            campo += 1
    '            Descripcion = ""
    '            Valor = " ( AUTORIZ� ) "
    '            'PosDescX = X3A : PosValorX = PosDescX + 35
    '            PosDescX = X1 : PosValorX = PosDescX + 35
    '            InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
    '            'PosDescY = PosDescY + 15

    '            'LINEA 13
    '            campo += 1
    '            PosDescY = PosDescY + 15 : PosValorY = PosDescY
    '            Descripcion = ""
    '            Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("NomOperador"))
    '            PosDescX = X1 : PosValorX = PosDescX + 25
    '            InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 4)
    '            'PosDescY = PosDescY + 15

    '            campo += 1
    '            Descripcion = ""
    '            Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("NomAutoriza"))
    '            'PosDescX = X3A : PosValorX = PosDescX + 25
    '            PosDescX = X1 : PosValorX = PosDescX + 25
    '            InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 4)
    '            'PosDescY = PosDescY + 15

    '            'LINEA 14
    '            'CODIGO DE BARRAS ORDEN SERVICIO
    '            campo += 1
    '            Descripcion = ""
    '            'PosDescY = PosDescY + 15 : PosValorY = PosDescY
    '            PosDescY = PosDescY + 75 : PosValorY = PosDescY
    '            'PosDescX = X4 : PosValorX = X4
    '            PosDescX = X2 : PosValorX = X2
    '            Valor = FormarBarCode(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idPadreOrdSer"))
    '            InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 2)

    '            'LINEA 15
    '            campo += 1
    '            Descripcion = "FOLIO: "
    '            PosDescY = PosDescY + 45 : PosValorY = PosDescY
    '            'PosDescX = X4 : PosValorX = X4 + 60
    '            PosDescX = X2 : PosValorX = X2 + 60
    '            Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idPadreOrdSer")
    '            InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)

    '            'LINEA 14
    '            'If idContrato = 0 Then
    '            '    '4 LINEAS
    '            '    PosDescY = PosDescY + 60
    '            'End If

    '            'campo += 1
    '            'Descripcion = "RECIBO NO."
    '            'PosDescX = X1 : PosDescY = PosDescY + 15 : PosValorX = PosDescX + 100 : PosValorY = PosDescY
    '            'Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("NUMRECIBO")
    '            'InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY)
    '        Next
    '    End If
    'End Sub
    Private Sub ArmaTablaImpresion(ByVal bOrdenTrab As Boolean)
        Dim campo As Integer
        Dim Descripcion As String
        Dim Valor As String
        Dim PosValorX, PosValorY, PosDescX, PosDescY As Single
        Dim NumAletra As New NumALetras
        Dim EnLetras As String = ""
        Dim Total As Double = 0

        Dim Y1 As Single = 150

        'Horizontal
        'Dim X1 As Single = 20
        'Dim X1A As Single = 120
        'Dim X1B As Single = 200
        'Dim X2 As Single = 320
        'Dim X3 As Single = 440
        'Dim X3A As Single = 540
        'Dim X4 As Single = 620

        'Vertical
        Dim X1 As Single = 20
        Dim X1A As Single = 120
        Dim X1B As Single = 200
        Dim X2 As Single = 160
        Dim X3 As Single = 220
        Dim X3A As Single = 260
        Dim X4 As Single = 400

        Dim ValPad As Integer = 12

        DsRep = New DataSet
        'MyTablaImprime = New DataTable("IMPRIME")
        If MyTablaImprime.Rows.Count > 0 Then
            MyTablaImprime.Rows.Clear()
        End If


        DsRep = ImpRep.LlenaDsImprime(ImpRep.NomTabla)
        If DsRep.Tables(ImpRep.NomTabla).DefaultView.Count > 0 Then

            For i As Integer = 0 To DsRep.Tables(ImpRep.NomTabla).DefaultView.Count - 1

                'LINEA 1
                campo += 1
                'Descripcion = "FECHA DE PAGO:"
                'Valor = CDate(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("FechaTrans"))
                Descripcion = ""
                Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("RazonSocial"))
                PosDescX = X2 : PosDescY = Y1 : PosValorX = X2 : PosValorY = PosDescY
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

                'CODIGO DE BARRAS ORDEN SERVICIO
                campo += 1
                Descripcion = ""
                'PosDescY = PosDescY + 15 : PosValorY = PosDescY
                PosDescX = X4 : PosValorX = X4
                'Valor = FormarBarCode(8)
                Valor = FormarBarCode(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idOrdenSer"))
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 2)

                'LINEA 2
                campo += 1
                PosDescX = X2
                PosDescY = PosDescY + 15
                'PosValorX = PosDescX + 100
                PosValorX = X2 + 35
                PosValorY = PosDescY
                Descripcion = ""
                'Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idOrdenSer")
                Valor = "ORDEN DE SERVICIO"
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

                'LINEA 3
                campo += 1
                Descripcion = ""
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                PosDescX = X2 : PosValorX = X2 + 40
                Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("NomTipOrdServ"))
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)

                'LINEA 4
                campo += 1
                Descripcion = "ORD.SER."
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                PosDescX = X4 : PosValorX = X4 + 80
                'Valor = FormarBarCode(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idOrdenSer"))
                Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idOrdenSer")
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)



                'LINEA 5
                campo += 1
                Descripcion = "FECHA:"
                Valor = CDate(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("fechaCaptura")).ToShortDateString
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                PosDescX = X4 : PosValorX = PosDescX + 50
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

                'campo += 1
                'PosDescY = PosDescY + 30 : PosValorY = PosDescY
                'Descripcion = "DIA: "
                'Valor = CDate(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("fechaCaptura")).Day
                'PosDescX = X1 : PosValorX = PosDescX + 35
                'InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

                'campo += 1
                'Descripcion = "MES:"
                'Valor = CDate(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("fechaCaptura")).Month
                'PosDescX = X2 : PosValorX = PosDescX + 35
                'InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

                'campo += 1
                'Descripcion = "A�O:"
                'Valor = CDate(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("fechaCaptura")).Year
                'PosDescX = X3 : PosValorX = PosDescX + 35
                'InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

                'LINEA 5.1
                campo += 1
                Descripcion = "HORA:"
                Valor = CDate(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("fechaCaptura")).ToShortTimeString
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                PosDescX = X4 : PosValorX = PosDescX + 40
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)


                'LINEA 6
                campo += 1
                PosDescY = PosDescY + 30 : PosValorY = PosDescY
                Descripcion = "TRACTOR: "
                Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idTractor"))
                PosDescX = X1 : PosValorX = PosDescX + 70
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

                campo += 1
                Descripcion = "REMOLQUE 1:"
                Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idTolva1"))
                PosDescX = X2 : PosValorX = PosDescX + 95
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)


                'LINEA 7
                campo += 1
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                Descripcion = "DOLLY: "
                Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idDolly"))
                PosDescX = X1 : PosValorX = PosDescX + 50
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

                campo += 1
                Descripcion = "REMOLQUE 2:"
                Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idTolva2"))
                PosDescX = X2 : PosValorX = PosDescX + 95
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

                'LINEA 8
                'campo += 1
                'PosDescY = PosDescY + 15 : PosValorY = PosDescY
                'Descripcion = "KILOMETRAJE: "
                'Valor = Format(CInt(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("Kilometraje")), "##,##0")
                'PosDescX = X1 : PosValorX = PosDescX + 110
                'InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)


                'LINEA 9
                campo += 1
                PosDescY = PosDescY + 30 : PosValorY = PosDescY
                Descripcion = ""
                If bOrdenTrab Then
                    Valor = "REPARACIONES SOLICITADAS"
                Else
                    Valor = "DESCRIPCION DEL SERVICIO"
                End If
                PosDescX = X2 : PosValorX = PosDescX + 25
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)
                PosDescY = PosDescY + 15 : PosValorY = PosDescY

                'LINEA 10
                'DETALLE
                For j As Integer = 0 To DsRep.Tables(ImpRep.NomTabla).DefaultView.Count - 1
                    If bOrdenTrab Then
                        If DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NomServicio") <> "" Then
                            campo += 1
                            PosDescY = PosDescY + 15 : PosValorY = PosDescY
                            Descripcion = ""
                            Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NomServicio")
                            PosDescX = X1 : PosValorX = PosDescX + 25
                            InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                        Else
                            If DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NombreAct") <> "" Then
                                campo += 1
                                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                                Descripcion = ""
                                Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NombreAct")
                                PosDescX = X1 : PosValorX = PosDescX + 25
                                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                            Else
                                'salto de linea
                                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                            End If
                        End If

                        campo += 1
                        'PosDescY = PosDescY + 30 : PosValorY = PosDescY
                        Descripcion = ""
                        Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NotaRecepcion")
                        PosDescX = X2 : PosValorX = PosDescX + 25
                        InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                    Else
                        campo += 1
                        PosDescY = PosDescY + 15 : PosValorY = PosDescY
                        Descripcion = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("idUnidadTrans")
                        Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NomServicio")
                        PosDescX = X2 - 35 : PosValorX = X2 + 25
                        InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                        'PosDescY = PosDescY + 15

                    End If
                Next




                'LINEA 11
                campo += 1
                PosDescY = PosDescY + 290 : PosValorY = PosDescY
                Descripcion = ""
                Valor = "___________________"
                PosDescX = X1 : PosValorX = PosDescX + 25
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                'PosDescY = PosDescY + 15

                campo += 1
                Descripcion = ""
                Valor = "___________________"
                PosDescX = X3A : PosValorX = PosDescX + 25
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                'PosDescY = PosDescY + 15

                'LINEA 12
                campo += 1
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                Descripcion = ""
                Valor = " ( OPERADOR ) "
                PosDescX = X1 : PosValorX = PosDescX + 35
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                'PosDescY = PosDescY + 15

                campo += 1
                Descripcion = ""
                Valor = " ( AUTORIZ� ) "
                PosDescX = X3A : PosValorX = PosDescX + 35
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                'PosDescY = PosDescY + 15

                'LINEA 13
                campo += 1
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                Descripcion = ""
                Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("NomOperador"))
                PosDescX = X1 : PosValorX = PosDescX + 25
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                'PosDescY = PosDescY + 15

                campo += 1
                Descripcion = ""
                Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("NomAutoriza"))
                PosDescX = X3A : PosValorX = PosDescX + 25
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                'PosDescY = PosDescY + 15

                'LINEA 14
                'CODIGO DE BARRAS ORDEN SERVICIO
                campo += 1
                Descripcion = ""
                'PosDescY = PosDescY + 15 : PosValorY = PosDescY
                PosDescY = PosDescY + 75 : PosValorY = PosDescY
                PosDescX = X4 : PosValorX = X4
                Valor = FormarBarCode(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idPadreOrdSer"))
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 2)

                'LINEA 15
                campo += 1
                Descripcion = "FOLIO: "
                PosDescY = PosDescY + 45 : PosValorY = PosDescY
                PosDescX = X4 : PosValorX = X4 + 60
                Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idPadreOrdSer")
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)

                'LINEA 14
                'If idContrato = 0 Then
                '    '4 LINEAS
                '    PosDescY = PosDescY + 60
                'End If

                'campo += 1
                'Descripcion = "RECIBO NO."
                'PosDescX = X1 : PosDescY = PosDescY + 15 : PosValorX = PosDescX + 100 : PosValorY = PosDescY
                'Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("NUMRECIBO")
                'InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY)
            Next
        End If
    End Sub

    Private Sub ArmaTablaImpresionTicket(ByVal bOrdenTrab As Boolean)
        Dim campo As Integer
        Dim Descripcion As String
        Dim Valor As String
        Dim PosValorX, PosValorY, PosDescX, PosDescY As Single
        Dim NumAletra As New NumALetras
        Dim EnLetras As String = ""
        Dim Total As Double = 0

        Dim Y1 As Single = 200

        'Vertical
        Dim X1 As Single = 20
        Dim X1A As Single = 80
        Dim X2 As Single = 150

        Dim X1B As Single = X1 + 25

        Dim BandImpServ = False
        'Dim XSERV = 20
        Dim XSERV As Single = 0
        Dim XACT As Single = 80

        Dim XCODBAR As Single = 100

        'Dim X1A As Single = 120
        'Dim X1B As Single = 200
        'Dim X3 As Single = 220
        '
        'Dim X4 As Single = 400

        Dim ValPad As Integer = 12

        DsRep = New DataSet
        If MyTablaImprime.Rows.Count > 0 Then
            MyTablaImprime.Rows.Clear()
        End If


        DsRep = ImpRep.LlenaDsImprime(ImpRep.NomTabla)
        If DsRep.Tables(ImpRep.NomTabla).DefaultView.Count > 0 Then

            For i As Integer = 0 To DsRep.Tables(ImpRep.NomTabla).DefaultView.Count - 1

                'LINEA 1
                campo += 1
                Descripcion = ""
                Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("RazonSocial"))
                PosDescX = X1A : PosDescY = Y1 : PosValorX = PosDescX : PosValorY = PosDescY
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

                'LINEA 2
                campo += 1
                PosDescX = X1A : PosValorX = PosDescX
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                Descripcion = ""
                Valor = "ORDEN DE SERVICIO"
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

                'LINEA 3
                campo += 1
                Descripcion = ""
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                PosDescX = X1A : PosValorX = PosDescX
                Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("NomTipOrdServ"))
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)


                'CODIGO DE BARRAS ORDEN SERVICIO
                campo += 1
                Descripcion = ""
                PosDescY = PosDescY + 30 : PosValorY = PosDescY
                PosDescX = XCODBAR : PosValorX = PosDescX
                'Valor = FormarBarCode(8)
                Valor = FormarBarCode(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idOrdenSer"))
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 2)

                'LINEA 4
                campo += 1
                Descripcion = "ORD.SER."
                PosDescY = PosDescY + 45 : PosValorY = PosDescY
                PosDescX = X2 : PosValorX = PosDescX + 80
                'Valor = FormarBarCode(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idOrdenSer"))
                Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idOrdenSer")
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)



                'LINEA 5
                campo += 1
                Descripcion = "FECHA:"
                Valor = CDate(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("fechaCaptura")).ToShortDateString
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                PosDescX = PosDescX : PosValorX = PosDescX + 50
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)


                'LINEA 5.1
                campo += 1
                Descripcion = "HORA:"
                Valor = CDate(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("fechaCaptura")).ToShortTimeString
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                PosDescX = PosDescX : PosValorX = PosDescX + 40
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)


                'LINEA 6
                campo += 1
                PosDescY = PosDescY + 30 : PosValorY = PosDescY
                Descripcion = "TRACTOR: "
                Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idTractor"))
                PosDescX = X1 : PosValorX = PosDescX + 70
                If DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idTractor") = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idUnidadTrans") Then
                    InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                Else
                    InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)
                End If


                campo += 1
                Descripcion = "REMOLQUE 1:"
                Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idTolva1"))
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                PosDescX = X1 : PosValorX = PosDescX + 95
                If DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idTolva1") = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idUnidadTrans") Then
                    InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                Else
                    InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)
                End If



                'LINEA 7
                campo += 1
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                Descripcion = "DOLLY: "
                Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idDolly"))
                PosDescX = X1 : PosValorX = PosDescX + 50
                If DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idDolly") = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idUnidadTrans") Then
                    InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                Else
                    InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)
                End If


                campo += 1
                Descripcion = "REMOLQUE 2:"
                Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idTolva2"))
                PosDescX = X1 : PosValorX = PosDescX + 95
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                If DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idTolva2") = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idUnidadTrans") Then
                    InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                Else
                    InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)
                End If


                'LINEA 8
                'campo += 1
                'PosDescY = PosDescY + 15 : PosValorY = PosDescY
                'Descripcion = "KILOMETRAJE: "
                'Valor = Format(CInt(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("Kilometraje")), "##,##0")
                'PosDescX = X1 : PosValorX = PosDescX + 110
                'InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)


                'LINEA 9
                campo += 1
                PosDescY = PosDescY + 30 : PosValorY = PosDescY
                Descripcion = ""
                'If bOrdenTrab Then
                '    Valor = "REPARACIONES SOLICITADAS"
                'Else
                '    Valor = "DESCRIPCION DEL SERVICIO"
                'End If
                Valor = "DESCRIPCION DEL SERVICIO"
                PosDescX = X1 : PosValorX = PosDescX + 25
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 5)

                PosDescY = PosDescY + 30 : PosValorY = PosDescY

                'LINEA 10
                'DETALLE
                For j As Integer = 0 To DsRep.Tables(ImpRep.NomTabla).DefaultView.Count - 1
                    If bOrdenTrab Then
                        If DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NomServicio") <> "" Then
                            If Not BandImpServ Then
                                campo += 1
                                Descripcion = ""
                                Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NomServicio")
                                PosDescX = XSERV : PosValorX = PosDescX + 25
                                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                                BandImpServ = True

                            End If
                        Else
                            BandImpServ = False
                            '12/JULIO/2017
                            'If DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NombreAct") <> "" Then
                            '    campo += 1
                            '    PosDescY = PosDescY + 15 : PosValorY = PosDescY
                            '    Descripcion = ""
                            '    Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NombreAct")
                            '    PosDescX = X1 : PosValorX = PosDescX + 25
                            '    InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                            'Else
                            '    'salto de linea
                            '    PosDescY = PosDescY + 15 : PosValorY = PosDescY
                            'End If
                        End If

                        campo += 1
                        'PosDescY = PosDescY + 30 : PosValorY = PosDescY
                        Descripcion = ""
                        Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NotaRecepcion")
                        PosDescY = PosDescY + 15 : PosValorY = PosDescY

                        If BandImpServ Then
                            PosDescX = XACT : PosValorX = PosDescX + 25
                            InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)
                        Else
                            PosDescX = XSERV : PosValorX = PosDescX + 25
                            InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                        End If



                    Else
                        campo += 1

                        PosDescY = PosDescY + 15 : PosValorY = PosDescY
                        Descripcion = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("idUnidadTrans")
                        Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NomServicio")
                        PosDescX = X1 - 35 : PosValorX = X2 + 25
                        InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                        'PosDescY = PosDescY + 15

                    End If
                Next




                'LINEA 11
                campo += 1
                Descripcion = ""
                Valor = "___________________"
                PosDescY = PosDescY + 100 : PosValorY = PosDescY
                PosDescX = X1 : PosValorX = PosDescX + 25
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                'PosDescY = PosDescY + 15

                campo += 1
                Descripcion = ""
                Valor = " ( OPERADOR ) "
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                PosDescX = X1 : PosValorX = PosDescX + 35
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)

                campo += 1
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                Descripcion = ""
                Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("NomOperador"))
                PosDescX = X1 : PosValorX = PosDescX + 25
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 4)



                campo += 1
                Descripcion = ""
                Valor = "___________________"
                PosDescY = PosDescY + 100 : PosValorY = PosDescY
                PosDescX = X1 : PosValorX = PosDescX + 25
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)

                campo += 1
                Descripcion = ""
                Valor = " ( AUTORIZ� ) "
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                PosDescX = X1 : PosValorX = PosDescX + 35
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                'LINEA 13

                campo += 1
                Descripcion = ""
                Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("NomAutoriza"))
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                PosDescX = X1 : PosValorX = PosDescX + 25
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 4)
                'PosDescY = PosDescY + 15

                'LINEA 14
                'CODIGO DE BARRAS ORDEN SERVICIO
                campo += 1
                Descripcion = ""
                'PosDescY = PosDescY + 15 : PosValorY = PosDescY
                PosDescY = PosDescY + 100 : PosValorY = PosDescY
                PosDescX = XCODBAR : PosValorX = XCODBAR
                Valor = FormarBarCode(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idPadreOrdSer"))
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 2)

                'LINEA 15
                campo += 1
                Descripcion = "FOLIO: "
                PosDescY = PosDescY + 45 : PosValorY = PosDescY
                PosDescX = X2 : PosValorX = X2 + 60
                Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idPadreOrdSer")
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)

                'LINEA 14
                'If idContrato = 0 Then
                '    '4 LINEAS
                '    PosDescY = PosDescY + 60
                'End If

                'campo += 1
                'Descripcion = "RECIBO NO."
                'PosDescX = X1 : PosDescY = PosDescY + 15 : PosValorX = PosDescX + 100 : PosValorY = PosDescY
                'Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("NUMRECIBO")
                'InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY)
            Next
        End If
    End Sub
    Private Function FormarBarCode(ByVal Codigo As String) As String

        Dim Barcode As String = vbEmpty
        'Barcode = Format("*{0}*", Codigo)
        If Len(Codigo) = 1 Then
            Barcode = "*00" & Codigo & "*"
        ElseIf Len(Codigo) = 2 Then
            Barcode = "*0" & Codigo & "*"
        ElseIf Len(Codigo) >= 3 Then
            Barcode = "*" & Codigo & "*"
        End If

        Return Barcode
    End Function


    Public Sub CreaTablaImprime()
        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Int32")
        myDataColumn.ColumnName = "Campo"
        myDataColumn.ReadOnly = False
        myDataColumn.Unique = False
        MyTablaImprime.Columns.Add(myDataColumn)


        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.String")
        myDataColumn.ColumnName = "Descripcion"
        myDataColumn.ReadOnly = False
        myDataColumn.Unique = False
        MyTablaImprime.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.String")
        myDataColumn.ColumnName = "Valor"
        myDataColumn.ReadOnly = False
        myDataColumn.Unique = False
        MyTablaImprime.Columns.Add(myDataColumn)


        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Single")
        myDataColumn.ColumnName = "PosDescX"
        myDataColumn.ReadOnly = False
        myDataColumn.Unique = False
        MyTablaImprime.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Single")
        myDataColumn.ColumnName = "PosDescY"
        myDataColumn.ReadOnly = False
        myDataColumn.Unique = False
        MyTablaImprime.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Single")
        myDataColumn.ColumnName = "PosValX"
        myDataColumn.ReadOnly = False
        myDataColumn.Unique = False
        MyTablaImprime.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Single")
        myDataColumn.ColumnName = "PosValY"
        myDataColumn.ReadOnly = False
        myDataColumn.Unique = False
        MyTablaImprime.Columns.Add(myDataColumn)


        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Single")
        myDataColumn.ColumnName = "Fuente"
        myDataColumn.ReadOnly = False
        myDataColumn.Unique = False
        MyTablaImprime.Columns.Add(myDataColumn)



    End Sub
    Private Sub InsertaRegistro(ByVal Campo As Integer, ByVal Descripcion As String, ByVal Valor As String, ByVal PosDescX As Single,
                           ByVal PosDescY As Single, ByVal PosValX As Single, ByVal PosValY As Single, ByVal vFuente As String)

        myDataRow = MyTablaImprime.NewRow()
        myDataRow("Campo") = Campo
        myDataRow("Descripcion") = Descripcion
        myDataRow("Valor") = Valor
        myDataRow("PosDescX") = PosDescX
        myDataRow("PosDescY") = PosDescY
        myDataRow("PosValX") = PosValX
        myDataRow("PosValY") = PosValY
        myDataRow("Fuente") = vFuente
        MyTablaImprime.Rows.Add(myDataRow)

    End Sub
#End Region


    Private Function Validar() As Boolean
        Validar = True

        '10/ENE/2017
        'If Not Inicio.ValidandoCampo(Trim(txtKilometraje.Text), TipoDato.TdNumerico) Then
        '    MsgBox("El Kilometraje no puede ser Cero o Vac�o", vbInformation, "Aviso" & Me.Text)
        '    If txtKilometraje.Enabled Then
        '        txtKilometraje.Focus()
        '        txtKilometraje.SelectAll()
        '    End If
        '    Validar = False
        'Else
        If txtTractor.Text = "" Then
            MsgBox("El Tractor no puede ser vac�o", vbInformation, "Aviso" & Me.Text)
            txtTractor.Focus()
            Validar = False
            Exit Function

        Else
            UniTrans = New UnidadesTranspClass(txtTractor.Text)
            If Not UniTrans.Existe Then
                MsgBox("El Tractor: " & txtTractor.Text & " no Existe ", vbInformation, "Aviso" & Me.Text)
                txtTractor.Focus()
                Validar = False
                Exit Function
            End If
        End If

        If txtRemolque1.Text <> "" Then
            UniTrans = New UnidadesTranspClass(txtRemolque1.Text)
            If Not UniTrans.Existe Then
                MsgBox("El Remolque: " & txtRemolque1.Text & " no Existe ", vbInformation, "Aviso" & Me.Text)
                txtRemolque1.Focus()
                Validar = False
                Exit Function
            End If
        End If
        If txtDolly.Text <> "" Then
            If txtRemolque1.Text = "" Then
                MsgBox("El Remolque 1 no puede ser vac�o", vbInformation, "Aviso" & Me.Text)
                txtRemolque1.Focus()
                Validar = False
                Exit Function
            Else
                UniTrans = New UnidadesTranspClass(txtDolly.Text)
                If Not UniTrans.Existe Then
                    MsgBox("El Dolly: " & txtDolly.Text & " no Existe ", vbInformation, "Aviso" & Me.Text)
                    txtDolly.Focus()
                    Validar = False
                    Exit Function
                End If
            End If
        End If
        If txtRemolque2.Text <> "" Then
            If txtDolly.Text = "" Then
                MsgBox("El Dolly no puede ser vac�o", vbInformation, "Aviso" & Me.Text)
                txtDolly.Focus()
                Validar = False
                Exit Function
            Else
                UniTrans = New UnidadesTranspClass(txtRemolque2.Text)
                If Not UniTrans.Existe Then
                    MsgBox("El Remolque 2: " & txtRemolque2.Text & " no Existe ", vbInformation, "Aviso" & Me.Text)
                    txtRemolque2.Focus()
                    Validar = False
                    Exit Function
                End If
            End If

        End If
        If txtRemolque1.Text = txtRemolque2.Text And txtRemolque1.Text <> "" Then
            MsgBox("El Remolque 2 no puede ser igual al Remolque 1", vbInformation, "Aviso" & Me.Text)
            txtRemolque2.Focus()
            Validar = False
            Exit Function
        End If
        If grdOrdenServCab.Rows.Count = 0 Then
            MsgBox("Debe de tener al Menos un Tipo de Orden de Servicio", vbInformation, "Aviso" & Me.Text)
            chkBoxTipoOrdServ.Focus()
            Validar = False
            Exit Function
        End If
    End Function

    'Private Function CalculaNoEmpleados() As Integer
    '    If txt Then
    'End Function


    Private Sub txtCantidad_TextChanged(sender As Object, e As EventArgs)

    End Sub

    'Private Function ChecaPersonalOcupadoForma(ByVal vLugarOrigen As Integer) As Integer
    '    Dim LugarOcupado As Integer = 0
    '    'Origen = 1 -> cmbResponsable 
    '    'Origen = 2 -> cmbayudante1 
    '    'Origen = 3 -> cmbayudante2
    '    If vLugarOrigen = 1 Then
    '        If cmbayudante1.SelectedValue = cmbResponsable.SelectedValue Then
    '            LugarOcupado = 2
    '        ElseIf cmbayudante2.SelectedValue = cmbResponsable.SelectedValue Then
    '            LugarOcupado = 3
    '        End If
    '    ElseIf vLugarOrigen = 2 Then
    '        If cmbResponsable.SelectedValue = cmbayudante1.SelectedValue Then
    '            LugarOcupado = 1
    '        ElseIf cmbayudante2.SelectedValue = cmbayudante1.SelectedValue Then
    '            LugarOcupado = 3
    '        End If
    '    ElseIf vLugarOrigen = 3 Then
    '        If cmbResponsable.SelectedValue = cmbayudante2.SelectedValue Then
    '            LugarOcupado = 1
    '        ElseIf cmbayudante1.SelectedValue = cmbayudante2.SelectedValue Then
    '            LugarOcupado = 2
    '        End If

    '    End If
    '    Return LugarOcupado
    'End Function

    'Private Function ChecaPersonalOcupado(ByVal idPersonal As Integer, ByVal NomPersonal As String) As Boolean
    '    Dim Resp As Boolean = False
    '    Personal = New PersonalClass(cmbResponsable.SelectedValue)
    '    If Personal.Existe Then
    '        'aqui()
    '        MyTablaPersOcupado = Personal.TablaMecanicoOCupado("DIAG")
    '        If MyTablaPersOcupado.Rows.Count > 0 Then
    '            Resp = True
    '            With MyTablaPersOcupado.Rows(0)
    '                MsgBox("EL Personal: " & NomPersonal & " tiene Asignado actualmente la Orden de Trabajo: " & .Item("idOrdenTrabajo") & vbCrLf & "de la Orden de Servicio: " & .Item("idordenSer") & "Asigne a Otro Personal", MsgBoxStyle.Information, Me.Text)
    '            End With
    '        End If
    '    End If
    '    Return Resp
    'End Function

    'Private Sub cmbResponsable_SelectedIndexChanged(sender As Object, e As EventArgs)
    '    If Salida Then Exit Sub
    '    If cmbResponsable.SelectedValue > 0 Then
    '        vLugOcupado = ChecaPersonalOcupadoForma(1)
    '        If vLugOcupado > 0 Then
    '            MsgBox("El Personal: " & cmbResponsable.Text & " ya esta Asignado para el" & NombreLugarPuesto(vLugOcupado), MsgBoxStyle.Information, Me.Text)
    '            cmbResponsable.SelectedValue = 0
    '            cmbResponsable.Focus()
    '        Else
    '            If ChecaPersonalOcupado(cmbResponsable.SelectedValue, cmbResponsable.Text) Then
    '                'txtNombreMecanico.Text = cmbResponsable.Text
    '                'txtPuestoMec.Text = cmbPuestoResp.Text
    '            End If

    '        End If
    '    Else

    '    End If



    'End Sub

    'Private Sub chkAyu1_CheckedChanged(sender As Object, e As EventArgs)
    '    ActivaAyudantes(chkAyu1.Checked, TipoNumAyudante.Ayudante1)
    'End Sub

    'Private Sub ActivaAyudantes(ByVal Valor As Boolean, ByVal Ayudante As TipoNumAyudante)
    '    If Ayudante = TipoNumAyudante.Ayudante1 Then
    '        cmbayudante1.Enabled = Valor
    '        cmbPuestoAyu1.Enabled = Valor

    '    ElseIf Ayudante = TipoNumAyudante.Ayudante2 Then
    '        cmbayudante2.Enabled = Valor
    '        cmbPuestoAyu2.Enabled = Valor

    '    End If
    'End Sub

    'Private Sub chkAyu2_CheckedChanged(sender As Object, e As EventArgs)
    '    ActivaAyudantes(chkAyu2.Checked, TipoNumAyudante.Ayudante2)
    'End Sub


    'Private Function ContadordePersonal() As Integer
    '    Dim cont As Integer = 0
    '    If cmbPuestoResp.SelectedValue > 0 Then
    '        cont += 1
    '    End If
    '    If chkAyu1.Checked And cmbayudante1.SelectedValue > 0 Then
    '        cont += 1
    '    End If
    '    If chkAyu2.Checked And cmbayudante2.SelectedValue > 0 Then
    '        cont += 1
    '    End If

    '    Return cont
    'End Function

    'Private Function CalculaCostoManoObra(ByVal vNoHoras As Double) As Double
    '    Dim cmo As Double = 0
    '    If cmbResponsable.SelectedValue > 0 Then
    '        Personal = New PersonalClass(cmbResponsable.SelectedValue)
    '        If Personal.Existe Then
    '            cmo = vNoHoras * Personal.PrecioHora
    '        End If
    '    End If
    '    If chkAyu1.Checked And cmbayudante1.SelectedValue > 0 Then
    '        Personal = New PersonalClass(cmbayudante1.SelectedValue)
    '        If Personal.Existe Then
    '            cmo = cmo + (vNoHoras * Personal.PrecioHora)
    '        End If
    '    End If
    '    If chkAyu2.Checked And cmbayudante2.SelectedValue > 0 Then
    '        Personal = New PersonalClass(cmbayudante2.SelectedValue)
    '        If Personal.Existe Then
    '            cmo = cmo + (vNoHoras * Personal.PrecioHora)
    '        End If
    '    End If

    '    Return cmo
    'End Function

    Private Function CalculaCostoRefacciones() As Double
        Dim cre As Double = 0
        If MyTablaRefac.Rows.Count > 0 Then
            For i = 0 To MyTablaRefac.Rows.Count - 1
                cre = cre + (MyTablaRefac.DefaultView.Item(i).Item("trefCantSalida") * MyTablaRefac.DefaultView.Item(i).Item("trefCOSTO"))
            Next
        End If
        Return cre
    End Function

    Private Function NombreLugarPuesto(ByVal vLugar As Integer) As String
        Dim Resp As String = ""
        If vLugar = 1 Then
            Resp = " Responsable "
        ElseIf vLugar = 2 Then
            Resp = " Ayudante 1 "
        ElseIf vLugar = 3 Then
            Resp = " Ayudante 2 "
        End If
        Return Resp
    End Function


    Private Sub txtTractor_KeyDown(sender As Object, e As KeyEventArgs) Handles txtTractor.KeyDown
        If e.KeyCode = Keys.F3 Then
            CargaForm(txtTractor.Text, "UNITRANS", "TC", txtTractor,
" tip.TipoUso = 'T' AND UNI.ESTATUS = 'ACT' AND UNI.IDEMPRESA = " & _cveEmpresa & " AND UNI.IDUNIDADTRANS LIKE '%" & Val(txtTractor.Text) & "%'", True)

            'txtTractor.Text = DespliegaGrid(UCase("UNITRANSTIPO"), Inicio.CONSTR, "", FiltroSinWhere)
            'txtTractor_Leave(sender, e)
        End If
    End Sub

    'Private Sub cmbayudante1_SelectedIndexChanged(sender As Object, e As EventArgs)
    '    If Salida Then Exit Sub
    '    If cmbayudante1.SelectedValue > 0 Then
    '        vLugOcupado = ChecaPersonalOcupadoForma(2)
    '        If vLugOcupado > 0 Then
    '            MsgBox("El Personal: " & cmbayudante1.Text & " ya esta Asignado para el " & NombreLugarPuesto(vLugOcupado))
    '            cmbayudante1.SelectedValue = 0
    '            cmbayudante1.Focus()
    '        Else
    '            If ChecaPersonalOcupado(cmbayudante1.SelectedValue, cmbayudante1.Text) Then
    '                'txtPuestoMec.Text = cmbayudante1.Text
    '            End If

    '        End If
    '    Else

    '    End If
    'End Sub

    'Private Sub cmbayudante2_SelectedIndexChanged(sender As Object, e As EventArgs)
    '    'If Salida Then Exit Sub
    '    'If cmbayudante2.SelectedValue > 0 Then
    '    '    If ChecaPersonalOcupado(cmbayudante2.SelectedValue, cmbayudante2.Text) Then

    '    '    End If
    '    'Else

    '    'End If
    '    If Salida Then Exit Sub
    '    If cmbayudante2.SelectedValue > 0 Then
    '        vLugOcupado = ChecaPersonalOcupadoForma(3)
    '        If vLugOcupado > 0 Then
    '            MsgBox("El Personal: " & cmbayudante2.Text & " ya esta Asignado para el " & NombreLugarPuesto(vLugOcupado))
    '            cmbayudante2.SelectedValue = 0
    '            cmbayudante2.Focus()
    '        Else
    '            If ChecaPersonalOcupado(cmbayudante2.SelectedValue, cmbayudante2.Text) Then
    '                'txtPuestoMec.Text = cmbayudante2.Text
    '            End If

    '        End If
    '    Else

    '    End If
    'End Sub

    'Private Sub cmbPuestoResp_SelectedIndexChanged(sender As Object, e As EventArgs)
    '    If Salida Then Exit Sub
    '    If cmbResponsable.SelectedValue > 0 Then
    '        vLugOcupado = ChecaPersonalOcupadoForma(1)
    '        If vLugOcupado > 0 Then
    '            MsgBox("El Personal: " & cmbResponsable.Text & " ya esta Asignado para el" & NombreLugarPuesto(vLugOcupado), MsgBoxStyle.Information, Me.Text)
    '            cmbResponsable.SelectedValue = 0
    '            cmbResponsable.Focus()
    '        Else
    '            If ChecaPersonalOcupado(cmbResponsable.SelectedValue, cmbResponsable.Text) Then
    '                'txtNombreMecanico.Text = cmbResponsable.Text
    '                'txtPuestoMec.Text = cmbPuestoResp.Text
    '            End If

    '        End If
    '    Else

    '    End If
    'End Sub

    'Private Sub cmbPuestoAyu1_SelectedIndexChanged(sender As Object, e As EventArgs)
    '    If Salida Then Exit Sub
    '    If cmbayudante1.SelectedValue > 0 Then
    '        vLugOcupado = ChecaPersonalOcupadoForma(2)
    '        If vLugOcupado > 0 Then
    '            MsgBox("El Personal: " & cmbayudante1.Text & " ya esta Asignado para el " & NombreLugarPuesto(vLugOcupado))
    '            cmbayudante1.SelectedValue = 0
    '            cmbayudante1.Focus()
    '        Else
    '            If ChecaPersonalOcupado(cmbayudante1.SelectedValue, cmbayudante1.Text) Then
    '                'txtPuestoMec.Text = cmbayudante1.Text
    '            End If

    '        End If
    '    Else

    '    End If
    'End Sub

    'Private Sub cmbPuestoAyu2_SelectedIndexChanged(sender As Object, e As EventArgs)
    '    If Salida Then Exit Sub
    '    If cmbayudante2.SelectedValue > 0 Then
    '        vLugOcupado = ChecaPersonalOcupadoForma(3)
    '        If vLugOcupado > 0 Then
    '            MsgBox("El Personal: " & cmbayudante2.Text & " ya esta Asignado para el " & NombreLugarPuesto(vLugOcupado))
    '            cmbayudante2.SelectedValue = 0
    '            cmbayudante2.Focus()
    '        Else
    '            If ChecaPersonalOcupado(cmbayudante2.SelectedValue, cmbayudante2.Text) Then
    '                'txtPuestoMec.Text = cmbayudante2.Text
    '            End If

    '        End If
    '    Else

    '    End If
    'End Sub
    Private Sub VerificaFaltantes(ByVal IdUnidad As String)
        StrSql = ObjSql.VerificaFaltantes(IdUnidad)

        DtFaltantes = BD.ExecuteReturn(StrSql)
        If DtFaltantes.Rows.Count > 0 Then
            MsgBox("La Unidad " & IdUnidad & " Tiene Insumos Pendientes por surtir", MsgBoxStyle.Exclamation, Me.Text)
        End If

    End Sub

    Private Sub txtTractor_KeyUp(sender As Object, e As KeyEventArgs) Handles txtTractor.KeyUp
        If txtTractor.Text.Length = 0 Then
            txtTractor.Focus()
            txtTractor.SelectAll()
        Else
            If e.KeyValue = Keys.Enter Then
                Salida = True
                If txtTractor.Text <> "" Then
                    CargaForm(txtTractor.Text, "UNITRANS", "TC", txtTractor,
               " tip.TipoUso = 'T' AND UNI.ESTATUS = 'ACT' AND UNI.IDEMPRESA = " & _cveEmpresa & " AND UNI.IDUNIDADTRANS LIKE '%" & Val(txtTractor.Text) & "%'", True)

                    VerificaFaltantes(txtTractor.Text)
                    ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)
                    chkBoxTipoOrdServ.Enabled = True
                    txtRemolque1.Focus()
                    txtRemolque1.SelectAll()
                Else
                    txtRemolque1.Focus()
                    txtRemolque1.SelectAll()
                End If

                Salida = False
            Else
                Salida = False
            End If
        End If
    End Sub

    Private Sub txtTractor_Leave(sender As Object, e As EventArgs) Handles txtTractor.Leave
        If Salida Then Exit Sub
        'If txtTractor.Text.Length = 0 Then
        '    txtTractor.Focus()
        '    txtTractor.SelectAll()
        'Else
        If txtTractor.Enabled Then
            CargaForm(txtTractor.Text, "UNITRANS", "TC", txtTractor,
            " tip.TipoUso = 'T' AND UNI.ESTATUS = 'ACT' AND UNI.IDEMPRESA = " & _cveEmpresa &
            " AND UNI.IDUNIDADTRANS LIKE '%" & Val(txtTractor.Text) & "%'", True)
            VerificaFaltantes(txtTractor.Text)

            ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)
            chkBoxTipoOrdServ.Enabled = True
            txtRemolque1.Focus()
            txtRemolque1.SelectAll()
        End If
        'End If

    End Sub


    Private Sub txtRemolque1_KeyUp(sender As Object, e As KeyEventArgs) Handles txtRemolque1.KeyUp

        If e.KeyValue = Keys.Enter Then
            Salida = True
            'CargaForm(txtRemolque1.Text, "UNITRANS", "TV", txtRemolque1,
            '" tip.TipoUso = 'Tolva' AND UNI.ESTATUS = 'ACT' AND UNI.IDEMPRESA = " & _cveEmpresa & " AND UNI.IDUNIDADTRANS LIKE '%" & Val(txtRemolque1.Text) & "%'")
            If txtRemolque1.Text <> "" Then
                CargaForm(txtRemolque1.Text, "UNITRANS", "", txtRemolque1,
           " tip.TipoUso = 'R' AND UNI.ESTATUS = 'ACT' AND UNI.IDEMPRESA = " &
           _cveEmpresa & " AND UNI.IDUNIDADTRANS LIKE '%" & Val(txtRemolque1.Text) & "%'", False)

                VerificaFaltantes(txtRemolque1.Text)

                If BandResultado Then
                    txtDolly.Focus()
                    txtDolly.SelectAll()
                Else
                    txtRemolque1.Text = ""
                    txtRemolque1.Focus()
                    txtRemolque1.SelectAll()
                End If
            Else
                txtDolly.Focus()
                txtDolly.SelectAll()

            End If


            Salida = False
        Else
            Salida = False
        End If


    End Sub

    Private Sub txtRemolque1_Leave(sender As Object, e As EventArgs) Handles txtRemolque1.Leave
        If Salida Then Exit Sub

        If txtRemolque1.Enabled Then
            If txtRemolque1.Text <> "" Then
                CargaForm(txtRemolque1.Text, "UNITRANS", "", txtRemolque1,
            " tip.TipoUso = 'R' AND UNI.ESTATUS = 'ACT' AND UNI.IDEMPRESA = " & _cveEmpresa &
            " AND UNI.IDUNIDADTRANS LIKE '%" & Val(txtRemolque1.Text) & "%'", False)

                VerificaFaltantes(txtRemolque1.Text)

                If BandResultado Then
                    txtDolly.Focus()
                    txtDolly.SelectAll()
                Else
                    txtRemolque1.Text = ""
                    txtRemolque1.Focus()
                    txtRemolque1.SelectAll()
                End If
            End If


        End If


    End Sub

    Private Sub txtDolly_KeyUp(sender As Object, e As KeyEventArgs) Handles txtDolly.KeyUp

        If e.KeyValue = Keys.Enter Then
            Salida = True
            If txtDolly.Text <> "" Then
                CargaForm(txtDolly.Text, "UNITRANS", "DL", txtDolly,
            " tip.TipoUso = 'D' AND UNI.ESTATUS = 'ACT' AND UNI.IDEMPRESA = " & _cveEmpresa & " AND UNI.IDUNIDADTRANS LIKE '%" & Val(txtDolly.Text) & "%'")

                VerificaFaltantes(txtDolly.Text)

                If BandResultado Then
                    txtRemolque2.Focus()
                    txtRemolque2.SelectAll()
                Else
                    txtDolly.Text = ""
                    txtDolly.Focus()
                    txtDolly.SelectAll()
                End If
            Else
                txtRemolque2.Focus()
                txtRemolque2.SelectAll()
            End If
            Salida = False
        Else
            Salida = False
        End If


    End Sub

    Private Sub txtDolly_Leave(sender As Object, e As EventArgs) Handles txtDolly.Leave
        If Salida Then Exit Sub

        If txtDolly.Enabled Then
            If txtDolly.Text <> "" Then
                CargaForm(txtDolly.Text, "UNITRANS", "DL", txtDolly,
            " tip.TipoUso = 'D' AND UNI.ESTATUS = 'ACT' AND UNI.IDEMPRESA = " & _cveEmpresa &
            " AND UNI.IDUNIDADTRANS LIKE '%" & Val(txtDolly.Text) & "%'")

                VerificaFaltantes(txtDolly.Text)

                If BandResultado Then
                    txtRemolque2.Focus()
                    txtRemolque2.SelectAll()
                Else
                    txtDolly.Text = ""
                    txtDolly.Focus()
                    txtDolly.SelectAll()
                End If
            End If

        End If


    End Sub

    Private Sub txtDolly_TextChanged(sender As Object, e As EventArgs) Handles txtDolly.TextChanged

    End Sub

    Private Sub txtRemolque2_KeyUp(sender As Object, e As KeyEventArgs) Handles txtRemolque2.KeyUp

        If e.KeyValue = Keys.Enter Then
            Salida = True
            If txtRemolque2.Text <> "" Then
                CargaForm(txtRemolque2.Text, "UNITRANS", "", txtRemolque2,
            " tip.TipoUso = 'R' AND UNI.ESTATUS = 'ACT' AND UNI.IDEMPRESA = " & _cveEmpresa & " AND UNI.IDUNIDADTRANS LIKE '%" & Val(txtRemolque2.Text) & "%'")

                VerificaFaltantes(txtRemolque2.Text)

                If BandResultado Then
                    cmbChofer.Focus()
                Else
                    txtRemolque2.Text = ""
                    txtRemolque2.Focus()
                    txtRemolque2.SelectAll()
                End If
            Else
                cmbChofer.Focus()
            End If
            Salida = False
        Else
            Salida = False
        End If


    End Sub

    Private Sub txtRemolque2_Leave(sender As Object, e As EventArgs) Handles txtRemolque2.Leave
        If Salida Then Exit Sub

        If txtRemolque2.Enabled Then
            If txtRemolque2.Text <> "" Then
                CargaForm(txtRemolque2.Text, "UNITRANS", "", txtRemolque2,
           " tip.TipoUso = 'R' AND UNI.ESTATUS = 'ACT' AND UNI.IDEMPRESA = " & _cveEmpresa & " AND UNI.IDUNIDADTRANS LIKE '%" & Val(txtRemolque2.Text) & "%'")

                VerificaFaltantes(txtRemolque2.Text)
                If BandResultado Then
                    cmbChofer.Focus()
                Else
                    txtRemolque2.Text = ""
                    txtRemolque2.Focus()
                    txtRemolque2.SelectAll()
                End If
            End If

        End If


    End Sub

    Private Sub txtRemolque2_TextChanged(sender As Object, e As EventArgs) Handles txtRemolque2.TextChanged

    End Sub

    Private Sub cmbChofer_KeyUp(sender As Object, e As KeyEventArgs) Handles cmbChofer.KeyUp
        If e.KeyValue = Keys.Enter Then
            Salida = True
            cmbAutoriza.Focus()
            Salida = False
        End If
    End Sub


    Private Sub cmbChofer_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbChofer.SelectedIndexChanged

    End Sub

    Private Sub cmbAutoriza_KeyUp(sender As Object, e As KeyEventArgs) Handles cmbAutoriza.KeyUp
        If e.KeyValue = Keys.Enter Then
            Salida = True
            '10/ENE/2017
            'txtKilometraje.Focus()
            'txtKilometraje.SelectAll()

            chkBoxTipoOrdServ.Focus()

            Salida = False
        End If

    End Sub

    Private Function IniciaOrdenTrabajo(ByVal IdTipo As Integer, ByVal nomTipOrdServ As String, ByVal NumOrdServ As Integer,
                                        ByVal bModifica As Boolean, ByVal TipoServ As Integer, ByVal IdUnidadTrans As String,
                                        ByVal vOpcion As TipoOrdenServicio) As DataTable

        'Dim list As List(Of UnidadesTranspClass)
        'Dim list = cmbTipoServ.SelectedItem


        Dim listObj As List(Of UnidadesTranspClass) = New List(Of UnidadesTranspClass)()
        If bModifica Then
            UniTrans = New UnidadesTranspClass(IdUnidadTrans)
            listObj.Add(UniTrans)
        Else
            UniTrans = New UnidadesTranspClass(txtTractor.Text)
            listObj.Add(UniTrans)
            If txtRemolque1.Text <> "" Then
                UniTrans = New UnidadesTranspClass(txtRemolque1.Text)
                listObj.Add(UniTrans)
            End If
            If txtDolly.Text <> "" Then
                UniTrans = New UnidadesTranspClass(txtDolly.Text)
                listObj.Add(UniTrans)
            End If
            If txtRemolque2.Text <> "" Then
                UniTrans = New UnidadesTranspClass(txtRemolque2.Text)
                listObj.Add(UniTrans)
            End If

        End If



        Dim frm As New fOrdenTrab(_con, _Usuario, _cveEmpresa, IdTipo, nomTipOrdServ, NumOrdServ, listObj, bModifica, TipoServ, vOpcion, "")
        If frm.ShowDialog = Windows.Forms.DialogResult.OK Then
            'Hace algo
            If frm.MyTablaRes.Rows.Count > 0 Then
                If grdOrdenServCab.Rows.Count > 0 Then
                    Clave = grdOrdenServCab.Item("oscCLAVE", grdOrdenServCab.CurrentCell.RowIndex).Value
                    FiltraDetallexClave(Clave)
                End If

            End If
            frm.Dispose()
            Return frm.MyTablaRes
        Else
            frm.Dispose()
            Return Nothing
        End If

        'If log.ShowDialog = Windows.Forms.DialogResult.OK Then
        '    sRutaEmpresaAdmPAQ = log.vRuta

        '    log.Dispose()

        '    Return True
        'Else
        '    log.Dispose()
        '    Return False
        'End If
    End Function

    Private Sub btnAgregaOS_Click(sender As Object, e As EventArgs) Handles btnAgregaOS.Click
        'Agregar del listado las diferentes ordenes de servicio.
        'Empezaremos con Combustible que es con un servicio Fijo.
        'Dim s As String = ""
        If chkBoxTipoOrdServ.CheckedItems.Count > 0 Then
            vNumOrdServ = 0

            'Tipos de Orden de Servicio Palomiados
            Dim list = chkBoxTipoOrdServ.CheckedItems

            Dim listObj As List(Of TiposOrdenServicio) = New List(Of TiposOrdenServicio)()
            For Each obj In list
                listObj.Add(CType(obj, TiposOrdenServicio))
            Next

            Dim ObjTipOrdServ As New TiposOrdenServicio(0)
            Dim vIdTipo As Integer = 0
            Dim myTabla As DataTable
            Dim vCatalogo As String
            'Dim vNomCampo As String
            Dim vValorId As Integer
            Dim tablaRes As DataTable
            Dim vUnidadTrans As String = ""
            Dim vNotaRecepcion As String = ""
            Dim vIdDivision As Integer = 0
            Dim vNomDivision As String = ""
            Dim vidServicio As Integer = 0
            Dim vidActividad As Integer = 0
            Dim vDuracionAct As Integer = 0
            Dim vNomServicio As String = ""
            'Dim idLlanta As String = ""
            'Dim PosLLanta As Integer = 0



            For Each lobj In listObj
                vIdTipo = lobj.idTipoOrden
                If lobj.bOrdenTrab Then
                    'Si tiene que abrir la Forma del Detalle
                    vNumOrdServ += 1
                    tablaRes = New DataTable()
                    tablaRes = IniciaOrdenTrabajo(vIdTipo, lobj.nomTipOrdServ, vNumOrdServ, False, 0, "", vIdTipo)
                    If Not tablaRes Is Nothing Then
                        If tablaRes.Rows.Count > 0 Then
                            vUnidadTrans = ""
                            Dim dv As New DataView(tablaRes)
                            dv.Sort = "totIdUnidadTransp"
                            Dim DTOrdenado As DataTable = dv.ToTable
                            For i = 0 To DTOrdenado.Rows.Count - 1
                                If vUnidadTrans = "" And i = 0 Then
                                    vUnidadTrans = DTOrdenado.DefaultView.Item(i).Item("totIdUnidadTransp")
                                End If

                                If vUnidadTrans = DTOrdenado.DefaultView.Item(i).Item("totIdUnidadTransp") Then
                                    'vUnidadTrans = DTOrdenado.DefaultView.Item(i).Item("totIdUnidadTransp")
                                    vNotaRecepcion = DTOrdenado.DefaultView.Item(i).Item("totNotaRecepcion")
                                    vIdDivision = DTOrdenado.DefaultView.Item(i).Item("totidDivision")
                                    vNomDivision = DTOrdenado.DefaultView.Item(i).Item("totNomDivision")
                                    vidServicio = DTOrdenado.DefaultView.Item(i).Item("totidServicio")
                                    If vidServicio > 0 Then
                                        Servicio = New ServicioClass(vidServicio)
                                        If Servicio.Existe Then
                                            vNomServicio = Servicio.NomServicio
                                        Else
                                            vNomServicio = ""
                                        End If
                                    Else
                                        vNomServicio = ""
                                    End If

                                    vidActividad = DTOrdenado.DefaultView.Item(i).Item("totidActividad")
                                    vDuracionAct = DTOrdenado.DefaultView.Item(i).Item("totDuracionActHr")
                                    vTipoServ = DTOrdenado.DefaultView.Item(i).Item("totTipoServ")
                                    idLlanta = DTOrdenado.DefaultView.Item(i).Item("totidLlanta")
                                    PosLLanta = DTOrdenado.DefaultView.Item(i).Item("totPosLlanta")

                                    InsertaRegGridDet(vNumOrdServ, vidServicio, vNomServicio, vidActividad, vNotaRecepcion, vIdDivision,
                                                      vNomDivision, vDuracionAct, 0, 0, False, 0, idLlanta, PosLLanta)

                                    If i = DTOrdenado.Rows.Count - 1 Then
                                        UniTrans = New UnidadesTranspClass(vUnidadTrans)
                                        TipoUniTrans = New TipoUniTransClass(UniTrans.idTipoUnidad)

                                        '10/ENE/2017 - KILOMETRAJE CERO 
                                        InsertaRegGridCab(vNumOrdServ, vIdTipo, lobj.nomTipOrdServ, vUnidadTrans, UniTrans.idTipoUnidad,
                                        TipoUniTrans.nomTipoUniTras, 0, cmbAutoriza.SelectedValue, lobj.bOrdenTrab, vTipoServ, False, False)

                                    End If

                                Else
                                    'CAMBIO DE UNIDAD
                                    UniTrans = New UnidadesTranspClass(vUnidadTrans)
                                    TipoUniTrans = New TipoUniTransClass(UniTrans.idTipoUnidad)
                                    'vUnidadTrans = DTOrdenado.DefaultView.Item(i).Item("totIdUnidadTransp")
                                    'Insertamos Cabecero Anterior

                                    '10/ENE/2017 -> KILOMETRAJE 0
                                    InsertaRegGridCab(vNumOrdServ, vIdTipo, lobj.nomTipOrdServ, vUnidadTrans, UniTrans.idTipoUnidad,
                                     TipoUniTrans.nomTipoUniTras, 0, cmbAutoriza.SelectedValue, lobj.bOrdenTrab, vTipoServ, False, False)


                                    vNumOrdServ += 1
                                    vUnidadTrans = DTOrdenado.DefaultView.Item(i).Item("totIdUnidadTransp")
                                    vNotaRecepcion = DTOrdenado.DefaultView.Item(i).Item("totNotaRecepcion")
                                    vIdDivision = DTOrdenado.DefaultView.Item(i).Item("totidDivision")
                                    vNomDivision = DTOrdenado.DefaultView.Item(i).Item("totNomDivision")
                                    vidServicio = DTOrdenado.DefaultView.Item(i).Item("totidServicio")
                                    If vidServicio > 0 Then
                                        Servicio = New ServicioClass(vidServicio)
                                        If Servicio.Existe Then
                                            vNomServicio = Servicio.NomServicio
                                        Else
                                            vNomServicio = ""
                                        End If
                                    Else
                                        vNomServicio = ""
                                    End If
                                    vidActividad = DTOrdenado.DefaultView.Item(i).Item("totidActividad")
                                    vDuracionAct = DTOrdenado.DefaultView.Item(i).Item("totDuracionActHr")
                                    vTipoServ = DTOrdenado.DefaultView.Item(i).Item("totTipoServ")

                                    idLlanta = DTOrdenado.DefaultView.Item(i).Item("totidLlanta")
                                    PosLLanta = DTOrdenado.DefaultView.Item(i).Item("totPosLlanta")

                                    'InsertaRegGridDet(vNumOrdServ, vidServicio, vNotaRecepcion, vidActividad, "", vIdDivision,
                                    '                  vNomDivision, vDuracionAct, 0, 0, False, 0, idLlanta, PosLLanta)

                                    '12/JUL/2017
                                    InsertaRegGridDet(vNumOrdServ, vidServicio, vNomServicio, vidActividad, vNotaRecepcion, vIdDivision,
                                                      vNomDivision, vDuracionAct, 0, 0, False, 0, idLlanta, PosLLanta)

                                    If i = DTOrdenado.Rows.Count - 1 Then
                                        UniTrans = New UnidadesTranspClass(vUnidadTrans)
                                        TipoUniTrans = New TipoUniTransClass(UniTrans.idTipoUnidad)

                                        '10/ENE/2017 -> KILOMETRAJE 0
                                        InsertaRegGridCab(vNumOrdServ, vIdTipo, lobj.nomTipOrdServ, vUnidadTrans, UniTrans.idTipoUnidad,
                                        TipoUniTrans.nomTipoUniTras, 0, cmbAutoriza.SelectedValue, lobj.bOrdenTrab, vTipoServ, False, False)

                                    End If
                                End If
                            Next
                            Clave = grdOrdenServCab.Item("oscCLAVE", grdOrdenServCab.CurrentCell.RowIndex).Value
                            FiltraDetallexClave(Clave)
                        End If
                    End If

                Else
                    'Checamos Si Existen Relaciones Automaticas para Este Tipo de Orden de Servicio
                    myTabla = New DataTable
                    myTabla = ObjTipOrdServ.TablaRelTipOrdServ("rel.idTipOrdServ = " & vIdTipo)
                    If myTabla.Rows.Count > 0 Then
                        For i = 0 To myTabla.Rows.Count - 1
                            vCatalogo = myTabla.DefaultView.Item(i).Item("NomCatalogo").ToString
                            'vNomCampo = myTabla.DefaultView.Item(i).Item("NombreId").ToString
                            vValorId = myTabla.DefaultView.Item(i).Item("IdRelacion")
                            Select Case UCase(vCatalogo)
                                Case UCase("catservicios")
                                    Servicio = New ServicioClass(vValorId)
                                    If Servicio.Existe Then
                                        'Recorrer todas las unidades de TRansporte
                                        For Each txtCTR As Control In Me.GroupBox4.Controls
                                            If TypeOf txtCTR Is TextBox Then
                                                If txtCTR.Tag >= 1 And txtCTR.Tag <= 4 Then
                                                    'Agregar Orden de Servicio CAB y DET
                                                    If txtCTR.Text <> "" Then
                                                        UniTrans = New UnidadesTranspClass(txtCTR.Text)
                                                        TipoUniTrans = New TipoUniTransClass(UniTrans.idTipoUnidad)
                                                        If TipoUniTrans.bCombustible Then
                                                            vNumOrdServ += 1

                                                            '10/ENE/2017 -> KILOMETRAJE 0
                                                            InsertaRegGridCab(vNumOrdServ, vIdTipo, lobj.nomTipOrdServ, UniTrans.iUnidadTrans, UniTrans.idTipoUnidad,
                                                            TipoUniTrans.nomTipoUniTras, 0, cmbAutoriza.SelectedValue, lobj.bOrdenTrab, Servicio.idTipoServicio, False, False)

                                                            idLlanta = ""
                                                            PosLLanta = 0

                                                            InsertaRegGridDet(vNumOrdServ, vValorId, Servicio.NomServicio, 0, "", 0, "", 0, 0, 0, False, 0, idLlanta, PosLLanta)
                                                        End If
                                                    End If
                                                End If
                                            End If
                                        Next
                                    End If
                            End Select
                        Next
                    Else
                        'No tiene RElaciones
                    End If
                End If


            Next
        End If
    End Sub
    Private Function InsertaRegGridCab(ByVal ClaveFicticia As Integer, ByVal IdTipoOrdServ As Integer, ByVal NomTipoOrdServ As String,
                                       ByVal idUnidadTrans As String, ByVal idTipoUnidadTrans As Integer, ByVal nomTipoUniTras As String,
                                       ByVal Km As Integer, ByVal idEmpleadoAutoriza As Integer, ByVal bOrdenTrab As Boolean,
                                       ByVal idTipoServ As Integer, ByVal bModificado As Boolean, ByVal bCancelado As Boolean) As Boolean
        Try

            grdOrdenServCab.Rows.Add(ClaveFicticia, ClaveFicticia, 0, dtpFechaCap.Value, IdTipoOrdServ, NomTipoOrdServ,
            idUnidadTrans, idTipoUnidadTrans, nomTipoUniTras, Km, idEmpleadoAutoriza, bOrdenTrab, idTipoServ, IIf(bModificado, 1, 0), IIf(bCancelado, 1, 0))

            grdOrdenServCab.PerformLayout()
            Return True

        Catch ex As Exception
            Return False
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, Me.Text)
        End Try

    End Function

    Private Function InsertaRegGridDet(ByVal ClaveFicticia As Integer, ByVal idServicio As Integer,
    ByVal NomServicio As String, ByVal idActividad As Integer, ByVal NombreAct As String,
    ByVal idDivision As Integer, ByVal NomDivision As String, ByVal DuracionActHr As Integer,
    ByVal IdOrdenSer As Integer, ByVal idOrdenTrabajo As Integer,
    ByVal bModificado As Boolean, ByVal idOrdenActividad As Integer,
    ByVal idLlanta As String, ByVal Posicion As Integer)
        Try

            grdOrdenServDet.Rows.Add(ClaveFicticia, ClaveFicticia, idServicio, NomServicio, idActividad, NombreAct, idOrdenTrabajo,
            idDivision, NomDivision, DuracionActHr, IIf(bModificado, 1, 0), idOrdenActividad, idLlanta, Posicion)

            myDataRow = MyTablaDetalle.NewRow()
            myDataRow("tosdCLAVE") = ClaveFicticia
            myDataRow("tosdIdOrdenSer") = IdOrdenSer
            myDataRow("tosdidServicio") = idServicio
            myDataRow("tosdNomServicio") = NomServicio
            myDataRow("tosdidActividad") = idActividad
            myDataRow("tosdNombreAct") = NombreAct
            myDataRow("tosdidOrdenTrabajo") = idOrdenTrabajo
            myDataRow("tosdidDivision") = idDivision
            myDataRow("tosdNomDivision") = NomDivision
            myDataRow("tosdDuracionActHr") = DuracionActHr

            myDataRow("tosdbModificado") = bModificado
            myDataRow("tosdidOrdenActividad") = idOrdenActividad

            myDataRow("tosdidLlanta") = idLlanta
            myDataRow("tosdPosicion") = Posicion

            MyTablaDetalle.Rows.Add(myDataRow)

            grdOrdenServDet.PerformLayout()
            Return True

        Catch ex As Exception
            Return False
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, Me.Text)
        End Try

    End Function


    Private Sub txtRemolque1_TextChanged(sender As Object, e As EventArgs) Handles txtRemolque1.TextChanged

    End Sub

    Private Sub txtTractor_TextChanged(sender As Object, e As EventArgs) Handles txtTractor.TextChanged

    End Sub

    '10/ENE/2017
    'Private Sub txtKilometraje_KeyUp(sender As Object, e As KeyEventArgs)
    '    If txtKilometraje.Text.Length = 0 Then
    '        txtKilometraje.Focus()
    '        txtKilometraje.SelectAll()
    '    Else
    '        If e.KeyValue = Keys.Enter Then
    '            Salida = True
    '            If Val(txtKilometraje.Text) > vUltKm And Val(txtKilometraje.Text) <= (vUltKm + MaximoKmdeMas) Then
    '                chkBoxTipoOrdServ.Focus()
    '            Else
    '                MsgBox("El Kilometraje no es Valido", MsgBoxStyle.Exclamation, Me.Text)
    '                txtKilometraje.Focus()
    '                txtKilometraje.SelectAll()
    '            End If
    '            Salida = False
    '        End If
    '    End If
    'End Sub

    '10/ENE/2017
    'Private Sub txtKilometraje_Leave(sender As Object, e As EventArgs)
    '    If Salida Then Exit Sub
    '    If txtKilometraje.Text.Length = 0 Then
    '        txtKilometraje.Focus()
    '        txtKilometraje.SelectAll()
    '    Else
    '        If txtKilometraje.Enabled Then
    '            If Val(txtKilometraje.Text) > vUltKm And Val(txtKilometraje.Text) <= (vUltKm + MaximoKmdeMas) Then
    '                chkBoxTipoOrdServ.Focus()
    '            Else
    '                MsgBox("El Kilometraje no es Valido", MsgBoxStyle.Exclamation, Me.Text)
    '                txtKilometraje.Focus()
    '                txtKilometraje.SelectAll()
    '            End If
    '        End If
    '    End If

    'End Sub


    Private Sub grdOrdenServCab_Click(sender As Object, e As EventArgs) Handles grdOrdenServCab.Click
        Salida = True
        Clave = grdOrdenServCab.Item("oscCLAVE", grdOrdenServCab.CurrentCell.RowIndex).Value
        FiltraDetallexClave(Clave)
        Salida = False
    End Sub

    Private Sub CreaTablaDetalle()
        Try
            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Int64")
            myDataColumn.ColumnName = "tosdCLAVE"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaDetalle.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Int64")
            myDataColumn.ColumnName = "tosdIdOrdenSer"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaDetalle.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Int64")
            myDataColumn.ColumnName = "tosdidServicio"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaDetalle.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.String")
            myDataColumn.ColumnName = "tosdNomServicio"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaDetalle.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Int64")
            myDataColumn.ColumnName = "tosdidActividad"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaDetalle.Columns.Add(myDataColumn)


            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.String")
            myDataColumn.ColumnName = "tosdNombreAct"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaDetalle.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Int64")
            myDataColumn.ColumnName = "tosdidOrdenTrabajo"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaDetalle.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Int64")
            myDataColumn.ColumnName = "tosdidDivision"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaDetalle.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.String")
            myDataColumn.ColumnName = "tosdNomDivision"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaDetalle.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Int64")
            myDataColumn.ColumnName = "tosdDuracionActHr"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaDetalle.Columns.Add(myDataColumn)


            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Boolean")
            myDataColumn.ColumnName = "tosdbModificado"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaDetalle.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Int64")
            myDataColumn.ColumnName = "tosdidOrdenActividad"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaDetalle.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.String")
            myDataColumn.ColumnName = "tosdidLlanta"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaDetalle.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Int64")
            myDataColumn.ColumnName = "tosdPosicion"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaDetalle.Columns.Add(myDataColumn)

        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try

    End Sub

    Private Sub FiltraDetallexClave(ByVal Clave As Integer)
        MyTablaDetalle.DefaultView.RowFilter = Nothing
        MyTablaDetalle.DefaultView.RowFilter = "tosdCLAVE = " & Clave

        grdOrdenServDet.Rows.Clear()
        For i = 0 To MyTablaDetalle.DefaultView.Count - 1
            grdOrdenServDet.Rows.Add()
            grdOrdenServDet.Item("osdCLAVE", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdCLAVE")
            grdOrdenServDet.Item("osdIdOrdenSer", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdIdOrdenSer")
            grdOrdenServDet.Item("osdidServicio", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdidServicio")
            grdOrdenServDet.Item("osdNomServicio", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdNomServicio")
            grdOrdenServDet.Item("osdidActividad", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdidActividad")
            grdOrdenServDet.Item("osdNombreAct", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdNombreAct")
            grdOrdenServDet.Item("osdidOrdenTrabajo", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdidOrdenTrabajo")
            grdOrdenServDet.Item("osdidDivision", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdidDivision")
            grdOrdenServDet.Item("osdNomDivision", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdNomDivision")

            grdOrdenServDet.Item("osdDuracionActHr", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdDuracionActHr")
            grdOrdenServDet.Item("osdModificado", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdbModificado")
            grdOrdenServDet.Item("osdidOrdenActividad", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdidOrdenActividad")

            grdOrdenServDet.Item("osdidLlanta", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdidLlanta")
            grdOrdenServDet.Item("osdPosicion", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdPosicion")


            'grdOrdenServDet.DataSource = MyTablaDetalle
        Next
        grdOrdenServDet.PerformLayout()
        'MyTablaDetalle.DefaultView.RowFilter = Nothing
    End Sub

    Private Sub AgregaTODOSDET()
        MyTablaDetalle.DefaultView.RowFilter = Nothing

        grdOrdenServDet.Rows.Clear()
        For i = 0 To MyTablaDetalle.DefaultView.Count - 1
            grdOrdenServDet.Rows.Add()
            grdOrdenServDet.Item("osdCLAVE", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdCLAVE")
            grdOrdenServDet.Item("osdIdOrdenSer", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdIdOrdenSer")
            grdOrdenServDet.Item("osdidServicio", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdidServicio")
            grdOrdenServDet.Item("osdNomServicio", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdNomServicio")
            grdOrdenServDet.Item("osdidActividad", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdidActividad")
            grdOrdenServDet.Item("osdNombreAct", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdNombreAct")
            grdOrdenServDet.Item("osdidOrdenTrabajo", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdidOrdenTrabajo")
            grdOrdenServDet.Item("osdidDivision", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdidDivision")
            grdOrdenServDet.Item("osdNomDivision", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdNomDivision")

            grdOrdenServDet.Item("osdDuracionActHr", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdDuracionActHr")
            grdOrdenServDet.Item("osdModificado", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdbModificado")
            grdOrdenServDet.Item("osdidOrdenActividad", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdidOrdenActividad")

            grdOrdenServDet.Item("osdidLlanta", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdidLlanta")
            grdOrdenServDet.Item("osdPosicion", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdPosicion")

            'grdOrdenServDet.DataSource = MyTablaDetalle
        Next
        grdOrdenServDet.PerformLayout()
        'MyTablaDetalle.DefaultView.RowFilter = Nothing
    End Sub

    'Private Sub Button5_Click(sender As Object, e As EventArgs)
    '    'ImprimeOrdenServicio(271, False, True, True)




    '    ImprimeOrdenServicio(287, False, True, True)
    '    'ImprimeOrdenServicio(288, True, True, True)
    '    'ImprimeOrdenServicio(289, True, True, True)


    '    'ImprimeOrdenServicio(272, True, True, True)

    '    'ImprimeOrdenServicio(284, False, True, True)
    'End Sub

    Private Sub txtidPadreOrdSer_KeyDown(sender As Object, e As KeyEventArgs) Handles txtidPadreOrdSer.KeyDown
        If e.KeyCode = Keys.F3 Then
            'cmdBuscaClave_Click(sender, e)
            'txtTractor.Text = DespliegaGrid(UCase("FolioOS"), Inicio.CONSTR, "idPadreOrdSer")
            'CargaForm(txtidPadreOrdSer.Text, "FolioOS", , , , True)
            txtidPadreOrdSer_Leave(sender, e)
            'asd()
        End If

    End Sub


    Private Sub txtidPadreOrdSer_KeyUp(sender As Object, e As KeyEventArgs) Handles txtidPadreOrdSer.KeyUp
        If e.KeyValue = Keys.Enter Then
            Salida = True
            CargaForm(txtidPadreOrdSer.Text, "FolioOS", , , , True)
            If txtEstatus.Text = "CAP" Then
                ActivaBotones(True, TipoOpcActivaBoton.tOpcEditar)
                ActivaCampos(False, TipoOpcActivaCampos.tOpcDEPENDEVALOR)
                chkBoxTipoOrdServ.Enabled = True
                chkBoxTipoOrdServ.Focus()
            Else
                ActivaBotones(True, TipoOpcActivaBoton.tOpcConsulta)
                ActivaCampos(False, TipoOpcActivaCampos.tOpcDEPENDEVALOR)
                chkBoxTipoOrdServ.Enabled = False
                grdOrdenServCab.Focus()
            End If


            Salida = False
        Else
            Salida = False
        End If
    End Sub

    Private Sub txtidPadreOrdSer_Leave(sender As Object, e As EventArgs) Handles txtidPadreOrdSer.Leave
        If Salida Then Exit Sub
        'CargaForm(txtidPadreOrdSer.Text, "FolioOS", , , , True)
        'ActivaBotones(True, TipoOpcActivaBoton.tOpcInicializa)
        'ActivaCampos(False, TipoOpcActivaCampos.tOpcDEPENDEVALOR)
        'chkBoxTipoOrdServ.Focus()
        Salida = True
        CargaForm(txtidPadreOrdSer.Text, "FolioOS", , , , True)
        If txtEstatus.Text = "CAP" Then
            'ActivaBotones(True, TipoOpcActivaBoton.tOpcInicializa)
            ActivaBotones(True, TipoOpcActivaBoton.tOpcEditar)
            ActivaCampos(False, TipoOpcActivaCampos.tOpcDEPENDEVALOR)
            chkBoxTipoOrdServ.Enabled = False
            chkBoxTipoOrdServ.Focus()
        Else
            ActivaBotones(True, TipoOpcActivaBoton.tOpcConsulta)
            ActivaCampos(False, TipoOpcActivaCampos.tOpcDEPENDEVALOR)
            chkBoxTipoOrdServ.Enabled = False
            grdOrdenServCab.Focus()
        End If
    End Sub

    Private Sub txtidPadreOrdSer_TextChanged(sender As Object, e As EventArgs) Handles txtidPadreOrdSer.TextChanged

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click

    End Sub
    Private Function SetupThePrinting() As Boolean
        Dim MyPrintDialog As New PrintDialog()

        MyPrintDialog.AllowCurrentPage = False
        MyPrintDialog.AllowPrintToFile = False
        MyPrintDialog.AllowSelection = False
        MyPrintDialog.AllowSomePages = False
        MyPrintDialog.PrintToFile = False
        MyPrintDialog.ShowHelp = False
        MyPrintDialog.ShowNetwork = False

        If MyPrintDialog.ShowDialog <> DialogResult.OK Then
            Return False
        End If

        If prtSettings Is Nothing Then
            prtSettings = New PrinterSettings
        End If
        prtSettings = MyPrintDialog.PrinterSettings

        'prtDoc.DocumentName = "Reporte de Auditoria"
        'prtDoc.PrinterSettings = prtSettings
        'prtDoc.PrinterSettings = MyPrintDialog.PrinterSettings
        'prtDoc.DefaultPageSettings = MyPrintDialog.PrinterSettings.DefaultPageSettings
        'prtDoc.DefaultPageSettings.Margins = New Margins(40, 40, 40, 40)
        Return True
    End Function

    Private Sub btnMenuReImprimir_Click(sender As Object, e As EventArgs) Handles btnMenuReImprimir.Click
        If Val(txtidPadreOrdSer.Text) > 0 Then
            If SetupThePrinting() Then
                For i = 0 To grdOrdenServCab.Rows.Count - 1
                    ImprimeOrdenServicio(grdOrdenServCab.Item("oscCLAVE", i).Value,
                    grdOrdenServCab.Item("oscbOrdenTrabajo", i).Value, True, False)
                Next
            End If
        Else
            MsgBox("tiene que Existir el numero de FOLIO", MsgBoxStyle.Exclamation, Me.Text)
        End If
    End Sub

    Private Sub ModificarToolStripMenuOrdSer_Click(sender As Object, e As EventArgs) Handles ModificarToolStripMenuOrdSer.Click
        Try
            Dim tablaRes As DataTable
            'Dim TablaNewMyTablaDetalle As DataTable
            Dim vUnidadTrans As String = ""
            Dim vNotaRecepcion As String = ""
            Dim vIdDivision As Integer = 0
            Dim vNomDivision As String = ""
            Dim vidServicio As Integer = 0
            Dim vidActividad As Integer = 0
            Dim vDuracionAct As Integer = 0
            Dim vNomServicio As String = ""
            Dim vIdTipo As Integer = 0
            Dim vidOrdenActividad As Integer = 0
            Dim vIdOrdenTrabajo As Integer = 0

            vNumOrdServ = grdOrdenServCab.Item("oscCLAVE", grdOrdenServCab.CurrentCell.RowIndex).Value
            vOrdenTrabajo = grdOrdenServCab.Item("oscbOrdenTrabajo", grdOrdenServCab.CurrentCell.RowIndex).Value
            vNomTipOrdServ = grdOrdenServCab.Item("oscNomTipOrdServ", grdOrdenServCab.CurrentCell.RowIndex).Value
            vTipoOrdServ = grdOrdenServCab.Item("oscTipoOrdenServ", grdOrdenServCab.CurrentCell.RowIndex).Value
            vidUnidadTrans = grdOrdenServCab.Item("oscidUnidadTrans", grdOrdenServCab.CurrentCell.RowIndex).Value
            vTipoServ = grdOrdenServCab.Item("oscTipoServ", grdOrdenServCab.CurrentCell.RowIndex).Value

            If vOrdenTrabajo Then
                'Preparar para Abrir
                tablaRes = New DataTable()
                tablaRes = IniciaOrdenTrabajo(vTipoOrdServ, vNomTipOrdServ, vNumOrdServ, True, vTipoServ, vidUnidadTrans, vTipoOrdServ)
                If Not tablaRes Is Nothing Then
                    If tablaRes.Rows.Count > 0 Then
                        vUnidadTrans = ""
                        'Borramos en el grid lo actual
                        'For z = 0 To grdOrdenServDet.Rows.Count - 1
                        '    If grdOrdenServDet.Item("osdIdOrdenSer", z).Value = vNumOrdServ Then
                        '        grdOrdenServDet.Rows.RemoveAt(z)
                        '    End If
                        'Next
                        'TablaNewMyTablaDetalle = New DataTable
                        'TablaNewMyTablaDetalle = MyTablaDetalle
                        Dim TablaNewMyTablaDetalle As DataTable = MyTablaDetalle.Copy

                        For z = MyTablaDetalle.Rows.Count - 1 To 0 Step -1
                            If MyTablaDetalle.Rows.Item(z).Item("tosdIdOrdenSer") = vNumOrdServ Then
                                'grdOrdenServDet.Rows.RemoveAt(z)
                                'TablaNewMyTablaDetalle.DefaultView.Delete(z)
                                TablaNewMyTablaDetalle.Rows.RemoveAt(z)
                                'z = z + 1
                            End If
                        Next
                        MyTablaDetalle.Clear()
                        MyTablaDetalle = TablaNewMyTablaDetalle

                        grdOrdenServDet.Rows.Clear()

                        'MyTablaDetalle.Rows.Add(myDataRow)

                        For i = 0 To tablaRes.Rows.Count - 1
                            If vUnidadTrans = "" And i = 0 Then
                                vUnidadTrans = tablaRes.DefaultView.Item(i).Item("totIdUnidadTransp")
                            End If
                            If vUnidadTrans = tablaRes.DefaultView.Item(i).Item("totIdUnidadTransp") Then
                                'vUnidadTrans = DTOrdenado.DefaultView.Item(i).Item("totIdUnidadTransp")
                                vNotaRecepcion = tablaRes.DefaultView.Item(i).Item("totNotaRecepcion")
                                vIdDivision = tablaRes.DefaultView.Item(i).Item("totidDivision")
                                vNomDivision = tablaRes.DefaultView.Item(i).Item("totNomDivision")
                                vidServicio = tablaRes.DefaultView.Item(i).Item("totidServicio")
                                vIdOrdenTrabajo = tablaRes.DefaultView.Item(i).Item("totidOrdenTrabajo")


                                If vidServicio > 0 Then
                                    Servicio = New ServicioClass(vidServicio)
                                    If Servicio.Existe Then
                                        vNomServicio = Servicio.NomServicio
                                    Else
                                        vNomServicio = ""
                                    End If
                                Else
                                    vNomServicio = ""
                                End If

                                vidActividad = tablaRes.DefaultView.Item(i).Item("totidActividad")
                                vDuracionAct = tablaRes.DefaultView.Item(i).Item("totDuracionActHr")
                                vTipoServ = tablaRes.DefaultView.Item(i).Item("totTipoServ")

                                idLlanta = tablaRes.DefaultView.Item(i).Item("totidLlanta")
                                PosLLanta = tablaRes.DefaultView.Item(i).Item("totPosLlanta")

                                'InsertaRegGridDet(vNumOrdServ, vNumOrdServ, vidServicio, vNomServicio, vidActividad, vNotaRecepcion, vIdOrdenTrabajo, vIdDivision,
                                '                  vNomDivision, vDuracionAct, True,
                                '                  tablaRes.DefaultView.Item(i).Item("totidOrdenActividad"))

                                InsertaRegGridDet(vNumOrdServ, vidServicio, vNomServicio, vidActividad, vNotaRecepcion, vIdDivision,
                                vNomDivision, vDuracionAct, vNumOrdServ, vIdOrdenTrabajo, True,
                                tablaRes.DefaultView.Item(i).Item("totidOrdenActividad"), idLlanta, PosLLanta)

                                If i = tablaRes.Rows.Count - 1 Then
                                    UniTrans = New UnidadesTranspClass(vUnidadTrans)
                                    TipoUniTrans = New TipoUniTransClass(UniTrans.idTipoUnidad)

                                    '10/ENE/2017 -> KILOMETRAJE CERO
                                    InsertaRegGridCab(vNumOrdServ, vIdTipo, vTipoServ, vUnidadTrans, UniTrans.idTipoUnidad,
                                    TipoUniTrans.nomTipoUniTras, 0, cmbAutoriza.SelectedValue, vOrdenTrabajo, vTipoServ, True, False)

                                End If

                            Else
                                'CAMBIO DE UNIDAD
                                'Insertamos Cabecero Anterior
                                '10/ENE/2017 -> KILOMETRAJE 0
                                InsertaRegGridCab(vNumOrdServ, vIdTipo, vNomTipOrdServ, txtTractor.Text, UniTrans.idTipoUnidad,
                                 TipoUniTrans.nomTipoUniTras, 0, cmbAutoriza.SelectedValue, vOrdenTrabajo, vTipoServ, True, False)

                                vNumOrdServ += 1
                                vUnidadTrans = tablaRes.DefaultView.Item(i).Item("totIdUnidadTransp")
                                vNotaRecepcion = tablaRes.DefaultView.Item(i).Item("totNotaRecepcion")
                                vIdDivision = tablaRes.DefaultView.Item(i).Item("totidDivision")
                                vNomDivision = tablaRes.DefaultView.Item(i).Item("totNomDivision")
                                vidServicio = tablaRes.DefaultView.Item(i).Item("totidServicio")
                                vidActividad = tablaRes.DefaultView.Item(i).Item("totidActividad")
                                vDuracionAct = tablaRes.DefaultView.Item(i).Item("totDuracionActHr")
                                vTipoServ = tablaRes.DefaultView.Item(i).Item("totTipoServ")

                                idLlanta = tablaRes.DefaultView.Item(i).Item("totidLlanta")
                                PosLLanta = tablaRes.DefaultView.Item(i).Item("totPosLlanta")

                                'InsertaRegGridDet(vNumOrdServ, vidServicio, vNotaRecepcion, vidActividad, "",
                                '                  vIdDivision, vNomDivision, vDuracionAct, vNumOrdServ, vIdOrdenTrabajo, True, tablaRes.DefaultView.Item(i).Item("totidOrdenActividad"))

                                '  InsertaRegGridDet(vNumOrdServ, vNumOrdServ, vidServicio, vNomServicio, vidActividad, vNotaRecepcion, vIdOrdenTrabajo, vIdDivision,
                                'vNomDivision, vDuracionAct, True,
                                'tablaRes.DefaultView.Item(i).Item("totidOrdenActividad"))
                                InsertaRegGridDet(vNumOrdServ, vidServicio, vNomServicio, vidActividad, vNotaRecepcion, vIdDivision, vNomDivision,
                                vDuracionAct, vNumOrdServ, vIdOrdenTrabajo, True,
                                tablaRes.DefaultView.Item(i).Item("totidOrdenActividad"), idLlanta, PosLLanta)



                                If i = tablaRes.Rows.Count - 1 Then
                                    UniTrans = New UnidadesTranspClass(vUnidadTrans)
                                    TipoUniTrans = New TipoUniTransClass(UniTrans.idTipoUnidad)

                                    '10/ENE/2017 -> KILOMETRAJE 0
                                    InsertaRegGridCab(vNumOrdServ, vIdTipo, vNomTipOrdServ, vUnidadTrans, UniTrans.idTipoUnidad,
                                    TipoUniTrans.nomTipoUniTras, 0, cmbAutoriza.SelectedValue, vOrdenTrabajo, vTipoServ, True, False)

                                End If
                            End If
                        Next

                    End If
                End If
            End If
        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try
    End Sub

    Private Sub ImprimirToolStripMenuOrdSer_Click(sender As Object, e As EventArgs) Handles ImprimirToolStripMenuOrdSer.Click
        Try
            vNumOrdServ = grdOrdenServCab.Item("oscCLAVE", grdOrdenServCab.CurrentCell.RowIndex).Value
            vOrdenTrabajo = grdOrdenServCab.Item("oscbOrdenTrabajo", grdOrdenServCab.CurrentCell.RowIndex).Value
            ImprimeOrdenServicio(vNumOrdServ, vOrdenTrabajo, True, False)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub txtKilometraje_TextChanged(sender As Object, e As EventArgs)

    End Sub

    Private Sub ConMnuOrdSer_Opening(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles ConMnuOrdSer.Opening
        With grdOrdenServCab.Rows(grdOrdenServCab.CurrentRow.Index)
            ImprimirToolStripMenuOrdSer.Enabled = True
            CancelarToolStripMenuOrdSer.Enabled = True
            If .Cells("oscbOrdenTrabajo").Value Then
                ModificarToolStripMenuOrdSer.Enabled = True
            Else
                ModificarToolStripMenuOrdSer.Enabled = False
            End If
        End With
    End Sub



    Private Sub ToolStripButton1_Click(sender As Object, e As EventArgs) Handles btnMnuCancelar.Click

        'Dim fCanc As New FCancelaSolServ(txtidPadreOrdSer.Text, txtNomEmpresa.Text, dtpFechaCap.Value, _Usuario)
        Dim fCanc As New FCancelaSolServ(txtidPadreOrdSer.Text, cmbIdEmpresa.Text, dtpFechaCap.Value, _Usuario)
        fCanc.WindowState = FormWindowState.Normal
        fCanc.Show()
        Cancelar()
        txtTractor.Focus()

    End Sub

    Private Sub chkBoxTipoOrdServ_EnabledChanged(sender As Object, e As EventArgs) Handles chkBoxTipoOrdServ.EnabledChanged
        btnAgregaOS.Enabled = chkBoxTipoOrdServ.Enabled
    End Sub

    Public Function AutoCompletarEmpresas(ByVal Control As ComboBox, ByVal bIncluyeTodos As Boolean) As AutoCompleteStringCollection

        Dim Coleccion As New AutoCompleteStringCollection
        Dim dt As DataTable


        Empresa = New EmpresaClass(0)
        dt = Empresa.TablaEmpresas(bIncluyeTodos)

        'Recorrer y cargar los items para el Autocompletado
        For Each row As DataRow In dt.Rows
            Coleccion.Add(Convert.ToString(row("RazonSocial")))
        Next
        'Ajustamos el control TextBox o ComboBox para recibir los datos de la siguiente manera.
        With Control
            .DataSource = dt
            .DisplayMember = "RazonSocial"
            .ValueMember = "idEmpresa"


            .AutoCompleteCustomSource = Coleccion
            .AutoCompleteMode = AutoCompleteMode.Suggest
            .AutoCompleteSource = AutoCompleteSource.CustomSource
        End With

        ' Aca le devuelvo los datos a recuperados de la Base
        Return Coleccion

    End Function

    Private Sub cmbIdEmpresa_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbIdEmpresa.SelectedIndexChanged

    End Sub

    Private Sub cmbIdEmpresa_KeyUp(sender As Object, e As KeyEventArgs) Handles cmbIdEmpresa.KeyUp
        If e.KeyValue = Keys.Enter Then
            Salida = True
            If cmbIdEmpresa.SelectedValue > 0 Then
                cmbIdEmpresa.Enabled = False
                AutoCompletarTodos(cmbIdEmpresa.SelectedValue)
                txtTractor.Focus()

            Else
                cmbIdEmpresa.Enabled = True
            End If
            Salida = False
        End If
    End Sub

    Private Sub cmbIdEmpresa_Leave(sender As Object, e As EventArgs) Handles cmbIdEmpresa.Leave
        If Salida Then Exit Sub
        If cmbIdEmpresa.SelectedValue > 0 Then
            cmbIdEmpresa.Enabled = False
            AutoCompletarTodos(cmbIdEmpresa.SelectedValue)
            txtTractor.Focus()
        Else
            cmbIdEmpresa.Enabled = True
        End If
    End Sub

    Private Sub AutoCompletarTodos(ByVal Empresa As Integer)

        If Empresa > 0 Then
            AutoCompletarEmpresas(cmbIdEmpresa, False)
            AutoCompletarPersonal(cmbChofer, TipotablasPer.tpChoferes, Empresa)
            AutoCompletarPersonal(cmbAutoriza, TipotablasPer.tpSupervisores, Empresa)
            AutoCompletarUnidadTransp(txtTractor, True, Empresa, "T")
            AutoCompletarUnidadTransp(txtRemolque1, True, Empresa, "R")
            AutoCompletarUnidadTransp(txtDolly, True, Empresa, "D")
            AutoCompletarUnidadTransp(txtRemolque2, True, Empresa, "R")
            AutoCompletarFolios(txtidPadreOrdSer, " INNER JOIN dbo.CatUnidadTrans T ON mos.idTractor = t.idUnidadTrans WHERE t.idEmpresa = " & Empresa)

        End If
    End Sub

    Private Sub grdOrdenServCab_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles grdOrdenServCab.CellContentClick

    End Sub

    Private Sub ArmaTablaImpresionCarta(ByVal bOrdenTrab As Boolean)
        Dim campo As Integer
        Dim Descripcion As String
        Dim Valor As String
        Dim PosValorX, PosValorY, PosDescX, PosDescY As Single
        Dim NumAletra As New NumALetras
        Dim EnLetras As String = ""
        Dim Total As Double = 0

        Dim Y1 As Single = 150
        'Vertical
        Dim X1 As Single = 20
        Dim X1A As Single = 120
        Dim X1B As Single = 200
        Dim X2 As Single = 160
        Dim X3 As Single = 220
        Dim X3A As Single = 260
        Dim X4 As Single = 400

        Dim ValPad As Integer = 12

        DsRep = New DataSet
        If MyTablaImprime.Rows.Count > 0 Then
            MyTablaImprime.Rows.Clear()
        End If


        DsRep = ImpRep.LlenaDsImprime(ImpRep.NomTabla)
        If DsRep.Tables(ImpRep.NomTabla).DefaultView.Count > 0 Then

            For i As Integer = 0 To DsRep.Tables(ImpRep.NomTabla).DefaultView.Count - 1

                ''LINEA 1
                'campo += 1
                'Descripcion = ""
                'Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("RazonSocial"))
                'PosDescX = X2 : PosDescY = Y1 : PosValorX = X2 : PosValorY = PosDescY
                'InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

                ''CODIGO DE BARRAS ORDEN SERVICIO
                'campo += 1
                'Descripcion = ""
                'PosDescX = X4 : PosValorX = X4
                'Valor = FormarBarCode(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idOrdenSer"))
                'InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 2)

                'LINEA 2
                'campo += 1
                'PosDescX = X2
                'PosDescY = PosDescY + 15
                'PosValorX = X2 + 35
                'PosValorY = PosDescY
                'Descripcion = ""
                'Valor = "ORDEN DE SERVICIO"
                'InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

                'LINEA 3
                'campo += 1
                'Descripcion = ""
                'PosDescY = PosDescY + 15 : PosValorY = PosDescY
                'PosDescX = X2 : PosValorX = X2 + 40
                'Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("NomTipOrdServ"))
                'InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)

                'LINEA 4
                'campo += 1
                'Descripcion = "ORD.SER."
                'PosDescY = PosDescY + 15 : PosValorY = PosDescY
                'PosDescX = X4 : PosValorX = X4 + 80
                'Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idOrdenSer")
                'InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)

                'LINEA 5
                'campo += 1
                'Descripcion = "FECHA:"
                'Valor = CDate(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("fechaCaptura")).ToShortDateString
                'PosDescY = PosDescY + 15 : PosValorY = PosDescY
                'PosDescX = X4 : PosValorX = PosDescX + 50
                'InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

                'LINEA 5.1
                'campo += 1
                'Descripcion = "HORA:"
                'Valor = CDate(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("fechaCaptura")).ToShortTimeString
                'PosDescY = PosDescY + 15 : PosValorY = PosDescY
                'PosDescX = X4 : PosValorX = PosDescX + 40
                'InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

                'LINEA 6
                'campo += 1
                'PosDescY = PosDescY + 30 : PosValorY = PosDescY
                'Descripcion = "TRACTOR: "
                'Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idTractor"))
                'PosDescX = X1 : PosValorX = PosDescX + 70
                'InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

                campo += 1
                Descripcion = "REMOLQUE 1:"
                Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idTolva1"))
                PosDescX = X2 : PosValorX = PosDescX + 95
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

                'LINEA 7
                campo += 1
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                Descripcion = "DOLLY: "
                Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idDolly"))
                PosDescX = X1 : PosValorX = PosDescX + 50
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

                campo += 1
                Descripcion = "REMOLQUE 2:"
                Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idTolva2"))
                PosDescX = X2 : PosValorX = PosDescX + 95
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

                'campo += 1
                'PosDescY = PosDescY + 30 : PosValorY = PosDescY
                'Descripcion = ""
                'If bOrdenTrab Then
                '    Valor = "REPARACIONES SOLICITADAS"
                'Else
                '    Valor = "DESCRIPCION DEL SERVICIO"
                'End If
                'PosDescX = X2 : PosValorX = PosDescX + 25
                'InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)
                'PosDescY = PosDescY + 15 : PosValorY = PosDescY

                'DETALLE
                For j As Integer = 0 To DsRep.Tables(ImpRep.NomTabla).DefaultView.Count - 1
                    If bOrdenTrab Then
                        If DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NomServicio") <> "" Then
                            campo += 1
                            PosDescY = PosDescY + 15 : PosValorY = PosDescY
                            Descripcion = ""
                            Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NomServicio")
                            PosDescX = X1 : PosValorX = PosDescX + 25
                            InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                        Else
                            If DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NombreAct") <> "" Then
                                campo += 1
                                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                                Descripcion = ""
                                Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NombreAct")
                                PosDescX = X1 : PosValorX = PosDescX + 25
                                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                            Else
                                'salto de linea
                                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                            End If
                        End If

                        campo += 1
                        Descripcion = ""
                        Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NotaRecepcion")
                        PosDescX = X2 : PosValorX = PosDescX + 25
                        InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                    Else
                        campo += 1
                        PosDescY = PosDescY + 15 : PosValorY = PosDescY
                        Descripcion = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("idUnidadTrans")
                        Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NomServicio")
                        PosDescX = X2 - 35 : PosValorX = X2 + 25
                        InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                        'PosDescY = PosDescY + 15

                    End If
                Next
                'LINEA 11
                'campo += 1
                'PosDescY = PosDescY + 290 : PosValorY = PosDescY
                'Descripcion = ""
                'Valor = "___________________"
                'PosDescX = X1 : PosValorX = PosDescX + 25
                'InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)

                'campo += 1
                'Descripcion = ""
                'Valor = "___________________"
                'PosDescX = X3A : PosValorX = PosDescX + 25
                'InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)

                'LINEA 12
                'campo += 1
                'PosDescY = PosDescY + 15 : PosValorY = PosDescY
                'Descripcion = ""
                'Valor = " ( OPERADOR ) "
                'PosDescX = X1 : PosValorX = PosDescX + 35
                'InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)

                'campo += 1
                'Descripcion = ""
                'Valor = " ( AUTORIZ� ) "
                'PosDescX = X3A : PosValorX = PosDescX + 35
                'InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)

                'LINEA 13
                'campo += 1
                'PosDescY = PosDescY + 15 : PosValorY = PosDescY
                'Descripcion = ""
                'Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("NomOperador"))
                'PosDescX = X1 : PosValorX = PosDescX + 25
                'InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)

                'campo += 1
                'Descripcion = ""
                'Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("NomAutoriza"))
                'PosDescX = X3A : PosValorX = PosDescX + 25
                'InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)

                'LINEA 14
                'CODIGO DE BARRAS ORDEN SERVICIO
                campo += 1
                Descripcion = ""
                PosDescY = PosDescY + 75 : PosValorY = PosDescY
                PosDescX = X4 : PosValorX = X4
                Valor = FormarBarCode(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idPadreOrdSer"))
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 2)

                'LINEA 15
                campo += 1
                Descripcion = "FOLIO: "
                PosDescY = PosDescY + 45 : PosValorY = PosDescY
                PosDescX = X4 : PosValorX = X4 + 60
                Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idPadreOrdSer")
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)

            Next
        End If
    End Sub

End Class
