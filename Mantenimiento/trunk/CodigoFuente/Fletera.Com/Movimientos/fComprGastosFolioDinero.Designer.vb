﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class fComprGastosFolioDinero
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.ToolStripMenu = New System.Windows.Forms.ToolStrip()
        Me.txtFolio = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtNoRecibo = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.dtpFecha = New System.Windows.Forms.DateTimePicker()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.cmbidEmpleadoRec = New System.Windows.Forms.ComboBox()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.txtSaldo = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtImporteComp = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtImporte = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.gdComprobantes = New System.Windows.Forms.DataGridView()
        Me.gcNoPago = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gcNomGasto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gcFolio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gcNoDocumento = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gcidGasto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gcNomAutoriza = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gcFecha = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gcImporte = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gcIVA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gcDeducible = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gcAutoriza = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gcObservaciones = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gcReqComprobante = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gcReqAutorizacion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtNoDocumento = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.chkNoDoc = New System.Windows.Forms.CheckBox()
        Me.txtNoPago = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtObservaciones = New System.Windows.Forms.TextBox()
        Me.cmbAutoriza = New System.Windows.Forms.ComboBox()
        Me.txtTotalC = New System.Windows.Forms.TextBox()
        Me.txtIVAC = New System.Windows.Forms.TextBox()
        Me.txtImporteC = New System.Windows.Forms.TextBox()
        Me.dtpFechaC = New System.Windows.Forms.DateTimePicker()
        Me.cmbidGasto = New System.Windows.Forms.ComboBox()
        Me.chkAutoriza = New System.Windows.Forms.CheckBox()
        Me.btnCancelaC = New System.Windows.Forms.Button()
        Me.btnBorraC = New System.Windows.Forms.Button()
        Me.btnAgregaC = New System.Windows.Forms.Button()
        Me.btnMnuOk = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuFinalizar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuLimpiar = New System.Windows.Forms.ToolStripButton()
        Me.btnMenuReImprimir = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuSalir = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripMenu.SuspendLayout()
        CType(Me.gdComprobantes, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'ToolStripMenu
        '
        Me.ToolStripMenu.AutoSize = False
        Me.ToolStripMenu.BackColor = System.Drawing.Color.Silver
        Me.ToolStripMenu.Dock = System.Windows.Forms.DockStyle.None
        Me.ToolStripMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnMnuOk, Me.btnMnuFinalizar, Me.btnMnuLimpiar, Me.btnMenuReImprimir, Me.btnMnuSalir})
        Me.ToolStripMenu.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow
        Me.ToolStripMenu.Location = New System.Drawing.Point(0, 0)
        Me.ToolStripMenu.Name = "ToolStripMenu"
        Me.ToolStripMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.ToolStripMenu.Size = New System.Drawing.Size(939, 42)
        Me.ToolStripMenu.TabIndex = 2
        Me.ToolStripMenu.Text = "ToolStrip1"
        '
        'txtFolio
        '
        Me.txtFolio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtFolio.ForeColor = System.Drawing.Color.Red
        Me.txtFolio.Location = New System.Drawing.Point(76, 54)
        Me.txtFolio.Name = "txtFolio"
        Me.txtFolio.Size = New System.Drawing.Size(94, 20)
        Me.txtFolio.TabIndex = 132
        Me.txtFolio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label6.Location = New System.Drawing.Point(9, 57)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(49, 13)
        Me.Label6.TabIndex = 133
        Me.Label6.Text = "No. Folio"
        '
        'txtNoRecibo
        '
        Me.txtNoRecibo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNoRecibo.ForeColor = System.Drawing.Color.Red
        Me.txtNoRecibo.Location = New System.Drawing.Point(250, 54)
        Me.txtNoRecibo.Name = "txtNoRecibo"
        Me.txtNoRecibo.ReadOnly = True
        Me.txtNoRecibo.Size = New System.Drawing.Size(94, 20)
        Me.txtNoRecibo.TabIndex = 134
        Me.txtNoRecibo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label4.Location = New System.Drawing.Point(183, 57)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(64, 13)
        Me.Label4.TabIndex = 135
        Me.Label4.Text = "No. Recibo:"
        '
        'dtpFecha
        '
        Me.dtpFecha.Enabled = False
        Me.dtpFecha.Location = New System.Drawing.Point(76, 86)
        Me.dtpFecha.Name = "dtpFecha"
        Me.dtpFecha.Size = New System.Drawing.Size(225, 20)
        Me.dtpFecha.TabIndex = 136
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label10.Location = New System.Drawing.Point(30, 86)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(40, 13)
        Me.Label10.TabIndex = 137
        Me.Label10.Text = "Fecha:"
        '
        'cmbidEmpleadoRec
        '
        Me.cmbidEmpleadoRec.FormattingEnabled = True
        Me.cmbidEmpleadoRec.Location = New System.Drawing.Point(76, 117)
        Me.cmbidEmpleadoRec.Name = "cmbidEmpleadoRec"
        Me.cmbidEmpleadoRec.Size = New System.Drawing.Size(244, 21)
        Me.cmbidEmpleadoRec.TabIndex = 138
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label37.Location = New System.Drawing.Point(26, 120)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(44, 13)
        Me.Label37.TabIndex = 139
        Me.Label37.Text = "Recibe:"
        '
        'txtSaldo
        '
        Me.txtSaldo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSaldo.ForeColor = System.Drawing.Color.Red
        Me.txtSaldo.Location = New System.Drawing.Point(186, 241)
        Me.txtSaldo.Name = "txtSaldo"
        Me.txtSaldo.Size = New System.Drawing.Size(136, 20)
        Me.txtSaldo.TabIndex = 144
        Me.txtSaldo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label11.Location = New System.Drawing.Point(110, 244)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(37, 13)
        Me.Label11.TabIndex = 145
        Me.Label11.Text = "Saldo:"
        '
        'txtImporteComp
        '
        Me.txtImporteComp.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtImporteComp.ForeColor = System.Drawing.Color.Red
        Me.txtImporteComp.Location = New System.Drawing.Point(186, 215)
        Me.txtImporteComp.Name = "txtImporteComp"
        Me.txtImporteComp.Size = New System.Drawing.Size(136, 20)
        Me.txtImporteComp.TabIndex = 141
        Me.txtImporteComp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label8.Location = New System.Drawing.Point(110, 218)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(70, 13)
        Me.Label8.TabIndex = 143
        Me.Label8.Text = "Comprobado:"
        '
        'txtImporte
        '
        Me.txtImporte.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtImporte.ForeColor = System.Drawing.Color.Red
        Me.txtImporte.Location = New System.Drawing.Point(186, 189)
        Me.txtImporte.Name = "txtImporte"
        Me.txtImporte.Size = New System.Drawing.Size(136, 20)
        Me.txtImporte.TabIndex = 140
        Me.txtImporte.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label5.Location = New System.Drawing.Point(110, 192)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(45, 13)
        Me.Label5.TabIndex = 142
        Me.Label5.Text = "Importe:"
        '
        'gdComprobantes
        '
        Me.gdComprobantes.AllowUserToAddRows = False
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.gdComprobantes.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle10
        Me.gdComprobantes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gdComprobantes.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.gcNoPago, Me.gcNomGasto, Me.gcFolio, Me.gcNoDocumento, Me.gcidGasto, Me.gcNomAutoriza, Me.gcFecha, Me.gcImporte, Me.gcIVA, Me.gcDeducible, Me.gcAutoriza, Me.gcObservaciones, Me.gcReqComprobante, Me.gcReqAutorizacion})
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.gdComprobantes.DefaultCellStyle = DataGridViewCellStyle11
        Me.gdComprobantes.Location = New System.Drawing.Point(12, 347)
        Me.gdComprobantes.Name = "gdComprobantes"
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.gdComprobantes.RowHeadersDefaultCellStyle = DataGridViewCellStyle12
        Me.gdComprobantes.Size = New System.Drawing.Size(781, 188)
        Me.gdComprobantes.TabIndex = 146
        '
        'gcNoPago
        '
        Me.gcNoPago.HeaderText = "NoPago"
        Me.gcNoPago.Name = "gcNoPago"
        '
        'gcNomGasto
        '
        Me.gcNomGasto.HeaderText = "NomGasto"
        Me.gcNomGasto.Name = "gcNomGasto"
        '
        'gcFolio
        '
        Me.gcFolio.HeaderText = "Folio"
        Me.gcFolio.Name = "gcFolio"
        Me.gcFolio.Visible = False
        '
        'gcNoDocumento
        '
        Me.gcNoDocumento.HeaderText = "NoDocumento"
        Me.gcNoDocumento.Name = "gcNoDocumento"
        '
        'gcidGasto
        '
        Me.gcidGasto.HeaderText = "idGasto"
        Me.gcidGasto.Name = "gcidGasto"
        Me.gcidGasto.Visible = False
        '
        'gcNomAutoriza
        '
        Me.gcNomAutoriza.HeaderText = "NomAutoriza"
        Me.gcNomAutoriza.Name = "gcNomAutoriza"
        Me.gcNomAutoriza.Visible = False
        '
        'gcFecha
        '
        Me.gcFecha.HeaderText = "Fecha"
        Me.gcFecha.Name = "gcFecha"
        '
        'gcImporte
        '
        Me.gcImporte.HeaderText = "Importe"
        Me.gcImporte.Name = "gcImporte"
        '
        'gcIVA
        '
        Me.gcIVA.HeaderText = "IVA"
        Me.gcIVA.Name = "gcIVA"
        '
        'gcDeducible
        '
        Me.gcDeducible.HeaderText = "Deducible"
        Me.gcDeducible.Name = "gcDeducible"
        Me.gcDeducible.Visible = False
        '
        'gcAutoriza
        '
        Me.gcAutoriza.HeaderText = "Autoriza"
        Me.gcAutoriza.Name = "gcAutoriza"
        Me.gcAutoriza.Visible = False
        '
        'gcObservaciones
        '
        Me.gcObservaciones.HeaderText = "Observaciones"
        Me.gcObservaciones.Name = "gcObservaciones"
        '
        'gcReqComprobante
        '
        Me.gcReqComprobante.HeaderText = "ReqComprobante"
        Me.gcReqComprobante.Name = "gcReqComprobante"
        Me.gcReqComprobante.Visible = False
        '
        'gcReqAutorizacion
        '
        Me.gcReqAutorizacion.HeaderText = "ReqAutorizacion"
        Me.gcReqAutorizacion.Name = "gcReqAutorizacion"
        Me.gcReqAutorizacion.Visible = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtNoDocumento)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.chkNoDoc)
        Me.GroupBox1.Controls.Add(Me.txtNoPago)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.btnCancelaC)
        Me.GroupBox1.Controls.Add(Me.btnBorraC)
        Me.GroupBox1.Controls.Add(Me.btnAgregaC)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.txtObservaciones)
        Me.GroupBox1.Controls.Add(Me.cmbAutoriza)
        Me.GroupBox1.Controls.Add(Me.txtTotalC)
        Me.GroupBox1.Controls.Add(Me.txtIVAC)
        Me.GroupBox1.Controls.Add(Me.txtImporteC)
        Me.GroupBox1.Controls.Add(Me.dtpFechaC)
        Me.GroupBox1.Controls.Add(Me.cmbidGasto)
        Me.GroupBox1.Controls.Add(Me.chkAutoriza)
        Me.GroupBox1.Location = New System.Drawing.Point(356, 45)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(437, 285)
        Me.GroupBox1.TabIndex = 170
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Comprobante"
        '
        'txtNoDocumento
        '
        Me.txtNoDocumento.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNoDocumento.Enabled = False
        Me.txtNoDocumento.ForeColor = System.Drawing.Color.Red
        Me.txtNoDocumento.Location = New System.Drawing.Point(121, 102)
        Me.txtNoDocumento.Name = "txtNoDocumento"
        Me.txtNoDocumento.Size = New System.Drawing.Size(188, 20)
        Me.txtNoDocumento.TabIndex = 4
        Me.txtNoDocumento.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label13.Location = New System.Drawing.Point(6, 262)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(44, 13)
        Me.Label13.TabIndex = 192
        Me.Label13.Text = "Observ."
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label1.Location = New System.Drawing.Point(6, 236)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(42, 13)
        Me.Label1.TabIndex = 191
        Me.Label1.Text = "TOTAL"
        '
        'chkNoDoc
        '
        Me.chkNoDoc.AutoSize = True
        Me.chkNoDoc.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.chkNoDoc.Location = New System.Drawing.Point(6, 104)
        Me.chkNoDoc.Name = "chkNoDoc"
        Me.chkNoDoc.Size = New System.Drawing.Size(104, 17)
        Me.chkNoDoc.TabIndex = 190
        Me.chkNoDoc.Text = "No. Documento:"
        Me.chkNoDoc.UseVisualStyleBackColor = True
        '
        'txtNoPago
        '
        Me.txtNoPago.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNoPago.Enabled = False
        Me.txtNoPago.ForeColor = System.Drawing.Color.Red
        Me.txtNoPago.Location = New System.Drawing.Point(213, 9)
        Me.txtNoPago.Name = "txtNoPago"
        Me.txtNoPago.Size = New System.Drawing.Size(96, 20)
        Me.txtNoPago.TabIndex = 1
        Me.txtNoPago.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label12.Location = New System.Drawing.Point(152, 12)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(55, 13)
        Me.Label12.TabIndex = 188
        Me.Label12.Text = "No. Pago:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label9.Location = New System.Drawing.Point(1, 210)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(33, 13)
        Me.Label9.TabIndex = 184
        Me.Label9.Text = "I.V.A."
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label7.Location = New System.Drawing.Point(1, 188)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(45, 13)
        Me.Label7.TabIndex = 183
        Me.Label7.Text = "Importe:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label3.Location = New System.Drawing.Point(3, 162)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(40, 13)
        Me.Label3.TabIndex = 182
        Me.Label3.Text = "Fecha:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label2.Location = New System.Drawing.Point(3, 46)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(62, 13)
        Me.Label2.TabIndex = 181
        Me.Label2.Text = "Tipo Gasto:"
        '
        'txtObservaciones
        '
        Me.txtObservaciones.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservaciones.ForeColor = System.Drawing.Color.Red
        Me.txtObservaciones.Location = New System.Drawing.Point(56, 259)
        Me.txtObservaciones.Name = "txtObservaciones"
        Me.txtObservaciones.Size = New System.Drawing.Size(375, 20)
        Me.txtObservaciones.TabIndex = 9
        Me.txtObservaciones.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cmbAutoriza
        '
        Me.cmbAutoriza.FormattingEnabled = True
        Me.cmbAutoriza.Location = New System.Drawing.Point(121, 75)
        Me.cmbAutoriza.Name = "cmbAutoriza"
        Me.cmbAutoriza.Size = New System.Drawing.Size(188, 21)
        Me.cmbAutoriza.TabIndex = 3
        '
        'txtTotalC
        '
        Me.txtTotalC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTotalC.ForeColor = System.Drawing.Color.Red
        Me.txtTotalC.Location = New System.Drawing.Point(171, 233)
        Me.txtTotalC.Name = "txtTotalC"
        Me.txtTotalC.Size = New System.Drawing.Size(136, 20)
        Me.txtTotalC.TabIndex = 8
        Me.txtTotalC.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtIVAC
        '
        Me.txtIVAC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtIVAC.ForeColor = System.Drawing.Color.Red
        Me.txtIVAC.Location = New System.Drawing.Point(171, 207)
        Me.txtIVAC.Name = "txtIVAC"
        Me.txtIVAC.Size = New System.Drawing.Size(136, 20)
        Me.txtIVAC.TabIndex = 7
        Me.txtIVAC.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtImporteC
        '
        Me.txtImporteC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtImporteC.ForeColor = System.Drawing.Color.Red
        Me.txtImporteC.Location = New System.Drawing.Point(171, 181)
        Me.txtImporteC.Name = "txtImporteC"
        Me.txtImporteC.Size = New System.Drawing.Size(136, 20)
        Me.txtImporteC.TabIndex = 6
        Me.txtImporteC.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'dtpFechaC
        '
        Me.dtpFechaC.CustomFormat = "dd/MMMM/yyyy"
        Me.dtpFechaC.Enabled = False
        Me.dtpFechaC.Location = New System.Drawing.Point(121, 155)
        Me.dtpFechaC.Name = "dtpFechaC"
        Me.dtpFechaC.Size = New System.Drawing.Size(188, 20)
        Me.dtpFechaC.TabIndex = 5
        '
        'cmbidGasto
        '
        Me.cmbidGasto.FormattingEnabled = True
        Me.cmbidGasto.Location = New System.Drawing.Point(121, 38)
        Me.cmbidGasto.Name = "cmbidGasto"
        Me.cmbidGasto.Size = New System.Drawing.Size(188, 21)
        Me.cmbidGasto.TabIndex = 2
        '
        'chkAutoriza
        '
        Me.chkAutoriza.AutoSize = True
        Me.chkAutoriza.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.chkAutoriza.Location = New System.Drawing.Point(4, 77)
        Me.chkAutoriza.Name = "chkAutoriza"
        Me.chkAutoriza.Size = New System.Drawing.Size(67, 17)
        Me.chkAutoriza.TabIndex = 170
        Me.chkAutoriza.Text = "Autoriza:"
        Me.chkAutoriza.UseVisualStyleBackColor = True
        '
        'btnCancelaC
        '
        Me.btnCancelaC.Image = Global.Fletera.My.Resources.Resources._1498185828_stock_undo
        Me.btnCancelaC.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnCancelaC.Location = New System.Drawing.Point(356, 73)
        Me.btnCancelaC.Name = "btnCancelaC"
        Me.btnCancelaC.Size = New System.Drawing.Size(75, 55)
        Me.btnCancelaC.TabIndex = 187
        Me.btnCancelaC.Text = "Cancela"
        Me.btnCancelaC.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnCancelaC.UseVisualStyleBackColor = True
        '
        'btnBorraC
        '
        Me.btnBorraC.Image = Global.Fletera.My.Resources.Resources.deleteCancelar
        Me.btnBorraC.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnBorraC.Location = New System.Drawing.Point(356, 134)
        Me.btnBorraC.Name = "btnBorraC"
        Me.btnBorraC.Size = New System.Drawing.Size(75, 55)
        Me.btnBorraC.TabIndex = 186
        Me.btnBorraC.Text = "Borra"
        Me.btnBorraC.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnBorraC.UseVisualStyleBackColor = True
        '
        'btnAgregaC
        '
        Me.btnAgregaC.Image = Global.Fletera.My.Resources.Resources._1487050106_add
        Me.btnAgregaC.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnAgregaC.Location = New System.Drawing.Point(356, 12)
        Me.btnAgregaC.Name = "btnAgregaC"
        Me.btnAgregaC.Size = New System.Drawing.Size(75, 55)
        Me.btnAgregaC.TabIndex = 185
        Me.btnAgregaC.Text = "Agrega"
        Me.btnAgregaC.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnAgregaC.UseVisualStyleBackColor = True
        '
        'btnMnuOk
        '
        Me.btnMnuOk.ForeColor = System.Drawing.Color.Red
        Me.btnMnuOk.Image = Global.Fletera.My.Resources.Resources.Save1
        Me.btnMnuOk.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuOk.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuOk.Name = "btnMnuOk"
        Me.btnMnuOk.Size = New System.Drawing.Size(56, 39)
        Me.btnMnuOk.Text = "&GRABAR"
        Me.btnMnuOk.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuFinalizar
        '
        Me.btnMnuFinalizar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuFinalizar.Image = Global.Fletera.My.Resources.Resources.fin2
        Me.btnMnuFinalizar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuFinalizar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuFinalizar.Name = "btnMnuFinalizar"
        Me.btnMnuFinalizar.Size = New System.Drawing.Size(68, 39)
        Me.btnMnuFinalizar.Text = "&FINALIZAR"
        Me.btnMnuFinalizar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuLimpiar
        '
        Me.btnMnuLimpiar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuLimpiar.Image = Global.Fletera.My.Resources.Resources.signos_f_059
        Me.btnMnuLimpiar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuLimpiar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuLimpiar.Name = "btnMnuLimpiar"
        Me.btnMnuLimpiar.Size = New System.Drawing.Size(56, 39)
        Me.btnMnuLimpiar.Text = "&LIMPIAR"
        Me.btnMnuLimpiar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMenuReImprimir
        '
        Me.btnMenuReImprimir.ForeColor = System.Drawing.Color.Red
        Me.btnMenuReImprimir.Image = Global.Fletera.My.Resources.Resources.impresora
        Me.btnMenuReImprimir.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMenuReImprimir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMenuReImprimir.Name = "btnMenuReImprimir"
        Me.btnMenuReImprimir.Size = New System.Drawing.Size(63, 39)
        Me.btnMenuReImprimir.Text = "&IMPRIMIR"
        Me.btnMenuReImprimir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuSalir
        '
        Me.btnMnuSalir.ForeColor = System.Drawing.Color.Red
        Me.btnMnuSalir.Image = Global.Fletera.My.Resources.Resources.Salida2
        Me.btnMnuSalir.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuSalir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuSalir.Name = "btnMnuSalir"
        Me.btnMnuSalir.Size = New System.Drawing.Size(41, 39)
        Me.btnMnuSalir.Text = "&SALIR"
        Me.btnMnuSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'fComprGastosFolioDinero
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(809, 539)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.gdComprobantes)
        Me.Controls.Add(Me.txtSaldo)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.txtImporteComp)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.txtImporte)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.cmbidEmpleadoRec)
        Me.Controls.Add(Me.Label37)
        Me.Controls.Add(Me.dtpFecha)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.txtNoRecibo)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtFolio)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.ToolStripMenu)
        Me.Name = "fComprGastosFolioDinero"
        Me.Text = "fComprGastosFolioDinero"
        Me.ToolStripMenu.ResumeLayout(False)
        Me.ToolStripMenu.PerformLayout()
        CType(Me.gdComprobantes, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents ToolStripMenu As ToolStrip
    Friend WithEvents btnMnuOk As ToolStripButton
    Friend WithEvents btnMnuFinalizar As ToolStripButton
    Friend WithEvents btnMnuLimpiar As ToolStripButton
    Friend WithEvents btnMenuReImprimir As ToolStripButton
    Friend WithEvents btnMnuSalir As ToolStripButton
    Friend WithEvents txtFolio As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents txtNoRecibo As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents dtpFecha As DateTimePicker
    Friend WithEvents Label10 As Label
    Friend WithEvents cmbidEmpleadoRec As ComboBox
    Friend WithEvents Label37 As Label
    Friend WithEvents txtSaldo As TextBox
    Friend WithEvents Label11 As Label
    Friend WithEvents txtImporteComp As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents txtImporte As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents gdComprobantes As DataGridView
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Label9 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents txtObservaciones As TextBox
    Friend WithEvents cmbAutoriza As ComboBox
    Friend WithEvents txtTotalC As TextBox
    Friend WithEvents txtIVAC As TextBox
    Friend WithEvents txtImporteC As TextBox
    Friend WithEvents dtpFechaC As DateTimePicker
    Friend WithEvents cmbidGasto As ComboBox
    Friend WithEvents chkAutoriza As CheckBox
    Friend WithEvents btnCancelaC As Button
    Friend WithEvents btnBorraC As Button
    Friend WithEvents btnAgregaC As Button
    Friend WithEvents txtNoPago As TextBox
    Friend WithEvents Label12 As Label
    Friend WithEvents chkNoDoc As CheckBox
    Friend WithEvents Label13 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents txtNoDocumento As TextBox
    Friend WithEvents gcNoPago As DataGridViewTextBoxColumn
    Friend WithEvents gcNomGasto As DataGridViewTextBoxColumn
    Friend WithEvents gcFolio As DataGridViewTextBoxColumn
    Friend WithEvents gcNoDocumento As DataGridViewTextBoxColumn
    Friend WithEvents gcidGasto As DataGridViewTextBoxColumn
    Friend WithEvents gcNomAutoriza As DataGridViewTextBoxColumn
    Friend WithEvents gcFecha As DataGridViewTextBoxColumn
    Friend WithEvents gcImporte As DataGridViewTextBoxColumn
    Friend WithEvents gcIVA As DataGridViewTextBoxColumn
    Friend WithEvents gcDeducible As DataGridViewTextBoxColumn
    Friend WithEvents gcAutoriza As DataGridViewTextBoxColumn
    Friend WithEvents gcObservaciones As DataGridViewTextBoxColumn
    Friend WithEvents gcReqComprobante As DataGridViewTextBoxColumn
    Friend WithEvents gcReqAutorizacion As DataGridViewTextBoxColumn
End Class
