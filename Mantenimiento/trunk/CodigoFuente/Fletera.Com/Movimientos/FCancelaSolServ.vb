'Fecha: 6 / Enero / 2017
'ROGER GALA PACHECO
'************************************************************************************************************************************
'*  Fecha Modificacion :   15 / Febrero / 2016                                                                                       
'*  
'************************************************************************************************************************************
'Imports LinqToExcel

Public Class FCancelaSolServ
    Inherits System.Windows.Forms.Form

    'Private _Tag As String

    Dim ObjSql As New ToolSQLs
    Dim StrSql As String
    Dim indice As Integer = 0
    Dim ArraySql() As String

    'Private _Opcion As TipoOpcionEstatus

    'Private OrdenTrabajo As OrdenTrabajoClass
    'Private Personal As PersonalClass
    'Private PersonalPuesto As PuestosClass
    'Private FamiliaOT As FamiliasClass
    'Private OrdenServ As OrdenServClass
    'Private TipOrd As TipoOrdenClass
    'Private User As UsuarioClass
    'Private DetOS As detOrdenServicioClass
    'Private Emp As EmpresaClass
    'Private Marca As MarcaClass
    'Private UniTrans As UnidadesTranspClass
    'Private ULTOT As OrdenTrabajoClass
    Dim Salida As Boolean

    Private _idPadreOrdSer As Integer
    Private _NomEmpresa As String
    Private _FechaSolicitud As Date
    Private _nomUsuario As String



    Public WithEvents MeStatus1 As ControlStatus.MeStatus
    Friend WithEvents btnMnuOk As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuSalir As System.Windows.Forms.ToolStripButton
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtMotivoCancela As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtNomEmpresa As System.Windows.Forms.TextBox
    Friend WithEvents dtpFechaCapturaHora As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpFechaCap As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtidPadreOrdSer As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label



#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents ToolStripMenu As System.Windows.Forms.ToolStrip
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FCancelaSolServ))
        Me.ToolStripMenu = New System.Windows.Forms.ToolStrip()
        Me.btnMnuOk = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuSalir = New System.Windows.Forms.ToolStripButton()
        Me.MeStatus1 = New ControlStatus.MeStatus()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtMotivoCancela = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtNomEmpresa = New System.Windows.Forms.TextBox()
        Me.dtpFechaCapturaHora = New System.Windows.Forms.DateTimePicker()
        Me.dtpFechaCap = New System.Windows.Forms.DateTimePicker()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtidPadreOrdSer = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.ToolStripMenu.SuspendLayout()
        Me.SuspendLayout()
        '
        'ToolStripMenu
        '
        Me.ToolStripMenu.AutoSize = False
        Me.ToolStripMenu.BackColor = System.Drawing.Color.Silver
        Me.ToolStripMenu.Dock = System.Windows.Forms.DockStyle.None
        Me.ToolStripMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnMnuOk, Me.btnMnuSalir})
        Me.ToolStripMenu.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow
        Me.ToolStripMenu.Location = New System.Drawing.Point(3, 0)
        Me.ToolStripMenu.Name = "ToolStripMenu"
        Me.ToolStripMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.ToolStripMenu.Size = New System.Drawing.Size(895, 42)
        Me.ToolStripMenu.TabIndex = 1
        Me.ToolStripMenu.Text = "ToolStrip1"
        '
        'btnMnuOk
        '
        Me.btnMnuOk.ForeColor = System.Drawing.Color.Red
        Me.btnMnuOk.Image = Global.Fletera.My.Resources.Resources.Save1
        Me.btnMnuOk.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuOk.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuOk.Name = "btnMnuOk"
        Me.btnMnuOk.Size = New System.Drawing.Size(56, 39)
        Me.btnMnuOk.Text = "&GRABAR"
        Me.btnMnuOk.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuSalir
        '
        Me.btnMnuSalir.ForeColor = System.Drawing.Color.Red
        Me.btnMnuSalir.Image = Global.Fletera.My.Resources.Resources.Salida2
        Me.btnMnuSalir.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuSalir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuSalir.Name = "btnMnuSalir"
        Me.btnMnuSalir.Size = New System.Drawing.Size(41, 39)
        Me.btnMnuSalir.Text = "&SALIR"
        Me.btnMnuSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'MeStatus1
        '
        Me.MeStatus1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.MeStatus1.Location = New System.Drawing.Point(0, 192)
        Me.MeStatus1.Margin = New System.Windows.Forms.Padding(4)
        Me.MeStatus1.Name = "MeStatus1"
        Me.MeStatus1.Size = New System.Drawing.Size(879, 26)
        Me.MeStatus1.TabIndex = 72
        '
        'Label11
        '
        Me.Label11.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label11.Location = New System.Drawing.Point(13, 101)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(79, 20)
        Me.Label11.TabIndex = 74
        Me.Label11.Text = "Motivo:"
        '
        'txtMotivoCancela
        '
        Me.txtMotivoCancela.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMotivoCancela.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMotivoCancela.Location = New System.Drawing.Point(93, 94)
        Me.txtMotivoCancela.MaxLength = 250
        Me.txtMotivoCancela.Multiline = True
        Me.txtMotivoCancela.Name = "txtMotivoCancela"
        Me.txtMotivoCancela.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtMotivoCancela.Size = New System.Drawing.Size(774, 54)
        Me.txtMotivoCancela.TabIndex = 73
        '
        'Label9
        '
        Me.Label9.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label9.Location = New System.Drawing.Point(174, 61)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(54, 20)
        Me.Label9.TabIndex = 106
        Me.Label9.Text = "Empresa:"
        '
        'txtNomEmpresa
        '
        Me.txtNomEmpresa.Enabled = False
        Me.txtNomEmpresa.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNomEmpresa.Location = New System.Drawing.Point(234, 58)
        Me.txtNomEmpresa.Name = "txtNomEmpresa"
        Me.txtNomEmpresa.ReadOnly = True
        Me.txtNomEmpresa.Size = New System.Drawing.Size(210, 20)
        Me.txtNomEmpresa.TabIndex = 105
        '
        'dtpFechaCapturaHora
        '
        Me.dtpFechaCapturaHora.Enabled = False
        Me.dtpFechaCapturaHora.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtpFechaCapturaHora.Location = New System.Drawing.Point(767, 55)
        Me.dtpFechaCapturaHora.Name = "dtpFechaCapturaHora"
        Me.dtpFechaCapturaHora.Size = New System.Drawing.Size(83, 20)
        Me.dtpFechaCapturaHora.TabIndex = 103
        '
        'dtpFechaCap
        '
        Me.dtpFechaCap.Enabled = False
        Me.dtpFechaCap.Location = New System.Drawing.Point(536, 55)
        Me.dtpFechaCap.Name = "dtpFechaCap"
        Me.dtpFechaCap.Size = New System.Drawing.Size(225, 20)
        Me.dtpFechaCap.TabIndex = 102
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label10.Location = New System.Drawing.Point(490, 55)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(40, 13)
        Me.Label10.TabIndex = 104
        Me.Label10.Text = "Fecha:"
        '
        'txtidPadreOrdSer
        '
        Me.txtidPadreOrdSer.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtidPadreOrdSer.ForeColor = System.Drawing.Color.Red
        Me.txtidPadreOrdSer.Location = New System.Drawing.Point(99, 59)
        Me.txtidPadreOrdSer.Name = "txtidPadreOrdSer"
        Me.txtidPadreOrdSer.ReadOnly = True
        Me.txtidPadreOrdSer.Size = New System.Drawing.Size(69, 20)
        Me.txtidPadreOrdSer.TabIndex = 107
        Me.txtidPadreOrdSer.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label13.Location = New System.Drawing.Point(13, 62)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(41, 13)
        Me.Label13.TabIndex = 101
        Me.Label13.Text = "FOLIO:"
        '
        'FCancelaSolServ
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(879, 218)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.txtNomEmpresa)
        Me.Controls.Add(Me.dtpFechaCapturaHora)
        Me.Controls.Add(Me.dtpFechaCap)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.txtidPadreOrdSer)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.txtMotivoCancela)
        Me.Controls.Add(Me.MeStatus1)
        Me.Controls.Add(Me.ToolStripMenu)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "FCancelaSolServ"
        Me.Text = "Cancelacion de Solicitud de Orden de Servicio"
        Me.ToolStripMenu.ResumeLayout(False)
        Me.ToolStripMenu.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    'Sub New(ByVal vTag As String, Optional ByVal vNoServ As Integer = 0, Optional ByVal vOrdenTrabajo As Integer = 0, Optional ByVal vOpcion As TipoOpcionEstatus = TipoOpcionEstatus.SinOpcion)
    '    InitializeComponent()
    '    _Tag = vTag
    '    _OrdenServicio = vNoServ
    '    _OrdenTrabajo = vOrdenTrabajo
    '    _Opcion = vOpcion

    'End Sub
    Sub New(Optional ByVal idPadreOrdSer As Integer = 0,
            Optional ByVal NomEmpresa As String = "",
            Optional ByVal FechaSolicitud As Date = #1/1/2017#,
             Optional ByVal nomUsuario As String = "")

        _idPadreOrdSer = idPadreOrdSer
        _NomEmpresa = NomEmpresa
        _FechaSolicitud = FechaSolicitud
        _nomUsuario = nomUsuario




        InitializeComponent()
        '_Tag = vTag
        '_OrdenServicio = vNoServ
        '_OrdenTrabajo = vOrdenTrabajo
        '_Opcion = vOpcion

    End Sub


    Private Sub btnMnuSalir_Click(sender As Object, e As EventArgs) Handles btnMnuSalir.Click
        Me.Close()
    End Sub

    Private Sub LimpiaCampos()
        txtNomEmpresa.Text = ""
        txtMotivoCancela.Text = ""
        txtidPadreOrdSer.Text = ""

    End Sub

    Private Sub FEstatusTaller_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        LimpiaCampos()
        txtidPadreOrdSer.Text = _idPadreOrdSer
        txtNomEmpresa.Text = _NomEmpresa
        dtpFechaCap.Value = _FechaSolicitud
        dtpFechaCapturaHora.Value = _FechaSolicitud


        'If _OrdenTrabajo > 0 Then
        '    If _Opcion = TipoOpcionEstatus.Terminar Then
        '        tabPrinc.SelectedIndex = 1
        '        txtOrdenTrabajo.Text = _OrdenTrabajo
        '        CargaForm(txtOrdenTrabajo.Text, "OrdenTrabajo")
        '        TabOS.Visible = False
        '        gpoOtCan.Visible = False
        '        gpoOT.Enabled = False
        '        txtSolucion.Focus()

        '    End If
        'End If
        'If _OrdenServicio > 0 Then
        '    If _Opcion = TipoOpcionEstatus.Entregar Then
        '        tabPrinc.SelectedIndex = 0
        '        txtOrdenServ.Text = _OrdenServicio
        '        CargaForm(txtOrdenServ.Text, "OrdenServicio")
        '    End If
        'End If
    End Sub

    Private Sub CargaForm(ByVal idClave As String, ByVal NomTabla As String)
        Try
            Select Case UCase(NomTabla)
                Case UCase("UNIPROD_COM")
                    'UnidadCom = New UnidadesComClass(idClave, "CCLAVEINT", TipoDato.TdCadena)
                    'If UnidadCom.Existe Then
                    '    txtNomUniProd.Text = UnidadCom.CNOMBREUNIDAD
                    '    vCIDUNIDAD = UnidadCom.CIDUNIDAD
                    'End If
                Case UCase("PROD_COM")
                    'ProdCom = New ProductosComClass(idClave)
                    'If ProdCom.Existe Then
                    '    If ProdCom.CSTATUSPRODUCTO = 1 Then
                    '        vCIDPRODUCTO = ProdCom.CIDPRODUCTO
                    '        txtNomProducto.Text = ProdCom.CNOMBREPRODUCTO
                    '        UnidadCom = New UnidadesComClass(ProdCom.CIDUNIDADBASE, "CIDUNIDAD", TipoDato.TdNumerico)
                    '        If UnidadCom.Existe Then
                    '            txtIdUnidadProd.Text = UnidadCom.CCLAVEINT
                    '        End If

                    '    Else
                    '        'Desactivado
                    '    End If
                    'End If
                Case UCase("OrdenTrabajo")
                    'OrdenTrabajo = New OrdenTrabajoClass(idClave)
                    'With OrdenTrabajo
                    '    'ORDEN DE TRABAJO
                    '    txtOrdenTrabajo.Text = idClave
                    '    If .Existe Then

                    '        'MECANICO RESPONSABLE
                    '        Personal = New PersonalClass(.idPersonalResp)
                    '        'cmbResponsable.SelectedValue = .idPersonalResp
                    '        If Personal.Existe Then
                    '            txtNombreMecanico.Text = Personal.NombreCompleto

                    '            PersonalPuesto = New PuestosClass(Personal.idPuesto)
                    '            If PersonalPuesto.Existe Then
                    '                txtPuestoMec.Text = PersonalPuesto.NomPuesto
                    '            End If
                    '        End If
                    '        'AYUDANTE 1
                    '        If .idPersonalAyu1 > 0 Then
                    '            'chkAyu1.Checked = True
                    '            Personal = New PersonalClass(.idPersonalAyu1)
                    '            'cmbayudante1.SelectedValue = .idPersonalAyu1
                    '        Else
                    '            'chkAyu1.Checked = False
                    '        End If

                    '        'AYUDANTE 2
                    '        If .idPersonalAyu2 > 0 Then
                    '            'chkAyu2.Checked = True
                    '            Personal = New PersonalClass(.idPersonalAyu2)
                    '            'cmbayudante2.SelectedValue = .idPersonalAyu2
                    '        Else
                    '            'chkAyu2.Checked = False
                    '        End If

                    '        'FALLA DE LA ORDEN DE TRABAJO
                    '        txtFalla.Text = .NotaRecepcion
                    '        FamiliaOT = New FamiliasClass(.idFamilia)
                    '        If FamiliaOT.Existe Then
                    '            txtFamilia.Text = FamiliaOT.NomFamilia
                    '            txtDivision.Text = FamiliaOT.NomDivision
                    '        End If

                    '        'DETALLE ORDEN DE SERVICIO
                    '        DetOS = New detOrdenServicioClass(idClave)
                    '        If DetOS.Existe Then
                    '            txtDuracion.Text = DetOS.DuracionActHr
                    '            dtpFechaAsig.Value = DetOS.FechaAsig
                    '            dtpFechaAsigHora.Value = DetOS.FechaAsig
                    '            User = New UsuarioClass(DetOS.UsuarioAsig)
                    '            If User.Existe Then
                    '                txtUsuarioAsig.Text = User.NombreUsuario
                    '            End If
                    '        End If

                    '        'ORDEN DE SERVICIO
                    '        OrdenServ = New OrdenServClass(.idOrdenSer)

                    '        txtOrdenServ.Text = .idOrdenSer
                    '        dtpFechaOrden.Value = OrdenServ.FechaRecepcion
                    '        dtpFechaOrdenhr.Value = OrdenServ.FechaRecepcion

                    '        Emp = New EmpresaClass(OrdenServ.IdEmpresa)
                    '        If Emp.Existe Then
                    '            txtNomEmpresa.Text = Emp.RazonSocial
                    '        End If

                    '        TipOrd = New TipoOrdenClass(OrdenServ.idTipoOrden)
                    '        If TipOrd.Existe Then
                    '            txtTipoOrden.Text = TipOrd.NomTipoOrden
                    '        End If
                    '        txtEstatusOrden.Text = OrdenServ.Estatus
                    '        txtKilometraje.Text = OrdenServ.Kilometraje
                    '        'RECEPCIONO
                    '        User = New UsuarioClass(OrdenServ.UsuarioRecepcion)
                    '        If User.Existe Then
                    '            txtRecepciono.Text = User.NombreUsuario
                    '        End If
                    '        'ENTREGO
                    '        Personal = New PersonalClass(OrdenServ.idEmpleadoEntrega)
                    '        If Personal.Existe Then
                    '            txtEntrego.Text = Personal.NombreCompleto
                    '        End If
                    '        'AUTORIZO
                    '        Personal = New PersonalClass(OrdenServ.idEmpleadoAutoriza)
                    '        If Personal.Existe Then
                    '            txtAutorizo.Text = Personal.NombreCompleto
                    '        End If
                    '        'ORDEN DE SERVICIO

                    '        'UNIDAD DE TRANSPORTE
                    '        txtIdUnidad.Text = OrdenServ.idUnidadTrans
                    '        UniTrans = New UnidadesTranspClass(OrdenServ.idUnidadTrans)
                    '        If UniTrans.Existe Then
                    '            txtNomUnidad.Text = UniTrans.DescripcionUni
                    '            Marca = New MarcaClass(UniTrans.idMarca)
                    '            If Marca.Existe Then
                    '                txtMarcaUni.Text = Marca.NOMBREMARCA
                    '            End If


                    '        End If


                    '        'Historial
                    '        'CargaOrdenesSerHis(OrdenServ.idUnidadTrans)

                    '        'IdOrdenServ = grdOrdenServHis.Item("hosIdServicio", grdOrdenServHis.CurrentCell.RowIndex).Value
                    '        'CargaOrdenesTrabHis(txtIdUnidad.Text, IdOrdenServ)

                    '        'VALIDACIONES
                    '        If OrdenServ.Estatus = "ASIG" Then
                    '            'ActivaCampos(False, TipoOpcActivaCampos.tOpcINICIALIZA)
                    '            'ActivaBotones(True, TipoOpcActivaBoton.tOpcInicializa)

                    '            txtDuracion.Focus()
                    '            txtDuracion.SelectAll()
                    '        ElseIf OrdenServ.Estatus = "DIA" Then
                    '            MsgBox("La Orden de Trabajo: " & idClave & " ya Esta en Proceso, No puede Modificarse")
                    '            'ActivaCampos(False, TipoOpcActivaCampos.tOpcINICIALIZA)

                    '            'ActivaBotones(False, TipoOpcActivaBoton.tOpcInicializa)
                    '            btnMnuCancelar.Enabled = True


                    '        ElseIf OrdenServ.Estatus = "REC" Then
                    '            MsgBox("La Orden de Trabajo: " & idClave & " No esta Asignada, No puede Modificarse")
                    '            txtOrdenTrabajo.Focus()
                    '            txtOrdenTrabajo.SelectAll()

                    '        End If
                    '    Else
                    '        'No existe 
                    '        MsgBox("La Orden de Trabajo: " & idClave & " No Existe !!!")
                    '        txtOrdenTrabajo.Focus()
                    '        txtOrdenTrabajo.SelectAll()
                    '    End If

                    'End With
                  
                Case UCase("OrdenServicio")
                    'OrdenServ = New OrdenServClass(idClave)
                    'With OrdenServ
                    '    txtOrdenServ.Text = idClave
                    '    If .Existe Then
                    '        txtOrdenServ.Text = .idOrdenSer
                    '        dtpFechaOrden.Value = OrdenServ.FechaRecepcion
                    '        dtpFechaOrdenhr.Value = OrdenServ.FechaRecepcion

                    '        Emp = New EmpresaClass(OrdenServ.IdEmpresa)
                    '        If Emp.Existe Then
                    '            txtNomEmpresa.Text = Emp.RazonSocial
                    '        End If

                    '        TipOrd = New TipoOrdenClass(OrdenServ.idTipoOrden)
                    '        If TipOrd.Existe Then
                    '            txtTipoOrden.Text = TipOrd.NomTipoOrden
                    '        End If
                    '        txtEstatusOrden.Text = OrdenServ.Estatus
                    '        txtKilometraje.Text = OrdenServ.Kilometraje

                    '        'RECEPCIONO
                    '        User = New UsuarioClass(OrdenServ.UsuarioRecepcion)
                    '        If User.Existe Then
                    '            txtRecepciono.Text = User.NombreUsuario
                    '        End If
                    '        'ENTREGO
                    '        Personal = New PersonalClass(OrdenServ.idEmpleadoEntrega)
                    '        If Personal.Existe Then
                    '            txtEntrego.Text = Personal.NombreCompleto
                    '        End If
                    '        'AUTORIZO
                    '        Personal = New PersonalClass(OrdenServ.idEmpleadoAutoriza)
                    '        If Personal.Existe Then
                    '            txtAutorizo.Text = Personal.NombreCompleto
                    '        End If
                    '    End If
                    'End With

            End Select



        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try

    End Sub

   

    Private Sub btnMnuOk_Click(sender As Object, e As EventArgs) Handles btnMnuOk.Click
        Try
            Dim objsql As New ToolSQLs

            Dim Mensaje As String = "!! Esta seguro que desea Cancelar la Solicitud de Servicio: " & txtidPadreOrdSer.Text & "? !!"

            'If _Opcion = TipoOpcionEstatus.Terminar Then
            '    Mensaje = "!! Esta seguro que desea Terminar la Orden de Trabajo: " & txtOrdenTrabajo.Text & "? !!"
            'ElseIf _Opcion = TipoOpcionEstatus.Entregar Then
            '    Mensaje = "!! Esta seguro que desea Entregar la Unidad de Transporte: " & txtIdUnidad.Text & "? !!"
            'ElseIf _Opcion = TipoOpcionEstatus.Cancelar Then
            '    If _OrdenServicio > 0 Then
            '        Mensaje = "!! Esta seguro que desea Cancelar la Orden de Servicio: " & txtOrdenServ.Text & "? !!"
            '    ElseIf _OrdenTrabajo > 0 Then
            '        Mensaje = "!! Esta seguro que desea Cancelar la Orden de Trabajo: " & txtOrdenTrabajo.Text & "? !!"
            '    End If
            'End If

            Salida = True

            'If Not Validar() Then Exit Sub

            If MsgBox(Mensaje, MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then

                StrSql = objsql.CancelaPadre(txtidPadreOrdSer.Text, _nomUsuario, txtMotivoCancela.Text)
                ReDim Preserve ArraySql(indice)
                ArraySql(indice) = StrSql
                indice += 1


                'If _Opcion = TipoOpcionEstatus.Terminar Then

                '    OrdenServ = New OrdenServClass(Convert.ToInt32(txtOrdenServ.Text))
                '    With OrdenServ
                '        If .Existe Then
                '            If .ContEstatusxOS("TODAS") - .ContEstatusxOS("TER") = 1 Then
                '                StrSql = .ConsultaActualizaDato("TER", "Estatus", TipoDato.TdCadena)
                '                ReDim Preserve ArraySql(indice)
                '                ArraySql(indice) = StrSql
                '                indice += 1

                '                StrSql = .ConsultaActualizaDato(Now, "FechaTerminado", TipoDato.TdFecha)
                '                ReDim Preserve ArraySql(indice)
                '                ArraySql(indice) = StrSql
                '                indice += 1
                '            End If
                '        End If
                '    End With


                '    DetOS = New detOrdenServicioClass(Convert.ToInt32(txtOrdenTrabajo.Text))
                '    With DetOS
                '        If .Existe Then
                '            StrSql = .ConsultaActualizaDato(txtSolucion.Text, "Solucion", TipoDato.TdCadena)
                '            ReDim Preserve ArraySql(indice)
                '            ArraySql(indice) = StrSql
                '            indice += 1

                '            StrSql = .ConsultaActualizaDato(txtObs.Text, "Observaciones", TipoDato.TdCadena)
                '            ReDim Preserve ArraySql(indice)
                '            ArraySql(indice) = StrSql
                '            indice += 1

                '            StrSql = .ConsultaActualizaDato(Now, "FechaTerminado", TipoDato.TdFecha)
                '            ReDim Preserve ArraySql(indice)
                '            ArraySql(indice) = StrSql
                '            indice += 1

                '            StrSql = .ConsultaActualizaDato(UserId, "UsuarioTerminado", TipoDato.TdCadena)
                '            ReDim Preserve ArraySql(indice)
                '            ArraySql(indice) = StrSql
                '            indice += 1

                '            StrSql = .ConsultaActualizaDato("TER", "Estatus", TipoDato.TdCadena)
                '            ReDim Preserve ArraySql(indice)
                '            ArraySql(indice) = StrSql
                '            indice += 1

                '        End If
                '    End With
                'ElseIf _Opcion = TipoOpcionEstatus.Entregar Then

                'ElseIf _Opcion = TipoOpcionEstatus.Cancelar Then
                '    If _OrdenServicio > 0 Then

                '    ElseIf _OrdenTrabajo > 0 Then

                '    End If
                'End If

                If indice > 0 Then
                    Dim obj As New CapaNegocio.Tablas
                    Dim Respuesta As String

                    Respuesta = obj.EjecutarSql(Inicio.CONSTR, "SQL", ArraySql, Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, indice)
                    FEspera.Close()
                    If Respuesta = "EFECTUADO" Then
                        MsgBox("Solicitud de Servicio Cancelada Satisfactoriamente", MsgBoxStyle.Information, Me.Text)
                        btnMnuSalir_Click(sender, e)
                    ElseIf Respuesta = "NOEFECTUADO" Then

                    ElseIf Respuesta = "ERROR" Then
                        indice = 0
                        SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")
                    End If
                Else
                    MsgBox("No se Encontraron Registros que Grabar", MsgBoxStyle.Exclamation, Me.Text)
                End If

            End If

        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try
    End Sub

    Private Function Validar() As Boolean
        Validar = True
        If txtMotivoCancela.Text = "" Then
            MsgBox("Para poder cancelar la solicitud de servicio " & txtidPadreOrdSer.Text & " tiene que tener un Motivo ", vbInformation, "Aviso" & Me.Text)
            txtMotivoCancela.Focus()
            txtMotivoCancela.SelectAll()
            Validar = False
        End If
    End Function
End Class
