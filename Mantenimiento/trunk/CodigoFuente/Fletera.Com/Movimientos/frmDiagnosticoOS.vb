'Fecha: 15 / Febrero / 2016
'ROGER GALA PACHECO
'************************************************************************************************************************************
'*  Fecha Modificacion :   15 / Febrero / 2016                                                                                       
'*  
'************************************************************************************************************************************
Imports System.Configuration
Imports Core.Models
Imports System.Drawing.Printing
Imports System.Drawing.Text
Public Class frmDiagnosticoOS

    Inherits System.Windows.Forms.Form
    Private _Tag As String
    Dim bEsIdentidad As Boolean = True
    Dim OpcionForma As TipoOpcionForma
    Dim TablaBd As String = "Diagnostico"


    '*******************************************VALIDOS
    Dim MyTablaTipoOrdServ As DataTable = New DataTable(TablaBd)
    Dim TipOrdServ As TipoServClass
    Dim Servicio As ServicioClass
    Dim vNumOrdServ As Integer = 0
    Dim vOrdenTrabajo As Boolean = False
    Dim vTipoOrdServ As Integer = 0
    Dim vidUnidadTrans As String = ""
    Dim vNomTipOrdServ As String = ""
    Dim vTipoServ As Integer = 0

    Dim UniTrans As UnidadesTranspClass
    Dim TipoUniTrans As TipoUniTransClass
    Dim Clave As Integer
    Dim MyTablaDetalle As DataTable = New DataTable(TablaBd)
    Dim vidPadreOrdSer As Integer = 0
    Dim ArrayOrdenSer() As Integer
    Dim ArrayOrdenSerTipo() As Boolean
    Dim indiceOS As Integer = 0
    Dim ArrayOrdenTrab() As Integer
    Dim indiceOT As Integer = 0
    Dim ArraySql() As String
    Dim indice As Integer = 0

    Dim ImpRep As New ImprimeRemision
    Private prtSettings As PrinterSettings
    Private prtDoc As PrintDocument

    Dim MyPaperSize As New Printing.PaperSize("MediaCarta", 827, 584.5)
    'Dim MyPaperSize As New Printing.PaperSize()

    Dim printFont As New System.Drawing.Font("Courier New", 7)
    Dim NumRegistros As Integer
    Private lineaActual As Integer = 0
    Dim ContPaginas As Integer = 0
    Dim MyTablaImprime As DataTable = New DataTable("IMPRIME")
    Dim DsRep As DataSet
    Dim PATH_FONTS As String = ""
    Dim Empresa As EmpresaClass

    Dim PadreOS As MasterOrdServClass
    Dim vUltKm As Integer
    Dim MaximoKmdeMas As Integer = 10000

    Dim vValorNomAct As String
    Dim vValorNomDivision As String
    Dim vValorDivision As Integer

    'Dim tablaTipOS As DataTable
    '*******************************************VALIDOS


    Private OrdenTrabajo As OrdenTrabajoClass
    Private ULTOT As OrdenTrabajoClass
    Private Personal As PersonalClass
    Private PersonalPuesto As PuestosClass
    Private FamiliaOT As FamiliasClass

    Private OrdenServ As OrdenServClass
    Private TipOrd As TipoOrdenClass
    Private User As UsuarioClass
    Private Emp As EmpresaClass
    Private DetOS As detOrdenServicioClass
    Private Marca As MarcaClass

    Dim MyTablaHisOS As DataTable = New DataTable(TablaBd)
    Dim MyTablaHisOT As DataTable = New DataTable(TablaBd)
    Dim MyTablaRefac As DataTable = New DataTable(TablaBd)

    Dim MyTablaComboResp As DataTable = New DataTable(TablaBd)
    Dim MyTablaComboAyu1 As DataTable = New DataTable(TablaBd)
    Dim MyTablaComboAyu2 As DataTable = New DataTable(TablaBd)

    Dim MyTablaPersOcupado As DataTable = New DataTable(TablaBd)

    Private ProdCom As ProductosComClass
    Private UnidadCom As UnidadesComClass

    Private ObjSql As New ToolSQLs

    Dim Salida As Boolean
    Dim myDataColumn As DataColumn
    Dim myDataRow As DataRow

    Dim CantidadGrid As Double = 0
    Dim BandNoExisteProdEnGrid As Boolean = False

    Dim ObjCom As New FuncionesComClass

    Dim vCIDPRODUCTO As Integer
    Dim vCIDUNIDAD As Integer

    Dim vIdAlmacen As Integer = 1
    Dim IdOrdenServ As Integer

    Dim CadCam As String = ""
    Dim StrSql As String = ""


    Dim NoIdPreOC As Integer = 0
    Dim NoIdPreSal As Integer = 0

    Dim objNumSig As New CapaNegocio.Tablas

    Dim ContPersonal As Integer = 0
    Dim CostoManoObra As Double = 0
    Dim CostoRef As Double = 0

    Dim vLugOcupado As Integer = 0

    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents txtidPadreOrdSer As TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents dtpFechaCapturaHora As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpFechaCap As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtRemolque2 As System.Windows.Forms.TextBox
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents txtDolly As System.Windows.Forms.TextBox
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents txtRemolque1 As System.Windows.Forms.TextBox
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents txtTractor As System.Windows.Forms.TextBox
    Friend WithEvents Label33 As System.Windows.Forms.Label
    '
    'Dim CampoLLave As String = "idHerramienta"
    'Dim TablaBd2 As String = "CatFamilia"
    'Dim TablaBd3 As String = "CatTipoHerramienta"
    'Dim TablaBd4 As String = "CatMarcas"

    'Private Herramienta As HerramientaClass
    'Private Familia As FamiliasClass
    'Private TipoHerramienta As TipoHerramClass
    'Private Marca As MarcaClass

    Private _NoServ As Integer
    Private _con As String
    Private _sqlConCom As String
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Private _Usuario As String
    Private _IdUsuario As Integer
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtNomEmpresa As System.Windows.Forms.TextBox
    Friend WithEvents btnMenuReImprimir As System.Windows.Forms.ToolStripButton
    Friend WithEvents ConMnuOrdSer As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents ModificarToolStripMenuOrdSer As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CancelarToolStripMenuOrdSer As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ImprimirToolStripMenuOrdSer As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnMnuCancelar As System.Windows.Forms.ToolStripButton
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtEstatus As System.Windows.Forms.TextBox
    Private _cveEmpresa As Integer

    Dim idLlanta As String = ""
    Friend WithEvents txtTipoOrdenServ As TextBox
    Dim PosLLanta As Integer = 0
    Dim _TipoOrdenServicio As TipoOrdenServicio

    Dim Empleado As PersonalClass
    Friend WithEvents txtChofer As TextBox
    Dim TipoOrdSer As TiposOrdenServicio
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents chkAyu2 As CheckBox
    Friend WithEvents chkAyu1 As CheckBox
    Friend WithEvents cmbPuestoAyu2 As ComboBox
    Friend WithEvents cmbPuestoAyu1 As ComboBox
    Friend WithEvents cmbPuestoResp As ComboBox
    Friend WithEvents cmbayudante2 As ComboBox
    Friend WithEvents cmbayudante1 As ComboBox
    Friend WithEvents cmbResponsable As ComboBox
    Friend WithEvents Label31 As Label
    Friend WithEvents Label30 As Label
    Friend WithEvents Label29 As Label
    Friend WithEvents txtOrdenServ As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents txtEstatusOS As TextBox
    Friend WithEvents Label3 As Label
    Dim _NombreUsuario As String
    Dim dtAsignados As DataTable = New DataTable(TablaBd)
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents GrupoCaptura As GroupBox
    Friend WithEvents chkMarcaTodasOT As CheckBox
    Friend WithEvents chkMarcaTodas As CheckBox
    Friend WithEvents grdOrdenServDet As DataGridView
    Friend WithEvents grdOrdenServCab As DataGridView
    Friend WithEvents oscDiagnostico As DataGridViewCheckBoxColumn
    Friend WithEvents oscCLAVE As DataGridViewTextBoxColumn
    Friend WithEvents oscConsecutivo As DataGridViewTextBoxColumn
    Friend WithEvents oscIdOrdenSer As DataGridViewTextBoxColumn
    Friend WithEvents oscfechaCaptura As DataGridViewTextBoxColumn
    Friend WithEvents oscTipoOrdenServ As DataGridViewTextBoxColumn
    Friend WithEvents oscNomTipOrdServ As DataGridViewTextBoxColumn
    Friend WithEvents oscidUnidadTrans As DataGridViewTextBoxColumn
    Friend WithEvents oscidTipoUnidadTrans As DataGridViewTextBoxColumn
    Friend WithEvents oscnomTipoUniTras As DataGridViewTextBoxColumn
    Friend WithEvents oscKm As DataGridViewTextBoxColumn
    Friend WithEvents oscAutorizo As DataGridViewTextBoxColumn
    Friend WithEvents oscbOrdenTrabajo As DataGridViewCheckBoxColumn
    Friend WithEvents oscTipoServ As DataGridViewTextBoxColumn
    Friend WithEvents oscModificado As DataGridViewCheckBoxColumn
    Friend WithEvents oscCancelado As DataGridViewCheckBoxColumn
    Friend WithEvents oscfechaAsignacion As DataGridViewTextBoxColumn
    Friend WithEvents oscEstatus As DataGridViewTextBoxColumn
    Friend WithEvents grdProductosOT As DataGridView
    Friend WithEvents txtCantidad As TextBox
    Friend WithEvents Label22 As Label
    Friend WithEvents txtNomUniProd As TextBox
    Friend WithEvents txtIdUnidadProd As TextBox
    Friend WithEvents Label21 As Label
    Friend WithEvents txtNomProducto As TextBox
    Friend WithEvents txtIdProducto As TextBox
    Friend WithEvents Label20 As Label
    Friend WithEvents cmdBuscaUniProd As Button
    Friend WithEvents cmdBuscaProd As Button
    Friend WithEvents btnAgregaOS As Button
    Friend WithEvents Label4 As Label
    Friend WithEvents cmbAutoriza As ComboBox
    Dim ClaveOS As Integer

    Dim _InitialCatalog As String
    Dim _NomServidor As String
    Friend WithEvents cmbActivRef As ComboBox
    Friend WithEvents Label5 As Label
    Friend WithEvents grefConsecutivo As DataGridViewTextBoxColumn
    Friend WithEvents grefCCODIGOP01_PRO As DataGridViewTextBoxColumn
    Friend WithEvents grefCIDPRODUCTO As DataGridViewTextBoxColumn
    Friend WithEvents grefCNOMBREP01 As DataGridViewTextBoxColumn
    Friend WithEvents grefExistencia As DataGridViewTextBoxColumn
    Friend WithEvents grefCantidad As DataGridViewTextBoxColumn
    Friend WithEvents grefCCLAVEINT As DataGridViewTextBoxColumn
    Friend WithEvents grefBComprar As DataGridViewCheckBoxColumn
    Friend WithEvents grefCOSTO As DataGridViewTextBoxColumn
    Friend WithEvents grefCIDUNIDADBASE As DataGridViewTextBoxColumn
    Friend WithEvents grefCantCompra As DataGridViewTextBoxColumn
    Friend WithEvents grefCantSalida As DataGridViewTextBoxColumn
    Friend WithEvents grefidOrdenTrabajo As DataGridViewTextBoxColumn
    Friend WithEvents ConMnuOrdTra As ContextMenuStrip
    Friend WithEvents CancelarToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents DiagnosticarToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ActivarToolStripMenuItem As ToolStripMenuItem
    Dim _CIDEMPRESA_COM As Integer
    Friend WithEvents osdDiagnostico As DataGridViewCheckBoxColumn
    Friend WithEvents osdCLAVE As DataGridViewTextBoxColumn
    Friend WithEvents osdIdOrdenSer As DataGridViewTextBoxColumn
    Friend WithEvents osdidServicio As DataGridViewTextBoxColumn
    Friend WithEvents osdidOrdenTrabajo As DataGridViewTextBoxColumn
    Friend WithEvents osdNomServicio As DataGridViewTextBoxColumn
    Friend WithEvents osdidActividad As DataGridViewTextBoxColumn
    Friend WithEvents osdNombreAct As DataGridViewTextBoxColumn
    Friend WithEvents osdNotaDiagnostico As DataGridViewTextBoxColumn
    Friend WithEvents osdCNOMBREPRODUCTO As DataGridViewTextBoxColumn
    Friend WithEvents osdPrecio_CIDPRODUCTO_SERV As DataGridViewTextBoxColumn
    Friend WithEvents osdidDivision As DataGridViewTextBoxColumn
    Friend WithEvents osdNomDivision As DataGridViewTextBoxColumn
    Friend WithEvents osdDuracionActHr As DataGridViewTextBoxColumn
    Friend WithEvents osdModificado As DataGridViewCheckBoxColumn
    Friend WithEvents osdidOrdenActividad As DataGridViewTextBoxColumn
    Friend WithEvents osdidLlanta As DataGridViewTextBoxColumn
    Friend WithEvents osdPosicion As DataGridViewTextBoxColumn
    Friend WithEvents osdidPersonalResp As DataGridViewTextBoxColumn
    Friend WithEvents osdidPersonalAyu1 As DataGridViewTextBoxColumn
    Friend WithEvents osdidPersonalAyu2 As DataGridViewTextBoxColumn
    Friend WithEvents osdidEnum As DataGridViewTextBoxColumn
    Friend WithEvents osdEstatus As DataGridViewTextBoxColumn
    Friend WithEvents osdCancela As DataGridViewCheckBoxColumn
    Friend WithEvents osdMotivoCancela As DataGridViewTextBoxColumn
    Friend WithEvents osdCIDPRODUCTO As DataGridViewTextBoxColumn
    Friend WithEvents osdCantidad_CIDPRODUCTO_SERV As DataGridViewTextBoxColumn
    Friend WithEvents osdCCODIGOPRODUCTO As DataGridViewTextBoxColumn
    Dim DtImprime As New DataTable

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents btnMnuOk As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuLimpiar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuSalir As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripMenu As System.Windows.Forms.ToolStrip
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDiagnosticoOS))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.ToolStripMenu = New System.Windows.Forms.ToolStrip()
        Me.btnMnuOk = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuCancelar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuLimpiar = New System.Windows.Forms.ToolStripButton()
        Me.btnMenuReImprimir = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuSalir = New System.Windows.Forms.ToolStripButton()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.btnAgregaOS = New System.Windows.Forms.Button()
        Me.txtEstatusOS = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtOrdenServ = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.chkAyu2 = New System.Windows.Forms.CheckBox()
        Me.chkAyu1 = New System.Windows.Forms.CheckBox()
        Me.cmbPuestoAyu2 = New System.Windows.Forms.ComboBox()
        Me.cmbPuestoAyu1 = New System.Windows.Forms.ComboBox()
        Me.cmbPuestoResp = New System.Windows.Forms.ComboBox()
        Me.cmbayudante2 = New System.Windows.Forms.ComboBox()
        Me.cmbayudante1 = New System.Windows.Forms.ComboBox()
        Me.cmbResponsable = New System.Windows.Forms.ComboBox()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.txtChofer = New System.Windows.Forms.TextBox()
        Me.txtTipoOrdenServ = New System.Windows.Forms.TextBox()
        Me.txtEstatus = New System.Windows.Forms.TextBox()
        Me.txtNomEmpresa = New System.Windows.Forms.TextBox()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.dtpFechaCapturaHora = New System.Windows.Forms.DateTimePicker()
        Me.dtpFechaCap = New System.Windows.Forms.DateTimePicker()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtRemolque2 = New System.Windows.Forms.TextBox()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.txtDolly = New System.Windows.Forms.TextBox()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.txtRemolque1 = New System.Windows.Forms.TextBox()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.txtTractor = New System.Windows.Forms.TextBox()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.txtidPadreOrdSer = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.ConMnuOrdSer = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ModificarToolStripMenuOrdSer = New System.Windows.Forms.ToolStripMenuItem()
        Me.CancelarToolStripMenuOrdSer = New System.Windows.Forms.ToolStripMenuItem()
        Me.ImprimirToolStripMenuOrdSer = New System.Windows.Forms.ToolStripMenuItem()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.GrupoCaptura = New System.Windows.Forms.GroupBox()
        Me.chkMarcaTodasOT = New System.Windows.Forms.CheckBox()
        Me.chkMarcaTodas = New System.Windows.Forms.CheckBox()
        Me.grdOrdenServDet = New System.Windows.Forms.DataGridView()
        Me.ConMnuOrdTra = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.CancelarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DiagnosticarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ActivarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.grdOrdenServCab = New System.Windows.Forms.DataGridView()
        Me.oscDiagnostico = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.oscCLAVE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.oscConsecutivo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.oscIdOrdenSer = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.oscfechaCaptura = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.oscTipoOrdenServ = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.oscNomTipOrdServ = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.oscidUnidadTrans = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.oscidTipoUnidadTrans = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.oscnomTipoUniTras = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.oscKm = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.oscAutorizo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.oscbOrdenTrabajo = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.oscTipoServ = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.oscModificado = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.oscCancelado = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.oscfechaAsignacion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.oscEstatus = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.cmbActivRef = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.grdProductosOT = New System.Windows.Forms.DataGridView()
        Me.grefConsecutivo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.grefCCODIGOP01_PRO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.grefCIDPRODUCTO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.grefCNOMBREP01 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.grefExistencia = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.grefCantidad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.grefCCLAVEINT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.grefBComprar = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.grefCOSTO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.grefCIDUNIDADBASE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.grefCantCompra = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.grefCantSalida = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.grefidOrdenTrabajo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.txtCantidad = New System.Windows.Forms.TextBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.txtNomUniProd = New System.Windows.Forms.TextBox()
        Me.txtIdUnidadProd = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.txtNomProducto = New System.Windows.Forms.TextBox()
        Me.txtIdProducto = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.cmdBuscaUniProd = New System.Windows.Forms.Button()
        Me.cmdBuscaProd = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cmbAutoriza = New System.Windows.Forms.ComboBox()
        Me.osdDiagnostico = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.osdCLAVE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.osdIdOrdenSer = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.osdidServicio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.osdidOrdenTrabajo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.osdNomServicio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.osdidActividad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.osdNombreAct = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.osdNotaDiagnostico = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.osdCNOMBREPRODUCTO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.osdPrecio_CIDPRODUCTO_SERV = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.osdidDivision = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.osdNomDivision = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.osdDuracionActHr = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.osdModificado = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.osdidOrdenActividad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.osdidLlanta = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.osdPosicion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.osdidPersonalResp = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.osdidPersonalAyu1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.osdidPersonalAyu2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.osdidEnum = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.osdEstatus = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.osdCancela = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.osdMotivoCancela = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.osdCIDPRODUCTO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.osdCantidad_CIDPRODUCTO_SERV = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.osdCCODIGOPRODUCTO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ToolStripMenu.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.ConMnuOrdSer.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.GrupoCaptura.SuspendLayout()
        CType(Me.grdOrdenServDet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ConMnuOrdTra.SuspendLayout()
        CType(Me.grdOrdenServCab, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        CType(Me.grdProductosOT, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ToolStripMenu
        '
        Me.ToolStripMenu.AutoSize = False
        Me.ToolStripMenu.BackColor = System.Drawing.Color.Silver
        Me.ToolStripMenu.Dock = System.Windows.Forms.DockStyle.None
        Me.ToolStripMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnMnuOk, Me.btnMnuCancelar, Me.btnMnuLimpiar, Me.btnMenuReImprimir, Me.btnMnuSalir})
        Me.ToolStripMenu.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow
        Me.ToolStripMenu.Location = New System.Drawing.Point(3, 0)
        Me.ToolStripMenu.Name = "ToolStripMenu"
        Me.ToolStripMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.ToolStripMenu.Size = New System.Drawing.Size(891, 42)
        Me.ToolStripMenu.TabIndex = 1
        Me.ToolStripMenu.Text = "ToolStrip1"
        '
        'btnMnuOk
        '
        Me.btnMnuOk.ForeColor = System.Drawing.Color.Red
        Me.btnMnuOk.Image = Global.Fletera.My.Resources.Resources.Save1
        Me.btnMnuOk.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuOk.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuOk.Name = "btnMnuOk"
        Me.btnMnuOk.Size = New System.Drawing.Size(56, 39)
        Me.btnMnuOk.Text = "&GRABAR"
        Me.btnMnuOk.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuCancelar
        '
        Me.btnMnuCancelar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuCancelar.Image = Global.Fletera.My.Resources.Resources.deleteCancelar
        Me.btnMnuCancelar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuCancelar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuCancelar.Name = "btnMnuCancelar"
        Me.btnMnuCancelar.Size = New System.Drawing.Size(71, 39)
        Me.btnMnuCancelar.Text = "&CANCELAR"
        Me.btnMnuCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnMnuCancelar.Visible = False
        '
        'btnMnuLimpiar
        '
        Me.btnMnuLimpiar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuLimpiar.Image = Global.Fletera.My.Resources.Resources.signos_f_059
        Me.btnMnuLimpiar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuLimpiar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuLimpiar.Name = "btnMnuLimpiar"
        Me.btnMnuLimpiar.Size = New System.Drawing.Size(56, 39)
        Me.btnMnuLimpiar.Text = "&LIMPIAR"
        Me.btnMnuLimpiar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMenuReImprimir
        '
        Me.btnMenuReImprimir.ForeColor = System.Drawing.Color.Red
        Me.btnMenuReImprimir.Image = Global.Fletera.My.Resources.Resources.impresora
        Me.btnMenuReImprimir.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMenuReImprimir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMenuReImprimir.Name = "btnMenuReImprimir"
        Me.btnMenuReImprimir.Size = New System.Drawing.Size(63, 39)
        Me.btnMenuReImprimir.Text = "&IMPRIMIR"
        Me.btnMenuReImprimir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuSalir
        '
        Me.btnMnuSalir.ForeColor = System.Drawing.Color.Red
        Me.btnMnuSalir.Image = Global.Fletera.My.Resources.Resources.Salida2
        Me.btnMnuSalir.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuSalir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuSalir.Name = "btnMnuSalir"
        Me.btnMnuSalir.Size = New System.Drawing.Size(41, 39)
        Me.btnMnuSalir.Text = "&SALIR"
        Me.btnMnuSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.btnAgregaOS)
        Me.GroupBox4.Controls.Add(Me.txtEstatusOS)
        Me.GroupBox4.Controls.Add(Me.Label3)
        Me.GroupBox4.Controls.Add(Me.txtOrdenServ)
        Me.GroupBox4.Controls.Add(Me.Label2)
        Me.GroupBox4.Controls.Add(Me.GroupBox3)
        Me.GroupBox4.Controls.Add(Me.txtChofer)
        Me.GroupBox4.Controls.Add(Me.txtTipoOrdenServ)
        Me.GroupBox4.Controls.Add(Me.txtEstatus)
        Me.GroupBox4.Controls.Add(Me.txtNomEmpresa)
        Me.GroupBox4.Controls.Add(Me.Label37)
        Me.GroupBox4.Controls.Add(Me.dtpFechaCapturaHora)
        Me.GroupBox4.Controls.Add(Me.dtpFechaCap)
        Me.GroupBox4.Controls.Add(Me.Label10)
        Me.GroupBox4.Controls.Add(Me.txtRemolque2)
        Me.GroupBox4.Controls.Add(Me.Label36)
        Me.GroupBox4.Controls.Add(Me.txtDolly)
        Me.GroupBox4.Controls.Add(Me.Label35)
        Me.GroupBox4.Controls.Add(Me.txtRemolque1)
        Me.GroupBox4.Controls.Add(Me.Label34)
        Me.GroupBox4.Controls.Add(Me.txtTractor)
        Me.GroupBox4.Controls.Add(Me.Label33)
        Me.GroupBox4.Controls.Add(Me.txtidPadreOrdSer)
        Me.GroupBox4.Controls.Add(Me.Label13)
        Me.GroupBox4.Controls.Add(Me.Label32)
        Me.GroupBox4.Controls.Add(Me.Label1)
        Me.GroupBox4.Controls.Add(Me.Label9)
        Me.GroupBox4.Location = New System.Drawing.Point(3, 45)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(854, 185)
        Me.GroupBox4.TabIndex = 75
        Me.GroupBox4.TabStop = False
        '
        'btnAgregaOS
        '
        Me.btnAgregaOS.Image = CType(resources.GetObject("btnAgregaOS.Image"), System.Drawing.Image)
        Me.btnAgregaOS.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnAgregaOS.Location = New System.Drawing.Point(805, 89)
        Me.btnAgregaOS.Name = "btnAgregaOS"
        Me.btnAgregaOS.Size = New System.Drawing.Size(39, 42)
        Me.btnAgregaOS.TabIndex = 118
        Me.btnAgregaOS.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'txtEstatusOS
        '
        Me.txtEstatusOS.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtEstatusOS.Enabled = False
        Me.txtEstatusOS.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEstatusOS.Location = New System.Drawing.Point(776, 159)
        Me.txtEstatusOS.Name = "txtEstatusOS"
        Me.txtEstatusOS.ReadOnly = True
        Me.txtEstatusOS.Size = New System.Drawing.Size(72, 20)
        Me.txtEstatusOS.TabIndex = 116
        '
        'Label3
        '
        Me.Label3.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label3.Location = New System.Drawing.Point(773, 134)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(75, 20)
        Me.Label3.TabIndex = 117
        Me.Label3.Text = "Estatus:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtOrdenServ
        '
        Me.txtOrdenServ.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtOrdenServ.ForeColor = System.Drawing.Color.Red
        Me.txtOrdenServ.Location = New System.Drawing.Point(155, 13)
        Me.txtOrdenServ.Name = "txtOrdenServ"
        Me.txtOrdenServ.Size = New System.Drawing.Size(67, 20)
        Me.txtOrdenServ.TabIndex = 114
        Me.txtOrdenServ.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label2.Location = New System.Drawing.Point(128, 15)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(28, 13)
        Me.Label2.TabIndex = 115
        Me.Label2.Text = "O.S."
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.chkAyu2)
        Me.GroupBox3.Controls.Add(Me.chkAyu1)
        Me.GroupBox3.Controls.Add(Me.cmbPuestoAyu2)
        Me.GroupBox3.Controls.Add(Me.cmbPuestoAyu1)
        Me.GroupBox3.Controls.Add(Me.cmbPuestoResp)
        Me.GroupBox3.Controls.Add(Me.cmbayudante2)
        Me.GroupBox3.Controls.Add(Me.cmbayudante1)
        Me.GroupBox3.Controls.Add(Me.cmbResponsable)
        Me.GroupBox3.Controls.Add(Me.Label31)
        Me.GroupBox3.Controls.Add(Me.Label30)
        Me.GroupBox3.Controls.Add(Me.Label29)
        Me.GroupBox3.Location = New System.Drawing.Point(5, 97)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(765, 88)
        Me.GroupBox3.TabIndex = 113
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Empleados Asignados"
        '
        'chkAyu2
        '
        Me.chkAyu2.AutoSize = True
        Me.chkAyu2.Enabled = False
        Me.chkAyu2.Location = New System.Drawing.Point(693, 63)
        Me.chkAyu2.Name = "chkAyu2"
        Me.chkAyu2.Size = New System.Drawing.Size(15, 14)
        Me.chkAyu2.TabIndex = 51
        Me.chkAyu2.UseVisualStyleBackColor = True
        '
        'chkAyu1
        '
        Me.chkAyu1.AutoSize = True
        Me.chkAyu1.Enabled = False
        Me.chkAyu1.Location = New System.Drawing.Point(693, 43)
        Me.chkAyu1.Name = "chkAyu1"
        Me.chkAyu1.Size = New System.Drawing.Size(15, 14)
        Me.chkAyu1.TabIndex = 50
        Me.chkAyu1.UseVisualStyleBackColor = True
        '
        'cmbPuestoAyu2
        '
        Me.cmbPuestoAyu2.AutoCompleteCustomSource.AddRange(New String() {"Regularizaci�n", "Vivienda"})
        Me.cmbPuestoAyu2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cmbPuestoAyu2.FormattingEnabled = True
        Me.cmbPuestoAyu2.Location = New System.Drawing.Point(524, 60)
        Me.cmbPuestoAyu2.Name = "cmbPuestoAyu2"
        Me.cmbPuestoAyu2.Size = New System.Drawing.Size(163, 21)
        Me.cmbPuestoAyu2.TabIndex = 49
        '
        'cmbPuestoAyu1
        '
        Me.cmbPuestoAyu1.AutoCompleteCustomSource.AddRange(New String() {"Regularizaci�n", "Vivienda"})
        Me.cmbPuestoAyu1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cmbPuestoAyu1.FormattingEnabled = True
        Me.cmbPuestoAyu1.Location = New System.Drawing.Point(524, 36)
        Me.cmbPuestoAyu1.Name = "cmbPuestoAyu1"
        Me.cmbPuestoAyu1.Size = New System.Drawing.Size(163, 21)
        Me.cmbPuestoAyu1.TabIndex = 48
        '
        'cmbPuestoResp
        '
        Me.cmbPuestoResp.AutoCompleteCustomSource.AddRange(New String() {"Regularizaci�n", "Vivienda"})
        Me.cmbPuestoResp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cmbPuestoResp.FormattingEnabled = True
        Me.cmbPuestoResp.Location = New System.Drawing.Point(524, 13)
        Me.cmbPuestoResp.Name = "cmbPuestoResp"
        Me.cmbPuestoResp.Size = New System.Drawing.Size(163, 21)
        Me.cmbPuestoResp.TabIndex = 47
        '
        'cmbayudante2
        '
        Me.cmbayudante2.AutoCompleteCustomSource.AddRange(New String() {"Regularizaci�n", "Vivienda"})
        Me.cmbayudante2.FormattingEnabled = True
        Me.cmbayudante2.Location = New System.Drawing.Point(86, 60)
        Me.cmbayudante2.Name = "cmbayudante2"
        Me.cmbayudante2.Size = New System.Drawing.Size(426, 21)
        Me.cmbayudante2.TabIndex = 46
        '
        'cmbayudante1
        '
        Me.cmbayudante1.AutoCompleteCustomSource.AddRange(New String() {"Regularizaci�n", "Vivienda"})
        Me.cmbayudante1.FormattingEnabled = True
        Me.cmbayudante1.Location = New System.Drawing.Point(86, 36)
        Me.cmbayudante1.Name = "cmbayudante1"
        Me.cmbayudante1.Size = New System.Drawing.Size(426, 21)
        Me.cmbayudante1.TabIndex = 45
        '
        'cmbResponsable
        '
        Me.cmbResponsable.AutoCompleteCustomSource.AddRange(New String() {"Regularizaci�n", "Vivienda"})
        Me.cmbResponsable.FormattingEnabled = True
        Me.cmbResponsable.Location = New System.Drawing.Point(86, 13)
        Me.cmbResponsable.Name = "cmbResponsable"
        Me.cmbResponsable.Size = New System.Drawing.Size(426, 21)
        Me.cmbResponsable.TabIndex = 44
        '
        'Label31
        '
        Me.Label31.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label31.Location = New System.Drawing.Point(6, 63)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(79, 20)
        Me.Label31.TabIndex = 42
        Me.Label31.Text = "Ayudante 2"
        '
        'Label30
        '
        Me.Label30.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label30.Location = New System.Drawing.Point(6, 36)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(79, 20)
        Me.Label30.TabIndex = 37
        Me.Label30.Text = "Ayudante 1"
        '
        'Label29
        '
        Me.Label29.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label29.Location = New System.Drawing.Point(6, 16)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(79, 20)
        Me.Label29.TabIndex = 32
        Me.Label29.Text = "Responsable:"
        '
        'txtChofer
        '
        Me.txtChofer.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtChofer.Location = New System.Drawing.Point(56, 65)
        Me.txtChofer.MaxLength = 10
        Me.txtChofer.Name = "txtChofer"
        Me.txtChofer.Size = New System.Drawing.Size(259, 20)
        Me.txtChofer.TabIndex = 112
        Me.txtChofer.Tag = "3"
        '
        'txtTipoOrdenServ
        '
        Me.txtTipoOrdenServ.Enabled = False
        Me.txtTipoOrdenServ.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTipoOrdenServ.Location = New System.Drawing.Point(588, 56)
        Me.txtTipoOrdenServ.Name = "txtTipoOrdenServ"
        Me.txtTipoOrdenServ.ReadOnly = True
        Me.txtTipoOrdenServ.Size = New System.Drawing.Size(260, 26)
        Me.txtTipoOrdenServ.TabIndex = 105
        '
        'txtEstatus
        '
        Me.txtEstatus.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtEstatus.Enabled = False
        Me.txtEstatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEstatus.Location = New System.Drawing.Point(370, 65)
        Me.txtEstatus.Name = "txtEstatus"
        Me.txtEstatus.ReadOnly = True
        Me.txtEstatus.Size = New System.Drawing.Size(72, 20)
        Me.txtEstatus.TabIndex = 102
        '
        'txtNomEmpresa
        '
        Me.txtNomEmpresa.Enabled = False
        Me.txtNomEmpresa.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNomEmpresa.Location = New System.Drawing.Point(272, 13)
        Me.txtNomEmpresa.Name = "txtNomEmpresa"
        Me.txtNomEmpresa.ReadOnly = True
        Me.txtNomEmpresa.Size = New System.Drawing.Size(210, 20)
        Me.txtNomEmpresa.TabIndex = 99
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label37.Location = New System.Drawing.Point(10, 68)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(41, 13)
        Me.Label37.TabIndex = 90
        Me.Label37.Text = "Chofer:"
        '
        'dtpFechaCapturaHora
        '
        Me.dtpFechaCapturaHora.Enabled = False
        Me.dtpFechaCapturaHora.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtpFechaCapturaHora.Location = New System.Drawing.Point(765, 13)
        Me.dtpFechaCapturaHora.Name = "dtpFechaCapturaHora"
        Me.dtpFechaCapturaHora.Size = New System.Drawing.Size(83, 20)
        Me.dtpFechaCapturaHora.TabIndex = 88
        '
        'dtpFechaCap
        '
        Me.dtpFechaCap.Enabled = False
        Me.dtpFechaCap.Location = New System.Drawing.Point(534, 13)
        Me.dtpFechaCap.Name = "dtpFechaCap"
        Me.dtpFechaCap.Size = New System.Drawing.Size(225, 20)
        Me.dtpFechaCap.TabIndex = 87
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label10.Location = New System.Drawing.Point(488, 15)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(40, 13)
        Me.Label10.TabIndex = 89
        Me.Label10.Text = "Fecha:"
        '
        'txtRemolque2
        '
        Me.txtRemolque2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRemolque2.Location = New System.Drawing.Point(502, 39)
        Me.txtRemolque2.MaxLength = 10
        Me.txtRemolque2.Name = "txtRemolque2"
        Me.txtRemolque2.Size = New System.Drawing.Size(69, 20)
        Me.txtRemolque2.TabIndex = 8
        Me.txtRemolque2.Tag = "4"
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label36.Location = New System.Drawing.Point(432, 42)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(64, 13)
        Me.Label36.TabIndex = 85
        Me.Label36.Text = "Remolque2:"
        '
        'txtDolly
        '
        Me.txtDolly.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDolly.Location = New System.Drawing.Point(349, 39)
        Me.txtDolly.MaxLength = 10
        Me.txtDolly.Name = "txtDolly"
        Me.txtDolly.Size = New System.Drawing.Size(69, 20)
        Me.txtDolly.TabIndex = 6
        Me.txtDolly.Tag = "3"
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label35.Location = New System.Drawing.Point(310, 39)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(33, 13)
        Me.Label35.TabIndex = 83
        Me.Label35.Text = "Dolly:"
        '
        'txtRemolque1
        '
        Me.txtRemolque1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRemolque1.Location = New System.Drawing.Point(213, 39)
        Me.txtRemolque1.MaxLength = 10
        Me.txtRemolque1.Name = "txtRemolque1"
        Me.txtRemolque1.Size = New System.Drawing.Size(69, 20)
        Me.txtRemolque1.TabIndex = 4
        Me.txtRemolque1.Tag = "2"
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label34.Location = New System.Drawing.Point(146, 42)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(64, 13)
        Me.Label34.TabIndex = 81
        Me.Label34.Text = "Remolque1:"
        '
        'txtTractor
        '
        Me.txtTractor.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTractor.Location = New System.Drawing.Point(56, 39)
        Me.txtTractor.MaxLength = 10
        Me.txtTractor.Name = "txtTractor"
        Me.txtTractor.Size = New System.Drawing.Size(69, 20)
        Me.txtTractor.TabIndex = 2
        Me.txtTractor.Tag = "1"
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label33.Location = New System.Drawing.Point(10, 39)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(44, 13)
        Me.Label33.TabIndex = 79
        Me.Label33.Text = "Tractor:"
        '
        'txtidPadreOrdSer
        '
        Me.txtidPadreOrdSer.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtidPadreOrdSer.ForeColor = System.Drawing.Color.Red
        Me.txtidPadreOrdSer.Location = New System.Drawing.Point(58, 13)
        Me.txtidPadreOrdSer.Name = "txtidPadreOrdSer"
        Me.txtidPadreOrdSer.Size = New System.Drawing.Size(67, 20)
        Me.txtidPadreOrdSer.TabIndex = 1
        Me.txtidPadreOrdSer.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtidPadreOrdSer.Visible = False
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label13.Location = New System.Drawing.Point(11, 16)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(41, 13)
        Me.Label13.TabIndex = 78
        Me.Label13.Text = "FOLIO:"
        Me.Label13.Visible = False
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label32.Location = New System.Drawing.Point(466, 65)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(116, 13)
        Me.Label32.TabIndex = 75
        Me.Label32.Text = "Tipo de Orden Servicio"
        '
        'Label1
        '
        Me.Label1.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label1.Location = New System.Drawing.Point(321, 65)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(54, 20)
        Me.Label1.TabIndex = 103
        Me.Label1.Text = "Estatus:"
        '
        'Label9
        '
        Me.Label9.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label9.Location = New System.Drawing.Point(228, 15)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(54, 20)
        Me.Label9.TabIndex = 100
        Me.Label9.Text = "Empresa:"
        '
        'ConMnuOrdSer
        '
        Me.ConMnuOrdSer.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ModificarToolStripMenuOrdSer, Me.CancelarToolStripMenuOrdSer, Me.ImprimirToolStripMenuOrdSer})
        Me.ConMnuOrdSer.Name = "ConMnuOrdSer"
        Me.ConMnuOrdSer.Size = New System.Drawing.Size(126, 70)
        '
        'ModificarToolStripMenuOrdSer
        '
        Me.ModificarToolStripMenuOrdSer.Name = "ModificarToolStripMenuOrdSer"
        Me.ModificarToolStripMenuOrdSer.Size = New System.Drawing.Size(125, 22)
        Me.ModificarToolStripMenuOrdSer.Text = "Modificar"
        '
        'CancelarToolStripMenuOrdSer
        '
        Me.CancelarToolStripMenuOrdSer.Name = "CancelarToolStripMenuOrdSer"
        Me.CancelarToolStripMenuOrdSer.Size = New System.Drawing.Size(125, 22)
        Me.CancelarToolStripMenuOrdSer.Text = "Cancelar"
        '
        'ImprimirToolStripMenuOrdSer
        '
        Me.ImprimirToolStripMenuOrdSer.Name = "ImprimirToolStripMenuOrdSer"
        Me.ImprimirToolStripMenuOrdSer.Size = New System.Drawing.Size(125, 22)
        Me.ImprimirToolStripMenuOrdSer.Text = "Imprimir"
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Location = New System.Drawing.Point(3, 255)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(854, 368)
        Me.TabControl1.TabIndex = 76
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.GrupoCaptura)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(846, 342)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Ordenes de Servicio"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'GrupoCaptura
        '
        Me.GrupoCaptura.Controls.Add(Me.chkMarcaTodasOT)
        Me.GrupoCaptura.Controls.Add(Me.chkMarcaTodas)
        Me.GrupoCaptura.Controls.Add(Me.grdOrdenServDet)
        Me.GrupoCaptura.Controls.Add(Me.grdOrdenServCab)
        Me.GrupoCaptura.Location = New System.Drawing.Point(6, 6)
        Me.GrupoCaptura.Name = "GrupoCaptura"
        Me.GrupoCaptura.Size = New System.Drawing.Size(834, 322)
        Me.GrupoCaptura.TabIndex = 4
        Me.GrupoCaptura.TabStop = False
        '
        'chkMarcaTodasOT
        '
        Me.chkMarcaTodasOT.AutoSize = True
        Me.chkMarcaTodasOT.Location = New System.Drawing.Point(267, 19)
        Me.chkMarcaTodasOT.Name = "chkMarcaTodasOT"
        Me.chkMarcaTodasOT.Size = New System.Drawing.Size(189, 17)
        Me.chkMarcaTodasOT.TabIndex = 108
        Me.chkMarcaTodasOT.Text = "Marcar Todas Ordenes de Trabajo"
        Me.chkMarcaTodasOT.UseVisualStyleBackColor = True
        Me.chkMarcaTodasOT.Visible = False
        '
        'chkMarcaTodas
        '
        Me.chkMarcaTodas.AutoSize = True
        Me.chkMarcaTodas.Location = New System.Drawing.Point(9, 19)
        Me.chkMarcaTodas.Name = "chkMarcaTodas"
        Me.chkMarcaTodas.Size = New System.Drawing.Size(191, 17)
        Me.chkMarcaTodas.TabIndex = 107
        Me.chkMarcaTodas.Text = "Marcar Todas Ordenes de Servicio"
        Me.chkMarcaTodas.UseVisualStyleBackColor = True
        Me.chkMarcaTodas.Visible = False
        '
        'grdOrdenServDet
        '
        Me.grdOrdenServDet.AllowUserToAddRows = False
        Me.grdOrdenServDet.AllowUserToResizeColumns = False
        Me.grdOrdenServDet.AllowUserToResizeRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Yellow
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black
        Me.grdOrdenServDet.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.grdOrdenServDet.BackgroundColor = System.Drawing.Color.White
        Me.grdOrdenServDet.ColumnHeadersHeight = 34
        Me.grdOrdenServDet.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.grdOrdenServDet.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.osdDiagnostico, Me.osdCLAVE, Me.osdIdOrdenSer, Me.osdidServicio, Me.osdidOrdenTrabajo, Me.osdNomServicio, Me.osdidActividad, Me.osdNombreAct, Me.osdNotaDiagnostico, Me.osdCNOMBREPRODUCTO, Me.osdPrecio_CIDPRODUCTO_SERV, Me.osdidDivision, Me.osdNomDivision, Me.osdDuracionActHr, Me.osdModificado, Me.osdidOrdenActividad, Me.osdidLlanta, Me.osdPosicion, Me.osdidPersonalResp, Me.osdidPersonalAyu1, Me.osdidPersonalAyu2, Me.osdidEnum, Me.osdEstatus, Me.osdCancela, Me.osdMotivoCancela, Me.osdCIDPRODUCTO, Me.osdCantidad_CIDPRODUCTO_SERV, Me.osdCCODIGOPRODUCTO})
        Me.grdOrdenServDet.ContextMenuStrip = Me.ConMnuOrdTra
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Yellow
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.grdOrdenServDet.DefaultCellStyle = DataGridViewCellStyle2
        Me.grdOrdenServDet.Location = New System.Drawing.Point(6, 172)
        Me.grdOrdenServDet.Name = "grdOrdenServDet"
        Me.grdOrdenServDet.RowHeadersWidth = 26
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.Yellow
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black
        Me.grdOrdenServDet.RowsDefaultCellStyle = DataGridViewCellStyle3
        Me.grdOrdenServDet.Size = New System.Drawing.Size(816, 137)
        Me.grdOrdenServDet.TabIndex = 35
        '
        'ConMnuOrdTra
        '
        Me.ConMnuOrdTra.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CancelarToolStripMenuItem, Me.DiagnosticarToolStripMenuItem, Me.ActivarToolStripMenuItem})
        Me.ConMnuOrdTra.Name = "ConMnuOrdTra"
        Me.ConMnuOrdTra.Size = New System.Drawing.Size(141, 70)
        '
        'CancelarToolStripMenuItem
        '
        Me.CancelarToolStripMenuItem.Name = "CancelarToolStripMenuItem"
        Me.CancelarToolStripMenuItem.Size = New System.Drawing.Size(140, 22)
        Me.CancelarToolStripMenuItem.Text = "Cancelar"
        '
        'DiagnosticarToolStripMenuItem
        '
        Me.DiagnosticarToolStripMenuItem.Name = "DiagnosticarToolStripMenuItem"
        Me.DiagnosticarToolStripMenuItem.Size = New System.Drawing.Size(140, 22)
        Me.DiagnosticarToolStripMenuItem.Text = "Diagnosticar"
        '
        'ActivarToolStripMenuItem
        '
        Me.ActivarToolStripMenuItem.Name = "ActivarToolStripMenuItem"
        Me.ActivarToolStripMenuItem.Size = New System.Drawing.Size(140, 22)
        Me.ActivarToolStripMenuItem.Text = "Activar"
        '
        'grdOrdenServCab
        '
        Me.grdOrdenServCab.AllowUserToAddRows = False
        Me.grdOrdenServCab.AllowUserToResizeColumns = False
        Me.grdOrdenServCab.AllowUserToResizeRows = False
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.Yellow
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black
        Me.grdOrdenServCab.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle4
        Me.grdOrdenServCab.BackgroundColor = System.Drawing.Color.White
        Me.grdOrdenServCab.ColumnHeadersHeight = 34
        Me.grdOrdenServCab.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.grdOrdenServCab.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.oscDiagnostico, Me.oscCLAVE, Me.oscConsecutivo, Me.oscIdOrdenSer, Me.oscfechaCaptura, Me.oscTipoOrdenServ, Me.oscNomTipOrdServ, Me.oscidUnidadTrans, Me.oscidTipoUnidadTrans, Me.oscnomTipoUniTras, Me.oscKm, Me.oscAutorizo, Me.oscbOrdenTrabajo, Me.oscTipoServ, Me.oscModificado, Me.oscCancelado, Me.oscfechaAsignacion, Me.oscEstatus})
        Me.grdOrdenServCab.ContextMenuStrip = Me.ConMnuOrdSer
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.Yellow
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.grdOrdenServCab.DefaultCellStyle = DataGridViewCellStyle8
        Me.grdOrdenServCab.Location = New System.Drawing.Point(6, 42)
        Me.grdOrdenServCab.Name = "grdOrdenServCab"
        Me.grdOrdenServCab.RowHeadersWidth = 26
        DataGridViewCellStyle9.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.Yellow
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.Black
        Me.grdOrdenServCab.RowsDefaultCellStyle = DataGridViewCellStyle9
        Me.grdOrdenServCab.Size = New System.Drawing.Size(816, 124)
        Me.grdOrdenServCab.TabIndex = 34
        '
        'oscDiagnostico
        '
        Me.oscDiagnostico.HeaderText = "Diagnostico"
        Me.oscDiagnostico.Name = "oscDiagnostico"
        Me.oscDiagnostico.Visible = False
        Me.oscDiagnostico.Width = 70
        '
        'oscCLAVE
        '
        Me.oscCLAVE.HeaderText = "oscCLAVE"
        Me.oscCLAVE.Name = "oscCLAVE"
        Me.oscCLAVE.Visible = False
        '
        'oscConsecutivo
        '
        Me.oscConsecutivo.HeaderText = "#"
        Me.oscConsecutivo.Name = "oscConsecutivo"
        Me.oscConsecutivo.Width = 50
        '
        'oscIdOrdenSer
        '
        Me.oscIdOrdenSer.HeaderText = "Orden Servicio"
        Me.oscIdOrdenSer.Name = "oscIdOrdenSer"
        Me.oscIdOrdenSer.Visible = False
        '
        'oscfechaCaptura
        '
        Me.oscfechaCaptura.HeaderText = "Fecha Captura"
        Me.oscfechaCaptura.Name = "oscfechaCaptura"
        Me.oscfechaCaptura.Width = 140
        '
        'oscTipoOrdenServ
        '
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.oscTipoOrdenServ.DefaultCellStyle = DataGridViewCellStyle5
        Me.oscTipoOrdenServ.HeaderText = "Tipo"
        Me.oscTipoOrdenServ.Name = "oscTipoOrdenServ"
        Me.oscTipoOrdenServ.Visible = False
        Me.oscTipoOrdenServ.Width = 60
        '
        'oscNomTipOrdServ
        '
        Me.oscNomTipOrdServ.HeaderText = "Nombre Tipo"
        Me.oscNomTipOrdServ.Name = "oscNomTipOrdServ"
        Me.oscNomTipOrdServ.Visible = False
        '
        'oscidUnidadTrans
        '
        Me.oscidUnidadTrans.HeaderText = "Unidad Transporte"
        Me.oscidUnidadTrans.Name = "oscidUnidadTrans"
        Me.oscidUnidadTrans.Width = 80
        '
        'oscidTipoUnidadTrans
        '
        Me.oscidTipoUnidadTrans.HeaderText = "idTipoUnidad"
        Me.oscidTipoUnidadTrans.Name = "oscidTipoUnidadTrans"
        Me.oscidTipoUnidadTrans.Visible = False
        '
        'oscnomTipoUniTras
        '
        Me.oscnomTipoUniTras.HeaderText = "Tipo Unidad"
        Me.oscnomTipoUniTras.Name = "oscnomTipoUniTras"
        Me.oscnomTipoUniTras.Width = 120
        '
        'oscKm
        '
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.oscKm.DefaultCellStyle = DataGridViewCellStyle6
        Me.oscKm.HeaderText = "Kilometraje"
        Me.oscKm.Name = "oscKm"
        Me.oscKm.Visible = False
        Me.oscKm.Width = 60
        '
        'oscAutorizo
        '
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.oscAutorizo.DefaultCellStyle = DataGridViewCellStyle7
        Me.oscAutorizo.HeaderText = "Autoriz�"
        Me.oscAutorizo.Name = "oscAutorizo"
        Me.oscAutorizo.Visible = False
        Me.oscAutorizo.Width = 60
        '
        'oscbOrdenTrabajo
        '
        Me.oscbOrdenTrabajo.HeaderText = "oscbOrdenTrabajo"
        Me.oscbOrdenTrabajo.Name = "oscbOrdenTrabajo"
        Me.oscbOrdenTrabajo.Visible = False
        '
        'oscTipoServ
        '
        Me.oscTipoServ.HeaderText = "oscTipoServ"
        Me.oscTipoServ.Name = "oscTipoServ"
        Me.oscTipoServ.Visible = False
        '
        'oscModificado
        '
        Me.oscModificado.HeaderText = "oscModificado"
        Me.oscModificado.Name = "oscModificado"
        Me.oscModificado.Visible = False
        '
        'oscCancelado
        '
        Me.oscCancelado.HeaderText = "oscCancelado"
        Me.oscCancelado.Name = "oscCancelado"
        Me.oscCancelado.Visible = False
        '
        'oscfechaAsignacion
        '
        Me.oscfechaAsignacion.HeaderText = "Fecha Asigna."
        Me.oscfechaAsignacion.Name = "oscfechaAsignacion"
        Me.oscfechaAsignacion.Width = 140
        '
        'oscEstatus
        '
        Me.oscEstatus.HeaderText = "Estatus"
        Me.oscEstatus.Name = "oscEstatus"
        Me.oscEstatus.Width = 70
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.cmbActivRef)
        Me.TabPage2.Controls.Add(Me.Label5)
        Me.TabPage2.Controls.Add(Me.grdProductosOT)
        Me.TabPage2.Controls.Add(Me.txtCantidad)
        Me.TabPage2.Controls.Add(Me.Label22)
        Me.TabPage2.Controls.Add(Me.txtNomUniProd)
        Me.TabPage2.Controls.Add(Me.txtIdUnidadProd)
        Me.TabPage2.Controls.Add(Me.Label21)
        Me.TabPage2.Controls.Add(Me.txtNomProducto)
        Me.TabPage2.Controls.Add(Me.txtIdProducto)
        Me.TabPage2.Controls.Add(Me.Label20)
        Me.TabPage2.Controls.Add(Me.cmdBuscaUniProd)
        Me.TabPage2.Controls.Add(Me.cmdBuscaProd)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(846, 342)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Refacciones"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'cmbActivRef
        '
        Me.cmbActivRef.AutoCompleteCustomSource.AddRange(New String() {"Regularizaci�n", "Vivienda"})
        Me.cmbActivRef.FormattingEnabled = True
        Me.cmbActivRef.Location = New System.Drawing.Point(65, 9)
        Me.cmbActivRef.Name = "cmbActivRef"
        Me.cmbActivRef.Size = New System.Drawing.Size(203, 21)
        Me.cmbActivRef.TabIndex = 100
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label5.Location = New System.Drawing.Point(8, 12)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(51, 13)
        Me.Label5.TabIndex = 115
        Me.Label5.Text = "Actividad"
        '
        'grdProductosOT
        '
        Me.grdProductosOT.AllowUserToAddRows = False
        Me.grdProductosOT.AllowUserToResizeColumns = False
        Me.grdProductosOT.AllowUserToResizeRows = False
        DataGridViewCellStyle10.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        DataGridViewCellStyle10.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.Yellow
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.Black
        Me.grdProductosOT.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle10
        Me.grdProductosOT.BackgroundColor = System.Drawing.Color.White
        Me.grdProductosOT.ColumnHeadersHeight = 34
        Me.grdProductosOT.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.grdProductosOT.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.grefConsecutivo, Me.grefCCODIGOP01_PRO, Me.grefCIDPRODUCTO, Me.grefCNOMBREP01, Me.grefExistencia, Me.grefCantidad, Me.grefCCLAVEINT, Me.grefBComprar, Me.grefCOSTO, Me.grefCIDUNIDADBASE, Me.grefCantCompra, Me.grefCantSalida, Me.grefidOrdenTrabajo})
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle14.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle14.SelectionBackColor = System.Drawing.Color.Yellow
        DataGridViewCellStyle14.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.grdProductosOT.DefaultCellStyle = DataGridViewCellStyle14
        Me.grdProductosOT.Location = New System.Drawing.Point(15, 162)
        Me.grdProductosOT.Name = "grdProductosOT"
        Me.grdProductosOT.RowHeadersWidth = 26
        DataGridViewCellStyle15.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle15.SelectionBackColor = System.Drawing.Color.Yellow
        DataGridViewCellStyle15.SelectionForeColor = System.Drawing.Color.Black
        Me.grdProductosOT.RowsDefaultCellStyle = DataGridViewCellStyle15
        Me.grdProductosOT.Size = New System.Drawing.Size(816, 174)
        Me.grdProductosOT.TabIndex = 109
        '
        'grefConsecutivo
        '
        Me.grefConsecutivo.HeaderText = "#"
        Me.grefConsecutivo.Name = "grefConsecutivo"
        Me.grefConsecutivo.Width = 30
        '
        'grefCCODIGOP01_PRO
        '
        Me.grefCCODIGOP01_PRO.HeaderText = "Producto"
        Me.grefCCODIGOP01_PRO.Name = "grefCCODIGOP01_PRO"
        '
        'grefCIDPRODUCTO
        '
        Me.grefCIDPRODUCTO.HeaderText = "CIDPRODUCTO"
        Me.grefCIDPRODUCTO.Name = "grefCIDPRODUCTO"
        Me.grefCIDPRODUCTO.Visible = False
        '
        'grefCNOMBREP01
        '
        Me.grefCNOMBREP01.HeaderText = "Nombre"
        Me.grefCNOMBREP01.Name = "grefCNOMBREP01"
        Me.grefCNOMBREP01.Width = 350
        '
        'grefExistencia
        '
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.grefExistencia.DefaultCellStyle = DataGridViewCellStyle11
        Me.grefExistencia.HeaderText = "Existencia"
        Me.grefExistencia.Name = "grefExistencia"
        Me.grefExistencia.Width = 60
        '
        'grefCantidad
        '
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.grefCantidad.DefaultCellStyle = DataGridViewCellStyle12
        Me.grefCantidad.HeaderText = "Cantidad"
        Me.grefCantidad.Name = "grefCantidad"
        Me.grefCantidad.Width = 60
        '
        'grefCCLAVEINT
        '
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.grefCCLAVEINT.DefaultCellStyle = DataGridViewCellStyle13
        Me.grefCCLAVEINT.HeaderText = "Unidad"
        Me.grefCCLAVEINT.Name = "grefCCLAVEINT"
        Me.grefCCLAVEINT.Width = 60
        '
        'grefBComprar
        '
        Me.grefBComprar.HeaderText = "Comprar"
        Me.grefBComprar.Name = "grefBComprar"
        Me.grefBComprar.ThreeState = True
        '
        'grefCOSTO
        '
        Me.grefCOSTO.HeaderText = "Costo"
        Me.grefCOSTO.Name = "grefCOSTO"
        Me.grefCOSTO.Visible = False
        '
        'grefCIDUNIDADBASE
        '
        Me.grefCIDUNIDADBASE.HeaderText = "CIDUNIDADBASE"
        Me.grefCIDUNIDADBASE.Name = "grefCIDUNIDADBASE"
        Me.grefCIDUNIDADBASE.Visible = False
        '
        'grefCantCompra
        '
        Me.grefCantCompra.HeaderText = "CantCompra"
        Me.grefCantCompra.Name = "grefCantCompra"
        Me.grefCantCompra.Visible = False
        '
        'grefCantSalida
        '
        Me.grefCantSalida.HeaderText = "CantSalida"
        Me.grefCantSalida.Name = "grefCantSalida"
        Me.grefCantSalida.Visible = False
        '
        'grefidOrdenTrabajo
        '
        Me.grefidOrdenTrabajo.HeaderText = "idOrdenTrabajo"
        Me.grefidOrdenTrabajo.Name = "grefidOrdenTrabajo"
        Me.grefidOrdenTrabajo.Visible = False
        '
        'txtCantidad
        '
        Me.txtCantidad.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCantidad.ForeColor = System.Drawing.Color.Black
        Me.txtCantidad.Location = New System.Drawing.Point(65, 134)
        Me.txtCantidad.MaxLength = 3
        Me.txtCantidad.Name = "txtCantidad"
        Me.txtCantidad.Size = New System.Drawing.Size(69, 20)
        Me.txtCantidad.TabIndex = 106
        Me.txtCantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label22
        '
        Me.Label22.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label22.Location = New System.Drawing.Point(12, 139)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(74, 20)
        Me.Label22.TabIndex = 108
        Me.Label22.Text = "Cantidad:"
        '
        'txtNomUniProd
        '
        Me.txtNomUniProd.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNomUniProd.ForeColor = System.Drawing.Color.Red
        Me.txtNomUniProd.Location = New System.Drawing.Point(194, 94)
        Me.txtNomUniProd.MaxLength = 3
        Me.txtNomUniProd.Name = "txtNomUniProd"
        Me.txtNomUniProd.ReadOnly = True
        Me.txtNomUniProd.Size = New System.Drawing.Size(528, 20)
        Me.txtNomUniProd.TabIndex = 105
        '
        'txtIdUnidadProd
        '
        Me.txtIdUnidadProd.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtIdUnidadProd.ForeColor = System.Drawing.Color.Black
        Me.txtIdUnidadProd.Location = New System.Drawing.Point(65, 94)
        Me.txtIdUnidadProd.MaxLength = 3
        Me.txtIdUnidadProd.Name = "txtIdUnidadProd"
        Me.txtIdUnidadProd.ReadOnly = True
        Me.txtIdUnidadProd.Size = New System.Drawing.Size(69, 20)
        Me.txtIdUnidadProd.TabIndex = 103
        Me.txtIdUnidadProd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label21
        '
        Me.Label21.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label21.Location = New System.Drawing.Point(12, 99)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(47, 20)
        Me.Label21.TabIndex = 107
        Me.Label21.Text = "Unidad:"
        '
        'txtNomProducto
        '
        Me.txtNomProducto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNomProducto.ForeColor = System.Drawing.Color.Red
        Me.txtNomProducto.Location = New System.Drawing.Point(194, 44)
        Me.txtNomProducto.MaxLength = 3
        Me.txtNomProducto.Name = "txtNomProducto"
        Me.txtNomProducto.ReadOnly = True
        Me.txtNomProducto.Size = New System.Drawing.Size(528, 20)
        Me.txtNomProducto.TabIndex = 102
        '
        'txtIdProducto
        '
        Me.txtIdProducto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtIdProducto.ForeColor = System.Drawing.Color.Black
        Me.txtIdProducto.Location = New System.Drawing.Point(65, 44)
        Me.txtIdProducto.MaxLength = 20
        Me.txtIdProducto.Name = "txtIdProducto"
        Me.txtIdProducto.Size = New System.Drawing.Size(69, 20)
        Me.txtIdProducto.TabIndex = 100
        Me.txtIdProducto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label20
        '
        Me.Label20.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label20.Location = New System.Drawing.Point(12, 49)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(56, 20)
        Me.Label20.TabIndex = 99
        Me.Label20.Text = "Producto:"
        '
        'cmdBuscaUniProd
        '
        Me.cmdBuscaUniProd.Image = CType(resources.GetObject("cmdBuscaUniProd.Image"), System.Drawing.Image)
        Me.cmdBuscaUniProd.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdBuscaUniProd.Location = New System.Drawing.Point(144, 84)
        Me.cmdBuscaUniProd.Name = "cmdBuscaUniProd"
        Me.cmdBuscaUniProd.Size = New System.Drawing.Size(32, 32)
        Me.cmdBuscaUniProd.TabIndex = 104
        Me.cmdBuscaUniProd.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.cmdBuscaUniProd.Visible = False
        '
        'cmdBuscaProd
        '
        Me.cmdBuscaProd.Image = CType(resources.GetObject("cmdBuscaProd.Image"), System.Drawing.Image)
        Me.cmdBuscaProd.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdBuscaProd.Location = New System.Drawing.Point(144, 34)
        Me.cmdBuscaProd.Name = "cmdBuscaProd"
        Me.cmdBuscaProd.Size = New System.Drawing.Size(32, 32)
        Me.cmdBuscaProd.TabIndex = 101
        Me.cmdBuscaProd.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label4.Location = New System.Drawing.Point(41, 231)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(48, 13)
        Me.Label4.TabIndex = 113
        Me.Label4.Text = "Autoriza:"
        '
        'cmbAutoriza
        '
        Me.cmbAutoriza.AutoCompleteCustomSource.AddRange(New String() {"Regularizaci�n", "Vivienda"})
        Me.cmbAutoriza.FormattingEnabled = True
        Me.cmbAutoriza.Location = New System.Drawing.Point(94, 229)
        Me.cmbAutoriza.Name = "cmbAutoriza"
        Me.cmbAutoriza.Size = New System.Drawing.Size(203, 21)
        Me.cmbAutoriza.TabIndex = 114
        '
        'osdDiagnostico
        '
        Me.osdDiagnostico.HeaderText = "Diagnosticar"
        Me.osdDiagnostico.Name = "osdDiagnostico"
        Me.osdDiagnostico.ReadOnly = True
        Me.osdDiagnostico.Width = 70
        '
        'osdCLAVE
        '
        Me.osdCLAVE.HeaderText = "osdCLAVE"
        Me.osdCLAVE.Name = "osdCLAVE"
        Me.osdCLAVE.Visible = False
        '
        'osdIdOrdenSer
        '
        Me.osdIdOrdenSer.HeaderText = "osdIdOrdenSer"
        Me.osdIdOrdenSer.Name = "osdIdOrdenSer"
        Me.osdIdOrdenSer.Visible = False
        '
        'osdidServicio
        '
        Me.osdidServicio.HeaderText = "osdidServicio"
        Me.osdidServicio.Name = "osdidServicio"
        Me.osdidServicio.Visible = False
        '
        'osdidOrdenTrabajo
        '
        Me.osdidOrdenTrabajo.HeaderText = "O. Trabajo"
        Me.osdidOrdenTrabajo.Name = "osdidOrdenTrabajo"
        '
        'osdNomServicio
        '
        Me.osdNomServicio.HeaderText = "osdNomServicio"
        Me.osdNomServicio.Name = "osdNomServicio"
        Me.osdNomServicio.Width = 120
        '
        'osdidActividad
        '
        Me.osdidActividad.HeaderText = "Servicio"
        Me.osdidActividad.Name = "osdidActividad"
        Me.osdidActividad.Visible = False
        '
        'osdNombreAct
        '
        Me.osdNombreAct.HeaderText = "Descripcion O. Trabajo"
        Me.osdNombreAct.Name = "osdNombreAct"
        Me.osdNombreAct.Width = 150
        '
        'osdNotaDiagnostico
        '
        Me.osdNotaDiagnostico.HeaderText = "Diagnostico"
        Me.osdNotaDiagnostico.Name = "osdNotaDiagnostico"
        Me.osdNotaDiagnostico.Width = 200
        '
        'osdCNOMBREPRODUCTO
        '
        Me.osdCNOMBREPRODUCTO.HeaderText = "Nombre Serv."
        Me.osdCNOMBREPRODUCTO.Name = "osdCNOMBREPRODUCTO"
        '
        'osdPrecio_CIDPRODUCTO_SERV
        '
        Me.osdPrecio_CIDPRODUCTO_SERV.HeaderText = "Precio"
        Me.osdPrecio_CIDPRODUCTO_SERV.Name = "osdPrecio_CIDPRODUCTO_SERV"
        '
        'osdidDivision
        '
        Me.osdidDivision.HeaderText = "osdidDivision"
        Me.osdidDivision.Name = "osdidDivision"
        Me.osdidDivision.Visible = False
        '
        'osdNomDivision
        '
        Me.osdNomDivision.HeaderText = "Division"
        Me.osdNomDivision.Name = "osdNomDivision"
        Me.osdNomDivision.Visible = False
        Me.osdNomDivision.Width = 200
        '
        'osdDuracionActHr
        '
        Me.osdDuracionActHr.HeaderText = "Duracion Horas"
        Me.osdDuracionActHr.Name = "osdDuracionActHr"
        Me.osdDuracionActHr.Width = 50
        '
        'osdModificado
        '
        Me.osdModificado.HeaderText = "osdModificado"
        Me.osdModificado.Name = "osdModificado"
        Me.osdModificado.Visible = False
        '
        'osdidOrdenActividad
        '
        Me.osdidOrdenActividad.HeaderText = "osdidOrdenActividad"
        Me.osdidOrdenActividad.Name = "osdidOrdenActividad"
        Me.osdidOrdenActividad.Visible = False
        '
        'osdidLlanta
        '
        Me.osdidLlanta.HeaderText = "Llanta"
        Me.osdidLlanta.Name = "osdidLlanta"
        Me.osdidLlanta.Width = 60
        '
        'osdPosicion
        '
        Me.osdPosicion.HeaderText = "Posicion"
        Me.osdPosicion.Name = "osdPosicion"
        Me.osdPosicion.Width = 50
        '
        'osdidPersonalResp
        '
        Me.osdidPersonalResp.HeaderText = "idPersonalResp"
        Me.osdidPersonalResp.Name = "osdidPersonalResp"
        Me.osdidPersonalResp.Visible = False
        '
        'osdidPersonalAyu1
        '
        Me.osdidPersonalAyu1.HeaderText = "idPersonalAyu1"
        Me.osdidPersonalAyu1.Name = "osdidPersonalAyu1"
        Me.osdidPersonalAyu1.Visible = False
        '
        'osdidPersonalAyu2
        '
        Me.osdidPersonalAyu2.HeaderText = "idPersonalAyu2"
        Me.osdidPersonalAyu2.Name = "osdidPersonalAyu2"
        Me.osdidPersonalAyu2.Visible = False
        '
        'osdidEnum
        '
        Me.osdidEnum.HeaderText = "idEnum"
        Me.osdidEnum.Name = "osdidEnum"
        Me.osdidEnum.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.osdidEnum.Visible = False
        '
        'osdEstatus
        '
        Me.osdEstatus.HeaderText = "Estatus"
        Me.osdEstatus.Name = "osdEstatus"
        Me.osdEstatus.Width = 50
        '
        'osdCancela
        '
        Me.osdCancela.HeaderText = "Cancelar"
        Me.osdCancela.Name = "osdCancela"
        Me.osdCancela.ReadOnly = True
        Me.osdCancela.Width = 60
        '
        'osdMotivoCancela
        '
        Me.osdMotivoCancela.HeaderText = "MotivoCancela"
        Me.osdMotivoCancela.Name = "osdMotivoCancela"
        '
        'osdCIDPRODUCTO
        '
        Me.osdCIDPRODUCTO.HeaderText = "CIDPRODUCTO"
        Me.osdCIDPRODUCTO.Name = "osdCIDPRODUCTO"
        Me.osdCIDPRODUCTO.Visible = False
        '
        'osdCantidad_CIDPRODUCTO_SERV
        '
        Me.osdCantidad_CIDPRODUCTO_SERV.HeaderText = "CantidadServ"
        Me.osdCantidad_CIDPRODUCTO_SERV.Name = "osdCantidad_CIDPRODUCTO_SERV"
        Me.osdCantidad_CIDPRODUCTO_SERV.Visible = False
        '
        'osdCCODIGOPRODUCTO
        '
        Me.osdCCODIGOPRODUCTO.HeaderText = "Servicio"
        Me.osdCCODIGOPRODUCTO.Name = "osdCCODIGOPRODUCTO"
        '
        'frmDiagnosticoOS
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(866, 635)
        Me.Controls.Add(Me.cmbAutoriza)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.ToolStripMenu)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmDiagnosticoOS"
        Me.Text = "Diagnostico Ordenes de Servicio"
        Me.ToolStripMenu.ResumeLayout(False)
        Me.ToolStripMenu.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.ConMnuOrdSer.ResumeLayout(False)
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.GrupoCaptura.ResumeLayout(False)
        Me.GrupoCaptura.PerformLayout()
        CType(Me.grdOrdenServDet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ConMnuOrdTra.ResumeLayout(False)
        CType(Me.grdOrdenServCab, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        CType(Me.grdProductosOT, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    'Sub New(ByVal vTag As String, Optional ByVal vNoServ As Integer = 0)
    '    InitializeComponent()
    '    _Tag = vTag
    '    _NoServ = vNoServ
    'End Sub

    '21/SEP/2016
    Public Sub New(con As String, nomUsuario As String, cveEmpresa As Integer, IdUsuario As Integer,
    ByVal TipoOS As TipoOrdenServicio, ByVal vNombreUsuario As String, sqlConComercial As String)
        _con = con
        _sqlConCom = sqlConComercial
        BD = New CapaDatos.UtilSQL(con, "Juan")
        BDCOM = New CapaDatos.UtilSQL(_sqlConCom, "Juan")
        InitializeComponent()
        _Usuario = nomUsuario
        _cveEmpresa = cveEmpresa
        _IdUsuario = IdUsuario
        Inicio.CONSTR = _con
        Inicio.CONSTR_COM = _sqlConCom
        _TipoOrdenServicio = TipoOS
        TipoOrdSer = New TiposOrdenServicio(_TipoOrdenServicio)
        If TipoOrdSer.Existe Then
            txtTipoOrdenServ.Text = TipoOrdSer.nomTipOrdServ
            Me.Text = Me.Text & " - " & TipoOrdSer.nomTipOrdServ
        End If
        _NombreUsuario = vNombreUsuario

    End Sub

    Private Function UltimoConsecutivo(ByVal vNomCampo As String) As Integer

        Dim Obj As New CapaNegocio.Tablas
        Dim Ultimo As Integer = 0
        '11/FEB/2016
        'Ultimo = objNumSig.NumSigEnTablas(Inicio.CONSTR, "NOMOVIMIENTO", "PARAMETROS", "PARAMETROS", " WHERE IDPARAMETRO = 1", " ORDER BY NOMOVIMIENTO DESC")

        Ultimo = objNumSig.NumSigEnTablas(Inicio.CONSTR, vNomCampo, "PARAMETROS", "PARAMETROS", " WHERE IDPARAMETRO = 1", " ORDER BY " & vNomCampo & " DESC")

        Return Ultimo
    End Function
    Private Function UltimoMaster() As Integer

        Dim Obj As New CapaNegocio.Tablas
        Dim Ultimo As Integer = 0
        Ultimo = objNumSig.NumSigEnTablas(Inicio.CONSTR, "idPadreOrdSer", "PARAMETROS", "MasterOrdServ", " WHERE IDPARAMETRO = 1", " ORDER BY idPadreOrdSer DESC")
        'Ultimo = Obj.NumSigEnTablasSinUpdate(Inicio.CONSTR, "Consecutivo", "detPromociones", " WHERE IdPromocion = " & Promocion)

        Return Ultimo
    End Function

    Private Function UltimoOrdenTrab() As Integer

        Dim Obj As New CapaNegocio.Tablas
        Dim Ultimo As Integer = 0
        Ultimo = objNumSig.NumSigEnTablas(Inicio.CONSTR, "idOrdenTrabajo", "PARAMETROS", "DetRecepcionOS", " WHERE IDPARAMETRO = 1", " ORDER BY idOrdenTrabajo DESC")
        'Ultimo = Obj.NumSigEnTablasSinUpdate(Inicio.CONSTR, "Consecutivo", "detPromociones", " WHERE IdPromocion = " & Promocion)

        Return Ultimo
    End Function

    Private Function UltimoOrdServ() As Integer
        Dim Obj As New CapaNegocio.Tablas
        Dim Ultimo As Integer = 0
        Ultimo = objNumSig.NumSigEnTablas(Inicio.CONSTR, "idOrdenSer", "PARAMETROS", "CabOrdenServicio", " WHERE IDPARAMETRO = 1", " ORDER BY idOrdenSer DESC")
        Return Ultimo
    End Function

    Public Function AutoCompletarFolios(ByVal Control As TextBox, Optional ByVal FiltroSinWhere As String = "") As AutoCompleteStringCollection
        Dim Coleccion As New AutoCompleteStringCollection

        PadreOS = New MasterOrdServClass(0)

        Dim dt As DataTable = PadreOS.tbPadreOS()

        For Each row As DataRow In dt.Rows
            Coleccion.Add(Convert.ToString(row("idPadreOrdSer")))
        Next
        'Ajustamos el control TextBox o ComboBox para recibir los datos de la siguiente manera.
        With Control

            .AutoCompleteCustomSource = Coleccion
            .AutoCompleteMode = AutoCompleteMode.Suggest
            .AutoCompleteSource = AutoCompleteSource.CustomSource
        End With

        ' Aca le devuelvo los datos a recuperados de la Base
        Return Coleccion
    End Function

    Public Function AutoCompletarUnidadTransp(ByVal Control As TextBox, ByVal Activo As Boolean, ByVal idEmpresa As Integer, ByVal Tipo As String, Optional ByVal FiltroSinWhere As String = "") As AutoCompleteStringCollection
        Dim Coleccion As New AutoCompleteStringCollection

        UniTrans = New UnidadesTranspClass(0)

        Dim dt As DataTable = UniTrans.TablaUnidadxTipo(Activo, idEmpresa, Tipo, FiltroSinWhere)
        'Recorrer y cargar los items para el Autocompletado
        For Each row As DataRow In dt.Rows
            Coleccion.Add(Convert.ToString(row("idUnidadTrans")))
        Next
        'Ajustamos el control TextBox o ComboBox para recibir los datos de la siguiente manera.
        With Control

            .AutoCompleteCustomSource = Coleccion
            .AutoCompleteMode = AutoCompleteMode.Suggest
            .AutoCompleteSource = AutoCompleteSource.CustomSource
        End With

        ' Aca le devuelvo los datos a recuperados de la Base
        Return Coleccion
    End Function

    Public Function AutoCompletarPersonal(ByVal Control As ComboBox, ByVal TipoCon As TipotablasPer,
                                          ByVal vEmpresa As Integer, Optional ByVal Control2 As ComboBox = Nothing) As AutoCompleteStringCollection

        Dim Coleccion As New AutoCompleteStringCollection
        Dim dt As DataTable

        Personal = New PersonalClass(0)
        If TipoCon = TipotablasPer.tpChoferes Then
            'dt = Personal.TablaChoferesCombo(True, _cveEmpresa)
            dt = Personal.TablaChoferesCombo(True, vEmpresa)
        ElseIf TipoCon = TipotablasPer.tpSupervisores Then
            If _cveEmpresa = 1 Then
                'dt = Personal.TablaAutorizaSolServ(True, _cveEmpresa, 2, 1)
                dt = Personal.TablaAutorizaSolServ(True, vEmpresa, 2, 1)
            ElseIf _cveEmpresa = 2 Then
                'dt = Personal.TablaAutorizaSolServ(True, _cveEmpresa, "1,2,3,5", "")
                dt = Personal.TablaAutorizaSolServ(True, vEmpresa, "1,2,3,5", "")
            End If
        ElseIf TipoCon = TipotablasPer.tpTodos Then
            dt = Personal.TablaAutorizaSolServ(True, vEmpresa, "", "")
        ElseIf TipoCon = TipotablasPer.TpTaller Then
            dt = Personal.TablaAutorizaSolServ(True, 0, "", "6")
        ElseIf TipoCon = TipotablasPer.TpAsignaTaller Then
            'Dim filtroEsp As String = " AND  NOT per.idPersonal IN ( " &
            '"SELECT DISTINCT dros.idPersonalResp AS idPersonal " &
            '"FROM dbo.DetRecepcionOS dros " &
            '"INNER JOIN dbo.CabOrdenServicio cab ON cab.idOrdenSer = dros.idOrdenSer " &
            '"WHERE cab.Estatus IN  ('ASIG','DIAG') AND ISNULL(dros.idPersonalResp,0) > 0 " &
            '"UNION " &
            '"SELECT DISTINCT dros.idPersonalAyu1 AS idPersonal " &
            '"FROM dbo.DetRecepcionOS dros " &
            '"INNER JOIN dbo.CabOrdenServicio cab ON cab.idOrdenSer = dros.idOrdenSer " &
            '"WHERE cab.Estatus IN  ('ASIG','DIAG') AND ISNULL(dros.idPersonalAyu1,0) > 0 " &
            '"union " &
            '"SELECT DISTINCT dros.idPersonalAyu2 AS idPersonal " &
            '"FROM dbo.DetRecepcionOS dros " &
            '"INNER JOIN dbo.CabOrdenServicio cab ON cab.idOrdenSer = dros.idOrdenSer " &
            '"WHERE cab.Estatus IN  ('ASIG','DIAG') AND ISNULL(dros.idPersonalAyu2,0) > 0)"

            dt = Personal.TablaRelPersonalRolOrdServ(True, 0, _TipoOrdenServicio, 3)
        End If

        'Recorrer y cargar los items para el Autocompletado
        For Each row As DataRow In dt.Rows
            Coleccion.Add(Convert.ToString(row("NombreCompleto")))
        Next
        'Ajustamos el control TextBox o ComboBox para recibir los datos de la siguiente manera.
        With Control
            .DataSource = dt
            .DisplayMember = "NombreCompleto"
            .ValueMember = "idPersonal"



            .AutoCompleteCustomSource = Coleccion
            .AutoCompleteMode = AutoCompleteMode.Suggest
            .AutoCompleteSource = AutoCompleteSource.CustomSource
        End With

        If Not Control2 Is Nothing Then
            Control2.DataSource = dt
            Control2.ValueMember = "idpuesto"
            Control2.DisplayMember = "Nompuesto"
        End If

        ' Aca le devuelvo los datos a recuperados de la Base
        Return Coleccion

    End Function

    Private Sub frmDiagnosticoTaller_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'LimpiaCampos()

        Salida = True
        If Inicio.CONSTR = "" Then
            Inicio.CONSTR = _con
        End If

        'LlenaCheckBox()
        'LlenaCombos()
        'Autocompletar()
        Cancelar()
        CreaTablaProdRefacciones()
        CreaTablaDetalle()

        'Salida = False
        If _NoServ > 0 Then
            'txtOrdenTrabajo.Text = _NoServ
            'txtOrdenTrabajo_Leave(sender, e)
        End If

        'AutoCompletarPersonal(cmbChofer, TipotablasPer.tpChoferes)
        'AutoCompletarPersonal(cmbAutoriza, TipotablasPer.tpSupervisores)
        AutoCompletarUnidadTransp(txtTractor, True, _cveEmpresa, "T")
        AutoCompletarUnidadTransp(txtRemolque1, True, _cveEmpresa, "R")
        AutoCompletarUnidadTransp(txtDolly, True, _cveEmpresa, "D")
        AutoCompletarUnidadTransp(txtRemolque2, True, _cveEmpresa, "R")
        AutoCompletarFolios(txtidPadreOrdSer)

        'Personal
        AutoCompletarPersonal(cmbAutoriza, TipotablasPer.tpSupervisores, _cveEmpresa)

        AutoCompletarPersonal(cmbResponsable, TipotablasPer.TpAsignaTaller, 0, cmbPuestoResp)
        AutoCompletarPersonal(cmbayudante1, TipotablasPer.TpAsignaTaller, 0, cmbPuestoAyu1)
        AutoCompletarPersonal(cmbayudante2, TipotablasPer.TpAsignaTaller, 0, cmbPuestoAyu2)


        'AutoCompletarPersonal(cmbRecibe, TipotablasPer.TpTaller, 0)

        'txtNomChofer.AutoCompleteCustomSource = Autocompletar()
        'txtNomChofer.AutoCompleteMode = AutoCompleteMode.Suggest
        'txtNomChofer.AutoCompleteSource = AutoCompleteSource.CustomSource

        'ActivaBotones(False, TipoOpcActivaBoton.tOpcInicializa)
        CreaTablaImprime()

        Empresa = New EmpresaClass(_cveEmpresa)
        If Empresa.Existe Then
            txtNomEmpresa.Text = Empresa.RazonSocial
        End If

        If _TipoOrdenServicio = TipoOrdenServicio.LLANTAS Then
            grdOrdenServDet.Columns("osdidLlanta").Visible = True
            grdOrdenServDet.Columns("osdPosicion").Visible = True
        Else
            grdOrdenServDet.Columns("osdidLlanta").Visible = False
            grdOrdenServDet.Columns("osdPosicion").Visible = False
        End If


        Me.Show()
        'txtidPadreOrdSer.Focus()
        'txtidPadreOrdSer.SelectAll()

        txtOrdenServ.SelectAll()
        txtOrdenServ.Focus()
        'txtTractor.Focus()

        'AgregaComboGrid()

    End Sub

    'Private Sub LlenaCombos()
    '    Try
    '        Personal = New PersonalClass(0)

    '        MyTablaComboResp = Personal.TablaPersonalCombo(" per.iddepto in (6) and per.idpuesto in (10,11,12,13,14,15,16,17,20,21,22,23,24,25) ")
    '        If Not MyTablaComboResp Is Nothing Then
    '            If MyTablaComboResp.Rows.Count > 0 Then
    '                cmbResponsable.DataSource = MyTablaComboResp
    '                cmbResponsable.ValueMember = "idpersonal"
    '                cmbResponsable.DisplayMember = "nombrecompleto"

    '                cmbPuestoResp.DataSource = MyTablaComboResp
    '                cmbPuestoResp.ValueMember = "idpuesto"
    '                cmbPuestoResp.DisplayMember = "Nompuesto"

    '                'DsComboMovimiento = DsCombo
    '            End If
    '        End If

    '        Personal = New PersonalClass(0)
    '        MyTablaComboAyu1 = Personal.TablaPersonalCombo(" per.iddepto in (6) and per.idpuesto in (10,11,12,13,14,15,16,17,20,21,22,23,24,25) ")
    '        If Not MyTablaComboAyu1 Is Nothing Then
    '            If MyTablaComboAyu1.Rows.Count > 0 Then
    '                cmbayudante1.DataSource = MyTablaComboAyu1
    '                cmbayudante1.ValueMember = "idpersonal"
    '                cmbayudante1.DisplayMember = "nombrecompleto"

    '                cmbPuestoAyu1.DataSource = MyTablaComboAyu1
    '                cmbPuestoAyu1.ValueMember = "idpuesto"
    '                cmbPuestoAyu1.DisplayMember = "Nompuesto"
    '            End If
    '        End If

    '        Personal = New PersonalClass(0)
    '        MyTablaComboAyu2 = Personal.TablaPersonalCombo(" per.iddepto in (6) and per.idpuesto in (10,11,12,13,14,15,16,17,20,21,22,23,24,25) ")
    '        If Not MyTablaComboAyu2 Is Nothing Then
    '            If MyTablaComboAyu2.Rows.Count > 0 Then
    '                cmbayudante2.DataSource = MyTablaComboAyu2
    '                cmbayudante2.ValueMember = "idpersonal"
    '                cmbayudante2.DisplayMember = "nombrecompleto"

    '                cmbPuestoAyu2.DataSource = MyTablaComboAyu2
    '                cmbPuestoAyu2.ValueMember = "idpuesto"
    '                cmbPuestoAyu2.DisplayMember = "Nompuesto"
    '            End If
    '        End If

    '    Catch ex As Exception
    '        MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
    '    End Try
    'End Sub


    Private Sub CreaTablaResultados()
        Try
            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Int64")
            myDataColumn.ColumnName = "trefCIDPRODUCTO"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRefac.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Int64")
            myDataColumn.ColumnName = "trefCIDUNIDADBASE"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRefac.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.String")
            myDataColumn.ColumnName = "trefCCODIGOP01_PRO"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRefac.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.String")
            myDataColumn.ColumnName = "trefCNOMBREP01"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRefac.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Double")
            myDataColumn.ColumnName = "trefCOSTO"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRefac.Columns.Add(myDataColumn)


            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.String")
            myDataColumn.ColumnName = "trefCCLAVEINT"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRefac.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Double")
            myDataColumn.ColumnName = "trefExistencia"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRefac.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Double")
            myDataColumn.ColumnName = "trefCantidad"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRefac.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Boolean")
            myDataColumn.ColumnName = "trefBComprar"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRefac.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Double")
            myDataColumn.ColumnName = "trefCantCompra"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRefac.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Double")
            myDataColumn.ColumnName = "trefCantSalida"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRefac.Columns.Add(myDataColumn)

        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try

    End Sub
    Private Sub LimpiaCampos()
        dtpFechaCap.Value = Now
        dtpFechaCapturaHora.Value = Now
        txtNomEmpresa.Text = ""

        txtidPadreOrdSer.Text = ""
        txtOrdenServ.Text = ""

        'txtKilometraje.Text = ""

        txtTractor.Text = ""
        txtRemolque1.Text = ""
        txtDolly.Text = ""
        txtRemolque1.Text = ""

        txtEstatus.Text = ""
        'txtMotivoCancela.Text = ""

        txtChofer.Text = ""
        'txtEmpleadoAutoriza.Text = ""

        'For i As Integer = 1 To chkBoxTipoOrdServ.Items.Count - 1

        '    chkBoxTipoOrdServ.SetItemChecked(i, False)

        'Next

        grdOrdenServCab.DataSource = Nothing
        grdOrdenServCab.Rows.Clear()
        grdOrdenServDet.DataSource = Nothing
        grdOrdenServDet.Rows.Clear()

        grdProductosOT.DataSource = Nothing
        grdProductosOT.Rows.Clear()
        MyTablaRefac.Clear()
        'grdRefaccionesHis.DataSource = Nothing
        'grdRefaccionesHis.Rows.Clear()

        'BorraCamposProd()

        'grdProductosOT.DataSource = Nothing
        'grdProductosOT.Rows.Clear()

        txtTractor.Focus()

    End Sub

    'Private Sub txtOrdenTrabajo_KeyDown(sender As Object, e As KeyEventArgs)
    '    If e.KeyCode = Keys.F3 Then
    '        cmdBuscaClave_Click(sender, e)
    '    End If
    'End Sub

    'Private Sub txtOrdenTrabajo_KeyPress(sender As Object, e As KeyPressEventArgs)
    '    If Asc(e.KeyChar) = 13 Then
    '        If txtOrdenTrabajo.Text.Trim <> "" Then
    '            CargaForm(txtOrdenTrabajo.Text, "OrdenTrabajo")


    '        End If
    '        e.Handled = True
    '    End If
    'End Sub

    'Private Sub txtOrdenTrabajo_Leave(sender As Object, e As EventArgs)
    '    If Salida Then Exit Sub
    '    If txtOrdenTrabajo.Text.Length = 0 Then
    '        txtOrdenTrabajo.Focus()
    '        txtOrdenTrabajo.SelectAll()
    '    Else
    '        If txtOrdenTrabajo.Enabled Then
    '            CargaForm(txtOrdenTrabajo.Text, "OrdenTrabajo")

    '        End If
    '    End If
    'End Sub

    'Private Sub txtOrdenTrabajo_MouseDown(sender As Object, e As MouseEventArgs)
    '    txtOrdenTrabajo.Focus()
    '    txtOrdenTrabajo.SelectAll()
    'End Sub



    Private Sub CargaForm(ByVal idClave As String, ByVal NomTabla As String, Optional ByVal ValorExtra1 As String = "",
                          Optional ByVal txtControl As TextBox = Nothing, Optional ByVal FiltroSinWhere As String = "",
                          Optional ByVal bRequerido As Boolean = False)

        Try
            Select Case UCase(NomTabla)
                'Case UCase("FOLIOOS")
                '    If Val(idClave) > 0 Then
                '        OrdenServ = New OrdenServClass(0)
                '        If OrdenServ.VerificaPadrexTOS(Val(idClave), _TipoOrdenServicio) Then
                '            PadreOS = New MasterOrdServClass(Val(idClave))
                '            If PadreOS.Existe Then


                '                txtTractor.Text = PadreOS.idTractor
                '                UniTrans = New UnidadesTranspClass(PadreOS.idTractor)
                '                If UniTrans.Existe Then
                '                    _cveEmpresa = UniTrans.idEmpresa
                '                End If

                '                txtRemolque1.Text = PadreOS.idTolva1
                '                txtDolly.Text = PadreOS.idDolly
                '                txtRemolque2.Text = PadreOS.idTolva2
                '                dtpFechaCap.Value = PadreOS.FechaCreacion
                '                dtpFechaCapturaHora.Value = PadreOS.FechaCreacion
                '                txtEstatus.Text = PadreOS.Estatus
                '                Empleado = New PersonalClass(PadreOS.idChofer)
                '                If Empleado.Existe Then
                '                    txtChofer.Text = Empleado.NombreCompleto
                '                End If

                '                If PadreOS.Estatus = "ENT" Then
                '                    'cmbEntrega.SelectedValue = PadreOS.idChofer
                '                End If
                '                'Aqui se Consulta las Ordenes de Servicio
                '                Dim TablaOrdSer As DataTable = PadreOS.tbOrdSerxPadrexTipoOrdServ(Val(idClave), _TipoOrdenServicio, " and cab.Estatus in ('DIAG','ASIG')")
                '                If TablaOrdSer.Rows.Count > 0 Then
                '                    For i = 0 To TablaOrdSer.Rows.Count - 1
                '                        InsertaRegGridCab(IIf(TablaOrdSer.DefaultView.Item(i).Item("Estatus") = "DIAG", 1, 0),
                '                        TablaOrdSer.DefaultView.Item(i).Item("CLAVE"),
                '                        TablaOrdSer.DefaultView.Item(i).Item("idTipOrdServ"),
                '                        TablaOrdSer.DefaultView.Item(i).Item("NomTipOrdServ").ToString(),
                '                        TablaOrdSer.DefaultView.Item(i).Item("idUnidadTrans").ToString(),
                '                        TablaOrdSer.DefaultView.Item(i).Item("idTipoUnidad"),
                '                        TablaOrdSer.DefaultView.Item(i).Item("nomTipoUniTras").ToString(),
                '                        TablaOrdSer.DefaultView.Item(i).Item("Kilometraje"),
                '                        TablaOrdSer.DefaultView.Item(i).Item("idEmpleadoAutoriza"),
                '                        TablaOrdSer.DefaultView.Item(i).Item("bOrdenTrab"),
                '                        TablaOrdSer.DefaultView.Item(i).Item("idTipoServicio"),
                '                        False,
                '                        False,
                '                        TablaOrdSer.DefaultView.Item(i).Item("FechaAsignado"),
                '                        TablaOrdSer.DefaultView.Item(i).Item("Estatus"))

                '                    Next
                '                Else
                '                    TablaOrdSer = PadreOS.tbOrdSerxPadrexTipoOrdServ(Val(idClave), _TipoOrdenServicio)
                '                    If TablaOrdSer.Rows.Count > 0 Then
                '                        For j = 0 To TablaOrdSer.Rows.Count - 1
                '                            MsgBox("La Orden de Servicio: " & TablaOrdSer.DefaultView.Item(j).Item("CLAVE") &
                '                                   " Se encuentra en Estatus: " & TablaOrdSer.DefaultView.Item(j).Item("Estatus"), MsgBoxStyle.Information, Me.Text)

                '                            Exit For

                '                        Next

                '                        Cancelar()
                '                        Exit Sub
                '                    End If
                '                End If
                '                Dim TablaDetOrdSer As DataTable = PadreOS.tbDetOrdSerxPadre(Val(idClave), " AND  cabos.idTipOrdServ = " & _TipoOrdenServicio & " and dos.Estatus in ('DIAG','ASIG')")
                '                If TablaDetOrdSer.Rows.Count > 0 Then
                '                    For i = 0 To TablaDetOrdSer.Rows.Count - 1

                '                        If TablaDetOrdSer.DefaultView.Item(i).Item("IDACTIVIDAD") = 0 Then
                '                            If TablaDetOrdSer.DefaultView.Item(i).Item("IDSERVICIO") = 0 Then
                '                                vValorNomAct = TablaDetOrdSer.DefaultView.Item(i).Item("NOTARECEPCION")
                '                                vValorNomDivision = TablaDetOrdSer.DefaultView.Item(i).Item("NOMDIVISIONOT")
                '                                vValorDivision = TablaDetOrdSer.DefaultView.Item(i).Item("IDDIVISIONOT")
                '                            Else
                '                                vValorNomAct = ""
                '                                'vValorNomAct = TablaDetOrdSer.DefaultView.Item(i).Item("NOMSERVICIO")
                '                                vValorNomDivision = TablaDetOrdSer.DefaultView.Item(i).Item("NOMDIVISION")
                '                                vValorDivision = TablaDetOrdSer.DefaultView.Item(i).Item("IDDIVISION")
                '                            End If
                '                        Else
                '                            vValorNomAct = TablaDetOrdSer.DefaultView.Item(i).Item("NOMBREACT")
                '                            vValorNomDivision = TablaDetOrdSer.DefaultView.Item(i).Item("NOMDIVISION")
                '                            vValorDivision = TablaDetOrdSer.DefaultView.Item(i).Item("IDDIVISION")
                '                        End If

                '                        InsertaRegGridDet(IIf(TablaDetOrdSer.DefaultView.Item(i).Item("Estatus") = "DIAG", 1, 0),
                '                        TablaDetOrdSer.DefaultView.Item(i).Item("CLAVE"),
                '                        TablaDetOrdSer.DefaultView.Item(i).Item("IDSERVICIO"),
                '                        TablaDetOrdSer.DefaultView.Item(i).Item("NOMSERVICIO"),
                '                        TablaDetOrdSer.DefaultView.Item(i).Item("IDACTIVIDAD"),
                '                        vValorNomAct,
                '                        vValorDivision,
                '                        vValorNomDivision,
                '                        TablaDetOrdSer.DefaultView.Item(i).Item("DURACIONACTHR"),
                '                        TablaDetOrdSer.DefaultView.Item(i).Item("IDORDENSER"),
                '                        TablaDetOrdSer.DefaultView.Item(i).Item("IDORDENTRABAJO"), False,
                '                        TablaDetOrdSer.DefaultView.Item(i).Item("idOrdenActividad"),
                '                        TablaDetOrdSer.DefaultView.Item(i).Item("idLlanta"),
                '                        TablaDetOrdSer.DefaultView.Item(i).Item("Posicion"),
                '                        TablaDetOrdSer.DefaultView.Item(i).Item("idPersonalResp"),
                '                        TablaDetOrdSer.DefaultView.Item(i).Item("idPersonalAyu1"),
                '                        TablaDetOrdSer.DefaultView.Item(i).Item("idPersonalAyu2"),
                '                        TablaDetOrdSer.DefaultView.Item(i).Item("idEnum"))

                '                        'TablaDetOrdSer.DefaultView.Item(i).Item("idopServLlantas"),


                '                    Next
                '                    'Bloquear Fila que ya se recepciono
                '                    For Each row As DataGridViewRow In grdOrdenServDet.Rows
                '                        If CBool(row.Cells("osdDiagnostico").Value) Then
                '                            row.ReadOnly = True
                '                            'row.Visible = False
                '                        End If
                '                    Next
                '                    grdOrdenServDet.PerformLayout()
                '                    'AgregaComboGrid()
                '                End If
                '                Clave = grdOrdenServCab.Item("oscCLAVE", grdOrdenServCab.CurrentCell.RowIndex).Value
                '                txtEstatusOS.Text = grdOrdenServCab.Item("oscEstatus", grdOrdenServCab.CurrentCell.RowIndex).Value
                '                FiltraDetallexClave(Clave)

                '            Else
                '                MsgBox("El Folio Padre: " & idClave & " no tiene Ordenes de Servicio de " & txtTipoOrdenServ.Text)
                '            End If
                '        Else

                '        End If


                '    Else
                '        If bRequerido Or Val(idClave) > 0 Then
                '            'txtidPadreOrdSer.Text = DespliegaGrid(UCase("UNITRANSTIPO"), Inicio.CONSTR, "", FiltroSinWhere)
                '            txtidPadreOrdSer.Text = DespliegaGrid(UCase("FolioOS"), Inicio.CONSTR, "idPadreOrdSer")
                '            If txtidPadreOrdSer.Text <> "" Then
                '                CargaForm(txtidPadreOrdSer.Text, "FOLIOOS")
                '            End If
                '        End If
                '    End If
                Case UCase("UNITRANS")

                    If Val(idClave) > 0 Then
                        UniTrans = New UnidadesTranspClass(Val(idClave), ValorExtra1)
                    Else
                        UniTrans = New UnidadesTranspClass(idClave)
                    End If

                    If UniTrans.Existe Then
                        txtControl.Text = UniTrans.iUnidadTrans

                        TipoUniTrans = New TipoUniTransClass(UniTrans.idTipoUnidad)

                        If TipoUniTrans.Existe Then
                            If UCase(TipoUniTrans.Clasificacion) = UCase("Tractor") Then
                                vUltKm = UniTrans.UltKilomValidado(UniTrans.iUnidadTrans)
                            End If

                        End If




                    Else
                        'txtControl.Text = DespliegaGrid(UCase("UNITRANSTIPO"), Inicio.CONSTR, "", "tip.TipoUso = 'tractor' AND UNI.ESTATUS = 'ACT' AND UNI.IDEMPRESA = " & _Usuario.personal.idEmpresa & " AND UNI.IDUNIDADTRANS LIKE '%" & Val(idClave) & "%'", "TRACTOR")
                        If bRequerido Or Val(idClave) > 0 Then
                            txtControl.Text = DespliegaGrid(UCase("UNITRANSTIPO"), Inicio.CONSTR, "", FiltroSinWhere)
                            If txtControl.Text <> "" Then
                                CargaForm(txtControl.Text, "UNITRANS", ValorExtra1, txtControl)
                            End If
                        End If
                        'MsgBox("La Unidad de Transporte : " & idClave & " No Existe")
                        'txtControl.Focus()
                        'txtControl.SelectAll()
                    End If



                Case UCase("OrdenServicio")
                    OrdenServ = New OrdenServClass(idClave)
                    With OrdenServ
                        txtOrdenServ.Text = idClave
                        If .Existe Then
                            If .idTipoOrden = _TipoOrdenServicio Then
                                'Si es del tipo
                                txtidPadreOrdSer.Text = .idPadreOrdSer
                                PadreOS = New MasterOrdServClass(Val(.idPadreOrdSer))
                                If PadreOS.Existe Then
                                    txtTractor.Text = PadreOS.idTractor
                                    UniTrans = New UnidadesTranspClass(PadreOS.idTractor)
                                    If UniTrans.Existe Then
                                        _cveEmpresa = UniTrans.idEmpresa
                                    End If

                                    'YA NO
                                    'Empresa = New EmpresaClass(_cveEmpresa)
                                    'If Empresa.Existe Then
                                    '    _InitialCatalog = Empresa.NomBaseDatosCOM
                                    '    _CIDEMPRESA_COM = Empresa.CIDEMPRESA_COM
                                    '    _NomServidor = Empresa.NomServidor

                                    '    _sqlConCom = "Data Source=198.38.85.199\SQLEXPRESS;Initial Catalog=" & _InitialCatalog & ";User ID=prueba;Password=p54321"
                                    '    BDCOM = New CapaDatos.UtilSQL(_sqlConCom, "Juan")


                                    '    'CreCadenaConexion()
                                    '    '    Dim value2 As String = value1.Replace("XX", "Areo")
                                    'End If

                                    txtRemolque1.Text = PadreOS.idTolva1
                                    txtDolly.Text = PadreOS.idDolly
                                    txtRemolque2.Text = PadreOS.idTolva2
                                    dtpFechaCap.Value = PadreOS.FechaCreacion
                                    dtpFechaCapturaHora.Value = PadreOS.FechaCreacion
                                    txtEstatus.Text = PadreOS.Estatus
                                    Empleado = New PersonalClass(PadreOS.idChofer)
                                    If Empleado.Existe Then
                                        txtChofer.Text = Empleado.NombreCompleto
                                    End If



                                    'If PadreOS.Estatus = "ENT" Then
                                    '    'cmbEntrega.SelectedValue = PadreOS.idChofer
                                    'End If
                                    'Aqui se Consulta las Ordenes de Servicio
                                    Dim TablaOrdSer As DataTable = PadreOS.tbOrdSerxPadrexTipoOrdServ(Val(.idPadreOrdSer),
                                    _TipoOrdenServicio, " and cab.idOrdenSer = " & idClave & " and cab.Estatus in ('DIAG','ASIG')")
                                    If TablaOrdSer.Rows.Count > 0 Then
                                        For i = 0 To TablaOrdSer.Rows.Count - 1
                                            InsertaRegGridCab(IIf(TablaOrdSer.DefaultView.Item(i).Item("Estatus") = "DIAG", 1, 0),
                                            TablaOrdSer.DefaultView.Item(i).Item("CLAVE"),
                                            TablaOrdSer.DefaultView.Item(i).Item("idTipOrdServ"),
                                            TablaOrdSer.DefaultView.Item(i).Item("NomTipOrdServ").ToString(),
                                            TablaOrdSer.DefaultView.Item(i).Item("idUnidadTrans").ToString(),
                                            TablaOrdSer.DefaultView.Item(i).Item("idTipoUnidad"),
                                            TablaOrdSer.DefaultView.Item(i).Item("nomTipoUniTras").ToString(),
                                            TablaOrdSer.DefaultView.Item(i).Item("Kilometraje"),
                                            TablaOrdSer.DefaultView.Item(i).Item("idEmpleadoAutoriza"),
                                            TablaOrdSer.DefaultView.Item(i).Item("bOrdenTrab"),
                                            TablaOrdSer.DefaultView.Item(i).Item("idTipoServicio"),
                                            False,
                                            False,
                                            TablaOrdSer.DefaultView.Item(i).Item("FechaAsignado"),
                                            TablaOrdSer.DefaultView.Item(i).Item("Estatus"))

                                        Next
                                        cmbAutoriza.SelectedValue = TablaOrdSer.DefaultView.Item(0).Item("idEmpleadoAutoriza")
                                    End If
                                    Dim TablaDetOrdSer As DataTable = PadreOS.tbDetOrdSerxPadre(Val(.idPadreOrdSer),
                                    " and DOS.IDORDENSER = " & idClave & " AND  cabos.idTipOrdServ = " & _TipoOrdenServicio & " and dos.Estatus in ('DIAG','ASIG')")
                                    If TablaDetOrdSer.Rows.Count > 0 Then
                                        For i = 0 To TablaDetOrdSer.Rows.Count - 1

                                            If TablaDetOrdSer.DefaultView.Item(i).Item("IDACTIVIDAD") = 0 Then
                                                If TablaDetOrdSer.DefaultView.Item(i).Item("IDSERVICIO") = 0 Then
                                                    vValorNomAct = TablaDetOrdSer.DefaultView.Item(i).Item("NOTARECEPCION")
                                                    vValorNomDivision = TablaDetOrdSer.DefaultView.Item(i).Item("NOMDIVISIONOT")
                                                    vValorDivision = TablaDetOrdSer.DefaultView.Item(i).Item("IDDIVISIONOT")
                                                Else
                                                    vValorNomAct = ""
                                                    'vValorNomAct = TablaDetOrdSer.DefaultView.Item(i).Item("NOMSERVICIO")
                                                    vValorNomDivision = TablaDetOrdSer.DefaultView.Item(i).Item("NOMDIVISION")
                                                    vValorDivision = TablaDetOrdSer.DefaultView.Item(i).Item("IDDIVISION")
                                                End If
                                            Else
                                                vValorNomAct = TablaDetOrdSer.DefaultView.Item(i).Item("NOMBREACT")
                                                vValorNomDivision = TablaDetOrdSer.DefaultView.Item(i).Item("NOMDIVISION")
                                                vValorDivision = TablaDetOrdSer.DefaultView.Item(i).Item("IDDIVISION")
                                            End If

                                            InsertaRegGridDet(IIf(TablaDetOrdSer.DefaultView.Item(i).Item("Estatus") = "DIAG", 1, 0),
                                            TablaDetOrdSer.DefaultView.Item(i).Item("CLAVE"),
                                            TablaDetOrdSer.DefaultView.Item(i).Item("IDSERVICIO"),
                                            TablaDetOrdSer.DefaultView.Item(i).Item("NOMSERVICIO"),
                                            TablaDetOrdSer.DefaultView.Item(i).Item("IDACTIVIDAD"),
                                            vValorNomAct,
                                            vValorDivision,
                                            vValorNomDivision,
                                            TablaDetOrdSer.DefaultView.Item(i).Item("DURACIONACTHR"),
                                            TablaDetOrdSer.DefaultView.Item(i).Item("IDORDENSER"),
                                            TablaDetOrdSer.DefaultView.Item(i).Item("IDORDENTRABAJO"), False,
                                            TablaDetOrdSer.DefaultView.Item(i).Item("idOrdenActividad"),
                                            TablaDetOrdSer.DefaultView.Item(i).Item("idLlanta"),
                                            TablaDetOrdSer.DefaultView.Item(i).Item("Posicion"),
                                            TablaDetOrdSer.DefaultView.Item(i).Item("idPersonalResp"),
                                            TablaDetOrdSer.DefaultView.Item(i).Item("idPersonalAyu1"),
                                            TablaDetOrdSer.DefaultView.Item(i).Item("idPersonalAyu2"),
                                            TablaDetOrdSer.DefaultView.Item(i).Item("idEnum"),
                                            TablaDetOrdSer.DefaultView.Item(i).Item("Estatus"),
                                                              TablaDetOrdSer.DefaultView.Item(i).Item("CIDPRODUCTO_SERV"),
                                                              TablaDetOrdSer.DefaultView.Item(i).Item("CCODIGOPRODUCTO"),
                                                              TablaDetOrdSer.DefaultView.Item(i).Item("CNOMBREPRODUCTO"),
                                                              TablaDetOrdSer.DefaultView.Item(i).Item("Cantidad_CIDPRODUCTO_SERV"),
                                                              TablaDetOrdSer.DefaultView.Item(i).Item("Precio_CIDPRODUCTO_SERV"))


                                        Next
                                        'Bloquear Fila que ya se recepciono
                                        For Each row As DataGridViewRow In grdOrdenServDet.Rows
                                            If CBool(row.Cells("osdDiagnostico").Value) Then
                                                row.ReadOnly = True
                                                'row.Visible = False
                                            End If
                                        Next
                                        grdOrdenServDet.PerformLayout()
                                        'AgregaComboGrid()

                                    End If
                                    Clave = grdOrdenServCab.Item("oscCLAVE", grdOrdenServCab.CurrentCell.RowIndex).Value
                                    txtEstatusOS.Text = grdOrdenServCab.Item("oscEstatus", grdOrdenServCab.CurrentCell.RowIndex).Value
                                    FiltraDetallexClave(Clave)
                                    'MyTablaRefac.Clear()


                                Else
                                    MsgBox("El Folio Padre: " & idClave & " no tiene Ordenes de Servicio de " & txtTipoOrdenServ.Text)
                                End If
                            End If


                            'dtpFechaOrden.Value = OrdenServ.FechaRecepcion
                            'dtpFechaOrdenhr.Value = OrdenServ.FechaRecepcion

                            Emp = New EmpresaClass(OrdenServ.IdEmpresa)
                            If Emp.Existe Then
                                txtNomEmpresa.Text = Emp.RazonSocial
                            End If

                            TipOrd = New TipoOrdenClass(OrdenServ.idTipoOrden)
                            'If TipOrd.Existe Then
                            '    txtTipoOrden.Text = TipOrd.NomTipoOrden
                            'End If
                            'txtEstatusOrden.Text = OrdenServ.Estatus

                            '10/ENE/2017
                            'txtKilometraje.Text = OrdenServ.Kilometraje

                            'RECEPCIONO
                            'User = New UsuarioClass(OrdenServ.UsuarioRecepcion)
                            'If User.Existe Then
                            '    txtRecepciono.Text = User.NombreUsuario
                            'End If
                            'ENTREGO
                            'Personal = New PersonalClass(OrdenServ.idEmpleadoEntrega)
                            'If Personal.Existe Then
                            '    txtEntrego.Text = Personal.NombreCompleto
                            'End If
                            'AUTORIZO
                            'Personal = New PersonalClass(OrdenServ.idEmpleadoAutoriza)
                            'If Personal.Existe Then
                            '    txtAutorizo.Text = Personal.NombreCompleto
                            'End If
                        End If
                    End With
                Case UCase("UNIPROD_COM")
                    UnidadCom = New UnidadesComClass(idClave, "CCLAVEINT", TipoDato.TdCadena)
                    If UnidadCom.Existe Then
                        txtNomUniProd.Text = UnidadCom.CNOMBREUNIDAD
                        vCIDUNIDAD = UnidadCom.CIDUNIDAD
                    End If
                Case UCase("PROD_COM")
                    ProdCom = New ProductosComClass(idClave, "")
                    If ProdCom.Existe Then
                        If ProdCom.CSTATUSPRODUCTO = 1 Then
                            vCIDPRODUCTO = ProdCom.CIDPRODUCTO
                            txtNomProducto.Text = ProdCom.CNOMBREPRODUCTO
                            UnidadCom = New UnidadesComClass(ProdCom.CIDUNIDADBASE, "CIDUNIDAD", TipoDato.TdNumerico)
                            If UnidadCom.Existe Then
                                txtIdUnidadProd.Text = UnidadCom.CABREVIATURA
                                txtNomUniProd.Text = UnidadCom.CNOMBREUNIDAD
                            End If

                        Else
                            'Desactivado
                        End If
                    End If
                Case UCase("OrdenTrabajo")
                    OrdenTrabajo = New OrdenTrabajoClass(idClave)
                    With OrdenTrabajo
                        'ORDEN DE TRABAJO
                        'txtOrdenTrabajo.Text = idClave
                        If .Existe Then
                            'MECANICO RESPONSABLE
                            'Personal = New PersonalClass(.idPersonalResp)
                            'cmbResponsable.SelectedValue = .idPersonalResp
                            'If Personal.Existe Then
                            '    'txtNombreMecanico.Text = Personal.NombreCompleto

                            '    PersonalPuesto = New PuestosClass(Personal.idPuesto)
                            '    If PersonalPuesto.Existe Then
                            '        'txtPuestoMec.Text = PersonalPuesto.NomPuesto
                            '    End If
                            'End If
                            ''AYUDANTE 1
                            'If .idPersonalAyu1 > 0 Then
                            '    chkAyu1.Checked = True
                            '    Personal = New PersonalClass(.idPersonalAyu1)
                            '    cmbayudante1.SelectedValue = .idPersonalAyu1
                            'Else
                            '    chkAyu1.Checked = False
                            'End If

                            ''AYUDANTE 2
                            'If .idPersonalAyu2 > 0 Then
                            '    chkAyu2.Checked = True
                            '    Personal = New PersonalClass(.idPersonalAyu2)
                            '    cmbayudante2.SelectedValue = .idPersonalAyu2
                            'Else
                            '    chkAyu2.Checked = False
                            'End If


                            'DETALLE ORDEN DE SERVICIO
                            DetOS = New detOrdenServicioClass(idClave)
                            If DetOS.Existe Then
                                dtpFechaCap.Value = DetOS.FechaAsig
                                dtpFechaCapturaHora.Value = DetOS.FechaAsig
                                User = New UsuarioClass(DetOS.UsuarioAsig)
                                'If User.Existe Then
                                '    txtUsuarioAsig.Text = User.NombreUsuario
                                'End If
                            End If

                            'ORDEN DE SERVICIO
                            OrdenServ = New OrdenServClass(.idOrdenSer)

                            txtidPadreOrdSer.Text = .idOrdenSer
                            'dtpFechaOrden.Value = OrdenServ.FechaRecepcion
                            'dtpFechaOrdenhr.Value = OrdenServ.FechaRecepcion

                            Emp = New EmpresaClass(OrdenServ.IdEmpresa)
                            If Emp.Existe Then
                                txtNomEmpresa.Text = Emp.RazonSocial
                            End If

                            'TipOrd = New TipoOrdenClass(OrdenServ.idTipoOrden)
                            'If TipOrd.Existe Then
                            '    txtTipoOrden.Text = TipOrd.NomTipoOrden
                            'End If
                            'txtEstatusOrden.Text = OrdenServ.Estatus

                            '10/ENE/2017
                            'txtKilometraje.Text = OrdenServ.Kilometraje

                            'RECEPCIONO
                            'User = New UsuarioClass(OrdenServ.UsuarioRecepcion)
                            'If User.Existe Then
                            '    txtRecepciono.Text = User.NombreUsuario
                            'End If
                            'ENTREGO
                            'Personal = New PersonalClass(OrdenServ.idEmpleadoEntrega)
                            'If Personal.Existe Then
                            '    txtEntrego.Text = Personal.NombreCompleto
                            'End If
                            'AUTORIZO
                            'Personal = New PersonalClass(OrdenServ.idEmpleadoAutoriza)
                            'If Personal.Existe Then
                            '    txtAutorizo.Text = Personal.NombreCompleto
                            'End If
                            'ORDEN DE SERVICIO

                            'UNIDAD DE TRANSPORTE
                            'txtIdUnidad.Text = OrdenServ.idUnidadTrans
                            'UniTrans = New UnidadesTranspClass(OrdenServ.idUnidadTrans)
                            'If UniTrans.Existe Then
                            '    txtNomUnidad.Text = UniTrans.DescripcionUni
                            '    Marca = New MarcaClass(UniTrans.idMarca)
                            '    If Marca.Existe Then
                            '        txtMarcaUni.Text = Marca.NOMBREMARCA
                            '    End If
                            '    'ULTIMA ORDEN DE SERVICIO

                            '    If UniTrans.UltOT > 0 Then
                            '        txtUltOrdenTrabajo.Text = UniTrans.UltOT
                            '        ULTOT = New OrdenTrabajoClass(UniTrans.UltOT)
                            '        If ULTOT.Existe Then
                            '            txtUltimaOrdenServ.Text = ULTOT.idOrdenSer
                            '            Personal = New PersonalClass(ULTOT.idPersonalResp)
                            '            If Personal.Existe Then
                            '                txtUltimoMecanico.Text = Personal.NombreCompleto
                            '            End If

                            '            txtUltFalla.Text = ULTOT.NotaRecepcion
                            '            FamiliaOT = New FamiliasClass(ULTOT.idFamilia)
                            '            If FamiliaOT.Existe Then
                            '                txtUltFamilia.Text = FamiliaOT.NomFamilia
                            '                txtUltDivision.Text = FamiliaOT.NomDivision
                            '            End If
                            '        End If
                            '    Else
                            '        txtUltOrdenTrabajo.Text = ""
                            '    End If
                            'End If


                            'Historial
                            CargaOrdenesSerHis(OrdenServ.idUnidadTrans)

                            IdOrdenServ = grdOrdenServCab.Item("hosIdServicio", grdOrdenServCab.CurrentCell.RowIndex).Value
                            'CargaOrdenesTrabHis(txtIdUnidad.Text, IdOrdenServ)

                            'VALIDACIONES
                            If OrdenServ.Estatus = "ASIG" Then
                                ActivaCampos(False, TipoOpcActivaCampos.tOpcINICIALIZA)
                                ActivaBotones(True, TipoOpcActivaBoton.tOpcInicializa)

                                'txtDuracion.Focus()
                                'txtDuracion.SelectAll()
                            ElseIf OrdenServ.Estatus = "DIA" Then
                                MsgBox("La Orden de Trabajo: " & idClave & " ya Esta en Proceso, No puede Modificarse")
                                ActivaCampos(False, TipoOpcActivaCampos.tOpcINICIALIZA)

                                ActivaBotones(False, TipoOpcActivaBoton.tOpcInicializa)
                                btnMnuLimpiar.Enabled = True
                                'txtOrdenTrabajo.Focus()
                                'txtOrdenTrabajo.SelectAll()

                            ElseIf OrdenServ.Estatus = "REC" Then
                                MsgBox("La Orden de Trabajo: " & idClave & " No esta Asignada, No puede Modificarse")
                                'txtOrdenTrabajo.Focus()
                                'txtOrdenTrabajo.SelectAll()

                            End If
                        Else
                            'No existe 
                            MsgBox("La Orden de Trabajo: " & idClave & " No Existe !!!")
                            'txtOrdenTrabajo.Focus()
                            'txtOrdenTrabajo.SelectAll()
                        End If


                    End With
                    'Case UCase(TablaBd2)
                    '    'aqui()
                    '    TipoServ = New TipoServClass(idClave)
                    '    With TipoServ
                    '        txtidTipoUnidad.Text = .idTipoServicio
                    '        If .Existe Then
                    '            txtnomTipoUniTras.Text = .NomTipoServicio
                    '            txtDescripcionUni.Focus()
                    '            txtDescripcionUni.SelectAll()
                    '        Else
                    '            MsgBox("El Tipo de Servicio con id: " & txtidTipoUnidad.Text & " No Existe")
                    '            txtidTipoUnidad.Focus()
                    '            txtidTipoUnidad.SelectAll()
                    '        End If
                    '    End With

            End Select



        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try

    End Sub



    Private Sub btnMnuSalir_Click(sender As Object, e As EventArgs) Handles btnMnuSalir.Click
        Me.Close()
    End Sub

    Private Sub btnMnuCancelar_Click(sender As Object, e As EventArgs) Handles btnMnuLimpiar.Click
        Cancelar()
        txtidPadreOrdSer.Focus()
    End Sub

    Private Sub CargaOrdenesSerHis(ByVal vidUnidadTrans As String)
        Try
            MyTablaHisOS.Rows.Clear()
            'grdOrdenServHis.DataSource = DetOS.CargaOrdSerxUni(OrdenServ.idUnidadTrans)
            MyTablaHisOS = DetOS.CargaOrdSerxUni(vidUnidadTrans)
            grdOrdenServCab.DataSource = Nothing
            grdOrdenServCab.Rows.Clear()

            If MyTablaHisOS.Rows.Count > 0 Then
                For i = 0 To MyTablaHisOS.DefaultView.Count - 1
                    grdOrdenServCab.Rows.Add()
                    grdOrdenServCab.Item("hosConsecutivo", i).Value = i + 1
                    'grdOrdenServHis.Item("hosIdServicio", i).Value = Trim(MyTabla.DefaultView.Item(i).Item("idOrdenSer").ToString)
                    grdOrdenServCab.Item("hosIdServicio", i).Value = MyTablaHisOS.DefaultView.Item(i).Item("idOrdenSer")
                    grdOrdenServCab.Item("hosFechaRecepcion", i).Value = MyTablaHisOS.DefaultView.Item(i).Item("FechaRecepcion")
                    grdOrdenServCab.Item("hosTipoOrden", i).Value = MyTablaHisOS.DefaultView.Item(i).Item("idTipoOrden")
                    grdOrdenServCab.Item("hosKm", i).Value = MyTablaHisOS.DefaultView.Item(i).Item("Kilometraje")
                    grdOrdenServCab.Item("hosAutorizo", i).Value = MyTablaHisOS.DefaultView.Item(i).Item("idEmpleadoAutoriza")
                    grdOrdenServCab.Item("hosEntrego", i).Value = MyTablaHisOS.DefaultView.Item(i).Item("idEmpleadoEntrega")
                    grdOrdenServCab.Item("hosRecepciono", i).Value = MyTablaHisOS.DefaultView.Item(i).Item("UsuarioRecepcion")
                    grdOrdenServCab.Item("hosFinalizo", i).Value = MyTablaHisOS.DefaultView.Item(i).Item("UsuarioEntrega")
                Next
                grdOrdenServCab.PerformLayout()
            End If

        Catch ex As Exception

        End Try
    End Sub
    Private Sub CargaOrdenesTrabHis(ByVal vidUnidadTrans As String, Optional ByVal vidOrdenSer As Integer = 0)
        Try
            MyTablaHisOT.Rows.Clear()
            'grdOrdenServHis.DataSource = DetOS.CargaOrdSerxUni(OrdenServ.idUnidadTrans)
            MyTablaHisOT = DetOS.CargaOrdTrabxUni(vidUnidadTrans, vidOrdenSer)
            grdOrdenServDet.DataSource = Nothing
            grdOrdenServDet.Rows.Clear()

            If MyTablaHisOT.Rows.Count > 0 Then
                For i = 0 To MyTablaHisOT.DefaultView.Count - 1
                    grdOrdenServDet.Rows.Add()
                    'grdOrdenServHis.Item("hosIdServicio", i).Value = Trim(MyTabla.DefaultView.Item(i).Item("idOrdenSer").ToString)
                    grdOrdenServDet.Item("hotIdOrdenSer", i).Value = MyTablaHisOT.DefaultView.Item(i).Item("idOrdenSer")
                    grdOrdenServDet.Item("hotidOrdenTrabajo", i).Value = MyTablaHisOT.DefaultView.Item(i).Item("idOrdenTrabajo")
                    grdOrdenServDet.Item("hotFechaFin", i).Value = MyTablaHisOT.DefaultView.Item(i).Item("FechaTerminado")
                    grdOrdenServDet.Item("hotAsigno", i).Value = MyTablaHisOT.DefaultView.Item(i).Item("UsuarioAsig")
                    grdOrdenServDet.Item("hotMecanico", i).Value = MyTablaHisOT.DefaultView.Item(i).Item("idPersonalResp")
                    grdOrdenServDet.Item("hotDuracion", i).Value = MyTablaHisOT.DefaultView.Item(i).Item("DuracionActHr")
                Next
                grdOrdenServDet.PerformLayout()
            End If

        Catch ex As Exception

        End Try
    End Sub



    'Private Sub grdOrdenServHis_Click(sender As Object, e As EventArgs)
    '    'DataTabla.DefaultView.RowFilter = Nothing
    '    'EncuentraDetalle(grdOrdenServHis.Item("hosIdServicio", grdOrdenServHis.CurrentCell.RowIndex).Value)
    '    IdOrdenServ = grdOrdenServCab.Item("hosIdServicio", grdOrdenServCab.CurrentCell.RowIndex).Value
    '    'CargaOrdenesTrabHis(txtIdUnidad.Text, IdOrdenServ)
    'End Sub
    Private Sub EncuentraDetalle(ByVal idOrdenSer As Integer)

        If MyTablaHisOT.DefaultView.Count > 0 Then
            With MyTablaHisOT
                .DefaultView.RowFilter = "idOrdenSer = " & idOrdenSer
                If .DefaultView.Count > 0 Then
                    'grdOrdenTrabajoHis.DataSource = .DefaultView
                End If
            End With
        End If

    End Sub


    'Private Sub cmdBuscaProd_Click(sender As Object, e As EventArgs)
    '    Salida = True
    '    txtIdProducto.Text = DespliegaGrid(UCase("PROD_COM"), Inicio.CONSTR_COM, "CCODIGOPRODUCTO")
    '    If txtIdProducto.Text <> "" Then CargaForm(txtIdProducto.Text, UCase("PROD_COM"))
    '    CargaForm(txtIdUnidadProd.Text, UCase("UNIPROD_COM"))

    '    txtCantidad.Focus()
    '    txtCantidad.SelectAll()
    '    Salida = False
    'End Sub

    'Private Sub txtIdProducto_EnabledChanged(sender As Object, e As EventArgs)
    '    cmdBuscaProd.Enabled = txtIdProducto.Enabled
    'End Sub

    'Private Sub txtIdProducto_KeyDown(sender As Object, e As KeyEventArgs)
    '    If e.KeyCode = Keys.F3 Then
    '        cmdBuscaProd_Click(sender, e)
    '    End If

    'End Sub

    'Private Sub txtIdProducto_KeyPress(sender As Object, e As KeyPressEventArgs)
    '    If txtIdProducto.Text.Length = 0 Then
    '        txtIdProducto.Focus()
    '        txtIdProducto.SelectAll()
    '    Else
    '        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
    '            Salida = True
    '            CargaForm(txtIdProducto.Text, UCase("PROD_COM"))
    '            CargaForm(txtIdUnidadProd.Text, UCase("UNIPROD_COM"))

    '            txtCantidad.Focus()
    '            txtCantidad.SelectAll()
    '            Salida = False
    '        End If
    '    End If
    'End Sub

    'Private Sub txtIdProducto_Leave(sender As Object, e As EventArgs)
    '    If Salida Then Exit Sub
    '    If txtIdProducto.Text.Length = 0 Then
    '        txtIdProducto.Focus()
    '        txtIdProducto.SelectAll()
    '    Else
    '        If txtIdProducto.Enabled Then
    '            CargaForm(txtIdProducto.Text, UCase("PROD_COM"))
    '            CargaForm(txtIdUnidadProd.Text, UCase("UNIPROD_COM"))

    '            txtCantidad.Focus()
    '            txtCantidad.SelectAll()
    '        End If
    '    End If
    'End Sub


    'Private Sub cmdBuscaUniProd_Click(sender As Object, e As EventArgs)
    '    Salida = True
    '    txtIdProducto.Text = DespliegaGrid(UCase("UNIPROD_COM"), Inicio.CONSTR_COM, "CCODIGOPRODUCTO")
    '    If txtIdProducto.Text <> "" Then CargaForm(txtIdProducto.Text, UCase("PROD_COM"))
    '    txtCantidad.Focus()
    '    txtCantidad.SelectAll()
    '    Salida = False
    'End Sub

    'Private Sub txtIdUnidadProd_EnabledChanged(sender As Object, e As EventArgs)
    '    cmdBuscaUniProd.Enabled = txtIdUnidadProd.Enabled
    'End Sub

    'Private Sub txtIdUnidadProd_KeyDown(sender As Object, e As KeyEventArgs)
    '    If e.KeyCode = Keys.F3 Then
    '        cmdBuscaUniProd_Click(sender, e)
    '    End If

    'End Sub

    'Private Sub txtIdUnidadProd_KeyPress(sender As Object, e As KeyPressEventArgs)
    '    If txtIdUnidadProd.Text.Length = 0 Then
    '        txtIdUnidadProd.Focus()
    '        txtIdUnidadProd.SelectAll()
    '    Else
    '        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
    '            Salida = True
    '            CargaForm(txtIdUnidadProd.Text, UCase("UNIPROD_COM"))

    '            txtCantidad.Focus()
    '            txtCantidad.SelectAll()
    '            Salida = False
    '        End If
    '    End If

    'End Sub

    'Private Sub txtIdUnidadProd_Leave(sender As Object, e As EventArgs)
    '    If Salida Then Exit Sub
    '    If txtIdUnidadProd.Text.Length = 0 Then
    '        txtIdUnidadProd.Focus()
    '        txtIdUnidadProd.SelectAll()
    '    Else
    '        If txtIdProducto.Enabled Then
    '            CargaForm(txtIdUnidadProd.Text, UCase("UNIPROD_COM"))
    '            txtCantidad.Focus()
    '            txtCantidad.SelectAll()
    '        End If
    '    End If
    'End Sub

    'Private Sub txtCantidad_KeyPress(sender As Object, e As KeyPressEventArgs)
    '    If txtCantidad.Text.Length = 0 Then
    '        txtCantidad.Focus()
    '        txtCantidad.SelectAll()
    '    Else
    '        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
    '            Salida = True
    '            'CargaForm(txtIdUnidadProd.Text, UCase("UNIPROD_COM"))
    '            'Agrega a Grid
    '            GrabaProductos()
    '            BorraCamposProd()

    '            txtIdProducto.Focus()
    '            txtIdProducto.SelectAll()
    '            Salida = False
    '        End If
    '    End If
    'End Sub

    'Private Sub GrabaProductos()
    '    GrabaGridProductos()
    'End Sub

    'Private Sub GrabaGridProductos()
    '    Dim BandGraba As Boolean = False
    '    Dim vExistencia As Double = 0
    '    Dim vCosto As Double = 0
    '    Dim CantCompra As Double = 0
    '    Dim CantSalida As Double = 0
    '    Try
    '        If txtIdProducto.Text <> "" Then
    '            If (txtCantidad.Text <> CantidadGrid) Or txtCantidad.Text = 0 Then
    '                BandGraba = True
    '            End If

    '            If BandGraba Then
    '                If grdProductosOT.Rows.Count = 0 Then
    '                    MyTablaRefac.Clear()
    '                    BandNoExisteProdEnGrid = True
    '                End If
    '                'Determinar si el producto existe en el grid, si no existe hay que agregarlo
    '                If BandNoExisteProdEnGrid Then
    '                    Salida = True
    '                    myDataRow = MyTablaRefac.NewRow()
    '                    '
    '                    myDataRow("trefCIDPRODUCTO") = vCIDPRODUCTO
    '                    myDataRow("trefCIDUNIDADBASE") = vCIDUNIDAD

    '                    myDataRow("trefCCODIGOP01_PRO") = txtIdProducto.Text
    '                    myDataRow("trefCNOMBREP01") = txtNomProducto.Text

    '                    'Calcula Existencia
    '                    vExistencia = ObjCom.Existencia(vCIDPRODUCTO, Now, vIdAlmacen)
    '                    myDataRow("trefExistencia") = vExistencia

    '                    myDataRow("trefCantidad") = Val(txtCantidad.Text)
    '                    myDataRow("trefCCLAVEINT") = txtIdUnidadProd.Text
    '                    myDataRow("trefBComprar") = IIf(vExistencia < Val(txtCantidad.Text), True, False)

    '                    'Calcula Costo
    '                    vCosto = ObjCom.ChecaCosto(vCIDPRODUCTO, Now, vIdAlmacen)
    '                    myDataRow("trefCOSTO") = vCosto

    '                    'Cantidades
    '                    'Por Comprar y Salidas
    '                    If vExistencia >= Val(txtCantidad.Text) Then
    '                        CantCompra = 0
    '                        CantSalida = Val(txtCantidad.Text)
    '                    Else
    '                        CantCompra = Val(txtCantidad.Text) - vExistencia
    '                        CantSalida = vExistencia
    '                    End If
    '                    myDataRow("trefCantCompra") = CantCompra
    '                    myDataRow("trefCantSalida") = CantSalida

    '                    MyTablaRefac.Rows.Add(myDataRow)

    '                    'Grabamos Producto
    '                    GrabaProductoLOC(txtIdProducto.Text)
    '                    GrabaUnidadLOC(vCIDUNIDAD)


    '                    InsertaRegGrid()

    '                    BandNoExisteProdEnGrid = False
    '                    Salida = False
    '                Else
    '                    For i = 0 To MyTablaRefac.DefaultView.Count - 1
    '                        BandNoExisteProdEnGrid = True
    '                        If Trim(txtIdProducto.Text) = Trim(MyTablaRefac.DefaultView.Item(i).Item("trefCCODIGOP01_PRO").ToString) And Trim(txtIdUnidadProd.Text) = Trim(grdProductosOT.Item("grefCCLAVEINT", i).Value) Then
    '                            MyTablaRefac.DefaultView.Item(i).Item("trefCIDPRODUCTO") = vCIDPRODUCTO
    '                            MyTablaRefac.DefaultView.Item(i).Item("trefCIDUNIDADBASE") = vCIDUNIDAD

    '                            MyTablaRefac.DefaultView.Item(i).Item("trefCCODIGOP01_PRO") = txtIdProducto.Text
    '                            MyTablaRefac.DefaultView.Item(i).Item("trefCNOMBREP01") = txtNomProducto.Text
    '                            'Calcula Existencia
    '                            vExistencia = ObjCom.Existencia(vCIDPRODUCTO, Now, vIdAlmacen)
    '                            MyTablaRefac.DefaultView.Item(i).Item("trefExistencia") = vExistencia

    '                            MyTablaRefac.DefaultView.Item(i).Item("trefCantidad") = Val(txtCantidad.Text)
    '                            MyTablaRefac.DefaultView.Item(i).Item("trefCCLAVEINT") = txtIdUnidadProd.Text

    '                            MyTablaRefac.DefaultView.Item(i).Item("trefBComprar") = IIf(vExistencia < Val(txtCantidad.Text), True, False)

    '                            'Calcula Costo
    '                            vCosto = ObjCom.ChecaCosto(vCIDPRODUCTO, Now, vIdAlmacen)
    '                            MyTablaRefac.DefaultView.Item(i).Item("trefCOSTO") = vCosto

    '                            If vExistencia >= Val(txtCantidad.Text) Then
    '                                CantCompra = 0
    '                                CantSalida = Val(txtCantidad.Text)
    '                            Else
    '                                CantCompra = Val(txtCantidad.Text) - vExistencia
    '                                CantSalida = vExistencia
    '                            End If
    '                            MyTablaRefac.DefaultView.Item(i).Item("trefCantCompra") = CantCompra
    '                            MyTablaRefac.DefaultView.Item(i).Item("trefCantSalida") = CantSalida

    '                            'Grabamos Producto
    '                            GrabaProductoLOC(txtIdProducto.Text)
    '                            GrabaUnidadLOC(vCIDUNIDAD)

    '                            BandNoExisteProdEnGrid = False
    '                            Exit For
    '                        End If
    '                    Next
    '                    If BandNoExisteProdEnGrid Then
    '                        Salida = True
    '                        myDataRow = MyTablaRefac.NewRow()
    '                        myDataRow("trefCIDPRODUCTO") = vCIDPRODUCTO
    '                        myDataRow("trefCIDUNIDADBASE") = vCIDUNIDAD

    '                        myDataRow("trefCCODIGOP01_PRO") = txtIdProducto.Text
    '                        myDataRow("trefCNOMBREP01") = txtNomProducto.Text
    '                        'Calcula Existencia
    '                        vExistencia = ObjCom.Existencia(vCIDPRODUCTO, Now, vIdAlmacen)
    '                        myDataRow("trefExistencia") = vExistencia

    '                        myDataRow("trefCantidad") = Val(txtCantidad.Text)
    '                        myDataRow("trefCCLAVEINT") = txtIdUnidadProd.Text

    '                        myDataRow("trefBComprar") = IIf(vExistencia < Val(txtCantidad.Text), True, False)

    '                        'Calcula Costo
    '                        vCosto = ObjCom.ChecaCosto(vCIDPRODUCTO, Now, vIdAlmacen)
    '                        myDataRow("trefCOSTO") = vCosto

    '                        If vExistencia >= Val(txtCantidad.Text) Then
    '                            CantCompra = 0
    '                            CantSalida = Val(txtCantidad.Text)
    '                        Else
    '                            CantCompra = Val(txtCantidad.Text) - vExistencia
    '                            CantSalida = vExistencia
    '                        End If
    '                        myDataRow("trefCantCompra") = CantCompra
    '                        myDataRow("trefCantSalida") = CantSalida



    '                        MyTablaRefac.Rows.Add(myDataRow)

    '                        'Grabamos Producto
    '                        GrabaProductoLOC(txtIdProducto.Text)
    '                        GrabaUnidadLOC(vCIDUNIDAD)
    '                    End If
    '                    InsertaRegGrid()
    '                End If
    '            End If
    '        Else
    '            txtIdProducto.Enabled = True
    '        End If
    '        'ActualizaTotales()
    '    Catch ex As Exception
    '        MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, Me.Text)
    '    End Try
    'End Sub

    'Private Function InsertaRegGrid() As Boolean
    '    Try

    '        grdProductosOT.Rows.Clear()

    '        For i = 0 To MyTablaRefac.DefaultView.Count - 1
    '            grdProductosOT.Rows.Add()
    '            'Consecutivo
    '            grdProductosOT.Item("grefConsecutivo", i).Value = i + 1
    '            grdProductosOT.Item("grefCCODIGOP01_PRO", i).Value = Trim(MyTablaRefac.DefaultView.Item(i).Item("trefCCODIGOP01_PRO").ToString)
    '            grdProductosOT.Item("grefCNOMBREP01", i).Value = Trim(MyTablaRefac.DefaultView.Item(i).Item("trefCNOMBREP01").ToString)
    '            grdProductosOT.Item("grefExistencia", i).Value = CDbl(MyTablaRefac.DefaultView.Item(i).Item("trefExistencia"))
    '            grdProductosOT.Item("grefCantidad", i).Value = CDbl(MyTablaRefac.DefaultView.Item(i).Item("trefCantidad"))
    '            grdProductosOT.Item("grefCCLAVEINT", i).Value = Trim(MyTablaRefac.DefaultView.Item(i).Item("trefCCLAVEINT").ToString)
    '            grdProductosOT.Item("grefBComprar", i).Value = MyTablaRefac.DefaultView.Item(i).Item("trefBComprar")
    '            grdProductosOT.Item("grefCOSTO", i).Value = CDbl(MyTablaRefac.DefaultView.Item(i).Item("trefCOSTO"))

    '            grdProductosOT.Item("grefCIDPRODUCTO", i).Value = CInt(MyTablaRefac.DefaultView.Item(i).Item("trefCIDPRODUCTO"))
    '            grdProductosOT.Item("grefCIDUNIDADBASE", i).Value = CInt(MyTablaRefac.DefaultView.Item(i).Item("trefCIDUNIDADBASE"))

    '            grdProductosOT.Item("grefCantCompra", i).Value = CDbl(MyTablaRefac.DefaultView.Item(i).Item("trefCantCompra"))
    '            grdProductosOT.Item("grefCantSalida", i).Value = CDbl(MyTablaRefac.DefaultView.Item(i).Item("trefCantSalida"))
    '        Next
    '        'grdVentas.Columns("Precio").DefaultCellStyle.Format = "c"
    '        grdProductosOT.PerformLayout()
    '        'grdVentas.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
    '        Return True
    '    Catch ex As Exception
    '        Return False
    '        MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, Me.Text)
    '    End Try

    'End Function

    'Private Sub GrabaProductoLOC(ByVal vCCODIGOPRODUCTO As String)
    '    'Grabamos Producto LOCAL
    '    ProdCom = New ProductosComClass(vCCODIGOPRODUCTO, "")
    '    If ProdCom.Existe Then
    '        'Existe en Comercial
    '        ProdCom.Guardar(False, vCCODIGOPRODUCTO, txtNomProducto.Text, ProdCom.CTIPOPRODUCTO, ProdCom.CSTATUSPRODUCTO, ProdCom.CIDUNIDADBASE, ProdCom.CCODALTERN)

    '    End If

    'End Sub

    'Private Sub GrabaUnidadLOC(ByVal vCIDUNIDAD As Integer)
    '    'Grabamos Producto LOCAL
    '    'UnidadCom = New UnidadesComClass(vCCODIGOPRODUC)
    '    UnidadCom = New UnidadesComClass(vCIDUNIDAD, "CIDUNIDAD", TipoDato.TdNumerico)
    '    If UnidadCom.Existe Then
    '        'Existe en Comercial
    '        UnidadCom.Guardar(False, txtNomUniProd.Text, UnidadCom.CABREVIATURA, UnidadCom.CDESPLIEGUE, UnidadCom.CCLAVEINT)

    '    End If

    'End Sub

    'Private Sub BorraCamposProd()
    '    txtIdProducto.Text = ""
    '    txtNomProducto.Text = ""
    '    txtIdUnidadProd.Text = ""
    '    txtNomUniProd.Text = ""
    '    txtCantidad.Text = ""

    'End Sub

    'Private Sub txtDuracion_KeyPress(sender As Object, e As KeyPressEventArgs)
    '    If txtDuracion.Text.Length = 0 Then
    '        txtDuracion.Focus()
    '        txtDuracion.SelectAll()
    '    Else
    '        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
    '            Salida = True
    '            tabPrinc.SelectedIndex = 2
    '            txtIdProducto.Focus()
    '            txtIdProducto.SelectAll()
    '            Salida = False
    '        End If
    '    End If
    'End Sub

    Private Sub Cancelar()

        LimpiaCampos()
        ActivaBotones(False, TipoOpcActivaBoton.tOpcInicializa)

        'ActivaCampos(False, TipoOpcActivaCampos.tOpcDESHABTODOS)

        ActivaCampos(False, TipoOpcActivaCampos.tOpcINICIALIZA)



        ToolStripMenu.Enabled = True
        'GroupBox2.Enabled = True
        'tabPrinc.SelectedIndex = 0
        'DsComboMovimiento.Tables("MGW10006").DefaultView.RowFilter = Nothing
        'chkAyu1.Checked = False
        'chkAyu2.Checked = False
        'ActivaAyudantes(chkAyu1.Checked, TipoNumAyudante.Ayudante1)
        'ActivaAyudantes(chkAyu2.Checked, TipoNumAyudante.Ayudante2)
        indice = 0
        indiceOS = 0
        indiceOT = 0



        grdOrdenServCab.DataSource = Nothing
        grdOrdenServCab.Rows.Clear()



        grdOrdenServDet.DataSource = Nothing
        grdOrdenServDet.Rows.Clear()


        MyTablaDetalle.Clear()


        Salida = True
        'txtOrdenTrabajo.Enabled = True
        'txtOrdenTrabajo.Focus()
        'Salida = False


    End Sub

    Private Sub ActivaBotones(ByVal Valor As Boolean, Optional ByVal vOpcion As TipoOpcActivaBoton = TipoOpcActivaBoton.tOpcInicializa)
        'Si es Verdadero Se activan Ok,Cancelar y Buscar y Terminar Se desactiva
        If vOpcion = TipoOpcActivaBoton.tOpcConsulta Then

            btnMnuOk.Enabled = False
            btnMnuLimpiar.Enabled = True
            btnMnuCancelar.Enabled = False
            btnMenuReImprimir.Enabled = True
            btnMnuSalir.Enabled = False
        ElseIf vOpcion = TipoOpcActivaBoton.tOpcInicializa Then
            'btnMnuOk.Enabled = Valor
            'btnMnuLimpiar.Enabled = Valor
            'btnMnuCancelar.Enabled = Valor
            'btnMnuSalir.Enabled = Not Valor

            btnMnuOk.Enabled = Valor
            btnMnuLimpiar.Enabled = Valor
            btnMnuCancelar.Enabled = Valor
            btnMenuReImprimir.Enabled = Valor
            btnMnuSalir.Enabled = Not Valor


        ElseIf vOpcion = TipoOpcActivaBoton.tOpcInsertar Then
            btnMnuOk.Enabled = True
            btnMenuReImprimir.Enabled = True
            btnMnuLimpiar.Enabled = True
            btnMnuCancelar.Enabled = False
            btnMnuSalir.Enabled = False

        ElseIf vOpcion = TipoOpcActivaBoton.tOpcEditar Then
            btnMnuOk.Enabled = Not Valor
            btnMnuLimpiar.Enabled = Valor
            btnMnuCancelar.Enabled = Valor
            btnMenuReImprimir.Enabled = Valor
            btnMnuSalir.Enabled = Not Valor

        End If
    End Sub

    Private Sub ActivaCampos(ByVal valor As Boolean, ByVal vOpcion As TipoOpcActivaCampos)
        txtEstatus.ReadOnly = True
        'txtMotivoCancela.ReadOnly = True
        txtTractor.ReadOnly = True
        txtRemolque1.ReadOnly = True
        txtDolly.ReadOnly = True
        txtRemolque2.ReadOnly = True
        txtChofer.ReadOnly = True

        cmbPuestoResp.Enabled = False
        cmbPuestoAyu1.Enabled = False
        cmbPuestoAyu2.Enabled = False
        cmbAutoriza.Enabled = False
        'txtEmpleadoAutoriza.ReadOnly = True
        'txtRecibe.ReadOnly = True
        If vOpcion = TipoOpcActivaCampos.tOpcDESHABTODOS Then
            dtpFechaCap.Enabled = False
            dtpFechaCapturaHora.Enabled = False
            txtidPadreOrdSer.Enabled = False
            txtOrdenServ.Enabled = False
            btnAgregaOS.Enabled = False
            'cmbEntrega.Enabled = False
            'cmbRecibe.Enabled = False

        ElseIf vOpcion = TipoOpcActivaCampos.tOpcINICIALIZA Then
            txtidPadreOrdSer.Enabled = True
            txtOrdenServ.Enabled = True
            dtpFechaCap.Enabled = False
            dtpFechaCapturaHora.Enabled = False

            cmbResponsable.Enabled = False
            cmbayudante1.Enabled = False
            cmbayudante2.Enabled = False

            btnAgregaOS.Enabled = False
            'cmbEntrega.Enabled = False
            'cmbRecibe.Enabled = False
        ElseIf vOpcion = TipoOpcActivaCampos.tOpcOTRO Then
            txtidPadreOrdSer.Enabled = False
            txtOrdenServ.Enabled = False
            dtpFechaCap.Enabled = False
            dtpFechaCapturaHora.Enabled = False

            btnAgregaOS.Enabled = True
            'cmbEntrega.Enabled = True
            'cmbRecibe.Enabled = True

        End If
    End Sub
    'Private Sub ActivaCamposProductos(ByVal valor As Boolean)
    '    txtIdProducto.Enabled = valor
    '    txtNomProducto.Enabled = valor
    '    txtIdUnidadProd.Enabled = valor
    '    txtNomUniProd.Enabled = valor
    '    txtCantidad.Enabled = valor
    'End Sub



    Private Sub btnMnuOk_Click(sender As Object, e As EventArgs) Handles btnMnuOk.Click
        Dim objsql As New ToolSQLs
        Dim ContOS As Integer
        Dim NumsOs As String = ""
        Dim Mensaje As String = ""

        Dim bCabPreOC As Boolean = False
        Dim bCabPreSal As Boolean = False

        Dim vidOrdenActividad As Integer

        Dim CostoMaOb As Decimal = 0
        Dim CostoProdTot As Decimal = 0

        '1.- aCTUALIZAR cab ORDEN DE SERVICIO
        '2.- ACTUALIZAR deT ORDEN DE SERVICIO
        '3.- VERIFICAR SI TODAS LAS ORDENDES DE SERVICIO YA ESTAN RECEPCIONADAS EN CASO DE TALLER
        Try
            Salida = True
            indice = 0
            If Not Validar() Then Exit Sub
            'Recorrer el Grid para 
            ClaveOS = grdOrdenServCab.Item("oscConsecutivo", grdOrdenServCab.CurrentCell.RowIndex).Value
            NumsOs = ClaveOS
            ContOS = ContOS + 1

            If _TipoOrdenServicio = TipoOrdenServicio.LLANTAS Then
                StrSql = objsql.DiagTerCabOrdenServicio(Now, ClaveOS)

            ElseIf _TipoOrdenServicio = TipoOrdenServicio.TALLER Then
                StrSql = objsql.DiagCabOrdenServicio(Now, ClaveOS)

            End If
            ReDim Preserve ArraySql(indice)
            ArraySql(indice) = StrSql
            indice += 1

            Dim DetOs As detOrdenServicioClass


            '*************************I N S U M O S ********************************* 
            'DetOrdenActividadProductos
            Dim vIdOrdenTrab As Integer = 0
            If MyTablaRefac.Rows.Count > 0 Then
                MyTablaRefac.DefaultView.Sort = "trefidOrdenTrabajo"
                For i = 0 To MyTablaRefac.Rows.Count - 1
                    If vIdOrdenTrab <> MyTablaRefac.DefaultView.Item(i).Item("trefidOrdenTrabajo") Then
                        vIdOrdenTrab = MyTablaRefac.DefaultView.Item(i).Item("trefidOrdenTrabajo")
                        bCabPreOC = False
                        bCabPreSal = False
                    Else

                    End If


                    DetOs = New detOrdenServicioClass(vIdOrdenTrab)
                    If DetOs.Existe Then
                        vidOrdenActividad = DetOs.idOrdenActividad
                    Else
                        'MENSAJE
                    End If

                    CostoProdTot = CostoProdTot + (MyTablaRefac.DefaultView.Item(i).Item("trefCantidad") * MyTablaRefac.DefaultView.Item(i).Item("trefCOSTO"))

                    StrSql = objsql.InsertaDetOrdenActividadProductos(txtOrdenServ.Text,
                        vidOrdenActividad,
                        MyTablaRefac.DefaultView.Item(i).Item("trefCIDPRODUCTO"),
                        MyTablaRefac.DefaultView.Item(i).Item("trefCIDUNIDADBASE"),
                        MyTablaRefac.DefaultView.Item(i).Item("trefCantidad"),
                        MyTablaRefac.DefaultView.Item(i).Item("trefCOSTO"),
                        MyTablaRefac.DefaultView.Item(i).Item("trefExistencia"),
                        MyTablaRefac.DefaultView.Item(i).Item("trefCantCompra"),
                        MyTablaRefac.DefaultView.Item(i).Item("trefCantSalida"))
                    ReDim Preserve ArraySql(indice)
                    ArraySql(indice) = StrSql
                    indice += 1

                    'PreOrden de Compra
                    If MyTablaRefac.DefaultView.Item(i).Item("trefCantCompra") > 0 Then
                        If Not bCabPreOC Then
                            'Consultar Consecutivos
                            'NoIdPreOC = UltimoConsecutivo("IdPreOC")

                            StrSql = objsql.InsertaCabPreOC(True, 0, vIdOrdenTrab, Now, _Usuario, vIdAlmacen, "PRE")
                            ReDim Preserve ArraySql(indice)
                            ArraySql(indice) = StrSql
                            indice += 1

                            StrSql = objsql.ActualizaCampoTabla("DetOrdenServicio", "FaltInsumo", TipoDato.TdNumerico, "1",
                            "idOrdenTrabajo", TipoDato.TdNumerico, vIdOrdenTrab)
                            ReDim Preserve ArraySql(indice)
                            ArraySql(indice) = StrSql
                            indice += 1


                            '                StrSql = ObjSql.ActualizaCampoTabla("Parametros", "idOrdenSer", TipoDato.TdNumerico, vidOrdServ,
                            '                "IdParametro", TipoDato.TdNumerico, "1")
                            '                ReDim Preserve ArraySql(indice)
                            '                ArraySql(indice) = StrSql
                            '                indice += 1


                            'StrSql = "DECLARE @IdPreOC INT = (SELECT TOP 1 IdPreOC FROM dbo.CabPreOC ORDER BY IdPreOC DESC)"
                            'ReDim Preserve ArraySql(indice)
                            'ArraySql(indice) = StrSql
                            'indice += 1


                            bCabPreOC = True
                        End If

                        StrSql = objsql.InsertaDetbPreOC(NoIdPreOC,
                            MyTablaRefac.DefaultView.Item(i).Item("trefCIDPRODUCTO"),
                            MyTablaRefac.DefaultView.Item(i).Item("trefCIDUNIDADBASE"),
                            MyTablaRefac.DefaultView.Item(i).Item("trefCantidad"))
                        ReDim Preserve ArraySql(indice)
                        ArraySql(indice) = StrSql
                        indice += 1


                    End If

                    'Pre Salida de Almacen
                    If MyTablaRefac.DefaultView.Item(i).Item("trefCantSalida") > 0 Then
                        If Not bCabPreSal Then
                            'NoIdPreSal = UltimoConsecutivo("IdPreSalida")

                            'NoIdPreSal = 100
                            'StrSql = objsql.InsertaCabPreSalida(False, NoIdPreSal, vidOrdenActividad, Now, UserId, OrdenTrabajo.idPersonalResp, vIdAlmacen, "PEN", 123123)
                            StrSql = objsql.InsertaCabPreSalida(True, NoIdPreSal, vIdOrdenTrab,
                            Now, _Usuario, cmbResponsable.SelectedValue, vIdAlmacen, "PEN", 123123)
                            ReDim Preserve ArraySql(indice)
                            ArraySql(indice) = StrSql
                            indice += 1

                            'StrSql = "DECLARE @IdPreSalida INT = (SELECT TOP 1 IdPreSalida FROM dbo.CabPreSalida ORDER BY IdPreSalida DESC)"
                            'ReDim Preserve ArraySql(indice)
                            'ArraySql(indice) = StrSql
                            'indice += 1

                            bCabPreSal = True
                        End If

                        StrSql = objsql.InsertaDetPreSalida(NoIdPreSal,
                            MyTablaRefac.DefaultView.Item(i).Item("trefCIDPRODUCTO"),
                            MyTablaRefac.DefaultView.Item(i).Item("trefCIDUNIDADBASE"),
                            MyTablaRefac.DefaultView.Item(i).Item("trefCantidad"),
                             MyTablaRefac.DefaultView.Item(i).Item("trefCOSTO"),
                             MyTablaRefac.DefaultView.Item(i).Item("trefExistencia"))

                        ReDim Preserve ArraySql(indice)
                        ArraySql(indice) = StrSql
                        indice += 1
                    End If

                Next
            End If
            '*************************I N S U M O S ********************************* 
            '*************************DETORDENSERVICIO ********************************* 
            For i = 0 To grdOrdenServDet.Rows.Count - 1
                Clave = grdOrdenServDet.Item("osdidOrdenTrabajo", i).Value
                If _TipoOrdenServicio = TipoOrdenServicio.LLANTAS Then
                    StrSql = objsql.DiagTerDetOrdenServicio(Now, ClaveOS, Clave)

                ElseIf _TipoOrdenServicio = TipoOrdenServicio.TALLER Then
                    StrSql = objsql.DiagDetOrdenServicio(Now, ClaveOS, Clave,
                grdOrdenServDet.Item("osdDuracionActHr", i).Value,
                grdOrdenServDet.Item("osdNotaDiagnostico", i).Value,
                CostoMaOb, CostoProdTot,
                grdOrdenServDet.Item("osdCIDPRODUCTO", i).Value,
                grdOrdenServDet.Item("osdCantidad_CIDPRODUCTO_SERV", i).Value,
                grdOrdenServDet.Item("osdPrecio_CIDPRODUCTO_SERV", i).Value)

                End If
                ReDim Preserve ArraySql(indice)
                ArraySql(indice) = StrSql
                indice += 1
                'End If

                'If NumsOs = "" Then
                '    NumsOs = ClaveOS
                'Else
                '    NumsOs = NumsOs & "," & ClaveOS
                'End If
                'ContOS = ContOS + 1
            Next
            '*************************DETORDENSERVICIO ********************************* 

            'If chkMarcaTodasOT.Checked Then
            '    ClaveOS = grdOrdenServCab.Item("oscConsecutivo", grdOrdenServCab.CurrentCell.RowIndex).Value
            '    StrSql = objsql.AsignaCabOrdenServicio(Now, ClaveOS)
            '    ReDim Preserve ArraySql(indice)
            '    ArraySql(indice) = StrSql
            '    indice += 1
            '    For j = 0 To grdOrdenServDet.Rows.Count - 1
            '        Clave = grdOrdenServDet.Item("osdidOrdenTrabajo", j).Value

            '        StrSql = objsql.AsignaDetOrdenServicio(Now, ClaveOS, Clave)
            '        ReDim Preserve ArraySql(indice)
            '        ArraySql(indice) = StrSql
            '        indice += 1

            '        StrSql = objsql.AsignaDetRecepcionOS(grdOrdenServDet.Item("osdidPersonalResp", j).Value,
            '        grdOrdenServDet.Item("osdidPersonalAyu1", j).Value, grdOrdenServDet.Item("osdidPersonalAyu2", j).Value,
            '        ClaveOS, Clave)
            '        ReDim Preserve ArraySql(indice)
            '        ArraySql(indice) = StrSql
            '        indice += 1


            '        'If Clave = grdOrdenServDet.Item("osdidOrdenTrabajo", j).Value Then
            '        'End If
            '    Next
            '    If NumsOs = "" Then
            '        NumsOs = ClaveOS
            '    Else
            '        NumsOs = NumsOs & "," & ClaveOS
            '    End If

            '    ContOS = ContOS + 1
            'ElseIf chkMarcaTodas.Checked Then
            '    For j = 1 To grdOrdenServCab.Rows.Count - 1
            '        If grdOrdenServCab.Item("oscEstatus", j).Value = "REC" Then
            '            ClaveOS = grdOrdenServCab.Item("oscConsecutivo", j).Value
            '            For i = 0 To MyTablaDetalle.Rows.Count - 1
            '                If ClaveOS = MyTablaDetalle.Rows.Item(i).Item("tosdIdOrdenSer") Then
            '                    Clave = MyTablaDetalle.Rows.Item(i).Item("tosdidOrdenTrabajo")

            '                    StrSql = objsql.AsignaDetOrdenServicio(Now, ClaveOS, Clave)
            '                    ReDim Preserve ArraySql(indice)
            '                    ArraySql(indice) = StrSql
            '                    indice += 1

            '                    StrSql = objsql.AsignaDetRecepcionOS(cmbResponsable.SelectedValue,
            '                    IIf(chkAyu1.Checked, cmbayudante1.SelectedValue, 0),
            '                    IIf(chkAyu2.Checked, cmbayudante2.SelectedValue, 0), ClaveOS, Clave)
            '                    ReDim Preserve ArraySql(indice)
            '                    ArraySql(indice) = StrSql
            '                    indice += 1

            '                End If

            '            Next
            '            StrSql = objsql.AsignaCabOrdenServicio(Now, ClaveOS)
            '            ReDim Preserve ArraySql(indice)
            '            ArraySql(indice) = StrSql
            '            indice += 1

            '            If NumsOs = "" Then
            '                NumsOs = ClaveOS
            '            Else
            '                NumsOs = NumsOs & "," & ClaveOS
            '            End If

            '            ContOS = ContOS + 1
            '        End If
            '    Next




            'End If

            If ContOS > 1 Then
                StrSql = objsql.CambiaEstatusPadre(Val(txtidPadreOrdSer.Text), "PRO")
                ReDim Preserve ArraySql(indice)
                ArraySql(indice) = StrSql
                indice += 1
                Mensaje = "!! Esta seguro de Terminar las Ordenes de Servicio : (" & NumsOs & ") ? !!"
            ElseIf ContOS = 1 Then
                StrSql = objsql.CambiaEstatusPadre(Val(txtidPadreOrdSer.Text), "PRO")
                ReDim Preserve ArraySql(indice)
                ArraySql(indice) = StrSql
                indice += 1
                Mensaje = "!! Esta seguro que desea Terminar la Orden de Servicio: " & NumsOs & " ? !!"
            ElseIf ContOS = 0 Then
                Exit Sub
            End If

            If MsgBox(Mensaje, MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                If indice > 0 Then
                    Dim obj As New CapaNegocio.Tablas
                    Dim Respuesta As String
                    'Para Impresion
                    'Dim sOrdenes As String = ""
                    'Dim idOS As Integer = 0
                    'Dim TipidOS As Boolean = False

                    Respuesta = obj.EjecutarSql(Inicio.CONSTR, "SQL", ArraySql, Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, indice)
                    FEspera.Close()
                    If Respuesta = "EFECTUADO" Then

                        If ContOS > 1 Then
                            MsgBox("Se Finalizo Correctamente las Ordenes de Servicio : (" & NumsOs & ")", MsgBoxStyle.Exclamation, Me.Text)
                        ElseIf ContOS = 1 Then
                            MsgBox("Se Finalizo Correctamente la Orden de Servicio : " & NumsOs, MsgBoxStyle.Exclamation, Me.Text)
                        End If


                        'Se debe Imprimir Algo ????
                        'If SetupThePrinting() Then
                        '    For imp = 0 To ArrayOrdenSer.Length - 1
                        '        idOS = ArrayOrdenSer(imp)
                        '        TipidOS = ArrayOrdenSerTipo(imp)
                        '        ImprimeOrdenServicio(idOS, TipidOS, True, True)
                        '    Next
                        'End If
                        StrSql = objsql.DeterminaPreSalidas(Val(txtOrdenServ.Text))
                        If SetupThePrinting() Then
                            DtImprime = BD.ExecuteReturn(StrSql)
                            If DtImprime.Rows.Count > 0 Then
                                For i = 0 To DtImprime.Rows.Count - 1
                                    ImprimeOrdenServicio(opcTipoTicket.PreSalida, txtOrdenServ.Text,
                                    False, False, True,, DtImprime.Rows.Item(i).Item("IdPreSalida"))
                                Next
                            End If
                        End If


                    ElseIf Respuesta = "NOEFECTUADO" Then

                    ElseIf Respuesta = "ERROR" Then
                        indice = 0
                        SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")
                    End If
                Else
                    MsgBox("No se Encontraron Registros que Grabar", MsgBoxStyle.Exclamation, Me.Text)
                End If
                Cancelar()
            End If
        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try







        'Dim bCabPreOC As Boolean = False
        'Dim bCabPreSal As Boolean = False

        'Dim vIdCLAVE As Integer
        'Dim vidOrdTrab As Integer
        'Dim vNewidOrdTrab As Integer
        'Dim vidOrdServ As Integer
        'Dim vUnidadTrans As String
        'Dim vidTipOrdServ As Integer
        'Dim vidTipServ As Integer
        'Dim vbOrdenTrabajo As Boolean

        'Dim vidServicio As Integer
        'Dim vidActividad As Integer
        'Dim vDurHora As Integer
        'Dim vidDivision As Integer
        'Dim vNotaRecepcion As String
        'Dim vidOrdenActividad As Integer



        'Try
        '    Salida = True
        '    indice = 0
        '    indiceOS = 0
        '    indiceOT = 0
        '    vidOrdTrab = 0
        '    If Not Validar() Then Exit Sub



        '    If MsgBox(Mensaje, MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then

        '        If Val(txtidPadreOrdSer.Text) > 0 Then
        '            AgregaTODOSDET()

        '            'Actualiza Fecha de Ordenes de Servicio
        '            For c = 0 To grdOrdenServCab.Rows.Count - 1
        '                vIdCLAVE = grdOrdenServCab.Item("oscCLAVE", c).Value
        '                vbOrdenTrabajo = grdOrdenServCab.Item("oscbOrdenTrabajo", c).Value

        '                ReDim Preserve ArrayOrdenSer(indiceOS)
        '                ArrayOrdenSer(indiceOS) = vIdCLAVE

        '                ReDim Preserve ArrayOrdenSerTipo(indiceOS)
        '                ArrayOrdenSerTipo(indiceOS) = vbOrdenTrabajo
        '                indiceOS += 1

        '                StrSql = ObjSql.ActualizaCampoTabla("CabOrdenServicio", "idOrdenSer", TipoDato.TdNumerico, vIdCLAVE,
        '                "fechaCaptura", TipoDato.TdFechaHora, FormatFecHora(Now, True))
        '                ReDim Preserve ArraySql(indice)
        '                ArraySql(indice) = StrSql
        '                indice += 1

        '                'Detalle de Orden Servicio
        '                'Borrar todas
        '                For d = 0 To grdOrdenServDet.Rows.Count - 1
        '                    If vIdCLAVE = grdOrdenServDet.Item("osdCLAVE", d).Value Then
        '                        If grdOrdenServDet.Item("osdModificado", d).Value Then

        '                            vidServicio = grdOrdenServDet.Item("osdidServicio", d).Value
        '                            vidActividad = grdOrdenServDet.Item("osdidActividad", d).Value
        '                            vDurHora = grdOrdenServDet.Item("osdDuracionActHr", d).Value
        '                            vidDivision = grdOrdenServDet.Item("osdidDivision", d).Value
        '                            vNotaRecepcion = grdOrdenServDet.Item("osdNomServicio", d).Value
        '                            vidOrdenActividad = grdOrdenServDet.Item("osdidOrdenActividad", d).Value
        '                            vidOrdTrab = grdOrdenServDet.Item("osdidOrdenTrabajo", d).Value

        '                            idLlanta = grdOrdenServDet.Item("osdidLlanta", d).Value
        '                            PosLLanta = grdOrdenServDet.Item("osdPosicion", d).Value

        '                            StrSql = "DELETE dbo.DetOrdenServicio WHERE idOrdenActividad = " & vidOrdenActividad
        '                            ReDim Preserve ArraySql(indice)
        '                            ArraySql(indice) = StrSql
        '                            indice += 1

        '                            If vidOrdTrab > 0 Then
        '                                'Cancelamos actual
        '                                StrSql = ObjSql.ActualizaCampoTabla("DetRecepcionOS", "idOrdenTrabajo", TipoDato.TdNumerico, vidOrdTrab,
        '                                "isCancelado", TipoDato.TdBoolean, 1)
        '                                ReDim Preserve ArraySql(indice)
        '                                ArraySql(indice) = StrSql
        '                                indice += 1

        '                                StrSql = ObjSql.ActualizaCampoTabla("DetRecepcionOS", "idOrdenTrabajo", TipoDato.TdNumerico, vidOrdTrab,
        '                                "motivoCancelacion", TipoDato.TdCadena, "DESDE SISTEMA")
        '                                ReDim Preserve ArraySql(indice)
        '                                ArraySql(indice) = StrSql
        '                                indice += 1
        '                                'Generamos nuevo
        '                                If d = 0 Or vNewidOrdTrab = 0 Then
        '                                    vNewidOrdTrab = UltimoOrdenTrab()
        '                                Else
        '                                    vNewidOrdTrab = vidOrdTrab + 1
        '                                End If

        '                                ReDim Preserve ArrayOrdenTrab(indiceOT)
        '                                ArrayOrdenTrab(indiceOT) = vNewidOrdTrab
        '                                indiceOT += 1

        '                                StrSql = ObjSql.InsertaOrdenTrabajo(vNewidOrdTrab, vidOrdServ, 0, vNotaRecepcion, 0, vidDivision, 0, 0, 0)
        '                                ReDim Preserve ArraySql(indice)
        '                                ArraySql(indice) = StrSql
        '                                indice += 1
        '                            End If

        '                            StrSql = ObjSql.InsertaOrdenServicioDET(vIdCLAVE, vidServicio,
        '                            vidActividad, vDurHora, 0, 0, "PRE", 0, 0, vNewidOrdTrab, vidDivision, idLlanta, PosLLanta)
        '                            ReDim Preserve ArraySql(indice)
        '                            ArraySql(indice) = StrSql
        '                            indice += 1
        '                            'aqui()
        '                        End If
        '                    End If


        '                Next

        '            Next
        '            StrSql = objsql.ActualizaCampoTabla("Parametros", "idOrdenTrabajo", TipoDato.TdNumerico, vidOrdTrab,
        '            "IdParametro", TipoDato.TdNumerico, "1")
        '            ReDim Preserve ArraySql(indice)
        '            ArraySql(indice) = StrSql
        '            indice += 1

        '        Else
        '            '1.- Grabar MasterOrdServ y Obtener Consecutivo para poder continuar
        '            '2- Recorrer Grid de Cabeceros para insertar Ordenes de servicio
        '            AgregaTODOSDET()
        '            vidPadreOrdSer = UltimoMaster()
        '            If vidPadreOrdSer > 0 Then
        '                'Insertar Padre
        '                StrSql = ObjSql.InsertaPadre(vidPadreOrdSer, txtTractor.Text, txtRemolque1.Text, txtDolly.Text,
        '                txtRemolque2.Text, 0, FormatFecHora(dtpFechaCap.Value, True), _Usuario, "CAP")
        '                ReDim Preserve ArraySql(indice)
        '                ArraySql(indice) = StrSql
        '                indice += 1

        '                'Ordenes de Servicio
        '                For c = 0 To grdOrdenServCab.Rows.Count - 1
        '                    vIdCLAVE = grdOrdenServCab.Item("oscCLAVE", c).Value
        '                    vUnidadTrans = grdOrdenServCab.Item("oscidUnidadTrans", c).Value
        '                    vidTipOrdServ = grdOrdenServCab.Item("oscTipoOrdenServ", c).Value
        '                    vidTipServ = grdOrdenServCab.Item("oscTipoServ", c).Value
        '                    vbOrdenTrabajo = grdOrdenServCab.Item("oscbOrdenTrabajo", c).Value

        '                    If c = 0 Then
        '                        vidOrdServ = UltimoOrdServ()
        '                    Else
        '                        vidOrdServ = vidOrdServ + 1
        '                    End If

        '                    If vidOrdServ > 0 Then
        '                        ReDim Preserve ArrayOrdenSer(indiceOS)
        '                        ArrayOrdenSer(indiceOS) = vidOrdServ

        '                        ReDim Preserve ArrayOrdenSerTipo(indiceOS)
        '                        ArrayOrdenSerTipo(indiceOS) = vbOrdenTrabajo
        '                        indiceOS += 1

        '                        '10/ENE/2017
        '                        StrSql = ObjSql.InsertaOrdenServicio(vidOrdServ, _cveEmpresa, vUnidadTrans, vidTipOrdServ,
        '                        0, "PRE", 1,
        '                        FormatFecHora(dtpFechaCap.Value, True), vidTipOrdServ, vidTipServ, vidPadreOrdSer)


        '                        ReDim Preserve ArraySql(indice)
        '                        ArraySql(indice) = StrSql
        '                        indice += 1

        '                        For d = 0 To grdOrdenServDet.Rows.Count - 1
        '                            vidServicio = grdOrdenServDet.Item("osdidServicio", d).Value
        '                            vidActividad = grdOrdenServDet.Item("osdidActividad", d).Value
        '                            vDurHora = grdOrdenServDet.Item("osdDuracionActHr", d).Value
        '                            vidDivision = grdOrdenServDet.Item("osdidDivision", d).Value
        '                            vNotaRecepcion = grdOrdenServDet.Item("osdNombreAct", d).Value

        '                            idLlanta = grdOrdenServDet.Item("osdidLlanta", d).Value
        '                            PosLLanta = grdOrdenServDet.Item("osdPosicion", d).Value

        '                            If vIdCLAVE = grdOrdenServDet.Item("osdCLAVE", d).Value Then

        '                                'Trabajos
        '                                If vbOrdenTrabajo Then
        '                                    If d = 0 Or vidOrdTrab = 0 Then
        '                                        vidOrdTrab = UltimoOrdenTrab()
        '                                    Else
        '                                        vidOrdTrab = vidOrdTrab + 1
        '                                    End If

        '                                    ReDim Preserve ArrayOrdenTrab(indiceOT)
        '                                    ArrayOrdenTrab(indiceOT) = vidOrdTrab
        '                                    indiceOT += 1

        '                                    StrSql = ObjSql.InsertaOrdenTrabajo(vidOrdTrab, vidOrdServ, 0, vNotaRecepcion, 0, vidDivision, 0, 0, 0)
        '                                    ReDim Preserve ArraySql(indice)
        '                                    ArraySql(indice) = StrSql
        '                                    indice += 1
        '                                Else
        '                                    vidOrdTrab = 0
        '                                End If
        '                                'Detalle - checar los ceros
        '                                StrSql = ObjSql.InsertaOrdenServicioDET(vidOrdServ, vidServicio,
        '                                vidActividad, vDurHora, 0, 0, "PRE", 0, 0, vidOrdTrab, vidDivision, idLlanta, PosLLanta)
        '                                ReDim Preserve ArraySql(indice)
        '                                ArraySql(indice) = StrSql
        '                                indice += 1

        '                            End If
        '                        Next

        '                    End If
        '                Next

        '                'Se actualiza Parametros
        '                StrSql = ObjSql.ActualizaCampoTabla("Parametros", "idOrdenSer", TipoDato.TdNumerico, vidOrdServ,
        '                "IdParametro", TipoDato.TdNumerico, "1")
        '                ReDim Preserve ArraySql(indice)
        '                ArraySql(indice) = StrSql
        '                indice += 1

        '                StrSql = ObjSql.ActualizaCampoTabla("Parametros", "idOrdenTrabajo", TipoDato.TdNumerico, vidOrdTrab,
        '                "IdParametro", TipoDato.TdNumerico, "1")
        '                ReDim Preserve ArraySql(indice)
        '                ArraySql(indice) = StrSql
        '                indice += 1

        '            End If
        '        End If
        '    End If
        '    Salida = False
        'Catch ex As Exception
        '    MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        'End Try
    End Sub

#Region "IMPRESION"


    Public Sub ImprimeOrdenServicio(ByVal Opcion As opcTipoTicket,
    ByVal idOrdenSer As Integer, ByVal bOrdenTrab As Boolean,
    ByVal BandPreview As Boolean,
    ByVal vbanHorizontal As Boolean,
    Optional ByVal nombreImpresora As String = "",
    Optional ByVal idOrdenTrabajo As Integer = 0)
        Dim objSql As New ToolSQLs

        ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 10)
        ImpRep.FuenteImprimeCampos2 = New Font("Free 3 of 9 Extended", 40)
        ImpRep.FuenteImprimeCampos3 = New System.Drawing.Font("Courier New", 10, FontStyle.Bold)
        ImpRep.FuenteImprimeCampos4 = New System.Drawing.Font("Courier New", 7, FontStyle.Bold)
        ImpRep.FuenteImprimeCampos5 = New System.Drawing.Font("Courier New", 10, FontStyle.Underline)

        'ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 10)
        'ImpRep.FuenteImprimeCampos3 = New System.Drawing.Font("Courier New", 10, FontStyle.Bold)
        'ImpRep.FuenteImprimeCampos4 = New System.Drawing.Font("Courier New", 7, FontStyle.Bold)

        'PATH_FONTS = "C:\Windows\Fonts\"
        'Dim pfc As PrivateFontCollection = New PrivateFontCollection()
        ''FRE3OF9X_0
        ''pfc.AddFontFile(PATH_FONTS & "BARCOD39.TTF")
        'pfc.AddFontFile(PATH_FONTS & "FREE3OF9_2.TTF")
        'Dim FontFam As FontFamily = pfc.Families(0)
        'ImpRep.FuenteImprimeCampos2 = New Font(FontFam, 40)

        'ImpRep.FuenteImprimeCampos2 = New Font("Free 3 of 9 Extended", 40)

        'BARCOD39
        ImpRep.Constr = Inicio.CONSTR
        ImpRep.TipoConexion = TipoConexion.TcSQL

        Select Case Opcion
            Case opcTipoTicket.OrdenServ
                ImpRep.NomTabla = "ORDSERV"

                If bOrdenTrab Then
                    ImpRep.StrSql = objSql.ImpOrdServConOT(idOrdenSer)
                Else
                    ImpRep.StrSql = objSql.ImpOrdServSinOT(idOrdenSer)
                End If

            Case opcTipoTicket.PreOrdenCompra

            Case opcTipoTicket.PreSalida
                ImpRep.NomTabla = "PRESAL"
                'ImpRep.StrSql = objSql.ImpPreSalida(idOrdenTrabajo)
                ImpRep.StrSql = objSql.ImpPreSalida(idOrdenTrabajo)
            Case Else

        End Select

        NumRegistros = ImpRep.LlenaDataSet
        If NumRegistros > 0 Then

            Select Case Opcion
                Case opcTipoTicket.OrdenServ
                    ArmaTablaImpresionTicket(bOrdenTrab)
                Case opcTipoTicket.PreOrdenCompra
                    ArmaTablaPreOCTicket(bOrdenTrab)
                Case opcTipoTicket.PreSalida
                    ArmaTablaPreSalidaTicket(bOrdenTrab)
            End Select

            ImpRep.TablaImprime = MyTablaImprime

            'If prtSettings Is Nothing Then
            '    prtSettings = New PrinterSettings
            'End If

            If prtDoc Is Nothing Then
                prtDoc = New PrintDocument
                AddHandler prtDoc.PrintPage, AddressOf prt_PrintPage
            End If

            'If ExisteColaImp = "" Then
            'ojoooo ExisteColaImp = LlenaDatos("1", "ColaImpresion", "IdParametro", TipoDato.TdNumerico, , )

            'End If

            If ExisteColaImp = KEY_RCORRECTO Then
                If StrColaImpRecibo <> "" Then
                    prtSettings.PrinterName = StrColaImpRecibo
                    MsgBox(StrColaImpRecibo, MsgBoxStyle.Information, Me.Text)
                End If
            End If

            'If StrColaImpresion = "" Then
            '    'MsgBox("No Hay Cola de Impresi�n, la Hoja de Surtido No se mando a Imprimir", MsgBoxStyle.Information, "Impresion de surtido")
            '    Exit Sub
            'Else
            '    ImpGlobal.PrinterSettings.PrinterName = strCola
            'End If

            If prtSettings Is Nothing Then
                prtSettings = New PrinterSettings
            End If


            prtDoc.PrinterSettings = prtSettings
            prtDoc.DefaultPageSettings = prtSettings.DefaultPageSettings

            prtDoc.DefaultPageSettings.Landscape = vbanHorizontal
            prtDoc.DefaultPageSettings.PaperSize = MyPaperSize


            If BandPreview Then
                Dim prtPrev As New PrintPreviewDialog
                prtPrev.Document = prtDoc
                prtPrev.Text = "Previsualizar datos de " & Me.Text
                prtPrev.WindowState = FormWindowState.Maximized
                prtPrev.Document.DefaultPageSettings.Landscape = vbanHorizontal
                'prtPrev.PrintPreviewControl.Zoom = 1
                If vbanHorizontal Then
                    prtPrev.PrintPreviewControl.Zoom = 1
                Else
                    prtPrev.PrintPreviewControl.Zoom = 1.5
                End If

                'prtPrev.MdiParent = Me
                prtPrev.ShowDialog()
            Else
                'prtDoc.Print()
                'Dim printDialog1 = New PrintDialog()
                'printDialog1.PrinterSettings.Copies = 1
                'Dim result = printDialog1.ShowDialog()
                'prtDoc.PrinterSettings.PrinterName = nombreImpresora
                prtDoc.Print()
            End If



        End If


    End Sub

    Private Sub prt_PrintPage(ByVal sender As Object, ByVal e As PrintPageEventArgs)
        ' Este evento se produce cada vez que se va a imprimir una p�gina
        Try
            'lineaActual = 0

            Dim yPos As Single = e.MarginBounds.Top
            'Margen Izquierdo
            Dim leftMargin As Single = e.MarginBounds.Left
            'Fuentes
            'Altura de la Linea
            Dim lineHeight As Single = printFont.GetHeight(e.Graphics)

            'Dim vFuenteImprimeCampos = New System.Drawing.Font("Courier New", 7)
            'Dim vFuenteImprimeCampos2 = New System.Drawing.Font("Courier New", 7)

            Dim bmp As New Bitmap(My.Resources.logo_ciltra_transparente1)
            ImpRep.Imagen1 = New Bitmap(bmp, bmp.Width / 1, bmp.Height / 1)

            'ImpRep.PosicionImagen1 = New Point(e.MarginBounds.Left - 70, yPos - 30)
            ImpRep.PosicionImagen1 = New Point(40, 0)

            Do
                yPos += lineHeight
                'Aqui se imprime el dato

                yPos = ImpRep.ImprimeLineaRecibo(e, lineaActual)

                lineaActual += 1

            Loop Until yPos >= e.MarginBounds.Bottom - 20 _
                OrElse lineaActual >= NumRegistros


            ContPaginas += 1
            'DS.Tables(vNomTabla).DefaultView.Count



            If lineaActual < NumRegistros Then
                e.HasMorePages = True
            Else
                ContPaginas = 0
                e.HasMorePages = False
            End If
        Catch ex As Exception
            MsgBox(ex.Message, Me.Text)
        End Try

    End Sub

    Private Sub ArmaTablaImpresionTicket(ByVal bOrdenTrab As Boolean)
        Dim campo As Integer
        Dim Descripcion As String
        Dim Valor As String
        Dim PosValorX, PosValorY, PosDescX, PosDescY As Single
        Dim NumAletra As New NumALetras
        Dim EnLetras As String = ""
        Dim Total As Double = 0

        Dim Y1 As Single = 200

        'Vertical
        Dim X1 As Single = 20
        Dim X1A As Single = 80
        Dim X2 As Single = 150

        Dim X1B As Single = X1 + 25

        Dim BandImpServ = False
        Dim XSERV = 20
        Dim XACT = 80

        'Dim X1A As Single = 120
        'Dim X1B As Single = 200
        'Dim X3 As Single = 220
        '
        'Dim X4 As Single = 400

        Dim ValPad As Integer = 12

        DsRep = New DataSet
        If MyTablaImprime.Rows.Count > 0 Then
            MyTablaImprime.Rows.Clear()
        End If


        DsRep = ImpRep.LlenaDsImprime(ImpRep.NomTabla)
        If DsRep.Tables(ImpRep.NomTabla).DefaultView.Count > 0 Then

            For i As Integer = 0 To DsRep.Tables(ImpRep.NomTabla).DefaultView.Count - 1

                'LINEA 1
                campo += 1
                Descripcion = ""
                Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("RazonSocial"))
                PosDescX = X1A : PosDescY = Y1 : PosValorX = PosDescX : PosValorY = PosDescY
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

                'LINEA 2
                campo += 1
                PosDescX = X1A : PosValorX = PosDescX
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                Descripcion = ""
                Valor = "ORDEN DE SERVICIO"
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

                'LINEA 3
                campo += 1
                Descripcion = ""
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                PosDescX = X1A : PosValorX = PosDescX
                Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("NomTipOrdServ"))
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)


                'CODIGO DE BARRAS ORDEN SERVICIO
                campo += 1
                Descripcion = ""
                PosDescY = PosDescY + 30 : PosValorY = PosDescY
                PosDescX = X2 : PosValorX = X2
                'Valor = FormarBarCode(8)
                Valor = FormarBarCode(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idOrdenSer"))
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 2)

                'LINEA 4
                campo += 1
                Descripcion = "ORD.SER."
                PosDescY = PosDescY + 45 : PosValorY = PosDescY
                PosDescX = X2 : PosValorX = PosDescX + 80
                'Valor = FormarBarCode(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idOrdenSer"))
                Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idOrdenSer")
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)



                'LINEA 5
                campo += 1
                Descripcion = "FECHA:"
                Valor = CDate(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("fechaCaptura")).ToShortDateString
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                PosDescX = PosDescX : PosValorX = PosDescX + 50
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)


                'LINEA 5.1
                campo += 1
                Descripcion = "HORA:"
                Valor = CDate(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("fechaCaptura")).ToShortTimeString
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                PosDescX = PosDescX : PosValorX = PosDescX + 40
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)


                'LINEA 6
                campo += 1
                PosDescY = PosDescY + 30 : PosValorY = PosDescY
                Descripcion = "TRACTOR: "
                Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idTractor"))
                PosDescX = X1 : PosValorX = PosDescX + 70
                If DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idTractor") = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idUnidadTrans") Then
                    InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                Else
                    InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)
                End If


                campo += 1
                Descripcion = "REMOLQUE 1:"
                Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idTolva1"))
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                PosDescX = X1 : PosValorX = PosDescX + 95
                If DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idTolva1") = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idUnidadTrans") Then
                    InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                Else
                    InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)
                End If



                'LINEA 7
                campo += 1
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                Descripcion = "DOLLY: "
                Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idDolly"))
                PosDescX = X1 : PosValorX = PosDescX + 50
                If DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idDolly") = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idUnidadTrans") Then
                    InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                Else
                    InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)
                End If


                campo += 1
                Descripcion = "REMOLQUE 2:"
                Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idTolva2"))
                PosDescX = X1 : PosValorX = PosDescX + 95
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                If DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idTolva2") = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idUnidadTrans") Then
                    InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                Else
                    InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)
                End If


                'LINEA 8
                'campo += 1
                'PosDescY = PosDescY + 15 : PosValorY = PosDescY
                'Descripcion = "KILOMETRAJE: "
                'Valor = Format(CInt(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("Kilometraje")), "##,##0")
                'PosDescX = X1 : PosValorX = PosDescX + 110
                'InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)


                'LINEA 9
                campo += 1
                PosDescY = PosDescY + 30 : PosValorY = PosDescY
                Descripcion = ""
                'If bOrdenTrab Then
                '    Valor = "REPARACIONES SOLICITADAS"
                'Else
                '    Valor = "DESCRIPCION DEL SERVICIO"
                'End If
                Valor = "DESCRIPCION DEL SERVICIO"
                PosDescX = X1 : PosValorX = PosDescX + 25
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 5)

                PosDescY = PosDescY + 30 : PosValorY = PosDescY

                'LINEA 10
                'DETALLE
                For j As Integer = 0 To DsRep.Tables(ImpRep.NomTabla).DefaultView.Count - 1
                    If bOrdenTrab Then
                        If DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NomServicio") <> "" Then
                            If Not BandImpServ Then
                                campo += 1
                                Descripcion = ""
                                Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NomServicio")
                                PosDescX = XSERV : PosValorX = PosDescX + 25
                                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                                BandImpServ = True

                            End If
                        Else
                            BandImpServ = False
                            '12/JULIO/2017
                            'If DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NombreAct") <> "" Then
                            '    campo += 1
                            '    PosDescY = PosDescY + 15 : PosValorY = PosDescY
                            '    Descripcion = ""
                            '    Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NombreAct")
                            '    PosDescX = X1 : PosValorX = PosDescX + 25
                            '    InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                            'Else
                            '    'salto de linea
                            '    PosDescY = PosDescY + 15 : PosValorY = PosDescY
                            'End If
                        End If

                        campo += 1
                        'PosDescY = PosDescY + 30 : PosValorY = PosDescY
                        Descripcion = ""
                        Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NotaRecepcion")
                        PosDescY = PosDescY + 15 : PosValorY = PosDescY

                        If BandImpServ Then
                            PosDescX = XACT : PosValorX = PosDescX + 25
                            InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)
                        Else
                            PosDescX = XSERV : PosValorX = PosDescX + 25
                            InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                        End If



                    Else
                        campo += 1

                        PosDescY = PosDescY + 15 : PosValorY = PosDescY
                        Descripcion = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("idUnidadTrans")
                        Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NomServicio")
                        PosDescX = X1 - 35 : PosValorX = X2 + 25
                        InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                        'PosDescY = PosDescY + 15

                    End If
                Next




                'LINEA 11
                campo += 1
                Descripcion = ""
                Valor = "___________________"
                PosDescY = PosDescY + 100 : PosValorY = PosDescY
                PosDescX = X1 : PosValorX = PosDescX + 25
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                'PosDescY = PosDescY + 15

                campo += 1
                Descripcion = ""
                Valor = " ( OPERADOR ) "
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                PosDescX = X1 : PosValorX = PosDescX + 35
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)

                campo += 1
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                Descripcion = ""
                Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("NomOperador"))
                PosDescX = X1 : PosValorX = PosDescX + 25
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 4)



                campo += 1
                Descripcion = ""
                Valor = "___________________"
                PosDescY = PosDescY + 100 : PosValorY = PosDescY
                PosDescX = X1 : PosValorX = PosDescX + 25
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)

                campo += 1
                Descripcion = ""
                Valor = " ( AUTORIZ� ) "
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                PosDescX = X1 : PosValorX = PosDescX + 35
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                'LINEA 13

                campo += 1
                Descripcion = ""
                Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("NomAutoriza"))
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                PosDescX = X1 : PosValorX = PosDescX + 25
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 4)
                'PosDescY = PosDescY + 15

                'LINEA 14
                'CODIGO DE BARRAS ORDEN SERVICIO
                campo += 1
                Descripcion = ""
                'PosDescY = PosDescY + 15 : PosValorY = PosDescY
                PosDescY = PosDescY + 100 : PosValorY = PosDescY
                PosDescX = X2 : PosValorX = X2
                Valor = FormarBarCode(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idPadreOrdSer"))
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 2)

                'LINEA 15
                campo += 1
                Descripcion = "FOLIO: "
                PosDescY = PosDescY + 45 : PosValorY = PosDescY
                PosDescX = X2 : PosValorX = X2 + 60
                Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idPadreOrdSer")
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)

                'LINEA 14
                'If idContrato = 0 Then
                '    '4 LINEAS
                '    PosDescY = PosDescY + 60
                'End If

                'campo += 1
                'Descripcion = "RECIBO NO."
                'PosDescX = X1 : PosDescY = PosDescY + 15 : PosValorX = PosDescX + 100 : PosValorY = PosDescY
                'Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("NUMRECIBO")
                'InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY)
            Next
        End If
    End Sub
    Private Sub ArmaTablaImpresion(ByVal bOrdenTrab As Boolean)
        Dim campo As Integer
        Dim Descripcion As String
        Dim Valor As String
        Dim PosValorX, PosValorY, PosDescX, PosDescY As Single
        Dim NumAletra As New NumALetras
        Dim EnLetras As String = ""
        Dim Total As Double = 0

        Dim Y1 As Single = 100

        'Horizontal
        'Dim X1 As Single = 20
        'Dim X1A As Single = 120
        'Dim X1B As Single = 200
        'Dim X2 As Single = 320
        'Dim X3 As Single = 440
        'Dim X3A As Single = 540
        'Dim X4 As Single = 620

        'Vertical
        Dim X1 As Single = 20
        Dim X1A As Single = 120
        Dim X1B As Single = 200
        Dim X2 As Single = 160
        Dim X3 As Single = 220
        Dim X3A As Single = 260
        Dim X4 As Single = 400

        Dim ValPad As Integer = 12

        DsRep = New DataSet
        'MyTablaImprime = New DataTable("IMPRIME")
        If MyTablaImprime.Rows.Count > 0 Then
            MyTablaImprime.Rows.Clear()
        End If


        DsRep = ImpRep.LlenaDsImprime(ImpRep.NomTabla)
        If DsRep.Tables(ImpRep.NomTabla).DefaultView.Count > 0 Then

            For i As Integer = 0 To DsRep.Tables(ImpRep.NomTabla).DefaultView.Count - 1

                'LINEA 1
                campo += 1
                'Descripcion = "FECHA DE PAGO:"
                'Valor = CDate(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("FechaTrans"))
                Descripcion = ""
                Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("RazonSocial"))
                PosDescX = X2 : PosDescY = Y1 : PosValorX = X2 : PosValorY = PosDescY
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

                'CODIGO DE BARRAS ORDEN SERVICIO
                campo += 1
                Descripcion = ""
                'PosDescY = PosDescY + 15 : PosValorY = PosDescY
                PosDescX = X4 : PosValorX = X4
                'Valor = FormarBarCode(8)
                Valor = FormarBarCode(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idOrdenSer"))
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 2)

                'LINEA 2
                campo += 1
                PosDescX = X2
                PosDescY = PosDescY + 15
                'PosValorX = PosDescX + 100
                PosValorX = X2 + 35
                PosValorY = PosDescY
                Descripcion = ""
                'Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idOrdenSer")
                Valor = "ORDEN DE SERVICIO"
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

                'LINEA 3
                campo += 1
                Descripcion = ""
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                PosDescX = X2 : PosValorX = X2 + 40
                Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("NomTipOrdServ"))
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)

                'LINEA 4
                campo += 1
                Descripcion = "ORD.SER."
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                PosDescX = X4 : PosValorX = X4 + 80
                'Valor = FormarBarCode(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idOrdenSer"))
                Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idOrdenSer")
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)



                'LINEA 5
                campo += 1
                Descripcion = "FECHA:"
                Valor = CDate(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("fechaCaptura")).ToShortDateString
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                PosDescX = X4 : PosValorX = PosDescX + 50
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

                'campo += 1
                'PosDescY = PosDescY + 30 : PosValorY = PosDescY
                'Descripcion = "DIA: "
                'Valor = CDate(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("fechaCaptura")).Day
                'PosDescX = X1 : PosValorX = PosDescX + 35
                'InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

                'campo += 1
                'Descripcion = "MES:"
                'Valor = CDate(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("fechaCaptura")).Month
                'PosDescX = X2 : PosValorX = PosDescX + 35
                'InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

                'campo += 1
                'Descripcion = "A�O:"
                'Valor = CDate(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("fechaCaptura")).Year
                'PosDescX = X3 : PosValorX = PosDescX + 35
                'InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

                'LINEA 5.1
                campo += 1
                Descripcion = "HORA:"
                Valor = CDate(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("fechaCaptura")).ToShortTimeString
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                PosDescX = X4 : PosValorX = PosDescX + 40
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)


                'LINEA 6
                campo += 1
                PosDescY = PosDescY + 30 : PosValorY = PosDescY
                Descripcion = "TRACTOR: "
                Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idTractor"))
                PosDescX = X1 : PosValorX = PosDescX + 70
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

                campo += 1
                Descripcion = "REMOLQUE 1:"
                Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idTolva1"))
                PosDescX = X2 : PosValorX = PosDescX + 95
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)


                'LINEA 7
                campo += 1
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                Descripcion = "DOLLY: "
                Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idDolly"))
                PosDescX = X1 : PosValorX = PosDescX + 50
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

                campo += 1
                Descripcion = "REMOLQUE 2:"
                Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idTolva2"))
                PosDescX = X2 : PosValorX = PosDescX + 95
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

                'LINEA 8
                'campo += 1
                'PosDescY = PosDescY + 15 : PosValorY = PosDescY
                'Descripcion = "KILOMETRAJE: "
                'Valor = Format(CInt(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("Kilometraje")), "##,##0")
                'PosDescX = X1 : PosValorX = PosDescX + 110
                'InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)


                'LINEA 9
                campo += 1
                PosDescY = PosDescY + 30 : PosValorY = PosDescY
                Descripcion = ""
                If bOrdenTrab Then
                    Valor = "REPARACIONES SOLICITADAS"
                Else
                    Valor = "DESCRIPCION DEL SERVICIO"
                End If
                PosDescX = X2 : PosValorX = PosDescX + 25
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)
                PosDescY = PosDescY + 15 : PosValorY = PosDescY

                'LINEA 10
                'DETALLE
                For j As Integer = 0 To DsRep.Tables(ImpRep.NomTabla).DefaultView.Count - 1
                    If bOrdenTrab Then
                        If DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NomServicio") <> "" Then
                            campo += 1
                            PosDescY = PosDescY + 15 : PosValorY = PosDescY
                            Descripcion = ""
                            Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NomServicio")
                            PosDescX = X1 : PosValorX = PosDescX + 25
                            InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                        Else
                            '12/JULIO/2017
                            'If DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NombreAct") <> "" Then
                            '    campo += 1
                            '    PosDescY = PosDescY + 15 : PosValorY = PosDescY
                            '    Descripcion = ""
                            '    Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NombreAct")
                            '    PosDescX = X1 : PosValorX = PosDescX + 25
                            '    InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                            'Else
                            '    'salto de linea
                            '    PosDescY = PosDescY + 15 : PosValorY = PosDescY
                            'End If
                        End If

                        campo += 1
                        'PosDescY = PosDescY + 30 : PosValorY = PosDescY
                        Descripcion = ""
                        Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NotaRecepcion")
                        PosDescY = PosDescY + 15 : PosValorY = PosDescY
                        PosDescX = X2 : PosValorX = PosDescX + 25
                        InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                    Else
                        campo += 1
                        PosDescY = PosDescY + 15 : PosValorY = PosDescY
                        Descripcion = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("idUnidadTrans")
                        Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NomServicio")
                        PosDescX = X2 - 35 : PosValorX = X2 + 25
                        InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                        'PosDescY = PosDescY + 15

                    End If
                Next




                'LINEA 11
                campo += 1
                PosDescY = PosDescY + 290 : PosValorY = PosDescY
                Descripcion = ""
                Valor = "___________________"
                PosDescX = X1 : PosValorX = PosDescX + 25
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                'PosDescY = PosDescY + 15

                campo += 1
                Descripcion = ""
                Valor = "___________________"
                PosDescX = X3A : PosValorX = PosDescX + 25
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                'PosDescY = PosDescY + 15

                'LINEA 12
                campo += 1
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                Descripcion = ""
                Valor = " ( OPERADOR ) "
                PosDescX = X1 : PosValorX = PosDescX + 35
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                'PosDescY = PosDescY + 15

                campo += 1
                Descripcion = ""
                Valor = " ( AUTORIZ� ) "
                PosDescX = X3A : PosValorX = PosDescX + 35
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                'PosDescY = PosDescY + 15

                'LINEA 13
                campo += 1
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                Descripcion = ""
                Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("NomOperador"))
                PosDescX = X1 : PosValorX = PosDescX + 25
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 4)
                'PosDescY = PosDescY + 15

                campo += 1
                Descripcion = ""
                Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("NomAutoriza"))
                PosDescX = X3A : PosValorX = PosDescX + 25
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 4)
                'PosDescY = PosDescY + 15

                'LINEA 14
                'CODIGO DE BARRAS ORDEN SERVICIO
                campo += 1
                Descripcion = ""
                'PosDescY = PosDescY + 15 : PosValorY = PosDescY
                PosDescY = PosDescY + 75 : PosValorY = PosDescY
                PosDescX = X4 : PosValorX = X4
                Valor = FormarBarCode(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idPadreOrdSer"))
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 2)

                'LINEA 15
                campo += 1
                Descripcion = "FOLIO: "
                PosDescY = PosDescY + 45 : PosValorY = PosDescY
                PosDescX = X4 : PosValorX = X4 + 60
                Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idPadreOrdSer")
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)

                'LINEA 14
                'If idContrato = 0 Then
                '    '4 LINEAS
                '    PosDescY = PosDescY + 60
                'End If

                'campo += 1
                'Descripcion = "RECIBO NO."
                'PosDescX = X1 : PosDescY = PosDescY + 15 : PosValorX = PosDescX + 100 : PosValorY = PosDescY
                'Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("NUMRECIBO")
                'InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY)
            Next
        End If
    End Sub

    Private Function FormarBarCode(ByVal Codigo As String) As String

        Dim Barcode As String = vbEmpty
        'Barcode = Format("*{0}*", Codigo)
        If Len(Codigo) = 1 Then
            Barcode = "*00" & Codigo & "*"
        ElseIf Len(Codigo) = 2 Then
            Barcode = "*0" & Codigo & "*"
        ElseIf Len(Codigo) >= 3 Then
            Barcode = "*" & Codigo & "*"
        End If

        Return Barcode
    End Function


    Public Sub CreaTablaImprime()
        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Int32")
        myDataColumn.ColumnName = "Campo"
        myDataColumn.ReadOnly = False
        myDataColumn.Unique = False
        MyTablaImprime.Columns.Add(myDataColumn)


        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.String")
        myDataColumn.ColumnName = "Descripcion"
        myDataColumn.ReadOnly = False
        myDataColumn.Unique = False
        MyTablaImprime.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.String")
        myDataColumn.ColumnName = "Valor"
        myDataColumn.ReadOnly = False
        myDataColumn.Unique = False
        MyTablaImprime.Columns.Add(myDataColumn)


        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Single")
        myDataColumn.ColumnName = "PosDescX"
        myDataColumn.ReadOnly = False
        myDataColumn.Unique = False
        MyTablaImprime.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Single")
        myDataColumn.ColumnName = "PosDescY"
        myDataColumn.ReadOnly = False
        myDataColumn.Unique = False
        MyTablaImprime.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Single")
        myDataColumn.ColumnName = "PosValX"
        myDataColumn.ReadOnly = False
        myDataColumn.Unique = False
        MyTablaImprime.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Single")
        myDataColumn.ColumnName = "PosValY"
        myDataColumn.ReadOnly = False
        myDataColumn.Unique = False
        MyTablaImprime.Columns.Add(myDataColumn)


        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Single")
        myDataColumn.ColumnName = "Fuente"
        myDataColumn.ReadOnly = False
        myDataColumn.Unique = False
        MyTablaImprime.Columns.Add(myDataColumn)



    End Sub
    Private Sub InsertaRegistro(ByVal Campo As Integer, ByVal Descripcion As String, ByVal Valor As String, ByVal PosDescX As Single,
                           ByVal PosDescY As Single, ByVal PosValX As Single, ByVal PosValY As Single, ByVal vFuente As String)

        myDataRow = MyTablaImprime.NewRow()
        myDataRow("Campo") = Campo
        myDataRow("Descripcion") = Descripcion
        myDataRow("Valor") = Valor
        myDataRow("PosDescX") = PosDescX
        myDataRow("PosDescY") = PosDescY
        myDataRow("PosValX") = PosValX
        myDataRow("PosValY") = PosValY
        myDataRow("Fuente") = vFuente
        MyTablaImprime.Rows.Add(myDataRow)

    End Sub
#End Region


    'Private Function Validar() As Boolean
    '    Validar = True

    '    '10/ENE/2017
    '    'If Not Inicio.ValidandoCampo(Trim(txtKilometraje.Text), TipoDato.TdNumerico) Then
    '    '    MsgBox("El Kilometraje no puede ser Cero o Vac�o", vbInformation, "Aviso" & Me.Text)
    '    '    If txtKilometraje.Enabled Then
    '    '        txtKilometraje.Focus()
    '    '        txtKilometraje.SelectAll()
    '    '    End If
    '    '    Validar = False
    '    'Else
    '    If txtTractor.Text = "" Then
    '        MsgBox("El Tractor no puede ser vac�o", vbInformation, "Aviso" & Me.Text)
    '        txtTractor.Focus()

    '        Validar = False

    '        'ElseIf chkAyu1.Checked Then
    '        '    If cmbayudante1.SelectedValue = 0 Then
    '        '        MsgBox("El Ayudante no puede ser vacio", vbInformation, "Aviso" & Me.Text)
    '        '        If cmbayudante1.Enabled Then
    '        '            cmbayudante1.Focus()
    '        '        End If
    '        '        Validar = False

    '        '    End If
    '        'ElseIf chkAyu2.Checked And Not chkAyu1.Checked Then
    '        '    MsgBox("No puede Seleccionar al Ayudante 2 Hasta que seleccione el Ayudante 1", vbInformation, "Aviso" & Me.Text)
    '        '    chkAyu2.Checked = False
    '        '    If chkAyu1.Enabled Then
    '        '        chkAyu1.Focus()
    '        '    End If

    '        'ElseIf chkAyu2.Checked Then
    '        '    If cmbayudante2.SelectedValue = 0 Then
    '        '        MsgBox("El Ayudante no puede ser vacio", vbInformation, "Aviso" & Me.Text)
    '        '        If cmbayudante2.Enabled Then
    '        '            cmbayudante2.Focus()
    '        '        End If
    '        '        Validar = False

    '        '    End If
    '    End If
    'End Function



    'Private Function ChecaPersonalOcupadoForma(ByVal vLugarOrigen As Integer) As Integer
    '    Dim LugarOcupado As Integer = 0
    '    'Origen = 1 -> cmbResponsable 
    '    'Origen = 2 -> cmbayudante1 
    '    'Origen = 3 -> cmbayudante2
    '    If vLugarOrigen = 1 Then
    '        If cmbayudante1.SelectedValue = cmbResponsable.SelectedValue Then
    '            LugarOcupado = 2
    '        ElseIf cmbayudante2.SelectedValue = cmbResponsable.SelectedValue Then
    '            LugarOcupado = 3
    '        End If
    '    ElseIf vLugarOrigen = 2 Then
    '        If cmbResponsable.SelectedValue = cmbayudante1.SelectedValue Then
    '            LugarOcupado = 1
    '        ElseIf cmbayudante2.SelectedValue = cmbayudante1.SelectedValue Then
    '            LugarOcupado = 3
    '        End If
    '    ElseIf vLugarOrigen = 3 Then
    '        If cmbResponsable.SelectedValue = cmbayudante2.SelectedValue Then
    '            LugarOcupado = 1
    '        ElseIf cmbayudante1.SelectedValue = cmbayudante2.SelectedValue Then
    '            LugarOcupado = 2
    '        End If

    '    End If
    '    Return LugarOcupado
    'End Function

    'Private Function ChecaPersonalOcupado(ByVal idPersonal As Integer, ByVal NomPersonal As String) As Boolean
    '    Dim Resp As Boolean = False
    '    Personal = New PersonalClass(cmbResponsable.SelectedValue)
    '    If Personal.Existe Then
    '        'aqui()
    '        MyTablaPersOcupado = Personal.TablaMecanicoOCupado("DIAG")
    '        If MyTablaPersOcupado.Rows.Count > 0 Then
    '            Resp = True
    '            With MyTablaPersOcupado.Rows(0)
    '                MsgBox("EL Personal: " & NomPersonal & " tiene Asignado actualmente la Orden de Trabajo: " & .Item("idOrdenTrabajo") & vbCrLf & "de la Orden de Servicio: " & .Item("idordenSer") & "Asigne a Otro Personal", MsgBoxStyle.Information, Me.Text)
    '            End With
    '        End If
    '    End If
    '    Return Resp
    'End Function

    'Private Sub cmbResponsable_SelectedIndexChanged(sender As Object, e As EventArgs)
    '    If Salida Then Exit Sub
    '    If cmbResponsable.SelectedValue > 0 Then
    '        vLugOcupado = ChecaPersonalOcupadoForma(1)
    '        If vLugOcupado > 0 Then
    '            MsgBox("El Personal: " & cmbResponsable.Text & " ya esta Asignado para el" & NombreLugarPuesto(vLugOcupado), MsgBoxStyle.Information, Me.Text)
    '            cmbResponsable.SelectedValue = 0
    '            cmbResponsable.Focus()
    '        Else
    '            If ChecaPersonalOcupado(cmbResponsable.SelectedValue, cmbResponsable.Text) Then
    '                'txtNombreMecanico.Text = cmbResponsable.Text
    '                'txtPuestoMec.Text = cmbPuestoResp.Text
    '            End If

    '        End If
    '    Else

    '    End If



    'End Sub

    'Private Sub chkAyu1_CheckedChanged(sender As Object, e As EventArgs)
    '    ActivaAyudantes(chkAyu1.Checked, TipoNumAyudante.Ayudante1)
    'End Sub

    'Private Sub ActivaAyudantes(ByVal Valor As Boolean, ByVal Ayudante As TipoNumAyudante)
    '    If Ayudante = TipoNumAyudante.Ayudante1 Then
    '        cmbayudante1.Enabled = Valor
    '        cmbPuestoAyu1.Enabled = Valor

    '    ElseIf Ayudante = TipoNumAyudante.Ayudante2 Then
    '        cmbayudante2.Enabled = Valor
    '        cmbPuestoAyu2.Enabled = Valor

    '    End If
    'End Sub

    'Private Sub chkAyu2_CheckedChanged(sender As Object, e As EventArgs)
    '    ActivaAyudantes(chkAyu2.Checked, TipoNumAyudante.Ayudante2)
    'End Sub


    'Private Function ContadordePersonal() As Integer
    '    Dim cont As Integer = 0
    '    If cmbPuestoResp.SelectedValue > 0 Then
    '        cont += 1
    '    End If
    '    If chkAyu1.Checked And cmbayudante1.SelectedValue > 0 Then
    '        cont += 1
    '    End If
    '    If chkAyu2.Checked And cmbayudante2.SelectedValue > 0 Then
    '        cont += 1
    '    End If

    '    Return cont
    'End Function

    'Private Function CalculaCostoManoObra(ByVal vNoHoras As Double) As Double
    '    Dim cmo As Double = 0
    '    If cmbResponsable.SelectedValue > 0 Then
    '        Personal = New PersonalClass(cmbResponsable.SelectedValue)
    '        If Personal.Existe Then
    '            cmo = vNoHoras * Personal.PrecioHora
    '        End If
    '    End If
    '    If chkAyu1.Checked And cmbayudante1.SelectedValue > 0 Then
    '        Personal = New PersonalClass(cmbayudante1.SelectedValue)
    '        If Personal.Existe Then
    '            cmo = cmo + (vNoHoras * Personal.PrecioHora)
    '        End If
    '    End If
    '    If chkAyu2.Checked And cmbayudante2.SelectedValue > 0 Then
    '        Personal = New PersonalClass(cmbayudante2.SelectedValue)
    '        If Personal.Existe Then
    '            cmo = cmo + (vNoHoras * Personal.PrecioHora)
    '        End If
    '    End If

    '    Return cmo
    'End Function

    Private Function CalculaCostoRefacciones() As Double
        Dim cre As Double = 0
        If MyTablaRefac.Rows.Count > 0 Then
            For i = 0 To MyTablaRefac.Rows.Count - 1
                cre = cre + (MyTablaRefac.DefaultView.Item(i).Item("trefCantSalida") * MyTablaRefac.DefaultView.Item(i).Item("trefCOSTO"))
            Next
        End If
        Return cre
    End Function

    Private Function NombreLugarPuesto(ByVal vLugar As Integer) As String
        Dim Resp As String = ""
        If vLugar = 1 Then
            Resp = " Responsable "
        ElseIf vLugar = 2 Then
            Resp = " Ayudante 1 "
        ElseIf vLugar = 3 Then
            Resp = " Ayudante 2 "
        End If
        Return Resp
    End Function


    Private Sub txtTractor_KeyDown(sender As Object, e As KeyEventArgs) Handles txtTractor.KeyDown
        If e.KeyCode = Keys.F3 Then
            CargaForm(txtTractor.Text, "UNITRANS", "TC", txtTractor,
" tip.TipoUso = 'T' AND UNI.ESTATUS = 'ACT' AND UNI.IDEMPRESA = " & _cveEmpresa & " AND UNI.IDUNIDADTRANS LIKE '%" & Val(txtTractor.Text) & "%'", True)

            'txtTractor.Text = DespliegaGrid(UCase("UNITRANSTIPO"), Inicio.CONSTR, "", FiltroSinWhere)
            'txtTractor_Leave(sender, e)
        End If
    End Sub

    'Private Sub cmbayudante1_SelectedIndexChanged(sender As Object, e As EventArgs)
    '    If Salida Then Exit Sub
    '    If cmbayudante1.SelectedValue > 0 Then
    '        vLugOcupado = ChecaPersonalOcupadoForma(2)
    '        If vLugOcupado > 0 Then
    '            MsgBox("El Personal: " & cmbayudante1.Text & " ya esta Asignado para el " & NombreLugarPuesto(vLugOcupado))
    '            cmbayudante1.SelectedValue = 0
    '            cmbayudante1.Focus()
    '        Else
    '            If ChecaPersonalOcupado(cmbayudante1.SelectedValue, cmbayudante1.Text) Then
    '                'txtPuestoMec.Text = cmbayudante1.Text
    '            End If

    '        End If
    '    Else

    '    End If
    'End Sub

    'Private Sub cmbayudante2_SelectedIndexChanged(sender As Object, e As EventArgs)
    '    'If Salida Then Exit Sub
    '    'If cmbayudante2.SelectedValue > 0 Then
    '    '    If ChecaPersonalOcupado(cmbayudante2.SelectedValue, cmbayudante2.Text) Then

    '    '    End If
    '    'Else

    '    'End If
    '    If Salida Then Exit Sub
    '    If cmbayudante2.SelectedValue > 0 Then
    '        vLugOcupado = ChecaPersonalOcupadoForma(3)
    '        If vLugOcupado > 0 Then
    '            MsgBox("El Personal: " & cmbayudante2.Text & " ya esta Asignado para el " & NombreLugarPuesto(vLugOcupado))
    '            cmbayudante2.SelectedValue = 0
    '            cmbayudante2.Focus()
    '        Else
    '            If ChecaPersonalOcupado(cmbayudante2.SelectedValue, cmbayudante2.Text) Then
    '                'txtPuestoMec.Text = cmbayudante2.Text
    '            End If

    '        End If
    '    Else

    '    End If
    'End Sub

    'Private Sub cmbPuestoResp_SelectedIndexChanged(sender As Object, e As EventArgs)
    '    If Salida Then Exit Sub
    '    If cmbResponsable.SelectedValue > 0 Then
    '        vLugOcupado = ChecaPersonalOcupadoForma(1)
    '        If vLugOcupado > 0 Then
    '            MsgBox("El Personal: " & cmbResponsable.Text & " ya esta Asignado para el" & NombreLugarPuesto(vLugOcupado), MsgBoxStyle.Information, Me.Text)
    '            cmbResponsable.SelectedValue = 0
    '            cmbResponsable.Focus()
    '        Else
    '            If ChecaPersonalOcupado(cmbResponsable.SelectedValue, cmbResponsable.Text) Then
    '                'txtNombreMecanico.Text = cmbResponsable.Text
    '                'txtPuestoMec.Text = cmbPuestoResp.Text
    '            End If

    '        End If
    '    Else

    '    End If
    'End Sub

    'Private Sub cmbPuestoAyu1_SelectedIndexChanged(sender As Object, e As EventArgs)
    '    If Salida Then Exit Sub
    '    If cmbayudante1.SelectedValue > 0 Then
    '        vLugOcupado = ChecaPersonalOcupadoForma(2)
    '        If vLugOcupado > 0 Then
    '            MsgBox("El Personal: " & cmbayudante1.Text & " ya esta Asignado para el " & NombreLugarPuesto(vLugOcupado))
    '            cmbayudante1.SelectedValue = 0
    '            cmbayudante1.Focus()
    '        Else
    '            If ChecaPersonalOcupado(cmbayudante1.SelectedValue, cmbayudante1.Text) Then
    '                'txtPuestoMec.Text = cmbayudante1.Text
    '            End If

    '        End If
    '    Else

    '    End If
    'End Sub

    'Private Sub cmbPuestoAyu2_SelectedIndexChanged(sender As Object, e As EventArgs)
    '    If Salida Then Exit Sub
    '    If cmbayudante2.SelectedValue > 0 Then
    '        vLugOcupado = ChecaPersonalOcupadoForma(3)
    '        If vLugOcupado > 0 Then
    '            MsgBox("El Personal: " & cmbayudante2.Text & " ya esta Asignado para el " & NombreLugarPuesto(vLugOcupado))
    '            cmbayudante2.SelectedValue = 0
    '            cmbayudante2.Focus()
    '        Else
    '            If ChecaPersonalOcupado(cmbayudante2.SelectedValue, cmbayudante2.Text) Then
    '                'txtPuestoMec.Text = cmbayudante2.Text
    '            End If

    '        End If
    '    Else

    '    End If
    'End Sub

    Private Sub txtTractor_KeyUp(sender As Object, e As KeyEventArgs) Handles txtTractor.KeyUp
        If txtTractor.Text.Length = 0 Then
            txtTractor.Focus()
            txtTractor.SelectAll()
        Else
            If e.KeyValue = Keys.Enter Then
                Salida = True
                CargaForm(txtTractor.Text, "UNITRANS", "TC", txtTractor,
                " tip.TipoUso = 'T' AND UNI.ESTATUS = 'ACT' AND UNI.IDEMPRESA = " & _cveEmpresa & " AND UNI.IDUNIDADTRANS LIKE '%" & Val(txtTractor.Text) & "%'", True)
                ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)
                'chkBoxTipoOrdServ.Enabled = True
                txtRemolque1.Focus()
                txtRemolque1.SelectAll()
                Salida = False
            Else
                Salida = False
            End If
        End If
    End Sub

    Private Sub txtTractor_Leave(sender As Object, e As EventArgs) Handles txtTractor.Leave
        If Salida Then Exit Sub
        'If txtTractor.Text.Length = 0 Then
        '    txtTractor.Focus()
        '    txtTractor.SelectAll()
        'Else
        If txtTractor.Enabled Then
            CargaForm(txtTractor.Text, "UNITRANS", "TC", txtTractor,
            " tip.TipoUso = 'T' AND UNI.ESTATUS = 'ACT' AND UNI.IDEMPRESA = " & _cveEmpresa & " AND UNI.IDUNIDADTRANS LIKE '%" & Val(txtTractor.Text) & "%'", True)
            ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)
            'chkBoxTipoOrdServ.Enabled = True
            txtRemolque1.Focus()
            txtRemolque1.SelectAll()
        End If
        'End If

    End Sub


    Private Sub txtRemolque1_KeyUp(sender As Object, e As KeyEventArgs) Handles txtRemolque1.KeyUp

        If e.KeyValue = Keys.Enter Then
            Salida = True
            'CargaForm(txtRemolque1.Text, "UNITRANS", "TV", txtRemolque1,
            '" tip.TipoUso = 'Tolva' AND UNI.ESTATUS = 'ACT' AND UNI.IDEMPRESA = " & _cveEmpresa & " AND UNI.IDUNIDADTRANS LIKE '%" & Val(txtRemolque1.Text) & "%'")

            CargaForm(txtRemolque1.Text, "UNITRANS", "", txtRemolque1,
            " tip.TipoUso = 'R' AND UNI.ESTATUS = 'ACT' AND UNI.IDEMPRESA = " &
            _cveEmpresa & " AND UNI.IDUNIDADTRANS LIKE '%" & Val(txtRemolque1.Text) & "%'")

            txtDolly.Focus()
            txtDolly.SelectAll()
            Salida = False
        Else
            Salida = False
        End If


    End Sub

    Private Sub txtRemolque1_Leave(sender As Object, e As EventArgs) Handles txtRemolque1.Leave
        If Salida Then Exit Sub

        If txtRemolque1.Enabled Then
            CargaForm(txtRemolque1.Text, "UNITRANS", "", txtRemolque1,
            " tip.TipoUso = 'R' AND UNI.ESTATUS = 'ACT' AND UNI.IDEMPRESA = " & _cveEmpresa & " AND UNI.IDUNIDADTRANS LIKE '%" & Val(txtRemolque1.Text) & "%'")

            txtDolly.Focus()
            txtDolly.SelectAll()
        End If


    End Sub

    Private Sub txtDolly_KeyUp(sender As Object, e As KeyEventArgs) Handles txtDolly.KeyUp

        If e.KeyValue = Keys.Enter Then
            Salida = True
            CargaForm(txtDolly.Text, "UNITRANS", "DL", txtDolly,
            " tip.TipoUso = 'D' AND UNI.ESTATUS = 'ACT' AND UNI.IDEMPRESA = " & _cveEmpresa & " AND UNI.IDUNIDADTRANS LIKE '%" & Val(txtDolly.Text) & "%'")
            txtRemolque2.Focus()
            txtRemolque2.SelectAll()
            Salida = False
        Else
            Salida = False
        End If


    End Sub

    Private Sub txtDolly_Leave(sender As Object, e As EventArgs) Handles txtDolly.Leave
        If Salida Then Exit Sub

        If txtDolly.Enabled Then
            CargaForm(txtDolly.Text, "UNITRANS", "DL", txtDolly,
            " tip.TipoUso = 'D' AND UNI.ESTATUS = 'ACT' AND UNI.IDEMPRESA = " & _cveEmpresa & " AND UNI.IDUNIDADTRANS LIKE '%" & Val(txtDolly.Text) & "%'")
            txtRemolque2.Focus()
            txtRemolque2.SelectAll()
        End If


    End Sub

    Private Sub txtDolly_TextChanged(sender As Object, e As EventArgs) Handles txtDolly.TextChanged

    End Sub

    Private Sub txtRemolque2_KeyUp(sender As Object, e As KeyEventArgs) Handles txtRemolque2.KeyUp

        If e.KeyValue = Keys.Enter Then
            Salida = True
            CargaForm(txtRemolque2.Text, "UNITRANS", "", txtRemolque2,
            " tip.TipoUso = 'R' AND UNI.ESTATUS = 'ACT' AND UNI.IDEMPRESA = " & _cveEmpresa & " AND UNI.IDUNIDADTRANS LIKE '%" & Val(txtRemolque2.Text) & "%'")

            'cmbChofer.Focus()
            Salida = False
        Else
            Salida = False
        End If


    End Sub

    Private Sub txtRemolque2_Leave(sender As Object, e As EventArgs) Handles txtRemolque2.Leave
        If Salida Then Exit Sub

        If txtRemolque2.Enabled Then
            CargaForm(txtRemolque2.Text, "UNITRANS", "", txtRemolque2,
            " tip.TipoUso = 'R' AND UNI.ESTATUS = 'ACT' AND UNI.IDEMPRESA = " & _cveEmpresa & " AND UNI.IDUNIDADTRANS LIKE '%" & Val(txtRemolque2.Text) & "%'")

            'cmbChofer.Focus()
        End If


    End Sub

    Private Sub txtRemolque2_TextChanged(sender As Object, e As EventArgs) Handles txtRemolque2.TextChanged

    End Sub

    'Private Sub cmbChofer_KeyUp(sender As Object, e As KeyEventArgs) Handles cmbChofer.KeyUp
    '    If e.KeyValue = Keys.Enter Then
    '        Salida = True
    '        'cmbAutoriza.Focus()
    '        Salida = False
    '    End If
    'End Sub


    'Private Sub cmbChofer_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbChofer.SelectedIndexChanged

    'End Sub



    Private Function IniciaOrdenTrabajo(ByVal IdTipo As Integer, ByVal nomTipOrdServ As String, ByVal NumOrdServ As Integer,
                                        ByVal bModifica As Boolean, ByVal TipoServ As Integer, ByVal IdUnidadTrans As String,
                                        ByVal vOpcion As TipoOrdenServicio) As DataTable

        'Dim list As List(Of UnidadesTranspClass)
        'Dim list = cmbTipoServ.SelectedItem


        Dim listObj As List(Of UnidadesTranspClass) = New List(Of UnidadesTranspClass)()
        If bModifica Then
            UniTrans = New UnidadesTranspClass(IdUnidadTrans)
            listObj.Add(UniTrans)
        Else
            UniTrans = New UnidadesTranspClass(txtTractor.Text)
            listObj.Add(UniTrans)
            If txtRemolque1.Text <> "" Then
                UniTrans = New UnidadesTranspClass(txtRemolque1.Text)
                listObj.Add(UniTrans)
            End If
            If txtDolly.Text <> "" Then
                UniTrans = New UnidadesTranspClass(txtDolly.Text)
                listObj.Add(UniTrans)
            End If
            If txtRemolque2.Text <> "" Then
                UniTrans = New UnidadesTranspClass(txtRemolque2.Text)
                listObj.Add(UniTrans)
            End If

        End If



        Dim frm As New fOrdenTrab(_con, _Usuario, _cveEmpresa, IdTipo, nomTipOrdServ, NumOrdServ, listObj,
                                  bModifica, TipoServ, vOpcion, IdUnidadTrans)
        If frm.ShowDialog = Windows.Forms.DialogResult.OK Then
            'Hace algo
            If frm.MyTablaRes.Rows.Count > 0 Then
                If grdOrdenServCab.Rows.Count > 0 Then
                    Clave = grdOrdenServCab.Item("oscCLAVE", grdOrdenServCab.CurrentCell.RowIndex).Value
                    txtEstatusOS.Text = grdOrdenServCab.Item("oscEstatus", grdOrdenServCab.CurrentCell.RowIndex).Value
                    FiltraDetallexClave(Clave)
                End If

            End If
            frm.Dispose()
            Return frm.MyTablaRes
        Else
            frm.Dispose()
            Return Nothing
        End If

        'If log.ShowDialog = Windows.Forms.DialogResult.OK Then
        '    sRutaEmpresaAdmPAQ = log.vRuta

        '    log.Dispose()

        '    Return True
        'Else
        '    log.Dispose()
        '    Return False
        'End If
    End Function

    'Private Sub btnAgregaOS_Click(sender As Object, e As EventArgs)
    '    'Agregar del listado las diferentes ordenes de servicio.
    '    'Empezaremos con Combustible que es con un servicio Fijo.
    '    'Dim s As String = ""
    '    If chkBoxTipoOrdServ.CheckedItems.Count > 0 Then
    '        vNumOrdServ = 0

    '        'Tipos de Orden de Servicio Palomiados
    '        Dim list = chkBoxTipoOrdServ.CheckedItems

    '        Dim listObj As List(Of TiposOrdenServicio) = New List(Of TiposOrdenServicio)()
    '        For Each obj In list
    '            listObj.Add(CType(obj, TiposOrdenServicio))
    '        Next

    '        Dim ObjTipOrdServ As New TiposOrdenServicio()
    '        Dim vIdTipo As Integer = 0
    '        Dim myTabla As DataTable
    '        Dim vCatalogo As String
    '        'Dim vNomCampo As String
    '        Dim vValorId As Integer
    '        Dim tablaRes As DataTable
    '        Dim vUnidadTrans As String = ""
    '        Dim vNotaRecepcion As String = ""
    '        Dim vIdDivision As Integer = 0
    '        Dim vNomDivision As String = ""
    '        Dim vidServicio As Integer = 0
    '        Dim vidActividad As Integer = 0
    '        Dim vDuracionAct As Integer = 0
    '        Dim vNomServicio As String = ""
    '        'Dim idLlanta As String = ""
    '        'Dim PosLLanta As Integer = 0


    '        For Each lobj In listObj
    '            vIdTipo = lobj.idTipoOrden
    '            If lobj.bOrdenTrab Then
    '                'Si tiene que abrir la Forma del Detalle
    '                vNumOrdServ += 1
    '                tablaRes = New DataTable()
    '                tablaRes = IniciaOrdenTrabajo(vIdTipo, lobj.nomTipOrdServ, vNumOrdServ, False, 0, "")
    '                If Not tablaRes Is Nothing Then
    '                    If tablaRes.Rows.Count > 0 Then
    '                        vUnidadTrans = ""
    '                        Dim dv As New DataView(tablaRes)
    '                        dv.Sort = "totIdUnidadTransp"
    '                        Dim DTOrdenado As DataTable = dv.ToTable
    '                        For i = 0 To DTOrdenado.Rows.Count - 1
    '                            If vUnidadTrans = "" And i = 0 Then
    '                                vUnidadTrans = DTOrdenado.DefaultView.Item(i).Item("totIdUnidadTransp")
    '                            End If

    '                            If vUnidadTrans = DTOrdenado.DefaultView.Item(i).Item("totIdUnidadTransp") Then
    '                                'vUnidadTrans = DTOrdenado.DefaultView.Item(i).Item("totIdUnidadTransp")
    '                                vNotaRecepcion = DTOrdenado.DefaultView.Item(i).Item("totNotaRecepcion")
    '                                vIdDivision = DTOrdenado.DefaultView.Item(i).Item("totidDivision")
    '                                vNomDivision = DTOrdenado.DefaultView.Item(i).Item("totNomDivision")
    '                                vidServicio = DTOrdenado.DefaultView.Item(i).Item("totidServicio")
    '                                If vidServicio > 0 Then
    '                                    Servicio = New ServicioClass(vidServicio)
    '                                    If Servicio.Existe Then
    '                                        vNomServicio = Servicio.NomServicio
    '                                    Else
    '                                        vNomServicio = ""
    '                                    End If
    '                                Else
    '                                    vNomServicio = ""
    '                                End If

    '                                vidActividad = DTOrdenado.DefaultView.Item(i).Item("totidActividad")
    '                                vDuracionAct = DTOrdenado.DefaultView.Item(i).Item("totDuracionActHr")
    '                                vTipoServ = DTOrdenado.DefaultView.Item(i).Item("totTipoServ")
    '                                idLlanta = DTOrdenado.DefaultView.Item(i).Item("totidLlanta")
    '                                PosLLanta = DTOrdenado.DefaultView.Item(i).Item("totPosLlanta")

    '                                InsertaRegGridDet(vNumOrdServ, vidServicio, vNomServicio, vidActividad, vNotaRecepcion, vIdDivision,
    '                                                  vNomDivision, vDuracionAct, 0, 0, False, 0, idLlanta, PosLLanta)

    '                                If i = DTOrdenado.Rows.Count - 1 Then
    '                                    UniTrans = New UnidadesTranspClass(vUnidadTrans)
    '                                    TipoUniTrans = New TipoUniTransClass(UniTrans.idTipoUnidad)

    '                                    '10/ENE/2017 - KILOMETRAJE CERO 
    '                                    InsertaRegGridCab(vNumOrdServ, vIdTipo, lobj.nomTipOrdServ, vUnidadTrans, UniTrans.idTipoUnidad,
    '                                    TipoUniTrans.nomTipoUniTras, 0, cmbAutoriza.SelectedValue, lobj.bOrdenTrab, vTipoServ, False, False)

    '                                End If

    '                            Else
    '                                'CAMBIO DE UNIDAD
    '                                'vUnidadTrans = DTOrdenado.DefaultView.Item(i).Item("totIdUnidadTransp")
    '                                'Insertamos Cabecero Anterior

    '                                '10/ENE/2017 -> KILOMETRAJE 0
    '                                InsertaRegGridCab(vNumOrdServ, vIdTipo, lobj.nomTipOrdServ, vUnidadTrans, UniTrans.idTipoUnidad,
    '                                 TipoUniTrans.nomTipoUniTras, 0, cmbAutoriza.SelectedValue, lobj.bOrdenTrab, vTipoServ, False, False)


    '                                vNumOrdServ += 1
    '                                vUnidadTrans = DTOrdenado.DefaultView.Item(i).Item("totIdUnidadTransp")
    '                                vNotaRecepcion = DTOrdenado.DefaultView.Item(i).Item("totNotaRecepcion")
    '                                vIdDivision = DTOrdenado.DefaultView.Item(i).Item("totidDivision")
    '                                vNomDivision = DTOrdenado.DefaultView.Item(i).Item("totNomDivision")
    '                                vidServicio = DTOrdenado.DefaultView.Item(i).Item("totidServicio")
    '                                If vidServicio > 0 Then
    '                                    Servicio = New ServicioClass(vidServicio)
    '                                    If Servicio.Existe Then
    '                                        vNomServicio = Servicio.NomServicio
    '                                    Else
    '                                        vNomServicio = ""
    '                                    End If
    '                                Else
    '                                    vNomServicio = ""
    '                                End If
    '                                vidActividad = DTOrdenado.DefaultView.Item(i).Item("totidActividad")
    '                                vDuracionAct = DTOrdenado.DefaultView.Item(i).Item("totDuracionActHr")
    '                                vTipoServ = DTOrdenado.DefaultView.Item(i).Item("totTipoServ")

    '                                idLlanta = DTOrdenado.DefaultView.Item(i).Item("totidLlanta")
    '                                PosLLanta = DTOrdenado.DefaultView.Item(i).Item("totPosLlanta")

    '                                'InsertaRegGridDet(vNumOrdServ, vidServicio, vNotaRecepcion, vidActividad, "", vIdDivision,
    '                                '                  vNomDivision, vDuracionAct, 0, 0, False, 0, idLlanta, PosLLanta)

    '                                '12/JUL/2017
    '                                InsertaRegGridDet(vNumOrdServ, vidServicio, vNomServicio, vidActividad, vNotaRecepcion, vIdDivision,
    '                                                  vNomDivision, vDuracionAct, 0, 0, False, 0, idLlanta, PosLLanta)

    '                                If i = DTOrdenado.Rows.Count - 1 Then
    '                                    UniTrans = New UnidadesTranspClass(vUnidadTrans)
    '                                    TipoUniTrans = New TipoUniTransClass(UniTrans.idTipoUnidad)

    '                                    '10/ENE/2017 -> KILOMETRAJE 0
    '                                    InsertaRegGridCab(vNumOrdServ, vIdTipo, lobj.nomTipOrdServ, vUnidadTrans, UniTrans.idTipoUnidad,
    '                                    TipoUniTrans.nomTipoUniTras, 0, cmbAutoriza.SelectedValue, lobj.bOrdenTrab, vTipoServ, False, False)

    '                                End If
    '                            End If
    '                        Next
    '                        Clave = grdOrdenServCab.Item("oscCLAVE", grdOrdenServCab.CurrentCell.RowIndex).Value
    '                        FiltraDetallexClave(Clave)
    '                    End If
    '                End If

    '            Else
    '                'Checamos Si Existen Relaciones Automaticas para Este Tipo de Orden de Servicio
    '                myTabla = New DataTable
    '                myTabla = ObjTipOrdServ.TablaRelTipOrdServ("rel.idTipOrdServ = " & vIdTipo)
    '                If myTabla.Rows.Count > 0 Then
    '                    For i = 0 To myTabla.Rows.Count - 1
    '                        vCatalogo = myTabla.DefaultView.Item(i).Item("NomCatalogo").ToString
    '                        'vNomCampo = myTabla.DefaultView.Item(i).Item("NombreId").ToString
    '                        vValorId = myTabla.DefaultView.Item(i).Item("IdRelacion")
    '                        Select Case UCase(vCatalogo)
    '                            Case UCase("catservicios")
    '                                Servicio = New ServicioClass(vValorId)
    '                                If Servicio.Existe Then
    '                                    'Recorrer todas las unidades de TRansporte
    '                                    For Each txtCTR As Control In Me.GroupBox4.Controls
    '                                        If TypeOf txtCTR Is TextBox Then
    '                                            If txtCTR.Tag >= 1 And txtCTR.Tag <= 4 Then
    '                                                'Agregar Orden de Servicio CAB y DET
    '                                                If txtCTR.Text <> "" Then
    '                                                    UniTrans = New UnidadesTranspClass(txtCTR.Text)
    '                                                    TipoUniTrans = New TipoUniTransClass(UniTrans.idTipoUnidad)
    '                                                    If TipoUniTrans.bCombustible Then
    '                                                        vNumOrdServ += 1

    '                                                        '10/ENE/2017 -> KILOMETRAJE 0
    '                                                        InsertaRegGridCab(vNumOrdServ, vIdTipo, lobj.nomTipOrdServ, UniTrans.iUnidadTrans, UniTrans.idTipoUnidad,
    '                                                        TipoUniTrans.nomTipoUniTras, 0, cmbAutoriza.SelectedValue, lobj.bOrdenTrab, Servicio.idTipoServicio, False, False)

    '                                                        idLlanta = ""
    '                                                        PosLLanta = 0

    '                                                        InsertaRegGridDet(vNumOrdServ, vValorId, Servicio.NomServicio, 0, "", 0, "", 0, 0, 0, False, 0, idLlanta, PosLLanta)
    '                                                    End If
    '                                                End If
    '                                            End If
    '                                        End If
    '                                    Next
    '                                End If
    '                        End Select
    '                    Next
    '                Else
    '                    'No tiene RElaciones
    '                End If
    '            End If


    '        Next
    '    End If
    'End Sub
    Private Function InsertaRegGridCab(ByVal bRecepcionado As Boolean, ByVal ClaveFicticia As Integer,
    ByVal IdTipoOrdServ As Integer, ByVal NomTipoOrdServ As String, ByVal idUnidadTrans As String,
    ByVal idTipoUnidadTrans As Integer, ByVal nomTipoUniTras As String,
    ByVal Km As Integer, ByVal idEmpleadoAutoriza As Integer, ByVal bOrdenTrab As Boolean,
    ByVal idTipoServ As Integer, ByVal bModificado As Boolean, ByVal bCancelado As Boolean,
    ByVal FechaAsigna As DateTime, ByVal Estatus As String) As Boolean
        Try

            grdOrdenServCab.Rows.Add(IIf(bRecepcionado, 1, 0), ClaveFicticia, ClaveFicticia, 0, dtpFechaCap.Value, IdTipoOrdServ,
            NomTipoOrdServ, idUnidadTrans, idTipoUnidadTrans, nomTipoUniTras, Km, idEmpleadoAutoriza, bOrdenTrab, idTipoServ,
            IIf(bModificado, 1, 0), IIf(bCancelado, 1, 0), FechaAsigna, Estatus)

            'Bloquear Fila que ya se recepciono
            For Each row As DataGridViewRow In grdOrdenServCab.Rows
                If CBool(row.Cells("oscDiagnostico").Value) Then
                    row.ReadOnly = True
                    'row.Visible = False
                End If
            Next

            grdOrdenServCab.PerformLayout()
            Return True

        Catch ex As Exception
            Return False
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, Me.Text)
        End Try

    End Function

    Private Function InsertaRegGridDet(ByVal bAsignado As Boolean, ByVal ClaveFicticia As Integer, ByVal idServicio As Integer,
    ByVal NomServicio As String, ByVal idActividad As Integer, ByVal NombreAct As String, ByVal idDivision As Integer,
    ByVal NomDivision As String, ByVal DuracionActHr As Integer, ByVal IdOrdenSer As Integer, ByVal idOrdenTrabajo As Integer,
    ByVal bModificado As Boolean, ByVal idOrdenActividad As Integer, ByVal idLlanta As String, ByVal Posicion As Integer,
    ByVal idPersonalResp As Integer, ByVal idPersonalAyu1 As Integer, ByVal idPersonalAyu2 As Integer,
    ByVal idEnum As Integer, ByVal Estatus As String,
    ByVal CIDPRODUCTO As Integer, ByVal CCODIGOPRODUCTO As String, ByVal CNOMBREPRODUCTO As String,
    ByVal Cantidad_CIDPRODUCTO_SERV As Decimal, ByVal Precio_CIDPRODUCTO_SERV As Decimal)

        Try

            grdOrdenServDet.Rows.Add(IIf(bAsignado, 1, 0), ClaveFicticia, ClaveFicticia, idServicio, idOrdenTrabajo,
            NomServicio, idActividad,
            NombreAct, idDivision, NomDivision, DuracionActHr, IIf(bModificado, 1, 0), idOrdenActividad, idLlanta, Posicion,
            idPersonalResp, idPersonalAyu1, idPersonalAyu2, idEnum, Estatus,
            CIDPRODUCTO, CCODIGOPRODUCTO, CNOMBREPRODUCTO, Cantidad_CIDPRODUCTO_SERV, Precio_CIDPRODUCTO_SERV)



            myDataRow = MyTablaDetalle.NewRow()
            myDataRow("tosdDiagnostico") = IIf(bAsignado, 1, 0)
            myDataRow("tosdCLAVE") = ClaveFicticia
            myDataRow("tosdIdOrdenSer") = IdOrdenSer
            myDataRow("tosdidServicio") = idServicio
            myDataRow("tosdNomServicio") = NomServicio
            myDataRow("tosdidActividad") = idActividad
            myDataRow("tosdNombreAct") = NombreAct
            myDataRow("tosdidOrdenTrabajo") = idOrdenTrabajo
            myDataRow("tosdidDivision") = idDivision
            myDataRow("tosdNomDivision") = NomDivision
            myDataRow("tosdDuracionActHr") = DuracionActHr

            myDataRow("tosdbModificado") = bModificado
            myDataRow("tosdidOrdenActividad") = idOrdenActividad

            myDataRow("tosdidLlanta") = idLlanta
            myDataRow("tosdPosicion") = Posicion

            myDataRow("tosdidPersonalResp") = idPersonalResp
            myDataRow("tosdidPersonalAyu1") = idPersonalAyu1
            myDataRow("tosdidPersonalAyu2") = idPersonalAyu2
            myDataRow("tosdidEnum") = idEnum
            myDataRow("tosdEstatus") = Estatus

            'servicios
            myDataRow("tosdCIDPRODUCTO") = CIDPRODUCTO
            myDataRow("tosdCCODIGOPRODUCTO") = CCODIGOPRODUCTO
            myDataRow("tosdCNOMBREPRODUCTO") = CNOMBREPRODUCTO
            myDataRow("tosdCantidad_CIDPRODUCTO_SERV") = Cantidad_CIDPRODUCTO_SERV
            myDataRow("tosdPrecio_CIDPRODUCTO_SERV") = Precio_CIDPRODUCTO_SERV



            MyTablaDetalle.Rows.Add(myDataRow)

            'Bloquear Fila que ya se recepciono
            For Each row As DataGridViewRow In grdOrdenServDet.Rows
                If CBool(row.Cells("osdDiagnostico").Value) Then
                    row.ReadOnly = True
                    'row.Visible = False
                End If
            Next

            grdOrdenServDet.PerformLayout()
            Return True

        Catch ex As Exception
            Return False
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, Me.Text)
        End Try

    End Function


    Private Sub txtRemolque1_TextChanged(sender As Object, e As EventArgs) Handles txtRemolque1.TextChanged

    End Sub

    Private Sub txtTractor_TextChanged(sender As Object, e As EventArgs) Handles txtTractor.TextChanged

    End Sub

    '10/ENE/2017
    'Private Sub txtKilometraje_KeyUp(sender As Object, e As KeyEventArgs)
    '    If txtKilometraje.Text.Length = 0 Then
    '        txtKilometraje.Focus()
    '        txtKilometraje.SelectAll()
    '    Else
    '        If e.KeyValue = Keys.Enter Then
    '            Salida = True
    '            If Val(txtKilometraje.Text) > vUltKm And Val(txtKilometraje.Text) <= (vUltKm + MaximoKmdeMas) Then
    '                chkBoxTipoOrdServ.Focus()
    '            Else
    '                MsgBox("El Kilometraje no es Valido", MsgBoxStyle.Exclamation, Me.Text)
    '                txtKilometraje.Focus()
    '                txtKilometraje.SelectAll()
    '            End If
    '            Salida = False
    '        End If
    '    End If
    'End Sub

    '10/ENE/2017
    'Private Sub txtKilometraje_Leave(sender As Object, e As EventArgs)
    '    If Salida Then Exit Sub
    '    If txtKilometraje.Text.Length = 0 Then
    '        txtKilometraje.Focus()
    '        txtKilometraje.SelectAll()
    '    Else
    '        If txtKilometraje.Enabled Then
    '            If Val(txtKilometraje.Text) > vUltKm And Val(txtKilometraje.Text) <= (vUltKm + MaximoKmdeMas) Then
    '                chkBoxTipoOrdServ.Focus()
    '            Else
    '                MsgBox("El Kilometraje no es Valido", MsgBoxStyle.Exclamation, Me.Text)
    '                txtKilometraje.Focus()
    '                txtKilometraje.SelectAll()
    '            End If
    '        End If
    '    End If

    'End Sub


    Private Sub grdOrdenServCab_Click(sender As Object, e As EventArgs) Handles grdOrdenServCab.Click
        Salida = True
        Clave = grdOrdenServCab.Item("oscCLAVE", grdOrdenServCab.CurrentCell.RowIndex).Value
        txtEstatusOS.Text = grdOrdenServCab.Item("oscEstatus", grdOrdenServCab.CurrentCell.RowIndex).Value
        FiltraDetallexClave(Clave)
        Salida = False
    End Sub

    Private Sub CreaTablaDetalle()
        Try
            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Boolean")
            myDataColumn.ColumnName = "tosdDiagnostico"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaDetalle.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Int32")
            myDataColumn.ColumnName = "tosdCLAVE"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaDetalle.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Int32")
            myDataColumn.ColumnName = "tosdIdOrdenSer"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaDetalle.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Int32")
            myDataColumn.ColumnName = "tosdidServicio"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaDetalle.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.String")
            myDataColumn.ColumnName = "tosdNomServicio"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaDetalle.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Int32")
            myDataColumn.ColumnName = "tosdidActividad"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaDetalle.Columns.Add(myDataColumn)


            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.String")
            myDataColumn.ColumnName = "tosdNombreAct"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaDetalle.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Int32")
            myDataColumn.ColumnName = "tosdidOrdenTrabajo"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaDetalle.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Int32")
            myDataColumn.ColumnName = "tosdidDivision"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaDetalle.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.String")
            myDataColumn.ColumnName = "tosdNomDivision"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaDetalle.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Int32")
            myDataColumn.ColumnName = "tosdDuracionActHr"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaDetalle.Columns.Add(myDataColumn)


            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Boolean")
            myDataColumn.ColumnName = "tosdbModificado"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaDetalle.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Int32")
            myDataColumn.ColumnName = "tosdidOrdenActividad"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaDetalle.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.String")
            myDataColumn.ColumnName = "tosdidLlanta"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaDetalle.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Int32")
            myDataColumn.ColumnName = "tosdPosicion"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaDetalle.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Int32")
            myDataColumn.ColumnName = "tosdidPersonalResp"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaDetalle.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Int32")
            myDataColumn.ColumnName = "tosdidPersonalAyu1"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaDetalle.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Int32")
            myDataColumn.ColumnName = "tosdidPersonalAyu2"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaDetalle.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Int32")
            myDataColumn.ColumnName = "tosdidEnum"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaDetalle.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.String")
            myDataColumn.ColumnName = "tosdEstatus"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaDetalle.Columns.Add(myDataColumn)

            'SERVICIOS COMERCIAL 
            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Int32")
            myDataColumn.ColumnName = "tosdCIDPRODUCTO"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaDetalle.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.String")
            myDataColumn.ColumnName = "tosdCCODIGOPRODUCTO"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaDetalle.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.String")
            myDataColumn.ColumnName = "tosdCNOMBREPRODUCTO"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaDetalle.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Decimal")
            myDataColumn.ColumnName = "tosdCantidad_CIDPRODUCTO_SERV"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaDetalle.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Decimal")
            myDataColumn.ColumnName = "tosdPrecio_CIDPRODUCTO_SERV"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaDetalle.Columns.Add(myDataColumn)


        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try

    End Sub

    Private Sub FiltraDetallexClave(ByVal Clave As Integer)
        Try
            MyTablaDetalle.DefaultView.RowFilter = Nothing
            MyTablaDetalle.DefaultView.RowFilter = "tosdCLAVE = " & Clave

            grdOrdenServDet.Rows.Clear()
            For i = 0 To MyTablaDetalle.DefaultView.Count - 1
                grdOrdenServDet.Rows.Add()
                grdOrdenServDet.Item("osdDiagnostico", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdDiagnostico")
                grdOrdenServDet.Item("osdCLAVE", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdCLAVE")
                grdOrdenServDet.Item("osdIdOrdenSer", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdIdOrdenSer")
                grdOrdenServDet.Item("osdidServicio", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdidServicio")
                grdOrdenServDet.Item("osdNomServicio", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdNomServicio")
                grdOrdenServDet.Item("osdidActividad", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdidActividad")
                grdOrdenServDet.Item("osdNombreAct", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdNombreAct")
                grdOrdenServDet.Item("osdidOrdenTrabajo", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdidOrdenTrabajo")
                grdOrdenServDet.Item("osdidDivision", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdidDivision")
                grdOrdenServDet.Item("osdNomDivision", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdNomDivision")

                grdOrdenServDet.Item("osdDuracionActHr", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdDuracionActHr")
                grdOrdenServDet.Item("osdModificado", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdbModificado")
                grdOrdenServDet.Item("osdidOrdenActividad", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdidOrdenActividad")

                grdOrdenServDet.Item("osdidLlanta", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdidLlanta")
                grdOrdenServDet.Item("osdPosicion", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdPosicion")

                grdOrdenServDet.Item("osdidPersonalResp", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdidPersonalResp")
                grdOrdenServDet.Item("osdidPersonalAyu1", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdidPersonalAyu1")
                grdOrdenServDet.Item("osdidPersonalAyu2", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdidPersonalAyu2")

                grdOrdenServDet.Item("osdidEnum", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdidEnum")

                grdOrdenServDet.Item("osdEstatus", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdEstatus")

                'Servicios
                grdOrdenServDet.Item("osdCIDPRODUCTO", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdCIDPRODUCTO")
                grdOrdenServDet.Item("osdCCODIGOPRODUCTO", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdCCODIGOPRODUCTO")
                grdOrdenServDet.Item("osdCNOMBREPRODUCTO", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdCNOMBREPRODUCTO")
                grdOrdenServDet.Item("osdCantidad_CIDPRODUCTO_SERV", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdCantidad_CIDPRODUCTO_SERV")
                grdOrdenServDet.Item("osdPrecio_CIDPRODUCTO_SERV", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdPrecio_CIDPRODUCTO_SERV")





                For Each row As DataGridViewRow In grdOrdenServDet.Rows
                    If CBool(row.Cells("osdDiagnostico").Value) Then
                        row.ReadOnly = True
                        'row.Visible = False
                    End If
                Next
                grdOrdenServDet.PerformLayout()
                'grdOrdenServDet.DataSource = MyTablaDetalle
            Next
            grdOrdenServDet.PerformLayout()
            'MyTablaDetalle.DefaultView.RowFilter = Nothing
        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try


    End Sub

    Private Sub AgregaTODOSDET()
        Try
            MyTablaDetalle.DefaultView.RowFilter = Nothing

            grdOrdenServDet.Rows.Clear()
            For i = 0 To MyTablaDetalle.DefaultView.Count - 1
                grdOrdenServDet.Rows.Add()
                grdOrdenServDet.Item("osdDiagnostico", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdDiagnostico")
                grdOrdenServDet.Item("osdCLAVE", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdCLAVE")
                grdOrdenServDet.Item("osdIdOrdenSer", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdIdOrdenSer")
                grdOrdenServDet.Item("osdidServicio", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdidServicio")
                grdOrdenServDet.Item("osdNomServicio", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdNomServicio")
                grdOrdenServDet.Item("osdidActividad", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdidActividad")
                grdOrdenServDet.Item("osdNombreAct", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdNombreAct")
                grdOrdenServDet.Item("osdidOrdenTrabajo", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdidOrdenTrabajo")
                grdOrdenServDet.Item("osdidDivision", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdidDivision")
                grdOrdenServDet.Item("osdNomDivision", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdNomDivision")

                grdOrdenServDet.Item("osdDuracionActHr", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdDuracionActHr")
                grdOrdenServDet.Item("osdModificado", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdbModificado")
                grdOrdenServDet.Item("osdidOrdenActividad", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdidOrdenActividad")

                grdOrdenServDet.Item("osdidLlanta", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdidLlanta")
                grdOrdenServDet.Item("osdPosicion", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdPosicion")

                grdOrdenServDet.Item("osdidPersonalResp", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdidPersonalResp")
                grdOrdenServDet.Item("osdidPersonalAyu1", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdidPersonalAyu1")
                grdOrdenServDet.Item("osdidPersonalAyu2", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdidPersonalAyu2")

                grdOrdenServDet.Item("osdidEnum", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdidEnum")

                grdOrdenServDet.Item("osdEstatus", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdEstatus")

                'servicios
                grdOrdenServDet.Item("osdCIDPRODUCTO", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdCIDPRODUCTO")
                grdOrdenServDet.Item("osdCCODIGOPRODUCTO", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdCCODIGOPRODUCTO")
                grdOrdenServDet.Item("osdCNOMBREPRODUCTO", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdCNOMBREPRODUCTO")
                grdOrdenServDet.Item("osdCantidad_CIDPRODUCTO_SERV", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdCantidad_CIDPRODUCTO_SERV")
                grdOrdenServDet.Item("osdPrecio_CIDPRODUCTO_SERV", i).Value = MyTablaDetalle.DefaultView.Item(i).Item("tosdPrecio_CIDPRODUCTO_SERV")

                For Each row As DataGridViewRow In grdOrdenServDet.Rows
                    If CBool(row.Cells("osdDiagnostico").Value) Then
                        row.ReadOnly = True
                        'row.Visible = False
                    End If
                Next
                grdOrdenServDet.PerformLayout()
                'grdOrdenServDet.DataSource = MyTablaDetalle
            Next
            grdOrdenServDet.PerformLayout()
            'MyTablaDetalle.DefaultView.RowFilter = Nothing
        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try


    End Sub

    'Private Sub Button5_Click(sender As Object, e As EventArgs)
    '    'ImprimeOrdenServicio(271, False, True, True)




    '    ImprimeOrdenServicio(287, False, True, True)
    '    'ImprimeOrdenServicio(288, True, True, True)
    '    'ImprimeOrdenServicio(289, True, True, True)


    '    'ImprimeOrdenServicio(272, True, True, True)

    '    'ImprimeOrdenServicio(284, False, True, True)
    'End Sub

    Private Sub txtidPadreOrdSer_KeyDown(sender As Object, e As KeyEventArgs) Handles txtidPadreOrdSer.KeyDown
        If e.KeyCode = Keys.F3 Then
            'cmdBuscaClave_Click(sender, e)
            'txtTractor.Text = DespliegaGrid(UCase("FolioOS"), Inicio.CONSTR, "idPadreOrdSer")
            'CargaForm(txtidPadreOrdSer.Text, "FolioOS", , , , True)
            txtidPadreOrdSer_Leave(sender, e)
            'asd()
        End If

    End Sub


    Private Sub txtidPadreOrdSer_KeyUp(sender As Object, e As KeyEventArgs) Handles txtidPadreOrdSer.KeyUp
        If e.KeyValue = Keys.Enter Then
            Salida = True
            CargaForm(txtidPadreOrdSer.Text, "FolioOS", , , , True)
            txtOrdenServ.Enabled = False
            If txtEstatus.Text = "ENT" Or txtEstatus.Text = "PRO" Then
                ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)
                ActivaCampos(False, TipoOpcActivaCampos.tOpcOTRO)
                'txtidPadreOrdSer.Enabled = False
                'cmbEntrega.Focus()

                'chkBoxTipoOrdServ.Enabled = True
                'chkBoxTipoOrdServ.Focus()
            Else
                ActivaBotones(True, TipoOpcActivaBoton.tOpcConsulta)
                ActivaCampos(False, TipoOpcActivaCampos.tOpcDEPENDEVALOR)
                'chkBoxTipoOrdServ.Enabled = False
                grdOrdenServCab.Focus()
            End If


            Salida = False
        Else
            Salida = False
        End If
    End Sub

    Private Sub txtidPadreOrdSer_Leave(sender As Object, e As EventArgs) Handles txtidPadreOrdSer.Leave
        If Salida Then Exit Sub
        'CargaForm(txtidPadreOrdSer.Text, "FolioOS", , , , True)
        'ActivaBotones(True, TipoOpcActivaBoton.tOpcInicializa)
        'ActivaCampos(False, TipoOpcActivaCampos.tOpcDEPENDEVALOR)
        'chkBoxTipoOrdServ.Focus()
        Salida = True
        CargaForm(txtidPadreOrdSer.Text, "FolioOS", , , , True)
        If txtEstatus.Text = "CAP" Then
            'ActivaBotones(True, TipoOpcActivaBoton.tOpcInicializa)
            ActivaBotones(True, TipoOpcActivaBoton.tOpcEditar)
            ActivaCampos(False, TipoOpcActivaCampos.tOpcDEPENDEVALOR)
            'chkBoxTipoOrdServ.Enabled = False
            'chkBoxTipoOrdServ.Focus()
        Else
            ActivaBotones(True, TipoOpcActivaBoton.tOpcConsulta)
            ActivaCampos(False, TipoOpcActivaCampos.tOpcDEPENDEVALOR)
            'chkBoxTipoOrdServ.Enabled = False
            grdOrdenServCab.Focus()
        End If
    End Sub

    Private Sub txtidPadreOrdSer_TextChanged(sender As Object, e As EventArgs) Handles txtidPadreOrdSer.TextChanged

    End Sub


    Private Function SetupThePrinting() As Boolean
        Dim MyPrintDialog As New PrintDialog()

        MyPrintDialog.AllowCurrentPage = False
        MyPrintDialog.AllowPrintToFile = False
        MyPrintDialog.AllowSelection = False
        MyPrintDialog.AllowSomePages = False
        MyPrintDialog.PrintToFile = False
        MyPrintDialog.ShowHelp = False
        MyPrintDialog.ShowNetwork = False

        If MyPrintDialog.ShowDialog <> DialogResult.OK Then
            Return False
        End If

        If prtSettings Is Nothing Then
            prtSettings = New PrinterSettings
        End If
        prtSettings = MyPrintDialog.PrinterSettings

        'prtDoc.DocumentName = "Reporte de Auditoria"
        'prtDoc.PrinterSettings = prtSettings
        'prtDoc.PrinterSettings = MyPrintDialog.PrinterSettings
        'prtDoc.DefaultPageSettings = MyPrintDialog.PrinterSettings.DefaultPageSettings
        'prtDoc.DefaultPageSettings.Margins = New Margins(40, 40, 40, 40)
        Return True
    End Function

    Private Sub btnMenuReImprimir_Click(sender As Object, e As EventArgs) Handles btnMenuReImprimir.Click
        'ImprimeOrdenServicio(opcTipoTicket.PreSalida, 3237, False, False, True,, 1702)
        If SetupThePrinting() Then
            If Val(txtOrdenServ.Text) > 0 Then
                StrSql = ObjSql.DeterminaPreSalidas(Val(txtOrdenServ.Text))
                'If SetupThePrinting() Then
                DtImprime = BD.ExecuteReturn(StrSql)
                If DtImprime.Rows.Count > 0 Then
                    For i = 0 To DtImprime.Rows.Count - 1
                        ImprimeOrdenServicio(opcTipoTicket.PreSalida, txtOrdenServ.Text,
                                        False, True, False,, DtImprime.Rows.Item(i).Item("IdPreSalida"))
                    Next
                End If
                'End If
            End If

            'If Val(txtidPadreOrdSer.Text) > 0 Then
            '    'If SetupThePrinting() Then
            '    For i = 0 To grdOrdenServCab.Rows.Count - 1
            '        ImprimeOrdenServicio(opcTipoTicket.OrdenServ,
            '            grdOrdenServCab.Item("oscCLAVE", i).Value,
            '            grdOrdenServCab.Item("oscbOrdenTrabajo", i).Value, False, False)
            '    Next
            '    'End If
            'Else
            '    MsgBox("tiene que Existir el numero de FOLIO", MsgBoxStyle.Exclamation, Me.Text)
            'End If
        End If





    End Sub



    Private Sub ImprimirToolStripMenuOrdSer_Click(sender As Object, e As EventArgs) Handles ImprimirToolStripMenuOrdSer.Click
        Try
            vNumOrdServ = grdOrdenServCab.Item("oscCLAVE", grdOrdenServCab.CurrentCell.RowIndex).Value
            vOrdenTrabajo = grdOrdenServCab.Item("oscbOrdenTrabajo", grdOrdenServCab.CurrentCell.RowIndex).Value
            ImprimeOrdenServicio(opcTipoTicket.OrdenServ, vNumOrdServ, vOrdenTrabajo, True, False)
        Catch ex As Exception

        End Try
    End Sub



    Private Sub ConMnuOrdSer_Opening(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles ConMnuOrdSer.Opening
        With grdOrdenServCab.Rows(grdOrdenServCab.CurrentRow.Index)
            ImprimirToolStripMenuOrdSer.Enabled = True
            CancelarToolStripMenuOrdSer.Enabled = True
            If .Cells("oscbOrdenTrabajo").Value Then
                ModificarToolStripMenuOrdSer.Enabled = True
            Else
                ModificarToolStripMenuOrdSer.Enabled = False
            End If
        End With
    End Sub



    Private Sub ToolStripButton1_Click(sender As Object, e As EventArgs) Handles btnMnuCancelar.Click

        Dim fCanc As New FCancelaSolServ(txtidPadreOrdSer.Text, txtNomEmpresa.Text, dtpFechaCap.Value, _Usuario)
        fCanc.WindowState = FormWindowState.Normal
        fCanc.Show()
        Cancelar()
        txtTractor.Focus()

    End Sub

    'Private Sub chkBoxTipoOrdServ_EnabledChanged(sender As Object, e As EventArgs)
    '    btnAgregaOS.Enabled = chkBoxTipoOrdServ.Enabled
    'End Sub



    Private Sub chkMarcaTodas_CheckedChanged(sender As Object, e As EventArgs) Handles chkMarcaTodas.CheckedChanged
        If grdOrdenServCab.RowCount > 0 Then
            For i = 0 To grdOrdenServCab.RowCount - 1
                If Not grdOrdenServCab.Rows(i).Cells(grdOrdenServCab.CurrentCell.RowIndex).ReadOnly Then
                    grdOrdenServCab.Item("oscDiagnostico", i).Value = IIf(chkMarcaTodas.Checked, 1, 0)
                    'MyTablaViaje.DefaultView.Item(i).Item("Facturar") = IIf(chkMarcaFacturas.Checked, 1, 0)
                    cmbResponsable.Enabled = True
                    cmbResponsable.SelectedIndex = 0
                    cmbayudante1.SelectedIndex = 0
                    cmbayudante2.SelectedIndex = 0
                    btnMnuOk.Enabled = True
                End If
            Next
        End If
    End Sub



    Private Sub grdOrdenServDet_Click(sender As Object, e As EventArgs) Handles grdOrdenServDet.Click
        Salida = True
        If txtEstatusOS.Text = "ASIG" Then
            'PRIMERO SE HABRE EL DIAGNOSTICO. Y SE ACTUALIZA EN EL GRID
            Dim fDiag As New FCapDiagnosticoOS(txtOrdenServ.Text,
       grdOrdenServDet.Item("osdidOrdenTrabajo", grdOrdenServDet.CurrentCell.RowIndex).Value,
        grdOrdenServDet.Item("osdNotaDiagnostico", grdOrdenServDet.CurrentCell.RowIndex).Value,
        grdOrdenServDet.Item("osdDuracionActHr", grdOrdenServDet.CurrentCell.RowIndex).Value, txtTipoOrdenServ.Text)

            If fDiag.ShowDialog = Windows.Forms.DialogResult.OK Then
                grdOrdenServDet.Item("osdNotaDiagnostico", grdOrdenServDet.CurrentCell.RowIndex).Value = fDiag.Diagnostico
                grdOrdenServDet.Item("osdDuracionActHr", grdOrdenServDet.CurrentCell.RowIndex).Value = fDiag.DuracionHR
                grdOrdenServDet.Item("osdDiagnostico", grdOrdenServDet.CurrentCell.RowIndex).Value = True

                'servicios
                grdOrdenServDet.Item("osdCIDPRODUCTO", grdOrdenServDet.CurrentCell.RowIndex).Value = fDiag.CIDPRODUCTO_SERV
                grdOrdenServDet.Item("osdCCODIGOPRODUCTO", grdOrdenServDet.CurrentCell.RowIndex).Value = fDiag.CCODIGOPRODUCTO_SERV
                grdOrdenServDet.Item("osdCNOMBREPRODUCTO", grdOrdenServDet.CurrentCell.RowIndex).Value = fDiag.CNOMBREPRODUCTO_SERV
                grdOrdenServDet.Item("osdCantidad_CIDPRODUCTO_SERV", grdOrdenServDet.CurrentCell.RowIndex).Value = fDiag.Cantidad_CIDPRODUCTO_SERV
                grdOrdenServDet.Item("osdPrecio_CIDPRODUCTO_SERV", grdOrdenServDet.CurrentCell.RowIndex).Value = fDiag.Precio_CIDPRODUCTO_SERV
            End If

            'LLANTAS
            If grdOrdenServDet.Item("osdidEnum", grdOrdenServDet.CurrentCell.RowIndex).Value > 0 Then
                Clave = grdOrdenServCab.Item("oscCLAVE", grdOrdenServCab.CurrentCell.RowIndex).Value
                Dim frm As New fServInternosLlantas(_con, True, _Usuario,
                grdOrdenServDet.Item("osdidLlanta", grdOrdenServDet.CurrentCell.RowIndex).Value,
                grdOrdenServDet.Item("osdidEnum", grdOrdenServDet.CurrentCell.RowIndex).Value)

                If frm.ShowDialog = Windows.Forms.DialogResult.OK Then
                    grdOrdenServDet.Item("osdDiagnostico", grdOrdenServDet.CurrentCell.RowIndex).Value = True
                    'Hace algo
                    'If frm.MyTablaRes.Rows.Count > 0 Then
                    '    If grdOrdenServCab.Rows.Count > 0 Then
                    '        Clave = grdOrdenServCab.Item("oscCLAVE", grdOrdenServCab.CurrentCell.RowIndex).Value
                    '        txtEstatusOS.Text = grdOrdenServCab.Item("oscEstatus", grdOrdenServCab.CurrentCell.RowIndex).Value
                    '        FiltraDetallexClave(Clave)
                    '    End If

                    'End If
                    'frm.Dispose()
                    'Return frm.MyTablaRes
                Else
                    'grdOrdenServDet.Item("osdDiagnostico", grdOrdenServDet.CurrentCell.RowIndex).Value = False
                End If
                grdOrdenServDet.PerformLayout()
                frm.Dispose()
            Else
                'If grdOrdenServDet.Item("osdDiagnostico", grdOrdenServDet.CurrentCell.RowIndex).Value Then
                '    grdOrdenServDet.Item("osdDiagnostico", grdOrdenServDet.CurrentCell.RowIndex).Value = False
                'Else
                '    grdOrdenServDet.Item("osdDiagnostico", grdOrdenServDet.CurrentCell.RowIndex).Value = True

                'End If


                'grdOrdenServDet.Item("osdDiagnostico", grdOrdenServDet.CurrentCell.RowIndex).Value = True

                cmbResponsable.SelectedValue = grdOrdenServDet.Item("osdidPersonalResp", grdOrdenServDet.CurrentCell.RowIndex).Value

                If (grdOrdenServDet.Item("osdidPersonalAyu1", grdOrdenServDet.CurrentCell.RowIndex).Value > 0) Then
                    chkAyu1.Checked = True
                    cmbayudante1.SelectedValue = grdOrdenServDet.Item("osdidPersonalAyu1", grdOrdenServDet.CurrentCell.RowIndex).Value

                End If

                If (grdOrdenServDet.Item("osdidPersonalAyu2", grdOrdenServDet.CurrentCell.RowIndex).Value > 0) Then
                    chkAyu2.Checked = True
                    cmbayudante2.SelectedValue = grdOrdenServDet.Item("osdidPersonalAyu2", grdOrdenServDet.CurrentCell.RowIndex).Value

                End If

                'If cmbayudante2.SelectedValue = grdOrdenServDet.Item("osdidPersonalAyu2", grdOrdenServDet.CurrentCell.RowIndex).Value > 0 Then
                '    chkAyu2.Checked = True
                '    cmbayudante2.SelectedValue = grdOrdenServDet.Item("osdidPersonalAyu2", grdOrdenServDet.CurrentCell.RowIndex).Value

                'End If
            End If


        Else
            'cmbResponsable.Enabled = True
            'If grdOrdenServDet.Item("osdidPersonalResp", grdOrdenServDet.CurrentCell.RowIndex).Value > 0 Then
            '    cmbResponsable.SelectedValue = grdOrdenServDet.Item("osdidPersonalResp", grdOrdenServDet.CurrentCell.RowIndex).Value
            'Else
            '    cmbResponsable.SelectedIndex = 0
            'End If
            'If grdOrdenServDet.Item("osdidPersonalAyu1", grdOrdenServDet.CurrentCell.RowIndex).Value > 0 Then
            '    chkAyu1.Checked = True
            '    cmbayudante1.SelectedValue = grdOrdenServDet.Item("osdidPersonalAyu1", grdOrdenServDet.CurrentCell.RowIndex).Value
            'Else
            '    chkAyu1.Checked = False
            '    cmbayudante1.SelectedIndex = 0
            'End If
            'If grdOrdenServDet.Item("osdidPersonalAyu2", grdOrdenServDet.CurrentCell.RowIndex).Value > 0 Then
            '    chkAyu2.Checked = True
            '    cmbayudante2.SelectedValue = grdOrdenServDet.Item("osdidPersonalAyu2", grdOrdenServDet.CurrentCell.RowIndex).Value
            'Else
            '    chkAyu2.Checked = False
            '    cmbayudante2.SelectedIndex = 0
            'End If

            'cmbResponsable.Focus()
        End If

        'FiltraDetallexClave(Clave)
        Salida = False
    End Sub

    Private Sub chkAyu1_CheckedChanged(sender As Object, e As EventArgs) Handles chkAyu1.CheckedChanged
        If chkAyu1.Checked Then
            cmbayudante1.Enabled = True
        Else
            cmbayudante1.Enabled = False
        End If
    End Sub

    Private Sub chkAyu2_CheckedChanged(sender As Object, e As EventArgs) Handles chkAyu2.CheckedChanged
        If chkAyu2.Checked Then
            cmbayudante2.Enabled = True
        Else
            cmbayudante2.Enabled = False

        End If
    End Sub

    Private Function Validar() As Boolean

        'If Not ValidarEmpleados() Then
        '    Return False
        'End If
        For i = 0 To grdOrdenServDet.Rows.Count - 1
            If Not grdOrdenServDet.Item("osdDiagnostico", i).Value Then
                MsgBox("Todas las Ordenes de Trabajo deben de Estar Diagnosticadas", vbInformation, "Aviso" & Me.Text)
                grdOrdenServDet.Focus()
                'Exit For
                Return False

            End If
        Next
        Return True

    End Function

    Private Function ValidarEmpleados() As Boolean
        'Dim Resp As Boolean = True
        'Empleados
        If chkAyu1.Checked Then
            If cmbayudante1.SelectedValue = cmbResponsable.SelectedValue Then
                MsgBox("No puede ser el mismo Empleado Responsable y Ayudante 1", vbInformation, "Aviso" & Me.Text)
                cmbayudante1.Focus()
                Return False
            End If
        End If
        If chkAyu2.Checked Then
            If cmbayudante2.SelectedValue = cmbResponsable.SelectedValue Then
                MsgBox("No puede ser el mismo Empleado Responsable y Ayudante 2", vbInformation, "Aviso" & Me.Text)
                cmbayudante2.Focus()
                Return False
            End If
        End If
        If chkAyu1.Checked And chkAyu2.Checked Then
            If cmbayudante1.SelectedValue = cmbayudante2.SelectedValue Then
                MsgBox("No puede ser el mismo Empleado Ayudante 1 y Ayudante 2", vbInformation, "Aviso" & Me.Text)
                cmbayudante2.Focus()
                Return False
            End If
        End If

        'Checar si cada Empleado no esta asignado
        ULTOT = New OrdenTrabajoClass(0)
        dtAsignados.Rows.Clear()
        dtAsignados = ULTOT.tbEmpleadoAsignado(cmbResponsable.SelectedValue, opTipoEmpleado.Responsable)
        If dtAsignados.Rows.Count > 0 Then
            MsgBox("El Empleado : " & cmbResponsable.Text & " se encuentra asignado a la Orden de Servicio: " &
                dtAsignados.Rows.Item(0).Item("idOrdenSer").ToString() & vbCrLf & "Seleccione a otro Empleado", vbInformation, "Aviso" & Me.Text)

            cmbResponsable.Focus()
            Return False
        End If

        If chkAyu1.Checked Then
            dtAsignados.Rows.Clear()
            dtAsignados = ULTOT.tbEmpleadoAsignado(cmbayudante1.SelectedValue, opTipoEmpleado.Ayudante1)
            If dtAsignados.Rows.Count > 0 Then
                MsgBox("El Empleado : " & cmbayudante1.Text & " se encuentra asignado a la Orden de Servicio: " &
                dtAsignados.Rows.Item(0).Item("idOrdenSer").ToString() & vbCrLf & "Seleccione a otro Empleado", vbInformation, "Aviso" & Me.Text)
                cmbayudante1.Focus()
                Return False
            End If
        End If
        If chkAyu2.Checked Then
            dtAsignados.Rows.Clear()
            dtAsignados = ULTOT.tbEmpleadoAsignado(cmbayudante2.SelectedValue, opTipoEmpleado.Ayudante2)
            If dtAsignados.Rows.Count > 0 Then
                MsgBox("El Empleado : " & cmbayudante2.Text & " se encuentra asignado a la Orden de Servicio: " &
                dtAsignados.Rows.Item(0).Item("idOrdenSer").ToString() & vbCrLf & "Seleccione a otro Empleado", vbInformation, "Aviso" & Me.Text)
                cmbayudante2.Focus()
                Return False
            End If
        End If

        Return True
    End Function

    Private Sub cmbResponsable_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbResponsable.SelectedIndexChanged

    End Sub

    Private Sub cmbResponsable_SelectedValueChanged(sender As Object, e As EventArgs) Handles cmbResponsable.SelectedValueChanged

    End Sub

    Private Sub cmbResponsable_KeyUp(sender As Object, e As KeyEventArgs) Handles cmbResponsable.KeyUp
        If e.KeyValue = Keys.Enter Then
            ULTOT = New OrdenTrabajoClass(0)
            dtAsignados.Rows.Clear()
            dtAsignados = ULTOT.tbEmpleadoAsignado(cmbResponsable.SelectedValue, opTipoEmpleado.Responsable)
            If dtAsignados.Rows.Count > 0 Then
                MsgBox("El Empleado : " & cmbResponsable.Text & " se encuentra asignado a la Orden de Servicio: " &
                dtAsignados.Rows.Item(0).Item("idOrdenSer").ToString() & vbCrLf & "Seleccione a otro Empleado", vbInformation, "Aviso" & Me.Text)
                cmbResponsable.Focus()

            End If
        End If
    End Sub



    Private Sub chkMarcaTodasOT_CheckedChanged(sender As Object, e As EventArgs) Handles chkMarcaTodasOT.CheckedChanged
        If grdOrdenServDet.RowCount > 0 Then
            For i = 0 To grdOrdenServDet.RowCount - 1
                If Not grdOrdenServDet.Rows(i).Cells(grdOrdenServDet.CurrentCell.RowIndex).ReadOnly Then
                    grdOrdenServDet.Item("osdDiagnostico", i).Value = IIf(chkMarcaTodasOT.Checked, 1, 0)

                    cmbResponsable.Enabled = True
                    cmbResponsable.SelectedIndex = 0
                    cmbayudante1.SelectedIndex = 0
                    cmbayudante2.SelectedIndex = 0
                    btnMnuOk.Enabled = True
                End If
            Next
        End If
    End Sub

    Private Sub txtOrdenServ_TextChanged(sender As Object, e As EventArgs) Handles txtOrdenServ.TextChanged

    End Sub

    Private Sub txtOrdenServ_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtOrdenServ.KeyPress

    End Sub

    Private Sub txtOrdenServ_KeyUp(sender As Object, e As KeyEventArgs) Handles txtOrdenServ.KeyUp
        If e.KeyValue = Keys.Enter Then
            Salida = True
            CargaForm(txtOrdenServ.Text, "OrdenServicio")
            txtidPadreOrdSer.Enabled = False
            If txtEstatus.Text = "ENT" Or txtEstatus.Text = "PRO" Then
                ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)
                ActivaCampos(False, TipoOpcActivaCampos.tOpcOTRO)
            Else
                ActivaBotones(True, TipoOpcActivaBoton.tOpcConsulta)
                ActivaCampos(False, TipoOpcActivaCampos.tOpcDEPENDEVALOR)
                grdOrdenServCab.Focus()
            End If


            Salida = False
        Else
            Salida = False
        End If
    End Sub

    Private Sub AgregaComboGrid()
        Dim dgccomboCodigo As DataGridViewComboBoxColumn = TryCast(grdOrdenServDet.Columns("osdidopServLlantas"), DataGridViewComboBoxColumn)
        Dim dt As New DataTable
        Dim obj As New Funciones()
        dt = obj.EnumToDataTable(GetType(opServLlantas), "KEY", "VALUE")
        dgccomboCodigo.DataSource = dt
        dgccomboCodigo.DisplayMember = "VALUE"
        dgccomboCodigo.ValueMember = "KEY"



    End Sub

    Private Sub grdOrdenServDet_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles grdOrdenServDet.CellContentClick

    End Sub

    Private Sub cmdBuscaProd_Click(sender As Object, e As EventArgs) Handles cmdBuscaProd.Click
        Salida = True
        txtIdProducto.Text = DespliegaGrid(UCase("PROD_COM"), Inicio.CONSTR_COM, "CCODIGOPRODUCTO")
        If txtIdProducto.Text <> "" Then CargaForm(txtIdProducto.Text, UCase("PROD_COM"))
        CargaForm(txtIdUnidadProd.Text, UCase("UNIPROD_COM"))
        CargaForm(txtIdUnidadProd.Text, UCase("UNIPROD_COM"))

        txtCantidad.Focus()
        txtCantidad.SelectAll()
        Salida = False
    End Sub

    Private Sub txtIdProducto_TextChanged(sender As Object, e As EventArgs) Handles txtIdProducto.TextChanged

    End Sub

    Private Sub txtIdProducto_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtIdProducto.KeyPress
        If txtIdProducto.Text.Length = 0 Then
            txtIdProducto.Focus()
            txtIdProducto.SelectAll()
        Else
            If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
                Salida = True
                CargaForm(txtIdProducto.Text, UCase("PROD_COM"))
                CargaForm(txtIdUnidadProd.Text, UCase("UNIPROD_COM"))

                txtCantidad.Focus()
                txtCantidad.SelectAll()
                Salida = False
            End If
        End If
    End Sub

    Private Sub txtIdProducto_KeyDown(sender As Object, e As KeyEventArgs) Handles txtIdProducto.KeyDown
        If e.KeyCode = Keys.F3 Then
            cmdBuscaProd_Click(sender, e)
        End If
    End Sub

    Private Sub txtIdProducto_Leave(sender As Object, e As EventArgs) Handles txtIdProducto.Leave
        If Salida Then Exit Sub
        If txtIdProducto.Text.Length = 0 Then
            txtIdProducto.Focus()
            txtIdProducto.SelectAll()
        Else
            If txtIdProducto.Enabled Then
                CargaForm(txtIdProducto.Text, UCase("PROD_COM"))
                CargaForm(txtIdUnidadProd.Text, UCase("UNIPROD_COM"))

                txtCantidad.Focus()
                txtCantidad.SelectAll()
            End If
        End If
    End Sub

    Private Sub txtIdUnidadProd_TextChanged(sender As Object, e As EventArgs) Handles txtIdUnidadProd.TextChanged

    End Sub

    Private Sub txtIdUnidadProd_KeyDown(sender As Object, e As KeyEventArgs) Handles txtIdUnidadProd.KeyDown
        If e.KeyCode = Keys.F3 Then
            cmdBuscaUniProd_Click(sender, e)
        End If
    End Sub

    Private Sub txtIdUnidadProd_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtIdUnidadProd.KeyPress
        If txtIdUnidadProd.Text.Length = 0 Then
            txtIdUnidadProd.Focus()
            txtIdUnidadProd.SelectAll()
        Else
            If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
                Salida = True
                CargaForm(txtIdUnidadProd.Text, UCase("UNIPROD_COM"))

                txtCantidad.Focus()
                txtCantidad.SelectAll()
                Salida = False
            End If
        End If
    End Sub

    Private Sub txtIdUnidadProd_Leave(sender As Object, e As EventArgs) Handles txtIdUnidadProd.Leave
        If Salida Then Exit Sub
        If txtIdUnidadProd.Text.Length = 0 Then
            txtIdUnidadProd.Focus()
            txtIdUnidadProd.SelectAll()
        Else
            If txtIdProducto.Enabled Then
                CargaForm(txtIdUnidadProd.Text, UCase("UNIPROD_COM"))
                txtCantidad.Focus()
                txtCantidad.SelectAll()
            End If
        End If
    End Sub

    Private Sub txtIdUnidadProd_EnabledChanged(sender As Object, e As EventArgs) Handles txtIdUnidadProd.EnabledChanged
        cmdBuscaUniProd.Enabled = txtIdUnidadProd.Enabled
    End Sub

    Private Sub cmdBuscaUniProd_Click(sender As Object, e As EventArgs) Handles cmdBuscaUniProd.Click
        Salida = True
        txtIdProducto.Text = DespliegaGrid(UCase("UNIPROD_COM"), Inicio.CONSTR_COM, "CCODIGOPRODUCTO")
        If txtIdProducto.Text <> "" Then CargaForm(txtIdProducto.Text, UCase("PROD_COM"))
        txtCantidad.Focus()
        txtCantidad.SelectAll()
        Salida = False
    End Sub

    Private Sub txtCantidad_TextChanged(sender As Object, e As EventArgs) Handles txtCantidad.TextChanged

    End Sub

    Private Sub txtCantidad_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCantidad.KeyPress
        If txtCantidad.Text.Length = 0 Then
            txtCantidad.Focus()
            txtCantidad.SelectAll()
        Else
            If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
                Salida = True
                'CargaForm(txtIdUnidadProd.Text, UCase("UNIPROD_COM"))
                'Agrega a Grid
                GrabaProductos()
                BorraCamposProd()

                cmbActivRef.Focus()
                'txtIdProducto.Focus()
                'txtIdProducto.SelectAll()
                Salida = False
            End If
        End If
    End Sub

    Private Sub GrabaProductos()
        GrabaGridProductos()
    End Sub

    Private Sub GrabaGridProductos()
        Dim BandGraba As Boolean = False
        Dim vExistencia As Double = 0
        Dim vCosto As Double = 0
        Dim CantCompra As Double = 0
        Dim CantSalida As Double = 0
        Try
            If txtIdProducto.Text <> "" Then
                If (txtCantidad.Text <> CantidadGrid) Or txtCantidad.Text = 0 Then
                    BandGraba = True
                End If

                If BandGraba Then
                    If grdProductosOT.Rows.Count = 0 Then
                        MyTablaRefac.Clear()
                        BandNoExisteProdEnGrid = True
                    End If
                    'Determinar si el producto existe en el grid, si no existe hay que agregarlo
                    If BandNoExisteProdEnGrid Then
                        Salida = True
                        myDataRow = MyTablaRefac.NewRow()
                        '
                        myDataRow("trefCIDPRODUCTO") = vCIDPRODUCTO
                        myDataRow("trefCIDUNIDADBASE") = vCIDUNIDAD

                        myDataRow("trefCCODIGOP01_PRO") = txtIdProducto.Text
                        myDataRow("trefCNOMBREP01") = txtNomProducto.Text

                        'Calcula Existencia
                        vExistencia = ObjCom.Existencia(vCIDPRODUCTO, Now, vIdAlmacen)
                        myDataRow("trefExistencia") = vExistencia

                        myDataRow("trefCantidad") = Val(txtCantidad.Text)
                        myDataRow("trefCCLAVEINT") = txtIdUnidadProd.Text
                        myDataRow("trefBComprar") = IIf(vExistencia < Val(txtCantidad.Text), True, False)

                        'Calcula Costo
                        vCosto = ObjCom.ChecaCosto(vCIDPRODUCTO, Now, vIdAlmacen)
                        myDataRow("trefCOSTO") = vCosto

                        'Cantidades
                        'Por Comprar y Salidas
                        If vExistencia >= Val(txtCantidad.Text) Then
                            CantCompra = 0
                            CantSalida = Val(txtCantidad.Text)
                        Else
                            CantCompra = Val(txtCantidad.Text) - vExistencia
                            CantSalida = vExistencia
                        End If
                        myDataRow("trefCantCompra") = CantCompra
                        myDataRow("trefCantSalida") = CantSalida

                        myDataRow("trefidOrdenTrabajo") = cmbActivRef.SelectedValue

                        MyTablaRefac.Rows.Add(myDataRow)

                        'Grabamos Producto
                        GrabaProductoLOC(txtIdProducto.Text)
                        GrabaUnidadLOC(vCIDUNIDAD)


                        InsertaRegGrid()

                        BandNoExisteProdEnGrid = False
                        Salida = False
                    Else
                        For i = 0 To MyTablaRefac.DefaultView.Count - 1
                            BandNoExisteProdEnGrid = True
                            If (Trim(txtIdProducto.Text) = Trim(MyTablaRefac.DefaultView.Item(i).Item("trefCCODIGOP01_PRO").ToString) _
                                And Trim(txtIdUnidadProd.Text) = Trim(grdProductosOT.Item("grefCCLAVEINT", i).Value)) And
                                (cmbActivRef.SelectedValue = grdProductosOT.Item("grefidOrdenTrabajo", i).Value) Then

                                MyTablaRefac.DefaultView.Item(i).Item("trefCIDPRODUCTO") = vCIDPRODUCTO
                                MyTablaRefac.DefaultView.Item(i).Item("trefCIDUNIDADBASE") = vCIDUNIDAD

                                MyTablaRefac.DefaultView.Item(i).Item("trefCCODIGOP01_PRO") = txtIdProducto.Text
                                MyTablaRefac.DefaultView.Item(i).Item("trefCNOMBREP01") = txtNomProducto.Text
                                'Calcula Existencia
                                vExistencia = ObjCom.Existencia(vCIDPRODUCTO, Now, vIdAlmacen)
                                MyTablaRefac.DefaultView.Item(i).Item("trefExistencia") = vExistencia

                                MyTablaRefac.DefaultView.Item(i).Item("trefCantidad") = Val(txtCantidad.Text)
                                MyTablaRefac.DefaultView.Item(i).Item("trefCCLAVEINT") = txtIdUnidadProd.Text

                                MyTablaRefac.DefaultView.Item(i).Item("trefBComprar") = IIf(vExistencia < Val(txtCantidad.Text), True, False)

                                'Calcula Costo
                                vCosto = ObjCom.ChecaCosto(vCIDPRODUCTO, Now, vIdAlmacen)
                                MyTablaRefac.DefaultView.Item(i).Item("trefCOSTO") = vCosto

                                If vExistencia >= Val(txtCantidad.Text) Then
                                    CantCompra = 0
                                    CantSalida = Val(txtCantidad.Text)
                                Else
                                    CantCompra = Val(txtCantidad.Text) - vExistencia
                                    CantSalida = vExistencia
                                End If
                                MyTablaRefac.DefaultView.Item(i).Item("trefCantCompra") = CantCompra
                                MyTablaRefac.DefaultView.Item(i).Item("trefCantSalida") = CantSalida


                                'Grabamos Producto
                                GrabaProductoLOC(txtIdProducto.Text)
                                GrabaUnidadLOC(vCIDUNIDAD)

                                BandNoExisteProdEnGrid = False
                                Exit For
                            End If
                        Next
                        If BandNoExisteProdEnGrid Then
                            Salida = True
                            myDataRow = MyTablaRefac.NewRow()
                            myDataRow("trefCIDPRODUCTO") = vCIDPRODUCTO
                            myDataRow("trefCIDUNIDADBASE") = vCIDUNIDAD

                            myDataRow("trefCCODIGOP01_PRO") = txtIdProducto.Text
                            myDataRow("trefCNOMBREP01") = txtNomProducto.Text
                            'Calcula Existencia
                            vExistencia = ObjCom.Existencia(vCIDPRODUCTO, Now, vIdAlmacen)
                            myDataRow("trefExistencia") = vExistencia

                            myDataRow("trefCantidad") = Val(txtCantidad.Text)
                            myDataRow("trefCCLAVEINT") = txtIdUnidadProd.Text

                            myDataRow("trefBComprar") = IIf(vExistencia < Val(txtCantidad.Text), True, False)

                            'Calcula Costo
                            vCosto = ObjCom.ChecaCosto(vCIDPRODUCTO, Now, vIdAlmacen)
                            myDataRow("trefCOSTO") = vCosto

                            If vExistencia >= Val(txtCantidad.Text) Then
                                CantCompra = 0
                                CantSalida = Val(txtCantidad.Text)
                            Else
                                CantCompra = Val(txtCantidad.Text) - vExistencia
                                CantSalida = vExistencia
                            End If
                            myDataRow("trefCantCompra") = CantCompra
                            myDataRow("trefCantSalida") = CantSalida

                            myDataRow("trefidOrdenTrabajo") = cmbActivRef.SelectedValue

                            MyTablaRefac.Rows.Add(myDataRow)

                            'Grabamos Producto
                            GrabaProductoLOC(txtIdProducto.Text)
                            GrabaUnidadLOC(vCIDUNIDAD)
                        End If
                        InsertaRegGrid()
                        BandNoExisteProdEnGrid = False
                    End If
                End If
            Else
                txtIdProducto.Enabled = True
            End If
            'ActualizaTotales()
        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, Me.Text)
        End Try
    End Sub

    Private Function InsertaRegGrid() As Boolean
        Try

            grdProductosOT.Rows.Clear()

            For i = 0 To MyTablaRefac.DefaultView.Count - 1
                grdProductosOT.Rows.Add()
                'Consecutivo
                grdProductosOT.Item("grefConsecutivo", i).Value = i + 1
                grdProductosOT.Item("grefCCODIGOP01_PRO", i).Value = Trim(MyTablaRefac.DefaultView.Item(i).Item("trefCCODIGOP01_PRO").ToString)
                grdProductosOT.Item("grefCNOMBREP01", i).Value = Trim(MyTablaRefac.DefaultView.Item(i).Item("trefCNOMBREP01").ToString)
                grdProductosOT.Item("grefExistencia", i).Value = CDbl(MyTablaRefac.DefaultView.Item(i).Item("trefExistencia"))
                grdProductosOT.Item("grefCantidad", i).Value = CDbl(MyTablaRefac.DefaultView.Item(i).Item("trefCantidad"))
                grdProductosOT.Item("grefCCLAVEINT", i).Value = Trim(MyTablaRefac.DefaultView.Item(i).Item("trefCCLAVEINT").ToString)
                grdProductosOT.Item("grefBComprar", i).Value = MyTablaRefac.DefaultView.Item(i).Item("trefBComprar")
                grdProductosOT.Item("grefCOSTO", i).Value = CDbl(MyTablaRefac.DefaultView.Item(i).Item("trefCOSTO"))

                grdProductosOT.Item("grefCIDPRODUCTO", i).Value = CInt(MyTablaRefac.DefaultView.Item(i).Item("trefCIDPRODUCTO"))
                grdProductosOT.Item("grefCIDUNIDADBASE", i).Value = CInt(MyTablaRefac.DefaultView.Item(i).Item("trefCIDUNIDADBASE"))

                grdProductosOT.Item("grefCantCompra", i).Value = CDbl(MyTablaRefac.DefaultView.Item(i).Item("trefCantCompra"))
                grdProductosOT.Item("grefCantSalida", i).Value = CDbl(MyTablaRefac.DefaultView.Item(i).Item("trefCantSalida"))

                grdProductosOT.Item("grefidOrdenTrabajo", i).Value = CInt(MyTablaRefac.DefaultView.Item(i).Item("trefidOrdenTrabajo"))
            Next
            'grdVentas.Columns("Precio").DefaultCellStyle.Format = "c"
            grdProductosOT.PerformLayout()
            'grdVentas.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
            Return True
        Catch ex As Exception
            Return False
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, Me.Text)
        End Try

    End Function
    Private Sub GrabaProductoLOC(ByVal vCCODIGOPRODUCTO As String)
        'Grabamos Producto LOCAL
        ProdCom = New ProductosComClass(vCCODIGOPRODUCTO, "")
        If ProdCom.Existe Then
            'Existe en Comercial
            ProdCom.Guardar(False, vCCODIGOPRODUCTO, txtNomProducto.Text, ProdCom.CTIPOPRODUCTO, ProdCom.CSTATUSPRODUCTO, ProdCom.CIDUNIDADBASE, ProdCom.CCODALTERN)

        End If

    End Sub
    Private Sub GrabaUnidadLOC(ByVal vCIDUNIDAD As Integer)
        'Grabamos Producto LOCAL
        'UnidadCom = New UnidadesComClass(vCCODIGOPRODUC)
        UnidadCom = New UnidadesComClass(vCIDUNIDAD, "CIDUNIDAD", TipoDato.TdNumerico)
        If UnidadCom.Existe Then
            'Existe en Comercial
            UnidadCom.Guardar(False, txtNomUniProd.Text, UnidadCom.CABREVIATURA, UnidadCom.CDESPLIEGUE, UnidadCom.CCLAVEINT)

        End If

    End Sub
    Private Sub BorraCamposProd()
        txtIdProducto.Text = ""
        txtNomProducto.Text = ""
        txtIdUnidadProd.Text = ""
        txtNomUniProd.Text = ""
        txtCantidad.Text = ""

    End Sub

    Private Sub btnAgregaOS_Click(sender As Object, e As EventArgs) Handles btnAgregaOS.Click

        'Tipos de Orden de Servicio Palomiados
        'Dim list = chkBoxTipoOrdServ.CheckedItems

        Dim listObj As List(Of TiposOrdenServicio) = New List(Of TiposOrdenServicio)()
        '    For Each obj In List
        '    listObj.Add(CType(obj, TiposOrdenServicio))
        'Next

        Dim ObjTipOrdServ As New TiposOrdenServicio(0)
        Dim vIdTipo As Integer = 0
        Dim myTabla As DataTable
        Dim vCatalogo As String
        Dim vValorId As Integer
        Dim tablaRes As DataTable
        Dim vUnidadTrans As String = ""
        Dim vNotaRecepcion As String = ""
        Dim vIdDivision As Integer = 0
        Dim vNomDivision As String = ""
        Dim vidServicio As Integer = 0
        Dim vidActividad As Integer = 0
        Dim vDuracionAct As Integer = 0
        Dim vNomServicio As String = ""

        Dim vNewidOrdTrab As Integer

        vNumOrdServ = txtOrdenServ.Text

        vIdTipo = _TipoOrdenServicio

        'oscTipoOrdenServ

        '*****************************************************************************************
        tablaRes = New DataTable()
        tablaRes = IniciaOrdenTrabajo(vIdTipo, txtTipoOrdenServ.Text, vNumOrdServ, False,
        grdOrdenServCab.Item("oscTipoOrdenServ", grdOrdenServCab.CurrentCell.RowIndex).Value,
        grdOrdenServCab.Item("oscidUnidadTrans", grdOrdenServCab.CurrentCell.RowIndex).Value, vIdTipo)

        If Not tablaRes Is Nothing Then
            If tablaRes.Rows.Count > 0 Then
                vUnidadTrans = ""
                indice = 0
                Dim dv As New DataView(tablaRes)
                dv.Sort = "totIdUnidadTransp"
                Dim DTOrdenado As DataTable = dv.ToTable
                For i = 0 To DTOrdenado.Rows.Count - 1
                    If vUnidadTrans = "" And i = 0 Then
                        vUnidadTrans = DTOrdenado.DefaultView.Item(i).Item("totIdUnidadTransp")
                    End If

                    If vUnidadTrans = DTOrdenado.DefaultView.Item(i).Item("totIdUnidadTransp") Then
                        'vUnidadTrans = DTOrdenado.DefaultView.Item(i).Item("totIdUnidadTransp")
                        vNotaRecepcion = DTOrdenado.DefaultView.Item(i).Item("totNotaRecepcion")
                        vIdDivision = DTOrdenado.DefaultView.Item(i).Item("totidDivision")
                        vNomDivision = DTOrdenado.DefaultView.Item(i).Item("totNomDivision")
                        vidServicio = DTOrdenado.DefaultView.Item(i).Item("totidServicio")
                        If vidServicio > 0 Then
                            Servicio = New ServicioClass(vidServicio)
                            If Servicio.Existe Then
                                vNomServicio = Servicio.NomServicio
                            Else
                                vNomServicio = ""
                            End If
                        Else
                            vNomServicio = ""
                        End If

                        vidActividad = DTOrdenado.DefaultView.Item(i).Item("totidActividad")
                        vDuracionAct = DTOrdenado.DefaultView.Item(i).Item("totDuracionActHr")
                        vTipoServ = DTOrdenado.DefaultView.Item(i).Item("totTipoServ")
                        idLlanta = DTOrdenado.DefaultView.Item(i).Item("totidLlanta")
                        PosLLanta = DTOrdenado.DefaultView.Item(i).Item("totPosLlanta")


                        vNewidOrdTrab = UltimoOrdenTrab()

                        StrSql = ObjSql.InsertaOrdenServicioDET(vNumOrdServ, vidServicio,
                        vidActividad, vDuracionAct, 0, 0, "ASIG", 0, 0, vNewidOrdTrab, vIdDivision, idLlanta, PosLLanta)
                        ReDim Preserve ArraySql(indice)
                        ArraySql(indice) = StrSql
                        indice += 1

                        StrSql = ObjSql.AsignaDetOrdenServicio(Now, vNumOrdServ, vNewidOrdTrab, _Usuario)
                        ReDim Preserve ArraySql(indice)
                        ArraySql(indice) = StrSql
                        indice += 1

                        StrSql = ObjSql.InsertaOrdenTrabajo(vNewidOrdTrab, vNumOrdServ, 0, vNotaRecepcion, 0, vIdDivision, cmbResponsable.SelectedValue,
                        IIf(chkAyu1.Checked, cmbayudante1.SelectedValue, 0), IIf(chkAyu2.Checked, cmbayudante2.SelectedValue, 0))
                        ReDim Preserve ArraySql(indice)
                        ArraySql(indice) = StrSql
                        indice += 1

                        'Insertamos al Grid
                        InsertaRegGridDet(True, vNumOrdServ, vidServicio, vNomServicio, vidActividad,
                        vValorNomAct, vIdDivision, vNomDivision, vDuracionAct, txtOrdenServ.Text,
                        vNewidOrdTrab, False, 0,
                        idLlanta, PosLLanta, cmbResponsable.SelectedValue,
                        IIf(chkAyu1.Checked, cmbayudante1.SelectedValue, 0),
                        IIf(chkAyu2.Checked, cmbayudante2.SelectedValue, 0), 0, "REC",
                        1, "1", "1", 1, 1)


                        'If i = DTOrdenado.Rows.Count - 1 Then
                        '    UniTrans = New UnidadesTranspClass(vUnidadTrans)
                        '    TipoUniTrans = New TipoUniTransClass(UniTrans.idTipoUnidad)

                        '    '10/ENE/2017 - KILOMETRAJE CERO 

                        '    InsertaRegGridCab(True, vNumOrdServ, vIdTipo, txtTipoOrdenServ.Text, vUnidadTrans, UniTrans.idTipoUnidad,
                        '            TipoUniTrans.nomTipoUniTras, 0, cmbAutoriza.SelectedValue, 1, vTipoServ, False, False, Now, "DIAG")

                        'End If

                        'Else
                        ''CAMBIO DE UNIDAD
                        'UniTrans = New UnidadesTranspClass(vUnidadTrans)
                        'TipoUniTrans = New TipoUniTransClass(UniTrans.idTipoUnidad)
                        ''vUnidadTrans = DTOrdenado.DefaultView.Item(i).Item("totIdUnidadTransp")
                        ''Insertamos Cabecero Anterior

                        ''10/ENE/2017 -> KILOMETRAJE 0
                        'InsertaRegGridCab(True, vNumOrdServ, vIdTipo, txtTipoOrdenServ.Text, vUnidadTrans, UniTrans.idTipoUnidad,
                        '            TipoUniTrans.nomTipoUniTras, 0, cmbAutoriza.SelectedValue, 1, vTipoServ, False, False, Now, "DIAG")


                        'vNumOrdServ += 1
                        'vUnidadTrans = DTOrdenado.DefaultView.Item(i).Item("totIdUnidadTransp")
                        'vNotaRecepcion = DTOrdenado.DefaultView.Item(i).Item("totNotaRecepcion")
                        'vIdDivision = DTOrdenado.DefaultView.Item(i).Item("totidDivision")
                        'vNomDivision = DTOrdenado.DefaultView.Item(i).Item("totNomDivision")
                        'vidServicio = DTOrdenado.DefaultView.Item(i).Item("totidServicio")
                        'If vidServicio > 0 Then
                        '    Servicio = New ServicioClass(vidServicio)
                        '    If Servicio.Existe Then
                        '        vNomServicio = Servicio.NomServicio
                        '    Else
                        '        vNomServicio = ""
                        '    End If
                        'Else
                        '    vNomServicio = ""
                        'End If
                        'vidActividad = DTOrdenado.DefaultView.Item(i).Item("totidActividad")
                        'vDuracionAct = DTOrdenado.DefaultView.Item(i).Item("totDuracionActHr")
                        'vTipoServ = DTOrdenado.DefaultView.Item(i).Item("totTipoServ")

                        'idLlanta = DTOrdenado.DefaultView.Item(i).Item("totidLlanta")
                        'PosLLanta = DTOrdenado.DefaultView.Item(i).Item("totPosLlanta")

                        ''InsertaRegGridDet(vNumOrdServ, vidServicio, vNotaRecepcion, vidActividad, "", vIdDivision,
                        ''                  vNomDivision, vDuracionAct, 0, 0, False, 0, idLlanta, PosLLanta)

                        ''InsertaRegGridDet(vNumOrdServ, vidServicio, vNomServicio, vidActividad, vNotaRecepcion, vIdDivision,
                        ''                      vNomDivision, vDuracionAct, 0, 0, False, 0, idLlanta, PosLLanta)

                        'InsertaRegGridDet(0, vNumOrdServ, vidServicio, vNomServicio, vidActividad, vValorNomAct,
                        '                vIdDivision, vNomDivision, vDuracionAct, txtOrdenServ.Text, 0, False, 0, idLlanta,
                        '                PosLLanta, 0, 0, 0, 0, "REC")


                        'If i = DTOrdenado.Rows.Count - 1 Then
                        '    UniTrans = New UnidadesTranspClass(vUnidadTrans)
                        '    TipoUniTrans = New TipoUniTransClass(UniTrans.idTipoUnidad)

                        '    InsertaRegGridCab(True, vNumOrdServ, vIdTipo, txtTipoOrdenServ.Text, vUnidadTrans, UniTrans.idTipoUnidad,
                        '            TipoUniTrans.nomTipoUniTras, 0, cmbAutoriza.SelectedValue, 1, vTipoServ, False, False, Now, "DIAG")


                        'End If
                    End If
                Next
                Clave = grdOrdenServCab.Item("oscCLAVE", grdOrdenServCab.CurrentCell.RowIndex).Value
                FiltraDetallexClave(Clave)
            End If
        End If
        If indice > 0 Then
            Dim obj As New CapaNegocio.Tablas
            Dim Respuesta As String
            'Para Impresion
            'Dim sOrdenes As String = ""
            'Dim idOS As Integer = 0
            'Dim TipidOS As Boolean = False

            Respuesta = obj.EjecutarSql(Inicio.CONSTR, "SQL", ArraySql, Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, indice)
            FEspera.Close()
            If Respuesta = "EFECTUADO" Then

                'If ContOS > 1 Then
                '    MsgBox("Se Finalizo Correctamente las Ordenes de Servicio : (" & NumsOs & ")", MsgBoxStyle.Exclamation, Me.Text)
                'ElseIf ContOS = 1 Then
                '    MsgBox("Se Finalizo Correctamente la Orden de Servicio : " & NumsOs, MsgBoxStyle.Exclamation, Me.Text)
                'End If


                'Se debe Imprimir Algo ????
                'If SetupThePrinting() Then
                '    For imp = 0 To ArrayOrdenSer.Length - 1
                '        idOS = ArrayOrdenSer(imp)
                '        TipidOS = ArrayOrdenSerTipo(imp)
                '        ImprimeOrdenServicio(idOS, TipidOS, True, True)
                '    Next
                'End If


            ElseIf Respuesta = "NOEFECTUADO" Then

            ElseIf Respuesta = "ERROR" Then
                indice = 0
                SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")
            End If
        Else
            MsgBox("No se Encontraron Registros que Grabar", MsgBoxStyle.Exclamation, Me.Text)
        End If
        '*****************************************************************************************
        'For Each lobj In listObj
        '    vIdTipo = lobj.idTipoOrden
        '    If lobj.bOrdenTrab Then
        '        'Si tiene que abrir la Forma del Detalle
        '        vNumOrdServ += 1
        '        tablaRes = New DataTable()
        '        tablaRes = IniciaOrdenTrabajo(vIdTipo, lobj.nomTipOrdServ, vNumOrdServ, False, 0, "", vIdTipo)
        '        If Not tablaRes Is Nothing Then
        '            If tablaRes.Rows.Count > 0 Then
        '                vUnidadTrans = ""
        '                Dim dv As New DataView(tablaRes)
        '                dv.Sort = "totIdUnidadTransp"
        '                Dim DTOrdenado As DataTable = dv.ToTable
        '                For i = 0 To DTOrdenado.Rows.Count - 1
        '                    If vUnidadTrans = "" And i = 0 Then
        '                        vUnidadTrans = DTOrdenado.DefaultView.Item(i).Item("totIdUnidadTransp")
        '                    End If

        '                    If vUnidadTrans = DTOrdenado.DefaultView.Item(i).Item("totIdUnidadTransp") Then
        '                        'vUnidadTrans = DTOrdenado.DefaultView.Item(i).Item("totIdUnidadTransp")
        '                        vNotaRecepcion = DTOrdenado.DefaultView.Item(i).Item("totNotaRecepcion")
        '                        vIdDivision = DTOrdenado.DefaultView.Item(i).Item("totidDivision")
        '                        vNomDivision = DTOrdenado.DefaultView.Item(i).Item("totNomDivision")
        '                        vidServicio = DTOrdenado.DefaultView.Item(i).Item("totidServicio")
        '                        If vidServicio > 0 Then
        '                            Servicio = New ServicioClass(vidServicio)
        '                            If Servicio.Existe Then
        '                                vNomServicio = Servicio.NomServicio
        '                            Else
        '                                vNomServicio = ""
        '                            End If
        '                        Else
        '                            vNomServicio = ""
        '                        End If

        '                        vidActividad = DTOrdenado.DefaultView.Item(i).Item("totidActividad")
        '                        vDuracionAct = DTOrdenado.DefaultView.Item(i).Item("totDuracionActHr")
        '                        vTipoServ = DTOrdenado.DefaultView.Item(i).Item("totTipoServ")
        '                        idLlanta = DTOrdenado.DefaultView.Item(i).Item("totidLlanta")
        '                        PosLLanta = DTOrdenado.DefaultView.Item(i).Item("totPosLlanta")

        '                        'InsertaRegGridDet(vNumOrdServ, vidServicio, vNomServicio, vidActividad, vNotaRecepcion,
        '                        '                  vIdDivision,
        '                        '                  vNomDivision, vDuracionAct, 0, 0, False, 0, idLlanta, PosLLanta)

        '                        InsertaRegGridDet(0, vNumOrdServ, vidServicio, vNomServicio, vidActividad, vValorNomAct,
        '                                vIdDivision, vNomDivision, vDuracionAct, txtOrdenServ.Text, 0, False, 0, idLlanta,
        '                                PosLLanta, 0, 0, 0, 0, "REC")

        '                        If i = DTOrdenado.Rows.Count - 1 Then
        '                            UniTrans = New UnidadesTranspClass(vUnidadTrans)
        '                            TipoUniTrans = New TipoUniTransClass(UniTrans.idTipoUnidad)

        '                            '10/ENE/2017 - KILOMETRAJE CERO 

        '                            InsertaRegGridCab(True, vNumOrdServ, vIdTipo, txtTipoOrdenServ.Text, vUnidadTrans, UniTrans.idTipoUnidad,
        '                            TipoUniTrans.nomTipoUniTras, 0, cmbAutoriza.SelectedValue, 1, vTipoServ, False, False, Now, "DIAG")

        '                        End If

        '                    Else
        '                        'CAMBIO DE UNIDAD
        '                        UniTrans = New UnidadesTranspClass(vUnidadTrans)
        '                        TipoUniTrans = New TipoUniTransClass(UniTrans.idTipoUnidad)
        '                        'vUnidadTrans = DTOrdenado.DefaultView.Item(i).Item("totIdUnidadTransp")
        '                        'Insertamos Cabecero Anterior

        '                        '10/ENE/2017 -> KILOMETRAJE 0
        '                        InsertaRegGridCab(True, vNumOrdServ, vIdTipo, txtTipoOrdenServ.Text, vUnidadTrans, UniTrans.idTipoUnidad,
        '                            TipoUniTrans.nomTipoUniTras, 0, cmbAutoriza.SelectedValue, 1, vTipoServ, False, False, Now, "DIAG")


        '                        vNumOrdServ += 1
        '                        vUnidadTrans = DTOrdenado.DefaultView.Item(i).Item("totIdUnidadTransp")
        '                        vNotaRecepcion = DTOrdenado.DefaultView.Item(i).Item("totNotaRecepcion")
        '                        vIdDivision = DTOrdenado.DefaultView.Item(i).Item("totidDivision")
        '                        vNomDivision = DTOrdenado.DefaultView.Item(i).Item("totNomDivision")
        '                        vidServicio = DTOrdenado.DefaultView.Item(i).Item("totidServicio")
        '                        If vidServicio > 0 Then
        '                            Servicio = New ServicioClass(vidServicio)
        '                            If Servicio.Existe Then
        '                                vNomServicio = Servicio.NomServicio
        '                            Else
        '                                vNomServicio = ""
        '                            End If
        '                        Else
        '                            vNomServicio = ""
        '                        End If
        '                        vidActividad = DTOrdenado.DefaultView.Item(i).Item("totidActividad")
        '                        vDuracionAct = DTOrdenado.DefaultView.Item(i).Item("totDuracionActHr")
        '                        vTipoServ = DTOrdenado.DefaultView.Item(i).Item("totTipoServ")

        '                        idLlanta = DTOrdenado.DefaultView.Item(i).Item("totidLlanta")
        '                        PosLLanta = DTOrdenado.DefaultView.Item(i).Item("totPosLlanta")

        '                        'InsertaRegGridDet(vNumOrdServ, vidServicio, vNotaRecepcion, vidActividad, "", vIdDivision,
        '                        '                  vNomDivision, vDuracionAct, 0, 0, False, 0, idLlanta, PosLLanta)

        '                        'InsertaRegGridDet(vNumOrdServ, vidServicio, vNomServicio, vidActividad, vNotaRecepcion, vIdDivision,
        '                        '                      vNomDivision, vDuracionAct, 0, 0, False, 0, idLlanta, PosLLanta)

        '                        InsertaRegGridDet(0, vNumOrdServ, vidServicio, vNomServicio, vidActividad, vValorNomAct,
        '                                vIdDivision, vNomDivision, vDuracionAct, txtOrdenServ.Text, 0, False, 0, idLlanta,
        '                                PosLLanta, 0, 0, 0, 0, "REC")


        '                        If i = DTOrdenado.Rows.Count - 1 Then
        '                            UniTrans = New UnidadesTranspClass(vUnidadTrans)
        '                            TipoUniTrans = New TipoUniTransClass(UniTrans.idTipoUnidad)

        '                            InsertaRegGridCab(True, vNumOrdServ, vIdTipo, txtTipoOrdenServ.Text, vUnidadTrans, UniTrans.idTipoUnidad,
        '                            TipoUniTrans.nomTipoUniTras, 0, cmbAutoriza.SelectedValue, 1, vTipoServ, False, False, Now, "DIAG")


        '                        End If
        '                    End If
        '                Next
        '                Clave = grdOrdenServCab.Item("oscCLAVE", grdOrdenServCab.CurrentCell.RowIndex).Value
        '                FiltraDetallexClave(Clave)
        '            End If
        '        End If

        '    Else
        '        'Checamos Si Existen Relaciones Automaticas para Este Tipo de Orden de Servicio
        '        myTabla = New DataTable
        '        myTabla = ObjTipOrdServ.TablaRelTipOrdServ("rel.idTipOrdServ = " & vIdTipo)
        '        If myTabla.Rows.Count > 0 Then
        '            For i = 0 To myTabla.Rows.Count - 1
        '                vCatalogo = myTabla.DefaultView.Item(i).Item("NomCatalogo").ToString
        '                'vNomCampo = myTabla.DefaultView.Item(i).Item("NombreId").ToString
        '                vValorId = myTabla.DefaultView.Item(i).Item("IdRelacion")
        '                Select Case UCase(vCatalogo)
        '                    Case UCase("catservicios")
        '                        Servicio = New ServicioClass(vValorId)
        '                        If Servicio.Existe Then
        '                            'Recorrer todas las unidades de TRansporte
        '                            For Each txtCTR As Control In Me.GroupBox4.Controls
        '                                If TypeOf txtCTR Is TextBox Then
        '                                    If txtCTR.Tag >= 1 And txtCTR.Tag <= 4 Then
        '                                        'Agregar Orden de Servicio CAB y DET
        '                                        If txtCTR.Text <> "" Then
        '                                            UniTrans = New UnidadesTranspClass(txtCTR.Text)
        '                                            TipoUniTrans = New TipoUniTransClass(UniTrans.idTipoUnidad)
        '                                            If TipoUniTrans.bCombustible Then
        '                                                vNumOrdServ += 1

        '                                                '10/ENE/2017 -> KILOMETRAJE 0
        '                                                InsertaRegGridCab(vNumOrdServ, vIdTipo, lobj.nomTipOrdServ, UniTrans.iUnidadTrans, UniTrans.idTipoUnidad,
        '                                                TipoUniTrans.nomTipoUniTras, 0, cmbAutoriza.SelectedValue, lobj.bOrdenTrab, Servicio.idTipoServicio, False, False)

        '                                                idLlanta = ""
        '                                                PosLLanta = 0

        '                                                InsertaRegGridDet(vNumOrdServ, vValorId, Servicio.NomServicio, 0, "", 0, "", 0, 0, 0, False, 0, idLlanta, PosLLanta)
        '                                            End If
        '                                        End If
        '                                    End If
        '                                End If
        '                            Next
        '                        End If
        '                End Select
        '            Next
        '        Else
        '            'No tiene RElaciones
        '        End If
        '    End If


        'Next
        'End If
    End Sub

    Private Sub CreaTablaProdRefacciones()
        Try
            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Int64")
            myDataColumn.ColumnName = "trefCIDPRODUCTO"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRefac.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Int64")
            myDataColumn.ColumnName = "trefCIDUNIDADBASE"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRefac.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.String")
            myDataColumn.ColumnName = "trefCCODIGOP01_PRO"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRefac.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.String")
            myDataColumn.ColumnName = "trefCNOMBREP01"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRefac.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Double")
            myDataColumn.ColumnName = "trefCOSTO"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRefac.Columns.Add(myDataColumn)


            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.String")
            myDataColumn.ColumnName = "trefCCLAVEINT"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRefac.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Double")
            myDataColumn.ColumnName = "trefExistencia"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRefac.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Double")
            myDataColumn.ColumnName = "trefCantidad"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRefac.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Boolean")
            myDataColumn.ColumnName = "trefBComprar"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRefac.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Double")
            myDataColumn.ColumnName = "trefCantCompra"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRefac.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Double")
            myDataColumn.ColumnName = "trefCantSalida"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRefac.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Int32")
            myDataColumn.ColumnName = "trefidOrdenTrabajo"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRefac.Columns.Add(myDataColumn)

        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try

    End Sub

    Private Sub TabControl1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles TabControl1.SelectedIndexChanged
        If TabControl1.SelectedIndex = 1 Then

            CargaActividades()
        End If
    End Sub

    Private Sub CargaActividades()
        Dim Dt As New DataTable()
        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Int32")
        myDataColumn.ColumnName = "tacidOrdenTrabajo"
        myDataColumn.ReadOnly = False
        myDataColumn.Unique = False
        Dt.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.String")
        myDataColumn.ColumnName = "tacNombreAct"
        myDataColumn.ReadOnly = False
        myDataColumn.Unique = False
        Dt.Columns.Add(myDataColumn)

        For i = 0 To grdOrdenServDet.Rows.Count - 1
            'Clave = grdOrdenServDet.Item("osdidOrdenTrabajo", i).Value
            If grdOrdenServDet.Item("osdDiagnostico", i).Value Then
                myDataRow = Dt.NewRow()
                myDataRow("tacidOrdenTrabajo") = grdOrdenServDet.Item("osdidOrdenTrabajo", i).Value
                myDataRow("tacNombreAct") = grdOrdenServDet.Item("osdNombreAct", i).Value
                Dt.Rows.Add(myDataRow)
            End If
        Next

        If Dt.Rows.Count > 0 Then
            cmbActivRef.DataSource = Dt
            cmbActivRef.DisplayMember = "tacNombreAct"
            cmbActivRef.ValueMember = "tacidOrdenTrabajo"

            cmbActivRef.Focus()

        End If
        'With Control
        '    .DataSource = dt
        '    
        '    

    End Sub

    Private Sub cmbActivRef_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbActivRef.SelectedIndexChanged

    End Sub

    Private Sub cmbActivRef_KeyUp(sender As Object, e As KeyEventArgs) Handles cmbActivRef.KeyUp

    End Sub

    Private Sub cmbActivRef_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbActivRef.KeyPress
        If Asc(e.KeyChar) = 13 Then
            txtIdProducto.Focus()
            txtIdProducto.SelectAll()
        End If
    End Sub

    Private Sub CancelarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CancelarToolStripMenuItem.Click
        Dim fCan As New FCancelarOT(txtOrdenServ.Text,
           grdOrdenServDet.Item("osdidOrdenTrabajo", grdOrdenServDet.CurrentCell.RowIndex).Value)
        If fCan.ShowDialog = Windows.Forms.DialogResult.OK Then
            grdOrdenServDet.Item("osdMotivoCancela", grdOrdenServDet.CurrentCell.RowIndex).Value = fCan.MotivoCancela
            grdOrdenServDet.Item("osdCancela", grdOrdenServDet.CurrentCell.RowIndex).Value = True
        End If
    End Sub

    Private Sub grdOrdenServDet_DoubleClick(sender As Object, e As EventArgs) Handles grdOrdenServDet.DoubleClick

    End Sub

    Private Sub DiagnosticarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DiagnosticarToolStripMenuItem.Click
        Dim fDiag As New FCapDiagnosticoOS(txtOrdenServ.Text,
       grdOrdenServDet.Item("osdidOrdenTrabajo", grdOrdenServDet.CurrentCell.RowIndex).Value,
        grdOrdenServDet.Item("osdNotaDiagnostico", grdOrdenServDet.CurrentCell.RowIndex).Value,
        grdOrdenServDet.Item("osdDuracionActHr", grdOrdenServDet.CurrentCell.RowIndex).Value, txtTipoOrdenServ.Text)

        If fDiag.ShowDialog = Windows.Forms.DialogResult.OK Then
            grdOrdenServDet.Item("osdNotaDiagnostico", grdOrdenServDet.CurrentCell.RowIndex).Value = fDiag.Diagnostico
            grdOrdenServDet.Item("osdDuracionActHr", grdOrdenServDet.CurrentCell.RowIndex).Value = fDiag.DuracionHR
            grdOrdenServDet.Item("osdDiagnostico", grdOrdenServDet.CurrentCell.RowIndex).Value = True
            'servicios
            grdOrdenServDet.Item("osdCIDPRODUCTO", grdOrdenServDet.CurrentCell.RowIndex).Value = fDiag.CIDPRODUCTO_SERV
            grdOrdenServDet.Item("osdCCODIGOPRODUCTO", grdOrdenServDet.CurrentCell.RowIndex).Value = fDiag.CCODIGOPRODUCTO_SERV
            grdOrdenServDet.Item("osdCNOMBREPRODUCTO", grdOrdenServDet.CurrentCell.RowIndex).Value = fDiag.CNOMBREPRODUCTO_SERV
            grdOrdenServDet.Item("osdCantidad_CIDPRODUCTO_SERV", grdOrdenServDet.CurrentCell.RowIndex).Value = fDiag.Cantidad_CIDPRODUCTO_SERV
            grdOrdenServDet.Item("osdPrecio_CIDPRODUCTO_SERV", grdOrdenServDet.CurrentCell.RowIndex).Value = fDiag.Precio_CIDPRODUCTO_SERV

        End If
    End Sub

    Private Sub ActivarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ActivarToolStripMenuItem.Click
        grdOrdenServDet.Item("osdMotivoCancela", grdOrdenServDet.CurrentCell.RowIndex).Value = ""
        grdOrdenServDet.Item("osdCancela", grdOrdenServDet.CurrentCell.RowIndex).Value = False
    End Sub

    Private Sub ConMnuOrdTra_Opening(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles ConMnuOrdTra.Opening

    End Sub

    Private Sub ArmaTablaPreOCTicket(ByVal bOrdenTrab As Boolean)

        Dim campo As Integer
        Dim Descripcion As String
        Dim Valor As String
        Dim PosValorX, PosValorY, PosDescX, PosDescY As Single
        Dim NumAletra As New NumALetras
        Dim EnLetras As String = ""
        Dim Total As Double = 0

        Dim Y1 As Single = 200

        'Vertical
        Dim X1 As Single = 20
        Dim X1A As Single = 80
        Dim X2 As Single = 150

        Dim X1B As Single = X1 + 25

        Dim BandImpServ = False
        Dim XSERV = 20
        Dim XACT = 80

        'Dim X1A As Single = 120
        'Dim X1B As Single = 200
        'Dim X3 As Single = 220
        '
        'Dim X4 As Single = 400

        Dim ValPad As Integer = 12

        DsRep = New DataSet
        If MyTablaImprime.Rows.Count > 0 Then
            MyTablaImprime.Rows.Clear()
        End If


        DsRep = ImpRep.LlenaDsImprime(ImpRep.NomTabla)
        If DsRep.Tables(ImpRep.NomTabla).DefaultView.Count > 0 Then

            For i As Integer = 0 To DsRep.Tables(ImpRep.NomTabla).DefaultView.Count - 1

                'LINEA 1
                campo += 1
                Descripcion = ""
                Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("RazonSocial"))
                PosDescX = X1A : PosDescY = Y1 : PosValorX = PosDescX : PosValorY = PosDescY
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

                'LINEA 2
                campo += 1
                PosDescX = X1A : PosValorX = PosDescX
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                Descripcion = ""
                Valor = "ORDEN DE SERVICIO"
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

                'LINEA 3
                campo += 1
                Descripcion = ""
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                PosDescX = X1A : PosValorX = PosDescX
                Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("NomTipOrdServ"))
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)


                'CODIGO DE BARRAS ORDEN SERVICIO
                campo += 1
                Descripcion = ""
                PosDescY = PosDescY + 30 : PosValorY = PosDescY
                PosDescX = X2 : PosValorX = X2
                'Valor = FormarBarCode(8)
                Valor = FormarBarCode(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idOrdenSer"))
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 2)

                'LINEA 4
                campo += 1
                Descripcion = "ORD.SER."
                PosDescY = PosDescY + 45 : PosValorY = PosDescY
                PosDescX = X2 : PosValorX = PosDescX + 80
                'Valor = FormarBarCode(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idOrdenSer"))
                Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idOrdenSer")
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)



                'LINEA 5
                campo += 1
                Descripcion = "FECHA:"
                Valor = CDate(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("fechaCaptura")).ToShortDateString
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                PosDescX = PosDescX : PosValorX = PosDescX + 50
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)


                'LINEA 5.1
                campo += 1
                Descripcion = "HORA:"
                Valor = CDate(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("fechaCaptura")).ToShortTimeString
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                PosDescX = PosDescX : PosValorX = PosDescX + 40
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)


                'LINEA 6
                campo += 1
                PosDescY = PosDescY + 30 : PosValorY = PosDescY
                Descripcion = "TRACTOR: "
                Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idTractor"))
                PosDescX = X1 : PosValorX = PosDescX + 70
                If DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idTractor") = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idUnidadTrans") Then
                    InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                Else
                    InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)
                End If


                campo += 1
                Descripcion = "REMOLQUE 1:"
                Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idTolva1"))
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                PosDescX = X1 : PosValorX = PosDescX + 95
                If DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idTolva1") = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idUnidadTrans") Then
                    InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                Else
                    InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)
                End If



                'LINEA 7
                campo += 1
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                Descripcion = "DOLLY: "
                Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idDolly"))
                PosDescX = X1 : PosValorX = PosDescX + 50
                If DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idDolly") = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idUnidadTrans") Then
                    InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                Else
                    InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)
                End If


                campo += 1
                Descripcion = "REMOLQUE 2:"
                Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idTolva2"))
                PosDescX = X1 : PosValorX = PosDescX + 95
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                If DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idTolva2") = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idUnidadTrans") Then
                    InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                Else
                    InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)
                End If


                'LINEA 8
                'campo += 1
                'PosDescY = PosDescY + 15 : PosValorY = PosDescY
                'Descripcion = "KILOMETRAJE: "
                'Valor = Format(CInt(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("Kilometraje")), "##,##0")
                'PosDescX = X1 : PosValorX = PosDescX + 110
                'InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)


                'LINEA 9
                campo += 1
                PosDescY = PosDescY + 30 : PosValorY = PosDescY
                Descripcion = ""
                'If bOrdenTrab Then
                '    Valor = "REPARACIONES SOLICITADAS"
                'Else
                '    Valor = "DESCRIPCION DEL SERVICIO"
                'End If
                Valor = "DESCRIPCION DEL SERVICIO"
                PosDescX = X1 : PosValorX = PosDescX + 25
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 5)

                PosDescY = PosDescY + 30 : PosValorY = PosDescY

                'LINEA 10
                'DETALLE
                For j As Integer = 0 To DsRep.Tables(ImpRep.NomTabla).DefaultView.Count - 1
                    If bOrdenTrab Then
                        If DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NomServicio") <> "" Then
                            If Not BandImpServ Then
                                campo += 1
                                Descripcion = ""
                                Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NomServicio")
                                PosDescX = XSERV : PosValorX = PosDescX + 25
                                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                                BandImpServ = True

                            End If
                        Else
                            BandImpServ = False
                            '12/JULIO/2017
                            'If DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NombreAct") <> "" Then
                            '    campo += 1
                            '    PosDescY = PosDescY + 15 : PosValorY = PosDescY
                            '    Descripcion = ""
                            '    Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NombreAct")
                            '    PosDescX = X1 : PosValorX = PosDescX + 25
                            '    InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                            'Else
                            '    'salto de linea
                            '    PosDescY = PosDescY + 15 : PosValorY = PosDescY
                            'End If
                        End If

                        campo += 1
                        'PosDescY = PosDescY + 30 : PosValorY = PosDescY
                        Descripcion = ""
                        Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NotaRecepcion")
                        PosDescY = PosDescY + 15 : PosValorY = PosDescY

                        If BandImpServ Then
                            PosDescX = XACT : PosValorX = PosDescX + 25
                            InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)
                        Else
                            PosDescX = XSERV : PosValorX = PosDescX + 25
                            InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                        End If



                    Else
                        campo += 1

                        PosDescY = PosDescY + 15 : PosValorY = PosDescY
                        Descripcion = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("idUnidadTrans")
                        Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NomServicio")
                        PosDescX = X1 - 35 : PosValorX = X2 + 25
                        InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                        'PosDescY = PosDescY + 15

                    End If
                Next




                'LINEA 11
                campo += 1
                Descripcion = ""
                Valor = "___________________"
                PosDescY = PosDescY + 100 : PosValorY = PosDescY
                PosDescX = X1 : PosValorX = PosDescX + 25
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                'PosDescY = PosDescY + 15

                campo += 1
                Descripcion = ""
                Valor = " ( OPERADOR ) "
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                PosDescX = X1 : PosValorX = PosDescX + 35
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)

                campo += 1
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                Descripcion = ""
                Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("NomOperador"))
                PosDescX = X1 : PosValorX = PosDescX + 25
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 4)



                campo += 1
                Descripcion = ""
                Valor = "___________________"
                PosDescY = PosDescY + 100 : PosValorY = PosDescY
                PosDescX = X1 : PosValorX = PosDescX + 25
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)

                campo += 1
                Descripcion = ""
                Valor = " ( AUTORIZ� ) "
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                PosDescX = X1 : PosValorX = PosDescX + 35
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                'LINEA 13

                campo += 1
                Descripcion = ""
                Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("NomAutoriza"))
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                PosDescX = X1 : PosValorX = PosDescX + 25
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 4)
                'PosDescY = PosDescY + 15

                'LINEA 14
                'CODIGO DE BARRAS ORDEN SERVICIO
                campo += 1
                Descripcion = ""
                'PosDescY = PosDescY + 15 : PosValorY = PosDescY
                PosDescY = PosDescY + 100 : PosValorY = PosDescY
                PosDescX = X2 : PosValorX = X2
                Valor = FormarBarCode(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idPadreOrdSer"))
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 2)

                'LINEA 15
                campo += 1
                Descripcion = "FOLIO: "
                PosDescY = PosDescY + 45 : PosValorY = PosDescY
                PosDescX = X2 : PosValorX = X2 + 60
                Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idPadreOrdSer")
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)

                'LINEA 14
                'If idContrato = 0 Then
                '    '4 LINEAS
                '    PosDescY = PosDescY + 60
                'End If

                'campo += 1
                'Descripcion = "RECIBO NO."
                'PosDescX = X1 : PosDescY = PosDescY + 15 : PosValorX = PosDescX + 100 : PosValorY = PosDescY
                'Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("NUMRECIBO")
                'InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY)
            Next
        End If
    End Sub
    Private Sub ArmaTablaPreSalidaTicket(ByVal bOrdenTrab As Boolean)
        Dim campo As Integer
        Dim Descripcion As String
        Dim Valor As String
        Dim PosValorX, PosValorY, PosDescX, PosDescY As Single
        Dim NumAletra As New NumALetras
        Dim EnLetras As String = ""
        Dim Total As Double = 0

        Dim Y1 As Single = 200

        'Vertical
        Dim X1 As Single = 20
        Dim X1A As Single = 80
        Dim X2 As Single = 150
        Dim X1B As Single = X2 + 25


        Dim XD1 As Single = X1 - 10
        'Dim XD2 As Single = XD1 + 40
        Dim XD2 As Single = XD1 + 35
        'Dim XD3 As Single = XD2 + 60
        Dim XD3 As Single = XD2 + 55





        Dim BandImpServ = False
        Dim XSERV = 20
        Dim XACT = 80

        'Dim X1A As Single = 120
        'Dim X1B As Single = 200
        'Dim X3 As Single = 220
        '
        'Dim X4 As Single = 400

        Dim ValPad As Integer = 12

        DsRep = New DataSet
        If MyTablaImprime.Rows.Count > 0 Then
            MyTablaImprime.Rows.Clear()
        End If


        DsRep = ImpRep.LlenaDsImprime(ImpRep.NomTabla)
        If DsRep.Tables(ImpRep.NomTabla).DefaultView.Count > 0 Then

            For i As Integer = 0 To DsRep.Tables(ImpRep.NomTabla).DefaultView.Count - 1

                'LINEA 1
                campo += 1
                Descripcion = ""
                'Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("RazonSocial"))
                Valor = "ENTREGA DE REFACCIONES"
                PosDescX = X1 : PosDescY = Y1 : PosValorX = PosDescX : PosValorY = PosDescY
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 5)

                'LINEA 2
                'campo += 1
                'PosDescX = X1A : PosValorX = PosDescX
                'PosDescY = PosDescY + 15 : PosValorY = PosDescY
                'Descripcion = ""
                'Valor = "ORDEN DE SERVICIO"
                'InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

                'LINEA 3
                campo += 1
                Descripcion = ""
                PosDescY = PosDescY + 30 : PosValorY = PosDescY
                PosDescX = X1A + 20 : PosValorX = PosDescX
                Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("NomTipOrdServ"))
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)


                'CODIGO DE BARRAS ORDEN SERVICIO
                campo += 1
                Descripcion = ""
                PosDescY = PosDescY + 30 : PosValorY = PosDescY
                PosDescX = X2 : PosValorX = X2
                'Valor = FormarBarCode(8)
                Valor = FormarBarCode(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("IdPreSalida"))
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 2)

                'LINEA 4
                campo += 1
                Descripcion = "SOL.ENT."
                PosDescY = PosDescY + 45 : PosValorY = PosDescY
                PosDescX = X2 : PosValorX = PosDescX + 80
                'Valor = FormarBarCode(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idOrdenSer"))
                Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("IdPreSalida")
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)

                campo += 1
                Descripcion = "ORD.SER."
                PosDescY = PosDescY + 45 : PosValorY = PosDescY
                PosDescX = X2 : PosValorX = PosDescX + 80
                'Valor = FormarBarCode(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idOrdenSer"))
                Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idOrdenSer")
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)

                campo += 1
                Descripcion = "ORD.TRA."
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                PosDescX = X2 : PosValorX = PosDescX + 80
                'Valor = FormarBarCode(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idOrdenSer"))
                Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idOrdenTrabajo")
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)

                'LINEA 5
                campo += 1
                Descripcion = "FECHA:"
                Valor = CDate(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("FEcha")).ToShortDateString
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                PosDescX = PosDescX : PosValorX = PosDescX + 50
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)


                'LINEA 5.1
                campo += 1
                Descripcion = "HORA:"
                Valor = CDate(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("FEcha")).ToShortTimeString
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                PosDescX = PosDescX : PosValorX = PosDescX + 40
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)


                'LINEA 6
                campo += 1
                PosDescY = PosDescY + 30 : PosValorY = PosDescY
                Descripcion = "UNIDAD: "
                Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idUnidadTrans")) & "  " & UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("Clasificacion"))
                PosDescX = X1 : PosValorX = PosDescX + 70
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)




                'LINEA 9
                campo += 1
                PosDescY = PosDescY + 30 : PosValorY = PosDescY
                Descripcion = ""
                'If bOrdenTrab Then
                '    Valor = "REPARACIONES SOLICITADAS"
                'Else
                '    Valor = "DESCRIPCION DEL SERVICIO"
                'End If
                Valor = "DESCRIPCION DEL SERVICIO"
                PosDescX = X1 : PosValorX = PosDescX + 25
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 5)


                campo += 1
                PosDescY = PosDescY + 30 : PosValorY = PosDescY
                Descripcion = ""
                Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("NotaRecepcion"))
                PosDescX = X1 : PosValorX = PosDescX + 25
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

                'LINEA 10
                'DETALLE
                For j As Integer = 0 To DsRep.Tables(ImpRep.NomTabla).DefaultView.Count - 1
                    campo += 1
                    PosDescY = PosDescY + 30 : PosValorY = PosDescY
                    Descripcion = ""
                    Valor = "[" & (Math.Round(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("Cantidad"))).ToString() & "]"
                    PosDescX = XD1 : PosValorX = PosDescX
                    InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)


                    campo += 1
                    Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("CCODIGOPRODUCTO"))
                    PosDescX = XD2 : PosValorX = PosDescX
                    InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

                    campo += 1
                    Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("CNOMBREPRODUCTO"))
                    PosDescX = XD3 : PosValorX = PosDescX
                    InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)


                    'If bOrdenTrab Then
                    '    If DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NomServicio") <> "" Then
                    '        If Not BandImpServ Then
                    '            campo += 1
                    '            Descripcion = ""
                    '            Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NomServicio")
                    '            PosDescX = XSERV : PosValorX = PosDescX + 25
                    '            InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                    '            BandImpServ = True

                    '        End If
                    '    Else
                    '        BandImpServ = False
                    '        '12/JULIO/2017
                    '        'If DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NombreAct") <> "" Then
                    '        '    campo += 1
                    '        '    PosDescY = PosDescY + 15 : PosValorY = PosDescY
                    '        '    Descripcion = ""
                    '        '    Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NombreAct")
                    '        '    PosDescX = X1 : PosValorX = PosDescX + 25
                    '        '    InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                    '        'Else
                    '        '    'salto de linea
                    '        '    PosDescY = PosDescY + 15 : PosValorY = PosDescY
                    '        'End If
                    '    End If

                    '    campo += 1
                    '    'PosDescY = PosDescY + 30 : PosValorY = PosDescY
                    '    Descripcion = ""
                    '    Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NotaRecepcion")
                    '    PosDescY = PosDescY + 15 : PosValorY = PosDescY

                    '    If BandImpServ Then
                    '        PosDescX = XACT : PosValorX = PosDescX + 25
                    '        InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)
                    '    Else
                    '        PosDescX = XSERV : PosValorX = PosDescX + 25
                    '        InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                    '    End If



                    'Else
                    'campo += 1

                    'PosDescY = PosDescY + 15 : PosValorY = PosDescY
                    '    Descripcion = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("idUnidadTrans")
                    '    Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NomServicio")
                    '    PosDescX = X1 - 35 : PosValorX = X2 + 25
                    '    InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                    'PosDescY = PosDescY + 15

                    'End If
                Next




                'LINEA 11
                campo += 1
                Descripcion = ""
                Valor = "___________________"
                PosDescY = PosDescY + 100 : PosValorY = PosDescY
                PosDescX = X1 : PosValorX = PosDescX + 25
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                'PosDescY = PosDescY + 15

                campo += 1
                Descripcion = ""
                Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("PuestoResp"))
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                PosDescX = X1 : PosValorX = PosDescX + 35
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)

                campo += 1
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                Descripcion = ""
                Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("Responsable"))
                PosDescX = X1 : PosValorX = PosDescX + 25
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 4)



                campo += 1
                Descripcion = ""
                Valor = "___________________"
                PosDescY = PosDescY + 100 : PosValorY = PosDescY
                PosDescX = X1 : PosValorX = PosDescX + 25
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)

                campo += 1
                Descripcion = ""
                Valor = " ( AUTORIZ� ) "
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                PosDescX = X1 : PosValorX = PosDescX + 35
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                'LINEA 13

                campo += 1
                Descripcion = ""
                Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("NomAutoriza"))
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                PosDescX = X1 : PosValorX = PosDescX + 25
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 4)
                'PosDescY = PosDescY + 15


                'LINEA 14
                'CODIGO DE BARRAS ORDEN SERVICIO
                campo += 1
                Descripcion = ""
                PosDescY = PosDescY + 100 : PosValorY = PosDescY
                PosDescX = X2 : PosValorX = X2
                Valor = FormarBarCode(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("IdPreSalida"))
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 2)

                'LINEA 15
                campo += 1
                Descripcion = "SOL.ENT.: "
                PosDescY = PosDescY + 45 : PosValorY = PosDescY
                PosDescX = X2 : PosValorX = X2 + 60
                Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("IdPreSalida")
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)

                'CODIGO DE BARRAS ORDEN SERVICIO


            Next
        End If
    End Sub

    Private Sub grdOrdenServCab_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles grdOrdenServCab.CellContentClick

    End Sub
End Class
