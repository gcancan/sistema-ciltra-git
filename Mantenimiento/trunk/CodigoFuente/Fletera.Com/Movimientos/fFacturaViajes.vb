'Fecha: 15 / Febrero / 2016
'ROGER GALA PACHECO
'************************************************************************************************************************************
'*  Fecha Modificacion :   15 / Febrero / 2016                                                                                       
'*  
'************************************************************************************************************************************
Public Class fFacturaViajes
    Inherits System.Windows.Forms.Form
    Private _Tag As String
    Dim bEsIdentidad As Boolean = True
    Dim OpcionForma As TipoOpcionForma
    Dim TablaBd As String = "Diagnostico"

    Dim MyTablaCli As DataTable = New DataTable(TablaBd)
    Dim MyTablaCombo As DataTable = New DataTable(TablaBd)
    Dim MyTablaViaje As DataTable = New DataTable(TablaBd)
    Dim MyTablaDetViaje As DataTable = New DataTable(TablaBd)

    Dim MyTablaPreCabFac As DataTable = New DataTable(TablaBd)
    Dim MyTablaConcepFac As DataTable = New DataTable(TablaBd)
    Dim MyTablaPreDetFac As DataTable = New DataTable(TablaBd)

    'TABLAS para Facturar en Comercial
    Dim MyTablaConcepCOM As DataTable = New DataTable(TablaBd)
    Private ClientesCOM As ClientesComClass
    Private ConceptosCOM As ConceptosCOMClass

    'Private ProdCom As ProductosComClass
    'Private UnidadCom As UnidadesComClass

    Private ObjSql As New ToolSQLs

    Dim Salida As Boolean
    Dim myDataColumn As DataColumn
    Dim myDataRow As DataRow

    Dim CantidadGrid As Double = 0
    Dim BandNoExisteProdEnGrid As Boolean = False

    Dim ObjCom As New FuncionesComClass

    Dim vCIDPRODUCTO As Integer
    Dim vCIDUNIDAD As Integer

    Dim vIdAlmacen As Integer = 1
    Dim IdNoGuia As Integer
    'Dim idFraccion As Integer

    Dim CadCam As String = ""
    Dim StrSql As String = ""

    Dim indice As Integer = 0
    Dim ArraySql() As String

    Dim indiceCOM As Integer = 0
    Dim ArraySqlCOM() As String


    Dim NoIdPreOC As Integer = 0
    Dim NoIdPreSal As Integer = 0

    Dim objNumSig As New CapaNegocio.Tablas

    Dim ContPersonal As Integer = 0
    Dim CostoManoObra As Double = 0
    Dim CostoRef As Double = 0

    Dim vLugOcupado As Integer = 0

    Friend WithEvents cmbClientes As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents gpoRemision As System.Windows.Forms.GroupBox
    Friend WithEvents txtRemision As System.Windows.Forms.TextBox
    Friend WithEvents txtRemFin As System.Windows.Forms.TextBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents txtRemIni As System.Windows.Forms.TextBox
    Friend WithEvents gpoRuta As System.Windows.Forms.GroupBox
    Friend WithEvents cmbRutas As System.Windows.Forms.ComboBox
    Friend WithEvents rdbRutSelec As System.Windows.Forms.RadioButton
    Friend WithEvents rdbRutTodos As System.Windows.Forms.RadioButton
    Friend WithEvents cmdBuscaRuta As System.Windows.Forms.Button
    Friend WithEvents txtRuta As System.Windows.Forms.TextBox
    Friend WithEvents gpoTipoUni As System.Windows.Forms.GroupBox
    Friend WithEvents rdbTUFull As System.Windows.Forms.RadioButton
    Friend WithEvents rdbTUSenc As System.Windows.Forms.RadioButton
    Friend WithEvents rdbTUTodos As System.Windows.Forms.RadioButton
    Friend WithEvents cmbRemisiones As System.Windows.Forms.ComboBox
    Friend WithEvents rdbRemSelec As System.Windows.Forms.RadioButton
    Friend WithEvents rdbRemRango As System.Windows.Forms.RadioButton
    Friend WithEvents rdbRemTodos As System.Windows.Forms.RadioButton
    Friend WithEvents cmdBuscaRem As System.Windows.Forms.Button
    Friend WithEvents btnMnuGenerar As System.Windows.Forms.ToolStripButton
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtNomEmpresa As System.Windows.Forms.TextBox
    Friend WithEvents gpoFecha As System.Windows.Forms.GroupBox
    Friend WithEvents dtpFechaFin As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dtpFechaIni As System.Windows.Forms.DateTimePicker
    Friend WithEvents rdbFecRango As System.Windows.Forms.RadioButton
    Friend WithEvents rdbFecTodos As System.Windows.Forms.RadioButton
    Friend WithEvents GpoMetFact As System.Windows.Forms.GroupBox
    Friend WithEvents rdbMFDetalle As System.Windows.Forms.RadioButton
    Friend WithEvents rdbMFGlobal As System.Windows.Forms.RadioButton
    Friend WithEvents rdbMFAgrupado As System.Windows.Forms.RadioButton
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents chkMarcaFacturas As System.Windows.Forms.CheckBox
    Friend WithEvents grdViajesDet As System.Windows.Forms.DataGridView
    Friend WithEvents grdViajes As System.Windows.Forms.DataGridView
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents grdDetFactura As System.Windows.Forms.DataGridView
    Friend WithEvents grdCabFactura As System.Windows.Forms.DataGridView
    Friend WithEvents txtTOTALVia As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtImpIVAVia As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtImpRetVia As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtSubTotVia As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnMnuPreFac As System.Windows.Forms.ToolStripButton
    Friend WithEvents GpoTexto As System.Windows.Forms.GroupBox
    Friend WithEvents chkITTon As System.Windows.Forms.CheckBox
    Friend WithEvents chkITFecha As System.Windows.Forms.CheckBox
    Friend WithEvents txtTexto As System.Windows.Forms.TextBox
    Friend WithEvents chkITKm As System.Windows.Forms.CheckBox
    Friend WithEvents cmdInsTexto As System.Windows.Forms.Button
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents cmbDocumCOM As System.Windows.Forms.ComboBox
    '
    'Dim CampoLLave As String = "idHerramienta"
    'Dim TablaBd2 As String = "CatFamilia"
    'Dim TablaBd3 As String = "CatTipoHerramienta"
    'Dim TablaBd4 As String = "CatMarcas"

    'Private Herramienta As HerramientaClass
    'Private Familia As FamiliasClass
    'Private TipoHerramienta As TipoHerramClass
    'Private Marca As MarcaClass

    Private _NoServ As Integer

    'PARA FACTURAR
    Dim TipDocumento As Integer = 0
    Dim BandCreditoConcepto As Boolean = False
    Dim FechaVencimiento As Date = Now
    Dim FechaFactura As Date = Now
    Dim DiasCredito As Integer
    Dim CliRFC As String

    Dim tipoDocto As Integer
    Dim TipDocumentoID As Integer
    Dim TextoDocumento As String = ""

    Dim TIPO_MOV As String = ""
    'Dim OrigenFolioSiguiente As String = ""
    Dim vSerie As String = ""
    Dim vFolio As Double = 0
   
    Dim lPlantilla As String = ""
    Dim lRutaEntDocs As String = ""
    Dim lPrefijo As String = ""
    Friend WithEvents gvdNumGuiaId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gvdNoRemision As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gvdDescripcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gvdDestino As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gvdVolDescarga As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gvdPrecio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gvdsubTotal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gvdRetencion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gvdIVA As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gpoTipoDoc As System.Windows.Forms.GroupBox
    Friend WithEvents rdbDocPen As System.Windows.Forms.RadioButton
    Friend WithEvents rdbDocVia As System.Windows.Forms.RadioButton
    Friend WithEvents gvFacturar As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents gvConsecutivo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gvNumGuiaId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gvNoViaje As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gvidTractor As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gvidRemolque1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gvidDolly As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gvidRemolque2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gvFecha As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gvTipoViaje As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gvidCliente As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gvFraccion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gvCantidad As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gvPrecio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gvImporte As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gfdCantidad As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gfdCCODIGOPRODUCTO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gfdCIDPRODUCTO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gfdCNOMBREPRODUCTO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gfdPrecioUnit As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gfdSubtotal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gfdRetencion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gfdIVA As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gfdTotal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gfdTextoComp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gfdKm As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gfdTipoViaje As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gfdPorcIVA As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gfdPorcRET As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gfConsecutivo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gfidCliente As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gfNomCliente As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gfFecha As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gfFechaVenc As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gfSubtotal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gfRetencion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gfIVA As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gfTOTAL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gfKm As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gfCantidad As System.Windows.Forms.DataGridViewTextBoxColumn
    Dim NomArchivo As String = ""
    Private _cveEmpresa As Integer

    'Especial
    Dim vSerieAdmPAQ As String
    Dim vFolioAdmPAQ As Integer

    Dim vSerieAdmPAQVer As String
    Dim vFolioAdmPAQVer As Integer

    Dim Existe As String
    Dim OrigenFolioSiguiente As String = ""

    'Private Cliente As ClientesClass
    'Dim vCambiaPrecio As Boolean

#Region " Código generado por el Diseñador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Diseñador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicialización después de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Diseñador de Windows Forms. 
    'No lo modifique con el editor de código.
    Public WithEvents MeStatus1 As ControlStatus.MeStatus
    Friend WithEvents btnMnuOk As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuCancelar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuSalir As System.Windows.Forms.ToolStripButton
    Friend WithEvents GrupoCaptura As System.Windows.Forms.GroupBox
    Friend WithEvents ToolStripMenu As System.Windows.Forms.ToolStrip
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(fFacturaViajes))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle23 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle24 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle19 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle20 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle21 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle22 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle25 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle32 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle33 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle26 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle27 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle28 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle29 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle30 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle31 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.GrupoCaptura = New System.Windows.Forms.GroupBox()
        Me.gpoTipoDoc = New System.Windows.Forms.GroupBox()
        Me.rdbDocPen = New System.Windows.Forms.RadioButton()
        Me.rdbDocVia = New System.Windows.Forms.RadioButton()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.cmbDocumCOM = New System.Windows.Forms.ComboBox()
        Me.GpoTexto = New System.Windows.Forms.GroupBox()
        Me.chkITKm = New System.Windows.Forms.CheckBox()
        Me.cmdInsTexto = New System.Windows.Forms.Button()
        Me.chkITFecha = New System.Windows.Forms.CheckBox()
        Me.txtTexto = New System.Windows.Forms.TextBox()
        Me.chkITTon = New System.Windows.Forms.CheckBox()
        Me.GpoMetFact = New System.Windows.Forms.GroupBox()
        Me.rdbMFDetalle = New System.Windows.Forms.RadioButton()
        Me.rdbMFGlobal = New System.Windows.Forms.RadioButton()
        Me.rdbMFAgrupado = New System.Windows.Forms.RadioButton()
        Me.gpoFecha = New System.Windows.Forms.GroupBox()
        Me.dtpFechaFin = New System.Windows.Forms.DateTimePicker()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dtpFechaIni = New System.Windows.Forms.DateTimePicker()
        Me.rdbFecRango = New System.Windows.Forms.RadioButton()
        Me.rdbFecTodos = New System.Windows.Forms.RadioButton()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtNomEmpresa = New System.Windows.Forms.TextBox()
        Me.gpoRuta = New System.Windows.Forms.GroupBox()
        Me.cmbRutas = New System.Windows.Forms.ComboBox()
        Me.rdbRutSelec = New System.Windows.Forms.RadioButton()
        Me.rdbRutTodos = New System.Windows.Forms.RadioButton()
        Me.cmdBuscaRuta = New System.Windows.Forms.Button()
        Me.txtRuta = New System.Windows.Forms.TextBox()
        Me.gpoTipoUni = New System.Windows.Forms.GroupBox()
        Me.rdbTUFull = New System.Windows.Forms.RadioButton()
        Me.rdbTUSenc = New System.Windows.Forms.RadioButton()
        Me.rdbTUTodos = New System.Windows.Forms.RadioButton()
        Me.gpoRemision = New System.Windows.Forms.GroupBox()
        Me.cmbRemisiones = New System.Windows.Forms.ComboBox()
        Me.rdbRemSelec = New System.Windows.Forms.RadioButton()
        Me.rdbRemRango = New System.Windows.Forms.RadioButton()
        Me.rdbRemTodos = New System.Windows.Forms.RadioButton()
        Me.cmdBuscaRem = New System.Windows.Forms.Button()
        Me.txtRemision = New System.Windows.Forms.TextBox()
        Me.txtRemFin = New System.Windows.Forms.TextBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.txtRemIni = New System.Windows.Forms.TextBox()
        Me.cmbClientes = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.ToolStripMenu = New System.Windows.Forms.ToolStrip()
        Me.btnMnuOk = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuPreFac = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuGenerar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuCancelar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuSalir = New System.Windows.Forms.ToolStripButton()
        Me.MeStatus1 = New ControlStatus.MeStatus()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.txtTOTALVia = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtImpIVAVia = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtImpRetVia = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtSubTotVia = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.chkMarcaFacturas = New System.Windows.Forms.CheckBox()
        Me.grdViajesDet = New System.Windows.Forms.DataGridView()
        Me.gvdNumGuiaId = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gvdNoRemision = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gvdDescripcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gvdDestino = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gvdVolDescarga = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gvdPrecio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gvdsubTotal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gvdRetencion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gvdIVA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.grdViajes = New System.Windows.Forms.DataGridView()
        Me.gvFacturar = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.gvConsecutivo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gvNumGuiaId = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gvNoViaje = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gvidTractor = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gvidRemolque1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gvidDolly = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gvidRemolque2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gvFecha = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gvTipoViaje = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gvidCliente = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gvFraccion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gvCantidad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gvPrecio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gvImporte = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.grdDetFactura = New System.Windows.Forms.DataGridView()
        Me.gfdCantidad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gfdCCODIGOPRODUCTO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gfdCIDPRODUCTO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gfdCNOMBREPRODUCTO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gfdPrecioUnit = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gfdSubtotal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gfdRetencion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gfdIVA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gfdTotal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gfdTextoComp = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gfdKm = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gfdTipoViaje = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gfdPorcIVA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gfdPorcRET = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.grdCabFactura = New System.Windows.Forms.DataGridView()
        Me.gfConsecutivo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gfidCliente = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gfNomCliente = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gfFecha = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gfFechaVenc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gfSubtotal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gfRetencion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gfIVA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gfTOTAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gfKm = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gfCantidad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GrupoCaptura.SuspendLayout()
        Me.gpoTipoDoc.SuspendLayout()
        Me.GpoTexto.SuspendLayout()
        Me.GpoMetFact.SuspendLayout()
        Me.gpoFecha.SuspendLayout()
        Me.gpoRuta.SuspendLayout()
        Me.gpoTipoUni.SuspendLayout()
        Me.gpoRemision.SuspendLayout()
        Me.ToolStripMenu.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.grdViajesDet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdViajes, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        CType(Me.grdDetFactura, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdCabFactura, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GrupoCaptura
        '
        Me.GrupoCaptura.Controls.Add(Me.gpoTipoDoc)
        Me.GrupoCaptura.Controls.Add(Me.Label7)
        Me.GrupoCaptura.Controls.Add(Me.cmbDocumCOM)
        Me.GrupoCaptura.Controls.Add(Me.GpoTexto)
        Me.GrupoCaptura.Controls.Add(Me.GpoMetFact)
        Me.GrupoCaptura.Controls.Add(Me.gpoFecha)
        Me.GrupoCaptura.Controls.Add(Me.Label9)
        Me.GrupoCaptura.Controls.Add(Me.txtNomEmpresa)
        Me.GrupoCaptura.Controls.Add(Me.gpoRuta)
        Me.GrupoCaptura.Controls.Add(Me.gpoTipoUni)
        Me.GrupoCaptura.Controls.Add(Me.gpoRemision)
        Me.GrupoCaptura.Controls.Add(Me.cmbClientes)
        Me.GrupoCaptura.Controls.Add(Me.Label5)
        Me.GrupoCaptura.Location = New System.Drawing.Point(12, 45)
        Me.GrupoCaptura.Name = "GrupoCaptura"
        Me.GrupoCaptura.Size = New System.Drawing.Size(1141, 213)
        Me.GrupoCaptura.TabIndex = 3
        Me.GrupoCaptura.TabStop = False
        Me.GrupoCaptura.Text = "Filtros"
        '
        'gpoTipoDoc
        '
        Me.gpoTipoDoc.Controls.Add(Me.rdbDocPen)
        Me.gpoTipoDoc.Controls.Add(Me.rdbDocVia)
        Me.gpoTipoDoc.Location = New System.Drawing.Point(12, 101)
        Me.gpoTipoDoc.Name = "gpoTipoDoc"
        Me.gpoTipoDoc.Size = New System.Drawing.Size(179, 55)
        Me.gpoTipoDoc.TabIndex = 89
        Me.gpoTipoDoc.TabStop = False
        Me.gpoTipoDoc.Text = "Documentos"
        '
        'rdbDocPen
        '
        Me.rdbDocPen.AutoSize = True
        Me.rdbDocPen.ForeColor = System.Drawing.Color.Red
        Me.rdbDocPen.Location = New System.Drawing.Point(68, 22)
        Me.rdbDocPen.Name = "rdbDocPen"
        Me.rdbDocPen.Size = New System.Drawing.Size(96, 17)
        Me.rdbDocPen.TabIndex = 70
        Me.rdbDocPen.Text = "Penalizaciones"
        Me.rdbDocPen.UseVisualStyleBackColor = True
        '
        'rdbDocVia
        '
        Me.rdbDocVia.AutoSize = True
        Me.rdbDocVia.Checked = True
        Me.rdbDocVia.ForeColor = System.Drawing.Color.Red
        Me.rdbDocVia.Location = New System.Drawing.Point(9, 22)
        Me.rdbDocVia.Name = "rdbDocVia"
        Me.rdbDocVia.Size = New System.Drawing.Size(53, 17)
        Me.rdbDocVia.TabIndex = 68
        Me.rdbDocVia.TabStop = True
        Me.rdbDocVia.Text = "Viajes"
        Me.rdbDocVia.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label7.Location = New System.Drawing.Point(770, 22)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(65, 20)
        Me.Label7.TabIndex = 88
        Me.Label7.Text = "Documento:"
        '
        'cmbDocumCOM
        '
        Me.cmbDocumCOM.AutoCompleteCustomSource.AddRange(New String() {"Regularización", "Vivienda"})
        Me.cmbDocumCOM.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbDocumCOM.FormattingEnabled = True
        Me.cmbDocumCOM.Location = New System.Drawing.Point(841, 19)
        Me.cmbDocumCOM.Name = "cmbDocumCOM"
        Me.cmbDocumCOM.Size = New System.Drawing.Size(293, 21)
        Me.cmbDocumCOM.TabIndex = 87
        '
        'GpoTexto
        '
        Me.GpoTexto.Controls.Add(Me.chkITKm)
        Me.GpoTexto.Controls.Add(Me.cmdInsTexto)
        Me.GpoTexto.Controls.Add(Me.chkITFecha)
        Me.GpoTexto.Controls.Add(Me.txtTexto)
        Me.GpoTexto.Controls.Add(Me.chkITTon)
        Me.GpoTexto.Location = New System.Drawing.Point(463, 109)
        Me.GpoTexto.Name = "GpoTexto"
        Me.GpoTexto.Size = New System.Drawing.Size(672, 92)
        Me.GpoTexto.TabIndex = 86
        Me.GpoTexto.TabStop = False
        Me.GpoTexto.Text = "Insertar Texto"
        '
        'chkITKm
        '
        Me.chkITKm.AutoSize = True
        Me.chkITKm.BackColor = System.Drawing.SystemColors.Control
        Me.chkITKm.ForeColor = System.Drawing.Color.Blue
        Me.chkITKm.Location = New System.Drawing.Point(141, 24)
        Me.chkITKm.Name = "chkITKm"
        Me.chkITKm.Size = New System.Drawing.Size(42, 17)
        Me.chkITKm.TabIndex = 76
        Me.chkITKm.Text = "KM"
        Me.chkITKm.UseVisualStyleBackColor = False
        '
        'cmdInsTexto
        '
        Me.cmdInsTexto.BackColor = System.Drawing.SystemColors.Control
        Me.cmdInsTexto.ForeColor = System.Drawing.Color.Blue
        Me.cmdInsTexto.Location = New System.Drawing.Point(6, 45)
        Me.cmdInsTexto.Name = "cmdInsTexto"
        Me.cmdInsTexto.Size = New System.Drawing.Size(57, 23)
        Me.cmdInsTexto.TabIndex = 75
        Me.cmdInsTexto.Text = "Insertar"
        Me.cmdInsTexto.UseVisualStyleBackColor = False
        '
        'chkITFecha
        '
        Me.chkITFecha.AutoSize = True
        Me.chkITFecha.BackColor = System.Drawing.SystemColors.Control
        Me.chkITFecha.ForeColor = System.Drawing.Color.Blue
        Me.chkITFecha.Location = New System.Drawing.Point(6, 24)
        Me.chkITFecha.Name = "chkITFecha"
        Me.chkITFecha.Size = New System.Drawing.Size(56, 17)
        Me.chkITFecha.TabIndex = 73
        Me.chkITFecha.Text = "Fecha"
        Me.chkITFecha.UseVisualStyleBackColor = False
        '
        'txtTexto
        '
        Me.txtTexto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTexto.Location = New System.Drawing.Point(189, 14)
        Me.txtTexto.Multiline = True
        Me.txtTexto.Name = "txtTexto"
        Me.txtTexto.Size = New System.Drawing.Size(477, 58)
        Me.txtTexto.TabIndex = 72
        '
        'chkITTon
        '
        Me.chkITTon.AutoSize = True
        Me.chkITTon.BackColor = System.Drawing.SystemColors.Control
        Me.chkITTon.ForeColor = System.Drawing.Color.Blue
        Me.chkITTon.Location = New System.Drawing.Point(68, 24)
        Me.chkITTon.Name = "chkITTon"
        Me.chkITTon.Size = New System.Drawing.Size(67, 17)
        Me.chkITTon.TabIndex = 74
        Me.chkITTon.Text = "Tonelaje"
        Me.chkITTon.UseVisualStyleBackColor = False
        '
        'GpoMetFact
        '
        Me.GpoMetFact.Controls.Add(Me.rdbMFDetalle)
        Me.GpoMetFact.Controls.Add(Me.rdbMFGlobal)
        Me.GpoMetFact.Controls.Add(Me.rdbMFAgrupado)
        Me.GpoMetFact.Location = New System.Drawing.Point(850, 53)
        Me.GpoMetFact.Name = "GpoMetFact"
        Me.GpoMetFact.Size = New System.Drawing.Size(284, 50)
        Me.GpoMetFact.TabIndex = 85
        Me.GpoMetFact.TabStop = False
        Me.GpoMetFact.Text = "Metodo Facturacion"
        '
        'rdbMFDetalle
        '
        Me.rdbMFDetalle.AutoSize = True
        Me.rdbMFDetalle.ForeColor = System.Drawing.Color.Red
        Me.rdbMFDetalle.Location = New System.Drawing.Point(154, 22)
        Me.rdbMFDetalle.Name = "rdbMFDetalle"
        Me.rdbMFDetalle.Size = New System.Drawing.Size(58, 17)
        Me.rdbMFDetalle.TabIndex = 71
        Me.rdbMFDetalle.Text = "Detalle"
        Me.rdbMFDetalle.UseVisualStyleBackColor = True
        '
        'rdbMFGlobal
        '
        Me.rdbMFGlobal.AutoSize = True
        Me.rdbMFGlobal.ForeColor = System.Drawing.Color.Red
        Me.rdbMFGlobal.Location = New System.Drawing.Point(93, 22)
        Me.rdbMFGlobal.Name = "rdbMFGlobal"
        Me.rdbMFGlobal.Size = New System.Drawing.Size(55, 17)
        Me.rdbMFGlobal.TabIndex = 70
        Me.rdbMFGlobal.Text = "Global"
        Me.rdbMFGlobal.UseVisualStyleBackColor = True
        '
        'rdbMFAgrupado
        '
        Me.rdbMFAgrupado.AutoSize = True
        Me.rdbMFAgrupado.Checked = True
        Me.rdbMFAgrupado.ForeColor = System.Drawing.Color.Red
        Me.rdbMFAgrupado.Location = New System.Drawing.Point(9, 22)
        Me.rdbMFAgrupado.Name = "rdbMFAgrupado"
        Me.rdbMFAgrupado.Size = New System.Drawing.Size(71, 17)
        Me.rdbMFAgrupado.TabIndex = 68
        Me.rdbMFAgrupado.TabStop = True
        Me.rdbMFAgrupado.Text = "Agrupado"
        Me.rdbMFAgrupado.UseVisualStyleBackColor = True
        '
        'gpoFecha
        '
        Me.gpoFecha.Controls.Add(Me.dtpFechaFin)
        Me.gpoFecha.Controls.Add(Me.Label1)
        Me.gpoFecha.Controls.Add(Me.dtpFechaIni)
        Me.gpoFecha.Controls.Add(Me.rdbFecRango)
        Me.gpoFecha.Controls.Add(Me.rdbFecTodos)
        Me.gpoFecha.Location = New System.Drawing.Point(12, 42)
        Me.gpoFecha.Name = "gpoFecha"
        Me.gpoFecha.Size = New System.Drawing.Size(827, 51)
        Me.gpoFecha.TabIndex = 77
        Me.gpoFecha.TabStop = False
        Me.gpoFecha.Text = "Fecha"
        '
        'dtpFechaFin
        '
        Me.dtpFechaFin.Location = New System.Drawing.Point(412, 18)
        Me.dtpFechaFin.Name = "dtpFechaFin"
        Me.dtpFechaFin.Size = New System.Drawing.Size(225, 20)
        Me.dtpFechaFin.TabIndex = 85
        Me.dtpFechaFin.Value = New Date(2016, 4, 27, 0, 0, 0, 0)
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label1.Location = New System.Drawing.Point(386, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(20, 13)
        Me.Label1.TabIndex = 84
        Me.Label1.Text = "AL"
        '
        'dtpFechaIni
        '
        Me.dtpFechaIni.Location = New System.Drawing.Point(146, 18)
        Me.dtpFechaIni.Name = "dtpFechaIni"
        Me.dtpFechaIni.Size = New System.Drawing.Size(225, 20)
        Me.dtpFechaIni.TabIndex = 83
        Me.dtpFechaIni.Value = New Date(2016, 4, 27, 0, 0, 0, 0)
        '
        'rdbFecRango
        '
        Me.rdbFecRango.AutoSize = True
        Me.rdbFecRango.ForeColor = System.Drawing.Color.Red
        Me.rdbFecRango.Location = New System.Drawing.Point(83, 22)
        Me.rdbFecRango.Name = "rdbFecRango"
        Me.rdbFecRango.Size = New System.Drawing.Size(57, 17)
        Me.rdbFecRango.TabIndex = 69
        Me.rdbFecRango.Text = "Rango"
        Me.rdbFecRango.UseVisualStyleBackColor = True
        '
        'rdbFecTodos
        '
        Me.rdbFecTodos.AutoSize = True
        Me.rdbFecTodos.Checked = True
        Me.rdbFecTodos.ForeColor = System.Drawing.Color.Red
        Me.rdbFecTodos.Location = New System.Drawing.Point(9, 22)
        Me.rdbFecTodos.Name = "rdbFecTodos"
        Me.rdbFecTodos.Size = New System.Drawing.Size(55, 17)
        Me.rdbFecTodos.TabIndex = 68
        Me.rdbFecTodos.TabStop = True
        Me.rdbFecTodos.Text = "Todos"
        Me.rdbFecTodos.UseVisualStyleBackColor = True
        '
        'Label9
        '
        Me.Label9.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label9.Location = New System.Drawing.Point(460, 20)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(54, 20)
        Me.Label9.TabIndex = 84
        Me.Label9.Text = "Empresa:"
        '
        'txtNomEmpresa
        '
        Me.txtNomEmpresa.Enabled = False
        Me.txtNomEmpresa.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNomEmpresa.Location = New System.Drawing.Point(524, 20)
        Me.txtNomEmpresa.Name = "txtNomEmpresa"
        Me.txtNomEmpresa.ReadOnly = True
        Me.txtNomEmpresa.Size = New System.Drawing.Size(173, 20)
        Me.txtNomEmpresa.TabIndex = 83
        '
        'gpoRuta
        '
        Me.gpoRuta.Controls.Add(Me.cmbRutas)
        Me.gpoRuta.Controls.Add(Me.rdbRutSelec)
        Me.gpoRuta.Controls.Add(Me.rdbRutTodos)
        Me.gpoRuta.Controls.Add(Me.cmdBuscaRuta)
        Me.gpoRuta.Controls.Add(Me.txtRuta)
        Me.gpoRuta.Location = New System.Drawing.Point(12, 162)
        Me.gpoRuta.Name = "gpoRuta"
        Me.gpoRuta.Size = New System.Drawing.Size(76, 55)
        Me.gpoRuta.TabIndex = 78
        Me.gpoRuta.TabStop = False
        Me.gpoRuta.Text = "Destino"
        Me.gpoRuta.Visible = False
        '
        'cmbRutas
        '
        Me.cmbRutas.AutoCompleteCustomSource.AddRange(New String() {"Regularización", "Vivienda"})
        Me.cmbRutas.FormattingEnabled = True
        Me.cmbRutas.Location = New System.Drawing.Point(295, 21)
        Me.cmbRutas.Name = "cmbRutas"
        Me.cmbRutas.Size = New System.Drawing.Size(293, 21)
        Me.cmbRutas.TabIndex = 71
        '
        'rdbRutSelec
        '
        Me.rdbRutSelec.AutoSize = True
        Me.rdbRutSelec.ForeColor = System.Drawing.Color.Red
        Me.rdbRutSelec.Location = New System.Drawing.Point(82, 22)
        Me.rdbRutSelec.Name = "rdbRutSelec"
        Me.rdbRutSelec.Size = New System.Drawing.Size(81, 17)
        Me.rdbRutSelec.TabIndex = 70
        Me.rdbRutSelec.Text = "Seleccionar"
        Me.rdbRutSelec.UseVisualStyleBackColor = True
        '
        'rdbRutTodos
        '
        Me.rdbRutTodos.AutoSize = True
        Me.rdbRutTodos.Checked = True
        Me.rdbRutTodos.ForeColor = System.Drawing.Color.Red
        Me.rdbRutTodos.Location = New System.Drawing.Point(9, 22)
        Me.rdbRutTodos.Name = "rdbRutTodos"
        Me.rdbRutTodos.Size = New System.Drawing.Size(55, 17)
        Me.rdbRutTodos.TabIndex = 68
        Me.rdbRutTodos.TabStop = True
        Me.rdbRutTodos.Text = "Todos"
        Me.rdbRutTodos.UseVisualStyleBackColor = True
        '
        'cmdBuscaRuta
        '
        Me.cmdBuscaRuta.Image = CType(resources.GetObject("cmdBuscaRuta.Image"), System.Drawing.Image)
        Me.cmdBuscaRuta.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdBuscaRuta.Location = New System.Drawing.Point(247, 15)
        Me.cmdBuscaRuta.Name = "cmdBuscaRuta"
        Me.cmdBuscaRuta.Size = New System.Drawing.Size(32, 32)
        Me.cmdBuscaRuta.TabIndex = 67
        Me.cmdBuscaRuta.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'txtRuta
        '
        Me.txtRuta.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRuta.ForeColor = System.Drawing.Color.Red
        Me.txtRuta.Location = New System.Drawing.Point(169, 19)
        Me.txtRuta.MaxLength = 3
        Me.txtRuta.Name = "txtRuta"
        Me.txtRuta.Size = New System.Drawing.Size(69, 20)
        Me.txtRuta.TabIndex = 15
        Me.txtRuta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'gpoTipoUni
        '
        Me.gpoTipoUni.Controls.Add(Me.rdbTUFull)
        Me.gpoTipoUni.Controls.Add(Me.rdbTUSenc)
        Me.gpoTipoUni.Controls.Add(Me.rdbTUTodos)
        Me.gpoTipoUni.Location = New System.Drawing.Point(197, 101)
        Me.gpoTipoUni.Name = "gpoTipoUni"
        Me.gpoTipoUni.Size = New System.Drawing.Size(227, 55)
        Me.gpoTipoUni.TabIndex = 77
        Me.gpoTipoUni.TabStop = False
        Me.gpoTipoUni.Text = "Tipo Viaje"
        '
        'rdbTUFull
        '
        Me.rdbTUFull.AutoSize = True
        Me.rdbTUFull.ForeColor = System.Drawing.Color.Red
        Me.rdbTUFull.Location = New System.Drawing.Point(165, 22)
        Me.rdbTUFull.Name = "rdbTUFull"
        Me.rdbTUFull.Size = New System.Drawing.Size(51, 17)
        Me.rdbTUFull.TabIndex = 71
        Me.rdbTUFull.Text = "FULL"
        Me.rdbTUFull.UseVisualStyleBackColor = True
        '
        'rdbTUSenc
        '
        Me.rdbTUSenc.AutoSize = True
        Me.rdbTUSenc.ForeColor = System.Drawing.Color.Red
        Me.rdbTUSenc.Location = New System.Drawing.Point(82, 22)
        Me.rdbTUSenc.Name = "rdbTUSenc"
        Me.rdbTUSenc.Size = New System.Drawing.Size(62, 17)
        Me.rdbTUSenc.TabIndex = 70
        Me.rdbTUSenc.Text = "Sencillo"
        Me.rdbTUSenc.UseVisualStyleBackColor = True
        '
        'rdbTUTodos
        '
        Me.rdbTUTodos.AutoSize = True
        Me.rdbTUTodos.Checked = True
        Me.rdbTUTodos.ForeColor = System.Drawing.Color.Red
        Me.rdbTUTodos.Location = New System.Drawing.Point(9, 22)
        Me.rdbTUTodos.Name = "rdbTUTodos"
        Me.rdbTUTodos.Size = New System.Drawing.Size(55, 17)
        Me.rdbTUTodos.TabIndex = 68
        Me.rdbTUTodos.TabStop = True
        Me.rdbTUTodos.Text = "Todos"
        Me.rdbTUTodos.UseVisualStyleBackColor = True
        '
        'gpoRemision
        '
        Me.gpoRemision.Controls.Add(Me.cmbRemisiones)
        Me.gpoRemision.Controls.Add(Me.rdbRemSelec)
        Me.gpoRemision.Controls.Add(Me.rdbRemRango)
        Me.gpoRemision.Controls.Add(Me.rdbRemTodos)
        Me.gpoRemision.Controls.Add(Me.cmdBuscaRem)
        Me.gpoRemision.Controls.Add(Me.txtRemision)
        Me.gpoRemision.Controls.Add(Me.txtRemFin)
        Me.gpoRemision.Controls.Add(Me.Label23)
        Me.gpoRemision.Controls.Add(Me.txtRemIni)
        Me.gpoRemision.Location = New System.Drawing.Point(95, 168)
        Me.gpoRemision.Name = "gpoRemision"
        Me.gpoRemision.Size = New System.Drawing.Size(96, 41)
        Me.gpoRemision.TabIndex = 76
        Me.gpoRemision.TabStop = False
        Me.gpoRemision.Text = "Remisión"
        Me.gpoRemision.Visible = False
        '
        'cmbRemisiones
        '
        Me.cmbRemisiones.AutoCompleteCustomSource.AddRange(New String() {"Regularización", "Vivienda"})
        Me.cmbRemisiones.FormattingEnabled = True
        Me.cmbRemisiones.Location = New System.Drawing.Point(640, 21)
        Me.cmbRemisiones.Name = "cmbRemisiones"
        Me.cmbRemisiones.Size = New System.Drawing.Size(116, 21)
        Me.cmbRemisiones.TabIndex = 71
        '
        'rdbRemSelec
        '
        Me.rdbRemSelec.AutoSize = True
        Me.rdbRemSelec.ForeColor = System.Drawing.Color.Red
        Me.rdbRemSelec.Location = New System.Drawing.Point(427, 22)
        Me.rdbRemSelec.Name = "rdbRemSelec"
        Me.rdbRemSelec.Size = New System.Drawing.Size(81, 17)
        Me.rdbRemSelec.TabIndex = 70
        Me.rdbRemSelec.Text = "Seleccionar"
        Me.rdbRemSelec.UseVisualStyleBackColor = True
        '
        'rdbRemRango
        '
        Me.rdbRemRango.AutoSize = True
        Me.rdbRemRango.ForeColor = System.Drawing.Color.Red
        Me.rdbRemRango.Location = New System.Drawing.Point(83, 22)
        Me.rdbRemRango.Name = "rdbRemRango"
        Me.rdbRemRango.Size = New System.Drawing.Size(57, 17)
        Me.rdbRemRango.TabIndex = 69
        Me.rdbRemRango.Text = "Rango"
        Me.rdbRemRango.UseVisualStyleBackColor = True
        '
        'rdbRemTodos
        '
        Me.rdbRemTodos.AutoSize = True
        Me.rdbRemTodos.Checked = True
        Me.rdbRemTodos.ForeColor = System.Drawing.Color.Red
        Me.rdbRemTodos.Location = New System.Drawing.Point(9, 22)
        Me.rdbRemTodos.Name = "rdbRemTodos"
        Me.rdbRemTodos.Size = New System.Drawing.Size(55, 17)
        Me.rdbRemTodos.TabIndex = 68
        Me.rdbRemTodos.TabStop = True
        Me.rdbRemTodos.Text = "Todos"
        Me.rdbRemTodos.UseVisualStyleBackColor = True
        '
        'cmdBuscaRem
        '
        Me.cmdBuscaRem.Image = CType(resources.GetObject("cmdBuscaRem.Image"), System.Drawing.Image)
        Me.cmdBuscaRem.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdBuscaRem.Location = New System.Drawing.Point(592, 15)
        Me.cmdBuscaRem.Name = "cmdBuscaRem"
        Me.cmdBuscaRem.Size = New System.Drawing.Size(32, 32)
        Me.cmdBuscaRem.TabIndex = 67
        Me.cmdBuscaRem.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'txtRemision
        '
        Me.txtRemision.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRemision.ForeColor = System.Drawing.Color.Red
        Me.txtRemision.Location = New System.Drawing.Point(514, 19)
        Me.txtRemision.MaxLength = 3
        Me.txtRemision.Name = "txtRemision"
        Me.txtRemision.Size = New System.Drawing.Size(69, 20)
        Me.txtRemision.TabIndex = 15
        Me.txtRemision.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtRemFin
        '
        Me.txtRemFin.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRemFin.Location = New System.Drawing.Point(309, 21)
        Me.txtRemFin.MaxLength = 50
        Me.txtRemFin.Name = "txtRemFin"
        Me.txtRemFin.Size = New System.Drawing.Size(101, 20)
        Me.txtRemFin.TabIndex = 19
        '
        'Label23
        '
        Me.Label23.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label23.Location = New System.Drawing.Point(280, 19)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(23, 20)
        Me.Label23.TabIndex = 45
        Me.Label23.Text = "AL"
        '
        'txtRemIni
        '
        Me.txtRemIni.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRemIni.Location = New System.Drawing.Point(170, 19)
        Me.txtRemIni.MaxLength = 50
        Me.txtRemIni.Name = "txtRemIni"
        Me.txtRemIni.Size = New System.Drawing.Size(101, 20)
        Me.txtRemIni.TabIndex = 18
        '
        'cmbClientes
        '
        Me.cmbClientes.AutoCompleteCustomSource.AddRange(New String() {"Regularización", "Vivienda"})
        Me.cmbClientes.FormattingEnabled = True
        Me.cmbClientes.Location = New System.Drawing.Point(56, 17)
        Me.cmbClientes.Name = "cmbClientes"
        Me.cmbClientes.Size = New System.Drawing.Size(402, 21)
        Me.cmbClientes.TabIndex = 67
        '
        'Label5
        '
        Me.Label5.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label5.Location = New System.Drawing.Point(9, 20)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(79, 20)
        Me.Label5.TabIndex = 66
        Me.Label5.Text = "Cliente:"
        '
        'ToolStripMenu
        '
        Me.ToolStripMenu.AutoSize = False
        Me.ToolStripMenu.BackColor = System.Drawing.Color.Silver
        Me.ToolStripMenu.Dock = System.Windows.Forms.DockStyle.None
        Me.ToolStripMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnMnuOk, Me.btnMnuPreFac, Me.btnMnuGenerar, Me.btnMnuCancelar, Me.btnMnuSalir})
        Me.ToolStripMenu.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow
        Me.ToolStripMenu.Location = New System.Drawing.Point(3, 0)
        Me.ToolStripMenu.Name = "ToolStripMenu"
        Me.ToolStripMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.ToolStripMenu.Size = New System.Drawing.Size(1194, 42)
        Me.ToolStripMenu.TabIndex = 1
        Me.ToolStripMenu.Text = "ToolStrip1"
        '
        'btnMnuOk
        '
        Me.btnMnuOk.ForeColor = System.Drawing.Color.Red
        Me.btnMnuOk.Image = Global.Fletera.My.Resources.Resources.Save1
        Me.btnMnuOk.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuOk.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuOk.Name = "btnMnuOk"
        Me.btnMnuOk.Size = New System.Drawing.Size(70, 39)
        Me.btnMnuOk.Text = "&FACTURAR"
        Me.btnMnuOk.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuPreFac
        '
        Me.btnMnuPreFac.ForeColor = System.Drawing.Color.Red
        Me.btnMnuPreFac.Image = Global.Fletera.My.Resources.Resources._1463173999_document_preview
        Me.btnMnuPreFac.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuPreFac.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuPreFac.Name = "btnMnuPreFac"
        Me.btnMnuPreFac.Size = New System.Drawing.Size(86, 39)
        Me.btnMnuPreFac.Text = "PRE FACTURA"
        Me.btnMnuPreFac.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuGenerar
        '
        Me.btnMnuGenerar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuGenerar.Image = Global.Fletera.My.Resources.Resources._1462930414_filter_data
        Me.btnMnuGenerar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuGenerar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuGenerar.Name = "btnMnuGenerar"
        Me.btnMnuGenerar.Size = New System.Drawing.Size(62, 39)
        Me.btnMnuGenerar.Text = "GENERAR"
        Me.btnMnuGenerar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuCancelar
        '
        Me.btnMnuCancelar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuCancelar.Image = Global.Fletera.My.Resources.Resources.signos_f_059
        Me.btnMnuCancelar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuCancelar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuCancelar.Name = "btnMnuCancelar"
        Me.btnMnuCancelar.Size = New System.Drawing.Size(71, 39)
        Me.btnMnuCancelar.Text = "&CANCELAR"
        Me.btnMnuCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuSalir
        '
        Me.btnMnuSalir.ForeColor = System.Drawing.Color.Red
        Me.btnMnuSalir.Image = Global.Fletera.My.Resources.Resources.Salida2
        Me.btnMnuSalir.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuSalir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuSalir.Name = "btnMnuSalir"
        Me.btnMnuSalir.Size = New System.Drawing.Size(41, 39)
        Me.btnMnuSalir.Text = "&SALIR"
        Me.btnMnuSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'MeStatus1
        '
        Me.MeStatus1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.MeStatus1.Location = New System.Drawing.Point(0, 596)
        Me.MeStatus1.Margin = New System.Windows.Forms.Padding(4)
        Me.MeStatus1.Name = "MeStatus1"
        Me.MeStatus1.Size = New System.Drawing.Size(1169, 26)
        Me.MeStatus1.TabIndex = 72
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Location = New System.Drawing.Point(12, 260)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(1145, 337)
        Me.TabControl1.TabIndex = 75
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.txtTOTALVia)
        Me.TabPage1.Controls.Add(Me.Label6)
        Me.TabPage1.Controls.Add(Me.txtImpIVAVia)
        Me.TabPage1.Controls.Add(Me.Label4)
        Me.TabPage1.Controls.Add(Me.txtImpRetVia)
        Me.TabPage1.Controls.Add(Me.Label3)
        Me.TabPage1.Controls.Add(Me.txtSubTotVia)
        Me.TabPage1.Controls.Add(Me.Label2)
        Me.TabPage1.Controls.Add(Me.chkMarcaFacturas)
        Me.TabPage1.Controls.Add(Me.grdViajesDet)
        Me.TabPage1.Controls.Add(Me.grdViajes)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(1137, 311)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Viajes"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'txtTOTALVia
        '
        Me.txtTOTALVia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTOTALVia.Location = New System.Drawing.Point(832, 264)
        Me.txtTOTALVia.MaxLength = 50
        Me.txtTOTALVia.Name = "txtTOTALVia"
        Me.txtTOTALVia.Size = New System.Drawing.Size(101, 20)
        Me.txtTOTALVia.TabIndex = 84
        Me.txtTOTALVia.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label6.Location = New System.Drawing.Point(770, 267)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(42, 13)
        Me.Label6.TabIndex = 85
        Me.Label6.Text = "TOTAL"
        '
        'txtImpIVAVia
        '
        Me.txtImpIVAVia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtImpIVAVia.Location = New System.Drawing.Point(832, 238)
        Me.txtImpIVAVia.MaxLength = 50
        Me.txtImpIVAVia.Name = "txtImpIVAVia"
        Me.txtImpIVAVia.Size = New System.Drawing.Size(101, 20)
        Me.txtImpIVAVia.TabIndex = 82
        Me.txtImpIVAVia.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label4.Location = New System.Drawing.Point(770, 241)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(33, 13)
        Me.Label4.TabIndex = 83
        Me.Label4.Text = "I.V.A."
        '
        'txtImpRetVia
        '
        Me.txtImpRetVia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtImpRetVia.Location = New System.Drawing.Point(832, 212)
        Me.txtImpRetVia.MaxLength = 50
        Me.txtImpRetVia.Name = "txtImpRetVia"
        Me.txtImpRetVia.Size = New System.Drawing.Size(101, 20)
        Me.txtImpRetVia.TabIndex = 80
        Me.txtImpRetVia.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label3.Location = New System.Drawing.Point(770, 215)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(56, 13)
        Me.Label3.TabIndex = 81
        Me.Label3.Text = "Retencion"
        '
        'txtSubTotVia
        '
        Me.txtSubTotVia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSubTotVia.Location = New System.Drawing.Point(832, 186)
        Me.txtSubTotVia.MaxLength = 50
        Me.txtSubTotVia.Name = "txtSubTotVia"
        Me.txtSubTotVia.Size = New System.Drawing.Size(101, 20)
        Me.txtSubTotVia.TabIndex = 78
        Me.txtSubTotVia.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label2.Location = New System.Drawing.Point(770, 189)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(46, 13)
        Me.Label2.TabIndex = 79
        Me.Label2.Text = "Subtotal"
        '
        'chkMarcaFacturas
        '
        Me.chkMarcaFacturas.AutoSize = True
        Me.chkMarcaFacturas.Location = New System.Drawing.Point(32, 6)
        Me.chkMarcaFacturas.Name = "chkMarcaFacturas"
        Me.chkMarcaFacturas.Size = New System.Drawing.Size(92, 17)
        Me.chkMarcaFacturas.TabIndex = 77
        Me.chkMarcaFacturas.Text = "Marcar Todas"
        Me.chkMarcaFacturas.UseVisualStyleBackColor = True
        '
        'grdViajesDet
        '
        Me.grdViajesDet.AllowUserToAddRows = False
        Me.grdViajesDet.AllowUserToResizeColumns = False
        Me.grdViajesDet.AllowUserToResizeRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Yellow
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black
        Me.grdViajesDet.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.grdViajesDet.BackgroundColor = System.Drawing.Color.White
        Me.grdViajesDet.ColumnHeadersHeight = 34
        Me.grdViajesDet.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.grdViajesDet.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.gvdNumGuiaId, Me.gvdNoRemision, Me.gvdDescripcion, Me.gvdDestino, Me.gvdVolDescarga, Me.gvdPrecio, Me.gvdsubTotal, Me.gvdRetencion, Me.gvdIVA})
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.Yellow
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.grdViajesDet.DefaultCellStyle = DataGridViewCellStyle5
        Me.grdViajesDet.Location = New System.Drawing.Point(8, 180)
        Me.grdViajesDet.Name = "grdViajesDet"
        Me.grdViajesDet.RowHeadersWidth = 26
        DataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.Yellow
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black
        Me.grdViajesDet.RowsDefaultCellStyle = DataGridViewCellStyle6
        Me.grdViajesDet.Size = New System.Drawing.Size(756, 114)
        Me.grdViajesDet.TabIndex = 76
        '
        'gvdNumGuiaId
        '
        Me.gvdNumGuiaId.HeaderText = "NumGuiaId"
        Me.gvdNumGuiaId.Name = "gvdNumGuiaId"
        Me.gvdNumGuiaId.Visible = False
        '
        'gvdNoRemision
        '
        Me.gvdNoRemision.HeaderText = "Remision"
        Me.gvdNoRemision.Name = "gvdNoRemision"
        '
        'gvdDescripcion
        '
        Me.gvdDescripcion.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.gvdDescripcion.HeaderText = "Descripcion"
        Me.gvdDescripcion.Name = "gvdDescripcion"
        Me.gvdDescripcion.Width = 88
        '
        'gvdDestino
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.gvdDestino.DefaultCellStyle = DataGridViewCellStyle2
        Me.gvdDestino.HeaderText = "Ruta"
        Me.gvdDestino.Name = "gvdDestino"
        Me.gvdDestino.Width = 200
        '
        'gvdVolDescarga
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.gvdVolDescarga.DefaultCellStyle = DataGridViewCellStyle3
        Me.gvdVolDescarga.HeaderText = "Volumen"
        Me.gvdVolDescarga.Name = "gvdVolDescarga"
        '
        'gvdPrecio
        '
        Me.gvdPrecio.HeaderText = "Precio"
        Me.gvdPrecio.Name = "gvdPrecio"
        '
        'gvdsubTotal
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.gvdsubTotal.DefaultCellStyle = DataGridViewCellStyle4
        Me.gvdsubTotal.HeaderText = "Importe"
        Me.gvdsubTotal.Name = "gvdsubTotal"
        Me.gvdsubTotal.Width = 60
        '
        'gvdRetencion
        '
        Me.gvdRetencion.HeaderText = "Retencion"
        Me.gvdRetencion.Name = "gvdRetencion"
        Me.gvdRetencion.Visible = False
        '
        'gvdIVA
        '
        Me.gvdIVA.HeaderText = "I.V.A"
        Me.gvdIVA.Name = "gvdIVA"
        Me.gvdIVA.Visible = False
        '
        'grdViajes
        '
        Me.grdViajes.AllowUserToAddRows = False
        Me.grdViajes.AllowUserToResizeColumns = False
        Me.grdViajes.AllowUserToResizeRows = False
        DataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        DataGridViewCellStyle7.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.Yellow
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black
        Me.grdViajes.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle7
        Me.grdViajes.BackgroundColor = System.Drawing.Color.White
        Me.grdViajes.ColumnHeadersHeight = 34
        Me.grdViajes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.grdViajes.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.gvFacturar, Me.gvConsecutivo, Me.gvNumGuiaId, Me.gvNoViaje, Me.gvidTractor, Me.gvidRemolque1, Me.gvidDolly, Me.gvidRemolque2, Me.gvFecha, Me.gvTipoViaje, Me.gvidCliente, Me.gvFraccion, Me.gvCantidad, Me.gvPrecio, Me.gvImporte})
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle14.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle14.SelectionBackColor = System.Drawing.Color.Yellow
        DataGridViewCellStyle14.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.grdViajes.DefaultCellStyle = DataGridViewCellStyle14
        Me.grdViajes.Location = New System.Drawing.Point(6, 24)
        Me.grdViajes.Name = "grdViajes"
        Me.grdViajes.RowHeadersWidth = 26
        DataGridViewCellStyle15.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle15.SelectionBackColor = System.Drawing.Color.Yellow
        DataGridViewCellStyle15.SelectionForeColor = System.Drawing.Color.Black
        Me.grdViajes.RowsDefaultCellStyle = DataGridViewCellStyle15
        Me.grdViajes.Size = New System.Drawing.Size(1119, 150)
        Me.grdViajes.TabIndex = 75
        '
        'gvFacturar
        '
        Me.gvFacturar.HeaderText = "Facturar"
        Me.gvFacturar.Name = "gvFacturar"
        '
        'gvConsecutivo
        '
        Me.gvConsecutivo.HeaderText = "#"
        Me.gvConsecutivo.Name = "gvConsecutivo"
        Me.gvConsecutivo.Width = 30
        '
        'gvNumGuiaId
        '
        Me.gvNumGuiaId.HeaderText = "NumGuiaId"
        Me.gvNumGuiaId.Name = "gvNumGuiaId"
        Me.gvNumGuiaId.Visible = False
        '
        'gvNoViaje
        '
        Me.gvNoViaje.HeaderText = "No. Viaje"
        Me.gvNoViaje.Name = "gvNoViaje"
        '
        'gvidTractor
        '
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.gvidTractor.DefaultCellStyle = DataGridViewCellStyle8
        Me.gvidTractor.HeaderText = "Tractor"
        Me.gvidTractor.Name = "gvidTractor"
        Me.gvidTractor.Width = 60
        '
        'gvidRemolque1
        '
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.gvidRemolque1.DefaultCellStyle = DataGridViewCellStyle9
        Me.gvidRemolque1.HeaderText = "Remolque 1"
        Me.gvidRemolque1.Name = "gvidRemolque1"
        '
        'gvidDolly
        '
        Me.gvidDolly.HeaderText = "Dolly"
        Me.gvidDolly.Name = "gvidDolly"
        '
        'gvidRemolque2
        '
        Me.gvidRemolque2.HeaderText = "Remolque 2"
        Me.gvidRemolque2.Name = "gvidRemolque2"
        Me.gvidRemolque2.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.gvidRemolque2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'gvFecha
        '
        Me.gvFecha.HeaderText = "Fecha"
        Me.gvFecha.Name = "gvFecha"
        Me.gvFecha.Width = 150
        '
        'gvTipoViaje
        '
        Me.gvTipoViaje.HeaderText = "Tipo Viaje"
        Me.gvTipoViaje.Name = "gvTipoViaje"
        '
        'gvidCliente
        '
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.gvidCliente.DefaultCellStyle = DataGridViewCellStyle10
        Me.gvidCliente.HeaderText = "Cliente"
        Me.gvidCliente.Name = "gvidCliente"
        Me.gvidCliente.Visible = False
        Me.gvidCliente.Width = 60
        '
        'gvFraccion
        '
        Me.gvFraccion.HeaderText = "Fraccion"
        Me.gvFraccion.Name = "gvFraccion"
        Me.gvFraccion.Visible = False
        '
        'gvCantidad
        '
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle11.Format = "N2"
        DataGridViewCellStyle11.NullValue = Nothing
        Me.gvCantidad.DefaultCellStyle = DataGridViewCellStyle11
        Me.gvCantidad.HeaderText = "Cantidad"
        Me.gvCantidad.Name = "gvCantidad"
        Me.gvCantidad.Width = 80
        '
        'gvPrecio
        '
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle12.Format = "C2"
        DataGridViewCellStyle12.NullValue = Nothing
        Me.gvPrecio.DefaultCellStyle = DataGridViewCellStyle12
        Me.gvPrecio.HeaderText = "Precio"
        Me.gvPrecio.Name = "gvPrecio"
        '
        'gvImporte
        '
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle13.Format = "C2"
        DataGridViewCellStyle13.NullValue = Nothing
        Me.gvImporte.DefaultCellStyle = DataGridViewCellStyle13
        Me.gvImporte.HeaderText = "Importe"
        Me.gvImporte.Name = "gvImporte"
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.grdDetFactura)
        Me.TabPage2.Controls.Add(Me.grdCabFactura)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(1137, 311)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Facturas"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'grdDetFactura
        '
        Me.grdDetFactura.AllowUserToAddRows = False
        Me.grdDetFactura.AllowUserToResizeColumns = False
        Me.grdDetFactura.AllowUserToResizeRows = False
        DataGridViewCellStyle16.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        DataGridViewCellStyle16.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle16.SelectionBackColor = System.Drawing.Color.Yellow
        DataGridViewCellStyle16.SelectionForeColor = System.Drawing.Color.Black
        Me.grdDetFactura.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle16
        Me.grdDetFactura.BackgroundColor = System.Drawing.Color.White
        Me.grdDetFactura.ColumnHeadersHeight = 34
        Me.grdDetFactura.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.grdDetFactura.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.gfdCantidad, Me.gfdCCODIGOPRODUCTO, Me.gfdCIDPRODUCTO, Me.gfdCNOMBREPRODUCTO, Me.gfdPrecioUnit, Me.gfdSubtotal, Me.gfdRetencion, Me.gfdIVA, Me.gfdTotal, Me.gfdTextoComp, Me.gfdKm, Me.gfdTipoViaje, Me.gfdPorcIVA, Me.gfdPorcRET})
        DataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle23.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle23.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle23.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle23.SelectionBackColor = System.Drawing.Color.Yellow
        DataGridViewCellStyle23.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.grdDetFactura.DefaultCellStyle = DataGridViewCellStyle23
        Me.grdDetFactura.Location = New System.Drawing.Point(8, 103)
        Me.grdDetFactura.Name = "grdDetFactura"
        Me.grdDetFactura.RowHeadersWidth = 26
        DataGridViewCellStyle24.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle24.SelectionBackColor = System.Drawing.Color.Yellow
        DataGridViewCellStyle24.SelectionForeColor = System.Drawing.Color.Black
        Me.grdDetFactura.RowsDefaultCellStyle = DataGridViewCellStyle24
        Me.grdDetFactura.Size = New System.Drawing.Size(1123, 202)
        Me.grdDetFactura.TabIndex = 78
        '
        'gfdCantidad
        '
        DataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle17.Format = "N4"
        DataGridViewCellStyle17.NullValue = Nothing
        Me.gfdCantidad.DefaultCellStyle = DataGridViewCellStyle17
        Me.gfdCantidad.HeaderText = "Cantidad"
        Me.gfdCantidad.Name = "gfdCantidad"
        Me.gfdCantidad.ReadOnly = True
        Me.gfdCantidad.Width = 80
        '
        'gfdCCODIGOPRODUCTO
        '
        Me.gfdCCODIGOPRODUCTO.HeaderText = "CodigoConcepto"
        Me.gfdCCODIGOPRODUCTO.Name = "gfdCCODIGOPRODUCTO"
        Me.gfdCCODIGOPRODUCTO.ReadOnly = True
        Me.gfdCCODIGOPRODUCTO.Visible = False
        '
        'gfdCIDPRODUCTO
        '
        Me.gfdCIDPRODUCTO.HeaderText = "idConcepto"
        Me.gfdCIDPRODUCTO.Name = "gfdCIDPRODUCTO"
        Me.gfdCIDPRODUCTO.ReadOnly = True
        Me.gfdCIDPRODUCTO.Visible = False
        '
        'gfdCNOMBREPRODUCTO
        '
        Me.gfdCNOMBREPRODUCTO.HeaderText = "Concepto"
        Me.gfdCNOMBREPRODUCTO.Name = "gfdCNOMBREPRODUCTO"
        Me.gfdCNOMBREPRODUCTO.ReadOnly = True
        Me.gfdCNOMBREPRODUCTO.Width = 350
        '
        'gfdPrecioUnit
        '
        DataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle18.Format = "C2"
        DataGridViewCellStyle18.NullValue = Nothing
        Me.gfdPrecioUnit.DefaultCellStyle = DataGridViewCellStyle18
        Me.gfdPrecioUnit.HeaderText = "Precio"
        Me.gfdPrecioUnit.Name = "gfdPrecioUnit"
        Me.gfdPrecioUnit.ReadOnly = True
        '
        'gfdSubtotal
        '
        DataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle19.Format = "C2"
        DataGridViewCellStyle19.NullValue = Nothing
        Me.gfdSubtotal.DefaultCellStyle = DataGridViewCellStyle19
        Me.gfdSubtotal.HeaderText = "SubTotal"
        Me.gfdSubtotal.Name = "gfdSubtotal"
        Me.gfdSubtotal.ReadOnly = True
        '
        'gfdRetencion
        '
        DataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle20.Format = "C2"
        DataGridViewCellStyle20.NullValue = Nothing
        Me.gfdRetencion.DefaultCellStyle = DataGridViewCellStyle20
        Me.gfdRetencion.HeaderText = "Retencion"
        Me.gfdRetencion.Name = "gfdRetencion"
        Me.gfdRetencion.ReadOnly = True
        '
        'gfdIVA
        '
        DataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle21.Format = "C2"
        DataGridViewCellStyle21.NullValue = Nothing
        Me.gfdIVA.DefaultCellStyle = DataGridViewCellStyle21
        Me.gfdIVA.HeaderText = "IVA"
        Me.gfdIVA.Name = "gfdIVA"
        Me.gfdIVA.ReadOnly = True
        '
        'gfdTotal
        '
        DataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle22.Format = "C2"
        DataGridViewCellStyle22.NullValue = Nothing
        Me.gfdTotal.DefaultCellStyle = DataGridViewCellStyle22
        Me.gfdTotal.HeaderText = "Total"
        Me.gfdTotal.Name = "gfdTotal"
        Me.gfdTotal.ReadOnly = True
        '
        'gfdTextoComp
        '
        Me.gfdTextoComp.HeaderText = "Texto"
        Me.gfdTextoComp.Name = "gfdTextoComp"
        Me.gfdTextoComp.Width = 350
        '
        'gfdKm
        '
        Me.gfdKm.HeaderText = "Km"
        Me.gfdKm.Name = "gfdKm"
        Me.gfdKm.Visible = False
        '
        'gfdTipoViaje
        '
        Me.gfdTipoViaje.HeaderText = "TipoViaje"
        Me.gfdTipoViaje.Name = "gfdTipoViaje"
        Me.gfdTipoViaje.Visible = False
        '
        'gfdPorcIVA
        '
        Me.gfdPorcIVA.HeaderText = "PorcIVA"
        Me.gfdPorcIVA.Name = "gfdPorcIVA"
        Me.gfdPorcIVA.Visible = False
        '
        'gfdPorcRET
        '
        Me.gfdPorcRET.HeaderText = "PorcRET"
        Me.gfdPorcRET.Name = "gfdPorcRET"
        Me.gfdPorcRET.Visible = False
        '
        'grdCabFactura
        '
        Me.grdCabFactura.AllowUserToAddRows = False
        Me.grdCabFactura.AllowUserToResizeColumns = False
        Me.grdCabFactura.AllowUserToResizeRows = False
        DataGridViewCellStyle25.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        DataGridViewCellStyle25.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle25.SelectionBackColor = System.Drawing.Color.Yellow
        DataGridViewCellStyle25.SelectionForeColor = System.Drawing.Color.Black
        Me.grdCabFactura.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle25
        Me.grdCabFactura.BackgroundColor = System.Drawing.Color.White
        Me.grdCabFactura.ColumnHeadersHeight = 34
        Me.grdCabFactura.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.grdCabFactura.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.gfConsecutivo, Me.gfidCliente, Me.gfNomCliente, Me.gfFecha, Me.gfFechaVenc, Me.gfSubtotal, Me.gfRetencion, Me.gfIVA, Me.gfTOTAL, Me.gfKm, Me.gfCantidad})
        DataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle32.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle32.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle32.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle32.SelectionBackColor = System.Drawing.Color.Yellow
        DataGridViewCellStyle32.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle32.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.grdCabFactura.DefaultCellStyle = DataGridViewCellStyle32
        Me.grdCabFactura.Location = New System.Drawing.Point(6, 18)
        Me.grdCabFactura.Name = "grdCabFactura"
        Me.grdCabFactura.RowHeadersWidth = 26
        DataGridViewCellStyle33.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle33.SelectionBackColor = System.Drawing.Color.Yellow
        DataGridViewCellStyle33.SelectionForeColor = System.Drawing.Color.Black
        Me.grdCabFactura.RowsDefaultCellStyle = DataGridViewCellStyle33
        Me.grdCabFactura.Size = New System.Drawing.Size(1119, 79)
        Me.grdCabFactura.TabIndex = 77
        '
        'gfConsecutivo
        '
        Me.gfConsecutivo.HeaderText = "#"
        Me.gfConsecutivo.Name = "gfConsecutivo"
        Me.gfConsecutivo.Width = 30
        '
        'gfidCliente
        '
        Me.gfidCliente.HeaderText = "Cliente"
        Me.gfidCliente.Name = "gfidCliente"
        Me.gfidCliente.Visible = False
        '
        'gfNomCliente
        '
        Me.gfNomCliente.HeaderText = "Razon Social"
        Me.gfNomCliente.Name = "gfNomCliente"
        Me.gfNomCliente.Width = 200
        '
        'gfFecha
        '
        Me.gfFecha.HeaderText = "Fecha"
        Me.gfFecha.Name = "gfFecha"
        Me.gfFecha.Width = 150
        '
        'gfFechaVenc
        '
        DataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.gfFechaVenc.DefaultCellStyle = DataGridViewCellStyle26
        Me.gfFechaVenc.HeaderText = "Fec. Vence"
        Me.gfFechaVenc.Name = "gfFechaVenc"
        Me.gfFechaVenc.Visible = False
        Me.gfFechaVenc.Width = 60
        '
        'gfSubtotal
        '
        DataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle27.Format = "C2"
        DataGridViewCellStyle27.NullValue = Nothing
        Me.gfSubtotal.DefaultCellStyle = DataGridViewCellStyle27
        Me.gfSubtotal.HeaderText = "Subtotal"
        Me.gfSubtotal.Name = "gfSubtotal"
        Me.gfSubtotal.Width = 120
        '
        'gfRetencion
        '
        DataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle28.Format = "C2"
        DataGridViewCellStyle28.NullValue = Nothing
        Me.gfRetencion.DefaultCellStyle = DataGridViewCellStyle28
        Me.gfRetencion.HeaderText = "Retencion"
        Me.gfRetencion.Name = "gfRetencion"
        Me.gfRetencion.Width = 120
        '
        'gfIVA
        '
        DataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle29.Format = "C2"
        DataGridViewCellStyle29.NullValue = Nothing
        Me.gfIVA.DefaultCellStyle = DataGridViewCellStyle29
        Me.gfIVA.HeaderText = "IVA"
        Me.gfIVA.Name = "gfIVA"
        Me.gfIVA.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.gfIVA.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.gfIVA.Width = 120
        '
        'gfTOTAL
        '
        DataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle30.Format = "C2"
        DataGridViewCellStyle30.NullValue = Nothing
        Me.gfTOTAL.DefaultCellStyle = DataGridViewCellStyle30
        Me.gfTOTAL.HeaderText = "TOTAL"
        Me.gfTOTAL.Name = "gfTOTAL"
        Me.gfTOTAL.Width = 150
        '
        'gfKm
        '
        DataGridViewCellStyle31.Format = "N2"
        DataGridViewCellStyle31.NullValue = Nothing
        Me.gfKm.DefaultCellStyle = DataGridViewCellStyle31
        Me.gfKm.HeaderText = "Km"
        Me.gfKm.Name = "gfKm"
        '
        'gfCantidad
        '
        Me.gfCantidad.HeaderText = "Cantidad Tons."
        Me.gfCantidad.Name = "gfCantidad"
        '
        'fFacturaViajes
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(1169, 622)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.MeStatus1)
        Me.Controls.Add(Me.ToolStripMenu)
        Me.Controls.Add(Me.GrupoCaptura)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "fFacturaViajes"
        Me.Text = "Factura Viajes"
        Me.GrupoCaptura.ResumeLayout(False)
        Me.GrupoCaptura.PerformLayout()
        Me.gpoTipoDoc.ResumeLayout(False)
        Me.gpoTipoDoc.PerformLayout()
        Me.GpoTexto.ResumeLayout(False)
        Me.GpoTexto.PerformLayout()
        Me.GpoMetFact.ResumeLayout(False)
        Me.GpoMetFact.PerformLayout()
        Me.gpoFecha.ResumeLayout(False)
        Me.gpoFecha.PerformLayout()
        Me.gpoRuta.ResumeLayout(False)
        Me.gpoRuta.PerformLayout()
        Me.gpoTipoUni.ResumeLayout(False)
        Me.gpoTipoUni.PerformLayout()
        Me.gpoRemision.ResumeLayout(False)
        Me.gpoRemision.PerformLayout()
        Me.ToolStripMenu.ResumeLayout(False)
        Me.ToolStripMenu.PerformLayout()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        CType(Me.grdViajesDet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdViajes, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        CType(Me.grdDetFactura, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdCabFactura, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Sub New(ByVal vTag As String, Optional ByVal vNoServ As Integer = 0)
        InitializeComponent()
        _Tag = vTag
        _NoServ = vNoServ
    End Sub
    Dim _sqlCon As String
    Dim _sqlConCom As String
    Dim _conConfigl As String
    Sub New(ByVal sqlCon As String, ByVal sqlConComercial As String, ByVal conConfigl As String, cveEmpresa As Integer)
        InitializeComponent()
        _sqlCon = sqlCon
        _sqlConCom = sqlConComercial
        _conConfigl = conConfigl

        _cveEmpresa = cveEmpresa

        Inicio.CONSTR = _sqlCon
        Inicio.CONSTR_COM = _sqlConCom


    End Sub

    Private Sub frmDiagnosticoTaller_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Salida = True
        'Cancelar()
        LlenaCombos()
        HabilitaCamposFecha()

        dtpFechaIni.Value = Now
        dtpFechaFin.Value = Now

        Salida = False
    End Sub

    Private Sub DespliegaDatosCli(ByVal idCliente As Integer)
        If MyTablaCli.DefaultView.Count > 0 Then
            MyTablaCli.DefaultView.RowFilter = "IdCliente = " & idCliente
            If MyTablaCli.DefaultView.Count > 0 Then
                txtNomEmpresa.Text = MyTablaCli.DefaultView.Item(0).Item("RazonSocial").ToString
            End If
            MyTablaCli.DefaultView.RowFilter = Nothing
            Salida = False
        End If

    End Sub



    Private Sub LlenaCombos()
        Try
            StrSql = "SELECT cli.IdCliente, " &
            "cli.idEmpresa, " &
            "emp.RazonSocial, " &
            "cli.Nombre, " &
            "cli.Domicilio, " &
            "cli.RFC " &
            "FROM dbo.CatClientes cli " &
            "INNER JOIN dbo.CatEmpresas emp ON cli.idEmpresa = emp.idEmpresa " &
             "WHERE cli.IdCliente > 0 and cli.idEmpresa = " & _cveEmpresa
            '"WHERE cli.IdCliente > 0"

            MyTablaCombo.Rows.Clear()

            If BD Is Nothing Then
                BD = New CapaDatos.UtilSQL(_sqlCon, "Juan")
            End If

            MyTablaCombo = BD.ExecuteReturn(StrSql)
            If Not MyTablaCombo Is Nothing Then
                If MyTablaCombo.Rows.Count > 0 Then
                    cmbClientes.DataSource = MyTablaCombo
                    cmbClientes.ValueMember = "IdCliente"
                    cmbClientes.DisplayMember = "Nombre"

                    MyTablaCli = MyTablaCombo
                End If
            End If

            If cmbClientes.SelectedValue > 0 Then
                Salida = True
                DespliegaDatosCli(cmbClientes.SelectedValue)
                Salida = False
            End If

            'COMERCIAL 
            StrSql = "SELECT * FROM admConceptos WHERE CIDDOCUMENTODE = 4"
            MyTablaConcepCOM.Rows.Clear()
            If BDCOM Is Nothing Then
                BDCOM = New CapaDatos.UtilSQL(_sqlConCom, "")
            End If
            MyTablaConcepCOM = BDCOM.ExecuteReturn(StrSql)
            If Not MyTablaConcepCOM Is Nothing Then
                If MyTablaConcepCOM.Rows.Count > 0 Then
                    cmbDocumCOM.DataSource = MyTablaConcepCOM
                    cmbDocumCOM.ValueMember = "CCODIGOCONCEPTO"
                    cmbDocumCOM.DisplayMember = "CNOMBRECONCEPTO"
                End If
            End If
            'cmbDocumCOM
            'MyTablaConcepCOM()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnMnuSalir_Click(sender As Object, e As EventArgs) Handles btnMnuSalir.Click
        Me.Close()
    End Sub

    Private Sub btnMnuCancelar_Click(sender As Object, e As EventArgs) Handles btnMnuCancelar.Click
        Cancelar(True)
    End Sub

    Private Sub ActivaCampos(ByVal valor As Boolean, ByVal vOpcion As TipoOpcActivaCampos)
        If vOpcion = TipoOpcActivaCampos.tOpcDESHABTODOS Then

            rdbFecTodos.Checked = True
            rdbRemTodos.Checked = True
            rdbTUTodos.Checked = True
            rdbRutTodos.Checked = True
            rdbMFAgrupado.Checked = True
            chkITFecha.Checked = False
            chkITTon.Checked = False
            chkITKm.Checked = False
            txtTexto.Enabled = False


        ElseIf vOpcion = TipoOpcActivaCampos.tOpcINICIALIZA Then

        End If
    End Sub

    Private Sub LimpiaCampos()
        cmbClientes.SelectedIndex = 0
        txtNomEmpresa.Text = ""
        cmbDocumCOM.SelectedIndex = 0
        dtpFechaIni.Value = Now
        dtpFechaFin.Value = Now
        txtRemIni.Text = ""
        txtRemFin.Text = ""
        txtRemision.Text = ""
        cmbRemisiones.DataSource = Nothing
        txtRuta.Text = ""
        cmbRutas.DataSource = Nothing
        txtTexto.Text = ""

        grdViajes.DataSource = Nothing
        grdViajes.Rows.Clear()

        grdViajesDet.DataSource = Nothing
        grdViajesDet.Rows.Clear()

        grdCabFactura.DataSource = Nothing
        grdCabFactura.Rows.Clear()


        grdDetFactura.DataSource = Nothing
        grdDetFactura.Rows.Clear()

        txtSubTotVia.Text = ""
        txtImpRetVia.Text = ""
        txtImpIVAVia.Text = ""
        txtTOTALVia.Text = ""

        chkMarcaFacturas.Checked = False

        indice = 0
        indiceCOM = 0



    End Sub

    Private Sub Cancelar(ByVal bFecha As Boolean)


        LlenaCombos()

        ActivaCampos(False, TipoOpcActivaCampos.tOpcDESHABTODOS)
        ActivaBotones(False, TipoOpcActivaBoton.tOpcInicializa)

        LimpiaCampos()
        ToolStripMenu.Enabled = True
        TabControl1.SelectedIndex = 0

        If bFecha Then
            dtpFechaIni.Value = Now
            dtpFechaFin.Value = Now
        End If

        cmbClientes.Enabled = True

        cmbClientes.Focus()

        Status("", Me)

    End Sub

    Private Sub ActivaBotones(ByVal Valor As Boolean, Optional ByVal vOpcion As TipoOpcActivaBoton = TipoOpcActivaBoton.tOpcInicializa)
        'Si es Verdadero Se activan Ok,Cancelar y Buscar y Terminar Se desactiva
        If vOpcion = TipoOpcActivaBoton.tOpcConsulta Then
            btnMnuOk.Enabled = False
            btnMnuCancelar.Enabled = Valor
            'btnMnuPendiente.Enabled = Not Valor
            'btnMnuEliminar.Enabled = Not Valor
            'btnMnuReporte.Enabled = Not Valor
            'btnMnuReportePed.Enabled = Not Valor
            btnMnuSalir.Enabled = Not Valor
        ElseIf vOpcion = TipoOpcActivaBoton.tOpcInicializa Then
            btnMnuOk.Enabled = Not Valor
            'btnMnuPendiente.Enabled = Valor
            btnMnuCancelar.Enabled = Valor
            'btnMnuEliminar.Enabled = Valor
            'btnMnuReporte.Enabled = Not Valor
            'btnMnuReportePed.Enabled = Valor
            btnMnuSalir.Enabled = Not Valor

        ElseIf vOpcion = TipoOpcActivaBoton.tOpcInsertar Then
            btnMnuOk.Enabled = False
            'btnMnuPendiente.Enabled = Valor
            btnMnuCancelar.Enabled = Valor
            'btnMnuEliminar.Enabled = Valor
            'btnMnuReporte.Enabled = Not Valor
            'btnMnuReportePed.Enabled = Valor
            btnMnuSalir.Enabled = Not Valor
        End If
    End Sub

    Private Function BuscaDatoCombo(ByVal combo As DataTable, ByVal CampoFiltra As String, ByVal valor As String,
        ByVal tipo As TipoDato, ByVal CampoBusca As String) As String
        BuscaDatoCombo = ""
        Dim Filtro As String = ""
        Dim Resultado As String = ""
        Filtro = CampoFiltra & " = '" & valor & "'"
        'If tipo = TipoDato.TdCadena Then
        '    Filtro = CampoFiltra & " = '" & valor & "'"
        'ElseIf tipo = TipoDato.TdNumerico Then
        '    Filtro = CampoFiltra & " = " & valor
        'ElseIf tipo = TipoDato.TdBoolean Then
        '    Filtro = CampoFiltra & " = " & valor
        'End If
        combo.DefaultView.RowFilter = Nothing
        combo.DefaultView.RowFilter = Filtro
        If combo.DefaultView.Count > 0 Then
            'Return combo.Tables(Nomtabla).DefaultView.Item(0).Item(CampoBusca)
            Resultado = combo.DefaultView.Item(0).Item(CampoBusca)
        End If
        combo.DefaultView.RowFilter = Nothing
        Return Resultado
    End Function


    Private Function InsertaRegGrid(ByVal vTabla As DataTable, ByVal NomGrid As String) As Boolean
        Try
            If NomGrid = grdViajes.Name Then
                grdViajes.Rows.Clear()
                For i = 0 To vTabla.DefaultView.Count - 1
                    grdViajes.Rows.Add()
                    grdViajes.Item("gvConsecutivo", i).Value = i + 1
                    grdViajes.Item("gvFacturar", i).Value = 0
                    grdViajes.Item("gvNumGuiaId", i).Value = vTabla.DefaultView.Item(i).Item("NumGuiaId")
                    grdViajes.Item("gvNoViaje", i).Value = Trim(vTabla.DefaultView.Item(i).Item("NoViaje").ToString)
                    grdViajes.Item("gvidCliente", i).Value = Trim(vTabla.DefaultView.Item(i).Item("idCliente").ToString)
                    grdViajes.Item("gvidTractor", i).Value = Trim(vTabla.DefaultView.Item(i).Item("idTractor").ToString)
                    grdViajes.Item("gvidRemolque1", i).Value = Trim(vTabla.DefaultView.Item(i).Item("idRemolque1").ToString)
                    grdViajes.Item("gvidDolly", i).Value = Trim(vTabla.DefaultView.Item(i).Item("idDolly").ToString)
                    grdViajes.Item("gvidRemolque2", i).Value = Trim(vTabla.DefaultView.Item(i).Item("idRemolque2").ToString)
                    grdViajes.Item("gvFecha", i).Value = Trim(vTabla.DefaultView.Item(i).Item("Fecha").ToString)
                    grdViajes.Item("gvTipoViaje", i).Value = Trim(vTabla.DefaultView.Item(i).Item("TipoViaje").ToString)
                    grdViajes.Item("gvFraccion", i).Value = Trim(vTabla.DefaultView.Item(i).Item("idFraccion").ToString)

                    If rdbDocVia.Checked Then
                        grdViajes.Columns("gvCantidad").Visible = Not rdbDocVia.Checked
                        grdViajes.Columns("gvCantidad").Visible = Not rdbDocVia.Checked
                        grdViajes.Columns("gvCantidad").Visible = Not rdbDocVia.Checked
                    Else
                        grdViajes.Columns("gvCantidad").Visible = rdbDocPen.Checked
                        grdViajes.Columns("gvCantidad").Visible = rdbDocPen.Checked
                        grdViajes.Columns("gvCantidad").Visible = rdbDocPen.Checked

                        grdViajes.Item("gvCantidad", i).Value = vTabla.DefaultView.Item(i).Item("Cantidad")
                        grdViajes.Item("gvPrecio", i).Value = vTabla.DefaultView.Item(i).Item("Precio")
                        grdViajes.Item("gvImporte", i).Value = vTabla.DefaultView.Item(i).Item("importe")
                    End If





                    'gdConteo.Columns["gdcDiferencia"].Visible = chkVerDif.Checked;  
                Next

                grdViajes.PerformLayout()
            ElseIf NomGrid = grdViajesDet.Name Then
                grdViajesDet.Rows.Clear()

                For i = 0 To vTabla.DefaultView.Count - 1
                    grdViajesDet.Rows.Add()
                    'Consecutivo
                    grdViajesDet.Item("gvdNumGuiaId", i).Value = Trim(vTabla.DefaultView.Item(i).Item("NumGuiaId").ToString)
                    grdViajesDet.Item("gvdNoRemision", i).Value = Trim(vTabla.DefaultView.Item(i).Item("NoRemision").ToString)
                    grdViajesDet.Item("gvdDescripcion", i).Value = Trim(vTabla.DefaultView.Item(i).Item("Descripcion").ToString)
                    grdViajesDet.Item("gvdDestino", i).Value = Trim(vTabla.DefaultView.Item(i).Item("Destino").ToString)
                    grdViajesDet.Item("gvdVolDescarga", i).Value = CDbl(vTabla.DefaultView.Item(i).Item("VolDescarga"))
                    grdViajesDet.Item("gvdPrecio", i).Value = CDbl(vTabla.DefaultView.Item(i).Item("Precio"))
                    grdViajesDet.Item("gvdsubTotal", i).Value = CDbl(vTabla.DefaultView.Item(i).Item("subTotal"))
                    grdViajesDet.Item("gvdRetencion", i).Value = CDbl(vTabla.DefaultView.Item(i).Item("ImpRetencion"))
                    grdViajesDet.Item("gvdIVA", i).Value = CDbl(vTabla.DefaultView.Item(i).Item("ImpIVA"))

                Next

                grdViajesDet.PerformLayout()
            End If


            'grdVentas.Columns("Precio").DefaultCellStyle.Format = "c"
            'grdVentas.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
            Return True
        Catch ex As Exception
            Return False
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, Me.Text)
        End Try

    End Function


    Private Sub btnMnuGenerar_Click(sender As Object, e As EventArgs) Handles btnMnuGenerar.Click
        Dim StrSQlFiltro As String = ""
        Dim Filtro As String = ""

        Salida = True
        'Fecha
        If rdbFecRango.Checked Then
            'Filtro = " AND cab.FechaHoraFin >= '" & dtpFechaIni.Value.ToShortDateString & " 00:00:00' and cab.FechaHoraFin <= '" & dtpFechaFin.Value.ToShortDateString & " 23:59:59'"
            Filtro = " AND cab.FechaHoraViaje >= '" & dtpFechaIni.Value.ToShortDateString & " 00:00:00.000' and cab.FechaHoraViaje <= '" & dtpFechaFin.Value.ToShortDateString & " 23:59:59.999'"
        End If
        If rdbRemRango.Checked Then
            'Filtro = IIf(Filtro = "", "", Filtro & " AND ") & " det.NoRemision >= " & CInt(txtRemIni.Text) & " and det.NoRemision <= " & CInt(txtRemFin.Text)
            Filtro = Filtro & " AND det.NoRemision >= " & CInt(txtRemIni.Text) & " and det.NoRemision <= " & CInt(txtRemFin.Text)

        End If
        If rdbTUFull.Checked Then
            'Filtro = IIf(Filtro = "", "", Filtro & " AND ") & " cab.TipoViaje = 'F' "
            Filtro = Filtro & " AND cab.TipoViaje = 'F' "
        End If
        If rdbTUSenc.Checked Then
            'Filtro = IIf(Filtro = "", "", Filtro & " AND ") & " cab.TipoViaje = 'S' "
            Filtro = Filtro & " AND cab.TipoViaje = 'S' "
        End If

        If Filtro <> "" Then
            'If rdbDocVia.Checked Then
            '    StrSQlFiltro = "SELECT DISTINCT cab.NumGuiaId FROM dbo.CabGuia cab " & _
            '    "INNER JOIN dbo.DetGuia det ON cab.NumGuiaId = det.NumGuiaId " & _
            '    "WHERE cab.EstatusGuia = 'FINALIZADO' " & Filtro

            '    StrSQlFiltro = " AND cab.NumGuiaId in (" & StrSQlFiltro & ")"

            'Else
            '    StrSQlFiltro = "SELECT DISTINCT cab.NumGuiaId FROM dbo.CabGuia gui " & _
            '    "INNER JOIN dbo.DetGuia det ON gui.NumGuiaId = det.NumGuiaId " & _
            '    "WHERE gui.EstatusGuia = 'FINALIZADO' " & Filtro

            '    StrSQlFiltro = " AND gui.NumGuiaId in (" & StrSQlFiltro & ")"

            'End If
            If rdbDocVia.Checked Then
                StrSQlFiltro = "SELECT DISTINCT cab.NumGuiaId FROM dbo.CabGuia cab " &
                "INNER JOIN dbo.DetGuia det ON cab.NumGuiaId = det.NumGuiaId " &
                "WHERE det.baja = 0 and cab.EstatusGuia in ('FINALIZADO') " & Filtro

            Else
                StrSQlFiltro = "SELECT DISTINCT cab.NumGuiaId FROM dbo.CabGuia cab " &
                "INNER JOIN dbo.DetGuia det ON cab.NumGuiaId = det.NumGuiaId " &
                "WHERE det.baja = 0 and cab.EstatusGuia in ('FINALIZADO','FACTURADO') " & Filtro

            End If

            StrSQlFiltro = " AND cab.NumGuiaId in (" & StrSQlFiltro & ")"
        End If

        If rdbDocVia.Checked Then
            StrSql = "SELECT cab.NumGuiaId, " &
            " (cab.SerieGuia + '' + CAST(cab.NumGuiaId AS varchar(10))) AS NoViaje, " &
            "cab.NumViaje AS NumViaje, " &
            "cab.idCliente, " &
            "cli.Nombre, " &
            "cli.RFC, " &
            "cab.IdEmpresa, " &
            "emp.RazonSocial, " &
            "cab.idOperador, " &
            "ISNULL((CASE WHEN per.Nombre <> '' THEN per.Nombre + ' ' + per.ApellidoPat + ' ' + per.ApellidoMat ELSE per.NombreCompleto end),'') AS Nomcompleto, " &
            "cab.idTractor, " &
            "cab.idRemolque1, " &
            "ISNULL(cab.idDolly,'') AS idDolly," &
            "ISNULL(cab.idRemolque2,'') AS idRemolque2," &
            "cab.FechaHoraViaje as Fecha, " &
            "cab.TipoViaje, " &
            "cli.clave_Fraccion as idFraccion " &
            "FROM dbo.CabGuia cab " &
            "INNER JOIN dbo.CatClientes cli ON cab.idCliente = cli.IdCliente " &
            "INNER JOIN dbo.CatEmpresas emp ON cab.IdEmpresa = emp.idEmpresa " &
            "INNER JOIN dbo.CatPersonal per ON cab.idOperador = per.idPersonal " &
            "WHERE cab.EstatusGuia  in ('FINALIZADO') AND cab.idCliente = " & cmbClientes.SelectedValue &
            IIf(StrSQlFiltro <> "", StrSQlFiltro, "")

            '"cab.FechaHoraFin as Fecha, " & _
            'FechaHoraViaje
        ElseIf rdbDocPen.Checked Then
            StrSql = "SELECT cab.NumGuiaId, " &
            "(cab.SerieGuia + '' + CAST(cab.NumGuiaId AS varchar(10))) AS NoViaje, " &
            "cab.NumViaje, " &
            "cab.idCliente, " &
            "cli.Nombre, cli.RFC, " &
            "cab.IdEmpresa, emp.RazonSocial, " &
            "cab.idOperador, " &
            "ISNULL((CASE WHEN per.Nombre <> '' THEN per.Nombre + ' ' + per.ApellidoPat + ' ' + per.ApellidoMat ELSE per.NombreCompleto end),'') AS Nomcompleto, " &
            "cab.idTractor, cab.idRemolque1, ISNULL(cab.idDolly,'') AS idDolly,ISNULL(cab.idRemolque2,'') AS idRemolque2, " &
            "cab.FechaHoraViaje as Fecha, cab.TipoViaje, cli.clave_Fraccion as idFraccion, " &
            "pen.diferencia AS Cantidad, " &
            "pen.precio AS Precio, " &
            "pen.importe as Importe " &
            "FROM dbo.penalizaciones pen " &
            "left JOIN dbo.CabGuia cab ON pen.idViaje = cab.NumGuiaId " &
            "INNER JOIN dbo.CatClientes cli ON cab.idCliente = cli.IdCliente " &
            "INNER JOIN dbo.CatEmpresas emp ON cab.IdEmpresa = emp.idEmpresa " &
            "INNER JOIN dbo.CatPersonal per ON cab.idOperador = per.idPersonal " &
            "WHERE pen.Estatus in ('FINALIZADO') AND cab.idCliente = " & cmbClientes.SelectedValue &
            IIf(StrSQlFiltro <> "", StrSQlFiltro, "")

            '"and pen.fecha >= '01/06/2016 00:00:00' AND pen.fecha <= '30/06/2016 23:59:59'"
        End If



        MyTablaViaje.Rows.Clear()
        MyTablaViaje = BD.ExecuteReturn(StrSql)
        If Not MyTablaViaje Is Nothing Then
            If MyTablaViaje.Rows.Count > 0 Then
                Salida = True
                Status(MyTablaViaje.Rows.Count & " Viajes", Me)

                InsertaRegGrid(MyTablaViaje, grdViajes.Name)
                'grdViajes.DataSource = MyTablaViaje


                IdNoGuia = grdViajes.Item("gvNumGuiaId", 0).Value
                'idFraccion = grdViajes.Item("gvFraccion", 0).Value
                CargaDetalleViaje(IdNoGuia)

                'ActivaBotones()
                cmbClientes.Enabled = False

                Salida = False
            Else
                'No se encontraron viajes
            End If
        End If
        Salida = False
    End Sub

    Private Sub rdbFecTodos_CheckedChanged(sender As Object, e As EventArgs) Handles rdbFecTodos.CheckedChanged
        HabilitaCamposFecha()

    End Sub

    Private Sub HabilitaCamposFecha()
        If rdbFecTodos.Checked Then
            rdbFecRango.Checked = False
            dtpFechaIni.Enabled = False
            dtpFechaFin.Enabled = False
        Else
            rdbFecRango.Checked = True
            dtpFechaIni.Enabled = True
            dtpFechaFin.Enabled = True
            dtpFechaIni.Focus()
        End If
    End Sub

    Private Sub rdbRemTodos_CheckedChanged(sender As Object, e As EventArgs) Handles rdbRemTodos.CheckedChanged
        HabilitaCamposRem()
    End Sub

    Private Sub HabilitaCamposRem()
        If rdbRemTodos.Checked Then
            rdbRemRango.Checked = False
            txtRemIni.Enabled = False
            txtRemFin.Enabled = False
            rdbRemSelec.Checked = False
            txtRemision.Enabled = False
            cmdBuscaRem.Enabled = False
            cmbRemisiones.Enabled = False
        ElseIf rdbRemRango.Checked Then
            rdbRemTodos.Checked = False
            txtRemIni.Enabled = True
            txtRemFin.Enabled = True
            rdbRemSelec.Checked = False
            txtRemision.Enabled = False
            cmdBuscaRem.Enabled = False
            cmbRemisiones.Enabled = False
            txtRemIni.Focus()
        ElseIf rdbRemSelec.Checked Then
            rdbRemTodos.Checked = False

            rdbRemRango.Checked = False
            txtRemIni.Enabled = False
            txtRemFin.Enabled = False

            txtRemision.Enabled = True
            cmdBuscaRem.Enabled = True
            cmbRemisiones.Enabled = True
            txtRemision.Focus()
        End If
    End Sub

    Private Sub rdbRemRango_CheckedChanged(sender As Object, e As EventArgs) Handles rdbRemRango.CheckedChanged
        HabilitaCamposRem()
    End Sub

    Private Sub rdbRemSelec_CheckedChanged(sender As Object, e As EventArgs) Handles rdbRemSelec.CheckedChanged
        HabilitaCamposRem()
    End Sub


    Private Sub rdbRutTodos_CheckedChanged(sender As Object, e As EventArgs) Handles rdbRutTodos.CheckedChanged
        HabilitaCamposRuta()
    End Sub

    Private Sub HabilitaCamposRuta()
        If rdbRutTodos.Checked Then
            txtRuta.Enabled = False
            cmdBuscaRuta.Enabled = False
            cmbRutas.Enabled = False
        Else
            txtRuta.Enabled = True
            cmdBuscaRuta.Enabled = True
            cmbRutas.Enabled = True
        End If
    End Sub

    Private Sub grdViajes_CellValueChanged(sender As Object, e As DataGridViewCellEventArgs) Handles grdViajes.CellValueChanged
        'If Salida Then Exit Sub
        'Try
        '    If grdViajes.Columns(e.ColumnIndex).Name = "gvFacturar" Then
        '        If grdViajes.Item("gvFacturar", e.RowIndex).Value = 0 Then
        '            grdViajes.Item("gvFacturar", e.RowIndex).Value = 1
        '        Else
        '            grdViajes.Item("gvFacturar", e.RowIndex).Value = 0
        '        End If
        '    End If
        'Catch ex As Exception

        'End Try
        'grdViajes.EndEdit()

    End Sub


    Private Sub grdViajes_Click(sender As Object, e As EventArgs) Handles grdViajes.Click

        IdNoGuia = grdViajes.Item("gvNumGuiaId", grdViajes.CurrentCell.RowIndex).Value
        'idFraccion = grdViajes.Item("gvFraccion", grdViajes.CurrentCell.RowIndex).Value
        CargaDetalleViaje(IdNoGuia)

        If grdViajes.Columns(grdViajes.CurrentCell.ColumnIndex).Name = "gvFacturar" Then
            If grdViajes.Item("gvFacturar", grdViajes.CurrentCell.RowIndex).Value = 0 Then
                grdViajes.Item("gvFacturar", grdViajes.CurrentCell.RowIndex).Value = 1
            Else
                grdViajes.Item("gvFacturar", grdViajes.CurrentCell.RowIndex).Value = 0
            End If
            grdViajes.EndEdit()
        End If

    End Sub

    'Private Sub CargaDetalleViaje(ByVal vNumGuiaId As Integer, ByVal vidFraccion As Integer)
    Private Sub CargaDetalleViaje(ByVal vNumGuiaId As Integer)
        '"(cab.SerieGuia + '' + CAST(cab.NumGuiaId AS varchar(10))) AS NoViaje," & _
        StrSql = ""

        StrSql = "SELECT det.NumGuiaId, " &
        "det.NoRemision, " &
        "det.IdProdTraf, " &
        "prod.Descripcion, " &
        "det.IdPlazaTraf, " &
        "pla.Descripcion AS Destino, " &
        "det.VolDescarga, " &
        "det.Precio, " &
        "det.PorcIVA, " &
        "det.ImpIVA, " &
        "det.ImpRetencion, " &
        "det.subTotal, " &
        "det.idTarea " &
        "FROM dbo.DetGuia det " &
        "INNER JOIN dbo.CatProductos prod ON det.IdProdTraf = prod.IdProducto " &
        " INNER JOIN trafico_plazas_cat pla ON det.IdPlazaTraf = pla.Clave " &
        "WHERE det.baja = 0 and det.NumGuiaId = " & vNumGuiaId &
        " ORDER BY det.Consecutivo"

        '"INNER JOIN dbo.CatProductos prod ON det.IdProdTraf = prod.IdProducto AND prod.IdFraccion = " & vidFraccion & _

        MyTablaDetViaje = Nothing
        'MyTablaDetViaje.Rows.Clear()
        MyTablaDetViaje = BD.ExecuteReturn(StrSql)
        If Not MyTablaDetViaje Is Nothing Then
            If MyTablaDetViaje.Rows.Count > 0 Then
                'grdViajesDet.DataSource = MyTablaDetViaje
                InsertaRegGrid(MyTablaDetViaje, grdViajesDet.Name)
                CalculaTotViajes()
            End If
        End If
    End Sub

    Private Sub CalculaTotViajes()
        Dim Subtotal As Double
        Dim ImpReten As Double
        Dim ImpIVA As Double

        Subtotal = 0
        ImpReten = 0
        ImpIVA = 0

        If grdViajesDet.RowCount > 0 Then
            For i = 0 To grdViajesDet.RowCount - 1
                Subtotal = Subtotal + grdViajesDet.Item("gvdsubTotal", i).Value
                ImpReten = ImpReten + grdViajesDet.Item("gvdRetencion", i).Value
                ImpIVA = ImpIVA + grdViajesDet.Item("gvdIVA", i).Value
            Next
        End If

        txtSubTotVia.Text = Format(Subtotal, "c")
        txtImpRetVia.Text = Format(ImpReten, "c")
        txtImpIVAVia.Text = Format(ImpIVA, "c")
        txtTOTALVia.Text = Format((Subtotal - ImpReten + ImpIVA), "c")
    End Sub


    Private Sub chkMarcaFacturas_CheckedChanged(sender As Object, e As EventArgs) Handles chkMarcaFacturas.CheckedChanged
        If grdViajes.RowCount > 0 Then
            For i = 0 To grdViajes.RowCount - 1
                grdViajes.Item("gvFacturar", i).Value = IIf(chkMarcaFacturas.Checked, 1, 0)
            Next
        End If
    End Sub

    'Private Function ConsultaDatoServicio(ByVal TipoViaje As String, ByVal NomCampo As String) As String
    Private Function ConsultaDatoServicio(ByVal CampoFiltra As String, ByVal NomCampoDev As String, ByVal ValorFiltra As String, ByVal tipoDato As TipoDato, ByVal Estado As Integer) As String
        'Optional ByVal Estado As Integer = 0
        Dim filtro As String = ""
        Dim filtroExtra As String = ""
        If Estado > 0 Then
            filtroExtra = " and idestado = " & Estado
        Else
            filtroExtra = ""
        End If

        MyTablaConcepFac.DefaultView.RowFilter = Nothing
        'MyTablaConcepFac.DefaultView.RowFilter = "TipoViaje = '" & TipoViaje & "'"
        If tipoDato = TipoDato.TdCadena Then
            filtro = CampoFiltra & " = '" & ValorFiltra & "'" & filtroExtra

        ElseIf tipoDato = TipoDato.TdNumerico Then
            filtro = CampoFiltra & " = " & CInt(ValorFiltra) & filtroExtra
        End If

        MyTablaConcepFac.DefaultView.RowFilter = filtro
        If MyTablaConcepFac.DefaultView.Count > 0 Then
            Return CStr(MyTablaConcepFac.DefaultView.Item(0).Item(NomCampoDev))
        Else
            Return ""
        End If

        MyTablaConcepFac.DefaultView.RowFilter = Nothing
    End Function


    'Private Sub CreaFactura(ByVal idCliente As Integer, ByVal idFraccion As Integer, ByVal NomCliente As String)
    Private Sub CreaFactura(ByVal idCliente As Integer, ByVal NomCliente As String)
        Dim fIdEstado As Integer = 0
        Dim fNomEstado As String = ""
        Dim fOrigen As String = ""


        Dim FiltroViajes As String = ""
        Dim fTipoViaje As String = ""
        Dim fCantidad As Double = 0
        Dim fPrecio As Double = 0
        Dim fPorcIVA As Double = 0
        Dim fPorcRET As Double = 0

        Dim fTotKm As Double = 0
        Dim fSubtotal As Double = 0
        Dim fRetencion As Double = 0
        Dim fIMPIVA As Double = 0
        Dim fTotal As Double = 0

        Dim fSubtotalTOT As Double = 0
        Dim fRetencionTOT As Double = 0
        Dim fIMPIVATOT As Double = 0
        Dim fTotalTOT As Double = 0
        Dim fTotKmTOT As Double = 0
        Dim fCantidadTOT As Double = 0

        Dim ContDetalle As Integer = 0
        Dim ContCabecero As Integer = 0

        Dim PrecioGlobal As Double = 0

        'Cargar Conceptos para Facturar
        StrSql = "SELECT * FROM dbo.CatServFact serv WHERE serv.IdCliente = " & idCliente
        MyTablaConcepFac.Rows.Clear()
        MyTablaConcepFac = BD.ExecuteReturn(StrSql)
        If Not MyTablaConcepFac Is Nothing Then
            If MyTablaConcepFac.Rows.Count > 0 Then
            Else
                MsgBox("No se encontraron Servicios para el cliente " & cmbClientes.Text & vbCrLf & "tiene que dar de alta en el Catalogo Servicios Clientes", MsgBoxStyle.Information, Me.Text)
                Exit Sub
            End If
        End If


        For i = 0 To grdViajes.RowCount - 1
            If grdViajes.Item("gvFacturar", i).Value = 1 Then
                FiltroViajes = FiltroViajes & grdViajes.Item("gvNumGuiaId", i).Value & ","
                'If i = grdViajes.RowCount - 1 Then
                '    FiltroViajes = FiltroViajes & grdViajes.Item("gvNumGuiaId", i).Value
                'End If
            End If
        Next
        If FiltroViajes <> "" Then
            FiltroViajes = Mid(FiltroViajes, 1, FiltroViajes.Length - 1)
        End If

        If FiltroViajes <> "" Then
            FiltroViajes = " (" & FiltroViajes & " )"

            If rdbDocVia.Checked Then
                '"CASE WHEN cab.TipoViaje = 'S' then pre.Precio_sencillo ELSE pre.Precio_full END AS Precio1, " &
                '"INNER JOIN DBO.TRAFICO_CLIENTES_PLAZAS PRE ON DET.IDPLAZATRAF = PRE.CLAVE_PLAZA AND CAB.IDCLIENTE = PRE.CLAVE_CLIENTE " &

                StrSql = "SELECT det.Consecutivo,det.NumGuiaId,det.SerieGuia,det.NoRemision,det.IdProdTraf, " &
                "det.IdPlazaTraf,det.pesoBruto,det.tara, " &
                "round(det.VolDescarga,4) AS VolDescarga, " &
                "det.precio as Precio, " &
                "det.PorcIVA, " &
                "det.ImpIVA, " &
                "det.PorcRetencion, " &
                "det.ImpRetencion, " &
                "det.idTarea,det.subTotal,det.horaLlegada,det.horaCaptura,det.horaImpresion, " &
                "det.observaciones,det.fCartaPorte, " &
                "cab.TipoViaje, " &
                "prod.Descripcion, pla.Descripcion AS Destino, pla.km, pla.Clave, pla.idestado, pla.Origen, pla.Origen, est.NomEstado  " &
                "FROM dbo.DetGuia det " &
                "INNER JOIN dbo.CabGuia cab ON det.NumGuiaId = cab.NumGuiaId " &
                "INNER JOIN dbo.CatProductos prod ON det.IdProdTraf = prod.IdProducto " &
                "INNER JOIN trafico_plazas_cat pla ON det.IdPlazaTraf = pla.Clave " &
                "INNER JOIN dbo.CatEstados est ON pla.idEstado = est.idEstado " &
                "WHERE det.baja = 0 and det.subTotal > 0 AND det.VolDescarga > 0 " &
                IIf(FiltroViajes <> "", " AND det.NumGuiaId IN " & FiltroViajes & " ", "") &
                " and CAB.ESTATUSGUIA in ('FINALIZADO') " &
                " AND cab.idCliente = " & idCliente &
                " ORDER BY pla.idEstado, pla.Origen,cab.TipoViaje,det.precio "
                '" ORDER BY cab.TipoViaje,Precio "

            Else
                StrSql = "SELECT pen.diferencia AS VolDescarga, pen.*, cab.TipoViaje, " &
                "16 AS PorcIVA,pen.importe * .16 AS ImpIVA, " &
                "4 AS PorcRetencion,pen.importe * .04 AS ImpRetencion, " &
                "0 AS KM, pen.importe AS subTotal " &
                "FROM dbo.penalizaciones pen " &
                "INNER JOIN dbo.CabGuia cab ON pen.idViaje = cab.NumGuiaId  " &
                IIf(FiltroViajes <> "", " WHERE  pen.idViaje IN " & FiltroViajes & " ", "") &
                " and CAB.ESTATUSGUIA in ('FINALIZADO','FACTURADO') " &
                " AND cab.idCliente = " & idCliente &
                " ORDER BY cab.TipoViaje,Precio "

            End If




            '"INNER JOIN dbo.CatProductos prod ON det.IdProdTraf = prod.IdProducto AND prod.IdFraccion =  " & idFraccion & _

            grdCabFactura.Rows.Clear()
            grdDetFactura.Rows.Clear()

            MyTablaPreCabFac.Rows.Clear()
            MyTablaPreCabFac = BD.ExecuteReturn(StrSql)
            If Not MyTablaPreCabFac Is Nothing Then
                If MyTablaPreCabFac.Rows.Count > 0 Then
                    grdCabFactura.Rows.Clear()
                    For i = 0 To MyTablaPreCabFac.Rows.Count - 1
                        'PARA FACTURACION AGRUPADA
                        If rdbMFAgrupado.Checked Then
                            'PARA FACTURACION AGRUPADA
                            If fNomEstado = "" And i = 0 Then
                                fNomEstado = MyTablaPreCabFac.DefaultView.Item(i).Item("NomEstado")
                                fIdEstado = MyTablaPreCabFac.DefaultView.Item(i).Item("idestado")
                            End If

                            If fOrigen = "" And i = 0 Then
                                fOrigen = MyTablaPreCabFac.DefaultView.Item(i).Item("Origen")
                            End If

                            If fTipoViaje = "" And i = 0 Then
                                fTipoViaje = MyTablaPreCabFac.DefaultView.Item(i).Item("TipoViaje")
                            End If

                            If fPrecio = 0 And i = 0 Then
                                fPrecio = MyTablaPreCabFac.DefaultView.Item(i).Item("precio")
                            End If

                            If fNomEstado = MyTablaPreCabFac.DefaultView.Item(i).Item("NomEstado") Then
                                If fOrigen = MyTablaPreCabFac.DefaultView.Item(i).Item("Origen") Then
                                    If fTipoViaje = MyTablaPreCabFac.DefaultView.Item(i).Item("TipoViaje") Then
                                        If fPrecio = MyTablaPreCabFac.DefaultView.Item(i).Item("precio") Then
                                            fTipoViaje = MyTablaPreCabFac.DefaultView.Item(i).Item("TipoViaje")
                                            fCantidad = fCantidad + MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga")
                                            fCantidadTOT = fCantidadTOT + MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga")
                                            fPrecio = MyTablaPreCabFac.DefaultView.Item(i).Item("precio")
                                            fPorcIVA = MyTablaPreCabFac.DefaultView.Item(i).Item("PorcIVA")
                                            fPorcRET = MyTablaPreCabFac.DefaultView.Item(i).Item("PorcRetencion")
                                            fTotKm = fTotKm + MyTablaPreCabFac.DefaultView.Item(i).Item("km")
                                            fTotKmTOT = fTotKmTOT + MyTablaPreCabFac.DefaultView.Item(i).Item("km")

                                            'fSubtotal = fSubtotal + MyTablaPreCabFac.DefaultView.Item(i).Item("subTotal")
                                            'fSubtotalTOT = fSubtotalTOT + MyTablaPreCabFac.DefaultView.Item(i).Item("subTotal")
                                            fSubtotal = fSubtotal + (MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio"))
                                            fSubtotalTOT = fSubtotalTOT + (MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio"))

                                            'fRetencion = fRetencion + MyTablaPreCabFac.DefaultView.Item(i).Item("ImpRetencion")
                                            'fRetencionTOT = fRetencionTOT + MyTablaPreCabFac.DefaultView.Item(i).Item("ImpRetencion")
                                            fRetencion = fRetencion + ((MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio")) * (MyTablaPreCabFac.DefaultView.Item(i).Item("PorcRetencion") / 100))
                                            fRetencionTOT = fRetencionTOT + ((MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio")) * (MyTablaPreCabFac.DefaultView.Item(i).Item("PorcRetencion") / 100))

                                            'fIMPIVA = fIMPIVA + MyTablaPreCabFac.DefaultView.Item(i).Item("ImpIVA")
                                            'fIMPIVATOT = fIMPIVATOT + MyTablaPreCabFac.DefaultView.Item(i).Item("ImpIVA")
                                            fIMPIVA = fIMPIVA + ((MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio")) * (MyTablaPreCabFac.DefaultView.Item(i).Item("PorcIVA") / 100))
                                            fIMPIVATOT = fIMPIVATOT + ((MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio")) * (MyTablaPreCabFac.DefaultView.Item(i).Item("PorcIVA") / 100))

                                            'fTotal = fTotal + MyTablaPreCabFac.DefaultView.Item(i).Item("subTotal") -
                                            'MyTablaPreCabFac.DefaultView.Item(i).Item("ImpRetencion") +
                                            'MyTablaPreCabFac.DefaultView.Item(i).Item("ImpIVA")
                                            'fTotalTOT = fTotalTOT + MyTablaPreCabFac.DefaultView.Item(i).Item("subTotal") -
                                            'MyTablaPreCabFac.DefaultView.Item(i).Item("ImpRetencion") +
                                            'MyTablaPreCabFac.DefaultView.Item(i).Item("ImpIVA")
                                            fTotal = fTotal + (MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio")) -
                                            (((MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio")) * (MyTablaPreCabFac.DefaultView.Item(i).Item("PorcRetencion") / 100))) +
                                            (((MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio")) * (MyTablaPreCabFac.DefaultView.Item(i).Item("PorcIVA") / 100)))
                                            fTotalTOT = fTotalTOT + (MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio")) -
                                            (((MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio")) * (MyTablaPreCabFac.DefaultView.Item(i).Item("PorcRetencion") / 100))) +
                                            (((MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio")) * (MyTablaPreCabFac.DefaultView.Item(i).Item("PorcIVA") / 100)))

                                        Else
                                            'CAMBIO DE PRECIO
                                            'Cambio Concepto
                                            'Agregar Concepto - Detalle
                                            grdDetFactura.Rows.Add()
                                            grdDetFactura.Item("gfdCantidad", ContDetalle).Value = fCantidad
                                            grdDetFactura.Item("gfdCIDPRODUCTO", ContDetalle).Value = CInt(ConsultaDatoServicio("TipoViaje", "CIDPRODUCTO", fTipoViaje, TipoDato.TdCadena, fIdEstado))
                                            '8 Feb 2017
                                            'grdDetFactura.Item("gfdCNOMBREPRODUCTO", ContDetalle).Value = ConsultaDatoServicio("TipoViaje", "CNOMBREPRODUCTO", fTipoViaje, TipoDato.TdCadena, fIdEstado) & " DE " & fNomEstado & IIf(fOrigen = "UMAN", "", " -" & fOrigen)
                                            grdDetFactura.Item("gfdCNOMBREPRODUCTO", ContDetalle).Value = ConsultaDatoServicio("TipoViaje", "CNOMBREPRODUCTO", fTipoViaje, TipoDato.TdCadena, fIdEstado)

                                            grdDetFactura.Item("gfdCCODIGOPRODUCTO", ContDetalle).Value = ConsultaDatoServicio("TipoViaje", "CCODIGOPRODUCTO", fTipoViaje, TipoDato.TdCadena, fIdEstado)

                                            grdDetFactura.Item("gfdPrecioUnit", ContDetalle).Value = fPrecio
                                            grdDetFactura.Item("gfdSubtotal", ContDetalle).Value = fSubtotal
                                            grdDetFactura.Item("gfdRetencion", ContDetalle).Value = fRetencion
                                            grdDetFactura.Item("gfdIVA", ContDetalle).Value = fIMPIVA
                                            grdDetFactura.Item("gfdTotal", ContDetalle).Value = fTotal
                                            grdDetFactura.Item("gfdTextoComp", ContDetalle).Value = ""
                                            grdDetFactura.Item("gfdTipoViaje", ContDetalle).Value = fTipoViaje
                                            grdDetFactura.Item("gfdKm", ContDetalle).Value = fTotKm
                                            grdDetFactura.Item("gfdPorcIVA", ContDetalle).Value = fPorcIVA
                                            grdDetFactura.Item("gfdPorcRET", ContDetalle).Value = fPorcRET

                                            ContDetalle += 1

                                            fCantidad = 0
                                            fPrecio = 0
                                            fTotKm = 0
                                            fSubtotal = 0
                                            fRetencion = 0
                                            fIMPIVA = 0
                                            fTotal = 0

                                            fTipoViaje = MyTablaPreCabFac.DefaultView.Item(i).Item("TipoViaje")
                                            fCantidad = fCantidad + MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga")
                                            fCantidadTOT = fCantidadTOT + MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga")
                                            fPrecio = MyTablaPreCabFac.DefaultView.Item(i).Item("precio")
                                            fPorcIVA = MyTablaPreCabFac.DefaultView.Item(i).Item("PorcIVA")
                                            fPorcRET = MyTablaPreCabFac.DefaultView.Item(i).Item("PorcRetencion")
                                            fTotKm = fTotKm + MyTablaPreCabFac.DefaultView.Item(i).Item("km")
                                            fTotKmTOT = fTotKmTOT + MyTablaPreCabFac.DefaultView.Item(i).Item("km")

                                            'fSubtotal = fSubtotal + MyTablaPreCabFac.DefaultView.Item(i).Item("subTotal")
                                            'fSubtotalTOT = fSubtotalTOT + MyTablaPreCabFac.DefaultView.Item(i).Item("subTotal")
                                            fSubtotal = fSubtotal + (MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio"))
                                            fSubtotalTOT = fSubtotalTOT + (MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio"))

                                            'fRetencion = fRetencion + MyTablaPreCabFac.DefaultView.Item(i).Item("ImpRetencion")
                                            'fRetencionTOT = fRetencionTOT + MyTablaPreCabFac.DefaultView.Item(i).Item("ImpRetencion")
                                            fRetencion = fRetencion + ((MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio")) * (MyTablaPreCabFac.DefaultView.Item(i).Item("PorcRetencion") / 100))
                                            fRetencionTOT = fRetencionTOT + ((MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio")) * (MyTablaPreCabFac.DefaultView.Item(i).Item("PorcRetencion") / 100))


                                            'fIMPIVA = fIMPIVA + MyTablaPreCabFac.DefaultView.Item(i).Item("ImpIVA")
                                            'fIMPIVATOT = fIMPIVATOT + MyTablaPreCabFac.DefaultView.Item(i).Item("ImpIVA")
                                            fIMPIVA = fIMPIVA + ((MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio")) * (MyTablaPreCabFac.DefaultView.Item(i).Item("PorcIVA") / 100))
                                            fIMPIVATOT = fIMPIVATOT + ((MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio")) * (MyTablaPreCabFac.DefaultView.Item(i).Item("PorcIVA") / 100))

                                            'fTotal = fTotal + MyTablaPreCabFac.DefaultView.Item(i).Item("subTotal") -
                                            'MyTablaPreCabFac.DefaultView.Item(i).Item("ImpRetencion") +
                                            'MyTablaPreCabFac.DefaultView.Item(i).Item("ImpIVA")
                                            'fTotalTOT = fTotalTOT + MyTablaPreCabFac.DefaultView.Item(i).Item("subTotal") -
                                            'MyTablaPreCabFac.DefaultView.Item(i).Item("ImpRetencion") +
                                            'MyTablaPreCabFac.DefaultView.Item(i).Item("ImpIVA")
                                            fTotal = fTotal + (MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio")) -
                                            (((MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio")) * (MyTablaPreCabFac.DefaultView.Item(i).Item("PorcRetencion") / 100))) +
                                            (((MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio")) * (MyTablaPreCabFac.DefaultView.Item(i).Item("PorcIVA") / 100)))
                                            fTotalTOT = fTotalTOT + (MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio")) -
                                            (((MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio")) * (MyTablaPreCabFac.DefaultView.Item(i).Item("PorcRetencion") / 100))) +
                                            (((MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio")) * (MyTablaPreCabFac.DefaultView.Item(i).Item("PorcIVA") / 100)))

                                        End If
                                    Else
                                        'CAMBIO DE TIPO
                                        'Cambio Concepto
                                        'Agregar Concepto - Detalle
                                        grdDetFactura.Rows.Add()
                                        grdDetFactura.Item("gfdCantidad", ContDetalle).Value = fCantidad
                                        grdDetFactura.Item("gfdCIDPRODUCTO", ContDetalle).Value = CInt(ConsultaDatoServicio("TipoViaje", "CIDPRODUCTO", fTipoViaje, TipoDato.TdCadena, fIdEstado))

                                        '8 FEB 2017
                                        'grdDetFactura.Item("gfdCNOMBREPRODUCTO", ContDetalle).Value = ConsultaDatoServicio("TipoViaje", "CNOMBREPRODUCTO", fTipoViaje, TipoDato.TdCadena, fIdEstado) & " DE " & fNomEstado & IIf(fOrigen = "UMAN", "", " -" & fOrigen)
                                        grdDetFactura.Item("gfdCNOMBREPRODUCTO", ContDetalle).Value = ConsultaDatoServicio("TipoViaje", "CNOMBREPRODUCTO", fTipoViaje, TipoDato.TdCadena, fIdEstado)

                                        grdDetFactura.Item("gfdCCODIGOPRODUCTO", ContDetalle).Value = ConsultaDatoServicio("TipoViaje", "CCODIGOPRODUCTO", fTipoViaje, TipoDato.TdCadena, fIdEstado)
                                        grdDetFactura.Item("gfdPrecioUnit", ContDetalle).Value = fPrecio
                                        grdDetFactura.Item("gfdSubtotal", ContDetalle).Value = fSubtotal
                                        grdDetFactura.Item("gfdRetencion", ContDetalle).Value = fRetencion
                                        grdDetFactura.Item("gfdIVA", ContDetalle).Value = fIMPIVA
                                        grdDetFactura.Item("gfdTotal", ContDetalle).Value = fTotal
                                        grdDetFactura.Item("gfdTextoComp", ContDetalle).Value = ""
                                        grdDetFactura.Item("gfdTipoViaje", ContDetalle).Value = fTipoViaje
                                        grdDetFactura.Item("gfdKm", ContDetalle).Value = fTotKm
                                        grdDetFactura.Item("gfdPorcIVA", ContDetalle).Value = fPorcIVA
                                        grdDetFactura.Item("gfdPorcRET", ContDetalle).Value = fPorcRET

                                        ContDetalle += 1

                                        fCantidad = 0
                                        fPrecio = 0
                                        fTotKm = 0
                                        fSubtotal = 0
                                        fRetencion = 0
                                        fIMPIVA = 0
                                        fTotal = 0

                                        fTipoViaje = MyTablaPreCabFac.DefaultView.Item(i).Item("TipoViaje")
                                        fCantidad = fCantidad + MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga")
                                        fCantidadTOT = fCantidadTOT + MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga")
                                        fPrecio = MyTablaPreCabFac.DefaultView.Item(i).Item("precio")
                                        fPorcIVA = MyTablaPreCabFac.DefaultView.Item(i).Item("PorcIVA")
                                        fPorcRET = MyTablaPreCabFac.DefaultView.Item(i).Item("PorcRetencion")
                                        fTotKm = fTotKm + MyTablaPreCabFac.DefaultView.Item(i).Item("km")
                                        fTotKmTOT = fTotKmTOT + MyTablaPreCabFac.DefaultView.Item(i).Item("km")

                                        'fSubtotal = fSubtotal + MyTablaPreCabFac.DefaultView.Item(i).Item("subTotal")
                                        'fSubtotalTOT = fSubtotalTOT + MyTablaPreCabFac.DefaultView.Item(i).Item("subTotal")
                                        fSubtotal = fSubtotal + (MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio"))
                                        fSubtotalTOT = fSubtotalTOT + (MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio"))

                                        'fRetencion = fRetencion + MyTablaPreCabFac.DefaultView.Item(i).Item("ImpRetencion")
                                        'fRetencionTOT = fRetencionTOT + MyTablaPreCabFac.DefaultView.Item(i).Item("ImpRetencion")
                                        fRetencion = fRetencion + ((MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio")) * (MyTablaPreCabFac.DefaultView.Item(i).Item("PorcRetencion") / 100))
                                        fRetencionTOT = fRetencionTOT + ((MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio")) * (MyTablaPreCabFac.DefaultView.Item(i).Item("PorcRetencion") / 100))

                                        'fIMPIVA = fIMPIVA + MyTablaPreCabFac.DefaultView.Item(i).Item("ImpIVA")
                                        'fIMPIVATOT = fIMPIVATOT + MyTablaPreCabFac.DefaultView.Item(i).Item("ImpIVA")
                                        fIMPIVA = fIMPIVA + ((MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio")) * (MyTablaPreCabFac.DefaultView.Item(i).Item("PorcIVA") / 100))
                                        fIMPIVATOT = fIMPIVATOT + ((MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio")) * (MyTablaPreCabFac.DefaultView.Item(i).Item("PorcIVA") / 100))

                                        'fTotal = fTotal + MyTablaPreCabFac.DefaultView.Item(i).Item("subTotal") -
                                        'MyTablaPreCabFac.DefaultView.Item(i).Item("ImpRetencion") +
                                        'MyTablaPreCabFac.DefaultView.Item(i).Item("ImpIVA")
                                        'fTotalTOT = fTotalTOT + MyTablaPreCabFac.DefaultView.Item(i).Item("subTotal") -
                                        'MyTablaPreCabFac.DefaultView.Item(i).Item("ImpRetencion") +
                                        'MyTablaPreCabFac.DefaultView.Item(i).Item("ImpIVA")
                                        fTotal = fTotal + (MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio")) -
                                        (((MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio")) * (MyTablaPreCabFac.DefaultView.Item(i).Item("PorcRetencion") / 100))) +
                                        (((MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio")) * (MyTablaPreCabFac.DefaultView.Item(i).Item("PorcIVA") / 100)))
                                        fTotalTOT = fTotalTOT + (MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio")) -
                                        (((MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio")) * (MyTablaPreCabFac.DefaultView.Item(i).Item("PorcRetencion") / 100))) +
                                        (((MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio")) * (MyTablaPreCabFac.DefaultView.Item(i).Item("PorcIVA") / 100)))

                                    End If
                                Else
                                    'CAMBIO DE ORIGEN
                                    'Agregar Concepto - Detalle
                                    grdDetFactura.Rows.Add()
                                    grdDetFactura.Item("gfdCantidad", ContDetalle).Value = fCantidad
                                    grdDetFactura.Item("gfdCIDPRODUCTO", ContDetalle).Value = CInt(ConsultaDatoServicio("TipoViaje", "CIDPRODUCTO", fTipoViaje, TipoDato.TdCadena, fIdEstado))

                                    '8 Feb 2017
                                    'grdDetFactura.Item("gfdCNOMBREPRODUCTO", ContDetalle).Value = ConsultaDatoServicio("TipoViaje", "CNOMBREPRODUCTO", fTipoViaje, TipoDato.TdCadena, fIdEstado) & " DE " & fNomEstado & IIf(fOrigen = "UMAN", "", " -" & fOrigen)
                                    grdDetFactura.Item("gfdCNOMBREPRODUCTO", ContDetalle).Value = ConsultaDatoServicio("TipoViaje", "CNOMBREPRODUCTO", fTipoViaje, TipoDato.TdCadena, fIdEstado)

                                    grdDetFactura.Item("gfdCCODIGOPRODUCTO", ContDetalle).Value = ConsultaDatoServicio("TipoViaje", "CCODIGOPRODUCTO", fTipoViaje, TipoDato.TdCadena, fIdEstado)
                                    grdDetFactura.Item("gfdPrecioUnit", ContDetalle).Value = fPrecio
                                    grdDetFactura.Item("gfdSubtotal", ContDetalle).Value = fSubtotal
                                    grdDetFactura.Item("gfdRetencion", ContDetalle).Value = fRetencion
                                    grdDetFactura.Item("gfdIVA", ContDetalle).Value = fIMPIVA
                                    grdDetFactura.Item("gfdTotal", ContDetalle).Value = fTotal
                                    grdDetFactura.Item("gfdTextoComp", ContDetalle).Value = ""
                                    grdDetFactura.Item("gfdTipoViaje", ContDetalle).Value = fTipoViaje
                                    grdDetFactura.Item("gfdKm", ContDetalle).Value = fTotKm
                                    grdDetFactura.Item("gfdPorcIVA", ContDetalle).Value = fPorcIVA
                                    grdDetFactura.Item("gfdPorcRET", ContDetalle).Value = fPorcRET

                                    ContDetalle += 1

                                    fCantidad = 0
                                    fPrecio = 0
                                    fTotKm = 0
                                    fSubtotal = 0
                                    fRetencion = 0
                                    fIMPIVA = 0
                                    fTotal = 0

                                    fTipoViaje = MyTablaPreCabFac.DefaultView.Item(i).Item("TipoViaje")
                                    fNomEstado = MyTablaPreCabFac.DefaultView.Item(i).Item("NomEstado")
                                    fIdEstado = MyTablaPreCabFac.DefaultView.Item(i).Item("idestado")

                                    fOrigen = MyTablaPreCabFac.DefaultView.Item(i).Item("Origen")

                                    fCantidad = fCantidad + MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga")
                                    fCantidadTOT = fCantidadTOT + MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga")
                                    fPrecio = MyTablaPreCabFac.DefaultView.Item(i).Item("precio")
                                    fPorcIVA = MyTablaPreCabFac.DefaultView.Item(i).Item("PorcIVA")
                                    fPorcRET = MyTablaPreCabFac.DefaultView.Item(i).Item("PorcRetencion")
                                    fTotKm = fTotKm + MyTablaPreCabFac.DefaultView.Item(i).Item("km")
                                    fTotKmTOT = fTotKmTOT + MyTablaPreCabFac.DefaultView.Item(i).Item("km")

                                    'fSubtotal = fSubtotal + MyTablaPreCabFac.DefaultView.Item(i).Item("subTotal")
                                    'fSubtotalTOT = fSubtotalTOT + MyTablaPreCabFac.DefaultView.Item(i).Item("subTotal")
                                    fSubtotal = fSubtotal + (MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio"))
                                    fSubtotalTOT = fSubtotalTOT + (MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio"))

                                    'fRetencion = fRetencion + MyTablaPreCabFac.DefaultView.Item(i).Item("ImpRetencion")
                                    'fRetencionTOT = fRetencionTOT + MyTablaPreCabFac.DefaultView.Item(i).Item("ImpRetencion")
                                    fRetencion = fRetencion + ((MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio")) * (MyTablaPreCabFac.DefaultView.Item(i).Item("PorcRetencion") / 100))
                                    fRetencionTOT = fRetencionTOT + ((MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio")) * (MyTablaPreCabFac.DefaultView.Item(i).Item("PorcRetencion") / 100))

                                    'fIMPIVA = fIMPIVA + MyTablaPreCabFac.DefaultView.Item(i).Item("ImpIVA")
                                    'fIMPIVATOT = fIMPIVATOT + MyTablaPreCabFac.DefaultView.Item(i).Item("ImpIVA")
                                    fIMPIVA = fIMPIVA + ((MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio")) * (MyTablaPreCabFac.DefaultView.Item(i).Item("PorcIVA") / 100))
                                    fIMPIVATOT = fIMPIVATOT + ((MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio")) * (MyTablaPreCabFac.DefaultView.Item(i).Item("PorcIVA") / 100))

                                    'fTotal = fTotal + MyTablaPreCabFac.DefaultView.Item(i).Item("subTotal") -
                                    'MyTablaPreCabFac.DefaultView.Item(i).Item("ImpRetencion") +
                                    'MyTablaPreCabFac.DefaultView.Item(i).Item("ImpIVA")
                                    'fTotalTOT = fTotalTOT + MyTablaPreCabFac.DefaultView.Item(i).Item("subTotal") -
                                    'MyTablaPreCabFac.DefaultView.Item(i).Item("ImpRetencion") +
                                    'MyTablaPreCabFac.DefaultView.Item(i).Item("ImpIVA")
                                    fTotal = fTotal + (MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio")) -
                                    (((MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio")) * (MyTablaPreCabFac.DefaultView.Item(i).Item("PorcRetencion") / 100))) +
                                    (((MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio")) * (MyTablaPreCabFac.DefaultView.Item(i).Item("PorcIVA") / 100)))
                                    fTotalTOT = fTotalTOT + (MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio")) -
                                    (((MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio")) * (MyTablaPreCabFac.DefaultView.Item(i).Item("PorcRetencion") / 100))) +
                                    (((MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio")) * (MyTablaPreCabFac.DefaultView.Item(i).Item("PorcIVA") / 100)))

                                End If
                            Else
                                'CAMBIO DE ESTADO
                                'Agregar Concepto - Detalle
                                grdDetFactura.Rows.Add()
                                grdDetFactura.Item("gfdCantidad", ContDetalle).Value = fCantidad
                                grdDetFactura.Item("gfdCIDPRODUCTO", ContDetalle).Value = CInt(ConsultaDatoServicio("TipoViaje", "CIDPRODUCTO", fTipoViaje, TipoDato.TdCadena, fIdEstado))

                                '8 Feb 2017
                                'grdDetFactura.Item("gfdCNOMBREPRODUCTO", ContDetalle).Value = ConsultaDatoServicio("TipoViaje", "CNOMBREPRODUCTO", fTipoViaje, TipoDato.TdCadena, fIdEstado) & " DE " & fNomEstado & IIf(fOrigen = "UMAN", "", " -" & fOrigen)
                                grdDetFactura.Item("gfdCNOMBREPRODUCTO", ContDetalle).Value = ConsultaDatoServicio("TipoViaje", "CNOMBREPRODUCTO", fTipoViaje, TipoDato.TdCadena, fIdEstado)

                                grdDetFactura.Item("gfdCCODIGOPRODUCTO", ContDetalle).Value = ConsultaDatoServicio("TipoViaje", "CCODIGOPRODUCTO", fTipoViaje, TipoDato.TdCadena, fIdEstado)
                                grdDetFactura.Item("gfdPrecioUnit", ContDetalle).Value = fPrecio
                                grdDetFactura.Item("gfdSubtotal", ContDetalle).Value = fSubtotal
                                grdDetFactura.Item("gfdRetencion", ContDetalle).Value = fRetencion
                                grdDetFactura.Item("gfdIVA", ContDetalle).Value = fIMPIVA
                                grdDetFactura.Item("gfdTotal", ContDetalle).Value = fTotal
                                grdDetFactura.Item("gfdTextoComp", ContDetalle).Value = ""
                                grdDetFactura.Item("gfdTipoViaje", ContDetalle).Value = fTipoViaje
                                grdDetFactura.Item("gfdKm", ContDetalle).Value = fTotKm
                                grdDetFactura.Item("gfdPorcIVA", ContDetalle).Value = fPorcIVA
                                grdDetFactura.Item("gfdPorcRET", ContDetalle).Value = fPorcRET

                                ContDetalle += 1

                                fCantidad = 0
                                fPrecio = 0
                                fTotKm = 0
                                fSubtotal = 0
                                fRetencion = 0
                                fIMPIVA = 0
                                fTotal = 0

                                fTipoViaje = MyTablaPreCabFac.DefaultView.Item(i).Item("TipoViaje")
                                fNomEstado = MyTablaPreCabFac.DefaultView.Item(i).Item("NomEstado")
                                fIdEstado = MyTablaPreCabFac.DefaultView.Item(i).Item("idestado")

                                fOrigen = MyTablaPreCabFac.DefaultView.Item(i).Item("Origen")

                                fCantidad = fCantidad + MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga")
                                fCantidadTOT = fCantidadTOT + MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga")
                                fPrecio = MyTablaPreCabFac.DefaultView.Item(i).Item("precio")
                                fPorcIVA = MyTablaPreCabFac.DefaultView.Item(i).Item("PorcIVA")
                                fPorcRET = MyTablaPreCabFac.DefaultView.Item(i).Item("PorcRetencion")
                                fTotKm = fTotKm + MyTablaPreCabFac.DefaultView.Item(i).Item("km")
                                fTotKmTOT = fTotKmTOT + MyTablaPreCabFac.DefaultView.Item(i).Item("km")

                                'fSubtotal = fSubtotal + MyTablaPreCabFac.DefaultView.Item(i).Item("subTotal")
                                'fSubtotalTOT = fSubtotalTOT + MyTablaPreCabFac.DefaultView.Item(i).Item("subTotal")
                                fSubtotal = fSubtotal + (MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio"))
                                fSubtotalTOT = fSubtotalTOT + (MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio"))

                                'fRetencion = fRetencion + MyTablaPreCabFac.DefaultView.Item(i).Item("ImpRetencion")
                                'fRetencionTOT = fRetencionTOT + MyTablaPreCabFac.DefaultView.Item(i).Item("ImpRetencion")
                                fRetencion = fRetencion + ((MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio")) * (MyTablaPreCabFac.DefaultView.Item(i).Item("PorcRetencion") / 100))
                                fRetencionTOT = fRetencionTOT + ((MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio")) * (MyTablaPreCabFac.DefaultView.Item(i).Item("PorcRetencion") / 100))


                                'fIMPIVA = fIMPIVA + MyTablaPreCabFac.DefaultView.Item(i).Item("ImpIVA")
                                'fIMPIVATOT = fIMPIVATOT + MyTablaPreCabFac.DefaultView.Item(i).Item("ImpIVA")
                                fIMPIVA = fIMPIVA + ((MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio")) * (MyTablaPreCabFac.DefaultView.Item(i).Item("PorcIVA") / 100))
                                fIMPIVATOT = fIMPIVATOT + ((MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio")) * (MyTablaPreCabFac.DefaultView.Item(i).Item("PorcIVA") / 100))

                                'fTotal = fTotal + MyTablaPreCabFac.DefaultView.Item(i).Item("subTotal") -
                                'MyTablaPreCabFac.DefaultView.Item(i).Item("ImpRetencion") +
                                'MyTablaPreCabFac.DefaultView.Item(i).Item("ImpIVA")
                                'fTotalTOT = fTotalTOT + MyTablaPreCabFac.DefaultView.Item(i).Item("subTotal") -
                                'MyTablaPreCabFac.DefaultView.Item(i).Item("ImpRetencion") +
                                'MyTablaPreCabFac.DefaultView.Item(i).Item("ImpIVA")
                                fTotal = fTotal + (MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio")) -
                                (((MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio")) * (MyTablaPreCabFac.DefaultView.Item(i).Item("PorcRetencion") / 100))) +
                                (((MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio")) * (MyTablaPreCabFac.DefaultView.Item(i).Item("PorcIVA") / 100)))
                                fTotalTOT = fTotalTOT + (MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio")) -
                                (((MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio")) * (MyTablaPreCabFac.DefaultView.Item(i).Item("PorcRetencion") / 100))) +
                                (((MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio")) * (MyTablaPreCabFac.DefaultView.Item(i).Item("PorcIVA") / 100)))

                            End If

                            If i = MyTablaPreCabFac.Rows.Count - 1 Then
                                'CABECERO
                                grdCabFactura.Rows.Add()
                                grdCabFactura.Item("gfConsecutivo", ContCabecero).Value = ContCabecero + 1
                                grdCabFactura.Item("gfidCliente", ContCabecero).Value = idCliente
                                grdCabFactura.Item("gfNomCliente", ContCabecero).Value = NomCliente
                                grdCabFactura.Item("gfFecha", ContCabecero).Value = Now
                                grdCabFactura.Item("gfFechaVenc", ContCabecero).Value = Now
                                grdCabFactura.Item("gfSubtotal", ContCabecero).Value = fSubtotalTOT
                                grdCabFactura.Item("gfRetencion", ContCabecero).Value = fRetencionTOT
                                grdCabFactura.Item("gfIVA", ContCabecero).Value = fIMPIVATOT
                                grdCabFactura.Item("gfTOTAL", ContCabecero).Value = fTotalTOT
                                grdCabFactura.Item("gfKm", ContCabecero).Value = fTotKmTOT
                                grdCabFactura.Item("gfCantidad", ContCabecero).Value = fCantidadTOT

                                ContCabecero += 1

                                'DETALLE 
                                grdDetFactura.Rows.Add()
                                grdDetFactura.Item("gfdCantidad", ContDetalle).Value = fCantidad
                                grdDetFactura.Item("gfdCIDPRODUCTO", ContDetalle).Value = CInt(ConsultaDatoServicio("TipoViaje", "CIDPRODUCTO", fTipoViaje, TipoDato.TdCadena, fIdEstado))

                                '8 Feb 2017
                                'grdDetFactura.Item("gfdCNOMBREPRODUCTO", ContDetalle).Value = ConsultaDatoServicio("TipoViaje", "CNOMBREPRODUCTO", fTipoViaje, TipoDato.TdCadena, fIdEstado) & " DE " & fNomEstado & IIf(fOrigen = "UMAN", "", " -" & fOrigen)
                                grdDetFactura.Item("gfdCNOMBREPRODUCTO", ContDetalle).Value = ConsultaDatoServicio("TipoViaje", "CNOMBREPRODUCTO", fTipoViaje, TipoDato.TdCadena, fIdEstado)

                                grdDetFactura.Item("gfdCCODIGOPRODUCTO", ContDetalle).Value = ConsultaDatoServicio("TipoViaje", "CCODIGOPRODUCTO", fTipoViaje, TipoDato.TdCadena, fIdEstado)
                                grdDetFactura.Item("gfdPrecioUnit", ContDetalle).Value = fPrecio
                                grdDetFactura.Item("gfdSubtotal", ContDetalle).Value = fSubtotal
                                grdDetFactura.Item("gfdRetencion", ContDetalle).Value = fRetencion
                                grdDetFactura.Item("gfdIVA", ContDetalle).Value = fIMPIVA
                                grdDetFactura.Item("gfdTotal", ContDetalle).Value = fTotal
                                grdDetFactura.Item("gfdTextoComp", ContDetalle).Value = ""
                                grdDetFactura.Item("gfdTipoViaje", ContDetalle).Value = fTipoViaje
                                grdDetFactura.Item("gfdKm", ContDetalle).Value = fTotKm
                                grdDetFactura.Item("gfdPorcIVA", ContDetalle).Value = fPorcIVA
                                grdDetFactura.Item("gfdPorcRET", ContDetalle).Value = fPorcRET


                                ContDetalle += 1

                                fCantidad = 0
                                fPrecio = 0
                                fTotKm = 0
                                fSubtotal = 0
                                fRetencion = 0
                                fIMPIVA = 0
                                fTotal = 0
                                fTotKmTOT = 0
                                fSubtotalTOT = 0
                                fRetencionTOT = 0
                                fIMPIVATOT = 0
                                fTotalTOT = 0
                                grdCabFactura.PerformLayout()
                            End If
                            'FIN ----- > PARA FACTURACION AGRUPADA

                            'FACTURACION GLOBAL
                        ElseIf rdbMFGlobal.Checked Then
                            'INICIO ----> FACTURACION GLOBAL

                            'If fTipoViaje = "" And i = 0 Then
                            '    fTipoViaje = MyTablaPreCabFac.DefaultView.Item(i).Item("TipoViaje")
                            'End If
                            'If fPrecio = 0 And i = 0 Then
                            '    fPrecio = MyTablaPreCabFac.DefaultView.Item(i).Item("precio")
                            'End If

                            fCantidad = fCantidad + MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga")
                            fPrecio = fPrecio + (MyTablaPreCabFac.DefaultView.Item(i).Item("precio") * MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga"))
                            fPorcIVA = MyTablaPreCabFac.DefaultView.Item(i).Item("PorcIVA")
                            fPorcRET = MyTablaPreCabFac.DefaultView.Item(i).Item("PorcRetencion")
                            fTotKm = fTotKm + MyTablaPreCabFac.DefaultView.Item(i).Item("km")

                            'fSubtotal = fSubtotal + MyTablaPreCabFac.DefaultView.Item(i).Item("subTotal")
                            fSubtotal = fSubtotal + (MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio"))

                            'fRetencion = fRetencion + MyTablaPreCabFac.DefaultView.Item(i).Item("ImpRetencion")
                            fRetencion = fRetencion + ((MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio")) * (MyTablaPreCabFac.DefaultView.Item(i).Item("PorcRetencion") / 100))

                            'fIMPIVA = fIMPIVA + MyTablaPreCabFac.DefaultView.Item(i).Item("ImpIVA")
                            fIMPIVA = fIMPIVA + ((MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio")) * (MyTablaPreCabFac.DefaultView.Item(i).Item("PorcIVA") / 100))

                            'fTotal = fTotal + MyTablaPreCabFac.DefaultView.Item(i).Item("subTotal") -
                            'MyTablaPreCabFac.DefaultView.Item(i).Item("ImpRetencion") +
                            'MyTablaPreCabFac.DefaultView.Item(i).Item("ImpIVA")
                            fTotal = fTotal + (MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio")) -
                            (((MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio")) * (MyTablaPreCabFac.DefaultView.Item(i).Item("PorcRetencion") / 100))) +
                            (((MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio")) * (MyTablaPreCabFac.DefaultView.Item(i).Item("PorcIVA") / 100)))


                            If i = MyTablaPreCabFac.Rows.Count - 1 Then
                                'CABECERO
                                grdCabFactura.Rows.Add()
                                grdCabFactura.Item("gfConsecutivo", ContCabecero).Value = ContCabecero + 1
                                grdCabFactura.Item("gfidCliente", ContCabecero).Value = idCliente
                                grdCabFactura.Item("gfNomCliente", ContCabecero).Value = NomCliente
                                grdCabFactura.Item("gfFecha", ContCabecero).Value = Now
                                grdCabFactura.Item("gfFechaVenc", ContCabecero).Value = Now
                                grdCabFactura.Item("gfSubtotal", ContCabecero).Value = fSubtotal
                                grdCabFactura.Item("gfRetencion", ContCabecero).Value = fRetencion
                                grdCabFactura.Item("gfIVA", ContCabecero).Value = fIMPIVA
                                grdCabFactura.Item("gfTOTAL", ContCabecero).Value = fTotal
                                grdCabFactura.Item("gfKm", ContCabecero).Value = fTotKm
                                grdCabFactura.Item("gfCantidad", ContCabecero).Value = fCantidad

                                ContCabecero += 1

                                'DETALLE 
                                grdDetFactura.Rows.Add()
                                'grdDetFactura.Item("gfdCantidad", ContDetalle).Value = fCantidad
                                grdDetFactura.Item("gfdCantidad", ContDetalle).Value = 1
                                grdDetFactura.Item("gfdCIDPRODUCTO", ContDetalle).Value = CInt(ConsultaDatoServicio("TipoViaje", "CIDPRODUCTO", "A", TipoDato.TdCadena, fIdEstado))

                                grdDetFactura.Item("gfdCNOMBREPRODUCTO", ContDetalle).Value = ConsultaDatoServicio("TipoViaje", "CNOMBREPRODUCTO", "A", TipoDato.TdCadena, fIdEstado)

                                'grdDetFactura.Item("gfdCNOMBREPRODUCTO", ContDetalle).Value = ConsultaDatoServicio("TipoViaje", "CNOMBREPRODUCTO", "A", TipoDato.TdCadena) & _
                                'IIf(fNomEstado = "", "", " DE " & fNomEstado) & IIf(fOrigen = "UMAN", "", " -" & fOrigen)

                                grdDetFactura.Item("gfdCCODIGOPRODUCTO", ContDetalle).Value = ConsultaDatoServicio("TipoViaje", "CCODIGOPRODUCTO", "A", TipoDato.TdCadena, fIdEstado)
                                grdDetFactura.Item("gfdPrecioUnit", ContDetalle).Value = fPrecio
                                grdDetFactura.Item("gfdSubtotal", ContDetalle).Value = fSubtotal
                                grdDetFactura.Item("gfdRetencion", ContDetalle).Value = fRetencion
                                grdDetFactura.Item("gfdIVA", ContDetalle).Value = fIMPIVA
                                grdDetFactura.Item("gfdTotal", ContDetalle).Value = fTotal
                                grdDetFactura.Item("gfdTextoComp", ContDetalle).Value = ""
                                grdDetFactura.Item("gfdTipoViaje", ContDetalle).Value = fTipoViaje
                                grdDetFactura.Item("gfdKm", ContDetalle).Value = fTotKm
                                grdDetFactura.Item("gfdPorcIVA", ContDetalle).Value = fPorcIVA
                                grdDetFactura.Item("gfdPorcRET", ContDetalle).Value = fPorcRET


                                ContDetalle += 1

                                fCantidad = 0
                                fPrecio = 0
                                fTotKm = 0
                                fSubtotal = 0
                                fRetencion = 0
                                fIMPIVA = 0
                                fTotal = 0
                                fTotKmTOT = 0
                                fSubtotalTOT = 0
                                fRetencionTOT = 0
                                fIMPIVATOT = 0
                                fTotalTOT = 0
                                grdCabFactura.PerformLayout()
                            End If
                            'FIN ----> FACTURACION GLOBAL
                        ElseIf rdbMFDetalle.Checked Then
                            'INICIO ----> FACTURACION DETALLE
                            fCantidad = fCantidad + MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga")
                            fPrecio = fPrecio + (MyTablaPreCabFac.DefaultView.Item(i).Item("precio") * MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga"))
                            fPorcIVA = MyTablaPreCabFac.DefaultView.Item(i).Item("PorcIVA")
                            fPorcRET = MyTablaPreCabFac.DefaultView.Item(i).Item("PorcRetencion")
                            fTotKm = fTotKm + MyTablaPreCabFac.DefaultView.Item(i).Item("km")

                            'fSubtotal = fSubtotal + MyTablaPreCabFac.DefaultView.Item(i).Item("subTotal")
                            fSubtotal = fSubtotal + (MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio"))

                            'fRetencion = fRetencion + MyTablaPreCabFac.DefaultView.Item(i).Item("ImpRetencion")
                            fRetencion = fRetencion + ((MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio")) * (MyTablaPreCabFac.DefaultView.Item(i).Item("PorcRetencion") / 100))

                            'fIMPIVA = fIMPIVA + MyTablaPreCabFac.DefaultView.Item(i).Item("ImpIVA")
                            fIMPIVA = fIMPIVA + ((MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio")) * (MyTablaPreCabFac.DefaultView.Item(i).Item("PorcIVA") / 100))

                            'fTotal = fTotal + MyTablaPreCabFac.DefaultView.Item(i).Item("subTotal") -
                            'MyTablaPreCabFac.DefaultView.Item(i).Item("ImpRetencion") +
                            'MyTablaPreCabFac.DefaultView.Item(i).Item("ImpIVA")
                            fTotal = fTotal + (MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio")) -
                            (((MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio")) * (MyTablaPreCabFac.DefaultView.Item(i).Item("PorcRetencion") / 100))) +
                            (((MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga") * MyTablaPreCabFac.DefaultView.Item(i).Item("precio")) * (MyTablaPreCabFac.DefaultView.Item(i).Item("PorcIVA") / 100)))


                            'DETALLE 
                            grdDetFactura.Rows.Add()
                            'grdDetFactura.Item("gfdCantidad", ContDetalle).Value = fCantidad
                            grdDetFactura.Item("gfdCantidad", ContDetalle).Value = MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga")
                            grdDetFactura.Item("gfdCIDPRODUCTO", ContDetalle).Value = MyTablaPreCabFac.DefaultView.Item(i).Item("Clave")

                            grdDetFactura.Item("gfdCNOMBREPRODUCTO", ContDetalle).Value = MyTablaPreCabFac.DefaultView.Item(i).Item("Destino")

                            grdDetFactura.Item("gfdCCODIGOPRODUCTO", ContDetalle).Value = MyTablaPreCabFac.DefaultView.Item(i).Item("Clave")
                            grdDetFactura.Item("gfdPrecioUnit", ContDetalle).Value = MyTablaPreCabFac.DefaultView.Item(i).Item("precio")
                            grdDetFactura.Item("gfdSubtotal", ContDetalle).Value = MyTablaPreCabFac.DefaultView.Item(i).Item("precio") * MyTablaPreCabFac.DefaultView.Item(i).Item("VolDescarga")
                            grdDetFactura.Item("gfdRetencion", ContDetalle).Value = MyTablaPreCabFac.DefaultView.Item(i).Item("ImpRetencion")
                            grdDetFactura.Item("gfdIVA", ContDetalle).Value = MyTablaPreCabFac.DefaultView.Item(i).Item("ImpIVA")
                            grdDetFactura.Item("gfdTotal", ContDetalle).Value = MyTablaPreCabFac.DefaultView.Item(i).Item("subTotal") -
                            MyTablaPreCabFac.DefaultView.Item(i).Item("ImpRetencion") +
                            MyTablaPreCabFac.DefaultView.Item(i).Item("ImpIVA")
                            grdDetFactura.Item("gfdTextoComp", ContDetalle).Value = ""
                            grdDetFactura.Item("gfdTipoViaje", ContDetalle).Value = MyTablaPreCabFac.DefaultView.Item(i).Item("TipoViaje")
                            grdDetFactura.Item("gfdKm", ContDetalle).Value = MyTablaPreCabFac.DefaultView.Item(i).Item("km")
                            grdDetFactura.Item("gfdPorcIVA", ContDetalle).Value = MyTablaPreCabFac.DefaultView.Item(i).Item("PorcIVA")
                            grdDetFactura.Item("gfdPorcRET", ContDetalle).Value = MyTablaPreCabFac.DefaultView.Item(i).Item("PorcRetencion")


                            ContDetalle += 1

                            If i = MyTablaPreCabFac.Rows.Count - 1 Then
                                'CABECERO
                                grdCabFactura.Rows.Add()
                                grdCabFactura.Item("gfConsecutivo", ContCabecero).Value = ContCabecero + 1
                                grdCabFactura.Item("gfidCliente", ContCabecero).Value = idCliente
                                grdCabFactura.Item("gfNomCliente", ContCabecero).Value = NomCliente
                                grdCabFactura.Item("gfFecha", ContCabecero).Value = Now
                                grdCabFactura.Item("gfFechaVenc", ContCabecero).Value = Now
                                grdCabFactura.Item("gfSubtotal", ContCabecero).Value = fSubtotal
                                grdCabFactura.Item("gfRetencion", ContCabecero).Value = fRetencion
                                grdCabFactura.Item("gfIVA", ContCabecero).Value = fIMPIVA
                                grdCabFactura.Item("gfTOTAL", ContCabecero).Value = fTotal
                                grdCabFactura.Item("gfKm", ContCabecero).Value = fTotKm
                                grdCabFactura.Item("gfCantidad", ContCabecero).Value = fCantidad

                                ContCabecero += 1

                                fCantidad = 0
                                fPrecio = 0
                                fTotKm = 0
                                fSubtotal = 0
                                fRetencion = 0
                                fIMPIVA = 0
                                fTotal = 0
                                fTotKmTOT = 0
                                fSubtotalTOT = 0
                                fRetencionTOT = 0
                                fIMPIVATOT = 0
                                fTotalTOT = 0
                                grdCabFactura.PerformLayout()
                            End If
                            'FIN ----> FACTURACION DETALLE
                        End If


                    Next


                    ''PRODUCTOS
                    'If rdbMFDetalle.Checked Then
                    '    CreaServiciosCOM(idCliente, "S")
                    'End If
                End If
            End If
            grdDetFactura.PerformLayout()

        Else
            MsgBox("No hay Viajes Marcados para Facturar", MsgBoxStyle.Information, Me.Text)
        End If
    End Sub
    Private Sub btnMnuPreFac_Click(sender As Object, e As EventArgs) Handles btnMnuPreFac.Click

        If grdViajes.RowCount > 0 Then
            'idFraccion = grdViajes.Item("gvFraccion", grdViajes.CurrentCell.RowIndex).Value
            CreaFactura(cmbClientes.SelectedValue, cmbClientes.Text)
            TabControl1.SelectedIndex = 1
            GpoTexto.Enabled = True
            txtTexto.Enabled = True
        End If
    End Sub



    Private Sub chkITFecha_CheckedChanged(sender As Object, e As EventArgs) Handles chkITFecha.CheckedChanged
        InsertarPreTexto()
    End Sub

    Private Sub InsertarPreTexto()
        txtTexto.Text = ""

        If chkITFecha.Checked Then
            If rdbFecRango.Checked Then
                txtTexto.Text = " DEL " & dtpFechaIni.Value.ToShortDateString & " AL " & dtpFechaFin.Value.ToShortDateString
            Else
                txtTexto.Text = " AL " & Format(Now, "d MMMM yyyy")
            End If

        End If
        If rdbMFGlobal.Checked Then
            If chkITTon.Checked Then
                If grdCabFactura.RowCount > 0 Then
                    txtTexto.Text = txtTexto.Text & " " & grdCabFactura.Item("gfKm", 0).Value & " KM. RECORRIDOS"
                End If
            End If
            If chkITKm.Checked Then
                If grdCabFactura.RowCount > 0 Then
                    txtTexto.Text = txtTexto.Text & " " & grdCabFactura.Item("gfCantidad", 0).Value & " TONS. TRASLAPADAS"
                End If
            End If
        End If


    End Sub

    Private Sub chkITTon_CheckedChanged(sender As Object, e As EventArgs) Handles chkITTon.CheckedChanged
        InsertarPreTexto()
    End Sub

    Private Sub chkITKm_CheckedChanged(sender As Object, e As EventArgs) Handles chkITKm.CheckedChanged
        InsertarPreTexto()
    End Sub

    Private Sub cmdInsTexto_Click(sender As Object, e As EventArgs) Handles cmdInsTexto.Click
        Dim TextoFinal As String = ""
        If grdDetFactura.RowCount > 0 Then
            For i = 0 To grdDetFactura.RowCount - 1
                If txtTexto.Text <> "" Then
                    TextoFinal = txtTexto.Text
                End If
                If rdbMFAgrupado.Checked Then
                    If chkITTon.Checked Then
                        TextoFinal = TextoFinal & " " & grdDetFactura.Item("gfdKm", i).Value & " KM. RECORRIDOS"
                    End If
                    If chkITKm.Checked Then
                        TextoFinal = TextoFinal & " " & grdDetFactura.Item("gfdCantidad", i).Value & " TONS. TRASLAPADAS"
                    End If
                End If

                grdDetFactura.Item("gfdTextoComp", i).Value = TextoFinal
            Next
        End If
    End Sub

    Private Function IniciarSesionSDK() As Boolean
        'frmLoginSDK.Close()
        BDCOM = New CapaDatos.UtilSQL(_sqlConCom, "Juan")
        Dim BDCOMConfig = New CapaDatos.UtilSQL(_conConfigl, "Juan")
        Dim log As New frmLoginSDK(BDCOM, BDCOMConfig)
        If log.ShowDialog = Windows.Forms.DialogResult.OK Then
            sRutaEmpresaAdmPAQ = log.vRuta

            log.Dispose()

            Return True
        Else
            log.Dispose()
            Return False
        End If


    End Function



    Private Sub btnMnuOk_Click(sender As Object, e As EventArgs) Handles btnMnuOk.Click
        Dim BandGraba As Boolean = False
        Dim objTool As New ToolSQLs
        Dim codigoCliente As String = ""
        Dim IdCliente As Integer = 0
        Dim SubTotal As Double = 0
        Dim IVA As Double = 0
        Dim Retencion As Double = 0
        Dim km As Double = 0
        Dim Toneladas As Double = 0
        Dim NoMovimiento As Integer = 0


        Try
            'If Not Validar() Then Exit Sub
            'Dim tipoDocto As String = BuscaDatoCombo(MyTablaConcepCOM, "CCODIGOCONCEPTO", TipDocumento, TipoDato.TdCadena, "CIDCONCEPTODOCUMENTO")
            'Dim TipDocumentoID As Integer = BuscaDatoCombo(MyTablaConcepCOM, "CCODIGOCONCEPTO", TipDocumento, TipoDato.TdCadena, "CIDCONCE01")
            'Dim TextoDocumento As String = Trim(cmbDocumCOM.Text)
            'BandCreditoConcepto = BuscaDatoCombo(MyTablaConcepCOM, "CCODIGOCONCEPTO", TipDocumento, TipoDato.TdBoolean, "CDOCTOACREDITO")
            TipDocumento = cmbDocumCOM.SelectedValue
            codigoCliente = cmbClientes.SelectedValue

            'GoTo Prueba




            Salida = True
            cmbDocumCOM.SelectedValue = TipDocumento

            If grdCabFactura.Rows.Count > 0 Then
                'OrigenFolioSiguiente = UltimoConsecutivoContpaq(TipDocumento, tipoDocto, TipDocumentoID)

                If IniciarSesionSDK() = False Then
                    'MessageBox.Show("vcvxcvx")

                End If
                'bEmpresaSDKAbierta = True
                If bEmpresaSDKAbierta Then
                    'OBTENER DATOS DEL CLIENTE

                    ClientesCOM = New ClientesComClass(codigoCliente)
                    If ClientesCOM.Existe Then
                        DiasCredito = ClientesCOM.CDIASCREDITOCLIENTE
                        CliRFC = ClientesCOM.CRFC
                        IdCliente = ClientesCOM.CIDCLIENTEPROVEEDOR

                    End If
                    TipDocumento = Val(Trim(cmbDocumCOM.SelectedValue))

                    'OBTENER DATOS DEL CONCEPTO
                    ConceptosCOM = New ConceptosCOMClass(TipDocumento)
                    If ConceptosCOM.Existe Then
                        tipoDocto = ConceptosCOM.CIDCONCEPTODOCUMENTO
                        TipDocumentoID = ConceptosCOM.CIDDOCUMENTODE
                        TextoDocumento = ConceptosCOM.CNOMBRECONCEPTO
                        BandCreditoConcepto = ConceptosCOM.CDOCTOACREDITO
                        lPlantilla = ConceptosCOM.CPLAMIGCFD
                        lRutaEntDocs = ConceptosCOM.CRUTAENTREGA
                        lPrefijo = ConceptosCOM.CPREFICON

                    End If

                    If BandCreditoConcepto Then
                        FechaVencimiento = DateAdd(DateInterval.Day, DiasCredito, FechaFactura)
                    Else
                        FechaVencimiento = Now
                    End If

                    If tipoDocto = "3" Then
                        TIPO_MOV = "REM"
                    ElseIf tipoDocto = "4" Then
                        TIPO_MOV = "FAC"
                    ElseIf tipoDocto = "2" Then
                        TIPO_MOV = "PED"
                    End If



                    'Probar con esta funcion 29/Mar/2017
                    OrigenFolioSiguiente = UltimoConsecutivoContpaq(Trim(TipDocumento), vSerie, vFolio)
                    If OrigenFolioSiguiente <> "" Then
                        lError = 0
                    End If

                    'Probar con esta funcion 29/Mar/2017
                    'lError = fSiguienteFolio(Trim(TipDocumento), vSerie, vFolio)
                    If lError = 0 Then
                        If vSerie = "" Then
                            vSerie = ConceptosCOM.CSERIEPOROMISION
                        End If
                        If MsgBox("!! Esta seguro que desea Aplicar el Documento: " & TextoDocumento & " : " & vSerie & " " & vFolio & " ? !!", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                            'Para Interno
                            NoMovimiento = UltimoConsecutivo()

                            FEspera.StartPosition = FormStartPosition.CenterScreen
                            FEspera.Show("Generando Factura AdminPAQ ...")

                            'If AgregaCabeceroDetalle_SDK(TipDocumento, tipoDocto, vSerie, vFolio, codigoCliente) Then
                            If AgregaCabeceroDetalle_SDK_ALTA(TipDocumento, tipoDocto, vSerie, vFolio, codigoCliente) Then
                                '
                                'BandGraba = True
                                'Prueba:
                                FEspera.Show("Guardando Datos Locales ...")

                                indice = 0

                                SubTotal = grdCabFactura.Item("gfSubtotal", 0).Value
                                IVA = grdCabFactura.Item("gfIVA", 0).Value
                                Retencion = grdCabFactura.Item("gfRetencion", 0).Value
                                km = grdCabFactura.Item("gfKm", 0).Value
                                Toneladas = grdCabFactura.Item("gfCantidad", 0).Value

                                'CABECERO DEL DOMUMENTO
                                StrSql = objTool.InsertaCabeceroMovto(NoMovimiento, vFolio, vSerie, TIPO_MOV,
                               codigoCliente, FechaFactura, SubTotal, 0, IVA, "TER", 0,
                                "", UserId, sIdEmpresa, "", TipDocumento,
                                FechaVencimiento, FechaFactura, UserId, Now(), UserId, Now(), 1,
                                1, "", "", "", "", "", "", True, False, False, Retencion, km, Toneladas)
                                ReDim Preserve ArraySql(indice)
                                ArraySql(indice) = StrSql
                                indice += 1

                                'D E T A L L E
                                For i = 0 To grdDetFactura.Rows.Count - 1
                                    If grdDetFactura.Item("gfdPrecioUnit", i).Value > 0 Then

                                        StrSql = objTool.InsertaDetalleMovto(NoMovimiento, vFolio, vSerie, TIPO_MOV,
                                        grdDetFactura.Item("gfdCCODIGOPRODUCTO", i).Value, grdDetFactura.Item("gfdCantidad", i).Value,
                                        grdDetFactura.Item("gfdPrecioUnit", i).Value, grdDetFactura.Item("gfdPorcIVA", i).Value, 0, 0,
                                        0, 1,
                                        0, 0,
                                        0, 0,
                                        0, False,
                                        i + 1, 0,
                                        0, grdDetFactura.Item("gfdTextoComp", i).Value)

                                        ReDim Preserve ArraySql(indice)
                                        ArraySql(indice) = StrSql
                                        indice += 1

                                        'StrSql = objTool.ActualizaCampoTabla()
                                        'ReDim Preserve ArraySqlCOM(indice)
                                        'ArraySqlCOM(indiceCOM) = StrSql
                                        'indiceCOM += 1

                                    End If
                                Next
                                'ACTUALIZAR GUIAS
                                For i = 0 To grdViajes.RowCount - 1
                                    If grdViajes.Item("gvFacturar", i).Value = 1 Then
                                        If rdbDocVia.Checked Then
                                            StrSql = objTool.ActualizaCampoTabla("CabGuia", "EstatusGuia", TipoDato.TdCadena, "FACTURADO",
                                            "NumGuiaId", TipoDato.TdNumerico, grdViajes.Item("gvNumGuiaId", i).Value)
                                            ReDim Preserve ArraySql(indice)
                                            ArraySql(indice) = StrSql
                                            indice += 1


                                            StrSql = objTool.ActualizaCampoTabla("CabGuia", "NoSerie", TipoDato.TdCadena, vSerie,
                                            "NumGuiaId", TipoDato.TdNumerico, grdViajes.Item("gvNumGuiaId", i).Value)

                                            ReDim Preserve ArraySql(indice)
                                            ArraySql(indice) = StrSql
                                            indice += 1

                                            StrSql = objTool.ActualizaCampoTabla("CabGuia", "NofOLIO", TipoDato.TdNumerico, vFolio,
                                            "NumGuiaId", TipoDato.TdNumerico, grdViajes.Item("gvNumGuiaId", i).Value)

                                            ReDim Preserve ArraySql(indice)
                                            ArraySql(indice) = StrSql
                                            indice += 1

                                        Else
                                            StrSql = objTool.ActualizaCampoTabla("penalizaciones", "Estatus", TipoDato.TdCadena, "FACTURADO",
                                           "idViaje", TipoDato.TdNumerico, grdViajes.Item("gvNumGuiaId", i).Value)
                                            ReDim Preserve ArraySql(indice)
                                            ArraySql(indice) = StrSql
                                            indice += 1


                                            StrSql = objTool.ActualizaCampoTabla("penalizaciones", "NoSerie", TipoDato.TdCadena, vSerie,
                                            "idViaje", TipoDato.TdNumerico, grdViajes.Item("gvNumGuiaId", i).Value)

                                            ReDim Preserve ArraySql(indice)
                                            ArraySql(indice) = StrSql
                                            indice += 1

                                            StrSql = objTool.ActualizaCampoTabla("penalizaciones", "NoFolio", TipoDato.TdNumerico, vFolio,
                                            "idViaje", TipoDato.TdNumerico, grdViajes.Item("gvNumGuiaId", i).Value)

                                            ReDim Preserve ArraySql(indice)
                                            ArraySql(indice) = StrSql
                                            indice += 1
                                        End If

                                    End If



                                Next


                            End If


                        End If
                    End If
                End If

            End If

            If indice > 0 Then
                Dim obj As New CapaNegocio.Tablas
                'Dim Respuesta As String = "EFECTUADO"
                Dim Respuesta As String = ""

                Respuesta = obj.EjecutarSql(Inicio.CONSTR, "SQL", ArraySql, Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, indice)
                FEspera.Close()
                If Respuesta = "EFECTUADO" Then
                    MsgBox("Actualizacion efectuada satisfactoriamente", MsgBoxStyle.Exclamation, Me.Text)
                    System.Diagnostics.Process.Start(sRutaEmpresaAdmPAQ & "\XML_SDK\" & vSerie & CInt(vFolio) & ".pdf")

                    'If indiceCOM > 0 Then
                    '    'Respuesta = obj.EjecutarSql(Inicio.CONSTR_COM, "SQL", ArraySqlCOM, Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, indiceCOM)
                    '    'If Respuesta = "EFECTUADO" Then
                    '    MsgBox("Actualizacion efectuada satisfactoriamente", MsgBoxStyle.Exclamation, Me.Text)
                    '    System.Diagnostics.Process.Start(sRutaEmpresaAdmPAQ & "\XML_SDK\" & vSerie & CInt(vFolio) & ".pdf")
                    '    'End If
                    'End If

                    'MsgBox("Actualizacion efectuada satisfactoriamente", MsgBoxStyle.Exclamation, Me.Text)
                    'System.Diagnostics.Process.Start("C:\Compac\Empresas\adPegaso\XML_SDK\" & vSerie & CInt(vFolio) & ".pdf")

                    'System.Diagnostics.Process.Start(sRutaEmpresaAdmPAQ & "\XML_SDK\" & vSerie & CInt(vFolio) & ".pdf")


                    'If OpcionForma = TipoOpcionForma.tOpcModificar Then
                    '    MsgBox("Orden de Trabajo Actualizado Satisfactoriamente" & vbCrLf & _
                    '    IIf(NoIdPreOC > 0, "Se Creo la El Folio de Pre Orden de Compra # " & NoIdPreOC, "") & vbCrLf & _
                    '    IIf(NoIdPreSal > 0, "Se Creo la El Folio de Pre Salida de ALmacen # " & NoIdPreSal, ""), MsgBoxStyle.Exclamation, Me.Text)
                    '    'ElseIf OpcionForma = TipoOpcionForma.tOpcEliminar Then
                    '    '    MsgBox("Grupo eliminado con exito!!!", MsgBoxStyle.Exclamation, Me.Text)
                    '    'ElseIf OpcionForma = TipoOpcionForma.tOpcInsertar Then
                    '    'MsgBox("Actualizacion efectuada satisfactoriamente", MsgBoxStyle.Exclamation, Me.Text)
                    'End If
                ElseIf Respuesta = "NOEFECTUADO" Then

                ElseIf Respuesta = "ERROR" Then
                    indice = 0
                    indiceCOM = 0

                    SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")
                End If
            Else
                MsgBox("No se Encontraron Registros que Grabar", MsgBoxStyle.Exclamation, Me.Text)
            End If
            Cancelar(False)
            'End If
            Salida = False
        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
            FEspera.Close()

        End Try
    End Sub


    Private Function UltimoConsecutivo() As Integer

        Dim Obj As New CapaNegocio.Tablas
        Dim Ultimo As Integer = 0



        Ultimo = objNumSig.NumSigEnTablas(Inicio.CONSTR, "NOMOVIMIENTO", "PARAMETROS", "CABMOVIMIENTOS", " WHERE IDPARAMETRO = 1", " ORDER BY NOMOVIMIENTO DESC")
        'Ultimo = Obj.NumSigEnTablasSinUpdate(Inicio.CONSTR, "Consecutivo", "detPromociones", " WHERE IdPromocion = " & Promocion)

        Return Ultimo
    End Function

    'Private Function UltimoConsecutivoContpaq(ByVal TipDocumento As Integer, _
    '                                          ByVal tipoDocto As String, ByVal TipDocumentoID As Integer) As String

    '    'UltimoConsecutivoContpaq()
    '    UltimoConsecutivoContpaq = ""

    '    Existe = LlenaDatosFox(TipDocumento, "MGW10006", "CCODIGOC01", TipoDato.TdCadena)
    '    If Existe = KEY_RCORRECTO Then
    '        'vSerie = vSerieAdmPAQ
    '        If vFolioAdmPAQ = 0 Then
    '            Existe = LlenaDatosFox(tipoDocto, "MGW10007", "CIDDOCUM01", TipoDato.TdCadena)
    '            If Existe = KEY_RCORRECTO Then
    '                'vFolio = vFolioAdmPAQ
    '                Existe = LlenaDatosFox(TipDocumentoID, "MGW10008", "CIDDOCUM01", TipoDato.TdCadena, tipoDocto)
    '                If Existe = KEY_RCORRECTO Then
    '                    UltimoConsecutivoContpaq = "TIPOCONCEPTO"
    '                    If vFolioAdmPAQ = vFolioAdmPAQVer And vSerieAdmPAQ = vSerieAdmPAQVer Then
    '                        vSerie = vSerieAdmPAQ
    '                        vFolio = vFolioAdmPAQ
    '                    ElseIf vFolioAdmPAQ < vFolioAdmPAQVer And vSerieAdmPAQ = vSerieAdmPAQVer Then
    '                        'que pasa ?
    '                        vSerie = vSerieAdmPAQ
    '                        vFolio = vFolioAdmPAQVer
    '                    ElseIf vFolioAdmPAQ > vFolioAdmPAQVer And vSerieAdmPAQ = vSerieAdmPAQVer Then
    '                        vSerie = vSerieAdmPAQ
    '                        vFolio = vFolioAdmPAQVer
    '                    End If
    '                Else
    '                    'que pasa
    '                End If
    '            Else

    '            End If
    '        Else
    '            'verificar
    '            Existe = LlenaDatosFox(TipDocumentoID, "MGW10008", "CIDDOCUM01", TipoDato.TdCadena, tipoDocto)
    '            If Existe = KEY_RCORRECTO Then
    '                UltimoConsecutivoContpaq = "CONCEPTO"
    '                If vFolioAdmPAQ = vFolioAdmPAQVer And vSerieAdmPAQ = vSerieAdmPAQVer Then
    '                    vSerie = vSerieAdmPAQ
    '                    vFolio = vFolioAdmPAQ
    '                ElseIf vFolioAdmPAQ < vFolioAdmPAQVer And vSerieAdmPAQ = vSerieAdmPAQVer Then
    '                    'que pasa ?
    '                    vSerie = vSerieAdmPAQ
    '                    vFolio = vFolioAdmPAQVer
    '                ElseIf vFolioAdmPAQ > vFolioAdmPAQVer And vSerieAdmPAQ = vSerieAdmPAQVer Then
    '                    vSerie = vSerieAdmPAQ
    '                    vFolio = vFolioAdmPAQ
    '                End If
    '            Else
    '                'que pasa
    '                vSerie = vSerieAdmPAQ
    '                vFolio = vFolioAdmPAQ
    '                UltimoConsecutivoContpaq = "CONCEPTO"
    '            End If
    '        End If


    '    ElseIf Existe = KEY_RINCORRECTO Then

    '        vSerie = ""
    '        vFolio = 0
    '        'no encontro el siguiente folio
    '    ElseIf Existe = KEY_RERROR Then
    '        vSerie = ""
    '        vFolio = 0
    '        'Ocurrio un Error

    '    End If
    'End Function
    Private Function AgregaCabeceroDetalle_SDK_ALTA(ByVal TipoDocumento As String, ByVal tipoDocto As String,
    ByVal vSerie As String, ByVal vFolio As Integer, ByVal CodigoCliente As String) As Boolean
        Dim idDoc As String = ""

        AgregaCabeceroDetalle_SDK_ALTA = False

        Dim lIdDocto As Integer
        Dim LIdMovto As Integer

        Dim ltDocto As New tDocumento
        Dim ltMovto As New tMovimiento



        FEspera.Show("Generando Factura en COMERCIAL ...")


        'PRODUCTOS
        If rdbMFDetalle.Checked Then
            CreaServiciosCOM(CInt(CodigoCliente), "S")
        End If


        ltDocto.aCodConcepto = TipoDocumento
        'ltDocto.aSerie = vSerie
        'ltDocto.aFolio = vFolio
        ltDocto.aFecha = FechaFactura.ToString("MM/dd/yyyy")
        ltDocto.aCodigoCteProv = CodigoCliente
        ltDocto.aCodigoAgente = "(Ninguno)"
        ltDocto.aSistemaOrigen = 0
        ltDocto.aNumMoneda = 1
        ltDocto.aTipoCambio = 1
        ltDocto.aAfecta = 0

        lError = fAltaDocumento(lIdDocto, ltDocto)
        If lError = 0 Then
            For i = 0 To grdDetFactura.Rows.Count - 1
                ltMovto.aCodAlmacen = "1"
                ltMovto.aConsecutivo = i + 1
                ltMovto.aCodProdSer = grdDetFactura.Item("gfdCCODIGOPRODUCTO", i).Value
                ltMovto.aUnidades = grdDetFactura.Item("gfdCantidad", i).Value
                ltMovto.aPrecio = grdDetFactura.Item("gfdPrecioUnit", i).Value
                ltMovto.aCosto = 0
                ltMovto.aReferencia = grdDetFactura.Item("gfdTextoComp", i).Value




                lError = fAltaMovimiento(lIdDocto, LIdMovto, ltMovto)
                If lError <> 0 Then
                    MensajeError(lError)
                    AgregaCabeceroDetalle_SDK_ALTA = False
                    FEspera.Close()
                    Exit For
                Else
                    'Texto Extra1
                    StrSql = ObjSql.ActualizaCampoTabla("admMovimientos", "COBSERVAMOV", TipoDato.TdCadena,
                    grdDetFactura.Item("gfdCNOMBREPRODUCTO", i).Value & " " & grdDetFactura.Item("gfdTextoComp", i).Value,
                    "CIDMOVIMIENTO", TipoDato.TdNumerico, LIdMovto)

                    ReDim Preserve ArraySqlCOM(indiceCOM)
                    ArraySqlCOM(indiceCOM) = StrSql
                    indiceCOM += 1
                End If

            Next
            AgregaCabeceroDetalle_SDK_ALTA = True
        Else
            MensajeError(lError)
            FEspera.Close()
            fBorraDocumento()

        End If

        'lError = fLeeDatoDocumento(kDocumento_IdDocumento, idDoc, kLongDescripcion - 1)
        'If lError <> 0 Then
        '    MensajeError(lError)
        '    FEspera.Close()
        '    Exit Function
        'End If

        'lError = fAfectaDocto_Param(TipoDocumento, vSerie, vFolio, True)
        'If lError <> 0 Then
        '    MensajeError(lError)
        '    'fCierraEmpresa()
        '    'fTerminaSDK(
        '    FEspera.Close()
        '    Exit Function
        'End If
        If indiceCOM > 0 Then
            FEspera.Show("Actualizando Datos a COMERCIAL ...")
            Dim obj As New CapaNegocio.Tablas
            Dim Respuesta As String = ""
            Respuesta = obj.EjecutarSql(Inicio.CONSTR_COM, "SQL", ArraySqlCOM, Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, indiceCOM)
            If Respuesta = "EFECTUADO" Then
                'Se actualiza de forma manual, luego se procede a timbrar
            End If
        End If

        If tipoDocto = "4" Then
            'Timbra Factura
            If TimbraFactura_SDK(TipoDocumento) Then
                Return AgregaCabeceroDetalle_SDK_ALTA
            End If
        Else
            Return AgregaCabeceroDetalle_SDK_ALTA
        End If

    End Function

    Private Function AgregaCabeceroDetalle_SDK(ByVal TipoDocumento As String, ByVal tipoDocto As String,
    ByVal vSerie As String, ByVal vFolio As Integer, ByVal CodigoCliente As String) As Boolean
        Dim idDoc As String = ""

        idDoc = f_RellenaConBlancos(idDoc, kLongDescripcion)
        AgregaCabeceroDetalle_SDK = False

        lError = fInsertarDocumento()
        'If lError <> 0 Then MensajeError(lError) : GoTo SALIR

        FEspera.Show("Generando Documento AdminPAQ ... CABECERO ")
        If lError = 0 Then
            lError = fSetDatoDocumento(kDocumento_CodigoConcepto, TipoDocumento)
            If lError = 0 Then
                lError = fSetDatoDocumento(kDocumento_Serie, vSerie)
                If lError = 0 Then
                    lError = fSetDatoDocumento(kDocumento_Folio, vFolio)
                    If lError = 0 Then
                        'lError = fSetDatoDocumento(kDocumento_Fecha, dtpFechaMov.Value.ToString("MM/dd/yyyy"))
                        lError = fSetDatoDocumento(kDocumento_Fecha, FechaFactura.ToString("MM/dd/yyyy"))
                        If lError = 0 Then
                            'lError = fSetDatoDocumento(kDocumento_IdMoneda, CInt(cbMoneda.SelectedValue))
                            'lError = fSetDatoDocumento(kDocumento_IdMoneda, "1")
                            If lError = 0 Then
                                'lError = fSetDatoDocumento(kDocumento_TipoCambio, CDbl(txtTipoCambio.Text))
                                'lError = fSetDatoDocumento(kDocumento_TipoCambio, CDbl(1))
                                If lError = 0 Then
                                    lError = fSetDatoDocumento(kDocumento_CodigoCteProv, CodigoCliente)
                                    If lError = 0 Then
                                        'lError = fSetDatoDocumento(kDocumento_CodigoAgente, CInt(cbAgente.SelectedValue))
                                        lError = fSetDatoDocumento(kDocumento_CodigoAgente, 0)
                                        If lError = 0 Then
                                            'lError = fSetDatoDocumento(kDocumento_Referencia, "Lista Precios")
                                            lError = fSetDatoDocumento("CREFEREN01", "")
                                            If lError = 0 Then
                                                'termina Cabecero
                                                lError = fSetDatoDocumento("CTEXTOEX01", "")
                                                If lError <> 0 Then MensajeError(lError)
                                                lError = fSetDatoDocumento("CTEXTOEX02", "")
                                                If lError <> 0 Then MensajeError(lError)
                                                If tipoDocto = "4" Then
                                                    lError = fSetDatoDocumento("CMETODOPAG", "")
                                                    If lError <> 0 Then MensajeError(lError)

                                                    lError = fSetDatoDocumento("CNUMCTAPAG", "")
                                                    If lError <> 0 Then MensajeError(lError)
                                                End If
                                                lError = fSetDatoDocumento("COBSERVACIONES", "")
                                                If lError <> 0 Then MensajeError(lError)

                                                If tipoDocto <> "4" Then
                                                    lError = fSetDatoDocumento("CESCFD", "0")
                                                ElseIf tipoDocto = "4" Then
                                                    lError = fSetDatoDocumento("CESCFD", "1")
                                                End If
                                                If lError <> 0 Then MensajeError(lError)

                                                lError = fGuardaDocumento()
                                                If lError = 0 Then
                                                    'EMPIEZA EL DETALLE
                                                    lError = fLeeDatoDocumento(kDocumento_IdDocumento, idDoc, kLongDescripcion - 1)
                                                    If lError = 0 Then
                                                        For i = 0 To grdDetFactura.Rows.Count - 1
                                                            If grdDetFactura.Item("gfdPrecioUnit", i).Value > 0 Then
                                                                '
                                                                'FEspera.Show("Generando AdminPAQ - Producto = " & grdVentas.Item("CCODIGOP01_PRO", i).Value)
                                                                FEspera.Show("Generando AdminPAQ - Producto = " & grdDetFactura.Item("gfdCNOMBREPRODUCTO", i).Value)

                                                                lError = fInsertarMovimiento()
                                                                If lError = 0 Then
                                                                    lError = fSetDatoMovimiento(kDocumento_IdDocumento, idDoc)
                                                                    If lError = 0 Then
                                                                        lError = fSetDatoMovimiento(kMovto_CodProducto, grdDetFactura.Item("gfdCCODIGOPRODUCTO", i).Value)
                                                                        If lError = 0 Then
                                                                            'lError = fSetDatoMovimiento(kMovto_NombreUnidad, grdVentas.Item("Unidad", i).Value)
                                                                            'lError = fSetDatoMovimiento(kMovto_NombreUnidad, grdVentas.Item("NomUnidadBASE", i).Value)
                                                                            If lError = 0 Then
                                                                                'lError = fSetDatoMovimiento(kMovto_CodAlmacen, Trim(grdDetFactura.Item("CCODIGOA01", i).Value))
                                                                                lError = fSetDatoMovimiento(kMovto_CodAlmacen, 1)
                                                                                If lError = 0 Then
                                                                                    lError = fSetDatoMovimiento(kMovto_Precio, grdDetFactura.Item("gfdPrecioUnit", i).Value)
                                                                                    If lError = 0 Then
                                                                                        ' En caso de tener una sola unidad con grabar este dato kMovto_unidades
                                                                                        'si la venta es en unidad no equivalente (pza) se graba la unidad BASE(kg) y en la no equivalente la pza
                                                                                        'If grdDetFactura.Item("Unidad2", i).Value > 0 Then
                                                                                        '    If grdDetFactura.Item("Unidad", i).Value <> grdDetFactura.Item("Unidad2", i).Value Then
                                                                                        '        'SE GUARDAN LAS 2 CANTIDADES 
                                                                                        '        lError = fSetDatoMovimiento(kMovto_Unidades, grdVentas.Item("Cantidad", i).Value)
                                                                                        '        If lError <> 0 Then MensajeError(lError) : Exit For
                                                                                        '        lError = fSetDatoMovimiento(kMovto_NombreUnidad, grdVentas.Item("NomUnidad", i).Value)
                                                                                        '        If lError <> 0 Then MensajeError(lError) : Exit For

                                                                                        '        lError = fSetDatoMovimiento(kMovto_UnidadesNC, grdVentas.Item("CantidadEQUI", i).Value)
                                                                                        '        If lError <> 0 Then MensajeError(lError) : Exit For
                                                                                        '        lError = fSetDatoMovimiento(kMovto_NombreUnidadNC, grdVentas.Item("CNOMBREU01uni2", i).Value)
                                                                                        '        If lError <> 0 Then MensajeError(lError) : Exit For
                                                                                        '        'si tiene unidad no equivalente
                                                                                        '    ElseIf grdVentas.Item("Unidad", i).Value = grdVentas.Item("Unidad2", i).Value Then
                                                                                        '        'CASO ESPECIAL Se toma la cantidad_base como cantidad y la cantidad como Cantidad_equivalente 
                                                                                        '        lError = fSetDatoMovimiento(kMovto_Unidades, grdVentas.Item("CantidadBase", i).Value)
                                                                                        '        If lError <> 0 Then MensajeError(lError) : Exit For
                                                                                        '        lError = fSetDatoMovimiento(kMovto_NombreUnidad, grdVentas.Item("NomUnidadBASE", i).Value)
                                                                                        '        If lError <> 0 Then MensajeError(lError) : Exit For

                                                                                        '        '22/FEBRERO/2015
                                                                                        '        lError = fSetDatoMovimiento(kMovto_UnidadesNC, grdVentas.Item("Cantidad", i).Value)
                                                                                        '        'lError = fSetDatoMovimiento(kMovto_UnidadesNC, grdVentas.Item("CantidadEQUI", i).Value)
                                                                                        '        If lError <> 0 Then MensajeError(lError) : Exit For
                                                                                        '        lError = fSetDatoMovimiento(kMovto_NombreUnidadNC, grdVentas.Item("NomUnidad", i).Value)
                                                                                        '        'lError = fSetDatoMovimiento(kMovto_NombreUnidadNC, grdVentas.Item("CNOMBREU01uni2", i).Value)
                                                                                        '        If lError <> 0 Then MensajeError(lError) : Exit For

                                                                                        '    End If
                                                                                        'Else
                                                                                        'SOLO SE GUARDA CANTIDAD
                                                                                        lError = fSetDatoMovimiento(kMovto_Unidades, grdDetFactura.Item("gfdCantidad", i).Value)
                                                                                        If lError <> 0 Then MensajeError(lError) : Exit For
                                                                                        'lError = fSetDatoMovimiento(kMovto_NombreUnidad, grdDetFactura.Item("NomUnidad", i).Value)
                                                                                        'If lError <> 0 Then MensajeError(lError) : Exit For
                                                                                        'End If

                                                                                        'lError = fSetDatoMovimiento(kMovto_Unidades, grdVentas.Item("Cantidad", i).Value)
                                                                                        'mover hacia aqui OJOOO
                                                                                        'lError = fSetDatoMovimiento(kMovto_NombreUnidad, grdVentas.Item("NomUnidadBASE", i).Value)

                                                                                        'si tiene unidad no equivalente
                                                                                        'kMovto_UnidadesNC 
                                                                                        'kMovto_NombreUnidadNC 


                                                                                        'lError = fSetDatoMovimiento(kMovto_PorcDescto1, grdVentas.Item("porcDesc", i).Value)
                                                                                        'lError = fSetDatoMovimiento(kMovto_ImportDescto1, grdVentas.Item("impdescto", i).Value)

                                                                                        '31/DICIEMBRE/2014
                                                                                        lError = fSetDatoMovimiento(kMovto_ObservaMov, grdDetFactura.Item("gfdTextoComp", i).Value)

                                                                                        lError = fGuardaMovimiento()

                                                                                        'If lError <> 0 Then MensajeError(lError) : GoTo SALIR

                                                                                        'AgregaCabeceroDetalle_SDK = True

                                                                                        'CHECANDO 24/MAR/2014
                                                                                        If lError = 0 Then
                                                                                            'TODO bien
                                                                                            AgregaCabeceroDetalle_SDK = True
                                                                                        Else
                                                                                            MensajeError(lError)
                                                                                            AgregaCabeceroDetalle_SDK = False
                                                                                            Exit For
                                                                                        End If
                                                                                    Else
                                                                                        MensajeError(lError)
                                                                                        AgregaCabeceroDetalle_SDK = False
                                                                                        Exit For

                                                                                    End If
                                                                                Else
                                                                                    MensajeError(lError)
                                                                                    AgregaCabeceroDetalle_SDK = False
                                                                                    Exit For

                                                                                End If
                                                                            Else
                                                                                MensajeError(lError)
                                                                                AgregaCabeceroDetalle_SDK = False
                                                                                Exit For
                                                                            End If
                                                                        Else
                                                                            MensajeError(lError)
                                                                            AgregaCabeceroDetalle_SDK = False
                                                                            Exit For
                                                                        End If
                                                                    Else
                                                                        MensajeError(lError)
                                                                        AgregaCabeceroDetalle_SDK = False
                                                                        Exit For
                                                                    End If
                                                                Else
                                                                    MensajeError(lError)
                                                                    AgregaCabeceroDetalle_SDK = False
                                                                    Exit For
                                                                End If
                                                            End If

                                                        Next
                                                        'AQUI TERMINA FOR
                                                    Else
                                                        MensajeError(lError)
                                                    End If
                                                Else
                                                    MensajeError(lError)
                                                End If
                                            Else
                                                MensajeError(lError)
                                            End If
                                        Else
                                            MensajeError(lError)
                                        End If
                                    Else
                                        MensajeError(lError)
                                    End If

                                Else
                                    MensajeError(lError)
                                End If

                            Else
                                MensajeError(lError)
                            End If
                        Else
                            MensajeError(lError)
                        End If
                    Else
                        MensajeError(lError)
                    End If
                Else
                    MensajeError(lError)
                End If
            Else
                MensajeError(lError)
            End If
        Else
            MensajeError(lError)
            fBorraDocumento()
        End If

        'SALIR:  MsgBox("hubo error ???")
        If Not AgregaCabeceroDetalle_SDK Then
            lError = fBorraDocumento()
            If lError <> 0 Then
                MensajeError(lError)
            End If
        Else
            'SIGUE
            'Afecta documento
            'MODIFICADO - 19MAR2014 - 001
            'lError = fAfectaDocto_Param(cbDocumento.SelectedValue, txtNoSerie.Text, txtIdMovimiento.Text, True)

            'iddocumento
            'lError = fAfectaDocto_Param(TipoDocumento, txtNoSerie.Text, txtIdMovimiento.Text, True)
            'lError = fAfectaDocto_Param(TipoDocumento, vSerie, vFolio, True)
            lError = fAfectaDocto_Param(TipoDocumento, vSerie, vFolio, True)
            If lError <> 0 Then
                MensajeError(lError)
                'fCierraEmpresa()
                'fTerminaSDK()
                Exit Function
            End If


            If Not AgregaCabeceroDetalle_SDK Then
                lError = fBorraDocumento()
                If lError = 0 Then

                Else
                    MensajeError(lError)
                End If
            End If

            'MODIFICADO - 19MAR2014 - 001 - se pasa como referencia
            'Dim tipoDocto As String = BuscaDatoCombo(DsComboMovimiento, "MGW10006", "CCODIGOC01", Trim(cbDocumento.SelectedValue), TipoDato.TdCadena, "CIDDOCUM01")

            'If tipoDocto = "4" And chkTimbraFactura.Checked Then
            If tipoDocto = "4" Then
                'Timbra Factura
                If TimbraFactura_SDK(TipoDocumento) Then
                    Return AgregaCabeceroDetalle_SDK
                End If
            Else
                Return AgregaCabeceroDetalle_SDK
            End If
        End If

    End Function

    Private Function TimbraFactura_SDK(ByVal TipoDocumento As String) As Boolean
        'Dim lError As Integer = 0
        Dim idDoc As String = ""

        FEspera.Show("Timbrando Factura en COMERCIAL ...")

        'MODIFICADO - 19MAR2014 - 001
        'Luego Poner en Parametros
        'If Parametro_CPLAMIGCFD = "" Then
        '    lPlantilla = Trim(BuscaDatoCombo(DsComboMovimiento, "MGW10006", "CCODIGOC01", TipoDocumento, TipoDato.TdCadena, "CPLAMIGCFD"))
        'Else
        '    lPlantilla = Trim(Parametro_CPLAMIGCFD)
        'End If

        'lRutaEntDocs = Trim(BuscaDatoCombo(DsComboMovimiento, "MGW10006", "CCODIGOC01", TipoDocumento, TipoDato.TdCadena, "CRUTAENT01"))

        'lPrefijo = Trim(BuscaDatoCombo(DsComboMovimiento, "MGW10006", "CCODIGOC01", TipoDocumento, TipoDato.TdCadena, "CPREFICON"))


        'MODIFICADO - 19MAR2014 - 001
        'lError = fEmitirDocumento(Trim(cbDocumento.SelectedValue), txtNoSerie.Text.ToString, CDbl(txtIdMovimiento.Text), Parametro_PassFacturacion, "")
        'GEnera Archivo
        If lPrefijo <> "" Then
            NomArchivo = CliRFC & lPrefijo & Format(Val(vFolio), "0000000000")
        Else
            NomArchivo = CliRFC & Format(Val(vFolio), "0000000000")
        End If

        'lError = fLeeDatoDocumento(kDocumento_IdDocumento, idDoc, kLongDescripcion - 1)
        'If lError <> 0 Then
        '    MensajeError(lError)
        '    FEspera.Close()
        '    Exit Function
        'End If

        lLicencia = 205
        ''Pruebas 28/mar/17
        'MsgBox("fInicializaLicenseInfo(lLicencia) " & lLicencia)
        lError = fInicializaLicenseInfo(lLicencia)

        If lError <> 0 Then
            MensajeError(lError)
            'fCierraEmpresa()
            'fTerminaSDK()
            Return False
        End If

        Parametro_PassFacturacion = "12345678a"
        lError = fEmitirDocumento(TipoDocumento, vSerie, CDbl(vFolio), Parametro_PassFacturacion, "")
        'lError = fEmitirDocumento(TipoDocumento, txtNoSerie.Text.ToString, CDbl(txtIdMovimiento.Text), Parametro_PassFacturacion, NomArchivo)
        '
        'lError = fEmitirDocumento(Text2.Text, "", lFolio, Text7.Text, lComplemento)

        ''Pruebas 28/mar/17
        'MsgBox("fEmitirDocumento TipoDocumento=" & TipoDocumento & "vSerie=" & vSerie & " vFolio = " & CDbl(vFolio) & " Parametro_PassFacturacion = " & Parametro_PassFacturacion)

        If lError <> 0 Then
            'MessageBox.Show(rError(lError))
            MensajeError(lError)
            'fCierraEmpresa()
            'fTerminaSDK()


            Return False
        End If


        'lPlantilla = BuscaDatoCombo(DsComboMovimiento, "MGW10006", "cidconce01", TipoDocumento, TipoDato.TdCadena, "CPLAMIGCFD")

        'lPlantilla = "C:\Compacw\Empresas\Reportes\Facturacion\Plantilla_Factura_cfdi_1.htm"
        'txtMsg.Text = "Entregando documento..."

        ''MODIFICADO - 19MAR2014 - 001
        'lError = fEntregEnDiscoXML(cbDocumento.SelectedValue, txtNoSerie.Text.ToString, CDbl(txtIdMovimiento.Text), 1, lPlantilla)
        'If lError <> 0 Then
        '    MensajeError(lError)
        '    'fCierraEmpresa()
        '    'fTerminaSDK()
        '    Return False
        'End If

        '23/ABRIL/2014
        'antes de esta funcion verificar que exista el timbre
        'y si existe el timbre verificar el documento
        'Dim UUID As String = Space(255)

        'lError = fDocumentoUUID(TipoDocumento, vSerie.ToString, CDbl(vFolio), UUID)
        'If lError <> 0 Then
        '    MensajeError(lError)
        'ElseIf UUID <> "" Then


        'End If

        'Pruebas 28/mar/17
        'MsgBox("fEntregEnDiscoXML 1")
        lError = fEntregEnDiscoXML(TipoDocumento, vSerie.ToString, CDbl(vFolio), 1, lPlantilla)
        FEspera.Show("Creando Archivo PDF en COMERCIAL ...")
        If lError <> 0 Then
            MensajeError(lError)
            'fCierraEmpresa()
            'fTerminaSDK()
            Return False
        Else
            'Pruebas 28/mar/17
            'MsgBox("fEntregEnDiscoXML 0")

            lError = fEntregEnDiscoXML(TipoDocumento, vSerie.ToString, CDbl(vFolio), 0, lPlantilla)
            FEspera.Show("Creando Archivo XML en COMERCIAL ...")
            If lError <> 0 Then
                MensajeError(lError)
                Return False
            End If
        End If


        Return True
        'txtMsg.Text = "Documento entregado. Folio: " & lFolioDocto
    End Function



    Private Sub CreaServiciosCOM(ByVal idCliente As Integer, ByVal TipoViaje As String)
        Dim ArticuloCOM As ProductosComClass
        Dim tdProd As New tProduto
        Dim idProd As Integer
        Dim objSql As New ToolSQLs

        'If IniciarSesionSDK() = False Then End

        For i = 0 To grdDetFactura.Rows.Count - 1
            ArticuloCOM = New ProductosComClass("", grdDetFactura.Item("gfdCNOMBREPRODUCTO", i).Value())
            If Not ArticuloCOM.Existe Then
                tdProd.cCodigoProducto = grdDetFactura.Item("gfdCCODIGOPRODUCTO", i).Value
                'tdProd.cCodigoProducto = 7
                tdProd.cNombreProducto = Microsoft.VisualBasic.Left(grdDetFactura.Item("gfdCNOMBREPRODUCTO", i).Value(), 60)
                tdProd.cTipoProducto = 3
                tdProd.cControlExistencia = 0
                tdProd.cMetodoCosteo = 1
                tdProd.cStatusProducto = 1
                'tdProd.cCodigoUnidadBase = "PIEZA"
                tdProd.cFechaAltaProducto = Now.ToString("MM/dd/yyyy")


                lError = fAltaProducto(idProd, tdProd)
                If lError <> 0 Then
                    FEspera.Close()
                    DespliegaError(lError)
                    fCierraEmpresa()
                    fTerminaSDK()
                    'BandGraba = False
                    Exit For
                Else
                    If Not ArticuloCOM.ExisteLOCServ Then
                        StrSql = objSql.InsertaCatServFact(idProd, Microsoft.VisualBasic.Left(grdDetFactura.Item("gfdCNOMBREPRODUCTO", i).Value(), 60),
                        grdDetFactura.Item("gfdCCODIGOPRODUCTO", i).Value, idCliente, TipoViaje)
                        ReDim Preserve ArraySql(indice)
                        ArraySql(indice) = StrSql
                        indice += 1
                    End If

                End If
            End If


        Next

        If indice > 0 Then
            Dim obj As New CapaNegocio.Tablas
            Dim Respuesta As String

            Respuesta = obj.EjecutarSql(Inicio.CONSTR, "SQL", ArraySql, Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, indice)
            FEspera.Close()
            If Respuesta = "EFECTUADO" Then
                'MsgBox("Actualizacion efectuada satisfactoriamente", MsgBoxStyle.Exclamation, Me.Text)

            ElseIf Respuesta = "NOEFECTUADO" Then

            ElseIf Respuesta = "ERROR" Then
                indice = 0
                indiceCOM = 0
                SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")
            End If
        Else
            'MsgBox("No se Encontraron Registros que Grabar", MsgBoxStyle.Exclamation, Me.Text)
        End If


        'Return True
    End Sub

    Private Sub grdViajes_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles grdViajes.CellContentClick

    End Sub


    'Private Sub cmbClientes_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbClientes.SelectedIndexChanged
    '    'If Salida Then Exit Sub
    '    If Not MyTablaCombo Is Nothing Then
    '        If Not cmbClientes.SelectedValue Is Nothing Then
    '            'Salida = True

    '            'Salida = False
    '        End If

    '    End If

    'End Sub


    Private Sub rdbDocPen_CheckedChanged(sender As Object, e As EventArgs) Handles rdbDocPen.CheckedChanged
        If rdbDocPen.Checked Then
            rdbMFGlobal.Checked = True
        Else
            rdbMFGlobal.Checked = False
        End If
    End Sub

    Private Sub dtpFechaIni_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFechaIni.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            dtpFechaFin.Focus()
        End If
    End Sub

    Private Sub dtpFechaIni_ValueChanged(sender As Object, e As EventArgs) Handles dtpFechaIni.ValueChanged

    End Sub

    Private Sub grdCabFactura_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles grdCabFactura.CellContentClick

    End Sub

    Private Function UltimoConsecutivoContpaq(ByVal TipDocumento As Integer,
                                              ByVal tipoDocto As String, ByVal TipDocumentoID As Integer) As String

        'UltimoConsecutivoContpaq()
        UltimoConsecutivoContpaq = ""

        Existe = LlenaDatosFox(TipDocumento, "MGW10006", "CCODIGOC01", TipoDato.TdCadena)
        If Existe = KEY_RCORRECTO Then
            'vSerie = vSerieAdmPAQ
            If vFolioAdmPAQ = 0 Then
                Existe = LlenaDatosFox(tipoDocto, "MGW10007", "CIDDOCUM01", TipoDato.TdCadena)
                If Existe = KEY_RCORRECTO Then
                    'vFolio = vFolioAdmPAQ
                    Existe = LlenaDatosFox(TipDocumentoID, "MGW10008", "CIDDOCUM01", TipoDato.TdCadena, tipoDocto)
                    If Existe = KEY_RCORRECTO Then
                        UltimoConsecutivoContpaq = "TIPOCONCEPTO"
                        If vFolioAdmPAQ = vFolioAdmPAQVer And vSerieAdmPAQ = vSerieAdmPAQVer Then
                            vSerie = vSerieAdmPAQ
                            vFolio = vFolioAdmPAQ
                        ElseIf vFolioAdmPAQ < vFolioAdmPAQVer And vSerieAdmPAQ = vSerieAdmPAQVer Then
                            'que pasa ?
                            vSerie = vSerieAdmPAQ
                            vFolio = vFolioAdmPAQVer
                        ElseIf vFolioAdmPAQ > vFolioAdmPAQVer And vSerieAdmPAQ = vSerieAdmPAQVer Then
                            vSerie = vSerieAdmPAQ
                            vFolio = vFolioAdmPAQVer
                        End If
                    Else
                        'que pasa
                    End If
                Else

                End If
            Else
                'verificar
                Existe = LlenaDatosFox(TipDocumentoID, "MGW10008", "CIDDOCUM01", TipoDato.TdCadena, tipoDocto)
                If Existe = KEY_RCORRECTO Then
                    UltimoConsecutivoContpaq = "CONCEPTO"
                    If vFolioAdmPAQ = vFolioAdmPAQVer And vSerieAdmPAQ = vSerieAdmPAQVer Then
                        vSerie = vSerieAdmPAQ
                        vFolio = vFolioAdmPAQ
                    ElseIf vFolioAdmPAQ < vFolioAdmPAQVer And vSerieAdmPAQ = vSerieAdmPAQVer Then
                        'que pasa ?
                        vSerie = vSerieAdmPAQ
                        vFolio = vFolioAdmPAQVer
                    ElseIf vFolioAdmPAQ > vFolioAdmPAQVer And vSerieAdmPAQ = vSerieAdmPAQVer Then
                        vSerie = vSerieAdmPAQ
                        vFolio = vFolioAdmPAQ
                    End If
                Else
                    'que pasa
                    vSerie = vSerieAdmPAQ
                    vFolio = vFolioAdmPAQ
                    UltimoConsecutivoContpaq = "CONCEPTO"
                End If
            End If
        ElseIf Existe = KEY_RINCORRECTO Then
            vSerie = ""
            vFolio = 0
        ElseIf Existe = KEY_RERROR Then
            vSerie = ""
            vFolio = 0

        End If
    End Function

    Private Function LlenaDatosFox(ByVal cCve As String, ByVal dbtabla As String, ByVal Campo As String, ByVal TipoCampo As TipoDato,
                                   Optional ByVal cCve2 As String = "", Optional ByVal cCve3 As String = "") As String
        Dim Consulta As String = ""
        'Dim Conexion As New OleDb.OleDbConnection(ConStrFox)
        Try
            Select Case UCase(dbtabla)
                Case UCase("MGW10006")
                    'Consulta = "select CSERIEPO01,(cnofolio + 1) as cnofolio ,ccodigoc01 from mgw10006 WHERE ccodigoc01 == '" & cCve & "'"
                    Consulta = "SELECT CSERIEPOROMISION as CSERIEPO01,(cnofolio + 1) as cnofolio, CCODIGOCONCEPTO as ccodigoc01  FROM admConceptos WHERE CCODIGOCONCEPTO = '" & cCve & "'"

                Case UCase("MGW10007")
                    'Consulta = "select (cnofolio + 1) as cnofolio,CIDDOCUM01 from mgw10007.dbf WHERE CIDDOCUM01 == " & Val(cCve)
                    Consulta = "SELECT (cnofolio + 1) as cnofolio,CIDDOCUMENTODE AS CIDDOCUM01 FROM admDocumentosModelo WHERE CIDDOCUMENTODE =  " & Val(cCve)
                Case UCase("MGW10008")
                    'Consulta = "SELECT TOP 1 (CFOLIO + 1) AS NEWFOLIO, CSERIEDO01 FROM MGW10008.dbf WHERE CSERIEDO01 == '" & vSerieAdmPAQ & "' AND CIDDOCUM02 == " & Val(cCve2) & " ORDER BY CFOLIO DESC"
                    Consulta = "SELECT TOP 1 (CFOLIO + 1) AS NEWFOLIO, CSERIEDOCUMENTO  AS CSERIEDO01 FROM dbo.admDocumentos WHERE CSERIEDOCUMENTO = '" & vSerieAdmPAQ & "' AND CIDDOCUMENTODE = " & Val(cCve2) & " ORDER BY CFOLIO DESC"

            End Select

            MyTablaCombo.Rows.Clear()
            MyTablaCombo = BDCOM.ExecuteReturn(Consulta)
            If Not MyTablaCombo Is Nothing Then
                If MyTablaCombo.Rows.Count > 0 Then
                    Select Case UCase(dbtabla)
                        Case UCase("MGW10006")
                            'vSerieAdmPAQ = Trim(DsComboFox.Tables(dbtabla).DefaultView.Item(0).Item("CSERIEPO01"))
                            'MyTablaDetPago.DefaultView.Item(i).Item("ImportePago").ToString
                            vSerieAdmPAQ = Trim(MyTablaCombo.DefaultView.Item(0).Item("CSERIEPO01"))
                            'vFolioAdmPAQ = DsComboFox.Tables(dbtabla).DefaultView.Item(0).Item("cnofolio")
                            vFolioAdmPAQ = MyTablaCombo.DefaultView.Item(0).Item("cnofolio")
                        Case UCase("MGW10007")
                            'vFolioAdmPAQ = DsComboFox.Tables(dbtabla).DefaultView.Item(0).Item("cnofolio")
                            vFolioAdmPAQ = MyTablaCombo.DefaultView.Item(0).Item("cnofolio")
                        Case UCase("MGW10008")
                            'vSerieAdmPAQVer = Trim(DsComboFox.Tables(dbtabla).DefaultView.Item(0).Item("CSERIEDO01"))
                            vSerieAdmPAQVer = Trim(MyTablaCombo.DefaultView.Item(0).Item("CSERIEPO01"))
                            'vFolioAdmPAQVer = DsComboFox.Tables(dbtabla).DefaultView.Item(0).Item("NEWFOLIO")
                            vFolioAdmPAQVer = MyTablaCombo.DefaultView.Item(0).Item("NEWFOLIO")

                    End Select
                    LlenaDatosFox = KEY_RCORRECTO
                Else
                    LlenaDatosFox = KEY_RINCORRECTO
                End If
            Else
                LlenaDatosFox = KEY_RINCORRECTO
            End If


            'Dim Adt As New OleDb.OleDbDataAdapter(Consulta, Conexion)
            'DsComboFox.Clear()
            'Adt.Fill(DsComboFox, dbtabla)
            'If Not DsComboFox Is Nothing Then
            '    If DsComboFox.Tables(dbtabla).DefaultView.Count > 0 Then
            '        Select Case UCase(dbtabla)
            '            Case UCase("MGW10006")
            '                vSerieAdmPAQ = Trim(DsComboFox.Tables(dbtabla).DefaultView.Item(0).Item("CSERIEPO01"))
            '                vFolioAdmPAQ = DsComboFox.Tables(dbtabla).DefaultView.Item(0).Item("cnofolio")
            '            Case UCase("MGW10007")
            '                vFolioAdmPAQ = DsComboFox.Tables(dbtabla).DefaultView.Item(0).Item("cnofolio")
            '            Case UCase("MGW10008")
            '                vSerieAdmPAQVer = Trim(DsComboFox.Tables(dbtabla).DefaultView.Item(0).Item("CSERIEDO01"))
            '                vFolioAdmPAQVer = DsComboFox.Tables(dbtabla).DefaultView.Item(0).Item("NEWFOLIO")

            '        End Select
            '        LlenaDatosFox = KEY_RCORRECTO
            '    Else
            '        LlenaDatosFox = KEY_RINCORRECTO
            '    End If
            'Else
            '    LlenaDatosFox = KEY_RINCORRECTO
            'End If
        Catch ex As Exception
            SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")
            LlenaDatosFox = KEY_RERROR
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, Me.Text)
        End Try
    End Function

    Private Sub cmbClientes_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbClientes.SelectedIndexChanged

    End Sub

    'Private Sub cmbClientes_KeyUp(sender As Object, e As KeyEventArgs) Handles cmbClientes.KeyUp
    '    If e.KeyValue = Keys.Enter Then
    '        Cliente = New ClientesClass(cmbClientes.SelectedValue)
    '        If Cliente.Existe Then
    '            vCambiaPrecio = Cliente.CambioPrecio
    '        End If

    '    End If
    'End Sub
End Class
