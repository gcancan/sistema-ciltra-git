'Fecha: 6 / Enero / 2017
'ROGER GALA PACHECO
'************************************************************************************************************************************
'*  Fecha Modificacion :   15 / Febrero / 2016                                                                                       
'*  
'************************************************************************************************************************************
'Imports LinqToExcel

Public Class FCapDiagnosticoOS
    Inherits System.Windows.Forms.Form

    'Private _Tag As String

    Dim ObjSql As New ToolSQLs
    Dim StrSql As String
    Dim indice As Integer = 0
    Dim ArraySql() As String

    Dim Salida As Boolean

    Private _idOrdenSer As Integer
    Private _idOrdenTrabajo As Integer
    Private _Diagnostico As String
    Private _DuracionHr As Decimal

    'Private _NomEmpresa As String
    'Private _FechaSolicitud As Date
    'Private _nomUsuario As String

    Private OS As OrdenServClass
    Private DOS As detOrdenServicioClass
    Private OT As OrdenTrabajoClass
    Private Emp As EmpresaClass
    Private Per As PersonalClass
    Private Uni As UnidadesTranspClass
    Private TipOS As TiposOrdenServicio
    Private Tuni As TipoUniTransClass
    Private Act As ActividadesClass

    Private obServicios As ProductosComClass

    Private _TipoOS As String



    Public Diagnostico As String
    Public DuracionHR As Decimal

    'servicios
    Public CIDPRODUCTO_SERV As Integer
    Public CNOMBREPRODUCTO_SERV As String
    Public CCODIGOPRODUCTO_SERV As String
    Public Cantidad_CIDPRODUCTO_SERV As Decimal
    Public Precio_CIDPRODUCTO_SERV As Decimal


    Public WithEvents MeStatus1 As ControlStatus.MeStatus
    Friend WithEvents btnMnuOk As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuSalir As System.Windows.Forms.ToolStripButton
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtDiagnostico As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtNomEmpresa As System.Windows.Forms.TextBox
    Friend WithEvents dtpFechaAsignadoHora As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpFechaAsignado As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtidOrdenSer As System.Windows.Forms.TextBox
    Friend WithEvents txtUnidad As TextBox
    Friend WithEvents Label33 As Label
    Friend WithEvents txtTipoOrdenServ As TextBox
    Friend WithEvents Label32 As Label
    Friend WithEvents txtPosicion As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents txtidLlanta As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents txtidOrdenTrabajo As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents txtidPersonalResp As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents txtidPersonalAyu1 As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents txtidPersonalAyu2 As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents txtDuracionHr As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents lblTipoTransp As Label
    Friend WithEvents txtNomActividad As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents cmbCIDPRODUCTO_SERV As ComboBox
    Friend WithEvents Label12 As Label
    Friend WithEvents txtCantidad_CIDPRODUCTO_SERV As TextBox
    Friend WithEvents Label14 As Label
    Friend WithEvents txtPrecio_CIDPRODUCTO_SERV As TextBox
    Friend WithEvents Label15 As Label
    Friend WithEvents txtTotal As TextBox
    Friend WithEvents Label16 As Label
    Friend WithEvents Label13 As System.Windows.Forms.Label



#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents ToolStripMenu As System.Windows.Forms.ToolStrip
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FCapDiagnosticoOS))
        Me.ToolStripMenu = New System.Windows.Forms.ToolStrip()
        Me.btnMnuOk = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuSalir = New System.Windows.Forms.ToolStripButton()
        Me.MeStatus1 = New ControlStatus.MeStatus()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtDiagnostico = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtNomEmpresa = New System.Windows.Forms.TextBox()
        Me.dtpFechaAsignadoHora = New System.Windows.Forms.DateTimePicker()
        Me.dtpFechaAsignado = New System.Windows.Forms.DateTimePicker()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtidOrdenSer = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtUnidad = New System.Windows.Forms.TextBox()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.txtTipoOrdenServ = New System.Windows.Forms.TextBox()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.txtPosicion = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtidLlanta = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtidOrdenTrabajo = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtidPersonalResp = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtidPersonalAyu1 = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtidPersonalAyu2 = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtDuracionHr = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.lblTipoTransp = New System.Windows.Forms.Label()
        Me.txtNomActividad = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.cmbCIDPRODUCTO_SERV = New System.Windows.Forms.ComboBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtCantidad_CIDPRODUCTO_SERV = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtPrecio_CIDPRODUCTO_SERV = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txtTotal = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.ToolStripMenu.SuspendLayout()
        Me.SuspendLayout()
        '
        'ToolStripMenu
        '
        Me.ToolStripMenu.AutoSize = False
        Me.ToolStripMenu.BackColor = System.Drawing.Color.Silver
        Me.ToolStripMenu.Dock = System.Windows.Forms.DockStyle.None
        Me.ToolStripMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnMnuOk, Me.btnMnuSalir})
        Me.ToolStripMenu.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow
        Me.ToolStripMenu.Location = New System.Drawing.Point(3, 0)
        Me.ToolStripMenu.Name = "ToolStripMenu"
        Me.ToolStripMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.ToolStripMenu.Size = New System.Drawing.Size(895, 42)
        Me.ToolStripMenu.TabIndex = 1
        Me.ToolStripMenu.Text = "ToolStrip1"
        '
        'btnMnuOk
        '
        Me.btnMnuOk.ForeColor = System.Drawing.Color.Red
        Me.btnMnuOk.Image = Global.Fletera.My.Resources.Resources.Save1
        Me.btnMnuOk.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuOk.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuOk.Name = "btnMnuOk"
        Me.btnMnuOk.Size = New System.Drawing.Size(56, 39)
        Me.btnMnuOk.Text = "&GRABAR"
        Me.btnMnuOk.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuSalir
        '
        Me.btnMnuSalir.ForeColor = System.Drawing.Color.Red
        Me.btnMnuSalir.Image = Global.Fletera.My.Resources.Resources.Salida2
        Me.btnMnuSalir.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuSalir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuSalir.Name = "btnMnuSalir"
        Me.btnMnuSalir.Size = New System.Drawing.Size(41, 39)
        Me.btnMnuSalir.Text = "&SALIR"
        Me.btnMnuSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'MeStatus1
        '
        Me.MeStatus1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.MeStatus1.Location = New System.Drawing.Point(0, 334)
        Me.MeStatus1.Margin = New System.Windows.Forms.Padding(4)
        Me.MeStatus1.Name = "MeStatus1"
        Me.MeStatus1.Size = New System.Drawing.Size(879, 26)
        Me.MeStatus1.TabIndex = 72
        '
        'Label11
        '
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label11.Location = New System.Drawing.Point(699, 233)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(97, 20)
        Me.Label11.TabIndex = 74
        Me.Label11.Text = "DI�GNOSTICO"
        Me.Label11.Visible = False
        '
        'txtDiagnostico
        '
        Me.txtDiagnostico.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDiagnostico.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDiagnostico.Location = New System.Drawing.Point(802, 233)
        Me.txtDiagnostico.MaxLength = 250
        Me.txtDiagnostico.Multiline = True
        Me.txtDiagnostico.Name = "txtDiagnostico"
        Me.txtDiagnostico.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDiagnostico.Size = New System.Drawing.Size(65, 54)
        Me.txtDiagnostico.TabIndex = 0
        Me.txtDiagnostico.Text = "D"
        Me.txtDiagnostico.Visible = False
        '
        'Label9
        '
        Me.Label9.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label9.Location = New System.Drawing.Point(211, 58)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(54, 20)
        Me.Label9.TabIndex = 106
        Me.Label9.Text = "Empresa:"
        '
        'txtNomEmpresa
        '
        Me.txtNomEmpresa.Enabled = False
        Me.txtNomEmpresa.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNomEmpresa.Location = New System.Drawing.Point(271, 55)
        Me.txtNomEmpresa.Name = "txtNomEmpresa"
        Me.txtNomEmpresa.Size = New System.Drawing.Size(163, 20)
        Me.txtNomEmpresa.TabIndex = 105
        '
        'dtpFechaAsignadoHora
        '
        Me.dtpFechaAsignadoHora.Enabled = False
        Me.dtpFechaAsignadoHora.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtpFechaAsignadoHora.Location = New System.Drawing.Point(767, 55)
        Me.dtpFechaAsignadoHora.Name = "dtpFechaAsignadoHora"
        Me.dtpFechaAsignadoHora.Size = New System.Drawing.Size(83, 20)
        Me.dtpFechaAsignadoHora.TabIndex = 103
        '
        'dtpFechaAsignado
        '
        Me.dtpFechaAsignado.Enabled = False
        Me.dtpFechaAsignado.Location = New System.Drawing.Point(536, 55)
        Me.dtpFechaAsignado.Name = "dtpFechaAsignado"
        Me.dtpFechaAsignado.Size = New System.Drawing.Size(225, 20)
        Me.dtpFechaAsignado.TabIndex = 102
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label10.Location = New System.Drawing.Point(445, 58)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(87, 13)
        Me.Label10.TabIndex = 104
        Me.Label10.Text = "Fecha Asignado:"
        '
        'txtidOrdenSer
        '
        Me.txtidOrdenSer.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtidOrdenSer.Enabled = False
        Me.txtidOrdenSer.ForeColor = System.Drawing.Color.Red
        Me.txtidOrdenSer.Location = New System.Drawing.Point(130, 58)
        Me.txtidOrdenSer.Name = "txtidOrdenSer"
        Me.txtidOrdenSer.Size = New System.Drawing.Size(69, 20)
        Me.txtidOrdenSer.TabIndex = 107
        Me.txtidOrdenSer.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label13.Location = New System.Drawing.Point(32, 61)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(92, 13)
        Me.Label13.TabIndex = 101
        Me.Label13.Text = "Orden de Servicio"
        '
        'txtUnidad
        '
        Me.txtUnidad.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtUnidad.Location = New System.Drawing.Point(130, 172)
        Me.txtUnidad.MaxLength = 10
        Me.txtUnidad.Name = "txtUnidad"
        Me.txtUnidad.Size = New System.Drawing.Size(69, 20)
        Me.txtUnidad.TabIndex = 108
        Me.txtUnidad.Tag = ""
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label33.Location = New System.Drawing.Point(12, 172)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(113, 13)
        Me.Label33.TabIndex = 109
        Me.Label33.Text = "Unidad de Transporte:"
        '
        'txtTipoOrdenServ
        '
        Me.txtTipoOrdenServ.Enabled = False
        Me.txtTipoOrdenServ.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTipoOrdenServ.Location = New System.Drawing.Point(130, 139)
        Me.txtTipoOrdenServ.Name = "txtTipoOrdenServ"
        Me.txtTipoOrdenServ.Size = New System.Drawing.Size(260, 26)
        Me.txtTipoOrdenServ.TabIndex = 111
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label32.Location = New System.Drawing.Point(8, 148)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(116, 13)
        Me.Label32.TabIndex = 110
        Me.Label32.Text = "Tipo de Orden Servicio"
        '
        'txtPosicion
        '
        Me.txtPosicion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPosicion.Location = New System.Drawing.Point(655, 172)
        Me.txtPosicion.MaxLength = 10
        Me.txtPosicion.Name = "txtPosicion"
        Me.txtPosicion.Size = New System.Drawing.Size(69, 20)
        Me.txtPosicion.TabIndex = 115
        Me.txtPosicion.Tag = ""
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label1.Location = New System.Drawing.Point(593, 172)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(50, 13)
        Me.Label1.TabIndex = 116
        Me.Label1.Text = "Posici�n:"
        '
        'txtidLlanta
        '
        Me.txtidLlanta.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtidLlanta.Location = New System.Drawing.Point(448, 172)
        Me.txtidLlanta.MaxLength = 10
        Me.txtidLlanta.Name = "txtidLlanta"
        Me.txtidLlanta.Size = New System.Drawing.Size(101, 20)
        Me.txtidLlanta.TabIndex = 117
        Me.txtidLlanta.Tag = ""
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label2.Location = New System.Drawing.Point(386, 172)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(39, 13)
        Me.Label2.TabIndex = 118
        Me.Label2.Text = "Llanta:"
        '
        'txtidOrdenTrabajo
        '
        Me.txtidOrdenTrabajo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtidOrdenTrabajo.ForeColor = System.Drawing.Color.Red
        Me.txtidOrdenTrabajo.Location = New System.Drawing.Point(130, 84)
        Me.txtidOrdenTrabajo.Name = "txtidOrdenTrabajo"
        Me.txtidOrdenTrabajo.Size = New System.Drawing.Size(69, 20)
        Me.txtidOrdenTrabajo.TabIndex = 120
        Me.txtidOrdenTrabajo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label3.Location = New System.Drawing.Point(32, 87)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(93, 13)
        Me.Label3.TabIndex = 119
        Me.Label3.Text = "Orden de Trabajo:"
        '
        'txtidPersonalResp
        '
        Me.txtidPersonalResp.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtidPersonalResp.Location = New System.Drawing.Point(130, 110)
        Me.txtidPersonalResp.MaxLength = 10
        Me.txtidPersonalResp.Name = "txtidPersonalResp"
        Me.txtidPersonalResp.Size = New System.Drawing.Size(190, 20)
        Me.txtidPersonalResp.TabIndex = 122
        Me.txtidPersonalResp.Tag = "3"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label4.Location = New System.Drawing.Point(55, 113)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(69, 13)
        Me.Label4.TabIndex = 121
        Me.Label4.Text = "Responsable"
        '
        'txtidPersonalAyu1
        '
        Me.txtidPersonalAyu1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtidPersonalAyu1.Location = New System.Drawing.Point(401, 110)
        Me.txtidPersonalAyu1.MaxLength = 10
        Me.txtidPersonalAyu1.Name = "txtidPersonalAyu1"
        Me.txtidPersonalAyu1.Size = New System.Drawing.Size(190, 20)
        Me.txtidPersonalAyu1.TabIndex = 124
        Me.txtidPersonalAyu1.Tag = "3"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label5.Location = New System.Drawing.Point(326, 113)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(64, 13)
        Me.Label5.TabIndex = 123
        Me.Label5.Text = "Ayudante 1:"
        '
        'txtidPersonalAyu2
        '
        Me.txtidPersonalAyu2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtidPersonalAyu2.Location = New System.Drawing.Point(677, 110)
        Me.txtidPersonalAyu2.MaxLength = 10
        Me.txtidPersonalAyu2.Name = "txtidPersonalAyu2"
        Me.txtidPersonalAyu2.Size = New System.Drawing.Size(190, 20)
        Me.txtidPersonalAyu2.TabIndex = 126
        Me.txtidPersonalAyu2.Tag = "3"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label6.Location = New System.Drawing.Point(602, 113)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(64, 13)
        Me.Label6.TabIndex = 125
        Me.Label6.Text = "Ayudante 2:"
        '
        'txtDuracionHr
        '
        Me.txtDuracionHr.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDuracionHr.Location = New System.Drawing.Point(758, 302)
        Me.txtDuracionHr.MaxLength = 10
        Me.txtDuracionHr.Name = "txtDuracionHr"
        Me.txtDuracionHr.Size = New System.Drawing.Size(109, 20)
        Me.txtDuracionHr.TabIndex = 1
        Me.txtDuracionHr.Tag = "1"
        Me.txtDuracionHr.Text = "1"
        Me.txtDuracionHr.Visible = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label7.Location = New System.Drawing.Point(690, 274)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(106, 13)
        Me.Label7.TabIndex = 128
        Me.Label7.Text = "DURACION HRS."
        Me.Label7.Visible = False
        '
        'lblTipoTransp
        '
        Me.lblTipoTransp.AutoSize = True
        Me.lblTipoTransp.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lblTipoTransp.Location = New System.Drawing.Point(211, 175)
        Me.lblTipoTransp.Name = "lblTipoTransp"
        Me.lblTipoTransp.Size = New System.Drawing.Size(44, 13)
        Me.lblTipoTransp.TabIndex = 129
        Me.lblTipoTransp.Text = "Tractor:"
        '
        'txtNomActividad
        '
        Me.txtNomActividad.Enabled = False
        Me.txtNomActividad.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNomActividad.Location = New System.Drawing.Point(130, 201)
        Me.txtNomActividad.Name = "txtNomActividad"
        Me.txtNomActividad.Size = New System.Drawing.Size(419, 26)
        Me.txtNomActividad.TabIndex = 131
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label8.Location = New System.Drawing.Point(8, 210)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(109, 13)
        Me.Label8.TabIndex = 130
        Me.Label8.Text = "FALLA REPORTADA"
        '
        'cmbCIDPRODUCTO_SERV
        '
        Me.cmbCIDPRODUCTO_SERV.AutoCompleteCustomSource.AddRange(New String() {"Regularizaci�n", "Vivienda"})
        Me.cmbCIDPRODUCTO_SERV.FormattingEnabled = True
        Me.cmbCIDPRODUCTO_SERV.Location = New System.Drawing.Point(130, 237)
        Me.cmbCIDPRODUCTO_SERV.Name = "cmbCIDPRODUCTO_SERV"
        Me.cmbCIDPRODUCTO_SERV.Size = New System.Drawing.Size(504, 21)
        Me.cmbCIDPRODUCTO_SERV.TabIndex = 133
        '
        'Label12
        '
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label12.Location = New System.Drawing.Point(3, 240)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(122, 37)
        Me.Label12.TabIndex = 134
        Me.Label12.Text = "ACTIVIDADES SAT"
        '
        'txtCantidad_CIDPRODUCTO_SERV
        '
        Me.txtCantidad_CIDPRODUCTO_SERV.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCantidad_CIDPRODUCTO_SERV.Location = New System.Drawing.Point(130, 274)
        Me.txtCantidad_CIDPRODUCTO_SERV.MaxLength = 10
        Me.txtCantidad_CIDPRODUCTO_SERV.Name = "txtCantidad_CIDPRODUCTO_SERV"
        Me.txtCantidad_CIDPRODUCTO_SERV.Size = New System.Drawing.Size(109, 20)
        Me.txtCantidad_CIDPRODUCTO_SERV.TabIndex = 135
        Me.txtCantidad_CIDPRODUCTO_SERV.Tag = "1"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label14.Location = New System.Drawing.Point(12, 277)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(57, 13)
        Me.Label14.TabIndex = 136
        Me.Label14.Text = "Cantidad"
        '
        'txtPrecio_CIDPRODUCTO_SERV
        '
        Me.txtPrecio_CIDPRODUCTO_SERV.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPrecio_CIDPRODUCTO_SERV.Location = New System.Drawing.Point(329, 274)
        Me.txtPrecio_CIDPRODUCTO_SERV.MaxLength = 10
        Me.txtPrecio_CIDPRODUCTO_SERV.Name = "txtPrecio_CIDPRODUCTO_SERV"
        Me.txtPrecio_CIDPRODUCTO_SERV.Size = New System.Drawing.Size(109, 20)
        Me.txtPrecio_CIDPRODUCTO_SERV.TabIndex = 137
        Me.txtPrecio_CIDPRODUCTO_SERV.Tag = "1"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label15.Location = New System.Drawing.Point(257, 277)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(43, 13)
        Me.Label15.TabIndex = 138
        Me.Label15.Text = "Precio"
        '
        'txtTotal
        '
        Me.txtTotal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTotal.Location = New System.Drawing.Point(525, 274)
        Me.txtTotal.MaxLength = 10
        Me.txtTotal.Name = "txtTotal"
        Me.txtTotal.Size = New System.Drawing.Size(109, 20)
        Me.txtTotal.TabIndex = 139
        Me.txtTotal.Tag = "1"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label16.Location = New System.Drawing.Point(453, 277)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(47, 13)
        Me.Label16.TabIndex = 140
        Me.Label16.Text = "TOTAL"
        '
        'FCapDiagnosticoOS
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(879, 360)
        Me.Controls.Add(Me.txtTotal)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.txtPrecio_CIDPRODUCTO_SERV)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.txtCantidad_CIDPRODUCTO_SERV)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.cmbCIDPRODUCTO_SERV)
        Me.Controls.Add(Me.txtNomActividad)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.lblTipoTransp)
        Me.Controls.Add(Me.txtDuracionHr)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.txtidPersonalAyu2)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.txtidPersonalAyu1)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.txtidPersonalResp)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtidOrdenTrabajo)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtidLlanta)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtPosicion)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtTipoOrdenServ)
        Me.Controls.Add(Me.Label32)
        Me.Controls.Add(Me.txtUnidad)
        Me.Controls.Add(Me.Label33)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.txtNomEmpresa)
        Me.Controls.Add(Me.dtpFechaAsignadoHora)
        Me.Controls.Add(Me.dtpFechaAsignado)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.txtidOrdenSer)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.txtDiagnostico)
        Me.Controls.Add(Me.MeStatus1)
        Me.Controls.Add(Me.ToolStripMenu)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "FCapDiagnosticoOS"
        Me.Text = "Diagnostico de Actividad"
        Me.ToolStripMenu.ResumeLayout(False)
        Me.ToolStripMenu.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    'Sub New(ByVal vTag As String, Optional ByVal vNoServ As Integer = 0, Optional ByVal vOrdenTrabajo As Integer = 0, Optional ByVal vOpcion As TipoOpcionEstatus = TipoOpcionEstatus.SinOpcion)
    '    InitializeComponent()
    '    _Tag = vTag
    '    _OrdenServicio = vNoServ
    '    _OrdenTrabajo = vOrdenTrabajo
    '    _Opcion = vOpcion

    'End Sub
    Sub New(Optional ByVal idOrdenSer As Integer = 0,
            Optional ByVal idOrdenTrabajo As Integer = 0,
            Optional ByVal Diagnostico As String = "",
            Optional ByVal DuracionHr As Decimal = 0,
            Optional ByVal TipoOS As String = "")

        _idOrdenSer = idOrdenSer
        _idOrdenTrabajo = idOrdenTrabajo
        _Diagnostico = Diagnostico
        _DuracionHr = DuracionHr
        _TipoOS = TipoOS

        InitializeComponent()
        '_Tag = vTag
        '_OrdenServicio = vNoServ
        '_OrdenTrabajo = vOrdenTrabajo
        '_Opcion = vOpcion

    End Sub


    Private Sub btnMnuSalir_Click(sender As Object, e As EventArgs) Handles btnMnuSalir.Click
        Me.Close()
    End Sub

    Private Sub LimpiaCampos()
        txtNomEmpresa.Text = ""
        txtDiagnostico.Text = ""
        txtidOrdenSer.Text = ""

        txtidOrdenSer.Text = ""
        txtidOrdenTrabajo.Text = ""
        txtNomEmpresa.Text = ""
        dtpFechaAsignado.Text = ""
        dtpFechaAsignadoHora.Text = ""
        txtidPersonalResp.Text = ""
        txtidPersonalAyu1.Text = ""
        txtidPersonalAyu2.Text = ""

        txtTipoOrdenServ.Text = ""
        txtUnidad.Text = ""
        txtidLlanta.Text = ""
        txtPosicion.Text = ""

        txtDiagnostico.Text = ""
        txtDuracionHr.Text = ""

    End Sub

    Private Sub FEstatusTaller_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        LimpiaCampos()
        ActivaCampos()
        CargaForm(_idOrdenSer, "INICIA", _idOrdenTrabajo)

    End Sub


    Private Sub CargaForm(ByVal idClave As String, ByVal NomTabla As String, ByVal idClave2 As String)
        Try
            Select Case UCase(NomTabla)
                Case UCase("INICIA")
                    OS = New OrdenServClass(Val(idClave))
                    If OS.Existe Then


                        txtidOrdenSer.Text = OS.idOrdenSer

                        TipOS = New TiposOrdenServicio(OS.idTipoOrden)
                        If TipOS.Existe Then
                            txtTipoOrdenServ.Text = TipOS.nomTipOrdServ
                        End If

                        Emp = New EmpresaClass(OS.IdEmpresa)
                        If Emp.Existe Then
                            txtNomEmpresa.Text = Emp.RazonSocial
                        End If

                        dtpFechaAsignado.Value = OS.FechaAsignado
                        dtpFechaAsignadoHora.Value = OS.FechaAsignado

                        Uni = New UnidadesTranspClass(OS.idUnidadTrans)
                        If Uni.Existe Then
                            txtUnidad.Text = OS.idUnidadTrans

                            Tuni = New TipoUniTransClass(Uni.idTipoUnidad)
                            If Tuni.Existe Then
                                lblTipoTransp.Text = Tuni.nomTipoUniTras
                            End If
                        End If

                        OT = New OrdenTrabajoClass(Val(idClave2))
                        If OT.Existe Then
                            txtNomActividad.Text = OT.NotaRecepcion


                            txtidOrdenTrabajo.Text = OT.idOrdenTrabajo



                            Per = New PersonalClass(OT.idPersonalResp)
                            If Per.Existe Then
                                txtidPersonalResp.Text = Per.NombreCompleto
                            End If

                            If OT.idPersonalAyu1 > 0 Then
                                Per = New PersonalClass(OT.idPersonalAyu1)
                                If Per.Existe Then
                                    txtidPersonalAyu1.Text = Per.NombreCompleto
                                End If
                            End If

                            If OT.idPersonalAyu2 > 0 Then
                                Per = New PersonalClass(OT.idPersonalAyu2)
                                If Per.Existe Then
                                    txtidPersonalAyu2.Text = Per.NombreCompleto
                                End If
                            End If

                        End If

                        DOS = New detOrdenServicioClass(Val(idClave2))
                        If DOS.Existe Then
                            txtidLlanta.Text = DOS.idLlanta
                            txtPosicion.Text = DOS.PosLlanta

                            If OS.Estatus = "ASIG" Then
                                txtDuracionHr.Enabled = True
                                txtDiagnostico.Enabled = True
                                If DOS.idActividad > 0 Then
                                    Act = New ActividadesClass(DOS.idActividad)
                                    If Act.Existe Then
                                        txtDuracionHr.Text = Act.DuracionHoras
                                    End If
                                    If _Diagnostico <> "" Then
                                        txtDiagnostico.Text = _Diagnostico
                                    Else
                                        txtDiagnostico.Text = OT.NotaRecepcion
                                    End If

                                Else
                                    txtDuracionHr.Text = ""
                                End If

                            Else


                                txtDuracionHr.Enabled = False
                                txtDiagnostico.Enabled = False

                                If _Diagnostico <> "" Then
                                    txtDiagnostico.Text = _Diagnostico
                                Else
                                    txtDiagnostico.Text = DOS.NotaDiagnostico
                                End If
                                If _DuracionHr Then
                                    txtDuracionHr.Text = _DuracionHr
                                Else
                                    txtDuracionHr.Text = DOS.DuracionActHr
                                End If
                            End If
                            'txtDiagnostico.Focus()
                            cmbCIDPRODUCTO_SERV.Focus()




                        End If

                        If _TipoOS = "TALLER" Then
                            AutoCompletarServicios(cmbCIDPRODUCTO_SERV, OpcTipoOrdenServ.Taller)

                        ElseIf _TipoOS = "LAVADERO" Then
                            AutoCompletarServicios(cmbCIDPRODUCTO_SERV, OpcTipoOrdenServ.Lavadero)
                        ElseIf _TipoOS = "LLANTAS" Then
                            AutoCompletarServicios(cmbCIDPRODUCTO_SERV, OpcTipoOrdenServ.Llantas)
                        End If

                    End If

            End Select



        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try

    End Sub

    Private Sub ActivaCampos()
        txtidOrdenSer.Enabled = False
        txtidOrdenTrabajo.Enabled = False
        txtNomEmpresa.Enabled = False
        dtpFechaAsignado.Enabled = False
        dtpFechaAsignadoHora.Enabled = False
        txtidPersonalResp.Enabled = False
        txtidPersonalAyu1.Enabled = False
        txtidPersonalAyu2.Enabled = False

        txtTipoOrdenServ.Enabled = False
        txtUnidad.Enabled = False
        txtidLlanta.Enabled = False
        txtPosicion.Enabled = False
        txtNomActividad.Enabled = False

        txtDiagnostico.Enabled = True
        txtDuracionHr.Enabled = True


    End Sub


    Private Sub btnMnuOk_Click(sender As Object, e As EventArgs) Handles btnMnuOk.Click
        Try
            Dim objsql As New ToolSQLs

            Dim Mensaje As String = "!! Esta seguro que desea Terminar de Di�gnosticar la Orden de trabajo: " & txtidOrdenTrabajo.Text & "? !!"

            Salida = True

            If Not Validar() Then Exit Sub

            Diagnostico = txtDiagnostico.Text.Trim()

            DuracionHR = txtDuracionHr.Text
            obServicios = New ProductosComClass(cmbCIDPRODUCTO_SERV.SelectedValue, "")
            If obServicios.Existe Then
                CIDPRODUCTO_SERV = obServicios.CIDPRODUCTO
                CCODIGOPRODUCTO_SERV = obServicios.CCODIGOPRODUCTO
                CNOMBREPRODUCTO_SERV = obServicios.CNOMBREPRODUCTO
            End If
            Cantidad_CIDPRODUCTO_SERV = Val(txtCantidad_CIDPRODUCTO_SERV.Text)
            Precio_CIDPRODUCTO_SERV = Val(txtPrecio_CIDPRODUCTO_SERV.Text)



            Me.DialogResult = Windows.Forms.DialogResult.OK

            'If MsgBox(Mensaje, MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then

            '    Diagnostico = txtDiagnostico.Text
            '    DuracionHR = txtDuracionHr.Text
            '    Me.DialogResult = Windows.Forms.DialogResult.OK


            'Else
            '    txtDiagnostico.Focus()


            'End If

        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try
    End Sub

    Private Function Validar() As Boolean
        Validar = True
        txtDiagnostico.Text = "D"
        txtDuracionHr.Text = "1"
        If txtDiagnostico.Text = "" Then
            MsgBox("La Nota de Diagnostico de la actividad No puede ser vacio ", vbInformation, "Aviso" & Me.Text)
            txtDiagnostico.Focus()
            Validar = False
        ElseIf txtDuracionHr.Text = "" Or txtDuracionHr.Text = 0 Then
            MsgBox("La Duraci�n de la actividad No puede ser vacio � Cero", vbInformation, "Aviso" & Me.Text)
            txtDuracionHr.Focus()
            Validar = False

        End If
    End Function

    Private Sub txtDiagnostico_TextChanged(sender As Object, e As EventArgs) Handles txtDiagnostico.TextChanged

    End Sub

    Private Sub txtDiagnostico_KeyUp(sender As Object, e As KeyEventArgs) Handles txtDiagnostico.KeyUp

        If e.KeyValue = Keys.Enter Then
            e.Handled = True
            txtDuracionHr.Focus()
            txtDuracionHr.SelectAll()
        End If
    End Sub

    Private Sub txtDuracionHr_TextChanged(sender As Object, e As EventArgs) Handles txtDuracionHr.TextChanged

    End Sub

    Private Sub txtDuracionHr_KeyUp(sender As Object, e As KeyEventArgs) Handles txtDuracionHr.KeyUp
        If e.KeyValue = Keys.Enter Then
            cmbCIDPRODUCTO_SERV.Focus()

        End If
    End Sub

    Public Function AutoCompletarServicios(ByVal Control As ComboBox, ByVal TipoOS As OpcTipoOrdenServ) As AutoCompleteStringCollection

        Dim Coleccion As New AutoCompleteStringCollection

        obServicios = New ProductosComClass("", "")

        Dim filtro As String = ""
        If TipoOS = OpcTipoOrdenServ.Lavadero Then
            filtro = " AND CIDVALORCLASIFICACION5 IN (14)"
        ElseIf TipoOS = OpcTipoOrdenServ.Taller Then
            filtro = " AND CIDVALORCLASIFICACION5 IN (9,10,11,12,13)"
        ElseIf TipoOS = OpcTipoOrdenServ.Llantas Then
            filtro = " AND CIDVALORCLASIFICACION5 IN (15)"
        End If

        Dim dt As DataTable = obServicios.TablaProdServicios("CTIPOPRODUCTO = 3 " & filtro)
        'Recorrer y cargar los items para el Autocompletado
        For Each row As DataRow In dt.Rows
            Coleccion.Add(Convert.ToString(row("CNOMBREPRODUCTO")))
        Next
        'Ajustamos el control TextBox o ComboBox para recibir los datos de la siguiente manera.
        With Control
            .DataSource = dt
            .DisplayMember = "CNOMBREPRODUCTO"
            .ValueMember = "CCODIGOPRODUCTO"


            .AutoCompleteCustomSource = Coleccion
            .AutoCompleteMode = AutoCompleteMode.Suggest
            .AutoCompleteSource = AutoCompleteSource.CustomSource
        End With

        ' Aca le devuelvo los datos a recuperados de la Base
        Return Coleccion

    End Function

    Private Sub cmbCIDPRODUCTO_SERV_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbCIDPRODUCTO_SERV.SelectedIndexChanged

    End Sub

    Private Sub cmbCIDPRODUCTO_SERV_KeyUp(sender As Object, e As KeyEventArgs) Handles cmbCIDPRODUCTO_SERV.KeyUp
        If e.KeyCode = Keys.Enter Then
            txtCantidad_CIDPRODUCTO_SERV.Text = "1"
            obServicios = New ProductosComClass(cmbCIDPRODUCTO_SERV.SelectedValue, "")
            If obServicios.Existe Then
                txtPrecio_CIDPRODUCTO_SERV.Text = obServicios.CPRECIO1
            End If
            CalculaTotal()
            txtPrecio_CIDPRODUCTO_SERV.Focus()
        End If
    End Sub

    Private Sub CalculaTotal()
        Dim Cantidad, Precio As Decimal
        Cantidad = Val(txtCantidad_CIDPRODUCTO_SERV.Text)
        Precio = Val(txtPrecio_CIDPRODUCTO_SERV.Text)

        txtTotal.Text = (Cantidad * Precio).ToString("C2")



    End Sub

    Private Sub txtPrecio_CIDPRODUCTO_SERV_TextChanged(sender As Object, e As EventArgs) Handles txtPrecio_CIDPRODUCTO_SERV.TextChanged

    End Sub

    Private Sub txtPrecio_CIDPRODUCTO_SERV_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPrecio_CIDPRODUCTO_SERV.KeyPress
        CalculaTotal()
        btnMnuOk_Click(sender, e)
    End Sub
End Class
