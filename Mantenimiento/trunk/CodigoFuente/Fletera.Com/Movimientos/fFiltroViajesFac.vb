'Fecha: 15 / Febrero / 2016
'ROGER GALA PACHECO
'************************************************************************************************************************************
'*  Fecha Modificacion :   15 / Febrero / 2016                                                                                       
'*  
'************************************************************************************************************************************
Public Class fFiltroViajesFac
    Inherits System.Windows.Forms.Form
    Private _Tag As String
    Dim bEsIdentidad As Boolean = True
    Dim OpcionForma As TipoOpcionForma
    Dim TablaBd As String = "Diagnostico"

    Dim MyTablaCli As DataTable = New DataTable(TablaBd)
    Dim MyTablaCombo As DataTable = New DataTable(TablaBd)
    Dim MyTablaViaje As DataTable = New DataTable(TablaBd)
    Dim MyTablaDetViaje As DataTable = New DataTable(TablaBd)

    Dim MyTablaPreCabFac As DataTable = New DataTable(TablaBd)
    'Dim MyTablaConcepFac As DataTable = New DataTable(TablaBd)
    'Dim MyTablaPreDetFac As DataTable = New DataTable(TablaBd)

    'TABLAS para Facturar en Comercial
    Dim MyTablaConcepCOM As DataTable = New DataTable(TablaBd)
    Private ClientesCOM As ClientesComClass
    Private ConceptosCOM As ConceptosCOMClass

    'Private ProdCom As ProductosComClass
    'Private UnidadCom As UnidadesComClass

    Private ObjSql As New ToolSQLs

    Dim Salida As Boolean
    Dim myDataColumn As DataColumn
    Dim myDataRow As DataRow

    Dim CantidadGrid As Double = 0
    Dim BandNoExisteProdEnGrid As Boolean = False

    Dim ObjCom As New FuncionesComClass

    'Dim vCIDPRODUCTO As Integer
    'Dim vCIDUNIDAD As Integer

    'Dim vIdAlmacen As Integer = 1
    Dim IdNoGuia As Integer
    'Dim idFraccion As Integer

    Dim CadCam As String = ""
    Dim StrSql As String = ""

    Dim indice As Integer = 0
    Dim ArraySql() As String

    Dim indiceCOM As Integer = 0
    Dim ArraySqlCOM() As String


    Dim NoIdPreOC As Integer = 0
    Dim NoIdPreSal As Integer = 0

    Dim objNumSig As New CapaNegocio.Tablas

    Dim ContPersonal As Integer = 0
    Dim CostoManoObra As Double = 0
    Dim CostoRef As Double = 0

    Dim vLugOcupado As Integer = 0

    Friend WithEvents cmbClientes As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents gpoRemision As System.Windows.Forms.GroupBox
    Friend WithEvents txtRemision As System.Windows.Forms.TextBox
    Friend WithEvents txtRemFin As System.Windows.Forms.TextBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents txtRemIni As System.Windows.Forms.TextBox
    Friend WithEvents gpoRuta As System.Windows.Forms.GroupBox
    Friend WithEvents cmbRutas As System.Windows.Forms.ComboBox
    Friend WithEvents rdbRutSelec As System.Windows.Forms.RadioButton
    Friend WithEvents rdbRutTodos As System.Windows.Forms.RadioButton
    Friend WithEvents cmdBuscaRuta As System.Windows.Forms.Button
    Friend WithEvents txtRuta As System.Windows.Forms.TextBox
    Friend WithEvents gpoTipoUni As System.Windows.Forms.GroupBox
    Friend WithEvents rdbTUFull As System.Windows.Forms.RadioButton
    Friend WithEvents rdbTUSenc As System.Windows.Forms.RadioButton
    Friend WithEvents rdbTUTodos As System.Windows.Forms.RadioButton
    Friend WithEvents cmbRemisiones As System.Windows.Forms.ComboBox
    Friend WithEvents rdbRemSelec As System.Windows.Forms.RadioButton
    Friend WithEvents rdbRemRango As System.Windows.Forms.RadioButton
    Friend WithEvents rdbRemTodos As System.Windows.Forms.RadioButton
    Friend WithEvents cmdBuscaRem As System.Windows.Forms.Button
    Friend WithEvents btnMnuGenerar As System.Windows.Forms.ToolStripButton
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtNomEmpresa As System.Windows.Forms.TextBox
    Friend WithEvents gpoFecha As System.Windows.Forms.GroupBox
    Friend WithEvents dtpFechaFin As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dtpFechaIni As System.Windows.Forms.DateTimePicker
    Friend WithEvents rdbFecRango As System.Windows.Forms.RadioButton
    Friend WithEvents rdbFecTodos As System.Windows.Forms.RadioButton
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents chkMarcaFacturas As System.Windows.Forms.CheckBox
    Friend WithEvents grdViajesDet As System.Windows.Forms.DataGridView
    Friend WithEvents grdViajes As System.Windows.Forms.DataGridView
    Friend WithEvents txtTOTALVia As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtImpIVAVia As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtImpRetVia As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtSubTotVia As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    '
    'Dim CampoLLave As String = "idHerramienta"
    'Dim TablaBd2 As String = "CatFamilia"
    'Dim TablaBd3 As String = "CatTipoHerramienta"
    'Dim TablaBd4 As String = "CatMarcas"

    'Private Herramienta As HerramientaClass
    'Private Familia As FamiliasClass
    'Private TipoHerramienta As TipoHerramClass
    'Private Marca As MarcaClass

    Private _NoServ As Integer

    'PARA FACTURAR
    Dim TipDocumento As Integer = 0
    Dim BandCreditoConcepto As Boolean = False
    Dim FechaVencimiento As Date = Now
    Dim FechaFactura As Date = Now
    Dim DiasCredito As Integer
    Dim CliRFC As String

    Dim tipoDocto As Integer
    Dim TipDocumentoID As Integer
    Dim TextoDocumento As String = ""

    Dim TIPO_MOV As String = ""
    'Dim OrigenFolioSiguiente As String = ""
    Dim vSerie As String = ""
    Dim vFolio As Double = 0

    Dim lPlantilla As String = ""
    Dim lRutaEntDocs As String = ""
    Dim lPrefijo As String = ""
    Friend WithEvents gvdNumGuiaId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gvdNoRemision As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gvdDescripcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gvdDestino As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gvdVolDescarga As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gvdPrecio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gvdsubTotal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gvdRetencion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gvdIVA As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gpoTipoDoc As System.Windows.Forms.GroupBox
    Friend WithEvents rdbDocPen As System.Windows.Forms.RadioButton
    Friend WithEvents rdbDocVia As System.Windows.Forms.RadioButton
    Dim NomArchivo As String = ""

    'variables nuevas
    'Dim _sqlCon As String
    'Dim _sqlConCom As String
    'Dim _conConfigl As String
    Private _Usuario As String
    Private _IdUsuario As Integer
    Private _con As String
    Friend WithEvents chkMarcaFacturasNO As CheckBox
    Friend WithEvents gvFacturar As DataGridViewCheckBoxColumn
    Friend WithEvents gvNoFacturar As DataGridViewCheckBoxColumn
    Friend WithEvents gvConsecutivo As DataGridViewTextBoxColumn
    Friend WithEvents gvNumGuiaId As DataGridViewTextBoxColumn
    Friend WithEvents gvNoViaje As DataGridViewTextBoxColumn
    Friend WithEvents gvidTractor As DataGridViewTextBoxColumn
    Friend WithEvents gvidRemolque1 As DataGridViewTextBoxColumn
    Friend WithEvents gvidDolly As DataGridViewTextBoxColumn
    Friend WithEvents gvidRemolque2 As DataGridViewTextBoxColumn
    Friend WithEvents gvChofer As DataGridViewTextBoxColumn
    Friend WithEvents gvFecha As DataGridViewTextBoxColumn
    Friend WithEvents gvTipoViaje As DataGridViewTextBoxColumn
    Friend WithEvents gvidCliente As DataGridViewTextBoxColumn
    Friend WithEvents gvFraccion As DataGridViewTextBoxColumn
    Friend WithEvents gvCantidad As DataGridViewTextBoxColumn
    Friend WithEvents gvPrecio As DataGridViewTextBoxColumn
    Friend WithEvents gvImporte As DataGridViewTextBoxColumn
    Friend WithEvents chkDesmarcarTODO As CheckBox
    Private _cveEmpresa As Integer

    Dim banSalida As Boolean = False




#Region " Código generado por el Diseñador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Diseñador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicialización después de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Diseñador de Windows Forms. 
    'No lo modifique con el editor de código.
    Public WithEvents MeStatus1 As ControlStatus.MeStatus
    Friend WithEvents btnMnuOk As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuCancelar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuSalir As System.Windows.Forms.ToolStripButton
    Friend WithEvents GrupoCaptura As System.Windows.Forms.GroupBox
    Friend WithEvents ToolStripMenu As System.Windows.Forms.ToolStrip
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(fFiltroViajesFac))
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle20 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle21 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle19 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle22 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle29 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle30 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle23 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle24 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle25 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle26 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle27 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle28 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.GrupoCaptura = New System.Windows.Forms.GroupBox()
        Me.gpoTipoDoc = New System.Windows.Forms.GroupBox()
        Me.rdbDocPen = New System.Windows.Forms.RadioButton()
        Me.rdbDocVia = New System.Windows.Forms.RadioButton()
        Me.gpoFecha = New System.Windows.Forms.GroupBox()
        Me.dtpFechaFin = New System.Windows.Forms.DateTimePicker()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dtpFechaIni = New System.Windows.Forms.DateTimePicker()
        Me.rdbFecRango = New System.Windows.Forms.RadioButton()
        Me.rdbFecTodos = New System.Windows.Forms.RadioButton()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtNomEmpresa = New System.Windows.Forms.TextBox()
        Me.gpoRuta = New System.Windows.Forms.GroupBox()
        Me.cmbRutas = New System.Windows.Forms.ComboBox()
        Me.rdbRutSelec = New System.Windows.Forms.RadioButton()
        Me.rdbRutTodos = New System.Windows.Forms.RadioButton()
        Me.cmdBuscaRuta = New System.Windows.Forms.Button()
        Me.txtRuta = New System.Windows.Forms.TextBox()
        Me.gpoTipoUni = New System.Windows.Forms.GroupBox()
        Me.rdbTUFull = New System.Windows.Forms.RadioButton()
        Me.rdbTUSenc = New System.Windows.Forms.RadioButton()
        Me.rdbTUTodos = New System.Windows.Forms.RadioButton()
        Me.gpoRemision = New System.Windows.Forms.GroupBox()
        Me.cmbRemisiones = New System.Windows.Forms.ComboBox()
        Me.rdbRemSelec = New System.Windows.Forms.RadioButton()
        Me.rdbRemRango = New System.Windows.Forms.RadioButton()
        Me.rdbRemTodos = New System.Windows.Forms.RadioButton()
        Me.cmdBuscaRem = New System.Windows.Forms.Button()
        Me.txtRemision = New System.Windows.Forms.TextBox()
        Me.txtRemFin = New System.Windows.Forms.TextBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.txtRemIni = New System.Windows.Forms.TextBox()
        Me.cmbClientes = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.ToolStripMenu = New System.Windows.Forms.ToolStrip()
        Me.btnMnuOk = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuGenerar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuCancelar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuSalir = New System.Windows.Forms.ToolStripButton()
        Me.MeStatus1 = New ControlStatus.MeStatus()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.txtTOTALVia = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtImpIVAVia = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtImpRetVia = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtSubTotVia = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.chkMarcaFacturasNO = New System.Windows.Forms.CheckBox()
        Me.chkMarcaFacturas = New System.Windows.Forms.CheckBox()
        Me.grdViajesDet = New System.Windows.Forms.DataGridView()
        Me.gvdNumGuiaId = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gvdNoRemision = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gvdDescripcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gvdDestino = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gvdVolDescarga = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gvdPrecio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gvdsubTotal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gvdRetencion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gvdIVA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.grdViajes = New System.Windows.Forms.DataGridView()
        Me.gvFacturar = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.gvNoFacturar = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.gvConsecutivo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gvNumGuiaId = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gvNoViaje = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gvidTractor = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gvidRemolque1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gvidDolly = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gvidRemolque2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gvChofer = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gvFecha = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gvTipoViaje = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gvidCliente = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gvFraccion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gvCantidad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gvPrecio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gvImporte = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.chkDesmarcarTODO = New System.Windows.Forms.CheckBox()
        Me.GrupoCaptura.SuspendLayout()
        Me.gpoTipoDoc.SuspendLayout()
        Me.gpoFecha.SuspendLayout()
        Me.gpoRuta.SuspendLayout()
        Me.gpoTipoUni.SuspendLayout()
        Me.gpoRemision.SuspendLayout()
        Me.ToolStripMenu.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.grdViajesDet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdViajes, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GrupoCaptura
        '
        Me.GrupoCaptura.Controls.Add(Me.gpoTipoDoc)
        Me.GrupoCaptura.Controls.Add(Me.gpoFecha)
        Me.GrupoCaptura.Controls.Add(Me.Label9)
        Me.GrupoCaptura.Controls.Add(Me.txtNomEmpresa)
        Me.GrupoCaptura.Controls.Add(Me.gpoRuta)
        Me.GrupoCaptura.Controls.Add(Me.gpoTipoUni)
        Me.GrupoCaptura.Controls.Add(Me.gpoRemision)
        Me.GrupoCaptura.Controls.Add(Me.cmbClientes)
        Me.GrupoCaptura.Controls.Add(Me.Label5)
        Me.GrupoCaptura.Location = New System.Drawing.Point(12, 45)
        Me.GrupoCaptura.Name = "GrupoCaptura"
        Me.GrupoCaptura.Size = New System.Drawing.Size(1141, 213)
        Me.GrupoCaptura.TabIndex = 3
        Me.GrupoCaptura.TabStop = False
        Me.GrupoCaptura.Text = "Filtros"
        '
        'gpoTipoDoc
        '
        Me.gpoTipoDoc.Controls.Add(Me.rdbDocPen)
        Me.gpoTipoDoc.Controls.Add(Me.rdbDocVia)
        Me.gpoTipoDoc.Location = New System.Drawing.Point(12, 101)
        Me.gpoTipoDoc.Name = "gpoTipoDoc"
        Me.gpoTipoDoc.Size = New System.Drawing.Size(179, 55)
        Me.gpoTipoDoc.TabIndex = 89
        Me.gpoTipoDoc.TabStop = False
        Me.gpoTipoDoc.Text = "Documentos"
        '
        'rdbDocPen
        '
        Me.rdbDocPen.AutoSize = True
        Me.rdbDocPen.ForeColor = System.Drawing.Color.Red
        Me.rdbDocPen.Location = New System.Drawing.Point(68, 22)
        Me.rdbDocPen.Name = "rdbDocPen"
        Me.rdbDocPen.Size = New System.Drawing.Size(96, 17)
        Me.rdbDocPen.TabIndex = 70
        Me.rdbDocPen.Text = "Penalizaciones"
        Me.rdbDocPen.UseVisualStyleBackColor = True
        '
        'rdbDocVia
        '
        Me.rdbDocVia.AutoSize = True
        Me.rdbDocVia.Checked = True
        Me.rdbDocVia.ForeColor = System.Drawing.Color.Red
        Me.rdbDocVia.Location = New System.Drawing.Point(9, 22)
        Me.rdbDocVia.Name = "rdbDocVia"
        Me.rdbDocVia.Size = New System.Drawing.Size(53, 17)
        Me.rdbDocVia.TabIndex = 68
        Me.rdbDocVia.TabStop = True
        Me.rdbDocVia.Text = "Viajes"
        Me.rdbDocVia.UseVisualStyleBackColor = True
        '
        'gpoFecha
        '
        Me.gpoFecha.Controls.Add(Me.dtpFechaFin)
        Me.gpoFecha.Controls.Add(Me.Label1)
        Me.gpoFecha.Controls.Add(Me.dtpFechaIni)
        Me.gpoFecha.Controls.Add(Me.rdbFecRango)
        Me.gpoFecha.Controls.Add(Me.rdbFecTodos)
        Me.gpoFecha.Location = New System.Drawing.Point(12, 42)
        Me.gpoFecha.Name = "gpoFecha"
        Me.gpoFecha.Size = New System.Drawing.Size(685, 51)
        Me.gpoFecha.TabIndex = 77
        Me.gpoFecha.TabStop = False
        Me.gpoFecha.Text = "Fecha"
        '
        'dtpFechaFin
        '
        Me.dtpFechaFin.Location = New System.Drawing.Point(412, 18)
        Me.dtpFechaFin.Name = "dtpFechaFin"
        Me.dtpFechaFin.Size = New System.Drawing.Size(225, 20)
        Me.dtpFechaFin.TabIndex = 85
        Me.dtpFechaFin.Value = New Date(2016, 4, 27, 0, 0, 0, 0)
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label1.Location = New System.Drawing.Point(386, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(20, 13)
        Me.Label1.TabIndex = 84
        Me.Label1.Text = "AL"
        '
        'dtpFechaIni
        '
        Me.dtpFechaIni.Location = New System.Drawing.Point(146, 18)
        Me.dtpFechaIni.Name = "dtpFechaIni"
        Me.dtpFechaIni.Size = New System.Drawing.Size(225, 20)
        Me.dtpFechaIni.TabIndex = 83
        Me.dtpFechaIni.Value = New Date(2016, 4, 27, 0, 0, 0, 0)
        '
        'rdbFecRango
        '
        Me.rdbFecRango.AutoSize = True
        Me.rdbFecRango.ForeColor = System.Drawing.Color.Red
        Me.rdbFecRango.Location = New System.Drawing.Point(83, 22)
        Me.rdbFecRango.Name = "rdbFecRango"
        Me.rdbFecRango.Size = New System.Drawing.Size(57, 17)
        Me.rdbFecRango.TabIndex = 69
        Me.rdbFecRango.Text = "Rango"
        Me.rdbFecRango.UseVisualStyleBackColor = True
        '
        'rdbFecTodos
        '
        Me.rdbFecTodos.AutoSize = True
        Me.rdbFecTodos.Checked = True
        Me.rdbFecTodos.ForeColor = System.Drawing.Color.Red
        Me.rdbFecTodos.Location = New System.Drawing.Point(9, 22)
        Me.rdbFecTodos.Name = "rdbFecTodos"
        Me.rdbFecTodos.Size = New System.Drawing.Size(55, 17)
        Me.rdbFecTodos.TabIndex = 68
        Me.rdbFecTodos.TabStop = True
        Me.rdbFecTodos.Text = "Todos"
        Me.rdbFecTodos.UseVisualStyleBackColor = True
        '
        'Label9
        '
        Me.Label9.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label9.Location = New System.Drawing.Point(460, 20)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(54, 20)
        Me.Label9.TabIndex = 84
        Me.Label9.Text = "Empresa:"
        '
        'txtNomEmpresa
        '
        Me.txtNomEmpresa.Enabled = False
        Me.txtNomEmpresa.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNomEmpresa.Location = New System.Drawing.Point(524, 20)
        Me.txtNomEmpresa.Name = "txtNomEmpresa"
        Me.txtNomEmpresa.ReadOnly = True
        Me.txtNomEmpresa.Size = New System.Drawing.Size(173, 20)
        Me.txtNomEmpresa.TabIndex = 83
        '
        'gpoRuta
        '
        Me.gpoRuta.Controls.Add(Me.cmbRutas)
        Me.gpoRuta.Controls.Add(Me.rdbRutSelec)
        Me.gpoRuta.Controls.Add(Me.rdbRutTodos)
        Me.gpoRuta.Controls.Add(Me.cmdBuscaRuta)
        Me.gpoRuta.Controls.Add(Me.txtRuta)
        Me.gpoRuta.Location = New System.Drawing.Point(12, 162)
        Me.gpoRuta.Name = "gpoRuta"
        Me.gpoRuta.Size = New System.Drawing.Size(76, 55)
        Me.gpoRuta.TabIndex = 78
        Me.gpoRuta.TabStop = False
        Me.gpoRuta.Text = "Destino"
        Me.gpoRuta.Visible = False
        '
        'cmbRutas
        '
        Me.cmbRutas.AutoCompleteCustomSource.AddRange(New String() {"Regularización", "Vivienda"})
        Me.cmbRutas.FormattingEnabled = True
        Me.cmbRutas.Location = New System.Drawing.Point(295, 21)
        Me.cmbRutas.Name = "cmbRutas"
        Me.cmbRutas.Size = New System.Drawing.Size(293, 21)
        Me.cmbRutas.TabIndex = 71
        '
        'rdbRutSelec
        '
        Me.rdbRutSelec.AutoSize = True
        Me.rdbRutSelec.ForeColor = System.Drawing.Color.Red
        Me.rdbRutSelec.Location = New System.Drawing.Point(82, 22)
        Me.rdbRutSelec.Name = "rdbRutSelec"
        Me.rdbRutSelec.Size = New System.Drawing.Size(81, 17)
        Me.rdbRutSelec.TabIndex = 70
        Me.rdbRutSelec.Text = "Seleccionar"
        Me.rdbRutSelec.UseVisualStyleBackColor = True
        '
        'rdbRutTodos
        '
        Me.rdbRutTodos.AutoSize = True
        Me.rdbRutTodos.Checked = True
        Me.rdbRutTodos.ForeColor = System.Drawing.Color.Red
        Me.rdbRutTodos.Location = New System.Drawing.Point(9, 22)
        Me.rdbRutTodos.Name = "rdbRutTodos"
        Me.rdbRutTodos.Size = New System.Drawing.Size(55, 17)
        Me.rdbRutTodos.TabIndex = 68
        Me.rdbRutTodos.TabStop = True
        Me.rdbRutTodos.Text = "Todos"
        Me.rdbRutTodos.UseVisualStyleBackColor = True
        '
        'cmdBuscaRuta
        '
        Me.cmdBuscaRuta.Image = CType(resources.GetObject("cmdBuscaRuta.Image"), System.Drawing.Image)
        Me.cmdBuscaRuta.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdBuscaRuta.Location = New System.Drawing.Point(247, 15)
        Me.cmdBuscaRuta.Name = "cmdBuscaRuta"
        Me.cmdBuscaRuta.Size = New System.Drawing.Size(32, 32)
        Me.cmdBuscaRuta.TabIndex = 67
        Me.cmdBuscaRuta.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'txtRuta
        '
        Me.txtRuta.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRuta.ForeColor = System.Drawing.Color.Red
        Me.txtRuta.Location = New System.Drawing.Point(169, 19)
        Me.txtRuta.MaxLength = 3
        Me.txtRuta.Name = "txtRuta"
        Me.txtRuta.Size = New System.Drawing.Size(69, 20)
        Me.txtRuta.TabIndex = 15
        Me.txtRuta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'gpoTipoUni
        '
        Me.gpoTipoUni.Controls.Add(Me.rdbTUFull)
        Me.gpoTipoUni.Controls.Add(Me.rdbTUSenc)
        Me.gpoTipoUni.Controls.Add(Me.rdbTUTodos)
        Me.gpoTipoUni.Location = New System.Drawing.Point(197, 101)
        Me.gpoTipoUni.Name = "gpoTipoUni"
        Me.gpoTipoUni.Size = New System.Drawing.Size(227, 55)
        Me.gpoTipoUni.TabIndex = 77
        Me.gpoTipoUni.TabStop = False
        Me.gpoTipoUni.Text = "Tipo Viaje"
        '
        'rdbTUFull
        '
        Me.rdbTUFull.AutoSize = True
        Me.rdbTUFull.ForeColor = System.Drawing.Color.Red
        Me.rdbTUFull.Location = New System.Drawing.Point(165, 22)
        Me.rdbTUFull.Name = "rdbTUFull"
        Me.rdbTUFull.Size = New System.Drawing.Size(51, 17)
        Me.rdbTUFull.TabIndex = 71
        Me.rdbTUFull.Text = "FULL"
        Me.rdbTUFull.UseVisualStyleBackColor = True
        '
        'rdbTUSenc
        '
        Me.rdbTUSenc.AutoSize = True
        Me.rdbTUSenc.ForeColor = System.Drawing.Color.Red
        Me.rdbTUSenc.Location = New System.Drawing.Point(82, 22)
        Me.rdbTUSenc.Name = "rdbTUSenc"
        Me.rdbTUSenc.Size = New System.Drawing.Size(62, 17)
        Me.rdbTUSenc.TabIndex = 70
        Me.rdbTUSenc.Text = "Sencillo"
        Me.rdbTUSenc.UseVisualStyleBackColor = True
        '
        'rdbTUTodos
        '
        Me.rdbTUTodos.AutoSize = True
        Me.rdbTUTodos.Checked = True
        Me.rdbTUTodos.ForeColor = System.Drawing.Color.Red
        Me.rdbTUTodos.Location = New System.Drawing.Point(9, 22)
        Me.rdbTUTodos.Name = "rdbTUTodos"
        Me.rdbTUTodos.Size = New System.Drawing.Size(55, 17)
        Me.rdbTUTodos.TabIndex = 68
        Me.rdbTUTodos.TabStop = True
        Me.rdbTUTodos.Text = "Todos"
        Me.rdbTUTodos.UseVisualStyleBackColor = True
        '
        'gpoRemision
        '
        Me.gpoRemision.Controls.Add(Me.cmbRemisiones)
        Me.gpoRemision.Controls.Add(Me.rdbRemSelec)
        Me.gpoRemision.Controls.Add(Me.rdbRemRango)
        Me.gpoRemision.Controls.Add(Me.rdbRemTodos)
        Me.gpoRemision.Controls.Add(Me.cmdBuscaRem)
        Me.gpoRemision.Controls.Add(Me.txtRemision)
        Me.gpoRemision.Controls.Add(Me.txtRemFin)
        Me.gpoRemision.Controls.Add(Me.Label23)
        Me.gpoRemision.Controls.Add(Me.txtRemIni)
        Me.gpoRemision.Location = New System.Drawing.Point(95, 168)
        Me.gpoRemision.Name = "gpoRemision"
        Me.gpoRemision.Size = New System.Drawing.Size(96, 41)
        Me.gpoRemision.TabIndex = 76
        Me.gpoRemision.TabStop = False
        Me.gpoRemision.Text = "Remisión"
        Me.gpoRemision.Visible = False
        '
        'cmbRemisiones
        '
        Me.cmbRemisiones.AutoCompleteCustomSource.AddRange(New String() {"Regularización", "Vivienda"})
        Me.cmbRemisiones.FormattingEnabled = True
        Me.cmbRemisiones.Location = New System.Drawing.Point(640, 21)
        Me.cmbRemisiones.Name = "cmbRemisiones"
        Me.cmbRemisiones.Size = New System.Drawing.Size(116, 21)
        Me.cmbRemisiones.TabIndex = 71
        '
        'rdbRemSelec
        '
        Me.rdbRemSelec.AutoSize = True
        Me.rdbRemSelec.ForeColor = System.Drawing.Color.Red
        Me.rdbRemSelec.Location = New System.Drawing.Point(427, 22)
        Me.rdbRemSelec.Name = "rdbRemSelec"
        Me.rdbRemSelec.Size = New System.Drawing.Size(81, 17)
        Me.rdbRemSelec.TabIndex = 70
        Me.rdbRemSelec.Text = "Seleccionar"
        Me.rdbRemSelec.UseVisualStyleBackColor = True
        '
        'rdbRemRango
        '
        Me.rdbRemRango.AutoSize = True
        Me.rdbRemRango.ForeColor = System.Drawing.Color.Red
        Me.rdbRemRango.Location = New System.Drawing.Point(83, 22)
        Me.rdbRemRango.Name = "rdbRemRango"
        Me.rdbRemRango.Size = New System.Drawing.Size(57, 17)
        Me.rdbRemRango.TabIndex = 69
        Me.rdbRemRango.Text = "Rango"
        Me.rdbRemRango.UseVisualStyleBackColor = True
        '
        'rdbRemTodos
        '
        Me.rdbRemTodos.AutoSize = True
        Me.rdbRemTodos.Checked = True
        Me.rdbRemTodos.ForeColor = System.Drawing.Color.Red
        Me.rdbRemTodos.Location = New System.Drawing.Point(9, 22)
        Me.rdbRemTodos.Name = "rdbRemTodos"
        Me.rdbRemTodos.Size = New System.Drawing.Size(55, 17)
        Me.rdbRemTodos.TabIndex = 68
        Me.rdbRemTodos.TabStop = True
        Me.rdbRemTodos.Text = "Todos"
        Me.rdbRemTodos.UseVisualStyleBackColor = True
        '
        'cmdBuscaRem
        '
        Me.cmdBuscaRem.Image = CType(resources.GetObject("cmdBuscaRem.Image"), System.Drawing.Image)
        Me.cmdBuscaRem.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdBuscaRem.Location = New System.Drawing.Point(592, 15)
        Me.cmdBuscaRem.Name = "cmdBuscaRem"
        Me.cmdBuscaRem.Size = New System.Drawing.Size(32, 32)
        Me.cmdBuscaRem.TabIndex = 67
        Me.cmdBuscaRem.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'txtRemision
        '
        Me.txtRemision.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRemision.ForeColor = System.Drawing.Color.Red
        Me.txtRemision.Location = New System.Drawing.Point(514, 19)
        Me.txtRemision.MaxLength = 3
        Me.txtRemision.Name = "txtRemision"
        Me.txtRemision.Size = New System.Drawing.Size(69, 20)
        Me.txtRemision.TabIndex = 15
        Me.txtRemision.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtRemFin
        '
        Me.txtRemFin.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRemFin.Location = New System.Drawing.Point(309, 21)
        Me.txtRemFin.MaxLength = 50
        Me.txtRemFin.Name = "txtRemFin"
        Me.txtRemFin.Size = New System.Drawing.Size(101, 20)
        Me.txtRemFin.TabIndex = 19
        '
        'Label23
        '
        Me.Label23.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label23.Location = New System.Drawing.Point(280, 19)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(23, 20)
        Me.Label23.TabIndex = 45
        Me.Label23.Text = "AL"
        '
        'txtRemIni
        '
        Me.txtRemIni.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRemIni.Location = New System.Drawing.Point(170, 19)
        Me.txtRemIni.MaxLength = 50
        Me.txtRemIni.Name = "txtRemIni"
        Me.txtRemIni.Size = New System.Drawing.Size(101, 20)
        Me.txtRemIni.TabIndex = 18
        '
        'cmbClientes
        '
        Me.cmbClientes.AutoCompleteCustomSource.AddRange(New String() {"Regularización", "Vivienda"})
        Me.cmbClientes.FormattingEnabled = True
        Me.cmbClientes.Location = New System.Drawing.Point(56, 17)
        Me.cmbClientes.Name = "cmbClientes"
        Me.cmbClientes.Size = New System.Drawing.Size(402, 21)
        Me.cmbClientes.TabIndex = 67
        '
        'Label5
        '
        Me.Label5.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label5.Location = New System.Drawing.Point(9, 20)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(79, 20)
        Me.Label5.TabIndex = 66
        Me.Label5.Text = "Cliente:"
        '
        'ToolStripMenu
        '
        Me.ToolStripMenu.AutoSize = False
        Me.ToolStripMenu.BackColor = System.Drawing.Color.Silver
        Me.ToolStripMenu.Dock = System.Windows.Forms.DockStyle.None
        Me.ToolStripMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnMnuOk, Me.btnMnuGenerar, Me.btnMnuCancelar, Me.btnMnuSalir})
        Me.ToolStripMenu.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow
        Me.ToolStripMenu.Location = New System.Drawing.Point(3, 0)
        Me.ToolStripMenu.Name = "ToolStripMenu"
        Me.ToolStripMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.ToolStripMenu.Size = New System.Drawing.Size(1194, 42)
        Me.ToolStripMenu.TabIndex = 1
        Me.ToolStripMenu.Text = "ToolStrip1"
        '
        'btnMnuOk
        '
        Me.btnMnuOk.ForeColor = System.Drawing.Color.Red
        Me.btnMnuOk.Image = Global.Fletera.My.Resources.Resources.Save1
        Me.btnMnuOk.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuOk.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuOk.Name = "btnMnuOk"
        Me.btnMnuOk.Size = New System.Drawing.Size(58, 39)
        Me.btnMnuOk.Text = "&APLICAR"
        Me.btnMnuOk.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuGenerar
        '
        Me.btnMnuGenerar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuGenerar.Image = Global.Fletera.My.Resources.Resources._1462930414_filter_data
        Me.btnMnuGenerar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuGenerar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuGenerar.Name = "btnMnuGenerar"
        Me.btnMnuGenerar.Size = New System.Drawing.Size(55, 39)
        Me.btnMnuGenerar.Text = "BUSCAR"
        Me.btnMnuGenerar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuCancelar
        '
        Me.btnMnuCancelar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuCancelar.Image = Global.Fletera.My.Resources.Resources.signos_f_059
        Me.btnMnuCancelar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuCancelar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuCancelar.Name = "btnMnuCancelar"
        Me.btnMnuCancelar.Size = New System.Drawing.Size(71, 39)
        Me.btnMnuCancelar.Text = "&CANCELAR"
        Me.btnMnuCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuSalir
        '
        Me.btnMnuSalir.ForeColor = System.Drawing.Color.Red
        Me.btnMnuSalir.Image = Global.Fletera.My.Resources.Resources.Salida2
        Me.btnMnuSalir.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuSalir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuSalir.Name = "btnMnuSalir"
        Me.btnMnuSalir.Size = New System.Drawing.Size(41, 39)
        Me.btnMnuSalir.Text = "&SALIR"
        Me.btnMnuSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'MeStatus1
        '
        Me.MeStatus1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.MeStatus1.Location = New System.Drawing.Point(0, 596)
        Me.MeStatus1.Margin = New System.Windows.Forms.Padding(4)
        Me.MeStatus1.Name = "MeStatus1"
        Me.MeStatus1.Size = New System.Drawing.Size(1169, 26)
        Me.MeStatus1.TabIndex = 72
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Location = New System.Drawing.Point(12, 260)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(1145, 337)
        Me.TabControl1.TabIndex = 75
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.chkDesmarcarTODO)
        Me.TabPage1.Controls.Add(Me.txtTOTALVia)
        Me.TabPage1.Controls.Add(Me.Label6)
        Me.TabPage1.Controls.Add(Me.txtImpIVAVia)
        Me.TabPage1.Controls.Add(Me.Label4)
        Me.TabPage1.Controls.Add(Me.txtImpRetVia)
        Me.TabPage1.Controls.Add(Me.Label3)
        Me.TabPage1.Controls.Add(Me.txtSubTotVia)
        Me.TabPage1.Controls.Add(Me.Label2)
        Me.TabPage1.Controls.Add(Me.chkMarcaFacturasNO)
        Me.TabPage1.Controls.Add(Me.chkMarcaFacturas)
        Me.TabPage1.Controls.Add(Me.grdViajesDet)
        Me.TabPage1.Controls.Add(Me.grdViajes)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(1137, 311)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Viajes"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'txtTOTALVia
        '
        Me.txtTOTALVia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTOTALVia.Location = New System.Drawing.Point(832, 264)
        Me.txtTOTALVia.MaxLength = 50
        Me.txtTOTALVia.Name = "txtTOTALVia"
        Me.txtTOTALVia.Size = New System.Drawing.Size(101, 20)
        Me.txtTOTALVia.TabIndex = 84
        Me.txtTOTALVia.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label6.Location = New System.Drawing.Point(770, 267)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(42, 13)
        Me.Label6.TabIndex = 85
        Me.Label6.Text = "TOTAL"
        '
        'txtImpIVAVia
        '
        Me.txtImpIVAVia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtImpIVAVia.Location = New System.Drawing.Point(832, 238)
        Me.txtImpIVAVia.MaxLength = 50
        Me.txtImpIVAVia.Name = "txtImpIVAVia"
        Me.txtImpIVAVia.Size = New System.Drawing.Size(101, 20)
        Me.txtImpIVAVia.TabIndex = 82
        Me.txtImpIVAVia.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label4.Location = New System.Drawing.Point(770, 241)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(33, 13)
        Me.Label4.TabIndex = 83
        Me.Label4.Text = "I.V.A."
        '
        'txtImpRetVia
        '
        Me.txtImpRetVia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtImpRetVia.Location = New System.Drawing.Point(832, 212)
        Me.txtImpRetVia.MaxLength = 50
        Me.txtImpRetVia.Name = "txtImpRetVia"
        Me.txtImpRetVia.Size = New System.Drawing.Size(101, 20)
        Me.txtImpRetVia.TabIndex = 80
        Me.txtImpRetVia.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label3.Location = New System.Drawing.Point(770, 215)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(56, 13)
        Me.Label3.TabIndex = 81
        Me.Label3.Text = "Retencion"
        '
        'txtSubTotVia
        '
        Me.txtSubTotVia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSubTotVia.Location = New System.Drawing.Point(832, 186)
        Me.txtSubTotVia.MaxLength = 50
        Me.txtSubTotVia.Name = "txtSubTotVia"
        Me.txtSubTotVia.Size = New System.Drawing.Size(101, 20)
        Me.txtSubTotVia.TabIndex = 78
        Me.txtSubTotVia.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label2.Location = New System.Drawing.Point(770, 189)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(46, 13)
        Me.Label2.TabIndex = 79
        Me.Label2.Text = "Subtotal"
        '
        'chkMarcaFacturasNO
        '
        Me.chkMarcaFacturasNO.AutoSize = True
        Me.chkMarcaFacturasNO.Location = New System.Drawing.Point(163, 6)
        Me.chkMarcaFacturasNO.Name = "chkMarcaFacturasNO"
        Me.chkMarcaFacturasNO.Size = New System.Drawing.Size(143, 17)
        Me.chkMarcaFacturasNO.TabIndex = 77
        Me.chkMarcaFacturasNO.Text = "Marcar Todas NO Aplica"
        Me.chkMarcaFacturasNO.UseVisualStyleBackColor = True
        '
        'chkMarcaFacturas
        '
        Me.chkMarcaFacturas.AutoSize = True
        Me.chkMarcaFacturas.Location = New System.Drawing.Point(24, 6)
        Me.chkMarcaFacturas.Name = "chkMarcaFacturas"
        Me.chkMarcaFacturas.Size = New System.Drawing.Size(124, 17)
        Me.chkMarcaFacturas.TabIndex = 77
        Me.chkMarcaFacturas.Text = "Marcar Todas Aplica"
        Me.chkMarcaFacturas.UseVisualStyleBackColor = True
        '
        'grdViajesDet
        '
        Me.grdViajesDet.AllowUserToAddRows = False
        Me.grdViajesDet.AllowUserToResizeColumns = False
        Me.grdViajesDet.AllowUserToResizeRows = False
        DataGridViewCellStyle16.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        DataGridViewCellStyle16.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle16.SelectionBackColor = System.Drawing.Color.Yellow
        DataGridViewCellStyle16.SelectionForeColor = System.Drawing.Color.Black
        Me.grdViajesDet.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle16
        Me.grdViajesDet.BackgroundColor = System.Drawing.Color.White
        Me.grdViajesDet.ColumnHeadersHeight = 34
        Me.grdViajesDet.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.grdViajesDet.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.gvdNumGuiaId, Me.gvdNoRemision, Me.gvdDescripcion, Me.gvdDestino, Me.gvdVolDescarga, Me.gvdPrecio, Me.gvdsubTotal, Me.gvdRetencion, Me.gvdIVA})
        DataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle20.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle20.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle20.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle20.SelectionBackColor = System.Drawing.Color.Yellow
        DataGridViewCellStyle20.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.grdViajesDet.DefaultCellStyle = DataGridViewCellStyle20
        Me.grdViajesDet.Location = New System.Drawing.Point(8, 180)
        Me.grdViajesDet.Name = "grdViajesDet"
        Me.grdViajesDet.RowHeadersWidth = 26
        DataGridViewCellStyle21.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle21.SelectionBackColor = System.Drawing.Color.Yellow
        DataGridViewCellStyle21.SelectionForeColor = System.Drawing.Color.Black
        Me.grdViajesDet.RowsDefaultCellStyle = DataGridViewCellStyle21
        Me.grdViajesDet.Size = New System.Drawing.Size(756, 114)
        Me.grdViajesDet.TabIndex = 76
        '
        'gvdNumGuiaId
        '
        Me.gvdNumGuiaId.HeaderText = "NumGuiaId"
        Me.gvdNumGuiaId.Name = "gvdNumGuiaId"
        Me.gvdNumGuiaId.Visible = False
        '
        'gvdNoRemision
        '
        Me.gvdNoRemision.HeaderText = "Remision"
        Me.gvdNoRemision.Name = "gvdNoRemision"
        '
        'gvdDescripcion
        '
        Me.gvdDescripcion.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.gvdDescripcion.HeaderText = "Descripcion"
        Me.gvdDescripcion.Name = "gvdDescripcion"
        Me.gvdDescripcion.Width = 88
        '
        'gvdDestino
        '
        DataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.gvdDestino.DefaultCellStyle = DataGridViewCellStyle17
        Me.gvdDestino.HeaderText = "Ruta"
        Me.gvdDestino.Name = "gvdDestino"
        Me.gvdDestino.Width = 200
        '
        'gvdVolDescarga
        '
        DataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.gvdVolDescarga.DefaultCellStyle = DataGridViewCellStyle18
        Me.gvdVolDescarga.HeaderText = "Volumen"
        Me.gvdVolDescarga.Name = "gvdVolDescarga"
        '
        'gvdPrecio
        '
        Me.gvdPrecio.HeaderText = "Precio"
        Me.gvdPrecio.Name = "gvdPrecio"
        '
        'gvdsubTotal
        '
        DataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.gvdsubTotal.DefaultCellStyle = DataGridViewCellStyle19
        Me.gvdsubTotal.HeaderText = "Importe"
        Me.gvdsubTotal.Name = "gvdsubTotal"
        Me.gvdsubTotal.Width = 60
        '
        'gvdRetencion
        '
        Me.gvdRetencion.HeaderText = "Retencion"
        Me.gvdRetencion.Name = "gvdRetencion"
        Me.gvdRetencion.Visible = False
        '
        'gvdIVA
        '
        Me.gvdIVA.HeaderText = "I.V.A"
        Me.gvdIVA.Name = "gvdIVA"
        Me.gvdIVA.Visible = False
        '
        'grdViajes
        '
        Me.grdViajes.AllowUserToAddRows = False
        Me.grdViajes.AllowUserToResizeColumns = False
        Me.grdViajes.AllowUserToResizeRows = False
        DataGridViewCellStyle22.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        DataGridViewCellStyle22.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle22.SelectionBackColor = System.Drawing.Color.Yellow
        DataGridViewCellStyle22.SelectionForeColor = System.Drawing.Color.Black
        Me.grdViajes.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle22
        Me.grdViajes.BackgroundColor = System.Drawing.Color.White
        Me.grdViajes.ColumnHeadersHeight = 34
        Me.grdViajes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.grdViajes.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.gvFacturar, Me.gvNoFacturar, Me.gvConsecutivo, Me.gvNumGuiaId, Me.gvNoViaje, Me.gvidTractor, Me.gvidRemolque1, Me.gvidDolly, Me.gvidRemolque2, Me.gvChofer, Me.gvFecha, Me.gvTipoViaje, Me.gvidCliente, Me.gvFraccion, Me.gvCantidad, Me.gvPrecio, Me.gvImporte})
        DataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle29.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle29.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle29.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle29.SelectionBackColor = System.Drawing.Color.Yellow
        DataGridViewCellStyle29.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle29.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.grdViajes.DefaultCellStyle = DataGridViewCellStyle29
        Me.grdViajes.Location = New System.Drawing.Point(6, 24)
        Me.grdViajes.Name = "grdViajes"
        Me.grdViajes.RowHeadersWidth = 26
        DataGridViewCellStyle30.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle30.SelectionBackColor = System.Drawing.Color.Yellow
        DataGridViewCellStyle30.SelectionForeColor = System.Drawing.Color.Black
        Me.grdViajes.RowsDefaultCellStyle = DataGridViewCellStyle30
        Me.grdViajes.Size = New System.Drawing.Size(1119, 150)
        Me.grdViajes.TabIndex = 75
        '
        'gvFacturar
        '
        Me.gvFacturar.HeaderText = "Aplica"
        Me.gvFacturar.Name = "gvFacturar"
        Me.gvFacturar.Width = 70
        '
        'gvNoFacturar
        '
        Me.gvNoFacturar.HeaderText = "No Aplica"
        Me.gvNoFacturar.Name = "gvNoFacturar"
        Me.gvNoFacturar.Width = 70
        '
        'gvConsecutivo
        '
        Me.gvConsecutivo.HeaderText = "#"
        Me.gvConsecutivo.Name = "gvConsecutivo"
        Me.gvConsecutivo.Width = 30
        '
        'gvNumGuiaId
        '
        Me.gvNumGuiaId.HeaderText = "NumGuiaId"
        Me.gvNumGuiaId.Name = "gvNumGuiaId"
        Me.gvNumGuiaId.Visible = False
        '
        'gvNoViaje
        '
        Me.gvNoViaje.HeaderText = "No. Viaje"
        Me.gvNoViaje.Name = "gvNoViaje"
        '
        'gvidTractor
        '
        DataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.gvidTractor.DefaultCellStyle = DataGridViewCellStyle23
        Me.gvidTractor.HeaderText = "Tractor"
        Me.gvidTractor.Name = "gvidTractor"
        Me.gvidTractor.Width = 60
        '
        'gvidRemolque1
        '
        DataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.gvidRemolque1.DefaultCellStyle = DataGridViewCellStyle24
        Me.gvidRemolque1.HeaderText = "Remolque 1"
        Me.gvidRemolque1.Name = "gvidRemolque1"
        Me.gvidRemolque1.Width = 70
        '
        'gvidDolly
        '
        Me.gvidDolly.HeaderText = "Dolly"
        Me.gvidDolly.Name = "gvidDolly"
        '
        'gvidRemolque2
        '
        Me.gvidRemolque2.HeaderText = "Remolque 2"
        Me.gvidRemolque2.Name = "gvidRemolque2"
        Me.gvidRemolque2.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.gvidRemolque2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.gvidRemolque2.Width = 70
        '
        'gvChofer
        '
        Me.gvChofer.HeaderText = "Chofer"
        Me.gvChofer.Name = "gvChofer"
        Me.gvChofer.Width = 250
        '
        'gvFecha
        '
        Me.gvFecha.HeaderText = "Fecha"
        Me.gvFecha.Name = "gvFecha"
        Me.gvFecha.Width = 150
        '
        'gvTipoViaje
        '
        Me.gvTipoViaje.HeaderText = "Tipo Viaje"
        Me.gvTipoViaje.Name = "gvTipoViaje"
        Me.gvTipoViaje.Width = 80
        '
        'gvidCliente
        '
        DataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.gvidCliente.DefaultCellStyle = DataGridViewCellStyle25
        Me.gvidCliente.HeaderText = "Cliente"
        Me.gvidCliente.Name = "gvidCliente"
        Me.gvidCliente.Visible = False
        Me.gvidCliente.Width = 60
        '
        'gvFraccion
        '
        Me.gvFraccion.HeaderText = "Fraccion"
        Me.gvFraccion.Name = "gvFraccion"
        Me.gvFraccion.Visible = False
        '
        'gvCantidad
        '
        DataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle26.Format = "N2"
        DataGridViewCellStyle26.NullValue = Nothing
        Me.gvCantidad.DefaultCellStyle = DataGridViewCellStyle26
        Me.gvCantidad.HeaderText = "Cantidad"
        Me.gvCantidad.Name = "gvCantidad"
        Me.gvCantidad.Visible = False
        Me.gvCantidad.Width = 80
        '
        'gvPrecio
        '
        DataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle27.Format = "C2"
        DataGridViewCellStyle27.NullValue = Nothing
        Me.gvPrecio.DefaultCellStyle = DataGridViewCellStyle27
        Me.gvPrecio.HeaderText = "Precio"
        Me.gvPrecio.Name = "gvPrecio"
        Me.gvPrecio.Visible = False
        '
        'gvImporte
        '
        DataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle28.Format = "C2"
        DataGridViewCellStyle28.NullValue = Nothing
        Me.gvImporte.DefaultCellStyle = DataGridViewCellStyle28
        Me.gvImporte.HeaderText = "Importe"
        Me.gvImporte.Name = "gvImporte"
        Me.gvImporte.Visible = False
        '
        'chkDesmarcarTODO
        '
        Me.chkDesmarcarTODO.AutoSize = True
        Me.chkDesmarcarTODO.Location = New System.Drawing.Point(312, 6)
        Me.chkDesmarcarTODO.Name = "chkDesmarcarTODO"
        Me.chkDesmarcarTODO.Size = New System.Drawing.Size(143, 17)
        Me.chkDesmarcarTODO.TabIndex = 86
        Me.chkDesmarcarTODO.Text = "Marcar Todas NO Aplica"
        Me.chkDesmarcarTODO.UseVisualStyleBackColor = True
        '
        'fFiltroViajesFac
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(1169, 622)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.MeStatus1)
        Me.Controls.Add(Me.ToolStripMenu)
        Me.Controls.Add(Me.GrupoCaptura)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "fFiltroViajesFac"
        Me.Text = "Viajes a Facturar"
        Me.GrupoCaptura.ResumeLayout(False)
        Me.GrupoCaptura.PerformLayout()
        Me.gpoTipoDoc.ResumeLayout(False)
        Me.gpoTipoDoc.PerformLayout()
        Me.gpoFecha.ResumeLayout(False)
        Me.gpoFecha.PerformLayout()
        Me.gpoRuta.ResumeLayout(False)
        Me.gpoRuta.PerformLayout()
        Me.gpoTipoUni.ResumeLayout(False)
        Me.gpoTipoUni.PerformLayout()
        Me.gpoRemision.ResumeLayout(False)
        Me.gpoRemision.PerformLayout()
        Me.ToolStripMenu.ResumeLayout(False)
        Me.ToolStripMenu.PerformLayout()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        CType(Me.grdViajesDet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdViajes, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region





    Private Sub DespliegaDatosCli(ByVal idCliente As Integer)
        If MyTablaCli.DefaultView.Count > 0 Then
            MyTablaCli.DefaultView.RowFilter = "IdCliente = " & idCliente
            If MyTablaCli.DefaultView.Count > 0 Then
                txtNomEmpresa.Text = MyTablaCli.DefaultView.Item(0).Item("RazonSocial").ToString
            End If
            MyTablaCli.DefaultView.RowFilter = Nothing
            Salida = False
        End If

    End Sub





    Private Sub btnMnuSalir_Click(sender As Object, e As EventArgs) Handles btnMnuSalir.Click
        Me.Close()
    End Sub

    Private Sub btnMnuCancelar_Click(sender As Object, e As EventArgs) Handles btnMnuCancelar.Click
        Cancelar(True)
    End Sub

    Private Sub ActivaCampos(ByVal valor As Boolean, ByVal vOpcion As TipoOpcActivaCampos)
        If vOpcion = TipoOpcActivaCampos.tOpcDESHABTODOS Then

            rdbFecTodos.Checked = True
            rdbRemTodos.Checked = True
            rdbTUTodos.Checked = True
            rdbRutTodos.Checked = True
            'txtTexto.Enabled = False


        ElseIf vOpcion = TipoOpcActivaCampos.tOpcINICIALIZA Then

        End If
    End Sub

    Private Sub LimpiaCampos()
        cmbClientes.SelectedIndex = 0
        txtNomEmpresa.Text = ""
        dtpFechaIni.Value = Now
        dtpFechaFin.Value = Now
        txtRemIni.Text = ""
        txtRemFin.Text = ""
        txtRemision.Text = ""
        cmbRemisiones.DataSource = Nothing
        txtRuta.Text = ""
        cmbRutas.DataSource = Nothing
        'txtTexto.Text = ""

        grdViajes.DataSource = Nothing
        grdViajes.Rows.Clear()

        grdViajesDet.DataSource = Nothing
        grdViajesDet.Rows.Clear()


        txtSubTotVia.Text = ""
        txtImpRetVia.Text = ""
        txtImpIVAVia.Text = ""
        txtTOTALVia.Text = ""

        chkMarcaFacturas.Checked = False

        indice = 0
        indiceCOM = 0



    End Sub

    Private Sub Cancelar(ByVal bFecha As Boolean)




        ActivaCampos(False, TipoOpcActivaCampos.tOpcDESHABTODOS)
        ActivaBotones(False, TipoOpcActivaBoton.tOpcInicializa)

        LimpiaCampos()
        ToolStripMenu.Enabled = True
        TabControl1.SelectedIndex = 0

        If bFecha Then
            dtpFechaIni.Value = Now
            dtpFechaFin.Value = Now
        End If

        cmbClientes.Focus()

        Status("", Me)

    End Sub

    Private Sub ActivaBotones(ByVal Valor As Boolean, Optional ByVal vOpcion As TipoOpcActivaBoton = TipoOpcActivaBoton.tOpcInicializa)
        'Si es Verdadero Se activan Ok,Cancelar y Buscar y Terminar Se desactiva
        If vOpcion = TipoOpcActivaBoton.tOpcConsulta Then
            btnMnuOk.Enabled = False
            btnMnuCancelar.Enabled = Valor
            'btnMnuPendiente.Enabled = Not Valor
            'btnMnuEliminar.Enabled = Not Valor
            'btnMnuReporte.Enabled = Not Valor
            'btnMnuReportePed.Enabled = Not Valor
            btnMnuSalir.Enabled = Not Valor
        ElseIf vOpcion = TipoOpcActivaBoton.tOpcInicializa Then
            btnMnuOk.Enabled = Not Valor
            'btnMnuPendiente.Enabled = Valor
            btnMnuCancelar.Enabled = Valor
            'btnMnuEliminar.Enabled = Valor
            'btnMnuReporte.Enabled = Not Valor
            'btnMnuReportePed.Enabled = Valor
            btnMnuSalir.Enabled = Not Valor

        ElseIf vOpcion = TipoOpcActivaBoton.tOpcInsertar Then
            btnMnuOk.Enabled = False
            'btnMnuPendiente.Enabled = Valor
            btnMnuCancelar.Enabled = Valor
            'btnMnuEliminar.Enabled = Valor
            'btnMnuReporte.Enabled = Not Valor
            'btnMnuReportePed.Enabled = Valor
            btnMnuSalir.Enabled = Not Valor
        End If
    End Sub

    Private Function BuscaDatoCombo(ByVal combo As DataTable, ByVal CampoFiltra As String, ByVal valor As String,
        ByVal tipo As TipoDato, ByVal CampoBusca As String) As String
        BuscaDatoCombo = ""
        Dim Filtro As String = ""
        Dim Resultado As String = ""
        Filtro = CampoFiltra & " = '" & valor & "'"
        'If tipo = TipoDato.TdCadena Then
        '    Filtro = CampoFiltra & " = '" & valor & "'"
        'ElseIf tipo = TipoDato.TdNumerico Then
        '    Filtro = CampoFiltra & " = " & valor
        'ElseIf tipo = TipoDato.TdBoolean Then
        '    Filtro = CampoFiltra & " = " & valor
        'End If
        combo.DefaultView.RowFilter = Nothing
        combo.DefaultView.RowFilter = Filtro
        If combo.DefaultView.Count > 0 Then
            'Return combo.Tables(Nomtabla).DefaultView.Item(0).Item(CampoBusca)
            Resultado = combo.DefaultView.Item(0).Item(CampoBusca)
        End If
        combo.DefaultView.RowFilter = Nothing
        Return Resultado
    End Function


    Private Function InsertaRegGrid(ByVal vTabla As DataTable, ByVal NomGrid As String) As Boolean
        Try
            If NomGrid = grdViajes.Name Then
                grdViajes.Rows.Clear()
                For i = 0 To vTabla.DefaultView.Count - 1
                    grdViajes.Rows.Add()
                    grdViajes.Item("gvConsecutivo", i).Value = i + 1
                    grdViajes.Item("gvFacturar", i).Value = 0
                    grdViajes.Item("gvNoFacturar", i).Value = 0
                    grdViajes.Item("gvNumGuiaId", i).Value = vTabla.DefaultView.Item(i).Item("NumGuiaId")
                    grdViajes.Item("gvNoViaje", i).Value = Trim(vTabla.DefaultView.Item(i).Item("NoViaje").ToString)
                    grdViajes.Item("gvidCliente", i).Value = Trim(vTabla.DefaultView.Item(i).Item("idCliente").ToString)
                    grdViajes.Item("gvidTractor", i).Value = Trim(vTabla.DefaultView.Item(i).Item("idTractor").ToString)
                    grdViajes.Item("gvidRemolque1", i).Value = Trim(vTabla.DefaultView.Item(i).Item("idRemolque1").ToString)
                    grdViajes.Item("gvidDolly", i).Value = Trim(vTabla.DefaultView.Item(i).Item("idDolly").ToString)
                    grdViajes.Item("gvidRemolque2", i).Value = Trim(vTabla.DefaultView.Item(i).Item("idRemolque2").ToString)
                    grdViajes.Item("gvChofer", i).Value = Trim(vTabla.DefaultView.Item(i).Item("Nomcompleto").ToString)
                    '
                    '
                    grdViajes.Item("gvFecha", i).Value = Trim(vTabla.DefaultView.Item(i).Item("Fecha").ToString)
                    grdViajes.Item("gvTipoViaje", i).Value = Trim(vTabla.DefaultView.Item(i).Item("TipoViaje").ToString)
                    grdViajes.Item("gvFraccion", i).Value = Trim(vTabla.DefaultView.Item(i).Item("idFraccion").ToString)

                    If rdbDocVia.Checked Then
                        grdViajes.Columns("gvCantidad").Visible = Not rdbDocVia.Checked
                        grdViajes.Columns("gvCantidad").Visible = Not rdbDocVia.Checked
                        grdViajes.Columns("gvCantidad").Visible = Not rdbDocVia.Checked
                    Else
                        grdViajes.Columns("gvCantidad").Visible = rdbDocPen.Checked
                        grdViajes.Columns("gvCantidad").Visible = rdbDocPen.Checked
                        grdViajes.Columns("gvCantidad").Visible = rdbDocPen.Checked

                        grdViajes.Item("gvCantidad", i).Value = vTabla.DefaultView.Item(i).Item("Cantidad")
                        grdViajes.Item("gvPrecio", i).Value = vTabla.DefaultView.Item(i).Item("Precio")
                        grdViajes.Item("gvImporte", i).Value = vTabla.DefaultView.Item(i).Item("importe")
                    End If





                    'gdConteo.Columns["gdcDiferencia"].Visible = chkVerDif.Checked;  
                Next

                grdViajes.PerformLayout()
            ElseIf NomGrid = grdViajesDet.Name Then
                grdViajesDet.Rows.Clear()

                For i = 0 To vTabla.DefaultView.Count - 1
                    grdViajesDet.Rows.Add()
                    'Consecutivo
                    grdViajesDet.Item("gvdNumGuiaId", i).Value = Trim(vTabla.DefaultView.Item(i).Item("NumGuiaId").ToString)
                    grdViajesDet.Item("gvdNoRemision", i).Value = Trim(vTabla.DefaultView.Item(i).Item("NoRemision").ToString)
                    grdViajesDet.Item("gvdDescripcion", i).Value = Trim(vTabla.DefaultView.Item(i).Item("Descripcion").ToString)
                    grdViajesDet.Item("gvdDestino", i).Value = Trim(vTabla.DefaultView.Item(i).Item("Destino").ToString)
                    grdViajesDet.Item("gvdVolDescarga", i).Value = CDbl(vTabla.DefaultView.Item(i).Item("VolDescarga"))
                    grdViajesDet.Item("gvdPrecio", i).Value = CDbl(vTabla.DefaultView.Item(i).Item("Precio"))
                    grdViajesDet.Item("gvdsubTotal", i).Value = CDbl(vTabla.DefaultView.Item(i).Item("subTotal"))
                    grdViajesDet.Item("gvdRetencion", i).Value = CDbl(vTabla.DefaultView.Item(i).Item("ImpRetencion"))
                    grdViajesDet.Item("gvdIVA", i).Value = CDbl(vTabla.DefaultView.Item(i).Item("ImpIVA"))

                Next

                grdViajesDet.PerformLayout()
            End If


            'grdVentas.Columns("Precio").DefaultCellStyle.Format = "c"
            'grdVentas.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
            Return True
        Catch ex As Exception
            Return False
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, Me.Text)
        End Try

    End Function




    Private Sub rdbFecTodos_CheckedChanged(sender As Object, e As EventArgs) Handles rdbFecTodos.CheckedChanged
        HabilitaCamposFecha()

    End Sub

    Private Sub HabilitaCamposFecha()
        If rdbFecTodos.Checked Then
            rdbFecRango.Checked = False
            dtpFechaIni.Enabled = False
            dtpFechaFin.Enabled = False
        Else
            rdbFecRango.Checked = True
            dtpFechaIni.Enabled = True
            dtpFechaFin.Enabled = True
            dtpFechaIni.Focus()
        End If
    End Sub

    Private Sub rdbRemTodos_CheckedChanged(sender As Object, e As EventArgs) Handles rdbRemTodos.CheckedChanged
        HabilitaCamposRem()
    End Sub

    Private Sub HabilitaCamposRem()
        If rdbRemTodos.Checked Then
            rdbRemRango.Checked = False
            txtRemIni.Enabled = False
            txtRemFin.Enabled = False
            rdbRemSelec.Checked = False
            txtRemision.Enabled = False
            cmdBuscaRem.Enabled = False
            cmbRemisiones.Enabled = False
        ElseIf rdbRemRango.Checked Then
            rdbRemTodos.Checked = False
            txtRemIni.Enabled = True
            txtRemFin.Enabled = True
            rdbRemSelec.Checked = False
            txtRemision.Enabled = False
            cmdBuscaRem.Enabled = False
            cmbRemisiones.Enabled = False
            txtRemIni.Focus()
        ElseIf rdbRemSelec.Checked Then
            rdbRemTodos.Checked = False

            rdbRemRango.Checked = False
            txtRemIni.Enabled = False
            txtRemFin.Enabled = False

            txtRemision.Enabled = True
            cmdBuscaRem.Enabled = True
            cmbRemisiones.Enabled = True
            txtRemision.Focus()
        End If
    End Sub

    Private Sub rdbRemRango_CheckedChanged(sender As Object, e As EventArgs) Handles rdbRemRango.CheckedChanged
        HabilitaCamposRem()
    End Sub

    Private Sub rdbRemSelec_CheckedChanged(sender As Object, e As EventArgs) Handles rdbRemSelec.CheckedChanged
        HabilitaCamposRem()
    End Sub


    Private Sub rdbRutTodos_CheckedChanged(sender As Object, e As EventArgs) Handles rdbRutTodos.CheckedChanged
        HabilitaCamposRuta()
    End Sub

    Private Sub HabilitaCamposRuta()
        If rdbRutTodos.Checked Then
            txtRuta.Enabled = False
            cmdBuscaRuta.Enabled = False
            cmbRutas.Enabled = False
        Else
            txtRuta.Enabled = True
            cmdBuscaRuta.Enabled = True
            cmbRutas.Enabled = True
        End If
    End Sub

    Private Sub grdViajes_CellValueChanged(sender As Object, e As DataGridViewCellEventArgs) Handles grdViajes.CellValueChanged
        'If Salida Then Exit Sub
        'Try
        '    If grdViajes.Columns(e.ColumnIndex).Name = "gvFacturar" Then
        '        If grdViajes.Item("gvFacturar", e.RowIndex).Value = 0 Then
        '            grdViajes.Item("gvFacturar", e.RowIndex).Value = 1
        '        Else
        '            grdViajes.Item("gvFacturar", e.RowIndex).Value = 0
        '        End If
        '    End If
        'Catch ex As Exception

        'End Try
        'grdViajes.EndEdit()

    End Sub


    Private Sub grdViajes_Click(sender As Object, e As EventArgs) Handles grdViajes.Click

        IdNoGuia = grdViajes.Item("gvNumGuiaId", grdViajes.CurrentCell.RowIndex).Value
        'idFraccion = grdViajes.Item("gvFraccion", grdViajes.CurrentCell.RowIndex).Value
        CargaDetalleViaje(IdNoGuia)

        If grdViajes.Columns(grdViajes.CurrentCell.ColumnIndex).Name = "gvFacturar" Then
            If grdViajes.Item("gvFacturar", grdViajes.CurrentCell.RowIndex).Value = 0 Then
                grdViajes.Item("gvFacturar", grdViajes.CurrentCell.RowIndex).Value = 1
                If grdViajes.Item("gvNoFacturar", grdViajes.CurrentCell.RowIndex).Value = 1 Then
                    grdViajes.Item("gvNoFacturar", grdViajes.CurrentCell.RowIndex).Value = 0
                End If
            Else
                grdViajes.Item("gvFacturar", grdViajes.CurrentCell.RowIndex).Value = 0
            End If
            grdViajes.EndEdit()
        End If

        If grdViajes.Columns(grdViajes.CurrentCell.ColumnIndex).Name = "gvNoFacturar" Then
            If grdViajes.Item("gvNoFacturar", grdViajes.CurrentCell.RowIndex).Value = 0 Then
                grdViajes.Item("gvNoFacturar", grdViajes.CurrentCell.RowIndex).Value = 1
                If grdViajes.Item("gvFacturar", grdViajes.CurrentCell.RowIndex).Value = 1 Then
                    grdViajes.Item("gvFacturar", grdViajes.CurrentCell.RowIndex).Value = 0
                End If
            Else
                grdViajes.Item("gvNoFacturar", grdViajes.CurrentCell.RowIndex).Value = 0
            End If
            grdViajes.EndEdit()
        End If


    End Sub

    'Private Sub CargaDetalleViaje(ByVal vNumGuiaId As Integer, ByVal vidFraccion As Integer)
    Private Sub CargaDetalleViaje(ByVal vNumGuiaId As Integer)
        '"(cab.SerieGuia + '' + CAST(cab.NumGuiaId AS varchar(10))) AS NoViaje," & _
        StrSql = ""

        StrSql = "SELECT det.NumGuiaId, " &
        "det.NoRemision, " &
        "det.IdProdTraf, " &
        "prod.Descripcion, " &
        "det.IdPlazaTraf, " &
        "pla.Descripcion AS Destino, " &
        "det.VolDescarga, " &
        "det.Precio, " &
        "det.PorcIVA, " &
        "det.ImpIVA, " &
        "det.ImpRetencion, " &
        "det.subTotal, " &
        "det.idTarea " &
        "FROM dbo.DetGuia det " &
        "INNER JOIN dbo.CatProductos prod ON det.IdProdTraf = prod.IdProducto " &
        " INNER JOIN trafico_plazas_cat pla ON det.IdPlazaTraf = pla.Clave " &
        "WHERE det.baja = 0 and det.NumGuiaId = " & vNumGuiaId &
        " ORDER BY det.Consecutivo"

        '"INNER JOIN dbo.CatProductos prod ON det.IdProdTraf = prod.IdProducto AND prod.IdFraccion = " & vidFraccion & _

        MyTablaDetViaje = Nothing
        'MyTablaDetViaje.Rows.Clear()
        MyTablaDetViaje = BD.ExecuteReturn(StrSql)
        If Not MyTablaDetViaje Is Nothing Then
            If MyTablaDetViaje.Rows.Count > 0 Then
                'grdViajesDet.DataSource = MyTablaDetViaje
                InsertaRegGrid(MyTablaDetViaje, grdViajesDet.Name)
                CalculaTotViajes()
            End If
        End If
    End Sub

    Private Sub CalculaTotViajes()
        Dim Subtotal As Double
        Dim ImpReten As Double
        Dim ImpIVA As Double

        Subtotal = 0
        ImpReten = 0
        ImpIVA = 0

        If grdViajesDet.RowCount > 0 Then
            For i = 0 To grdViajesDet.RowCount - 1
                Subtotal = Subtotal + grdViajesDet.Item("gvdsubTotal", i).Value
                ImpReten = ImpReten + grdViajesDet.Item("gvdRetencion", i).Value
                ImpIVA = ImpIVA + grdViajesDet.Item("gvdIVA", i).Value
            Next
        End If

        txtSubTotVia.Text = Format(Subtotal, "c")
        txtImpRetVia.Text = Format(ImpReten, "c")
        txtImpIVAVia.Text = Format(ImpIVA, "c")
        txtTOTALVia.Text = Format((Subtotal - ImpReten + ImpIVA), "c")
    End Sub


    Private Sub chkMarcaFacturas_CheckedChanged(sender As Object, e As EventArgs) Handles chkMarcaFacturas.CheckedChanged
        If banSalida Then Exit Sub
        If grdViajes.RowCount > 0 Then
            chkMarcaFacturasNO.Checked = IIf(chkMarcaFacturas.Checked, 0, 1)
            For i = 0 To grdViajes.RowCount - 1
                grdViajes.Item("gvFacturar", i).Value = IIf(chkMarcaFacturas.Checked, 1, 0)
                grdViajes.Item("gvNoFacturar", i).Value = IIf(chkMarcaFacturas.Checked, 0, 1)
            Next
        End If
    End Sub

    'Private Function ConsultaDatoServicio(ByVal TipoViaje As String, ByVal NomCampo As String) As String



    Private Sub chkITFecha_CheckedChanged(sender As Object, e As EventArgs)
        InsertarPreTexto()
    End Sub

    Private Sub InsertarPreTexto()
        'txtTexto.Text = ""




    End Sub

    Private Sub chkITTon_CheckedChanged(sender As Object, e As EventArgs)
        InsertarPreTexto()
    End Sub

    Private Sub chkITKm_CheckedChanged(sender As Object, e As EventArgs)
        InsertarPreTexto()
    End Sub

    Private Sub cmdInsTexto_Click(sender As Object, e As EventArgs)
        Dim TextoFinal As String = ""
    End Sub








    Private Function UltimoConsecutivo() As Integer

        Dim Obj As New CapaNegocio.Tablas
        Dim Ultimo As Integer = 0



        Ultimo = objNumSig.NumSigEnTablas(Inicio.CONSTR, "NOMOVIMIENTO", "PARAMETROS", "CABMOVIMIENTOS", " WHERE IDPARAMETRO = 1", " ORDER BY NOMOVIMIENTO DESC")
        'Ultimo = Obj.NumSigEnTablasSinUpdate(Inicio.CONSTR, "Consecutivo", "detPromociones", " WHERE IdPromocion = " & Promocion)

        Return Ultimo
    End Function

    'Private Function UltimoConsecutivoContpaq(ByVal TipDocumento As Integer, _
    '                                          ByVal tipoDocto As String, ByVal TipDocumentoID As Integer) As String

    '    'UltimoConsecutivoContpaq()
    '    UltimoConsecutivoContpaq = ""

    '    Existe = LlenaDatosFox(TipDocumento, "MGW10006", "CCODIGOC01", TipoDato.TdCadena)
    '    If Existe = KEY_RCORRECTO Then
    '        'vSerie = vSerieAdmPAQ
    '        If vFolioAdmPAQ = 0 Then
    '            Existe = LlenaDatosFox(tipoDocto, "MGW10007", "CIDDOCUM01", TipoDato.TdCadena)
    '            If Existe = KEY_RCORRECTO Then
    '                'vFolio = vFolioAdmPAQ
    '                Existe = LlenaDatosFox(TipDocumentoID, "MGW10008", "CIDDOCUM01", TipoDato.TdCadena, tipoDocto)
    '                If Existe = KEY_RCORRECTO Then
    '                    UltimoConsecutivoContpaq = "TIPOCONCEPTO"
    '                    If vFolioAdmPAQ = vFolioAdmPAQVer And vSerieAdmPAQ = vSerieAdmPAQVer Then
    '                        vSerie = vSerieAdmPAQ
    '                        vFolio = vFolioAdmPAQ
    '                    ElseIf vFolioAdmPAQ < vFolioAdmPAQVer And vSerieAdmPAQ = vSerieAdmPAQVer Then
    '                        'que pasa ?
    '                        vSerie = vSerieAdmPAQ
    '                        vFolio = vFolioAdmPAQVer
    '                    ElseIf vFolioAdmPAQ > vFolioAdmPAQVer And vSerieAdmPAQ = vSerieAdmPAQVer Then
    '                        vSerie = vSerieAdmPAQ
    '                        vFolio = vFolioAdmPAQVer
    '                    End If
    '                Else
    '                    'que pasa
    '                End If
    '            Else

    '            End If
    '        Else
    '            'verificar
    '            Existe = LlenaDatosFox(TipDocumentoID, "MGW10008", "CIDDOCUM01", TipoDato.TdCadena, tipoDocto)
    '            If Existe = KEY_RCORRECTO Then
    '                UltimoConsecutivoContpaq = "CONCEPTO"
    '                If vFolioAdmPAQ = vFolioAdmPAQVer And vSerieAdmPAQ = vSerieAdmPAQVer Then
    '                    vSerie = vSerieAdmPAQ
    '                    vFolio = vFolioAdmPAQ
    '                ElseIf vFolioAdmPAQ < vFolioAdmPAQVer And vSerieAdmPAQ = vSerieAdmPAQVer Then
    '                    'que pasa ?
    '                    vSerie = vSerieAdmPAQ
    '                    vFolio = vFolioAdmPAQVer
    '                ElseIf vFolioAdmPAQ > vFolioAdmPAQVer And vSerieAdmPAQ = vSerieAdmPAQVer Then
    '                    vSerie = vSerieAdmPAQ
    '                    vFolio = vFolioAdmPAQ
    '                End If
    '            Else
    '                'que pasa
    '                vSerie = vSerieAdmPAQ
    '                vFolio = vFolioAdmPAQ
    '                UltimoConsecutivoContpaq = "CONCEPTO"
    '            End If
    '        End If


    '    ElseIf Existe = KEY_RINCORRECTO Then

    '        vSerie = ""
    '        vFolio = 0
    '        'no encontro el siguiente folio
    '    ElseIf Existe = KEY_RERROR Then
    '        vSerie = ""
    '        vFolio = 0
    '        'Ocurrio un Error

    '    End If
    'End Function




    Private Function TimbraFactura_SDK(ByVal TipoDocumento As String) As Boolean
        'Dim lError As Integer = 0
        Dim idDoc As String = ""

        FEspera.Show("Timbrando Factura en COMERCIAL ...")

        'MODIFICADO - 19MAR2014 - 001
        'Luego Poner en Parametros
        'If Parametro_CPLAMIGCFD = "" Then
        '    lPlantilla = Trim(BuscaDatoCombo(DsComboMovimiento, "MGW10006", "CCODIGOC01", TipoDocumento, TipoDato.TdCadena, "CPLAMIGCFD"))
        'Else
        '    lPlantilla = Trim(Parametro_CPLAMIGCFD)
        'End If

        'lRutaEntDocs = Trim(BuscaDatoCombo(DsComboMovimiento, "MGW10006", "CCODIGOC01", TipoDocumento, TipoDato.TdCadena, "CRUTAENT01"))

        'lPrefijo = Trim(BuscaDatoCombo(DsComboMovimiento, "MGW10006", "CCODIGOC01", TipoDocumento, TipoDato.TdCadena, "CPREFICON"))


        'MODIFICADO - 19MAR2014 - 001
        'lError = fEmitirDocumento(Trim(cbDocumento.SelectedValue), txtNoSerie.Text.ToString, CDbl(txtIdMovimiento.Text), Parametro_PassFacturacion, "")
        'GEnera Archivo
        If lPrefijo <> "" Then
            NomArchivo = CliRFC & lPrefijo & Format(Val(vFolio), "0000000000")
        Else
            NomArchivo = CliRFC & Format(Val(vFolio), "0000000000")
        End If

        'lError = fLeeDatoDocumento(kDocumento_IdDocumento, idDoc, kLongDescripcion - 1)
        'If lError <> 0 Then
        '    MensajeError(lError)
        '    FEspera.Close()
        '    Exit Function
        'End If

        lLicencia = 205
        'MsgBox("fInicializaLicenseInfo(lLicencia) " & lLicencia)
        lError = fInicializaLicenseInfo(lLicencia)

        If lError <> 0 Then
            MensajeError(lError)
            'fCierraEmpresa()
            'fTerminaSDK()
            Return False
        End If

        Parametro_PassFacturacion = "12345678a"
        lError = fEmitirDocumento(TipoDocumento, vSerie, CDbl(vFolio), Parametro_PassFacturacion, "")
        'lError = fEmitirDocumento(TipoDocumento, txtNoSerie.Text.ToString, CDbl(txtIdMovimiento.Text), Parametro_PassFacturacion, NomArchivo)
        '
        'lError = fEmitirDocumento(Text2.Text, "", lFolio, Text7.Text, lComplemento)
        If lError <> 0 Then
            'MessageBox.Show(rError(lError))
            MensajeError(lError)
            'fCierraEmpresa()
            'fTerminaSDK()


            Return False
        End If


        'lPlantilla = BuscaDatoCombo(DsComboMovimiento, "MGW10006", "cidconce01", TipoDocumento, TipoDato.TdCadena, "CPLAMIGCFD")

        'lPlantilla = "C:\Compacw\Empresas\Reportes\Facturacion\Plantilla_Factura_cfdi_1.htm"
        'txtMsg.Text = "Entregando documento..."

        ''MODIFICADO - 19MAR2014 - 001
        'lError = fEntregEnDiscoXML(cbDocumento.SelectedValue, txtNoSerie.Text.ToString, CDbl(txtIdMovimiento.Text), 1, lPlantilla)
        'If lError <> 0 Then
        '    MensajeError(lError)
        '    'fCierraEmpresa()
        '    'fTerminaSDK()
        '    Return False
        'End If

        '23/ABRIL/2014
        'antes de esta funcion verificar que exista el timbre
        'y si existe el timbre verificar el documento
        'Dim UUID As String = Space(255)

        'lError = fDocumentoUUID(TipoDocumento, vSerie.ToString, CDbl(vFolio), UUID)
        'If lError <> 0 Then
        '    MensajeError(lError)
        'ElseIf UUID <> "" Then


        'End If

        lError = fEntregEnDiscoXML(TipoDocumento, vSerie.ToString, CDbl(vFolio), 1, lPlantilla)
        FEspera.Show("Creando Archivo PDF en COMERCIAL ...")
        If lError <> 0 Then
            MensajeError(lError)
            'fCierraEmpresa()
            'fTerminaSDK()
            Return False
        Else
            lError = fEntregEnDiscoXML(TipoDocumento, vSerie.ToString, CDbl(vFolio), 0, lPlantilla)
            FEspera.Show("Creando Archivo XML en COMERCIAL ...")
            If lError <> 0 Then
                MensajeError(lError)
                Return False
            End If
        End If


        Return True
        'txtMsg.Text = "Documento entregado. Folio: " & lFolioDocto
    End Function





    Private Sub grdViajes_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles grdViajes.CellContentClick

    End Sub

    Private Sub cmbClientes_SelectedValueChanged(sender As Object, e As EventArgs) Handles cmbClientes.SelectedValueChanged
        'Try
        '    DespliegaDatosCli(cmbClientes.SelectedValue)
        'Catch ex As Exception

        'End Try

    End Sub
    'Private Sub cmbClientes_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbClientes.SelectedIndexChanged
    '    'If Salida Then Exit Sub
    '    If Not MyTablaCombo Is Nothing Then
    '        If Not cmbClientes.SelectedValue Is Nothing Then
    '            'Salida = True

    '            'Salida = False
    '        End If

    '    End If

    'End Sub


    Private Sub rdbDocPen_CheckedChanged(sender As Object, e As EventArgs) Handles rdbDocPen.CheckedChanged

    End Sub

    Private Sub dtpFechaIni_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFechaIni.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            dtpFechaFin.Focus()
        End If
    End Sub

#Region "CODIGO NUEVO"
    Sub New(ByVal vTag As String, Optional ByVal vNoServ As Integer = 0)
        InitializeComponent()
        _Tag = vTag
        _NoServ = vNoServ
    End Sub
    Public Sub New(con As String, nomUsuario As String, cveEmpresa As Integer, IdUsuario As Integer)
        _con = con
        BD = New CapaDatos.UtilSQL(con, "Juan")
        InitializeComponent()
        _Usuario = nomUsuario
        _cveEmpresa = cveEmpresa
        _IdUsuario = IdUsuario
        Inicio.CONSTR = _con

    End Sub

    'Sub New(ByVal sqlCon As String, ByVal sqlConComercial As String, ByVal conConfigl As String)
    '    InitializeComponent()
    '    _sqlCon = sqlCon
    '    _sqlConCom = sqlConComercial
    '    _conConfigl = conConfigl

    '    Inicio.CONSTR = _sqlCon
    '    Inicio.CONSTR_COM = _sqlConCom


    'End Sub
    Private Sub fFiltroViajesFac_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Salida = True

        If Inicio.CONSTR = "" Then
            Inicio.CONSTR = _con
        End If
        'Cancelar()
        LlenaCombos()
        HabilitaCamposFecha()

        dtpFechaIni.Value = Now
        dtpFechaFin.Value = Now

        Salida = False
    End Sub
    Private Sub LlenaCombos()
        Try
            StrSql = "SELECT cli.IdCliente, " &
            "cli.idEmpresa, " &
            "emp.RazonSocial, " &
            "cli.Nombre, " &
            "cli.Domicilio, " &
            "cli.RFC " &
            "FROM dbo.CatClientes cli " &
            "INNER JOIN dbo.CatEmpresas emp ON cli.idEmpresa = emp.idEmpresa " &
            "WHERE cli.IdCliente > 0 and cli.idEmpresa = " & _cveEmpresa

            MyTablaCombo.Rows.Clear()

            If BD Is Nothing Then
                BD = New CapaDatos.UtilSQL(_con, "Juan")
            End If

            MyTablaCombo = BD.ExecuteReturn(StrSql)
            If Not MyTablaCombo Is Nothing Then
                If MyTablaCombo.Rows.Count > 0 Then
                    cmbClientes.DataSource = MyTablaCombo
                    cmbClientes.ValueMember = "IdCliente"
                    cmbClientes.DisplayMember = "Nombre"

                    MyTablaCli = MyTablaCombo
                End If
            End If

            If cmbClientes.SelectedValue > 0 Then
                Salida = True
                DespliegaDatosCli(cmbClientes.SelectedValue)
                Salida = False
            End If

        Catch ex As Exception

        End Try
    End Sub
    Private Sub btnMnuGenerar_Click(sender As Object, e As EventArgs) Handles btnMnuGenerar.Click
        Dim StrSQlFiltro As String = ""
        Dim Filtro As String = ""

        Salida = True
        'Fecha
        If rdbFecRango.Checked Then
            'Filtro = " AND cab.FechaHoraFin >= '" & dtpFechaIni.Value.ToShortDateString & " 00:00:00' and cab.FechaHoraFin <= '" & dtpFechaFin.Value.ToShortDateString & " 23:59:59'"
            Filtro = " AND cab.FechaHoraViaje >= '" & dtpFechaIni.Value.ToShortDateString & " 00:00:00.000' and cab.FechaHoraViaje <= '" & dtpFechaFin.Value.ToShortDateString & " 23:59:59.999'"
        End If
        If rdbRemRango.Checked Then
            'Filtro = IIf(Filtro = "", "", Filtro & " AND ") & " det.NoRemision >= " & CInt(txtRemIni.Text) & " and det.NoRemision <= " & CInt(txtRemFin.Text)
            Filtro = Filtro & " AND det.NoRemision >= " & CInt(txtRemIni.Text) & " and det.NoRemision <= " & CInt(txtRemFin.Text)

        End If
        If rdbTUFull.Checked Then
            'Filtro = IIf(Filtro = "", "", Filtro & " AND ") & " cab.TipoViaje = 'F' "
            Filtro = Filtro & " AND cab.TipoViaje = 'F' "
        End If
        If rdbTUSenc.Checked Then
            'Filtro = IIf(Filtro = "", "", Filtro & " AND ") & " cab.TipoViaje = 'S' "
            Filtro = Filtro & " AND cab.TipoViaje = 'S' "
        End If

        If Filtro <> "" Then
            'If rdbDocVia.Checked Then
            '    StrSQlFiltro = "SELECT DISTINCT cab.NumGuiaId FROM dbo.CabGuia cab " & _
            '    "INNER JOIN dbo.DetGuia det ON cab.NumGuiaId = det.NumGuiaId " & _
            '    "WHERE cab.EstatusGuia = 'FINALIZADO' " & Filtro

            '    StrSQlFiltro = " AND cab.NumGuiaId in (" & StrSQlFiltro & ")"

            'Else
            '    StrSQlFiltro = "SELECT DISTINCT cab.NumGuiaId FROM dbo.CabGuia gui " & _
            '    "INNER JOIN dbo.DetGuia det ON gui.NumGuiaId = det.NumGuiaId " & _
            '    "WHERE gui.EstatusGuia = 'FINALIZADO' " & Filtro

            '    StrSQlFiltro = " AND gui.NumGuiaId in (" & StrSQlFiltro & ")"

            'End If
            If rdbDocVia.Checked Then
                StrSQlFiltro = "SELECT DISTINCT cab.NumGuiaId FROM dbo.CabGuia cab " &
                "INNER JOIN dbo.DetGuia det ON cab.NumGuiaId = det.NumGuiaId " &
                "WHERE det.baja = 0 and cab.EstatusGuia in ('FINALIZADO') " & Filtro

            Else
                StrSQlFiltro = "SELECT DISTINCT cab.NumGuiaId FROM dbo.CabGuia cab " &
                "INNER JOIN dbo.DetGuia det ON cab.NumGuiaId = det.NumGuiaId " &
                "WHERE det.baja = 0 and cab.EstatusGuia in ('FINALIZADO','FACTURADO') " & Filtro

            End If

            StrSQlFiltro = " AND cab.NumGuiaId in (" & StrSQlFiltro & ")"
        End If

        If rdbDocVia.Checked Then
            StrSql = "SELECT cab.NumGuiaId, " &
            " (cab.SerieGuia + '' + CAST(cab.NumGuiaId AS varchar(10))) AS NoViaje, " &
            "cab.NumViaje AS NumViaje, " &
            "cab.idCliente, " &
            "cli.Nombre, " &
            "cli.RFC, " &
            "cab.IdEmpresa, " &
            "emp.RazonSocial, " &
            "cab.idOperador, " &
            "ISNULL((CASE WHEN per.Nombre <> '' THEN per.Nombre + ' ' + per.ApellidoPat + ' ' + per.ApellidoMat ELSE per.NombreCompleto end),'') AS Nomcompleto, " &
            "cab.idTractor, " &
            "cab.idRemolque1, " &
            "ISNULL(cab.idDolly,'') AS idDolly," &
            "ISNULL(cab.idRemolque2,'') AS idRemolque2," &
            "cab.FechaHoraViaje as Fecha, " &
            "cab.TipoViaje, " &
            "cli.clave_Fraccion as idFraccion " &
            "FROM dbo.CabGuia cab " &
            "INNER JOIN dbo.CatClientes cli ON cab.idCliente = cli.IdCliente " &
            "INNER JOIN dbo.CatEmpresas emp ON cab.IdEmpresa = emp.idEmpresa " &
            "INNER JOIN dbo.CatPersonal per ON cab.idOperador = per.idPersonal " &
            "WHERE cab.EstatusGuia  in ('FINALIZADO') AND cab.idCliente = " & cmbClientes.SelectedValue &
            IIf(StrSQlFiltro <> "", StrSQlFiltro, "")

            '"cab.FechaHoraFin as Fecha, " & _
            'FechaHoraViaje
        ElseIf rdbDocPen.Checked Then
            StrSql = "SELECT cab.NumGuiaId, " &
            "(cab.SerieGuia + '' + CAST(cab.NumGuiaId AS varchar(10))) AS NoViaje, " &
            "cab.NumViaje, " &
            "cab.idCliente, " &
            "cli.Nombre, cli.RFC, " &
            "cab.IdEmpresa, emp.RazonSocial, " &
            "cab.idOperador, " &
            "ISNULL((CASE WHEN per.Nombre <> '' THEN per.Nombre + ' ' + per.ApellidoPat + ' ' + per.ApellidoMat ELSE per.NombreCompleto end),'') AS Nomcompleto, " &
            "cab.idTractor, cab.idRemolque1, ISNULL(cab.idDolly,'') AS idDolly,ISNULL(cab.idRemolque2,'') AS idRemolque2, " &
            "cab.FechaHoraViaje as Fecha, cab.TipoViaje, cli.clave_Fraccion as idFraccion, " &
            "pen.diferencia AS Cantidad, " &
            "pen.precio AS Precio, " &
            "pen.importe as Importe " &
            "FROM dbo.penalizaciones pen " &
            "left JOIN dbo.CabGuia cab ON pen.idViaje = cab.NumGuiaId " &
            "INNER JOIN dbo.CatClientes cli ON cab.idCliente = cli.IdCliente " &
            "INNER JOIN dbo.CatEmpresas emp ON cab.IdEmpresa = emp.idEmpresa " &
            "INNER JOIN dbo.CatPersonal per ON cab.idOperador = per.idPersonal " &
            "WHERE pen.Estatus in ('FINALIZADO') AND cab.idCliente = " & cmbClientes.SelectedValue &
            IIf(StrSQlFiltro <> "", StrSQlFiltro, "")

            '"and pen.fecha >= '01/06/2016 00:00:00' AND pen.fecha <= '30/06/2016 23:59:59'"
        End If



        MyTablaViaje.Rows.Clear()
        MyTablaViaje = BD.ExecuteReturn(StrSql)
        If Not MyTablaViaje Is Nothing Then
            If MyTablaViaje.Rows.Count > 0 Then
                Salida = True
                Status(MyTablaViaje.Rows.Count & " Viajes", Me)

                InsertaRegGrid(MyTablaViaje, grdViajes.Name)
                'grdViajes.DataSource = MyTablaViaje


                IdNoGuia = grdViajes.Item("gvNumGuiaId", 0).Value
                'idFraccion = grdViajes.Item("gvFraccion", 0).Value
                CargaDetalleViaje(IdNoGuia)

                'ActivaBotones()

                Salida = False
            Else
                'No se encontraron viajes
            End If
        End If
        Salida = False
    End Sub
    Private Sub btnMnuOk_Click(sender As Object, e As EventArgs) Handles btnMnuOk.Click
        Dim BandGraba As Boolean = False
        Dim objTool As New ToolSQLs
        Dim codigoCliente As String = ""
        Dim IdCliente As Integer = 0
        'Dim SubTotal As Double = 0
        'Dim IVA As Double = 0
        'Dim Retencion As Double = 0
        'Dim km As Double = 0
        'Dim Toneladas As Double = 0
        'Dim NoMovimiento As Integer = 0


        Try
            'If Not Validar() Then Exit Sub
            'Dim tipoDocto As String = BuscaDatoCombo(MyTablaConcepCOM, "CCODIGOCONCEPTO", TipDocumento, TipoDato.TdCadena, "CIDCONCEPTODOCUMENTO")
            'Dim TipDocumentoID As Integer = BuscaDatoCombo(MyTablaConcepCOM, "CCODIGOCONCEPTO", TipDocumento, TipoDato.TdCadena, "CIDCONCE01")
            'Dim TextoDocumento As String = Trim(cmbDocumCOM.Text)
            'BandCreditoConcepto = BuscaDatoCombo(MyTablaConcepCOM, "CCODIGOCONCEPTO", TipDocumento, TipoDato.TdBoolean, "CDOCTOACREDITO")

            codigoCliente = cmbClientes.SelectedValue
            Salida = True

            If MsgBox("!! Esta seguro que desea Aplicar para Facturar los viajes Seleccionados ? !!", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                'Para Interno
                FEspera.StartPosition = FormStartPosition.CenterScreen
                FEspera.Show("Guardando Datos Locales ...")

                indice = 0
                'ACTUALIZAR GUIAS
                grdViajes.Refresh()

                For i = 0 To grdViajes.RowCount - 1
                    If grdViajes.Item("gvFacturar", i).Value = 1 Then
                        If rdbDocVia.Checked Then
                            StrSql = objTool.ActualizaCampoTabla("CabGuia", "EstatusGuia", TipoDato.TdCadena, "TERMINADO",
                                        "NumGuiaId", TipoDato.TdNumerico, grdViajes.Item("gvNumGuiaId", i).Value)
                            ReDim Preserve ArraySql(indice)
                            ArraySql(indice) = StrSql
                            indice += 1
                        End If
                    ElseIf grdViajes.Item("gvNoFacturar", i).Value = 1 Then
                        If rdbDocVia.Checked Then
                            StrSql = objTool.ActualizaCampoTabla("CabGuia", "EstatusGuia", TipoDato.TdCadena, "CANCEL",
                                        "NumGuiaId", TipoDato.TdNumerico, grdViajes.Item("gvNumGuiaId", i).Value)
                            ReDim Preserve ArraySql(indice)
                            ArraySql(indice) = StrSql
                            indice += 1
                        End If
                    End If
                Next

            End If

            If indice > 0 Then
                Dim obj As New CapaNegocio.Tablas
                'Dim Respuesta As String = "EFECTUADO"
                Dim Respuesta As String = ""

                Respuesta = obj.EjecutarSql(Inicio.CONSTR, "SQL", ArraySql, Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, indice)
                FEspera.Close()
                If Respuesta = "EFECTUADO" Then
                    MsgBox("Actualizacion efectuada satisfactoriamente", MsgBoxStyle.Exclamation, Me.Text)
                    If indiceCOM > 0 Then

                    End If

                ElseIf Respuesta = "NOEFECTUADO" Then

                ElseIf Respuesta = "ERROR" Then
                    indice = 0
                    indiceCOM = 0

                    SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")
                End If
            Else
                MsgBox("No se Encontraron Registros que Grabar", MsgBoxStyle.Exclamation, Me.Text)
            End If
            Cancelar(False)
            'End If
            Salida = False
        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
            FEspera.Close()

        End Try
    End Sub

    Private Sub chkMarcaFacturasNO_CheckedChanged(sender As Object, e As EventArgs) Handles chkMarcaFacturasNO.CheckedChanged
        If banSalida Then Exit Sub

        If grdViajes.RowCount > 0 Then
            chkMarcaFacturas.Checked = IIf(chkMarcaFacturasNO.Checked, 0, 1)
            For i = 0 To grdViajes.RowCount - 1
                grdViajes.Item("gvNoFacturar", i).Value = IIf(chkMarcaFacturasNO.Checked, 1, 0)
                grdViajes.Item("gvFacturar", i).Value = IIf(chkMarcaFacturasNO.Checked, 0, 1)
            Next
        End If
    End Sub

    Private Sub chkDesmarcarTODO_CheckedChanged(sender As Object, e As EventArgs) Handles chkDesmarcarTODO.CheckedChanged
        If banSalida Then Exit Sub
        If grdViajes.RowCount > 0 Then
            banSalida = True
            chkMarcaFacturas.Checked = False
            chkMarcaFacturasNO.Checked = False
            chkDesmarcarTODO.Checked = False


            For i = 0 To grdViajes.RowCount - 1
                If Not grdViajes.Rows(i).Cells(grdViajes.CurrentCell.RowIndex).ReadOnly Then
                    'grdViajes.Item("gvFacturar", i).Value = IIf(chkMarcaFacturas.Checked, 1, 0)
                    'grdViajes.Item("gvNoFacturar", i).Value = IIf(chkMarcaFacturasNO.Checked, 1, 0)

                    grdViajes.Item("gvFacturar", i).Value = 0
                    grdViajes.Item("gvNoFacturar", i).Value = 0

                End If
            Next
            banSalida = False

        End If
    End Sub

#End Region
End Class
