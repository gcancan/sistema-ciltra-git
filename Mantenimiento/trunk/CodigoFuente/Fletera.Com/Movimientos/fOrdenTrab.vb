'Fecha: 18 / Febrero / 2016
'ROGER GALA PACHECO
'************************************************************************************************************************************
'*  Fecha Modificacion :   18 / Febrero / 2016                                                                                       
'*  
'************************************************************************************************************************************
Imports System.Threading
Imports Microsoft.VisualBasic.PowerPacks

Public Class fOrdenTrab
    Inherits System.Windows.Forms.Form
    Friend WithEvents btnMnuOk As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuCancelar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuSalir As System.Windows.Forms.ToolStripButton
    Private _Tag As String
    Dim bEsIdentidad As Boolean = True
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtNomTipOrdServ As System.Windows.Forms.TextBox
    Friend WithEvents cmbTipoServ As System.Windows.Forms.ComboBox
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents txtDescripcion As System.Windows.Forms.TextBox
    Friend WithEvents cmbDivision As System.Windows.Forms.ComboBox
    Friend WithEvents lblDivision As System.Windows.Forms.Label
    Friend WithEvents cmbServicios As System.Windows.Forms.ComboBox
    Dim Salida As Boolean
    Private _con As String
    Private _Usuario As String
    Private _cveEmpresa As Integer
    Private _idTipOrdServ As Integer
    Private _NomTipOrdServ As String
    Private _NumOrdServ As Integer
    Private _bModifica As Boolean
    Private _TipoServ As Integer
    Dim myTablaRelaciones As DataTable
    Dim ObjTipOrdServ As New TiposOrdenServicio(0)
    Dim TipoServicio As TipoServClass
    Dim Servicio As ServicioClass
    Dim myTablaServicios As DataTable
    Dim Division As DivisionClass
    Dim TablaBd As String = "OrdTrab"
    Dim MyTablaComboDiv As DataTable = New DataTable(TablaBd)
    Dim MyTablaComboActiv As DataTable = New DataTable(TablaBd)
    Friend WithEvents rdbOpcDesc As System.Windows.Forms.RadioButton
    Friend WithEvents cmbActividad As System.Windows.Forms.ComboBox
    Friend WithEvents rdbOpcAct As System.Windows.Forms.RadioButton
    Dim MyTablaRelServAct As DataTable = New DataTable(TablaBd)
    Friend WithEvents cmbUnidades As System.Windows.Forms.ComboBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Dim Actividades As ActividadesClass
    Dim _Unidades As List(Of UnidadesTranspClass) = New List(Of UnidadesTranspClass)()
    Public MyTablaRes As New DataTable
    Dim myDataRow As DataRow
    Friend WithEvents grdOrdenTrabajo As System.Windows.Forms.DataGridView
    Friend WithEvents rdbOpcServ As System.Windows.Forms.RadioButton
    Dim myDataColumn As DataColumn

    Dim bandPrimera As Boolean = False
    Dim vUnidadAct As String = ""

    Dim vIdRelacion As Integer
    Dim vNomTabla As String
    Dim vNomCampo As String
    Dim BExisteTipoServ As Boolean = False
    Friend WithEvents txtOrdenServicio As System.Windows.Forms.TextBox

    Dim OrdenServicio As OrdenServClass
    Friend WithEvents ConMnuOrdTra As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents ModificarToolStripMenuOrdTra As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CancelarToolStripMenuOrdTra As System.Windows.Forms.ToolStripMenuItem
    Dim Clave As Integer
    Dim OrdenTrabajo As Integer
    Friend WithEvents GpoPosiciones As GroupBox
    'Friend WithEvents ShapeContainer2 As PowerPacks.ShapeContainer
    'Friend WithEvents LineShape1 As PowerPacks.LineShape
    Dim idOrdenActividad As Integer


    Friend WithEvents gpoPosicion As GroupBox
    Friend WithEvents txtPosicion As TextBox
    Friend WithEvents Label13 As Label
    Friend WithEvents txtTipoUni As TextBox
    Friend WithEvents Label12 As Label
    Friend WithEvents txtUniTrans As TextBox
    Friend WithEvents Label33 As Label

    'llantas
    Dim UniTrans As UnidadesTranspClass
    Dim TipUni As TipoUniTransClass
    Dim ClasLlan As ClasLlanClass
    Dim rdbPosicion() As RadioButton
    Dim TablaEstruc As DataTable
    Dim llanta As LlantasClass
    Dim llantaProd As LLantasProductoClass


    Private Marca As MarcaClass
    Dim tipllanta As TipoLlantaClass
    Dim disenio As CatDiseniosClass


    Private Empresa As EmpresaClass
    Dim tt As New ToolTip
    Dim TextoToolTip As String = ""
    Dim NomEmpresa As String = ""
    Dim NomMarca As String = ""
    Dim NomTipLlanta As String = ""
    Dim NomDisenio As String = ""

    Friend WithEvents otCLAVE As DataGridViewTextBoxColumn
    Friend WithEvents otidOrdenTrabajo As DataGridViewTextBoxColumn
    Friend WithEvents otNotaRecepcion As DataGridViewTextBoxColumn
    Friend WithEvents otNomDivision As DataGridViewTextBoxColumn
    Friend WithEvents otidOrdenSer As DataGridViewTextBoxColumn
    Friend WithEvents otidDivision As DataGridViewTextBoxColumn
    Friend WithEvents otidServicio As DataGridViewTextBoxColumn
    Friend WithEvents otidActividad As DataGridViewTextBoxColumn
    Friend WithEvents otDuracionActHr As DataGridViewTextBoxColumn
    Friend WithEvents otIdUnidadTransp As DataGridViewTextBoxColumn
    Friend WithEvents otidTipoServicio As DataGridViewTextBoxColumn
    Friend WithEvents otidOrdenActividad As DataGridViewTextBoxColumn
    Friend WithEvents otidLlanta As DataGridViewTextBoxColumn
    Friend WithEvents otPosLlanta As DataGridViewTextBoxColumn
    Friend WithEvents lblPosicion As Label
    Friend WithEvents cmbPosicion As ComboBox
    Dim pos As Integer

    Dim MyTablaComboLlanta As DataTable = New DataTable(TablaBd)
    Dim MyTablaComboPosicion As DataTable = New DataTable(TablaBd)
    Dim idLlanta As String = ""
    Friend WithEvents lblLlanta As Label
    Friend WithEvents cmbidLlanta As ComboBox
    Dim PosLLanta As Integer = 0
    Dim _OpcionTipoOrdServ As TipoOrdenServicio

    Dim _UnidadRecibe As String

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents ToolStripMenu As System.Windows.Forms.ToolStrip
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(fOrdenTrab))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.ToolStripMenu = New System.Windows.Forms.ToolStrip()
        Me.btnMnuOk = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuCancelar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuSalir = New System.Windows.Forms.ToolStripButton()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtNomTipOrdServ = New System.Windows.Forms.TextBox()
        Me.cmbTipoServ = New System.Windows.Forms.ComboBox()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.txtDescripcion = New System.Windows.Forms.TextBox()
        Me.cmbDivision = New System.Windows.Forms.ComboBox()
        Me.lblDivision = New System.Windows.Forms.Label()
        Me.cmbServicios = New System.Windows.Forms.ComboBox()
        Me.rdbOpcDesc = New System.Windows.Forms.RadioButton()
        Me.cmbActividad = New System.Windows.Forms.ComboBox()
        Me.rdbOpcAct = New System.Windows.Forms.RadioButton()
        Me.cmbUnidades = New System.Windows.Forms.ComboBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.grdOrdenTrabajo = New System.Windows.Forms.DataGridView()
        Me.otCLAVE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.otidOrdenTrabajo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.otNotaRecepcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.otNomDivision = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.otidOrdenSer = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.otidDivision = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.otidServicio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.otidActividad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.otDuracionActHr = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.otIdUnidadTransp = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.otidTipoServicio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.otidOrdenActividad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.otidLlanta = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.otPosLlanta = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ConMnuOrdTra = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ModificarToolStripMenuOrdTra = New System.Windows.Forms.ToolStripMenuItem()
        Me.CancelarToolStripMenuOrdTra = New System.Windows.Forms.ToolStripMenuItem()
        Me.rdbOpcServ = New System.Windows.Forms.RadioButton()
        Me.txtOrdenServicio = New System.Windows.Forms.TextBox()
        Me.GpoPosiciones = New System.Windows.Forms.GroupBox()
        'Me.ShapeContainer2 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        'Me.LineShape1 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.gpoPosicion = New System.Windows.Forms.GroupBox()
        Me.txtPosicion = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtTipoUni = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtUniTrans = New System.Windows.Forms.TextBox()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.lblPosicion = New System.Windows.Forms.Label()
        Me.cmbPosicion = New System.Windows.Forms.ComboBox()
        Me.lblLlanta = New System.Windows.Forms.Label()
        Me.cmbidLlanta = New System.Windows.Forms.ComboBox()
        Me.ToolStripMenu.SuspendLayout()
        CType(Me.grdOrdenTrabajo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ConMnuOrdTra.SuspendLayout()
        Me.GpoPosiciones.SuspendLayout()
        Me.gpoPosicion.SuspendLayout()
        Me.SuspendLayout()
        '
        'ToolStripMenu
        '
        Me.ToolStripMenu.AutoSize = False
        Me.ToolStripMenu.BackColor = System.Drawing.Color.Silver
        Me.ToolStripMenu.Dock = System.Windows.Forms.DockStyle.None
        Me.ToolStripMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnMnuOk, Me.btnMnuCancelar, Me.btnMnuSalir})
        Me.ToolStripMenu.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow
        Me.ToolStripMenu.Location = New System.Drawing.Point(3, 0)
        Me.ToolStripMenu.Name = "ToolStripMenu"
        Me.ToolStripMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.ToolStripMenu.Size = New System.Drawing.Size(1152, 42)
        Me.ToolStripMenu.TabIndex = 1
        Me.ToolStripMenu.Text = "ToolStrip1"
        '
        'btnMnuOk
        '
        Me.btnMnuOk.ForeColor = System.Drawing.Color.Red
        Me.btnMnuOk.Image = CType(resources.GetObject("btnMnuOk.Image"), System.Drawing.Image)
        Me.btnMnuOk.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuOk.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuOk.Name = "btnMnuOk"
        Me.btnMnuOk.Size = New System.Drawing.Size(56, 39)
        Me.btnMnuOk.Text = "&GRABAR"
        Me.btnMnuOk.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuCancelar
        '
        Me.btnMnuCancelar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuCancelar.Image = CType(resources.GetObject("btnMnuCancelar.Image"), System.Drawing.Image)
        Me.btnMnuCancelar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuCancelar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuCancelar.Name = "btnMnuCancelar"
        Me.btnMnuCancelar.Size = New System.Drawing.Size(71, 39)
        Me.btnMnuCancelar.Text = "&CANCELAR"
        Me.btnMnuCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuSalir
        '
        Me.btnMnuSalir.ForeColor = System.Drawing.Color.Red
        Me.btnMnuSalir.Image = CType(resources.GetObject("btnMnuSalir.Image"), System.Drawing.Image)
        Me.btnMnuSalir.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuSalir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuSalir.Name = "btnMnuSalir"
        Me.btnMnuSalir.Size = New System.Drawing.Size(41, 39)
        Me.btnMnuSalir.Text = "&SALIR"
        Me.btnMnuSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'Label9
        '
        Me.Label9.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label9.Location = New System.Drawing.Point(351, 52)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(329, 13)
        Me.Label9.TabIndex = 102
        Me.Label9.Text = "Tipo de Orden de Servicio"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'txtNomTipOrdServ
        '
        Me.txtNomTipOrdServ.Enabled = False
        Me.txtNomTipOrdServ.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNomTipOrdServ.ForeColor = System.Drawing.Color.Red
        Me.txtNomTipOrdServ.Location = New System.Drawing.Point(354, 72)
        Me.txtNomTipOrdServ.Name = "txtNomTipOrdServ"
        Me.txtNomTipOrdServ.ReadOnly = True
        Me.txtNomTipOrdServ.Size = New System.Drawing.Size(326, 26)
        Me.txtNomTipOrdServ.TabIndex = 101
        Me.txtNomTipOrdServ.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'cmbTipoServ
        '
        Me.cmbTipoServ.FormattingEnabled = True
        Me.cmbTipoServ.Location = New System.Drawing.Point(158, 77)
        Me.cmbTipoServ.Name = "cmbTipoServ"
        Me.cmbTipoServ.Size = New System.Drawing.Size(192, 21)
        Me.cmbTipoServ.TabIndex = 2
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label37.Location = New System.Drawing.Point(12, 78)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(84, 13)
        Me.Label37.TabIndex = 103
        Me.Label37.Text = "Tipo de Servicio"
        '
        'txtDescripcion
        '
        Me.txtDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDescripcion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescripcion.Location = New System.Drawing.Point(158, 166)
        Me.txtDescripcion.MaxLength = 50
        Me.txtDescripcion.Multiline = True
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(522, 40)
        Me.txtDescripcion.TabIndex = 6
        '
        'cmbDivision
        '
        Me.cmbDivision.FormattingEnabled = True
        Me.cmbDivision.Location = New System.Drawing.Point(158, 239)
        Me.cmbDivision.Name = "cmbDivision"
        Me.cmbDivision.Size = New System.Drawing.Size(302, 21)
        Me.cmbDivision.TabIndex = 8
        '
        'lblDivision
        '
        Me.lblDivision.AutoSize = True
        Me.lblDivision.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lblDivision.Location = New System.Drawing.Point(12, 242)
        Me.lblDivision.Name = "lblDivision"
        Me.lblDivision.Size = New System.Drawing.Size(47, 13)
        Me.lblDivision.TabIndex = 107
        Me.lblDivision.Text = "Divisi�n:"
        '
        'cmbServicios
        '
        Me.cmbServicios.FormattingEnabled = True
        Me.cmbServicios.Location = New System.Drawing.Point(158, 139)
        Me.cmbServicios.Name = "cmbServicios"
        Me.cmbServicios.Size = New System.Drawing.Size(192, 21)
        Me.cmbServicios.TabIndex = 5
        '
        'rdbOpcDesc
        '
        Me.rdbOpcDesc.AutoSize = True
        Me.rdbOpcDesc.Checked = True
        Me.rdbOpcDesc.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.rdbOpcDesc.Location = New System.Drawing.Point(15, 189)
        Me.rdbOpcDesc.Name = "rdbOpcDesc"
        Me.rdbOpcDesc.Size = New System.Drawing.Size(84, 17)
        Me.rdbOpcDesc.TabIndex = 110
        Me.rdbOpcDesc.TabStop = True
        Me.rdbOpcDesc.Text = "Descripci�n:"
        Me.rdbOpcDesc.UseVisualStyleBackColor = True
        '
        'cmbActividad
        '
        Me.cmbActividad.FormattingEnabled = True
        Me.cmbActividad.Location = New System.Drawing.Point(158, 212)
        Me.cmbActividad.Name = "cmbActividad"
        Me.cmbActividad.Size = New System.Drawing.Size(522, 21)
        Me.cmbActividad.TabIndex = 7
        '
        'rdbOpcAct
        '
        Me.rdbOpcAct.AutoSize = True
        Me.rdbOpcAct.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.rdbOpcAct.Location = New System.Drawing.Point(15, 213)
        Me.rdbOpcAct.Name = "rdbOpcAct"
        Me.rdbOpcAct.Size = New System.Drawing.Size(72, 17)
        Me.rdbOpcAct.TabIndex = 112
        Me.rdbOpcAct.Text = "Actividad:"
        Me.rdbOpcAct.UseVisualStyleBackColor = True
        '
        'cmbUnidades
        '
        Me.cmbUnidades.FormattingEnabled = True
        Me.cmbUnidades.Location = New System.Drawing.Point(158, 44)
        Me.cmbUnidades.Name = "cmbUnidades"
        Me.cmbUnidades.Size = New System.Drawing.Size(192, 21)
        Me.cmbUnidades.TabIndex = 1
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label10.Location = New System.Drawing.Point(9, 47)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(113, 13)
        Me.Label10.TabIndex = 114
        Me.Label10.Text = "Unidad de Transporte:"
        '
        'grdOrdenTrabajo
        '
        Me.grdOrdenTrabajo.AllowUserToAddRows = False
        Me.grdOrdenTrabajo.AllowUserToResizeColumns = False
        Me.grdOrdenTrabajo.AllowUserToResizeRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Yellow
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black
        Me.grdOrdenTrabajo.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.grdOrdenTrabajo.BackgroundColor = System.Drawing.Color.White
        Me.grdOrdenTrabajo.ColumnHeadersHeight = 34
        Me.grdOrdenTrabajo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.grdOrdenTrabajo.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.otCLAVE, Me.otidOrdenTrabajo, Me.otNotaRecepcion, Me.otNomDivision, Me.otidOrdenSer, Me.otidDivision, Me.otidServicio, Me.otidActividad, Me.otDuracionActHr, Me.otIdUnidadTransp, Me.otidTipoServicio, Me.otidOrdenActividad, Me.otidLlanta, Me.otPosLlanta})
        Me.grdOrdenTrabajo.ContextMenuStrip = Me.ConMnuOrdTra
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Yellow
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.grdOrdenTrabajo.DefaultCellStyle = DataGridViewCellStyle2
        Me.grdOrdenTrabajo.Location = New System.Drawing.Point(12, 266)
        Me.grdOrdenTrabajo.Name = "grdOrdenTrabajo"
        Me.grdOrdenTrabajo.RowHeadersWidth = 26
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.Yellow
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black
        Me.grdOrdenTrabajo.RowsDefaultCellStyle = DataGridViewCellStyle3
        Me.grdOrdenTrabajo.Size = New System.Drawing.Size(668, 207)
        Me.grdOrdenTrabajo.TabIndex = 9
        '
        'otCLAVE
        '
        Me.otCLAVE.HeaderText = "otCLAVE"
        Me.otCLAVE.Name = "otCLAVE"
        Me.otCLAVE.Visible = False
        '
        'otidOrdenTrabajo
        '
        Me.otidOrdenTrabajo.HeaderText = "O. Trabajo"
        Me.otidOrdenTrabajo.Name = "otidOrdenTrabajo"
        '
        'otNotaRecepcion
        '
        Me.otNotaRecepcion.HeaderText = "Descripcion"
        Me.otNotaRecepcion.Name = "otNotaRecepcion"
        Me.otNotaRecepcion.Width = 250
        '
        'otNomDivision
        '
        Me.otNomDivision.HeaderText = "Division"
        Me.otNomDivision.Name = "otNomDivision"
        Me.otNomDivision.Width = 150
        '
        'otidOrdenSer
        '
        Me.otidOrdenSer.HeaderText = "otidOrdenSer"
        Me.otidOrdenSer.Name = "otidOrdenSer"
        Me.otidOrdenSer.Visible = False
        '
        'otidDivision
        '
        Me.otidDivision.HeaderText = "otidDivision"
        Me.otidDivision.Name = "otidDivision"
        Me.otidDivision.Visible = False
        '
        'otidServicio
        '
        Me.otidServicio.HeaderText = "otidServicio"
        Me.otidServicio.Name = "otidServicio"
        Me.otidServicio.Visible = False
        '
        'otidActividad
        '
        Me.otidActividad.HeaderText = "otidActividad"
        Me.otidActividad.Name = "otidActividad"
        Me.otidActividad.Visible = False
        '
        'otDuracionActHr
        '
        Me.otDuracionActHr.HeaderText = "otDuracionActHr"
        Me.otDuracionActHr.Name = "otDuracionActHr"
        Me.otDuracionActHr.Visible = False
        '
        'otIdUnidadTransp
        '
        Me.otIdUnidadTransp.HeaderText = "Unidad"
        Me.otIdUnidadTransp.Name = "otIdUnidadTransp"
        '
        'otidTipoServicio
        '
        Me.otidTipoServicio.HeaderText = "otidTipoServicio"
        Me.otidTipoServicio.Name = "otidTipoServicio"
        Me.otidTipoServicio.Visible = False
        '
        'otidOrdenActividad
        '
        Me.otidOrdenActividad.HeaderText = "otidOrdenActividad"
        Me.otidOrdenActividad.Name = "otidOrdenActividad"
        Me.otidOrdenActividad.Visible = False
        '
        'otidLlanta
        '
        Me.otidLlanta.HeaderText = "idLlanta"
        Me.otidLlanta.Name = "otidLlanta"
        Me.otidLlanta.Visible = False
        '
        'otPosLlanta
        '
        Me.otPosLlanta.HeaderText = "PosLlanta"
        Me.otPosLlanta.Name = "otPosLlanta"
        Me.otPosLlanta.Visible = False
        '
        'ConMnuOrdTra
        '
        Me.ConMnuOrdTra.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ModificarToolStripMenuOrdTra, Me.CancelarToolStripMenuOrdTra})
        Me.ConMnuOrdTra.Name = "ConMnuOrdTra"
        Me.ConMnuOrdTra.Size = New System.Drawing.Size(126, 48)
        '
        'ModificarToolStripMenuOrdTra
        '
        Me.ModificarToolStripMenuOrdTra.Name = "ModificarToolStripMenuOrdTra"
        Me.ModificarToolStripMenuOrdTra.Size = New System.Drawing.Size(125, 22)
        Me.ModificarToolStripMenuOrdTra.Text = "Modificar"
        '
        'CancelarToolStripMenuOrdTra
        '
        Me.CancelarToolStripMenuOrdTra.Name = "CancelarToolStripMenuOrdTra"
        Me.CancelarToolStripMenuOrdTra.Size = New System.Drawing.Size(125, 22)
        Me.CancelarToolStripMenuOrdTra.Text = "Eliminar"
        '
        'rdbOpcServ
        '
        Me.rdbOpcServ.AutoSize = True
        Me.rdbOpcServ.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.rdbOpcServ.Location = New System.Drawing.Point(15, 140)
        Me.rdbOpcServ.Name = "rdbOpcServ"
        Me.rdbOpcServ.Size = New System.Drawing.Size(71, 17)
        Me.rdbOpcServ.TabIndex = 116
        Me.rdbOpcServ.Text = "Servicios:"
        Me.rdbOpcServ.UseVisualStyleBackColor = True
        '
        'txtOrdenServicio
        '
        Me.txtOrdenServicio.Enabled = False
        Me.txtOrdenServicio.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOrdenServicio.ForeColor = System.Drawing.Color.Red
        Me.txtOrdenServicio.Location = New System.Drawing.Point(354, 134)
        Me.txtOrdenServicio.Name = "txtOrdenServicio"
        Me.txtOrdenServicio.ReadOnly = True
        Me.txtOrdenServicio.Size = New System.Drawing.Size(326, 26)
        Me.txtOrdenServicio.TabIndex = 117
        Me.txtOrdenServicio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'GpoPosiciones
        '
        'Me.GpoPosiciones.Controls.Add(Me.ShapeContainer2)
        'Me.GpoPosiciones.Location = New System.Drawing.Point(686, 104)
        'Me.GpoPosiciones.Name = "GpoPosiciones"
        'Me.GpoPosiciones.Size = New System.Drawing.Size(385, 349)
        'Me.GpoPosiciones.TabIndex = 118
        'Me.GpoPosiciones.TabStop = False
        'Me.GpoPosiciones.Text = "Posiciones"
        ''
        ''ShapeContainer2
        ''
        'Me.ShapeContainer2.Location = New System.Drawing.Point(3, 16)
        'Me.ShapeContainer2.Margin = New System.Windows.Forms.Padding(0)
        'Me.ShapeContainer2.Name = "ShapeContainer2"
        ''Me.ShapeContainer2.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.LineShape1})
        'Me.ShapeContainer2.Size = New System.Drawing.Size(379, 330)
        'Me.ShapeContainer2.TabIndex = 2
        'Me.ShapeContainer2.TabStop = False
        ''
        ''LineShape1
        ''
        'Me.LineShape1.BorderWidth = 10
        'Me.LineShape1.Name = "LineaPrinc"
        'Me.LineShape1.X1 = 186
        'Me.LineShape1.X2 = 186
        'Me.LineShape1.Y1 = 36
        'Me.LineShape1.Y2 = 280
        '
        'gpoPosicion
        '
        Me.gpoPosicion.Controls.Add(Me.txtPosicion)
        Me.gpoPosicion.Controls.Add(Me.Label13)
        Me.gpoPosicion.Controls.Add(Me.txtTipoUni)
        Me.gpoPosicion.Controls.Add(Me.Label12)
        Me.gpoPosicion.Controls.Add(Me.txtUniTrans)
        Me.gpoPosicion.Controls.Add(Me.Label33)
        Me.gpoPosicion.Location = New System.Drawing.Point(689, 48)
        Me.gpoPosicion.Name = "gpoPosicion"
        Me.gpoPosicion.Size = New System.Drawing.Size(438, 50)
        Me.gpoPosicion.TabIndex = 119
        Me.gpoPosicion.TabStop = False
        Me.gpoPosicion.Text = "Ubicaci�n"
        '
        'txtPosicion
        '
        Me.txtPosicion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPosicion.Location = New System.Drawing.Point(360, 20)
        Me.txtPosicion.MaxLength = 10
        Me.txtPosicion.Name = "txtPosicion"
        Me.txtPosicion.Size = New System.Drawing.Size(33, 20)
        Me.txtPosicion.TabIndex = 84
        Me.txtPosicion.Tag = "1"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label13.Location = New System.Drawing.Point(304, 22)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(50, 13)
        Me.Label13.TabIndex = 85
        Me.Label13.Text = "Posici�n:"
        '
        'txtTipoUni
        '
        Me.txtTipoUni.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTipoUni.Location = New System.Drawing.Point(194, 20)
        Me.txtTipoUni.MaxLength = 10
        Me.txtTipoUni.Name = "txtTipoUni"
        Me.txtTipoUni.ReadOnly = True
        Me.txtTipoUni.Size = New System.Drawing.Size(90, 20)
        Me.txtTipoUni.TabIndex = 82
        Me.txtTipoUni.Tag = "1"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label12.Location = New System.Drawing.Point(157, 20)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(31, 13)
        Me.Label12.TabIndex = 83
        Me.Label12.Text = "Tipo:"
        '
        'txtUniTrans
        '
        Me.txtUniTrans.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtUniTrans.Location = New System.Drawing.Point(63, 20)
        Me.txtUniTrans.MaxLength = 10
        Me.txtUniTrans.Name = "txtUniTrans"
        Me.txtUniTrans.Size = New System.Drawing.Size(90, 20)
        Me.txtUniTrans.TabIndex = 80
        Me.txtUniTrans.Tag = "1"
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label33.Location = New System.Drawing.Point(6, 23)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(44, 13)
        Me.Label33.TabIndex = 81
        Me.Label33.Text = "Unidad:"
        '
        'lblPosicion
        '
        Me.lblPosicion.AutoSize = True
        Me.lblPosicion.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lblPosicion.Location = New System.Drawing.Point(9, 112)
        Me.lblPosicion.Name = "lblPosicion"
        Me.lblPosicion.Size = New System.Drawing.Size(78, 13)
        Me.lblPosicion.TabIndex = 123
        Me.lblPosicion.Text = "Posici�n llanta:"
        '
        'cmbPosicion
        '
        Me.cmbPosicion.FormattingEnabled = True
        Me.cmbPosicion.Location = New System.Drawing.Point(158, 104)
        Me.cmbPosicion.Name = "cmbPosicion"
        Me.cmbPosicion.Size = New System.Drawing.Size(82, 21)
        Me.cmbPosicion.TabIndex = 3
        '
        'lblLlanta
        '
        Me.lblLlanta.AutoSize = True
        Me.lblLlanta.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lblLlanta.Location = New System.Drawing.Point(289, 107)
        Me.lblLlanta.Name = "lblLlanta"
        Me.lblLlanta.Size = New System.Drawing.Size(59, 13)
        Me.lblLlanta.TabIndex = 125
        Me.lblLlanta.Text = "No. Llanta:"
        '
        'cmbidLlanta
        '
        Me.cmbidLlanta.FormattingEnabled = True
        Me.cmbidLlanta.Location = New System.Drawing.Point(354, 104)
        Me.cmbidLlanta.Name = "cmbidLlanta"
        Me.cmbidLlanta.Size = New System.Drawing.Size(151, 21)
        Me.cmbidLlanta.TabIndex = 4
        '
        'fOrdenTrab
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(1130, 495)
        Me.Controls.Add(Me.lblLlanta)
        Me.Controls.Add(Me.cmbidLlanta)
        Me.Controls.Add(Me.lblPosicion)
        Me.Controls.Add(Me.cmbPosicion)
        Me.Controls.Add(Me.gpoPosicion)
        Me.Controls.Add(Me.GpoPosiciones)
        Me.Controls.Add(Me.txtOrdenServicio)
        Me.Controls.Add(Me.rdbOpcServ)
        Me.Controls.Add(Me.grdOrdenTrabajo)
        Me.Controls.Add(Me.cmbUnidades)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.rdbOpcAct)
        Me.Controls.Add(Me.cmbActividad)
        Me.Controls.Add(Me.rdbOpcDesc)
        Me.Controls.Add(Me.cmbServicios)
        Me.Controls.Add(Me.cmbDivision)
        Me.Controls.Add(Me.lblDivision)
        Me.Controls.Add(Me.txtDescripcion)
        Me.Controls.Add(Me.cmbTipoServ)
        Me.Controls.Add(Me.Label37)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.txtNomTipOrdServ)
        Me.Controls.Add(Me.ToolStripMenu)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "fOrdenTrab"
        Me.Text = "Actividades y Servicios"
        Me.ToolStripMenu.ResumeLayout(False)
        Me.ToolStripMenu.PerformLayout()
        CType(Me.grdOrdenTrabajo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ConMnuOrdTra.ResumeLayout(False)
        Me.GpoPosiciones.ResumeLayout(False)
        Me.gpoPosicion.ResumeLayout(False)
        Me.gpoPosicion.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region
    Sub New(ByVal vTag As String)
        InitializeComponent()
        _Tag = vTag
    End Sub

    Public Sub New(con As String, nomUsuario As String, cveEmpresa As Integer,
    ByVal idTipOrdServ As Integer, ByVal NomTipOrdServ As String, ByVal NumOrdServ As Integer,
    ByVal Unidades As List(Of UnidadesTranspClass), ByVal vbModifica As Boolean,
    ByVal TipoServ As Integer, ByVal vOpcion As TipoOrdenServicio, ByVal vUnidadRecibe As String)
        _con = con
        BD = New CapaDatos.UtilSQL(con, "Juan")
        InitializeComponent()
        _Usuario = nomUsuario
        _cveEmpresa = cveEmpresa
        _idTipOrdServ = idTipOrdServ
        _NomTipOrdServ = NomTipOrdServ
        _NumOrdServ = NumOrdServ
        _Unidades = Unidades
        _bModifica = vbModifica
        _TipoServ = TipoServ
        _OpcionTipoOrdServ = vOpcion
        _UnidadRecibe = vUnidadRecibe
    End Sub

    Private Sub SeleccionaItemServ(ByVal vServ As Integer)
        Dim i As Integer = 0
        For Each obj In cmbServicios.Items
            Dim TipoServ As TipoServClass = New TipoServClass(0)
            TipoServ = CType(obj, TipoServClass)
            If TipoServ.idTipoServicio = vServ Then
                cmbServicios.SelectedIndex = i
                cmbServicios.Enabled = False
                Exit For
            Else
                i = i + 1
                cmbServicios.Enabled = True
            End If


        Next
    End Sub
    Private Sub SeleccionaItemTipoServ(ByVal vTipoServ As Integer)
        'Dim ListTipoServ = cmbTipoServ.Items
        'For Each lobjTS In ListTipoServ
        '    lobjTS()
        'Next

        'Dim list = chkBoxTipoOrdServ.CheckedItems

        'Dim listObj As List(Of TiposOrdenServicio) = New List(Of TiposOrdenServicio)()
        'For Each obj In list
        '    listObj.Add(CType(obj, TiposOrdenServicio))
        'Next

        Dim i As Integer = 0
        For Each obj In cmbTipoServ.Items
            Dim TipoServ As TipoServClass = New TipoServClass(0)
            TipoServ = CType(obj, TipoServClass)
            If TipoServ.idTipoServicio = vTipoServ Then
                cmbTipoServ.SelectedIndex = i
                cmbTipoServ.Enabled = False
                Exit For
            Else
                i = i + 1
                cmbTipoServ.Enabled = True
            End If

        Next

    End Sub


    Private Function IdTipoServicio() As Integer
        Dim vValorDev As Integer = 0
        Dim ListTipoServ = cmbTipoServ.SelectedItem
        Dim listObjTS As List(Of TipoServClass) = New List(Of TipoServClass)()
        listObjTS.Add(ListTipoServ)
        For Each lobjTS In listObjTS
            vValorDev = lobjTS.idTipoServicio
        Next
        Return vValorDev
    End Function

    Private Sub CreaTablaRespuesta()
        Try
            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Int64")
            myDataColumn.ColumnName = "totCLAVE"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRes.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Int64")
            myDataColumn.ColumnName = "totidOrdenTrabajo"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRes.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.String")
            myDataColumn.ColumnName = "totNotaRecepcion"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRes.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.String")
            myDataColumn.ColumnName = "totNomDivision"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRes.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Int64")
            myDataColumn.ColumnName = "totidOrdenSer"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRes.Columns.Add(myDataColumn)


            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Int64")
            myDataColumn.ColumnName = "totidDivision"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRes.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Int64")
            myDataColumn.ColumnName = "totidServicio"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRes.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.String")
            myDataColumn.ColumnName = "totidActividad"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRes.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Int64")
            myDataColumn.ColumnName = "totDuracionActHr"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRes.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.String")
            myDataColumn.ColumnName = "totIdUnidadTransp"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRes.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Int64")
            myDataColumn.ColumnName = "totTipoServ"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRes.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Int64")
            myDataColumn.ColumnName = "totidOrdenActividad"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRes.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.String")
            myDataColumn.ColumnName = "totidLlanta"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRes.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Int64")
            myDataColumn.ColumnName = "totPosLlanta"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRes.Columns.Add(myDataColumn)
        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try

    End Sub

    Private Sub fOrdenTrab_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            Salida = True
            If Inicio.CONSTR = "" Then
                Inicio.CONSTR = _con
            End If

            CreaTablaRespuesta()
            cmbTipoServ.Enabled = False
            cmbServicios.Enabled = False
            cmbActividad.Enabled = False

            If _NomTipOrdServ = "LLANTAS" Then
                Me.Size = New Size(1146, 504)
                grdOrdenTrabajo.Columns("otidLlanta").Visible = True
                grdOrdenTrabajo.Columns("otPosLlanta").Visible = True
                cmbDivision.Visible = False
                lblDivision.Visible = False
                cmbServicios.Visible = False
                rdbOpcServ.Checked = False
                rdbOpcServ.Visible = False


                cmbPosicion.Visible = True
                cmbidLlanta.Visible = True
                lblPosicion.Visible = True
                lblLlanta.Visible = True

            Else
                'Me.Size = New Size(708, 482)
                Me.Size = New Size(700, 510)
                grdOrdenTrabajo.Columns("otidLlanta").Visible = False
                grdOrdenTrabajo.Columns("otPosLlanta").Visible = False
                cmbPosicion.Visible = False
                cmbidLlanta.Visible = False
                lblPosicion.Visible = False
                lblLlanta.Visible = False


            End If



            If _NomTipOrdServ <> "" Then
                'bandPrimera = True
                txtNomTipOrdServ.Text = _NomTipOrdServ

                'Checar si tiene Relaciones RELTIPORDSERV
                myTablaRelaciones = ObjTipOrdServ.TablaRelTipOrdServ("rel.idTipOrdServ = " & _idTipOrdServ)
                If myTablaRelaciones.Rows.Count > 0 Then
                    For i = 0 To myTablaRelaciones.Rows.Count - 1
                        vNomTabla = myTablaRelaciones.DefaultView.Item(i).Item("NomCatalogo").ToString
                        vNomCampo = myTablaRelaciones.DefaultView.Item(i).Item("NombreId").ToString
                        vIdRelacion = myTablaRelaciones.DefaultView.Item(i).Item("IdRelacion")
                        If UCase(vNomTabla) = UCase("CatTipoServicio") Then
                            TipoServicio = New TipoServClass(vIdRelacion)
                            If TipoServicio.Existe Then
                                cmbTipoServ.Items.Add(TipoServicio)
                                cmbTipoServ.ValueMember = "idTipoServicio"
                                cmbTipoServ.DisplayMember = "NomTipoServicio"
                                cmbTipoServ.Enabled = True
                                BExisteTipoServ = True
                            End If

                            'TipoServicio = New TipoServClass(0)
                            'Dim TablaTS As New DataTable
                            'TablaTS = TipoServicio.TablaTipoServicio_RelTipOrdServ(_idTipOrdServ)
                            'If TablaTS.Rows.Count > 0 Then
                            '    cmbTipoServ.DataSource = TablaTS
                            '    cmbTipoServ.ValueMember = "idTipoServicio"
                            '    cmbTipoServ.DisplayMember = "NomTipoServicio"
                            '    cmbTipoServ.Enabled = True
                            '    BExisteTipoServ = True
                            'End If
                            ''NO APLICA - CHECANDO
                        ElseIf UCase(vNomTabla) = UCase("CatServicios") Then
                            Servicio = New ServicioClass(vIdRelacion)
                            If Servicio.Existe Then
                                cmbServicios.Items.Add(Servicio)
                                cmbServicios.ValueMember = "idServicio"
                                cmbServicios.DisplayMember = "NomServicio"
                                cmbServicios.Enabled = True
                            End If
                            'Servicio = New ServicioClass(0)
                            'Dim TablaSer As New DataTable
                            'TablaSer = Servicio.TablaServicio_RelTipOrdServ(_idTipOrdServ)
                            'If TablaSer.Rows.Count > 0 Then
                            '    cmbServicios.DataSource = TablaSer
                            '    cmbServicios.ValueMember = "idServicio"
                            '    cmbServicios.DisplayMember = "NomServicio"

                            '    cmbServicios.Enabled = True
                            'End If


                        ElseIf UCase(vNomTabla) = UCase("CatActividades") Then
                            Actividades = New ActividadesClass(vIdRelacion)
                            If Actividades.Existe Then
                                cmbActividad.Items.Add(Actividades)
                                cmbActividad.ValueMember = "idActividad"
                                cmbActividad.DisplayMember = "NombreAct"
                                cmbActividad.Enabled = True
                            End If
                            'MyTablaComboActiv = Actividades.TablaActividades("")
                            'If Not MyTablaComboActiv Is Nothing Then
                            '    If MyTablaComboActiv.Rows.Count > 0 Then
                            '        cmbActividad.DataSource = MyTablaComboActiv
                            '        cmbActividad.ValueMember = "idActividad"
                            '        cmbActividad.DisplayMember = "NombreAct"
                            '    End If
                            'End If
                        End If
                    Next
                End If

                If BExisteTipoServ Then
                    LlenaCombo()
                    If _NomTipOrdServ = "LLANTAS" Then
                        ParaLlantas(cmbUnidades.SelectedValue)
                        LlenaComboLlantas(cmbUnidades.SelectedValue)
                    End If



                    If _bModifica Then
                        txtOrdenServicio.Text = "ORDEN DE SERVICIO : " & _NumOrdServ
                        SeleccionaItemTipoServ(_TipoServ)
                        'cmbTipoServ.SelectedValue = _TipoServ X
                        'Cargar la Orden de Servicio
                        OrdenServicio = New OrdenServClass(_NumOrdServ)

                        If OrdenServicio.Existe Then
                            Dim tablaDetOS As DataTable = OrdenServicio.tbDetOrdSerxID(_NumOrdServ)
                            If tablaDetOS.Rows.Count > 0 Then
                                For i = 0 To tablaDetOS.Rows.Count - 1
                                    InsertaRegGrid(tablaDetOS.DefaultView.Item(i).Item("CLAVE"),
                                    tablaDetOS.DefaultView.Item(i).Item("idOrdenTrabajo"),
                                    tablaDetOS.DefaultView.Item(i).Item("NotaRecepcion").ToString(), tablaDetOS.DefaultView.Item(i).Item("NomDivision").ToString(),
                                    tablaDetOS.DefaultView.Item(i).Item("idOrdenSer"), tablaDetOS.DefaultView.Item(i).Item("idDivision"),
                                    tablaDetOS.DefaultView.Item(i).Item("idServicio"), tablaDetOS.DefaultView.Item(i).Item("idActividad"),
                                    tablaDetOS.DefaultView.Item(i).Item("DuracionActHr"), tablaDetOS.DefaultView.Item(i).Item("IdUnidadTransp").ToString(),
                                    tablaDetOS.DefaultView.Item(i).Item("idTipoServicio"), tablaDetOS.DefaultView.Item(i).Item("idOrdenActividad"))

                                Next
                                'InsertaRegGrid(_NumOrdServ, 0, txtDescripcion.Text, cmbDivision.Text, 0, cmbDivision.SelectedValue, 0, 0, 0, cmbUnidades.SelectedValue, IdTipoServicio())


                            End If
                        End If



                    Else
                        txtOrdenServicio.Text = ""
                        CombosACero()
                        cmbUnidades.SelectedIndex = 0
                        cmbUnidades.Focus()

                    End If

                Else
                    MsgBox("No Existen Tipos de Servicio para Este Modulo de: " & _NomTipOrdServ, MsgBoxStyle.Information, Me.Text)
                    Me.Close()
                End If

                If _OpcionTipoOrdServ = TipoOrdenServicio.LAVADERO Then
                    rdbOpcServ.Enabled = False
                    cmbServicios.Enabled = False
                    rdbOpcDesc.Enabled = False
                    txtDescripcion.Enabled = False
                End If

            End If

            If _UnidadRecibe <> "" Then
                SeleccionaItemTipoServ(_idTipOrdServ)
                cmbTipoServ.Enabled = False
                cmbUnidades.SelectedValue = _UnidadRecibe
                cmbUnidades.Enabled = False
                cmbServicios.Focus()
            Else
                cmbUnidades.Enabled = True
                cmbUnidades.Focus()
            End If

            Salida = False
        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, Me.Text)
        End Try
    End Sub

    Private Sub btnMnuOk_Click(sender As Object, e As EventArgs) Handles btnMnuOk.Click
        'Agregar a Tabla el contenido del grid
        GrabaGridATabla()

        Me.DialogResult = Windows.Forms.DialogResult.OK
        'tablaRes = grdOrdenTrabajo.DataSource

    End Sub

    Private Sub GrabaGridATabla()
        If grdOrdenTrabajo.Rows.Count > 0 Then
            For i As Integer = 0 To grdOrdenTrabajo.Rows.Count - 1
                myDataRow = MyTablaRes.NewRow()
                myDataRow("totCLAVE") = grdOrdenTrabajo.Item("otCLAVE", i).Value
                myDataRow("totidOrdenTrabajo") = grdOrdenTrabajo.Item("otidOrdenTrabajo", i).Value
                myDataRow("totNotaRecepcion") = grdOrdenTrabajo.Item("otNotaRecepcion", i).Value
                myDataRow("totNomDivision") = grdOrdenTrabajo.Item("otNomDivision", i).Value
                myDataRow("totidOrdenSer") = grdOrdenTrabajo.Item("otidOrdenSer", i).Value
                myDataRow("totidDivision") = grdOrdenTrabajo.Item("otidDivision", i).Value
                myDataRow("totidServicio") = grdOrdenTrabajo.Item("otidServicio", i).Value
                myDataRow("totidActividad") = grdOrdenTrabajo.Item("otidActividad", i).Value
                myDataRow("totDuracionActHr") = grdOrdenTrabajo.Item("otDuracionActHr", i).Value
                myDataRow("totIdUnidadTransp") = grdOrdenTrabajo.Item("otIdUnidadTransp", i).Value
                myDataRow("totTipoServ") = grdOrdenTrabajo.Item("otidTipoServicio", i).Value
                myDataRow("totidOrdenActividad") = grdOrdenTrabajo.Item("otidOrdenActividad", i).Value
                If _NomTipOrdServ = "LLANTAS" Then
                    myDataRow("totidLlanta") = grdOrdenTrabajo.Item("otidLlanta", i).Value
                    myDataRow("totPosLlanta") = grdOrdenTrabajo.Item("otPosLlanta", i).Value
                Else
                    myDataRow("totidLlanta") = ""
                    myDataRow("totPosLlanta") = 0

                End If

                MyTablaRes.Rows.Add(myDataRow)

            Next
        End If

    End Sub



    Private Sub cmbTipoServ_KeyUp(sender As Object, e As KeyEventArgs) Handles cmbTipoServ.KeyUp
        If e.KeyValue = Keys.Enter Then
            If myTablaRelaciones.Rows.Count > 0 Then
                cmbServicios.Items.Clear()
                For i = 0 To myTablaRelaciones.Rows.Count - 1
                    vNomTabla = myTablaRelaciones.DefaultView.Item(i).Item("NomCatalogo").ToString
                    vNomCampo = myTablaRelaciones.DefaultView.Item(i).Item("NombreId").ToString
                    vIdRelacion = myTablaRelaciones.DefaultView.Item(i).Item("IdRelacion")
                    If UCase(vNomTabla) = UCase("CatServicios") Then
                        Servicio = New ServicioClass(vIdRelacion)
                        If Servicio.Existe And Servicio.idTipoServicio = IdTipoServicio() Then
                            cmbServicios.Items.Add(Servicio)
                            cmbServicios.ValueMember = "idServicio"
                            cmbServicios.DisplayMember = "NomServicio"

                            rdbOpcServ.Enabled = True
                            'cmbServicios.Enabled = True
                        Else
                            rdbOpcServ.Enabled = False
                            'cmbServicios.Enabled = False
                        End If
                    End If
                Next
            End If
            'Servicio = New ServicioClass(0)
            'Dim list = cmbTipoServ.SelectedItem
            'Dim listObj As List(Of TipoServClass) = New List(Of TipoServClass)()
            'listObj.Add(CType(list, TipoServClass))
            'For Each lobj In listObj
            '    myTablaServicios = Servicio.TablaServicios(" ser.idTipoServicio = " & lobj.idTipoServicio)
            '    If myTablaServicios.Rows.Count > 0 Then
            '        cmbServicios.DataSource = myTablaServicios
            '        cmbServicios.ValueMember = "idServicio"
            '        cmbServicios.DisplayMember = "NomServicio"
            '        cmbServicios.Enabled = True
            '    End If
            'Next

            If rdbOpcServ.Enabled Then
                rdbOpcServ.Checked = True
                ValorSegunOpcion()
                If cmbServicios.Items.Count > 0 Then
                    cmbServicios.SelectedIndex = 0
                    cmbServicios.Focus()
                Else
                    rdbOpcServ.Enabled = False
                    rdbOpcServ.Checked = False

                    rdbOpcDesc.Enabled = True
                    rdbOpcDesc.Checked = True
                    txtDescripcion.Focus()
                End If

            ElseIf rdbOpcAct.Enabled Then
                rdbOpcAct.Checked = True
                cmbActividad.Focus()
            ElseIf rdbOpcDesc.Enabled Then
                rdbOpcDesc.Checked = True
                ValorSegunOpcion()
                txtDescripcion.Focus()

            End If


        End If
    End Sub



    Private Sub CombosACero()
        If cmbUnidades.Items.Count > 0 Then
            cmbUnidades.SelectedIndex = 0
        End If
        If cmbDivision.Items.Count > 0 Then
            cmbDivision.SelectedIndex = 0
        End If
        'cmbServicios.SelectedIndex = 0
        If cmbActividad.Items.Count > 0 Then
            cmbActividad.SelectedIndex = 0
        End If

        If cmbTipoServ.Items.Count > 0 Then
            cmbTipoServ.SelectedIndex = 0
        End If

        txtDescripcion.Text = ""
    End Sub
    Private Sub LlenaCombo()
        cmbUnidades.DataSource = _Unidades
        cmbUnidades.ValueMember = "iUnidadTrans"
        cmbUnidades.DisplayMember = "iUnidadTrans"

        Division = New DivisionClass(0)
        MyTablaComboDiv = Division.TablaDivisiones(" div.idDivision > 0 ")
        If Not MyTablaComboDiv Is Nothing Then
            If MyTablaComboDiv.Rows.Count > 0 Then
                cmbDivision.DataSource = MyTablaComboDiv
                cmbDivision.ValueMember = "idDivision"
                cmbDivision.DisplayMember = "NomDivision"
            End If
        End If
    End Sub

    Private Sub cmbServicios_KeyUp(sender As Object, e As KeyEventArgs) Handles cmbServicios.KeyUp
        If e.KeyValue = Keys.Enter Then
            'Checar las actividades Relacionadas a los servicios, si existen meterlos al grid, si no tiene se inserta solo el servicio
            'TablaRelServAct

            Dim list = cmbServicios.SelectedItem
            Dim listObj As List(Of ServicioClass) = New List(Of ServicioClass)()
            listObj.Add(list)

            For Each lobj In listObj
                Servicio = New ServicioClass(0)
                MyTablaRelServAct = Servicio.TablaRelServAct("rel.idServicio = " & lobj.idServicio)
                If Not MyTablaRelServAct Is Nothing Then
                    If MyTablaRelServAct.Rows.Count > 0 Then
                        'Insertar Actividades al Grid
                        For i = 0 To MyTablaRelServAct.Rows.Count - 1
                            'MyTablaHisOS.DefaultView.Item(i).Item("idOrdenSer")
                            InsertaRegGrid(_NumOrdServ, 0, MyTablaRelServAct.DefaultView.Item(i).Item("NombreAct"),
                            MyTablaRelServAct.DefaultView.Item(i).Item("NomDivision"), 0,
                            Val(MyTablaRelServAct.DefaultView.Item(i).Item("idDivision")),
                            lobj.idServicio, Val(MyTablaRelServAct.DefaultView.Item(i).Item("idActividad")),
                            Val(MyTablaRelServAct.DefaultView.Item(i).Item("DuracionHoras")), cmbUnidades.SelectedValue, IdTipoServicio(), 0)
                        Next
                    Else
                        'Insertar solo servicio al Grid
                        InsertaRegGrid(_NumOrdServ, 0, "", "", 0, 0, lobj.idServicio, 0, 0, cmbUnidades.SelectedValue, IdTipoServicio, 0)
                    End If
                End If

            Next
            cmbUnidades.Focus()

        End If
    End Sub

    Private Function InsertaRegGrid(ByVal ClaveFicticia As Integer, ByVal idOrdenTrabajo As Integer,
    ByVal NotaRecepcion As String, ByVal NomDivision As String, ByVal idOrdenSer As Integer, ByVal idDivision As Integer,
    ByVal idServicio As Integer, ByVal idActividad As Integer, ByVal DuracionActHr As Integer,
    ByVal UnidadTrans As String, ByVal idTipoServicio As Integer, ByVal idOrdenActividad As Integer) As Boolean
        Try
            Dim BExisteElemento As Boolean = False

            For i = 0 To grdOrdenTrabajo.Rows.Count - 1
                If UnidadTrans = grdOrdenTrabajo.Item("otIdUnidadTransp", i).Value Then
                    If idActividad = grdOrdenTrabajo.Item("otidActividad", i).Value And idActividad > 0 Then
                        If _NomTipOrdServ = "LLANTAS" Then
                            If cmbPosicion.SelectedValue = grdOrdenTrabajo.Item("otPosLlanta", i).Value Then

                            End If
                        Else
                            BExisteElemento = True
                            Exit For

                        End If
                    End If
                    If (idServicio = grdOrdenTrabajo.Item("otidServicio", i).Value) And (idActividad = grdOrdenTrabajo.Item("otidActividad", i).Value) Then
                        If idServicio > 0 Then
                            BExisteElemento = True
                            Exit For
                        End If
                    End If
                End If
            Next
            'otCLAVE
            'otidOrdenTrabajo
            'otNotaRecepcion
            'otNomDivision
            'otidOrdenSer
            'otidDivision
            'otidServicio
            'otidActividad
            'otDuracionActHr
            'otIdUnidadTransp
            'otidTipoServicio
            'otidOrdenActividad

            If Not BExisteElemento Then
                If _NomTipOrdServ = "LLANTAS" Then
                    idLlanta = cmbidLlanta.SelectedValue
                    PosLLanta = cmbPosicion.SelectedValue

                Else
                    idLlanta = ""
                    PosLLanta = 0
                End If

                grdOrdenTrabajo.Rows.Add(ClaveFicticia, idOrdenTrabajo, NotaRecepcion, NomDivision, idOrdenSer,
                                         idDivision, idServicio, idActividad, DuracionActHr, UnidadTrans,
                                         idTipoServicio, idOrdenActividad, idLlanta, PosLLanta)

                grdOrdenTrabajo.PerformLayout()
                CombosACero()

                If bandPrimera Then
                    bandPrimera = False
                    cmbTipoServ.Enabled = False
                End If
                cmbUnidades.Focus()

                Return True
            Else
                Return False
            End If



        Catch ex As Exception
            Return False
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, Me.Text)
        End Try

    End Function

    Private Sub cmbServicios_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbServicios.SelectedIndexChanged

    End Sub

    Private Sub ValorSegunOpcion()
        txtDescripcion.Enabled = rdbOpcDesc.Checked
        cmbActividad.Enabled = rdbOpcAct.Checked
        cmbServicios.Enabled = rdbOpcServ.Checked
    End Sub

    Private Sub rdbOpcDesc_CheckedChanged(sender As Object, e As EventArgs) Handles rdbOpcDesc.CheckedChanged

        ValorSegunOpcion()

        txtDescripcion.Focus()
        txtDescripcion.SelectionStart = Len(txtDescripcion.Text)
    End Sub

    Private Sub rdbOpcDesc_EnabledChanged(sender As Object, e As EventArgs) Handles rdbOpcDesc.EnabledChanged

    End Sub

    Private Sub rdbOpcAct_CheckedChanged(sender As Object, e As EventArgs) Handles rdbOpcAct.CheckedChanged
        ValorSegunOpcion()

        cmbActividad.Focus()
    End Sub




    Private Sub cmbDivision_KeyUp(sender As Object, e As KeyEventArgs) Handles cmbDivision.KeyUp
        If e.KeyValue = Keys.Enter Then
            If rdbOpcDesc.Checked Then
                Dim vUnidad As String
                If txtDescripcion.Text <> "" Then
                    vUnidad = cmbUnidades.SelectedValue
                    If _bModifica Then
                        Me.grdOrdenTrabajo.Rows.Remove(Me.grdOrdenTrabajo.CurrentRow)
                        InsertaRegGrid(_NumOrdServ, OrdenTrabajo, txtDescripcion.Text, cmbDivision.Text, 0,
                        cmbDivision.SelectedValue, 0, 0, 0, cmbUnidades.SelectedValue, IIf(rdbOpcDesc.Checked, 0, IdTipoServicio()), idOrdenActividad)
                    Else
                        'InsertaRegGrid(_NumOrdServ, 0, txtDescripcion.Text, cmbDivision.Text, 0, cmbDivision.SelectedValue, 0, 0, 0, cmbUnidades.SelectedValue, IIf(rdbOpcDesc.Checked, 0, IdTipoServicio()), 0)
                        InsertaRegGrid(_NumOrdServ, 0, txtDescripcion.Text, cmbDivision.Text, 0, cmbDivision.SelectedValue, 0, 0, 0, cmbUnidades.SelectedValue, IdTipoServicio(), 0)

                    End If

                    cmbUnidades.SelectedValue = vUnidad
                    cmbUnidades.Focus()
                End If
            End If
        End If
    End Sub

    Private Sub cmbDivision_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbDivision.SelectedIndexChanged

    End Sub

    Private Sub cmbActividad_KeyUp(sender As Object, e As KeyEventArgs) Handles cmbActividad.KeyUp
        If e.KeyValue = Keys.Enter Then
            Dim vUnidad As String
            If rdbOpcAct.Checked Then
                vUnidad = cmbUnidades.SelectedValue
                Dim list = cmbActividad.SelectedItem
                Dim listObj As List(Of ActividadesClass) = New List(Of ActividadesClass)()
                listObj.Add(list)
                For Each lobj In listObj
                    'Actividades = New ActividadesClass(lobj.idActividad)
                    'InsertaRegGrid(_NumOrdServ, 0, Actividades.NombreAct, Actividades.NomDivision, 0, Actividades.idDivision, 0, cmbActividad.SelectedValue, 0, cmbUnidades.SelectedValue, IdTipoServicio())

                    InsertaRegGrid(_NumOrdServ, 0, lobj.NombreAct, lobj.NomDivision, 0, lobj.idDivision, 0, lobj.idActividad, 0, cmbUnidades.SelectedValue, IdTipoServicio(), 0)
                Next
                cmbUnidades.SelectedValue = vUnidad
                cmbUnidades.Focus()
            End If
        End If


    End Sub

    Private Sub cmbUnidades_KeyUp(sender As Object, e As KeyEventArgs) Handles cmbUnidades.KeyUp
        If Salida Then Exit Sub
        If e.KeyValue = Keys.Enter Then
            If vUnidadAct = "" Then
                vUnidadAct = cmbUnidades.SelectedValue
                bandPrimera = True
            ElseIf vUnidadAct = cmbUnidades.SelectedValue Then

            Else
                bandPrimera = True
                cmbTipoServ.Enabled = True
                vUnidadAct = cmbUnidades.SelectedValue
            End If

            If cmbPosicion.Visible And cmbPosicion.Enabled Then
                'cmbPosicion.SelectedIndex = 0
                cmbPosicion.Focus()
            ElseIf cmbTipoServ.Enabled Then
                cmbTipoServ.SelectedIndex = 0
                cmbTipoServ.Focus()
            ElseIf cmbServicios.Enabled Then
                cmbServicios.SelectedIndex = 0
                cmbServicios.Focus()
            ElseIf cmbActividad.Enabled Then
                cmbActividad.Focus()
            ElseIf txtDescripcion.Enabled Then
                txtDescripcion.Focus()
            End If
        End If
    End Sub

    Private Sub cmbUnidades_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbUnidades.SelectedIndexChanged
        'ParaLlantas(cmbUnidades.SelectedValue)
    End Sub

    Private Sub rdbOpcServ_CheckedChanged(sender As Object, e As EventArgs) Handles rdbOpcServ.CheckedChanged
        ValorSegunOpcion()
        cmbServicios.Focus()
    End Sub


    Private Sub cmbActividad_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbActividad.SelectedIndexChanged

    End Sub

    Private Sub btnMnuCancelar_Click(sender As Object, e As EventArgs) Handles btnMnuCancelar.Click
        'Borrar Todo
        If grdOrdenTrabajo.Rows.Count > 0 Then
            'Seguro que desea borrar
            If MsgBox("!! Esta seguro que desea Reiniciar todas las Actividades y Servicios? !!", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                txtDescripcion.Text = ""
                grdOrdenTrabajo.Rows.Clear()
                cmbTipoServ.Enabled = True
                bandPrimera = True
                cmbUnidades.Focus()
            End If
        End If
    End Sub

    Private Sub btnMnuSalir_Click(sender As Object, e As EventArgs) Handles btnMnuSalir.Click
        Me.Close()
    End Sub

    Private Sub grdOrdenTrabajo_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles grdOrdenTrabajo.CellContentClick

    End Sub

    'Private Sub grdOrdenTrabajo_Click(sender As Object, e As EventArgs) Handles grdOrdenTrabajo.Click
    '    'Clave = grdOrdenTrabajo.Item("oscCLAVE", grdOrdenTrabajo.CurrentCell.RowIndex).Value
    '    LlenaForma()
    'End Sub

    Private Sub LlenaServicios()
        If myTablaRelaciones.Rows.Count > 0 Then
            cmbServicios.Items.Clear()
            For i = 0 To myTablaRelaciones.Rows.Count - 1
                vNomTabla = myTablaRelaciones.DefaultView.Item(i).Item("NomCatalogo").ToString
                vNomCampo = myTablaRelaciones.DefaultView.Item(i).Item("NombreId").ToString
                vIdRelacion = myTablaRelaciones.DefaultView.Item(i).Item("IdRelacion")
                If UCase(vNomTabla) = UCase("CatServicios") Then
                    Servicio = New ServicioClass(vIdRelacion)
                    If Servicio.Existe And Servicio.idTipoServicio = IdTipoServicio() Then
                        cmbServicios.Items.Add(Servicio)
                        cmbServicios.ValueMember = "idServicio"
                        cmbServicios.DisplayMember = "NomServicio"

                        rdbOpcServ.Enabled = True
                        'cmbServicios.Enabled = True
                    Else
                        rdbOpcServ.Enabled = False
                        'cmbServicios.Enabled = False
                    End If
                End If
            Next
        End If
    End Sub

    Private Sub LlenaForma()
        'otCLAVE
        'otidOrdenTrabajo
        'otNotaRecepcion
        'otNomDivision
        'otidOrdenSer
        'otidDivision
        'otidServicio
        'otidActividad
        'otDuracionActHr
        'otIdUnidadTransp
        'otidTipoServicio

        Clave = grdOrdenTrabajo.Item("otCLAVE", grdOrdenTrabajo.CurrentCell.RowIndex).Value
        SeleccionaItemTipoServ(grdOrdenTrabajo.Item("otidTipoServicio", grdOrdenTrabajo.CurrentCell.RowIndex).Value)
        'LlenaServicios()
        If grdOrdenTrabajo.Item("otidServicio", grdOrdenTrabajo.CurrentCell.RowIndex).Value > 0 Then
            'rdbOpcServ.Checked = True
            'SeleccionaItemServ(grdOrdenTrabajo.Item("otidServicio", grdOrdenTrabajo.CurrentCell.RowIndex).Value)

        ElseIf grdOrdenTrabajo.Item("otidActividad", grdOrdenTrabajo.CurrentCell.RowIndex).Value > 0 Then

        Else
            rdbOpcAct.Enabled = False
            rdbOpcServ.Enabled = False
            rdbOpcDesc.Enabled = True
            rdbOpcDesc.Checked = True
            ValorSegunOpcion()
            OrdenTrabajo = grdOrdenTrabajo.Item("otidOrdenTrabajo", grdOrdenTrabajo.CurrentCell.RowIndex).Value
            idOrdenActividad = grdOrdenTrabajo.Item("otidOrdenActividad", grdOrdenTrabajo.CurrentCell.RowIndex).Value
            txtDescripcion.Text = grdOrdenTrabajo.Item("otNotaRecepcion", grdOrdenTrabajo.CurrentCell.RowIndex).Value
            cmbDivision.SelectedValue = grdOrdenTrabajo.Item("otidDivision", grdOrdenTrabajo.CurrentCell.RowIndex).Value

        End If





    End Sub

    Private Sub ConMnuOrdTra_Opening(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles ConMnuOrdTra.Opening
        With grdOrdenTrabajo.Rows(grdOrdenTrabajo.CurrentRow.Index)
            If .Cells("otidServicio").Value Then
                ModificarToolStripMenuOrdTra.Enabled = False
                CancelarToolStripMenuOrdTra.Enabled = True

            ElseIf .Cells("otidActividad").Value Then
                ModificarToolStripMenuOrdTra.Enabled = False
                CancelarToolStripMenuOrdTra.Enabled = True
            Else
                ModificarToolStripMenuOrdTra.Enabled = True
                CancelarToolStripMenuOrdTra.Enabled = True

            End If
        End With
    End Sub

    Private Sub ModificarToolStripMenuOrdTra_Click(sender As Object, e As EventArgs) Handles ModificarToolStripMenuOrdTra.Click
        LlenaForma()
    End Sub

    Private Sub ParaLlantas(ByVal idUnidad As String)
        txtUniTrans.Text = idUnidad
        UniTrans = New UnidadesTranspClass(idUnidad)
        If UniTrans.Existe Then
            TipUni = New TipoUniTransClass(UniTrans.idTipoUnidad)
            If TipUni.Existe Then
                txtTipoUni.Text = TipUni.Clasificacion
                ClasLlan = New ClasLlanClass(TipUni.idClasLlan)
                If ClasLlan.Existe Then
                    DibujaEsquema(TipUni.idClasLlan, TipUni.NoLlantas)
                    If txtPosicion.Text <> "" Then
                        SeleccionaPosicion(Val(txtPosicion.Text))
                    End If
                End If
            End If
        End If
    End Sub
    Private Sub DibujaEsquema(ByVal vidClasLlan As Integer, ByVal TotalLlantas As Integer)
        Dim vNoEje As Integer
        Dim vNoLlantas As Integer
        Dim LlantxLado As Integer
        Dim NoPosicion As Integer = 0

        'Dim FactorX As Integer = 71
        Dim FactorX As Integer = 201

        'Dim FactorY As Integer = 41
        Dim FactorY As Integer = 70

        Dim PosX As Integer = 29
        'Dim PosFacLinX As Integer = 201
        Dim PosFacLinX As Integer = 195

        'Dim PosY As Integer = 62
        Dim PosY As Integer = 37

        ReDim rdbPosicion(TotalLlantas - 1)
        For i = 0 To rdbPosicion.Length - 1
            rdbPosicion(i) = New RadioButton()
        Next
        LimpiaPosiciones()

        ClasLlan = New ClasLlanClass(0)
        TablaEstruc = New DataTable

        TablaEstruc = ClasLlan.TablaPosLlanta(vidClasLlan)
        If Not TablaEstruc Is Nothing Then
            If TablaEstruc.Rows.Count > 0 Then
                For i = 0 To TablaEstruc.Rows.Count - 1
                    vNoEje = TablaEstruc.DefaultView.Item(i).Item("EjeNo")
                    vNoLlantas = TablaEstruc.DefaultView.Item(i).Item("NoLlantas")
                    LlantxLado = vNoLlantas / 2
                    'Posiciones Iniciales
                    If LlantxLado = 0 Then
                        'PosX = 29
                        'FactorX = 71
                        PosX = 95
                        FactorX = 37
                    ElseIf LlantxLado = 1 Then
                        'PosX = 29
                        'FactorX = 71
                        PosX = 50
                        FactorX = 37
                    Else
                        'PosX = 5
                        PosX = 25
                        'FactorX = 99
                        'FactorX = 90
                        FactorX = 95
                    End If

                    For z = 0 To vNoLlantas - 1
                        'rdbPosicion(NoPosicion) = New RadioButton
                        rdbPosicion(NoPosicion).Name = "pos" & NoPosicion

                        'Verificamos Nombre de la llanta()
                        llanta = New LlantasClass((NoPosicion + 1), OpcionLlanta.Posicion, txtUniTrans.Text)
                        If llanta.Existe Then
                            rdbPosicion(NoPosicion).Text = NoPosicion + 1 & " (" & llanta.idLlanta & " )"
                        Else
                            rdbPosicion(NoPosicion).Text = NoPosicion + 1
                        End If

                        'rdbPosicion(NoPosicion).Text = NoPosicion + 1
                        'rdbPosicion(NoPosicion).AutoSize = True
                        rdbPosicion(NoPosicion).Checked = False

                        'rdbPosicion(NoPosicion).Size = New System.Drawing.Size(31, 17)

                        'rdbPosicion(NoPosicion).Size = New System.Drawing.Size(95, 64)
                        rdbPosicion(NoPosicion).Size = New System.Drawing.Size(80, 64)

                        rdbPosicion(NoPosicion).Image = My.Resources.llanta2
                        rdbPosicion(NoPosicion).TextImageRelation = TextImageRelation.Overlay
                        rdbPosicion(NoPosicion).ImageAlign = ContentAlignment.TopCenter
                        rdbPosicion(NoPosicion).TextAlign = ContentAlignment.BottomCenter
                        rdbPosicion(NoPosicion).ForeColor = Color.Blue

                        'rdbPosicion(i).Location = New Point(6, 4)

                        If z = 0 And vNoLlantas > 2 Then
                            PosX = 5
                        End If
                        rdbPosicion(NoPosicion).Location = New Point(PosX, PosY)

                        AddHandler rdbPosicion(NoPosicion).Click, AddressOf RadioButtons_Click
                        AddHandler rdbPosicion(NoPosicion).KeyDown, AddressOf ObjEnter_KeyDown
                        AddHandler rdbPosicion(NoPosicion).MouseHover, AddressOf RadioButtons_MouseHover
                        'PanelControles.Controls.Add(rdbLogico1(i))
                        GpoPosiciones.Controls.Add(rdbPosicion(NoPosicion))

                        'PanelLogico1(i).Controls.Add(rdbLogico1(i))
                        PosX = PosX + FactorX



                        If z < vNoLlantas - 1 Then
                            NoPosicion = NoPosicion + 1
                        End If

                        If LlantxLado = 1 Then
                            PosX = PosX + PosFacLinX - FactorX
                        End If

                    Next
                    PosY = PosY + FactorY
                    NoPosicion = NoPosicion + 1
                Next
            End If
        End If
    End Sub
    Private Sub LimpiaPosiciones()
        Dim num_controles As Int32 = Me.GpoPosiciones.Controls.Count - 1
        For n As Integer = num_controles To 0 Step -1
            Dim ctrl As Windows.Forms.Control = Me.GpoPosiciones.Controls(n)
            If TypeOf ctrl Is RadioButton Then

            End If
            Me.GpoPosiciones.Controls.Remove(ctrl)
            ctrl.Dispose()
        Next
        DibujaLinea()
    End Sub

    Private Sub DibujaLinea()
        'Dim canvas As New ShapeContainer
        'Dim theLine As New LineShape
        ' Set the form as the parent of the ShapeContainer.
        'canvas.Parent = Me.GpoPosiciones
        '' Set the ShapeContainer as the parent of the LineShape.
        'theLine.Parent = canvas
        '' Set the starting and ending coordinates for the line.
        'theLine.BorderWidth = 10
        'theLine.StartPoint = New System.Drawing.Point(183, 25)
        'theLine.EndPoint = New System.Drawing.Point(183, 300)
    End Sub
    Private Sub RadioButtons_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs)
        With TryCast(sender, RadioButton)
            pos = ObtienePosicion(.Text)
            '//////// llena datos LLANTA /////////////////
            llanta = New LlantasClass(pos,  OpcionLlanta.Posicion ,txtUniTrans.Text)
            If llanta.Existe Then
                Empresa = New EmpresaClass(llanta.idEmpresa)
                If Empresa.Existe Then
                    NomEmpresa = Empresa.RazonSocial
                End If
                llantaProd = New LLantasProductoClass(llanta.idProductoLlanta)
                If llantaProd.Existe Then
                    Marca = New MarcaClass(llantaProd.idMarca)
                    If Marca.Existe Then
                        NomMarca = Marca.NOMBREMARCA
                    End If
                End If


                tipllanta = New TipoLlantaClass(llanta.idTipoLLanta)
                If tipllanta.Existe Then
                    NomTipLlanta = tipllanta.Descripcion
                End If

                disenio = New CatDiseniosClass(llantaProd.idDisenio)
                If disenio.Existe Then
                    NomDisenio = disenio.Descripcion
                End If

                TextoToolTip = "idLlanta = " & llanta.idLlanta & vbCrLf &
                                "Empresa = " & NomEmpresa & vbCrLf &
                                "Marca = " & NomMarca & vbCrLf &
                                "Dise�o = " & NomDisenio & vbCrLf &
                                "Medida = " & llantaProd.Medida & vbCrLf &
                                "Profundidad = " & llantaProd.Profundidad & vbCrLf &
                                "TipoLlanta = " & NomTipLlanta & vbCrLf &
                                "Ubicacion = " & llanta.idUbicacion & vbCrLf &
                                "Posicion = " & llanta.Posicion
            Else

                TextoToolTip = pos
            End If
            tt.IsBalloon = True
            tt.ShowAlways = True
            tt.SetToolTip(sender, TextoToolTip)
            '//////// llena datos LLANTA /////////////////
            Thread.Sleep(2500)
            'threads


        End With
    End Sub
    Private Sub RadioButtons_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        With TryCast(sender, RadioButton)
            pos = ObtienePosicion(.Text)

            txtPosicion.Text = pos
            cmbPosicion.SelectedValue = pos
            '//////// llena datos LLANTA /////////////////
            llanta = New LlantasClass(pos, OpcionLlanta.Posicion, txtUniTrans.Text)
            If llanta.Existe Then
                Empresa = New EmpresaClass(llanta.idEmpresa)
                If Empresa.Existe Then
                    NomEmpresa = Empresa.RazonSocial
                End If
                llantaProd = New LLantasProductoClass(llanta.idProductoLlanta)
                If llantaProd.Existe Then
                    Marca = New MarcaClass(llantaProd.idMarca)
                    If Marca.Existe Then
                        NomMarca = Marca.NOMBREMARCA
                    End If
                End If
                tipllanta = New TipoLlantaClass(llanta.idTipoLLanta)
                If tipllanta.Existe Then
                    NomTipLlanta = tipllanta.Descripcion
                End If
                disenio = New CatDiseniosClass(llantaProd.idDisenio)
                If disenio.Existe Then
                    NomDisenio = disenio.Descripcion
                End If

                TextoToolTip = "idLlanta = " & llanta.idLlanta & vbCrLf &
                                "Empresa = " & NomEmpresa & vbCrLf &
                                "Marca = " & NomMarca & vbCrLf &
                                "Dise�o = " & NomDisenio & vbCrLf &
                                "Medida = " & llantaProd.Medida & vbCrLf &
                                "Profundidad = " & llantaProd.Profundidad & vbCrLf &
                                "TipoLlanta = " & NomTipLlanta & vbCrLf &
                                "Ubicacion = " & llanta.idUbicacion & vbCrLf &
                                "Posicion = " & llanta.Posicion
            Else

                TextoToolTip = pos
            End If
            tt.IsBalloon = True
            tt.ShowAlways = True
            tt.SetToolTip(txtPosicion, TextoToolTip)
            '//////// llena datos LLANTA /////////////////
            For i = 0 To rdbPosicion.Count - 1
                rdbPosicion(i).BackColor = SystemColors.Control
            Next
            .BackColor = Color.Yellow
            'MessageBox.Show("El Label Tiene Nombre: " & .Name & " y Tiene Escrito:" & .Text, "informacion del Label", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End With
    End Sub
    Private Sub ObjEnter_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        If e.KeyCode = 13 Then
            SendKeys.Send("{TAB}")
            e.Handled = True
        End If
    End Sub
    Private Sub SeleccionaPosicion(ByVal Posicion As Integer)
        If Posicion > rdbPosicion.Count Then
            MsgBox("La Estructura del tipo de unidad " & txtTipoUni.Text & " No cuenta con la posicion " & txtPosicion.Text & vbCrLf & "Favor de seleccionar la Posicion Correspondiente", MsgBoxStyle.Information, Me.Text)
            txtPosicion.Text = ""
            For i = 0 To rdbPosicion.Count - 1
                rdbPosicion(i).BackColor = SystemColors.Control
                rdbPosicion(i).Checked = False
            Next

        Else
            rdbPosicion(Posicion - 1).Checked = True
            For i = 0 To rdbPosicion.Count - 1
                rdbPosicion(i).BackColor = SystemColors.Control
            Next
            rdbPosicion(Posicion - 1).BackColor = Color.Yellow

        End If
    End Sub

    Private Function ObtienePosicion(ByVal Cadena As String) As Integer
        Dim PosiblePos As String
        Dim RespuestaPos As Integer = 0

        PosiblePos = Mid(Cadena, 1, 2)
        If IsNumeric(PosiblePos) Then
            RespuestaPos = Val(PosiblePos)
        Else
            PosiblePos = Mid(Cadena, 1, 1)
            If IsNumeric(PosiblePos) Then
                RespuestaPos = Val(PosiblePos)
            End If
        End If
        Return RespuestaPos

    End Function
    Private Sub cmbUnidades_SelectedValueChanged(sender As Object, e As EventArgs) Handles cmbUnidades.SelectedValueChanged
        If Salida Then Exit Sub
        If _NomTipOrdServ = "LLANTAS" Then
            ParaLlantas(cmbUnidades.SelectedValue)
            LlenaComboLlantas(cmbUnidades.SelectedValue)

        End If

    End Sub
    Private Sub LlenaComboLlantas(ByVal iUnidadTrans As String)
        llanta = New LlantasClass("0", OpcionLlanta.idLlanta)
        MyTablaComboLlanta = llanta.TablaLlantasCombo(" idUbicacion = '" & iUnidadTrans & "'")
        If MyTablaComboLlanta.Rows.Count > 0 Then
            cmbidLlanta.DataSource = MyTablaComboLlanta
            cmbidLlanta.ValueMember = "idLlanta"
            cmbidLlanta.DisplayMember = "idLlanta"

            cmbPosicion.DataSource = MyTablaComboLlanta
            cmbPosicion.ValueMember = "Posicion"
            cmbPosicion.DisplayMember = "Posicion"

        End If



    End Sub

    Private Sub cmbPosicion_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbPosicion.SelectedIndexChanged

    End Sub

    Private Sub cmbPosicion_KeyUp(sender As Object, e As KeyEventArgs) Handles cmbPosicion.KeyUp
        If e.KeyValue = Keys.Enter Then
            If cmbTipoServ.Enabled Then
                cmbTipoServ.SelectedIndex = 0
                cmbTipoServ.Focus()
            ElseIf cmbServicios.Enabled Then
                cmbServicios.SelectedIndex = 0
                cmbServicios.Focus()
            ElseIf txtDescripcion.Enabled Then
                txtDescripcion.Focus()
            End If
        End If
    End Sub

    Private Sub cmbidLlanta_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbidLlanta.SelectedIndexChanged

    End Sub

    Private Sub cmbidLlanta_KeyUp(sender As Object, e As KeyEventArgs) Handles cmbidLlanta.KeyUp
        If e.KeyValue = Keys.Enter Then
            If cmbTipoServ.Enabled Then
                cmbTipoServ.SelectedIndex = 0
                cmbTipoServ.Focus()
            ElseIf cmbServicios.Enabled Then
                cmbServicios.SelectedIndex = 0
                cmbServicios.Focus()
            ElseIf txtDescripcion.Enabled Then
                txtDescripcion.Focus()
            End If
        End If

    End Sub

    Private Sub txtDescripcion_TextChanged(sender As Object, e As EventArgs) Handles txtDescripcion.TextChanged

    End Sub

    Private Sub txtDescripcion_KeyUp(sender As Object, e As KeyEventArgs) Handles txtDescripcion.KeyUp
        If e.KeyValue = Keys.Enter Then
            e.Handled = True
            If cmbDivision.Enabled And cmbDivision.Visible Then
                cmbDivision.Focus()
            Else
                If rdbOpcDesc.Checked Then
                    Dim vUnidad As String
                    If txtDescripcion.Text <> "" Then
                        vUnidad = cmbUnidades.SelectedValue
                        If _bModifica Then
                            Me.grdOrdenTrabajo.Rows.Remove(Me.grdOrdenTrabajo.CurrentRow)
                            InsertaRegGrid(_NumOrdServ, OrdenTrabajo, txtDescripcion.Text, cmbDivision.Text, 0,
                            cmbDivision.SelectedValue, 0, 0, 0, cmbUnidades.SelectedValue, IIf(rdbOpcDesc.Checked, 0, IdTipoServicio()), idOrdenActividad)
                        Else
                            'InsertaRegGrid(_NumOrdServ, 0, txtDescripcion.Text, cmbDivision.Text, 0, cmbDivision.SelectedValue, 0, 0, 0, cmbUnidades.SelectedValue, IIf(rdbOpcDesc.Checked, 0, IdTipoServicio()), 0)
                            InsertaRegGrid(_NumOrdServ, 0, txtDescripcion.Text, cmbDivision.Text, 0, cmbDivision.SelectedValue, 0, 0, 0, cmbUnidades.SelectedValue, IdTipoServicio(), 0)

                        End If

                        cmbUnidades.SelectedValue = vUnidad
                        cmbUnidades.Focus()
                    End If
                End If

            End If

        End If
    End Sub

    Private Sub cmbTipoServ_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbTipoServ.SelectedIndexChanged

    End Sub
End Class
