﻿Imports System.IO
Imports Microsoft.Win32

Public Class fEntregaInsumosOS
    Private _con As String
    Private _conConfigl As String
    Private _IdUsuario As Integer
    Private _nomUsuario As String
    Private _NombreUsuario As String
    Private _IdPreSalida As Integer
    Dim _sqlConCom As String

    Dim PreSal As PreSalidaClass
    Dim personal As PersonalClass
    Dim OT As OrdenTrabajoClass
    Dim OS As OrdenServClass
    Dim Uni As UnidadesTranspClass
    Dim TUni As TipoUniTransClass
    Dim Emp As EmpresaClass

    Dim tdDet As New DataTable

    Dim _TipoOrdenServicio As TipoOrdenServicio = TipoOrdenServicio.TALLER

    Dim Salida As Boolean

    Dim strSql As String
    Dim indice As Integer = 0
    Dim ArraySql() As String

    Dim vIdAlmacen As Integer = 1
    Dim DsConsulta As New DataTable
    Dim _CIDEMPRESA_COM As Integer

    Dim TipDocumento As String
    Private ConceptosCOM As ConceptosCOMClass
    Dim tipoDocto As Integer
    Dim TextoDocumento As String = ""
    Dim vSerie As String = ""
    Dim vFolio As Double = 0
    Dim CIDDOCUMENTO As Integer = 0
    Dim _bPreSal As Boolean

    'Dim IdPreSalida As Integer = 0



    Public Sub New(con As String, nomUsuario As String, IdUsuario As Integer,
    ByVal IdPreSalida As Integer, ByVal vNombreUsuario As String, ByVal conConfigl As String,
    ByVal sqlConComercial As String, ByVal bPreSal As Boolean)
        _con = con
        _IdUsuario = IdUsuario
        _nomUsuario = nomUsuario
        _NombreUsuario = vNombreUsuario
        BD = New CapaDatos.UtilSQL(con, "Juan")
        Inicio.CONSTR = _con
        _IdPreSalida = IdPreSalida
        _conConfigl = conConfigl
        _sqlConCom = sqlConComercial
        _bPreSal = bPreSal

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub

    Private Sub fEntregaInsumosOS_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Salida = True
        AutoCompletarPersonal(cmbIdPersonalRec, TipotablasPer.TpAsignaTaller, 0)

        If _IdPreSalida > 0 Then
            ActivaBotones(False, TipoOpcActivaBoton.tOpcInsertar)
            'Cargar Cabecero y Detalle
            If _bPreSal Then
                'es Pre Salida
                CargaForm(_IdPreSalida, "CABECERO")
            Else
                'es PreOC
                CargaForm(_IdPreSalida, "CABECERO_PREOC")
            End If

            txtIdPreSalida.Enabled = False
        Else
            txtIdPreSalida.Enabled = True
            txtIdPreSalida.Focus()
            'Consultalo
        End If
        Salida = False
    End Sub

    Private Sub CargaForm(ByVal Valor As String, ByVal Opcion As String)
        Select Case Opcion
            Case "CABECERO_PREOC"
                PreSal = New PreSalidaClass(Val(Valor))
                If PreSal.Existe Then
                    txtIdPreSalida.Text = Val(Valor)
                    txtidOrdenTrabajo.Text = PreSal.idOrdenTrabajo
                    dtpFecSolicitud.Value = PreSal.FecSolicitud
                    txtEstatus.Text = PreSal.Estatus

                    txtUserSolicitud.Text = PreSal.NombreCompleto

                    personal = New PersonalClass(PreSal.IdEmpleadoEnc)
                    If personal.Existe Then
                        txtIdEmpleadoEnc.Text = personal.NombreCompleto
                    End If

                    'Orden de Servicio
                    OT = New OrdenTrabajoClass(PreSal.idOrdenTrabajo)
                    If OT.Existe Then
                        txtActividad.Text = OT.NotaRecepcion
                        OS = New OrdenServClass(OT.idOrdenSer)
                        If OS.Existe Then
                            txtidUnidadTrans.Text = OS.idUnidadTrans
                            Uni = New UnidadesTranspClass(OS.idUnidadTrans)
                            If Uni.Existe Then
                                TUni = New TipoUniTransClass(Uni.idTipoUnidad)
                                txtTipoUnidad.Text = TUni.Clasificacion
                            End If
                            Emp = New EmpresaClass(OS.IdEmpresa)
                            If Emp.Existe Then
                                txtNomEmpresa.Text = Emp.RazonSocial
                            End If
                        End If
                    End If

                    'Detalle
                    CargaForm(Valor, "DETALLE_PREOC")
                End If
            Case "CABECERO"
                PreSal = New PreSalidaClass(Val(Valor))
                If PreSal.Existe Then
                    txtIdPreSalida.Text = Val(Valor)
                    txtidOrdenTrabajo.Text = PreSal.idOrdenTrabajo
                    dtpFecSolicitud.Value = PreSal.FecSolicitud
                    txtEstatus.Text = PreSal.Estatus

                    txtUserSolicitud.Text = PreSal.NombreCompleto

                    personal = New PersonalClass(PreSal.IdEmpleadoEnc)
                    If personal.Existe Then
                        txtIdEmpleadoEnc.Text = personal.NombreCompleto
                    End If

                    'Orden de Servicio
                    OT = New OrdenTrabajoClass(PreSal.idOrdenTrabajo)
                    If OT.Existe Then
                        txtActividad.Text = OT.NotaRecepcion
                        OS = New OrdenServClass(OT.idOrdenSer)
                        If OS.Existe Then
                            txtidUnidadTrans.Text = OS.idUnidadTrans
                            Uni = New UnidadesTranspClass(OS.idUnidadTrans)
                            If Uni.Existe Then
                                TUni = New TipoUniTransClass(Uni.idTipoUnidad)
                                txtTipoUnidad.Text = TUni.Clasificacion
                            End If
                            Emp = New EmpresaClass(OS.IdEmpresa)
                            If Emp.Existe Then
                                txtNomEmpresa.Text = Emp.RazonSocial
                            End If
                        End If
                    End If

                    'Detalle
                    CargaForm(Valor, "DETALLE")
                End If
            Case "DETALLE"
                PreSal = New PreSalidaClass(0)
                tdDet = PreSal.tbDetPreSalidaxId(Val(Valor))
                grdDetalle.DataSource = Nothing
                If tdDet.Rows.Count > 0 Then
                    grdDetalle.DataSource = tdDet
                    FormatoGridDetalle()
                End If
            Case "DETALLE_PREOC"
                PreSal = New PreSalidaClass(0)
                tdDet = PreSal.tbDetPreSalidaPreOCxId(Val(Valor))
                grdDetalle.DataSource = Nothing
                If tdDet.Rows.Count > 0 Then
                    grdDetalle.DataSource = tdDet
                    FormatoGridDetalleOC()
                End If
        End Select
    End Sub

    Public Function AutoCompletarPersonal(ByVal Control As ComboBox, ByVal TipoCon As TipotablasPer,
                                          ByVal vEmpresa As Integer, Optional ByVal Control2 As ComboBox = Nothing) As AutoCompleteStringCollection

        Dim Coleccion As New AutoCompleteStringCollection
        Dim dt As DataTable

        personal = New PersonalClass(0)
        If TipoCon = TipotablasPer.tpChoferes Then
            'dt = Personal.TablaChoferesCombo(True, _cveEmpresa)
            dt = personal.TablaChoferesCombo(True, vEmpresa)
        ElseIf TipoCon = TipotablasPer.tpSupervisores Then
            If vEmpresa = 1 Then
                'dt = Personal.TablaAutorizaSolServ(True, _cveEmpresa, 2, 1)
                dt = personal.TablaAutorizaSolServ(True, vEmpresa, 2, 1)
            ElseIf vEmpresa = 2 Then
                'dt = Personal.TablaAutorizaSolServ(True, _cveEmpresa, "1,2,3,5", "")
                dt = personal.TablaAutorizaSolServ(True, vEmpresa, "1,2,3,5", "")
            End If
        ElseIf TipoCon = TipotablasPer.tpTodos Then
            dt = personal.TablaAutorizaSolServ(True, vEmpresa, "", "")
        ElseIf TipoCon = TipotablasPer.TpTaller Then
            dt = personal.TablaAutorizaSolServ(True, 0, "", "6")
        ElseIf TipoCon = TipotablasPer.TpAsignaTaller Then
            Dim filtroEsp As String = " AND  NOT per.idPersonal IN ( " &
            "SELECT DISTINCT dros.idPersonalResp AS idPersonal " &
            "FROM dbo.DetRecepcionOS dros " &
            "INNER JOIN dbo.CabOrdenServicio cab ON cab.idOrdenSer = dros.idOrdenSer " &
            "WHERE cab.Estatus IN  ('ASIG','DIAG') AND ISNULL(dros.idPersonalResp,0) > 0 " &
            "UNION " &
            "SELECT DISTINCT dros.idPersonalAyu1 AS idPersonal " &
            "FROM dbo.DetRecepcionOS dros " &
            "INNER JOIN dbo.CabOrdenServicio cab ON cab.idOrdenSer = dros.idOrdenSer " &
            "WHERE cab.Estatus IN  ('ASIG','DIAG') AND ISNULL(dros.idPersonalAyu1,0) > 0 " &
            "union " &
            "SELECT DISTINCT dros.idPersonalAyu2 AS idPersonal " &
            "FROM dbo.DetRecepcionOS dros " &
            "INNER JOIN dbo.CabOrdenServicio cab ON cab.idOrdenSer = dros.idOrdenSer " &
            "WHERE cab.Estatus IN  ('ASIG','DIAG') AND ISNULL(dros.idPersonalAyu2,0) > 0)"

            dt = personal.TablaRelPersonalRolOrdServ(True, 0, _TipoOrdenServicio, 3, filtroEsp)
        End If

        'Recorrer y cargar los items para el Autocompletado
        For Each row As DataRow In dt.Rows
            Coleccion.Add(Convert.ToString(row("NombreCompleto")))
        Next
        'Ajustamos el control TextBox o ComboBox para recibir los datos de la siguiente manera.
        With Control
            .DataSource = dt
            .DisplayMember = "NombreCompleto"
            .ValueMember = "idPersonal"



            .AutoCompleteCustomSource = Coleccion
            .AutoCompleteMode = AutoCompleteMode.Suggest
            .AutoCompleteSource = AutoCompleteSource.CustomSource
        End With

        If Not Control2 Is Nothing Then
            Control2.DataSource = dt
            Control2.ValueMember = "idpuesto"
            Control2.DisplayMember = "Nompuesto"
        End If

        ' Aca le devuelvo los datos a recuperados de la Base
        Return Coleccion

    End Function

    Private Sub FormatoGridDetalle()
        grdDetalle.Columns("IdPreSalida").Visible = False

        grdDetalle.Columns("Cantidad").HeaderText = "Cantidad"
        grdDetalle.Columns("idProducto").HeaderText = "Producto"
        grdDetalle.Columns("CNOMBREPRODUCTO").HeaderText = "Descripción"
        grdDetalle.Columns("CantidadEnt").HeaderText = "Entregado"

        grdDetalle.Columns("Cantidad").AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader
        grdDetalle.Columns("idProducto").AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader
        grdDetalle.Columns("CNOMBREPRODUCTO").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        grdDetalle.Columns("CantidadEnt").AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader
    End Sub

    Private Sub FormatoGridDetalleOC()
        grdDetalle.Columns("IdPreOC").Visible = False

        grdDetalle.Columns("CantidadSol").HeaderText = "Cantidad"
        grdDetalle.Columns("idProducto").HeaderText = "Producto"
        grdDetalle.Columns("CNOMBREPRODUCTO").HeaderText = "Descripción"
        grdDetalle.Columns("CantidadSol").HeaderText = "Entregado"

        grdDetalle.Columns("CantidadSol").AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader
        grdDetalle.Columns("idProducto").AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader
        grdDetalle.Columns("CNOMBREPRODUCTO").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        grdDetalle.Columns("CantidadSol").AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader
    End Sub

    'Private Sub grdDetalle_Click(sender As Object, e As EventArgs) Handles grdDetalle.Click
    '    If grdDetalle.Item("Selecciona", grdDetalle.CurrentCell.RowIndex).Value = 1 Then
    '        grdDetalle.Item("CantidadEnt", grdDetalle.CurrentCell.RowIndex).Value =
    '            grdDetalle.Item("Cantidad", grdDetalle.CurrentCell.RowIndex).Value
    '    End If
    'End Sub

    'Private Sub grdDetalle_CurrentCellDirtyStateChanged(sender As Object, e As EventArgs) Handles grdDetalle.CurrentCellDirtyStateChanged
    '    If grdDetalle.IsCurrentCellDirty Then
    '        grdDetalle.CommitEdit(DataGridViewDataErrorContexts.Commit)

    '    End If
    'End Sub

    Private Sub grdDetalle_CellValueChanged(sender As Object, e As DataGridViewCellEventArgs) Handles grdDetalle.CellValueChanged
        If Salida Then Exit Sub

        If grdDetalle.Item("Entregar", grdDetalle.CurrentCell.RowIndex).Value Then

            If grdDetalle.Item("CantidadEnt", grdDetalle.CurrentCell.RowIndex).Value >
                grdDetalle.Item("Cantidad", grdDetalle.CurrentCell.RowIndex).Value Then
                grdDetalle.Item("CantidadEnt", grdDetalle.CurrentCell.RowIndex).Value =
                grdDetalle.Item("Cantidad", grdDetalle.CurrentCell.RowIndex).Value
            ElseIf grdDetalle.Item("CantidadEnt", grdDetalle.CurrentCell.RowIndex).Value = 0 Then
                grdDetalle.Item("CantidadEnt", grdDetalle.CurrentCell.RowIndex).Value =
             grdDetalle.Item("Cantidad", grdDetalle.CurrentCell.RowIndex).Value
            End If
        Else
            If grdDetalle.Item("CantidadEnt", grdDetalle.CurrentCell.RowIndex).Value > 0 Then
                grdDetalle.Item("CantidadEnt", grdDetalle.CurrentCell.RowIndex).Value = 0
            End If
        End If
    End Sub

    Private Sub ActivaBotones(ByVal Valor As Boolean, Optional ByVal vOpcion As TipoOpcActivaBoton = TipoOpcActivaBoton.tOpcInicializa)
        'Si es Verdadero Se activan Ok,Cancelar y Buscar y Terminar Se desactiva
        If vOpcion = TipoOpcActivaBoton.tOpcConsulta Then

            btnMnuOk.Enabled = False
            btnMnuLimpiar.Enabled = True
            btnMnuCancelar.Enabled = False
            btnMenuReImprimir.Enabled = True
            btnMnuSalir.Enabled = False
        ElseIf vOpcion = TipoOpcActivaBoton.tOpcInicializa Then
            'btnMnuOk.Enabled = Valor
            'btnMnuLimpiar.Enabled = Valor
            'btnMnuCancelar.Enabled = Valor
            'btnMnuSalir.Enabled = Not Valor

            btnMnuOk.Enabled = Valor
            btnMnuLimpiar.Enabled = Valor
            btnMnuCancelar.Enabled = Valor
            btnMenuReImprimir.Enabled = Valor
            btnMnuSalir.Enabled = Not Valor


        ElseIf vOpcion = TipoOpcActivaBoton.tOpcInsertar Then
            btnMnuOk.Enabled = True
            btnMenuReImprimir.Enabled = True
            btnMnuLimpiar.Enabled = False
            btnMnuCancelar.Enabled = False
            btnMnuSalir.Enabled = False

        ElseIf vOpcion = TipoOpcActivaBoton.tOpcEditar Then
            btnMnuOk.Enabled = Not Valor
            btnMnuLimpiar.Enabled = Valor
            btnMnuCancelar.Enabled = Valor
            btnMenuReImprimir.Enabled = Valor
            btnMnuSalir.Enabled = Not Valor

        End If
    End Sub

    Private Sub btnMnuOk_Click(sender As Object, e As EventArgs) Handles btnMnuOk.Click
        Dim objsql As New ToolSQLs
        Dim BandPreOc As Boolean = False
        Dim CantidadFaltante As Decimal
        Dim Mensaje As String
        Dim BandGrabadoCOM As Boolean = False
        Try
            Salida = True
            indice = 0
            Mensaje = "!! Desea Entregar los Insumos y/o Refacciones del Folio ? " & txtIdPreSalida.Text & " !!"

            If MsgBox(Mensaje, MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then

                If Not Validar() Then Exit Sub

                'se checa si es presalida o PreOC
                If _bPreSal Then
                    For i = 0 To grdDetalle.Rows.Count - 1
                        'Agregamos Detalle
                        strSql = objsql.ActualizaDetPreSalida(txtIdPreSalida.Text,
                        grdDetalle.Item("idProducto", i).Value,
                        grdDetalle.Item("CantidadEnt", i).Value)

                        ReDim Preserve ArraySql(indice)
                        ArraySql(indice) = strSql
                        indice += 1

                        If grdDetalle.Item("Cantidad", i).Value < grdDetalle.Item("CantidadEnt", i).Value Then
                            'Se tiene que Crear PreOrden Compra
                            If Not BandPreOc Then
                                BandPreOc = True
                                'Creamos Cabecero
                                strSql = objsql.InsertaCabPreOC(True, 0, txtidOrdenTrabajo.Text, Now, _nomUsuario, vIdAlmacen, "PRE")
                                ReDim Preserve ArraySql(indice)
                                ArraySql(indice) = strSql
                                indice += 1
                            End If
                            'Agregamos Detalle PREOC
                            CantidadFaltante = grdDetalle.Item("Cantidad", i).Value - grdDetalle.Item("CantidadEnt", i).Value
                            strSql = objsql.InsertaDetbPreOC(0,
                                   grdDetalle.Item("idProducto", i).Value,
                                   grdDetalle.Item("idUnidadProd", i).Value,
                                   CantidadFaltante)
                            ReDim Preserve ArraySql(indice)
                            ArraySql(indice) = strSql
                            indice += 1
                        End If
                    Next

                    strSql = objsql.ActualizaCabPreSalida(txtIdPreSalida.Text, _nomUsuario, cmbIdPersonalRec.SelectedValue, chkEntregaCambio.Checked, 0, "ENT")
                    ReDim Preserve ArraySql(indice)
                    ArraySql(indice) = strSql
                    indice += 1
                Else
                    'presalida
                    'Se crea la Presalida
                    'Cabecero
                    'strSql = objsql.InsertaCabPreSalida(True, NoIdPreSal, vIdOrdenTrab,
                    '        Now, _Usuario, cmbResponsable.SelectedValue, vIdAlmacen, "PEN", 123123)
                    'ReDim Preserve ArraySql(indice)
                    'ArraySql(indice) = strSql
                    'indice += 1
                    ''Detalle
                    'For i = 0 To grdDetalle.Rows.Count - 1
                    '    strSql = objsql.InsertaDetPreSalida(NoIdPreSal,
                    '        grdDetalle.Item("idProducto", i).Value,
                    '        grdDetalle.Item("idUnidadProd", i).Value,
                    '        grdDetalle.Item("Cantidad", i).Value,
                    '        grdDetalle.Item("Costo", i).Value,
                    '        grdDetalle.Item("trefExistencia", i).Value)

                    '    ReDim Preserve ArraySql(indice)
                    '    ArraySql(indice) = strSql
                    '    indice += 1
                    'Next
                End If





                If indice > 0 Then
                    Dim obj As New CapaNegocio.Tablas
                    Dim Respuesta As String
                    'Para Impresion
                    'Dim sOrdenes As String = ""
                    'Dim idOS As Integer = 0
                    'Dim TipidOS As Boolean = False

                    Respuesta = obj.EjecutarSql(Inicio.CONSTR, "SQL", ArraySql, Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, indice)
                    FEspera.Close()
                    If Respuesta = "EFECTUADO" Then

                        InicializaSDK()
                        If bEmpresaSDKAbierta Then
                            If BDCOM Is Nothing Then
                                BDCOM = New CapaDatos.UtilSQL(_sqlConCom, "")
                            End If
                            'Ya se puede hacer la Salida
                            TipDocumento = 35
                            ''OBTENER DATOS DEL CONCEPTO
                            ConceptosCOM = New ConceptosCOMClass(TipDocumento)
                            If ConceptosCOM.Existe Then
                                tipoDocto = ConceptosCOM.CIDCONCEPTODOCUMENTO
                                TextoDocumento = ConceptosCOM.CNOMBRECONCEPTO
                            End If
                            lError = fSiguienteFolio(Trim(TipDocumento), vSerie, vFolio)
                            If lError <> 0 Then
                                FEspera.Close()
                                DespliegaError(lError)
                                fCierraEmpresa()
                                fTerminaSDK()
                                'BandGraba = False
                            Else
                                If vSerie = "" Then
                                    vSerie = ConceptosCOM.CSERIEPOROMISION
                                End If
                                If MsgBox("!! Esta seguro que desea Aplicar el Documento: " & TextoDocumento & " : " & vSerie & " " & vFolio & " ? !!", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                                    FEspera.StartPosition = FormStartPosition.CenterScreen
                                    FEspera.Show("Generando " & TextoDocumento & " en Sistema COMERCIAL ...")

                                    If AgregaCabeceroDetalle_SDK_ALTA(TipDocumento, tipoDocto, vSerie, vFolio, "", vIdAlmacen) Then
                                        'FEspera.Show("Guardando Datos Locales ...")
                                        '27/DIC/2017
                                        'Actualizar Folio,Noserie y CIDDOCUMENTO
                                        indice = 0
                                        strSql = objsql.ActualizaCampoTabla("CabPreSalida", "CSERIEDOCUMENTO",
                                        TipoDato.TdCadena, vSerie, "IdPreSalida", TipoDato.TdNumerico, txtIdPreSalida.Text)
                                        ReDim Preserve ArraySql(indice)
                                        ArraySql(indice) = strSql
                                        indice += 1

                                        strSql = objsql.ActualizaCampoTabla("CabPreSalida", "CFOLIO",
                                        TipoDato.TdNumerico, vFolio, "IdPreSalida", TipoDato.TdNumerico, txtIdPreSalida.Text)
                                        ReDim Preserve ArraySql(indice)
                                        ArraySql(indice) = strSql
                                        indice += 1

                                        strSql = objsql.ActualizaCampoTabla("CabPreSalida", "CIDDOCUMENTO",
                                        TipoDato.TdNumerico, CIDDOCUMENTO, "IdPreSalida", TipoDato.TdNumerico, txtIdPreSalida.Text)
                                        ReDim Preserve ArraySql(indice)
                                        ArraySql(indice) = strSql
                                        indice += 1


                                        Respuesta = obj.EjecutarSql(Inicio.CONSTR, "SQL", ArraySql, Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, indice)
                                        If Respuesta = "EFECTUADO" Then
                                            BandGrabadoCOM = True
                                            FEspera.Close()
                                        End If


                                    End If
                                End If
                            End If


                        End If

                        If BandGrabadoCOM Then
                            MsgBox("Se Entrego Correctamente el Folio " & txtIdPreSalida.Text, MsgBoxStyle.Exclamation, Me.Text)


                            'Se debe Imprimir Algo ????
                            'If SetupThePrinting() Then
                            '    For imp = 0 To ArrayOrdenSer.Length - 1
                            '        idOS = ArrayOrdenSer(imp)
                            '        TipidOS = ArrayOrdenSerTipo(imp)
                            '        ImprimeOrdenServicio(idOS, TipidOS, True, True)
                            '    Next
                            'End If

                        End If


                    ElseIf Respuesta = "NOEFECTUADO" Then

                    ElseIf Respuesta = "ERROR" Then
                        indice = 0
                        SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")
                    End If
                Else
                    MsgBox("No se Encontraron Registros que Grabar", MsgBoxStyle.Exclamation, Me.Text)
                End If
                Cancelar()
            End If


        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try

        'Pasos:
        '1 Actualizar Cabecero
        '2 Actualizar Detalle
        '3 En caso de tener Faltantes realizar Orden de Compra
        'Enviar a Comercial

    End Sub
    Private Function Validar() As Boolean
        If cmbIdPersonalRec.Text = "" Then
            MsgBox("Tiene que seleccionar quien recibe los insumos o Refacciones", vbInformation, "Aviso" & Me.Text)
            cmbIdPersonalRec.Focus()
            Return False
        End If

        For i = 0 To grdDetalle.Rows.Count - 1
            If Not grdDetalle.Item("Entregar", i).Value Then
                MsgBox("Todas los insumos o Refacciones deben estar Marcados", vbInformation, "Aviso" & Me.Text)
                'Exit For
                grdDetalle.Focus()
                Return False
            End If
        Next
        Return True
    End Function

    Private Sub Cancelar()
        If _IdPreSalida > 0 Then
            Close()
        Else

        End If
    End Sub


    Private Sub InicializaSDK()

        Dim KeySistema As RegistryKey = Registry.LocalMachine.OpenSubKey(szRegKeySistema)
        Dim lEntrada = KeySistema.GetValue("DirectorioBase")
        Directory.SetCurrentDirectory(lEntrada)

        _CIDEMPRESA_COM = 3
        Dim RutaDatosCOM = ConsultaEmpresaCOM(_CIDEMPRESA_COM)

        lError = fSetNombrePAQ("CONTPAQ I COMERCIAL")
        If lError <> 0 Then
            If lError = 999999 Then
                bActivoSDK = True
            Else
                rError(lError)
                bActivoSDK = False
            End If

        Else
            lError = fAbreEmpresa(RutaDatosCOM)
            If lError <> 0 Then
                bEmpresaSDKAbierta = False
                DespliegaError(lError)
            Else
                bEmpresaSDKAbierta = True
                sRutaEmpresaAdmPAQ = RutaDatosCOM
            End If

        End If
    End Sub
    Private Function ConsultaEmpresaCOM(ByVal idEmpresa As Integer) As String
        Dim util As New CapaNegocio.Parametros
        Dim RutaDatosCOM As String = ""
        BDCOMConf = New CapaDatos.UtilSQL(_conConfigl, "Juan")

        strSql = "SELECT CIDEMPRESA,CNOMBREEMPRESA,CRUTADATOS,CRUTARESPALDOS FROM Empresas where CIDEMPRESA = " & idEmpresa

        DsConsulta = BDCOMConf.ExecuteReturn(strSql)
        If DsConsulta.Rows.Count > 0 Then
            For i = 0 To DsConsulta.Rows.Count - 1
                RutaDatosCOM = DsConsulta.Rows(i).Item("CRUTADATOS")
            Next
        End If

        Return RutaDatosCOM

    End Function

    Private Function AgregaCabeceroDetalle_SDK_ALTA(ByVal TipoDocumento As String, ByVal tipoDocto As String,
    ByVal vSerie As String, ByVal vFolio As Integer, ByVal CodigoCliente As String, ByVal vAlmacen As String) As Boolean
        Dim idDoc As String = ""

        AgregaCabeceroDetalle_SDK_ALTA = False

        Dim lIdDocto As Integer
        Dim LIdMovto As Integer

        Dim ltDocto As New tDocumento
        Dim ltMovto As New tMovimiento

        ltDocto.aCodConcepto = TipoDocumento
        ltDocto.aCodigoCteProv = CodigoCliente
        ltDocto.aCodigoAgente = "(Ninguno)"
        ltDocto.aSistemaOrigen = 0
        ltDocto.aNumMoneda = 1
        ltDocto.aTipoCambio = 1
        ltDocto.aAfecta = 1

        lError = fAltaDocumento(lIdDocto, ltDocto)

        If lError = 0 Then
            CIDDOCUMENTO = lIdDocto
            For i = 0 To grdDetalle.RowCount - 1
                'If grdDetalle.Item("geEnvasado", i).Value <> "" Then
                'ltMovto.aCodAlmacen = "1"
                ltMovto.aCodAlmacen = vAlmacen
                ltMovto.aConsecutivo = i + 1
                ltMovto.aCodProdSer = grdDetalle.Item("CCODIGOPRODUCTO", i).Value
                ltMovto.aUnidades = grdDetalle.Item("CantidadEnt", i).Value
                ltMovto.aPrecio = 0
                ltMovto.aCosto = grdDetalle.Item("Costo", i).Value
                'ltMovto.aReferencia = grdDetFactura.Item("gfdTextoComp", i).Value

                lError = fAltaMovimiento(lIdDocto, LIdMovto, ltMovto)
                If lError <> 0 Then
                    MensajeError(lError)
                    AgregaCabeceroDetalle_SDK_ALTA = False
                    FEspera.Close()
                    Exit For
                End If
                'End If
            Next

            AgregaCabeceroDetalle_SDK_ALTA = True
        Else
            MensajeError(lError)
            FEspera.Close()
            fBorraDocumento()

        End If

        'lError = fLeeDatoDocumento(kDocumento_IdDocumento, idDoc, kLongDescripcion - 1)
        'If lError <> 0 Then
        '    MensajeError(lError)
        '    FEspera.Close()
        '    Exit Function
        'End If

        'lError = fAfectaDocto_Param(TipoDocumento, vSerie, vFolio, True)
        'If lError <> 0 Then
        '    MensajeError(lError)
        '    'fCierraEmpresa()
        '    'fTerminaSDK(
        '    FEspera.Close()
        '    Exit Function
        'End If


        If tipoDocto = "4" Then
            'Timbra Factura
            'If TimbraFactura_SDK(TipoDocumento) Then
            '    Return AgregaCabeceroDetalle_SDK_ALTA
            'End If
        Else
            Return AgregaCabeceroDetalle_SDK_ALTA
        End If

    End Function

    Private Sub btnMnuLimpiar_Click(sender As Object, e As EventArgs) Handles btnMnuLimpiar.Click

    End Sub

    Private Sub btnMnuCancelar_Click(sender As Object, e As EventArgs) Handles btnMnuCancelar.Click

    End Sub

    Private Sub grdDetalle_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles grdDetalle.CellContentClick

    End Sub

    Private Sub btnMnuSalir_Click(sender As Object, e As EventArgs) Handles btnMnuSalir.Click

    End Sub
End Class