'Fecha: 15 / Febrero / 2016
'ROGER GALA PACHECO
'************************************************************************************************************************************
'*  Fecha Modificacion :   15 / Febrero / 2016                                                                                       
'*  
'************************************************************************************************************************************
Public Class frmDiagnosticoTaller
    Inherits System.Windows.Forms.Form
    Private _Tag As String
    Dim bEsIdentidad As Boolean = True
    Dim OpcionForma As TipoOpcionForma
    Dim TablaBd As String = "Diagnostico"

    Private OrdenTrabajo As OrdenTrabajoClass
    Private ULTOT As OrdenTrabajoClass
    Private Personal As PersonalClass
    Private PersonalPuesto As PuestosClass
    Private FamiliaOT As FamiliasClass

    Private OrdenServ As OrdenServClass
    Private TipOrd As TipoOrdenClass
    Private User As UsuarioClass
    Private Emp As EmpresaClass
    Private DetOS As detOrdenServicioClass
    Private UniTrans As UnidadesTranspClass
    Private Marca As MarcaClass

    Dim MyTablaHisOS As DataTable = New DataTable(TablaBd)
    Dim MyTablaHisOT As DataTable = New DataTable(TablaBd)
    Dim MyTablaRefac As DataTable = New DataTable(TablaBd)

    Dim MyTablaComboResp As DataTable = New DataTable(TablaBd)
    Dim MyTablaComboAyu1 As DataTable = New DataTable(TablaBd)
    Dim MyTablaComboAyu2 As DataTable = New DataTable(TablaBd)

    Dim MyTablaPersOcupado As DataTable = New DataTable(TablaBd)

    Private ProdCom As ProductosComClass
    Private UnidadCom As UnidadesComClass

    Private ObjSql As New ToolSQLs

    Dim Salida As Boolean
    Dim myDataColumn As DataColumn
    Dim myDataRow As DataRow

    Dim CantidadGrid As Double = 0
    Dim BandNoExisteProdEnGrid As Boolean = False

    Dim ObjCom As New FuncionesComClass

    Dim vCIDPRODUCTO As Integer
    Dim vCIDUNIDAD As Integer

    Dim vIdAlmacen As Integer = 1
    Dim IdOrdenServ As Integer

    Dim CadCam As String = ""
    Dim StrSql As String = ""

    Dim indice As Integer = 0
    Dim ArraySql() As String

    Dim NoIdPreOC As Integer = 0
    Dim NoIdPreSal As Integer = 0

    Dim objNumSig As New CapaNegocio.Tablas

    Dim ContPersonal As Integer = 0
    Dim CostoManoObra As Double = 0
    Dim CostoRef As Double = 0

    Dim vLugOcupado As Integer = 0

    Friend WithEvents txtUltDivision As System.Windows.Forms.TextBox
    Friend WithEvents txtUltFamilia As System.Windows.Forms.TextBox
    Friend WithEvents txtUltFalla As System.Windows.Forms.TextBox
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents hosConsecutivo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents hosIdServicio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents hosFechaRecepcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents hosTipoOrden As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents hosKm As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents hosAutorizo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents hosEntrego As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents hosRecepciono As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents hosFinalizo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents hotidOrdenTrabajo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents hotIdOrdenSer As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents hotFechaFin As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents hotAsigno As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents hotMecanico As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents hotDuracion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents grefConsecutivo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents grefCCODIGOP01_PRO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents grefCIDPRODUCTO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents grefCNOMBREP01 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents grefExistencia As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents grefCantidad As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents grefCCLAVEINT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents grefBComprar As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents grefCOSTO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents grefCIDUNIDADBASE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents grefCantCompra As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents grefCantSalida As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents cmbayudante2 As System.Windows.Forms.ComboBox
    Friend WithEvents cmbayudante1 As System.Windows.Forms.ComboBox
    Friend WithEvents cmbResponsable As System.Windows.Forms.ComboBox
    Friend WithEvents cmbPuestoAyu2 As System.Windows.Forms.ComboBox
    Friend WithEvents cmbPuestoAyu1 As System.Windows.Forms.ComboBox
    Friend WithEvents cmbPuestoResp As System.Windows.Forms.ComboBox
    Friend WithEvents chkAyu2 As System.Windows.Forms.CheckBox
    Friend WithEvents chkAyu1 As System.Windows.Forms.CheckBox
    Friend WithEvents Label28 As System.Windows.Forms.Label
    '
    'Dim CampoLLave As String = "idHerramienta"
    'Dim TablaBd2 As String = "CatFamilia"
    'Dim TablaBd3 As String = "CatTipoHerramienta"
    'Dim TablaBd4 As String = "CatMarcas"

    'Private Herramienta As HerramientaClass
    'Private Familia As FamiliasClass
    'Private TipoHerramienta As TipoHerramClass
    'Private Marca As MarcaClass

    Private _NoServ As Integer
    Private _con As String
    Private _Usuario As String
    Private _IdUsuario As Integer
    Private _cveEmpresa As Integer

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Public WithEvents MeStatus1 As ControlStatus.MeStatus
    Friend WithEvents btnMnuOk As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuCancelar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuSalir As System.Windows.Forms.ToolStripButton
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtNomEmpresa As System.Windows.Forms.TextBox
    Friend WithEvents dtpFechaAsigHora As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpFechaAsig As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtUsuarioAsig As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtDuracion As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtPuestoMec As System.Windows.Forms.TextBox
    Friend WithEvents txtNombreMecanico As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents tabPrinc As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents txtUltimoMecanico As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents txtUltOrdenTrabajo As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents txtUltimaOrdenServ As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txtMarcaUni As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtNomUnidad As System.Windows.Forms.TextBox
    Friend WithEvents txtIdUnidad As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtKilometraje As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents txtAutorizo As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txtEntrego As System.Windows.Forms.TextBox
    Friend WithEvents txtRecepciono As System.Windows.Forms.TextBox
    Friend WithEvents dtpFechaOrden As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtOrdenServ As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents dtpFechaOrdenhr As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtEstatusOrden As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtTipoOrden As System.Windows.Forms.TextBox
    Friend WithEvents label2 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents grdOrdenServHis As System.Windows.Forms.DataGridView
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents grdProductosOT As System.Windows.Forms.DataGridView
    Friend WithEvents txtCantidad As System.Windows.Forms.TextBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents txtNomUniProd As System.Windows.Forms.TextBox
    Friend WithEvents cmdBuscaUniProd As System.Windows.Forms.Button
    Friend WithEvents txtIdUnidadProd As System.Windows.Forms.TextBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents txtNomProducto As System.Windows.Forms.TextBox
    Friend WithEvents cmdBuscaProd As System.Windows.Forms.Button
    Friend WithEvents txtIdProducto As System.Windows.Forms.TextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents txtDivision As System.Windows.Forms.TextBox
    Friend WithEvents txtFamilia As System.Windows.Forms.TextBox
    Friend WithEvents txtFalla As System.Windows.Forms.TextBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents grdOrdenTrabajoHis As System.Windows.Forms.DataGridView
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents grdRefaccionesHis As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn16 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn17 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn18 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn19 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn20 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn21 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewCheckBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GrupoCaptura As System.Windows.Forms.GroupBox
    Friend WithEvents ToolStripMenu As System.Windows.Forms.ToolStrip
    Friend WithEvents label1 As System.Windows.Forms.Label
    Friend WithEvents cmdBuscaClave As System.Windows.Forms.Button
    Friend WithEvents txtOrdenTrabajo As System.Windows.Forms.TextBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDiagnosticoTaller))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle19 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle23 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle24 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle20 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle21 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle22 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.txtOrdenTrabajo = New System.Windows.Forms.TextBox()
        Me.label1 = New System.Windows.Forms.Label()
        Me.GrupoCaptura = New System.Windows.Forms.GroupBox()
        Me.txtDivision = New System.Windows.Forms.TextBox()
        Me.txtFamilia = New System.Windows.Forms.TextBox()
        Me.txtFalla = New System.Windows.Forms.TextBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.txtUsuarioAsig = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtDuracion = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtPuestoMec = New System.Windows.Forms.TextBox()
        Me.txtNombreMecanico = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.dtpFechaAsigHora = New System.Windows.Forms.DateTimePicker()
        Me.dtpFechaAsig = New System.Windows.Forms.DateTimePicker()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtNomEmpresa = New System.Windows.Forms.TextBox()
        Me.cmdBuscaClave = New System.Windows.Forms.Button()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.ToolStripMenu = New System.Windows.Forms.ToolStrip()
        Me.btnMnuOk = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuCancelar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuSalir = New System.Windows.Forms.ToolStripButton()
        Me.MeStatus1 = New ControlStatus.MeStatus()
        Me.tabPrinc = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.chkAyu2 = New System.Windows.Forms.CheckBox()
        Me.chkAyu1 = New System.Windows.Forms.CheckBox()
        Me.cmbPuestoAyu2 = New System.Windows.Forms.ComboBox()
        Me.cmbPuestoAyu1 = New System.Windows.Forms.ComboBox()
        Me.cmbPuestoResp = New System.Windows.Forms.ComboBox()
        Me.cmbayudante2 = New System.Windows.Forms.ComboBox()
        Me.cmbayudante1 = New System.Windows.Forms.ComboBox()
        Me.cmbResponsable = New System.Windows.Forms.ComboBox()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.txtUltDivision = New System.Windows.Forms.TextBox()
        Me.txtUltFamilia = New System.Windows.Forms.TextBox()
        Me.txtUltFalla = New System.Windows.Forms.TextBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.txtUltimoMecanico = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.txtUltOrdenTrabajo = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.txtUltimaOrdenServ = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.txtMarcaUni = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtNomUnidad = New System.Windows.Forms.TextBox()
        Me.txtIdUnidad = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtKilometraje = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txtAutorizo = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txtEntrego = New System.Windows.Forms.TextBox()
        Me.txtRecepciono = New System.Windows.Forms.TextBox()
        Me.dtpFechaOrden = New System.Windows.Forms.DateTimePicker()
        Me.txtOrdenServ = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.dtpFechaOrdenhr = New System.Windows.Forms.DateTimePicker()
        Me.txtEstatusOrden = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtTipoOrden = New System.Windows.Forms.TextBox()
        Me.label2 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.grdRefaccionesHis = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn16 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn17 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn18 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn19 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn20 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn21 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewCheckBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.grdOrdenTrabajoHis = New System.Windows.Forms.DataGridView()
        Me.hotidOrdenTrabajo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.hotIdOrdenSer = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.hotFechaFin = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.hotAsigno = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.hotMecanico = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.hotDuracion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.grdOrdenServHis = New System.Windows.Forms.DataGridView()
        Me.hosConsecutivo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.hosIdServicio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.hosFechaRecepcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.hosTipoOrden = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.hosKm = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.hosAutorizo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.hosEntrego = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.hosRecepciono = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.hosFinalizo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.grdProductosOT = New System.Windows.Forms.DataGridView()
        Me.grefConsecutivo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.grefCCODIGOP01_PRO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.grefCIDPRODUCTO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.grefCNOMBREP01 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.grefExistencia = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.grefCantidad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.grefCCLAVEINT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.grefBComprar = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.grefCOSTO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.grefCIDUNIDADBASE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.grefCantCompra = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.grefCantSalida = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.txtCantidad = New System.Windows.Forms.TextBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.txtNomUniProd = New System.Windows.Forms.TextBox()
        Me.txtIdUnidadProd = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.txtNomProducto = New System.Windows.Forms.TextBox()
        Me.txtIdProducto = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.cmdBuscaUniProd = New System.Windows.Forms.Button()
        Me.cmdBuscaProd = New System.Windows.Forms.Button()
        Me.GrupoCaptura.SuspendLayout()
        Me.ToolStripMenu.SuspendLayout()
        Me.tabPrinc.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        CType(Me.grdRefaccionesHis, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdOrdenTrabajoHis, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdOrdenServHis, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage3.SuspendLayout()
        CType(Me.grdProductosOT, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtOrdenTrabajo
        '
        Me.txtOrdenTrabajo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtOrdenTrabajo.ForeColor = System.Drawing.Color.Red
        Me.txtOrdenTrabajo.Location = New System.Drawing.Point(138, 19)
        Me.txtOrdenTrabajo.MaxLength = 20
        Me.txtOrdenTrabajo.Name = "txtOrdenTrabajo"
        Me.txtOrdenTrabajo.Size = New System.Drawing.Size(69, 20)
        Me.txtOrdenTrabajo.TabIndex = 2
        Me.txtOrdenTrabajo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'label1
        '
        Me.label1.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.label1.Location = New System.Drawing.Point(10, 22)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(126, 20)
        Me.label1.TabIndex = 14
        Me.label1.Text = "Folio O.T.:"
        '
        'GrupoCaptura
        '
        Me.GrupoCaptura.Controls.Add(Me.txtDivision)
        Me.GrupoCaptura.Controls.Add(Me.txtFamilia)
        Me.GrupoCaptura.Controls.Add(Me.txtFalla)
        Me.GrupoCaptura.Controls.Add(Me.Label23)
        Me.GrupoCaptura.Controls.Add(Me.txtUsuarioAsig)
        Me.GrupoCaptura.Controls.Add(Me.Label6)
        Me.GrupoCaptura.Controls.Add(Me.txtDuracion)
        Me.GrupoCaptura.Controls.Add(Me.Label5)
        Me.GrupoCaptura.Controls.Add(Me.txtPuestoMec)
        Me.GrupoCaptura.Controls.Add(Me.txtNombreMecanico)
        Me.GrupoCaptura.Controls.Add(Me.Label4)
        Me.GrupoCaptura.Controls.Add(Me.dtpFechaAsigHora)
        Me.GrupoCaptura.Controls.Add(Me.dtpFechaAsig)
        Me.GrupoCaptura.Controls.Add(Me.Label10)
        Me.GrupoCaptura.Controls.Add(Me.Label9)
        Me.GrupoCaptura.Controls.Add(Me.txtNomEmpresa)
        Me.GrupoCaptura.Controls.Add(Me.cmdBuscaClave)
        Me.GrupoCaptura.Controls.Add(Me.txtOrdenTrabajo)
        Me.GrupoCaptura.Controls.Add(Me.label1)
        Me.GrupoCaptura.Controls.Add(Me.Label25)
        Me.GrupoCaptura.Controls.Add(Me.Label24)
        Me.GrupoCaptura.Location = New System.Drawing.Point(12, 54)
        Me.GrupoCaptura.Name = "GrupoCaptura"
        Me.GrupoCaptura.Size = New System.Drawing.Size(845, 133)
        Me.GrupoCaptura.TabIndex = 3
        Me.GrupoCaptura.TabStop = False
        Me.GrupoCaptura.Text = "Orden de Trabajo"
        '
        'txtDivision
        '
        Me.txtDivision.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDivision.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDivision.ForeColor = System.Drawing.Color.Red
        Me.txtDivision.Location = New System.Drawing.Point(679, 99)
        Me.txtDivision.MaxLength = 3
        Me.txtDivision.Name = "txtDivision"
        Me.txtDivision.ReadOnly = True
        Me.txtDivision.Size = New System.Drawing.Size(159, 26)
        Me.txtDivision.TabIndex = 13
        '
        'txtFamilia
        '
        Me.txtFamilia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtFamilia.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFamilia.ForeColor = System.Drawing.Color.Red
        Me.txtFamilia.Location = New System.Drawing.Point(449, 99)
        Me.txtFamilia.MaxLength = 3
        Me.txtFamilia.Name = "txtFamilia"
        Me.txtFamilia.ReadOnly = True
        Me.txtFamilia.Size = New System.Drawing.Size(217, 26)
        Me.txtFamilia.TabIndex = 12
        '
        'txtFalla
        '
        Me.txtFalla.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtFalla.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFalla.ForeColor = System.Drawing.Color.Red
        Me.txtFalla.Location = New System.Drawing.Point(76, 99)
        Me.txtFalla.MaxLength = 3
        Me.txtFalla.Name = "txtFalla"
        Me.txtFalla.ReadOnly = True
        Me.txtFalla.Size = New System.Drawing.Size(367, 26)
        Me.txtFalla.TabIndex = 11
        '
        'Label23
        '
        Me.Label23.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label23.Location = New System.Drawing.Point(10, 99)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(60, 20)
        Me.Label23.TabIndex = 59
        Me.Label23.Text = "FALLA:"
        '
        'txtUsuarioAsig
        '
        Me.txtUsuarioAsig.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtUsuarioAsig.ForeColor = System.Drawing.Color.Red
        Me.txtUsuarioAsig.Location = New System.Drawing.Point(384, 44)
        Me.txtUsuarioAsig.MaxLength = 3
        Me.txtUsuarioAsig.Name = "txtUsuarioAsig"
        Me.txtUsuarioAsig.ReadOnly = True
        Me.txtUsuarioAsig.Size = New System.Drawing.Size(159, 20)
        Me.txtUsuarioAsig.TabIndex = 7
        '
        'Label6
        '
        Me.Label6.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label6.Location = New System.Drawing.Point(318, 44)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(60, 20)
        Me.Label6.TabIndex = 57
        Me.Label6.Text = "Asign�:"
        '
        'txtDuracion
        '
        Me.txtDuracion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDuracion.ForeColor = System.Drawing.Color.Red
        Me.txtDuracion.Location = New System.Drawing.Point(648, 45)
        Me.txtDuracion.MaxLength = 3
        Me.txtDuracion.Name = "txtDuracion"
        Me.txtDuracion.Size = New System.Drawing.Size(69, 20)
        Me.txtDuracion.TabIndex = 10
        '
        'Label5
        '
        Me.Label5.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label5.Location = New System.Drawing.Point(579, 44)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(63, 20)
        Me.Label5.TabIndex = 55
        Me.Label5.Text = "Duraci�n:"
        '
        'txtPuestoMec
        '
        Me.txtPuestoMec.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPuestoMec.ForeColor = System.Drawing.Color.Red
        Me.txtPuestoMec.Location = New System.Drawing.Point(321, 73)
        Me.txtPuestoMec.MaxLength = 3
        Me.txtPuestoMec.Name = "txtPuestoMec"
        Me.txtPuestoMec.ReadOnly = True
        Me.txtPuestoMec.Size = New System.Drawing.Size(188, 20)
        Me.txtPuestoMec.TabIndex = 9
        '
        'txtNombreMecanico
        '
        Me.txtNombreMecanico.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNombreMecanico.ForeColor = System.Drawing.Color.Red
        Me.txtNombreMecanico.Location = New System.Drawing.Point(76, 73)
        Me.txtNombreMecanico.MaxLength = 3
        Me.txtNombreMecanico.Name = "txtNombreMecanico"
        Me.txtNombreMecanico.ReadOnly = True
        Me.txtNombreMecanico.Size = New System.Drawing.Size(228, 20)
        Me.txtNombreMecanico.TabIndex = 8
        '
        'Label4
        '
        Me.Label4.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label4.Location = New System.Drawing.Point(6, 76)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(60, 20)
        Me.Label4.TabIndex = 52
        Me.Label4.Text = "Mecanico:"
        '
        'dtpFechaAsigHora
        '
        Me.dtpFechaAsigHora.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtpFechaAsigHora.Location = New System.Drawing.Point(634, 19)
        Me.dtpFechaAsigHora.Name = "dtpFechaAsigHora"
        Me.dtpFechaAsigHora.Size = New System.Drawing.Size(83, 20)
        Me.dtpFechaAsigHora.TabIndex = 5
        '
        'dtpFechaAsig
        '
        Me.dtpFechaAsig.Location = New System.Drawing.Point(384, 19)
        Me.dtpFechaAsig.Name = "dtpFechaAsig"
        Me.dtpFechaAsig.Size = New System.Drawing.Size(225, 20)
        Me.dtpFechaAsig.TabIndex = 4
        '
        'Label10
        '
        Me.Label10.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label10.Location = New System.Drawing.Point(287, 22)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(91, 20)
        Me.Label10.TabIndex = 39
        Me.Label10.Text = "Fecha Asignado:"
        '
        'Label9
        '
        Me.Label9.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label9.Location = New System.Drawing.Point(10, 47)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(54, 20)
        Me.Label9.TabIndex = 36
        Me.Label9.Text = "Empresa:"
        '
        'txtNomEmpresa
        '
        Me.txtNomEmpresa.Enabled = False
        Me.txtNomEmpresa.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNomEmpresa.Location = New System.Drawing.Point(76, 47)
        Me.txtNomEmpresa.Name = "txtNomEmpresa"
        Me.txtNomEmpresa.ReadOnly = True
        Me.txtNomEmpresa.Size = New System.Drawing.Size(196, 20)
        Me.txtNomEmpresa.TabIndex = 6
        '
        'cmdBuscaClave
        '
        Me.cmdBuscaClave.Image = CType(resources.GetObject("cmdBuscaClave.Image"), System.Drawing.Image)
        Me.cmdBuscaClave.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdBuscaClave.Location = New System.Drawing.Point(224, 10)
        Me.cmdBuscaClave.Name = "cmdBuscaClave"
        Me.cmdBuscaClave.Size = New System.Drawing.Size(32, 32)
        Me.cmdBuscaClave.TabIndex = 3
        Me.cmdBuscaClave.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'Label25
        '
        Me.Label25.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label25.Location = New System.Drawing.Point(729, 81)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(109, 20)
        Me.Label25.TabIndex = 63
        Me.Label25.Text = "Division"
        '
        'Label24
        '
        Me.Label24.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label24.Location = New System.Drawing.Point(515, 76)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(151, 20)
        Me.Label24.TabIndex = 62
        Me.Label24.Text = "Familia"
        '
        'ToolStripMenu
        '
        Me.ToolStripMenu.AutoSize = False
        Me.ToolStripMenu.BackColor = System.Drawing.Color.Silver
        Me.ToolStripMenu.Dock = System.Windows.Forms.DockStyle.None
        Me.ToolStripMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnMnuOk, Me.btnMnuCancelar, Me.btnMnuSalir})
        Me.ToolStripMenu.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow
        Me.ToolStripMenu.Location = New System.Drawing.Point(3, 0)
        Me.ToolStripMenu.Name = "ToolStripMenu"
        Me.ToolStripMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.ToolStripMenu.Size = New System.Drawing.Size(891, 42)
        Me.ToolStripMenu.TabIndex = 1
        Me.ToolStripMenu.Text = "ToolStrip1"
        '
        'btnMnuOk
        '
        Me.btnMnuOk.ForeColor = System.Drawing.Color.Red
        Me.btnMnuOk.Image = Global.Fletera.My.Resources.Resources.Save1
        Me.btnMnuOk.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuOk.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuOk.Name = "btnMnuOk"
        Me.btnMnuOk.Size = New System.Drawing.Size(56, 39)
        Me.btnMnuOk.Text = "&GRABAR"
        Me.btnMnuOk.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuCancelar
        '
        Me.btnMnuCancelar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuCancelar.Image = Global.Fletera.My.Resources.Resources.signos_f_059
        Me.btnMnuCancelar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuCancelar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuCancelar.Name = "btnMnuCancelar"
        Me.btnMnuCancelar.Size = New System.Drawing.Size(71, 39)
        Me.btnMnuCancelar.Text = "&CANCELAR"
        Me.btnMnuCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuSalir
        '
        Me.btnMnuSalir.ForeColor = System.Drawing.Color.Red
        Me.btnMnuSalir.Image = Global.Fletera.My.Resources.Resources.Salida2
        Me.btnMnuSalir.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuSalir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuSalir.Name = "btnMnuSalir"
        Me.btnMnuSalir.Size = New System.Drawing.Size(41, 39)
        Me.btnMnuSalir.Text = "&SALIR"
        Me.btnMnuSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'MeStatus1
        '
        Me.MeStatus1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.MeStatus1.Location = New System.Drawing.Point(0, 715)
        Me.MeStatus1.Margin = New System.Windows.Forms.Padding(4)
        Me.MeStatus1.Name = "MeStatus1"
        Me.MeStatus1.Size = New System.Drawing.Size(873, 26)
        Me.MeStatus1.TabIndex = 72
        '
        'tabPrinc
        '
        Me.tabPrinc.Controls.Add(Me.TabPage1)
        Me.tabPrinc.Controls.Add(Me.TabPage2)
        Me.tabPrinc.Controls.Add(Me.TabPage3)
        Me.tabPrinc.Location = New System.Drawing.Point(10, 193)
        Me.tabPrinc.Name = "tabPrinc"
        Me.tabPrinc.SelectedIndex = 0
        Me.tabPrinc.Size = New System.Drawing.Size(847, 504)
        Me.tabPrinc.TabIndex = 14
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.GroupBox3)
        Me.TabPage1.Controls.Add(Me.GroupBox2)
        Me.TabPage1.Controls.Add(Me.GroupBox1)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(839, 478)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Generales"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.chkAyu2)
        Me.GroupBox3.Controls.Add(Me.chkAyu1)
        Me.GroupBox3.Controls.Add(Me.cmbPuestoAyu2)
        Me.GroupBox3.Controls.Add(Me.cmbPuestoAyu1)
        Me.GroupBox3.Controls.Add(Me.cmbPuestoResp)
        Me.GroupBox3.Controls.Add(Me.cmbayudante2)
        Me.GroupBox3.Controls.Add(Me.cmbayudante1)
        Me.GroupBox3.Controls.Add(Me.cmbResponsable)
        Me.GroupBox3.Controls.Add(Me.Label31)
        Me.GroupBox3.Controls.Add(Me.Label30)
        Me.GroupBox3.Controls.Add(Me.Label29)
        Me.GroupBox3.Location = New System.Drawing.Point(7, 282)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(719, 163)
        Me.GroupBox3.TabIndex = 77
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Empleados"
        '
        'chkAyu2
        '
        Me.chkAyu2.AutoSize = True
        Me.chkAyu2.Location = New System.Drawing.Point(693, 108)
        Me.chkAyu2.Name = "chkAyu2"
        Me.chkAyu2.Size = New System.Drawing.Size(15, 14)
        Me.chkAyu2.TabIndex = 51
        Me.chkAyu2.UseVisualStyleBackColor = True
        '
        'chkAyu1
        '
        Me.chkAyu1.AutoSize = True
        Me.chkAyu1.Location = New System.Drawing.Point(693, 77)
        Me.chkAyu1.Name = "chkAyu1"
        Me.chkAyu1.Size = New System.Drawing.Size(15, 14)
        Me.chkAyu1.TabIndex = 50
        Me.chkAyu1.UseVisualStyleBackColor = True
        '
        'cmbPuestoAyu2
        '
        Me.cmbPuestoAyu2.AutoCompleteCustomSource.AddRange(New String() {"Regularizaci�n", "Vivienda"})
        Me.cmbPuestoAyu2.FormattingEnabled = True
        Me.cmbPuestoAyu2.Location = New System.Drawing.Point(524, 105)
        Me.cmbPuestoAyu2.Name = "cmbPuestoAyu2"
        Me.cmbPuestoAyu2.Size = New System.Drawing.Size(163, 21)
        Me.cmbPuestoAyu2.TabIndex = 49
        '
        'cmbPuestoAyu1
        '
        Me.cmbPuestoAyu1.AutoCompleteCustomSource.AddRange(New String() {"Regularizaci�n", "Vivienda"})
        Me.cmbPuestoAyu1.FormattingEnabled = True
        Me.cmbPuestoAyu1.Location = New System.Drawing.Point(524, 70)
        Me.cmbPuestoAyu1.Name = "cmbPuestoAyu1"
        Me.cmbPuestoAyu1.Size = New System.Drawing.Size(163, 21)
        Me.cmbPuestoAyu1.TabIndex = 48
        '
        'cmbPuestoResp
        '
        Me.cmbPuestoResp.AutoCompleteCustomSource.AddRange(New String() {"Regularizaci�n", "Vivienda"})
        Me.cmbPuestoResp.FormattingEnabled = True
        Me.cmbPuestoResp.Location = New System.Drawing.Point(524, 29)
        Me.cmbPuestoResp.Name = "cmbPuestoResp"
        Me.cmbPuestoResp.Size = New System.Drawing.Size(163, 21)
        Me.cmbPuestoResp.TabIndex = 47
        '
        'cmbayudante2
        '
        Me.cmbayudante2.AutoCompleteCustomSource.AddRange(New String() {"Regularizaci�n", "Vivienda"})
        Me.cmbayudante2.FormattingEnabled = True
        Me.cmbayudante2.Location = New System.Drawing.Point(86, 105)
        Me.cmbayudante2.Name = "cmbayudante2"
        Me.cmbayudante2.Size = New System.Drawing.Size(426, 21)
        Me.cmbayudante2.TabIndex = 46
        '
        'cmbayudante1
        '
        Me.cmbayudante1.AutoCompleteCustomSource.AddRange(New String() {"Regularizaci�n", "Vivienda"})
        Me.cmbayudante1.FormattingEnabled = True
        Me.cmbayudante1.Location = New System.Drawing.Point(86, 67)
        Me.cmbayudante1.Name = "cmbayudante1"
        Me.cmbayudante1.Size = New System.Drawing.Size(426, 21)
        Me.cmbayudante1.TabIndex = 45
        '
        'cmbResponsable
        '
        Me.cmbResponsable.AutoCompleteCustomSource.AddRange(New String() {"Regularizaci�n", "Vivienda"})
        Me.cmbResponsable.FormattingEnabled = True
        Me.cmbResponsable.Location = New System.Drawing.Point(86, 29)
        Me.cmbResponsable.Name = "cmbResponsable"
        Me.cmbResponsable.Size = New System.Drawing.Size(426, 21)
        Me.cmbResponsable.TabIndex = 44
        '
        'Label31
        '
        Me.Label31.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label31.Location = New System.Drawing.Point(6, 108)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(79, 20)
        Me.Label31.TabIndex = 42
        Me.Label31.Text = "Ayudante 1"
        '
        'Label30
        '
        Me.Label30.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label30.Location = New System.Drawing.Point(6, 70)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(79, 20)
        Me.Label30.TabIndex = 37
        Me.Label30.Text = "Ayudante 1"
        '
        'Label29
        '
        Me.Label29.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label29.Location = New System.Drawing.Point(6, 32)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(79, 20)
        Me.Label29.TabIndex = 32
        Me.Label29.Text = "Responsable:"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.txtUltDivision)
        Me.GroupBox2.Controls.Add(Me.txtUltFamilia)
        Me.GroupBox2.Controls.Add(Me.txtUltFalla)
        Me.GroupBox2.Controls.Add(Me.Label26)
        Me.GroupBox2.Controls.Add(Me.Label27)
        Me.GroupBox2.Controls.Add(Me.Label28)
        Me.GroupBox2.Controls.Add(Me.txtUltimoMecanico)
        Me.GroupBox2.Controls.Add(Me.Label19)
        Me.GroupBox2.Controls.Add(Me.txtUltOrdenTrabajo)
        Me.GroupBox2.Controls.Add(Me.Label18)
        Me.GroupBox2.Controls.Add(Me.txtUltimaOrdenServ)
        Me.GroupBox2.Controls.Add(Me.Label17)
        Me.GroupBox2.Controls.Add(Me.txtMarcaUni)
        Me.GroupBox2.Controls.Add(Me.Label11)
        Me.GroupBox2.Controls.Add(Me.txtNomUnidad)
        Me.GroupBox2.Controls.Add(Me.txtIdUnidad)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Location = New System.Drawing.Point(7, 135)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(719, 141)
        Me.GroupBox2.TabIndex = 76
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Unidad de Transporte"
        '
        'txtUltDivision
        '
        Me.txtUltDivision.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtUltDivision.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtUltDivision.ForeColor = System.Drawing.Color.Red
        Me.txtUltDivision.Location = New System.Drawing.Point(577, 100)
        Me.txtUltDivision.MaxLength = 3
        Me.txtUltDivision.Name = "txtUltDivision"
        Me.txtUltDivision.ReadOnly = True
        Me.txtUltDivision.Size = New System.Drawing.Size(110, 20)
        Me.txtUltDivision.TabIndex = 32
        '
        'txtUltFamilia
        '
        Me.txtUltFamilia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtUltFamilia.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtUltFamilia.ForeColor = System.Drawing.Color.Red
        Me.txtUltFamilia.Location = New System.Drawing.Point(357, 100)
        Me.txtUltFamilia.MaxLength = 3
        Me.txtUltFamilia.Name = "txtUltFamilia"
        Me.txtUltFamilia.ReadOnly = True
        Me.txtUltFamilia.Size = New System.Drawing.Size(217, 20)
        Me.txtUltFamilia.TabIndex = 31
        '
        'txtUltFalla
        '
        Me.txtUltFalla.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtUltFalla.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtUltFalla.ForeColor = System.Drawing.Color.Red
        Me.txtUltFalla.Location = New System.Drawing.Point(46, 100)
        Me.txtUltFalla.MaxLength = 3
        Me.txtUltFalla.Name = "txtUltFalla"
        Me.txtUltFalla.ReadOnly = True
        Me.txtUltFalla.Size = New System.Drawing.Size(306, 20)
        Me.txtUltFalla.TabIndex = 30
        '
        'Label26
        '
        Me.Label26.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label26.Location = New System.Drawing.Point(6, 105)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(60, 20)
        Me.Label26.TabIndex = 65
        Me.Label26.Text = "FALLA:"
        '
        'Label27
        '
        Me.Label27.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label27.Location = New System.Drawing.Point(361, 82)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(151, 20)
        Me.Label27.TabIndex = 68
        Me.Label27.Text = "Familia"
        '
        'Label28
        '
        Me.Label28.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label28.Location = New System.Drawing.Point(585, 82)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(151, 20)
        Me.Label28.TabIndex = 69
        Me.Label28.Text = "Division"
        '
        'txtUltimoMecanico
        '
        Me.txtUltimoMecanico.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtUltimoMecanico.ForeColor = System.Drawing.Color.Red
        Me.txtUltimoMecanico.Location = New System.Drawing.Point(522, 59)
        Me.txtUltimoMecanico.MaxLength = 3
        Me.txtUltimoMecanico.Name = "txtUltimoMecanico"
        Me.txtUltimoMecanico.ReadOnly = True
        Me.txtUltimoMecanico.Size = New System.Drawing.Size(179, 20)
        Me.txtUltimoMecanico.TabIndex = 29
        '
        'Label19
        '
        Me.Label19.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label19.Location = New System.Drawing.Point(463, 62)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(60, 20)
        Me.Label19.TabIndex = 54
        Me.Label19.Text = "Mecanico:"
        '
        'txtUltOrdenTrabajo
        '
        Me.txtUltOrdenTrabajo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtUltOrdenTrabajo.Location = New System.Drawing.Point(328, 59)
        Me.txtUltOrdenTrabajo.MaxLength = 3
        Me.txtUltOrdenTrabajo.Name = "txtUltOrdenTrabajo"
        Me.txtUltOrdenTrabajo.ReadOnly = True
        Me.txtUltOrdenTrabajo.Size = New System.Drawing.Size(69, 20)
        Me.txtUltOrdenTrabajo.TabIndex = 28
        '
        'Label18
        '
        Me.Label18.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label18.Location = New System.Drawing.Point(225, 59)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(94, 20)
        Me.Label18.TabIndex = 47
        Me.Label18.Text = "Ult. O.T."
        '
        'txtUltimaOrdenServ
        '
        Me.txtUltimaOrdenServ.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtUltimaOrdenServ.Location = New System.Drawing.Point(107, 59)
        Me.txtUltimaOrdenServ.MaxLength = 3
        Me.txtUltimaOrdenServ.Name = "txtUltimaOrdenServ"
        Me.txtUltimaOrdenServ.ReadOnly = True
        Me.txtUltimaOrdenServ.Size = New System.Drawing.Size(69, 20)
        Me.txtUltimaOrdenServ.TabIndex = 27
        '
        'Label17
        '
        Me.Label17.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label17.Location = New System.Drawing.Point(4, 59)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(94, 20)
        Me.Label17.TabIndex = 45
        Me.Label17.Text = "Ult. O.S."
        '
        'txtMarcaUni
        '
        Me.txtMarcaUni.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMarcaUni.Location = New System.Drawing.Point(490, 29)
        Me.txtMarcaUni.MaxLength = 50
        Me.txtMarcaUni.Name = "txtMarcaUni"
        Me.txtMarcaUni.ReadOnly = True
        Me.txtMarcaUni.Size = New System.Drawing.Size(211, 20)
        Me.txtMarcaUni.TabIndex = 26
        '
        'Label11
        '
        Me.Label11.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label11.Location = New System.Drawing.Point(434, 32)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(50, 20)
        Me.Label11.TabIndex = 43
        Me.Label11.Text = "Marca:"
        '
        'txtNomUnidad
        '
        Me.txtNomUnidad.Enabled = False
        Me.txtNomUnidad.Location = New System.Drawing.Point(222, 29)
        Me.txtNomUnidad.Name = "txtNomUnidad"
        Me.txtNomUnidad.ReadOnly = True
        Me.txtNomUnidad.Size = New System.Drawing.Size(203, 20)
        Me.txtNomUnidad.TabIndex = 25
        '
        'txtIdUnidad
        '
        Me.txtIdUnidad.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtIdUnidad.Location = New System.Drawing.Point(107, 29)
        Me.txtIdUnidad.MaxLength = 3
        Me.txtIdUnidad.Name = "txtIdUnidad"
        Me.txtIdUnidad.ReadOnly = True
        Me.txtIdUnidad.Size = New System.Drawing.Size(69, 20)
        Me.txtIdUnidad.TabIndex = 24
        '
        'Label3
        '
        Me.Label3.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label3.Location = New System.Drawing.Point(4, 29)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(94, 20)
        Me.Label3.TabIndex = 20
        Me.Label3.Text = "Unidad:"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtKilometraje)
        Me.GroupBox1.Controls.Add(Me.Label16)
        Me.GroupBox1.Controls.Add(Me.txtAutorizo)
        Me.GroupBox1.Controls.Add(Me.Label15)
        Me.GroupBox1.Controls.Add(Me.txtEntrego)
        Me.GroupBox1.Controls.Add(Me.txtRecepciono)
        Me.GroupBox1.Controls.Add(Me.dtpFechaOrden)
        Me.GroupBox1.Controls.Add(Me.txtOrdenServ)
        Me.GroupBox1.Controls.Add(Me.Label14)
        Me.GroupBox1.Controls.Add(Me.dtpFechaOrdenhr)
        Me.GroupBox1.Controls.Add(Me.txtEstatusOrden)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.txtTipoOrden)
        Me.GroupBox1.Controls.Add(Me.label2)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Location = New System.Drawing.Point(7, 6)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(721, 123)
        Me.GroupBox1.TabIndex = 75
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Orden de SERVICIO"
        '
        'txtKilometraje
        '
        Me.txtKilometraje.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtKilometraje.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtKilometraje.Location = New System.Drawing.Point(544, 55)
        Me.txtKilometraje.MaxLength = 50
        Me.txtKilometraje.Name = "txtKilometraje"
        Me.txtKilometraje.ReadOnly = True
        Me.txtKilometraje.Size = New System.Drawing.Size(157, 20)
        Me.txtKilometraje.TabIndex = 20
        Me.txtKilometraje.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label16
        '
        Me.Label16.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label16.Location = New System.Drawing.Point(478, 55)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(68, 20)
        Me.Label16.TabIndex = 65
        Me.Label16.Text = "Kilometraje:"
        '
        'txtAutorizo
        '
        Me.txtAutorizo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtAutorizo.ForeColor = System.Drawing.Color.Red
        Me.txtAutorizo.Location = New System.Drawing.Point(544, 85)
        Me.txtAutorizo.MaxLength = 3
        Me.txtAutorizo.Name = "txtAutorizo"
        Me.txtAutorizo.ReadOnly = True
        Me.txtAutorizo.Size = New System.Drawing.Size(159, 20)
        Me.txtAutorizo.TabIndex = 23
        '
        'Label15
        '
        Me.Label15.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label15.Location = New System.Drawing.Point(478, 85)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(60, 20)
        Me.Label15.TabIndex = 63
        Me.Label15.Text = "Autorizo:"
        '
        'txtEntrego
        '
        Me.txtEntrego.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtEntrego.ForeColor = System.Drawing.Color.Red
        Me.txtEntrego.Location = New System.Drawing.Point(307, 88)
        Me.txtEntrego.MaxLength = 3
        Me.txtEntrego.Name = "txtEntrego"
        Me.txtEntrego.ReadOnly = True
        Me.txtEntrego.Size = New System.Drawing.Size(159, 20)
        Me.txtEntrego.TabIndex = 22
        '
        'txtRecepciono
        '
        Me.txtRecepciono.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRecepciono.ForeColor = System.Drawing.Color.Red
        Me.txtRecepciono.Location = New System.Drawing.Point(72, 88)
        Me.txtRecepciono.MaxLength = 3
        Me.txtRecepciono.Name = "txtRecepciono"
        Me.txtRecepciono.ReadOnly = True
        Me.txtRecepciono.Size = New System.Drawing.Size(159, 20)
        Me.txtRecepciono.TabIndex = 21
        '
        'dtpFechaOrden
        '
        Me.dtpFechaOrden.Location = New System.Drawing.Point(384, 19)
        Me.dtpFechaOrden.Name = "dtpFechaOrden"
        Me.dtpFechaOrden.Size = New System.Drawing.Size(225, 20)
        Me.dtpFechaOrden.TabIndex = 16
        '
        'txtOrdenServ
        '
        Me.txtOrdenServ.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtOrdenServ.ForeColor = System.Drawing.Color.Red
        Me.txtOrdenServ.Location = New System.Drawing.Point(72, 25)
        Me.txtOrdenServ.MaxLength = 3
        Me.txtOrdenServ.Name = "txtOrdenServ"
        Me.txtOrdenServ.ReadOnly = True
        Me.txtOrdenServ.Size = New System.Drawing.Size(69, 20)
        Me.txtOrdenServ.TabIndex = 15
        Me.txtOrdenServ.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label14
        '
        Me.Label14.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label14.Location = New System.Drawing.Point(287, 22)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(91, 20)
        Me.Label14.TabIndex = 48
        Me.Label14.Text = "Fecha Orden:"
        '
        'dtpFechaOrdenhr
        '
        Me.dtpFechaOrdenhr.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtpFechaOrdenhr.Location = New System.Drawing.Point(620, 19)
        Me.dtpFechaOrdenhr.Name = "dtpFechaOrdenhr"
        Me.dtpFechaOrdenhr.Size = New System.Drawing.Size(83, 20)
        Me.dtpFechaOrdenhr.TabIndex = 17
        '
        'txtEstatusOrden
        '
        Me.txtEstatusOrden.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtEstatusOrden.Location = New System.Drawing.Point(307, 55)
        Me.txtEstatusOrden.MaxLength = 50
        Me.txtEstatusOrden.Name = "txtEstatusOrden"
        Me.txtEstatusOrden.ReadOnly = True
        Me.txtEstatusOrden.Size = New System.Drawing.Size(101, 20)
        Me.txtEstatusOrden.TabIndex = 19
        '
        'Label12
        '
        Me.Label12.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label12.Location = New System.Drawing.Point(263, 55)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(56, 20)
        Me.Label12.TabIndex = 45
        Me.Label12.Text = "Estatus:"
        '
        'txtTipoOrden
        '
        Me.txtTipoOrden.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTipoOrden.Location = New System.Drawing.Point(130, 55)
        Me.txtTipoOrden.MaxLength = 50
        Me.txtTipoOrden.Name = "txtTipoOrden"
        Me.txtTipoOrden.ReadOnly = True
        Me.txtTipoOrden.Size = New System.Drawing.Size(101, 20)
        Me.txtTipoOrden.TabIndex = 18
        '
        'label2
        '
        Me.label2.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.label2.Location = New System.Drawing.Point(6, 55)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(91, 20)
        Me.label2.TabIndex = 16
        Me.label2.Text = "Tipo de Orden"
        '
        'Label8
        '
        Me.Label8.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label8.Location = New System.Drawing.Point(259, 88)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(60, 20)
        Me.Label8.TabIndex = 61
        Me.Label8.Text = "Entrego:"
        '
        'Label7
        '
        Me.Label7.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label7.Location = New System.Drawing.Point(6, 88)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(69, 20)
        Me.Label7.TabIndex = 59
        Me.Label7.Text = "Recepciono:"
        '
        'Label13
        '
        Me.Label13.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label13.Location = New System.Drawing.Point(6, 25)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(126, 20)
        Me.Label13.TabIndex = 47
        Me.Label13.Text = "Folio O.S.:"
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.grdRefaccionesHis)
        Me.TabPage2.Controls.Add(Me.grdOrdenTrabajoHis)
        Me.TabPage2.Controls.Add(Me.grdOrdenServHis)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(839, 478)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Historial de Servicios"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'grdRefaccionesHis
        '
        Me.grdRefaccionesHis.AllowUserToAddRows = False
        Me.grdRefaccionesHis.AllowUserToResizeColumns = False
        Me.grdRefaccionesHis.AllowUserToResizeRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Yellow
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black
        Me.grdRefaccionesHis.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.grdRefaccionesHis.BackgroundColor = System.Drawing.Color.White
        Me.grdRefaccionesHis.ColumnHeadersHeight = 34
        Me.grdRefaccionesHis.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.grdRefaccionesHis.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn16, Me.DataGridViewTextBoxColumn17, Me.DataGridViewTextBoxColumn18, Me.DataGridViewTextBoxColumn19, Me.DataGridViewTextBoxColumn20, Me.DataGridViewTextBoxColumn21, Me.DataGridViewCheckBoxColumn2, Me.Column3})
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.Yellow
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.grdRefaccionesHis.DefaultCellStyle = DataGridViewCellStyle5
        Me.grdRefaccionesHis.Location = New System.Drawing.Point(6, 323)
        Me.grdRefaccionesHis.Name = "grdRefaccionesHis"
        Me.grdRefaccionesHis.RowHeadersWidth = 26
        DataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.Yellow
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black
        Me.grdRefaccionesHis.RowsDefaultCellStyle = DataGridViewCellStyle6
        Me.grdRefaccionesHis.Size = New System.Drawing.Size(816, 149)
        Me.grdRefaccionesHis.TabIndex = 35
        '
        'DataGridViewTextBoxColumn16
        '
        Me.DataGridViewTextBoxColumn16.HeaderText = "#"
        Me.DataGridViewTextBoxColumn16.Name = "DataGridViewTextBoxColumn16"
        Me.DataGridViewTextBoxColumn16.Width = 30
        '
        'DataGridViewTextBoxColumn17
        '
        Me.DataGridViewTextBoxColumn17.HeaderText = "Producto"
        Me.DataGridViewTextBoxColumn17.Name = "DataGridViewTextBoxColumn17"
        '
        'DataGridViewTextBoxColumn18
        '
        Me.DataGridViewTextBoxColumn18.HeaderText = "Nombre"
        Me.DataGridViewTextBoxColumn18.Name = "DataGridViewTextBoxColumn18"
        Me.DataGridViewTextBoxColumn18.Width = 250
        '
        'DataGridViewTextBoxColumn19
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.DataGridViewTextBoxColumn19.DefaultCellStyle = DataGridViewCellStyle2
        Me.DataGridViewTextBoxColumn19.HeaderText = "Existencia"
        Me.DataGridViewTextBoxColumn19.Name = "DataGridViewTextBoxColumn19"
        Me.DataGridViewTextBoxColumn19.Width = 60
        '
        'DataGridViewTextBoxColumn20
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.DataGridViewTextBoxColumn20.DefaultCellStyle = DataGridViewCellStyle3
        Me.DataGridViewTextBoxColumn20.HeaderText = "Cantidad"
        Me.DataGridViewTextBoxColumn20.Name = "DataGridViewTextBoxColumn20"
        Me.DataGridViewTextBoxColumn20.Width = 60
        '
        'DataGridViewTextBoxColumn21
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.DataGridViewTextBoxColumn21.DefaultCellStyle = DataGridViewCellStyle4
        Me.DataGridViewTextBoxColumn21.HeaderText = "Unidad"
        Me.DataGridViewTextBoxColumn21.Name = "DataGridViewTextBoxColumn21"
        Me.DataGridViewTextBoxColumn21.Width = 60
        '
        'DataGridViewCheckBoxColumn2
        '
        Me.DataGridViewCheckBoxColumn2.HeaderText = "Orden Compra"
        Me.DataGridViewCheckBoxColumn2.Name = "DataGridViewCheckBoxColumn2"
        Me.DataGridViewCheckBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewCheckBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'Column3
        '
        Me.Column3.HeaderText = "Entrada Almacen"
        Me.Column3.Name = "Column3"
        '
        'grdOrdenTrabajoHis
        '
        Me.grdOrdenTrabajoHis.AllowUserToAddRows = False
        Me.grdOrdenTrabajoHis.AllowUserToResizeColumns = False
        Me.grdOrdenTrabajoHis.AllowUserToResizeRows = False
        DataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        DataGridViewCellStyle7.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.Yellow
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black
        Me.grdOrdenTrabajoHis.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle7
        Me.grdOrdenTrabajoHis.BackgroundColor = System.Drawing.Color.White
        Me.grdOrdenTrabajoHis.ColumnHeadersHeight = 34
        Me.grdOrdenTrabajoHis.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.grdOrdenTrabajoHis.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.hotidOrdenTrabajo, Me.hotIdOrdenSer, Me.hotFechaFin, Me.hotAsigno, Me.hotMecanico, Me.hotDuracion})
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle11.SelectionBackColor = System.Drawing.Color.Yellow
        DataGridViewCellStyle11.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.grdOrdenTrabajoHis.DefaultCellStyle = DataGridViewCellStyle11
        Me.grdOrdenTrabajoHis.Location = New System.Drawing.Point(6, 171)
        Me.grdOrdenTrabajoHis.Name = "grdOrdenTrabajoHis"
        Me.grdOrdenTrabajoHis.RowHeadersWidth = 26
        DataGridViewCellStyle12.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle12.SelectionBackColor = System.Drawing.Color.Yellow
        DataGridViewCellStyle12.SelectionForeColor = System.Drawing.Color.Black
        Me.grdOrdenTrabajoHis.RowsDefaultCellStyle = DataGridViewCellStyle12
        Me.grdOrdenTrabajoHis.Size = New System.Drawing.Size(816, 146)
        Me.grdOrdenTrabajoHis.TabIndex = 34
        '
        'hotidOrdenTrabajo
        '
        Me.hotidOrdenTrabajo.HeaderText = "Orden Trabajo"
        Me.hotidOrdenTrabajo.Name = "hotidOrdenTrabajo"
        '
        'hotIdOrdenSer
        '
        Me.hotIdOrdenSer.HeaderText = "idOrdenSer"
        Me.hotIdOrdenSer.Name = "hotIdOrdenSer"
        Me.hotIdOrdenSer.Visible = False
        '
        'hotFechaFin
        '
        Me.hotFechaFin.HeaderText = "Fecha Fin"
        Me.hotFechaFin.Name = "hotFechaFin"
        Me.hotFechaFin.Width = 200
        '
        'hotAsigno
        '
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.hotAsigno.DefaultCellStyle = DataGridViewCellStyle8
        Me.hotAsigno.HeaderText = "Asign�"
        Me.hotAsigno.Name = "hotAsigno"
        Me.hotAsigno.Width = 120
        '
        'hotMecanico
        '
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.hotMecanico.DefaultCellStyle = DataGridViewCellStyle9
        Me.hotMecanico.HeaderText = "M�canico"
        Me.hotMecanico.Name = "hotMecanico"
        Me.hotMecanico.Width = 250
        '
        'hotDuracion
        '
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.hotDuracion.DefaultCellStyle = DataGridViewCellStyle10
        Me.hotDuracion.HeaderText = "Duraci�n"
        Me.hotDuracion.Name = "hotDuracion"
        Me.hotDuracion.Width = 60
        '
        'grdOrdenServHis
        '
        Me.grdOrdenServHis.AllowUserToAddRows = False
        Me.grdOrdenServHis.AllowUserToResizeColumns = False
        Me.grdOrdenServHis.AllowUserToResizeRows = False
        DataGridViewCellStyle13.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        DataGridViewCellStyle13.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle13.SelectionBackColor = System.Drawing.Color.Yellow
        DataGridViewCellStyle13.SelectionForeColor = System.Drawing.Color.Black
        Me.grdOrdenServHis.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle13
        Me.grdOrdenServHis.BackgroundColor = System.Drawing.Color.White
        Me.grdOrdenServHis.ColumnHeadersHeight = 34
        Me.grdOrdenServHis.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.grdOrdenServHis.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.hosConsecutivo, Me.hosIdServicio, Me.hosFechaRecepcion, Me.hosTipoOrden, Me.hosKm, Me.hosAutorizo, Me.hosEntrego, Me.hosRecepciono, Me.hosFinalizo})
        DataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle17.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle17.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle17.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle17.SelectionBackColor = System.Drawing.Color.Yellow
        DataGridViewCellStyle17.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.grdOrdenServHis.DefaultCellStyle = DataGridViewCellStyle17
        Me.grdOrdenServHis.Location = New System.Drawing.Point(6, 16)
        Me.grdOrdenServHis.Name = "grdOrdenServHis"
        Me.grdOrdenServHis.RowHeadersWidth = 26
        DataGridViewCellStyle18.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle18.SelectionBackColor = System.Drawing.Color.Yellow
        DataGridViewCellStyle18.SelectionForeColor = System.Drawing.Color.Black
        Me.grdOrdenServHis.RowsDefaultCellStyle = DataGridViewCellStyle18
        Me.grdOrdenServHis.Size = New System.Drawing.Size(816, 149)
        Me.grdOrdenServHis.TabIndex = 33
        '
        'hosConsecutivo
        '
        Me.hosConsecutivo.HeaderText = "#"
        Me.hosConsecutivo.Name = "hosConsecutivo"
        Me.hosConsecutivo.Width = 30
        '
        'hosIdServicio
        '
        Me.hosIdServicio.HeaderText = "Orden Servicio"
        Me.hosIdServicio.Name = "hosIdServicio"
        '
        'hosFechaRecepcion
        '
        Me.hosFechaRecepcion.HeaderText = "Fecha"
        Me.hosFechaRecepcion.Name = "hosFechaRecepcion"
        Me.hosFechaRecepcion.Width = 150
        '
        'hosTipoOrden
        '
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.hosTipoOrden.DefaultCellStyle = DataGridViewCellStyle14
        Me.hosTipoOrden.HeaderText = "Tipo"
        Me.hosTipoOrden.Name = "hosTipoOrden"
        Me.hosTipoOrden.Width = 60
        '
        'hosKm
        '
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.hosKm.DefaultCellStyle = DataGridViewCellStyle15
        Me.hosKm.HeaderText = "Kilometraje"
        Me.hosKm.Name = "hosKm"
        Me.hosKm.Width = 60
        '
        'hosAutorizo
        '
        DataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.hosAutorizo.DefaultCellStyle = DataGridViewCellStyle16
        Me.hosAutorizo.HeaderText = "Autoriz�"
        Me.hosAutorizo.Name = "hosAutorizo"
        Me.hosAutorizo.Width = 60
        '
        'hosEntrego
        '
        Me.hosEntrego.HeaderText = "Entreg�"
        Me.hosEntrego.Name = "hosEntrego"
        Me.hosEntrego.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.hosEntrego.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'hosRecepciono
        '
        Me.hosRecepciono.HeaderText = "Recepcion�"
        Me.hosRecepciono.Name = "hosRecepciono"
        '
        'hosFinalizo
        '
        Me.hosFinalizo.HeaderText = "Finaliz�"
        Me.hosFinalizo.Name = "hosFinalizo"
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.grdProductosOT)
        Me.TabPage3.Controls.Add(Me.txtCantidad)
        Me.TabPage3.Controls.Add(Me.Label22)
        Me.TabPage3.Controls.Add(Me.txtNomUniProd)
        Me.TabPage3.Controls.Add(Me.txtIdUnidadProd)
        Me.TabPage3.Controls.Add(Me.Label21)
        Me.TabPage3.Controls.Add(Me.txtNomProducto)
        Me.TabPage3.Controls.Add(Me.txtIdProducto)
        Me.TabPage3.Controls.Add(Me.Label20)
        Me.TabPage3.Controls.Add(Me.cmdBuscaUniProd)
        Me.TabPage3.Controls.Add(Me.cmdBuscaProd)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(839, 478)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Refacciones"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'grdProductosOT
        '
        Me.grdProductosOT.AllowUserToAddRows = False
        Me.grdProductosOT.AllowUserToResizeColumns = False
        Me.grdProductosOT.AllowUserToResizeRows = False
        DataGridViewCellStyle19.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        DataGridViewCellStyle19.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle19.SelectionBackColor = System.Drawing.Color.Yellow
        DataGridViewCellStyle19.SelectionForeColor = System.Drawing.Color.Black
        Me.grdProductosOT.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle19
        Me.grdProductosOT.BackgroundColor = System.Drawing.Color.White
        Me.grdProductosOT.ColumnHeadersHeight = 34
        Me.grdProductosOT.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.grdProductosOT.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.grefConsecutivo, Me.grefCCODIGOP01_PRO, Me.grefCIDPRODUCTO, Me.grefCNOMBREP01, Me.grefExistencia, Me.grefCantidad, Me.grefCCLAVEINT, Me.grefBComprar, Me.grefCOSTO, Me.grefCIDUNIDADBASE, Me.grefCantCompra, Me.grefCantSalida})
        DataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle23.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle23.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle23.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle23.SelectionBackColor = System.Drawing.Color.Yellow
        DataGridViewCellStyle23.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.grdProductosOT.DefaultCellStyle = DataGridViewCellStyle23
        Me.grdProductosOT.Location = New System.Drawing.Point(7, 133)
        Me.grdProductosOT.Name = "grdProductosOT"
        Me.grdProductosOT.RowHeadersWidth = 26
        DataGridViewCellStyle24.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle24.SelectionBackColor = System.Drawing.Color.Yellow
        DataGridViewCellStyle24.SelectionForeColor = System.Drawing.Color.Black
        Me.grdProductosOT.RowsDefaultCellStyle = DataGridViewCellStyle24
        Me.grdProductosOT.Size = New System.Drawing.Size(816, 280)
        Me.grdProductosOT.TabIndex = 98
        '
        'grefConsecutivo
        '
        Me.grefConsecutivo.HeaderText = "#"
        Me.grefConsecutivo.Name = "grefConsecutivo"
        Me.grefConsecutivo.Width = 30
        '
        'grefCCODIGOP01_PRO
        '
        Me.grefCCODIGOP01_PRO.HeaderText = "Producto"
        Me.grefCCODIGOP01_PRO.Name = "grefCCODIGOP01_PRO"
        '
        'grefCIDPRODUCTO
        '
        Me.grefCIDPRODUCTO.HeaderText = "CIDPRODUCTO"
        Me.grefCIDPRODUCTO.Name = "grefCIDPRODUCTO"
        Me.grefCIDPRODUCTO.Visible = False
        '
        'grefCNOMBREP01
        '
        Me.grefCNOMBREP01.HeaderText = "Nombre"
        Me.grefCNOMBREP01.Name = "grefCNOMBREP01"
        Me.grefCNOMBREP01.Width = 350
        '
        'grefExistencia
        '
        DataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.grefExistencia.DefaultCellStyle = DataGridViewCellStyle20
        Me.grefExistencia.HeaderText = "Existencia"
        Me.grefExistencia.Name = "grefExistencia"
        Me.grefExistencia.Width = 60
        '
        'grefCantidad
        '
        DataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.grefCantidad.DefaultCellStyle = DataGridViewCellStyle21
        Me.grefCantidad.HeaderText = "Cantidad"
        Me.grefCantidad.Name = "grefCantidad"
        Me.grefCantidad.Width = 60
        '
        'grefCCLAVEINT
        '
        DataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.grefCCLAVEINT.DefaultCellStyle = DataGridViewCellStyle22
        Me.grefCCLAVEINT.HeaderText = "Unidad"
        Me.grefCCLAVEINT.Name = "grefCCLAVEINT"
        Me.grefCCLAVEINT.Width = 60
        '
        'grefBComprar
        '
        Me.grefBComprar.HeaderText = "Comprar"
        Me.grefBComprar.Name = "grefBComprar"
        Me.grefBComprar.ThreeState = True
        '
        'grefCOSTO
        '
        Me.grefCOSTO.HeaderText = "Costo"
        Me.grefCOSTO.Name = "grefCOSTO"
        Me.grefCOSTO.Visible = False
        '
        'grefCIDUNIDADBASE
        '
        Me.grefCIDUNIDADBASE.HeaderText = "CIDUNIDADBASE"
        Me.grefCIDUNIDADBASE.Name = "grefCIDUNIDADBASE"
        Me.grefCIDUNIDADBASE.Visible = False
        '
        'grefCantCompra
        '
        Me.grefCantCompra.HeaderText = "CantCompra"
        Me.grefCantCompra.Name = "grefCantCompra"
        Me.grefCantCompra.Visible = False
        '
        'grefCantSalida
        '
        Me.grefCantSalida.HeaderText = "CantSalida"
        Me.grefCantSalida.Name = "grefCantSalida"
        Me.grefCantSalida.Visible = False
        '
        'txtCantidad
        '
        Me.txtCantidad.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCantidad.ForeColor = System.Drawing.Color.Black
        Me.txtCantidad.Location = New System.Drawing.Point(57, 92)
        Me.txtCantidad.MaxLength = 3
        Me.txtCantidad.Name = "txtCantidad"
        Me.txtCantidad.Size = New System.Drawing.Size(69, 20)
        Me.txtCantidad.TabIndex = 42
        Me.txtCantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label22
        '
        Me.Label22.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label22.Location = New System.Drawing.Point(4, 97)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(126, 20)
        Me.Label22.TabIndex = 58
        Me.Label22.Text = "Cantidad:"
        '
        'txtNomUniProd
        '
        Me.txtNomUniProd.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNomUniProd.ForeColor = System.Drawing.Color.Red
        Me.txtNomUniProd.Location = New System.Drawing.Point(186, 54)
        Me.txtNomUniProd.MaxLength = 3
        Me.txtNomUniProd.Name = "txtNomUniProd"
        Me.txtNomUniProd.ReadOnly = True
        Me.txtNomUniProd.Size = New System.Drawing.Size(528, 20)
        Me.txtNomUniProd.TabIndex = 41
        '
        'txtIdUnidadProd
        '
        Me.txtIdUnidadProd.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtIdUnidadProd.ForeColor = System.Drawing.Color.Black
        Me.txtIdUnidadProd.Location = New System.Drawing.Point(57, 54)
        Me.txtIdUnidadProd.MaxLength = 3
        Me.txtIdUnidadProd.Name = "txtIdUnidadProd"
        Me.txtIdUnidadProd.Size = New System.Drawing.Size(69, 20)
        Me.txtIdUnidadProd.TabIndex = 39
        Me.txtIdUnidadProd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label21
        '
        Me.Label21.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label21.Location = New System.Drawing.Point(4, 59)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(126, 20)
        Me.Label21.TabIndex = 55
        Me.Label21.Text = "Unidad:"
        '
        'txtNomProducto
        '
        Me.txtNomProducto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNomProducto.ForeColor = System.Drawing.Color.Red
        Me.txtNomProducto.Location = New System.Drawing.Point(186, 14)
        Me.txtNomProducto.MaxLength = 3
        Me.txtNomProducto.Name = "txtNomProducto"
        Me.txtNomProducto.ReadOnly = True
        Me.txtNomProducto.Size = New System.Drawing.Size(528, 20)
        Me.txtNomProducto.TabIndex = 38
        '
        'txtIdProducto
        '
        Me.txtIdProducto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtIdProducto.ForeColor = System.Drawing.Color.Black
        Me.txtIdProducto.Location = New System.Drawing.Point(57, 14)
        Me.txtIdProducto.MaxLength = 3
        Me.txtIdProducto.Name = "txtIdProducto"
        Me.txtIdProducto.Size = New System.Drawing.Size(69, 20)
        Me.txtIdProducto.TabIndex = 36
        Me.txtIdProducto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label20
        '
        Me.Label20.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label20.Location = New System.Drawing.Point(4, 19)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(126, 20)
        Me.Label20.TabIndex = 17
        Me.Label20.Text = "Producto:"
        '
        'cmdBuscaUniProd
        '
        Me.cmdBuscaUniProd.Image = CType(resources.GetObject("cmdBuscaUniProd.Image"), System.Drawing.Image)
        Me.cmdBuscaUniProd.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdBuscaUniProd.Location = New System.Drawing.Point(136, 44)
        Me.cmdBuscaUniProd.Name = "cmdBuscaUniProd"
        Me.cmdBuscaUniProd.Size = New System.Drawing.Size(32, 32)
        Me.cmdBuscaUniProd.TabIndex = 40
        Me.cmdBuscaUniProd.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cmdBuscaProd
        '
        Me.cmdBuscaProd.Image = CType(resources.GetObject("cmdBuscaProd.Image"), System.Drawing.Image)
        Me.cmdBuscaProd.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdBuscaProd.Location = New System.Drawing.Point(136, 4)
        Me.cmdBuscaProd.Name = "cmdBuscaProd"
        Me.cmdBuscaProd.Size = New System.Drawing.Size(32, 32)
        Me.cmdBuscaProd.TabIndex = 37
        Me.cmdBuscaProd.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'frmDiagnosticoTaller
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(873, 741)
        Me.Controls.Add(Me.tabPrinc)
        Me.Controls.Add(Me.MeStatus1)
        Me.Controls.Add(Me.ToolStripMenu)
        Me.Controls.Add(Me.GrupoCaptura)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmDiagnosticoTaller"
        Me.Text = "Diagnostico de Ordenes de Trabajo"
        Me.GrupoCaptura.ResumeLayout(False)
        Me.GrupoCaptura.PerformLayout()
        Me.ToolStripMenu.ResumeLayout(False)
        Me.ToolStripMenu.PerformLayout()
        Me.tabPrinc.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        CType(Me.grdRefaccionesHis, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdOrdenTrabajoHis, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdOrdenServHis, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        CType(Me.grdProductosOT, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Sub New(ByVal vTag As String, Optional ByVal vNoServ As Integer = 0)
        InitializeComponent()
        _Tag = vTag
        _NoServ = vNoServ
    End Sub
    Public Sub New(con As String, nomUsuario As String, cveEmpresa As Integer, IdUsuario As Integer)
        _con = con
        BD = New CapaDatos.UtilSQL(con, "Juan")
        InitializeComponent()
        _Usuario = nomUsuario
        _cveEmpresa = cveEmpresa
        _IdUsuario = IdUsuario
        Inicio.CONSTR = _con

    End Sub

    'Public Sub New(ByVal vNoServ As Integer)
    '    InitializeComponent()
    '    _NoServ = vNoServ
    'End Sub

    Private Sub frmDiagnosticoTaller_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'LimpiaCampos()

        If Inicio.CONSTR = "" Then
            Inicio.CONSTR = _con
        End If

        Salida = True
        LlenaCombos()
        Cancelar()
        CreaTablaProdRefacciones()
        Salida = False
        If _NoServ > 0 Then
            txtOrdenTrabajo.Text = _NoServ
            txtOrdenTrabajo_Leave(sender, e)
        End If


    End Sub

    Private Sub LlenaCombos()
        Try
            Personal = New PersonalClass(0)

            MyTablaComboResp = Personal.TablaPersonalCombo(" per.iddepto in (6) and per.idpuesto in (10,11,12,13,14,15,16,17,20,21,22,23,24,25) ")
            If Not MyTablaComboResp Is Nothing Then
                If MyTablaComboResp.Rows.Count > 0 Then
                    cmbResponsable.DataSource = MyTablaComboResp
                    cmbResponsable.ValueMember = "idpersonal"
                    cmbResponsable.DisplayMember = "nombrecompleto"

                    cmbPuestoResp.DataSource = MyTablaComboResp
                    cmbPuestoResp.ValueMember = "idpuesto"
                    cmbPuestoResp.DisplayMember = "Nompuesto"

                    'DsComboMovimiento = DsCombo
                End If
            End If

            Personal = New PersonalClass(0)
            MyTablaComboAyu1 = Personal.TablaPersonalCombo(" per.iddepto in (6) and per.idpuesto in (10,11,12,13,14,15,16,17,20,21,22,23,24,25) ")
            If Not MyTablaComboAyu1 Is Nothing Then
                If MyTablaComboAyu1.Rows.Count > 0 Then
                    cmbayudante1.DataSource = MyTablaComboAyu1
                    cmbayudante1.ValueMember = "idpersonal"
                    cmbayudante1.DisplayMember = "nombrecompleto"

                    cmbPuestoAyu1.DataSource = MyTablaComboAyu1
                    cmbPuestoAyu1.ValueMember = "idpuesto"
                    cmbPuestoAyu1.DisplayMember = "Nompuesto"
                End If
            End If

            Personal = New PersonalClass(0)
            MyTablaComboAyu2 = Personal.TablaPersonalCombo(" per.iddepto in (6) and per.idpuesto in (10,11,12,13,14,15,16,17,20,21,22,23,24,25) ")
            If Not MyTablaComboAyu2 Is Nothing Then
                If MyTablaComboAyu2.Rows.Count > 0 Then
                    cmbayudante2.DataSource = MyTablaComboAyu2
                    cmbayudante2.ValueMember = "idpersonal"
                    cmbayudante2.DisplayMember = "nombrecompleto"

                    cmbPuestoAyu2.DataSource = MyTablaComboAyu2
                    cmbPuestoAyu2.ValueMember = "idpuesto"
                    cmbPuestoAyu2.DisplayMember = "Nompuesto"
                End If
            End If

        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try
    End Sub

    Private Sub CreaTablaProdRefacciones()
        Try
            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Int64")
            myDataColumn.ColumnName = "trefCIDPRODUCTO"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRefac.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Int64")
            myDataColumn.ColumnName = "trefCIDUNIDADBASE"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRefac.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.String")
            myDataColumn.ColumnName = "trefCCODIGOP01_PRO"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRefac.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.String")
            myDataColumn.ColumnName = "trefCNOMBREP01"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRefac.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Double")
            myDataColumn.ColumnName = "trefCOSTO"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRefac.Columns.Add(myDataColumn)


            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.String")
            myDataColumn.ColumnName = "trefCCLAVEINT"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRefac.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Double")
            myDataColumn.ColumnName = "trefExistencia"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRefac.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Double")
            myDataColumn.ColumnName = "trefCantidad"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRefac.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Boolean")
            myDataColumn.ColumnName = "trefBComprar"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRefac.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Double")
            myDataColumn.ColumnName = "trefCantCompra"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRefac.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Double")
            myDataColumn.ColumnName = "trefCantSalida"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            MyTablaRefac.Columns.Add(myDataColumn)

        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try

    End Sub

    Private Sub LimpiaCampos()
        txtOrdenTrabajo.Text = ""
        dtpFechaAsig.Value = Now
        dtpFechaAsigHora.Value = Now
        txtNomEmpresa.Text = ""
        txtUsuarioAsig.Text = ""
        txtNombreMecanico.Text = ""
        txtPuestoMec.Text = ""
        txtDuracion.Text = ""

        txtFalla.Text = ""
        txtFamilia.Text = ""
        txtDivision.Text = ""

        txtOrdenServ.Text = ""
        dtpFechaOrden.Value = Now
        dtpFechaOrdenhr.Value = Now
        txtTipoOrden.Text = ""
        txtEstatusOrden.Text = ""
        txtKilometraje.Text = ""
        txtRecepciono.Text = ""
        txtEntrego.Text = ""
        txtAutorizo.Text = ""

        txtIdUnidad.Text = ""
        txtNomUnidad.Text = ""
        txtMarcaUni.Text = ""
        txtUltimaOrdenServ.Text = ""
        txtUltOrdenTrabajo.Text = ""
        txtUltimoMecanico.Text = ""
        txtUltFalla.Text = ""
        txtUltFamilia.Text = ""
        txtUltDivision.Text = ""

        'grdVentas.DataSource = Nothing
        'grdVentas.Rows.Clear()

        grdOrdenServHis.DataSource = Nothing
        grdOrdenServHis.Rows.Clear()
        grdOrdenTrabajoHis.DataSource = Nothing
        grdOrdenTrabajoHis.Rows.Clear()
        grdRefaccionesHis.DataSource = Nothing
        grdRefaccionesHis.Rows.Clear()

        BorraCamposProd()

        grdProductosOT.DataSource = Nothing
        grdProductosOT.Rows.Clear()

        cmbResponsable.SelectedIndex = 0
        cmbPuestoResp.SelectedIndex = 0

        cmbayudante1.SelectedIndex = 0
        cmbPuestoAyu1.SelectedIndex = 0

        cmbayudante2.SelectedIndex = 0
        cmbPuestoAyu2.SelectedIndex = 0


    End Sub

    Private Sub txtOrdenTrabajo_KeyDown(sender As Object, e As KeyEventArgs) Handles txtOrdenTrabajo.KeyDown
        If e.KeyCode = Keys.F3 Then
            cmdBuscaClave_Click(sender, e)
        End If
    End Sub

    Private Sub txtOrdenTrabajo_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtOrdenTrabajo.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If txtOrdenTrabajo.Text.Trim <> "" Then
                CargaForm(txtOrdenTrabajo.Text, "OrdenTrabajo")

              
            End If
            e.Handled = True
        End If
    End Sub

    Private Sub txtOrdenTrabajo_Leave(sender As Object, e As EventArgs) Handles txtOrdenTrabajo.Leave
        If Salida Then Exit Sub
        If txtOrdenTrabajo.Text.Length = 0 Then
            txtOrdenTrabajo.Focus()
            txtOrdenTrabajo.SelectAll()
        Else
            If txtOrdenTrabajo.Enabled Then
                CargaForm(txtOrdenTrabajo.Text, "OrdenTrabajo")

            End If
        End If
    End Sub

    Private Sub txtOrdenTrabajo_MouseDown(sender As Object, e As MouseEventArgs) Handles txtOrdenTrabajo.MouseDown
        txtOrdenTrabajo.Focus()
        txtOrdenTrabajo.SelectAll()
    End Sub

    Private Sub cmdBuscaClave_Click(sender As Object, e As EventArgs) Handles cmdBuscaClave.Click
        txtOrdenTrabajo.Text = DespliegaGrid(UCase(TablaBd), Inicio.CONSTR, "")
        'txtidEstado_Leave(sender, e)
    End Sub

    Private Sub CargaForm(ByVal idClave As String, ByVal NomTabla As String)
        Try
            Select Case UCase(NomTabla)
                Case UCase("UNIPROD_COM")
                    UnidadCom = New UnidadesComClass(idClave, "CCLAVEINT", TipoDato.TdCadena)
                    If UnidadCom.Existe Then
                        txtNomUniProd.Text = UnidadCom.CNOMBREUNIDAD
                        vCIDUNIDAD = UnidadCom.CIDUNIDAD
                    End If
                Case UCase("PROD_COM")
                    ProdCom = New ProductosComClass(idClave, "")
                    If ProdCom.Existe Then
                        If ProdCom.CSTATUSPRODUCTO = 1 Then
                            vCIDPRODUCTO = ProdCom.CIDPRODUCTO
                            txtNomProducto.Text = ProdCom.CNOMBREPRODUCTO
                            UnidadCom = New UnidadesComClass(ProdCom.CIDUNIDADBASE, "CIDUNIDAD", TipoDato.TdNumerico)
                            If UnidadCom.Existe Then
                                txtIdUnidadProd.Text = UnidadCom.CCLAVEINT
                            End If

                        Else
                            'Desactivado
                        End If
                    End If
                Case UCase("OrdenServicio")
                    OrdenServ = New OrdenServClass(idClave)
                    With OrdenServ
                        txtOrdenServ.Text = idClave
                        If .Existe Then
                            txtOrdenServ.Text = .idOrdenSer
                            dtpFechaOrden.Value = OrdenServ.FechaRecepcion
                            dtpFechaOrdenhr.Value = OrdenServ.FechaRecepcion

                            Emp = New EmpresaClass(OrdenServ.IdEmpresa)
                            If Emp.Existe Then
                                txtNomEmpresa.Text = Emp.RazonSocial
                            End If

                            TipOrd = New TipoOrdenClass(OrdenServ.idTipoOrden)
                            If TipOrd.Existe Then
                                txtTipoOrden.Text = TipOrd.NomTipoOrden
                            End If
                            txtEstatusOrden.Text = OrdenServ.Estatus
                            txtKilometraje.Text = OrdenServ.Kilometraje

                            'RECEPCIONO
                            User = New UsuarioClass(OrdenServ.UsuarioRecepcion)
                            If User.Existe Then
                                txtRecepciono.Text = User.NombreUsuario
                            End If
                            'ENTREGO
                            Personal = New PersonalClass(OrdenServ.idEmpleadoEntrega)
                            If Personal.Existe Then
                                txtEntrego.Text = Personal.NombreCompleto
                            End If
                            'AUTORIZO
                            Personal = New PersonalClass(OrdenServ.idEmpleadoAutoriza)
                            If Personal.Existe Then
                                txtAutorizo.Text = Personal.NombreCompleto
                            End If
                        End If
                    End With
                Case UCase("OrdenTrabajo")
                    OrdenTrabajo = New OrdenTrabajoClass(idClave)
                    With OrdenTrabajo
                        'ORDEN DE TRABAJO
                        txtOrdenTrabajo.Text = idClave
                        If .Existe Then
                            'MECANICO RESPONSABLE
                            Personal = New PersonalClass(.idPersonalResp)
                            cmbResponsable.SelectedValue = .idPersonalResp
                            If Personal.Existe Then
                                txtNombreMecanico.Text = Personal.NombreCompleto

                                PersonalPuesto = New PuestosClass(Personal.idPuesto)
                                If PersonalPuesto.Existe Then
                                    txtPuestoMec.Text = PersonalPuesto.NomPuesto
                                End If
                            End If
                            'AYUDANTE 1
                            If .idPersonalAyu1 > 0 Then
                                chkAyu1.Checked = True
                                Personal = New PersonalClass(.idPersonalAyu1)
                                cmbayudante1.SelectedValue = .idPersonalAyu1
                            Else
                                chkAyu1.Checked = False
                            End If

                            'AYUDANTE 2
                            If .idPersonalAyu2 > 0 Then
                                chkAyu2.Checked = True
                                Personal = New PersonalClass(.idPersonalAyu2)
                                cmbayudante2.SelectedValue = .idPersonalAyu2
                            Else
                                chkAyu2.Checked = False
                            End If

                            'FALLA DE LA ORDEN DE TRABAJO
                            txtFalla.Text = .NotaRecepcion
                            FamiliaOT = New FamiliasClass(.idFamilia)
                            If FamiliaOT.Existe Then
                                txtFamilia.Text = FamiliaOT.NomFamilia
                                txtDivision.Text = FamiliaOT.NomDivision
                            End If

                            'DETALLE ORDEN DE SERVICIO
                            DetOS = New detOrdenServicioClass(idClave)
                            If DetOS.Existe Then
                                dtpFechaAsig.Value = DetOS.FechaAsig
                                dtpFechaAsigHora.Value = DetOS.FechaAsig
                                User = New UsuarioClass(DetOS.UsuarioAsig)
                                If User.Existe Then
                                    txtUsuarioAsig.Text = User.NombreUsuario
                                End If
                            End If

                            'ORDEN DE SERVICIO
                            OrdenServ = New OrdenServClass(.idOrdenSer)

                            txtOrdenServ.Text = .idOrdenSer
                            dtpFechaOrden.Value = OrdenServ.FechaRecepcion
                            dtpFechaOrdenhr.Value = OrdenServ.FechaRecepcion

                            Emp = New EmpresaClass(OrdenServ.IdEmpresa)
                            If Emp.Existe Then
                                txtNomEmpresa.Text = Emp.RazonSocial
                            End If

                            TipOrd = New TipoOrdenClass(OrdenServ.idTipoOrden)
                            If TipOrd.Existe Then
                                txtTipoOrden.Text = TipOrd.NomTipoOrden
                            End If
                            txtEstatusOrden.Text = OrdenServ.Estatus
                            txtKilometraje.Text = OrdenServ.Kilometraje
                            'RECEPCIONO
                            User = New UsuarioClass(OrdenServ.UsuarioRecepcion)
                            If User.Existe Then
                                txtRecepciono.Text = User.NombreUsuario
                            End If
                            'ENTREGO
                            Personal = New PersonalClass(OrdenServ.idEmpleadoEntrega)
                            If Personal.Existe Then
                                txtEntrego.Text = Personal.NombreCompleto
                            End If
                            'AUTORIZO
                            Personal = New PersonalClass(OrdenServ.idEmpleadoAutoriza)
                            If Personal.Existe Then
                                txtAutorizo.Text = Personal.NombreCompleto
                            End If
                            'ORDEN DE SERVICIO

                            'UNIDAD DE TRANSPORTE
                            txtIdUnidad.Text = OrdenServ.idUnidadTrans
                            UniTrans = New UnidadesTranspClass(OrdenServ.idUnidadTrans)
                            If UniTrans.Existe Then
                                txtNomUnidad.Text = UniTrans.DescripcionUni
                                Marca = New MarcaClass(UniTrans.idMarca)
                                If Marca.Existe Then
                                    txtMarcaUni.Text = Marca.NOMBREMARCA
                                End If
                                'ULTIMA ORDEN DE SERVICIO

                                If UniTrans.UltOT > 0 Then
                                    txtUltOrdenTrabajo.Text = UniTrans.UltOT
                                    ULTOT = New OrdenTrabajoClass(UniTrans.UltOT)
                                    If ULTOT.Existe Then
                                        txtUltimaOrdenServ.Text = ULTOT.idOrdenSer
                                        Personal = New PersonalClass(ULTOT.idPersonalResp)
                                        If Personal.Existe Then
                                            txtUltimoMecanico.Text = Personal.NombreCompleto
                                        End If

                                        txtUltFalla.Text = ULTOT.NotaRecepcion
                                        FamiliaOT = New FamiliasClass(ULTOT.idFamilia)
                                        If FamiliaOT.Existe Then
                                            txtUltFamilia.Text = FamiliaOT.NomFamilia
                                            txtUltDivision.Text = FamiliaOT.NomDivision
                                        End If
                                    End If
                                Else
                                    txtUltOrdenTrabajo.Text = ""
                                End If
                            End If


                            'Historial
                            CargaOrdenesSerHis(OrdenServ.idUnidadTrans)

                            IdOrdenServ = grdOrdenServHis.Item("hosIdServicio", grdOrdenServHis.CurrentCell.RowIndex).Value
                            CargaOrdenesTrabHis(txtIdUnidad.Text, IdOrdenServ)

                            'VALIDACIONES
                            If OrdenServ.Estatus = "ASIG" Then
                                ActivaCampos(False, TipoOpcActivaCampos.tOpcINICIALIZA)
                                ActivaBotones(True, TipoOpcActivaBoton.tOpcInicializa)

                                txtDuracion.Focus()
                                txtDuracion.SelectAll()
                            ElseIf OrdenServ.Estatus = "DIA" Then
                                MsgBox("La Orden de Trabajo: " & idClave & " ya Esta en Proceso, No puede Modificarse")
                                ActivaCampos(False, TipoOpcActivaCampos.tOpcINICIALIZA)

                                ActivaBotones(False, TipoOpcActivaBoton.tOpcInicializa)
                                btnMnuCancelar.Enabled = True
                                'txtOrdenTrabajo.Focus()
                                'txtOrdenTrabajo.SelectAll()

                            ElseIf OrdenServ.Estatus = "REC" Then
                                MsgBox("La Orden de Trabajo: " & idClave & " No esta Asignada, No puede Modificarse")
                                txtOrdenTrabajo.Focus()
                                txtOrdenTrabajo.SelectAll()

                            End If
                        Else
                            'No existe 
                            MsgBox("La Orden de Trabajo: " & idClave & " No Existe !!!")
                            txtOrdenTrabajo.Focus()
                            txtOrdenTrabajo.SelectAll()
                        End If

                    End With
                    'Case UCase(TablaBd2)
                    '    'aqui()
                    '    TipoServ = New TipoServClass(idClave)
                    '    With TipoServ
                    '        txtidTipoUnidad.Text = .idTipoServicio
                    '        If .Existe Then
                    '            txtnomTipoUniTras.Text = .NomTipoServicio
                    '            txtDescripcionUni.Focus()
                    '            txtDescripcionUni.SelectAll()
                    '        Else
                    '            MsgBox("El Tipo de Servicio con id: " & txtidTipoUnidad.Text & " No Existe")
                    '            txtidTipoUnidad.Focus()
                    '            txtidTipoUnidad.SelectAll()
                    '        End If
                    '    End With

            End Select



        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try

    End Sub

    Private Sub txtOrdenTrabajo_TextChanged(sender As Object, e As EventArgs) Handles txtOrdenTrabajo.TextChanged

    End Sub

    Private Sub btnMnuSalir_Click(sender As Object, e As EventArgs) Handles btnMnuSalir.Click
        Me.Close()
    End Sub

    Private Sub btnMnuCancelar_Click(sender As Object, e As EventArgs) Handles btnMnuCancelar.Click
        Cancelar()
    End Sub

    Private Sub CargaOrdenesSerHis(ByVal vidUnidadTrans As String)
        Try
            MyTablaHisOS.Rows.Clear()
            'grdOrdenServHis.DataSource = DetOS.CargaOrdSerxUni(OrdenServ.idUnidadTrans)
            MyTablaHisOS = DetOS.CargaOrdSerxUni(vidUnidadTrans)
            grdOrdenServHis.DataSource = Nothing
            grdOrdenServHis.Rows.Clear()

            If MyTablaHisOS.Rows.Count > 0 Then
                For i = 0 To MyTablaHisOS.DefaultView.Count - 1
                    grdOrdenServHis.Rows.Add()
                    grdOrdenServHis.Item("hosConsecutivo", i).Value = i + 1
                    'grdOrdenServHis.Item("hosIdServicio", i).Value = Trim(MyTabla.DefaultView.Item(i).Item("idOrdenSer").ToString)
                    grdOrdenServHis.Item("hosIdServicio", i).Value = MyTablaHisOS.DefaultView.Item(i).Item("idOrdenSer")
                    grdOrdenServHis.Item("hosFechaRecepcion", i).Value = MyTablaHisOS.DefaultView.Item(i).Item("FechaRecepcion")
                    grdOrdenServHis.Item("hosTipoOrden", i).Value = MyTablaHisOS.DefaultView.Item(i).Item("idTipoOrden")
                    grdOrdenServHis.Item("hosKm", i).Value = MyTablaHisOS.DefaultView.Item(i).Item("Kilometraje")
                    grdOrdenServHis.Item("hosAutorizo", i).Value = MyTablaHisOS.DefaultView.Item(i).Item("idEmpleadoAutoriza")
                    grdOrdenServHis.Item("hosEntrego", i).Value = MyTablaHisOS.DefaultView.Item(i).Item("idEmpleadoEntrega")
                    grdOrdenServHis.Item("hosRecepciono", i).Value = MyTablaHisOS.DefaultView.Item(i).Item("UsuarioRecepcion")
                    grdOrdenServHis.Item("hosFinalizo", i).Value = MyTablaHisOS.DefaultView.Item(i).Item("UsuarioEntrega")
                Next
                grdOrdenServHis.PerformLayout()
            End If

        Catch ex As Exception

        End Try
    End Sub
    Private Sub CargaOrdenesTrabHis(ByVal vidUnidadTrans As String, Optional ByVal vidOrdenSer As Integer = 0)
        Try
            MyTablaHisOT.Rows.Clear()
            'grdOrdenServHis.DataSource = DetOS.CargaOrdSerxUni(OrdenServ.idUnidadTrans)
            MyTablaHisOT = DetOS.CargaOrdTrabxUni(vidUnidadTrans, vidOrdenSer)
            grdOrdenTrabajoHis.DataSource = Nothing
            grdOrdenTrabajoHis.Rows.Clear()

            If MyTablaHisOT.Rows.Count > 0 Then
                For i = 0 To MyTablaHisOT.DefaultView.Count - 1
                    grdOrdenTrabajoHis.Rows.Add()
                    'grdOrdenServHis.Item("hosIdServicio", i).Value = Trim(MyTabla.DefaultView.Item(i).Item("idOrdenSer").ToString)
                    grdOrdenTrabajoHis.Item("hotIdOrdenSer", i).Value = MyTablaHisOT.DefaultView.Item(i).Item("idOrdenSer")
                    grdOrdenTrabajoHis.Item("hotidOrdenTrabajo", i).Value = MyTablaHisOT.DefaultView.Item(i).Item("idOrdenTrabajo")
                    grdOrdenTrabajoHis.Item("hotFechaFin", i).Value = MyTablaHisOT.DefaultView.Item(i).Item("FechaTerminado")
                    grdOrdenTrabajoHis.Item("hotAsigno", i).Value = MyTablaHisOT.DefaultView.Item(i).Item("UsuarioAsig")
                    grdOrdenTrabajoHis.Item("hotMecanico", i).Value = MyTablaHisOT.DefaultView.Item(i).Item("idPersonalResp")
                    grdOrdenTrabajoHis.Item("hotDuracion", i).Value = MyTablaHisOT.DefaultView.Item(i).Item("DuracionActHr")
                Next
                grdOrdenTrabajoHis.PerformLayout()
            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub grdOrdenServHis_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles grdOrdenServHis.CellContentClick

    End Sub

    Private Sub grdOrdenServHis_Click(sender As Object, e As EventArgs) Handles grdOrdenServHis.Click
        'DataTabla.DefaultView.RowFilter = Nothing
        'EncuentraDetalle(grdOrdenServHis.Item("hosIdServicio", grdOrdenServHis.CurrentCell.RowIndex).Value)
        IdOrdenServ = grdOrdenServHis.Item("hosIdServicio", grdOrdenServHis.CurrentCell.RowIndex).Value
        CargaOrdenesTrabHis(txtIdUnidad.Text, IdOrdenServ)
    End Sub
    Private Sub EncuentraDetalle(ByVal idOrdenSer As Integer)

        If MyTablaHisOT.DefaultView.Count > 0 Then
            With MyTablaHisOT
                .DefaultView.RowFilter = "idOrdenSer = " & idOrdenSer
                If .DefaultView.Count > 0 Then
                    'grdOrdenTrabajoHis.DataSource = .DefaultView
                End If
            End With
        End If

    End Sub

   
    Private Sub cmdBuscaProd_Click(sender As Object, e As EventArgs) Handles cmdBuscaProd.Click
        Salida = True
        txtIdProducto.Text = DespliegaGrid(UCase("PROD_COM"), Inicio.CONSTR_COM, "CCODIGOPRODUCTO")
        If txtIdProducto.Text <> "" Then CargaForm(txtIdProducto.Text, UCase("PROD_COM"))
        CargaForm(txtIdUnidadProd.Text, UCase("UNIPROD_COM"))

        txtCantidad.Focus()
        txtCantidad.SelectAll()
        Salida = False
    End Sub

    Private Sub txtIdProducto_EnabledChanged(sender As Object, e As EventArgs) Handles txtIdProducto.EnabledChanged
        cmdBuscaProd.Enabled = txtIdProducto.Enabled
    End Sub

    Private Sub txtIdProducto_KeyDown(sender As Object, e As KeyEventArgs) Handles txtIdProducto.KeyDown
        If e.KeyCode = Keys.F3 Then
            cmdBuscaProd_Click(sender, e)
        End If

    End Sub

    Private Sub txtIdProducto_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtIdProducto.KeyPress
        If txtIdProducto.Text.Length = 0 Then
            txtIdProducto.Focus()
            txtIdProducto.SelectAll()
        Else
            If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
                Salida = True
                CargaForm(txtIdProducto.Text, UCase("PROD_COM"))
                CargaForm(txtIdUnidadProd.Text, UCase("UNIPROD_COM"))

                txtCantidad.Focus()
                txtCantidad.SelectAll()
                Salida = False
            End If
        End If
    End Sub

    Private Sub txtIdProducto_Leave(sender As Object, e As EventArgs) Handles txtIdProducto.Leave
        If Salida Then Exit Sub
        If txtIdProducto.Text.Length = 0 Then
            txtIdProducto.Focus()
            txtIdProducto.SelectAll()
        Else
            If txtIdProducto.Enabled Then
                CargaForm(txtIdProducto.Text, UCase("PROD_COM"))
                CargaForm(txtIdUnidadProd.Text, UCase("UNIPROD_COM"))

                txtCantidad.Focus()
                txtCantidad.SelectAll()
            End If
        End If
    End Sub

   
    Private Sub cmdBuscaUniProd_Click(sender As Object, e As EventArgs) Handles cmdBuscaUniProd.Click
        Salida = True
        txtIdProducto.Text = DespliegaGrid(UCase("UNIPROD_COM"), Inicio.CONSTR_COM, "CCODIGOPRODUCTO")
        If txtIdProducto.Text <> "" Then CargaForm(txtIdProducto.Text, UCase("PROD_COM"))
        txtCantidad.Focus()
        txtCantidad.SelectAll()
        Salida = False
    End Sub

    Private Sub txtIdUnidadProd_EnabledChanged(sender As Object, e As EventArgs) Handles txtIdUnidadProd.EnabledChanged
        cmdBuscaUniProd.Enabled = txtIdUnidadProd.Enabled
    End Sub

    Private Sub txtIdUnidadProd_KeyDown(sender As Object, e As KeyEventArgs) Handles txtIdUnidadProd.KeyDown
        If e.KeyCode = Keys.F3 Then
            cmdBuscaUniProd_Click(sender, e)
        End If

    End Sub

    Private Sub txtIdUnidadProd_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtIdUnidadProd.KeyPress
        If txtIdUnidadProd.Text.Length = 0 Then
            txtIdUnidadProd.Focus()
            txtIdUnidadProd.SelectAll()
        Else
            If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
                Salida = True
                CargaForm(txtIdUnidadProd.Text, UCase("UNIPROD_COM"))

                txtCantidad.Focus()
                txtCantidad.SelectAll()
                Salida = False
            End If
        End If

    End Sub

    Private Sub txtIdUnidadProd_Leave(sender As Object, e As EventArgs) Handles txtIdUnidadProd.Leave
        If Salida Then Exit Sub
        If txtIdUnidadProd.Text.Length = 0 Then
            txtIdUnidadProd.Focus()
            txtIdUnidadProd.SelectAll()
        Else
            If txtIdProducto.Enabled Then
                CargaForm(txtIdUnidadProd.Text, UCase("UNIPROD_COM"))
                txtCantidad.Focus()
                txtCantidad.SelectAll()
            End If
        End If
    End Sub

    Private Sub txtCantidad_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCantidad.KeyPress
        If txtCantidad.Text.Length = 0 Then
            txtCantidad.Focus()
            txtCantidad.SelectAll()
        Else
            If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
                Salida = True
                'CargaForm(txtIdUnidadProd.Text, UCase("UNIPROD_COM"))
                'Agrega a Grid
                GrabaProductos()
                BorraCamposProd()

                txtIdProducto.Focus()
                txtIdProducto.SelectAll()
                Salida = False
            End If
        End If
    End Sub

    Private Sub GrabaProductos()
        GrabaGridProductos()
    End Sub

    Private Sub GrabaGridProductos()
        Dim BandGraba As Boolean = False
        Dim vExistencia As Double = 0
        Dim vCosto As Double = 0
        Dim CantCompra As Double = 0
        Dim CantSalida As Double = 0
        Try
            If txtIdProducto.Text <> "" Then
                If (txtCantidad.Text <> CantidadGrid) Or txtCantidad.Text = 0 Then
                    BandGraba = True
                End If

                If BandGraba Then
                    If grdProductosOT.Rows.Count = 0 Then
                        MyTablaRefac.Clear()
                        BandNoExisteProdEnGrid = True
                    End If
                    'Determinar si el producto existe en el grid, si no existe hay que agregarlo
                    If BandNoExisteProdEnGrid Then
                        Salida = True
                        myDataRow = MyTablaRefac.NewRow()
                        '
                        myDataRow("trefCIDPRODUCTO") = vCIDPRODUCTO
                        myDataRow("trefCIDUNIDADBASE") = vCIDUNIDAD

                        myDataRow("trefCCODIGOP01_PRO") = txtIdProducto.Text
                        myDataRow("trefCNOMBREP01") = txtNomProducto.Text

                        'Calcula Existencia
                        vExistencia = ObjCom.Existencia(vCIDPRODUCTO, Now, vIdAlmacen)
                        myDataRow("trefExistencia") = vExistencia

                        myDataRow("trefCantidad") = Val(txtCantidad.Text)
                        myDataRow("trefCCLAVEINT") = txtIdUnidadProd.Text
                        myDataRow("trefBComprar") = IIf(vExistencia < Val(txtCantidad.Text), True, False)

                        'Calcula Costo
                        vCosto = ObjCom.ChecaCosto(vCIDPRODUCTO, Now, vIdAlmacen)
                        myDataRow("trefCOSTO") = vCosto

                        'Cantidades
                        'Por Comprar y Salidas
                        If vExistencia >= Val(txtCantidad.Text) Then
                            CantCompra = 0
                            CantSalida = Val(txtCantidad.Text)
                        Else
                            CantCompra = Val(txtCantidad.Text) - vExistencia
                            CantSalida = vExistencia
                        End If
                        myDataRow("trefCantCompra") = CantCompra
                        myDataRow("trefCantSalida") = CantSalida

                        MyTablaRefac.Rows.Add(myDataRow)

                        'Grabamos Producto
                        GrabaProductoLOC(txtIdProducto.Text)
                        GrabaUnidadLOC(vCIDUNIDAD)


                        InsertaRegGrid()

                        BandNoExisteProdEnGrid = False
                        Salida = False
                    Else
                        For i = 0 To MyTablaRefac.DefaultView.Count - 1
                            BandNoExisteProdEnGrid = True
                            If Trim(txtIdProducto.Text) = Trim(MyTablaRefac.DefaultView.Item(i).Item("trefCCODIGOP01_PRO").ToString) And Trim(txtIdUnidadProd.Text) = Trim(grdProductosOT.Item("grefCCLAVEINT", i).Value) Then
                                MyTablaRefac.DefaultView.Item(i).Item("trefCIDPRODUCTO") = vCIDPRODUCTO
                                MyTablaRefac.DefaultView.Item(i).Item("trefCIDUNIDADBASE") = vCIDUNIDAD

                                MyTablaRefac.DefaultView.Item(i).Item("trefCCODIGOP01_PRO") = txtIdProducto.Text
                                MyTablaRefac.DefaultView.Item(i).Item("trefCNOMBREP01") = txtNomProducto.Text
                                'Calcula Existencia
                                vExistencia = ObjCom.Existencia(vCIDPRODUCTO, Now, vIdAlmacen)
                                MyTablaRefac.DefaultView.Item(i).Item("trefExistencia") = vExistencia

                                MyTablaRefac.DefaultView.Item(i).Item("trefCantidad") = Val(txtCantidad.Text)
                                MyTablaRefac.DefaultView.Item(i).Item("trefCCLAVEINT") = txtIdUnidadProd.Text

                                MyTablaRefac.DefaultView.Item(i).Item("trefBComprar") = IIf(vExistencia < Val(txtCantidad.Text), True, False)

                                'Calcula Costo
                                vCosto = ObjCom.ChecaCosto(vCIDPRODUCTO, Now, vIdAlmacen)
                                MyTablaRefac.DefaultView.Item(i).Item("trefCOSTO") = vCosto

                                If vExistencia >= Val(txtCantidad.Text) Then
                                    CantCompra = 0
                                    CantSalida = Val(txtCantidad.Text)
                                Else
                                    CantCompra = Val(txtCantidad.Text) - vExistencia
                                    CantSalida = vExistencia
                                End If
                                MyTablaRefac.DefaultView.Item(i).Item("trefCantCompra") = CantCompra
                                MyTablaRefac.DefaultView.Item(i).Item("trefCantSalida") = CantSalida

                                'Grabamos Producto
                                GrabaProductoLOC(txtIdProducto.Text)
                                GrabaUnidadLOC(vCIDUNIDAD)

                                BandNoExisteProdEnGrid = False
                                Exit For
                            End If
                        Next
                        If BandNoExisteProdEnGrid Then
                            Salida = True
                            myDataRow = MyTablaRefac.NewRow()
                            myDataRow("trefCIDPRODUCTO") = vCIDPRODUCTO
                            myDataRow("trefCIDUNIDADBASE") = vCIDUNIDAD

                            myDataRow("trefCCODIGOP01_PRO") = txtIdProducto.Text
                            myDataRow("trefCNOMBREP01") = txtNomProducto.Text
                            'Calcula Existencia
                            vExistencia = ObjCom.Existencia(vCIDPRODUCTO, Now, vIdAlmacen)
                            myDataRow("trefExistencia") = vExistencia

                            myDataRow("trefCantidad") = Val(txtCantidad.Text)
                            myDataRow("trefCCLAVEINT") = txtIdUnidadProd.Text

                            myDataRow("trefBComprar") = IIf(vExistencia < Val(txtCantidad.Text), True, False)

                            'Calcula Costo
                            vCosto = ObjCom.ChecaCosto(vCIDPRODUCTO, Now, vIdAlmacen)
                            myDataRow("trefCOSTO") = vCosto

                            If vExistencia >= Val(txtCantidad.Text) Then
                                CantCompra = 0
                                CantSalida = Val(txtCantidad.Text)
                            Else
                                CantCompra = Val(txtCantidad.Text) - vExistencia
                                CantSalida = vExistencia
                            End If
                            myDataRow("trefCantCompra") = CantCompra
                            myDataRow("trefCantSalida") = CantSalida



                            MyTablaRefac.Rows.Add(myDataRow)

                            'Grabamos Producto
                            GrabaProductoLOC(txtIdProducto.Text)
                            GrabaUnidadLOC(vCIDUNIDAD)
                        End If
                        InsertaRegGrid()
                    End If
                End If
            Else
                txtIdProducto.Enabled = True
            End If
            'ActualizaTotales()
        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, Me.Text)
        End Try
    End Sub
    Private Function InsertaRegGrid() As Boolean
        Try

            grdProductosOT.Rows.Clear()

            For i = 0 To MyTablaRefac.DefaultView.Count - 1
                grdProductosOT.Rows.Add()
                'Consecutivo
                grdProductosOT.Item("grefConsecutivo", i).Value = i + 1
                grdProductosOT.Item("grefCCODIGOP01_PRO", i).Value = Trim(MyTablaRefac.DefaultView.Item(i).Item("trefCCODIGOP01_PRO").ToString)
                grdProductosOT.Item("grefCNOMBREP01", i).Value = Trim(MyTablaRefac.DefaultView.Item(i).Item("trefCNOMBREP01").ToString)
                grdProductosOT.Item("grefExistencia", i).Value = CDbl(MyTablaRefac.DefaultView.Item(i).Item("trefExistencia"))
                grdProductosOT.Item("grefCantidad", i).Value = CDbl(MyTablaRefac.DefaultView.Item(i).Item("trefCantidad"))
                grdProductosOT.Item("grefCCLAVEINT", i).Value = Trim(MyTablaRefac.DefaultView.Item(i).Item("trefCCLAVEINT").ToString)
                grdProductosOT.Item("grefBComprar", i).Value = MyTablaRefac.DefaultView.Item(i).Item("trefBComprar")
                grdProductosOT.Item("grefCOSTO", i).Value = CDbl(MyTablaRefac.DefaultView.Item(i).Item("trefCOSTO"))

                grdProductosOT.Item("grefCIDPRODUCTO", i).Value = CInt(MyTablaRefac.DefaultView.Item(i).Item("trefCIDPRODUCTO"))
                grdProductosOT.Item("grefCIDUNIDADBASE", i).Value = CInt(MyTablaRefac.DefaultView.Item(i).Item("trefCIDUNIDADBASE"))

                grdProductosOT.Item("grefCantCompra", i).Value = CDbl(MyTablaRefac.DefaultView.Item(i).Item("trefCantCompra"))
                grdProductosOT.Item("grefCantSalida", i).Value = CDbl(MyTablaRefac.DefaultView.Item(i).Item("trefCantSalida"))
            Next
            'grdVentas.Columns("Precio").DefaultCellStyle.Format = "c"
            grdProductosOT.PerformLayout()
            'grdVentas.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
            Return True
        Catch ex As Exception
            Return False
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, Me.Text)
        End Try

    End Function

    Private Sub GrabaProductoLOC(ByVal vCCODIGOPRODUCTO As String)
        'Grabamos Producto LOCAL
        ProdCom = New ProductosComClass(vCCODIGOPRODUCTO, "")
        If ProdCom.Existe Then
            'Existe en Comercial
            ProdCom.Guardar(False, vCCODIGOPRODUCTO, txtNomProducto.Text, ProdCom.CTIPOPRODUCTO, ProdCom.CSTATUSPRODUCTO, ProdCom.CIDUNIDADBASE, ProdCom.CCODALTERN)

        End If

    End Sub

    Private Sub GrabaUnidadLOC(ByVal vCIDUNIDAD As Integer)
        'Grabamos Producto LOCAL
        'UnidadCom = New UnidadesComClass(vCCODIGOPRODUC)
        UnidadCom = New UnidadesComClass(vCIDUNIDAD, "CIDUNIDAD", TipoDato.TdNumerico)
        If UnidadCom.Existe Then
            'Existe en Comercial
            UnidadCom.Guardar(False, txtNomUniProd.Text, UnidadCom.CABREVIATURA, UnidadCom.CDESPLIEGUE, UnidadCom.CCLAVEINT)

        End If

    End Sub

    Private Sub BorraCamposProd()
        txtIdProducto.Text = ""
        txtNomProducto.Text = ""
        txtIdUnidadProd.Text = ""
        txtNomUniProd.Text = ""
        txtCantidad.Text = ""

    End Sub

    Private Sub txtDuracion_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtDuracion.KeyPress
        If txtDuracion.Text.Length = 0 Then
            txtDuracion.Focus()
            txtDuracion.SelectAll()
        Else
            If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
                Salida = True
                tabPrinc.SelectedIndex = 2
                txtIdProducto.Focus()
                txtIdProducto.SelectAll()
                Salida = False
            End If
        End If
    End Sub

    Private Sub Cancelar()
        ActivaCampos(False, TipoOpcActivaCampos.tOpcDESHABTODOS)
        ActivaBotones(False, TipoOpcActivaBoton.tOpcInicializa)
        LimpiaCampos()
        ToolStripMenu.Enabled = True
        GroupBox2.Enabled = True
        tabPrinc.SelectedIndex = 0
        'DsComboMovimiento.Tables("MGW10006").DefaultView.RowFilter = Nothing
        chkAyu1.Checked = False
        chkAyu2.Checked = False
        ActivaAyudantes(chkAyu1.Checked, TipoNumAyudante.Ayudante1)
        ActivaAyudantes(chkAyu2.Checked, TipoNumAyudante.Ayudante2)

        txtOrdenTrabajo.Enabled = True
        txtOrdenTrabajo.Focus()

    End Sub

    Private Sub ActivaBotones(ByVal Valor As Boolean, Optional ByVal vOpcion As TipoOpcActivaBoton = TipoOpcActivaBoton.tOpcInicializa)
        'Si es Verdadero Se activan Ok,Cancelar y Buscar y Terminar Se desactiva
        If vOpcion = TipoOpcActivaBoton.tOpcConsulta Then
            btnMnuOk.Enabled = False
            btnMnuCancelar.Enabled = Valor
            'btnMnuPendiente.Enabled = Not Valor
            'btnMnuEliminar.Enabled = Not Valor
            'btnMnuReporte.Enabled = Not Valor
            'btnMnuReportePed.Enabled = Not Valor
            btnMnuSalir.Enabled = Not Valor
        ElseIf vOpcion = TipoOpcActivaBoton.tOpcInicializa Then
            btnMnuOk.Enabled = Valor
            'btnMnuPendiente.Enabled = Valor
            btnMnuCancelar.Enabled = Valor
            'btnMnuEliminar.Enabled = Valor
            'btnMnuReporte.Enabled = Not Valor
            'btnMnuReportePed.Enabled = Valor
            btnMnuSalir.Enabled = Not Valor

        ElseIf vOpcion = TipoOpcActivaBoton.tOpcInsertar Then
            btnMnuOk.Enabled = False
            'btnMnuPendiente.Enabled = Valor
            btnMnuCancelar.Enabled = Valor
            'btnMnuEliminar.Enabled = Valor
            'btnMnuReporte.Enabled = Not Valor
            'btnMnuReportePed.Enabled = Valor
            btnMnuSalir.Enabled = Not Valor
        End If
    End Sub

    Private Sub ActivaCampos(ByVal valor As Boolean, ByVal vOpcion As TipoOpcActivaCampos)
        If vOpcion = TipoOpcActivaCampos.tOpcDESHABTODOS Then
            txtOrdenTrabajo.Enabled = False
            txtDuracion.Enabled = False
            dtpFechaAsig.Enabled = False
            dtpFechaAsigHora.Enabled = False
            dtpFechaOrden.Enabled = False
            dtpFechaOrdenhr.Enabled = False
            'txtBodegaEnt.Enabled = False
            'grdVentas.Enabled = False
            ActivaCamposProductos(False)
            'ActivaCamposClientes(False, TipoOpcActivaCampos.tOpcDESHABTODOS)
            'GpoProductos.Enabled = False
            'GpoBotonesEsp.Enabled = False



            cmbResponsable.Enabled = False
            cmbPuestoResp.Enabled = False
            chkAyu1.Enabled = False
            cmbayudante1.Enabled = False
            cmbPuestoAyu1.Enabled = False
            chkAyu2.Enabled = False
            cmbayudante2.Enabled = False
            cmbPuestoAyu2.Enabled = False

            ActivaAyudantes(chkAyu1.Checked, TipoNumAyudante.Ayudante1)
            ActivaAyudantes(chkAyu2.Checked, TipoNumAyudante.Ayudante2)


        ElseIf vOpcion = TipoOpcActivaCampos.tOpcINICIALIZA Then
            txtOrdenTrabajo.Enabled = valor
            txtDuracion.Enabled = Not valor
            cmbResponsable.Enabled = Not valor
            cmbPuestoResp.Enabled = Not valor
            cmbayudante1.Enabled = Not valor
            cmbPuestoAyu1.Enabled = Not valor
            cmbayudante2.Enabled = Not valor
            cmbPuestoAyu2.Enabled = Not valor

            'txtBodegaEnt.Enabled = valor
            'grdVentas.Enabled = valor
            ActivaCamposProductos(Not valor)
            'GpoProductos.Enabled = Not valor
            'GpoBotonesEsp.Enabled = Not valor

            dtpFechaAsig.Enabled = False
            dtpFechaAsigHora.Enabled = False
            dtpFechaOrden.Enabled = False
            dtpFechaOrdenhr.Enabled = False

            chkAyu1.Enabled = Not valor
            chkAyu2.Enabled = Not valor

            ActivaAyudantes(chkAyu1.Checked, TipoNumAyudante.Ayudante1)
            ActivaAyudantes(chkAyu2.Checked, TipoNumAyudante.Ayudante2)


        End If
    End Sub
    Private Sub ActivaCamposProductos(ByVal valor As Boolean)
        txtIdProducto.Enabled = valor
        txtNomProducto.Enabled = valor
        txtIdUnidadProd.Enabled = valor
        txtNomUniProd.Enabled = valor
        txtCantidad.Enabled = valor
    End Sub

    Private Sub txtIdProducto_TextChanged(sender As Object, e As EventArgs) Handles txtIdProducto.TextChanged

    End Sub

    Private Sub txtIdUnidadProd_TextChanged(sender As Object, e As EventArgs) Handles txtIdUnidadProd.TextChanged

    End Sub

    Private Sub btnMnuOk_Click(sender As Object, e As EventArgs) Handles btnMnuOk.Click
        Dim bCabPreOC As Boolean = False
        Dim bCabPreSal As Boolean = False

        Try
            Salida = True

            If Not Validar() Then Exit Sub

            If MsgBox("!! Esta seguro que desea Aplicar la Orden de Trabajo: " & txtOrdenTrabajo.Text & "? !!", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then


                With OrdenServ
                    'Status("Guardando Datos de Orden de Servico: " & .idOrdenSer, Me, 1, 2)
                    If .ContEstatusxOS("TODAS") - .ContEstatusxOS("DIAG") = 1 Then
                        StrSql = .ConsultaActualizaDato("DIAG", "Estatus", TipoDato.TdCadena)
                        ReDim Preserve ArraySql(indice)
                        ArraySql(indice) = StrSql
                        indice += 1

                        StrSql = .ConsultaActualizaDato(Now, "FechaDiagnostico", TipoDato.TdFecha)
                        ReDim Preserve ArraySql(indice)
                        ArraySql(indice) = StrSql
                        indice += 1
                    End If
                End With

                With DetOS
                    StrSql = .ConsultaActualizaDato(txtDuracion.Text, "DuracionActHr", TipoDato.TdNumerico)
                    ReDim Preserve ArraySql(indice)
                    ArraySql(indice) = StrSql
                    indice += 1

                    'Se debe de Sumar todos el personal involucrado
                    ContPersonal = ContadordePersonal()
                    StrSql = .ConsultaActualizaDato(ContPersonal, "NoPersonal", TipoDato.TdNumerico)
                    ReDim Preserve ArraySql(indice)
                    ArraySql(indice) = StrSql
                    indice += 1

                    StrSql = .ConsultaActualizaDato("DIAG", "Estatus", TipoDato.TdCadena)
                    ReDim Preserve ArraySql(indice)
                    ArraySql(indice) = StrSql
                    indice += 1

                    StrSql = .ConsultaActualizaDato(Now, "FechaDiag", TipoDato.TdFecha)
                    ReDim Preserve ArraySql(indice)
                    ArraySql(indice) = StrSql
                    indice += 1

                    StrSql = .ConsultaActualizaDato(UserId, "UsuarioDiag", TipoDato.TdCadena)
                    ReDim Preserve ArraySql(indice)
                    ArraySql(indice) = StrSql
                    indice += 1


                    'Calcular total costo Mano de Obra
                    CostoManoObra = CalculaCostoManoObra(CDbl(txtDuracion.Text))
                    StrSql = .ConsultaActualizaDato(CostoManoObra, "CostoManoObra", TipoDato.TdNumerico)
                    ReDim Preserve ArraySql(indice)
                    ArraySql(indice) = StrSql
                    indice += 1

                    'Calcular total costo de Refacciones
                    CostoRef = CalculaCostoRefacciones()
                    StrSql = .ConsultaActualizaDato(CostoRef, "CostoProductos", TipoDato.TdNumerico)
                    ReDim Preserve ArraySql(indice)
                    ArraySql(indice) = StrSql
                    indice += 1
                End With

                With OrdenTrabajo
                    StrSql = .ConsultaActualizaDato(cmbResponsable.SelectedValue, "idPersonalResp", TipoDato.TdNumerico)
                    ReDim Preserve ArraySql(indice)
                    ArraySql(indice) = StrSql
                    indice += 1

                    If chkAyu1.Checked Then
                        If cmbayudante1.SelectedValue > 0 Then
                            StrSql = .ConsultaActualizaDato(cmbayudante1.SelectedValue, "idPersonalAyu1", TipoDato.TdNumerico)
                            ReDim Preserve ArraySql(indice)
                            ArraySql(indice) = StrSql
                            indice += 1
                        End If

                    End If

                    If chkAyu2.Checked Then
                        If cmbayudante2.SelectedValue > 0 Then
                            StrSql = .ConsultaActualizaDato(cmbayudante2.SelectedValue, "idPersonalAyu2", TipoDato.TdNumerico)
                            ReDim Preserve ArraySql(indice)
                            ArraySql(indice) = StrSql
                            indice += 1
                        End If
                    End If

                End With

                '*************************I N S U M O S ********************************* 
                'DetOrdenActividadProductos
                If MyTablaRefac.Rows.Count > 0 Then
                    For i = 0 To MyTablaRefac.Rows.Count - 1
                        StrSql = ObjSql.InsertaDetOrdenActividadProductos(txtOrdenServ.Text, DetOS.idOrdenActividad,
                        MyTablaRefac.DefaultView.Item(i).Item("trefCIDPRODUCTO"),
                        MyTablaRefac.DefaultView.Item(i).Item("trefCIDUNIDADBASE"),
                        MyTablaRefac.DefaultView.Item(i).Item("trefCantidad"),
                        MyTablaRefac.DefaultView.Item(i).Item("trefCOSTO"),
                        MyTablaRefac.DefaultView.Item(i).Item("trefExistencia"),
                        MyTablaRefac.DefaultView.Item(i).Item("trefCantCompra"),
                        MyTablaRefac.DefaultView.Item(i).Item("trefCantSalida"))
                        ReDim Preserve ArraySql(indice)
                        ArraySql(indice) = StrSql
                        indice += 1

                        'PreOrden de Compra
                        If MyTablaRefac.DefaultView.Item(i).Item("trefCantCompra") > 0 Then
                            If Not bCabPreOC Then
                                'Consultar Consecutivos
                                NoIdPreOC = UltimoConsecutivo("IdPreOC")

                                StrSql = ObjSql.InsertaCabPreOC(False, NoIdPreOC, txtOrdenTrabajo.Text, Now, UserId, vIdAlmacen, "PRE")
                                ReDim Preserve ArraySql(indice)
                                ArraySql(indice) = StrSql
                                indice += 1

                                bCabPreOC = True
                            End If

                            StrSql = ObjSql.InsertaDetbPreOC(NoIdPreOC,
                            MyTablaRefac.DefaultView.Item(i).Item("trefCIDPRODUCTO"),
                            MyTablaRefac.DefaultView.Item(i).Item("trefCIDUNIDADBASE"),
                            MyTablaRefac.DefaultView.Item(i).Item("trefCantidad"))
                            ReDim Preserve ArraySql(indice)
                            ArraySql(indice) = StrSql
                            indice += 1


                        End If

                        'Pre Salida de Almacen
                        If MyTablaRefac.DefaultView.Item(i).Item("trefCantSalida") > 0 Then
                            If Not bCabPreSal Then
                                NoIdPreSal = UltimoConsecutivo("IdPreSalida")

                                StrSql = ObjSql.InsertaCabPreSalida(False, NoIdPreSal, txtOrdenTrabajo.Text, Now, UserId, OrdenTrabajo.idPersonalResp, vIdAlmacen, "PEN", 123123)
                                ReDim Preserve ArraySql(indice)
                                ArraySql(indice) = StrSql
                                indice += 1

                                bCabPreSal = True
                            End If

                            StrSql = ObjSql.InsertaDetPreSalida(NoIdPreSal,
                            MyTablaRefac.DefaultView.Item(i).Item("trefCIDPRODUCTO"),
                            MyTablaRefac.DefaultView.Item(i).Item("trefCIDUNIDADBASE"),
                            MyTablaRefac.DefaultView.Item(i).Item("trefCantidad"),
                             MyTablaRefac.DefaultView.Item(i).Item("trefCOSTO"),
                             MyTablaRefac.DefaultView.Item(i).Item("trefExistencia"))

                            ReDim Preserve ArraySql(indice)
                            ArraySql(indice) = StrSql
                            indice += 1
                        End If

                    Next
                End If
                '*************************I N S U M O S ********************************* 

                If indice > 0 Then
                    Dim obj As New CapaNegocio.Tablas
                    Dim Respuesta As String

                    Respuesta = obj.EjecutarSql(Inicio.CONSTR, "SQL", ArraySql, Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, indice)
                    FEspera.Close()
                    If Respuesta = "EFECTUADO" Then
                        MsgBox("Orden de Trabajo Actualizado Satisfactoriamente" & vbCrLf & _
                        IIf(NoIdPreOC > 0, "Se Creo la El Folio de Pre Orden de Compra # " & NoIdPreOC, "") & vbCrLf & _
                        IIf(NoIdPreSal > 0, "Se Creo la El Folio de Pre Salida de ALmacen # " & NoIdPreSal, ""), MsgBoxStyle.Exclamation, Me.Text)

                        'If OpcionForma = TipoOpcionForma.tOpcModificar Then
                        '    MsgBox("Orden de Trabajo Actualizado Satisfactoriamente" & vbCrLf & _
                        '    IIf(NoIdPreOC > 0, "Se Creo la El Folio de Pre Orden de Compra # " & NoIdPreOC, "") & vbCrLf & _
                        '    IIf(NoIdPreSal > 0, "Se Creo la El Folio de Pre Salida de ALmacen # " & NoIdPreSal, ""), MsgBoxStyle.Exclamation, Me.Text)
                        '    'ElseIf OpcionForma = TipoOpcionForma.tOpcEliminar Then
                        '    '    MsgBox("Grupo eliminado con exito!!!", MsgBoxStyle.Exclamation, Me.Text)
                        '    'ElseIf OpcionForma = TipoOpcionForma.tOpcInsertar Then
                        '    'MsgBox("Actualizacion efectuada satisfactoriamente", MsgBoxStyle.Exclamation, Me.Text)
                        'End If
                    ElseIf Respuesta = "NOEFECTUADO" Then

                    ElseIf Respuesta = "ERROR" Then
                        indice = 0
                        SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")
                    End If
                Else
                    MsgBox("No se Encontraron Registros que Grabar", MsgBoxStyle.Exclamation, Me.Text)
                End If
                Cancelar()
            End If
            Salida = False
        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try
    End Sub
    Private Function Validar() As Boolean
        Validar = True
        If Not Inicio.ValidandoCampo(Trim(txtDuracion.Text), TipoDato.TdNumerico) Then
            MsgBox("La Duracion de la Orden de Trabajo No Puede Ser Vacio", vbInformation, "Aviso" & Me.Text)
            If txtDuracion.Enabled Then
                txtDuracion.Focus()
                txtDuracion.SelectAll()
            End If
            Validar = False
        ElseIf cmbResponsable.SelectedValue = 0 Then
            MsgBox("El Mecanico Responsable no puede ser vacio", vbInformation, "Aviso" & Me.Text)
            If cmbResponsable.Enabled Then
                cmbResponsable.Focus()
            End If
            Validar = False
        ElseIf chkAyu1.Checked Then
            If cmbayudante1.SelectedValue = 0 Then
                MsgBox("El Ayudante no puede ser vacio", vbInformation, "Aviso" & Me.Text)
                If cmbayudante1.Enabled Then
                    cmbayudante1.Focus()
                End If
                Validar = False

            End If
        ElseIf chkAyu2.Checked And Not chkAyu1.Checked Then
            MsgBox("No puede Seleccionar al Ayudante 2 Hasta que seleccione el Ayudante 1", vbInformation, "Aviso" & Me.Text)
            chkAyu2.Checked = False
            If chkAyu1.Enabled Then
                chkAyu1.Focus()
            End If

        ElseIf chkAyu2.Checked Then
            If cmbayudante2.SelectedValue = 0 Then
                MsgBox("El Ayudante no puede ser vacio", vbInformation, "Aviso" & Me.Text)
                If cmbayudante2.Enabled Then
                    cmbayudante2.Focus()
                End If
                Validar = False

            End If
        End If
    End Function

    'Private Function CalculaNoEmpleados() As Integer
    '    If txt Then
    'End Function

    Private Function UltimoConsecutivo(ByVal vNomCampo As String) As Integer

        Dim Obj As New CapaNegocio.Tablas
        Dim Ultimo As Integer = 0
        '11/FEB/2016
        'Ultimo = objNumSig.NumSigEnTablas(Inicio.CONSTR, "NOMOVIMIENTO", "PARAMETROS", "PARAMETROS", " WHERE IDPARAMETRO = 1", " ORDER BY NOMOVIMIENTO DESC")

        Ultimo = objNumSig.NumSigEnTablas(Inicio.CONSTR, vNomCampo, "PARAMETROS", "PARAMETROS", " WHERE IDPARAMETRO = 1", " ORDER BY " & vNomCampo & " DESC")

        Return Ultimo
    End Function

    Private Sub txtCantidad_TextChanged(sender As Object, e As EventArgs) Handles txtCantidad.TextChanged

    End Sub

    Private Function ChecaPersonalOcupadoForma(ByVal vLugarOrigen As Integer) As Integer
        Dim LugarOcupado As Integer = 0
        'Origen = 1 -> cmbResponsable 
        'Origen = 2 -> cmbayudante1 
        'Origen = 3 -> cmbayudante2
        If vLugarOrigen = 1 Then
            If cmbayudante1.SelectedValue = cmbResponsable.SelectedValue Then
                LugarOcupado = 2
            ElseIf cmbayudante2.SelectedValue = cmbResponsable.SelectedValue Then
                LugarOcupado = 3
            End If
        ElseIf vLugarOrigen = 2 Then
            If cmbResponsable.SelectedValue = cmbayudante1.SelectedValue Then
                LugarOcupado = 1
            ElseIf cmbayudante2.SelectedValue = cmbayudante1.SelectedValue Then
                LugarOcupado = 3
            End If
        ElseIf vLugarOrigen = 3 Then
            If cmbResponsable.SelectedValue = cmbayudante2.SelectedValue Then
                LugarOcupado = 1
            ElseIf cmbayudante1.SelectedValue = cmbayudante2.SelectedValue Then
                LugarOcupado = 2
            End If

        End If
        Return LugarOcupado
    End Function

    Private Function ChecaPersonalOcupado(ByVal idPersonal As Integer, ByVal NomPersonal As String) As Boolean
        Dim Resp As Boolean = False
        Personal = New PersonalClass(cmbResponsable.SelectedValue)
        If Personal.Existe Then
            'aqui()
            MyTablaPersOcupado = Personal.TablaMecanicoOCupado("DIAG")
            If MyTablaPersOcupado.Rows.Count > 0 Then
                Resp = True
                With MyTablaPersOcupado.Rows(0)
                    MsgBox("EL Personal: " & NomPersonal & " tiene Asignado actualmente la Orden de Trabajo: " & .Item("idOrdenTrabajo") & vbCrLf & "de la Orden de Servicio: " & .Item("idordenSer") & "Asigne a Otro Personal", MsgBoxStyle.Information, Me.Text)
                End With
            End If
        End If
        Return Resp
    End Function

    Private Sub cmbResponsable_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbResponsable.SelectedIndexChanged
        If Salida Then Exit Sub
        If cmbResponsable.SelectedValue > 0 Then
            vLugOcupado = ChecaPersonalOcupadoForma(1)
            If vLugOcupado > 0 Then
                MsgBox("El Personal: " & cmbResponsable.Text & " ya esta Asignado para el" & NombreLugarPuesto(vLugOcupado), MsgBoxStyle.Information, Me.Text)
                cmbResponsable.SelectedValue = 0
                cmbResponsable.Focus()
            Else
                If ChecaPersonalOcupado(cmbResponsable.SelectedValue, cmbResponsable.Text) Then
                    txtNombreMecanico.Text = cmbResponsable.Text
                    txtPuestoMec.Text = cmbPuestoResp.Text
                End If

            End If
        Else

        End If

        

    End Sub

    Private Sub chkAyu1_CheckedChanged(sender As Object, e As EventArgs) Handles chkAyu1.CheckedChanged
        ActivaAyudantes(chkAyu1.Checked, TipoNumAyudante.Ayudante1)
    End Sub

    Private Sub ActivaAyudantes(ByVal Valor As Boolean, ByVal Ayudante As TipoNumAyudante)
        If Ayudante = TipoNumAyudante.Ayudante1 Then
            cmbayudante1.Enabled = Valor
            cmbPuestoAyu1.Enabled = Valor

        ElseIf Ayudante = TipoNumAyudante.Ayudante2 Then
            cmbayudante2.Enabled = Valor
            cmbPuestoAyu2.Enabled = Valor

        End If
    End Sub

    Private Sub chkAyu2_CheckedChanged(sender As Object, e As EventArgs) Handles chkAyu2.CheckedChanged
        ActivaAyudantes(chkAyu2.Checked, TipoNumAyudante.Ayudante2)
    End Sub


    Private Function ContadordePersonal() As Integer
        Dim cont As Integer = 0
        If cmbPuestoResp.SelectedValue > 0 Then
            cont += 1
        End If
        If chkAyu1.Checked And cmbayudante1.SelectedValue > 0 Then
            cont += 1
        End If
        If chkAyu2.Checked And cmbayudante2.SelectedValue > 0 Then
            cont += 1
        End If

        Return cont
    End Function

    Private Function CalculaCostoManoObra(ByVal vNoHoras As Double) As Double
        Dim cmo As Double = 0
        If cmbResponsable.SelectedValue > 0 Then
            Personal = New PersonalClass(cmbResponsable.SelectedValue)
            If Personal.Existe Then
                cmo = vNoHoras * Personal.PrecioHora
            End If
        End If
        If chkAyu1.Checked And cmbayudante1.SelectedValue > 0 Then
            Personal = New PersonalClass(cmbayudante1.SelectedValue)
            If Personal.Existe Then
                cmo = cmo + (vNoHoras * Personal.PrecioHora)
            End If
        End If
        If chkAyu2.Checked And cmbayudante2.SelectedValue > 0 Then
            Personal = New PersonalClass(cmbayudante2.SelectedValue)
            If Personal.Existe Then
                cmo = cmo + (vNoHoras * Personal.PrecioHora)
            End If
        End If

        Return cmo
    End Function

    Private Function CalculaCostoRefacciones() As Double
        Dim cre As Double = 0
        If MyTablaRefac.Rows.Count > 0 Then
            For i = 0 To MyTablaRefac.Rows.Count - 1
                cre = cre + (MyTablaRefac.DefaultView.Item(i).Item("trefCantSalida") * MyTablaRefac.DefaultView.Item(i).Item("trefCOSTO"))
            Next
        End If
        Return cre
    End Function

    Private Function NombreLugarPuesto(ByVal vLugar As Integer) As String
        Dim Resp As String = ""
        If vLugar = 1 Then
            Resp = " Responsable "
        ElseIf vLugar = 2 Then
            Resp = " Ayudante 1 "
        ElseIf vLugar = 3 Then
            Resp = " Ayudante 2 "
        End If
        Return Resp
    End Function

    Private Sub cmbayudante1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbayudante1.SelectedIndexChanged
        'If Salida Then Exit Sub
        'If cmbayudante1.SelectedValue > 0 Then

        '    If ChecaPersonalOcupado(cmbayudante1.SelectedValue, cmbayudante1.Text) Then

        '    End If
        'Else

        'End If

        If Salida Then Exit Sub
        If cmbayudante1.SelectedValue > 0 Then
            vLugOcupado = ChecaPersonalOcupadoForma(2)
            If vLugOcupado > 0 Then
                MsgBox("El Personal: " & cmbayudante1.Text & " ya esta Asignado para el " & NombreLugarPuesto(vLugOcupado))
                cmbayudante1.SelectedValue = 0
                cmbayudante1.Focus()
            Else
                If ChecaPersonalOcupado(cmbayudante1.SelectedValue, cmbayudante1.Text) Then
                    txtPuestoMec.Text = cmbayudante1.Text
                End If

            End If
        Else

        End If
    End Sub

    Private Sub cmbayudante2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbayudante2.SelectedIndexChanged
        'If Salida Then Exit Sub
        'If cmbayudante2.SelectedValue > 0 Then
        '    If ChecaPersonalOcupado(cmbayudante2.SelectedValue, cmbayudante2.Text) Then

        '    End If
        'Else

        'End If
        If Salida Then Exit Sub
        If cmbayudante2.SelectedValue > 0 Then
            vLugOcupado = ChecaPersonalOcupadoForma(3)
            If vLugOcupado > 0 Then
                MsgBox("El Personal: " & cmbayudante2.Text & " ya esta Asignado para el " & NombreLugarPuesto(vLugOcupado))
                cmbayudante2.SelectedValue = 0
                cmbayudante2.Focus()
            Else
                If ChecaPersonalOcupado(cmbayudante2.SelectedValue, cmbayudante2.Text) Then
                    txtPuestoMec.Text = cmbayudante2.Text
                End If

            End If
        Else

        End If
    End Sub

    Private Sub cmbPuestoResp_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbPuestoResp.SelectedIndexChanged
        If Salida Then Exit Sub
        If cmbResponsable.SelectedValue > 0 Then
            vLugOcupado = ChecaPersonalOcupadoForma(1)
            If vLugOcupado > 0 Then
                MsgBox("El Personal: " & cmbResponsable.Text & " ya esta Asignado para el" & NombreLugarPuesto(vLugOcupado), MsgBoxStyle.Information, Me.Text)
                cmbResponsable.SelectedValue = 0
                cmbResponsable.Focus()
            Else
                If ChecaPersonalOcupado(cmbResponsable.SelectedValue, cmbResponsable.Text) Then
                    txtNombreMecanico.Text = cmbResponsable.Text
                    txtPuestoMec.Text = cmbPuestoResp.Text
                End If

            End If
        Else

        End If
    End Sub

    Private Sub cmbPuestoAyu1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbPuestoAyu1.SelectedIndexChanged
        If Salida Then Exit Sub
        If cmbayudante1.SelectedValue > 0 Then
            vLugOcupado = ChecaPersonalOcupadoForma(2)
            If vLugOcupado > 0 Then
                MsgBox("El Personal: " & cmbayudante1.Text & " ya esta Asignado para el " & NombreLugarPuesto(vLugOcupado))
                cmbayudante1.SelectedValue = 0
                cmbayudante1.Focus()
            Else
                If ChecaPersonalOcupado(cmbayudante1.SelectedValue, cmbayudante1.Text) Then
                    txtPuestoMec.Text = cmbayudante1.Text
                End If

            End If
        Else

        End If
    End Sub

    Private Sub cmbPuestoAyu2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbPuestoAyu2.SelectedIndexChanged
        If Salida Then Exit Sub
        If cmbayudante2.SelectedValue > 0 Then
            vLugOcupado = ChecaPersonalOcupadoForma(3)
            If vLugOcupado > 0 Then
                MsgBox("El Personal: " & cmbayudante2.Text & " ya esta Asignado para el " & NombreLugarPuesto(vLugOcupado))
                cmbayudante2.SelectedValue = 0
                cmbayudante2.Focus()
            Else
                If ChecaPersonalOcupado(cmbayudante2.SelectedValue, cmbayudante2.Text) Then
                    txtPuestoMec.Text = cmbayudante2.Text
                End If

            End If
        Else

        End If
    End Sub
End Class
