﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class fEntregaInsumosOS
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtidOrdenTrabajo = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtIdPreSalida = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.dtpFecSolicitud = New System.Windows.Forms.DateTimePicker()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtUserSolicitud = New System.Windows.Forms.TextBox()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.txtIdEmpleadoEnc = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cmbIdPersonalRec = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.chkEntregaCambio = New System.Windows.Forms.CheckBox()
        Me.txtidOrdenSer = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtNomEmpresa = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtidUnidadTrans = New System.Windows.Forms.TextBox()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.txtTipoUnidad = New System.Windows.Forms.TextBox()
        Me.grdDetalle = New System.Windows.Forms.DataGridView()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtEstatus = New System.Windows.Forms.TextBox()
        Me.txtActividad = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.ToolStripMenu = New System.Windows.Forms.ToolStrip()
        Me.btnMnuOk = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuCancelar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuLimpiar = New System.Windows.Forms.ToolStripButton()
        Me.btnMenuReImprimir = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuSalir = New System.Windows.Forms.ToolStripButton()
        CType(Me.grdDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ToolStripMenu.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtidOrdenTrabajo
        '
        Me.txtidOrdenTrabajo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtidOrdenTrabajo.Enabled = False
        Me.txtidOrdenTrabajo.ForeColor = System.Drawing.Color.Red
        Me.txtidOrdenTrabajo.Location = New System.Drawing.Point(93, 90)
        Me.txtidOrdenTrabajo.Name = "txtidOrdenTrabajo"
        Me.txtidOrdenTrabajo.Size = New System.Drawing.Size(67, 20)
        Me.txtidOrdenTrabajo.TabIndex = 120
        Me.txtidOrdenTrabajo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label4.Location = New System.Drawing.Point(9, 93)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(78, 13)
        Me.Label4.TabIndex = 121
        Me.Label4.Text = "Orden Trabajo:"
        '
        'txtIdPreSalida
        '
        Me.txtIdPreSalida.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtIdPreSalida.ForeColor = System.Drawing.Color.Red
        Me.txtIdPreSalida.Location = New System.Drawing.Point(93, 58)
        Me.txtIdPreSalida.Name = "txtIdPreSalida"
        Me.txtIdPreSalida.Size = New System.Drawing.Size(67, 20)
        Me.txtIdPreSalida.TabIndex = 118
        Me.txtIdPreSalida.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label13.Location = New System.Drawing.Point(11, 61)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(41, 13)
        Me.Label13.TabIndex = 119
        Me.Label13.Text = "FOLIO:"
        '
        'dtpFecSolicitud
        '
        Me.dtpFecSolicitud.Enabled = False
        Me.dtpFecSolicitud.Location = New System.Drawing.Point(256, 54)
        Me.dtpFecSolicitud.Name = "dtpFecSolicitud"
        Me.dtpFecSolicitud.Size = New System.Drawing.Size(225, 20)
        Me.dtpFecSolicitud.TabIndex = 122
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label10.Location = New System.Drawing.Point(167, 60)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(83, 13)
        Me.Label10.TabIndex = 123
        Me.Label10.Text = "Fecha Solicitud:"
        '
        'txtUserSolicitud
        '
        Me.txtUserSolicitud.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtUserSolicitud.Enabled = False
        Me.txtUserSolicitud.Location = New System.Drawing.Point(93, 128)
        Me.txtUserSolicitud.MaxLength = 10
        Me.txtUserSolicitud.Name = "txtUserSolicitud"
        Me.txtUserSolicitud.Size = New System.Drawing.Size(259, 20)
        Me.txtUserSolicitud.TabIndex = 125
        Me.txtUserSolicitud.Tag = "3"
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label38.Location = New System.Drawing.Point(9, 131)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(44, 13)
        Me.Label38.TabIndex = 124
        Me.Label38.Text = "Solicita:"
        '
        'txtIdEmpleadoEnc
        '
        Me.txtIdEmpleadoEnc.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtIdEmpleadoEnc.Enabled = False
        Me.txtIdEmpleadoEnc.Location = New System.Drawing.Point(93, 154)
        Me.txtIdEmpleadoEnc.MaxLength = 10
        Me.txtIdEmpleadoEnc.Name = "txtIdEmpleadoEnc"
        Me.txtIdEmpleadoEnc.Size = New System.Drawing.Size(259, 20)
        Me.txtIdEmpleadoEnc.TabIndex = 127
        Me.txtIdEmpleadoEnc.Tag = "3"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label1.Location = New System.Drawing.Point(7, 157)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(59, 13)
        Me.Label1.TabIndex = 126
        Me.Label1.Text = "Encargado"
        '
        'cmbIdPersonalRec
        '
        Me.cmbIdPersonalRec.FormattingEnabled = True
        Me.cmbIdPersonalRec.Location = New System.Drawing.Point(93, 180)
        Me.cmbIdPersonalRec.Name = "cmbIdPersonalRec"
        Me.cmbIdPersonalRec.Size = New System.Drawing.Size(259, 21)
        Me.cmbIdPersonalRec.TabIndex = 128
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label2.Location = New System.Drawing.Point(7, 180)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(44, 13)
        Me.Label2.TabIndex = 129
        Me.Label2.Text = "Recibe:"
        '
        'chkEntregaCambio
        '
        Me.chkEntregaCambio.AutoSize = True
        Me.chkEntregaCambio.Location = New System.Drawing.Point(367, 182)
        Me.chkEntregaCambio.Name = "chkEntregaCambio"
        Me.chkEntregaCambio.Size = New System.Drawing.Size(188, 17)
        Me.chkEntregaCambio.TabIndex = 130
        Me.chkEntregaCambio.Text = "Entrego Cambio de Refacciones ?"
        Me.chkEntregaCambio.UseVisualStyleBackColor = True
        '
        'txtidOrdenSer
        '
        Me.txtidOrdenSer.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtidOrdenSer.Enabled = False
        Me.txtidOrdenSer.ForeColor = System.Drawing.Color.Red
        Me.txtidOrdenSer.Location = New System.Drawing.Point(206, 90)
        Me.txtidOrdenSer.Name = "txtidOrdenSer"
        Me.txtidOrdenSer.Size = New System.Drawing.Size(67, 20)
        Me.txtidOrdenSer.TabIndex = 135
        Me.txtidOrdenSer.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label3.Location = New System.Drawing.Point(179, 92)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(28, 13)
        Me.Label3.TabIndex = 136
        Me.Label3.Text = "O.S."
        '
        'txtNomEmpresa
        '
        Me.txtNomEmpresa.Enabled = False
        Me.txtNomEmpresa.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNomEmpresa.Location = New System.Drawing.Point(330, 89)
        Me.txtNomEmpresa.Name = "txtNomEmpresa"
        Me.txtNomEmpresa.Size = New System.Drawing.Size(210, 20)
        Me.txtNomEmpresa.TabIndex = 133
        '
        'Label9
        '
        Me.Label9.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label9.Location = New System.Drawing.Point(282, 91)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(54, 20)
        Me.Label9.TabIndex = 134
        Me.Label9.Text = "Empresa:"
        '
        'txtidUnidadTrans
        '
        Me.txtidUnidadTrans.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtidUnidadTrans.Enabled = False
        Me.txtidUnidadTrans.Location = New System.Drawing.Point(596, 90)
        Me.txtidUnidadTrans.MaxLength = 10
        Me.txtidUnidadTrans.Name = "txtidUnidadTrans"
        Me.txtidUnidadTrans.Size = New System.Drawing.Size(69, 20)
        Me.txtidUnidadTrans.TabIndex = 137
        Me.txtidUnidadTrans.Tag = "1"
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label33.Location = New System.Drawing.Point(546, 91)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(44, 13)
        Me.Label33.TabIndex = 138
        Me.Label33.Text = "Unidad:"
        '
        'txtTipoUnidad
        '
        Me.txtTipoUnidad.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTipoUnidad.Enabled = False
        Me.txtTipoUnidad.Location = New System.Drawing.Point(671, 90)
        Me.txtTipoUnidad.MaxLength = 10
        Me.txtTipoUnidad.Name = "txtTipoUnidad"
        Me.txtTipoUnidad.Size = New System.Drawing.Size(130, 20)
        Me.txtTipoUnidad.TabIndex = 139
        Me.txtTipoUnidad.Tag = "1"
        '
        'grdDetalle
        '
        Me.grdDetalle.AllowUserToAddRows = False
        Me.grdDetalle.AllowUserToDeleteRows = False
        Me.grdDetalle.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdDetalle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdDetalle.Location = New System.Drawing.Point(10, 223)
        Me.grdDetalle.Name = "grdDetalle"
        Me.grdDetalle.Size = New System.Drawing.Size(791, 204)
        Me.grdDetalle.TabIndex = 140
        '
        'Label5
        '
        Me.Label5.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label5.Location = New System.Drawing.Point(637, 180)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(64, 20)
        Me.Label5.TabIndex = 142
        Me.Label5.Text = "ESTATUS"
        '
        'txtEstatus
        '
        Me.txtEstatus.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtEstatus.Enabled = False
        Me.txtEstatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEstatus.Location = New System.Drawing.Point(707, 177)
        Me.txtEstatus.Name = "txtEstatus"
        Me.txtEstatus.Size = New System.Drawing.Size(94, 20)
        Me.txtEstatus.TabIndex = 141
        '
        'txtActividad
        '
        Me.txtActividad.Enabled = False
        Me.txtActividad.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtActividad.Location = New System.Drawing.Point(425, 146)
        Me.txtActividad.Name = "txtActividad"
        Me.txtActividad.Size = New System.Drawing.Size(376, 24)
        Me.txtActividad.TabIndex = 143
        '
        'Label6
        '
        Me.Label6.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label6.Location = New System.Drawing.Point(365, 148)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(54, 20)
        Me.Label6.TabIndex = 144
        Me.Label6.Text = "Actividad"
        '
        'ToolStripMenu
        '
        Me.ToolStripMenu.AutoSize = False
        Me.ToolStripMenu.BackColor = System.Drawing.Color.Silver
        Me.ToolStripMenu.Dock = System.Windows.Forms.DockStyle.None
        Me.ToolStripMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnMnuOk, Me.btnMnuCancelar, Me.btnMnuLimpiar, Me.btnMenuReImprimir, Me.btnMnuSalir})
        Me.ToolStripMenu.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow
        Me.ToolStripMenu.Location = New System.Drawing.Point(0, 0)
        Me.ToolStripMenu.Name = "ToolStripMenu"
        Me.ToolStripMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.ToolStripMenu.Size = New System.Drawing.Size(891, 42)
        Me.ToolStripMenu.TabIndex = 145
        Me.ToolStripMenu.Text = "ToolStrip1"
        '
        'btnMnuOk
        '
        Me.btnMnuOk.ForeColor = System.Drawing.Color.Red
        Me.btnMnuOk.Image = Global.Fletera.My.Resources.Resources.Save1
        Me.btnMnuOk.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuOk.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuOk.Name = "btnMnuOk"
        Me.btnMnuOk.Size = New System.Drawing.Size(56, 39)
        Me.btnMnuOk.Text = "&GRABAR"
        Me.btnMnuOk.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuCancelar
        '
        Me.btnMnuCancelar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuCancelar.Image = Global.Fletera.My.Resources.Resources.deleteCancelar
        Me.btnMnuCancelar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuCancelar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuCancelar.Name = "btnMnuCancelar"
        Me.btnMnuCancelar.Size = New System.Drawing.Size(71, 39)
        Me.btnMnuCancelar.Text = "&CANCELAR"
        Me.btnMnuCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnMnuCancelar.Visible = False
        '
        'btnMnuLimpiar
        '
        Me.btnMnuLimpiar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuLimpiar.Image = Global.Fletera.My.Resources.Resources.signos_f_059
        Me.btnMnuLimpiar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuLimpiar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuLimpiar.Name = "btnMnuLimpiar"
        Me.btnMnuLimpiar.Size = New System.Drawing.Size(56, 39)
        Me.btnMnuLimpiar.Text = "&LIMPIAR"
        Me.btnMnuLimpiar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMenuReImprimir
        '
        Me.btnMenuReImprimir.ForeColor = System.Drawing.Color.Red
        Me.btnMenuReImprimir.Image = Global.Fletera.My.Resources.Resources.impresora
        Me.btnMenuReImprimir.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMenuReImprimir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMenuReImprimir.Name = "btnMenuReImprimir"
        Me.btnMenuReImprimir.Size = New System.Drawing.Size(63, 39)
        Me.btnMenuReImprimir.Text = "&IMPRIMIR"
        Me.btnMenuReImprimir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuSalir
        '
        Me.btnMnuSalir.ForeColor = System.Drawing.Color.Red
        Me.btnMnuSalir.Image = Global.Fletera.My.Resources.Resources.Salida2
        Me.btnMnuSalir.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuSalir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuSalir.Name = "btnMnuSalir"
        Me.btnMnuSalir.Size = New System.Drawing.Size(41, 39)
        Me.btnMnuSalir.Text = "&SALIR"
        Me.btnMnuSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'fEntregaInsumosOS
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(819, 439)
        Me.Controls.Add(Me.ToolStripMenu)
        Me.Controls.Add(Me.txtActividad)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.txtEstatus)
        Me.Controls.Add(Me.grdDetalle)
        Me.Controls.Add(Me.txtTipoUnidad)
        Me.Controls.Add(Me.txtidUnidadTrans)
        Me.Controls.Add(Me.Label33)
        Me.Controls.Add(Me.txtidOrdenSer)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtNomEmpresa)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.chkEntregaCambio)
        Me.Controls.Add(Me.cmbIdPersonalRec)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtIdEmpleadoEnc)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtUserSolicitud)
        Me.Controls.Add(Me.Label38)
        Me.Controls.Add(Me.dtpFecSolicitud)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.txtidOrdenTrabajo)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtIdPreSalida)
        Me.Controls.Add(Me.Label13)
        Me.Name = "fEntregaInsumosOS"
        Me.Text = "ENTREGA DE REFACCIONES E INSUMOS"
        CType(Me.grdDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ToolStripMenu.ResumeLayout(False)
        Me.ToolStripMenu.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents txtidOrdenTrabajo As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents txtIdPreSalida As TextBox
    Friend WithEvents Label13 As Label
    Friend WithEvents dtpFecSolicitud As DateTimePicker
    Friend WithEvents Label10 As Label
    Friend WithEvents txtUserSolicitud As TextBox
    Friend WithEvents Label38 As Label
    Friend WithEvents txtIdEmpleadoEnc As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents cmbIdPersonalRec As ComboBox
    Friend WithEvents Label2 As Label
    Friend WithEvents chkEntregaCambio As CheckBox
    Friend WithEvents txtidOrdenSer As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents txtNomEmpresa As TextBox
    Friend WithEvents Label9 As Label
    Friend WithEvents txtidUnidadTrans As TextBox
    Friend WithEvents Label33 As Label
    Friend WithEvents txtTipoUnidad As TextBox
    Friend WithEvents grdDetalle As DataGridView
    Friend WithEvents Label5 As Label
    Friend WithEvents txtEstatus As TextBox
    Friend WithEvents txtActividad As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents ToolStripMenu As ToolStrip
    Friend WithEvents btnMnuOk As ToolStripButton
    Friend WithEvents btnMnuCancelar As ToolStripButton
    Friend WithEvents btnMnuLimpiar As ToolStripButton
    Friend WithEvents btnMenuReImprimir As ToolStripButton
    Friend WithEvents btnMnuSalir As ToolStripButton
End Class
