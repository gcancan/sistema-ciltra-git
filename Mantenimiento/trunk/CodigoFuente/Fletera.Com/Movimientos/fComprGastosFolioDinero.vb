﻿Public Class fComprGastosFolioDinero
    Dim Salida As Boolean
    Private _con As String
    Private _Usuario As String
    Private _NombreEmpleado As String
    Private _IdEmpleado As Integer
    Private _IdUsuario As Integer
    Private _cveEmpresa As Integer

    Private Personal As PersonalClass

    Dim indice As Integer = 0
    Dim ArraySql() As String


    Dim folioDinero As FoliosDineroClass
    Dim Gasto As CatGastosClass

    Dim dtCatGastos As DataTable

    Dim NoComprobante As Integer = 0

    Dim TablaBd As String = "CompGastos"
    Dim myDataColumn As DataColumn
    Dim myDataRow As DataRow
    Dim dtComprobacion As DataTable = New DataTable(TablaBd)
    Dim StrSql As String = ""

    Dim Comprobado As Decimal = 0
    Dim Saldo As Decimal = 0

    Public Sub New(con As String, nomUsuario As String, cveEmpresa As Integer, IdUsuario As Integer,
                   NombreEmpleado As String, IdEmpleado As Integer)
        _con = con
        BD = New CapaDatos.UtilSQL(con, "Juan")
        InitializeComponent()
        _Usuario = nomUsuario
        _cveEmpresa = cveEmpresa
        _IdUsuario = IdUsuario
        _NombreEmpleado = NombreEmpleado
        _IdEmpleado = IdEmpleado

    End Sub
    Private Sub fComprGastosFolioDinero_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        If Inicio.CONSTR = "" Then
            Inicio.CONSTR = _con
        End If

        CreaTablaCompGastos()

        Cancelar()
    End Sub

    Private Sub ComboCatGastos()
        Dim obj As New CatGastosClass(0)
        dtCatGastos = New DataTable

        dtCatGastos = obj.TablaGastos(False)

        If dtCatGastos.Rows.Count > 0 Then
            cmbidGasto.DataSource = dtCatGastos
            cmbidGasto.ValueMember = "idGasto"
            cmbidGasto.DisplayMember = "NomGasto"
        End If
    End Sub
    Private Sub Cancelar()
        Salida = True
        'ComboCatGastos()

        LimpiaCampos()
        ActivaBotones(False, TipoOpcActivaBoton.tOpcInicializa)
        ActivaCampos(False, TipoOpcActivaCampos.tOpcDESHABTODOS)

        LimpiaCamposComp()
        HabilitaCamposComp(TipoOpcActivaCampos.tOpcDESHABTODOS)
        HabilitaBotonesComp(TipoOpcActivaBoton.tOpcConsulta)

        ToolStripMenu.Enabled = True
        indice = 0
        txtNoRecibo.Focus()

        Salida = False
    End Sub
    Private Sub LimpiaCampos()
        txtFolio.Text = ""
        txtNoRecibo.Text = ""
        dtpFecha.Value = Now
        txtImporte.Text = ""
        txtImporteComp.Text = ""
        txtSaldo.Text = ""

        'AutoCompletarPersonal(cmbidEmpleadoEnt, TipotablasPer.tpTrafico, _cveEmpresa, " per.idempresa in (" & _cveEmpresa & ") and not per.idpuesto in (7,1) ")
        AutoCompletarPersonal(cmbidEmpleadoRec, TipotablasPer.tpChoferes, _cveEmpresa, "")
        If cmbidEmpleadoRec.Items.Count > 0 Then
            cmbidEmpleadoRec.SelectedIndex = 0
        End If

    End Sub
    Public Function AutoCompletarPersonal(ByVal Control As ComboBox, ByVal TipoCon As TipotablasPer,
    ByVal Empresa As Integer, ByVal Filtro As String) As AutoCompleteStringCollection

        Dim Coleccion As New AutoCompleteStringCollection
        Dim dt As DataTable

        Personal = New PersonalClass(0)
        If TipoCon = TipotablasPer.tpChoferes Then
            dt = Personal.TablaChoferesCombo(True, Empresa)
        ElseIf TipoCon = TipotablasPer.tpSupervisores Then
            dt = Personal.TablaAutorizaSolServ(True, Empresa, 2, 1)
        ElseIf TipoCon = TipotablasPer.tpDespachadores Then
            dt = Personal.TablaAutorizaSolServ(True, Empresa, "42", "")
        ElseIf TipoCon = TipotablasPer.tpTrafico Then
            dt = Personal.TablaPersonalComboSinNinguno(Filtro)



        End If

        'Recorrer y cargar los items para el Autocompletado
        For Each row As DataRow In dt.Rows
            Coleccion.Add(Convert.ToString(row("NombreCompleto")))
        Next
        'Ajustamos el control TextBox o ComboBox para recibir los datos de la siguiente manera.
        With Control
            .DataSource = dt
            .DisplayMember = "NombreCompleto"
            .ValueMember = "idPersonal"


            .AutoCompleteCustomSource = Coleccion
            .AutoCompleteMode = AutoCompleteMode.Suggest
            .AutoCompleteSource = AutoCompleteSource.CustomSource
        End With

        ' Aca le devuelvo los datos a recuperados de la Base
        Return Coleccion

    End Function
    Private Sub ActivaBotones(ByVal Valor As Boolean, Optional ByVal vOpcion As TipoOpcActivaBoton = TipoOpcActivaBoton.tOpcInicializa)
        'Si es Verdadero Se activan Ok,Cancelar y Buscar y Terminar Se desactiva
        If vOpcion = TipoOpcActivaBoton.tOpcConsulta Then

            btnMnuOk.Enabled = False
            btnMnuLimpiar.Enabled = True
            btnMnuFinalizar.Enabled = False
            btnMenuReImprimir.Enabled = False
            btnMnuSalir.Enabled = False
        ElseIf vOpcion = TipoOpcActivaBoton.tOpcInicializa Then
            'btnMnuOk.Enabled = Valor
            'btnMnuLimpiar.Enabled = Valor
            'btnMnuCancelar.Enabled = Valor
            'btnMnuSalir.Enabled = Not Valor

            btnMnuOk.Enabled = Valor
            btnMnuFinalizar.Enabled = Valor
            btnMnuLimpiar.Enabled = Valor
            btnMenuReImprimir.Enabled = Valor
            btnMnuSalir.Enabled = Not Valor
            'btnMnuNuevo.Enabled = Not Valor


        ElseIf vOpcion = TipoOpcActivaBoton.tOpcInsertar Then
            btnMnuOk.Enabled = True
            btnMnuFinalizar.Enabled = True
            btnMenuReImprimir.Enabled = False
            btnMnuLimpiar.Enabled = True
            btnMnuSalir.Enabled = False
            'btnMnuNuevo.Enabled = False

        ElseIf vOpcion = TipoOpcActivaBoton.tOpcEspecial Then
            btnMnuOk.Enabled = True
            btnMnuFinalizar.Enabled = True
            btnMenuReImprimir.Enabled = False
            btnMnuLimpiar.Enabled = True
            btnMnuSalir.Enabled = False
            'btnMnuNuevo.Enabled = False

        End If
    End Sub
    Private Sub ActivaCampos(ByVal valor As Boolean, ByVal vOpcion As TipoOpcActivaCampos)
        dtpFecha.Enabled = False
        txtImporteComp.Enabled = False
        txtSaldo.Enabled = False

        If vOpcion = TipoOpcActivaCampos.tOpcDESHABTODOS Then
            txtFolio.Enabled = True
            cmbidEmpleadoRec.Enabled = False
            txtImporte.Enabled = False
            txtNoRecibo.Enabled = False
            gdComprobantes.Enabled = False
        ElseIf vOpcion = TipoOpcActivaCampos.tOpcINICIALIZA Then
            txtFolio.Enabled = False
            txtNoRecibo.Enabled = True
            cmbidEmpleadoRec.Enabled = True
            txtImporte.Enabled = True
            gdComprobantes.Enabled = False
        ElseIf vOpcion = TipoOpcActivaCampos.tOpcOTRO Then
            txtFolio.Enabled = False
            txtNoRecibo.Enabled = True
            txtImporte.Enabled = False
            txtImporteComp.Enabled = False
            txtSaldo.Enabled = False
            cmbidEmpleadoRec.Enabled = False
            gdComprobantes.Enabled = False

        ElseIf vOpcion = TipoOpcActivaCampos.tOpcDEPENDEVALOR Then
            cmbidEmpleadoRec.Enabled = False
        End If
    End Sub

    Private Sub CargaForm(ByVal idClave As String, ByVal NomTabla As String, Optional ByVal ValorExtra1 As String = "",
                          Optional ByVal txtControl As ComboBox = Nothing, Optional ByVal FiltroSinWhere As String = "",
                          Optional ByVal bRequerido As Boolean = False)

        Try
            Select Case UCase(NomTabla)

                Case UCase("FoliosDinero")
                    folioDinero = New FoliosDineroClass(Val(idClave))
                    If folioDinero.Existe Then

                        txtNoRecibo.Text = folioDinero.NoRecibo

                        dtpFecha.Value = folioDinero.Fecha
                        'dtpFechaHora.Value = folioDinero.Fecha
                        'Empleado Entrega
                        'cmbidEmpleadoEnt.SelectedValue = folioDinero.idEmpleadoEnt

                        'If folioDinero.NumViaje > 0 Then
                        '    txtNumViaje.Text = folioDinero.NumViaje
                        '    CargaForm(folioDinero.NumViaje, "Viaje")
                        'End If

                        'txtEstatus.Text = folioDinero.Estatus
                        'Personal = New PersonalClass(folioDinero.idEmpleadoEnt)
                        'If Personal.Existe Then
                        '    cmbidEmpleadoEnt.SelectedValue = Personal.idPersonal
                        'End If

                        'Empleado Recibe
                        cmbidEmpleadoRec.SelectedValue = folioDinero.idEmpleadoRec

                        'Empresa
                        'Empresa = New EmpresaClass(folioDinero.IdEmpresa)
                        'If Empresa.Existe Then
                        '    txtNomEmpresa.Text = Empresa.RazonSocial
                        'End If

                        'Personal = New PersonalClass(folioDinero.idEmpleadoRec)
                        'If Personal.Existe Then

                        'End If

                        'Concepto
                        'txtConcepto.Text = reciboDinero.Concepto

                        txtImporte.Text = folioDinero.Importe
                        txtImporteComp.Text = folioDinero.ImporteComp

                        'Verificar Comprobantes guardados



                        ActivaCampos(True, TipoOpcActivaCampos.tOpcOTRO)
                        ActivaBotones(True, TipoOpcActivaBoton.tOpcEspecial)


                        HabilitaCamposComp(TipoOpcActivaCampos.tOpcDESHABTODOS)
                        HabilitaBotonesComp(TipoOpcActivaBoton.tOpcInicializa)

                        txtNoRecibo.Focus()
                    Else
                        ActivaCampos(True, TipoOpcActivaCampos.tOpcDESHABTODOS)

                        ActivaBotones(False, TipoOpcActivaBoton.tOpcEspecial)

                        txtFolio.Focus()
                        'No existe
                    End If
                Case UCase("Gasto")
                    Gasto = New CatGastosClass(Val(idClave))
                    If Gasto.Existe Then
                        Salida = True
                        HabilitaCamposComp(TipoOpcActivaCampos.tOpcOTRO)

                        If Gasto.Deducible Then
                            txtIVAC.Enabled = True
                        Else
                            txtIVAC.Enabled = False
                        End If

                        If Gasto.ReqComprobante Then
                            chkNoDoc.Checked = True
                            txtNoDocumento.Enabled = True
                        Else
                            chkNoDoc.Checked = False
                            txtNoDocumento.Enabled = False
                        End If

                        If Gasto.ReqAutorizacion Then
                            chkAutoriza.Checked = True
                            cmbAutoriza.Enabled = True
                        Else
                            chkAutoriza.Checked = False
                            cmbAutoriza.Enabled = False
                        End If
                        'chkSinComprobante.Enabled = True
                        'txtNoDocumento.Enabled = False
                        'cmbAutoriza.Enabled = False
                        'cmbidGasto.Enabled = False

                        dtpFechaC.Enabled = True
                        txtImporteC.Enabled = True
                        txtTotalC.Enabled = False

                        txtObservaciones.Enabled = True

                        If cmbAutoriza.Enabled Then
                            cmbAutoriza.Focus()
                        ElseIf txtNoDocumento.Enabled Then
                            txtNoDocumento.Focus()
                        Else
                            dtpFechaC.Focus()
                        End If
                        'cmbidGasto.Enabled = True




                        Salida = False

                    Else
                        cmbidGasto.Enabled = False
                    End If


                    'reciboDinero = New RecibosDineroClass(Val(idClave))
                    'If reciboDinero.Existe Then
                    '    SaldoNoREcibo = reciboDinero.Importe - reciboDinero.ImporteComp
                    '    txtSaldoRecibo.Text = SaldoNoREcibo
                    '    'txtNumViaje.Focus()
                    '    cmbTipoFolioDin.Focus()
                    'Else
                    '    MsgBox("No se encontro el No. de Recibo !!!", MsgBoxStyle.Information, Me.Text)
                    '    txtNoRecibo.Focus()
                    'End If

                Case UCase("Viaje")
                    'Dim vObj = New Nombre.Interfaces.CartaPorteSvc().getCartaPorteById(Convert.ToInt32(idClave))
                    'If vObj.valor = 0 Then
                    '    Dim Viaje As Nombre.Models.Viaje
                    '    Viaje = CType(vObj.result, List(Of Nombre.Models.Viaje))(0)
                    '    If Viaje.EstatusGuia = "ACTIVO" Then
                    '        txtTractor.Text = Viaje.tractor.clave
                    '        txtRemolque1.Text = Viaje.remolque.clave
                    '        If Not Viaje.dolly Is Nothing Then
                    '            txtDolly.Text = Viaje.dolly.clave
                    '        End If

                    '        If Not Viaje.remolque2 Is Nothing Then
                    '            txtRemolque2.Text = Viaje.remolque2.clave
                    '        End If
                    '        cmbidEmpleadoRec.SelectedValue = Viaje.operador.clave
                    '        cmbidEmpleadoRec.Enabled = False
                    '        'llena Origen - Destino
                    '        reciboDinero = New RecibosDineroClass(0)
                    '        MyTablaOriDes = reciboDinero.tbOriDes(Viaje.NumGuiaId)

                    '        cmbOriDes.DataSource = Nothing
                    '        If MyTablaOriDes.Rows.Count > 0 Then
                    '            cmbOriDes.DataSource = MyTablaOriDes
                    '            cmbOriDes.DisplayMember = "OriDes"
                    '            cmbOriDes.ValueMember = "OriDes"

                    '        End If




                    '        cmbidEmpleadoEnt.Focus()

                    '    End If
                    'End If
            End Select
        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try

    End Sub

    Private Sub txtFolio_TextChanged(sender As Object, e As EventArgs) Handles txtFolio.TextChanged

    End Sub

    Private Sub txtFolio_KeyUp(sender As Object, e As KeyEventArgs) Handles txtFolio.KeyUp
        If e.KeyValue = Keys.Enter Then
            Salida = True
            CargaForm(txtFolio.Text, "FoliosDinero")
            'txtNumViaje.Focus()

            Salida = False
        End If
    End Sub

    Private Sub LimpiaCamposComp()
        txtNoPago.Text = ""
        txtNoDocumento.Text = ""
        ComboCatGastos()
        If cmbidGasto.Items.Count > 0 Then
            cmbidGasto.SelectedIndex = 0
        End If
        AutoCompletarPersonal(cmbAutoriza, TipotablasPer.tpTrafico, _cveEmpresa, " per.idempresa in (" & _cveEmpresa & ") and not per.idpuesto in (7,1) ")
        If cmbAutoriza.Items.Count > 0 Then
            cmbAutoriza.SelectedIndex = 0
        End If


        dtpFechaC.Value = Now
        txtImporteC.Text = ""
        txtIVAC.Text = ""
        txtTotalC.Text = ""
        chkAutoriza.Checked = False
        chkNoDoc.Checked = False
        'cmbAutoriza.SelectedIndex = 0
        txtObservaciones.Text = ""


    End Sub

    Private Sub HabilitaCamposComp(ByVal vOpcion As TipoOpcActivaCampos)
        txtNoPago.Enabled = False
        Select Case vOpcion
            Case TipoOpcActivaCampos.tOpcDEPENDEVALOR

            Case TipoOpcActivaCampos.tOpcDESHABTODOS
                chkAutoriza.Enabled = False
                txtNoDocumento.Enabled = False
                cmbidGasto.Enabled = False
                dtpFechaC.Enabled = False
                txtImporteC.Enabled = False
                txtIVAC.Enabled = False
                txtTotalC.Enabled = False
                cmbAutoriza.Enabled = False
                chkNoDoc.Enabled = False
                txtObservaciones.Enabled = False

            Case TipoOpcActivaCampos.tOpcINICIALIZA
                cmbidGasto.Enabled = False
                chkAutoriza.Enabled = False
                txtNoDocumento.Enabled = False

                dtpFechaC.Enabled = False
                txtImporteC.Enabled = False
                txtIVAC.Enabled = False
                txtTotalC.Enabled = False
                cmbAutoriza.Enabled = False
                chkNoDoc.Enabled = False
                txtObservaciones.Enabled = False
            Case TipoOpcActivaCampos.tOpcOTRO
                cmbidGasto.Enabled = False

                chkAutoriza.Enabled = True
                cmbAutoriza.Enabled = False

                chkNoDoc.Enabled = True
                txtNoDocumento.Enabled = False

                dtpFechaC.Enabled = True
                txtImporteC.Enabled = True
                txtIVAC.Enabled = True
                txtTotalC.Enabled = False

                txtObservaciones.Enabled = True


        End Select




    End Sub

    Private Sub btnAgregaC_Click(sender As Object, e As EventArgs) Handles btnAgregaC.Click
        Salida = True
        LimpiaCamposComp()
        HabilitaCamposComp(TipoOpcActivaCampos.tOpcINICIALIZA)
        HabilitaBotonesComp(TipoOpcActivaBoton.tOpcInsertar)
        NoComprobante = NoComprobante + 1
        txtNoPago.Text = NoComprobante
        cmbidGasto.Enabled = True
        cmbidGasto.Focus()

        If dtComprobacion.Rows.Count > 0 Then
            ActivaBotones(True, TipoOpcActivaBoton.tOpcEditar)
        End If

        'If chkAutoriza.Checked Then
        '    cmbAutoriza.Enabled = True
        '    txtNoDocumento.Enabled = False
        '    cmbAutoriza.Focus()
        'Else
        '    cmbAutoriza.Enabled = False
        '    txtNoDocumento.Enabled = True
        '    txtNoDocumento.Focus()
        'End If
        Salida = False
    End Sub

    Private Sub HabilitaBotonesComp(ByVal Opcion As TipoOpcActivaBoton)
        Select Case Opcion
            Case TipoOpcActivaBoton.tOpcConsulta
                btnAgregaC.Enabled = False
                btnCancelaC.Enabled = False
                btnBorraC.Enabled = False

            Case TipoOpcActivaBoton.tOpcInicializa
                btnAgregaC.Enabled = True
                btnCancelaC.Enabled = False
                btnBorraC.Enabled = True
            Case TipoOpcActivaBoton.tOpcInsertar
                btnAgregaC.Enabled = False
                btnCancelaC.Enabled = True
                btnBorraC.Enabled = False


        End Select
    End Sub

    Private Sub chkSinComprobante_CheckedChanged(sender As Object, e As EventArgs) Handles chkAutoriza.CheckedChanged
        If Salida Then Exit Sub
        If chkAutoriza.Checked Then
            cmbAutoriza.Enabled = True
            cmbAutoriza.Focus()
        Else
            cmbAutoriza.Enabled = False
            'txtNoDocumento.Enabled = True
            'txtNoDocumento.Focus()
        End If
        'HabilitaCamposComp(TipoOpcActivaCampos.tOpcDEPENDEVALOR)
    End Sub

    Private Sub cmbAutoriza_KeyUp(sender As Object, e As KeyEventArgs) Handles cmbAutoriza.KeyUp
        If e.KeyValue = Keys.Enter Then
            Salida = True
            If txtNoDocumento.Enabled Then
                txtNoDocumento.Focus()
            Else
                dtpFechaC.Focus()
            End If
            Salida = False
        End If
    End Sub
    'Private Sub txtNoDocumento_KeyUp(sender As Object, e As KeyEventArgs) Handles txtNoDocumento.KeyUp
    '    If e.KeyValue = Keys.Enter Then
    '        Salida = True
    '        dtpFecha.Focus()
    '        Salida = False
    '    End If
    'End Sub

    Private Sub cmbidGasto_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbidGasto.SelectedIndexChanged

    End Sub

    Private Sub cmbidGasto_KeyUp(sender As Object, e As KeyEventArgs) Handles cmbidGasto.KeyUp
        If e.KeyValue = Keys.Enter Then
            Salida = True
            CargaForm(cmbidGasto.SelectedValue, "Gasto")

            Salida = False
        End If
    End Sub

    Private Sub dtpFechaC_KeyUp(sender As Object, e As KeyEventArgs) Handles dtpFechaC.KeyUp
        If Salida Then Exit Sub


        If e.KeyValue = Keys.Enter Then
            Salida = True
            txtImporteC.Focus()
            txtImporteC.SelectAll()

            Salida = False
        End If
    End Sub

    Private Sub txtImporteC_KeyUp(sender As Object, e As KeyEventArgs) Handles txtImporteC.KeyUp
        If e.KeyValue = Keys.Enter Then
            Salida = True
            If txtIVAC.Enabled Then
                txtIVAC.Focus()
                txtIVAC.SelectAll()
            Else
                txtObservaciones.Focus()
            End If
            ImprimeTotal()
            Salida = False
        End If
    End Sub

    Private Sub ImprimeTotal()
        txtTotalC.Text = Val(txtImporteC.Text) + Val(txtIVAC.Text)
    End Sub
    Private Sub txtIVAC_KeyUp(sender As Object, e As KeyEventArgs) Handles txtIVAC.KeyUp
        If e.KeyValue = Keys.Enter Then
            Salida = True
            ImprimeTotal()
            txtObservaciones.Focus()
            Salida = False
        End If
    End Sub

    Private Sub txtObservaciones_TextChanged(sender As Object, e As EventArgs) Handles txtObservaciones.TextChanged

    End Sub

    Private Sub txtObservaciones_KeyUp(sender As Object, e As KeyEventArgs) Handles txtObservaciones.KeyUp
        If e.KeyValue = Keys.Enter Then
            Salida = True
            'Se graba en el grid
            InsertaRegistroTabla(NoComprobante, Val(txtFolio.Text), txtNoDocumento.Text, cmbidGasto.SelectedValue, dtpFechaC.Value, Val(txtImporteC.Text),
            Val(txtIVAC.Text), IIf(Val(txtIVAC.Text) > 0, True, False), cmbAutoriza.SelectedValue, txtObservaciones.Text, chkNoDoc.Checked,
            chkAutoriza.Checked, cmbidGasto.Text, cmbAutoriza.Text)

            LimpiaCamposComp()
            HabilitaCamposComp(TipoOpcActivaCampos.tOpcDESHABTODOS)
            HabilitaBotonesComp(TipoOpcActivaBoton.tOpcInicializa)
            CalculaTotales()
            btnAgregaC.Focus()
            Salida = False
        End If
    End Sub

    Private Sub chkNoDoc_CheckedChanged(sender As Object, e As EventArgs) Handles chkNoDoc.CheckedChanged
        If Salida Then Exit Sub
        If chkNoDoc.Checked Then
            txtNoDocumento.Enabled = True
            txtNoDocumento.Focus()
        Else
            txtNoDocumento.Enabled = False
        End If
    End Sub

    Private Sub CreaTablaCompGastos()
        Try
            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Int64")
            myDataColumn.ColumnName = "gcNoPago"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            dtComprobacion.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Int64")
            myDataColumn.ColumnName = "gcFolio"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            dtComprobacion.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.String")
            myDataColumn.ColumnName = "gcNoDocumento"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            dtComprobacion.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Int64")
            myDataColumn.ColumnName = "gcidGasto"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            dtComprobacion.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.DateTime")
            myDataColumn.ColumnName = "gcFecha"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            dtComprobacion.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Decimal")
            myDataColumn.ColumnName = "gcImporte"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            dtComprobacion.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Decimal")
            myDataColumn.ColumnName = "gcIVA"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            dtComprobacion.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType(" System.Boolean")
            myDataColumn.ColumnName = "gcDeducible"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            dtComprobacion.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Int64")
            myDataColumn.ColumnName = "gcAutoriza"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            dtComprobacion.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.String")
            myDataColumn.ColumnName = "gcObservaciones"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            dtComprobacion.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Boolean")
            myDataColumn.ColumnName = "gcReqComprobante"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            dtComprobacion.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Boolean")
            myDataColumn.ColumnName = "gcReqAutorizacion"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            dtComprobacion.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.String")
            myDataColumn.ColumnName = "gcNomGasto"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            dtComprobacion.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.String")
            myDataColumn.ColumnName = "gcNomAutoriza"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            dtComprobacion.Columns.Add(myDataColumn)


            '''''''''////////////////////////////////////
        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try
    End Sub
    Private Sub InsertaGrid(ByVal vTabla As DataTable)
        gdComprobantes.Rows.Clear()
        For i = 0 To vTabla.DefaultView.Count - 1
            gdComprobantes.Rows.Add()
            gdComprobantes.Item("gcNoPago", i).Value = vTabla.DefaultView.Item(i).Item("gcNoPago")
            gdComprobantes.Item("gcFolio", i).Value = vTabla.DefaultView.Item(i).Item("gcFolio")
            gdComprobantes.Item("gcNoDocumento", i).Value = vTabla.DefaultView.Item(i).Item("gcNoDocumento")
            gdComprobantes.Item("gcidGasto", i).Value = Trim(vTabla.DefaultView.Item(i).Item("gcidGasto").ToString)
            gdComprobantes.Item("gcFecha", i).Value = Trim(vTabla.DefaultView.Item(i).Item("gcFecha").ToString)
            gdComprobantes.Item("gcImporte", i).Value = Trim(vTabla.DefaultView.Item(i).Item("gcImporte").ToString)
            gdComprobantes.Item("gcIVA", i).Value = Trim(vTabla.DefaultView.Item(i).Item("gcIVA").ToString)
            gdComprobantes.Item("gcDeducible", i).Value = Trim(vTabla.DefaultView.Item(i).Item("gcDeducible").ToString)
            gdComprobantes.Item("gcAutoriza", i).Value = Trim(vTabla.DefaultView.Item(i).Item("gcAutoriza").ToString)
            gdComprobantes.Item("gcObservaciones", i).Value = Trim(vTabla.DefaultView.Item(i).Item("gcObservaciones").ToString)
            gdComprobantes.Item("gcReqComprobante", i).Value = Trim(vTabla.DefaultView.Item(i).Item("gcReqComprobante").ToString)
            gdComprobantes.Item("gcReqAutorizacion", i).Value = Trim(vTabla.DefaultView.Item(i).Item("gcReqAutorizacion").ToString)
            gdComprobantes.Item("gcNomGasto", i).Value = Trim(vTabla.DefaultView.Item(i).Item("gcNomGasto").ToString)
            gdComprobantes.Item("gcNomAutoriza", i).Value = Trim(vTabla.DefaultView.Item(i).Item("gcNomAutoriza").ToString)
        Next
        gdComprobantes.PerformLayout()
    End Sub

    Private Sub InsertaRegistroTabla(ByVal vNoPago As Integer, ByVal vFolio As Integer, ByVal vNoDocumento As String, ByVal vidGasto As Integer,
    ByVal vFecha As DateTime, ByVal vImporte As Decimal, ByVal vIVA As Decimal, ByVal vDeducible As Boolean, ByVal vAutoriza As Integer,
    ByVal vObservaciones As String, ByVal vReqComprobante As Boolean, ByVal vReqAutorizacion As Boolean, ByVal vNomGasto As String, ByVal vNomAutoriza As String)
        myDataRow = dtComprobacion.NewRow()
        myDataRow("gcNoPago") = vNoPago
        myDataRow("gcFolio") = vFolio
        myDataRow("gcNoDocumento") = vNoDocumento
        myDataRow("gcidGasto") = vidGasto
        myDataRow("gcFecha") = vFecha
        myDataRow("gcImporte") = vImporte
        myDataRow("gcIVA") = vIVA
        myDataRow("gcDeducible") = vDeducible
        myDataRow("gcAutoriza") = vAutoriza
        myDataRow("gcObservaciones") = vObservaciones
        myDataRow("gcReqComprobante") = vReqComprobante
        myDataRow("gcReqAutorizacion") = vReqAutorizacion
        myDataRow("gcNomGasto") = vNomGasto
        myDataRow("gcNomAutoriza") = vNomAutoriza
        dtComprobacion.Rows.Add(myDataRow)

        InsertaGrid(dtComprobacion)
    End Sub

    Private Sub txtIVAC_TextChanged(sender As Object, e As EventArgs) Handles txtIVAC.TextChanged

    End Sub

    Private Sub txtImporteC_TextChanged(sender As Object, e As EventArgs) Handles txtImporteC.TextChanged

    End Sub

    Private Sub txtNoDocumento_TextChanged(sender As Object, e As EventArgs)

    End Sub


    Private Sub txtNoDocumento_KeyUp(sender As Object, e As KeyEventArgs) Handles txtNoDocumento.KeyUp
        If e.KeyValue = Keys.Enter Then
            Salida = True
            dtpFechaC.Focus()
            Salida = False
        End If
    End Sub

    Private Sub dtpFechaC_ValueChanged(sender As Object, e As EventArgs) Handles dtpFechaC.ValueChanged

    End Sub

    Private Sub btnCancelaC_Click(sender As Object, e As EventArgs) Handles btnCancelaC.Click
        Salida = True
        LimpiaCamposComp()
        HabilitaCamposComp(TipoOpcActivaCampos.tOpcDESHABTODOS)
        HabilitaBotonesComp(TipoOpcActivaBoton.tOpcInicializa)

        btnAgregaC.Focus()
        Salida = False

    End Sub

    Private Sub btnBorraC_Click(sender As Object, e As EventArgs) Handles btnBorraC.Click
        'Borra el que este en el grid, esto antes de grabar, cuando se graba ya no es permitido.



    End Sub

    Private Sub btnMnuOk_Click(sender As Object, e As EventArgs) Handles btnMnuOk.Click
        'GRabar los pagos
        Dim objsql As New ToolSQLs
        If dtComprobacion.Rows.Count > 0 Then
            indice = 0
            For i = 0 To dtComprobacion.Rows.Count - 1
                StrSql = objsql.InsertaComprobanteFolio(Val(dtComprobacion.Rows.Item(i).Item("gcFolio")),
                dtComprobacion.Rows.Item(i).Item("gcNoDocumento").ToString(),
                dtComprobacion.Rows.Item(i).Item("gcidGasto"),
                dtComprobacion.Rows.Item(i).Item("gcFecha").ToString(),
                dtComprobacion.Rows.Item(i).Item("gcImporte"),
                dtComprobacion.Rows.Item(i).Item("gcIVA"),
                dtComprobacion.Rows.Item(i).Item("gcDeducible"),
                dtComprobacion.Rows.Item(i).Item("gcAutoriza"),
                dtComprobacion.Rows.Item(i).Item("gcObservaciones").ToString(),
                dtComprobacion.Rows.Item(i).Item("gcReqComprobante"),
                dtComprobacion.Rows.Item(i).Item("gcReqAutorizacion"))
                ReDim Preserve ArraySql(indice)
                ArraySql(indice) = StrSql
                indice += 1
            Next
            StrSql = objsql.ActualizaCampoTabla("FoliosDinero", "Estatus", TipoDato.TdCadena, "COMPROBANDO",
                                                        "Folio", TipoDato.TdNumerico, Val(txtFolio.Text))
            ReDim Preserve ArraySql(indice)
            ArraySql(indice) = StrSql
            indice += 1
            StrSql = objsql.ActualizaCampoTabla("FoliosDinero", "ImporteComp", TipoDato.TdNumerico, Comprobado,
                                                        "Folio", TipoDato.TdNumerico, Val(txtFolio.Text))
            ReDim Preserve ArraySql(indice)
            ArraySql(indice) = StrSql
            indice += 1


            If indice > 0 Then
                Dim obj As New CapaNegocio.Tablas
                Dim Respuesta As String
                Respuesta = obj.EjecutarSql(Inicio.CONSTR, "SQL", ArraySql, Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, indice)
                FEspera.Close()
                If Respuesta = "EFECTUADO" Then
                    'Se imprime los gastos
                    MsgBox("Actualización Efectuada Satisfactoriamente", MsgBoxStyle.Exclamation, Me.Text)
                    Cancelar()
                End If
            End If
        Else
            'No hat Pagos por grabar
            MsgBox("No hay Comprobaciones de gastos para guardar" , MsgBoxStyle.Exclamation, Me.Text)
        End If

    End Sub

    Private Sub CalculaTotales()

        If dtComprobacion.Rows.Count > 0 Then
            For i = 0 To dtComprobacion.Rows.Count - 1
                Comprobado = Comprobado + dtComprobacion.Rows.Item(i).Item("gcImporte") + dtComprobacion.Rows.Item(i).Item("gcIVA")
            Next
        End If

        Saldo = Val(txtImporte.Text) - Comprobado

        txtImporteComp.Text = Format(Comprobado, "c")
        txtSaldo.Text = Format(Saldo, "c")
    End Sub

    Private Sub btnMnuLimpiar_Click(sender As Object, e As EventArgs) Handles btnMnuLimpiar.Click
        Cancelar()
    End Sub

    Private Sub btnMnuSalir_Click(sender As Object, e As EventArgs) Handles btnMnuSalir.Click
        Me.Close()
    End Sub

    Private Sub btnMnuFinalizar_Click(sender As Object, e As EventArgs) Handles btnMnuFinalizar.Click
        If dtComprobacion.Rows.Count > 0 Then
        Else
            MsgBox("No hay Comprobaciones de gastos para guardar", MsgBoxStyle.Exclamation, Me.Text)
        End If
    End Sub


    'Private Sub txtNoDocumento_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtNoDocumento.KeyPress
    '    If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
    '        Salida = True
    '        dtpFechaC.Focus()
    '        Salida = False

    '    End If

    'End Sub
End Class