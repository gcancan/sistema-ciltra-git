'Fecha: 14 / FEBRERO / 2016
'ROGER GALA PACHECO
'************************************************************************************************************************************
'*  Fecha Modificacion :  14 / febrero / 2017                                                                             
'*  
'************************************************************************************************************************************
Imports Microsoft.VisualBasic.PowerPacks
Public Class fServInternosLlantas
    Inherits System.Windows.Forms.Form
    Dim OpcionForma As TipoOpcionForma
    Dim TablaBd As String = "CatLlantas"
    Dim CampoLLave As String = "idLlanta"
    Public WithEvents MeStatus1 As ControlStatus.MeStatus
    Friend WithEvents GridTodos As System.Windows.Forms.DataGridView

    Private Llanta As LlantasClass
    Dim llantaProd As LLantasProductoClass
    Private TipoUniTrans As TipoUniTransClass
    Private Empresa As EmpresaClass
    Private Marca As MarcaClass
    Private TipoLLanta As TipoLlantaClass

    Friend WithEvents btnMnuOk As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuCancelar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuSalir As System.Windows.Forms.ToolStripButton
    Private _Tag As String
    'Dim bEsIdentidad As Boolean = True
    Dim Salida As Boolean
    Dim DsCombo As New DataSet
    Private _con As String
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents GrupoCaptura As GroupBox
    Friend WithEvents label1 As Label
    Friend WithEvents txtidLlanta As TextBox
    Friend WithEvents Label7 As Label
    Private _v As Boolean

    Private Prefijo As String
    'Private UltimoConsecutivo As String
    Dim UniTrans As UnidadesTranspClass
    Dim TipUni As TipoUniTransClass

    Dim ClasLlan As ClasLlanClass
    Dim TablaEstruc As DataTable

    Dim rdbPosicion() As CheckBox
    Private _Usuario As String

    Dim vUnidadOrig As String
    Friend WithEvents GpoPosiciones As GroupBox
    'Friend WithEvents ShapeContainer2 As ShapeContainer
    'Friend WithEvents LineShape1 As LineShape
    Friend WithEvents cmbidEmpresa As ComboBox
    Dim vPosUniOrig As Integer
    Friend WithEvents gpoProdLlanta As GroupBox
    Friend WithEvents Label6 As Label
    Friend WithEvents txtProfundidad As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents txtMedida As TextBox
    Friend WithEvents cmbidDisenio As ComboBox
    Friend WithEvents label2 As Label
    Friend WithEvents cmbidMarca As ComboBox
    Friend WithEvents Label4 As Label
    Friend WithEvents cmbidProductoLlanta As ComboBox
    Friend WithEvents Label16 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents chkActivo As CheckBox
    Private Disenio As CatDiseniosClass
    Private Condicion As CatCondicionesClass
    Private TipoUbicacion As CatLugaresClass
    'Private TipoUbica As CatLugaresClass
    Private RelProvTos As RelProvTipOrdSerClass
    Friend WithEvents btnMnuModificar As ToolStripButton
    Friend WithEvents cmbActividades As ComboBox
    Friend WithEvents Label9 As Label
    Friend WithEvents cmbidCondicion As ComboBox
    Friend WithEvents Label20 As Label
    Friend WithEvents gpoRevitalizado As GroupBox
    Friend WithEvents chkRevitalizado As CheckBox
    Friend WithEvents cmbidDisenioRev As ComboBox
    Friend WithEvents Label17 As Label
    Friend WithEvents cmbidTipoLLanta As ComboBox
    Friend WithEvents Label3 As Label
    Friend WithEvents gpoDestino As GroupBox
    Friend WithEvents txtTipoUniDES As TextBox
    Friend WithEvents Label10 As Label
    Friend WithEvents txtPosicionDES As NumericUpDown
    Friend WithEvents Label11 As Label
    Friend WithEvents cmbidUbicacionDES As ComboBox
    Friend WithEvents Label15 As Label
    Friend WithEvents Label19 As Label
    Friend WithEvents cmbidtipoUbicacionDES As ComboBox
    Friend WithEvents gpoOrigen As GroupBox
    Friend WithEvents txtTipoUni As TextBox
    Friend WithEvents Label12 As Label
    Friend WithEvents txtPosicion As NumericUpDown
    Friend WithEvents Label13 As Label
    Friend WithEvents cmbidUbicacion As ComboBox
    Friend WithEvents lblUbicacion As Label
    Friend WithEvents Label21 As Label
    Friend WithEvents cmbidtipoUbicacion As ComboBox
    Friend WithEvents Label22 As Label
    Friend WithEvents txtObserva As TextBox
    Private Almacen As AlmacenClass
    Private idSisLlanta As Integer

    Private idSisLlantaDES As Integer
    Private PosicionDES As Integer
    Private idLLantaDES As String


    Dim indice As Integer = 0
    Dim ArraySql() As String
    Dim StrSql As String = ""
    Friend WithEvents Label23 As Label
    Friend WithEvents txtProfundidadDES As TextBox
    Friend WithEvents Label14 As Label
    Friend WithEvents txtProfundidadAct As TextBox
    Friend WithEvents Label24 As Label
    Friend WithEvents txtIdLlantaDES As TextBox
    Friend WithEvents Label27 As Label
    Friend WithEvents txtProfundidadFINAL_DES As TextBox
    Friend WithEvents Label26 As Label
    Friend WithEvents txtProfundidadFINAL As TextBox
    Dim opcActual As opServLlantas
    Private _IdLLanta As String
    'Private _idMovimiento As opServLlantas
    Private _idMovimiento As Integer


#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer
    Friend WithEvents ToolStripMenu As System.Windows.Forms.ToolStrip
    Friend WithEvents txtMenuBusca As System.Windows.Forms.ToolStripTextBox


    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(fServInternosLlantas))
        Me.ToolStripMenu = New System.Windows.Forms.ToolStrip()
        Me.txtMenuBusca = New System.Windows.Forms.ToolStripTextBox()
        Me.btnMnuModificar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuOk = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuCancelar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuSalir = New System.Windows.Forms.ToolStripButton()
        Me.MeStatus1 = New ControlStatus.MeStatus()
        Me.GridTodos = New System.Windows.Forms.DataGridView()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.GpoPosiciones = New System.Windows.Forms.GroupBox()
        'Me.ShapeContainer2 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        'Me.LineShape1 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.GrupoCaptura = New System.Windows.Forms.GroupBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.txtObserva = New System.Windows.Forms.TextBox()
        Me.gpoDestino = New System.Windows.Forms.GroupBox()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.txtProfundidadFINAL_DES = New System.Windows.Forms.TextBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.txtIdLlantaDES = New System.Windows.Forms.TextBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.txtProfundidadDES = New System.Windows.Forms.TextBox()
        Me.txtTipoUniDES = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtPosicionDES = New System.Windows.Forms.NumericUpDown()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.cmbidUbicacionDES = New System.Windows.Forms.ComboBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.cmbidtipoUbicacionDES = New System.Windows.Forms.ComboBox()
        Me.gpoOrigen = New System.Windows.Forms.GroupBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.txtProfundidadFINAL = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtProfundidadAct = New System.Windows.Forms.TextBox()
        Me.txtTipoUni = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtPosicion = New System.Windows.Forms.NumericUpDown()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.cmbidUbicacion = New System.Windows.Forms.ComboBox()
        Me.lblUbicacion = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.cmbidtipoUbicacion = New System.Windows.Forms.ComboBox()
        Me.cmbActividades = New System.Windows.Forms.ComboBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.gpoProdLlanta = New System.Windows.Forms.GroupBox()
        Me.cmbidCondicion = New System.Windows.Forms.ComboBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.chkActivo = New System.Windows.Forms.CheckBox()
        Me.gpoRevitalizado = New System.Windows.Forms.GroupBox()
        Me.chkRevitalizado = New System.Windows.Forms.CheckBox()
        Me.cmbidDisenioRev = New System.Windows.Forms.ComboBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.cmbidTipoLLanta = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtProfundidad = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtMedida = New System.Windows.Forms.TextBox()
        Me.cmbidDisenio = New System.Windows.Forms.ComboBox()
        Me.label2 = New System.Windows.Forms.Label()
        Me.cmbidMarca = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cmbidProductoLlanta = New System.Windows.Forms.ComboBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.cmbidEmpresa = New System.Windows.Forms.ComboBox()
        Me.label1 = New System.Windows.Forms.Label()
        Me.txtidLlanta = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.ToolStripMenu.SuspendLayout()
        CType(Me.GridTodos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.GpoPosiciones.SuspendLayout()
        Me.GrupoCaptura.SuspendLayout()
        Me.gpoDestino.SuspendLayout()
        CType(Me.txtPosicionDES, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gpoOrigen.SuspendLayout()
        CType(Me.txtPosicion, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gpoProdLlanta.SuspendLayout()
        Me.gpoRevitalizado.SuspendLayout()
        Me.SuspendLayout()
        '
        'ToolStripMenu
        '
        Me.ToolStripMenu.AutoSize = False
        Me.ToolStripMenu.BackColor = System.Drawing.Color.Silver
        Me.ToolStripMenu.Dock = System.Windows.Forms.DockStyle.None
        Me.ToolStripMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.txtMenuBusca, Me.btnMnuModificar, Me.btnMnuOk, Me.btnMnuCancelar, Me.btnMnuSalir})
        Me.ToolStripMenu.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow
        Me.ToolStripMenu.Location = New System.Drawing.Point(3, 0)
        Me.ToolStripMenu.Name = "ToolStripMenu"
        Me.ToolStripMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.ToolStripMenu.Size = New System.Drawing.Size(1186, 42)
        Me.ToolStripMenu.TabIndex = 100
        Me.ToolStripMenu.Text = "ToolStrip1"
        '
        'txtMenuBusca
        '
        Me.txtMenuBusca.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.txtMenuBusca.Name = "txtMenuBusca"
        Me.txtMenuBusca.Size = New System.Drawing.Size(150, 42)
        Me.txtMenuBusca.ToolTipText = "Filtra Listado"
        '
        'btnMnuModificar
        '
        Me.btnMnuModificar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuModificar.Image = Global.Fletera.My.Resources.Resources.Tool
        Me.btnMnuModificar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuModificar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuModificar.Name = "btnMnuModificar"
        Me.btnMnuModificar.Size = New System.Drawing.Size(52, 39)
        Me.btnMnuModificar.Text = "&Servicio"
        Me.btnMnuModificar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnMnuModificar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuOk
        '
        Me.btnMnuOk.ForeColor = System.Drawing.Color.Red
        Me.btnMnuOk.Image = CType(resources.GetObject("btnMnuOk.Image"), System.Drawing.Image)
        Me.btnMnuOk.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuOk.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuOk.Name = "btnMnuOk"
        Me.btnMnuOk.Size = New System.Drawing.Size(56, 39)
        Me.btnMnuOk.Text = "&GRABAR"
        Me.btnMnuOk.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuCancelar
        '
        Me.btnMnuCancelar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuCancelar.Image = CType(resources.GetObject("btnMnuCancelar.Image"), System.Drawing.Image)
        Me.btnMnuCancelar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuCancelar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuCancelar.Name = "btnMnuCancelar"
        Me.btnMnuCancelar.Size = New System.Drawing.Size(71, 39)
        Me.btnMnuCancelar.Text = "&CANCELAR"
        Me.btnMnuCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuSalir
        '
        Me.btnMnuSalir.ForeColor = System.Drawing.Color.Red
        Me.btnMnuSalir.Image = CType(resources.GetObject("btnMnuSalir.Image"), System.Drawing.Image)
        Me.btnMnuSalir.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuSalir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuSalir.Name = "btnMnuSalir"
        Me.btnMnuSalir.Size = New System.Drawing.Size(41, 39)
        Me.btnMnuSalir.Text = "&SALIR"
        Me.btnMnuSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'MeStatus1
        '
        Me.MeStatus1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.MeStatus1.Location = New System.Drawing.Point(0, 558)
        Me.MeStatus1.Margin = New System.Windows.Forms.Padding(4)
        Me.MeStatus1.Name = "MeStatus1"
        Me.MeStatus1.Size = New System.Drawing.Size(1171, 26)
        Me.MeStatus1.TabIndex = 120
        '
        'GridTodos
        '
        Me.GridTodos.AllowUserToAddRows = False
        Me.GridTodos.AllowUserToDeleteRows = False
        Me.GridTodos.AllowUserToResizeRows = False
        Me.GridTodos.BackgroundColor = System.Drawing.Color.White
        Me.GridTodos.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.GridTodos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.GridTodos.Enabled = False
        Me.GridTodos.Location = New System.Drawing.Point(3, 45)
        Me.GridTodos.MultiSelect = False
        Me.GridTodos.Name = "GridTodos"
        Me.GridTodos.ReadOnly = True
        Me.GridTodos.RowHeadersVisible = False
        Me.GridTodos.Size = New System.Drawing.Size(162, 437)
        Me.GridTodos.TabIndex = 105
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Location = New System.Drawing.Point(171, 45)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(988, 506)
        Me.TabControl1.TabIndex = 101
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.GpoPosiciones)
        Me.TabPage1.Controls.Add(Me.GrupoCaptura)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(980, 480)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Datos"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'GpoPosiciones
        '
        'Me.GpoPosiciones.Controls.Add(Me.ShapeContainer2)
        'Me.GpoPosiciones.Location = New System.Drawing.Point(536, 6)
        'Me.GpoPosiciones.Name = "GpoPosiciones"
        'Me.GpoPosiciones.Size = New System.Drawing.Size(427, 349)
        'Me.GpoPosiciones.TabIndex = 107
        'Me.GpoPosiciones.TabStop = False
        'Me.GpoPosiciones.Text = "Posiciones"
        '
        'ShapeContainer2
        '
        'Me.ShapeContainer2.Location = New System.Drawing.Point(3, 16)
        'Me.ShapeContainer2.Margin = New System.Windows.Forms.Padding(0)
        'Me.ShapeContainer2.Name = "ShapeContainer2"
        'Me.ShapeContainer2.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.LineShape1})
        'Me.ShapeContainer2.Size = New System.Drawing.Size(421, 330)
        'Me.ShapeContainer2.TabIndex = 2
        'Me.ShapeContainer2.TabStop = False
        ''
        ''LineShape1
        ''
        'Me.LineShape1.BorderWidth = 10
        'Me.LineShape1.Name = "LineaPrinc"
        'Me.LineShape1.X1 = 186
        'Me.LineShape1.X2 = 186
        'Me.LineShape1.Y1 = 36
        'Me.LineShape1.Y2 = 280
        '
        'GrupoCaptura
        '
        Me.GrupoCaptura.Controls.Add(Me.Label22)
        Me.GrupoCaptura.Controls.Add(Me.txtObserva)
        Me.GrupoCaptura.Controls.Add(Me.gpoDestino)
        Me.GrupoCaptura.Controls.Add(Me.gpoOrigen)
        Me.GrupoCaptura.Controls.Add(Me.cmbActividades)
        Me.GrupoCaptura.Controls.Add(Me.Label9)
        Me.GrupoCaptura.Controls.Add(Me.gpoProdLlanta)
        Me.GrupoCaptura.Controls.Add(Me.cmbidEmpresa)
        Me.GrupoCaptura.Controls.Add(Me.label1)
        Me.GrupoCaptura.Controls.Add(Me.txtidLlanta)
        Me.GrupoCaptura.Controls.Add(Me.Label7)
        Me.GrupoCaptura.Location = New System.Drawing.Point(6, 6)
        Me.GrupoCaptura.Name = "GrupoCaptura"
        Me.GrupoCaptura.Size = New System.Drawing.Size(524, 468)
        Me.GrupoCaptura.TabIndex = 150
        Me.GrupoCaptura.TabStop = False
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label22.Location = New System.Drawing.Point(2, 435)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(53, 13)
        Me.Label22.TabIndex = 153
        Me.Label22.Text = "Observa.:"
        '
        'txtObserva
        '
        Me.txtObserva.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObserva.Location = New System.Drawing.Point(69, 430)
        Me.txtObserva.MaxLength = 150
        Me.txtObserva.Name = "txtObserva"
        Me.txtObserva.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtObserva.Size = New System.Drawing.Size(442, 20)
        Me.txtObserva.TabIndex = 152
        '
        'gpoDestino
        '
        Me.gpoDestino.Controls.Add(Me.Label27)
        Me.gpoDestino.Controls.Add(Me.txtProfundidadFINAL_DES)
        Me.gpoDestino.Controls.Add(Me.Label24)
        Me.gpoDestino.Controls.Add(Me.txtIdLlantaDES)
        Me.gpoDestino.Controls.Add(Me.Label23)
        Me.gpoDestino.Controls.Add(Me.txtProfundidadDES)
        Me.gpoDestino.Controls.Add(Me.txtTipoUniDES)
        Me.gpoDestino.Controls.Add(Me.Label10)
        Me.gpoDestino.Controls.Add(Me.txtPosicionDES)
        Me.gpoDestino.Controls.Add(Me.Label11)
        Me.gpoDestino.Controls.Add(Me.cmbidUbicacionDES)
        Me.gpoDestino.Controls.Add(Me.Label15)
        Me.gpoDestino.Controls.Add(Me.Label19)
        Me.gpoDestino.Controls.Add(Me.cmbidtipoUbicacionDES)
        Me.gpoDestino.ForeColor = System.Drawing.Color.Blue
        Me.gpoDestino.Location = New System.Drawing.Point(3, 334)
        Me.gpoDestino.Name = "gpoDestino"
        Me.gpoDestino.Size = New System.Drawing.Size(512, 90)
        Me.gpoDestino.TabIndex = 151
        Me.gpoDestino.TabStop = False
        Me.gpoDestino.Text = "DESTINO"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label27.Location = New System.Drawing.Point(238, 43)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(86, 13)
        Me.Label27.TabIndex = 165
        Me.Label27.Text = "Profundidad Act."
        '
        'txtProfundidadFINAL_DES
        '
        Me.txtProfundidadFINAL_DES.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtProfundidadFINAL_DES.Location = New System.Drawing.Point(327, 40)
        Me.txtProfundidadFINAL_DES.MaxLength = 50
        Me.txtProfundidadFINAL_DES.Name = "txtProfundidadFINAL_DES"
        Me.txtProfundidadFINAL_DES.Size = New System.Drawing.Size(45, 20)
        Me.txtProfundidadFINAL_DES.TabIndex = 164
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label24.Location = New System.Drawing.Point(21, 71)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(53, 13)
        Me.Label24.TabIndex = 161
        Me.Label24.Text = "ID Llanta:"
        '
        'txtIdLlantaDES
        '
        Me.txtIdLlantaDES.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtIdLlantaDES.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIdLlantaDES.ForeColor = System.Drawing.Color.Blue
        Me.txtIdLlantaDES.Location = New System.Drawing.Point(79, 66)
        Me.txtIdLlantaDES.MaxLength = 10
        Me.txtIdLlantaDES.Name = "txtIdLlantaDES"
        Me.txtIdLlantaDES.ReadOnly = True
        Me.txtIdLlantaDES.Size = New System.Drawing.Size(112, 22)
        Me.txtIdLlantaDES.TabIndex = 160
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label23.Location = New System.Drawing.Point(100, 43)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(86, 13)
        Me.Label23.TabIndex = 159
        Me.Label23.Text = "Ult. Profundidad:"
        '
        'txtProfundidadDES
        '
        Me.txtProfundidadDES.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtProfundidadDES.Location = New System.Drawing.Point(188, 40)
        Me.txtProfundidadDES.MaxLength = 50
        Me.txtProfundidadDES.Name = "txtProfundidadDES"
        Me.txtProfundidadDES.Size = New System.Drawing.Size(45, 20)
        Me.txtProfundidadDES.TabIndex = 158
        '
        'txtTipoUniDES
        '
        Me.txtTipoUniDES.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTipoUniDES.Location = New System.Drawing.Point(419, 40)
        Me.txtTipoUniDES.MaxLength = 100
        Me.txtTipoUniDES.Name = "txtTipoUniDES"
        Me.txtTipoUniDES.ReadOnly = True
        Me.txtTipoUniDES.Size = New System.Drawing.Size(90, 20)
        Me.txtTipoUniDES.TabIndex = 154
        Me.txtTipoUniDES.Tag = "1"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label10.Location = New System.Drawing.Point(371, 43)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(50, 13)
        Me.Label10.TabIndex = 155
        Me.Label10.Text = "Tipo Uni:"
        '
        'txtPosicionDES
        '
        Me.txtPosicionDES.Location = New System.Drawing.Point(55, 40)
        Me.txtPosicionDES.Name = "txtPosicionDES"
        Me.txtPosicionDES.Size = New System.Drawing.Size(39, 20)
        Me.txtPosicionDES.TabIndex = 152
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label11.Location = New System.Drawing.Point(3, 43)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(50, 13)
        Me.Label11.TabIndex = 153
        Me.Label11.Text = "Posici�n:"
        '
        'cmbidUbicacionDES
        '
        Me.cmbidUbicacionDES.AutoCompleteCustomSource.AddRange(New String() {"Regularizaci�n", "Vivienda"})
        Me.cmbidUbicacionDES.FormattingEnabled = True
        Me.cmbidUbicacionDES.Location = New System.Drawing.Point(284, 16)
        Me.cmbidUbicacionDES.Name = "cmbidUbicacionDES"
        Me.cmbidUbicacionDES.Size = New System.Drawing.Size(225, 21)
        Me.cmbidUbicacionDES.TabIndex = 150
        '
        'Label15
        '
        Me.Label15.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label15.Location = New System.Drawing.Point(212, 19)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(55, 13)
        Me.Label15.TabIndex = 151
        Me.Label15.Text = "Ubicacion"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label19.Location = New System.Drawing.Point(15, 16)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(58, 13)
        Me.Label19.TabIndex = 149
        Me.Label19.Text = "Ubicaci�n:"
        '
        'cmbidtipoUbicacionDES
        '
        Me.cmbidtipoUbicacionDES.AutoCompleteCustomSource.AddRange(New String() {"Regularizaci�n", "Vivienda"})
        Me.cmbidtipoUbicacionDES.FormattingEnabled = True
        Me.cmbidtipoUbicacionDES.Location = New System.Drawing.Point(79, 13)
        Me.cmbidtipoUbicacionDES.Name = "cmbidtipoUbicacionDES"
        Me.cmbidtipoUbicacionDES.Size = New System.Drawing.Size(128, 21)
        Me.cmbidtipoUbicacionDES.TabIndex = 148
        '
        'gpoOrigen
        '
        Me.gpoOrigen.Controls.Add(Me.Label26)
        Me.gpoOrigen.Controls.Add(Me.txtProfundidadFINAL)
        Me.gpoOrigen.Controls.Add(Me.Label14)
        Me.gpoOrigen.Controls.Add(Me.txtProfundidadAct)
        Me.gpoOrigen.Controls.Add(Me.txtTipoUni)
        Me.gpoOrigen.Controls.Add(Me.Label12)
        Me.gpoOrigen.Controls.Add(Me.txtPosicion)
        Me.gpoOrigen.Controls.Add(Me.Label13)
        Me.gpoOrigen.Controls.Add(Me.cmbidUbicacion)
        Me.gpoOrigen.Controls.Add(Me.lblUbicacion)
        Me.gpoOrigen.Controls.Add(Me.Label21)
        Me.gpoOrigen.Controls.Add(Me.cmbidtipoUbicacion)
        Me.gpoOrigen.ForeColor = System.Drawing.Color.Red
        Me.gpoOrigen.Location = New System.Drawing.Point(6, 263)
        Me.gpoOrigen.Name = "gpoOrigen"
        Me.gpoOrigen.Size = New System.Drawing.Size(512, 65)
        Me.gpoOrigen.TabIndex = 150
        Me.gpoOrigen.TabStop = False
        Me.gpoOrigen.Text = "ORIGEN"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label26.Location = New System.Drawing.Point(238, 43)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(86, 13)
        Me.Label26.TabIndex = 159
        Me.Label26.Text = "Profundidad Act."
        '
        'txtProfundidadFINAL
        '
        Me.txtProfundidadFINAL.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtProfundidadFINAL.Location = New System.Drawing.Point(327, 40)
        Me.txtProfundidadFINAL.MaxLength = 50
        Me.txtProfundidadFINAL.Name = "txtProfundidadFINAL"
        Me.txtProfundidadFINAL.Size = New System.Drawing.Size(45, 20)
        Me.txtProfundidadFINAL.TabIndex = 158
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label14.Location = New System.Drawing.Point(100, 43)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(86, 13)
        Me.Label14.TabIndex = 157
        Me.Label14.Text = "Ult. Profundidad:"
        '
        'txtProfundidadAct
        '
        Me.txtProfundidadAct.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtProfundidadAct.Location = New System.Drawing.Point(188, 40)
        Me.txtProfundidadAct.MaxLength = 50
        Me.txtProfundidadAct.Name = "txtProfundidadAct"
        Me.txtProfundidadAct.Size = New System.Drawing.Size(45, 20)
        Me.txtProfundidadAct.TabIndex = 156
        '
        'txtTipoUni
        '
        Me.txtTipoUni.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTipoUni.Location = New System.Drawing.Point(419, 40)
        Me.txtTipoUni.MaxLength = 100
        Me.txtTipoUni.Name = "txtTipoUni"
        Me.txtTipoUni.ReadOnly = True
        Me.txtTipoUni.Size = New System.Drawing.Size(90, 20)
        Me.txtTipoUni.TabIndex = 154
        Me.txtTipoUni.Tag = "1"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label12.Location = New System.Drawing.Point(371, 43)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(50, 13)
        Me.Label12.TabIndex = 155
        Me.Label12.Text = "Tipo Uni:"
        '
        'txtPosicion
        '
        Me.txtPosicion.Location = New System.Drawing.Point(55, 40)
        Me.txtPosicion.Name = "txtPosicion"
        Me.txtPosicion.Size = New System.Drawing.Size(39, 20)
        Me.txtPosicion.TabIndex = 152
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label13.Location = New System.Drawing.Point(3, 43)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(50, 13)
        Me.Label13.TabIndex = 153
        Me.Label13.Text = "Posici�n:"
        '
        'cmbidUbicacion
        '
        Me.cmbidUbicacion.AutoCompleteCustomSource.AddRange(New String() {"Regularizaci�n", "Vivienda"})
        Me.cmbidUbicacion.FormattingEnabled = True
        Me.cmbidUbicacion.Location = New System.Drawing.Point(284, 16)
        Me.cmbidUbicacion.Name = "cmbidUbicacion"
        Me.cmbidUbicacion.Size = New System.Drawing.Size(225, 21)
        Me.cmbidUbicacion.TabIndex = 150
        '
        'lblUbicacion
        '
        Me.lblUbicacion.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lblUbicacion.Location = New System.Drawing.Point(212, 19)
        Me.lblUbicacion.Name = "lblUbicacion"
        Me.lblUbicacion.Size = New System.Drawing.Size(55, 13)
        Me.lblUbicacion.TabIndex = 151
        Me.lblUbicacion.Text = "Ubicacion"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label21.Location = New System.Drawing.Point(15, 16)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(58, 13)
        Me.Label21.TabIndex = 149
        Me.Label21.Text = "Ubicaci�n:"
        '
        'cmbidtipoUbicacion
        '
        Me.cmbidtipoUbicacion.AutoCompleteCustomSource.AddRange(New String() {"Regularizaci�n", "Vivienda"})
        Me.cmbidtipoUbicacion.FormattingEnabled = True
        Me.cmbidtipoUbicacion.Location = New System.Drawing.Point(79, 13)
        Me.cmbidtipoUbicacion.Name = "cmbidtipoUbicacion"
        Me.cmbidtipoUbicacion.Size = New System.Drawing.Size(128, 21)
        Me.cmbidtipoUbicacion.TabIndex = 148
        '
        'cmbActividades
        '
        Me.cmbActividades.AutoCompleteCustomSource.AddRange(New String() {"Regularizaci�n", "Vivienda"})
        Me.cmbActividades.FormattingEnabled = True
        Me.cmbActividades.Location = New System.Drawing.Point(69, 236)
        Me.cmbActividades.Name = "cmbActividades"
        Me.cmbActividades.Size = New System.Drawing.Size(193, 21)
        Me.cmbActividades.TabIndex = 148
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label9.Location = New System.Drawing.Point(5, 239)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(64, 13)
        Me.Label9.TabIndex = 149
        Me.Label9.Text = "Movimiento:"
        '
        'gpoProdLlanta
        '
        Me.gpoProdLlanta.Controls.Add(Me.cmbidCondicion)
        Me.gpoProdLlanta.Controls.Add(Me.Label20)
        Me.gpoProdLlanta.Controls.Add(Me.Label8)
        Me.gpoProdLlanta.Controls.Add(Me.chkActivo)
        Me.gpoProdLlanta.Controls.Add(Me.gpoRevitalizado)
        Me.gpoProdLlanta.Controls.Add(Me.cmbidTipoLLanta)
        Me.gpoProdLlanta.Controls.Add(Me.Label3)
        Me.gpoProdLlanta.Controls.Add(Me.Label6)
        Me.gpoProdLlanta.Controls.Add(Me.txtProfundidad)
        Me.gpoProdLlanta.Controls.Add(Me.Label5)
        Me.gpoProdLlanta.Controls.Add(Me.txtMedida)
        Me.gpoProdLlanta.Controls.Add(Me.cmbidDisenio)
        Me.gpoProdLlanta.Controls.Add(Me.label2)
        Me.gpoProdLlanta.Controls.Add(Me.cmbidMarca)
        Me.gpoProdLlanta.Controls.Add(Me.Label4)
        Me.gpoProdLlanta.Controls.Add(Me.cmbidProductoLlanta)
        Me.gpoProdLlanta.Controls.Add(Me.Label16)
        Me.gpoProdLlanta.ForeColor = System.Drawing.Color.Black
        Me.gpoProdLlanta.Location = New System.Drawing.Point(3, 37)
        Me.gpoProdLlanta.Name = "gpoProdLlanta"
        Me.gpoProdLlanta.Size = New System.Drawing.Size(515, 193)
        Me.gpoProdLlanta.TabIndex = 125
        Me.gpoProdLlanta.TabStop = False
        Me.gpoProdLlanta.Text = "Caracter�sticas"
        '
        'cmbidCondicion
        '
        Me.cmbidCondicion.AutoCompleteCustomSource.AddRange(New String() {"Regularizaci�n", "Vivienda"})
        Me.cmbidCondicion.FormattingEnabled = True
        Me.cmbidCondicion.Location = New System.Drawing.Point(388, 129)
        Me.cmbidCondicion.Name = "cmbidCondicion"
        Me.cmbidCondicion.Size = New System.Drawing.Size(112, 21)
        Me.cmbidCondicion.TabIndex = 146
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label20.Location = New System.Drawing.Point(273, 132)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(86, 13)
        Me.Label20.TabIndex = 150
        Me.Label20.Text = "Condici�n Llanta"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label8.Location = New System.Drawing.Point(414, 156)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(40, 13)
        Me.Label8.TabIndex = 145
        Me.Label8.Text = "Activo:"
        '
        'chkActivo
        '
        Me.chkActivo.AutoSize = True
        Me.chkActivo.Location = New System.Drawing.Point(484, 156)
        Me.chkActivo.Name = "chkActivo"
        Me.chkActivo.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.chkActivo.Size = New System.Drawing.Size(15, 14)
        Me.chkActivo.TabIndex = 14
        Me.chkActivo.UseVisualStyleBackColor = True
        '
        'gpoRevitalizado
        '
        Me.gpoRevitalizado.Controls.Add(Me.chkRevitalizado)
        Me.gpoRevitalizado.Controls.Add(Me.cmbidDisenioRev)
        Me.gpoRevitalizado.Controls.Add(Me.Label17)
        Me.gpoRevitalizado.ForeColor = System.Drawing.Color.Black
        Me.gpoRevitalizado.Location = New System.Drawing.Point(6, 129)
        Me.gpoRevitalizado.Name = "gpoRevitalizado"
        Me.gpoRevitalizado.Size = New System.Drawing.Size(256, 53)
        Me.gpoRevitalizado.TabIndex = 148
        Me.gpoRevitalizado.TabStop = False
        Me.gpoRevitalizado.Text = "Revitalizado"
        '
        'chkRevitalizado
        '
        Me.chkRevitalizado.AutoSize = True
        Me.chkRevitalizado.Location = New System.Drawing.Point(6, 22)
        Me.chkRevitalizado.Name = "chkRevitalizado"
        Me.chkRevitalizado.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.chkRevitalizado.Size = New System.Drawing.Size(15, 14)
        Me.chkRevitalizado.TabIndex = 137
        Me.chkRevitalizado.UseVisualStyleBackColor = True
        '
        'cmbidDisenioRev
        '
        Me.cmbidDisenioRev.AutoCompleteCustomSource.AddRange(New String() {"Regularizaci�n", "Vivienda"})
        Me.cmbidDisenioRev.FormattingEnabled = True
        Me.cmbidDisenioRev.Location = New System.Drawing.Point(91, 19)
        Me.cmbidDisenioRev.Name = "cmbidDisenioRev"
        Me.cmbidDisenioRev.Size = New System.Drawing.Size(159, 21)
        Me.cmbidDisenioRev.TabIndex = 118
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label17.Location = New System.Drawing.Point(39, 22)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(43, 13)
        Me.Label17.TabIndex = 136
        Me.Label17.Text = "Dise�o:"
        '
        'cmbidTipoLLanta
        '
        Me.cmbidTipoLLanta.AutoCompleteCustomSource.AddRange(New String() {"Regularizaci�n", "Vivienda"})
        Me.cmbidTipoLLanta.FormattingEnabled = True
        Me.cmbidTipoLLanta.Location = New System.Drawing.Point(70, 99)
        Me.cmbidTipoLLanta.Name = "cmbidTipoLLanta"
        Me.cmbidTipoLLanta.Size = New System.Drawing.Size(155, 21)
        Me.cmbidTipoLLanta.TabIndex = 144
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label3.Location = New System.Drawing.Point(3, 99)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(63, 13)
        Me.Label3.TabIndex = 147
        Me.Label3.Text = "Tipo Llanta:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label6.Location = New System.Drawing.Point(241, 76)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(67, 13)
        Me.Label6.TabIndex = 133
        Me.Label6.Text = "Profundidad:"
        '
        'txtProfundidad
        '
        Me.txtProfundidad.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtProfundidad.Enabled = False
        Me.txtProfundidad.Location = New System.Drawing.Point(361, 73)
        Me.txtProfundidad.MaxLength = 50
        Me.txtProfundidad.Name = "txtProfundidad"
        Me.txtProfundidad.Size = New System.Drawing.Size(112, 20)
        Me.txtProfundidad.TabIndex = 110
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label5.Location = New System.Drawing.Point(6, 76)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(45, 13)
        Me.Label5.TabIndex = 131
        Me.Label5.Text = "Medida:"
        '
        'txtMedida
        '
        Me.txtMedida.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMedida.Enabled = False
        Me.txtMedida.Location = New System.Drawing.Point(67, 73)
        Me.txtMedida.MaxLength = 50
        Me.txtMedida.Name = "txtMedida"
        Me.txtMedida.Size = New System.Drawing.Size(112, 20)
        Me.txtMedida.TabIndex = 109
        '
        'cmbidDisenio
        '
        Me.cmbidDisenio.AutoCompleteCustomSource.AddRange(New String() {"Regularizaci�n", "Vivienda"})
        Me.cmbidDisenio.Enabled = False
        Me.cmbidDisenio.FormattingEnabled = True
        Me.cmbidDisenio.Location = New System.Drawing.Point(314, 41)
        Me.cmbidDisenio.Name = "cmbidDisenio"
        Me.cmbidDisenio.Size = New System.Drawing.Size(159, 21)
        Me.cmbidDisenio.TabIndex = 108
        '
        'label2
        '
        Me.label2.AutoSize = True
        Me.label2.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.label2.Location = New System.Drawing.Point(245, 49)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(43, 13)
        Me.label2.TabIndex = 128
        Me.label2.Text = "Dise�o:"
        '
        'cmbidMarca
        '
        Me.cmbidMarca.AutoCompleteCustomSource.AddRange(New String() {"Regularizaci�n", "Vivienda"})
        Me.cmbidMarca.Enabled = False
        Me.cmbidMarca.FormattingEnabled = True
        Me.cmbidMarca.Location = New System.Drawing.Point(67, 46)
        Me.cmbidMarca.Name = "cmbidMarca"
        Me.cmbidMarca.Size = New System.Drawing.Size(155, 21)
        Me.cmbidMarca.TabIndex = 107
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label4.Location = New System.Drawing.Point(18, 49)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(40, 13)
        Me.Label4.TabIndex = 126
        Me.Label4.Text = "Marca:"
        '
        'cmbidProductoLlanta
        '
        Me.cmbidProductoLlanta.AutoCompleteCustomSource.AddRange(New String() {"Regularizaci�n", "Vivienda"})
        Me.cmbidProductoLlanta.FormattingEnabled = True
        Me.cmbidProductoLlanta.Location = New System.Drawing.Point(67, 19)
        Me.cmbidProductoLlanta.Name = "cmbidProductoLlanta"
        Me.cmbidProductoLlanta.Size = New System.Drawing.Size(156, 21)
        Me.cmbidProductoLlanta.TabIndex = 7
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label16.Location = New System.Drawing.Point(3, 22)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(62, 13)
        Me.Label16.TabIndex = 124
        Me.Label16.Text = "Id Producto"
        '
        'cmbidEmpresa
        '
        Me.cmbidEmpresa.AutoCompleteCustomSource.AddRange(New String() {"Regularizaci�n", "Vivienda"})
        Me.cmbidEmpresa.FormattingEnabled = True
        Me.cmbidEmpresa.Items.AddRange(New Object() {"MORAL", "FISICA"})
        Me.cmbidEmpresa.Location = New System.Drawing.Point(230, 12)
        Me.cmbidEmpresa.Name = "cmbidEmpresa"
        Me.cmbidEmpresa.Size = New System.Drawing.Size(115, 21)
        Me.cmbidEmpresa.TabIndex = 1
        '
        'label1
        '
        Me.label1.AutoSize = True
        Me.label1.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.label1.Location = New System.Drawing.Point(3, 17)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(53, 13)
        Me.label1.TabIndex = 92
        Me.label1.Text = "ID Llanta:"
        '
        'txtidLlanta
        '
        Me.txtidLlanta.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtidLlanta.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtidLlanta.ForeColor = System.Drawing.Color.Blue
        Me.txtidLlanta.Location = New System.Drawing.Point(61, 12)
        Me.txtidLlanta.MaxLength = 10
        Me.txtidLlanta.Name = "txtidLlanta"
        Me.txtidLlanta.Size = New System.Drawing.Size(112, 22)
        Me.txtidLlanta.TabIndex = 4
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label7.Location = New System.Drawing.Point(179, 16)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(51, 13)
        Me.Label7.TabIndex = 52
        Me.Label7.Text = "Empresa:"
        '
        'fServInternosLlantas
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(1171, 584)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.GridTodos)
        Me.Controls.Add(Me.MeStatus1)
        Me.Controls.Add(Me.ToolStripMenu)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "fServInternosLlantas"
        Me.Text = "Servicios Internos Llantas"
        Me.ToolStripMenu.ResumeLayout(False)
        Me.ToolStripMenu.PerformLayout()
        CType(Me.GridTodos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.GpoPosiciones.ResumeLayout(False)
        Me.GrupoCaptura.ResumeLayout(False)
        Me.GrupoCaptura.PerformLayout()
        Me.gpoDestino.ResumeLayout(False)
        Me.gpoDestino.PerformLayout()
        CType(Me.txtPosicionDES, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gpoOrigen.ResumeLayout(False)
        Me.gpoOrigen.PerformLayout()
        CType(Me.txtPosicion, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gpoProdLlanta.ResumeLayout(False)
        Me.gpoProdLlanta.PerformLayout()
        Me.gpoRevitalizado.ResumeLayout(False)
        Me.gpoRevitalizado.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

#End Region
    Sub New(ByVal vTag As String)
        InitializeComponent()
        _Tag = vTag
    End Sub

    Public Sub New(con As String, v As Boolean, nomUsuario As String, Optional ByVal idLlanta As String = "",
    Optional ByVal idmovimiento As Integer = 0)
        _con = con
        BD = New CapaDatos.UtilSQL(con, "Juan")
        InitializeComponent()
        _v = v
        _Usuario = nomUsuario
        _IdLLanta = idLlanta
        _idMovimiento = idmovimiento

    End Sub

#Region "MenusToolStrip"
    Private Sub LimpiaVariables()
        SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")

    End Sub
    Private Sub LimpiaCampos()
        txtidLlanta.Text = ""
        cmbidTipoLLanta.SelectedIndex = 0
        cmbidMarca.SelectedIndex = 0
        chkActivo.Checked = False
        txtMedida.Text = ""
        txtProfundidad.Text = ""

        txtTipoUni.Text = ""
        txtPosicion.Value = ""

        LimpiaVariables()
    End Sub

    Private Sub ActivaCampos(ByVal valor As Boolean, ByVal vOpcion As TipoOpcActivaCampos)
        If vOpcion = TipoOpcActivaCampos.tOpcDESHABTODOS Then
            cmbidEmpresa.Enabled = False
            cmbidtipoUbicacion.Enabled = False
            cmbidUbicacion.Enabled = False
            txtidLlanta.Enabled = True
            cmbidTipoLLanta.Enabled = False
            cmbidDisenio.Enabled = False
            txtMedida.Enabled = False
            txtProfundidad.Enabled = False
            cmbidMarca.Enabled = False
            chkActivo.Enabled = False
            GpoPosiciones.Enabled = False
            txtTipoUni.Enabled = False
            txtPosicion.Enabled = False
            cmbidDisenioRev.Enabled = False
        ElseIf vOpcion = TipoOpcActivaCampos.tOpcINICIALIZA Then
            cmbidEmpresa.Enabled = False
            cmbidtipoUbicacion.Enabled = False
            cmbidUbicacion.Enabled = False
            txtPosicion.Enabled = False
            cmbidCondicion.Enabled = False
            txtProfundidad.Enabled = False
            gpoRevitalizado.Enabled = False

            'txtNoSerie.Enabled = True
            gpoProdLlanta.Enabled = True
            cmbidProductoLlanta.Enabled = True
            cmbidTipoLLanta.Enabled = True
            chkActivo.Enabled = True
        ElseIf vOpcion = TipoOpcActivaCampos.tOpcOTRO Then
            cmbidEmpresa.Enabled = False
            cmbidtipoUbicacion.Enabled = False
            cmbidUbicacion.Enabled = False
            txtidLlanta.Enabled = True
            'txtNoSerie.Enabled = True
            txtPosicion.Enabled = True
            cmbidTipoLLanta.Enabled = True
            txtProfundidadAct.Enabled = True
            cmbidCondicion.Enabled = True
            chkActivo.Enabled = True
            gpoProdLlanta.Enabled = True
            gpoRevitalizado.Enabled = True
            cmbidDisenioRev.Enabled = False

        End If
    End Sub


    Private Sub ActivaBotones(ByVal valor As Boolean, Optional ByVal vOpcion As TipoOpcActivaBoton = TipoOpcActivaBoton.tOpcInicializa)
        GridTodos.Enabled = False
        If vOpcion = TipoOpcActivaBoton.tOpcConsulta Then
            btnMnuOk.Enabled = False
            btnMnuCancelar.Enabled = valor
            btnMnuSalir.Enabled = Not valor

        ElseIf vOpcion = TipoOpcActivaBoton.tOpcInicializa Then
            btnMnuOk.Enabled = valor
            btnMnuCancelar.Enabled = valor
            btnMnuSalir.Enabled = Not valor
            txtMenuBusca.Enabled = Not valor
            GridTodos.Enabled = Not valor
        ElseIf vOpcion = TipoOpcActivaBoton.tOpcInsertar Then
            btnMnuOk.Enabled = valor
            btnMnuCancelar.Enabled = valor
            btnMnuSalir.Enabled = Not valor
        End If
    End Sub






#End Region
    'Private Sub ActualizaGrid(ByVal vidLlanta As String, ByVal vidUnidadTrans As String)

    '    If vidLlanta = "" And vidUnidadTrans = "" Then
    '        GridTodos.DataSource = BD.ExecuteReturn("SELECT idLlanta, Posicion FROM " & TablaBd, TablaBd)
    '        Status(GridTodos.RowCount & " Registros", Me)
    '    Else
    '        '
    '        'GridTodos.DataSource = BD.ExecuteReturn("SELECT idLlanta FROM " & TablaBd & " WHERE idLlanta Like '%" & vidLlanta & "%'")
    '        GridTodos.DataSource = BD.ExecuteReturn("SELECT idLlanta, Posicion FROM " & TablaBd & " WHERE idLlanta Like '%" & vidLlanta &
    '                                                "%' and idUnidadTrans Like '%" & vidUnidadTrans & "%'")
    '        'GridTodos.DataSource = BD.ExecuteReturn("SELECT idLlanta FROM " & TablaBd & " WHERE idUnidadTrans Like '%" & vidLlanta & "%'")
    '        Status(GridTodos.RowCount & " Registros obtenidos con el filtro %" & vidLlanta & "%", Me)
    '    End If
    '    'GridTodos.Columns(0).Width = GridTodos.Width - 20
    '    GridTodos.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
    '    GridTodos.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill

    'End Sub
    Private Sub ActualizaGrid(ByVal vidLlanta As String)
        If vidLlanta = "" Then
            GridTodos.DataSource = BD.ExecuteReturn("SELECT idLlanta FROM " & TablaBd & " where idtipoUbicacion <> 5", TablaBd)
            Status(GridTodos.RowCount & " Registros", Me)
        Else
            GridTodos.DataSource = BD.ExecuteReturn("SELECT idLlanta FROM " & TablaBd &
                                                " WHERE idLlanta Like '%" & vidLlanta & "%'")
            Status(GridTodos.RowCount & " Registros obtenidos con el filtro %" & vidLlanta & "%", Me)
        End If
    End Sub

    Private Sub ActualizaGridxUnidad(ByVal vidUnidadTrans As String)
        If vidUnidadTrans = "" Then
            GridTodos.DataSource = BD.ExecuteReturn("SELECT idLlanta, Posicion FROM " & TablaBd, TablaBd)
            Status(GridTodos.RowCount & " Registros", Me)
        Else
            '
            'GridTodos.DataSource = BD.ExecuteReturn("SELECT idLlanta FROM " & TablaBd & " WHERE idLlanta Like '%" & vidLlanta & "%'")
            GridTodos.DataSource = BD.ExecuteReturn("SELECT idLlanta, Posicion FROM " & TablaBd & " WHERE idUbicacion Like '%" & vidUnidadTrans & "%'")
            Status(GridTodos.RowCount & " Registros obtenidos con el filtro %" & vidUnidadTrans & "%", Me)
        End If
        'GridTodos.Columns(0).Width = GridTodos.Width - 20
        GridTodos.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
        GridTodos.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill

    End Sub


    Private Sub OpcionMnuTools(ByVal vOpcion As TipoOpcionForma)
        If vOpcion <> TipoOpcionForma.tOpcImprimir And vOpcion <> TipoOpcionForma.tOpcEliminar Then
            OpcionForma = vOpcion
        End If
        txtMenuBusca.Enabled = True
        GridTodos.Enabled = True
        If vOpcion = TipoOpcionForma.tOpcConsultar Then
            'CONSULTA
            For Each ct As Control In GrupoCaptura.Controls
                ct.Enabled = False
                'If Not ct Is cmdBuscaClave Then
                '    ct.Enabled = False
                'Else
                '    ct.Enabled = True
                'End If
            Next
            GpoPosiciones.Enabled = False
            txtMenuBusca.Enabled = True
            btnMnuOk.Enabled = False
            btnMnuCancelar.Enabled = False
            btnMnuSalir.Enabled = True
            cmbidEmpresa.Enabled = False
            txtidLlanta.Enabled = True
            GeneraAutoacompletar()
            txtidLlanta.Focus()

        End If
    End Sub


    Private Sub txtMenuBusca_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtMenuBusca.KeyPress
        If Asc(e.KeyChar) = 39 Then e.Handled = True : Exit Sub 'Caracter ' Evita errores
        If Asc(e.KeyChar) = 13 Then
            GridTodos.Focus()
            e.Handled = True
        End If
    End Sub

    'Private Sub txtMenuBusca_KeyUp(sender As Object, e As KeyEventArgs) Handles txtMenuBusca.KeyUp
    '    If txtMenuBusca.Text.Trim = "" Then Exit Sub
    '    If e.KeyCode = Keys.Down Or e.KeyCode = Keys.Up Then
    '        GridTodos.Focus()
    '        e.Handled = True
    '        Exit Sub
    '    End If
    '    If e.KeyCode = Keys.Left Or e.KeyCode = Keys.Right Then Exit Sub
    '    ActualizaGrid(txtMenuBusca.Text)
    'End Sub

    Public Function AutoCompletar(ByVal Control As TextBox, ByVal strSql As String, ByVal NomCampo As String, ByVal NomTabla As String, Optional ByVal FiltroSinWhere As String = "")
        Dim objSql As New ToolSQLs
        Dim Coleccion As New AutoCompleteStringCollection
        Dim dt As DataTable = objSql.CreaTabla(NomTabla, strSql, FiltroSinWhere)
        For Each row As DataRow In dt.Rows
            Coleccion.Add(Convert.ToString(row(NomCampo)))
        Next
        With Control

            .AutoCompleteCustomSource = Coleccion
            .AutoCompleteMode = AutoCompleteMode.Suggest
            .AutoCompleteSource = AutoCompleteSource.CustomSource
        End With
        ' Aca le devuelvo los datos a recuperados de la Base
        Return Coleccion
    End Function

    Public Function AutoCompletarUnidadTransp(ByVal Control As ComboBox, ByVal Activo As Boolean, ByVal idEmpresa As Integer,
                                              ByVal Tipo As String, Optional ByVal FiltroSinWhere As String = "") As AutoCompleteStringCollection

        Dim Coleccion As New AutoCompleteStringCollection

        UniTrans = New UnidadesTranspClass(0)

        Dim dt As DataTable = UniTrans.TablaUnidadxTipo2(idEmpresa, FiltroSinWhere)
        'Recorrer y cargar los items para el Autocompletado
        For Each row As DataRow In dt.Rows
            Coleccion.Add(Convert.ToString(row("idUnidadTrans")))
        Next
        'Ajustamos el control TextBox o ComboBox para recibir los datos de la siguiente manera.
        With Control
            .DataSource = dt
            .DisplayMember = "idUnidadTrans"
            .ValueMember = "idUnidadTrans"


            .AutoCompleteCustomSource = Coleccion
            .AutoCompleteMode = AutoCompleteMode.Suggest
            .AutoCompleteSource = AutoCompleteSource.CustomSource
        End With

        ' Aca le devuelvo los datos a recuperados de la Base
        Return Coleccion
    End Function

    Private Sub GeneraAutoacompletar()
        'AutoCompletarUnidadTransp(txtUniTrans, True, 0, "tractor")
        'AutoCompletar(txtDise�o, "SELECT DISTINCT Dise�o AS dise�o FROM dbo.CatLLantasProducto", "Dise�o", "Dise�o")
        AutoCompletar(txtProfundidadAct, "SELECT DISTINCT Profundidad AS Profundidad FROM dbo.CatLLantasProducto", "Profundidad", "Profundidad")
        'AutoCompletar(txtMedida, "SELECT DISTINCT Medida AS Medida FROM dbo.CatLLantasProducto", "Medida", "Medida")

    End Sub
    Private Sub frmCatFamilias_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Salida = True
        'OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
        'If Inicio.CONSTR = "" Then
        '    Inicio.CONSTR = _con
        'End If
        'ActivaCampos(True, TipoOpcActivaCampos.tOpcDESHABTODOS)
        'GeneraAutoacompletar()
        'LlenaCombos()
        'GridTodos.Enabled = False
        'TabControl1.SelectedIndex = 0
        'SendKeys.Send("{TAB}")
        OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
        If Inicio.CONSTR = "" Then
            Inicio.CONSTR = _con
        End If
        ActivaCampos(True, TipoOpcActivaCampos.tOpcDESHABTODOS)
        'ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)
        GeneraAutoacompletar()
        LlenaCombos()
        ActualizaGrid("")

        If _IdLLanta <> "" Then
            txtidLlanta.Text = _IdLLanta
            GridTodos.Enabled = False
            'txtidLlanta_Leave(sender, e)
            CargaForm(txtidLlanta.Text, UCase(TablaBd))
            btnMnuModificar_Click(sender, e)
            '(opServLlantas)cmbActividades.SelectedValue = _idMovimiento
            cmbActividades.SelectedValue = _idMovimiento
            OpcionActividad()
        Else

        End If
        GridTodos.Columns(0).Width = GridTodos.Width - 20
        'GridTodos.Columns(1).Visible = False
        txtidLlanta.Focus()
        Salida = False
    End Sub
    Private Sub GridTodos_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles GridTodos.CellEnter
        'If Salida Then Exit Sub
        Salida = True
        CargaForm(GridTodos.Rows(GridTodos.CurrentRow.Index).Cells(0).Value, UCase(TablaBd))
        Salida = False
    End Sub
    Private Sub GridTodos_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles GridTodos.KeyPress
        If Asc(e.KeyChar) = 8 Then
            txtMenuBusca.Focus()
            e.Handled = True
        End If
    End Sub
    Private Sub CargaForm(ByVal idClave As String, ByVal NomTabla As String)
        Try
            Select Case UCase(NomTabla)
                Case UCase(TablaBd)
                    Llanta = New LlantasClass(idClave, OpcionLlanta.idLlanta)
                    With Llanta
                        txtidLlanta.Text = .idLlanta
                        If .Existe Then
                            Salida = True
                            idSisLlanta = .idSisLlanta
                            cmbidEmpresa.SelectedValue = .idEmpresa
                            'txtNoSerie.Text = .NoSerie

                            'CargaForm(.idProductoLlanta, "LLANTAPROD")
                            cmbidTipoLLanta.SelectedValue = .idTipoLLanta
                            txtProfundidadAct.Text = .ProfundidadAct
                            txtProfundidadFINAL.Text = .ProfundidadAct
                            cmbidCondicion.SelectedValue = .idCondicion
                            chkActivo.Checked = .Activo


                            cmbidProductoLlanta.SelectedValue = .idProductoLlanta
                            llantaProd = New LLantasProductoClass(Llanta.idProductoLlanta)
                            If llantaProd.Existe Then
                                cmbidMarca.SelectedValue = llantaProd.idMarca
                                cmbidDisenio.SelectedValue = llantaProd.idDisenio
                                txtMedida.Text = llantaProd.Medida
                                txtProfundidad.Text = llantaProd.Profundidad

                            End If

                            cmbidtipoUbicacion.SelectedValue = .idtipoUbicacion
                            AutoCompletarUbicaciones(.idtipoUbicacion, cmbidUbicacion)
                            vUnidadOrig = .idUbicacion
                            cmbidUbicacion.SelectedValue = .idUbicacion
                            If CType(.idtipoUbicacion, TipoUbicacionLLa) <> TipoUbicacionLLa.UNIDADTRANS Then

                                txtPosicion.Minimum = 0
                                txtPosicion.Value = .Posicion
                            Else
                                txtPosicion.Value = .Posicion
                            End If

                            vPosUniOrig = .Posicion
                            TipoUbicacion = New CatLugaresClass(.idtipoUbicacion)
                            If TipoUbicacion.Existe Then
                                'CargaForm(.idUbicacion, "CatUnidadTrans")
                                CargaForm(.idUbicacion, TipoUbicacion.Descripcion)

                            End If

                            'Revitalizado
                            If .idDisenioRev > 0 Then
                                cmbidDisenioRev.SelectedValue = .idDisenioRev
                                chkRevitalizado.Checked = True
                            Else
                                chkRevitalizado.Checked = False
                            End If

                            'txtCIDALMACEN_COM.Text = .CIDALMACEN_COM
                            'If OpcionForma = TipoOpcionForma.tOpcInsertar Then
                            '    Status("La Llanta Con id: " & .idLlanta &
                            '           " Ya existe!!!, Favor de modificarlo para poder dar de alta a otra Llanta", Me, Err:=True)
                            '    MessageBox.Show("La Llanta con id: " & .idLlanta &
                            '                    " YA EXISTE!!!, Favor de Cambiarlo y volver a intentarlo",
                            '                    "La Llanta con id: " & .idLlanta & " YA EXISTE!!!", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            'End If
                        Else 'No existe 
                            If OpcionForma = TipoOpcionForma.tOpcInsertar Then

                                ActivaCampos(True, TipoOpcActivaCampos.tOpcINICIALIZA)
                                ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)
                                btnMnuOk.Enabled = True
                                cmbidTipoLLanta.Focus()
                                Status("Listo para Guardar nueva Unidad con id: " & .idLlanta, Me)
                            End If
                            Salida = False
                        End If
                    End With
                'Case UCase(TablaBd2)
                '    'aqui()
                '    TipoUniTrans = New TipoUniTransClass(Val(idClave))
                '    With TipoUniTrans
                '        txtidTipoLLanta.Text = .idTipoUniTras
                '        If .Existe Then
                '            txtDescripTLLA.Text = .nomTipoUniTras
                '            txtDise�o.Focus()
                '            'txtDescripcionUni.SelectAll()
                '        Else
                '            MsgBox("El Tipo de Unidad con id: " & txtidTipoLLanta.Text & " No Existe")
                '            txtidTipoLLanta.Focus()
                '            txtidTipoLLanta.SelectAll()
                '        End If
                '    End With
                Case UCase("LLANTAPROD")
                    llantaProd = New LLantasProductoClass(idClave)
                    If llantaProd.Existe Then
                        cmbidMarca.SelectedValue = llantaProd.idMarca
                        cmbidDisenio.SelectedValue = llantaProd.idDisenio
                        txtMedida.Text = llantaProd.Medida
                        txtProfundidad.Text = llantaProd.Profundidad
                    End If

                Case UCase("CatEmpresas")
                    'OK
                    Empresa = New EmpresaClass(Val(idClave))
                    If Empresa.Existe Then
                        Prefijo = Empresa.PrefLla
                        'ConsecutivoLlantas
                        Dim ultLlantas As New LlantasClass("0", OpcionLlanta.idLlanta)

                        'UltimoConsecutivo = ultLlantas.ConsecutivoLlantas(Val(idClave), dtpFecha.Value.Year, dtpFecha.Value.Month)

                        AutoCompletarUnidadTransp(cmbidUbicacion, True, Val(idClave), "")
                    End If
                Case UCase("UNIDADES TRANS.")
                    UniTrans = New UnidadesTranspClass(idClave)
                    If UniTrans.Existe Then
                        TipUni = New TipoUniTransClass(UniTrans.idTipoUnidad)
                        If TipUni.Existe Then
                            txtTipoUni.Text = TipUni.Clasificacion
                            ClasLlan = New ClasLlanClass(TipUni.idClasLlan)
                            If ClasLlan.Existe Then

                                txtPosicion.Minimum = 1
                                txtPosicion.Maximum = TipUni.NoLlantas


                                DibujaEsquema(TipUni.idClasLlan, TipUni.NoLlantas)
                                If txtPosicion.Value <> 0 Then
                                    SeleccionaPosicion(Val(txtPosicion.Value), Color.Yellow)
                                End If
                            End If
                        End If
                    End If
                Case UCase("UNIDADES TRANS.DES")
                    UniTrans = New UnidadesTranspClass(idClave)
                    If UniTrans.Existe Then
                        TipUni = New TipoUniTransClass(UniTrans.idTipoUnidad)
                        If TipUni.Existe Then
                            txtTipoUniDES.Text = TipUni.Clasificacion
                            ClasLlan = New ClasLlanClass(TipUni.idClasLlan)
                            If ClasLlan.Existe Then
                                txtPosicionDES.Minimum = 1
                                txtPosicionDES.Maximum = TipUni.NoLlantas
                                txtPosicionDES.Value = 1

                                DibujaEsquema(TipUni.idClasLlan, TipUni.NoLlantas)
                                If txtPosicionDES.Value <> 0 Then
                                    Llanta = New LlantasClass(txtPosicionDES.Value, OpcionLlanta.Posicion, cmbidUbicacionDES.SelectedValue)
                                    If Llanta.Existe Then
                                        txtIdLlantaDES.Text = Llanta.idLlanta
                                        'txtNoSerieDES.Text = Llanta.NoSerie

                                    End If
                                    SeleccionaPosicion(Val(txtPosicion.Value), Color.Yellow, Val(txtPosicionDES.Value), Color.LightGreen)

                                End If
                            End If
                        End If
                    End If
            End Select
        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try

    End Sub
    Private Function Validar() As Boolean
        Validar = True
        If Not Inicio.ValidandoCampo(Trim(txtidLlanta.Text), TipoDato.TdCadena) Then
            'If Not bEsIdentidad Then
            MsgBox(MENSAJE_CLAVE, vbInformation, "Aviso" & Me.Text)
            If txtidLlanta.Enabled Then
                txtidLlanta.Focus()
                txtidLlanta.SelectAll()
            End If
            Validar = False
            'End If
            'ElseIf Not Inicio.ValidandoCampo(Trim(txtNoSerie.Text), TipoDato.TdCadena) Then
            '    MsgBox("EL No. de Serie Puede Ser Vacio", vbInformation, "Aviso" & Me.Text)
            '    If txtNoSerie.Enabled Then
            '        txtNoSerie.SelectAll()
            '        txtNoSerie.Focus()
            '    End If
            '    Validar = False
            'ElseIf cmbidDisenio.SelectedValue = 0 Then
            '    MsgBox("La Descripcion de la unidad No Puede Ser Vacio", vbInformation, "Aviso" & Me.Text)
            '    If cmbidDisenio.Enabled Then
            '        cmbidDisenio.Focus()
            '    End If
            '    Validar = False
        ElseIf Not Inicio.ValidandoCampo(Trim(txtProfundidadFINAL.Text), TipoDato.TdNumerico) Then
            MsgBox("La profundida no puede ser Vacio", vbInformation, "Aviso" & Me.Text)
            If txtProfundidadFINAL.Enabled Then
                txtProfundidadFINAL.SelectAll()
                txtProfundidadFINAL.Focus()
            End If
            Validar = False
        ElseIf Not Inicio.ValidandoCampo(Trim(txtProfundidadFINAL_DES.Text), TipoDato.TdNumerico) Then
            MsgBox("La profundida no puede ser Vacio", vbInformation, "Aviso" & Me.Text)
            If txtProfundidadFINAL_DES.Enabled Then
                txtProfundidadFINAL_DES.SelectAll()
                txtProfundidadFINAL_DES.Focus()
            End If
            Validar = False
        ElseIf cmbidTipoLLanta.SelectedValue = 0 Then
            MsgBox("El Tipo de Unidad no Puede ser Vacio", vbInformation, "Aviso" & Me.Text)
            If cmbidTipoLLanta.Enabled Then
                cmbidTipoLLanta.Focus()
            End If
            Validar = False
        ElseIf cmbidCondicion.SelectedValue = 0 Then
            MsgBox("La Condici�n no Puede ser Vacia", vbInformation, "Aviso" & Me.Text)
            If cmbidTipoLLanta.Enabled Then
                cmbidTipoLLanta.Focus()
            End If
            Validar = False
        End If
    End Function

    'Private Sub txtidAlmacen_EnabledChanged(sender As Object, e As EventArgs) Handles txtidAlmacen.EnabledChanged
    '    cmdBuscaClave.Enabled = txtidAlmacen.Enabled
    'End Sub
    'CONTROLES

    Private Sub txtidAlmacen_KeyPress(sender As Object, e As KeyPressEventArgs)
        If Asc(e.KeyChar) = 13 Then
            If txtidLlanta.Text.Trim <> "" Then
                CargaForm(txtidLlanta.Text, UCase(TablaBd))
            End If
            e.Handled = True
        End If
    End Sub





    'Private Sub txtidSucursal_EnabledChanged(sender As Object, e As EventArgs) Handles txtidSucursal.EnabledChanged
    '    cmdBuscaClaveTPU.Enabled = txtidTipoUnidad.Enabled
    'End Sub










    Private Sub cmdCancelar_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        With TryCast(sender, Button)
            If .Enabled Then
                Me.CancelButton = sender
            End If
        End With
    End Sub
    Private Sub cmdTerminar_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        With TryCast(sender, Button)
            If .Enabled Then
                Me.CancelButton = sender
            End If
        End With
    End Sub


    Private Sub btnMnuOk_Click(sender As Object, e As EventArgs) Handles btnMnuOk.Click
        Dim CadCam As String = ""
        Dim objTool As New ToolSQLs
        Dim Indentity As Integer
        Dim Indentity2 As Integer = 0
        Dim obj As New CapaNegocio.Tablas
        Dim Respuesta As String = ""
        Dim Respuesta2 As String

        Try
            Salida = True
            If Not Validar() Then Exit Sub
            If GridTodos.RowCount = 0 Then
                'Servicio = New ServicioClass(0)
                Llanta = New LlantasClass("", OpcionLlanta.idLlanta)
            End If
            With Llanta
                If OpcionForma = TipoOpcionForma.tOpcInsertar Then
                    .Existe = False
                    '.DescripcionUni = txtDise�o.Text
                    'If Not bEsIdentidad Then
                    '    .idLlanta = txtidLlanta.Text
                    'End If
                    Status("Guardando Nueva Llanta: " & .idLlanta & ". . .", Me, 1, 2)

                    'ORIGEN
                    'Insertar Movimiento 1 y obtener su id en Indentity
                    StrSql = objTool.InsertaMovimientos(cmbActividades.SelectedValue, Now, _Usuario, idSisLlanta,
                    cmbidtipoUbicacion.SelectedValue, cmbidUbicacion.SelectedValue, txtPosicion.Value, cmbidtipoUbicacionDES.SelectedValue,
                    cmbidUbicacionDES.SelectedValue, txtPosicionDES.Value, txtObserva.Text)
                    ReDim Preserve ArraySql(indice)
                    ArraySql(indice) = StrSql
                    indice += 1

                    If indice > 0 Then
                        Respuesta = obj.EjecutarSql(Inicio.CONSTR, "SQL", ArraySql, Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, indice, 0)
                        Respuesta2 = Respuesta.Substring(0, 9)
                        If Respuesta2 = "EFECTUADO" Then
                            Indentity = Respuesta.Substring(10, Respuesta.Length - 10)
                            indice = 0
                            If opcActual = opServLlantas.ROTAR_MISMA_UNIDAD Or opcActual = opServLlantas.CAMBIAR_OTRA_UNIDAD Then

                                If idSisLlantaDES > 0 Then
                                    'Movimiento 2
                                    StrSql = objTool.InsertaMovimientos(cmbActividades.SelectedValue, Now, _Usuario, idSisLlantaDES,
                                    cmbidtipoUbicacionDES.SelectedValue, cmbidUbicacionDES.SelectedValue, txtPosicionDES.Value,
                                    cmbidtipoUbicacion.SelectedValue, cmbidUbicacion.SelectedValue, txtPosicion.Value, txtObserva.Text)

                                    ReDim Preserve ArraySql(indice)
                                    ArraySql(indice) = StrSql
                                    indice += 1

                                    If indice > 0 Then
                                        Respuesta = obj.EjecutarSql(Inicio.CONSTR, "SQL", ArraySql, Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, indice, 0)
                                        Respuesta2 = Respuesta.Substring(0, 9)
                                        If Respuesta2 = "EFECTUADO" Then
                                            Indentity2 = Respuesta.Substring(10, Respuesta.Length - 10)
                                            'ORIGEN
                                            .GuardaHistorico(cmbidtipoUbicacion.SelectedValue, cmbidUbicacion.SelectedValue, Now, _Usuario, idSisLlanta,
                                             txtidLlanta.Text, True, TipoDocumento.ServicioInterno, Indentity, txtProfundidadFINAL.Text,
                                                             cmbidDisenio.SelectedValue, txtPosicionDES.Value)

                                            'DESTINO
                                            .GuardaHistorico(cmbidtipoUbicacionDES.SelectedValue, cmbidUbicacionDES.SelectedValue, Now, _Usuario, idSisLlantaDES,
                                             txtIdLlantaDES.Text, True, TipoDocumento.ServicioInterno, Indentity2, txtProfundidadFINAL_DES.Text,
                                                             cmbidDisenio.SelectedValue, txtPosicion.Value)

                                            'ORIGEN
                                            .ActualizaCatLlantas(cmbidtipoUbicacionDES.SelectedValue, cmbidUbicacionDES.SelectedValue, txtPosicionDES.Value, Now, idSisLlanta, txtProfundidadFINAL.Text)

                                            'DESTINO
                                            .ActualizaCatLlantas(cmbidtipoUbicacion.SelectedValue, cmbidUbicacion.SelectedValue, txtPosicion.Value, Now, idSisLlantaDES, txtProfundidadFINAL_DES.Text)
                                        End If
                                    End If
                                Else
                                    If opcActual = opServLlantas.CAMBIAR_OTRA_UNIDAD Then
                                        'DESTINO
                                        .GuardaHistorico(cmbidtipoUbicacionDES.SelectedValue, cmbidUbicacionDES.SelectedValue, Now, _Usuario,
                                        idSisLlanta, txtidLlanta.Text, True, TipoDocumento.ServicioInterno, Indentity, txtProfundidadAct.Text,
                                        cmbidDisenio.SelectedValue, txtPosicionDES.Value)

                                        .ActualizaCatLlantas(cmbidtipoUbicacionDES.SelectedValue, cmbidUbicacionDES.SelectedValue, txtPosicionDES.Value, Now, idSisLlanta, txtProfundidadFINAL.Text)
                                    Else
                                        .GuardaHistorico(cmbidtipoUbicacionDES.SelectedValue, cmbidUbicacionDES.SelectedValue, Now, _Usuario, idSisLlanta,
                                 txtidLlanta.Text, True, TipoDocumento.ServicioInterno, Indentity, txtProfundidadFINAL_DES.Text, cmbidCondicion.SelectedValue, txtPosicionDES.Value)

                                        .ActualizaCatLlantas(cmbidtipoUbicacionDES.SelectedValue, cmbidUbicacionDES.SelectedValue, txtPosicionDES.Value, Now, idSisLlanta, txtProfundidadFINAL_DES.Text)

                                    End If
                                End If
                            ElseIf opcActual = opServLlantas.MONTAR_UNIDAD Then
                                .GuardaHistorico(cmbidtipoUbicacionDES.SelectedValue, cmbidUbicacionDES.SelectedValue, Now, _Usuario, idSisLlanta,
                                 txtidLlanta.Text, True, TipoDocumento.ServicioInterno, Indentity, txtProfundidadFINAL_DES.Text, cmbidCondicion.SelectedValue, txtPosicionDES.Value)

                                .ActualizaCatLlantas(cmbidtipoUbicacionDES.SelectedValue, cmbidUbicacionDES.SelectedValue, txtPosicionDES.Value, Now, idSisLlanta, txtProfundidadFINAL_DES.Text)

                            Else
                                .GuardaHistorico(cmbidtipoUbicacionDES.SelectedValue, cmbidUbicacionDES.SelectedValue, Now, _Usuario, idSisLlanta,
                                 txtidLlanta.Text, True, TipoDocumento.ServicioInterno, Indentity, txtProfundidadFINAL_DES.Text, cmbidCondicion.SelectedValue, txtPosicionDES.Value)

                                .ActualizaCatLlantas(cmbidtipoUbicacionDES.SelectedValue, cmbidUbicacionDES.SelectedValue, txtPosicionDES.Value, Now, idSisLlanta, txtProfundidadFINAL_DES.Text)

                                ''ORIGEN
                                ''Solo una vez
                                '.GuardaHistorico(cmbidtipoUbicacion.SelectedValue, cmbidUbicacion.SelectedValue, Now, _Usuario, Llanta.idSisLlanta,
                                '         txtidLlanta.Text, True, TipoDocumento.ServicioInterno, Indentity, txtProfundidadFINAL_DES.Text, cmbidDisenioRev.SelectedValue, txtPosicionDES.Value)

                                ''ORIGEN
                                '.ActualizaCatLlantas(cmbidtipoUbicacionDES.SelectedValue, cmbidUbicacionDES.SelectedValue, txtPosicionDES.Value, Now, idSisLlanta, txtProfundidadFINAL.Text)

                            End If
                        End If

                    End If

                    '.Guardar(bEsIdentidad, cmbidEmpresa.SelectedValue, cmbidProductoLlanta.SelectedValue, cmbidTipoLLanta.SelectedValue,
                    '                     txtFactura.Text, FormatFecHora(dtpFecha.Value, True), txtCosto.Text, "", Val(txtPosicion.Text), cmbidUbicacion.SelectedValue, dtpFecha.Value.Year, dtpFecha.Value.Month,
                    '                     chkActivo.Checked, txtObserva.Text, FormatFecHora(dtpFecha.Value, True), cmbidtipoUbicacion.SelectedValue,
                    '                    txtidLlanta.Text, txtNoSerie.Text, cmbidCondicion.SelectedValue, txtProfundidadAct.Text, cmbidDisenioRev.SelectedValue)

                    'Llanta = New LlantasClass(txtidLlanta.Text, OpcionLlanta.idLlanta)
                    'If Llanta.Existe Then
                    'End If

                    '.GuardaMov("ALTA", FormatFecHora(Now, True), _Usuario, txtidLlanta.Text, cmbidUbicacion.SelectedValue, txtPosicion.Text, cmbidUbicacion.SelectedValue, txtPosicion.Text, txtObserva.Text)

                    Status("Llanta: " & .idLlanta & " Guardado con Exito!!!", Me, 2, 2)
                    MessageBox.Show("Nueva Llanta: " & .idLlanta & " Guardado con exito!!!",
                                    "Accion terminada con exito!!!", MessageBoxButtons.OK, MessageBoxIcon.Information)


                    Salida = True
                    If _IdLLanta <> "" Then
                        'Close()
                        Me.DialogResult = Windows.Forms.DialogResult.OK
                    Else
                        CargaForm(cmbidUbicacion.SelectedValue, cmbidtipoUbicacion.Text)
                        ActivaCampos(True, TipoOpcActivaCampos.tOpcOTRO)
                        btnMnuOk.Enabled = False
                        txtidLlanta.SelectAll()
                        txtidLlanta.Focus()
                        Cancelar()
                    End If
                    Salida = False
                End If
            End With
            Salida = False
        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try
    End Sub


    Private Sub btnMnuCancelar_Click(sender As Object, e As EventArgs) Handles btnMnuCancelar.Click
        'If txtidLlanta.Text <> "" Then
        '    CargaForm(txtidLlanta.Text, UCase(TablaBd))
        'End If
        'OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
        'btnMnuModificar.Enabled = True
        'cmbActividades.SelectedIndex = 0

        'txtidLlanta.Focus()
        If _IdLLanta <> "" Then
            Close()
        Else
            Cancelar()
        End If




    End Sub

    Private Sub btnMnuSalir_Click(sender As Object, e As EventArgs) Handles btnMnuSalir.Click
        Me.Close()
    End Sub

    Private Sub txtMenuBusca_TextChanged(sender As Object, e As EventArgs) Handles txtMenuBusca.TextChanged
        ActualizaGrid(txtMenuBusca.Text)
    End Sub


    Private Function LlenaCombos() As Boolean
        Dim StrSql As String = ""
        LlenaCombos = True
        Try



            'DsCombo.Clear()
            'StrSql = "select 'ACT' as IdEstatus, 'ACTIVO' as Estatus " &
            '"union " &
            '"select 'BAJA' as IdEstatus, 'BAJA' as Estatus"

            'DsCombo = Inicio.LlenaCombos("IdEstatus", "Estatus", "CatEstatus",
            '                   TipoConexion.TcSQL, Inicio.CONSTR, Process.GetCurrentProcess.ProcessName,
            '                   KEY_CHECAERROR, KEY_ESTATUS, , StrSql)
            ''DsCombo = BD.ExecuteReturn(StrSql)
            'If Not DsCombo Is Nothing Then
            '    If DsCombo.Tables("CatEstatus").DefaultView.Count > 0 Then
            '        cmbEstatus.DataSource = DsCombo.Tables("CatEstatus")
            '        cmbEstatus.ValueMember = "IdEstatus"
            '        cmbEstatus.DisplayMember = "Estatus"
            '    End If
            'Else
            '    MsgBox("No se han dado de alta a Estatus ", vbInformation, "Aviso" & Me.Text)
            '    LlenaCombos = False
            '    Exit Function
            'End If

            'Empresas
            AutoCompletarEmpresas(cmbidEmpresa, False)
            'Marcas
            AutoCompletarMarcas(cmbidMarca, False)
            'Tipo Llantas
            AutoCompletarTipoLlanta(cmbidTipoLLanta, False)
            'Producto LLantas
            AutoCompletarProductoLlanta(cmbidProductoLlanta, False)
            'Disenios
            AutoCompletarDisenios(cmbidDisenio)
            'Condicion
            AutoCompletarCondicion(cmbidCondicion)
            'Tipos de Ubicacion
            AutoCompletarUbicacion(cmbidtipoUbicacion)
            'Tipos de Ubicacion DESTINO
            AutoCompletarUbicacion(cmbidtipoUbicacionDES)
            'Disenios ReVitalizado
            AutoCompletarDisenios(cmbidDisenioRev)

            'Servicios a partir de enum
            Dim dt As New DataTable
            Dim obj As New Funciones()
            dt = obj.EnumToDataTable(GetType(opServLlantas), "KEY", "VALUE")
            cmbActividades.DataSource = dt
            cmbActividades.DisplayMember = "VALUE"
            cmbActividades.ValueMember = "KEY"

            'cmbActividades.ValueMember = "key"
            'cmbActividades.DisplayMember = "value"
            'cmbActividades.DataSource = [Enum].GetValues(GetType(opServLlantas))

            ''Tipos de DEstino desde Enun
            'cmbidtipoUbicacionDES.ValueMember = "key"
            'cmbidtipoUbicacionDES.DisplayMember = "value"
            'cmbidtipoUbicacionDES.DataSource = [Enum].GetValues(GetType(TipoUbicacionLLa))

        Catch ex As Exception

        End Try
    End Function




    'Private Sub txtidMarca_Leave(sender As Object, e As EventArgs)
    '    If Salida Then Exit Sub
    '    If txtidMarca.Text.Length = 0 Then
    '        txtidMarca.Focus()
    '        txtidMarca.SelectAll()
    '    Else
    '        If txtidMarca.Enabled Then
    '            CargaForm(txtidMarca.Text, "Marca")
    '            'txtidSucursal.Focus()
    '            'txtidSucursal.SelectAll()

    '        End If
    '    End If
    'End Sub

    'Private Sub txtidMarca_KeyDown(sender As Object, e As KeyEventArgs)
    '    If e.KeyCode = Keys.F3 Then
    '        cmdBuscaClaveMar_Click(sender, e)
    '    End If
    'End Sub

    Private Sub cmdBuscaClaveSuc_Click(sender As Object, e As EventArgs)
        'txtidSucursal.Text = DespliegaGrid("CatSucursal", Inicio.CONSTR, "id_sucursal")
        txtidSucursal_Leave(sender, e)
    End Sub

    Private Sub txtidSucursal_Leave(sender As Object, e As EventArgs)
        If Salida Then Exit Sub
        'If txtidSucursal.Text.Length = 0 Then
        '    txtidSucursal.Focus()
        '    txtidSucursal.SelectAll()
        'Else
        '    If txtidSucursal.Enabled Then
        '        CargaForm(txtidSucursal.Text, "Sucursal")
        '        txtIdAlmacen.Focus()
        '        txtIdAlmacen.SelectAll()

        '    End If
        'End If
    End Sub

    'Private Sub txtidSucursal_KeyDown(sender As Object, e As KeyEventArgs) Handles txtidSucursal.KeyDown, txtidTipoUnidad.KeyDown
    '    If e.KeyCode = Keys.F3 Then
    '        cmdBuscaClaveSuc_Click(sender, e)
    '    End If
    'End Sub


    Private Sub txtIdAlmacen_Leave(sender As Object, e As EventArgs)
        'If txtIdAlmacen.Enabled Then
        '    CargaForm(txtIdAlmacen.Text, "Almacen")
        '    cmbEstatus.Focus()


        'End If
    End Sub



    Private Sub txtidUnidadTrans_KeyPress(sender As Object, e As KeyPressEventArgs)
        If Asc(e.KeyChar) = 13 Then
            If txtidLlanta.Text.Trim <> "" Then
                CargaForm(txtidLlanta.Text, UCase(TablaBd))
            End If
            e.Handled = True
        End If
    End Sub

    Private Sub txtidSucursal_KeyDown(sender As Object, e As KeyEventArgs)
        If e.KeyCode = Keys.F3 Then
            cmdBuscaClaveSuc_Click(sender, e)
        End If
    End Sub










    'Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnGenerar.Click
    '    txtidLlanta.Text = Trim(Prefijo &
    '    dtpFecha.Value.Year.ToString.Substring(2) &
    '    IIf(dtpFecha.Value.Month < 10, "0" & dtpFecha.Value.Month, dtpFecha.Value.Month) &
    '    UltimoConsecutivo)
    'End Sub

    Private Sub txtidLlanta_TextChanged(sender As Object, e As EventArgs) Handles txtidLlanta.TextChanged

    End Sub

    Private Sub txtidLlanta_EnabledChanged(sender As Object, e As EventArgs) Handles txtidLlanta.EnabledChanged
        'cmdBuscaClave.Enabled = txtidLlanta.Enabled
    End Sub



    'Private Sub txtidLlanta_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtidLlanta.KeyPress
    '    If Asc(e.KeyChar) = 13 Then
    '        If txtidLlanta.Text.Trim <> "" Then
    '            CargaForm(txtidLlanta.Text, UCase(TablaBd))
    '            cmbidEmpresa.Focus()
    '        End If
    '        e.Handled = True
    '    End If
    'End Sub

    Private Sub txtidLlanta_Leave(sender As Object, e As EventArgs) Handles txtidLlanta.Leave
        If Salida Then Exit Sub
        CargaForm(txtidLlanta.Text, UCase(TablaBd))
        'Llanta = New LlantasClass(txtidLlanta.Text, OpcionLlanta.idLlanta)
        'If Llanta.Existe Then
        '    MsgBox("El id Llanta: " & txtidLlanta.Text & " ya Existe")
        '    txtidLlanta.SelectAll()
        '    txtidLlanta.Focus()
        'Else
        '    txtNoSerie.SelectAll()
        '    txtNoSerie.Focus()
        'End If
    End Sub

    Private Sub txtidLlanta_MouseDown(sender As Object, e As MouseEventArgs) Handles txtidLlanta.MouseDown
        txtidLlanta.Focus()
        txtidLlanta.SelectAll()
    End Sub

    Private Sub txtMedida_TextChanged(sender As Object, e As EventArgs) Handles txtMedida.TextChanged

    End Sub

    Private Sub txtMedida_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtMedida.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            txtProfundidad.Focus()
        End If
    End Sub

    Private Sub txtMedida_MouseDown(sender As Object, e As MouseEventArgs) Handles txtMedida.MouseDown
        txtMedida.Focus()
        txtMedida.SelectAll()
    End Sub

    Private Sub txtProfundidad_TextChanged(sender As Object, e As EventArgs) Handles txtProfundidad.TextChanged

    End Sub

    Private Sub txtProfundidad_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtProfundidad.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            'txtFactura.Focus()
        End If
    End Sub

    Private Sub txtProfundidad_MouseDown(sender As Object, e As MouseEventArgs) Handles txtProfundidad.MouseDown
        txtProfundidad.Focus()
        txtProfundidad.SelectAll()
    End Sub


    Private Sub txtUniTrans_Leave(sender As Object, e As EventArgs) Handles cmbidUbicacion.Leave
        'Private Sub txtUniTrans_Leave(sender As Object, e As EventArgs) Handles txtUniTrans.Leave
        If Salida Then Exit Sub
        If cmbidUbicacion.Enabled Then
            CargaForm(cmbidUbicacion.Text, "CatUnidadTrans")
            ActualizaGridxUnidad(cmbidUbicacion.SelectedValue)
            txtPosicion.Focus()
        End If
        'If txtUniTrans.Text.Length = 0 Then
        '    txtUniTrans.Focus()
        '    txtUniTrans.SelectAll()
        'Else
        '    If cmbidUbicacion.Enabled Then
        '        CargaForm(cmbidUbicacion.Text, "CatUnidadTrans")
        '        ActualizaGridxUnidad(txtUniTrans.Text)
        '        txtPosicion.Focus()
        '    End If
        'End If
    End Sub





    'Private Sub txtidSucursal_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtidSucursal.KeyPress, txtidTipoUnidad.KeyPress
    '    If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
    '        txtIdAlmacen.Focus()
    '        txtIdAlmacen.SelectAll()
    '    End If

    'End Sub
    Private Sub DibujaEsquema(ByVal vidClasLlan As Integer, ByVal TotalLlantas As Integer)
        Dim vNoEje As Integer
        Dim vNoLlantas As Integer
        Dim LlantxLado As Integer
        Dim NoPosicion As Integer = 0

        'Dim FactorX As Integer = 71
        Dim FactorX As Integer = 201

        'Dim FactorY As Integer = 41
        Dim FactorY As Integer = 70

        Dim PosX As Integer = 29
        'Dim PosFacLinX As Integer = 201
        Dim PosFacLinX As Integer = 195

        'Dim PosY As Integer = 62
        Dim PosY As Integer = 37

        ReDim rdbPosicion(TotalLlantas - 1)

        LimpiaPosiciones()

        ClasLlan = New ClasLlanClass(0)
        TablaEstruc = New DataTable

        TablaEstruc = ClasLlan.TablaPosLlanta(vidClasLlan)
        If Not TablaEstruc Is Nothing Then
            If TablaEstruc.Rows.Count > 0 Then
                For i = 0 To TablaEstruc.Rows.Count - 1
                    vNoEje = TablaEstruc.DefaultView.Item(i).Item("EjeNo")
                    vNoLlantas = TablaEstruc.DefaultView.Item(i).Item("NoLlantas")
                    LlantxLado = vNoLlantas / 2
                    'Posiciones Iniciales
                    If LlantxLado = 0 Then
                        'PosX = 29
                        'FactorX = 71
                        PosX = 95
                        FactorX = 37
                    ElseIf LlantxLado = 1 Then
                        'PosX = 29
                        'FactorX = 71
                        PosX = 50
                        FactorX = 37
                    Else
                        'PosX = 5
                        'FactorX = 37
                        'PosX = 3
                        PosX = 5
                        'FactorX = 99
                        FactorX = 90
                    End If

                    For z = 0 To vNoLlantas - 1
                        rdbPosicion(NoPosicion) = New CheckBox
                        rdbPosicion(NoPosicion).Name = "pos" & NoPosicion

                        rdbPosicion(NoPosicion).Text = NoPosicion + 1
                        'rdbPosicion(NoPosicion).AutoSize = True
                        rdbPosicion(NoPosicion).Checked = False

                        'rdbPosicion(NoPosicion).Size = New System.Drawing.Size(31, 17)
                        rdbPosicion(NoPosicion).Size = New System.Drawing.Size(95, 64)

                        rdbPosicion(NoPosicion).Image = My.Resources.llanta2
                        rdbPosicion(NoPosicion).TextImageRelation = TextImageRelation.Overlay
                        rdbPosicion(NoPosicion).ImageAlign = ContentAlignment.MiddleCenter

                        'rdbPosicion(i).Location = New Point(6, 4)

                        rdbPosicion(NoPosicion).Location = New Point(PosX, PosY)

                        AddHandler rdbPosicion(NoPosicion).Click, AddressOf RadioButtons_Click
                        AddHandler rdbPosicion(NoPosicion).KeyDown, AddressOf ObjEnter_KeyDown
                        'PanelControles.Controls.Add(rdbLogico1(i))
                        GpoPosiciones.Controls.Add(rdbPosicion(NoPosicion))

                        'PanelLogico1(i).Controls.Add(rdbLogico1(i))

                        PosX = PosX + FactorX

                        If z < vNoLlantas - 1 Then
                            NoPosicion = NoPosicion + 1
                        End If

                        If LlantxLado = 1 Then
                            PosX = PosX + PosFacLinX - FactorX
                        End If

                    Next
                    PosY = PosY + FactorY
                    NoPosicion = NoPosicion + 1
                Next
            End If
        End If



    End Sub

    Private Sub RadioButtons_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        With TryCast(sender, RadioButton)
            txtPosicion.Text = .Text
            For i = 0 To rdbPosicion.Count - 1
                rdbPosicion(i).BackColor = SystemColors.Control
            Next
            .BackColor = Color.Yellow
            'MessageBox.Show("El Label Tiene Nombre: " & .Name & " y Tiene Escrito:" & .Text, "informacion del Label", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End With
    End Sub

    Private Sub ObjEnter_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        If e.KeyCode = 13 Then
            SendKeys.Send("{TAB}")
            e.Handled = True
        End If
    End Sub

    Private Sub SeleccionaPosicion(ByVal Posicion1 As Integer, ByVal Color1 As Color,
    Optional ByVal Posicion2 As Integer = 0, Optional ByVal Color2 As Color = Nothing)
        If Posicion1 > rdbPosicion.Count Then
            MsgBox("La Estructura del tipo de unidad " & txtTipoUni.Text & " No cuenta con la posicion " & txtPosicion.Text & vbCrLf & "Favor de seleccionar la Posicion Correspondiente", MsgBoxStyle.Information, Me.Text)
            txtPosicion.Value = ""
            For i = 0 To rdbPosicion.Count - 1
                rdbPosicion(i).BackColor = SystemColors.Control
                rdbPosicion(i).Checked = False
            Next

        Else
            For i = 0 To rdbPosicion.Count - 1
                rdbPosicion(i).BackColor = SystemColors.Control
                rdbPosicion(i).Checked = False
            Next

            If Posicion1 > 0 Then
                rdbPosicion(Posicion1 - 1).Checked = True
                rdbPosicion(Posicion1 - 1).BackColor = Color1
            End If
            If Posicion2 > 0 Then
                rdbPosicion(Posicion2 - 1).Checked = True
                rdbPosicion(Posicion2 - 1).BackColor = Color2
            End If

            'For i = 0 To rdbPosicion.Count - 1
            '    rdbPosicion(i).BackColor = SystemColors.Control
            'Next
            'rdbPosicion(Posicion - 1).BackColor = vColor

        End If


    End Sub

    Private Sub LimpiaPosiciones()
        'For Each ct As Control In GrupoCaptura.Controls
        '    If TypeOf ct Is RadioButton Then
        '        ct.Dispose()
        '    End If
        'Next

        'Dim num_controles As Int32 = Me.TLPanel_convenio.Controls.Count - 1
        Dim num_controles As Int32 = Me.GpoPosiciones.Controls.Count - 1
        For n As Integer = num_controles To 0 Step -1
            Dim ctrl As Windows.Forms.Control = Me.GpoPosiciones.Controls(n)
            If TypeOf ctrl Is RadioButton Then

            End If
            Me.GpoPosiciones.Controls.Remove(ctrl)
            ctrl.Dispose()
        Next
        DibujaLinea()
    End Sub

    Private Sub DibujaLinea()
        'Dim canvas As New ShapeContainer
        'Dim theLine As New LineShape
        '' Set the form as the parent of the ShapeContainer.
        'canvas.Parent = Me.GpoPosiciones
        '' Set the ShapeContainer as the parent of the LineShape.
        'theLine.Parent = canvas
        '' Set the starting and ending coordinates for the line.
        'theLine.StartPoint = New System.Drawing.Point(70, 34)
        'theLine.EndPoint = New System.Drawing.Point(70, 191)

        'Dim canvas As New ShapeContainer
        'Dim theLine As New LineShape
        ' Set the form as the parent of the ShapeContainer.
        'canvas.Parent = Me.GpoPosiciones
        '' Set the ShapeContainer as the parent of the LineShape.
        'theLine.Parent = canvas
        '' Set the starting and ending coordinates for the line.
        'theLine.BorderWidth = 10
        'theLine.StartPoint = New System.Drawing.Point(183, 25)
        'theLine.EndPoint = New System.Drawing.Point(183, 300)
    End Sub

    Private Sub txtPosicion_TextChanged(sender As Object, e As EventArgs)

    End Sub

    'Private Sub txtPosicion_KeyPress(sender As Object, e As KeyPressEventArgs)
    '    'Private Sub txtPosicion_KeyPress(sender As Object, e As KeyPressEventArgs) 
    '    If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
    '        SeleccionaPosicion(txtPosicion.Value)
    '    End If
    'End Sub

    Private Sub dtpFecha_ValueChanged(sender As Object, e As EventArgs)

    End Sub

    Private Sub dtpFecha_KeyPress(sender As Object, e As KeyPressEventArgs)
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            If OpcionForma = TipoOpcionForma.tOpcInsertar Then
                'btnGenerar.Focus()
            Else
                cmbidMarca.Focus()
            End If

        End If
    End Sub


    Private Sub txtMedida_KeyUp(sender As Object, e As KeyEventArgs) Handles txtMedida.KeyUp
        If e.KeyValue = Keys.Enter Then
            txtProfundidad.Focus()
        End If
    End Sub

    Private Sub txtProfundidad_KeyUp(sender As Object, e As KeyEventArgs) Handles txtProfundidad.KeyUp
        If e.KeyValue = Keys.Enter Then
            'txtFactura.Focus()
        End If
    End Sub
    Public Function AutoCompletarEmpresas(ByVal Control As ComboBox, ByVal bIncluyeTodos As Boolean) As AutoCompleteStringCollection

        Dim Coleccion As New AutoCompleteStringCollection
        Dim dt As DataTable


        Empresa = New EmpresaClass(0)
        dt = Empresa.TablaEmpresas(bIncluyeTodos)

        'Recorrer y cargar los items para el Autocompletado
        For Each row As DataRow In dt.Rows
            Coleccion.Add(Convert.ToString(row("RazonSocial")))
        Next
        'Ajustamos el control TextBox o ComboBox para recibir los datos de la siguiente manera.
        With Control
            .DataSource = dt
            .DisplayMember = "RazonSocial"
            .ValueMember = "idEmpresa"


            .AutoCompleteCustomSource = Coleccion
            .AutoCompleteMode = AutoCompleteMode.Suggest
            .AutoCompleteSource = AutoCompleteSource.CustomSource
        End With

        ' Aca le devuelvo los datos a recuperados de la Base
        Return Coleccion

    End Function

    Private Sub GridTodos_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles GridTodos.CellContentClick

    End Sub
    Public Function AutoCompletarMarcas(ByVal Control As ComboBox, ByVal bIncluyeTodos As Boolean) As AutoCompleteStringCollection

        Dim Coleccion As New AutoCompleteStringCollection
        Dim dt As DataTable


        Marca = New MarcaClass(0)
        dt = Marca.TablaMarcas(bIncluyeTodos)

        'Recorrer y cargar los items para el Autocompletado
        For Each row As DataRow In dt.Rows
            Coleccion.Add(Convert.ToString(row("NombreMarca")))
        Next
        'Ajustamos el control TextBox o ComboBox para recibir los datos de la siguiente manera.
        With Control
            .DataSource = dt
            .DisplayMember = "NombreMarca"
            .ValueMember = "idMarca"


            .AutoCompleteCustomSource = Coleccion
            .AutoCompleteMode = AutoCompleteMode.Suggest
            .AutoCompleteSource = AutoCompleteSource.CustomSource
        End With

        ' Aca le devuelvo los datos a recuperados de la Base
        Return Coleccion

    End Function

    Public Function AutoCompletarTipoLlanta(ByVal Control As ComboBox, ByVal bIncluyeTodos As Boolean) As AutoCompleteStringCollection

        Dim Coleccion As New AutoCompleteStringCollection
        Dim dt As DataTable


        TipoLLanta = New TipoLlantaClass(0)
        dt = TipoLLanta.TablaTipoLlantas(bIncluyeTodos)

        'Recorrer y cargar los items para el Autocompletado
        For Each row As DataRow In dt.Rows
            Coleccion.Add(Convert.ToString(row("Descripcion")))
        Next
        'Ajustamos el control TextBox o ComboBox para recibir los datos de la siguiente manera.
        With Control
            .DataSource = dt
            .DisplayMember = "Descripcion"
            .ValueMember = "idTipoLLanta"


            .AutoCompleteCustomSource = Coleccion
            .AutoCompleteMode = AutoCompleteMode.Suggest
            .AutoCompleteSource = AutoCompleteSource.CustomSource
        End With

        ' Aca le devuelvo los datos a recuperados de la Base
        Return Coleccion

    End Function
    Public Function AutoCompletarProductoLlanta(ByVal Control As ComboBox, ByVal bIncluyeTodos As Boolean) As AutoCompleteStringCollection

        Dim Coleccion As New AutoCompleteStringCollection
        Dim dt As DataTable


        llantaProd = New LLantasProductoClass(0)
        dt = llantaProd.TablaLLantasProducto(bIncluyeTodos)

        'Recorrer y cargar los items para el Autocompletado
        For Each row As DataRow In dt.Rows
            Coleccion.Add(Convert.ToString(row("Descripcion")))
        Next
        'Ajustamos el control TextBox o ComboBox para recibir los datos de la siguiente manera.
        With Control
            .DataSource = dt
            .DisplayMember = "Descripcion"
            .ValueMember = "idProductoLlanta"


            .AutoCompleteCustomSource = Coleccion
            .AutoCompleteMode = AutoCompleteMode.Suggest
            .AutoCompleteSource = AutoCompleteSource.CustomSource
        End With

        ' Aca le devuelvo los datos a recuperados de la Base
        Return Coleccion

    End Function
    Public Function AutoCompletarDisenios(ByVal Control As ComboBox) As AutoCompleteStringCollection

        Dim Coleccion As New AutoCompleteStringCollection
        Dim dt As DataTable

        Disenio = New CatDiseniosClass(0)
        dt = Disenio.TablaDisenios(False)

        'Recorrer y cargar los items para el Autocompletado
        For Each row As DataRow In dt.Rows
            Coleccion.Add(Convert.ToString(row("Descripcion")))
        Next
        'Ajustamos el control TextBox o ComboBox para recibir los datos de la siguiente manera.
        With Control
            .DataSource = dt
            .DisplayMember = "Descripcion"
            .ValueMember = "idDisenio"


            .AutoCompleteCustomSource = Coleccion
            .AutoCompleteMode = AutoCompleteMode.Suggest
            .AutoCompleteSource = AutoCompleteSource.CustomSource
        End With

        ' Aca le devuelvo los datos a recuperados de la Base
        Return Coleccion

    End Function
    Public Function AutoCompletarCondicion(ByVal Control As ComboBox) As AutoCompleteStringCollection

        Dim Coleccion As New AutoCompleteStringCollection
        Dim dt As DataTable

        Condicion = New CatCondicionesClass(0)
        dt = Condicion.TablaCondiciones(False)

        'Recorrer y cargar los items para el Autocompletado
        For Each row As DataRow In dt.Rows
            Coleccion.Add(Convert.ToString(row("Descripcion")))
        Next
        'Ajustamos el control TextBox o ComboBox para recibir los datos de la siguiente manera.
        With Control
            .DataSource = dt
            .DisplayMember = "Descripcion"
            .ValueMember = "idCondicion"


            .AutoCompleteCustomSource = Coleccion
            .AutoCompleteMode = AutoCompleteMode.Suggest
            .AutoCompleteSource = AutoCompleteSource.CustomSource
        End With

        ' Aca le devuelvo los datos a recuperados de la Base
        Return Coleccion

    End Function
    '
    Public Function AutoCompletarUbicacion(ByVal Control As ComboBox) As AutoCompleteStringCollection

        Dim Coleccion As New AutoCompleteStringCollection
        Dim dt As DataTable

        TipoUbicacion = New CatLugaresClass(0)
        dt = TipoUbicacion.TablaLugares(False, " NOT  Descripcion IN  ('PROVEEDOR','SIN_UBICACION') ")

        'Recorrer y cargar los items para el Autocompletado
        For Each row As DataRow In dt.Rows
            Coleccion.Add(Convert.ToString(row("Descripcion")))
        Next
        'Ajustamos el control TextBox o ComboBox para recibir los datos de la siguiente manera.
        With Control
            .DataSource = dt
            .DisplayMember = "Descripcion"
            .ValueMember = "idLugar"


            .AutoCompleteCustomSource = Coleccion
            .AutoCompleteMode = AutoCompleteMode.Suggest
            .AutoCompleteSource = AutoCompleteSource.CustomSource
        End With

        ' Aca le devuelvo los datos a recuperados de la Base
        Return Coleccion

    End Function

    Private Sub cmbidtipoUbicacion_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbidtipoUbicacion.SelectedIndexChanged

    End Sub

    Private Sub cmbidtipoUbicacion_KeyUp(sender As Object, e As KeyEventArgs) Handles cmbidtipoUbicacion.KeyUp
        If e.KeyValue = Keys.Enter Then
            cmbidtipoUbicacion.Enabled = False
            cmbidUbicacion.Enabled = True
            AutoCompletarUbicaciones(cmbidtipoUbicacion.SelectedValue, cmbidUbicacion)
            cmbidUbicacion.Focus()
        End If
    End Sub

    Private Sub cmbidtipoUbicacion_Leave(sender As Object, e As EventArgs) Handles cmbidtipoUbicacion.Leave
        If Salida Then Exit Sub
        cmbidtipoUbicacion.Enabled = False
        cmbidUbicacion.Enabled = True
        cmbidUbicacion.Focus()
    End Sub

    Private Sub AutoCompletarUbicaciones(ByVal idLugar As Integer, ByVal Combo As ComboBox)
        TipoUbicacion = New CatLugaresClass(idLugar, TipoOrdenServicio.LLANTAS)

        If TipoUbicacion.Existe Then
            lblUbicacion.Text = TipoUbicacion.Descripcion

            If TipoUbicacion.NomTabla = "CATALMACEN" Then
                AutoCompletarAlmacen(Combo)
            ElseIf TipoUbicacion.NomTabla = "CATPROVEEDORES" Then
                AutoCompletarRelProvTOS(Combo)
            ElseIf TipoUbicacion.NomTabla = "CATUNIDADTRANS" Then
                AutoCompletarUnidadTransp(Combo, True, 0, "tractor")
            End If
        End If

    End Sub

    Public Function AutoCompletarRelProvTOS(ByVal Control As ComboBox) As AutoCompleteStringCollection

        Dim Coleccion As New AutoCompleteStringCollection
        Dim dt As DataTable

        RelProvTos = New RelProvTipOrdSerClass(0, 0)
        dt = RelProvTos.TablaProveedoresxTos(True, TipoOrdenServicio.LLANTAS)
        'Recorrer y cargar los items para el Autocompletado
        For Each row As DataRow In dt.Rows
            Coleccion.Add(Convert.ToString(row("RazonSocial")))
        Next
        'Ajustamos el control TextBox o ComboBox para recibir los datos de la siguiente manera.
        With Control
            .DataSource = dt
            .DisplayMember = "RazonSocial"
            .ValueMember = "idProveedor"


            .AutoCompleteCustomSource = Coleccion
            .AutoCompleteMode = AutoCompleteMode.Suggest
            .AutoCompleteSource = AutoCompleteSource.CustomSource
        End With

        ' Aca le devuelvo los datos a recuperados de la Base
        Return Coleccion

    End Function

    Private Sub cmbidUbicacion_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbidUbicacion.SelectedIndexChanged

    End Sub

    Private Sub cmbidEmpresa_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbidEmpresa.SelectedIndexChanged

    End Sub

    Private Sub cmbidEmpresa_KeyUp(sender As Object, e As KeyEventArgs) Handles cmbidEmpresa.KeyUp
        If e.KeyValue = Keys.Enter Then
            cmbidEmpresa.Enabled = False
            cmbidtipoUbicacion.Enabled = True
            cmbidtipoUbicacion.Focus()

        End If
    End Sub

    Private Sub cmbidEmpresa_Leave(sender As Object, e As EventArgs) Handles cmbidEmpresa.Leave
        If Salida Then Exit Sub
        cmbidEmpresa.Enabled = False
        cmbidtipoUbicacion.Enabled = True
        cmbidtipoUbicacion.Focus()
    End Sub

    Private Sub cmbidUbicacion_PaddingChanged(sender As Object, e As EventArgs) Handles cmbidUbicacion.PaddingChanged

    End Sub

    Private Sub txtidLlanta_KeyUp(sender As Object, e As KeyEventArgs) Handles txtidLlanta.KeyUp
        If e.KeyValue = Keys.Enter Then
            'Checar si Existe
            Salida = True
            CargaForm(txtidLlanta.Text, UCase(TablaBd))


            'Llanta = New LlantasClass(txtidLlanta.Text, OpcionLlanta.idLlanta)
            'If Llanta.Existe Then
            '    MsgBox("El id Llanta: " & txtidLlanta.Text & " ya Existe")
            '    txtidLlanta.SelectAll()
            '    txtidLlanta.Focus()
            'Else
            '    txtNoSerie.SelectAll()
            '    txtNoSerie.Focus()


            'End If
            Salida = False
        End If
    End Sub

    Private Sub txtNoSerie_TextChanged(sender As Object, e As EventArgs)

    End Sub

    'Private Sub txtNoSerie_KeyUp(sender As Object, e As KeyEventArgs)
    '    If e.KeyValue = Keys.Enter Then
    '        Salida = True
    '        Llanta = New LlantasClass(txtNoSerie.Text, OpcionLlanta.NoSerie)
    '        If Llanta.Existe Then
    '            MsgBox("El No Serie: " & txtNoSerie.Text & " ya Existe")
    '            txtNoSerie.SelectAll()
    '            txtNoSerie.Focus()
    '        Else
    '            txtPosicion.Select()
    '            txtPosicion.Focus()
    '        End If
    '        Salida = False
    '    End If
    'End Sub

    Private Sub txtPosicion_ValueChanged(sender As Object, e As EventArgs) Handles txtPosicion.ValueChanged
        If Salida Then Exit Sub
        If rdbPosicion.Length > 0 Then
            SeleccionaPosicion(Val(txtPosicion.Value), Color.Yellow)
        End If

    End Sub

    Private Sub cmbidProductoLlanta_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbidProductoLlanta.SelectedIndexChanged
        If Salida Then Exit Sub
        CargaForm(cmbidProductoLlanta.SelectedValue, "LLANTAPROD")

    End Sub



    Private Sub txtPosicion_KeyUp(sender As Object, e As KeyEventArgs) Handles txtPosicion.KeyUp
        If e.KeyValue = Keys.Enter Then
            Salida = True
            TipoUbicacion = New CatLugaresClass(cmbidtipoUbicacion.SelectedValue, TipoOrdenServicio.LLANTAS)
            If TipoUbicacion.Existe Then
                If TipoUbicacion.NomTabla = "CATUNIDADTRANS" Then
                    Llanta = New LlantasClass(txtPosicion.Value, OpcionLlanta.Posicion, cmbidUbicacion.SelectedValue)
                    If Llanta.Existe Then
                        MsgBox("La Posicion: " & txtPosicion.Value & " ya la ocupa la unidad: " & cmbidUbicacion.SelectedValue & " con la llanta: " & Llanta.idLlanta)
                        txtPosicion.Focus()
                    Else
                        OpcionForma = TipoOpcionForma.tOpcInsertar
                        btnMnuOk.Enabled = True
                        If rdbPosicion.Length > 0 Then
                            SeleccionaPosicion(Val(txtPosicion.Value), Color.Yellow)
                            cmbidProductoLlanta.Focus()
                        End If
                    End If
                End If
            End If
            Salida = False

        End If
    End Sub

    Private Sub cmbidProductoLlanta_KeyUp(sender As Object, e As KeyEventArgs) Handles cmbidProductoLlanta.KeyUp
        If e.KeyValue = Keys.Enter Then
            CargaForm(cmbidProductoLlanta.SelectedValue, "LLANTAPROD")
            cmbidTipoLLanta.Focus()
        End If
    End Sub

    Private Sub cmbidTipoLLanta_SelectedIndexChanged(sender As Object, e As EventArgs)

    End Sub

    Private Sub cmbidTipoLLanta_KeyUp(sender As Object, e As KeyEventArgs)
        If e.KeyValue = Keys.Enter Then
            txtProfundidadAct.SelectAll()
            txtProfundidadAct.Focus()
        End If
    End Sub

    Private Sub txtProfundidadAct_TextChanged(sender As Object, e As EventArgs)

    End Sub

    'Private Sub txtProfundidadAct_KeyUp(sender As Object, e As KeyEventArgs)
    '    If e.KeyValue = Keys.Enter Then
    '        txtKmActual.SelectAll()
    '        txtKmActual.Focus()
    '    End If
    'End Sub

    Private Sub txtKmActual_TextChanged(sender As Object, e As EventArgs)

    End Sub

    Private Sub txtKmActual_KeyUp(sender As Object, e As KeyEventArgs)
        If e.KeyValue = Keys.Enter Then
            If cmbidDisenioRev.Enabled Then
                cmbidDisenioRev.Focus()
            Else
                cmbidCondicion.Focus()
            End If

        End If
    End Sub

    Private Sub chkRevitalizado_CheckedChanged(sender As Object, e As EventArgs)
        cmbidDisenioRev.Enabled = chkRevitalizado.Checked
    End Sub

    Private Sub cmbidCondicion_SelectedIndexChanged(sender As Object, e As EventArgs)

    End Sub

    Private Sub cmbidCondicion_KeyUp(sender As Object, e As KeyEventArgs)
        If e.KeyValue = Keys.Enter Then
            'txtFactura.SelectAll()
            'txtFactura.Focus()

        End If
    End Sub

    Private Sub txtFactura_KeyUp(sender As Object, e As KeyEventArgs)
        If e.KeyValue = Keys.Enter Then
            'txtCosto.SelectAll()
            'txtCosto.Focus()
        End If
    End Sub

    Private Sub txtCosto_KeyUp(sender As Object, e As KeyEventArgs)
        If e.KeyValue = Keys.Enter Then
            'dtpFecha.Focus()
        End If
    End Sub
    Public Function AutoCompletarAlmacen(ByVal Control As ComboBox) As AutoCompleteStringCollection

        Dim Coleccion As New AutoCompleteStringCollection
        Dim dt As DataTable

        Almacen = New AlmacenClass(0)
        dt = Almacen.TablaAlmacenes(False)
        'Recorrer y cargar los items para el Autocompletado
        For Each row As DataRow In dt.Rows
            Coleccion.Add(Convert.ToString(row("NomAlmacen")))
        Next
        'Ajustamos el control TextBox o ComboBox para recibir los datos de la siguiente manera.
        With Control
            .DataSource = dt
            .DisplayMember = "NomAlmacen"
            .ValueMember = "idAlmacen"


            .AutoCompleteCustomSource = Coleccion
            .AutoCompleteMode = AutoCompleteMode.Suggest
            .AutoCompleteSource = AutoCompleteSource.CustomSource
        End With

        ' Aca le devuelvo los datos a recuperados de la Base
        Return Coleccion

    End Function

    'Private Sub txtNoSerie_Leave(sender As Object, e As EventArgs)
    '    If Salida Or txtNoSerie.Text.Length = 0 Then Exit Sub
    '    Llanta = New LlantasClass(txtNoSerie.Text, OpcionLlanta.NoSerie)
    '    If Llanta.Existe Then
    '        MsgBox("El No Serie: " & txtNoSerie.Text & " ya Existe")
    '        txtNoSerie.SelectAll()
    '        txtNoSerie.Focus()
    '    Else
    '        txtPosicion.Select()
    '        txtPosicion.Focus()
    '    End If
    'End Sub

    Private Sub txtPosicion_Leave(sender As Object, e As EventArgs) Handles txtPosicion.Leave
        If Salida Then Exit Sub
        TipoUbicacion = New CatLugaresClass(cmbidtipoUbicacion.SelectedValue, TipoOrdenServicio.LLANTAS)
        If TipoUbicacion.Existe Then
            If TipoUbicacion.NomTabla = "CATUNIDADTRANS" Then
                Llanta = New LlantasClass(txtPosicion.Value, OpcionLlanta.Posicion, cmbidUbicacion.SelectedValue)
                If Llanta.Existe Then
                    MsgBox("La Posicion: " & txtPosicion.Value & " ya la ocupa la unidad: " & cmbidUbicacion.SelectedValue & "En la llanta: " & Llanta.idLlanta)
                    txtPosicion.Focus()
                Else
                    OpcionForma = TipoOpcionForma.tOpcInsertar
                    btnMnuOk.Enabled = True
                    If rdbPosicion.Length > 0 Then
                        SeleccionaPosicion(Val(txtPosicion.Value), Color.Yellow)
                        cmbidProductoLlanta.Focus()
                    End If
                End If
            End If
        End If
    End Sub

    'Private Sub BorrarCamposReutilizar()
    '    txtidLlanta.Text = ""
    '    'txtNoSerie.Text = ""
    '    cmbidProductoLlanta.SelectedIndex = 0
    '    chkRevitalizado.Checked = False
    '    cmbidTipoLLanta.SelectedIndex = 0
    '    txtProfundidadAct.Text = ""
    '    chkActivo.Checked = False

    'End Sub

    Private Sub txtObserva_TextChanged(sender As Object, e As EventArgs)

    End Sub

    Private Sub txtObserva_KeyUp(sender As Object, e As KeyEventArgs)
        If e.KeyValue = Keys.Enter Then
            btnMnuOk_Click(sender, e)

        End If
    End Sub

    Private Sub txtMenuBusca_Click(sender As Object, e As EventArgs) Handles txtMenuBusca.Click

    End Sub

    Private Sub btnMnuModificar_Click(sender As Object, e As EventArgs) Handles btnMnuModificar.Click
        'MODIFICAR
        'If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Modificar) = False Then
        '    MessageBox.Show("No tiene los permisos Necesarios para poder Modificar la Informacion del Servicio!!!",
        '           "Acceso Denegado a Modificacion de la Informacion del Servicio", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    OpcionForma = TipoOpcionForma.tOpcConsultar
        '    Exit Sub
        'End If
        If Trim(txtidLlanta.Text) = "" Then
            txtidLlanta.Text = DespliegaGrid(TablaBd, Inicio.CONSTR, CampoLLave)
            CargaForm(txtidLlanta.Text, UCase(TablaBd))
        End If

        OpcionForma = TipoOpcionForma.tOpcInsertar
        ActivaCampos(True, TipoOpcActivaCampos.tOpcINICIALIZA)
        ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)

        'btnMnuEliminar.Enabled = True
        'If bEsIdentidad Then
        '    txtidLlanta.Enabled = False
        '    'txtDescripcionUni.Focus()
        'Else
        '    'txtidUnidadTrans.Focus()
        'End If
        'txtidTipoLLanta.Focus()
        btnMnuOk.Enabled = False
        cmbActividades.Enabled = True

        cmbActividades.Focus()


        Status("Ingrese Unidad para guardar", Me)
    End Sub

    Private Sub cmbActividades_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbActividades.SelectedIndexChanged

    End Sub

    Private Sub cmbActividades_KeyUp(sender As Object, e As KeyEventArgs) Handles cmbActividades.KeyUp
        If e.KeyValue = Keys.Enter Then

            OpcionActividad()

        End If
    End Sub
    Private Sub cmbidUbicacionDES_KeyUp(sender As Object, e As KeyEventArgs) Handles cmbidUbicacionDES.KeyUp
        If Salida Then Exit Sub
        'Dim Band As Boolean = True
        If e.KeyValue = Keys.Enter Then
            Salida = True
            'If CType(cmbidtipoUbicacionDES.SelectedValue, TipoUbicacionLLa) = TipoUbicacionLLa.UNIDADTRANS Then
            '    'Unidad de TRansporte

            'End If
            If opcActual = opServLlantas.CAMBIO_ALMANCEN Then
                If (cmbidUbicacion.SelectedValue = cmbidUbicacionDES.SelectedValue) Then
                    MsgBox("Escoja un Almacen diferente a: " & cmbidUbicacion.Text, vbInformation, "Aviso" & Me.Text)
                    cmbidUbicacionDES.Focus()
                    Exit Sub
                End If
            ElseIf opcActual = opServLlantas.CAMBIAR_OTRA_UNIDAD Then

                If cmbidUbicacion.SelectedValue <> cmbidUbicacionDES.SelectedValue Then
                    'CargaForm(cmbidUbicacionDES.SelectedValue, cmbidtipoUbicacionDES.Text & "DES")
                    '///////////////////////////////////////
                    UniTrans = New UnidadesTranspClass(cmbidUbicacionDES.SelectedValue)
                    If UniTrans.Existe Then
                        txtProfundidadFINAL_DES.Enabled = True
                        TipUni = New TipoUniTransClass(UniTrans.idTipoUnidad)
                        If TipUni.Existe Then
                            txtTipoUniDES.Text = TipUni.Clasificacion
                            ClasLlan = New ClasLlanClass(TipUni.idClasLlan)
                            If ClasLlan.Existe Then
                                txtPosicionDES.Minimum = 1
                                txtPosicionDES.Maximum = TipUni.NoLlantas

                                DibujaEsquema(TipUni.idClasLlan, TipUni.NoLlantas)
                                If txtPosicionDES.Value <> 0 Then
                                    Llanta = New LlantasClass(txtPosicionDES.Value, OpcionLlanta.Posicion, cmbidUbicacionDES.SelectedValue)
                                    If Llanta.Existe Then
                                        txtIdLlantaDES.Text = Llanta.idLlanta
                                        'txtNoSerieDES.Text = Llanta.NoSerie

                                    End If
                                    SeleccionaPosicion(Val(txtPosicion.Value), Color.Yellow, Val(txtPosicionDES.Value), Color.LightGreen)

                                End If
                            End If
                        End If
                    End If
                    '///////////////////////////////////////
                Else
                    MsgBox("Escoja una unidad diferente a: " & cmbidUbicacion.Text, vbInformation, "Aviso" & Me.Text)
                    cmbidUbicacionDES.Focus()
                    Exit Sub
                End If

            ElseIf opcActual = opServLlantas.ROTAR_MISMA_UNIDAD Then
                CargaForm(cmbidUbicacionDES.SelectedValue, cmbidtipoUbicacionDES.Text)
            ElseIf opcActual = opServLlantas.MONTAR_UNIDAD Then
                'CargaForm(cmbidUbicacionDES.SelectedValue, cmbidtipoUbicacionDES.Text)
                UniTrans = New UnidadesTranspClass(cmbidUbicacionDES.SelectedValue)
                If UniTrans.Existe Then
                    TipUni = New TipoUniTransClass(UniTrans.idTipoUnidad)
                    If TipUni.Existe Then
                        txtTipoUniDES.Text = TipUni.Clasificacion
                        ClasLlan = New ClasLlanClass(TipUni.idClasLlan)
                        If ClasLlan.Existe Then
                            txtPosicionDES.Minimum = 1
                            txtPosicionDES.Maximum = TipUni.NoLlantas
                            txtPosicionDES.Value = 1
                            DibujaEsquema(TipUni.idClasLlan, TipUni.NoLlantas)
                            If txtPosicionDES.Value <> 0 Then
                                Llanta = New LlantasClass(txtPosicionDES.Value, OpcionLlanta.Posicion, cmbidUbicacionDES.SelectedValue)
                                If Llanta.Existe Then
                                    'txtIdLlantaDES.Text = Llanta.idLlanta
                                    'txtNoSerieDES.Text = Llanta.NoSerie
                                    MsgBox("Escoja una Posicion diferente de : " & txtPosicionDES.Value & " para la Unidad = " & cmbidUbicacionDES.SelectedValue & " � Seleccione Otra Unidad ", vbInformation, "Aviso" & Me.Text)
                                    Salida = False
                                    txtPosicionDES.Focus()
                                    Exit Sub
                                Else
                                    SeleccionaPosicion(Val(txtPosicion.Value), Color.Yellow, Val(txtPosicionDES.Value), Color.LightGreen)
                                End If


                            End If
                        End If
                    End If
                End If
            End If
            If txtPosicionDES.Enabled Then
                txtPosicionDES.Focus()
            ElseIf txtProfundidadFINAL_DES.Enabled Then
                txtProfundidadFINAL_DES.SelectAll()
                txtProfundidadFINAL_DES.Focus()
            ElseIf txtObserva.Enabled Then
                txtObserva.SelectAll()
                txtObserva.Focus()
            End If
            Salida = False
        End If
    End Sub

    Private Sub txtPosicionDES_KeyUp(sender As Object, e As KeyEventArgs) Handles txtPosicionDES.KeyUp
        If e.KeyValue = Keys.Enter Then
            'Verificar Disponibilidad
            Llanta = New LlantasClass(txtPosicionDES.Value, OpcionLlanta.Posicion, cmbidUbicacionDES.SelectedValue)
            If Llanta.Existe Then
                If opcActual = opServLlantas.ROTAR_MISMA_UNIDAD Then
                    If txtPosicion.Value <> txtPosicionDES.Value Then
                        idSisLlantaDES = Llanta.idSisLlanta
                        PosicionDES = Llanta.Posicion
                        idLLantaDES = Llanta.idLlanta

                        txtProfundidadFINAL_DES.SelectAll()
                        txtProfundidadFINAL_DES.Focus()
                    Else
                        MsgBox("Escoja una posicion diferente a la : " & txtPosicion.Value & " para la Unidad: " & cmbidUbicacionDES.SelectedValue, vbInformation, "Aviso" & Me.Text)
                        txtPosicionDES.Focus()
                    End If

                ElseIf opcActual = opServLlantas.CAMBIAR_OTRA_UNIDAD Then
                    idSisLlantaDES = Llanta.idSisLlanta
                    PosicionDES = Llanta.Posicion
                    idLLantaDES = Llanta.idLlanta

                    txtProfundidadFINAL_DES.SelectAll()
                    txtProfundidadFINAL_DES.Focus()
                ElseIf opcActual = opServLlantas.MONTAR_UNIDAD Then

                    MsgBox("La Posicion: " & txtPosicionDES.Value & " para la Unidad: " & cmbidUbicacionDES.SelectedValue & " Esta Ocupada por la Unidad: " & Llanta.idUbicacion, vbInformation, "Aviso" & Me.Text)
                    txtPosicionDES.Focus()

                End If


            Else
                'txtObserva.SelectAll()
                'txtObserva.Focus()
                If opcActual = opServLlantas.CAMBIAR_OTRA_UNIDAD Then
                    txtObserva.SelectAll()
                    txtObserva.Focus()

                Else
                    txtProfundidadFINAL_DES.SelectAll()
                    txtProfundidadFINAL_DES.Focus()

                End If


            End If


        End If
    End Sub

    Private Sub txtPosicionDES_ValueChanged(sender As Object, e As EventArgs) Handles txtPosicionDES.ValueChanged
        If Salida Then Exit Sub
        If rdbPosicion.Length > 0 Then
            'SeleccionaPosicion(Val(txtPosicionDES.Value), Color.Green)
            SeleccionaPosicion(Val(txtPosicion.Value), Color.Yellow, Val(txtPosicionDES.Value), Color.LightGreen)
            Llanta = New LlantasClass(txtPosicionDES.Value, OpcionLlanta.Posicion, cmbidUbicacionDES.SelectedValue)
            If Llanta.Existe Then
                If CType(cmbActividades.SelectedValue, opServLlantas) = opServLlantas.MONTAR_UNIDAD Then
                    MsgBox("Escoja una Posicion diferente de : " & txtPosicionDES.Value & " para la Unidad = " & cmbidUbicacionDES.SelectedValue & " � Seleccione Otra Unidad ", vbInformation, "Aviso" & Me.Text)
                    Salida = False
                    txtPosicionDES.Focus()
                    'Exit Sub
                Else
                    txtIdLlantaDES.Text = Llanta.idLlanta
                    idSisLlantaDES = Llanta.idSisLlanta
                    txtProfundidadDES.Text = Llanta.ProfundidadAct
                    If idSisLlantaDES > 0 Then
                        gpoOrigen.Enabled = True
                        txtProfundidadFINAL.Text = ""
                        txtProfundidadFINAL_DES.Text = ""
                        txtProfundidadAct.Enabled = False
                        txtProfundidadFINAL.Enabled = True
                        txtProfundidadFINAL_DES.Enabled = True
                    Else
                        gpoOrigen.Enabled = False
                        txtProfundidadAct.Enabled = False
                        txtProfundidadFINAL.Enabled = False
                    End If
                End If
            Else
                'gpoOrigen.Enabled = False

                'gpoDestino.Enabled = False
                'txtProfundidadDES.Text = ""

                If opcActual = opServLlantas.CAMBIAR_OTRA_UNIDAD Then
                    txtProfundidadFINAL.Text = ""
                    txtProfundidadDES.Text = "1"
                    txtProfundidadFINAL_DES.Text = "1"


                ElseIf opcActual = opServLlantas.ROTAR_MISMA_UNIDAD Then
                    txtProfundidadFINAL_DES.Text = txtProfundidadDES.Text
                End If

                txtProfundidadDES.Enabled = False
                txtProfundidadFINAL_DES.Enabled = False

                gpoOrigen.Enabled = True
                txtProfundidadAct.Enabled = False
                txtProfundidadFINAL.Enabled = True
                txtIdLlantaDES.Text = ""
                idSisLlantaDES = 0
            End If
        End If
        Salida = False
    End Sub

    Private Sub cmbidUbicacionDES_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbidUbicacionDES.SelectedIndexChanged

    End Sub

    Private Sub cmbidUbicacionDES_SelectedValueChanged(sender As Object, e As EventArgs) Handles cmbidUbicacionDES.SelectedValueChanged
        Salida = False
    End Sub

    Private Sub txtProfundidadFINAL_DES_TextChanged(sender As Object, e As EventArgs) Handles txtProfundidadFINAL_DES.TextChanged

    End Sub

    Private Sub txtProfundidadFINAL_DES_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtProfundidadFINAL_DES.KeyPress
        If Asc(e.KeyChar) = 13 Then
            txtObserva.SelectAll()
            txtObserva.Focus()
        End If
    End Sub
    Private Sub Cancelar()
        If txtidLlanta.Text <> "" Then
            CargaForm(txtidLlanta.Text, UCase(TablaBd))
        End If
        OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
        btnMnuModificar.Enabled = True
        cmbActividades.SelectedIndex = 0

        txtProfundidadFINAL_DES.Text = 0
        txtObserva.Text = ""

        txtidLlanta.Focus()
    End Sub

    Private Sub cmbActividades_Leave(sender As Object, e As EventArgs) Handles cmbActividades.Leave

    End Sub

    Private Sub OpcionActividad()
        opcActual = CType(cmbActividades.SelectedValue, opServLlantas)
        Select Case CType(cmbActividades.SelectedValue, opServLlantas)
            Case opServLlantas.DESMONTAR
                Salida = True
                If CType(cmbidtipoUbicacion.SelectedValue, TipoUbicacionLLa) = TipoUbicacionLLa.UNIDADTRANS Then
                    gpoDestino.Enabled = True
                    cmbidtipoUbicacionDES.SelectedValue = TipoUbicacionLLa.ALMACEN
                    AutoCompletarUbicaciones(cmbidtipoUbicacionDES.SelectedValue, cmbidUbicacionDES)
                    cmbidtipoUbicacionDES.Enabled = False
                    txtPosicionDES.Value = 0
                    txtPosicionDES.Enabled = False
                    txtObserva.Enabled = True
                    txtProfundidadFINAL.Text = txtProfundidadAct.Text
                    txtProfundidadDES.Text = txtProfundidadAct.Text
                    txtProfundidadDES.Enabled = False
                    btnMnuOk.Enabled = True
                    cmbActividades.Enabled = False
                    btnMnuModificar.Enabled = False
                    btnMnuSalir.Enabled = False
                    cmbidUbicacionDES.Focus()


                Else
                    MsgBox("La Llanta se encuentra en un Almacen,no puede ser DesMontada", vbInformation, "Aviso" & Me.Text)
                    cmbActividades.Focus()
                End If

                Salida = False
            Case opServLlantas.MONTAR_UNIDAD
                Salida = True
                If CType(cmbidtipoUbicacion.SelectedValue, TipoUbicacionLLa) = TipoUbicacionLLa.ALMACEN Then
                    gpoDestino.Enabled = True
                    cmbidtipoUbicacionDES.SelectedValue = TipoUbicacionLLa.UNIDADTRANS

                    AutoCompletarUbicaciones(cmbidtipoUbicacionDES.SelectedValue, cmbidUbicacionDES)
                    cmbidtipoUbicacionDES.Enabled = False
                    txtObserva.Enabled = True
                    btnMnuOk.Enabled = True
                    txtProfundidadDES.Text = txtProfundidadAct.Text
                    txtProfundidadDES.Enabled = False
                    cmbActividades.Enabled = False
                    btnMnuModificar.Enabled = False
                    btnMnuSalir.Enabled = False

                    btnMnuOk.Enabled = True
                    cmbActividades.Enabled = False
                    btnMnuModificar.Enabled = False
                    btnMnuSalir.Enabled = False
                    cmbidUbicacionDES.Focus()

                Else
                    MsgBox("La Llanta no se encuentra en un Almacen,no puede ser Montada", vbInformation, "Aviso" & Me.Text)
                    cmbActividades.Focus()
                End If

                Salida = False
            Case opServLlantas.CAMBIO_ALMANCEN
                Salida = True
                If CType(cmbidtipoUbicacion.SelectedValue, TipoUbicacionLLa) = TipoUbicacionLLa.ALMACEN Then
                    gpoDestino.Enabled = True
                    cmbidtipoUbicacionDES.SelectedValue = TipoUbicacionLLa.ALMACEN
                    AutoCompletarUbicaciones(cmbidtipoUbicacionDES.SelectedValue, cmbidUbicacionDES)
                    cmbidtipoUbicacionDES.Enabled = False
                    txtPosicionDES.Value = 0
                    txtPosicionDES.Enabled = False
                    txtObserva.Enabled = True
                    btnMnuOk.Enabled = True
                    cmbActividades.Enabled = False
                    btnMnuModificar.Enabled = False
                    btnMnuSalir.Enabled = False
                    txtProfundidadDES.Text = txtProfundidadAct.Text
                    cmbidUbicacionDES.Focus()
                Else
                    MsgBox("La Llanta no se encuentra en un Almacen,no puede ser Cambiar de Almacen", vbInformation, "Aviso" & Me.Text)
                    cmbActividades.Focus()
                End If
                Salida = False
            Case opServLlantas.RECHAZADA_PROVEEDOR
                Salida = True
                If CType(cmbidtipoUbicacion.SelectedValue, TipoUbicacionLLa) = TipoUbicacionLLa.PROVEEDOR Then

                Else
                    MsgBox("La Llanta no se encuentra con un Proveedor,no procede la Actividad", vbInformation, "Aviso" & Me.Text)
                    cmbActividades.Focus()

                End If
                Salida = False

            Case opServLlantas.ROTAR_MISMA_UNIDAD
                Salida = True
                If CType(cmbidtipoUbicacion.SelectedValue, TipoUbicacionLLa) <> TipoUbicacionLLa.UNIDADTRANS Then
                    MsgBox("La Llanta no se encuentra en una Unidad de Transporte,no puede ser Rotada", vbInformation, "Aviso" & Me.Text)
                    cmbActividades.Focus()
                Else
                    gpoDestino.Enabled = True
                    cmbidtipoUbicacionDES.SelectedValue = TipoUbicacionLLa.UNIDADTRANS
                    AutoCompletarUbicaciones(cmbidtipoUbicacionDES.SelectedValue, cmbidUbicacionDES)
                    cmbidtipoUbicacionDES.Enabled = False
                    txtObserva.Enabled = True
                    cmbidUbicacionDES.SelectedValue = cmbidUbicacion.SelectedValue
                    'Llanta = New LlantasClass(txtPosici, OpcionLlanta.Posicion ,cmbidUbicacionDES.SelectedValue)
                    '///////////////////////////////////////
                    UniTrans = New UnidadesTranspClass(cmbidUbicacionDES.SelectedValue)
                    If UniTrans.Existe Then
                        TipUni = New TipoUniTransClass(UniTrans.idTipoUnidad)
                        If TipUni.Existe Then
                            txtTipoUniDES.Text = TipUni.Clasificacion
                            ClasLlan = New ClasLlanClass(TipUni.idClasLlan)
                            If ClasLlan.Existe Then
                                txtPosicionDES.Minimum = 1
                                txtPosicionDES.Maximum = TipUni.NoLlantas

                                DibujaEsquema(TipUni.idClasLlan, TipUni.NoLlantas)
                                If txtPosicionDES.Value <> 0 Then
                                    Llanta = New LlantasClass(txtPosicionDES.Value, OpcionLlanta.Posicion, cmbidUbicacionDES.SelectedValue)
                                    If Llanta.Existe Then
                                        txtIdLlantaDES.Text = Llanta.idLlanta
                                        idSisLlantaDES = Llanta.idSisLlanta
                                        'txtNoSerieDES.Text = Llanta.NoSerie
                                    Else
                                        txtIdLlantaDES.Text = ""
                                        idSisLlantaDES = 0

                                    End If
                                    SeleccionaPosicion(Val(txtPosicion.Value), Color.Yellow, Val(txtPosicionDES.Value), Color.LightGreen)

                                End If
                            End If
                        End If
                    End If
                    '///////////////////////////////////////


                    txtProfundidadDES.Text = txtProfundidadAct.Text
                    txtProfundidadDES.Enabled = False
                    cmbidUbicacionDES.Enabled = False

                    btnMnuOk.Enabled = True
                    cmbActividades.Enabled = False
                    btnMnuModificar.Enabled = False
                    btnMnuSalir.Enabled = False

                    txtPosicionDES.Focus()
                End If



                Salida = False
            Case opServLlantas.CAMBIAR_OTRA_UNIDAD
                Salida = True
                If CType(cmbidtipoUbicacion.SelectedValue, TipoUbicacionLLa) <> TipoUbicacionLLa.UNIDADTRANS Then
                    MsgBox("La Llanta no se encuentra en una Unidad de Transporte,no puede ser Rotada", vbInformation, "Aviso" & Me.Text)
                    cmbActividades.Focus()
                Else
                    gpoDestino.Enabled = True
                    cmbidtipoUbicacionDES.SelectedValue = TipoUbicacionLLa.UNIDADTRANS
                    AutoCompletarUbicaciones(cmbidtipoUbicacionDES.SelectedValue, cmbidUbicacionDES)
                    cmbidtipoUbicacionDES.Enabled = False
                    txtObserva.Enabled = True
                    txtProfundidadDES.Enabled = True
                    'cmbidUbicacionDES.SelectedValue = cmbidUbicacion.SelectedValue
                    '///////////////////////////////////////
                    UniTrans = New UnidadesTranspClass(cmbidUbicacionDES.SelectedValue)
                    If UniTrans.Existe Then
                        TipUni = New TipoUniTransClass(UniTrans.idTipoUnidad)
                        If TipUni.Existe Then
                            txtTipoUniDES.Text = TipUni.Clasificacion
                            ClasLlan = New ClasLlanClass(TipUni.idClasLlan)
                            If ClasLlan.Existe Then
                                txtPosicionDES.Minimum = 1
                                txtPosicionDES.Maximum = TipUni.NoLlantas

                                DibujaEsquema(TipUni.idClasLlan, TipUni.NoLlantas)
                                If txtPosicionDES.Value <> 0 Then
                                    Llanta = New LlantasClass(txtPosicionDES.Value, OpcionLlanta.Posicion, cmbidUbicacionDES.SelectedValue)
                                    If Llanta.Existe Then
                                        txtIdLlantaDES.Text = Llanta.idLlanta
                                        idSisLlantaDES = Llanta.idSisLlanta
                                        'txtNoSerieDES.Text = Llanta.NoSerie
                                    Else
                                        txtIdLlantaDES.Text = ""
                                        idSisLlantaDES = 0

                                    End If
                                    SeleccionaPosicion(Val(txtPosicion.Value), Color.Yellow, Val(txtPosicionDES.Value), Color.LightGreen)

                                End If
                            End If
                        End If
                    End If
                    '///////////////////////////////////////

                    btnMnuOk.Enabled = True
                    cmbActividades.Enabled = False
                    btnMnuModificar.Enabled = False
                    btnMnuSalir.Enabled = False
                    cmbidUbicacionDES.Enabled = True
                    cmbidUbicacionDES.Focus()
                End If
                Salida = False


        End Select
    End Sub
End Class
