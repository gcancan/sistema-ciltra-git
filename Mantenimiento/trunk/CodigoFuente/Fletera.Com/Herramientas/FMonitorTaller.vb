'Fecha: 15 / Febrero / 2016
'ROGER GALA PACHECO
'************************************************************************************************************************************
'*  Fecha Modificacion :   15 / Febrero / 2016                                                                                       
'*  
'************************************************************************************************************************************
Imports FiltroDinamicoGrid

Public Class FMonitorTaller
    Inherits System.Windows.Forms.Form
    Public WithEvents MeStatus1 As ControlStatus.MeStatus
    Private _Tag As String
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    'Dim Salida As Boolean
    Dim ObjSql As New ToolSQLs
    Dim StrSql As String
    Dim indice As Integer = 0
    Friend WithEvents grdMonitor As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents ChkEnt As System.Windows.Forms.CheckBox
    Friend WithEvents ChkCan As System.Windows.Forms.CheckBox
    Friend WithEvents ChkTer As System.Windows.Forms.CheckBox
    Friend WithEvents ChkAsig As System.Windows.Forms.CheckBox
    Friend WithEvents ChkCap As System.Windows.Forms.CheckBox
    Friend WithEvents ChkRecep As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents ChkHoy As System.Windows.Forms.CheckBox
    Friend WithEvents ChkTmpReal As System.Windows.Forms.CheckBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Cale1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents TSActualizar As System.Windows.Forms.ToolStripButton
    Friend WithEvents TSFiltroAvanzado As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
    Dim ArraySql() As String

    Private _ColorCap As Color = Color.LightGray
    Private _ColorRec As Color = Color.LightSalmon
    Private _ColorAsig As Color = Color.LightBlue
    'Private _ColorDiag As Color = Color.LightGoldenrodYellow

    Private _ColorTer As Color = Color.LightGreen
    'Private _ColorEnt As Color = Color.LightGoldenrodYellow
    Private _ColorEnt As Color = Color.Orange

    Private _ColorCan As Color = Color.OrangeRed

    Private _ColorExterno As Color = Color.Yellow

    Private _TiempoActu As Integer = 20 'Cada 10 segundos

    Dim FAct As Boolean = False

    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents LTiempo As System.Windows.Forms.Label
    Friend WithEvents Context1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents MAsigServ As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents MReimprimir As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MReimp2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents MResumen As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents MRecepServ As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MEntregServ As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents grdMonitorDet As System.Windows.Forms.DataGridView
    Friend WithEvents MCancelar As System.Windows.Forms.ToolStripMenuItem

    Dim IdOrdenServ As Integer

    Dim TablaBd As String = "OT"
    Friend WithEvents Context2 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents MTrabDiag As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MTrabFin As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MTrabCan As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MTrabSep1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents MTrabRes As System.Windows.Forms.ToolStripMenuItem
    Dim MyTablaDet As DataTable = New DataTable(TablaBd)

    Private _con As String
    Private _Usuario As String
    Private _IdUsuario As Integer
    Private _cveEmpresa As Integer
    Friend WithEvents gpoTOS As GroupBox
    Friend WithEvents chkTosTaller As CheckBox
    Friend WithEvents chkTosLlan As CheckBox
    Friend WithEvents chkTosComb As CheckBox
    Friend WithEvents chkTosLav As CheckBox
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents Label9 As Label
    Friend WithEvents txtOrden As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents txtPadre As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents txtUnidad As TextBox
    Friend WithEvents Label33 As Label
    Private _OpcTipoServ As TipoOrdenServicio
    Private _bTodasEmpresas As Boolean

    Dim Empresa As EmpresaClass
    Dim PadreOS As MasterOrdServClass
    Dim ordenes As OrdenServClass
    Friend WithEvents cmbIdEmpresa As ComboBox
    Friend WithEvents chkEnvTallerExt As CheckBox
    Dim UniTrans As UnidadesTranspClass


#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FMonitorTaller))
        Me.MeStatus1 = New ControlStatus.MeStatus()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.grdMonitor = New System.Windows.Forms.DataGridView()
        Me.Context1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.MRecepServ = New System.Windows.Forms.ToolStripMenuItem()
        Me.MAsigServ = New System.Windows.Forms.ToolStripMenuItem()
        Me.MEntregServ = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.MReimprimir = New System.Windows.Forms.ToolStripMenuItem()
        Me.MReimp2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.MResumen = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.MCancelar = New System.Windows.Forms.ToolStripMenuItem()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.ChkEnt = New System.Windows.Forms.CheckBox()
        Me.ChkCan = New System.Windows.Forms.CheckBox()
        Me.ChkTer = New System.Windows.Forms.CheckBox()
        Me.ChkAsig = New System.Windows.Forms.CheckBox()
        Me.ChkCap = New System.Windows.Forms.CheckBox()
        Me.ChkRecep = New System.Windows.Forms.CheckBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.ChkHoy = New System.Windows.Forms.CheckBox()
        Me.ChkTmpReal = New System.Windows.Forms.CheckBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Cale1 = New System.Windows.Forms.DateTimePicker()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.TSFiltroAvanzado = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.TSActualizar = New System.Windows.Forms.ToolStripButton()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.LTiempo = New System.Windows.Forms.Label()
        Me.grdMonitorDet = New System.Windows.Forms.DataGridView()
        Me.Context2 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.MTrabDiag = New System.Windows.Forms.ToolStripMenuItem()
        Me.MTrabFin = New System.Windows.Forms.ToolStripMenuItem()
        Me.MTrabCan = New System.Windows.Forms.ToolStripMenuItem()
        Me.MTrabSep1 = New System.Windows.Forms.ToolStripSeparator()
        Me.MTrabRes = New System.Windows.Forms.ToolStripMenuItem()
        Me.gpoTOS = New System.Windows.Forms.GroupBox()
        Me.chkTosTaller = New System.Windows.Forms.CheckBox()
        Me.chkTosLlan = New System.Windows.Forms.CheckBox()
        Me.chkTosComb = New System.Windows.Forms.CheckBox()
        Me.chkTosLav = New System.Windows.Forms.CheckBox()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.cmbIdEmpresa = New System.Windows.Forms.ComboBox()
        Me.txtOrden = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtPadre = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtUnidad = New System.Windows.Forms.TextBox()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.chkEnvTallerExt = New System.Windows.Forms.CheckBox()
        CType(Me.grdMonitor, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Context1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.ToolStrip1.SuspendLayout()
        CType(Me.grdMonitorDet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Context2.SuspendLayout()
        Me.gpoTOS.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.SuspendLayout()
        '
        'MeStatus1
        '
        Me.MeStatus1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.MeStatus1.Location = New System.Drawing.Point(0, 455)
        Me.MeStatus1.Margin = New System.Windows.Forms.Padding(4)
        Me.MeStatus1.Name = "MeStatus1"
        Me.MeStatus1.Size = New System.Drawing.Size(1118, 26)
        Me.MeStatus1.TabIndex = 72
        '
        'grdMonitor
        '
        Me.grdMonitor.AllowUserToAddRows = False
        Me.grdMonitor.AllowUserToDeleteRows = False
        Me.grdMonitor.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdMonitor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdMonitor.ContextMenuStrip = Me.Context1
        Me.grdMonitor.Location = New System.Drawing.Point(3, 200)
        Me.grdMonitor.Name = "grdMonitor"
        Me.grdMonitor.Size = New System.Drawing.Size(1094, 139)
        Me.grdMonitor.TabIndex = 73
        '
        'Context1
        '
        Me.Context1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MRecepServ, Me.MAsigServ, Me.MEntregServ, Me.ToolStripSeparator1, Me.MReimprimir, Me.MReimp2, Me.ToolStripSeparator2, Me.MResumen, Me.ToolStripSeparator3, Me.MCancelar})
        Me.Context1.Name = "Context1"
        Me.Context1.Size = New System.Drawing.Size(214, 176)
        '
        'MRecepServ
        '
        Me.MRecepServ.Name = "MRecepServ"
        Me.MRecepServ.Size = New System.Drawing.Size(213, 22)
        Me.MRecepServ.Text = "Recepcionar Unidad"
        '
        'MAsigServ
        '
        Me.MAsigServ.Name = "MAsigServ"
        Me.MAsigServ.Size = New System.Drawing.Size(213, 22)
        Me.MAsigServ.Text = "Asignar Servicio"
        '
        'MEntregServ
        '
        Me.MEntregServ.Name = "MEntregServ"
        Me.MEntregServ.Size = New System.Drawing.Size(213, 22)
        Me.MEntregServ.Text = "Entregar Unidad"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(210, 6)
        '
        'MReimprimir
        '
        Me.MReimprimir.Name = "MReimprimir"
        Me.MReimprimir.Size = New System.Drawing.Size(213, 22)
        Me.MReimprimir.Text = "Reimprimir Orden Servicio"
        '
        'MReimp2
        '
        Me.MReimp2.Name = "MReimp2"
        Me.MReimp2.Size = New System.Drawing.Size(213, 22)
        Me.MReimp2.Text = "Reimprimir Formato2"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(210, 6)
        '
        'MResumen
        '
        Me.MResumen.Name = "MResumen"
        Me.MResumen.Size = New System.Drawing.Size(213, 22)
        Me.MResumen.Text = "Ver Resumen"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(210, 6)
        '
        'MCancelar
        '
        Me.MCancelar.Name = "MCancelar"
        Me.MCancelar.Size = New System.Drawing.Size(213, 22)
        Me.MCancelar.Text = "Cancelar Servicio"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.ChkEnt)
        Me.GroupBox1.Controls.Add(Me.ChkCan)
        Me.GroupBox1.Controls.Add(Me.ChkTer)
        Me.GroupBox1.Controls.Add(Me.ChkAsig)
        Me.GroupBox1.Controls.Add(Me.ChkCap)
        Me.GroupBox1.Controls.Add(Me.ChkRecep)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.GroupBox1.Location = New System.Drawing.Point(3, 28)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(864, 48)
        Me.GroupBox1.TabIndex = 74
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Estatus de las Ordenes de Servicio"
        '
        'ChkEnt
        '
        Me.ChkEnt.AutoSize = True
        Me.ChkEnt.BackColor = System.Drawing.SystemColors.Control
        Me.ChkEnt.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ChkEnt.ForeColor = System.Drawing.Color.Black
        Me.ChkEnt.Location = New System.Drawing.Point(617, 21)
        Me.ChkEnt.Name = "ChkEnt"
        Me.ChkEnt.Size = New System.Drawing.Size(97, 20)
        Me.ChkEnt.TabIndex = 6
        Me.ChkEnt.Text = "Entregados"
        Me.ChkEnt.UseVisualStyleBackColor = False
        '
        'ChkCan
        '
        Me.ChkCan.AutoSize = True
        Me.ChkCan.BackColor = System.Drawing.SystemColors.Control
        Me.ChkCan.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ChkCan.ForeColor = System.Drawing.Color.Black
        Me.ChkCan.Location = New System.Drawing.Point(758, 21)
        Me.ChkCan.Name = "ChkCan"
        Me.ChkCan.Size = New System.Drawing.Size(100, 20)
        Me.ChkCan.TabIndex = 3
        Me.ChkCan.Text = "Cancelados"
        Me.ChkCan.UseVisualStyleBackColor = False
        '
        'ChkTer
        '
        Me.ChkTer.AutoSize = True
        Me.ChkTer.BackColor = System.Drawing.SystemColors.Control
        Me.ChkTer.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ChkTer.ForeColor = System.Drawing.Color.Black
        Me.ChkTer.Location = New System.Drawing.Point(503, 21)
        Me.ChkTer.Name = "ChkTer"
        Me.ChkTer.Size = New System.Drawing.Size(100, 20)
        Me.ChkTer.TabIndex = 2
        Me.ChkTer.Text = "Terminados"
        Me.ChkTer.UseVisualStyleBackColor = False
        '
        'ChkAsig
        '
        Me.ChkAsig.AutoSize = True
        Me.ChkAsig.BackColor = System.Drawing.SystemColors.Control
        Me.ChkAsig.Checked = True
        Me.ChkAsig.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ChkAsig.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ChkAsig.ForeColor = System.Drawing.Color.Black
        Me.ChkAsig.Location = New System.Drawing.Point(246, 21)
        Me.ChkAsig.Name = "ChkAsig"
        Me.ChkAsig.Size = New System.Drawing.Size(92, 20)
        Me.ChkAsig.TabIndex = 1
        Me.ChkAsig.Text = "Asignados"
        Me.ChkAsig.UseVisualStyleBackColor = False
        '
        'ChkCap
        '
        Me.ChkCap.AutoSize = True
        Me.ChkCap.BackColor = System.Drawing.SystemColors.Control
        Me.ChkCap.Checked = True
        Me.ChkCap.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ChkCap.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ChkCap.ForeColor = System.Drawing.Color.Black
        Me.ChkCap.Location = New System.Drawing.Point(13, 21)
        Me.ChkCap.Name = "ChkCap"
        Me.ChkCap.Size = New System.Drawing.Size(97, 20)
        Me.ChkCap.TabIndex = 0
        Me.ChkCap.Text = "Capturados"
        Me.ChkCap.UseVisualStyleBackColor = False
        '
        'ChkRecep
        '
        Me.ChkRecep.AutoSize = True
        Me.ChkRecep.BackColor = System.Drawing.SystemColors.Control
        Me.ChkRecep.Checked = True
        Me.ChkRecep.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ChkRecep.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ChkRecep.ForeColor = System.Drawing.Color.Black
        Me.ChkRecep.Location = New System.Drawing.Point(116, 21)
        Me.ChkRecep.Name = "ChkRecep"
        Me.ChkRecep.Size = New System.Drawing.Size(124, 20)
        Me.ChkRecep.TabIndex = 4
        Me.ChkRecep.Text = "Recepcionados"
        Me.ChkRecep.UseVisualStyleBackColor = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.ChkHoy)
        Me.GroupBox2.Controls.Add(Me.ChkTmpReal)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.Cale1)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.ForeColor = System.Drawing.Color.Black
        Me.GroupBox2.Location = New System.Drawing.Point(3, 145)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(477, 49)
        Me.GroupBox2.TabIndex = 75
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Opciones de Busqueda"
        '
        'ChkHoy
        '
        Me.ChkHoy.AutoSize = True
        Me.ChkHoy.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ChkHoy.Location = New System.Drawing.Point(129, 20)
        Me.ChkHoy.Name = "ChkHoy"
        Me.ChkHoy.Size = New System.Drawing.Size(128, 20)
        Me.ChkHoy.TabIndex = 10
        Me.ChkHoy.Text = "Solo Fecha HOY"
        Me.ChkHoy.UseVisualStyleBackColor = True
        '
        'ChkTmpReal
        '
        Me.ChkTmpReal.AutoSize = True
        Me.ChkTmpReal.Checked = True
        Me.ChkTmpReal.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ChkTmpReal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ChkTmpReal.Location = New System.Drawing.Point(13, 22)
        Me.ChkTmpReal.Name = "ChkTmpReal"
        Me.ChkTmpReal.Size = New System.Drawing.Size(119, 20)
        Me.ChkTmpReal.TabIndex = 9
        Me.ChkTmpReal.Text = "En tiempo Real"
        Me.ChkTmpReal.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(253, 18)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(112, 20)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Buscar Fecha:"
        '
        'Cale1
        '
        Me.Cale1.Enabled = False
        Me.Cale1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Cale1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.Cale1.Location = New System.Drawing.Point(370, 16)
        Me.Cale1.Name = "Cale1"
        Me.Cale1.Size = New System.Drawing.Size(105, 22)
        Me.Cale1.TabIndex = 7
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TSFiltroAvanzado, Me.ToolStripButton1, Me.TSActualizar})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(1118, 25)
        Me.ToolStrip1.TabIndex = 76
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'TSFiltroAvanzado
        '
        Me.TSFiltroAvanzado.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TSFiltroAvanzado.Name = "TSFiltroAvanzado"
        Me.TSFiltroAvanzado.Size = New System.Drawing.Size(133, 22)
        Me.TSFiltroAvanzado.Text = "Activar Filtro Avanzado"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.ToolStripButton1.Image = Global.Fletera.My.Resources.Resources.Salida2
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(49, 22)
        Me.ToolStripButton1.Text = "Salir"
        '
        'TSActualizar
        '
        Me.TSActualizar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.TSActualizar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.TSActualizar.Image = CType(resources.GetObject("TSActualizar.Image"), System.Drawing.Image)
        Me.TSActualizar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TSActualizar.Name = "TSActualizar"
        Me.TSActualizar.Size = New System.Drawing.Size(23, 22)
        Me.TSActualizar.Text = "Actualizar"
        '
        'Timer1
        '
        Me.Timer1.Interval = 1000
        '
        'LTiempo
        '
        Me.LTiempo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LTiempo.AutoSize = True
        Me.LTiempo.Font = New System.Drawing.Font("Century Gothic", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LTiempo.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.LTiempo.Location = New System.Drawing.Point(1052, 28)
        Me.LTiempo.Name = "LTiempo"
        Me.LTiempo.Size = New System.Drawing.Size(39, 30)
        Me.LTiempo.TabIndex = 78
        Me.LTiempo.Text = "10"
        '
        'grdMonitorDet
        '
        Me.grdMonitorDet.AllowUserToAddRows = False
        Me.grdMonitorDet.AllowUserToDeleteRows = False
        Me.grdMonitorDet.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdMonitorDet.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdMonitorDet.ContextMenuStrip = Me.Context2
        Me.grdMonitorDet.Location = New System.Drawing.Point(3, 345)
        Me.grdMonitorDet.Name = "grdMonitorDet"
        Me.grdMonitorDet.Size = New System.Drawing.Size(1094, 103)
        Me.grdMonitorDet.TabIndex = 79
        '
        'Context2
        '
        Me.Context2.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MTrabDiag, Me.MTrabFin, Me.MTrabCan, Me.MTrabSep1, Me.MTrabRes})
        Me.Context2.Name = "Context1"
        Me.Context2.Size = New System.Drawing.Size(183, 98)
        '
        'MTrabDiag
        '
        Me.MTrabDiag.Name = "MTrabDiag"
        Me.MTrabDiag.Size = New System.Drawing.Size(182, 22)
        Me.MTrabDiag.Text = "Diagnosticar Trabajo"
        '
        'MTrabFin
        '
        Me.MTrabFin.Name = "MTrabFin"
        Me.MTrabFin.Size = New System.Drawing.Size(182, 22)
        Me.MTrabFin.Text = "Terminar Trabajo"
        '
        'MTrabCan
        '
        Me.MTrabCan.Name = "MTrabCan"
        Me.MTrabCan.Size = New System.Drawing.Size(182, 22)
        Me.MTrabCan.Text = "Cancelar Servicio"
        '
        'MTrabSep1
        '
        Me.MTrabSep1.Name = "MTrabSep1"
        Me.MTrabSep1.Size = New System.Drawing.Size(179, 6)
        '
        'MTrabRes
        '
        Me.MTrabRes.Name = "MTrabRes"
        Me.MTrabRes.Size = New System.Drawing.Size(182, 22)
        Me.MTrabRes.Text = "Ver Resumen"
        '
        'gpoTOS
        '
        Me.gpoTOS.Controls.Add(Me.chkTosTaller)
        Me.gpoTOS.Controls.Add(Me.chkTosLlan)
        Me.gpoTOS.Controls.Add(Me.chkTosComb)
        Me.gpoTOS.Controls.Add(Me.chkTosLav)
        Me.gpoTOS.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gpoTOS.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.gpoTOS.Location = New System.Drawing.Point(3, 82)
        Me.gpoTOS.Name = "gpoTOS"
        Me.gpoTOS.Size = New System.Drawing.Size(444, 57)
        Me.gpoTOS.TabIndex = 80
        Me.gpoTOS.TabStop = False
        Me.gpoTOS.Text = "Tipos de Ordenes de Servicio"
        '
        'chkTosTaller
        '
        Me.chkTosTaller.AutoSize = True
        Me.chkTosTaller.BackColor = System.Drawing.SystemColors.Control
        Me.chkTosTaller.Checked = True
        Me.chkTosTaller.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkTosTaller.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkTosTaller.ForeColor = System.Drawing.Color.Black
        Me.chkTosTaller.Location = New System.Drawing.Point(370, 21)
        Me.chkTosTaller.Name = "chkTosTaller"
        Me.chkTosTaller.Size = New System.Drawing.Size(62, 20)
        Me.chkTosTaller.TabIndex = 5
        Me.chkTosTaller.Text = "Taller"
        Me.chkTosTaller.UseVisualStyleBackColor = False
        '
        'chkTosLlan
        '
        Me.chkTosLlan.AutoSize = True
        Me.chkTosLlan.BackColor = System.Drawing.SystemColors.Control
        Me.chkTosLlan.Checked = True
        Me.chkTosLlan.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkTosLlan.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkTosLlan.ForeColor = System.Drawing.Color.Black
        Me.chkTosLlan.Location = New System.Drawing.Point(268, 21)
        Me.chkTosLlan.Name = "chkTosLlan"
        Me.chkTosLlan.Size = New System.Drawing.Size(70, 20)
        Me.chkTosLlan.TabIndex = 1
        Me.chkTosLlan.Text = "Llantas"
        Me.chkTosLlan.UseVisualStyleBackColor = False
        '
        'chkTosComb
        '
        Me.chkTosComb.AutoSize = True
        Me.chkTosComb.BackColor = System.Drawing.SystemColors.Control
        Me.chkTosComb.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkTosComb.ForeColor = System.Drawing.Color.Black
        Me.chkTosComb.Location = New System.Drawing.Point(13, 21)
        Me.chkTosComb.Name = "chkTosComb"
        Me.chkTosComb.Size = New System.Drawing.Size(102, 20)
        Me.chkTosComb.TabIndex = 0
        Me.chkTosComb.Text = "Combustible"
        Me.chkTosComb.UseVisualStyleBackColor = False
        '
        'chkTosLav
        '
        Me.chkTosLav.AutoSize = True
        Me.chkTosLav.BackColor = System.Drawing.SystemColors.Control
        Me.chkTosLav.Checked = True
        Me.chkTosLav.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkTosLav.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkTosLav.ForeColor = System.Drawing.Color.Black
        Me.chkTosLav.Location = New System.Drawing.Point(155, 21)
        Me.chkTosLav.Name = "chkTosLav"
        Me.chkTosLav.Size = New System.Drawing.Size(85, 20)
        Me.chkTosLav.TabIndex = 4
        Me.chkTosLav.Text = "Lavadero"
        Me.chkTosLav.UseVisualStyleBackColor = False
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.chkEnvTallerExt)
        Me.GroupBox4.Controls.Add(Me.cmbIdEmpresa)
        Me.GroupBox4.Controls.Add(Me.txtOrden)
        Me.GroupBox4.Controls.Add(Me.Label3)
        Me.GroupBox4.Controls.Add(Me.txtPadre)
        Me.GroupBox4.Controls.Add(Me.Label2)
        Me.GroupBox4.Controls.Add(Me.txtUnidad)
        Me.GroupBox4.Controls.Add(Me.Label33)
        Me.GroupBox4.Controls.Add(Me.Label9)
        Me.GroupBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.GroupBox4.Location = New System.Drawing.Point(485, 82)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(624, 112)
        Me.GroupBox4.TabIndex = 81
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Otros Filtros"
        '
        'cmbIdEmpresa
        '
        Me.cmbIdEmpresa.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbIdEmpresa.FormattingEnabled = True
        Me.cmbIdEmpresa.Location = New System.Drawing.Point(69, 25)
        Me.cmbIdEmpresa.Name = "cmbIdEmpresa"
        Me.cmbIdEmpresa.Size = New System.Drawing.Size(124, 21)
        Me.cmbIdEmpresa.TabIndex = 115
        '
        'txtOrden
        '
        Me.txtOrden.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtOrden.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOrden.Location = New System.Drawing.Point(530, 25)
        Me.txtOrden.MaxLength = 10
        Me.txtOrden.Name = "txtOrden"
        Me.txtOrden.Size = New System.Drawing.Size(88, 20)
        Me.txtOrden.TabIndex = 113
        Me.txtOrden.Tag = "1"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label3.Location = New System.Drawing.Point(480, 28)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(46, 13)
        Me.Label3.TabIndex = 114
        Me.Label3.Text = "ORDEN"
        '
        'txtPadre
        '
        Me.txtPadre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPadre.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPadre.Location = New System.Drawing.Point(386, 25)
        Me.txtPadre.MaxLength = 10
        Me.txtPadre.Name = "txtPadre"
        Me.txtPadre.Size = New System.Drawing.Size(88, 20)
        Me.txtPadre.TabIndex = 111
        Me.txtPadre.Tag = "1"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label2.Location = New System.Drawing.Point(342, 28)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(38, 13)
        Me.Label2.TabIndex = 112
        Me.Label2.Text = "FOLIO"
        '
        'txtUnidad
        '
        Me.txtUnidad.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtUnidad.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtUnidad.Location = New System.Drawing.Point(248, 25)
        Me.txtUnidad.MaxLength = 10
        Me.txtUnidad.Name = "txtUnidad"
        Me.txtUnidad.Size = New System.Drawing.Size(88, 20)
        Me.txtUnidad.TabIndex = 108
        Me.txtUnidad.Tag = "1"
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label33.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label33.Location = New System.Drawing.Point(192, 28)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(49, 13)
        Me.Label33.TabIndex = 109
        Me.Label33.Text = "UNIDAD"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label9.Location = New System.Drawing.Point(6, 28)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(59, 13)
        Me.Label9.TabIndex = 106
        Me.Label9.Text = "EMPRESA"
        '
        'chkEnvTallerExt
        '
        Me.chkEnvTallerExt.AutoSize = True
        Me.chkEnvTallerExt.BackColor = System.Drawing.SystemColors.Control
        Me.chkEnvTallerExt.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkEnvTallerExt.ForeColor = System.Drawing.Color.Black
        Me.chkEnvTallerExt.Location = New System.Drawing.Point(9, 63)
        Me.chkEnvTallerExt.Name = "chkEnvTallerExt"
        Me.chkEnvTallerExt.Size = New System.Drawing.Size(174, 20)
        Me.chkEnvTallerExt.TabIndex = 116
        Me.chkEnvTallerExt.Text = "Enviado a Taller Externo"
        Me.chkEnvTallerExt.UseVisualStyleBackColor = False
        '
        'FMonitorTaller
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1118, 481)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.gpoTOS)
        Me.Controls.Add(Me.grdMonitorDet)
        Me.Controls.Add(Me.LTiempo)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.grdMonitor)
        Me.Controls.Add(Me.MeStatus1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimizeBox = False
        Me.Name = "FMonitorTaller"
        Me.Text = "Monitor Taller"
        CType(Me.grdMonitor, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Context1.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        CType(Me.grdMonitorDet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Context2.ResumeLayout(False)
        Me.gpoTOS.ResumeLayout(False)
        Me.gpoTOS.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Sub New(ByVal vTag As String)
        InitializeComponent()
        _Tag = vTag
    End Sub

    Public Sub New(con As String, nomUsuario As String, cveEmpresa As Integer, IdUsuario As Integer,
    OpcTipoServ As TipoOrdenServicio, bTodasEmpresas As Boolean)
        _con = con
        BD = New CapaDatos.UtilSQL(con, "Juan")
        InitializeComponent()
        _Usuario = nomUsuario
        _cveEmpresa = cveEmpresa
        _IdUsuario = IdUsuario
        Inicio.CONSTR = _con
        _OpcTipoServ = OpcTipoServ
        _bTodasEmpresas = bTodasEmpresas

    End Sub

    Private Sub btnMnuSalir_Click(sender As Object, e As EventArgs)
        Me.Close()
    End Sub

    Private Sub FMonitorTaller_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'grdMonitor.MaximumSize.Width = Me.Width - 120
        If Inicio.CONSTR = "" Then
            Inicio.CONSTR = _con
        End If

        LTiempo.Text = _TiempoActu
        ChkCap.BackColor = _ColorCap
        ChkRecep.BackColor = _ColorRec
        ChkAsig.BackColor = _ColorAsig
        ChkTer.BackColor = _ColorTer
        ChkCan.BackColor = _ColorCan
        'ChkDiag.BackColor = _ColorDiag
        ChkEnt.BackColor = _ColorEnt

        chkEnvTallerExt.BackColor = _ColorExterno

        ChkHoy.Checked = True

        AutoaCompletar(_cveEmpresa)
        PermisosFiltros(_bTodasEmpresas)
        PermisosTiposOS(_OpcTipoServ)

        Me.KeyPreview = True
        CargaGrid()
        grdMonitor.DefaultCellStyle.ForeColor = Color.Black
        Timer1.Enabled = True
        Status("Monitor de Servicios Iniciado", Me)

    End Sub


    Private Sub CargaGrid()

        Dim SWhere As String = "", sStatusFiltro As String = ""
        Dim STOS As String = ""
        Dim sOtrosfilt As String = ""
        Dim filtroEmpresa As String = ""
        Dim filtroExterno As String = ""
        Dim filtroUnidad As String = ""
        Dim filtroFolio As String = ""
        Dim filtroOrdenServ As String = ""

        'ESTATUS DE ORDENES DE SERVICIO
        If ChkCap.Checked Then
            SWhere += "'PRE',"
        End If
        If ChkAsig.Checked Then
            SWhere += "'ASIG',"
        End If
        If ChkTer.Checked Then
            SWhere += "'TER',"
        End If
        If ChkCan.Checked Then
            SWhere += "'CAN',"
        End If
        'If ChkDiag.Checked Then
        '    SWhere += "'DIAG',"
        'End If
        If ChkEnt.Checked Then
            SWhere += "'ENT',"
        End If
        If ChkRecep.Checked Then
            SWhere += "'REC',"
        End If

        'TIPOS de ORDENES DE SERVICIO
        If chkTosComb.Checked Then
            STOS += "1,"
        End If
        If chkTosLav.Checked Then
            STOS += "4,"
        End If
        If chkTosLlan.Checked Then
            STOS += "3,"
        End If
        If chkTosTaller.Checked Then
            STOS += "2,"
        End If

        'Otros Filtros 
        If cmbIdEmpresa.SelectedValue > 0 Then
            filtroEmpresa += " and cabos.IdEmpresa = " & cmbIdEmpresa.SelectedValue
        End If
        If txtUnidad.Text <> "" Then
            filtroUnidad += " and cabos.idUnidadTrans = '" & txtUnidad.Text & "'"
        End If
        If txtPadre.Text <> "" Then
            filtroFolio += " and cabos.idPadreOrdSer = '" & txtPadre.Text & "'"
        End If
        If txtOrden.Text <> "" Then
            filtroOrdenServ += " and cabos.idOrdenSer = '" & txtOrden.Text & "'"
        End If
        filtroExterno += " and isnull(cabos.externo,0) = " & IIf(chkEnvTallerExt.Checked, 1, 0)


        StrSql = "SELECT cabos.idPadreOrdSer AS FOLIO, " &
        "cabos.idOrdenSer as [No. Servicio], " &
        "tos.NomTipOrdServ as Tipo, " &
        "cabos.idUnidadTrans as Unidad, " &
        "emp.RazonSocial as Empresa, " &
        "cabos.Estatus,  " &
        "ISNULL((PerEnt.Nombre + ' ' + PerEnt.ApellidoPat + ' ' + PerEnt.ApellidoMat),'') AS Entrego, " &
        "ISNULL((PerRec.sNomUser),'') AS Recibio, " &
        "ISNULL((PerAut.Nombre + ' ' + PerAut.ApellidoPat + ' ' + PerAut.ApellidoMat),'') AS Autorizo, " &
        "'' AS TiempoTrasncurrido, " &
        "cabos.fechaCaptura as Captura, " &
        "cabos.FechaRecepcion as Recepcion, " &
        "cabos.FechaAsignado as Asignado, " &
        "cabos.FechaDiagnostico as Diagnostico, " &
        "cabos.FechaTerminado as Terminado, " &
        "cabos.FechaEntregado as Entregado, " &
        "cabos.fechaCancela as Cancelado, " &
        "cabos.externo as externo " &
        "FROM dbo.CabOrdenServicio cabos " &
        "INNER JOIN dbo.CatEmpresas Emp ON cabos.IdEmpresa = emp.idEmpresa " &
        "left JOIN dbo.CatPersonal PerEnt ON cabos.idEmpleadoEntrega = PerEnt.idPersonal " &
        "left JOIN dbo.Usuarios PerRec ON cabos.UsuarioRecepcion = PerRec.sUserId " &
        "left JOIN dbo.CatPersonal PerAut ON cabos.idEmpleadoAutoriza = PerAut.idPersonal " &
        "INNER JOIN dbo.CatTipoOrdServ TOS ON TOS.idTipOrdServ = cabos.idTipOrdServ "

        sStatusFiltro = SWhere
        TiempoUsado.StartNew()
        If SWhere.Trim <> "" Then
            If ChkHoy.Checked Then
                sStatusFiltro += " y SOLO FECHA de Hoy: " & Now.ToShortDateString
                SWhere = " WHERE cabos.Estatus IN(" & Mid(SWhere, 1, SWhere.Length - 1) & ") AND CAST(cabos.fechaCaptura AS DATE) = '" & FormatFecHora(Now.Date, True, False) & "'"
                'Filtro x tipo Orden
                SWhere = SWhere + " and cabos.idTipoOrden in (" & Mid(STOS, 1, STOS.Length - 1) & ")"
                SWhere = SWhere + filtroEmpresa
                SWhere = SWhere + filtroUnidad
                SWhere = SWhere + filtroFolio
                SWhere = SWhere + filtroOrdenServ
                SWhere = SWhere + filtroExterno

            Else
                SWhere = " WHERE cabos.Estatus IN(" & Mid(SWhere, 1, SWhere.Length - 1) & ") "
                'Filtro x tipo Orden
                SWhere = SWhere + " and cabos.idTipoOrden in (" & Mid(STOS, 1, STOS.Length - 1) & ")"
                SWhere = SWhere + filtroEmpresa
                SWhere = SWhere + filtroUnidad
                SWhere = SWhere + filtroFolio
                SWhere = SWhere + filtroOrdenServ
                SWhere = SWhere + filtroExterno

            End If
        ElseIf ChkHoy.Checked Then
            sStatusFiltro += " y SOLO FECHA de Hoy: " & Now.ToShortDateString
            SWhere = " WHERE CAST(cabos.fechaCaptura AS DATE) = '" & FormatFecHora(Now.Date, True, False) & "'"
        End If
        If ChkTmpReal.Checked = False Then
            SWhere += "AND CAST(cabos.fechaCaptura AS Date) = '" & FormatFecHora(Cale1.Value, True, False) & "'"
            sStatusFiltro += " y Fecha Captura = " & Cale1.Value.ToString("dd/MMMM/yyyy")
        End If
        Dim dataSource As New BindingSource(BD.ExecuteReturn(StrSql & SWhere & "ORDER BY cabos.fechaCaptura DESC"), Nothing)
        grdMonitor.DataSource = dataSource
        'Grid1.DataSource = BD.ExecuteReturn(sSqlServ & SWhere & "ORDER BY Serv.FechaSolicita DESC")
        Status(grdMonitor.Rows.Count & " Servicios Con el Filtro: " & sStatusFiltro & TiempoUsado.GetTiempo, Me)
        With grdMonitor
            .Columns("No. Servicio").Width = 50
            .Columns("FOLIO").Width = 50
            .Columns("Tipo").Width = 100
            .Columns("Unidad").Width = 80
            .Columns("Empresa").Width = 100
            .Columns("Estatus").Width = 50
            .Columns("Entrego").Width = 100
            .Columns("Recibio").Width = 100
            .Columns("Autorizo").Width = 100
            .Columns("TiempoTrasncurrido").Width = 180
            .Columns("Captura").Width = 120
            .Columns("Recepcion").Width = 120
            .Columns("Asignado").Width = 120
            .Columns("Diagnostico").Visible = False
            .Columns("Terminado").Width = 120
            .Columns("Entregado").Width = 120
            .Columns("Cancelado").Width = 120
            .Columns("externo").Width = 50

        End With

        'ORDENES DE TRABAJO
        If grdMonitor.Rows.Count > 0 Then
            StrSql = "SELECT ot.idOrdenSer AS NoServicio, " &
            "ot.idOrdenTrabajo AS [No. Trabajo], " &
            "ISNULL(ot.NotaRecepcionA,'') AS Descripcion, " &
            "ISNULL(div.NomDivision,'') AS Division, " &
            "ISNULL(CASE WHEN PerRes.Nombre <> '' THEN PerRes.Nombre + ' ' + PerRes.ApellidoPat + ' ' + PerRes.ApellidoMat ELSE PerRes.NombreCompleto END,'') AS Responsable, " &
            "ISNULL(CASE WHEN PerAyu1.Nombre <> '' THEN PerAyu1.Nombre + ' ' + PerAyu1.ApellidoPat + ' ' + PerAyu1.ApellidoMat ELSE PerAyu1.NombreCompleto END,'') AS Ayudante1, " &
            "ISNULL(CASE WHEN PerAyu2.Nombre <> '' THEN PerAyu2.Nombre + ' ' + PerAyu2.ApellidoPat + ' ' + PerAyu2.ApellidoMat ELSE PerAyu2.NombreCompleto END,'') AS Ayudante2, " &
            "ISNULL(det.Estatus,'') AS Estatus, " &
            "det.FechaAsig AS Asignado, " &
            "det.FechaDiag AS Diagnostico , " &
            "det.FechaTerminado AS Terminado, " &
            "ISNULL(det.DuracionActHr,0) AS DuracionHR, " &
            "'' AS TiempoTrasncurrido " &
            "FROM dbo.DetRecepcionOS ot " &
            "left JOIN dbo.DetOrdenServicio det ON ot.idOrdenTrabajo = det.idOrdenTrabajo " &
            "left JOIN dbo.CatPersonal PerRes ON ot.idPersonalResp = PerRes.idPersonal " &
            "left JOIN dbo.CatPersonal PerAyu1 ON ot.idPersonalAyu1 = PerAyu1.idPersonal " &
            "left JOIN dbo.CatPersonal PerAyu2 ON ot.idPersonalAyu2 = PerAyu2.idPersonal " &
            "LEFT JOIN dbo.CatDivision div ON ot.idDivision = div.idDivision"



            '            "ISNULL(CAST(ot. idPersonalResp AS VARCHAR(10)) + ' - ' + CASE WHEN PerRes.Nombre <> '' THEN PerRes.Nombre + ' ' + PerRes.ApellidoPat + ' ' + PerRes.ApellidoMat ELSE PerRes.NombreCompleto END,'') AS Responsable, " & _
            '"ISNULL(CAST(ot. idPersonalAyu1 AS VARCHAR(10)) + ' - ' + CASE WHEN PerAyu1.Nombre <> '' THEN PerAyu1.Nombre + ' ' + PerAyu1.ApellidoPat + ' ' + PerAyu1.ApellidoMat ELSE PerAyu1.NombreCompleto END,'') AS Ayudante1, " & _
            '"ISNULL(CAST(ot. idPersonalAyu2 AS VARCHAR(10)) + ' - ' + CASE WHEN PerAyu2.Nombre <> '' THEN PerAyu2.Nombre + ' ' + PerAyu2.ApellidoPat + ' ' + PerAyu2.ApellidoMat ELSE PerAyu2.NombreCompleto END,'') AS Ayudante2, " & _

            'Dim dataSource2 As New BindingSource(BD.ExecuteReturn(StrSql), Nothing)
            'MyTablaDet = dataSource2

            MyTablaDet.Rows.Clear()
            MyTablaDet = BD.ExecuteReturn(StrSql)

            grdMonitorDet.DataSource = MyTablaDet

            With grdMonitorDet
                .Columns("NoServicio").Width = 100
                .Columns("No. Trabajo").Width = 100
                .Columns("Descripcion").Width = 250
                .Columns("Division").Width = 150
                .Columns("Responsable").Width = 200
                .Columns("Ayudante1").Width = 150
                .Columns("Ayudante2").Width = 150
                .Columns("Estatus").Width = 50
                .Columns("Asignado").Width = 120
                .Columns("Diagnostico").Visible = False
                .Columns("Terminado").Width = 120
                .Columns("DuracionHR").Width = 80
                .Columns("TiempoTrasncurrido").Width = 180

            End With

            IdOrdenServ = grdMonitor.Item("No. Servicio", 0).Value
            CargaOrdenesTrab(IdOrdenServ)
        Else
            MyTablaDet.Rows.Clear()
        End If

    End Sub
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Static TmpActu As Integer = 0
        LTiempo.Text = _TiempoActu - TmpActu
        If TmpActu >= _TiempoActu Then
            TmpActu = 0
            Timer1.Enabled = False
            Dim RIndx As Integer, CIndx As Integer
            Try
                RIndx = grdMonitor.CurrentCell.RowIndex
                CIndx = grdMonitor.CurrentCell.ColumnIndex
            Catch ex As Exception
                RIndx = -1
                CIndx = -1
            End Try
            CargaGrid()
            If RIndx > 0 And CIndx > 0 And RIndx < grdMonitor.RowCount Then
                Me.grdMonitor.CurrentCell = Me.grdMonitor.Rows(RIndx).Cells(CIndx)
            End If
            Timer1.Enabled = True
        Else
            TmpActu += 1
        End If
        'ORDENES DE SERVICIO
        For i As Integer = 0 To grdMonitor.Rows.Count - 1
            If grdMonitor.Rows(i).Cells("Estatus").Value = "PRE" Then
                grdMonitor.Rows(i).Cells("TiempoTrasncurrido").Value = IntervaloHoras(CDate(grdMonitor.Rows(i).Cells("Captura").Value),
                                                                                Now)
                'ElseIf grdMonitor.Rows(i).Cells("Estatus").Value = "DIAG" Then
                '    grdMonitor.Rows(i).Cells("TiempoTrasncurrido").Value = IntervaloHoras(CDate(grdMonitor.Rows(i).Cells("Diagnostico").Value),
                '                                                                    Now)
            ElseIf grdMonitor.Rows(i).Cells("Estatus").Value = "REC" Then
                grdMonitor.Rows(i).Cells("TiempoTrasncurrido").Value = IntervaloHoras(CDate(grdMonitor.Rows(i).Cells("Recepcion").Value),
                                                                                Now)
            ElseIf grdMonitor.Rows(i).Cells("Estatus").Value = "ENT" Then
                grdMonitor.Rows(i).Cells("TiempoTrasncurrido").Value = IntervaloHoras(CDate(grdMonitor.Rows(i).Cells("Entregado").Value),
                                                                            Now)

            ElseIf grdMonitor.Rows(i).Cells("Estatus").Value = "ASIG" Then
                grdMonitor.Rows(i).Cells("TiempoTrasncurrido").Value = IntervaloHoras(CDate(grdMonitor.Rows(i).Cells("Asignado").Value),
                                                                                Now)
            ElseIf grdMonitor.Rows(i).Cells("Estatus").Value = "TER" Then
                Try
                    grdMonitor.Rows(i).Cells("TiempoTrasncurrido").Value = IntervaloHoras(CDate(grdMonitor.Rows(i).Cells("Asignado").Value),
                                                                                CDate(grdMonitor.Rows(i).Cells("Terminado").Value))
                Catch ex As Exception
                End Try
            End If
        Next

        'ORDENES DE TRABAJO
        For i As Integer = 0 To grdMonitorDet.Rows.Count - 1
            'If grdMonitorDet.Rows(i).Cells("Estatus").Value = "DIAG" Then
            '    grdMonitorDet.Rows(i).Cells("TiempoTrasncurrido").Value = IntervaloHoras(CDate(grdMonitorDet.Rows(i).Cells("Diagnostico").Value),
            '                                                                    Now)
            If grdMonitorDet.Rows(i).Cells("Estatus").Value = "ASIG" Then
                grdMonitorDet.Rows(i).Cells("TiempoTrasncurrido").Value = IntervaloHoras(CDate(grdMonitorDet.Rows(i).Cells("Asignado").Value),
                                                                                Now)
            ElseIf grdMonitorDet.Rows(i).Cells("Estatus").Value = "TER" Then
                Try
                    grdMonitorDet.Rows(i).Cells("TiempoTrasncurrido").Value = IntervaloHoras(CDate(grdMonitorDet.Rows(i).Cells("Asignado").Value),
                                                                                CDate(grdMonitorDet.Rows(i).Cells("Terminado").Value))
                Catch ex As Exception
                End Try
            End If
        Next
    End Sub
    Private Sub FMonitorServicios_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        FAct = True
    End Sub

    Private Sub FMonitorServicios_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Timer1.Enabled = False
    End Sub

    Private Sub FMonitorServicios_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = 27 Then
            Me.Close()
            e.Handled = True
        End If
    End Sub

    Private Sub grdMonitor_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles grdMonitor.CellContentClick

    End Sub

    Private Sub grdMonitor_CellFormatting(sender As Object, e As DataGridViewCellFormattingEventArgs) Handles grdMonitor.CellFormatting
        With grdMonitor.Rows(e.RowIndex).Cells(e.ColumnIndex)
            If IsDate(.Value) AndAlso CDate(.Value).ToString("yyyy/MM/dd") <> "0001/01/01" Then
                If CDate(.Value).Date = Now.Date Then
                    .ToolTipText = "HOY - " & FormatDateTime(.Value, DateFormat.LongDate)
                ElseIf CDate(.Value).Date = DateAdd(DateInterval.Day, -1, Now).Date Then
                    .ToolTipText = "AYER - " & FormatDateTime(.Value, DateFormat.LongDate)
                ElseIf CDate(.Value).Date = DateAdd(DateInterval.Day, 1, Now).Date Then
                    .ToolTipText = "MA�ANA - " & FormatDateTime(.Value, DateFormat.LongDate)
                Else
                    .ToolTipText = FormatDateTime(.Value, DateFormat.LongDate) & vbNewLine &
                        IntervaloTiempoCad(.Value)
                End If
            Else
                .ToolTipText = .Value & ""
            End If
        End With
    End Sub



    Private Sub TSActualizar_Click(sender As Object, e As EventArgs) Handles TSActualizar.Click
        CargaGrid()
    End Sub

    Private Sub grdMonitor_Click(sender As Object, e As EventArgs) Handles grdMonitor.Click
        IdOrdenServ = grdMonitor.Item("No. Servicio", grdMonitor.CurrentCell.RowIndex).Value
        CargaOrdenesTrab(IdOrdenServ)
    End Sub

    Private Sub grdMonitor_RowPostPaint(sender As Object, e As DataGridViewRowPostPaintEventArgs) Handles grdMonitor.RowPostPaint
        With grdMonitor.Rows(e.RowIndex)
            .DefaultCellStyle.ForeColor = Color.Black
            If grdMonitor.Columns.Contains("Estatus") AndAlso .Cells("Estatus").Value = "PRE" Then
                .DefaultCellStyle.BackColor = _ColorCap
            ElseIf grdMonitor.Columns.Contains("Estatus") AndAlso .Cells("Estatus").Value = "ASIG" Then
                .DefaultCellStyle.BackColor = _ColorAsig
            ElseIf grdMonitor.Columns.Contains("Estatus") AndAlso .Cells("Estatus").Value = "TER" Then
                .DefaultCellStyle.BackColor = _ColorTer
            ElseIf grdMonitor.Columns.Contains("Estatus") AndAlso .Cells("Estatus").Value = "CAN" Then
                .DefaultCellStyle.BackColor = _ColorCan
                'ElseIf grdMonitor.Columns.Contains("Estatus") AndAlso .Cells("Estatus").Value = "DIAG" Then
                '    .DefaultCellStyle.BackColor = _ColorDiag
            ElseIf grdMonitor.Columns.Contains("Estatus") AndAlso .Cells("Estatus").Value = "REC" Then
                .DefaultCellStyle.BackColor = _ColorRec
            ElseIf grdMonitor.Columns.Contains("Estatus") AndAlso .Cells("Estatus").Value = "ENT" Then
                .DefaultCellStyle.BackColor = _ColorEnt
            End If
            If .Cells("externo").Value Then
                .DefaultCellStyle.BackColor = _ColorExterno
            End If
        End With
    End Sub

    'Private Sub ChkCap_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ChkCap.CheckedChanged,
    'ChkAsig.CheckedChanged, ChkTer.CheckedChanged, ChkCan.CheckedChanged, ChkDiag.CheckedChanged, ChkEnt.CheckedChanged,
    'ChkRecep.CheckedChanged
    Private Sub ChkCap_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ChkCap.CheckedChanged,
    ChkAsig.CheckedChanged, ChkTer.CheckedChanged, ChkCan.CheckedChanged, ChkEnt.CheckedChanged,
    ChkRecep.CheckedChanged


        If FAct = False Then Exit Sub
        If TryCast(sender, CheckBox).Checked = False Then
            Dim Bnd As Boolean = False
            For Each Cnt As Control In GroupBox1.Controls
                If TypeOf (Cnt) Is CheckBox Then
                    If TryCast(Cnt, CheckBox).Checked Then
                        Bnd = True
                        Exit For
                    End If
                End If
            Next
            If Bnd = False Then
                TryCast(sender, CheckBox).Checked = True
                MessageBox.Show("Se requiere que almenos un Elemento este seleccionado", "Se requiere seleccionar almenos UNO!!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
        End If
    End Sub
    Private Sub ChkTmpReal_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ChkTmpReal.CheckedChanged
        If ChkTmpReal.Checked Then
            Timer1.Enabled = True
            Cale1.Enabled = False
            'btnBuscar.Enabled = False
            LTiempo.ForeColor = Color.Blue
            ChkHoy.Enabled = True
        Else
            Timer1.Enabled = False
            Cale1.Enabled = True
            'btnBuscar.Enabled = True
            LTiempo.ForeColor = Color.Red
            ChkHoy.Checked = False
            ChkHoy.Enabled = False
        End If
    End Sub


    Private Sub btnBuscar_Click(sender As Object, e As EventArgs)
        CargaGrid()
    End Sub

    Private Sub Cale1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cale1.ValueChanged
        CargaGrid()
    End Sub

    Private Sub TSFiltroAvanzado_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSFiltroAvanzado.Click
        If GroupBox2.Enabled Then
            GroupBox2.Enabled = False
            TSActualizar.Enabled = False
            ChkTmpReal.Checked = False

            For i As Integer = 0 To grdMonitor.Columns.Count - 1
                grdMonitor.Columns(i).HeaderCell = New CellHeaderGrid(grdMonitor.Columns(i).HeaderCell)
                TryCast(grdMonitor.Columns(i).HeaderCell, CellHeaderGrid).TipoDato = CellHeaderGrid.EnTipoDato.Automatico
                TryCast(grdMonitor.Columns(i).HeaderCell, CellHeaderGrid).MuestraPopUp = True
            Next i
        Else
            GroupBox2.Enabled = True
            TSActualizar.Enabled = True
            ChkTmpReal.Checked = True
            For i As Integer = 0 To grdMonitor.Columns.Count - 1
                TryCast(grdMonitor.Columns(i).HeaderCell, CellHeaderGrid).MuestraPopUp = False
            Next i
        End If

    End Sub

    Private Sub ToolStripButton1_Click(sender As Object, e As EventArgs) Handles ToolStripButton1.Click
        Me.Close()
    End Sub

    Private Sub Context1_Opening(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles Context1.Opening
        If grdMonitor.CurrentRow.Index < 0 Then
            e.Cancel = True
            Exit Sub
        End If

        If grdMonitorDet.CurrentRow.Index < 0 Then
            e.Cancel = True
            Exit Sub
        End If

        MReimp2.Visible = False
        MReimprimir.Visible = False
        ToolStripSeparator2.Visible = False
        With grdMonitor.Rows(grdMonitor.CurrentRow.Index)
            If .Cells("Estatus").Value & "" = "PRE" Then
                MRecepServ.Visible = True
                MAsigServ.Visible = False
                MEntregServ.Visible = False
                'MReimprimir.Visible = False
            ElseIf .Cells("Estatus").Value & "" = "REC" Then
                MRecepServ.Visible = False
                MAsigServ.Visible = True
                MEntregServ.Visible = False
                'MReimprimir.Visible = False
            ElseIf .Cells("Estatus").Value & "" = "ASIG" Then
                MRecepServ.Visible = False
                MAsigServ.Visible = False
                MEntregServ.Visible = False
                'MReimprimir.Visible = False
            ElseIf .Cells("Estatus").Value & "" = "ENT" Then
                MRecepServ.Visible = False
                MAsigServ.Visible = False
                MEntregServ.Visible = False
                'MReimprimir.Visible = False
            Else
                MReimprimir.Visible = False
            End If
        End With


    End Sub



    Private Sub MRecepServ_Click(sender As Object, e As EventArgs) Handles MRecepServ.Click
        'MENU_MOVIMIENTOS_RECEPCION()

        FMenu.OpenMainWindow("MENU_MOVIMIENTOS_RECEPCION", True, grdMonitor.Rows(grdMonitor.CurrentRow.Index).Cells("No. Servicio").Value)

    End Sub

    Private Sub MAsigServ_Click(sender As Object, e As EventArgs) Handles MAsigServ.Click
        FMenu.OpenMainWindow("MENU_MOVIMIENTOS_DIAGNOSTICO", True, grdMonitor.Rows(grdMonitor.CurrentRow.Index).Cells("No. Servicio").Value)
        '
    End Sub

    Private Sub CargaOrdenesTrab(ByVal vidOrdenSer As Integer)
        Try
            MyTablaDet.DefaultView.RowFilter = Nothing
            MyTablaDet.DefaultView.RowFilter = "NoServicio = " & vidOrdenSer
            If MyTablaDet.DefaultView.Count > 0 Then

            End If
            grdMonitorDet.PerformLayout()


        Catch ex As Exception

        End Try
    End Sub

    Private Sub Context2_Opening(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles Context2.Opening
        With grdMonitorDet.Rows(grdMonitorDet.CurrentRow.Index)
            MTrabRes.Visible = True
            If .Cells("Estatus").Value & "" = "PRE" Then
                MTrabDiag.Visible = False
                MTrabFin.Visible = False
                MTrabCan.Visible = True
            ElseIf .Cells("Estatus").Value & "" = "REC" Then
                MTrabDiag.Visible = False
                MTrabFin.Visible = False
                MTrabCan.Visible = True
            ElseIf .Cells("Estatus").Value & "" = "ASIG" Then
                MTrabDiag.Visible = True
                MTrabFin.Visible = False
                MTrabCan.Visible = True
            ElseIf .Cells("Estatus").Value & "" = "DIAG" Then
                MTrabDiag.Visible = True
                MTrabFin.Visible = True
                MTrabCan.Visible = True
            ElseIf .Cells("Estatus").Value & "" = "TER" Then
                MTrabDiag.Visible = False
                MTrabFin.Visible = False
                MTrabCan.Visible = False
                MTrabSep1.Visible = False
            Else
                MTrabDiag.Visible = False
                MTrabFin.Visible = False
                MTrabCan.Visible = False
                MTrabSep1.Visible = False
            End If
        End With
    End Sub

    Private Sub grdMonitorDet_RowPostPaint(sender As Object, e As DataGridViewRowPostPaintEventArgs) Handles grdMonitorDet.RowPostPaint
        With grdMonitorDet.Rows(e.RowIndex)
            .DefaultCellStyle.ForeColor = Color.Black
            If grdMonitorDet.Columns.Contains("Estatus") AndAlso .Cells("Estatus").Value = "PRE" Then
                .DefaultCellStyle.BackColor = _ColorCap
            ElseIf grdMonitorDet.Columns.Contains("Estatus") AndAlso .Cells("Estatus").Value = "ASIG" Then
                .DefaultCellStyle.BackColor = _ColorAsig
            ElseIf grdMonitorDet.Columns.Contains("Estatus") AndAlso .Cells("Estatus").Value = "TER" Then
                .DefaultCellStyle.BackColor = _ColorTer
            ElseIf grdMonitorDet.Columns.Contains("Estatus") AndAlso .Cells("Estatus").Value = "CAN" Then
                .DefaultCellStyle.BackColor = _ColorCan
                'ElseIf grdMonitorDet.Columns.Contains("Estatus") AndAlso .Cells("Estatus").Value = "DIAG" Then
                '    .DefaultCellStyle.BackColor = _ColorDiag
            ElseIf grdMonitorDet.Columns.Contains("Estatus") AndAlso .Cells("Estatus").Value = "REC" Then
                .DefaultCellStyle.BackColor = _ColorRec
            ElseIf grdMonitorDet.Columns.Contains("Estatus") AndAlso .Cells("Estatus").Value = "ENT" Then
                .DefaultCellStyle.BackColor = _ColorEnt
            End If
        End With
    End Sub

    Private Sub MTrabFin_Click(sender As Object, e As EventArgs) Handles MTrabFin.Click
        If grdMonitorDet.CurrentRow.Index < 0 Then Exit Sub
        Dim FAsg As New FEstatusTaller(0, grdMonitorDet.Rows(grdMonitorDet.CurrentRow.Index).Cells("No. Trabajo").Value, TipoOpcionEstatus.Terminar)
        FAsg.MdiParent = Fletera.FMenu
        'FAsg.StartPosition = FormStartPosition.CenterScreen
        FAsg.WindowState = FormWindowState.Normal
        FAsg.Show()
        CargaGrid()
    End Sub

    Private Sub MEntregServ_Click(sender As Object, e As EventArgs) Handles MEntregServ.Click
        If grdMonitor.CurrentRow.Index < 0 Then Exit Sub
        Dim FAsg As New FEstatusTaller(grdMonitor.Rows(grdMonitorDet.CurrentRow.Index).Cells("No. Servicio").Value, 0, TipoOpcionEstatus.Terminar)
        FAsg.MdiParent = Fletera.FMenu
        'FAsg.StartPosition = FormStartPosition.CenterScreen
        FAsg.WindowState = FormWindowState.Normal
        FAsg.Show()
        CargaGrid()
    End Sub

    Private Sub AutoaCompletar(ByVal Empresa As Integer)
        'AutoCompletarEmpresas(txtEmpresa, False)
        AutoCompletarEmpresas(cmbIdEmpresa, True)
        AutoCompletarUnidadTransp(txtUnidad, 0)
        'AutoCompletarFolios(txtPadre, " INNER JOIN dbo.CatUnidadTrans T ON mos.idTractor = t.idUnidadTrans WHERE t.idEmpresa = " & Empresa)
        AutoCompletarFolios(txtPadre)
        AutoCompletarOrdenes(txtOrden)

    End Sub
    'Public Function AutoCompletarEmpresas(ByVal Control As TextBox, ByVal bIncluyeTodos As Boolean) As AutoCompleteStringCollection

    '    Dim Coleccion As New AutoCompleteStringCollection
    '    Dim dt As DataTable
    '    Empresa = New EmpresaClass(0)
    '    dt = Empresa.TablaEmpresas(bIncluyeTodos)

    '    'Recorrer y cargar los items para el Autocompletado
    '    For Each row As DataRow In dt.Rows
    '        Coleccion.Add(Convert.ToString(row("RazonSocial")))
    '    Next
    '    'Ajustamos el control TextBox o ComboBox para recibir los datos de la siguiente manera.
    '    With Control
    '        .AutoCompleteCustomSource = Coleccion
    '        .AutoCompleteMode = AutoCompleteMode.Suggest
    '        .AutoCompleteSource = AutoCompleteSource.CustomSource
    '    End With

    '    ' Aca le devuelvo los datos a recuperados de la Base
    '    Return Coleccion

    'End Function
    Public Function AutoCompletarEmpresas(ByVal Control As ComboBox, ByVal bIncluyeTodos As Boolean) As AutoCompleteStringCollection

        Dim Coleccion As New AutoCompleteStringCollection
        Dim dt As DataTable


        Empresa = New EmpresaClass(0)
        dt = Empresa.TablaEmpresas(bIncluyeTodos)

        'Recorrer y cargar los items para el Autocompletado
        For Each row As DataRow In dt.Rows
            Coleccion.Add(Convert.ToString(row("RazonSocial")))
        Next
        'Ajustamos el control TextBox o ComboBox para recibir los datos de la siguiente manera.
        With Control
            .DataSource = dt
            .DisplayMember = "RazonSocial"
            .ValueMember = "idEmpresa"


            .AutoCompleteCustomSource = Coleccion
            .AutoCompleteMode = AutoCompleteMode.Suggest
            .AutoCompleteSource = AutoCompleteSource.CustomSource
        End With

        ' Aca le devuelvo los datos a recuperados de la Base
        Return Coleccion

    End Function

    Public Function AutoCompletarFolios(ByVal Control As TextBox, Optional ByVal FiltroSinWhere As String = "") As AutoCompleteStringCollection
        Dim Coleccion As New AutoCompleteStringCollection

        PadreOS = New MasterOrdServClass(0)

        Dim dt As DataTable = PadreOS.tbPadreOS()

        For Each row As DataRow In dt.Rows
            Coleccion.Add(Convert.ToString(row("idPadreOrdSer")))
        Next
        'Ajustamos el control TextBox o ComboBox para recibir los datos de la siguiente manera.
        With Control

            .AutoCompleteCustomSource = Coleccion
            .AutoCompleteMode = AutoCompleteMode.Suggest
            .AutoCompleteSource = AutoCompleteSource.CustomSource
        End With

        ' Aca le devuelvo los datos a recuperados de la Base
        Return Coleccion
    End Function

    Public Function AutoCompletarOrdenes(ByVal Control As TextBox, Optional ByVal FiltroSinWhere As String = "") As AutoCompleteStringCollection
        Dim Coleccion As New AutoCompleteStringCollection

        ordenes = New OrdenServClass(0)
        Dim dt As DataTable = ordenes.tbOrdenesServ()

        For Each row As DataRow In dt.Rows
            Coleccion.Add(Convert.ToString(row("idOrdenSer")))
        Next
        'Ajustamos el control TextBox o ComboBox para recibir los datos de la siguiente manera.
        With Control

            .AutoCompleteCustomSource = Coleccion
            .AutoCompleteMode = AutoCompleteMode.Suggest
            .AutoCompleteSource = AutoCompleteSource.CustomSource
        End With

        ' Aca le devuelvo los datos a recuperados de la Base
        Return Coleccion
    End Function

    Public Function AutoCompletarUnidadTransp(ByVal Control As TextBox, ByVal idEmpresa As Integer,
    Optional ByVal FiltroSinWhere As String = "") As AutoCompleteStringCollection
        Dim Coleccion As New AutoCompleteStringCollection

        UniTrans = New UnidadesTranspClass(0)

        Dim dt As DataTable = UniTrans.TablaUnidadesTodas(idEmpresa, FiltroSinWhere)
        'Recorrer y cargar los items para el Autocompletado
        For Each row As DataRow In dt.Rows
            Coleccion.Add(Convert.ToString(row("idUnidadTrans")))
        Next
        'Ajustamos el control TextBox o ComboBox para recibir los datos de la siguiente manera.
        With Control

            .AutoCompleteCustomSource = Coleccion
            .AutoCompleteMode = AutoCompleteMode.Suggest
            .AutoCompleteSource = AutoCompleteSource.CustomSource
        End With

        ' Aca le devuelvo los datos a recuperados de la Base
        Return Coleccion
    End Function

    Private Sub PermisosFiltros(ByVal bTodasEmpresas As Boolean)
        If Not bTodasEmpresas Then
            Empresa = New EmpresaClass(_cveEmpresa)
            If Empresa.Existe Then
                cmbIdEmpresa.SelectedValue = _cveEmpresa
                cmbIdEmpresa.Enabled = False
            End If
            AutoCompletarFolios(txtPadre, " INNER JOIN dbo.CatUnidadTrans T ON mos.idTractor = t.idUnidadTrans WHERE t.idEmpresa = " & _cveEmpresa)
            AutoCompletarOrdenes(txtOrden, " where mos.IdEmpresa = " & _cveEmpresa)
            AutoCompletarUnidadTransp(txtUnidad, _cveEmpresa)
        End If
    End Sub

    Private Sub PermisosTiposOS(ByVal OpcTipoServ As TipoOrdenServicio)
        Select Case OpcTipoServ
            Case TipoOrdenServicio.COMBUSTIBLE
                chkTosComb.Enabled = False
                chkTosComb.Checked = True

                chkTosLav.Enabled = False
                chkTosLav.Checked = False

                chkTosLlan.Enabled = False
                chkTosLlan.Checked = False

                chkTosTaller.Enabled = False
                chkTosTaller.Checked = False
            Case TipoOrdenServicio.LAVADERO
                chkTosComb.Enabled = False
                chkTosComb.Checked = False

                chkTosLav.Enabled = False
                chkTosLav.Checked = True

                chkTosLlan.Enabled = False
                chkTosLlan.Checked = False

                chkTosTaller.Enabled = False
                chkTosTaller.Checked = False

            Case TipoOrdenServicio.LLANTAS
                chkTosComb.Enabled = False
                chkTosComb.Checked = False

                chkTosLav.Enabled = False
                chkTosLav.Checked = False

                chkTosLlan.Enabled = False
                chkTosLlan.Checked = True

                chkTosTaller.Enabled = False
                chkTosTaller.Checked = False

            Case TipoOrdenServicio.TALLER
                chkTosComb.Enabled = False
                chkTosComb.Checked = False

                chkTosLav.Enabled = False
                chkTosLav.Checked = False

                chkTosLlan.Enabled = False
                chkTosLlan.Checked = False

                chkTosTaller.Enabled = False
                chkTosTaller.Checked = True
            Case TipoOrdenServicio.TODAS
                chkTosComb.Enabled = True
                chkTosComb.Checked = False

                chkTosLav.Enabled = True
                chkTosLav.Checked = True

                chkTosLlan.Enabled = True
                chkTosLlan.Checked = True

                chkTosTaller.Enabled = True
                chkTosTaller.Checked = True
            Case Else

        End Select
    End Sub

    'Private Sub chkTosComb_CheckedChanged(sender As Object, e As EventArgs) Handles chkTosComb.CheckedChanged,
    'chkTosLav.CheckedChanged, chkTosLlan.CheckedChanged, chkTosTaller.CheckedChanged

    Private Sub chkTosComb_CheckedChanged(sender As Object, e As EventArgs) Handles chkTosComb.CheckedChanged,
    chkTosLav.CheckedChanged, chkTosLlan.CheckedChanged, chkTosTaller.CheckedChanged
        If FAct = False Then Exit Sub
        If TryCast(sender, CheckBox).Checked = False Then
            Dim Bnd As Boolean = False
            For Each Cnt As Control In gpoTOS.Controls
                If TypeOf (Cnt) Is CheckBox Then
                    If TryCast(Cnt, CheckBox).Checked Then
                        Bnd = True
                        Exit For
                    End If
                End If
            Next
            If Bnd = False Then
                TryCast(sender, CheckBox).Checked = True
                MessageBox.Show("Se requiere que almenos un Elemento este seleccionado", "Se requiere seleccionar almenos UNO!!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
        End If
    End Sub
End Class
