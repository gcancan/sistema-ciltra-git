﻿Imports LinqToExcel
Imports Microsoft.Office.Interop

Public Class fAbreExcelFacturacion
    Private _Columna1 As String
    Private _Columna2 As String
    Dim TablaBd As String = "ExcelFactura"
    Private _OpcionExcelFactura As OpcionExcelFactura
    'Dim _TablaListado As DataTable = New DataTable(TablaBd)
    Dim MyTablaCombo As DataTable = New DataTable(TablaBd)

    Dim wb As Excel.Workbook            ' Libro de trabajo
    Dim myDataColumn As DataColumn
    Dim myDataRow As DataRow
    Dim TablaHojas As DataTable = New DataTable(TablaBd)
    Dim TablaResultante As DataTable = New DataTable(TablaBd)
    Dim TablaResultante2 As DataTable = New DataTable(TablaBd)
    Public TablaFinal As DataTable = New DataTable(TablaBd)
    Dim vNomArchivo As String = ""
    Dim StrSql As String
    Dim BandTabla2 As Boolean = False
    Sub New(ByVal sqlCon As String, ByVal vColumna1 As String, ByVal vColumna2 As String, ByVal vOpcionExcelFactura As OpcionExcelFactura)

        _Columna1 = _Columna1
        _Columna2 = _Columna2
        _OpcionExcelFactura = vOpcionExcelFactura
        Inicio.CONSTR = sqlCon
        '_TablaListado = vTablaListado
        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub

    Private Sub btnOpenFile_Click(sender As Object, e As EventArgs) Handles btnOpenFile.Click

        Dim filtro As String = "Archivos Excel(*.XLS;*.XLSX;)|*.XLS;*.XLSX;|Todos (*.*)|*.* "

        openFileDialog1.Filter = filtro

        If openFileDialog1.ShowDialog() = DialogResult.Cancel Then
            Return
        End If

        vNomArchivo = openFileDialog1.FileName
        txtNomArchivo.Text = vNomArchivo

        CargaHojasExcelCAB(vNomArchivo)

        CargaColumnasLibro()
        'Dim excel As Object = New ExcelQueryFactory(vNomArchivo)



    End Sub

    Private Sub LlenaComboColListado()
        If _OpcionExcelFactura = OpcionExcelFactura.NoSAp Then
            StrSql = "SELECT 1 AS Clave, 'No.Remision' AS Descripcion " &
            " union " &
            "SELECT 2 AS Clave, 'No.SAP' AS Descripcion "
        ElseIf _OpcionExcelFactura = OpcionExcelFactura.Preliquidacion Then
            StrSql = "SELECT 1 AS Clave, 'No.Viaje' AS Descripcion " &
            " union " &
            "SELECT 2 AS Clave, 'Precio' AS Descripcion "
        End If

        MyTablaCombo = BD.ExecuteReturn(StrSql)
        If Not MyTablaCombo Is Nothing Then
            If MyTablaCombo.Rows.Count > 0 Then
                cmbColListado.DataSource = MyTablaCombo
                cmbColListado.ValueMember = "Clave"
                cmbColListado.DisplayMember = "Descripcion"

                'MyTablaCli = MyTablaCombo
            End If
        End If
    End Sub
    Private Sub fAbreExcelFacturacion_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CreaTablaHojas()
        CreaTablaResultante()
        LlenaComboColListado()
        InicializandoGrids()
        InicializaGrdListado()

        If _OpcionExcelFactura = OpcionExcelFactura.NoSAp Then
            Me.Text = "Importar Numeros SAP"
        ElseIf _OpcionExcelFactura = OpcionExcelFactura.Preliquidacion Then
            Me.Text = "Cargar Preliquidación"
        End If


    End Sub
    Private Sub CreaTablaResultante()
        Try

            myDataColumn = New DataColumn
            If _OpcionExcelFactura = OpcionExcelFactura.NoSAp Then
                myDataColumn.ColumnName = "NoRemision"
                myDataColumn.ReadOnly = False
                myDataColumn.Unique = False
                myDataColumn.DataType = System.Type.GetType("System.String")
            ElseIf _OpcionExcelFactura = OpcionExcelFactura.Preliquidacion Then
                myDataColumn.ColumnName = "NoViaje"
                myDataColumn.ReadOnly = False
                myDataColumn.Unique = False
                myDataColumn.DataType = System.Type.GetType("System.String")
            End If


            TablaResultante.Columns.Add(myDataColumn)

                myDataColumn = New DataColumn
                If _OpcionExcelFactura = OpcionExcelFactura.NoSAp Then
                    myDataColumn.DataType = System.Type.GetType("System.String")
                    myDataColumn.ColumnName = "NoSAP"
                    myDataColumn.ReadOnly = False
                    myDataColumn.Unique = False
                ElseIf _OpcionExcelFactura = OpcionExcelFactura.Preliquidacion Then
                    myDataColumn.DataType = System.Type.GetType("System.String")
                    myDataColumn.ColumnName = "Precio"
                    myDataColumn.ReadOnly = False
                    myDataColumn.Unique = False
                End If
                TablaResultante.Columns.Add(myDataColumn)




        Catch ex As Exception

        End Try
    End Sub
    Private Sub CreaTablaHojas()
        Try
            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.Int64")
            myDataColumn.ColumnName = "Indice"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            TablaHojas.Columns.Add(myDataColumn)


            myDataColumn = New DataColumn
            myDataColumn.DataType = System.Type.GetType("System.String")
            myDataColumn.ColumnName = "Nombre"
            myDataColumn.ReadOnly = False
            myDataColumn.Unique = False
            TablaHojas.Columns.Add(myDataColumn)
        Catch ex As Exception

        End Try
    End Sub

    'Private Sub Excel_test1(sPath As String)
    '    Try


    '        'Excel Application 1st way
    '        Dim oExcel As Excel.Application
    '        oExcel = New Excel.Application
    '        oExcel.Workbooks.Open(sPath)


    '        'Excel Application Alternative
    '        'Dim oExcel As Excel.Application
    '        'oExcel = CreateObject("Excel.Application")
    '        'oExcel.Workbooks.Open(sPath)

    '        'Excel WorkBook
    '        Dim wb As Excel.Workbook
    '        wb = oExcel.Workbooks(1)


    '        'Excel Sheet (for the first sheet)
    '        Dim ws As Excel.Worksheet
    '        ws = wb.Worksheets(1)


    '        'Excel Range
    '        Dim oRange As Excel.Range
    '        Dim mRow As Integer = CInt(ws.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell).Row)
    '        Dim mColumn As Integer = CInt(ws.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell).Column)


    '        For index1 = 1 To mRow
    '            For index3 = 0 To mColumn - 1 Step 1

    '                'MsgBox(CChar(ChrW(65 + index3))) '65=>A
    '                oRange = ws.Range((CChar(ChrW(65 + index3)) & index1))
    '                If MsgBox(oRange.Value & vbCrLf & "Display next value? ", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
    '                    index3 = mColumn
    '                    index1 = mRow
    '                End If

    '            Next index3
    '        Next index1



    '        'Close Excel 
    '        oExcel.Quit()
    '        System.Runtime.InteropServices.Marshal.ReleaseComObject(oRange)
    '        System.Runtime.InteropServices.Marshal.ReleaseComObject(ws)
    '        System.Runtime.InteropServices.Marshal.ReleaseComObject(wb)
    '        System.Runtime.InteropServices.Marshal.ReleaseComObject(oExcel)
    '        oExcel = Nothing
    '    Catch ex As Exception


    '    End Try
    'End Sub
    Private Sub CargaHojasExcelCAB(ByVal sFileName As String)
        Try

            Dim xlApp As New Excel.Application ' Aplicación Excel

            FEspera.StartPosition = FormStartPosition.CenterScreen
            FEspera.Show("Cargando Hojas de Excel . . .")

            ' Abrimos el libro de trabajo.
            wb = xlApp.Workbooks.Open(Filename:=sFileName)
            ' Recorrermos la colección de hojas de cálculo
            Dim indice As Integer = 1
            TablaHojas.Clear()

            For Each sheet As Excel.Worksheet In wb.Worksheets
                myDataRow = TablaHojas.NewRow()
                myDataRow("Indice") = indice
                myDataRow("Nombre") = sheet.Name

                TablaHojas.Rows.Add(myDataRow)

                indice = indice + 1
            Next

            If Not TablaHojas Is Nothing Then
                If TablaHojas.Rows.Count > 0 Then
                    cmbHojasCAB.DataSource = TablaHojas
                    cmbHojasCAB.ValueMember = "Indice"
                    cmbHojasCAB.DisplayMember = "Nombre"
                End If
            End If

            'cmbHojasCAB.Items.Insert(indice, sheet.Name)
            cmbHojasCAB.SelectedIndex = 0


            ' Cerramos el libro de trabajo.
            wb.Close()
            wb = Nothing
            ' Cerramos Excel.
            xlApp.Quit()
            xlApp = Nothing
            FEspera.Close()
            cmbHojasCAB.SelectedIndex = 0
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub

    Private Sub CargaColumnasLibro()
        Dim xlApp As New Excel.Application ' Aplicación Excel
        ' Abrimos el libro de trabajo.
        Dim wb As Excel.Workbook
        FEspera.StartPosition = FormStartPosition.CenterScreen
        FEspera.Show("Cargando Columnas de libro . . ." & cmbHojasCAB.Text)


        wb = xlApp.Workbooks.Open(Filename:=vNomArchivo)
        wb = xlApp.Workbooks(1)

        Dim ws As Excel.Worksheet
        ws = wb.Worksheets(cmbHojasCAB.SelectedValue)

        Dim oRange As Excel.Range
        Dim mRow As Integer = CInt(ws.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell).Row)
        Dim mColumn As Integer = CInt(ws.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell).Column)

        For index1 = 1 To mRow
            For index3 = 0 To mColumn - 1 Step 1
                oRange = ws.Range((CChar(ChrW(65 + index3)) & index1))
                If oRange.Value <> Nothing Then
                    cmbColumnasCAB.Items.Add(oRange.Value)
                End If


                'index3 = mColumn
                'index1 = mRow
            Next index3
        Next index1
        'wb = xlApp.Workbooks.Open(Filename:=vNomArchivo)

        'Dim sheet As Excel.Worksheet = wb.Worksheets(cmbHojasCAB.SelectedValue)


        'cmbColumnasCAB.Items.Clear()
        'For Each Col As Excel.ListColumn In sheet.Columns
        '    cmbColumnasCAB.Items.Add(Col)
        'Next
        'cmbColumnasCAB.SelectedIndex = 0
        ' Cerramos el libro de trabajo.
        wb.Close()
        wb = Nothing
        ' Cerramos Excel.
        xlApp.Quit()
        xlApp = Nothing
        FEspera.Close()
        cmbColumnasCAB.SelectedIndex = 0
    End Sub
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click

        CargaColumnasLibro()

    End Sub

    Private Sub cmdRelacionaCAB_Click(sender As Object, e As EventArgs) Handles cmdRelacionaCAB.Click
        If RelacionaColumnasCAB(cmbColumnasCAB.Text, cmbColListado.Text) Then
            If (cmbColListado.Items.Count - 1) > cmbColListado.SelectedIndex Then
                cmbColListado.SelectedIndex = (cmbColListado.SelectedIndex) + 1
            End If

        End If
    End Sub

    Private Function RelacionaColumnasCAB(ByVal sColumnaExcel As String, ByVal sColumnaListado As String) As Boolean
        Dim Resultado As Boolean = False
        If _OpcionExcelFactura = OpcionExcelFactura.NoSAp Then
            If sColumnaListado = "No.Remision" Then
                grdRelaciones.Item("ColExcel", 0).Value = sColumnaExcel
                Resultado = True
            ElseIf sColumnaListado = "No.SAP" Then
                grdRelaciones.Item("ColExcel", 1).Value = sColumnaExcel
                Resultado = True
            End If
        ElseIf _OpcionExcelFactura = OpcionExcelFactura.Preliquidacion Then
            If sColumnaListado = "No.Viaje" Then
                grdRelaciones.Item("ColExcel", 0).Value = sColumnaExcel
                Resultado = True
            ElseIf sColumnaListado = "Precio" Then
                grdRelaciones.Item("ColExcel", 1).Value = sColumnaExcel
                Resultado = True
            End If
        End If

        Return Resultado

        'If sColumnaListado = "FOLIO" Then
        '    grdRelaciones.Item("ColExcel", 0).Value = sColumnaExcel
        'ElseIf sColumnaListado = "TIPOPOL" Then
        '    grdRelaciones.Item("ColExcel", 1).Value = sColumnaExcel
        'ElseIf sColumnaListado = "FECHA" Then
        '    grdRelaciones.Item("ColExcel", 2).Value = sColumnaExcel
        'ElseIf sColumnaListado = "CONCEPTO" Then
        '    grdRelaciones.Item("ColExcel", 3).Value = sColumnaExcel

        'End If
    End Function

    Private Sub InicializaGrdListado()
        If _OpcionExcelFactura = OpcionExcelFactura.NoSAp Then
            grdRelaciones.Item("ColExcel", 0).Value = ""
            grdRelaciones.Item("ColExcel", 1).Value = ""
        ElseIf _OpcionExcelFactura = OpcionExcelFactura.NoSAp Then
            grdRelaciones.Item("ColExcel", 0).Value = ""
            grdRelaciones.Item("ColExcel", 1).Value = ""
        End If
    End Sub

    Private Sub InicializaGrid(ByVal sColumnaExcel As String, ByVal sColumnaListado As String, ByVal indice As Integer)
        grdRelaciones.Rows.Add()
        grdRelaciones.Item("ColExcel", indice).Value = sColumnaExcel
        grdRelaciones.Item("colListado", indice).Value = sColumnaListado
        cmbColListado.SelectedIndex = 0
    End Sub

    Private Sub InicializandoGrids()

        If _OpcionExcelFactura = OpcionExcelFactura.NoSAp Then
            InicializaGrid("", "No.Remision", 0)
            InicializaGrid("", "No.SAP", 1)
        ElseIf _OpcionExcelFactura = OpcionExcelFactura.Preliquidacion Then
            InicializaGrid("", "No.Viaje", 0)
            InicializaGrid("", "Precio", 1)
        End If

        'InicializaGrid("", "CtaSup", 2)
        'InicializaGrid("", "CtaMayor", 3)
        'InicializaGrid("", "FechaRegistro", 4)


    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub LlenaGridResultante()
        Dim vNoViaje As String = ""
        Dim vNoSAp As String = ""

        Try
            TablaResultante.Clear()


            For i As Integer = 0 To grdCuentas.Rows.Count - 1
                If _OpcionExcelFactura = OpcionExcelFactura.NoSAp Then
                    If grdCuentas.Item("NoViaje", i).Value <> "" Then
                        myDataRow = TablaResultante.NewRow()
                        myDataRow("NoRemision") = grdCuentas.Item("NoViaje", i).Value
                        myDataRow("NoSAP") = grdCuentas.Item("NoSAP", i).Value
                        TablaResultante.Rows.Add(myDataRow)
                    End If
                ElseIf _OpcionExcelFactura = OpcionExcelFactura.Preliquidacion Then
                    If grdCuentas.Item("Precio", i).Value > 0 And (grdCuentas.Item("NoViaje", i).Value > 0 Or grdCuentas.Item("NoViaje", i).Value <> "") Then
                        myDataRow = TablaResultante.NewRow()
                        myDataRow("NoViaje") = grdCuentas.Item("NoViaje", i).Value
                        myDataRow("Precio") = grdCuentas.Item("Precio", i).Value
                        TablaResultante.Rows.Add(myDataRow)
                    End If

                End If

            Next

            'No Repetidos & no Ceros
            If chkNoRep.Checked Then
                If TablaResultante.Rows.Count > 0 Then
                    'ORDENAR
                    'TablaResultante.DefaultView.Sort = "NoViaje,NoSAP"
                    TablaResultante2 = TablaResultante.Copy()
                    TablaResultante2.Clear()
                    If _OpcionExcelFactura = OpcionExcelFactura.NoSAp Then
                        TablaResultante.DefaultView.Sort = "NoSAP"
                        For i = 0 To TablaResultante.DefaultView.Count - 1
                            If vNoSAp = "" And i = 0 Then
                                vNoSAp = TablaResultante.DefaultView.Item(i).Item("NoSAP")
                            End If
                            If vNoSAp <> TablaResultante.DefaultView.Item(i).Item("NoSAP").ToString() Then
                                'vNoViaje = TablaResultante.DefaultView.Item(i).Item("NoViaje")
                                vNoSAp = TablaResultante.DefaultView.Item(i).Item("NoSAP")

                                myDataRow = TablaResultante2.NewRow()
                                myDataRow("NoRemision") = TablaResultante.DefaultView.Item(i).Item("NoRemision").ToString()
                                myDataRow("NoSAP") = TablaResultante.DefaultView.Item(i).Item("NoSAP").ToString()
                                TablaResultante2.Rows.Add(myDataRow)

                            End If
                        Next
                    ElseIf _OpcionExcelFactura = OpcionExcelFactura.Preliquidacion Then
                        TablaResultante.DefaultView.Sort = "NoViaje"
                        For i = 0 To TablaResultante.DefaultView.Count - 1

                            If vNoViaje = ""  And i = 0 Then
                                vNoViaje = TablaResultante.DefaultView.Item(i).Item("NoViaje")
                            End If
                            If vNoViaje <> TablaResultante.DefaultView.Item(i).Item("NoViaje").ToString() Then
                                vNoViaje = TablaResultante.DefaultView.Item(i).Item("NoViaje")

                                myDataRow = TablaResultante2.NewRow()
                                myDataRow("NoViaje") = TablaResultante.DefaultView.Item(i).Item("NoViaje").ToString()
                                myDataRow("precio") = TablaResultante.DefaultView.Item(i).Item("precio").ToString()
                                TablaResultante2.Rows.Add(myDataRow)
                            End If
                        Next
                    End If

                    grdResultante.DataSource = TablaResultante2
                    grdResultante.PerformLayout()
                    BandTabla2 = True
                End If
            Else
                grdResultante.DataSource = TablaResultante
                grdResultante.PerformLayout()
                BandTabla2 = False
            End If
            lblNoRegRes.Text = "No. de Registros " & grdResultante.Rows.Count
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try



    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs)
        LlenaGridResultante()
    End Sub

    Private Sub btnMnuGenerar_Click(sender As Object, e As EventArgs) Handles btnMnuGenerar.Click
        Try
            'Dim Excel = New ExcelQueryFactory(vNomArchivo)
            'Dim resultado = Nothing
            'grdCuentas.DataSource = Nothing
            'If _OpcionExcelFactura = OpcionExcelFactura.NoSAp Then
            '    resultado = (From row In Excel.Worksheet(cmbHojasCAB.Text) Let item = New SAP("", "") With {
            '                        .NoSAP = row(grdRelaciones.Item("ColExcel", 1).Value.ToString()).Cast(Of String),
            '                        .NoViaje = row(grdRelaciones.Item("ColExcel", 0).Value.ToString()).Cast(Of String)}
            '                 Select item).ToList()

            '    '(From a In excelFile.Worksheet(sheetName)a).ToList().Foreach(Function(e) Console.WriteLine(e("EmpID") + "-----" + e("EmpName") + "-------" + e("Address")))
            'ElseIf _OpcionExcelFactura = OpcionExcelFactura.Preliquidacion Then
            '    resultado = (From row In Excel.Worksheet(cmbHojasCAB.Text) Let item = New Preliquida("", "") With {
            '                       .Precio = row(grdRelaciones.Item("ColExcel", 1).Value.ToString()).Cast(Of String),
            '                       .NoViaje = row(grdRelaciones.Item("ColExcel", 0).Value.ToString()).Cast(Of String)}
            '                 Select item).ToList()

            '    'resultado = (From row In Excel.Worksheet(cmbHojasCAB.Text) Let item = New Preliquida("", "") With {
            '    '              .Precio = row(grdRelaciones.Item("ColExcel", 1).Value.ToString()).Cast(Of Decimal),
            '    '              .NoViaje = row(grdRelaciones.Item("ColExcel", 0).Value.ToString()).Cast(Of String)}
            '    '             Select item).ToList()
            'End If
            'Excel.Dispose()
            'grdCuentas.DataSource = resultado
            'grdCuentas.PerformLayout()
            'lblNoRegExcel.Text = "No. de Registros " & grdCuentas.Rows.Count
            'LlenaGridResultante()
            'TabControl1.SelectedIndex = 1

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try


    End Sub

    Private Sub btnMnuOk_Click(sender As Object, e As EventArgs) Handles btnMnuOk.Click
        If grdResultante.Rows.Count > 0 Then
            TablaFinal.Clear()
            If BandTabla2 Then
                TablaFinal = TablaResultante2
            Else
                TablaFinal = TablaResultante
            End If

        Else
            MsgBox("No hay Datos de Importar", MsgBoxStyle.Exclamation, Me.Text)

        End If
        Me.DialogResult = Windows.Forms.DialogResult.OK
        'Me.Close()
    End Sub

    Private Sub btnMnuCancelar_Click(sender As Object, e As EventArgs) Handles btnMnuCancelar.Click

    End Sub

    Private Sub btnMnuSalir_Click(sender As Object, e As EventArgs) Handles btnMnuSalir.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
    End Sub
End Class