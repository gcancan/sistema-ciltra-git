'Fecha: 15 / Febrero / 2016
'ROGER GALA PACHECO
'************************************************************************************************************************************
'*  Fecha Modificacion :   15 / Febrero / 2016                                                                                       
'*  
'************************************************************************************************************************************
Imports LinqToExcel

Public Class FMigraComercial
    Inherits System.Windows.Forms.Form
    Public WithEvents MeStatus1 As ControlStatus.MeStatus
    Friend WithEvents btnMnuOk As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuCancelar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuSalir As System.Windows.Forms.ToolStripButton
    Private _Tag As String
    Friend WithEvents tabMigra As System.Windows.Forms.TabControl
    Friend WithEvents tbProductos As System.Windows.Forms.TabPage
    Friend WithEvents tbFamilias As System.Windows.Forms.TabPage
    Friend WithEvents grdFamilias As System.Windows.Forms.DataGridView
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents cmbFamilia As System.Windows.Forms.ComboBox
    Friend WithEvents txtFamilia As System.Windows.Forms.TextBox
    Friend WithEvents label1 As System.Windows.Forms.Label
    Friend WithEvents grdProductos As System.Windows.Forms.DataGridView
    'Dim Salida As Boolean
    Dim ObjSql As New ToolSQLs
    Dim StrSql As String
    Dim indice As Integer = 0
    Dim ArraySql() As String



#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents ToolStripMenu As System.Windows.Forms.ToolStrip
    Friend WithEvents txtMenuBusca As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents btnMnuReporte As System.Windows.Forms.ToolStripButton
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FMigraComercial))
        Me.ToolStripMenu = New System.Windows.Forms.ToolStrip()
        Me.txtMenuBusca = New System.Windows.Forms.ToolStripTextBox()
        Me.btnMnuReporte = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuOk = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuCancelar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuSalir = New System.Windows.Forms.ToolStripButton()
        Me.MeStatus1 = New ControlStatus.MeStatus()
        Me.tabMigra = New System.Windows.Forms.TabControl()
        Me.tbProductos = New System.Windows.Forms.TabPage()
        Me.grdProductos = New System.Windows.Forms.DataGridView()
        Me.tbFamilias = New System.Windows.Forms.TabPage()
        Me.label1 = New System.Windows.Forms.Label()
        Me.cmbFamilia = New System.Windows.Forms.ComboBox()
        Me.txtFamilia = New System.Windows.Forms.TextBox()
        Me.grdFamilias = New System.Windows.Forms.DataGridView()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.ToolStripMenu.SuspendLayout()
        Me.tabMigra.SuspendLayout()
        Me.tbProductos.SuspendLayout()
        CType(Me.grdProductos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbFamilias.SuspendLayout()
        CType(Me.grdFamilias, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ToolStripMenu
        '
        Me.ToolStripMenu.AutoSize = False
        Me.ToolStripMenu.BackColor = System.Drawing.Color.Silver
        Me.ToolStripMenu.Dock = System.Windows.Forms.DockStyle.None
        Me.ToolStripMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.txtMenuBusca, Me.btnMnuReporte, Me.btnMnuOk, Me.btnMnuCancelar, Me.btnMnuSalir})
        Me.ToolStripMenu.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow
        Me.ToolStripMenu.Location = New System.Drawing.Point(3, 0)
        Me.ToolStripMenu.Name = "ToolStripMenu"
        Me.ToolStripMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.ToolStripMenu.Size = New System.Drawing.Size(730, 42)
        Me.ToolStripMenu.TabIndex = 1
        Me.ToolStripMenu.Text = "ToolStrip1"
        '
        'txtMenuBusca
        '
        Me.txtMenuBusca.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.txtMenuBusca.Name = "txtMenuBusca"
        Me.txtMenuBusca.Size = New System.Drawing.Size(150, 42)
        Me.txtMenuBusca.ToolTipText = "Filtra Listado"
        '
        'btnMnuReporte
        '
        Me.btnMnuReporte.ForeColor = System.Drawing.Color.Red
        Me.btnMnuReporte.Image = Global.Fletera.My.Resources.Resources._1460375269_excel
        Me.btnMnuReporte.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuReporte.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuReporte.Name = "btnMnuReporte"
        Me.btnMnuReporte.Size = New System.Drawing.Size(71, 39)
        Me.btnMnuReporte.Text = "Carga Excel"
        Me.btnMnuReporte.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuOk
        '
        Me.btnMnuOk.ForeColor = System.Drawing.Color.Red
        Me.btnMnuOk.Image = Global.Fletera.My.Resources.Resources.Save1
        Me.btnMnuOk.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuOk.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuOk.Name = "btnMnuOk"
        Me.btnMnuOk.Size = New System.Drawing.Size(56, 39)
        Me.btnMnuOk.Text = "&GRABAR"
        Me.btnMnuOk.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuCancelar
        '
        Me.btnMnuCancelar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuCancelar.Image = Global.Fletera.My.Resources.Resources.signos_f_059
        Me.btnMnuCancelar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuCancelar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuCancelar.Name = "btnMnuCancelar"
        Me.btnMnuCancelar.Size = New System.Drawing.Size(71, 39)
        Me.btnMnuCancelar.Text = "&CANCELAR"
        Me.btnMnuCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuSalir
        '
        Me.btnMnuSalir.ForeColor = System.Drawing.Color.Red
        Me.btnMnuSalir.Image = Global.Fletera.My.Resources.Resources.Salida2
        Me.btnMnuSalir.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuSalir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuSalir.Name = "btnMnuSalir"
        Me.btnMnuSalir.Size = New System.Drawing.Size(41, 39)
        Me.btnMnuSalir.Text = "&SALIR"
        Me.btnMnuSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'MeStatus1
        '
        Me.MeStatus1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.MeStatus1.Location = New System.Drawing.Point(0, 379)
        Me.MeStatus1.Margin = New System.Windows.Forms.Padding(4)
        Me.MeStatus1.Name = "MeStatus1"
        Me.MeStatus1.Size = New System.Drawing.Size(879, 26)
        Me.MeStatus1.TabIndex = 72
        '
        'tabMigra
        '
        Me.tabMigra.Controls.Add(Me.tbProductos)
        Me.tabMigra.Controls.Add(Me.tbFamilias)
        Me.tabMigra.Location = New System.Drawing.Point(3, 56)
        Me.tabMigra.Name = "tabMigra"
        Me.tabMigra.SelectedIndex = 0
        Me.tabMigra.Size = New System.Drawing.Size(864, 316)
        Me.tabMigra.TabIndex = 73
        '
        'tbProductos
        '
        Me.tbProductos.Controls.Add(Me.grdProductos)
        Me.tbProductos.Location = New System.Drawing.Point(4, 22)
        Me.tbProductos.Name = "tbProductos"
        Me.tbProductos.Padding = New System.Windows.Forms.Padding(3)
        Me.tbProductos.Size = New System.Drawing.Size(856, 290)
        Me.tbProductos.TabIndex = 0
        Me.tbProductos.Text = "Productos"
        Me.tbProductos.UseVisualStyleBackColor = True
        '
        'grdProductos
        '
        Me.grdProductos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdProductos.Location = New System.Drawing.Point(6, 6)
        Me.grdProductos.Name = "grdProductos"
        Me.grdProductos.Size = New System.Drawing.Size(828, 248)
        Me.grdProductos.TabIndex = 2
        '
        'tbFamilias
        '
        Me.tbFamilias.Controls.Add(Me.label1)
        Me.tbFamilias.Controls.Add(Me.cmbFamilia)
        Me.tbFamilias.Controls.Add(Me.txtFamilia)
        Me.tbFamilias.Controls.Add(Me.grdFamilias)
        Me.tbFamilias.Location = New System.Drawing.Point(4, 22)
        Me.tbFamilias.Name = "tbFamilias"
        Me.tbFamilias.Padding = New System.Windows.Forms.Padding(3)
        Me.tbFamilias.Size = New System.Drawing.Size(856, 290)
        Me.tbFamilias.TabIndex = 1
        Me.tbFamilias.Text = "Familias"
        Me.tbFamilias.UseVisualStyleBackColor = True
        '
        'label1
        '
        Me.label1.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.label1.Location = New System.Drawing.Point(431, 21)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(94, 20)
        Me.label1.TabIndex = 15
        Me.label1.Text = "Agrega Familia:"
        '
        'cmbFamilia
        '
        Me.cmbFamilia.FormattingEnabled = True
        Me.cmbFamilia.Location = New System.Drawing.Point(531, 58)
        Me.cmbFamilia.Name = "cmbFamilia"
        Me.cmbFamilia.Size = New System.Drawing.Size(304, 21)
        Me.cmbFamilia.TabIndex = 5
        '
        'txtFamilia
        '
        Me.txtFamilia.Location = New System.Drawing.Point(531, 18)
        Me.txtFamilia.Name = "txtFamilia"
        Me.txtFamilia.Size = New System.Drawing.Size(304, 20)
        Me.txtFamilia.TabIndex = 4
        '
        'grdFamilias
        '
        Me.grdFamilias.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdFamilias.Location = New System.Drawing.Point(6, 6)
        Me.grdFamilias.Name = "grdFamilias"
        Me.grdFamilias.Size = New System.Drawing.Size(368, 278)
        Me.grdFamilias.TabIndex = 3
        '
        'FMigraComercial
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(879, 405)
        Me.Controls.Add(Me.tabMigra)
        Me.Controls.Add(Me.MeStatus1)
        Me.Controls.Add(Me.ToolStripMenu)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "FMigraComercial"
        Me.Text = "Migracion a Comercial"
        Me.ToolStripMenu.ResumeLayout(False)
        Me.ToolStripMenu.PerformLayout()
        Me.tabMigra.ResumeLayout(False)
        Me.tbProductos.ResumeLayout(False)
        CType(Me.grdProductos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbFamilias.ResumeLayout(False)
        Me.tbFamilias.PerformLayout()
        CType(Me.grdFamilias, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region
    Sub New(ByVal vTag As String)
        InitializeComponent()
        _Tag = vTag
    End Sub



    Private Function IniciarSesionSDK() As Boolean
        frmLoginSDK.Close()
        If frmLoginSDK.ShowDialog = Windows.Forms.DialogResult.OK Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub btnMnuOk_Click(sender As Object, e As EventArgs) Handles btnMnuOk.Click
        Dim BandGraba As Boolean = False

        If IniciarSesionSDK() = False Then End

        FEspera.StartPosition = FormStartPosition.CenterScreen
        If bEmpresaSDKAbierta Then
            If tabMigra.SelectedTab.Name = tbProductos.Name Then
                'PRODUCTOS
                If grdProductos.RowCount > 0 Then
                    indice = 0
                    Dim tdProd As New tProduto
                    Dim idProd As Integer

                    FEspera.Show("Migrando Productos a Sistema COMERCIAL i ...")
                    For i As Integer = 0 To grdProductos.Rows.Count - 1
                        'For i As Integer = 0 To 10


                        tdProd.cCodigoProducto = grdProductos.Item("CCODIGOPRODUCTO", i).Value
                        tdProd.cNombreProducto = Microsoft.VisualBasic.Left(grdProductos.Item("CNOMBREPRODUCTO", i).Value + " " + grdProductos.Item("CMODELOPRODUCTO", i).Value, 60)
                        tdProd.cTipoProducto = 1
                        tdProd.cControlExistencia = 1
                        tdProd.cMetodoCosteo = 1
                        tdProd.cStatusProducto = 1
                        tdProd.cCodigoUnidadBase = "PIEZA"
                        tdProd.cFechaAltaProducto = Now.ToString("MM/dd/yyyy")
                        'tdProd.cCodigoValorClasificacion1 = grdProductos.Item("cCodigoValorClasificacion1", i).Value


                        'Agregar para actualizar codigo
                        'Codigo Alterno es por SQL
                        StrSql = ObjSql.ActualizaCampoTabla("admProductos", "CCODALTERN", TipoDato.TdCadena, grdProductos.Item("CCODIGOALTERNO", i).Value, _
                        "CCODIGOPRODUCTO", TipoDato.TdCadena, tdProd.cCodigoProducto)
                        ReDim Preserve ArraySql(indice)
                        ArraySql(indice) = StrSql
                        indice += 1

                        'CDESGLOSAI2
                        StrSql = ObjSql.ActualizaCampoTabla("admProductos", "CDESGLOSAI2", TipoDato.TdNumerico, "1", _
                        "CCODIGOPRODUCTO", TipoDato.TdCadena, tdProd.cCodigoProducto)
                        ReDim Preserve ArraySql(indice)
                        ArraySql(indice) = StrSql
                        indice += 1

                        'CIDVALORCLASIFICACION1
                        StrSql = ObjSql.ActualizaCampoTabla("admProductos", "CIDVALORCLASIFICACION1", TipoDato.TdNumerico, grdProductos.Item("cCodigoValorClasificacion1", i).Value, _
                        "CCODIGOPRODUCTO", TipoDato.TdCadena, tdProd.cCodigoProducto)
                        ReDim Preserve ArraySql(indice)
                        ArraySql(indice) = StrSql
                        indice += 1


                        lError = fAltaProducto(idProd, tdProd)
                        If lError <> 0 Then
                            FEspera.Close()
                            DespliegaError(lError)
                            fCierraEmpresa()
                            fTerminaSDK()
                            BandGraba = False
                            Exit For
                        Else
                            'MsgBox("Articulo: " & grdProductos.Item("CNOMBREPRODUCTO", i).Value & " Dado de Alta en Comercial")
                            FEspera.Show("Articulo: " & grdProductos.Item("CNOMBREPRODUCTO", i).Value & " Migrado a Comercial")
                            BandGraba = True
                        End If
                    Next

                    If BandGraba Then
                        If indice > 0 Then
                            Dim obj As New CapaNegocio.Tablas
                            Dim Respuesta As String
                            Respuesta = obj.EjecutarSql(Inicio.CONSTR_COM, "SQL", ArraySql, Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, indice)
                            If Respuesta = "EFECTUADO" Then
                                MsgBox("Se Efectuo correctamente la migracion de Productos", MsgBoxStyle.Exclamation, Me.Text)
                                FEspera.Close()

                            End If
                        End If
                    End If
                Else
                    'no hay datos que grabar
                End If
            ElseIf tabMigra.SelectedTab.Name = tbFamilias.Name Then
                FEspera.Show("Actualiza Clasificacion COMERCIAL i ...")
                lError = fActualizaClasificacion(5, 1, "FAMILIA")
                If lError <> 0 Then
                    FEspera.Close()
                    DespliegaError(lError)
                    fCierraEmpresa()
                    fTerminaSDK()
                Else
                    Dim tdClas As New tValorClasificacion
                    Dim aIdValorClasif As Integer = 0
                    If grdFamilias.RowCount > 0 Then
                        For i As Integer = 0 To grdFamilias.Rows.Count - 1
                            lError = fInsertaValorClasif()
                            If lError <> 0 Then
                                FEspera.Close()
                                DespliegaError(lError)
                                fCierraEmpresa()
                                fTerminaSDK()
                                Exit For
                            Else
                                lError = fSetDatoValorClasif("CIDCLASIFICACION", 25)
                                If lError <> 0 Then
                                    FEspera.Close()
                                    DespliegaError(lError)
                                    fCierraEmpresa()
                                    fTerminaSDK()
                                    Exit For
                                Else
                                    lError = fSetDatoValorClasif("CVALORCLASIFICACION", grdFamilias.Item("cValorClasificacion", i).Value)
                                    If lError <> 0 Then
                                        FEspera.Close()
                                        DespliegaError(lError)
                                        fCierraEmpresa()
                                        fTerminaSDK()
                                        Exit For
                                    Else
                                        lError = fSetDatoValorClasif("CCODIGOVALORCLASIFICACION", IIf(i + 1 < 10, "0" & i + 1, i + 1))
                                        If lError <> 0 Then

                                        Else

                                        End If

                                        lError = fGuardaValorClasif()
                                        If lError <> 0 Then
                                            FEspera.Close()
                                            DespliegaError(lError)
                                            fCierraEmpresa()
                                            fTerminaSDK()
                                            Exit For
                                        Else
                                            FEspera.Show("Familia: " & grdFamilias.Item("cValorClasificacion", i).Value & " Migrado a Comercial")
                                        End If
                                    End If
                                End If
                            End If


                        Next
                        FEspera.Close()
                        MsgBox("Datos Migrados Satisfactoriamente al Programa COMERCIAL", MsgBoxStyle.Information, Me.Text)
                    End If
                    'Si tiene Datos el Grid se toma del grid
                    'si no del textbox


                End If
            End If
        End If
    End Sub

    Private Sub btnMnuReporte_Click(sender As Object, e As EventArgs) Handles btnMnuReporte.Click
        Dim vNomArchivo As String = ""
        Dim filtro As String = "Archivos Excel(*.XLS;*.XLSX;)|*.XLS;*.XLSX;|Todos (*.*)|*.* "
        OpenFileDialog1.Filter = filtro
        If OpenFileDialog1.ShowDialog = DialogResult.Cancel Then
            Exit Sub
        End If

        vNomArchivo = OpenFileDialog1.FileName
        'Dim excel = New ExcelQueryFactory(vNomArchivo)
        'If tabMigra.SelectedTab.Name = tbProductos.Name Then
        '    excel.AddMapping(Of CamposMigraCOMClass)(Function(m) m.CCODIGOPRODUCTO, "CODIGO NUEVO")
        '    excel.AddMapping(Of CamposMigraCOMClass)(Function(m) m.CNOMBREPRODUCTO, "PRODUCTO")
        '    excel.AddMapping(Of CamposMigraCOMClass)(Function(m) m.cCodigoValorClasificacion1, "FAMILIA")
        '    excel.AddMapping(Of CamposMigraCOMClass)(Function(m) m.CMODELOPRODUCTO, "MODELO")
        '    excel.AddMapping(Of CamposMigraCOMClass)(Function(m) m.CCODIGOALTERNO, "CODIGO")
        'CCODIGOALTERNO


        'Dim data = From x In excel.Worksheet(Of CamposMigraCOMClass)("ARTICULOS") Where x.CCODIGOPRODUCTO <> ""
        '        Select x

        '    'CargaExcelClass
        '    grdProductos.DataSource = data.ToList
        'ElseIf tabMigra.SelectedTab.Name = tbFamilias.Name Then
        '    'CamposMigraCOMClass
        '    excel.AddMapping(Of CamposMigraCOMClass)(Function(m) m.cCodigoValorClasificacion, "NO")
        '    excel.AddMapping(Of CamposMigraCOMClass)(Function(m) m.cValorClasificacion, "NOMBRE")

        '    Dim data = From x In excel.Worksheet(Of CamposMigraCOMClass)("FAMILIAS") Where x.cValorClasificacion <> ""
        '     Select x

        '    grdFamilias.DataSource = data.ToList
        'End If
    End Sub

    Private Sub btnMnuCancelar_Click(sender As Object, e As EventArgs) Handles btnMnuCancelar.Click
        IniciarSesionSDK()
    End Sub

    Private Sub btnMnuSalir_Click(sender As Object, e As EventArgs) Handles btnMnuSalir.Click
        Me.Close()
    End Sub

    Private Sub txtFamilia_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtFamilia.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then

            'Dim item As New ComboBoxItem

            'ComboBoxItem item = new ComboBoxItem();
            'item.SetValue  = glob.Articulo.MAKTX;
            'item.Value = glob.Articulo.MATNR;

            'cmbArticulos.Items.Add(item);

            'cmbArticulos.SelectedIndex = 0;

            'cmbFamilia()
        End If
    End Sub

    Private Sub txtFamilia_TextChanged(sender As Object, e As EventArgs) Handles txtFamilia.TextChanged

    End Sub
End Class
