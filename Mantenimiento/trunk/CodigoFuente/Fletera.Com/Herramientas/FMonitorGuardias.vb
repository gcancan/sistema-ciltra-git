'Fecha: 15 / Febrero / 2016
'ROGER GALA PACHECO
'************************************************************************************************************************************
'*  Fecha Modificacion :   15 / Febrero / 2016                                                                                       
'*  
'************************************************************************************************************************************
Imports FiltroDinamicoGrid

Public Class FMonitorGuardias
    Inherits System.Windows.Forms.Form
    Public WithEvents MeStatus1 As ControlStatus.MeStatus
    Private _Tag As String
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    'Dim Salida As Boolean
    Dim ObjSql As New ToolSQLs
    Dim StrSql As String
    Dim indice As Integer = 0
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents ChkHoy As System.Windows.Forms.CheckBox
    Friend WithEvents ChkTmpReal As System.Windows.Forms.CheckBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Cale1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents btnBuscar As System.Windows.Forms.Button
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents TSActualizar As System.Windows.Forms.ToolStripButton
    Friend WithEvents TSFiltroAvanzado As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
    Dim ArraySql() As String

    Private _ColorCapES As Color = Color.LightGray
    Private _ColorEntES As Color = Color.LightGreen
    Private _ColorSalES As Color = Color.LightBlue
    Private _ColorCanES As Color = Color.OrangeRed

    'Private _ColorTer As Color = Color.LightGreen
    ''Private _ColorEnt As Color = Color.LightGoldenrodYellow
    'Private _ColorEnt As Color = Color.Orange
    'Private _ColorCan As Color = Color.OrangeRed

    'Private _TiempoActu As Integer = 10 'Cada 10 segundos
    Private _TiempoActu As Integer = 20 'Cada 10 segundos

    Dim FAct As Boolean = False

    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents LTiempo As System.Windows.Forms.Label

    Dim IdOrdenServ As Integer

    Dim TablaBd As String = "OT"
    Friend WithEvents tabMonitor As TabControl
    Friend WithEvents tbMonEntSal As TabPage
    Friend WithEvents gpoEntSalEst As GroupBox
    Friend WithEvents ChkCan As CheckBox
    Friend WithEvents ChkSal As CheckBox
    Friend WithEvents ChkCap As CheckBox
    Friend WithEvents ChkEnt As CheckBox
    Friend WithEvents grdMonitorES As DataGridView
    Friend WithEvents tbMonComb As TabPage
    Friend WithEvents grdMonitorCom As DataGridView
    Friend WithEvents tbMonTaller As TabPage
    Friend WithEvents tbMonLlantas As TabPage
    Friend WithEvents tbMonLavadero As TabPage
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents cmbEmpresa As ComboBox
    Dim MyTablaDet As DataTable = New DataTable(TablaBd)
    Friend WithEvents grdMonitorESBit As DataGridView
    Friend WithEvents Panel2 As Panel
    Friend WithEvents Panel1 As Panel
    Private con As String
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents grdMonitorDet As DataGridView
    Friend WithEvents gpoComb As GroupBox
    Friend WithEvents ChkCanCom As CheckBox
    Friend WithEvents ChkTerCom As CheckBox
    Friend WithEvents ChkCapCom As CheckBox
    Friend WithEvents ChkEntCom As CheckBox
    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents gpoTaller As GroupBox
    Friend WithEvents CheckBox1 As CheckBox
    Friend WithEvents CheckBox2 As CheckBox
    Friend WithEvents ChkPreTall As CheckBox
    Friend WithEvents CheckBox4 As CheckBox
    Friend WithEvents DataGridView2 As DataGridView
    Friend WithEvents GroupBox5 As GroupBox
    Friend WithEvents CheckBox5 As CheckBox
    Friend WithEvents CheckBox6 As CheckBox
    Friend WithEvents CheckBox7 As CheckBox
    Friend WithEvents CheckBox8 As CheckBox
    Friend WithEvents DataGridView3 As DataGridView
    Friend WithEvents GroupBox6 As GroupBox
    Friend WithEvents CheckBox9 As CheckBox
    Friend WithEvents CheckBox10 As CheckBox
    Friend WithEvents CheckBox11 As CheckBox
    Friend WithEvents CheckBox12 As CheckBox
    Private Empresas As EmpresaClass



#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FMonitorGuardias))
        Me.MeStatus1 = New ControlStatus.MeStatus()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.ChkHoy = New System.Windows.Forms.CheckBox()
        Me.ChkTmpReal = New System.Windows.Forms.CheckBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Cale1 = New System.Windows.Forms.DateTimePicker()
        Me.btnBuscar = New System.Windows.Forms.Button()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.TSFiltroAvanzado = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.TSActualizar = New System.Windows.Forms.ToolStripButton()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.LTiempo = New System.Windows.Forms.Label()
        Me.tabMonitor = New System.Windows.Forms.TabControl()
        Me.tbMonEntSal = New System.Windows.Forms.TabPage()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.grdMonitorESBit = New System.Windows.Forms.DataGridView()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.grdMonitorES = New System.Windows.Forms.DataGridView()
        Me.gpoEntSalEst = New System.Windows.Forms.GroupBox()
        Me.ChkCan = New System.Windows.Forms.CheckBox()
        Me.ChkSal = New System.Windows.Forms.CheckBox()
        Me.ChkCap = New System.Windows.Forms.CheckBox()
        Me.ChkEnt = New System.Windows.Forms.CheckBox()
        Me.tbMonComb = New System.Windows.Forms.TabPage()
        Me.grdMonitorCom = New System.Windows.Forms.DataGridView()
        Me.tbMonTaller = New System.Windows.Forms.TabPage()
        Me.tbMonLlantas = New System.Windows.Forms.TabPage()
        Me.tbMonLavadero = New System.Windows.Forms.TabPage()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.cmbEmpresa = New System.Windows.Forms.ComboBox()
        Me.grdMonitorDet = New System.Windows.Forms.DataGridView()
        Me.gpoComb = New System.Windows.Forms.GroupBox()
        Me.ChkCanCom = New System.Windows.Forms.CheckBox()
        Me.ChkTerCom = New System.Windows.Forms.CheckBox()
        Me.ChkCapCom = New System.Windows.Forms.CheckBox()
        Me.ChkEntCom = New System.Windows.Forms.CheckBox()
        Me.gpoTaller = New System.Windows.Forms.GroupBox()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.CheckBox2 = New System.Windows.Forms.CheckBox()
        Me.ChkPreTall = New System.Windows.Forms.CheckBox()
        Me.CheckBox4 = New System.Windows.Forms.CheckBox()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.CheckBox5 = New System.Windows.Forms.CheckBox()
        Me.CheckBox6 = New System.Windows.Forms.CheckBox()
        Me.CheckBox7 = New System.Windows.Forms.CheckBox()
        Me.CheckBox8 = New System.Windows.Forms.CheckBox()
        Me.DataGridView2 = New System.Windows.Forms.DataGridView()
        Me.DataGridView3 = New System.Windows.Forms.DataGridView()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.CheckBox9 = New System.Windows.Forms.CheckBox()
        Me.CheckBox10 = New System.Windows.Forms.CheckBox()
        Me.CheckBox11 = New System.Windows.Forms.CheckBox()
        Me.CheckBox12 = New System.Windows.Forms.CheckBox()
        Me.GroupBox2.SuspendLayout()
        Me.ToolStrip1.SuspendLayout()
        Me.tabMonitor.SuspendLayout()
        Me.tbMonEntSal.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.grdMonitorESBit, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.grdMonitorES, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gpoEntSalEst.SuspendLayout()
        Me.tbMonComb.SuspendLayout()
        CType(Me.grdMonitorCom, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbMonTaller.SuspendLayout()
        Me.tbMonLlantas.SuspendLayout()
        Me.tbMonLavadero.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.grdMonitorDet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gpoComb.SuspendLayout()
        Me.gpoTaller.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox5.SuspendLayout()
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridView3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox6.SuspendLayout()
        Me.SuspendLayout()
        '
        'MeStatus1
        '
        Me.MeStatus1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.MeStatus1.Location = New System.Drawing.Point(0, 552)
        Me.MeStatus1.Margin = New System.Windows.Forms.Padding(4)
        Me.MeStatus1.Name = "MeStatus1"
        Me.MeStatus1.Size = New System.Drawing.Size(1243, 26)
        Me.MeStatus1.TabIndex = 72
        '
        'GroupBox2
        '
        Me.GroupBox2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox2.Controls.Add(Me.ChkHoy)
        Me.GroupBox2.Controls.Add(Me.ChkTmpReal)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.Cale1)
        Me.GroupBox2.Controls.Add(Me.btnBuscar)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.ForeColor = System.Drawing.Color.Black
        Me.GroupBox2.Location = New System.Drawing.Point(624, 27)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(532, 49)
        Me.GroupBox2.TabIndex = 75
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Opciones de Busqueda"
        '
        'ChkHoy
        '
        Me.ChkHoy.AutoSize = True
        Me.ChkHoy.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ChkHoy.Location = New System.Drawing.Point(150, 22)
        Me.ChkHoy.Name = "ChkHoy"
        Me.ChkHoy.Size = New System.Drawing.Size(128, 20)
        Me.ChkHoy.TabIndex = 10
        Me.ChkHoy.Text = "Solo Fecha HOY"
        Me.ChkHoy.UseVisualStyleBackColor = True
        '
        'ChkTmpReal
        '
        Me.ChkTmpReal.AutoSize = True
        Me.ChkTmpReal.Checked = True
        Me.ChkTmpReal.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ChkTmpReal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ChkTmpReal.Location = New System.Drawing.Point(13, 22)
        Me.ChkTmpReal.Name = "ChkTmpReal"
        Me.ChkTmpReal.Size = New System.Drawing.Size(119, 20)
        Me.ChkTmpReal.TabIndex = 9
        Me.ChkTmpReal.Text = "En tiempo Real"
        Me.ChkTmpReal.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(296, 20)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(112, 20)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Buscar Fecha:"
        '
        'Cale1
        '
        Me.Cale1.Enabled = False
        Me.Cale1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Cale1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.Cale1.Location = New System.Drawing.Point(414, 18)
        Me.Cale1.Name = "Cale1"
        Me.Cale1.Size = New System.Drawing.Size(105, 22)
        Me.Cale1.TabIndex = 7
        '
        'btnBuscar
        '
        Me.btnBuscar.Enabled = False
        Me.btnBuscar.FlatAppearance.BorderSize = 0
        Me.btnBuscar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.btnBuscar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBuscar.Location = New System.Drawing.Point(525, 13)
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.Size = New System.Drawing.Size(23, 27)
        Me.btnBuscar.TabIndex = 8
        Me.btnBuscar.UseVisualStyleBackColor = True
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TSFiltroAvanzado, Me.ToolStripButton1, Me.TSActualizar})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(1243, 25)
        Me.ToolStrip1.TabIndex = 76
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'TSFiltroAvanzado
        '
        Me.TSFiltroAvanzado.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TSFiltroAvanzado.Name = "TSFiltroAvanzado"
        Me.TSFiltroAvanzado.Size = New System.Drawing.Size(133, 22)
        Me.TSFiltroAvanzado.Text = "Activar Filtro Avanzado"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.ToolStripButton1.Image = Global.Fletera.My.Resources.Resources.Salida2
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(49, 22)
        Me.ToolStripButton1.Text = "Salir"
        '
        'TSActualizar
        '
        Me.TSActualizar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.TSActualizar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.TSActualizar.Image = CType(resources.GetObject("TSActualizar.Image"), System.Drawing.Image)
        Me.TSActualizar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TSActualizar.Name = "TSActualizar"
        Me.TSActualizar.Size = New System.Drawing.Size(23, 22)
        Me.TSActualizar.Text = "Actualizar"
        '
        'Timer1
        '
        Me.Timer1.Interval = 1000
        '
        'LTiempo
        '
        Me.LTiempo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LTiempo.AutoSize = True
        Me.LTiempo.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LTiempo.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.LTiempo.Location = New System.Drawing.Point(1193, 48)
        Me.LTiempo.Name = "LTiempo"
        Me.LTiempo.Size = New System.Drawing.Size(28, 21)
        Me.LTiempo.TabIndex = 78
        Me.LTiempo.Text = "10"
        '
        'tabMonitor
        '
        Me.tabMonitor.Controls.Add(Me.tbMonEntSal)
        Me.tabMonitor.Controls.Add(Me.tbMonComb)
        Me.tabMonitor.Controls.Add(Me.tbMonTaller)
        Me.tabMonitor.Controls.Add(Me.tbMonLlantas)
        Me.tabMonitor.Controls.Add(Me.tbMonLavadero)
        Me.tabMonitor.Location = New System.Drawing.Point(12, 82)
        Me.tabMonitor.Name = "tabMonitor"
        Me.tabMonitor.SelectedIndex = 0
        Me.tabMonitor.Size = New System.Drawing.Size(1219, 463)
        Me.tabMonitor.TabIndex = 80
        '
        'tbMonEntSal
        '
        Me.tbMonEntSal.Controls.Add(Me.Panel2)
        Me.tbMonEntSal.Controls.Add(Me.Panel1)
        Me.tbMonEntSal.Controls.Add(Me.gpoEntSalEst)
        Me.tbMonEntSal.Location = New System.Drawing.Point(4, 22)
        Me.tbMonEntSal.Name = "tbMonEntSal"
        Me.tbMonEntSal.Padding = New System.Windows.Forms.Padding(3)
        Me.tbMonEntSal.Size = New System.Drawing.Size(1211, 437)
        Me.tbMonEntSal.TabIndex = 0
        Me.tbMonEntSal.Text = "Entradas/Salidas"
        Me.tbMonEntSal.UseVisualStyleBackColor = True
        '
        'Panel2
        '
        Me.Panel2.AutoScroll = True
        Me.Panel2.Controls.Add(Me.Label3)
        Me.Panel2.Controls.Add(Me.grdMonitorESBit)
        Me.Panel2.Location = New System.Drawing.Point(6, 259)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(1196, 172)
        Me.Panel2.TabIndex = 79
        '
        'Label3
        '
        Me.Label3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Red
        Me.Label3.Location = New System.Drawing.Point(18, -4)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(18, 182)
        Me.Label3.TabIndex = 78
        Me.Label3.Text = "BIT�CORA"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'grdMonitorESBit
        '
        Me.grdMonitorESBit.AllowUserToAddRows = False
        Me.grdMonitorESBit.AllowUserToDeleteRows = False
        Me.grdMonitorESBit.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdMonitorESBit.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdMonitorESBit.Location = New System.Drawing.Point(53, 3)
        Me.grdMonitorESBit.Name = "grdMonitorESBit"
        Me.grdMonitorESBit.Size = New System.Drawing.Size(1140, 166)
        Me.grdMonitorESBit.TabIndex = 77
        '
        'Panel1
        '
        Me.Panel1.AutoScroll = True
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.grdMonitorES)
        Me.Panel1.Location = New System.Drawing.Point(8, 60)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1197, 193)
        Me.Panel1.TabIndex = 78
        '
        'Label2
        '
        Me.Label2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Blue
        Me.Label2.Location = New System.Drawing.Point(16, 3)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(10, 179)
        Me.Label2.TabIndex = 76
        Me.Label2.Text = "SOLICITUDES"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'grdMonitorES
        '
        Me.grdMonitorES.AllowUserToAddRows = False
        Me.grdMonitorES.AllowUserToDeleteRows = False
        Me.grdMonitorES.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdMonitorES.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdMonitorES.Location = New System.Drawing.Point(51, 3)
        Me.grdMonitorES.Name = "grdMonitorES"
        Me.grdMonitorES.Size = New System.Drawing.Size(1143, 177)
        Me.grdMonitorES.TabIndex = 74
        '
        'gpoEntSalEst
        '
        Me.gpoEntSalEst.Controls.Add(Me.ChkCan)
        Me.gpoEntSalEst.Controls.Add(Me.ChkSal)
        Me.gpoEntSalEst.Controls.Add(Me.ChkCap)
        Me.gpoEntSalEst.Controls.Add(Me.ChkEnt)
        Me.gpoEntSalEst.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gpoEntSalEst.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.gpoEntSalEst.Location = New System.Drawing.Point(8, 6)
        Me.gpoEntSalEst.Name = "gpoEntSalEst"
        Me.gpoEntSalEst.Size = New System.Drawing.Size(449, 48)
        Me.gpoEntSalEst.TabIndex = 75
        Me.gpoEntSalEst.TabStop = False
        Me.gpoEntSalEst.Text = "Estatus"
        '
        'ChkCan
        '
        Me.ChkCan.AutoSize = True
        Me.ChkCan.BackColor = System.Drawing.SystemColors.Control
        Me.ChkCan.Checked = True
        Me.ChkCan.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ChkCan.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ChkCan.ForeColor = System.Drawing.Color.Black
        Me.ChkCan.Location = New System.Drawing.Point(344, 22)
        Me.ChkCan.Name = "ChkCan"
        Me.ChkCan.Size = New System.Drawing.Size(93, 20)
        Me.ChkCan.TabIndex = 5
        Me.ChkCan.Text = "Cancelado"
        Me.ChkCan.UseVisualStyleBackColor = False
        '
        'ChkSal
        '
        Me.ChkSal.AutoSize = True
        Me.ChkSal.BackColor = System.Drawing.SystemColors.Control
        Me.ChkSal.Checked = True
        Me.ChkSal.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ChkSal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ChkSal.ForeColor = System.Drawing.Color.Black
        Me.ChkSal.Location = New System.Drawing.Point(189, 20)
        Me.ChkSal.Name = "ChkSal"
        Me.ChkSal.Size = New System.Drawing.Size(66, 20)
        Me.ChkSal.TabIndex = 1
        Me.ChkSal.Text = "Salida"
        Me.ChkSal.UseVisualStyleBackColor = False
        '
        'ChkCap
        '
        Me.ChkCap.AutoSize = True
        Me.ChkCap.BackColor = System.Drawing.SystemColors.Control
        Me.ChkCap.Checked = True
        Me.ChkCap.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ChkCap.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ChkCap.ForeColor = System.Drawing.Color.Black
        Me.ChkCap.Location = New System.Drawing.Point(6, 22)
        Me.ChkCap.Name = "ChkCap"
        Me.ChkCap.Size = New System.Drawing.Size(86, 20)
        Me.ChkCap.TabIndex = 0
        Me.ChkCap.Text = "Por Entrar"
        Me.ChkCap.UseVisualStyleBackColor = False
        '
        'ChkEnt
        '
        Me.ChkEnt.AutoSize = True
        Me.ChkEnt.BackColor = System.Drawing.SystemColors.Control
        Me.ChkEnt.Checked = True
        Me.ChkEnt.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ChkEnt.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ChkEnt.ForeColor = System.Drawing.Color.Black
        Me.ChkEnt.Location = New System.Drawing.Point(109, 22)
        Me.ChkEnt.Name = "ChkEnt"
        Me.ChkEnt.Size = New System.Drawing.Size(74, 20)
        Me.ChkEnt.TabIndex = 4
        Me.ChkEnt.Text = "Entrada"
        Me.ChkEnt.UseVisualStyleBackColor = False
        '
        'tbMonComb
        '
        Me.tbMonComb.Controls.Add(Me.gpoComb)
        Me.tbMonComb.Controls.Add(Me.grdMonitorDet)
        Me.tbMonComb.Controls.Add(Me.grdMonitorCom)
        Me.tbMonComb.Location = New System.Drawing.Point(4, 22)
        Me.tbMonComb.Name = "tbMonComb"
        Me.tbMonComb.Padding = New System.Windows.Forms.Padding(3)
        Me.tbMonComb.Size = New System.Drawing.Size(1211, 437)
        Me.tbMonComb.TabIndex = 1
        Me.tbMonComb.Text = "Combustible"
        Me.tbMonComb.UseVisualStyleBackColor = True
        '
        'grdMonitorCom
        '
        Me.grdMonitorCom.AllowUserToAddRows = False
        Me.grdMonitorCom.AllowUserToDeleteRows = False
        Me.grdMonitorCom.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdMonitorCom.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdMonitorCom.Location = New System.Drawing.Point(3, 52)
        Me.grdMonitorCom.Name = "grdMonitorCom"
        Me.grdMonitorCom.Size = New System.Drawing.Size(1202, 258)
        Me.grdMonitorCom.TabIndex = 75
        '
        'tbMonTaller
        '
        Me.tbMonTaller.Controls.Add(Me.DataGridView1)
        Me.tbMonTaller.Controls.Add(Me.gpoTaller)
        Me.tbMonTaller.Location = New System.Drawing.Point(4, 22)
        Me.tbMonTaller.Name = "tbMonTaller"
        Me.tbMonTaller.Padding = New System.Windows.Forms.Padding(3)
        Me.tbMonTaller.Size = New System.Drawing.Size(1211, 437)
        Me.tbMonTaller.TabIndex = 2
        Me.tbMonTaller.Text = "Taller"
        Me.tbMonTaller.UseVisualStyleBackColor = True
        '
        'tbMonLlantas
        '
        Me.tbMonLlantas.Controls.Add(Me.DataGridView2)
        Me.tbMonLlantas.Controls.Add(Me.GroupBox5)
        Me.tbMonLlantas.Location = New System.Drawing.Point(4, 22)
        Me.tbMonLlantas.Name = "tbMonLlantas"
        Me.tbMonLlantas.Size = New System.Drawing.Size(1211, 437)
        Me.tbMonLlantas.TabIndex = 3
        Me.tbMonLlantas.Text = "llantas"
        Me.tbMonLlantas.UseVisualStyleBackColor = True
        '
        'tbMonLavadero
        '
        Me.tbMonLavadero.Controls.Add(Me.DataGridView3)
        Me.tbMonLavadero.Controls.Add(Me.GroupBox6)
        Me.tbMonLavadero.Location = New System.Drawing.Point(4, 22)
        Me.tbMonLavadero.Name = "tbMonLavadero"
        Me.tbMonLavadero.Size = New System.Drawing.Size(1211, 437)
        Me.tbMonLavadero.TabIndex = 4
        Me.tbMonLavadero.Text = "Lavadero"
        Me.tbMonLavadero.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.GroupBox1.Location = New System.Drawing.Point(12, 28)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(449, 48)
        Me.GroupBox1.TabIndex = 75
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Empresa"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.cmbEmpresa)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.GroupBox3.Location = New System.Drawing.Point(12, 28)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(449, 48)
        Me.GroupBox3.TabIndex = 75
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Empresa"
        '
        'cmbEmpresa
        '
        Me.cmbEmpresa.FormattingEnabled = True
        Me.cmbEmpresa.Location = New System.Drawing.Point(90, 14)
        Me.cmbEmpresa.Name = "cmbEmpresa"
        Me.cmbEmpresa.Size = New System.Drawing.Size(353, 28)
        Me.cmbEmpresa.TabIndex = 1
        '
        'grdMonitorDet
        '
        Me.grdMonitorDet.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdMonitorDet.Location = New System.Drawing.Point(347, 350)
        Me.grdMonitorDet.Name = "grdMonitorDet"
        Me.grdMonitorDet.Size = New System.Drawing.Size(240, 150)
        Me.grdMonitorDet.TabIndex = 76
        '
        'gpoComb
        '
        Me.gpoComb.Controls.Add(Me.ChkCanCom)
        Me.gpoComb.Controls.Add(Me.ChkTerCom)
        Me.gpoComb.Controls.Add(Me.ChkCapCom)
        Me.gpoComb.Controls.Add(Me.ChkEntCom)
        Me.gpoComb.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gpoComb.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.gpoComb.Location = New System.Drawing.Point(3, 3)
        Me.gpoComb.Name = "gpoComb"
        Me.gpoComb.Size = New System.Drawing.Size(449, 48)
        Me.gpoComb.TabIndex = 77
        Me.gpoComb.TabStop = False
        Me.gpoComb.Text = "Estatus"
        '
        'ChkCanCom
        '
        Me.ChkCanCom.AutoSize = True
        Me.ChkCanCom.BackColor = System.Drawing.SystemColors.Control
        Me.ChkCanCom.Checked = True
        Me.ChkCanCom.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ChkCanCom.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ChkCanCom.ForeColor = System.Drawing.Color.Black
        Me.ChkCanCom.Location = New System.Drawing.Point(344, 22)
        Me.ChkCanCom.Name = "ChkCanCom"
        Me.ChkCanCom.Size = New System.Drawing.Size(93, 20)
        Me.ChkCanCom.TabIndex = 5
        Me.ChkCanCom.Text = "Cancelado"
        Me.ChkCanCom.UseVisualStyleBackColor = False
        '
        'ChkTerCom
        '
        Me.ChkTerCom.AutoSize = True
        Me.ChkTerCom.BackColor = System.Drawing.SystemColors.Control
        Me.ChkTerCom.Checked = True
        Me.ChkTerCom.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ChkTerCom.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ChkTerCom.ForeColor = System.Drawing.Color.Black
        Me.ChkTerCom.Location = New System.Drawing.Point(204, 22)
        Me.ChkTerCom.Name = "ChkTerCom"
        Me.ChkTerCom.Size = New System.Drawing.Size(93, 20)
        Me.ChkTerCom.TabIndex = 1
        Me.ChkTerCom.Text = "Terminado"
        Me.ChkTerCom.UseVisualStyleBackColor = False
        '
        'ChkCapCom
        '
        Me.ChkCapCom.AutoSize = True
        Me.ChkCapCom.BackColor = System.Drawing.SystemColors.Control
        Me.ChkCapCom.Checked = True
        Me.ChkCapCom.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ChkCapCom.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ChkCapCom.ForeColor = System.Drawing.Color.Black
        Me.ChkCapCom.Location = New System.Drawing.Point(6, 22)
        Me.ChkCapCom.Name = "ChkCapCom"
        Me.ChkCapCom.Size = New System.Drawing.Size(86, 20)
        Me.ChkCapCom.TabIndex = 0
        Me.ChkCapCom.Text = "Por Entrar"
        Me.ChkCapCom.UseVisualStyleBackColor = False
        '
        'ChkEntCom
        '
        Me.ChkEntCom.AutoSize = True
        Me.ChkEntCom.BackColor = System.Drawing.SystemColors.Control
        Me.ChkEntCom.Checked = True
        Me.ChkEntCom.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ChkEntCom.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ChkEntCom.ForeColor = System.Drawing.Color.Black
        Me.ChkEntCom.Location = New System.Drawing.Point(109, 22)
        Me.ChkEntCom.Name = "ChkEntCom"
        Me.ChkEntCom.Size = New System.Drawing.Size(78, 20)
        Me.ChkEntCom.TabIndex = 4
        Me.ChkEntCom.Text = "En Base"
        Me.ChkEntCom.UseVisualStyleBackColor = False
        '
        'gpoTaller
        '
        Me.gpoTaller.Controls.Add(Me.CheckBox1)
        Me.gpoTaller.Controls.Add(Me.CheckBox2)
        Me.gpoTaller.Controls.Add(Me.ChkPreTall)
        Me.gpoTaller.Controls.Add(Me.CheckBox4)
        Me.gpoTaller.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gpoTaller.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.gpoTaller.Location = New System.Drawing.Point(6, 6)
        Me.gpoTaller.Name = "gpoTaller"
        Me.gpoTaller.Size = New System.Drawing.Size(449, 48)
        Me.gpoTaller.TabIndex = 78
        Me.gpoTaller.TabStop = False
        Me.gpoTaller.Text = "Estatus"
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.BackColor = System.Drawing.SystemColors.Control
        Me.CheckBox1.Checked = True
        Me.CheckBox1.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox1.ForeColor = System.Drawing.Color.Black
        Me.CheckBox1.Location = New System.Drawing.Point(344, 22)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(93, 20)
        Me.CheckBox1.TabIndex = 5
        Me.CheckBox1.Text = "Cancelado"
        Me.CheckBox1.UseVisualStyleBackColor = False
        '
        'CheckBox2
        '
        Me.CheckBox2.AutoSize = True
        Me.CheckBox2.BackColor = System.Drawing.SystemColors.Control
        Me.CheckBox2.Checked = True
        Me.CheckBox2.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox2.ForeColor = System.Drawing.Color.Black
        Me.CheckBox2.Location = New System.Drawing.Point(204, 22)
        Me.CheckBox2.Name = "CheckBox2"
        Me.CheckBox2.Size = New System.Drawing.Size(93, 20)
        Me.CheckBox2.TabIndex = 1
        Me.CheckBox2.Text = "Terminado"
        Me.CheckBox2.UseVisualStyleBackColor = False
        '
        'ChkPreTall
        '
        Me.ChkPreTall.AutoSize = True
        Me.ChkPreTall.BackColor = System.Drawing.SystemColors.Control
        Me.ChkPreTall.Checked = True
        Me.ChkPreTall.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ChkPreTall.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ChkPreTall.ForeColor = System.Drawing.Color.Black
        Me.ChkPreTall.Location = New System.Drawing.Point(6, 22)
        Me.ChkPreTall.Name = "ChkPreTall"
        Me.ChkPreTall.Size = New System.Drawing.Size(86, 20)
        Me.ChkPreTall.TabIndex = 0
        Me.ChkPreTall.Text = "Por Entrar"
        Me.ChkPreTall.UseVisualStyleBackColor = False
        '
        'CheckBox4
        '
        Me.CheckBox4.AutoSize = True
        Me.CheckBox4.BackColor = System.Drawing.SystemColors.Control
        Me.CheckBox4.Checked = True
        Me.CheckBox4.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox4.ForeColor = System.Drawing.Color.Black
        Me.CheckBox4.Location = New System.Drawing.Point(109, 22)
        Me.CheckBox4.Name = "CheckBox4"
        Me.CheckBox4.Size = New System.Drawing.Size(78, 20)
        Me.CheckBox4.TabIndex = 4
        Me.CheckBox4.Text = "En Base"
        Me.CheckBox4.UseVisualStyleBackColor = False
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(6, 60)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(1202, 258)
        Me.DataGridView1.TabIndex = 79
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.CheckBox5)
        Me.GroupBox5.Controls.Add(Me.CheckBox6)
        Me.GroupBox5.Controls.Add(Me.CheckBox7)
        Me.GroupBox5.Controls.Add(Me.CheckBox8)
        Me.GroupBox5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.GroupBox5.Location = New System.Drawing.Point(3, 3)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(449, 48)
        Me.GroupBox5.TabIndex = 79
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Estatus"
        '
        'CheckBox5
        '
        Me.CheckBox5.AutoSize = True
        Me.CheckBox5.BackColor = System.Drawing.SystemColors.Control
        Me.CheckBox5.Checked = True
        Me.CheckBox5.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBox5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox5.ForeColor = System.Drawing.Color.Black
        Me.CheckBox5.Location = New System.Drawing.Point(344, 22)
        Me.CheckBox5.Name = "CheckBox5"
        Me.CheckBox5.Size = New System.Drawing.Size(93, 20)
        Me.CheckBox5.TabIndex = 5
        Me.CheckBox5.Text = "Cancelado"
        Me.CheckBox5.UseVisualStyleBackColor = False
        '
        'CheckBox6
        '
        Me.CheckBox6.AutoSize = True
        Me.CheckBox6.BackColor = System.Drawing.SystemColors.Control
        Me.CheckBox6.Checked = True
        Me.CheckBox6.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBox6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox6.ForeColor = System.Drawing.Color.Black
        Me.CheckBox6.Location = New System.Drawing.Point(204, 22)
        Me.CheckBox6.Name = "CheckBox6"
        Me.CheckBox6.Size = New System.Drawing.Size(93, 20)
        Me.CheckBox6.TabIndex = 1
        Me.CheckBox6.Text = "Terminado"
        Me.CheckBox6.UseVisualStyleBackColor = False
        '
        'CheckBox7
        '
        Me.CheckBox7.AutoSize = True
        Me.CheckBox7.BackColor = System.Drawing.SystemColors.Control
        Me.CheckBox7.Checked = True
        Me.CheckBox7.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBox7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox7.ForeColor = System.Drawing.Color.Black
        Me.CheckBox7.Location = New System.Drawing.Point(6, 22)
        Me.CheckBox7.Name = "CheckBox7"
        Me.CheckBox7.Size = New System.Drawing.Size(86, 20)
        Me.CheckBox7.TabIndex = 0
        Me.CheckBox7.Text = "Por Entrar"
        Me.CheckBox7.UseVisualStyleBackColor = False
        '
        'CheckBox8
        '
        Me.CheckBox8.AutoSize = True
        Me.CheckBox8.BackColor = System.Drawing.SystemColors.Control
        Me.CheckBox8.Checked = True
        Me.CheckBox8.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBox8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox8.ForeColor = System.Drawing.Color.Black
        Me.CheckBox8.Location = New System.Drawing.Point(109, 22)
        Me.CheckBox8.Name = "CheckBox8"
        Me.CheckBox8.Size = New System.Drawing.Size(78, 20)
        Me.CheckBox8.TabIndex = 4
        Me.CheckBox8.Text = "En Base"
        Me.CheckBox8.UseVisualStyleBackColor = False
        '
        'DataGridView2
        '
        Me.DataGridView2.AllowUserToAddRows = False
        Me.DataGridView2.AllowUserToDeleteRows = False
        Me.DataGridView2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView2.Location = New System.Drawing.Point(3, 57)
        Me.DataGridView2.Name = "DataGridView2"
        Me.DataGridView2.Size = New System.Drawing.Size(1202, 258)
        Me.DataGridView2.TabIndex = 80
        '
        'DataGridView3
        '
        Me.DataGridView3.AllowUserToAddRows = False
        Me.DataGridView3.AllowUserToDeleteRows = False
        Me.DataGridView3.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView3.Location = New System.Drawing.Point(3, 57)
        Me.DataGridView3.Name = "DataGridView3"
        Me.DataGridView3.Size = New System.Drawing.Size(1202, 258)
        Me.DataGridView3.TabIndex = 82
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.CheckBox9)
        Me.GroupBox6.Controls.Add(Me.CheckBox10)
        Me.GroupBox6.Controls.Add(Me.CheckBox11)
        Me.GroupBox6.Controls.Add(Me.CheckBox12)
        Me.GroupBox6.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox6.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.GroupBox6.Location = New System.Drawing.Point(3, 3)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(449, 48)
        Me.GroupBox6.TabIndex = 81
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Estatus"
        '
        'CheckBox9
        '
        Me.CheckBox9.AutoSize = True
        Me.CheckBox9.BackColor = System.Drawing.SystemColors.Control
        Me.CheckBox9.Checked = True
        Me.CheckBox9.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBox9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox9.ForeColor = System.Drawing.Color.Black
        Me.CheckBox9.Location = New System.Drawing.Point(344, 22)
        Me.CheckBox9.Name = "CheckBox9"
        Me.CheckBox9.Size = New System.Drawing.Size(93, 20)
        Me.CheckBox9.TabIndex = 5
        Me.CheckBox9.Text = "Cancelado"
        Me.CheckBox9.UseVisualStyleBackColor = False
        '
        'CheckBox10
        '
        Me.CheckBox10.AutoSize = True
        Me.CheckBox10.BackColor = System.Drawing.SystemColors.Control
        Me.CheckBox10.Checked = True
        Me.CheckBox10.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBox10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox10.ForeColor = System.Drawing.Color.Black
        Me.CheckBox10.Location = New System.Drawing.Point(204, 22)
        Me.CheckBox10.Name = "CheckBox10"
        Me.CheckBox10.Size = New System.Drawing.Size(93, 20)
        Me.CheckBox10.TabIndex = 1
        Me.CheckBox10.Text = "Terminado"
        Me.CheckBox10.UseVisualStyleBackColor = False
        '
        'CheckBox11
        '
        Me.CheckBox11.AutoSize = True
        Me.CheckBox11.BackColor = System.Drawing.SystemColors.Control
        Me.CheckBox11.Checked = True
        Me.CheckBox11.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBox11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox11.ForeColor = System.Drawing.Color.Black
        Me.CheckBox11.Location = New System.Drawing.Point(6, 22)
        Me.CheckBox11.Name = "CheckBox11"
        Me.CheckBox11.Size = New System.Drawing.Size(86, 20)
        Me.CheckBox11.TabIndex = 0
        Me.CheckBox11.Text = "Por Entrar"
        Me.CheckBox11.UseVisualStyleBackColor = False
        '
        'CheckBox12
        '
        Me.CheckBox12.AutoSize = True
        Me.CheckBox12.BackColor = System.Drawing.SystemColors.Control
        Me.CheckBox12.Checked = True
        Me.CheckBox12.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBox12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox12.ForeColor = System.Drawing.Color.Black
        Me.CheckBox12.Location = New System.Drawing.Point(109, 22)
        Me.CheckBox12.Name = "CheckBox12"
        Me.CheckBox12.Size = New System.Drawing.Size(78, 20)
        Me.CheckBox12.TabIndex = 4
        Me.CheckBox12.Text = "En Base"
        Me.CheckBox12.UseVisualStyleBackColor = False
        '
        'FMonitorGuardias
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1243, 578)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.tabMonitor)
        Me.Controls.Add(Me.LTiempo)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.MeStatus1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimizeBox = False
        Me.Name = "FMonitorGuardias"
        Me.Text = "Monitor de Unidades de Transporte"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.tabMonitor.ResumeLayout(False)
        Me.tbMonEntSal.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        CType(Me.grdMonitorESBit, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.grdMonitorES, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gpoEntSalEst.ResumeLayout(False)
        Me.gpoEntSalEst.PerformLayout()
        Me.tbMonComb.ResumeLayout(False)
        CType(Me.grdMonitorCom, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbMonTaller.ResumeLayout(False)
        Me.tbMonLlantas.ResumeLayout(False)
        Me.tbMonLavadero.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        CType(Me.grdMonitorDet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gpoComb.ResumeLayout(False)
        Me.gpoComb.PerformLayout()
        Me.gpoTaller.ResumeLayout(False)
        Me.gpoTaller.PerformLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridView3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    'Sub New(ByVal vTag As String)
    '    InitializeComponent()
    '    _Tag = vTag
    'End Sub

    Public Sub New(con As String)
        Me.con = con
        BD = New CapaDatos.UtilSQL(con, "Juan")
        InitializeComponent()
    End Sub

    'Private Sub btnMnuSalir_Click(sender As Object, e As EventArgs)

    'End Sub




    Private Sub FMonitorTaller_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'grdMonitor.MaximumSize.Width = Me.Width - 120

        AutoCompletarEmpresas(cmbEmpresa, True)


        ChkCap.BackColor = _ColorCapES
        ChkEnt.BackColor = _ColorEntES
        ChkSal.BackColor = _ColorSalES
        ChkCan.BackColor = _ColorCanES

        Me.KeyPreview = True


        CargaGrid(tabMonitor.SelectedTab.Name)
        grdMonitorES.DefaultCellStyle.ForeColor = Color.Black
        Timer1.Enabled = True
        Status("Monitor de Servicios Iniciado", Me)

    End Sub

    Private Sub CargaGrid(ByVal Opcion As String)
        Dim SWhere As String = "", sStatusFiltro As String = ""
        Dim sOrderby As String = ""
        Dim objsql As New ToolSQLs

        Dim strsql2 As String = ""
        Dim SWhere2 As String = ""
        Dim sOrderby2 As String = ""

        Dim filtroEmpresa As String = ""
        Dim FiltroTipoServicio As String = ""

        Select Case Opcion
            Case tbMonEntSal.Name
                'GUARDIAS ENTRADAS / SALIDAS
                If ChkCap.Checked Then
                    SWhere += "'CAP',"
                End If
                If ChkSal.Checked Then
                    SWhere += "'SAL',"
                    SWhere2 += "SAL,"
                End If

                If ChkCan.Checked Then
                    SWhere += "'CAN',"
                End If
                If ChkEnt.Checked Then
                    SWhere += "'ENT',"
                    SWhere2 += "ENT"
                End If

                StrSql = objsql.MonitorEntSal()

                strsql2 = objsql.MonitorEntSalBitacora()

                If cmbEmpresa.Text = "TODAS" Then
                    filtroEmpresa = ""
                Else
                    filtroEmpresa = cmbEmpresa.SelectedValue

                End If

            Case tbMonComb.Name
                'COMBUSTIBLE
                If ChkCapCom.Checked Then
                    SWhere += "'PRE',"
                End If
                If ChkEntCom.Checked Then
                    SWhere += "'CAP',"
                End If

                If ChkTerCom.Checked Then
                    SWhere += "'TER',"
                End If
                If ChkCanCom.Checked Then
                    SWhere += "'CAN',"
                End If

                StrSql = objsql.MonitorServicios(0)
                If cmbEmpresa.Text = "TODAS" Then
                    filtroEmpresa = ""
                Else
                    filtroEmpresa = cmbEmpresa.SelectedValue

                End If

                FiltroTipoServicio = " and os.idTipOrdServ = 1 "

        End Select

        sStatusFiltro = SWhere
        TiempoUsado.StartNew()
        If SWhere.Trim <> "" Then
            If ChkHoy.Checked Then
                sStatusFiltro += " y SOLO FECHA de Hoy: " & Now.ToShortDateString
                ' MA.ESTATUS
                Select Case Opcion
                    Case tbMonEntSal.Name
                        SWhere = " WHERE MA.ESTATUS IN(" & Mid(SWhere, 1, SWhere.Length - 1) & ") AND CAST(ma.FechaCreacion AS DATE) = '" & FormatFecHora(Now.Date, True, False) & "'"

                        If SWhere2 = "SAL," Then
                            SWhere2 = " WHERE  isnull(BES.fechaSalida,'') <>  '' AND CAST(BES.fechaEntrada AS DATE) = '" & FormatFecHora(Now.Date, True, False) & "'"
                        ElseIf SWhere2 = "ENT" Then
                            SWhere2 = " WHERE  isnull(BES.fechaSalida,'') =  '' AND CAST(BES.fechaEntrada AS DATE) = '" & FormatFecHora(Now.Date, True, False) & "'"
                        Else
                            SWhere2 = " WHERE CAST(BES.fechaEntrada AS DATE) = '" & FormatFecHora(Now.Date, True, False) & "'"
                        End If

                        'empresa
                        If filtroEmpresa <> "" Then
                            SWhere += " AND em.idEmpresa = " & filtroEmpresa

                            If SWhere2 <> "" Then
                                'UT.idEmpresa
                                SWhere2 += " AND UT.idEmpresa = " & filtroEmpresa
                            Else
                                SWhere2 = " WHERE UT.idEmpresa = " & filtroEmpresa
                            End If
                        End If

                    Case tbMonComb.Name

                        SWhere = " WHERE os.Estatus IN(" & Mid(SWhere, 1, SWhere.Length - 1) & ") AND CAST(os.fechaCaptura AS DATE) = '" & FormatFecHora(Now.Date, True, False) & "'"
                        'empresa
                        If filtroEmpresa <> "" Then
                            SWhere += " AND os.IdEmpresa = " & filtroEmpresa
                        End If

                End Select



            Else
                Select Case Opcion
                    Case tbMonEntSal.Name
                        SWhere = " WHERE  MA.ESTATUS IN(" & Mid(SWhere, 1, SWhere.Length - 1) & ") "



                        If SWhere2 = "SAL," Then
                            SWhere2 = " WHERE  isnull(BES.fechaSalida,'') <>  '' "
                        ElseIf SWhere2 = "ENT" Then
                            SWhere2 = " WHERE  isnull(BES.fechaSalida,'') =  '' "
                        Else
                            SWhere2 = ""
                        End If

                        'empresa
                        If filtroEmpresa <> "" Then
                            SWhere += " AND em.idEmpresa = " & filtroEmpresa

                            If SWhere2 <> "" Then
                                'UT.idEmpresa
                                SWhere2 += " AND UT.idEmpresa = " & filtroEmpresa
                            Else
                                SWhere2 = " WHERE UT.idEmpresa = " & filtroEmpresa
                            End If
                        End If

                    Case tbMonComb.Name
                        SWhere = " WHERE os.Estatus IN(" & Mid(SWhere, 1, SWhere.Length - 1) & ") "
                        'empresa
                        If filtroEmpresa <> "" Then
                            SWhere += " AND os.IdEmpresa = " & filtroEmpresa
                        End If
                End Select

            End If
        ElseIf ChkHoy.Checked Then
            sStatusFiltro += " y SOLO FECHA de Hoy: " & Now.ToShortDateString
            Select Case Opcion
                Case tbMonEntSal.Name
                    SWhere = " WHERE CAST(ma.FechaCreacion AS DATE) = '" & FormatFecHora(Now.Date, True, False) & "'"
                Case tbMonComb.Name
                    SWhere = " WHERE CAST(os.fechaCaptura AS DATE) = '" & FormatFecHora(Now.Date, True, False) & "'"
            End Select

        End If
        If ChkTmpReal.Checked = False Then
            Select Case Opcion
                Case tbMonEntSal.Name
                    SWhere += "AND CAST(ma.FechaCreacion AS Date) = '" & FormatFecHora(Cale1.Value, True, False) & "'"
                Case tbMonComb.Name
                    SWhere = " WHERE CAST(os.fechaCaptura AS DATE) = '" & FormatFecHora(Now.Date, True, False) & "'"
            End Select


            sStatusFiltro += " y Fecha Captura = " & Cale1.Value.ToString("dd/MMMM/yyyy")
        End If
        Select Case Opcion
            Case tbMonEntSal.Name
                sOrderby = " ORDER BY MA.FechaCreacion DESC"

                sOrderby2 = " ORDER BY bes.fechaEntrada DESC"
            Case tbMonComb.Name
                sOrderby = " ORDER BY os.fechaCaptura DESC"
        End Select

        Dim dataSource As New BindingSource(BD.ExecuteReturn(StrSql & SWhere & FiltroTipoServicio & sOrderby), Nothing)

        Select Case Opcion
            Case tbMonEntSal.Name
                Dim dataSource2 As New BindingSource(BD.ExecuteReturn(strsql2 & SWhere2 & sOrderby2), Nothing)
                grdMonitorESBit.DataSource = dataSource2

                grdMonitorES.DataSource = dataSource

                Status(grdMonitorES.Rows.Count & " Servicios Con el Filtro: " & sStatusFiltro & TiempoUsado.GetTiempo, Me)
                With grdMonitorES
                    .Columns("Fecha").Width = 120
                    .Columns("TiempoTrasncurrido").Width = 120
                    .Columns("FOLIO").Width = 50
                    .Columns("Estatus").Width = 100
                    .Columns("Camion").Width = 50
                    .Columns("Remolque1").Width = 65
                    .Columns("Dolly").Width = 50
                    .Columns("Remolque2").Width = 65
                    .Columns("Chofer").Width = 150
                    .Columns("Empresa").Width = 120
                    .Columns("FechaEntrada").Width = 120
                    .Columns("FechaSalida").Width = 120
                    .Columns("servicios").Width = 120

                    .Columns("idChofer").Visible = False
                    .Columns("FechaCancela").Visible = False
                    .Columns("idEstatus").Visible = False

                End With

                With grdMonitorESBit
                    .Columns("Fecha Entrada").Width = 120
                    .Columns("Fecha Salida").Width = 120
                    .Columns("TiempoTrasncurrido").Width = 120
                    .Columns("Folio Bitacora").Width = 50
                    .Columns("Estatus").Width = 100
                    .Columns("Camion").Width = 50
                    .Columns("Remolque1").Width = 65
                    .Columns("Dolly").Width = 50
                    .Columns("Remolque2").Width = 65
                    .Columns("Chofer").Width = 150
                    .Columns("Empresa").Width = 120
                    .Columns("Usuario").Width = 150

                    .Columns("idOperador").Visible = False

                End With

            Case tbMonComb.Name
                grdMonitorCom.DataSource = dataSource

                Status(grdMonitorCom.Rows.Count & " Servicios Con el Filtro: " & sStatusFiltro & TiempoUsado.GetTiempo, Me)

                With grdMonitorCom
                    '
                    .Columns("Fecha").Width = 120
                    .Columns("TiempoTrasncurrido").Width = 120
                    .Columns("Folio").Width = 50
                    .Columns("OrdenServicio").Width = 50
                    .Columns("Empresa").Width = 120
                    .Columns("UniTrans").Width = 50
                    .Columns("clasificacion").Width = 50
                    .Columns("Estatus").Width = 50

                    .Columns("NomTipOrdServ").Width = 70
                    .Columns("Fecha Carga").Width = 120
                    .Columns("despachador").Width = 150
                    .Columns("estatus2").Width = 100

                    .Columns("FechaRecepcion").Visible = False
                    .Columns("UsuarioRecepcion").Visible = False
                    .Columns("FechaEntregado").Visible = False
                    .Columns("idEmpleadoRecibe").Visible = False
                    .Columns("idDespachador").Visible = False
                    .Columns("idCargaCombustible").Visible = False
                    '
                    '

                End With
        End Select

        'Grid1.DataSource = BD.ExecuteReturn(sSqlServ & SWhere & "ORDER BY Serv.FechaSolicita DESC")


        'D E T A L L E
        'ORDENES DE TRABAJO
        'If grdMonitorES.Rows.Count > 0 Then
        '    StrSql = "SELECT ot.idOrdenSer AS NoServicio, " &
        '    "ot.idOrdenTrabajo AS [No. Trabajo], " &
        '    "ISNULL(ot.NotaRecepcion,'') AS Descripcion, " &
        '    "ISNULL(div.NomDivision,'') AS Division, " &
        '    "ISNULL(CASE WHEN PerRes.Nombre <> '' THEN PerRes.Nombre + ' ' + PerRes.ApellidoPat + ' ' + PerRes.ApellidoMat ELSE PerRes.NombreCompleto END,'') AS Responsable, " &
        '    "ISNULL(CASE WHEN PerAyu1.Nombre <> '' THEN PerAyu1.Nombre + ' ' + PerAyu1.ApellidoPat + ' ' + PerAyu1.ApellidoMat ELSE PerAyu1.NombreCompleto END,'') AS Ayudante1, " &
        '    "ISNULL(CASE WHEN PerAyu2.Nombre <> '' THEN PerAyu2.Nombre + ' ' + PerAyu2.ApellidoPat + ' ' + PerAyu2.ApellidoMat ELSE PerAyu2.NombreCompleto END,'') AS Ayudante2, " &
        '    "ISNULL(det.Estatus,'') AS Estatus, " &
        '    "det.FechaAsig AS Asignado, " &
        '    "det.FechaDiag AS Diagnostico , " &
        '    "det.FechaTerminado AS Terminado, " &
        '    "ISNULL(det.DuracionActHr,0) AS DuracionHR, " &
        '    "'' AS TiempoTrasncurrido " &
        '    "FROM dbo.DetRecepcionOS ot " &
        '    "left JOIN dbo.DetOrdenServicio det ON ot.idOrdenTrabajo = det.idOrdenTrabajo " &
        '    "left JOIN dbo.CatPersonal PerRes ON ot.idPersonalResp = PerRes.idPersonal " &
        '    "left JOIN dbo.CatPersonal PerAyu1 ON ot.idPersonalAyu1 = PerAyu1.idPersonal " &
        '    "left JOIN dbo.CatPersonal PerAyu2 ON ot.idPersonalAyu2 = PerAyu2.idPersonal " &
        '    "LEFT JOIN dbo.CatDivision div ON ot.idDivision = div.idDivision"



        '    '            "ISNULL(CAST(ot. idPersonalResp AS VARCHAR(10)) + ' - ' + CASE WHEN PerRes.Nombre <> '' THEN PerRes.Nombre + ' ' + PerRes.ApellidoPat + ' ' + PerRes.ApellidoMat ELSE PerRes.NombreCompleto END,'') AS Responsable, " & _
        '    '"ISNULL(CAST(ot. idPersonalAyu1 AS VARCHAR(10)) + ' - ' + CASE WHEN PerAyu1.Nombre <> '' THEN PerAyu1.Nombre + ' ' + PerAyu1.ApellidoPat + ' ' + PerAyu1.ApellidoMat ELSE PerAyu1.NombreCompleto END,'') AS Ayudante1, " & _
        '    '"ISNULL(CAST(ot. idPersonalAyu2 AS VARCHAR(10)) + ' - ' + CASE WHEN PerAyu2.Nombre <> '' THEN PerAyu2.Nombre + ' ' + PerAyu2.ApellidoPat + ' ' + PerAyu2.ApellidoMat ELSE PerAyu2.NombreCompleto END,'') AS Ayudante2, " & _

        '    'Dim dataSource2 As New BindingSource(BD.ExecuteReturn(StrSql), Nothing)
        '    'MyTablaDet = dataSource2

        '    MyTablaDet.Rows.Clear()
        '    MyTablaDet = BD.ExecuteReturn(StrSql)

        '    grdMonitorDet.DataSource = MyTablaDet

        '    With grdMonitorDet
        '        .Columns("NoServicio").Width = 100
        '        .Columns("No. Trabajo").Width = 100
        '        .Columns("Descripcion").Width = 250
        '        .Columns("Division").Width = 150
        '        .Columns("Responsable").Width = 200
        '        .Columns("Ayudante1").Width = 150
        '        .Columns("Ayudante2").Width = 150
        '        .Columns("Estatus").Width = 50
        '        .Columns("Asignado").Width = 120
        '        .Columns("Diagnostico").Width = 120
        '        .Columns("Terminado").Width = 120
        '        .Columns("DuracionHR").Width = 80
        '        .Columns("TiempoTrasncurrido").Width = 180

        '    End With

        '    IdOrdenServ = grdMonitorES.Item("No. Servicio", 0).Value
        '    CargaOrdenesTrab(IdOrdenServ)
        'Else
        '    MyTablaDet.Rows.Clear()
        'End If

    End Sub
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Static TmpActu As Integer = 0
        LTiempo.Text = _TiempoActu - TmpActu
        If TmpActu >= _TiempoActu Then
            TmpActu = 0
            Timer1.Enabled = False
            Dim RIndx As Integer, CIndx As Integer
            Try
                RIndx = grdMonitorES.CurrentCell.RowIndex
                CIndx = grdMonitorES.CurrentCell.ColumnIndex
            Catch ex As Exception
                RIndx = -1
                CIndx = -1
            End Try
            CargaGrid(tabMonitor.SelectedTab.Name)
            If RIndx > 0 And CIndx > 0 And RIndx < grdMonitorES.RowCount Then
                Me.grdMonitorES.CurrentCell = Me.grdMonitorES.Rows(RIndx).Cells(CIndx)
            End If
            Timer1.Enabled = True
        Else
            TmpActu += 1
        End If

        Select Case tabMonitor.SelectedTab.Name
            Case tbMonEntSal.Name
                For i As Integer = 0 To grdMonitorES.Rows.Count - 1
                    If UCase(grdMonitorES.Rows(i).Cells("idEstatus").Value) = UCase("sal") Then
                        grdMonitorES.Rows(i).Cells("TiempoTrasncurrido").Value = IntervaloHoras(CDate(grdMonitorES.Rows(i).Cells("FechaSalida").Value),
                                                                                Now)
                    ElseIf UCase(grdMonitorES.Rows(i).Cells("idEstatus").Value) = UCase("Ent") Then
                        grdMonitorES.Rows(i).Cells("TiempoTrasncurrido").Value = IntervaloHoras(CDate(grdMonitorES.Rows(i).Cells("FechaEntrada").Value),
                                                                                Now)
                    ElseIf UCase(grdMonitorES.Rows(i).Cells("idEstatus").Value) = UCase("Cap") Then
                        grdMonitorES.Rows(i).Cells("TiempoTrasncurrido").Value = IntervaloHoras(CDate(grdMonitorES.Rows(i).Cells("Fecha").Value),
                                                                                Now)
                    ElseIf UCase(grdMonitorES.Rows(i).Cells("idEstatus").Value) = UCase("can") Then
                        grdMonitorES.Rows(i).Cells("TiempoTrasncurrido").Value = IntervaloHoras(CDate(grdMonitorES.Rows(i).Cells("FechaCancela").Value),
                                                                            Now)
                    End If

                    'For j As Integer = 0 To grdMonitorESBit.Rows.Count - 1
                    '    If UCase(grdMonitorES.Rows(i).Cells("idEstatus").Value) = UCase("sal") Then
                    '        grdMonitorES.Rows(i).Cells("TiempoTrasncurrido").Value = IntervaloHoras(CDate(grdMonitorES.Rows(i).Cells("FechaSalida").Value),
                    '                                                                Now)
                    '    ElseIf UCase(grdMonitorES.Rows(i).Cells("idEstatus").Value) = UCase("Ent") Then
                    '        grdMonitorES.Rows(i).Cells("TiempoTrasncurrido").Value = IntervaloHoras(CDate(grdMonitorES.Rows(i).Cells("FechaEntrada").Value),
                    '                                                                Now)
                    '    End If

                    'Next
                Next

                'For i As Integer = 0 To grdMonitorESBit.Rows.Count - 1
                '    If UCase(grdMonitorESBit.Rows(i).Cells("idEstatus").Value) = UCase("sal") Then
                '        grdMonitorESBit.Rows(i).Cells("TiempoTrasncurrido").Value = IntervaloHoras(CDate(grdMonitorESBit.Rows(i).Cells("FechaSalida").Value),
                '                                                                Now)
                '    ElseIf UCase(grdMonitorESBit.Rows(i).Cells("idEstatus").Value) = UCase("Ent") Then
                '        grdMonitorESBit.Rows(i).Cells("TiempoTrasncurrido").Value = IntervaloHoras(CDate(grdMonitorESBit.Rows(i).Cells("FechaEntrada").Value),
                '                                                                Now)
                '    ElseIf UCase(grdMonitorES.Rows(i).Cells("idEstatus").Value) = UCase("Cap") Then
                '        grdMonitorES.Rows(i).Cells("TiempoTrasncurrido").Value = IntervaloHoras(CDate(grdMonitorES.Rows(i).Cells("Fecha").Value),
                '                                                                Now)
                '    ElseIf UCase(grdMonitorES.Rows(i).Cells("idEstatus").Value) = UCase("can") Then
                '        grdMonitorES.Rows(i).Cells("TiempoTrasncurrido").Value = IntervaloHoras(CDate(grdMonitorES.Rows(i).Cells("FechaCancela").Value),
                '                                                            Now)


                '    End If
                'Next



        End Select




        'ORDENES DE TRABAJO
        For i As Integer = 0 To grdMonitorDet.Rows.Count - 1
            If grdMonitorDet.Rows(i).Cells("Estatus").Value = "DIAG" Then
                grdMonitorDet.Rows(i).Cells("TiempoTrasncurrido").Value = IntervaloHoras(CDate(grdMonitorDet.Rows(i).Cells("Diagnostico").Value),
                                                                                Now)
            ElseIf grdMonitorDet.Rows(i).Cells("Estatus").Value = "ASIG" Then
                grdMonitorDet.Rows(i).Cells("TiempoTrasncurrido").Value = IntervaloHoras(CDate(grdMonitorDet.Rows(i).Cells("Asignado").Value),
                                                                                Now)
            ElseIf grdMonitorDet.Rows(i).Cells("Estatus").Value = "TER" Then
                Try
                    grdMonitorDet.Rows(i).Cells("TiempoTrasncurrido").Value = IntervaloHoras(CDate(grdMonitorDet.Rows(i).Cells("Diagnostico").Value),
                                                                                CDate(grdMonitorDet.Rows(i).Cells("Terminado").Value))
                Catch ex As Exception
                End Try
            End If
        Next
    End Sub
    Private Sub FMonitorServicios_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        FAct = True
    End Sub

    Private Sub FMonitorServicios_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Timer1.Enabled = False
    End Sub

    Private Sub FMonitorServicios_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = 27 Then
            Me.Close()
            e.Handled = True
        End If
    End Sub



    Private Sub grdMonitorES_CellFormatting(sender As Object, e As DataGridViewCellFormattingEventArgs) Handles grdMonitorES.CellFormatting
        With grdMonitorES.Rows(e.RowIndex).Cells(e.ColumnIndex)
            If IsDate(.Value) AndAlso CDate(.Value).ToString("yyyy/MM/dd") <> "0001/01/01" Then
                If CDate(.Value).Date = Now.Date Then
                    .ToolTipText = "HOY - " & FormatDateTime(.Value, DateFormat.LongDate)
                ElseIf CDate(.Value).Date = DateAdd(DateInterval.Day, -1, Now).Date Then
                    .ToolTipText = "AYER - " & FormatDateTime(.Value, DateFormat.LongDate)
                ElseIf CDate(.Value).Date = DateAdd(DateInterval.Day, 1, Now).Date Then
                    .ToolTipText = "MA�ANA - " & FormatDateTime(.Value, DateFormat.LongDate)
                Else
                    .ToolTipText = FormatDateTime(.Value, DateFormat.LongDate) & vbNewLine &
                        IntervaloTiempoCad(.Value)
                End If
            Else
                .ToolTipText = .Value & ""
            End If
        End With
    End Sub



    Private Sub TSActualizar_Click(sender As Object, e As EventArgs) Handles TSActualizar.Click
        CargaGrid(tabMonitor.SelectedTab.Name)
    End Sub

    Private Sub grdMonitorES_Click(sender As Object, e As EventArgs) Handles grdMonitorES.Click
        'PAra los que tienen detalle
        'IdOrdenServ = grdMonitorES.Item("No. Servicio", grdMonitorES.CurrentCell.RowIndex).Value
        'CargaOrdenesTrab(IdOrdenServ)
    End Sub

    Private Sub grdMonitorES_RowPostPaint(sender As Object, e As DataGridViewRowPostPaintEventArgs) Handles grdMonitorES.RowPostPaint

        With grdMonitorES.Rows(e.RowIndex)
            .DefaultCellStyle.ForeColor = Color.Black
            If grdMonitorES.Columns.Contains("idEstatus") AndAlso .Cells("idEstatus").Value = "CAP" Then
                .DefaultCellStyle.BackColor = _ColorCapES
            ElseIf grdMonitorES.Columns.Contains("idEstatus") AndAlso .Cells("idEstatus").Value = "ENT" Then
                .DefaultCellStyle.BackColor = _ColorEntES
            ElseIf grdMonitorES.Columns.Contains("idEstatus") AndAlso .Cells("idEstatus").Value = "SAL" Then
                .DefaultCellStyle.BackColor = _ColorSalES
            ElseIf grdMonitorES.Columns.Contains("idEstatus") AndAlso .Cells("idEstatus").Value = "CAN" Then
                .DefaultCellStyle.BackColor = _ColorCanES
            End If
        End With
    End Sub

    'Private Sub ChkCap_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ChkCap.CheckedChanged,
    'ChkAsig.CheckedChanged, ChkTer.CheckedChanged, ChkCan.CheckedChanged, ChkDiag.CheckedChanged, ChkEnt.CheckedChanged,
    'ChkRecep.CheckedChanged

    Private Sub ChkCap_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ChkCap.CheckedChanged,
    ChkCan.CheckedChanged, ChkEnt.CheckedChanged, ChkSal.CheckedChanged

        If FAct = False Then Exit Sub
        If TryCast(sender, CheckBox).Checked = False Then
            Dim Bnd As Boolean = False
            For Each Cnt As Control In gpoEntSalEst.Controls
                If TypeOf (Cnt) Is CheckBox Then
                    If TryCast(Cnt, CheckBox).Checked Then
                        Bnd = True
                        Exit For
                    End If
                End If
            Next
            If Bnd = False Then
                TryCast(sender, CheckBox).Checked = True
                MessageBox.Show("Se requiere que almenos un Elemento este seleccionado", "Se requiere seleccionar almenos UNO!!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
        End If
    End Sub
    Private Sub ChkTmpReal_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ChkTmpReal.CheckedChanged
        If ChkTmpReal.Checked Then
            Timer1.Enabled = True
            Cale1.Enabled = False
            btnBuscar.Enabled = False
            LTiempo.ForeColor = Color.Blue
            ChkHoy.Enabled = True
        Else
            Timer1.Enabled = False
            Cale1.Enabled = True
            btnBuscar.Enabled = True
            LTiempo.ForeColor = Color.Red
            ChkHoy.Checked = False
            ChkHoy.Enabled = False
        End If
    End Sub


    Private Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        CargaGrid(tabMonitor.SelectedTab.Name)
    End Sub

    Private Sub Cale1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cale1.ValueChanged
        CargaGrid(tabMonitor.SelectedTab.Name)
    End Sub

    Private Sub TSFiltroAvanzado_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSFiltroAvanzado.Click
        If GroupBox2.Enabled Then
            GroupBox2.Enabled = False
            TSActualizar.Enabled = False
            ChkTmpReal.Checked = False

            For i As Integer = 0 To grdMonitorES.Columns.Count - 1
                grdMonitorES.Columns(i).HeaderCell = New CellHeaderGrid(grdMonitorES.Columns(i).HeaderCell)
                TryCast(grdMonitorES.Columns(i).HeaderCell, CellHeaderGrid).TipoDato = CellHeaderGrid.EnTipoDato.Automatico
                TryCast(grdMonitorES.Columns(i).HeaderCell, CellHeaderGrid).MuestraPopUp = True
            Next i
        Else
            GroupBox2.Enabled = True
            TSActualizar.Enabled = True
            ChkTmpReal.Checked = True
            For i As Integer = 0 To grdMonitorES.Columns.Count - 1
                TryCast(grdMonitorES.Columns(i).HeaderCell, CellHeaderGrid).MuestraPopUp = False
            Next i
        End If

    End Sub

    Private Sub ToolStripButton1_Click(sender As Object, e As EventArgs) Handles ToolStripButton1.Click
        Me.Close()
    End Sub





    'Private Sub MRecepServ_Click(sender As Object, e As EventArgs)
    '    'MENU_MOVIMIENTOS_RECEPCION()

    '    FMenu.OpenMainWindow("MENU_MOVIMIENTOS_RECEPCION", True, grdMonitorES.Rows(grdMonitorES.CurrentRow.Index).Cells("No. Servicio").Value)

    'End Sub

    'Private Sub MAsigServ_Click(sender As Object, e As EventArgs)
    '    FMenu.OpenMainWindow("MENU_MOVIMIENTOS_DIAGNOSTICO", True, grdMonitorES.Rows(grdMonitorES.CurrentRow.Index).Cells("No. Servicio").Value)
    '    '
    'End Sub

    Private Sub CargaOrdenesTrab(ByVal vidOrdenSer As Integer)
        Try
            MyTablaDet.DefaultView.RowFilter = Nothing
            MyTablaDet.DefaultView.RowFilter = "NoServicio = " & vidOrdenSer
            If MyTablaDet.DefaultView.Count > 0 Then

            End If
            grdMonitorDet.PerformLayout()
            'MyTablaDet.DefaultView.RowFilter = Nothing




            'MyTablaHisOT.Rows.Clear()
            ''grdOrdenServHis.DataSource = DetOS.CargaOrdSerxUni(OrdenServ.idUnidadTrans)
            'MyTablaHisOT = DetOS.CargaOrdTrabxUni(vidUnidadTrans, vidOrdenSer)
            'grdOrdenTrabajoHis.DataSource = Nothing
            'grdOrdenTrabajoHis.Rows.Clear()

            'If MyTablaHisOT.Rows.Count > 0 Then
            '    For i = 0 To MyTablaHisOT.DefaultView.Count - 1
            '        grdOrdenTrabajoHis.Rows.Add()
            '        'grdOrdenServHis.Item("hosIdServicio", i).Value = Trim(MyTabla.DefaultView.Item(i).Item("idOrdenSer").ToString)
            '        grdOrdenTrabajoHis.Item("hotIdOrdenSer", i).Value = MyTablaHisOT.DefaultView.Item(i).Item("idOrdenSer")
            '        grdOrdenTrabajoHis.Item("hotidOrdenTrabajo", i).Value = MyTablaHisOT.DefaultView.Item(i).Item("idOrdenTrabajo")
            '        grdOrdenTrabajoHis.Item("hotFechaFin", i).Value = MyTablaHisOT.DefaultView.Item(i).Item("FechaTerminado")
            '        grdOrdenTrabajoHis.Item("hotAsigno", i).Value = MyTablaHisOT.DefaultView.Item(i).Item("UsuarioAsig")
            '        grdOrdenTrabajoHis.Item("hotMecanico", i).Value = MyTablaHisOT.DefaultView.Item(i).Item("idPersonalResp")
            '        grdOrdenTrabajoHis.Item("hotDuracion", i).Value = MyTablaHisOT.DefaultView.Item(i).Item("DuracionActHr")
            '    Next
            '    grdOrdenTrabajoHis.PerformLayout()
            'End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub grdMonitorDet_RowPostPaint(sender As Object, e As DataGridViewRowPostPaintEventArgs)
        With grdMonitorDet.Rows(e.RowIndex)
            .DefaultCellStyle.ForeColor = Color.Black
            If grdMonitorDet.Columns.Contains("idEstatus") AndAlso .Cells("idEstatus").Value = "CAP" Then
                .DefaultCellStyle.BackColor = _ColorCapES
            ElseIf grdMonitorDet.Columns.Contains("idEstatus") AndAlso .Cells("idEstatus").Value = "ENT" Then
                .DefaultCellStyle.BackColor = _ColorEntES
            ElseIf grdMonitorDet.Columns.Contains("idEstatus") AndAlso .Cells("idEstatus").Value = "SAL" Then
                .DefaultCellStyle.BackColor = _ColorSalES
            ElseIf grdMonitorDet.Columns.Contains("idEstatus") AndAlso .Cells("idEstatus").Value = "CAN" Then
                .DefaultCellStyle.BackColor = _ColorCanES
            End If
        End With
    End Sub

    Private Sub grdMonitorESBit_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles grdMonitorESBit.CellContentClick

    End Sub

    Private Sub grdMonitorESBit_RowPostPaint(sender As Object, e As DataGridViewRowPostPaintEventArgs) Handles grdMonitorESBit.RowPostPaint
        With grdMonitorESBit.Rows(e.RowIndex)
            .DefaultCellStyle.ForeColor = Color.Black
            If grdMonitorESBit.Columns.Contains("Estatus") AndAlso .Cells("Estatus").Value = "ENTRADA" Then
                .DefaultCellStyle.BackColor = _ColorEntES
            ElseIf grdMonitorESBit.Columns.Contains("Estatus") AndAlso .Cells("Estatus").Value = "SALIDA" Then
                .DefaultCellStyle.BackColor = _ColorSalES
            End If

        End With
    End Sub

    Public Function AutoCompletarEmpresas(ByVal Control As ComboBox, ByVal bIncluyeTodos As Boolean) As AutoCompleteStringCollection

        Dim Coleccion As New AutoCompleteStringCollection
        Dim dt As DataTable


        Empresas = New EmpresaClass(0)
        dt = Empresas.TablaEmpresas(bIncluyeTodos)

        'Recorrer y cargar los items para el Autocompletado
        For Each row As DataRow In dt.Rows
            Coleccion.Add(Convert.ToString(row("RazonSocial")))
        Next
        'Ajustamos el control TextBox o ComboBox para recibir los datos de la siguiente manera.
        With Control
            .DataSource = dt
            .DisplayMember = "RazonSocial"
            .ValueMember = "idEmpresa"


            .AutoCompleteCustomSource = Coleccion
            .AutoCompleteMode = AutoCompleteMode.Suggest
            .AutoCompleteSource = AutoCompleteSource.CustomSource
        End With

        ' Aca le devuelvo los datos a recuperados de la Base
        Return Coleccion

    End Function
End Class
