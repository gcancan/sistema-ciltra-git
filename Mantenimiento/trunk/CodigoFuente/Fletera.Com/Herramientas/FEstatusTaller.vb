'Fecha: 15 / Febrero / 2016
'ROGER GALA PACHECO
'************************************************************************************************************************************
'*  Fecha Modificacion :   15 / Febrero / 2016                                                                                       
'*  
'************************************************************************************************************************************
Imports LinqToExcel

Public Class FEstatusTaller
    Inherits System.Windows.Forms.Form
  
    Private _Tag As String
    
    'Dim Salida As Boolean
    Dim ObjSql As New ToolSQLs
    Dim StrSql As String
    Dim indice As Integer = 0
    
    Dim ArraySql() As String

    Private _OrdenServicio As Integer

    
    Private _OrdenTrabajo As Integer
    Private _Opcion As TipoOpcionEstatus

    Private OrdenTrabajo As OrdenTrabajoClass
    Private Personal As PersonalClass
    Private PersonalPuesto As PuestosClass
    Private FamiliaOT As FamiliasClass
    Private OrdenServ As OrdenServClass
    Private TipOrd As TipoOrdenClass
    Private User As UsuarioClass
    Private DetOS As detOrdenServicioClass
    Private Emp As EmpresaClass
    Private Marca As MarcaClass
    Private UniTrans As UnidadesTranspClass
    Public WithEvents MeStatus1 As ControlStatus.MeStatus
    Friend WithEvents btnMnuOk As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuCancelar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuSalir As System.Windows.Forms.ToolStripButton
    Friend WithEvents tabPrinc As System.Windows.Forms.TabControl
    Friend WithEvents tbServicios As System.Windows.Forms.TabPage
    Friend WithEvents tbTrabajos As System.Windows.Forms.TabPage
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents gpoOT As System.Windows.Forms.GroupBox
    Friend WithEvents txtDivision As System.Windows.Forms.TextBox
    Friend WithEvents txtFamilia As System.Windows.Forms.TextBox
    Friend WithEvents txtFalla As System.Windows.Forms.TextBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents txtUsuarioAsig As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtDuracion As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtPuestoMec As System.Windows.Forms.TextBox
    Friend WithEvents txtNombreMecanico As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents dtpFechaAsigHora As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpFechaAsig As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtNomEmpresa As System.Windows.Forms.TextBox
    Friend WithEvents cmdBuscaClave As System.Windows.Forms.Button
    Friend WithEvents txtOrdenTrabajo As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents gpoSerD As System.Windows.Forms.GroupBox
    Friend WithEvents txtKilometraje As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents txtAutorizo As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txtEntrego As System.Windows.Forms.TextBox
    Friend WithEvents txtRecepciono As System.Windows.Forms.TextBox
    Friend WithEvents dtpFechaOrden As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtOrdenServ As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents dtpFechaOrdenhr As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtEstatusOrden As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtTipoOrden As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents cmdBuscaOS As System.Windows.Forms.Button
    Friend WithEvents tabOT As System.Windows.Forms.TabControl
    Friend WithEvents tbTer As System.Windows.Forms.TabPage
    Friend WithEvents tbCan As System.Windows.Forms.TabPage
    Friend WithEvents TabOS As System.Windows.Forms.TabControl
    Friend WithEvents tbSEnt As System.Windows.Forms.TabPage
    Friend WithEvents tbSCan As System.Windows.Forms.TabPage
    Friend WithEvents gpoOSEnt As System.Windows.Forms.GroupBox
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents gpoOSCan As System.Windows.Forms.GroupBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents gpoOtTer As System.Windows.Forms.GroupBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtSolucion As System.Windows.Forms.TextBox
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents txtObs As System.Windows.Forms.TextBox
    Friend WithEvents gpoOtCan As System.Windows.Forms.GroupBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents txtMarcaUni As System.Windows.Forms.TextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents txtNomUnidad As System.Windows.Forms.TextBox
    Friend WithEvents txtIdUnidad As System.Windows.Forms.TextBox
    Friend WithEvents Label21 As System.Windows.Forms.Label

    Private ULTOT As OrdenTrabajoClass
    Dim Salida As Boolean


#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents ToolStripMenu As System.Windows.Forms.ToolStrip
    Friend WithEvents txtMenuBusca As System.Windows.Forms.ToolStripTextBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FEstatusTaller))
        Me.ToolStripMenu = New System.Windows.Forms.ToolStrip()
        Me.txtMenuBusca = New System.Windows.Forms.ToolStripTextBox()
        Me.btnMnuOk = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuCancelar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuSalir = New System.Windows.Forms.ToolStripButton()
        Me.MeStatus1 = New ControlStatus.MeStatus()
        Me.tabPrinc = New System.Windows.Forms.TabControl()
        Me.tbServicios = New System.Windows.Forms.TabPage()
        Me.TabOS = New System.Windows.Forms.TabControl()
        Me.tbSEnt = New System.Windows.Forms.TabPage()
        Me.gpoOSEnt = New System.Windows.Forms.GroupBox()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.tbSCan = New System.Windows.Forms.TabPage()
        Me.gpoOSCan = New System.Windows.Forms.GroupBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.gpoSerD = New System.Windows.Forms.GroupBox()
        Me.txtMarcaUni = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.txtNomUnidad = New System.Windows.Forms.TextBox()
        Me.txtIdUnidad = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.cmdBuscaOS = New System.Windows.Forms.Button()
        Me.txtKilometraje = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txtAutorizo = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txtEntrego = New System.Windows.Forms.TextBox()
        Me.txtRecepciono = New System.Windows.Forms.TextBox()
        Me.dtpFechaOrden = New System.Windows.Forms.DateTimePicker()
        Me.txtOrdenServ = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.dtpFechaOrdenhr = New System.Windows.Forms.DateTimePicker()
        Me.txtEstatusOrden = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtTipoOrden = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.tbTrabajos = New System.Windows.Forms.TabPage()
        Me.tabOT = New System.Windows.Forms.TabControl()
        Me.tbTer = New System.Windows.Forms.TabPage()
        Me.gpoOtTer = New System.Windows.Forms.GroupBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtSolucion = New System.Windows.Forms.TextBox()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.txtObs = New System.Windows.Forms.TextBox()
        Me.tbCan = New System.Windows.Forms.TabPage()
        Me.gpoOtCan = New System.Windows.Forms.GroupBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.gpoOT = New System.Windows.Forms.GroupBox()
        Me.txtDivision = New System.Windows.Forms.TextBox()
        Me.txtFamilia = New System.Windows.Forms.TextBox()
        Me.txtFalla = New System.Windows.Forms.TextBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.txtUsuarioAsig = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtDuracion = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtPuestoMec = New System.Windows.Forms.TextBox()
        Me.txtNombreMecanico = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.dtpFechaAsigHora = New System.Windows.Forms.DateTimePicker()
        Me.dtpFechaAsig = New System.Windows.Forms.DateTimePicker()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtNomEmpresa = New System.Windows.Forms.TextBox()
        Me.cmdBuscaClave = New System.Windows.Forms.Button()
        Me.txtOrdenTrabajo = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.ToolStripMenu.SuspendLayout()
        Me.tabPrinc.SuspendLayout()
        Me.tbServicios.SuspendLayout()
        Me.TabOS.SuspendLayout()
        Me.tbSEnt.SuspendLayout()
        Me.gpoOSEnt.SuspendLayout()
        Me.tbSCan.SuspendLayout()
        Me.gpoOSCan.SuspendLayout()
        Me.gpoSerD.SuspendLayout()
        Me.tbTrabajos.SuspendLayout()
        Me.tabOT.SuspendLayout()
        Me.tbTer.SuspendLayout()
        Me.gpoOtTer.SuspendLayout()
        Me.tbCan.SuspendLayout()
        Me.gpoOtCan.SuspendLayout()
        Me.gpoOT.SuspendLayout()
        Me.SuspendLayout()
        '
        'ToolStripMenu
        '
        Me.ToolStripMenu.AutoSize = False
        Me.ToolStripMenu.BackColor = System.Drawing.Color.Silver
        Me.ToolStripMenu.Dock = System.Windows.Forms.DockStyle.None
        Me.ToolStripMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.txtMenuBusca, Me.btnMnuOk, Me.btnMnuCancelar, Me.btnMnuSalir})
        Me.ToolStripMenu.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow
        Me.ToolStripMenu.Location = New System.Drawing.Point(3, 0)
        Me.ToolStripMenu.Name = "ToolStripMenu"
        Me.ToolStripMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.ToolStripMenu.Size = New System.Drawing.Size(895, 42)
        Me.ToolStripMenu.TabIndex = 1
        Me.ToolStripMenu.Text = "ToolStrip1"
        '
        'txtMenuBusca
        '
        Me.txtMenuBusca.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.txtMenuBusca.Name = "txtMenuBusca"
        Me.txtMenuBusca.Size = New System.Drawing.Size(150, 42)
        Me.txtMenuBusca.ToolTipText = "Filtra Listado"
        '
        'btnMnuOk
        '
        Me.btnMnuOk.ForeColor = System.Drawing.Color.Red
        Me.btnMnuOk.Image = Global.Fletera.My.Resources.Resources.Save1
        Me.btnMnuOk.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuOk.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuOk.Name = "btnMnuOk"
        Me.btnMnuOk.Size = New System.Drawing.Size(56, 39)
        Me.btnMnuOk.Text = "&GRABAR"
        Me.btnMnuOk.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuCancelar
        '
        Me.btnMnuCancelar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuCancelar.Image = Global.Fletera.My.Resources.Resources.signos_f_059
        Me.btnMnuCancelar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuCancelar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuCancelar.Name = "btnMnuCancelar"
        Me.btnMnuCancelar.Size = New System.Drawing.Size(71, 39)
        Me.btnMnuCancelar.Text = "&CANCELAR"
        Me.btnMnuCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuSalir
        '
        Me.btnMnuSalir.ForeColor = System.Drawing.Color.Red
        Me.btnMnuSalir.Image = Global.Fletera.My.Resources.Resources.Salida2
        Me.btnMnuSalir.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuSalir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuSalir.Name = "btnMnuSalir"
        Me.btnMnuSalir.Size = New System.Drawing.Size(41, 39)
        Me.btnMnuSalir.Text = "&SALIR"
        Me.btnMnuSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'MeStatus1
        '
        Me.MeStatus1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.MeStatus1.Location = New System.Drawing.Point(0, 443)
        Me.MeStatus1.Margin = New System.Windows.Forms.Padding(4)
        Me.MeStatus1.Name = "MeStatus1"
        Me.MeStatus1.Size = New System.Drawing.Size(879, 26)
        Me.MeStatus1.TabIndex = 72
        '
        'tabPrinc
        '
        Me.tabPrinc.Controls.Add(Me.tbServicios)
        Me.tabPrinc.Controls.Add(Me.tbTrabajos)
        Me.tabPrinc.Location = New System.Drawing.Point(3, 45)
        Me.tabPrinc.Name = "tabPrinc"
        Me.tabPrinc.SelectedIndex = 0
        Me.tabPrinc.Size = New System.Drawing.Size(864, 391)
        Me.tabPrinc.TabIndex = 73
        '
        'tbServicios
        '
        Me.tbServicios.Controls.Add(Me.TabOS)
        Me.tbServicios.Controls.Add(Me.gpoSerD)
        Me.tbServicios.Location = New System.Drawing.Point(4, 22)
        Me.tbServicios.Name = "tbServicios"
        Me.tbServicios.Padding = New System.Windows.Forms.Padding(3)
        Me.tbServicios.Size = New System.Drawing.Size(856, 365)
        Me.tbServicios.TabIndex = 0
        Me.tbServicios.Text = "Servicios"
        Me.tbServicios.UseVisualStyleBackColor = True
        '
        'TabOS
        '
        Me.TabOS.Controls.Add(Me.tbSEnt)
        Me.TabOS.Controls.Add(Me.tbSCan)
        Me.TabOS.Location = New System.Drawing.Point(5, 206)
        Me.TabOS.Name = "TabOS"
        Me.TabOS.SelectedIndex = 0
        Me.TabOS.Size = New System.Drawing.Size(844, 143)
        Me.TabOS.TabIndex = 77
        '
        'tbSEnt
        '
        Me.tbSEnt.Controls.Add(Me.gpoOSEnt)
        Me.tbSEnt.Location = New System.Drawing.Point(4, 22)
        Me.tbSEnt.Name = "tbSEnt"
        Me.tbSEnt.Padding = New System.Windows.Forms.Padding(3)
        Me.tbSEnt.Size = New System.Drawing.Size(836, 117)
        Me.tbSEnt.TabIndex = 1
        Me.tbSEnt.Text = "Entregar"
        Me.tbSEnt.UseVisualStyleBackColor = True
        '
        'gpoOSEnt
        '
        Me.gpoOSEnt.Controls.Add(Me.Label32)
        Me.gpoOSEnt.Controls.Add(Me.TextBox4)
        Me.gpoOSEnt.Controls.Add(Me.ComboBox1)
        Me.gpoOSEnt.Controls.Add(Me.Label33)
        Me.gpoOSEnt.Location = New System.Drawing.Point(6, 6)
        Me.gpoOSEnt.Name = "gpoOSEnt"
        Me.gpoOSEnt.Size = New System.Drawing.Size(824, 176)
        Me.gpoOSEnt.TabIndex = 50
        Me.gpoOSEnt.TabStop = False
        '
        'Label32
        '
        Me.Label32.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label32.Location = New System.Drawing.Point(8, 59)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(79, 20)
        Me.Label32.TabIndex = 53
        Me.Label32.Text = "Observaciones:"
        '
        'TextBox4
        '
        Me.TextBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox4.Location = New System.Drawing.Point(88, 52)
        Me.TextBox4.MaxLength = 200
        Me.TextBox4.Multiline = True
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.TextBox4.Size = New System.Drawing.Size(426, 54)
        Me.TextBox4.TabIndex = 52
        '
        'ComboBox1
        '
        Me.ComboBox1.AutoCompleteCustomSource.AddRange(New String() {"Regularizaci�n", "Vivienda"})
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(88, 19)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(426, 21)
        Me.ComboBox1.TabIndex = 51
        '
        'Label33
        '
        Me.Label33.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label33.Location = New System.Drawing.Point(8, 22)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(79, 20)
        Me.Label33.TabIndex = 50
        Me.Label33.Text = "Responsable:"
        '
        'tbSCan
        '
        Me.tbSCan.Controls.Add(Me.gpoOSCan)
        Me.tbSCan.Location = New System.Drawing.Point(4, 22)
        Me.tbSCan.Name = "tbSCan"
        Me.tbSCan.Padding = New System.Windows.Forms.Padding(3)
        Me.tbSCan.Size = New System.Drawing.Size(836, 117)
        Me.tbSCan.TabIndex = 0
        Me.tbSCan.Text = "Cancelar"
        Me.tbSCan.UseVisualStyleBackColor = True
        '
        'gpoOSCan
        '
        Me.gpoOSCan.Controls.Add(Me.Label11)
        Me.gpoOSCan.Controls.Add(Me.TextBox2)
        Me.gpoOSCan.Location = New System.Drawing.Point(0, 6)
        Me.gpoOSCan.Name = "gpoOSCan"
        Me.gpoOSCan.Size = New System.Drawing.Size(830, 176)
        Me.gpoOSCan.TabIndex = 52
        Me.gpoOSCan.TabStop = False
        '
        'Label11
        '
        Me.Label11.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label11.Location = New System.Drawing.Point(13, 26)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(79, 20)
        Me.Label11.TabIndex = 53
        Me.Label11.Text = "Observaciones:"
        '
        'TextBox2
        '
        Me.TextBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox2.Location = New System.Drawing.Point(93, 19)
        Me.TextBox2.MaxLength = 200
        Me.TextBox2.Multiline = True
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.TextBox2.Size = New System.Drawing.Size(426, 54)
        Me.TextBox2.TabIndex = 52
        '
        'gpoSerD
        '
        Me.gpoSerD.Controls.Add(Me.txtMarcaUni)
        Me.gpoSerD.Controls.Add(Me.Label20)
        Me.gpoSerD.Controls.Add(Me.txtNomUnidad)
        Me.gpoSerD.Controls.Add(Me.txtIdUnidad)
        Me.gpoSerD.Controls.Add(Me.Label21)
        Me.gpoSerD.Controls.Add(Me.cmdBuscaOS)
        Me.gpoSerD.Controls.Add(Me.txtKilometraje)
        Me.gpoSerD.Controls.Add(Me.Label16)
        Me.gpoSerD.Controls.Add(Me.txtAutorizo)
        Me.gpoSerD.Controls.Add(Me.Label15)
        Me.gpoSerD.Controls.Add(Me.txtEntrego)
        Me.gpoSerD.Controls.Add(Me.txtRecepciono)
        Me.gpoSerD.Controls.Add(Me.dtpFechaOrden)
        Me.gpoSerD.Controls.Add(Me.txtOrdenServ)
        Me.gpoSerD.Controls.Add(Me.Label14)
        Me.gpoSerD.Controls.Add(Me.dtpFechaOrdenhr)
        Me.gpoSerD.Controls.Add(Me.txtEstatusOrden)
        Me.gpoSerD.Controls.Add(Me.Label12)
        Me.gpoSerD.Controls.Add(Me.txtTipoOrden)
        Me.gpoSerD.Controls.Add(Me.Label1)
        Me.gpoSerD.Controls.Add(Me.Label8)
        Me.gpoSerD.Controls.Add(Me.Label7)
        Me.gpoSerD.Controls.Add(Me.Label13)
        Me.gpoSerD.Location = New System.Drawing.Point(6, 6)
        Me.gpoSerD.Name = "gpoSerD"
        Me.gpoSerD.Size = New System.Drawing.Size(721, 194)
        Me.gpoSerD.TabIndex = 76
        Me.gpoSerD.TabStop = False
        Me.gpoSerD.Text = "Orden de SERVICIO"
        '
        'txtMarcaUni
        '
        Me.txtMarcaUni.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMarcaUni.Location = New System.Drawing.Point(493, 123)
        Me.txtMarcaUni.MaxLength = 50
        Me.txtMarcaUni.Name = "txtMarcaUni"
        Me.txtMarcaUni.ReadOnly = True
        Me.txtMarcaUni.Size = New System.Drawing.Size(211, 20)
        Me.txtMarcaUni.TabIndex = 70
        '
        'Label20
        '
        Me.Label20.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label20.Location = New System.Drawing.Point(437, 126)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(50, 20)
        Me.Label20.TabIndex = 71
        Me.Label20.Text = "Marca:"
        '
        'txtNomUnidad
        '
        Me.txtNomUnidad.Enabled = False
        Me.txtNomUnidad.Location = New System.Drawing.Point(225, 123)
        Me.txtNomUnidad.Name = "txtNomUnidad"
        Me.txtNomUnidad.ReadOnly = True
        Me.txtNomUnidad.Size = New System.Drawing.Size(203, 20)
        Me.txtNomUnidad.TabIndex = 69
        '
        'txtIdUnidad
        '
        Me.txtIdUnidad.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtIdUnidad.Location = New System.Drawing.Point(110, 123)
        Me.txtIdUnidad.MaxLength = 3
        Me.txtIdUnidad.Name = "txtIdUnidad"
        Me.txtIdUnidad.ReadOnly = True
        Me.txtIdUnidad.Size = New System.Drawing.Size(69, 20)
        Me.txtIdUnidad.TabIndex = 68
        '
        'Label21
        '
        Me.Label21.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label21.Location = New System.Drawing.Point(7, 123)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(94, 20)
        Me.Label21.TabIndex = 67
        Me.Label21.Text = "Unidad:"
        '
        'cmdBuscaOS
        '
        Me.cmdBuscaOS.Image = CType(resources.GetObject("cmdBuscaOS.Image"), System.Drawing.Image)
        Me.cmdBuscaOS.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdBuscaOS.Location = New System.Drawing.Point(199, 13)
        Me.cmdBuscaOS.Name = "cmdBuscaOS"
        Me.cmdBuscaOS.Size = New System.Drawing.Size(32, 32)
        Me.cmdBuscaOS.TabIndex = 66
        Me.cmdBuscaOS.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'txtKilometraje
        '
        Me.txtKilometraje.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtKilometraje.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtKilometraje.Location = New System.Drawing.Point(544, 55)
        Me.txtKilometraje.MaxLength = 50
        Me.txtKilometraje.Name = "txtKilometraje"
        Me.txtKilometraje.ReadOnly = True
        Me.txtKilometraje.Size = New System.Drawing.Size(157, 20)
        Me.txtKilometraje.TabIndex = 20
        Me.txtKilometraje.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label16
        '
        Me.Label16.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label16.Location = New System.Drawing.Point(478, 55)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(68, 20)
        Me.Label16.TabIndex = 65
        Me.Label16.Text = "Kilometraje:"
        '
        'txtAutorizo
        '
        Me.txtAutorizo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtAutorizo.ForeColor = System.Drawing.Color.Red
        Me.txtAutorizo.Location = New System.Drawing.Point(544, 85)
        Me.txtAutorizo.MaxLength = 3
        Me.txtAutorizo.Name = "txtAutorizo"
        Me.txtAutorizo.ReadOnly = True
        Me.txtAutorizo.Size = New System.Drawing.Size(159, 20)
        Me.txtAutorizo.TabIndex = 23
        '
        'Label15
        '
        Me.Label15.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label15.Location = New System.Drawing.Point(478, 85)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(60, 20)
        Me.Label15.TabIndex = 63
        Me.Label15.Text = "Autorizo:"
        '
        'txtEntrego
        '
        Me.txtEntrego.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtEntrego.ForeColor = System.Drawing.Color.Red
        Me.txtEntrego.Location = New System.Drawing.Point(307, 88)
        Me.txtEntrego.MaxLength = 3
        Me.txtEntrego.Name = "txtEntrego"
        Me.txtEntrego.ReadOnly = True
        Me.txtEntrego.Size = New System.Drawing.Size(159, 20)
        Me.txtEntrego.TabIndex = 22
        '
        'txtRecepciono
        '
        Me.txtRecepciono.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRecepciono.ForeColor = System.Drawing.Color.Red
        Me.txtRecepciono.Location = New System.Drawing.Point(82, 85)
        Me.txtRecepciono.MaxLength = 3
        Me.txtRecepciono.Name = "txtRecepciono"
        Me.txtRecepciono.ReadOnly = True
        Me.txtRecepciono.Size = New System.Drawing.Size(159, 20)
        Me.txtRecepciono.TabIndex = 21
        '
        'dtpFechaOrden
        '
        Me.dtpFechaOrden.Location = New System.Drawing.Point(384, 19)
        Me.dtpFechaOrden.Name = "dtpFechaOrden"
        Me.dtpFechaOrden.Size = New System.Drawing.Size(225, 20)
        Me.dtpFechaOrden.TabIndex = 16
        '
        'txtOrdenServ
        '
        Me.txtOrdenServ.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtOrdenServ.ForeColor = System.Drawing.Color.Red
        Me.txtOrdenServ.Location = New System.Drawing.Point(82, 22)
        Me.txtOrdenServ.MaxLength = 3
        Me.txtOrdenServ.Name = "txtOrdenServ"
        Me.txtOrdenServ.ReadOnly = True
        Me.txtOrdenServ.Size = New System.Drawing.Size(69, 20)
        Me.txtOrdenServ.TabIndex = 15
        Me.txtOrdenServ.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label14
        '
        Me.Label14.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label14.Location = New System.Drawing.Point(287, 22)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(91, 20)
        Me.Label14.TabIndex = 48
        Me.Label14.Text = "Fecha Orden:"
        '
        'dtpFechaOrdenhr
        '
        Me.dtpFechaOrdenhr.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtpFechaOrdenhr.Location = New System.Drawing.Point(620, 19)
        Me.dtpFechaOrdenhr.Name = "dtpFechaOrdenhr"
        Me.dtpFechaOrdenhr.Size = New System.Drawing.Size(83, 20)
        Me.dtpFechaOrdenhr.TabIndex = 17
        '
        'txtEstatusOrden
        '
        Me.txtEstatusOrden.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtEstatusOrden.Location = New System.Drawing.Point(307, 55)
        Me.txtEstatusOrden.MaxLength = 50
        Me.txtEstatusOrden.Name = "txtEstatusOrden"
        Me.txtEstatusOrden.ReadOnly = True
        Me.txtEstatusOrden.Size = New System.Drawing.Size(101, 20)
        Me.txtEstatusOrden.TabIndex = 19
        '
        'Label12
        '
        Me.Label12.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label12.Location = New System.Drawing.Point(263, 55)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(56, 20)
        Me.Label12.TabIndex = 45
        Me.Label12.Text = "Estatus:"
        '
        'txtTipoOrden
        '
        Me.txtTipoOrden.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTipoOrden.Location = New System.Drawing.Point(82, 52)
        Me.txtTipoOrden.MaxLength = 50
        Me.txtTipoOrden.Name = "txtTipoOrden"
        Me.txtTipoOrden.ReadOnly = True
        Me.txtTipoOrden.Size = New System.Drawing.Size(101, 20)
        Me.txtTipoOrden.TabIndex = 18
        '
        'Label1
        '
        Me.Label1.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label1.Location = New System.Drawing.Point(6, 55)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(91, 20)
        Me.Label1.TabIndex = 16
        Me.Label1.Text = "Tipo de Orden"
        '
        'Label8
        '
        Me.Label8.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label8.Location = New System.Drawing.Point(259, 88)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(60, 20)
        Me.Label8.TabIndex = 61
        Me.Label8.Text = "Entrego:"
        '
        'Label7
        '
        Me.Label7.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label7.Location = New System.Drawing.Point(6, 88)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(69, 20)
        Me.Label7.TabIndex = 59
        Me.Label7.Text = "Recepciono:"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label13.Location = New System.Drawing.Point(6, 25)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(56, 13)
        Me.Label13.TabIndex = 47
        Me.Label13.Text = "Folio O.S.:"
        '
        'tbTrabajos
        '
        Me.tbTrabajos.Controls.Add(Me.tabOT)
        Me.tbTrabajos.Controls.Add(Me.gpoOT)
        Me.tbTrabajos.Location = New System.Drawing.Point(4, 22)
        Me.tbTrabajos.Name = "tbTrabajos"
        Me.tbTrabajos.Padding = New System.Windows.Forms.Padding(3)
        Me.tbTrabajos.Size = New System.Drawing.Size(856, 365)
        Me.tbTrabajos.TabIndex = 1
        Me.tbTrabajos.Text = "Orden Trabajo"
        Me.tbTrabajos.UseVisualStyleBackColor = True
        '
        'tabOT
        '
        Me.tabOT.Controls.Add(Me.tbTer)
        Me.tabOT.Controls.Add(Me.tbCan)
        Me.tabOT.Location = New System.Drawing.Point(6, 145)
        Me.tabOT.Name = "tabOT"
        Me.tabOT.SelectedIndex = 0
        Me.tabOT.Size = New System.Drawing.Size(844, 214)
        Me.tabOT.TabIndex = 76
        '
        'tbTer
        '
        Me.tbTer.Controls.Add(Me.gpoOtTer)
        Me.tbTer.Location = New System.Drawing.Point(4, 22)
        Me.tbTer.Name = "tbTer"
        Me.tbTer.Padding = New System.Windows.Forms.Padding(3)
        Me.tbTer.Size = New System.Drawing.Size(836, 188)
        Me.tbTer.TabIndex = 0
        Me.tbTer.Text = "Terminar"
        Me.tbTer.UseVisualStyleBackColor = True
        '
        'gpoOtTer
        '
        Me.gpoOtTer.Controls.Add(Me.Label3)
        Me.gpoOtTer.Controls.Add(Me.txtSolucion)
        Me.gpoOtTer.Controls.Add(Me.Label34)
        Me.gpoOtTer.Controls.Add(Me.txtObs)
        Me.gpoOtTer.Location = New System.Drawing.Point(8, 6)
        Me.gpoOtTer.Name = "gpoOtTer"
        Me.gpoOtTer.Size = New System.Drawing.Size(822, 176)
        Me.gpoOtTer.TabIndex = 54
        Me.gpoOtTer.TabStop = False
        '
        'Label3
        '
        Me.Label3.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label3.Location = New System.Drawing.Point(8, 26)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(79, 20)
        Me.Label3.TabIndex = 57
        Me.Label3.Text = "Solucion:"
        '
        'txtSolucion
        '
        Me.txtSolucion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSolucion.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSolucion.Location = New System.Drawing.Point(88, 19)
        Me.txtSolucion.MaxLength = 200
        Me.txtSolucion.Multiline = True
        Me.txtSolucion.Name = "txtSolucion"
        Me.txtSolucion.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtSolucion.Size = New System.Drawing.Size(426, 54)
        Me.txtSolucion.TabIndex = 56
        '
        'Label34
        '
        Me.Label34.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label34.Location = New System.Drawing.Point(8, 97)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(79, 20)
        Me.Label34.TabIndex = 55
        Me.Label34.Text = "Observaciones:"
        '
        'txtObs
        '
        Me.txtObs.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObs.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtObs.Location = New System.Drawing.Point(88, 90)
        Me.txtObs.MaxLength = 200
        Me.txtObs.Multiline = True
        Me.txtObs.Name = "txtObs"
        Me.txtObs.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtObs.Size = New System.Drawing.Size(426, 54)
        Me.txtObs.TabIndex = 57
        '
        'tbCan
        '
        Me.tbCan.Controls.Add(Me.gpoOtCan)
        Me.tbCan.Location = New System.Drawing.Point(4, 22)
        Me.tbCan.Name = "tbCan"
        Me.tbCan.Padding = New System.Windows.Forms.Padding(3)
        Me.tbCan.Size = New System.Drawing.Size(836, 188)
        Me.tbCan.TabIndex = 1
        Me.tbCan.Text = "Cancelar"
        Me.tbCan.UseVisualStyleBackColor = True
        '
        'gpoOtCan
        '
        Me.gpoOtCan.Controls.Add(Me.Label22)
        Me.gpoOtCan.Controls.Add(Me.TextBox1)
        Me.gpoOtCan.Location = New System.Drawing.Point(6, 6)
        Me.gpoOtCan.Name = "gpoOtCan"
        Me.gpoOtCan.Size = New System.Drawing.Size(824, 176)
        Me.gpoOtCan.TabIndex = 50
        Me.gpoOtCan.TabStop = False
        '
        'Label22
        '
        Me.Label22.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label22.Location = New System.Drawing.Point(11, 26)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(79, 20)
        Me.Label22.TabIndex = 51
        Me.Label22.Text = "Motivo:"
        '
        'TextBox1
        '
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(91, 19)
        Me.TextBox1.MaxLength = 200
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.TextBox1.Size = New System.Drawing.Size(426, 54)
        Me.TextBox1.TabIndex = 50
        '
        'gpoOT
        '
        Me.gpoOT.Controls.Add(Me.txtDivision)
        Me.gpoOT.Controls.Add(Me.txtFamilia)
        Me.gpoOT.Controls.Add(Me.txtFalla)
        Me.gpoOT.Controls.Add(Me.Label23)
        Me.gpoOT.Controls.Add(Me.txtUsuarioAsig)
        Me.gpoOT.Controls.Add(Me.Label6)
        Me.gpoOT.Controls.Add(Me.txtDuracion)
        Me.gpoOT.Controls.Add(Me.Label5)
        Me.gpoOT.Controls.Add(Me.txtPuestoMec)
        Me.gpoOT.Controls.Add(Me.txtNombreMecanico)
        Me.gpoOT.Controls.Add(Me.Label4)
        Me.gpoOT.Controls.Add(Me.dtpFechaAsigHora)
        Me.gpoOT.Controls.Add(Me.dtpFechaAsig)
        Me.gpoOT.Controls.Add(Me.Label10)
        Me.gpoOT.Controls.Add(Me.Label9)
        Me.gpoOT.Controls.Add(Me.txtNomEmpresa)
        Me.gpoOT.Controls.Add(Me.cmdBuscaClave)
        Me.gpoOT.Controls.Add(Me.txtOrdenTrabajo)
        Me.gpoOT.Controls.Add(Me.Label2)
        Me.gpoOT.Controls.Add(Me.Label25)
        Me.gpoOT.Controls.Add(Me.Label24)
        Me.gpoOT.Location = New System.Drawing.Point(5, 6)
        Me.gpoOT.Name = "gpoOT"
        Me.gpoOT.Size = New System.Drawing.Size(845, 133)
        Me.gpoOT.TabIndex = 75
        Me.gpoOT.TabStop = False
        Me.gpoOT.Text = "Orden de Trabajo"
        '
        'txtDivision
        '
        Me.txtDivision.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDivision.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDivision.ForeColor = System.Drawing.Color.Red
        Me.txtDivision.Location = New System.Drawing.Point(679, 99)
        Me.txtDivision.MaxLength = 3
        Me.txtDivision.Name = "txtDivision"
        Me.txtDivision.ReadOnly = True
        Me.txtDivision.Size = New System.Drawing.Size(159, 26)
        Me.txtDivision.TabIndex = 13
        '
        'txtFamilia
        '
        Me.txtFamilia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtFamilia.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFamilia.ForeColor = System.Drawing.Color.Red
        Me.txtFamilia.Location = New System.Drawing.Point(449, 99)
        Me.txtFamilia.MaxLength = 3
        Me.txtFamilia.Name = "txtFamilia"
        Me.txtFamilia.ReadOnly = True
        Me.txtFamilia.Size = New System.Drawing.Size(217, 26)
        Me.txtFamilia.TabIndex = 12
        '
        'txtFalla
        '
        Me.txtFalla.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtFalla.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFalla.ForeColor = System.Drawing.Color.Red
        Me.txtFalla.Location = New System.Drawing.Point(76, 99)
        Me.txtFalla.MaxLength = 3
        Me.txtFalla.Name = "txtFalla"
        Me.txtFalla.ReadOnly = True
        Me.txtFalla.Size = New System.Drawing.Size(367, 26)
        Me.txtFalla.TabIndex = 11
        '
        'Label23
        '
        Me.Label23.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label23.Location = New System.Drawing.Point(10, 99)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(60, 20)
        Me.Label23.TabIndex = 59
        Me.Label23.Text = "FALLA:"
        '
        'txtUsuarioAsig
        '
        Me.txtUsuarioAsig.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtUsuarioAsig.ForeColor = System.Drawing.Color.Red
        Me.txtUsuarioAsig.Location = New System.Drawing.Point(384, 44)
        Me.txtUsuarioAsig.MaxLength = 3
        Me.txtUsuarioAsig.Name = "txtUsuarioAsig"
        Me.txtUsuarioAsig.ReadOnly = True
        Me.txtUsuarioAsig.Size = New System.Drawing.Size(159, 20)
        Me.txtUsuarioAsig.TabIndex = 7
        '
        'Label6
        '
        Me.Label6.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label6.Location = New System.Drawing.Point(318, 44)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(60, 20)
        Me.Label6.TabIndex = 57
        Me.Label6.Text = "Asign�:"
        '
        'txtDuracion
        '
        Me.txtDuracion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDuracion.ForeColor = System.Drawing.Color.Red
        Me.txtDuracion.Location = New System.Drawing.Point(648, 45)
        Me.txtDuracion.MaxLength = 3
        Me.txtDuracion.Name = "txtDuracion"
        Me.txtDuracion.ReadOnly = True
        Me.txtDuracion.Size = New System.Drawing.Size(69, 20)
        Me.txtDuracion.TabIndex = 10
        '
        'Label5
        '
        Me.Label5.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label5.Location = New System.Drawing.Point(579, 44)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(63, 20)
        Me.Label5.TabIndex = 55
        Me.Label5.Text = "Duraci�n:"
        '
        'txtPuestoMec
        '
        Me.txtPuestoMec.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPuestoMec.ForeColor = System.Drawing.Color.Red
        Me.txtPuestoMec.Location = New System.Drawing.Point(321, 73)
        Me.txtPuestoMec.MaxLength = 3
        Me.txtPuestoMec.Name = "txtPuestoMec"
        Me.txtPuestoMec.ReadOnly = True
        Me.txtPuestoMec.Size = New System.Drawing.Size(188, 20)
        Me.txtPuestoMec.TabIndex = 9
        '
        'txtNombreMecanico
        '
        Me.txtNombreMecanico.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNombreMecanico.ForeColor = System.Drawing.Color.Red
        Me.txtNombreMecanico.Location = New System.Drawing.Point(76, 73)
        Me.txtNombreMecanico.MaxLength = 3
        Me.txtNombreMecanico.Name = "txtNombreMecanico"
        Me.txtNombreMecanico.ReadOnly = True
        Me.txtNombreMecanico.Size = New System.Drawing.Size(228, 20)
        Me.txtNombreMecanico.TabIndex = 8
        '
        'Label4
        '
        Me.Label4.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label4.Location = New System.Drawing.Point(6, 76)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(60, 20)
        Me.Label4.TabIndex = 52
        Me.Label4.Text = "Mecanico:"
        '
        'dtpFechaAsigHora
        '
        Me.dtpFechaAsigHora.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtpFechaAsigHora.Location = New System.Drawing.Point(634, 19)
        Me.dtpFechaAsigHora.Name = "dtpFechaAsigHora"
        Me.dtpFechaAsigHora.Size = New System.Drawing.Size(83, 20)
        Me.dtpFechaAsigHora.TabIndex = 5
        '
        'dtpFechaAsig
        '
        Me.dtpFechaAsig.Location = New System.Drawing.Point(384, 19)
        Me.dtpFechaAsig.Name = "dtpFechaAsig"
        Me.dtpFechaAsig.Size = New System.Drawing.Size(225, 20)
        Me.dtpFechaAsig.TabIndex = 4
        '
        'Label10
        '
        Me.Label10.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label10.Location = New System.Drawing.Point(287, 22)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(91, 20)
        Me.Label10.TabIndex = 39
        Me.Label10.Text = "Fecha Asignado:"
        '
        'Label9
        '
        Me.Label9.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label9.Location = New System.Drawing.Point(10, 47)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(54, 20)
        Me.Label9.TabIndex = 36
        Me.Label9.Text = "Empresa:"
        '
        'txtNomEmpresa
        '
        Me.txtNomEmpresa.Enabled = False
        Me.txtNomEmpresa.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNomEmpresa.Location = New System.Drawing.Point(76, 47)
        Me.txtNomEmpresa.Name = "txtNomEmpresa"
        Me.txtNomEmpresa.ReadOnly = True
        Me.txtNomEmpresa.Size = New System.Drawing.Size(196, 20)
        Me.txtNomEmpresa.TabIndex = 6
        '
        'cmdBuscaClave
        '
        Me.cmdBuscaClave.Image = CType(resources.GetObject("cmdBuscaClave.Image"), System.Drawing.Image)
        Me.cmdBuscaClave.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdBuscaClave.Location = New System.Drawing.Point(224, 10)
        Me.cmdBuscaClave.Name = "cmdBuscaClave"
        Me.cmdBuscaClave.Size = New System.Drawing.Size(32, 32)
        Me.cmdBuscaClave.TabIndex = 3
        Me.cmdBuscaClave.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'txtOrdenTrabajo
        '
        Me.txtOrdenTrabajo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtOrdenTrabajo.ForeColor = System.Drawing.Color.Red
        Me.txtOrdenTrabajo.Location = New System.Drawing.Point(138, 19)
        Me.txtOrdenTrabajo.MaxLength = 100
        Me.txtOrdenTrabajo.Name = "txtOrdenTrabajo"
        Me.txtOrdenTrabajo.Size = New System.Drawing.Size(69, 20)
        Me.txtOrdenTrabajo.TabIndex = 2
        Me.txtOrdenTrabajo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label2
        '
        Me.Label2.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label2.Location = New System.Drawing.Point(10, 22)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(126, 20)
        Me.Label2.TabIndex = 14
        Me.Label2.Text = "Folio O.T.:"
        '
        'Label25
        '
        Me.Label25.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label25.Location = New System.Drawing.Point(729, 81)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(109, 20)
        Me.Label25.TabIndex = 63
        Me.Label25.Text = "Division"
        '
        'Label24
        '
        Me.Label24.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label24.Location = New System.Drawing.Point(515, 76)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(151, 20)
        Me.Label24.TabIndex = 62
        Me.Label24.Text = "Familia"
        '
        'FEstatusTaller
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(879, 469)
        Me.Controls.Add(Me.tabPrinc)
        Me.Controls.Add(Me.MeStatus1)
        Me.Controls.Add(Me.ToolStripMenu)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "FEstatusTaller"
        Me.Text = "Cambia Estatus"
        Me.ToolStripMenu.ResumeLayout(False)
        Me.ToolStripMenu.PerformLayout()
        Me.tabPrinc.ResumeLayout(False)
        Me.tbServicios.ResumeLayout(False)
        Me.TabOS.ResumeLayout(False)
        Me.tbSEnt.ResumeLayout(False)
        Me.gpoOSEnt.ResumeLayout(False)
        Me.gpoOSEnt.PerformLayout()
        Me.tbSCan.ResumeLayout(False)
        Me.gpoOSCan.ResumeLayout(False)
        Me.gpoOSCan.PerformLayout()
        Me.gpoSerD.ResumeLayout(False)
        Me.gpoSerD.PerformLayout()
        Me.tbTrabajos.ResumeLayout(False)
        Me.tabOT.ResumeLayout(False)
        Me.tbTer.ResumeLayout(False)
        Me.gpoOtTer.ResumeLayout(False)
        Me.gpoOtTer.PerformLayout()
        Me.tbCan.ResumeLayout(False)
        Me.gpoOtCan.ResumeLayout(False)
        Me.gpoOtCan.PerformLayout()
        Me.gpoOT.ResumeLayout(False)
        Me.gpoOT.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

#End Region

    'Sub New(ByVal vTag As String, Optional ByVal vNoServ As Integer = 0, Optional ByVal vOrdenTrabajo As Integer = 0, Optional ByVal vOpcion As TipoOpcionEstatus = TipoOpcionEstatus.SinOpcion)
    '    InitializeComponent()
    '    _Tag = vTag
    '    _OrdenServicio = vNoServ
    '    _OrdenTrabajo = vOrdenTrabajo
    '    _Opcion = vOpcion

    'End Sub
    Sub New(Optional ByVal vNoServ As Integer = 0, Optional ByVal vOrdenTrabajo As Integer = 0, Optional ByVal vOpcion As TipoOpcionEstatus = TipoOpcionEstatus.SinOpcion)
        InitializeComponent()
        '_Tag = vTag
        _OrdenServicio = vNoServ
        _OrdenTrabajo = vOrdenTrabajo
        _Opcion = vOpcion

    End Sub


    Private Sub btnMnuSalir_Click(sender As Object, e As EventArgs) Handles btnMnuSalir.Click
        Me.Close()
    End Sub

    Private Sub LimpiaCampos()
        txtOrdenTrabajo.Text = ""
        dtpFechaAsig.Value = Now
        dtpFechaAsigHora.Value = Now
        txtNomEmpresa.Text = ""
        txtUsuarioAsig.Text = ""
        txtNombreMecanico.Text = ""
        txtPuestoMec.Text = ""
        txtDuracion.Text = ""

        txtFalla.Text = ""
        txtFamilia.Text = ""
        txtDivision.Text = ""

        txtOrdenServ.Text = ""
        dtpFechaOrden.Value = Now
        dtpFechaOrdenhr.Value = Now
        txtTipoOrden.Text = ""
        txtEstatusOrden.Text = ""
        txtKilometraje.Text = ""
        txtRecepciono.Text = ""
        txtEntrego.Text = ""
        txtAutorizo.Text = ""

        txtIdUnidad.Text = ""
        txtNomUnidad.Text = ""
        txtMarcaUni.Text = ""
        'txtUltimaOrdenServ.Text = ""
        'txtUltOrdenTrabajo.Text = ""
        'txtUltimoMecanico.Text = ""
        'txtUltFalla.Text = ""
        'txtUltFamilia.Text = ""
        'txtUltDivision.Text = ""


    End Sub
   
    Private Sub FEstatusTaller_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        LimpiaCampos()
        If _OrdenTrabajo > 0 Then
            If _Opcion = TipoOpcionEstatus.Terminar Then
                tabPrinc.SelectedIndex = 1
                txtOrdenTrabajo.Text = _OrdenTrabajo
                CargaForm(txtOrdenTrabajo.Text, "OrdenTrabajo")
                TabOS.Visible = False
                gpoOtCan.Visible = False
                gpoOT.Enabled = False
                txtSolucion.Focus()

            End If
        End If
        If _OrdenServicio > 0 Then
            If _Opcion = TipoOpcionEstatus.Entregar Then
                tabPrinc.SelectedIndex = 0
                txtOrdenServ.Text = _OrdenServicio
                CargaForm(txtOrdenServ.Text, "OrdenServicio")
            End If
        End If
    End Sub

    Private Sub CargaForm(ByVal idClave As String, ByVal NomTabla As String)
        Try
            Select Case UCase(NomTabla)
                Case UCase("UNIPROD_COM")
                    'UnidadCom = New UnidadesComClass(idClave, "CCLAVEINT", TipoDato.TdCadena)
                    'If UnidadCom.Existe Then
                    '    txtNomUniProd.Text = UnidadCom.CNOMBREUNIDAD
                    '    vCIDUNIDAD = UnidadCom.CIDUNIDAD
                    'End If
                Case UCase("PROD_COM")
                    'ProdCom = New ProductosComClass(idClave)
                    'If ProdCom.Existe Then
                    '    If ProdCom.CSTATUSPRODUCTO = 1 Then
                    '        vCIDPRODUCTO = ProdCom.CIDPRODUCTO
                    '        txtNomProducto.Text = ProdCom.CNOMBREPRODUCTO
                    '        UnidadCom = New UnidadesComClass(ProdCom.CIDUNIDADBASE, "CIDUNIDAD", TipoDato.TdNumerico)
                    '        If UnidadCom.Existe Then
                    '            txtIdUnidadProd.Text = UnidadCom.CCLAVEINT
                    '        End If

                    '    Else
                    '        'Desactivado
                    '    End If
                    'End If
                Case UCase("OrdenTrabajo")
                    OrdenTrabajo = New OrdenTrabajoClass(idClave)
                    With OrdenTrabajo
                        'ORDEN DE TRABAJO
                        txtOrdenTrabajo.Text = idClave
                        If .Existe Then

                            'MECANICO RESPONSABLE
                            Personal = New PersonalClass(.idPersonalResp)
                            'cmbResponsable.SelectedValue = .idPersonalResp
                            If Personal.Existe Then
                                txtNombreMecanico.Text = Personal.NombreCompleto

                                PersonalPuesto = New PuestosClass(Personal.idPuesto)
                                If PersonalPuesto.Existe Then
                                    txtPuestoMec.Text = PersonalPuesto.NomPuesto
                                End If
                            End If
                            'AYUDANTE 1
                            If .idPersonalAyu1 > 0 Then
                                'chkAyu1.Checked = True
                                Personal = New PersonalClass(.idPersonalAyu1)
                                'cmbayudante1.SelectedValue = .idPersonalAyu1
                            Else
                                'chkAyu1.Checked = False
                            End If

                            'AYUDANTE 2
                            If .idPersonalAyu2 > 0 Then
                                'chkAyu2.Checked = True
                                Personal = New PersonalClass(.idPersonalAyu2)
                                'cmbayudante2.SelectedValue = .idPersonalAyu2
                            Else
                                'chkAyu2.Checked = False
                            End If

                            'FALLA DE LA ORDEN DE TRABAJO
                            txtFalla.Text = .NotaRecepcion
                            FamiliaOT = New FamiliasClass(.idFamilia)
                            If FamiliaOT.Existe Then
                                txtFamilia.Text = FamiliaOT.NomFamilia
                                txtDivision.Text = FamiliaOT.NomDivision
                            End If

                            'DETALLE ORDEN DE SERVICIO
                            DetOS = New detOrdenServicioClass(idClave)
                            If DetOS.Existe Then
                                txtDuracion.Text = DetOS.DuracionActHr
                                dtpFechaAsig.Value = DetOS.FechaAsig
                                dtpFechaAsigHora.Value = DetOS.FechaAsig
                                User = New UsuarioClass(DetOS.UsuarioAsig)
                                If User.Existe Then
                                    txtUsuarioAsig.Text = User.NombreUsuario
                                End If
                            End If

                            'ORDEN DE SERVICIO
                            OrdenServ = New OrdenServClass(.idOrdenSer)

                            txtOrdenServ.Text = .idOrdenSer
                            dtpFechaOrden.Value = OrdenServ.FechaRecepcion
                            dtpFechaOrdenhr.Value = OrdenServ.FechaRecepcion

                            Emp = New EmpresaClass(OrdenServ.IdEmpresa)
                            If Emp.Existe Then
                                txtNomEmpresa.Text = Emp.RazonSocial
                            End If

                            TipOrd = New TipoOrdenClass(OrdenServ.idTipoOrden)
                            If TipOrd.Existe Then
                                txtTipoOrden.Text = TipOrd.NomTipoOrden
                            End If
                            txtEstatusOrden.Text = OrdenServ.Estatus
                            txtKilometraje.Text = OrdenServ.Kilometraje
                            'RECEPCIONO
                            User = New UsuarioClass(OrdenServ.UsuarioRecepcion)
                            If User.Existe Then
                                txtRecepciono.Text = User.NombreUsuario
                            End If
                            'ENTREGO
                            Personal = New PersonalClass(OrdenServ.idEmpleadoEntrega)
                            If Personal.Existe Then
                                txtEntrego.Text = Personal.NombreCompleto
                            End If
                            'AUTORIZO
                            Personal = New PersonalClass(OrdenServ.idEmpleadoAutoriza)
                            If Personal.Existe Then
                                txtAutorizo.Text = Personal.NombreCompleto
                            End If
                            'ORDEN DE SERVICIO

                            'UNIDAD DE TRANSPORTE
                            txtIdUnidad.Text = OrdenServ.idUnidadTrans
                            UniTrans = New UnidadesTranspClass(OrdenServ.idUnidadTrans)
                            If UniTrans.Existe Then
                                txtNomUnidad.Text = UniTrans.DescripcionUni
                                Marca = New MarcaClass(UniTrans.idMarca)
                                If Marca.Existe Then
                                    txtMarcaUni.Text = Marca.NOMBREMARCA
                                End If


                            End If


                            'Historial
                            'CargaOrdenesSerHis(OrdenServ.idUnidadTrans)

                            'IdOrdenServ = grdOrdenServHis.Item("hosIdServicio", grdOrdenServHis.CurrentCell.RowIndex).Value
                            'CargaOrdenesTrabHis(txtIdUnidad.Text, IdOrdenServ)

                            'VALIDACIONES
                            If OrdenServ.Estatus = "ASIG" Then
                                'ActivaCampos(False, TipoOpcActivaCampos.tOpcINICIALIZA)
                                'ActivaBotones(True, TipoOpcActivaBoton.tOpcInicializa)

                                txtDuracion.Focus()
                                txtDuracion.SelectAll()
                            ElseIf OrdenServ.Estatus = "DIA" Then
                                MsgBox("La Orden de Trabajo: " & idClave & " ya Esta en Proceso, No puede Modificarse")
                                'ActivaCampos(False, TipoOpcActivaCampos.tOpcINICIALIZA)

                                'ActivaBotones(False, TipoOpcActivaBoton.tOpcInicializa)
                                btnMnuCancelar.Enabled = True


                            ElseIf OrdenServ.Estatus = "REC" Then
                                MsgBox("La Orden de Trabajo: " & idClave & " No esta Asignada, No puede Modificarse")
                                txtOrdenTrabajo.Focus()
                                txtOrdenTrabajo.SelectAll()

                            End If
                        Else
                            'No existe 
                            MsgBox("La Orden de Trabajo: " & idClave & " No Existe !!!")
                            txtOrdenTrabajo.Focus()
                            txtOrdenTrabajo.SelectAll()
                        End If

                    End With
                    'Case UCase(TablaBd2)
                    '    'aqui()
                    '    TipoServ = New TipoServClass(idClave)
                    '    With TipoServ
                    '        txtidTipoUnidad.Text = .idTipoServicio
                    '        If .Existe Then
                    '            txtnomTipoUniTras.Text = .NomTipoServicio
                    '            txtDescripcionUni.Focus()
                    '            txtDescripcionUni.SelectAll()
                    '        Else
                    '            MsgBox("El Tipo de Servicio con id: " & txtidTipoUnidad.Text & " No Existe")
                    '            txtidTipoUnidad.Focus()
                    '            txtidTipoUnidad.SelectAll()
                    '        End If
                    '    End With
                Case UCase("OrdenServicio")
                    OrdenServ = New OrdenServClass(idClave)
                    With OrdenServ
                        txtOrdenServ.Text = idClave
                        If .Existe Then
                            txtOrdenServ.Text = .idOrdenSer
                            dtpFechaOrden.Value = OrdenServ.FechaRecepcion
                            dtpFechaOrdenhr.Value = OrdenServ.FechaRecepcion

                            Emp = New EmpresaClass(OrdenServ.IdEmpresa)
                            If Emp.Existe Then
                                txtNomEmpresa.Text = Emp.RazonSocial
                            End If

                            TipOrd = New TipoOrdenClass(OrdenServ.idTipoOrden)
                            If TipOrd.Existe Then
                                txtTipoOrden.Text = TipOrd.NomTipoOrden
                            End If
                            txtEstatusOrden.Text = OrdenServ.Estatus
                            txtKilometraje.Text = OrdenServ.Kilometraje

                            'RECEPCIONO
                            User = New UsuarioClass(OrdenServ.UsuarioRecepcion)
                            If User.Existe Then
                                txtRecepciono.Text = User.NombreUsuario
                            End If
                            'ENTREGO
                            Personal = New PersonalClass(OrdenServ.idEmpleadoEntrega)
                            If Personal.Existe Then
                                txtEntrego.Text = Personal.NombreCompleto
                            End If
                            'AUTORIZO
                            Personal = New PersonalClass(OrdenServ.idEmpleadoAutoriza)
                            If Personal.Existe Then
                                txtAutorizo.Text = Personal.NombreCompleto
                            End If
                        End If
                    End With

            End Select



        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try

    End Sub

    Private Sub txtOrdenServ_EnabledChanged(sender As Object, e As EventArgs) Handles txtOrdenServ.EnabledChanged
        cmdBuscaOS.Enabled = txtOrdenServ.Enabled
    End Sub

    Private Sub txtOrdenServ_TextChanged(sender As Object, e As EventArgs) Handles txtOrdenServ.TextChanged

    End Sub

    Private Sub btnMnuOk_Click(sender As Object, e As EventArgs) Handles btnMnuOk.Click
        Try
            Dim Mensaje As String = ""

            If _Opcion = TipoOpcionEstatus.Terminar Then
                Mensaje = "!! Esta seguro que desea Terminar la Orden de Trabajo: " & txtOrdenTrabajo.Text & "? !!"
            ElseIf _Opcion = TipoOpcionEstatus.Entregar Then
                Mensaje = "!! Esta seguro que desea Entregar la Unidad de Transporte: " & txtIdUnidad.Text & "? !!"
            ElseIf _Opcion = TipoOpcionEstatus.Cancelar Then
                If _OrdenServicio > 0 Then
                    Mensaje = "!! Esta seguro que desea Cancelar la Orden de Servicio: " & txtOrdenServ.Text & "? !!"
                ElseIf _OrdenTrabajo > 0 Then
                    Mensaje = "!! Esta seguro que desea Cancelar la Orden de Trabajo: " & txtOrdenTrabajo.Text & "? !!"
                End If
            End If

            Salida = True

            'If Not Validar() Then Exit Sub

            If MsgBox(Mensaje, MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                If _Opcion = TipoOpcionEstatus.Terminar Then

                    OrdenServ = New OrdenServClass(Convert.ToInt32(txtOrdenServ.Text))
                    With OrdenServ
                        If .Existe Then
                            If .ContEstatusxOS("TODAS") - .ContEstatusxOS("TER") = 1 Then
                                StrSql = .ConsultaActualizaDato("TER", "Estatus", TipoDato.TdCadena)
                                ReDim Preserve ArraySql(indice)
                                ArraySql(indice) = StrSql
                                indice += 1

                                StrSql = .ConsultaActualizaDato(Now, "FechaTerminado", TipoDato.TdFecha)
                                ReDim Preserve ArraySql(indice)
                                ArraySql(indice) = StrSql
                                indice += 1
                            End If
                        End If
                    End With


                    DetOS = New detOrdenServicioClass(Convert.ToInt32(txtOrdenTrabajo.Text))
                    With DetOS
                        If .Existe Then
                            StrSql = .ConsultaActualizaDato(txtSolucion.Text, "Solucion", TipoDato.TdCadena)
                            ReDim Preserve ArraySql(indice)
                            ArraySql(indice) = StrSql
                            indice += 1

                            StrSql = .ConsultaActualizaDato(txtObs.Text, "Observaciones", TipoDato.TdCadena)
                            ReDim Preserve ArraySql(indice)
                            ArraySql(indice) = StrSql
                            indice += 1

                            StrSql = .ConsultaActualizaDato(Now, "FechaTerminado", TipoDato.TdFecha)
                            ReDim Preserve ArraySql(indice)
                            ArraySql(indice) = StrSql
                            indice += 1

                            StrSql = .ConsultaActualizaDato(UserId, "UsuarioTerminado", TipoDato.TdCadena)
                            ReDim Preserve ArraySql(indice)
                            ArraySql(indice) = StrSql
                            indice += 1

                            StrSql = .ConsultaActualizaDato("TER", "Estatus", TipoDato.TdCadena)
                            ReDim Preserve ArraySql(indice)
                            ArraySql(indice) = StrSql
                            indice += 1

                        End If
                    End With
                ElseIf _Opcion = TipoOpcionEstatus.Entregar Then

                ElseIf _Opcion = TipoOpcionEstatus.Cancelar Then
                    If _OrdenServicio > 0 Then

                    ElseIf _OrdenTrabajo > 0 Then

                    End If
                End If

                If indice > 0 Then
                    Dim obj As New CapaNegocio.Tablas
                    Dim Respuesta As String

                    Respuesta = obj.EjecutarSql(Inicio.CONSTR, "SQL", ArraySql, Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, indice)
                    FEspera.Close()
                    If Respuesta = "EFECTUADO" Then
                        If _Opcion = TipoOpcionEstatus.Terminar Then
                            MsgBox("Orden de Trabajo Actualizado Satisfactoriamente", MsgBoxStyle.Information, Me.Text)
                        ElseIf _Opcion = TipoOpcionEstatus.Entregar Then

                        ElseIf _Opcion = TipoOpcionEstatus.Cancelar Then
                            If _OrdenServicio > 0 Then

                            ElseIf _OrdenTrabajo > 0 Then

                            End If
                        End If
                        btnMnuSalir_Click(sender, e)
                    ElseIf Respuesta = "NOEFECTUADO" Then

                    ElseIf Respuesta = "ERROR" Then
                        indice = 0
                        SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")
                    End If
                Else
                    MsgBox("No se Encontraron Registros que Grabar", MsgBoxStyle.Exclamation, Me.Text)
                End If
                '
            End If

        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try
    End Sub

    Private Function Validar() As Boolean
        Validar = True
        If Not Inicio.ValidandoCampo(Trim(txtSolucion.Text), TipoDato.TdCadena) Then
            MsgBox("La Duracion de la Orden de Trabajo No Puede Ser Vacio", vbInformation, "Aviso" & Me.Text)
            If txtDuracion.Enabled Then
                txtDuracion.Focus()
                txtDuracion.SelectAll()
            End If
            Validar = False
            'ElseIf cmbResponsable.SelectedValue = 0 Then
            '    MsgBox("El Mecanico Responsable no puede ser vacio", vbInformation, "Aviso" & Me.Text)
            '    If cmbResponsable.Enabled Then
            '        cmbResponsable.Focus()
            '    End If
            '    Validar = False
            'ElseIf chkAyu1.Checked Then
            '    If cmbayudante1.SelectedValue = 0 Then
            '        MsgBox("El Ayudante no puede ser vacio", vbInformation, "Aviso" & Me.Text)
            '        If cmbayudante1.Enabled Then
            '            cmbayudante1.Focus()
            '        End If
            '        Validar = False

            '    End If
            'ElseIf chkAyu2.Checked And Not chkAyu1.Checked Then
            '    MsgBox("No puede Seleccionar al Ayudante 2 Hasta que seleccione el Ayudante 1", vbInformation, "Aviso" & Me.Text)
            '    chkAyu2.Checked = False
            '    If chkAyu1.Enabled Then
            '        chkAyu1.Focus()
            '    End If

            'ElseIf chkAyu2.Checked Then
            '    If cmbayudante2.SelectedValue = 0 Then
            '        MsgBox("El Ayudante no puede ser vacio", vbInformation, "Aviso" & Me.Text)
            '        If cmbayudante2.Enabled Then
            '            cmbayudante2.Focus()
            '        End If
            '        Validar = False

            '    End If
        End If
    End Function
End Class
