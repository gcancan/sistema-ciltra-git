﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class fAbreExcelFacturacion
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.btnOpenFile = New System.Windows.Forms.Button()
        Me.txtNomArchivo = New System.Windows.Forms.TextBox()
        Me.label12 = New System.Windows.Forms.Label()
        Me.openFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cmbHojasCAB = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cmbColumnasCAB = New System.Windows.Forms.ComboBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.grdRelaciones = New System.Windows.Forms.DataGridView()
        Me.ColExcel = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colListado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdRelacionaCAB = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cmbColListado = New System.Windows.Forms.ComboBox()
        Me.chkNoRep = New System.Windows.Forms.CheckBox()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.lblNoRegExcel = New System.Windows.Forms.Label()
        Me.grdCuentas = New System.Windows.Forms.DataGridView()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.lblNoRegRes = New System.Windows.Forms.Label()
        Me.grdResultante = New System.Windows.Forms.DataGridView()
        Me.ToolStripMenu = New System.Windows.Forms.ToolStrip()
        Me.btnMnuOk = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuGenerar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuCancelar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuSalir = New System.Windows.Forms.ToolStripButton()
        CType(Me.grdRelaciones, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.grdCuentas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        CType(Me.grdResultante, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ToolStripMenu.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnOpenFile
        '
        Me.btnOpenFile.Location = New System.Drawing.Point(673, 50)
        Me.btnOpenFile.Name = "btnOpenFile"
        Me.btnOpenFile.Size = New System.Drawing.Size(34, 26)
        Me.btnOpenFile.TabIndex = 95
        Me.btnOpenFile.Text = "..."
        Me.btnOpenFile.UseVisualStyleBackColor = True
        '
        'txtNomArchivo
        '
        Me.txtNomArchivo.Enabled = False
        Me.txtNomArchivo.Location = New System.Drawing.Point(104, 56)
        Me.txtNomArchivo.Name = "txtNomArchivo"
        Me.txtNomArchivo.ReadOnly = True
        Me.txtNomArchivo.Size = New System.Drawing.Size(563, 20)
        Me.txtNomArchivo.TabIndex = 94
        '
        'label12
        '
        Me.label12.AutoSize = True
        Me.label12.ForeColor = System.Drawing.Color.Blue
        Me.label12.Location = New System.Drawing.Point(12, 59)
        Me.label12.Name = "label12"
        Me.label12.Size = New System.Drawing.Size(86, 13)
        Me.label12.TabIndex = 93
        Me.label12.Text = "Nombre Archivo:"
        '
        'openFileDialog1
        '
        Me.openFileDialog1.FileName = "openFileDialog1"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(64, 85)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(34, 13)
        Me.Label1.TabIndex = 148
        Me.Label1.Text = "Hojas"
        '
        'cmbHojasCAB
        '
        Me.cmbHojasCAB.FormattingEnabled = True
        Me.cmbHojasCAB.Location = New System.Drawing.Point(104, 82)
        Me.cmbHojasCAB.Name = "cmbHojasCAB"
        Me.cmbHojasCAB.Size = New System.Drawing.Size(273, 21)
        Me.cmbHojasCAB.TabIndex = 147
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(45, 112)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(53, 13)
        Me.Label2.TabIndex = 150
        Me.Label2.Text = "Columnas"
        '
        'cmbColumnasCAB
        '
        Me.cmbColumnasCAB.FormattingEnabled = True
        Me.cmbColumnasCAB.Location = New System.Drawing.Point(104, 109)
        Me.cmbColumnasCAB.Name = "cmbColumnasCAB"
        Me.cmbColumnasCAB.Size = New System.Drawing.Size(273, 21)
        Me.cmbColumnasCAB.TabIndex = 149
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(394, 78)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(34, 26)
        Me.Button1.TabIndex = 151
        Me.Button1.Text = "..."
        Me.Button1.UseVisualStyleBackColor = True
        '
        'grdRelaciones
        '
        Me.grdRelaciones.AllowUserToAddRows = False
        Me.grdRelaciones.AllowUserToDeleteRows = False
        Me.grdRelaciones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdRelaciones.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ColExcel, Me.colListado})
        Me.grdRelaciones.Location = New System.Drawing.Point(434, 85)
        Me.grdRelaciones.Name = "grdRelaciones"
        Me.grdRelaciones.Size = New System.Drawing.Size(273, 125)
        Me.grdRelaciones.TabIndex = 158
        '
        'ColExcel
        '
        Me.ColExcel.HeaderText = "Columnas Excel"
        Me.ColExcel.Name = "ColExcel"
        '
        'colListado
        '
        Me.colListado.HeaderText = "Columnas Listado"
        Me.colListado.Name = "colListado"
        '
        'cmdRelacionaCAB
        '
        Me.cmdRelacionaCAB.Location = New System.Drawing.Point(247, 146)
        Me.cmdRelacionaCAB.Name = "cmdRelacionaCAB"
        Me.cmdRelacionaCAB.Size = New System.Drawing.Size(121, 25)
        Me.cmdRelacionaCAB.TabIndex = 157
        Me.cmdRelacionaCAB.Text = "Relacionar Columnas"
        Me.cmdRelacionaCAB.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(8, 152)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(90, 13)
        Me.Label4.TabIndex = 156
        Me.Label4.Text = "Columnas Listado"
        '
        'cmbColListado
        '
        Me.cmbColListado.AutoCompleteCustomSource.AddRange(New String() {"Producto", "Nombre", "Unidad"})
        Me.cmbColListado.FormattingEnabled = True
        Me.cmbColListado.Items.AddRange(New Object() {"CODIGO", "NOMBRE", "CTASUP", "TIPO", "FECHAREGISTRO", ""})
        Me.cmbColListado.Location = New System.Drawing.Point(104, 149)
        Me.cmbColListado.Name = "cmbColListado"
        Me.cmbColListado.Size = New System.Drawing.Size(137, 21)
        Me.cmbColListado.TabIndex = 155
        '
        'chkNoRep
        '
        Me.chkNoRep.AutoSize = True
        Me.chkNoRep.Checked = True
        Me.chkNoRep.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkNoRep.Location = New System.Drawing.Point(104, 179)
        Me.chkNoRep.Name = "chkNoRep"
        Me.chkNoRep.Size = New System.Drawing.Size(91, 17)
        Me.chkNoRep.TabIndex = 161
        Me.chkNoRep.Text = "No Repetidos"
        Me.chkNoRep.UseVisualStyleBackColor = True
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Location = New System.Drawing.Point(11, 216)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(696, 296)
        Me.TabControl1.TabIndex = 162
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.lblNoRegExcel)
        Me.TabPage1.Controls.Add(Me.grdCuentas)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(688, 270)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Datos Excel"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'lblNoRegExcel
        '
        Me.lblNoRegExcel.AutoSize = True
        Me.lblNoRegExcel.ForeColor = System.Drawing.Color.Blue
        Me.lblNoRegExcel.Location = New System.Drawing.Point(564, 234)
        Me.lblNoRegExcel.Name = "lblNoRegExcel"
        Me.lblNoRegExcel.Size = New System.Drawing.Size(74, 13)
        Me.lblNoRegExcel.TabIndex = 163
        Me.lblNoRegExcel.Text = "No. Registros:"
        '
        'grdCuentas
        '
        Me.grdCuentas.AllowUserToAddRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Yellow
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black
        Me.grdCuentas.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.grdCuentas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.grdCuentas.DefaultCellStyle = DataGridViewCellStyle2
        Me.grdCuentas.Location = New System.Drawing.Point(6, 6)
        Me.grdCuentas.Name = "grdCuentas"
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.Yellow
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black
        Me.grdCuentas.RowsDefaultCellStyle = DataGridViewCellStyle3
        Me.grdCuentas.Size = New System.Drawing.Size(632, 210)
        Me.grdCuentas.TabIndex = 160
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.lblNoRegRes)
        Me.TabPage2.Controls.Add(Me.grdResultante)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(688, 270)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Datos a Exportar"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'lblNoRegRes
        '
        Me.lblNoRegRes.AutoSize = True
        Me.lblNoRegRes.ForeColor = System.Drawing.Color.Blue
        Me.lblNoRegRes.Location = New System.Drawing.Point(564, 233)
        Me.lblNoRegRes.Name = "lblNoRegRes"
        Me.lblNoRegRes.Size = New System.Drawing.Size(74, 13)
        Me.lblNoRegRes.TabIndex = 162
        Me.lblNoRegRes.Text = "No. Registros:"
        '
        'grdResultante
        '
        Me.grdResultante.AllowUserToAddRows = False
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.Yellow
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black
        Me.grdResultante.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle4
        Me.grdResultante.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.grdResultante.DefaultCellStyle = DataGridViewCellStyle5
        Me.grdResultante.Location = New System.Drawing.Point(6, 6)
        Me.grdResultante.Name = "grdResultante"
        DataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.Yellow
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black
        Me.grdResultante.RowsDefaultCellStyle = DataGridViewCellStyle6
        Me.grdResultante.Size = New System.Drawing.Size(632, 210)
        Me.grdResultante.TabIndex = 161
        '
        'ToolStripMenu
        '
        Me.ToolStripMenu.AutoSize = False
        Me.ToolStripMenu.BackColor = System.Drawing.Color.Silver
        Me.ToolStripMenu.Dock = System.Windows.Forms.DockStyle.None
        Me.ToolStripMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnMnuOk, Me.btnMnuGenerar, Me.btnMnuCancelar, Me.btnMnuSalir})
        Me.ToolStripMenu.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow
        Me.ToolStripMenu.Location = New System.Drawing.Point(3, 0)
        Me.ToolStripMenu.Name = "ToolStripMenu"
        Me.ToolStripMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.ToolStripMenu.Size = New System.Drawing.Size(1194, 42)
        Me.ToolStripMenu.TabIndex = 163
        Me.ToolStripMenu.Text = "ToolStrip1"
        '
        'btnMnuOk
        '
        Me.btnMnuOk.ForeColor = System.Drawing.Color.Red
        Me.btnMnuOk.Image = Global.Fletera.My.Resources.Resources.Save1
        Me.btnMnuOk.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuOk.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuOk.Name = "btnMnuOk"
        Me.btnMnuOk.Size = New System.Drawing.Size(65, 39)
        Me.btnMnuOk.Text = "&GUARDAR"
        Me.btnMnuOk.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuGenerar
        '
        Me.btnMnuGenerar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuGenerar.Image = Global.Fletera.My.Resources.Resources._1462930414_filter_data
        Me.btnMnuGenerar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuGenerar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuGenerar.Name = "btnMnuGenerar"
        Me.btnMnuGenerar.Size = New System.Drawing.Size(62, 39)
        Me.btnMnuGenerar.Text = "GENERAR"
        Me.btnMnuGenerar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuCancelar
        '
        Me.btnMnuCancelar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuCancelar.Image = Global.Fletera.My.Resources.Resources.signos_f_059
        Me.btnMnuCancelar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuCancelar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuCancelar.Name = "btnMnuCancelar"
        Me.btnMnuCancelar.Size = New System.Drawing.Size(71, 39)
        Me.btnMnuCancelar.Text = "&CANCELAR"
        Me.btnMnuCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuSalir
        '
        Me.btnMnuSalir.ForeColor = System.Drawing.Color.Red
        Me.btnMnuSalir.Image = Global.Fletera.My.Resources.Resources.Salida2
        Me.btnMnuSalir.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuSalir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuSalir.Name = "btnMnuSalir"
        Me.btnMnuSalir.Size = New System.Drawing.Size(41, 39)
        Me.btnMnuSalir.Text = "&SALIR"
        Me.btnMnuSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'fAbreExcelFacturacion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(718, 527)
        Me.Controls.Add(Me.ToolStripMenu)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.chkNoRep)
        Me.Controls.Add(Me.grdRelaciones)
        Me.Controls.Add(Me.cmdRelacionaCAB)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.cmbColListado)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cmbColumnasCAB)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cmbHojasCAB)
        Me.Controls.Add(Me.btnOpenFile)
        Me.Controls.Add(Me.txtNomArchivo)
        Me.Controls.Add(Me.label12)
        Me.Name = "fAbreExcelFacturacion"
        CType(Me.grdRelaciones, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        CType(Me.grdCuentas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        CType(Me.grdResultante, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ToolStripMenu.ResumeLayout(False)
        Me.ToolStripMenu.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Private WithEvents btnOpenFile As Button
    Friend WithEvents txtNomArchivo As TextBox
    Friend WithEvents label12 As Label
    Private WithEvents openFileDialog1 As OpenFileDialog
    Friend WithEvents Label1 As Label
    Friend WithEvents cmbHojasCAB As ComboBox
    Friend WithEvents Label2 As Label
    Friend WithEvents cmbColumnasCAB As ComboBox
    Private WithEvents Button1 As Button
    Friend WithEvents grdRelaciones As DataGridView
    Friend WithEvents ColExcel As DataGridViewTextBoxColumn
    Friend WithEvents colListado As DataGridViewTextBoxColumn
    Friend WithEvents cmdRelacionaCAB As Button
    Friend WithEvents Label4 As Label
    Friend WithEvents cmbColListado As ComboBox
    Friend WithEvents chkNoRep As CheckBox
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents grdCuentas As DataGridView
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents grdResultante As DataGridView
    Friend WithEvents lblNoRegExcel As Label
    Friend WithEvents lblNoRegRes As Label
    Friend WithEvents ToolStripMenu As ToolStrip
    Friend WithEvents btnMnuOk As ToolStripButton
    Friend WithEvents btnMnuGenerar As ToolStripButton
    Friend WithEvents btnMnuCancelar As ToolStripButton
    Friend WithEvents btnMnuSalir As ToolStripButton
End Class
