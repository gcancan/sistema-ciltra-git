'Fecha: 14 / FEBRERO / 2016
'ROGER GALA PACHECO
'************************************************************************************************************************************
'*  Fecha Modificacion :  14 / febrero / 2017                                                                             
'*  
'************************************************************************************************************************************
Imports Microsoft.VisualBasic.PowerPacks

Public Class fInventarioLlantas
    Inherits System.Windows.Forms.Form
    Dim OpcionForma As TipoOpcionForma
    Dim TablaBd As String = "CatLlantas"
    Dim CampoLLave As String = "idLlanta"
    'Dim TablaBd2 As String = "CatTipoUniTrans"
    Public WithEvents MeStatus1 As ControlStatus.MeStatus
    Friend WithEvents GridTodos As System.Windows.Forms.DataGridView
    'Private Servicio As ServicioClass
    'Private TipoServ As TipoServClass

    Private Llanta As LlantasClass
    Dim llantaProd As LLantasProductoClass
    'Private UniTrans As UnidadesTranspClass
    Private TipoUniTrans As TipoUniTransClass
    Private Empresa As EmpresaClass
    'Private Almacen As AlmacenClass
    'Private Sucursal As SucursalClass
    Private Marca As MarcaClass
    Private TipoLLanta As TipoLlantaClass

    Friend WithEvents btnMnuOk As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuCancelar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuSalir As System.Windows.Forms.ToolStripButton
    Private _Tag As String
    Dim bEsIdentidad As Boolean = True
    Dim Salida As Boolean
    'Dim DsCombo As New DataTable
    Dim DsCombo As New DataSet
    Private _con As String
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents GrupoCaptura As GroupBox
    Friend WithEvents Label10 As Label
    Friend WithEvents txtCosto As TextBox
    Friend WithEvents Label11 As Label
    Friend WithEvents txtFactura As TextBox
    Friend WithEvents label1 As Label
    Friend WithEvents txtidLlanta As TextBox
    Friend WithEvents Label9 As Label
    Friend WithEvents dtpFecha As DateTimePicker
    Friend WithEvents Label7 As Label
    Friend WithEvents Label3 As Label
    Private _v As Boolean

    Private Prefijo As String
    Private UltimoConsecutivo As String
    Dim UniTrans As UnidadesTranspClass
    Dim TipUni As TipoUniTransClass

    Dim ClasLlan As ClasLlanClass
    Dim TablaEstruc As DataTable

    Dim rdbPosicion() As RadioButton
    Private _Usuario As String

    Dim vUnidadOrig As String
    Friend WithEvents Label15 As Label
    Friend WithEvents txtObserva As TextBox
    Friend WithEvents GpoPosiciones As GroupBox
    'Friend WithEvents ShapeContainer2 As ShapeContainer
    'Friend WithEvents LineShape1 As LineShape
    Friend WithEvents cmbidEmpresa As ComboBox
    Friend WithEvents txtTipoUni As TextBox
    Friend WithEvents Label12 As Label
    Friend WithEvents lblUbicacion As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents cmbidTipoLLanta As ComboBox
    Dim vPosUniOrig As Integer
    Friend WithEvents gpoProdLlanta As GroupBox
    Friend WithEvents Label6 As Label
    Friend WithEvents txtProfundidad As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents txtMedida As TextBox
    Friend WithEvents cmbidDisenio As ComboBox
    Friend WithEvents label2 As Label
    Friend WithEvents cmbidMarca As ComboBox
    Friend WithEvents Label4 As Label
    Friend WithEvents cmbidProductoLlanta As ComboBox
    Friend WithEvents Label16 As Label
    Friend WithEvents gpoRevitalizado As GroupBox
    Friend WithEvents Label14 As Label
    Friend WithEvents txtProfundidadAct As TextBox
    Friend WithEvents cmbidDisenioRev As ComboBox
    Friend WithEvents Label17 As Label
    Friend WithEvents cmbidCondicion As ComboBox
    Friend WithEvents Label20 As Label
    Friend WithEvents Label19 As Label
    Friend WithEvents txtKmActual As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents chkActivo As CheckBox
    Private Disenio As CatDiseniosClass
    Friend WithEvents Label21 As Label
    Friend WithEvents cmbidtipoUbicacion As ComboBox
    Private Condicion As CatCondicionesClass
    Private TipoUbicacion As CatLugaresClass
    Friend WithEvents cmbidUbicacion As ComboBox
    Friend WithEvents txtPosicion As NumericUpDown
    Friend WithEvents chkRevitalizado As CheckBox
    'Private TipoUbica As CatLugaresClass
    Private RelProvTos As RelProvTipOrdSerClass
    Private Almacen As AlmacenClass
    Private _idEmpresa As Integer
    Private _ubicacion As String
    Private _posicion As Integer

    Public idLlanta As String


#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer
    Friend WithEvents ToolStripMenu As System.Windows.Forms.ToolStrip
    Friend WithEvents txtMenuBusca As System.Windows.Forms.ToolStripTextBox


    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(fInventarioLlantas))
        Me.ToolStripMenu = New System.Windows.Forms.ToolStrip()
        Me.txtMenuBusca = New System.Windows.Forms.ToolStripTextBox()
        Me.btnMnuOk = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuCancelar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuSalir = New System.Windows.Forms.ToolStripButton()
        Me.MeStatus1 = New ControlStatus.MeStatus()
        Me.GridTodos = New System.Windows.Forms.DataGridView()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.GpoPosiciones = New System.Windows.Forms.GroupBox()
        'Me.ShapeContainer2 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        'Me.LineShape1 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.GrupoCaptura = New System.Windows.Forms.GroupBox()
        Me.txtPosicion = New System.Windows.Forms.NumericUpDown()
        Me.cmbidUbicacion = New System.Windows.Forms.ComboBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.cmbidtipoUbicacion = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.chkActivo = New System.Windows.Forms.CheckBox()
        Me.cmbidCondicion = New System.Windows.Forms.ComboBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.txtKmActual = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtProfundidadAct = New System.Windows.Forms.TextBox()
        Me.gpoRevitalizado = New System.Windows.Forms.GroupBox()
        Me.chkRevitalizado = New System.Windows.Forms.CheckBox()
        Me.cmbidDisenioRev = New System.Windows.Forms.ComboBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.gpoProdLlanta = New System.Windows.Forms.GroupBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtProfundidad = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtMedida = New System.Windows.Forms.TextBox()
        Me.cmbidDisenio = New System.Windows.Forms.ComboBox()
        Me.label2 = New System.Windows.Forms.Label()
        Me.cmbidMarca = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cmbidProductoLlanta = New System.Windows.Forms.ComboBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.cmbidTipoLLanta = New System.Windows.Forms.ComboBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtTipoUni = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.lblUbicacion = New System.Windows.Forms.Label()
        Me.cmbidEmpresa = New System.Windows.Forms.ComboBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txtObserva = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtCosto = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtFactura = New System.Windows.Forms.TextBox()
        Me.label1 = New System.Windows.Forms.Label()
        Me.txtidLlanta = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.dtpFecha = New System.Windows.Forms.DateTimePicker()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.ToolStripMenu.SuspendLayout()
        CType(Me.GridTodos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.GpoPosiciones.SuspendLayout()
        Me.GrupoCaptura.SuspendLayout()
        CType(Me.txtPosicion, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gpoRevitalizado.SuspendLayout()
        Me.gpoProdLlanta.SuspendLayout()
        Me.SuspendLayout()
        '
        'ToolStripMenu
        '
        Me.ToolStripMenu.AutoSize = False
        Me.ToolStripMenu.BackColor = System.Drawing.Color.Silver
        Me.ToolStripMenu.Dock = System.Windows.Forms.DockStyle.None
        Me.ToolStripMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.txtMenuBusca, Me.btnMnuOk, Me.btnMnuCancelar, Me.btnMnuSalir})
        Me.ToolStripMenu.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow
        Me.ToolStripMenu.Location = New System.Drawing.Point(3, 0)
        Me.ToolStripMenu.Name = "ToolStripMenu"
        Me.ToolStripMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.ToolStripMenu.Size = New System.Drawing.Size(1186, 42)
        Me.ToolStripMenu.TabIndex = 100
        Me.ToolStripMenu.Text = "ToolStrip1"
        '
        'txtMenuBusca
        '
        Me.txtMenuBusca.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.txtMenuBusca.Name = "txtMenuBusca"
        Me.txtMenuBusca.Size = New System.Drawing.Size(150, 42)
        Me.txtMenuBusca.ToolTipText = "Filtra Listado"
        '
        'btnMnuOk
        '
        Me.btnMnuOk.ForeColor = System.Drawing.Color.Red
        Me.btnMnuOk.Image = CType(resources.GetObject("btnMnuOk.Image"), System.Drawing.Image)
        Me.btnMnuOk.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuOk.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuOk.Name = "btnMnuOk"
        Me.btnMnuOk.Size = New System.Drawing.Size(56, 39)
        Me.btnMnuOk.Text = "&GRABAR"
        Me.btnMnuOk.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuCancelar
        '
        Me.btnMnuCancelar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuCancelar.Image = CType(resources.GetObject("btnMnuCancelar.Image"), System.Drawing.Image)
        Me.btnMnuCancelar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuCancelar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuCancelar.Name = "btnMnuCancelar"
        Me.btnMnuCancelar.Size = New System.Drawing.Size(71, 39)
        Me.btnMnuCancelar.Text = "&CANCELAR"
        Me.btnMnuCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuSalir
        '
        Me.btnMnuSalir.ForeColor = System.Drawing.Color.Red
        Me.btnMnuSalir.Image = CType(resources.GetObject("btnMnuSalir.Image"), System.Drawing.Image)
        Me.btnMnuSalir.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuSalir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuSalir.Name = "btnMnuSalir"
        Me.btnMnuSalir.Size = New System.Drawing.Size(41, 39)
        Me.btnMnuSalir.Text = "&SALIR"
        Me.btnMnuSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'MeStatus1
        '
        Me.MeStatus1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.MeStatus1.Location = New System.Drawing.Point(0, 493)
        Me.MeStatus1.Margin = New System.Windows.Forms.Padding(4)
        Me.MeStatus1.Name = "MeStatus1"
        Me.MeStatus1.Size = New System.Drawing.Size(1171, 26)
        Me.MeStatus1.TabIndex = 120
        '
        'GridTodos
        '
        Me.GridTodos.AllowUserToAddRows = False
        Me.GridTodos.AllowUserToDeleteRows = False
        Me.GridTodos.AllowUserToResizeRows = False
        Me.GridTodos.BackgroundColor = System.Drawing.Color.White
        Me.GridTodos.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.GridTodos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.GridTodos.Enabled = False
        Me.GridTodos.Location = New System.Drawing.Point(3, 45)
        Me.GridTodos.MultiSelect = False
        Me.GridTodos.Name = "GridTodos"
        Me.GridTodos.ReadOnly = True
        Me.GridTodos.RowHeadersVisible = False
        Me.GridTodos.Size = New System.Drawing.Size(162, 437)
        Me.GridTodos.TabIndex = 105
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Location = New System.Drawing.Point(171, 45)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(988, 441)
        Me.TabControl1.TabIndex = 101
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.GpoPosiciones)
        Me.TabPage1.Controls.Add(Me.GrupoCaptura)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(980, 415)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Datos"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'GpoPosiciones
        '
        'Me.GpoPosiciones.Controls.Add(Me.ShapeContainer2)
        'Me.GpoPosiciones.Location = New System.Drawing.Point(536, 6)
        'Me.GpoPosiciones.Name = "GpoPosiciones"
        'Me.GpoPosiciones.Size = New System.Drawing.Size(427, 349)
        'Me.GpoPosiciones.TabIndex = 107
        'Me.GpoPosiciones.TabStop = False
        'Me.GpoPosiciones.Text = "Posiciones"
        '
        'ShapeContainer2
        '
        'Me.ShapeContainer2.Location = New System.Drawing.Point(3, 16)
        'Me.ShapeContainer2.Margin = New System.Windows.Forms.Padding(0)
        'Me.ShapeContainer2.Name = "ShapeContainer2"
        'Me.ShapeContainer2.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.LineShape1})
        'Me.ShapeContainer2.Size = New System.Drawing.Size(421, 330)
        'Me.ShapeContainer2.TabIndex = 2
        'Me.ShapeContainer2.TabStop = False
        ''
        ''LineShape1
        ''
        'Me.LineShape1.BorderWidth = 10
        'Me.LineShape1.Name = "LineaPrinc"
        'Me.LineShape1.X1 = 186
        'Me.LineShape1.X2 = 186
        'Me.LineShape1.Y1 = 36
        'Me.LineShape1.Y2 = 280
        '
        'GrupoCaptura
        '
        Me.GrupoCaptura.Controls.Add(Me.txtPosicion)
        Me.GrupoCaptura.Controls.Add(Me.cmbidUbicacion)
        Me.GrupoCaptura.Controls.Add(Me.Label21)
        Me.GrupoCaptura.Controls.Add(Me.cmbidtipoUbicacion)
        Me.GrupoCaptura.Controls.Add(Me.Label8)
        Me.GrupoCaptura.Controls.Add(Me.chkActivo)
        Me.GrupoCaptura.Controls.Add(Me.cmbidCondicion)
        Me.GrupoCaptura.Controls.Add(Me.Label20)
        Me.GrupoCaptura.Controls.Add(Me.Label19)
        Me.GrupoCaptura.Controls.Add(Me.txtKmActual)
        Me.GrupoCaptura.Controls.Add(Me.Label14)
        Me.GrupoCaptura.Controls.Add(Me.txtProfundidadAct)
        Me.GrupoCaptura.Controls.Add(Me.gpoRevitalizado)
        Me.GrupoCaptura.Controls.Add(Me.gpoProdLlanta)
        Me.GrupoCaptura.Controls.Add(Me.cmbidTipoLLanta)
        Me.GrupoCaptura.Controls.Add(Me.Label13)
        Me.GrupoCaptura.Controls.Add(Me.txtTipoUni)
        Me.GrupoCaptura.Controls.Add(Me.Label12)
        Me.GrupoCaptura.Controls.Add(Me.lblUbicacion)
        Me.GrupoCaptura.Controls.Add(Me.cmbidEmpresa)
        Me.GrupoCaptura.Controls.Add(Me.Label15)
        Me.GrupoCaptura.Controls.Add(Me.txtObserva)
        Me.GrupoCaptura.Controls.Add(Me.Label10)
        Me.GrupoCaptura.Controls.Add(Me.txtCosto)
        Me.GrupoCaptura.Controls.Add(Me.Label11)
        Me.GrupoCaptura.Controls.Add(Me.txtFactura)
        Me.GrupoCaptura.Controls.Add(Me.label1)
        Me.GrupoCaptura.Controls.Add(Me.txtidLlanta)
        Me.GrupoCaptura.Controls.Add(Me.Label9)
        Me.GrupoCaptura.Controls.Add(Me.dtpFecha)
        Me.GrupoCaptura.Controls.Add(Me.Label7)
        Me.GrupoCaptura.Controls.Add(Me.Label3)
        Me.GrupoCaptura.Location = New System.Drawing.Point(6, 6)
        Me.GrupoCaptura.Name = "GrupoCaptura"
        Me.GrupoCaptura.Size = New System.Drawing.Size(524, 403)
        Me.GrupoCaptura.TabIndex = 150
        Me.GrupoCaptura.TabStop = False
        '
        'txtPosicion
        '
        Me.txtPosicion.Location = New System.Drawing.Point(272, 67)
        Me.txtPosicion.Name = "txtPosicion"
        Me.txtPosicion.Size = New System.Drawing.Size(39, 20)
        Me.txtPosicion.TabIndex = 6
        '
        'cmbidUbicacion
        '
        Me.cmbidUbicacion.AutoCompleteCustomSource.AddRange(New String() {"Regularizaci�n", "Vivienda"})
        Me.cmbidUbicacion.FormattingEnabled = True
        Me.cmbidUbicacion.Location = New System.Drawing.Point(86, 38)
        Me.cmbidUbicacion.Name = "cmbidUbicacion"
        Me.cmbidUbicacion.Size = New System.Drawing.Size(225, 21)
        Me.cmbidUbicacion.TabIndex = 3
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label21.Location = New System.Drawing.Point(216, 15)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(58, 13)
        Me.Label21.TabIndex = 147
        Me.Label21.Text = "Ubicaci�n:"
        '
        'cmbidtipoUbicacion
        '
        Me.cmbidtipoUbicacion.AutoCompleteCustomSource.AddRange(New String() {"Regularizaci�n", "Vivienda"})
        Me.cmbidtipoUbicacion.FormattingEnabled = True
        Me.cmbidtipoUbicacion.Location = New System.Drawing.Point(276, 12)
        Me.cmbidtipoUbicacion.Name = "cmbidtipoUbicacion"
        Me.cmbidtipoUbicacion.Size = New System.Drawing.Size(148, 21)
        Me.cmbidtipoUbicacion.TabIndex = 2
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label8.Location = New System.Drawing.Point(3, 356)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(40, 13)
        Me.Label8.TabIndex = 145
        Me.Label8.Text = "Activo:"
        '
        'chkActivo
        '
        Me.chkActivo.AutoSize = True
        Me.chkActivo.Checked = True
        Me.chkActivo.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkActivo.Location = New System.Drawing.Point(73, 356)
        Me.chkActivo.Name = "chkActivo"
        Me.chkActivo.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.chkActivo.Size = New System.Drawing.Size(15, 14)
        Me.chkActivo.TabIndex = 14
        Me.chkActivo.UseVisualStyleBackColor = True
        '
        'cmbidCondicion
        '
        Me.cmbidCondicion.AutoCompleteCustomSource.AddRange(New String() {"Regularizaci�n", "Vivienda"})
        Me.cmbidCondicion.FormattingEnabled = True
        Me.cmbidCondicion.Location = New System.Drawing.Point(388, 280)
        Me.cmbidCondicion.Name = "cmbidCondicion"
        Me.cmbidCondicion.Size = New System.Drawing.Size(112, 21)
        Me.cmbidCondicion.TabIndex = 11
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label20.Location = New System.Drawing.Point(273, 283)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(86, 13)
        Me.Label20.TabIndex = 143
        Me.Label20.Text = "Condici�n Llanta"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label19.Location = New System.Drawing.Point(268, 256)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(91, 13)
        Me.Label19.TabIndex = 141
        Me.Label19.Text = "Kilometraje Actual"
        Me.Label19.Visible = False
        '
        'txtKmActual
        '
        Me.txtKmActual.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtKmActual.Location = New System.Drawing.Point(388, 254)
        Me.txtKmActual.MaxLength = 50
        Me.txtKmActual.Name = "txtKmActual"
        Me.txtKmActual.Size = New System.Drawing.Size(112, 20)
        Me.txtKmActual.TabIndex = 10
        Me.txtKmActual.Visible = False
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label14.Location = New System.Drawing.Point(268, 227)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(100, 13)
        Me.Label14.TabIndex = 137
        Me.Label14.Text = "Profundidad Actual:"
        '
        'txtProfundidadAct
        '
        Me.txtProfundidadAct.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtProfundidadAct.Location = New System.Drawing.Point(388, 225)
        Me.txtProfundidadAct.MaxLength = 50
        Me.txtProfundidadAct.Name = "txtProfundidadAct"
        Me.txtProfundidadAct.Size = New System.Drawing.Size(112, 20)
        Me.txtProfundidadAct.TabIndex = 9
        '
        'gpoRevitalizado
        '
        Me.gpoRevitalizado.Controls.Add(Me.chkRevitalizado)
        Me.gpoRevitalizado.Controls.Add(Me.cmbidDisenioRev)
        Me.gpoRevitalizado.Controls.Add(Me.Label17)
        Me.gpoRevitalizado.ForeColor = System.Drawing.Color.Red
        Me.gpoRevitalizado.Location = New System.Drawing.Point(6, 254)
        Me.gpoRevitalizado.Name = "gpoRevitalizado"
        Me.gpoRevitalizado.Size = New System.Drawing.Size(256, 53)
        Me.gpoRevitalizado.TabIndex = 126
        Me.gpoRevitalizado.TabStop = False
        Me.gpoRevitalizado.Text = "Revitalizado"
        '
        'chkRevitalizado
        '
        Me.chkRevitalizado.AutoSize = True
        Me.chkRevitalizado.Location = New System.Drawing.Point(6, 22)
        Me.chkRevitalizado.Name = "chkRevitalizado"
        Me.chkRevitalizado.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.chkRevitalizado.Size = New System.Drawing.Size(15, 14)
        Me.chkRevitalizado.TabIndex = 137
        Me.chkRevitalizado.UseVisualStyleBackColor = True
        '
        'cmbidDisenioRev
        '
        Me.cmbidDisenioRev.AutoCompleteCustomSource.AddRange(New String() {"Regularizaci�n", "Vivienda"})
        Me.cmbidDisenioRev.FormattingEnabled = True
        Me.cmbidDisenioRev.Location = New System.Drawing.Point(91, 19)
        Me.cmbidDisenioRev.Name = "cmbidDisenioRev"
        Me.cmbidDisenioRev.Size = New System.Drawing.Size(159, 21)
        Me.cmbidDisenioRev.TabIndex = 118
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label17.Location = New System.Drawing.Point(39, 22)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(43, 13)
        Me.Label17.TabIndex = 136
        Me.Label17.Text = "Dise�o:"
        '
        'gpoProdLlanta
        '
        Me.gpoProdLlanta.Controls.Add(Me.Label6)
        Me.gpoProdLlanta.Controls.Add(Me.txtProfundidad)
        Me.gpoProdLlanta.Controls.Add(Me.Label5)
        Me.gpoProdLlanta.Controls.Add(Me.txtMedida)
        Me.gpoProdLlanta.Controls.Add(Me.cmbidDisenio)
        Me.gpoProdLlanta.Controls.Add(Me.label2)
        Me.gpoProdLlanta.Controls.Add(Me.cmbidMarca)
        Me.gpoProdLlanta.Controls.Add(Me.Label4)
        Me.gpoProdLlanta.Controls.Add(Me.cmbidProductoLlanta)
        Me.gpoProdLlanta.Controls.Add(Me.Label16)
        Me.gpoProdLlanta.ForeColor = System.Drawing.Color.Red
        Me.gpoProdLlanta.Location = New System.Drawing.Point(3, 114)
        Me.gpoProdLlanta.Name = "gpoProdLlanta"
        Me.gpoProdLlanta.Size = New System.Drawing.Size(515, 104)
        Me.gpoProdLlanta.TabIndex = 125
        Me.gpoProdLlanta.TabStop = False
        Me.gpoProdLlanta.Text = "Producto Llanta"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label6.Location = New System.Drawing.Point(241, 76)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(67, 13)
        Me.Label6.TabIndex = 133
        Me.Label6.Text = "Profundidad:"
        '
        'txtProfundidad
        '
        Me.txtProfundidad.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtProfundidad.Enabled = False
        Me.txtProfundidad.Location = New System.Drawing.Point(361, 73)
        Me.txtProfundidad.MaxLength = 50
        Me.txtProfundidad.Name = "txtProfundidad"
        Me.txtProfundidad.Size = New System.Drawing.Size(112, 20)
        Me.txtProfundidad.TabIndex = 110
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label5.Location = New System.Drawing.Point(6, 76)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(45, 13)
        Me.Label5.TabIndex = 131
        Me.Label5.Text = "Medida:"
        '
        'txtMedida
        '
        Me.txtMedida.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMedida.Enabled = False
        Me.txtMedida.Location = New System.Drawing.Point(67, 73)
        Me.txtMedida.MaxLength = 50
        Me.txtMedida.Name = "txtMedida"
        Me.txtMedida.Size = New System.Drawing.Size(112, 20)
        Me.txtMedida.TabIndex = 109
        '
        'cmbidDisenio
        '
        Me.cmbidDisenio.AutoCompleteCustomSource.AddRange(New String() {"Regularizaci�n", "Vivienda"})
        Me.cmbidDisenio.Enabled = False
        Me.cmbidDisenio.FormattingEnabled = True
        Me.cmbidDisenio.Location = New System.Drawing.Point(314, 41)
        Me.cmbidDisenio.Name = "cmbidDisenio"
        Me.cmbidDisenio.Size = New System.Drawing.Size(159, 21)
        Me.cmbidDisenio.TabIndex = 108
        '
        'label2
        '
        Me.label2.AutoSize = True
        Me.label2.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.label2.Location = New System.Drawing.Point(245, 49)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(43, 13)
        Me.label2.TabIndex = 128
        Me.label2.Text = "Dise�o:"
        '
        'cmbidMarca
        '
        Me.cmbidMarca.AutoCompleteCustomSource.AddRange(New String() {"Regularizaci�n", "Vivienda"})
        Me.cmbidMarca.Enabled = False
        Me.cmbidMarca.FormattingEnabled = True
        Me.cmbidMarca.Location = New System.Drawing.Point(67, 46)
        Me.cmbidMarca.Name = "cmbidMarca"
        Me.cmbidMarca.Size = New System.Drawing.Size(155, 21)
        Me.cmbidMarca.TabIndex = 107
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label4.Location = New System.Drawing.Point(18, 49)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(40, 13)
        Me.Label4.TabIndex = 126
        Me.Label4.Text = "Marca:"
        '
        'cmbidProductoLlanta
        '
        Me.cmbidProductoLlanta.AutoCompleteCustomSource.AddRange(New String() {"Regularizaci�n", "Vivienda"})
        Me.cmbidProductoLlanta.FormattingEnabled = True
        Me.cmbidProductoLlanta.Location = New System.Drawing.Point(67, 19)
        Me.cmbidProductoLlanta.Name = "cmbidProductoLlanta"
        Me.cmbidProductoLlanta.Size = New System.Drawing.Size(241, 21)
        Me.cmbidProductoLlanta.TabIndex = 7
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label16.Location = New System.Drawing.Point(3, 22)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(62, 13)
        Me.Label16.TabIndex = 124
        Me.Label16.Text = "Id Producto"
        '
        'cmbidTipoLLanta
        '
        Me.cmbidTipoLLanta.AutoCompleteCustomSource.AddRange(New String() {"Regularizaci�n", "Vivienda"})
        Me.cmbidTipoLLanta.FormattingEnabled = True
        Me.cmbidTipoLLanta.Location = New System.Drawing.Point(70, 224)
        Me.cmbidTipoLLanta.Name = "cmbidTipoLLanta"
        Me.cmbidTipoLLanta.Size = New System.Drawing.Size(155, 21)
        Me.cmbidTipoLLanta.TabIndex = 8
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label13.Location = New System.Drawing.Point(222, 74)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(50, 13)
        Me.Label13.TabIndex = 120
        Me.Label13.Text = "Posici�n:"
        '
        'txtTipoUni
        '
        Me.txtTipoUni.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTipoUni.Location = New System.Drawing.Point(425, 37)
        Me.txtTipoUni.MaxLength = 100
        Me.txtTipoUni.Name = "txtTipoUni"
        Me.txtTipoUni.ReadOnly = True
        Me.txtTipoUni.Size = New System.Drawing.Size(90, 20)
        Me.txtTipoUni.TabIndex = 102
        Me.txtTipoUni.Tag = "1"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label12.Location = New System.Drawing.Point(359, 40)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(31, 13)
        Me.Label12.TabIndex = 118
        Me.Label12.Text = "Tipo:"
        '
        'lblUbicacion
        '
        Me.lblUbicacion.AutoSize = True
        Me.lblUbicacion.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lblUbicacion.Location = New System.Drawing.Point(3, 45)
        Me.lblUbicacion.Name = "lblUbicacion"
        Me.lblUbicacion.Size = New System.Drawing.Size(55, 13)
        Me.lblUbicacion.TabIndex = 116
        Me.lblUbicacion.Text = "Ubicacion"
        '
        'cmbidEmpresa
        '
        Me.cmbidEmpresa.AutoCompleteCustomSource.AddRange(New String() {"Regularizaci�n", "Vivienda"})
        Me.cmbidEmpresa.FormattingEnabled = True
        Me.cmbidEmpresa.Items.AddRange(New Object() {"MORAL", "FISICA"})
        Me.cmbidEmpresa.Location = New System.Drawing.Point(83, 11)
        Me.cmbidEmpresa.Name = "cmbidEmpresa"
        Me.cmbidEmpresa.Size = New System.Drawing.Size(115, 21)
        Me.cmbidEmpresa.TabIndex = 1
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label15.Location = New System.Drawing.Point(3, 384)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(53, 13)
        Me.Label15.TabIndex = 109
        Me.Label15.Text = "Observa.:"
        '
        'txtObserva
        '
        Me.txtObserva.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObserva.Location = New System.Drawing.Point(73, 379)
        Me.txtObserva.MaxLength = 150
        Me.txtObserva.Name = "txtObserva"
        Me.txtObserva.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtObserva.Size = New System.Drawing.Size(442, 20)
        Me.txtObserva.TabIndex = 16
        Me.txtObserva.Text = "INVENTARIO"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label10.Location = New System.Drawing.Point(290, 332)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(37, 13)
        Me.Label10.TabIndex = 104
        Me.Label10.Text = "Costo:"
        '
        'txtCosto
        '
        Me.txtCosto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCosto.Location = New System.Drawing.Point(351, 329)
        Me.txtCosto.MaxLength = 50
        Me.txtCosto.Name = "txtCosto"
        Me.txtCosto.Size = New System.Drawing.Size(112, 20)
        Me.txtCosto.TabIndex = 13
        Me.txtCosto.Text = "1"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label11.Location = New System.Drawing.Point(3, 331)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(46, 13)
        Me.Label11.TabIndex = 102
        Me.Label11.Text = "Factura:"
        '
        'txtFactura
        '
        Me.txtFactura.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtFactura.Location = New System.Drawing.Point(73, 326)
        Me.txtFactura.MaxLength = 50
        Me.txtFactura.Name = "txtFactura"
        Me.txtFactura.Size = New System.Drawing.Size(150, 20)
        Me.txtFactura.TabIndex = 12
        Me.txtFactura.Text = "1"
        '
        'label1
        '
        Me.label1.AutoSize = True
        Me.label1.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.label1.Location = New System.Drawing.Point(6, 70)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(53, 13)
        Me.label1.TabIndex = 92
        Me.label1.Text = "ID Llanta:"
        '
        'txtidLlanta
        '
        Me.txtidLlanta.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtidLlanta.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtidLlanta.ForeColor = System.Drawing.Color.Blue
        Me.txtidLlanta.Location = New System.Drawing.Point(86, 65)
        Me.txtidLlanta.MaxLength = 10
        Me.txtidLlanta.Name = "txtidLlanta"
        Me.txtidLlanta.Size = New System.Drawing.Size(112, 22)
        Me.txtidLlanta.TabIndex = 4
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label9.Location = New System.Drawing.Point(244, 357)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(40, 13)
        Me.Label9.TabIndex = 85
        Me.Label9.Text = "Fecha:"
        '
        'dtpFecha
        '
        Me.dtpFecha.Location = New System.Drawing.Point(290, 355)
        Me.dtpFecha.Name = "dtpFecha"
        Me.dtpFecha.Size = New System.Drawing.Size(225, 20)
        Me.dtpFecha.TabIndex = 15
        Me.dtpFecha.Value = New Date(2018, 6, 18, 0, 0, 0, 0)
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label7.Location = New System.Drawing.Point(6, 15)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(51, 13)
        Me.Label7.TabIndex = 52
        Me.Label7.Text = "Empresa:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label3.Location = New System.Drawing.Point(3, 224)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(63, 13)
        Me.Label3.TabIndex = 20
        Me.Label3.Text = "Tipo Llanta:"
        '
        'fInventarioLlantas
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(1171, 519)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.GridTodos)
        Me.Controls.Add(Me.MeStatus1)
        Me.Controls.Add(Me.ToolStripMenu)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "fInventarioLlantas"
        Me.Text = "Inventario de Llantas"
        Me.ToolStripMenu.ResumeLayout(False)
        Me.ToolStripMenu.PerformLayout()
        CType(Me.GridTodos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.GpoPosiciones.ResumeLayout(False)
        Me.GrupoCaptura.ResumeLayout(False)
        Me.GrupoCaptura.PerformLayout()
        CType(Me.txtPosicion, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gpoRevitalizado.ResumeLayout(False)
        Me.gpoRevitalizado.PerformLayout()
        Me.gpoProdLlanta.ResumeLayout(False)
        Me.gpoProdLlanta.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

#End Region
    Sub New(ByVal vTag As String)
        InitializeComponent()
        _Tag = vTag
    End Sub

    Public Sub New(con As String, v As Boolean, nomUsuario As String,
                   Optional idEmpresa As Integer = 0,
                   Optional ubicacion As String = "", Optional posicion As Integer = 0)


        _con = con
        BD = New CapaDatos.UtilSQL(con, "Juan")
        InitializeComponent()
        _v = v
        _Usuario = nomUsuario
        _idEmpresa = idEmpresa
        _ubicacion = ubicacion
        _posicion = posicion
    End Sub
#Region "MenusToolStrip"
    Private Sub LimpiaVariables()
        SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")

    End Sub
    Private Sub LimpiaCampos()
        txtidLlanta.Text = ""
        cmbidTipoLLanta.SelectedIndex = 0
        cmbidMarca.SelectedIndex = 0
        chkActivo.Checked = False
        txtMedida.Text = ""
        txtProfundidad.Text = ""
        txtFactura.Text = ""
        txtCosto.Text = ""

        txtTipoUni.Text = ""
        txtPosicion.Value = 1

        LimpiaVariables()
    End Sub

    Private Sub ActivaCampos(ByVal valor As Boolean, ByVal vOpcion As TipoOpcActivaCampos)
        If vOpcion = TipoOpcActivaCampos.tOpcDESHABTODOS Then
            cmbidEmpresa.Enabled = True
            cmbidtipoUbicacion.Enabled = False
            cmbidUbicacion.Enabled = False
            txtidLlanta.Enabled = False
            cmbidTipoLLanta.Enabled = False
            'txtnomTipoUniTras.Enabled = False
            cmbidDisenio.Enabled = False
            txtMedida.Enabled = False
            txtProfundidad.Enabled = False
            txtFactura.Enabled = False
            txtCosto.Enabled = False

            cmbidMarca.Enabled = False
            chkActivo.Enabled = False

            dtpFecha.Enabled = False

            GpoPosiciones.Enabled = False


            txtTipoUni.Enabled = False
            txtPosicion.Enabled = False

            cmbidDisenioRev.Enabled = False

        ElseIf vOpcion = TipoOpcActivaCampos.tOpcINICIALIZA Then
            cmbidEmpresa.Enabled = valor
            cmbidtipoUbicacion.Enabled = Not valor
            txtidLlanta.Enabled = Not valor
            cmbidTipoLLanta.Enabled = valor
            'txtnomTipoUniTras.Enabled = valor
            cmbidDisenio.Enabled = valor
            txtMedida.Enabled = valor
            txtProfundidad.Enabled = valor
            txtFactura.Enabled = valor
            txtCosto.Enabled = valor

            cmbidMarca.Enabled = valor
            chkActivo.Enabled = valor

            dtpFecha.Enabled = valor

            cmbidDisenioRev.Enabled = valor

            If OpcionForma = TipoOpcionForma.tOpcInsertar Or OpcionForma = TipoOpcionForma.tOpcModificar Then
                GpoPosiciones.Enabled = valor
                cmbidUbicacion.Enabled = valor
                txtTipoUni.Enabled = valor
                txtPosicion.Enabled = valor
            Else
                GpoPosiciones.Enabled = False
                cmbidUbicacion.Enabled = False
                txtTipoUni.Enabled = False
                txtPosicion.Enabled = False

            End If
        ElseIf vOpcion = TipoOpcActivaCampos.tOpcOTRO Then
            cmbidEmpresa.Enabled = False
            cmbidtipoUbicacion.Enabled = False
            cmbidUbicacion.Enabled = False
            txtidLlanta.Enabled = True
            'txtNoSerie.Enabled = True
            txtPosicion.Enabled = True
            cmbidTipoLLanta.Enabled = True
            txtProfundidadAct.Enabled = True
            txtKmActual.Enabled = True
            cmbidCondicion.Enabled = True
            txtFactura.Enabled = True
            txtCosto.Enabled = True
            chkActivo.Enabled = True
            dtpFecha.Enabled = True
            txtObserva.Enabled = True
            gpoProdLlanta.Enabled = True
            gpoRevitalizado.Enabled = True
            cmbidDisenioRev.Enabled = False

        End If
    End Sub


    Private Sub ActivaBotones(ByVal valor As Boolean, Optional ByVal vOpcion As TipoOpcActivaBoton = TipoOpcActivaBoton.tOpcInicializa)
        GridTodos.Enabled = False
        If vOpcion = TipoOpcActivaBoton.tOpcConsulta Then
            btnMnuOk.Enabled = False
            btnMnuCancelar.Enabled = valor
            btnMnuSalir.Enabled = Not valor

        ElseIf vOpcion = TipoOpcActivaBoton.tOpcInicializa Then
            btnMnuOk.Enabled = valor
            btnMnuCancelar.Enabled = valor
            btnMnuSalir.Enabled = Not valor
            txtMenuBusca.Enabled = Not valor
            'GridTodos.Enabled = Not valor
        ElseIf vOpcion = TipoOpcActivaBoton.tOpcInsertar Then
            btnMnuOk.Enabled = valor
            btnMnuCancelar.Enabled = valor
            btnMnuSalir.Enabled = Not valor
        End If
    End Sub






#End Region
    Private Sub ActualizaGrid(ByVal vidLlanta As String, ByVal vidUnidadTrans As String)

        If vidLlanta = "" And vidUnidadTrans = "" Then
            GridTodos.DataSource = BD.ExecuteReturn("SELECT idLlanta, Posicion FROM " & TablaBd, TablaBd)
            Status(GridTodos.RowCount & " Registros", Me)
        Else
            '
            'GridTodos.DataSource = BD.ExecuteReturn("SELECT idLlanta FROM " & TablaBd & " WHERE idLlanta Like '%" & vidLlanta & "%'")
            GridTodos.DataSource = BD.ExecuteReturn("SELECT idLlanta, Posicion FROM " & TablaBd & " WHERE idLlanta Like '%" & vidLlanta &
                                                    "%' and idUnidadTrans Like '%" & vidUnidadTrans & "%'")
            'GridTodos.DataSource = BD.ExecuteReturn("SELECT idLlanta FROM " & TablaBd & " WHERE idUnidadTrans Like '%" & vidLlanta & "%'")
            Status(GridTodos.RowCount & " Registros obtenidos con el filtro %" & vidLlanta & "%", Me)
        End If
        'GridTodos.Columns(0).Width = GridTodos.Width - 20
        GridTodos.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
        GridTodos.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill

    End Sub

    Private Sub ActualizaGridxUnidad(ByVal vidUnidadTrans As String)
        If vidUnidadTrans = "" Then
            GridTodos.DataSource = BD.ExecuteReturn("SELECT idLlanta, Posicion FROM " & TablaBd, TablaBd)
            Status(GridTodos.RowCount & " Registros", Me)
        Else
            '
            'GridTodos.DataSource = BD.ExecuteReturn("SELECT idLlanta FROM " & TablaBd & " WHERE idLlanta Like '%" & vidLlanta & "%'")
            GridTodos.DataSource = BD.ExecuteReturn("SELECT idLlanta, Posicion FROM " & TablaBd & " WHERE idUbicacion Like '%" & vidUnidadTrans & "%'")
            Status(GridTodos.RowCount & " Registros obtenidos con el filtro %" & vidUnidadTrans & "%", Me)
        End If
        'GridTodos.Columns(0).Width = GridTodos.Width - 20
        GridTodos.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
        GridTodos.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill

    End Sub


    Private Sub OpcionMnuTools(ByVal vOpcion As TipoOpcionForma)
        If vOpcion <> TipoOpcionForma.tOpcImprimir And vOpcion <> TipoOpcionForma.tOpcEliminar Then
            OpcionForma = vOpcion
        End If
        txtMenuBusca.Enabled = True
        GridTodos.Enabled = False
        If vOpcion = TipoOpcionForma.tOpcConsultar Then
            'CONSULTA
            For Each ct As Control In GrupoCaptura.Controls
                ct.Enabled = False
                'If Not ct Is cmdBuscaClave Then
                '    ct.Enabled = False
                'Else
                '    ct.Enabled = True
                'End If
            Next
            GpoPosiciones.Enabled = False
            txtMenuBusca.Enabled = True
            btnMnuOk.Enabled = False
            btnMnuCancelar.Enabled = True
            btnMnuSalir.Enabled = True
            cmbidEmpresa.Enabled = True
            GeneraAutoacompletar()
        End If
    End Sub


    Private Sub txtMenuBusca_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtMenuBusca.KeyPress
        If Asc(e.KeyChar) = 39 Then e.Handled = True : Exit Sub 'Caracter ' Evita errores
        If Asc(e.KeyChar) = 13 Then
            GridTodos.Focus()
            e.Handled = True
        End If
    End Sub

    'Private Sub txtMenuBusca_KeyUp(sender As Object, e As KeyEventArgs) Handles txtMenuBusca.KeyUp
    '    If txtMenuBusca.Text.Trim = "" Then Exit Sub
    '    If e.KeyCode = Keys.Down Or e.KeyCode = Keys.Up Then
    '        GridTodos.Focus()
    '        e.Handled = True
    '        Exit Sub
    '    End If
    '    If e.KeyCode = Keys.Left Or e.KeyCode = Keys.Right Then Exit Sub
    '    ActualizaGrid(txtMenuBusca.Text)
    'End Sub

    Public Function AutoCompletar(ByVal Control As TextBox, ByVal strSql As String, ByVal NomCampo As String, ByVal NomTabla As String, Optional ByVal FiltroSinWhere As String = "")
        Dim objSql As New ToolSQLs
        Dim Coleccion As New AutoCompleteStringCollection
        Dim dt As DataTable = objSql.CreaTabla(NomTabla, strSql, FiltroSinWhere)
        For Each row As DataRow In dt.Rows
            Coleccion.Add(Convert.ToString(row(NomCampo)))
        Next
        With Control

            .AutoCompleteCustomSource = Coleccion
            .AutoCompleteMode = AutoCompleteMode.Suggest
            .AutoCompleteSource = AutoCompleteSource.CustomSource
        End With
        ' Aca le devuelvo los datos a recuperados de la Base
        Return Coleccion
    End Function

    Public Function AutoCompletarUnidadTransp(ByVal Control As ComboBox, ByVal Activo As Boolean, ByVal idEmpresa As Integer,
                                              ByVal Tipo As String, Optional ByVal FiltroSinWhere As String = "") As AutoCompleteStringCollection

        Dim Coleccion As New AutoCompleteStringCollection

        UniTrans = New UnidadesTranspClass(0)

        Dim dt As DataTable = UniTrans.TablaUnidadxTipo2(idEmpresa, FiltroSinWhere)
        'Recorrer y cargar los items para el Autocompletado
        For Each row As DataRow In dt.Rows
            Coleccion.Add(Convert.ToString(row("idUnidadTrans")))
        Next
        'Ajustamos el control TextBox o ComboBox para recibir los datos de la siguiente manera.
        With Control
            .DataSource = dt
            .DisplayMember = "idUnidadTrans"
            .ValueMember = "idUnidadTrans"


            .AutoCompleteCustomSource = Coleccion
            .AutoCompleteMode = AutoCompleteMode.Suggest
            .AutoCompleteSource = AutoCompleteSource.CustomSource
        End With

        ' Aca le devuelvo los datos a recuperados de la Base
        Return Coleccion
    End Function

    Private Sub GeneraAutoacompletar()
        'AutoCompletarUnidadTransp(txtUniTrans, True, 0, "tractor")
        'AutoCompletar(txtDise�o, "SELECT DISTINCT Dise�o AS dise�o FROM dbo.CatLLantasProducto", "Dise�o", "Dise�o")
        AutoCompletar(txtProfundidadAct, "SELECT DISTINCT Profundidad AS Profundidad FROM dbo.CatLLantasProducto", "Profundidad", "Profundidad")
        'AutoCompletar(txtMedida, "SELECT DISTINCT Medida AS Medida FROM dbo.CatLLantasProducto", "Medida", "Medida")

    End Sub
    Private Sub frmCatFamilias_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Salida = True
        dtpFecha.Value = Now
        OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
        If Inicio.CONSTR = "" Then
            Inicio.CONSTR = _con
        End If
        ActivaCampos(True, TipoOpcActivaCampos.tOpcDESHABTODOS)
        GeneraAutoacompletar()
        LlenaCombos()
        GridTodos.Enabled = False
        cmbidCondicion.SelectedIndex = 1
        TabControl1.SelectedIndex = 0
        SendKeys.Send("{TAB}")

        'En caso de traer datos
        If _v Then
            cmbidEmpresa.SelectedValue = _idEmpresa
            cmbidtipoUbicacion.SelectedValue = 3
            AutoCompletarUbicaciones(cmbidtipoUbicacion.SelectedValue)
            cmbidUbicacion.SelectedValue = _ubicacion
            txtPosicion.Value = _posicion
            cmbidEmpresa.Enabled = False
            CargaForm(cmbidUbicacion.SelectedValue, cmbidtipoUbicacion.Text)
            ActualizaGridxUnidad(cmbidUbicacion.SelectedValue)
            ActivaCampos(True, TipoOpcActivaCampos.tOpcOTRO)
            txtPosicion.Enabled = False
            txtidLlanta.SelectAll()
            txtidLlanta.Focus()



        End If
        'cmbidEmpresa.Focus()
        'cmbidEmpresa.Focus()
        Salida = False
    End Sub
    Private Sub GridTodos_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles GridTodos.CellEnter
        If Salida Then Exit Sub
        CargaForm(GridTodos.Rows(GridTodos.CurrentRow.Index).Cells(0).Value, UCase(TablaBd))
    End Sub
    Private Sub GridTodos_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles GridTodos.KeyPress
        If Asc(e.KeyChar) = 8 Then
            txtMenuBusca.Focus()
            e.Handled = True
        End If
    End Sub
    Private Sub CargaForm(ByVal idClave As String, ByVal NomTabla As String)
        Try
            Select Case UCase(NomTabla)
                Case UCase(TablaBd)
                    Llanta = New LlantasClass(idClave, OpcionLlanta.idLlanta)
                    With Llanta
                        txtidLlanta.Text = .idLlanta
                        If .Existe Then

                            'txtNoSerie.Text = .NoSerie
                            cmbidCondicion.SelectedValue = .idCondicion
                            cmbidEmpresa.SelectedValue = .idEmpresa
                            CargaForm(.idEmpresa, "Empresa")
                            dtpFecha.Value = .Fecha

                            llantaProd = New LLantasProductoClass(Llanta.idProductoLlanta)
                            If llantaProd.Existe Then
                                cmbidMarca.SelectedValue = llantaProd.idMarca
                                CargaForm(llantaProd.idMarca, "Marca")
                                cmbidDisenio.SelectedValue = llantaProd.idDisenio
                                txtMedida.Text = llantaProd.Medida
                                txtProfundidad.Text = llantaProd.Profundidad

                            End If



                            cmbidTipoLLanta.SelectedValue = .idTipoLLanta
                            CargaForm(.idTipoLLanta, "CatTipoLlanta")

                            txtFactura.Text = .Factura
                            txtCosto.Text = .Costo
                            chkActivo.Checked = .Activo

                            vUnidadOrig = .idUbicacion
                            cmbidUbicacion.SelectedValue = .idUbicacion
                            txtPosicion.Value = .Posicion
                            vPosUniOrig = .Posicion
                            TipoUbicacion = New CatLugaresClass(.idtipoUbicacion)
                            If TipoUbicacion.Existe Then
                                'CargaForm(.idUbicacion, "CatUnidadTrans")
                                CargaForm(.idUbicacion, TipoUbicacion.Descripcion)

                            End If





                            'txtCIDALMACEN_COM.Text = .CIDALMACEN_COM
                            If OpcionForma = TipoOpcionForma.tOpcInsertar Then
                                Status("La Llanta Con id: " & .idLlanta &
                                       " Ya existe!!!, Favor de modificarlo para poder dar de alta a otra Llanta", Me, Err:=True)
                                MessageBox.Show("La Llanta con id: " & .idLlanta &
                                                " YA EXISTE!!!, Favor de Cambiarlo y volver a intentarlo",
                                                "La Llanta con id: " & .idLlanta & " YA EXISTE!!!", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            End If
                        Else 'No existe 
                            If OpcionForma = TipoOpcionForma.tOpcInsertar Then

                                ActivaCampos(True, TipoOpcActivaCampos.tOpcINICIALIZA)
                                ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)
                                btnMnuOk.Enabled = True
                                cmbidTipoLLanta.Focus()
                                Status("Listo para Guardar nueva Unidad con id: " & .idLlanta, Me)
                            End If
                        End If
                    End With
                'Case UCase(TablaBd2)
                '    'aqui()
                '    TipoUniTrans = New TipoUniTransClass(Val(idClave))
                '    With TipoUniTrans
                '        txtidTipoLLanta.Text = .idTipoUniTras
                '        If .Existe Then
                '            txtDescripTLLA.Text = .nomTipoUniTras
                '            txtDise�o.Focus()
                '            'txtDescripcionUni.SelectAll()
                '        Else
                '            MsgBox("El Tipo de Unidad con id: " & txtidTipoLLanta.Text & " No Existe")
                '            txtidTipoLLanta.Focus()
                '            txtidTipoLLanta.SelectAll()
                '        End If
                '    End With
                Case UCase("LLANTAPROD")
                    llantaProd = New LLantasProductoClass(idClave)
                    If llantaProd.Existe Then
                        cmbidMarca.SelectedValue = llantaProd.idMarca
                        cmbidDisenio.SelectedValue = llantaProd.idDisenio
                        txtMedida.Text = llantaProd.Medida
                        txtProfundidad.Text = llantaProd.Profundidad
                    End If

                Case UCase("CatEmpresas")
                    'OK
                    Empresa = New EmpresaClass(Val(idClave))
                    If Empresa.Existe Then
                        Prefijo = Empresa.PrefLla
                        'ConsecutivoLlantas
                        Dim ultLlantas As New LlantasClass("0", OpcionLlanta.idLlanta)

                        UltimoConsecutivo = ultLlantas.ConsecutivoLlantas(Val(idClave), dtpFecha.Value.Year, dtpFecha.Value.Month)

                        AutoCompletarUnidadTransp(cmbidUbicacion, True, Val(idClave), "")
                    End If
                Case UCase("UNIDADES TRANS.")
                    UniTrans = New UnidadesTranspClass(idClave)
                    If UniTrans.Existe Then
                        TipUni = New TipoUniTransClass(UniTrans.idTipoUnidad)
                        If TipUni.Existe Then
                            txtTipoUni.Text = TipUni.Clasificacion
                            ClasLlan = New ClasLlanClass(TipUni.idClasLlan)
                            If ClasLlan.Existe Then
                                txtPosicion.Minimum = 1
                                txtPosicion.Maximum = TipUni.NoLlantas

                                DibujaEsquema(TipUni.idClasLlan, TipUni.NoLlantas)
                                If txtPosicion.Value <> 0 Then
                                    SeleccionaPosicion(Val(txtPosicion.Value))
                                End If
                            End If
                        End If
                    End If
            End Select
        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try

    End Sub
    Private Function Validar() As Boolean
        Validar = True
        If Not Inicio.ValidandoCampo(Trim(txtidLlanta.Text), TipoDato.TdCadena) Then
            'If Not bEsIdentidad Then
            MsgBox(MENSAJE_CLAVE, vbInformation, "Aviso" & Me.Text)
            If txtidLlanta.Enabled Then
                txtidLlanta.Focus()
                txtidLlanta.SelectAll()
            End If
            Validar = False
            'End If

        ElseIf Not Inicio.ValidandoCampo(Trim(txtProfundidadAct.Text), TipoDato.TdNumerico) Then
            MsgBox("La profundida no puede ser Vacio", vbInformation, "Aviso" & Me.Text)
            If txtProfundidadAct.Enabled Then
                txtProfundidadAct.SelectAll()
                txtProfundidadAct.Focus()
            End If
            Validar = False
            'ElseIf Not Inicio.ValidandoCampo(Trim(txtKmActual.Text), TipoDato.TdNumerico) Then
            '    MsgBox("El Kilometraje no puede ser Vacio", vbInformation, "Aviso" & Me.Text)
            '    If txtKmActual.Enabled Then
            '        txtKmActual.SelectAll()
            '        txtKmActual.Focus()
            '    End If
            '    Validar = False
        ElseIf Not Inicio.ValidandoCampo(Trim(txtFactura.Text), TipoDato.TdCadena) Then
            MsgBox("La Factura no puede ser Vacio", vbInformation, "Aviso" & Me.Text)
            If txtFactura.Enabled Then
                txtFactura.SelectAll()
                txtFactura.Focus()
            End If
            Validar = False
        ElseIf Not Inicio.ValidandoCampo(Trim(txtCosto.Text), TipoDato.TdNumerico) Then
            MsgBox("El Costo no puede ser Vacio", vbInformation, "Aviso" & Me.Text)
            If txtCosto.Enabled Then
                txtCosto.SelectAll()
                txtCosto.Focus()
            End If
            Validar = False
        ElseIf cmbidTipoLLanta.SelectedValue = 0 Then
            MsgBox("El Tipo de Unidad no Puede ser Vacio", vbInformation, "Aviso" & Me.Text)
            If cmbidTipoLLanta.Enabled Then
                cmbidTipoLLanta.Focus()
            End If
            Validar = False
        ElseIf cmbidCondicion.SelectedValue = 0 Then
            MsgBox("La Condici�n no Puede ser Vacia", vbInformation, "Aviso" & Me.Text)
            If cmbidTipoLLanta.Enabled Then
                cmbidTipoLLanta.Focus()
            End If
            Validar = False
        End If
    End Function

    'Private Sub txtidAlmacen_EnabledChanged(sender As Object, e As EventArgs) Handles txtidAlmacen.EnabledChanged
    '    cmdBuscaClave.Enabled = txtidAlmacen.Enabled
    'End Sub
    'CONTROLES

    Private Sub txtidAlmacen_KeyPress(sender As Object, e As KeyPressEventArgs)
        If Asc(e.KeyChar) = 13 Then
            If txtidLlanta.Text.Trim <> "" Then
                CargaForm(txtidLlanta.Text, UCase(TablaBd))
            End If
            e.Handled = True
        End If
    End Sub





    'Private Sub txtidSucursal_EnabledChanged(sender As Object, e As EventArgs) Handles txtidSucursal.EnabledChanged
    '    cmdBuscaClaveTPU.Enabled = txtidTipoUnidad.Enabled
    'End Sub










    Private Sub cmdCancelar_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        With TryCast(sender, Button)
            If .Enabled Then
                Me.CancelButton = sender
            End If
        End With
    End Sub
    Private Sub cmdTerminar_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        With TryCast(sender, Button)
            If .Enabled Then
                Me.CancelButton = sender
            End If
        End With
    End Sub

    Private Sub btnMnuOk_Click(sender As Object, e As EventArgs) Handles btnMnuOk.Click
        Dim CadCam As String = ""
        Try
            Salida = True
            If Not Validar() Then Exit Sub
            idLlanta = txtidLlanta.Text
            If GridTodos.RowCount = 0 Then
                'Servicio = New ServicioClass(0)
                Llanta = New LlantasClass("", OpcionLlanta.idLlanta)
            End If
            With Llanta
                If OpcionForma = TipoOpcionForma.tOpcModificar Then

                    'CadCam = .GetCambios(txtDise�o.Text, txtidTipoLLanta.Text, txtidMarca.Text, Val(txtidEmpresa.Text), Val(txtidSucursal.Text), cmbEstatus.SelectedValue, Val(txtIdAlmacen.Text))
                    CadCam = .GetCambios(cmbidEmpresa.SelectedValue, cmbidMarca.SelectedValue, cmbidDisenio.SelectedValue, txtMedida.Text, txtProfundidad.Text, cmbidTipoLLanta.SelectedValue,
                                         txtFactura.Text, dtpFecha.Value, txtCosto.Text, "", 1, "", dtpFecha.Value.Year, dtpFecha.Value.Month,
                                         chkActivo.Checked, txtObserva.Text, dtpFecha.Value, cmbidtipoUbicacion.SelectedValue, txtidLlanta.Text,
                                         "", cmbidCondicion.SelectedValue, txtProfundidadAct.Text, cmbidDisenioRev.SelectedValue)
                    If CadCam <> "" Then
                        'If MessageBox.Show("Esta completamente Seguro que Desea guardar los Cambios Realizados al Estado Con id: " & .IDMARCA & vbNewLine &
                        '                CadCam, "Confirma que desea guardar los Cambios Realizados???", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then Exit Sub
                        Status("Guardando Modificaciones de la Llanta Con id: " & .idLlanta, Me, 1, 2)


                        .Guardar(bEsIdentidad, cmbidEmpresa.SelectedValue, cmbidProductoLlanta.SelectedValue, cmbidTipoLLanta.SelectedValue,
                                         txtFactura.Text, FormatFecHora(dtpFecha.Value, True, True), txtCosto.Text, "", Val(txtPosicion.Text),
                                 cmbidUbicacion.SelectedValue, dtpFecha.Value.Year, dtpFecha.Value.Month,
                                         chkActivo.Checked, txtObserva.Text, FormatFecHora(dtpFecha.Value, True, True), cmbidtipoUbicacion.SelectedValue,
                                            txtidLlanta.Text, "", cmbidCondicion.SelectedValue, txtProfundidadAct.Text, cmbidDisenioRev.SelectedValue)

                        If vUnidadOrig <> cmbidUbicacion.SelectedValue Then
                            '.GuardaMov("MODI", FormatFecHora(Now, True), _Usuario, txtidLlanta.Text, vUnidadOrig, vPosUniOrig, cmbidUbicacion.SelectedValue, txtPosicion.Text, txtObserva.Text)
                        End If



                        Status("Llanta con Id: " & .idLlanta & " Modificado Con Exito!!!", Me, 2, 2)
                        MessageBox.Show("Llanta con Id: " & .idLlanta & " Modificada con exito!!!",
                                        "Accion terminada con exito!!!", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
                        ActualizaGrid(txtMenuBusca.Text, cmbidUbicacion.SelectedValue)
                    Else
                        Status("No se detectaron cambios para realizar!!!", Me, Err:=True)
                    End If
                ElseIf OpcionForma = TipoOpcionForma.tOpcInsertar Then
                    .Existe = False
                    '.DescripcionUni = txtDise�o.Text
                    If Not bEsIdentidad Then
                        .idLlanta = txtidLlanta.Text
                    End If
                    Status("Guardando Nueva Llanta: " & .idLlanta & ". . .", Me, 1, 2)
                    '.Guardar(txtDise�o.Text, txtidTipoLLanta.Text, txtidMarca.Text, Val(txtidEmpresa.Text), Val(txtidSucursal.Text), cmbEstatus.SelectedValue, Val(txtIdAlmacen.Text))
                    .Guardar(bEsIdentidad, cmbidEmpresa.SelectedValue, cmbidProductoLlanta.SelectedValue, cmbidTipoLLanta.SelectedValue,
                                         txtFactura.Text, FormatFecHora(dtpFecha.Value, True, True), txtCosto.Text, "", Val(txtPosicion.Text), cmbidUbicacion.SelectedValue, dtpFecha.Value.Year, dtpFecha.Value.Month,
                                         chkActivo.Checked, txtObserva.Text, FormatFecHora(dtpFecha.Value, True, True), cmbidtipoUbicacion.SelectedValue,
                                        txtidLlanta.Text, "", cmbidCondicion.SelectedValue, txtProfundidadAct.Text, cmbidDisenioRev.SelectedValue)

                    Llanta = New LlantasClass(txtidLlanta.Text, OpcionLlanta.idLlanta)
                    If Llanta.Existe Then
                        .GuardaHistorico(cmbidtipoUbicacion.SelectedValue, cmbidUbicacion.SelectedValue, Now, _Usuario, Llanta.idSisLlanta,
                                         txtidLlanta.Text, True, TipoDocumento.Inventario, "0", txtProfundidadAct.Text, cmbidDisenioRev.SelectedValue, txtPosicion.Value)
                    End If

                    '.GuardaMov("ALTA", FormatFecHora(Now, True), _Usuario, txtidLlanta.Text, cmbidUbicacion.SelectedValue, txtPosicion.Text, cmbidUbicacion.SelectedValue, txtPosicion.Text, txtObserva.Text)

                    Status("Llanta: " & .idLlanta & " Guardado con Exito!!!", Me, 2, 2)
                    MessageBox.Show("Nueva Llanta: " & .idLlanta & " Guardado con exito!!!",
                                    "Accion terminada con exito!!!", MessageBoxButtons.OK, MessageBoxIcon.Information)


                    Salida = True
                    BorrarCamposReutilizar()
                    CargaForm(cmbidUbicacion.SelectedValue, cmbidtipoUbicacion.Text)
                    ActualizaGridxUnidad(cmbidUbicacion.SelectedValue)
                    ActivaCampos(True, TipoOpcActivaCampos.tOpcOTRO)
                    btnMnuOk.Enabled = False
                    If _v Then

                        Close()
                    Else
                        txtidLlanta.SelectAll()
                        txtidLlanta.Focus()

                    End If

                    'OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
                    'ActualizaGridxUnidad(cmbidUbicacion.SelectedValue)
                    'btnMnuAltas.Enabled = False
                    'txtidLlanta.Focus()
                    'ActualizaGrid(txtMenuBusca.Text, cmbidUbicacion.SelectedValue)
                    Salida = False
                End If
            End With
            Salida = False
        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try
    End Sub


    Private Sub btnMnuCancelar_Click(sender As Object, e As EventArgs) Handles btnMnuCancelar.Click
        If txtidLlanta.Text <> "" Then
            CargaForm(txtidLlanta.Text, UCase(TablaBd))
        End If
        OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
        ActivaCampos(True, TipoOpcActivaCampos.tOpcDESHABTODOS)
        GeneraAutoacompletar()
        LlenaCombos()
        GridTodos.Enabled = False
        cmbidCondicion.SelectedIndex = 1
        TabControl1.SelectedIndex = 0
        AutoCompletarUbicaciones(cmbidtipoUbicacion.SelectedValue)
        'SendKeys.Send("{TAB}")
        LimpiaCampos()

        cmbidEmpresa.Focus()
    End Sub

    Private Sub btnMnuSalir_Click(sender As Object, e As EventArgs) Handles btnMnuSalir.Click
        Me.Close()
    End Sub

    Private Sub txtMenuBusca_TextChanged(sender As Object, e As EventArgs) Handles txtMenuBusca.TextChanged
        ActualizaGrid(txtMenuBusca.Text, cmbidUbicacion.SelectedValue)
    End Sub








    Private Sub GrupoCaptura_Enter(sender As Object, e As EventArgs)

    End Sub

    Private Function LlenaCombos() As Boolean
        Dim StrSql As String = ""
        LlenaCombos = True
        Try



            'DsCombo.Clear()
            'StrSql = "select 'ACT' as IdEstatus, 'ACTIVO' as Estatus " &
            '"union " &
            '"select 'BAJA' as IdEstatus, 'BAJA' as Estatus"

            'DsCombo = Inicio.LlenaCombos("IdEstatus", "Estatus", "CatEstatus",
            '                   TipoConexion.TcSQL, Inicio.CONSTR, Process.GetCurrentProcess.ProcessName,
            '                   KEY_CHECAERROR, KEY_ESTATUS, , StrSql)
            ''DsCombo = BD.ExecuteReturn(StrSql)
            'If Not DsCombo Is Nothing Then
            '    If DsCombo.Tables("CatEstatus").DefaultView.Count > 0 Then
            '        cmbEstatus.DataSource = DsCombo.Tables("CatEstatus")
            '        cmbEstatus.ValueMember = "IdEstatus"
            '        cmbEstatus.DisplayMember = "Estatus"
            '    End If
            'Else
            '    MsgBox("No se han dado de alta a Estatus ", vbInformation, "Aviso" & Me.Text)
            '    LlenaCombos = False
            '    Exit Function
            'End If

            'Empresas
            AutoCompletarEmpresas(cmbidEmpresa, False)
            'Marcas
            AutoCompletarMarcas(cmbidMarca, False)
            'Tipo Llantas
            AutoCompletarTipoLlanta(cmbidTipoLLanta, False)
            'Producto LLantas
            AutoCompletarProductoLlanta(cmbidProductoLlanta, False)
            'Disenios
            AutoCompletarDisenios(cmbidDisenio)
            'Condicion
            AutoCompletarCondicion(cmbidCondicion)
            'Tipos de Ubicacion
            AutoCompletarUbicacion(cmbidtipoUbicacion)
            'Disenios ReVitalizado
            AutoCompletarDisenios(cmbidDisenioRev)

        Catch ex As Exception

        End Try
    End Function




    'Private Sub txtidMarca_Leave(sender As Object, e As EventArgs)
    '    If Salida Then Exit Sub
    '    If txtidMarca.Text.Length = 0 Then
    '        txtidMarca.Focus()
    '        txtidMarca.SelectAll()
    '    Else
    '        If txtidMarca.Enabled Then
    '            CargaForm(txtidMarca.Text, "Marca")
    '            'txtidSucursal.Focus()
    '            'txtidSucursal.SelectAll()

    '        End If
    '    End If
    'End Sub

    'Private Sub txtidMarca_KeyDown(sender As Object, e As KeyEventArgs)
    '    If e.KeyCode = Keys.F3 Then
    '        cmdBuscaClaveMar_Click(sender, e)
    '    End If
    'End Sub

    Private Sub cmdBuscaClaveSuc_Click(sender As Object, e As EventArgs)
        'txtidSucursal.Text = DespliegaGrid("CatSucursal", Inicio.CONSTR, "id_sucursal")
        txtidSucursal_Leave(sender, e)
    End Sub

    Private Sub txtidSucursal_Leave(sender As Object, e As EventArgs)
        If Salida Then Exit Sub
        'If txtidSucursal.Text.Length = 0 Then
        '    txtidSucursal.Focus()
        '    txtidSucursal.SelectAll()
        'Else
        '    If txtidSucursal.Enabled Then
        '        CargaForm(txtidSucursal.Text, "Sucursal")
        '        txtIdAlmacen.Focus()
        '        txtIdAlmacen.SelectAll()

        '    End If
        'End If
    End Sub

    'Private Sub txtidSucursal_KeyDown(sender As Object, e As KeyEventArgs) Handles txtidSucursal.KeyDown, txtidTipoUnidad.KeyDown
    '    If e.KeyCode = Keys.F3 Then
    '        cmdBuscaClaveSuc_Click(sender, e)
    '    End If
    'End Sub


    Private Sub txtIdAlmacen_Leave(sender As Object, e As EventArgs)
        'If txtIdAlmacen.Enabled Then
        '    CargaForm(txtIdAlmacen.Text, "Almacen")
        '    cmbEstatus.Focus()


        'End If
    End Sub



    Private Sub txtidUnidadTrans_KeyPress(sender As Object, e As KeyPressEventArgs)
        If Asc(e.KeyChar) = 13 Then
            If txtidLlanta.Text.Trim <> "" Then
                CargaForm(txtidLlanta.Text, UCase(TablaBd))
            End If
            e.Handled = True
        End If
    End Sub

    Private Sub txtidSucursal_KeyDown(sender As Object, e As KeyEventArgs)
        If e.KeyCode = Keys.F3 Then
            cmdBuscaClaveSuc_Click(sender, e)
        End If
    End Sub










    'Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnGenerar.Click
    '    txtidLlanta.Text = Trim(Prefijo &
    '    dtpFecha.Value.Year.ToString.Substring(2) &
    '    IIf(dtpFecha.Value.Month < 10, "0" & dtpFecha.Value.Month, dtpFecha.Value.Month) &
    '    UltimoConsecutivo)
    'End Sub

    Private Sub txtidLlanta_TextChanged(sender As Object, e As EventArgs) Handles txtidLlanta.TextChanged

    End Sub

    Private Sub txtidLlanta_EnabledChanged(sender As Object, e As EventArgs) Handles txtidLlanta.EnabledChanged
        'cmdBuscaClave.Enabled = txtidLlanta.Enabled
    End Sub



    'Private Sub txtidLlanta_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtidLlanta.KeyPress
    '    If Asc(e.KeyChar) = 13 Then
    '        If txtidLlanta.Text.Trim <> "" Then
    '            CargaForm(txtidLlanta.Text, UCase(TablaBd))
    '            cmbidEmpresa.Focus()
    '        End If
    '        e.Handled = True
    '    End If
    'End Sub

    Private Sub txtidLlanta_Leave(sender As Object, e As EventArgs) Handles txtidLlanta.Leave
        If Salida Then Exit Sub
        Llanta = New LlantasClass(txtidLlanta.Text, OpcionLlanta.idLlanta)
        If Llanta.Existe Then
            MsgBox("El id Llanta: " & txtidLlanta.Text & " ya Existe")
            txtidLlanta.SelectAll()
            txtidLlanta.Focus()
        Else
            txtPosicion.Focus()
        End If
    End Sub

    Private Sub txtidLlanta_MouseDown(sender As Object, e As MouseEventArgs) Handles txtidLlanta.MouseDown
        txtidLlanta.Focus()
        txtidLlanta.SelectAll()
    End Sub

    Private Sub txtMedida_TextChanged(sender As Object, e As EventArgs) Handles txtMedida.TextChanged

    End Sub

    Private Sub txtMedida_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtMedida.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            txtProfundidad.Focus()
        End If
    End Sub

    Private Sub txtMedida_MouseDown(sender As Object, e As MouseEventArgs) Handles txtMedida.MouseDown
        txtMedida.Focus()
        txtMedida.SelectAll()
    End Sub

    Private Sub txtProfundidad_TextChanged(sender As Object, e As EventArgs) Handles txtProfundidad.TextChanged

    End Sub

    Private Sub txtProfundidad_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtProfundidad.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            txtFactura.Focus()
        End If
    End Sub

    Private Sub txtProfundidad_MouseDown(sender As Object, e As MouseEventArgs) Handles txtProfundidad.MouseDown
        txtProfundidad.Focus()
        txtProfundidad.SelectAll()
    End Sub

    Private Sub txtFactura_TextChanged(sender As Object, e As EventArgs) Handles txtFactura.TextChanged

    End Sub



    Private Sub txtFactura_MouseDown(sender As Object, e As MouseEventArgs) Handles txtFactura.MouseDown
        txtFactura.Focus()
        txtFactura.SelectAll()
    End Sub

    Private Sub txtCosto_TextChanged(sender As Object, e As EventArgs) Handles txtCosto.TextChanged

    End Sub

    Private Sub txtCosto_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCosto.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            chkActivo.Focus()

        End If
    End Sub

    Private Sub txtCosto_MouseDown(sender As Object, e As MouseEventArgs) Handles txtCosto.MouseDown
        txtCosto.Focus()
        txtCosto.SelectAll()
    End Sub




    Private Sub txtUniTrans_TextChanged(sender As Object, e As EventArgs)

    End Sub

    Private Sub txtUniTrans_Leave(sender As Object, e As EventArgs) Handles cmbidUbicacion.Leave
        'Private Sub txtUniTrans_Leave(sender As Object, e As EventArgs) Handles txtUniTrans.Leave
        If Salida Then Exit Sub
        If cmbidUbicacion.Enabled Then
            CargaForm(cmbidUbicacion.Text, "CatUnidadTrans")
            ActualizaGridxUnidad(cmbidUbicacion.SelectedValue)
            txtPosicion.Focus()
        End If
        'If txtUniTrans.Text.Length = 0 Then
        '    txtUniTrans.Focus()
        '    txtUniTrans.SelectAll()
        'Else
        '    If cmbidUbicacion.Enabled Then
        '        CargaForm(cmbidUbicacion.Text, "CatUnidadTrans")
        '        ActualizaGridxUnidad(txtUniTrans.Text)
        '        txtPosicion.Focus()
        '    End If
        'End If
    End Sub



    Private Sub txtUniTrans_KeyUp(sender As Object, e As KeyEventArgs) Handles cmbidUbicacion.KeyUp
        'Cuando tiene AutoCompletar no usa Keypress
        If e.KeyValue = Keys.Enter Then
            Salida = True
            'CargaForm(cmbidUbicacion.SelectedValue, "CatUnidadTrans")
            CargaForm(cmbidUbicacion.SelectedValue, cmbidtipoUbicacion.Text)
            ActualizaGridxUnidad(cmbidUbicacion.SelectedValue)
            ActivaCampos(True, TipoOpcActivaCampos.tOpcOTRO)
            txtidLlanta.SelectAll()
            txtidLlanta.Focus()
            'If GridTodos.Rows.Count = 0 Then
            'End If
            Salida = False

        End If
    End Sub


    'Private Sub txtidSucursal_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtidSucursal.KeyPress, txtidTipoUnidad.KeyPress
    '    If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
    '        txtIdAlmacen.Focus()
    '        txtIdAlmacen.SelectAll()
    '    End If

    'End Sub
    Private Sub DibujaEsquema(ByVal vidClasLlan As Integer, ByVal TotalLlantas As Integer)
        'Dim vNoEje As Integer
        'Dim vNoLlantas As Integer
        'Dim LlantxLado As Integer
        'Dim NoPosicion As Integer = 0
        'Dim Consecutivo As Integer

        'Dim FactorX As Integer = 71
        'Dim FactorY As Integer = 41

        'Dim PosXL1 As Integer = 29
        'Dim PosXL2 As Integer = 100
        'Dim PosY As Integer = 62

        'ReDim rdbPosicion(TotalLlantas - 1)

        'ClasLlan = New ClasLlanClass(0)
        'TablaEstruc = New DataTable

        'TablaEstruc = ClasLlan.TablaPosLlanta(vidClasLlan)

        'LimpiaPosiciones()

        'If Not TablaEstruc Is Nothing Then
        '    If TablaEstruc.Rows.Count > 0 Then
        '        For i = 0 To TablaEstruc.Rows.Count - 1
        '            vNoEje = TablaEstruc.DefaultView.Item(i).Item("EjeNo")
        '            vNoLlantas = TablaEstruc.DefaultView.Item(i).Item("NoLlantas")
        '            LlantxLado = vNoLlantas / 2



        '            'Posiciones Iniciales
        '            If LlantxLado = 1 Then
        '                PosXL1 = 29
        '                FactorX = 71
        '            Else
        '                PosXL1 = 5
        '                FactorX = 37
        '            End If

        '            For z = 0 To vNoLlantas - 1
        '                rdbPosicion(NoPosicion) = New RadioButton
        '                rdbPosicion(NoPosicion).Name = "pos" & NoPosicion
        '                rdbPosicion(NoPosicion).Text = NoPosicion + 1
        '                rdbPosicion(NoPosicion).AutoSize = True
        '                rdbPosicion(NoPosicion).Checked = False
        '                rdbPosicion(NoPosicion).Size = New System.Drawing.Size(31, 17)
        '                'rdbPosicion(i).Location = New Point(6, 4)
        '                rdbPosicion(NoPosicion).Location = New Point(PosXL1, PosY)

        '                AddHandler rdbPosicion(NoPosicion).Click, AddressOf RadioButtons_Click
        '                AddHandler rdbPosicion(NoPosicion).KeyDown, AddressOf ObjEnter_KeyDown
        '                'PanelControles.Controls.Add(rdbLogico1(i))
        '                GpoPosiciones.Controls.Add(rdbPosicion(NoPosicion))

        '                'PanelLogico1(i).Controls.Add(rdbLogico1(i))

        '                PosXL1 = PosXL1 + FactorX

        '                If z < vNoLlantas - 1 Then
        '                    NoPosicion = NoPosicion + 1
        '                End If


        '            Next
        '            PosY = PosY + FactorY
        '            NoPosicion = NoPosicion + 1


        '        Next

        '    End If
        'End If

        Dim vNoEje As Integer
        Dim vNoLlantas As Integer
        Dim LlantxLado As Integer
        Dim NoPosicion As Integer = 0

        'Dim FactorX As Integer = 71
        Dim FactorX As Integer = 201

        'Dim FactorY As Integer = 41
        Dim FactorY As Integer = 70

        Dim PosX As Integer = 29
        'Dim PosFacLinX As Integer = 201
        Dim PosFacLinX As Integer = 195

        'Dim PosY As Integer = 62
        Dim PosY As Integer = 37

        ReDim rdbPosicion(TotalLlantas - 1)

        LimpiaPosiciones()

        ClasLlan = New ClasLlanClass(0)
        TablaEstruc = New DataTable

        TablaEstruc = ClasLlan.TablaPosLlanta(vidClasLlan)
        If Not TablaEstruc Is Nothing Then
            If TablaEstruc.Rows.Count > 0 Then
                For i = 0 To TablaEstruc.Rows.Count - 1
                    vNoEje = TablaEstruc.DefaultView.Item(i).Item("EjeNo")
                    vNoLlantas = TablaEstruc.DefaultView.Item(i).Item("NoLlantas")
                    LlantxLado = vNoLlantas / 2
                    'Posiciones Iniciales
                    If LlantxLado = 0 Then
                        'PosX = 29
                        'FactorX = 71
                        PosX = 95
                        FactorX = 37
                    ElseIf LlantxLado = 1 Then
                        'PosX = 29
                        'FactorX = 71
                        PosX = 50
                        FactorX = 37
                    Else
                        'PosX = 5
                        'FactorX = 37
                        'PosX = 3
                        PosX = 5
                        'FactorX = 99
                        FactorX = 90
                    End If

                    For z = 0 To vNoLlantas - 1
                        rdbPosicion(NoPosicion) = New RadioButton
                        rdbPosicion(NoPosicion).Name = "pos" & NoPosicion

                        rdbPosicion(NoPosicion).Text = NoPosicion + 1
                        'rdbPosicion(NoPosicion).AutoSize = True
                        rdbPosicion(NoPosicion).Checked = False

                        'rdbPosicion(NoPosicion).Size = New System.Drawing.Size(31, 17)
                        rdbPosicion(NoPosicion).Size = New System.Drawing.Size(95, 64)

                        rdbPosicion(NoPosicion).Image = My.Resources.llanta2
                        rdbPosicion(NoPosicion).TextImageRelation = TextImageRelation.Overlay
                        rdbPosicion(NoPosicion).ImageAlign = ContentAlignment.MiddleCenter

                        'rdbPosicion(i).Location = New Point(6, 4)

                        rdbPosicion(NoPosicion).Location = New Point(PosX, PosY)

                        AddHandler rdbPosicion(NoPosicion).Click, AddressOf RadioButtons_Click
                        AddHandler rdbPosicion(NoPosicion).KeyDown, AddressOf ObjEnter_KeyDown
                        'PanelControles.Controls.Add(rdbLogico1(i))
                        GpoPosiciones.Controls.Add(rdbPosicion(NoPosicion))

                        'PanelLogico1(i).Controls.Add(rdbLogico1(i))

                        PosX = PosX + FactorX

                        If z < vNoLlantas - 1 Then
                            NoPosicion = NoPosicion + 1
                        End If

                        If LlantxLado = 1 Then
                            PosX = PosX + PosFacLinX - FactorX
                        End If

                    Next
                    PosY = PosY + FactorY
                    NoPosicion = NoPosicion + 1
                Next
            End If
        End If



    End Sub

    Private Sub RadioButtons_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        With TryCast(sender, RadioButton)
            txtPosicion.Text = .Text
            For i = 0 To rdbPosicion.Count - 1
                rdbPosicion(i).BackColor = SystemColors.Control
            Next
            .BackColor = Color.Yellow
            'MessageBox.Show("El Label Tiene Nombre: " & .Name & " y Tiene Escrito:" & .Text, "informacion del Label", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End With
    End Sub

    Private Sub ObjEnter_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        If e.KeyCode = 13 Then
            SendKeys.Send("{TAB}")
            e.Handled = True
        End If
    End Sub

    Private Sub SeleccionaPosicion(ByVal Posicion As Integer)



        If Posicion > rdbPosicion.Count Then
            MsgBox("La Estructura del tipo de unidad " & txtTipoUni.Text & " No cuenta con la posicion " & txtPosicion.Text & vbCrLf & "Favor de seleccionar la Posicion Correspondiente", MsgBoxStyle.Information, Me.Text)
            txtPosicion.Value = ""
            For i = 0 To rdbPosicion.Count - 1
                rdbPosicion(i).BackColor = SystemColors.Control
                rdbPosicion(i).Checked = False
            Next

        Else
            rdbPosicion(Posicion - 1).Checked = True
            For i = 0 To rdbPosicion.Count - 1
                rdbPosicion(i).BackColor = SystemColors.Control
            Next
            rdbPosicion(Posicion - 1).BackColor = Color.Yellow

        End If


    End Sub

    Private Sub LimpiaPosiciones()
        'For Each ct As Control In GrupoCaptura.Controls
        '    If TypeOf ct Is RadioButton Then
        '        ct.Dispose()
        '    End If
        'Next

        'Dim num_controles As Int32 = Me.TLPanel_convenio.Controls.Count - 1
        Dim num_controles As Int32 = Me.GpoPosiciones.Controls.Count - 1
        For n As Integer = num_controles To 0 Step -1
            Dim ctrl As Windows.Forms.Control = Me.GpoPosiciones.Controls(n)
            If TypeOf ctrl Is RadioButton Then

            End If
            Me.GpoPosiciones.Controls.Remove(ctrl)
            ctrl.Dispose()
        Next
        DibujaLinea()
    End Sub

    Private Sub DibujaLinea()
        'Dim canvas As New ShapeContainer
        'Dim theLine As New LineShape
        '' Set the form as the parent of the ShapeContainer.
        'canvas.Parent = Me.GpoPosiciones
        '' Set the ShapeContainer as the parent of the LineShape.
        'theLine.Parent = canvas
        '' Set the starting and ending coordinates for the line.
        'theLine.StartPoint = New System.Drawing.Point(70, 34)
        'theLine.EndPoint = New System.Drawing.Point(70, 191)

        'Dim canvas As New ShapeContainer
        'Dim theLine As New LineShape
        ' Set the form as the parent of the ShapeContainer.
        'canvas.Parent = Me.GpoPosiciones
        '' Set the ShapeContainer as the parent of the LineShape.
        'theLine.Parent = canvas
        '' Set the starting and ending coordinates for the line.
        'theLine.BorderWidth = 10
        'theLine.StartPoint = New System.Drawing.Point(183, 25)
        'theLine.EndPoint = New System.Drawing.Point(183, 300)
    End Sub

    Private Sub txtPosicion_TextChanged(sender As Object, e As EventArgs)

    End Sub

    'Private Sub txtPosicion_KeyPress(sender As Object, e As KeyPressEventArgs)
    '    'Private Sub txtPosicion_KeyPress(sender As Object, e As KeyPressEventArgs) 
    '    If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
    '        SeleccionaPosicion(txtPosicion.Value)
    '    End If
    'End Sub

    Private Sub dtpFecha_ValueChanged(sender As Object, e As EventArgs) Handles dtpFecha.ValueChanged

    End Sub

    Private Sub dtpFecha_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFecha.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            If OpcionForma = TipoOpcionForma.tOpcInsertar Then
                'btnGenerar.Focus()
            Else
                cmbidMarca.Focus()
            End If

        End If
    End Sub


    Private Sub txtMedida_KeyUp(sender As Object, e As KeyEventArgs) Handles txtMedida.KeyUp
        If e.KeyValue = Keys.Enter Then
            txtProfundidad.Focus()
        End If
    End Sub

    Private Sub txtProfundidad_KeyUp(sender As Object, e As KeyEventArgs) Handles txtProfundidad.KeyUp
        If e.KeyValue = Keys.Enter Then
            txtFactura.Focus()
        End If
    End Sub
    Public Function AutoCompletarEmpresas(ByVal Control As ComboBox, ByVal bIncluyeTodos As Boolean) As AutoCompleteStringCollection

        Dim Coleccion As New AutoCompleteStringCollection
        Dim dt As DataTable


        Empresa = New EmpresaClass(0)
        dt = Empresa.TablaEmpresas(bIncluyeTodos)

        'Recorrer y cargar los items para el Autocompletado
        For Each row As DataRow In dt.Rows
            Coleccion.Add(Convert.ToString(row("RazonSocial")))
        Next
        'Ajustamos el control TextBox o ComboBox para recibir los datos de la siguiente manera.
        With Control
            .DataSource = dt
            .DisplayMember = "RazonSocial"
            .ValueMember = "idEmpresa"


            .AutoCompleteCustomSource = Coleccion
            .AutoCompleteMode = AutoCompleteMode.Suggest
            .AutoCompleteSource = AutoCompleteSource.CustomSource
        End With

        ' Aca le devuelvo los datos a recuperados de la Base
        Return Coleccion

    End Function

    Private Sub GridTodos_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles GridTodos.CellContentClick

    End Sub
    Public Function AutoCompletarMarcas(ByVal Control As ComboBox, ByVal bIncluyeTodos As Boolean) As AutoCompleteStringCollection

        Dim Coleccion As New AutoCompleteStringCollection
        Dim dt As DataTable


        Marca = New MarcaClass(0)
        dt = Marca.TablaMarcas(bIncluyeTodos)

        'Recorrer y cargar los items para el Autocompletado
        For Each row As DataRow In dt.Rows
            Coleccion.Add(Convert.ToString(row("NombreMarca")))
        Next
        'Ajustamos el control TextBox o ComboBox para recibir los datos de la siguiente manera.
        With Control
            .DataSource = dt
            .DisplayMember = "NombreMarca"
            .ValueMember = "idMarca"


            .AutoCompleteCustomSource = Coleccion
            .AutoCompleteMode = AutoCompleteMode.Suggest
            .AutoCompleteSource = AutoCompleteSource.CustomSource
        End With

        ' Aca le devuelvo los datos a recuperados de la Base
        Return Coleccion

    End Function

    Public Function AutoCompletarTipoLlanta(ByVal Control As ComboBox, ByVal bIncluyeTodos As Boolean) As AutoCompleteStringCollection

        Dim Coleccion As New AutoCompleteStringCollection
        Dim dt As DataTable


        TipoLLanta = New TipoLlantaClass(0)
        dt = TipoLLanta.TablaTipoLlantas(bIncluyeTodos)

        'Recorrer y cargar los items para el Autocompletado
        For Each row As DataRow In dt.Rows
            Coleccion.Add(Convert.ToString(row("Descripcion")))
        Next
        'Ajustamos el control TextBox o ComboBox para recibir los datos de la siguiente manera.
        With Control
            .DataSource = dt
            .DisplayMember = "Descripcion"
            .ValueMember = "idTipoLLanta"


            .AutoCompleteCustomSource = Coleccion
            .AutoCompleteMode = AutoCompleteMode.Suggest
            .AutoCompleteSource = AutoCompleteSource.CustomSource
        End With

        ' Aca le devuelvo los datos a recuperados de la Base
        Return Coleccion

    End Function
    Public Function AutoCompletarProductoLlanta(ByVal Control As ComboBox, ByVal bIncluyeTodos As Boolean) As AutoCompleteStringCollection

        Dim Coleccion As New AutoCompleteStringCollection
        Dim dt As DataTable


        llantaProd = New LLantasProductoClass(0)
        dt = llantaProd.TablaLLantasProducto(bIncluyeTodos)

        'Recorrer y cargar los items para el Autocompletado
        For Each row As DataRow In dt.Rows
            Coleccion.Add(Convert.ToString(row("Descripcion")))
        Next
        'Ajustamos el control TextBox o ComboBox para recibir los datos de la siguiente manera.
        With Control
            .DataSource = dt
            .DisplayMember = "Descripcion"
            .ValueMember = "idProductoLlanta"


            .AutoCompleteCustomSource = Coleccion
            .AutoCompleteMode = AutoCompleteMode.Suggest
            .AutoCompleteSource = AutoCompleteSource.CustomSource
        End With

        ' Aca le devuelvo los datos a recuperados de la Base
        Return Coleccion

    End Function
    Public Function AutoCompletarDisenios(ByVal Control As ComboBox) As AutoCompleteStringCollection

        Dim Coleccion As New AutoCompleteStringCollection
        Dim dt As DataTable

        Disenio = New CatDiseniosClass(0)
        dt = Disenio.TablaDisenios(False)

        'Recorrer y cargar los items para el Autocompletado
        For Each row As DataRow In dt.Rows
            Coleccion.Add(Convert.ToString(row("Descripcion")))
        Next
        'Ajustamos el control TextBox o ComboBox para recibir los datos de la siguiente manera.
        With Control
            .DataSource = dt
            .DisplayMember = "Descripcion"
            .ValueMember = "idDisenio"


            .AutoCompleteCustomSource = Coleccion
            .AutoCompleteMode = AutoCompleteMode.Suggest
            .AutoCompleteSource = AutoCompleteSource.CustomSource
        End With

        ' Aca le devuelvo los datos a recuperados de la Base
        Return Coleccion

    End Function
    Public Function AutoCompletarCondicion(ByVal Control As ComboBox) As AutoCompleteStringCollection

        Dim Coleccion As New AutoCompleteStringCollection
        Dim dt As DataTable

        Condicion = New CatCondicionesClass(0)
        dt = Condicion.TablaCondiciones(False)

        'Recorrer y cargar los items para el Autocompletado
        For Each row As DataRow In dt.Rows
            Coleccion.Add(Convert.ToString(row("Descripcion")))
        Next
        'Ajustamos el control TextBox o ComboBox para recibir los datos de la siguiente manera.
        With Control
            .DataSource = dt
            .DisplayMember = "Descripcion"
            .ValueMember = "idCondicion"


            .AutoCompleteCustomSource = Coleccion
            .AutoCompleteMode = AutoCompleteMode.Suggest
            .AutoCompleteSource = AutoCompleteSource.CustomSource
        End With

        ' Aca le devuelvo los datos a recuperados de la Base
        Return Coleccion

    End Function
    '
    Public Function AutoCompletarUbicacion(ByVal Control As ComboBox) As AutoCompleteStringCollection

        Dim Coleccion As New AutoCompleteStringCollection
        Dim dt As DataTable

        TipoUbicacion = New CatLugaresClass(0)
        dt = TipoUbicacion.TablaLugares(False, " Descripcion <> 'PROVEEDOR'")

        'Recorrer y cargar los items para el Autocompletado
        For Each row As DataRow In dt.Rows
            Coleccion.Add(Convert.ToString(row("Descripcion")))
        Next
        'Ajustamos el control TextBox o ComboBox para recibir los datos de la siguiente manera.
        With Control
            .DataSource = dt
            .DisplayMember = "Descripcion"
            .ValueMember = "idLugar"


            .AutoCompleteCustomSource = Coleccion
            .AutoCompleteMode = AutoCompleteMode.Suggest
            .AutoCompleteSource = AutoCompleteSource.CustomSource
        End With

        ' Aca le devuelvo los datos a recuperados de la Base
        Return Coleccion

    End Function

    Private Sub cmbidtipoUbicacion_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbidtipoUbicacion.SelectedIndexChanged

    End Sub

    Private Sub cmbidtipoUbicacion_KeyUp(sender As Object, e As KeyEventArgs) Handles cmbidtipoUbicacion.KeyUp
        If e.KeyValue = Keys.Enter Then
            cmbidtipoUbicacion.Enabled = False
            cmbidUbicacion.Enabled = True
            AutoCompletarUbicaciones(cmbidtipoUbicacion.SelectedValue)
            cmbidUbicacion.Focus()
        End If
    End Sub

    Private Sub cmbidtipoUbicacion_Leave(sender As Object, e As EventArgs) Handles cmbidtipoUbicacion.Leave
        If Salida Then Exit Sub
        cmbidtipoUbicacion.Enabled = False
        cmbidUbicacion.Enabled = True
        cmbidUbicacion.Focus()
    End Sub

    Private Sub AutoCompletarUbicaciones(ByVal idLugar As Integer)
        TipoUbicacion = New CatLugaresClass(idLugar, TipoOrdenServicio.LLANTAS)

        If TipoUbicacion.Existe Then
            lblUbicacion.Text = TipoUbicacion.Descripcion

            If TipoUbicacion.NomTabla = "CATALMACEN" Then
                AutoCompletarAlmacen(cmbidUbicacion)
            ElseIf TipoUbicacion.NomTabla = "CATPROVEEDORES" Then
                AutoCompletarRelProvTOS(cmbidUbicacion)
            ElseIf TipoUbicacion.NomTabla = "CATUNIDADTRANS" Then
                AutoCompletarUnidadTransp(cmbidUbicacion, True, 0, "tractor")
            End If
        End If

    End Sub

    Public Function AutoCompletarRelProvTOS(ByVal Control As ComboBox) As AutoCompleteStringCollection

        Dim Coleccion As New AutoCompleteStringCollection
        Dim dt As DataTable

        RelProvTos = New RelProvTipOrdSerClass(0, 0)
        dt = RelProvTos.TablaProveedoresxTos(True, TipoOrdenServicio.LLANTAS)
        'Recorrer y cargar los items para el Autocompletado
        For Each row As DataRow In dt.Rows
            Coleccion.Add(Convert.ToString(row("RazonSocial")))
        Next
        'Ajustamos el control TextBox o ComboBox para recibir los datos de la siguiente manera.
        With Control
            .DataSource = dt
            .DisplayMember = "RazonSocial"
            .ValueMember = "idProveedor"


            .AutoCompleteCustomSource = Coleccion
            .AutoCompleteMode = AutoCompleteMode.Suggest
            .AutoCompleteSource = AutoCompleteSource.CustomSource
        End With

        ' Aca le devuelvo los datos a recuperados de la Base
        Return Coleccion

    End Function

    Private Sub cmbidUbicacion_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbidUbicacion.SelectedIndexChanged

    End Sub

    Private Sub cmbidEmpresa_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbidEmpresa.SelectedIndexChanged

    End Sub

    Private Sub cmbidEmpresa_KeyUp(sender As Object, e As KeyEventArgs) Handles cmbidEmpresa.KeyUp
        If e.KeyValue = Keys.Enter Then
            cmbidEmpresa.Enabled = False
            cmbidtipoUbicacion.Enabled = True
            cmbidtipoUbicacion.Focus()

        End If
    End Sub

    Private Sub cmbidEmpresa_Leave(sender As Object, e As EventArgs) Handles cmbidEmpresa.Leave
        If Salida Then Exit Sub
        cmbidEmpresa.Enabled = False
        cmbidtipoUbicacion.Enabled = True
        cmbidtipoUbicacion.Focus()
    End Sub

    Private Sub cmbidUbicacion_PaddingChanged(sender As Object, e As EventArgs) Handles cmbidUbicacion.PaddingChanged

    End Sub

    Private Sub txtidLlanta_KeyUp(sender As Object, e As KeyEventArgs) Handles txtidLlanta.KeyUp
        If e.KeyValue = Keys.Enter Then
            'Checar si Existe
            Salida = True
            Llanta = New LlantasClass(txtidLlanta.Text, OpcionLlanta.idLlanta)
            If Llanta.Existe Then
                MsgBox("El id Llanta: " & txtidLlanta.Text & " ya Existe")
                txtidLlanta.SelectAll()
                txtidLlanta.Focus()
            Else
                If _v Then
                    txtPosicion_KeyUp(sender, e)
                Else
                    txtPosicion.Focus()
                End If

            End If
            Salida = False
        End If
    End Sub

    Private Sub txtNoSerie_TextChanged(sender As Object, e As EventArgs)

    End Sub

    'Private Sub txtNoSerie_KeyUp(sender As Object, e As KeyEventArgs)
    '    If e.KeyValue = Keys.Enter Then
    '        Salida = True
    '        Llanta = New LlantasClass(txtNoSerie.Text, OpcionLlanta.NoSerie)
    '        If Llanta.Existe Then
    '            MsgBox("El No Serie: " & txtNoSerie.Text & " ya Existe")
    '            txtNoSerie.SelectAll()
    '            txtNoSerie.Focus()
    '        Else
    '            txtPosicion.Select()
    '            txtPosicion.Focus()
    '        End If
    '        Salida = False
    '    End If
    'End Sub

    Private Sub txtPosicion_ValueChanged(sender As Object, e As EventArgs) Handles txtPosicion.ValueChanged
        If Salida Then Exit Sub
        If rdbPosicion.Length > 0 Then
            SeleccionaPosicion(Val(txtPosicion.Value))

            'For index = 0 To rdbPosicion.Length - 1
            '    If rdbPosicion(index).Text = txtPosicion.Value Then
            '        rdbPosicion(index).Checked = True
            '        Exit For
            '    End If
            'Next

        End If

    End Sub

    Private Sub cmbidProductoLlanta_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbidProductoLlanta.SelectedIndexChanged
        'If Salida Then Exit Sub
        'CargaForm(cmbidProductoLlanta.SelectedValue, "LLANTAPROD")

    End Sub



    Private Sub txtPosicion_KeyUp(sender As Object, e As KeyEventArgs) Handles txtPosicion.KeyUp
        If e.KeyValue = Keys.Enter Then
            Salida = True
            TipoUbicacion = New CatLugaresClass(cmbidtipoUbicacion.SelectedValue, TipoOrdenServicio.LLANTAS)
            If TipoUbicacion.Existe Then
                If TipoUbicacion.NomTabla = "CATUNIDADTRANS" Then
                    Llanta = New LlantasClass(txtPosicion.Value, OpcionLlanta.Posicion, cmbidUbicacion.SelectedValue)
                    If Llanta.Existe Then
                        MsgBox("La Posicion: " & txtPosicion.Value & " ya la ocupa la unidad: " & cmbidUbicacion.SelectedValue & " con la llanta: " & Llanta.idLlanta)
                        txtPosicion.Focus()
                    Else
                        OpcionForma = TipoOpcionForma.tOpcInsertar
                        btnMnuOk.Enabled = True
                        If rdbPosicion.Length > 0 Then
                            SeleccionaPosicion(Val(txtPosicion.Value))
                            cmbidProductoLlanta.Focus()
                        End If
                    End If
                End If
            End If
            Salida = False

        End If
    End Sub

    Private Sub cmbidProductoLlanta_KeyUp(sender As Object, e As KeyEventArgs) Handles cmbidProductoLlanta.KeyUp
        If e.KeyValue = Keys.Enter Then
            'CargaForm(cmbidProductoLlanta.SelectedValue, "LLANTAPROD")
            cmbidTipoLLanta.Focus()
        End If
    End Sub

    Private Sub cmbidTipoLLanta_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbidTipoLLanta.SelectedIndexChanged

    End Sub

    Private Sub cmbidTipoLLanta_KeyUp(sender As Object, e As KeyEventArgs) Handles cmbidTipoLLanta.KeyUp
        If e.KeyValue = Keys.Enter Then
            txtProfundidadAct.SelectAll()
            txtProfundidadAct.Focus()
        End If
    End Sub

    Private Sub txtProfundidadAct_TextChanged(sender As Object, e As EventArgs) Handles txtProfundidadAct.TextChanged

    End Sub

    Private Sub txtProfundidadAct_KeyUp(sender As Object, e As KeyEventArgs) Handles txtProfundidadAct.KeyUp
        If e.KeyValue = Keys.Enter Then
            cmbidCondicion.Focus()
        End If
    End Sub

    Private Sub txtKmActual_TextChanged(sender As Object, e As EventArgs) Handles txtKmActual.TextChanged

    End Sub

    Private Sub txtKmActual_KeyUp(sender As Object, e As KeyEventArgs) Handles txtKmActual.KeyUp
        If e.KeyValue = Keys.Enter Then
            If cmbidDisenioRev.Enabled Then
                cmbidDisenioRev.Focus()
            Else
                cmbidCondicion.Focus()
            End If

        End If
    End Sub

    Private Sub chkRevitalizado_CheckedChanged(sender As Object, e As EventArgs) Handles chkRevitalizado.CheckedChanged
        cmbidDisenioRev.Enabled = chkRevitalizado.Checked
    End Sub

    Private Sub cmbidCondicion_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbidCondicion.SelectedIndexChanged

    End Sub

    Private Sub cmbidCondicion_KeyUp(sender As Object, e As KeyEventArgs) Handles cmbidCondicion.KeyUp
        If e.KeyValue = Keys.Enter Then
            txtFactura.SelectAll()
            txtFactura.Focus()

        End If
    End Sub

    Private Sub txtFactura_KeyUp(sender As Object, e As KeyEventArgs) Handles txtFactura.KeyUp
        If e.KeyValue = Keys.Enter Then
            txtCosto.SelectAll()
            txtCosto.Focus()
        End If
    End Sub

    Private Sub txtCosto_KeyUp(sender As Object, e As KeyEventArgs) Handles txtCosto.KeyUp
        If e.KeyValue = Keys.Enter Then
            dtpFecha.Focus()
        End If
    End Sub

    Private Sub dtpFecha_KeyUp(sender As Object, e As KeyEventArgs) Handles dtpFecha.KeyUp
        If e.KeyValue = Keys.Enter Then
            txtObserva.SelectAll()
            txtObserva.Focus()
        End If
    End Sub

    Public Function AutoCompletarAlmacen(ByVal Control As ComboBox) As AutoCompleteStringCollection

        Dim Coleccion As New AutoCompleteStringCollection
        Dim dt As DataTable

        Almacen = New AlmacenClass(0)
        dt = Almacen.TablaAlmacenes(False)
        'Recorrer y cargar los items para el Autocompletado
        For Each row As DataRow In dt.Rows
            Coleccion.Add(Convert.ToString(row("NomAlmacen")))
        Next
        'Ajustamos el control TextBox o ComboBox para recibir los datos de la siguiente manera.
        With Control
            .DataSource = dt
            .DisplayMember = "NomAlmacen"
            .ValueMember = "idAlmacen"


            .AutoCompleteCustomSource = Coleccion
            .AutoCompleteMode = AutoCompleteMode.Suggest
            .AutoCompleteSource = AutoCompleteSource.CustomSource
        End With

        ' Aca le devuelvo los datos a recuperados de la Base
        Return Coleccion

    End Function

    'Private Sub txtNoSerie_Leave(sender As Object, e As EventArgs)
    '    If Salida Or txtNoSerie.Text.Length = 0 Then Exit Sub
    '    Llanta = New LlantasClass(txtNoSerie.Text, OpcionLlanta.NoSerie)
    '    If Llanta.Existe Then
    '        MsgBox("El No Serie: " & txtNoSerie.Text & " ya Existe")
    '        txtNoSerie.SelectAll()
    '        txtNoSerie.Focus()
    '    Else
    '        txtPosicion.Select()
    '        txtPosicion.Focus()
    '    End If
    'End Sub

    Private Sub txtPosicion_Leave(sender As Object, e As EventArgs) Handles txtPosicion.Leave
        If Salida Then Exit Sub
        TipoUbicacion = New CatLugaresClass(cmbidtipoUbicacion.SelectedValue, TipoOrdenServicio.LLANTAS)
        If TipoUbicacion.Existe Then
            If TipoUbicacion.NomTabla = "CATUNIDADTRANS" Then
                Llanta = New LlantasClass(txtPosicion.Value, OpcionLlanta.Posicion, cmbidUbicacion.SelectedValue)
                If Llanta.Existe Then
                    MsgBox("La Posicion: " & txtPosicion.Value & " ya la ocupa la unidad: " & cmbidUbicacion.SelectedValue & "En la llanta: " & Llanta.idLlanta)
                    txtPosicion.Focus()
                Else
                    OpcionForma = TipoOpcionForma.tOpcInsertar
                    btnMnuOk.Enabled = True
                    If rdbPosicion.Length > 0 Then
                        SeleccionaPosicion(Val(txtPosicion.Value))
                        cmbidProductoLlanta.Focus()
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub BorrarCamposReutilizar()
        txtidLlanta.Text = ""
        'txtNoSerie.Text = ""
        cmbidProductoLlanta.SelectedIndex = 0
        chkRevitalizado.Checked = False
        cmbidTipoLLanta.SelectedIndex = 0
        txtProfundidadAct.Text = ""
        'txtKmActual.Text = ""
        'cmbidCondicion.SelectedIndex = 0
        txtFactura.Text = "1"
        txtCosto.Text = "1"
        chkActivo.Checked = True
        dtpFecha.Value = Now
        txtObserva.Text = "INVENTARIO"
        cmbidCondicion.SelectedIndex = 1

    End Sub

    Private Sub txtObserva_TextChanged(sender As Object, e As EventArgs) Handles txtObserva.TextChanged

    End Sub

    Private Sub txtObserva_KeyUp(sender As Object, e As KeyEventArgs) Handles txtObserva.KeyUp
        If e.KeyValue = Keys.Enter Then
            btnMnuOk_Click(sender, e)

        End If
    End Sub

    Private Sub cmbidProductoLlanta_SelectedValueChanged(sender As Object, e As EventArgs) Handles cmbidProductoLlanta.SelectedValueChanged
        If Salida Then Exit Sub
        CargaForm(cmbidProductoLlanta.SelectedValue, "LLANTAPROD")
    End Sub
End Class
