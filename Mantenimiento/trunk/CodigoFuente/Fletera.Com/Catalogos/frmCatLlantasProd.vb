'Fecha: 14 / Septiembre / 2016
'************************************************************************************************************************************
'*  Fecha Modificacion :   14 / Septiembre / 2016                                                                                       
'*  
'************************************************************************************************************************************
Public Class frmCatLlantasProd
    Inherits System.Windows.Forms.Form
    Dim OpcionForma As TipoOpcionForma
    Dim TablaBd As String = "CatLLantasProducto"
    Dim CampoLLave As String = "idProductoLlanta"
    Public WithEvents MeStatus1 As ControlStatus.MeStatus
    Friend WithEvents GridTodos As System.Windows.Forms.DataGridView
    'Private Depto As DeptosClass
    Private LLantaProd As LLantasProductoClass
    'Private Marca As MarcaClass
    Private RelMarcaTos As RelMarcaTipOrdSerClass
    Private Disenio As CatDiseniosClass

    Friend WithEvents btnMnuOk As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuCancelar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuSalir As System.Windows.Forms.ToolStripButton
    Private _Tag As String
    Friend WithEvents Label4 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents txtProfundidad As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents txtMedida As TextBox
    Friend WithEvents label2 As Label
    Friend WithEvents cmbidDisenio As ComboBox
    Dim bEsIdentidad As Boolean = True
    Friend WithEvents cmbMarcas As ComboBox
    Dim DsCombo As New DataSet

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents GrupoCaptura As System.Windows.Forms.GroupBox
    Friend WithEvents ToolStripMenu As System.Windows.Forms.ToolStrip
    Friend WithEvents txtMenuBusca As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents btnMnuAltas As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuModificar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuEliminar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuReporte As System.Windows.Forms.ToolStripButton
    Friend WithEvents label1 As System.Windows.Forms.Label
    Friend WithEvents cmdBuscaClave As System.Windows.Forms.Button
    Friend WithEvents txtidProductoLlanta As System.Windows.Forms.TextBox
    Private con As String
    Private v As Boolean

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCatLlantasProd))
        Me.txtidProductoLlanta = New System.Windows.Forms.TextBox()
        Me.label1 = New System.Windows.Forms.Label()
        Me.GrupoCaptura = New System.Windows.Forms.GroupBox()
        Me.cmbidDisenio = New System.Windows.Forms.ComboBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtProfundidad = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtMedida = New System.Windows.Forms.TextBox()
        Me.label2 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cmdBuscaClave = New System.Windows.Forms.Button()
        Me.ToolStripMenu = New System.Windows.Forms.ToolStrip()
        Me.txtMenuBusca = New System.Windows.Forms.ToolStripTextBox()
        Me.btnMnuAltas = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuModificar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuEliminar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuReporte = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuOk = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuCancelar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuSalir = New System.Windows.Forms.ToolStripButton()
        Me.MeStatus1 = New ControlStatus.MeStatus()
        Me.GridTodos = New System.Windows.Forms.DataGridView()
        Me.cmbMarcas = New System.Windows.Forms.ComboBox()
        Me.GrupoCaptura.SuspendLayout()
        Me.ToolStripMenu.SuspendLayout()
        CType(Me.GridTodos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtidProductoLlanta
        '
        Me.txtidProductoLlanta.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtidProductoLlanta.Location = New System.Drawing.Point(109, 27)
        Me.txtidProductoLlanta.Name = "txtidProductoLlanta"
        Me.txtidProductoLlanta.Size = New System.Drawing.Size(69, 20)
        Me.txtidProductoLlanta.TabIndex = 1
        '
        'label1
        '
        Me.label1.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.label1.Location = New System.Drawing.Point(6, 27)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(94, 20)
        Me.label1.TabIndex = 14
        Me.label1.Text = "Id Producto"
        '
        'GrupoCaptura
        '
        Me.GrupoCaptura.Controls.Add(Me.cmbMarcas)
        Me.GrupoCaptura.Controls.Add(Me.cmbidDisenio)
        Me.GrupoCaptura.Controls.Add(Me.Label6)
        Me.GrupoCaptura.Controls.Add(Me.txtProfundidad)
        Me.GrupoCaptura.Controls.Add(Me.Label5)
        Me.GrupoCaptura.Controls.Add(Me.txtMedida)
        Me.GrupoCaptura.Controls.Add(Me.label2)
        Me.GrupoCaptura.Controls.Add(Me.Label4)
        Me.GrupoCaptura.Controls.Add(Me.label1)
        Me.GrupoCaptura.Controls.Add(Me.cmdBuscaClave)
        Me.GrupoCaptura.Controls.Add(Me.txtidProductoLlanta)
        Me.GrupoCaptura.Location = New System.Drawing.Point(168, 45)
        Me.GrupoCaptura.Name = "GrupoCaptura"
        Me.GrupoCaptura.Size = New System.Drawing.Size(524, 222)
        Me.GrupoCaptura.TabIndex = 3
        Me.GrupoCaptura.TabStop = False
        '
        'cmbidDisenio
        '
        Me.cmbidDisenio.AutoCompleteCustomSource.AddRange(New String() {"Regularizaci�n", "Vivienda"})
        Me.cmbidDisenio.FormattingEnabled = True
        Me.cmbidDisenio.Location = New System.Drawing.Point(106, 105)
        Me.cmbidDisenio.Name = "cmbidDisenio"
        Me.cmbidDisenio.Size = New System.Drawing.Size(151, 21)
        Me.cmbidDisenio.TabIndex = 107
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label6.Location = New System.Drawing.Point(2, 179)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(67, 13)
        Me.Label6.TabIndex = 106
        Me.Label6.Text = "Profundidad:"
        '
        'txtProfundidad
        '
        Me.txtProfundidad.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtProfundidad.Location = New System.Drawing.Point(106, 176)
        Me.txtProfundidad.MaxLength = 50
        Me.txtProfundidad.Name = "txtProfundidad"
        Me.txtProfundidad.Size = New System.Drawing.Size(151, 20)
        Me.txtProfundidad.TabIndex = 7
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label5.Location = New System.Drawing.Point(6, 143)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(45, 13)
        Me.Label5.TabIndex = 104
        Me.Label5.Text = "Medida:"
        '
        'txtMedida
        '
        Me.txtMedida.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMedida.Location = New System.Drawing.Point(106, 140)
        Me.txtMedida.MaxLength = 30
        Me.txtMedida.Name = "txtMedida"
        Me.txtMedida.Size = New System.Drawing.Size(151, 20)
        Me.txtMedida.TabIndex = 6
        '
        'label2
        '
        Me.label2.AutoSize = True
        Me.label2.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.label2.Location = New System.Drawing.Point(3, 108)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(43, 13)
        Me.label2.TabIndex = 102
        Me.label2.Text = "Dise�o:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label4.Location = New System.Drawing.Point(6, 67)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(40, 13)
        Me.Label4.TabIndex = 44
        Me.Label4.Text = "Marca:"
        '
        'cmdBuscaClave
        '
        Me.cmdBuscaClave.Image = CType(resources.GetObject("cmdBuscaClave.Image"), System.Drawing.Image)
        Me.cmdBuscaClave.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdBuscaClave.Location = New System.Drawing.Point(186, 19)
        Me.cmdBuscaClave.Name = "cmdBuscaClave"
        Me.cmdBuscaClave.Size = New System.Drawing.Size(32, 32)
        Me.cmdBuscaClave.TabIndex = 2
        Me.cmdBuscaClave.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'ToolStripMenu
        '
        Me.ToolStripMenu.AutoSize = False
        Me.ToolStripMenu.BackColor = System.Drawing.Color.Silver
        Me.ToolStripMenu.Dock = System.Windows.Forms.DockStyle.None
        Me.ToolStripMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.txtMenuBusca, Me.btnMnuAltas, Me.btnMnuModificar, Me.btnMnuEliminar, Me.btnMnuReporte, Me.btnMnuOk, Me.btnMnuCancelar, Me.btnMnuSalir})
        Me.ToolStripMenu.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow
        Me.ToolStripMenu.Location = New System.Drawing.Point(3, 0)
        Me.ToolStripMenu.Name = "ToolStripMenu"
        Me.ToolStripMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.ToolStripMenu.Size = New System.Drawing.Size(730, 42)
        Me.ToolStripMenu.TabIndex = 1
        Me.ToolStripMenu.Text = "ToolStrip1"
        '
        'txtMenuBusca
        '
        Me.txtMenuBusca.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.txtMenuBusca.Name = "txtMenuBusca"
        Me.txtMenuBusca.Size = New System.Drawing.Size(150, 42)
        Me.txtMenuBusca.ToolTipText = "Filtra Listado"
        '
        'btnMnuAltas
        '
        Me.btnMnuAltas.ForeColor = System.Drawing.Color.Red
        Me.btnMnuAltas.Image = CType(resources.GetObject("btnMnuAltas.Image"), System.Drawing.Image)
        Me.btnMnuAltas.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuAltas.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuAltas.Name = "btnMnuAltas"
        Me.btnMnuAltas.Size = New System.Drawing.Size(44, 39)
        Me.btnMnuAltas.Text = "&ALTAS"
        Me.btnMnuAltas.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnMnuAltas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuModificar
        '
        Me.btnMnuModificar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuModificar.Image = CType(resources.GetObject("btnMnuModificar.Image"), System.Drawing.Image)
        Me.btnMnuModificar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuModificar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuModificar.Name = "btnMnuModificar"
        Me.btnMnuModificar.Size = New System.Drawing.Size(74, 39)
        Me.btnMnuModificar.Text = "&MODIFICAR"
        Me.btnMnuModificar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnMnuModificar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuEliminar
        '
        Me.btnMnuEliminar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuEliminar.Image = CType(resources.GetObject("btnMnuEliminar.Image"), System.Drawing.Image)
        Me.btnMnuEliminar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuEliminar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuEliminar.Name = "btnMnuEliminar"
        Me.btnMnuEliminar.Size = New System.Drawing.Size(64, 39)
        Me.btnMnuEliminar.Text = "&ELIMINAR"
        Me.btnMnuEliminar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuReporte
        '
        Me.btnMnuReporte.ForeColor = System.Drawing.Color.Red
        Me.btnMnuReporte.Image = CType(resources.GetObject("btnMnuReporte.Image"), System.Drawing.Image)
        Me.btnMnuReporte.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuReporte.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuReporte.Name = "btnMnuReporte"
        Me.btnMnuReporte.Size = New System.Drawing.Size(59, 39)
        Me.btnMnuReporte.Text = "&REPORTE"
        Me.btnMnuReporte.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuOk
        '
        Me.btnMnuOk.ForeColor = System.Drawing.Color.Red
        Me.btnMnuOk.Image = Global.Fletera.My.Resources.Resources.Save1
        Me.btnMnuOk.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuOk.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuOk.Name = "btnMnuOk"
        Me.btnMnuOk.Size = New System.Drawing.Size(56, 39)
        Me.btnMnuOk.Text = "&GRABAR"
        Me.btnMnuOk.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuCancelar
        '
        Me.btnMnuCancelar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuCancelar.Image = Global.Fletera.My.Resources.Resources.signos_f_059
        Me.btnMnuCancelar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuCancelar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuCancelar.Name = "btnMnuCancelar"
        Me.btnMnuCancelar.Size = New System.Drawing.Size(71, 39)
        Me.btnMnuCancelar.Text = "&CANCELAR"
        Me.btnMnuCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuSalir
        '
        Me.btnMnuSalir.ForeColor = System.Drawing.Color.Red
        Me.btnMnuSalir.Image = Global.Fletera.My.Resources.Resources.Salida2
        Me.btnMnuSalir.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuSalir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuSalir.Name = "btnMnuSalir"
        Me.btnMnuSalir.Size = New System.Drawing.Size(41, 39)
        Me.btnMnuSalir.Text = "&SALIR"
        Me.btnMnuSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'MeStatus1
        '
        Me.MeStatus1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.MeStatus1.Location = New System.Drawing.Point(0, 266)
        Me.MeStatus1.Margin = New System.Windows.Forms.Padding(4)
        Me.MeStatus1.Name = "MeStatus1"
        Me.MeStatus1.Size = New System.Drawing.Size(692, 26)
        Me.MeStatus1.TabIndex = 72
        '
        'GridTodos
        '
        Me.GridTodos.AllowUserToAddRows = False
        Me.GridTodos.AllowUserToDeleteRows = False
        Me.GridTodos.AllowUserToResizeRows = False
        Me.GridTodos.BackgroundColor = System.Drawing.Color.White
        Me.GridTodos.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.GridTodos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.GridTodos.Location = New System.Drawing.Point(3, 45)
        Me.GridTodos.MultiSelect = False
        Me.GridTodos.Name = "GridTodos"
        Me.GridTodos.ReadOnly = True
        Me.GridTodos.RowHeadersVisible = False
        Me.GridTodos.Size = New System.Drawing.Size(162, 222)
        Me.GridTodos.TabIndex = 73
        '
        'cmbMarcas
        '
        Me.cmbMarcas.AutoCompleteCustomSource.AddRange(New String() {"Regularizaci�n", "Vivienda"})
        Me.cmbMarcas.FormattingEnabled = True
        Me.cmbMarcas.Location = New System.Drawing.Point(109, 64)
        Me.cmbMarcas.Name = "cmbMarcas"
        Me.cmbMarcas.Size = New System.Drawing.Size(151, 21)
        Me.cmbMarcas.TabIndex = 108
        '
        'frmCatLlantasProd
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(692, 292)
        Me.Controls.Add(Me.GridTodos)
        Me.Controls.Add(Me.MeStatus1)
        Me.Controls.Add(Me.ToolStripMenu)
        Me.Controls.Add(Me.GrupoCaptura)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmCatLlantasProd"
        Me.Text = "Catal�go de Llantas Producto"
        Me.GrupoCaptura.ResumeLayout(False)
        Me.GrupoCaptura.PerformLayout()
        Me.ToolStripMenu.ResumeLayout(False)
        Me.ToolStripMenu.PerformLayout()
        CType(Me.GridTodos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region
    Sub New(ByVal vTag As String)
        InitializeComponent()
        _Tag = vTag
    End Sub

    Public Sub New(con As String, v As Boolean)
        InitializeComponent()
        Me.con = con
        BD = New CapaDatos.UtilSQL(con, "Juan")
        Me.v = v
    End Sub
#Region "MenusToolStrip"
    Private Sub LimpiaVariables()
        SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")

    End Sub
    Private Sub LimpiaCampos()
        txtidProductoLlanta.Text = ""
        cmbidDisenio.SelectedIndex = 0
        cmbMarcas.SelectedIndex = 0
        txtMedida.Text = ""
        txtProfundidad.Text = ""
        LimpiaVariables()
    End Sub

    Private Sub ActivaCampos(ByVal valor As Boolean, ByVal vOpcion As TipoOpcActivaCampos)
        If vOpcion = TipoOpcActivaCampos.tOpcDESHABTODOS Then
            txtidProductoLlanta.Enabled = False
            cmbMarcas.Enabled = False
            cmbidDisenio.Enabled = False
            txtMedida.Enabled = False
            txtProfundidad.Enabled = False
        ElseIf vOpcion = TipoOpcActivaCampos.tOpcINICIALIZA Then
            txtidProductoLlanta.Enabled = Not valor
            cmbMarcas.Enabled = valor
            cmbidDisenio.Enabled = valor
            txtMedida.Enabled = valor
            txtProfundidad.Enabled = valor

        End If
    End Sub


    Private Sub ActivaBotones(ByVal valor As Boolean, Optional ByVal vOpcion As TipoOpcActivaBoton = TipoOpcActivaBoton.tOpcInicializa)
        'Si es Verdadero Se activan Ok,Cancelar y Buscar y Terminar Se desactiva
        If vOpcion = TipoOpcActivaBoton.tOpcConsulta Then
            btnMnuOk.Enabled = False
            btnMnuCancelar.Enabled = valor
            'cmdBuscaClave.Enabled = valor
            btnMnuSalir.Enabled = Not valor

            btnMnuAltas.Enabled = Not valor
            btnMnuModificar.Enabled = Not valor
            btnMnuEliminar.Enabled = Not valor
            btnMnuReporte.Enabled = Not valor


        ElseIf vOpcion = TipoOpcActivaBoton.tOpcInicializa Then
            btnMnuOk.Enabled = valor
            btnMnuCancelar.Enabled = valor
            'cmdBuscaClave.Enabled = valor
            btnMnuSalir.Enabled = Not valor

            btnMnuAltas.Enabled = Not valor
            btnMnuModificar.Enabled = Not valor
            btnMnuEliminar.Enabled = Not valor
            btnMnuReporte.Enabled = Not valor

            txtMenuBusca.Enabled = Not valor
            GridTodos.Enabled = Not valor
        ElseIf vOpcion = TipoOpcActivaBoton.tOpcInsertar Then
            btnMnuOk.Enabled = valor
            btnMnuCancelar.Enabled = valor
            'cmdBuscaClave.Enabled = False
            btnMnuSalir.Enabled = Not valor

            btnMnuAltas.Enabled = Not valor
            btnMnuModificar.Enabled = Not valor
            btnMnuEliminar.Enabled = Not valor
            btnMnuReporte.Enabled = Not valor
        End If
    End Sub

    Private Sub btnMnuAltas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuAltas.Click
        'ALTAS
        ' OpcionMnuTools(TipoOpcionForma.tOpcInsertar)
        '15/FEB/2016

        'txtMenuBusca.Enabled = True
        'GridTodos.Enabled = True

        'If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Alta) = False Then
        '    MessageBox.Show("No tiene los permisos Necesarios para poder dar de Alta en el Catalogo de Marcas!!!",
        '        "Acceso Denegado a Alta de Marcas", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    OpcionForma = TipoOpcionForma.tOpcConsultar
        '    Exit Sub


        'For Each ct As Control In GrupoCaptura.Controls
        '    If Not ct Is txtidMarca Then
        '        ct.Enabled = False
        '    Else
        '        ct.Enabled = True
        '    End If
        '    If TypeOf (ct) Is TextBox Then
        '        TryCast(ct, TextBox).Text = ""
        '    End If
        'Next
        'btnMnuAltas.Enabled = False
        'btnMnuModificar.Enabled = False
        'btnMnuEliminar.Enabled = False
        'btnMnuOk.Enabled = False
        'btnMnuCancelar.Enabled = True
        'txtMenuBusca.Enabled = False
        'GridTodos.Enabled = False

        'btnMnuSalir.Enabled = False

        OpcionForma = TipoOpcionForma.tOpcInsertar
        LimpiaCampos()
        ActivaCampos(True, TipoOpcActivaCampos.tOpcINICIALIZA)
        ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)

        If bEsIdentidad Then
            txtidProductoLlanta.Enabled = False
            cmbMarcas.Focus()
        Else
            txtidProductoLlanta.Focus()
        End If

        Status("Ingrese nuevo Producto Llanta para guardar", Me)
    End Sub
    Private Sub btnMnuModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuModificar.Click
        'MODIFICAR
        'OpcionMnuTools(TipoOpcionForma.tOpcModificar)
        'txtsIdColonia_Leave(sender, e)
        'txtsDescripcion.Enabled = False

        'If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Modificar) = False Then
        '    MessageBox.Show("No tiene los permisos Necesarios para poder Modificar la Informacion de la Marca!!!",
        '           "Acceso Denegado a Modificacion de la Informacion de la Marca", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    OpcionForma = TipoOpcionForma.tOpcConsultar
        '    Exit Sub
        'End If

        'btnMnuAltas.Enabled = False
        'btnMnuModificar.Enabled = False
        'btnMnuEliminar.Enabled = True
        'btnMnuCancelar.Enabled = True
        'btnMnuOk.Enabled = True

        'GridTodos.Enabled = False
        'btnMnuSalir.Enabled = False

        If Trim(txtidProductoLlanta.Text) = "" Then
            txtidProductoLlanta.Text = DespliegaGrid(TablaBd, Inicio.CONSTR, CampoLLave)
            CargaForm(txtidProductoLlanta.Text)
        End If
        'txtidMarca.Focus()
        ' Status("Formulario Preparado para modificar Marcas", Me)

        OpcionForma = TipoOpcionForma.tOpcModificar
        ActivaCampos(True, TipoOpcActivaCampos.tOpcINICIALIZA)
        ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)

        btnMnuEliminar.Enabled = True
        If bEsIdentidad Then
            txtidProductoLlanta.Enabled = False
            cmbMarcas.Focus()
        Else
            txtidProductoLlanta.Focus()
        End If

        Status("Ingrese nuevo Departamento para guardar", Me)
    End Sub
    Private Sub btnMnuEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuEliminar.Click
        'ELIMINAR
        'OpcionMnuTools(TipoOpcionForma.tOpcEliminar)
        'txtsIdColonia_Leave(sender, e)
        'txtsDescripcion.Enabled = False

        'If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Baja) = False Then
        '    MessageBox.Show("No tiene los permisos Necesarios para poder Eliminar la Marca!!!",
        '        "Acceso Denegado a Eliminacion de Marca", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    OpcionForma = TipoOpcionForma.tOpcConsultar
        '    Exit Sub
        'End If
        If txtidProductoLlanta.Text = LLantaProd.idProductoLlanta Then
            With LLantaProd
                If MessageBox.Show("Esta completamente seguro que desea Eliminar la llanta : " &
                                   .idProductoLlanta & " ????", "Confirma que desea eliminar la llanta: " & .idProductoLlanta & "???",
                                 MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then Exit Sub
                Try
                    .Eliminar() 'Elimina el municipio
                    MessageBox.Show("llanta : " & .idProductoLlanta & " Eliminado con exito!!!",
                                    "llanta Eliminada con exito", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Status("Llanta: " & .idProductoLlanta & " Eliminado con exito!!!", Me)
                    ActualizaGrid(txtMenuBusca.Text)
                    txtidProductoLlanta.Text = ""
                    OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
                Catch ex As Exception
                    Status("Error al tratar de eliminar la Llanta: " & ex.Message, Me, Err:=True)
                    MessageBox.Show("Error al Tratar de eliminar la Llanta: " & .idProductoLlanta & vbNewLine &
                                    "Error. " & ex.Message, "Error: " & Err.Number, MessageBoxButtons.OK, MessageBoxIcon.Error)

                End Try
            End With
        End If
    End Sub
    Private Sub btnMnuConsulta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'CONSULTA
        OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
        'txtsIdColonia_Leave(sender, e)
        'txtsNomColonia.Enabled = False
    End Sub
    Private Sub btnMnuReporte_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuReporte.Click
        'REPORTE
        'OpcionMnuTools(TipoOpcionForma.tOpcImprimir)

        'If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Reporte) = False Then
        '    MessageBox.Show("No tiene los permisos Necesarios para poder realizar Reporte de Estados!!!",
        '        "Acceso Denegado a Reportes de Estados", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    OpcionForma = TipoOpcionForma.tOpcConsultar
        '    Exit Sub
        'End If
        ToolStripMenu.Enabled = False
        'comboMnu.SelectedIndex = 4
        Status("Abriendo Reporte Espere. . . ", Me)
        Me.Cursor = Cursors.WaitCursor
        DespliegaReporte(TablaBd, CONSTR)
        Status("Finalizo el reporte. . . ", Me)
        Me.Cursor = Cursors.Default
        ToolStripMenu.Enabled = True
    End Sub

#End Region
    Private Sub ActualizaGrid(ByVal vidProductoLlanta As String)
        If vidProductoLlanta = "" Then
            GridTodos.DataSource = BD.ExecuteReturn("SELECT idProductoLlanta, idProductoLlanta as Descripcion FROM " & TablaBd, TablaBd)
            Status(GridTodos.RowCount & " Registros", Me)
        Else
            GridTodos.DataSource = BD.ExecuteReturn("SELECT idProductoLlanta, NomDepto FROM " & TablaBd &
                                                " WHERE Descripcion Like '%" & vidProductoLlanta & "%'")
            Status(GridTodos.RowCount & " Registros obtenidos con el filtro %" & vidProductoLlanta & "%", Me)
        End If

    End Sub
    Private Sub OpcionMnuTools(ByVal vOpcion As TipoOpcionForma)
        If vOpcion <> TipoOpcionForma.tOpcImprimir And vOpcion <> TipoOpcionForma.tOpcEliminar Then
            OpcionForma = vOpcion
        End If
        txtMenuBusca.Enabled = True
        GridTodos.Enabled = True
        If vOpcion = TipoOpcionForma.tOpcInsertar Then
            'ALTAS

        ElseIf vOpcion = TipoOpcionForma.tOpcModificar Then
            'MODIFICAR

        ElseIf vOpcion = TipoOpcionForma.tOpcEliminar Then

        ElseIf vOpcion = TipoOpcionForma.tOpcConsultar Then
            'CONSULTA
            For Each ct As Control In GrupoCaptura.Controls
                If Not ct Is cmdBuscaClave Then
                    ct.Enabled = False
                Else
                    ct.Enabled = True
                End If
            Next
            txtMenuBusca.Enabled = True
            btnMnuAltas.Enabled = True
            btnMnuModificar.Enabled = True
            btnMnuEliminar.Enabled = False
            btnMnuReporte.Enabled = True
            btnMnuOk.Enabled = False
            btnMnuCancelar.Enabled = False

            btnMnuSalir.Enabled = True

            'Status("Formulario preparado para Consultar los Municipios Existentes!!!", Me)
        ElseIf vOpcion = TipoOpcionForma.tOpcImprimir Then
            'REPORTE

        End If
    End Sub


    Private Sub txtMenuBusca_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtMenuBusca.KeyPress
        If Asc(e.KeyChar) = 39 Then e.Handled = True : Exit Sub 'Caracter ' Evita errores
        If Asc(e.KeyChar) = 13 Then
            GridTodos.Focus()
            e.Handled = True
        End If
    End Sub

    'Private Sub txtMenuBusca_KeyUp(sender As Object, e As KeyEventArgs) Handles txtMenuBusca.KeyUp
    '    If txtMenuBusca.Text.Trim = "" Then Exit Sub
    '    If e.KeyCode = Keys.Down Or e.KeyCode = Keys.Up Then
    '        GridTodos.Focus()
    '        e.Handled = True
    '        Exit Sub
    '    End If
    '    If e.KeyCode = Keys.Left Or e.KeyCode = Keys.Right Then Exit Sub
    '    ActualizaGrid(txtMenuBusca.Text)
    'End Sub



    Private Sub frmc_Estado_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Inicio.CONSTR = "" Then
            Inicio.CONSTR = con
        End If
        LlenaCombos()
        OpcionMnuTools(TipoOpcionForma.tOpcConsultar)

        ActualizaGrid("")
        GridTodos.Columns(1).Width = GridTodos.Width - 20
        GridTodos.Columns(0).Visible = False
        txtMenuBusca.Focus()
    End Sub
    Private Sub GridTodos_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles GridTodos.CellEnter
        'With GridTodos
        CargaForm(GridTodos.Rows(GridTodos.CurrentRow.Index).Cells(0).Value)
        'End With
    End Sub
    Private Sub GridTodos_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles GridTodos.KeyPress
        If Asc(e.KeyChar) = 8 Then
            txtMenuBusca.Focus()
            e.Handled = True
        End If
    End Sub
    Private Sub CargaForm(ByVal vId As Integer)
        Try

            LLantaProd = New LLantasProductoClass(vId)
            With LLantaProd
                txtidProductoLlanta.Text = .idProductoLlanta
                If .Existe Then
                    cmbMarcas.SelectedValue = .idMarca
                    'Marca = New MarcaClass(.idMarca)
                    'If Marca.Existe Then
                    '    txtNombreMarca.Text = Marca.NOMBREMARCA
                    'End If

                    cmbidDisenio.SelectedValue = .idDisenio
                    txtMedida.Text = .Medida
                    txtProfundidad.Text = .Profundidad
                    If OpcionForma = TipoOpcionForma.tOpcInsertar Then
                        Status("La llanta Con id: " & .idProductoLlanta & " Ya existe!!!, Favor de modificarlo para poder dar de alta a otra Llanta", Me, Err:=True)
                        MessageBox.Show("La Llanta Con id: " & .idProductoLlanta & " YA EXISTE!!!, Favor de Cambiarlo y volver a intentarlo",
                                        "La Lanta Con id: " & .idProductoLlanta & " YA EXISTE!!!", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    End If
                Else 'No existe la colonia!!!
                    If OpcionForma = TipoOpcionForma.tOpcInsertar Then
                        For Each Ct As Control In GrupoCaptura.Controls
                            If Not Ct Is txtidProductoLlanta And Not Ct Is cmdBuscaClave Then
                                Ct.Enabled = True
                            Else
                                Ct.Enabled = False
                            End If
                            If TypeOf (Ct) Is TextBox Then
                                If Not Ct Is txtidProductoLlanta Then
                                    TryCast(Ct, TextBox).Text = ""
                                End If
                            End If
                        Next
                        btnMnuOk.Enabled = True
                        'cmdOk.Enabled = True

                        cmbMarcas.Focus()
                        Status("Listo para Guardar la nueva Llanta con id: " & .idProductoLlanta, Me)
                    End If
                End If
            End With
        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try

    End Sub
    Private Function Validar() As Boolean
        Validar = True
        If Not Inicio.ValidandoCampo(Trim(txtidProductoLlanta.Text), TipoDato.TdNumerico) Then
            If Not bEsIdentidad Then
                MsgBox(MENSAJE_CLAVE, vbInformation, "Aviso" & Me.Text)
                If txtidProductoLlanta.Enabled Then
                    txtidProductoLlanta.Focus()
                    txtidProductoLlanta.SelectAll()
                End If
                Validar = False
            End If
        ElseIf cmbMarcas.SelectedValue = 0 Then
            MsgBox("La Marca No Puede Ser Vacio", vbInformation, "Aviso" & Me.Text)
            If cmbMarcas.Enabled Then
                cmbMarcas.Focus()
            End If
            Validar = False
        ElseIf cmbidDisenio.SelectedValue = 0 Then
            MsgBox("El Dise�o No Puede Ser Vacio", vbInformation, "Aviso" & Me.Text)
            cmbidDisenio.Focus()
            Validar = False
            'If Not Inicio.ValidandoCampo(Trim(cmbidDisenio.SelectedValue), TipoDato.TdNumerico) Then

            'End If
        End If
    End Function
    'CONTROLES
    Private Sub txtsIdEstado_KeyDown(sender As Object, e As KeyEventArgs) Handles txtidProductoLlanta.KeyDown
        If e.KeyCode = Keys.F3 Then
            cmdBuscaClave_Click(sender, e)
        End If

    End Sub
    Private Sub txtsIdEstado_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtidProductoLlanta.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If txtidProductoLlanta.Text.Trim <> "" Then
                CargaForm(txtidProductoLlanta.Text)
            End If
            e.Handled = True
        End If
    End Sub
    Private Sub txtsNomEstado_KeyPress(sender As Object, e As KeyPressEventArgs)
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            btnMnuOk_Click(sender, e)
        End If
    End Sub

    'BOTONES
    Private Sub cmdCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
    Private Sub cmdTerminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub cmdOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
    Private Sub cmdBuscaClave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBuscaClave.Click
        Dim idEst As String = DespliegaGrid(TablaBd, Inicio.CONSTR, CampoLLave)
        If idEst <> "" Then CargaForm(idEst)
    End Sub
    'Handles cmdCancelar.EnabledChanged, cmdTerminar.EnabledChanged
    '        With TryCast(sender, Button)
    '            If .Enabled Then
    '                Me.CancelButton = sender
    '            End If
    '        End With
    '    End Sub
    Private Sub cmdCancelar_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        With TryCast(sender, Button)
            If .Enabled Then
                Me.CancelButton = sender
            End If
        End With
    End Sub
    Private Sub cmdTerminar_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        With TryCast(sender, Button)
            If .Enabled Then
                Me.CancelButton = sender
            End If
        End With
    End Sub

    Private Sub btnMnuOk_Click(sender As Object, e As EventArgs) Handles btnMnuOk.Click
        Dim CadCam As String = ""
        Try
            If Not Validar() Then Exit Sub
            If GridTodos.RowCount = 0 Then
                LLantaProd = New LLantasProductoClass(0)
            End If
            With LLantaProd
                If OpcionForma = TipoOpcionForma.tOpcModificar Then

                    CadCam = .GetCambios(cmbMarcas.SelectedValue, cmbidDisenio.SelectedValue, txtMedida.Text, txtProfundidad.Text, True)
                    If CadCam <> "" Then
                        'If MessageBox.Show("Esta completamente Seguro que Desea guardar los Cambios Realizados al Estado Con id: " & .IDMARCA & vbNewLine &
                        '                CadCam, "Confirma que desea guardar los Cambios Realizados???", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then Exit Sub
                        Status("Guardando Modificaciones de la Lanta Con id: " & .idProductoLlanta, Me, 1, 2)
                        .Guardar(bEsIdentidad, cmbMarcas.SelectedValue, cmbidDisenio.SelectedValue, txtMedida.Text, txtProfundidad.Text, True)
                        Status("Llanta con Id: " & .idProductoLlanta & " Modificado Con Exito!!!", Me, 2, 2)
                        MessageBox.Show("Llanta con Id: " & .idProductoLlanta & " Modificada con exito!!!",
                                        "Accion terminada con exito!!!", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
                        ActualizaGrid(txtMenuBusca.Text)
                    Else
                        Status("No se detectaron cambios para realizar!!!", Me, Err:=True)
                    End If
                ElseIf OpcionForma = TipoOpcionForma.tOpcInsertar Then
                    .Existe = False
                    .idMarca = cmbMarcas.SelectedValue
                    .idDisenio = cmbidDisenio.SelectedValue
                    .Profundidad = txtProfundidad.Text
                    .Medida = txtMedida.Text

                    If Not bEsIdentidad Then
                        .idProductoLlanta = txtidProductoLlanta.Text
                    End If

                    Status("Guardando Nueva Llanta: " & txtidProductoLlanta.Text & ". . .", Me, 1, 2)
                    .Guardar(bEsIdentidad, cmbMarcas.SelectedValue, cmbidDisenio.SelectedValue, txtMedida.Text, txtProfundidad.Text, True)
                    Status("Llanta: " & .idProductoLlanta & " Guardado con Exito!!!", Me, 2, 2)
                    MessageBox.Show("Nueva Lanta: " & .idProductoLlanta & " Guardado con exito!!!",
                                    "Accion terminada con exito!!!", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
                    ActualizaGrid(txtMenuBusca.Text)

                End If
            End With

        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try
    End Sub


    Private Sub btnMnuCancelar_Click(sender As Object, e As EventArgs) Handles btnMnuCancelar.Click
        If txtidProductoLlanta.Text <> "" Then
            CargaForm(txtidProductoLlanta.Text)
        End If
        OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
    End Sub

    Private Sub btnMnuSalir_Click(sender As Object, e As EventArgs) Handles btnMnuSalir.Click
        Me.Close()
    End Sub

    Private Sub txtMenuBusca_Click(sender As Object, e As EventArgs) Handles txtMenuBusca.Click

    End Sub

    Private Sub GridTodos_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles GridTodos.CellContentClick

    End Sub


    Private Sub txtMenuBusca_TextChanged(sender As Object, e As EventArgs) Handles txtMenuBusca.TextChanged
        'DsLista.Tables(TablaBd).DefaultView.RowFilter = Nothing
        'DsLista.Tables(TablaBd).DefaultView.RowFilter = "Descripcion Like '*" & txtMenuBusca.Text & "*'"
        'Status(DsLista.Tables(TablaBd).DefaultView.Count & " Areas Encontradas. . . ", Me)
        'ActualizaLista()

        ActualizaGrid(txtMenuBusca.Text)
    End Sub

    Private Sub txtNomDepto_TextChanged(sender As Object, e As EventArgs)

    End Sub

    'Private Sub cmdBuscaClaveMar_Click(sender As Object, e As EventArgs)
    '    txtidMarca.Text = DespliegaGrid("CatMarcas", Inicio.CONSTR, "idMarca")
    '    Marca = New MarcaClass(Val(txtidMarca.Text))
    '    If Marca.Existe Then
    '        txtNombreMarca.Text = Marca.NOMBREMARCA
    '    Else

    '    End If

    'End Sub

    Private Sub txtidMarca_TextChanged(sender As Object, e As EventArgs)

    End Sub
    'Private Sub txtidMarca_EnabledChanged(sender As Object, e As EventArgs)
    '    cmdBuscaClaveMar.Enabled = txtidMarca.Enabled
    'End Sub

    Private Sub txtidProductoLlanta_TextChanged(sender As Object, e As EventArgs) Handles txtidProductoLlanta.TextChanged

    End Sub

    Private Sub txtidProductoLlanta_EnabledChanged(sender As Object, e As EventArgs) Handles txtidProductoLlanta.EnabledChanged
        cmdBuscaClave.Enabled = txtidProductoLlanta.Enabled
    End Sub

    'Private Sub txtidMarca_KeyDown(sender As Object, e As KeyEventArgs)
    '    If e.KeyCode = Keys.F3 Then
    '        cmdBuscaClaveMar_Click(sender, e)
    '    End If
    'End Sub

    'Private Sub txtidMarca_KeyPress(sender As Object, e As KeyPressEventArgs)
    '    If txtidMarca.Text.Length = 0 Then
    '        txtidMarca.Focus()
    '    Else
    '        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
    '            Marca = New MarcaClass(Val(txtidMarca.Text))
    '            If Marca.Existe Then
    '                txtNombreMarca.Text = Marca.NOMBREMARCA
    '                cmbidDisenio.Focus()
    '            Else
    '                txtidMarca.Focus()
    '            End If


    '        End If
    '    End If
    'End Sub

    'Private Sub txtidMarca_Leave(sender As Object, e As EventArgs)
    '    If txtidMarca.Text.Length = 0 Then
    '        txtidMarca.Focus()
    '        txtidMarca.SelectAll()
    '    Else
    '        If txtidMarca.Enabled Then
    '            Marca = New MarcaClass(Val(txtidMarca.Text))
    '            If Marca.Existe Then
    '                txtNombreMarca.Text = Marca.NOMBREMARCA
    '                cmbidDisenio.Focus()
    '            Else
    '                txtidMarca.Focus()
    '            End If
    '        End If
    '    End If
    'End Sub

    'Private Sub txtidMarca_MouseDown(sender As Object, e As MouseEventArgs)
    '    txtidMarca.SelectAll()
    '    txtidMarca.Focus()
    'End Sub

    Private Sub txtDise�o_TextChanged(sender As Object, e As EventArgs)

    End Sub

    Private Sub txtDise�o_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbidDisenio.KeyPress

        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            txtMedida.SelectAll()
            txtMedida.Focus()
        End If
    End Sub



    Private Sub txtMedida_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtMedida.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            txtProfundidad.SelectAll()
            txtProfundidad.Focus()
        End If
    End Sub

    Private Sub txtMedida_MouseDown(sender As Object, e As MouseEventArgs) Handles txtMedida.MouseDown
        txtMedida.SelectAll()
        txtMedida.Focus()
    End Sub


    Private Sub txtProfundidad_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtProfundidad.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            cmdOk_Click(sender, e)
        End If
    End Sub

    Private Sub txtProfundidad_MouseDown(sender As Object, e As MouseEventArgs) Handles txtProfundidad.MouseDown
        txtProfundidad.SelectAll()
        txtProfundidad.Focus()

    End Sub

    Private Sub GrupoCaptura_Enter(sender As Object, e As EventArgs) Handles GrupoCaptura.Enter

    End Sub
    Private Function LlenaCombos() As Boolean
        Dim StrSql As String = ""
        LlenaCombos = True
        Try
            AutoCompletarMarcas(cmbMarcas)
            AutoCompletarDisenios(cmbidDisenio)



            'DsCombo.Clear()

            'StrSql = "SELECT di.idDisenio, di.Descripcion " &
            '"FROM dbo.CatDisenios di"

            'DsCombo = Inicio.LlenaCombos("idDisenio", "Descripcion", "CatDisenios",
            '                   TipoConexion.TcSQL, Inicio.CONSTR, Process.GetCurrentProcess.ProcessName,
            '                   KEY_CHECAERROR, KEY_ESTATUS, , StrSql)
            ''DsCombo = BD.ExecuteReturn(StrSql)
            'If Not DsCombo Is Nothing Then
            '    If DsCombo.Tables("CatDisenios").DefaultView.Count > 0 Then
            '        cmbidDisenio.DataSource = DsCombo.Tables("CatDisenios")
            '        cmbidDisenio.ValueMember = "idDisenio"
            '        cmbidDisenio.DisplayMember = "Descripcion"
            '    End If
            'Else
            '    MsgBox("No se han dado de alta a Dise�os de Llantas ", vbInformation, "Aviso" & Me.Text)
            '    LlenaCombos = False
            '    Exit Function
            'End If
        Catch ex As Exception

        End Try
    End Function

    Public Function AutoCompletarMarcas(ByVal Control As ComboBox) As AutoCompleteStringCollection

        Dim Coleccion As New AutoCompleteStringCollection
        Dim dt As DataTable

        RelMarcaTos = New RelMarcaTipOrdSerClass(0, 0)
        dt = RelMarcaTos.TablaMarcasxTos(True, TipoOrdenServicio.LLANTAS)

        'Recorrer y cargar los items para el Autocompletado
        For Each row As DataRow In dt.Rows
            Coleccion.Add(Convert.ToString(row("NombreMarca")))
        Next
        'Ajustamos el control TextBox o ComboBox para recibir los datos de la siguiente manera.
        With Control
            .DataSource = dt
            .DisplayMember = "NombreMarca"
            .ValueMember = "idMarca"


            .AutoCompleteCustomSource = Coleccion
            .AutoCompleteMode = AutoCompleteMode.Suggest
            .AutoCompleteSource = AutoCompleteSource.CustomSource
        End With

        ' Aca le devuelvo los datos a recuperados de la Base
        Return Coleccion

    End Function
    Public Function AutoCompletarDisenios(ByVal Control As ComboBox) As AutoCompleteStringCollection

        Dim Coleccion As New AutoCompleteStringCollection
        Dim dt As DataTable

        Disenio = New CatDiseniosClass(0)
        dt = Disenio.TablaDisenios(False)

        'Recorrer y cargar los items para el Autocompletado
        For Each row As DataRow In dt.Rows
            Coleccion.Add(Convert.ToString(row("Descripcion")))
        Next
        'Ajustamos el control TextBox o ComboBox para recibir los datos de la siguiente manera.
        With Control
            .DataSource = dt
            .DisplayMember = "Descripcion"
            .ValueMember = "idDisenio"


            .AutoCompleteCustomSource = Coleccion
            .AutoCompleteMode = AutoCompleteMode.Suggest
            .AutoCompleteSource = AutoCompleteSource.CustomSource
        End With

        ' Aca le devuelvo los datos a recuperados de la Base
        Return Coleccion

    End Function
End Class
