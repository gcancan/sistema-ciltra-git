'Fecha: 14 / FEBRERO / 2016
'ROGER GALA PACHECO
'************************************************************************************************************************************
'*  Fecha Modificacion :  14 / febrero / 2017                                                                             
'*  
'************************************************************************************************************************************
Imports Microsoft.VisualBasic.PowerPacks
Public Class frmCatLlantas
    Inherits System.Windows.Forms.Form
    Dim OpcionForma As TipoOpcionForma
    Dim TablaBd As String = "CatLlantas"
    Dim CampoLLave As String = "idLlanta"
    'Dim TablaBd2 As String = "CatTipoUniTrans"
    Public WithEvents MeStatus1 As ControlStatus.MeStatus
    Friend WithEvents GridTodos As System.Windows.Forms.DataGridView
    'Private Servicio As ServicioClass
    'Private TipoServ As TipoServClass

    Private Llanta As LlantasClass
    Dim llantaProd As LLantasProductoClass
    'Private UniTrans As UnidadesTranspClass
    Private TipoUniTrans As TipoUniTransClass
    Private Empresa As EmpresaClass
    'Private Almacen As AlmacenClass
    'Private Sucursal As SucursalClass
    Private Marca As MarcaClass
    Private TipoLLanta As TipoLlantaClass

    Friend WithEvents btnMnuOk As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuCancelar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuSalir As System.Windows.Forms.ToolStripButton
    Private _Tag As String
    Dim bEsIdentidad As Boolean = False
    Dim Salida As Boolean
    'Dim DsCombo As New DataTable
    Dim DsCombo As New DataSet
    Private _con As String
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents gpoPosicion As GroupBox
    Friend WithEvents GrupoCaptura As GroupBox
    Friend WithEvents Label10 As Label
    Friend WithEvents txtCosto As TextBox
    Friend WithEvents Label11 As Label
    Friend WithEvents txtFactura As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents txtProfundidad As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents txtMedida As TextBox
    Friend WithEvents txtLlantaP1 As TextBox
    Friend WithEvents txtLlantaP4 As TextBox
    Friend WithEvents txtLlantaP3 As TextBox
    Friend WithEvents txtLlantaP2 As TextBox
    Friend WithEvents label1 As Label
    Friend WithEvents cmdBuscaClave As Button
    Friend WithEvents txtidLlanta As TextBox
    Friend WithEvents Label9 As Label
    Friend WithEvents dtpFecha As DateTimePicker
    Friend WithEvents Label7 As Label
    Friend WithEvents cmdBuscaClaveEmp As Button
    Friend WithEvents txtidEmpresa As TextBox
    Friend WithEvents txtRazonSocial As TextBox
    Friend WithEvents txtidMarca As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents cmdBuscaClaveMar As Button
    Friend WithEvents txtNombreMarca As TextBox
    Friend WithEvents cmbEstatus As ComboBox
    Friend WithEvents Label8 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents cmdBuscaClaveTPLla As Button
    Friend WithEvents txtidTipoLLanta As TextBox
    Friend WithEvents txtDescripTLLA As TextBox
    Friend WithEvents label2 As Label
    Friend WithEvents txtDise�o As TextBox
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents txtPosicion As TextBox
    Friend WithEvents Label13 As Label
    Friend WithEvents txtTipoUni As TextBox
    Friend WithEvents Label12 As Label
    Friend WithEvents txtUniTrans As TextBox
    Friend WithEvents Label33 As Label
    Friend WithEvents btnGenerar As Button
    Friend WithEvents Label14 As Label
    Friend WithEvents txtRFDI As TextBox
    Private _v As Boolean

    Private Prefijo As String
    Private UltimoConsecutivo As String
    Dim UniTrans As UnidadesTranspClass
    Dim TipUni As TipoUniTransClass

    Dim ClasLlan As ClasLlanClass
    Dim TablaEstruc As DataTable

    Dim rdbPosicion() As RadioButton
    Private _Usuario As String

    Dim vUnidadOrig As String
    Friend WithEvents Label15 As Label
    Friend WithEvents txtObserva As TextBox
    Friend WithEvents GpoPosiciones As GroupBox
    Friend WithEvents ShapeContainer2 As ShapeContainer
    Friend WithEvents LineShape1 As LineShape
    Friend WithEvents Label16 As Label
    Friend WithEvents cmdBuscaClavellantaProd As Button
    Friend WithEvents txtidProductoLlanta As TextBox
    Dim vPosUniOrig As Integer


#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer
    Friend WithEvents ToolStripMenu As System.Windows.Forms.ToolStrip
    Friend WithEvents txtMenuBusca As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents btnMnuAltas As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuModificar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuEliminar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuReporte As System.Windows.Forms.ToolStripButton


    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCatLlantas))
        Me.ToolStripMenu = New System.Windows.Forms.ToolStrip()
        Me.txtMenuBusca = New System.Windows.Forms.ToolStripTextBox()
        Me.MeStatus1 = New ControlStatus.MeStatus()
        Me.GridTodos = New System.Windows.Forms.DataGridView()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.GpoPosiciones = New System.Windows.Forms.GroupBox()
        Me.ShapeContainer2 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        Me.LineShape1 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.gpoPosicion = New System.Windows.Forms.GroupBox()
        Me.txtPosicion = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtTipoUni = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtUniTrans = New System.Windows.Forms.TextBox()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.GrupoCaptura = New System.Windows.Forms.GroupBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txtidProductoLlanta = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txtObserva = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtRFDI = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtCosto = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtFactura = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtProfundidad = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtMedida = New System.Windows.Forms.TextBox()
        Me.txtLlantaP1 = New System.Windows.Forms.TextBox()
        Me.txtLlantaP4 = New System.Windows.Forms.TextBox()
        Me.txtLlantaP3 = New System.Windows.Forms.TextBox()
        Me.txtLlantaP2 = New System.Windows.Forms.TextBox()
        Me.label1 = New System.Windows.Forms.Label()
        Me.txtidLlanta = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.dtpFecha = New System.Windows.Forms.DateTimePicker()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtidEmpresa = New System.Windows.Forms.TextBox()
        Me.txtRazonSocial = New System.Windows.Forms.TextBox()
        Me.txtidMarca = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtNombreMarca = New System.Windows.Forms.TextBox()
        Me.cmbEstatus = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtidTipoLLanta = New System.Windows.Forms.TextBox()
        Me.txtDescripTLLA = New System.Windows.Forms.TextBox()
        Me.label2 = New System.Windows.Forms.Label()
        Me.txtDise�o = New System.Windows.Forms.TextBox()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.cmdBuscaClavellantaProd = New System.Windows.Forms.Button()
        Me.btnGenerar = New System.Windows.Forms.Button()
        Me.cmdBuscaClave = New System.Windows.Forms.Button()
        Me.cmdBuscaClaveEmp = New System.Windows.Forms.Button()
        Me.cmdBuscaClaveMar = New System.Windows.Forms.Button()
        Me.cmdBuscaClaveTPLla = New System.Windows.Forms.Button()
        Me.btnMnuAltas = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuModificar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuEliminar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuReporte = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuOk = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuCancelar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuSalir = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripMenu.SuspendLayout()
        CType(Me.GridTodos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.GpoPosiciones.SuspendLayout()
        Me.gpoPosicion.SuspendLayout()
        Me.GrupoCaptura.SuspendLayout()
        Me.SuspendLayout()
        '
        'ToolStripMenu
        '
        Me.ToolStripMenu.AutoSize = False
        Me.ToolStripMenu.BackColor = System.Drawing.Color.Silver
        Me.ToolStripMenu.Dock = System.Windows.Forms.DockStyle.None
        Me.ToolStripMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.txtMenuBusca, Me.btnMnuAltas, Me.btnMnuModificar, Me.btnMnuEliminar, Me.btnMnuReporte, Me.btnMnuOk, Me.btnMnuCancelar, Me.btnMnuSalir})
        Me.ToolStripMenu.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow
        Me.ToolStripMenu.Location = New System.Drawing.Point(3, 0)
        Me.ToolStripMenu.Name = "ToolStripMenu"
        Me.ToolStripMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.ToolStripMenu.Size = New System.Drawing.Size(730, 42)
        Me.ToolStripMenu.TabIndex = 1
        Me.ToolStripMenu.Text = "ToolStrip1"
        '
        'txtMenuBusca
        '
        Me.txtMenuBusca.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.txtMenuBusca.Name = "txtMenuBusca"
        Me.txtMenuBusca.Size = New System.Drawing.Size(150, 42)
        Me.txtMenuBusca.ToolTipText = "Filtra Listado"
        '
        'MeStatus1
        '
        Me.MeStatus1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.MeStatus1.Location = New System.Drawing.Point(0, 493)
        Me.MeStatus1.Margin = New System.Windows.Forms.Padding(4)
        Me.MeStatus1.Name = "MeStatus1"
        Me.MeStatus1.Size = New System.Drawing.Size(1171, 26)
        Me.MeStatus1.TabIndex = 72
        '
        'GridTodos
        '
        Me.GridTodos.AllowUserToAddRows = False
        Me.GridTodos.AllowUserToDeleteRows = False
        Me.GridTodos.AllowUserToResizeRows = False
        Me.GridTodos.BackgroundColor = System.Drawing.Color.White
        Me.GridTodos.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.GridTodos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.GridTodos.Location = New System.Drawing.Point(3, 45)
        Me.GridTodos.MultiSelect = False
        Me.GridTodos.Name = "GridTodos"
        Me.GridTodos.ReadOnly = True
        Me.GridTodos.RowHeadersVisible = False
        Me.GridTodos.Size = New System.Drawing.Size(162, 412)
        Me.GridTodos.TabIndex = 73
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Location = New System.Drawing.Point(171, 45)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(988, 441)
        Me.TabControl1.TabIndex = 74
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.GpoPosiciones)
        Me.TabPage1.Controls.Add(Me.gpoPosicion)
        Me.TabPage1.Controls.Add(Me.GrupoCaptura)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(980, 415)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Datos"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'GpoPosiciones
        '
        Me.GpoPosiciones.Controls.Add(Me.ShapeContainer2)
        Me.GpoPosiciones.Location = New System.Drawing.Point(536, 63)
        Me.GpoPosiciones.Name = "GpoPosiciones"
        Me.GpoPosiciones.Size = New System.Drawing.Size(427, 349)
        Me.GpoPosiciones.TabIndex = 44
        Me.GpoPosiciones.TabStop = False
        Me.GpoPosiciones.Text = "Posiciones"
        '
        'ShapeContainer2
        '
        Me.ShapeContainer2.Location = New System.Drawing.Point(3, 16)
        Me.ShapeContainer2.Margin = New System.Windows.Forms.Padding(0)
        Me.ShapeContainer2.Name = "ShapeContainer2"
        Me.ShapeContainer2.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.LineShape1})
        Me.ShapeContainer2.Size = New System.Drawing.Size(421, 330)
        Me.ShapeContainer2.TabIndex = 2
        Me.ShapeContainer2.TabStop = False
        '
        'LineShape1
        '
        Me.LineShape1.BorderWidth = 10
        Me.LineShape1.Name = "LineaPrinc"
        Me.LineShape1.X1 = 186
        Me.LineShape1.X2 = 186
        Me.LineShape1.Y1 = 36
        Me.LineShape1.Y2 = 280
        '
        'gpoPosicion
        '
        Me.gpoPosicion.Controls.Add(Me.txtPosicion)
        Me.gpoPosicion.Controls.Add(Me.Label13)
        Me.gpoPosicion.Controls.Add(Me.txtTipoUni)
        Me.gpoPosicion.Controls.Add(Me.Label12)
        Me.gpoPosicion.Controls.Add(Me.txtUniTrans)
        Me.gpoPosicion.Controls.Add(Me.Label33)
        Me.gpoPosicion.Location = New System.Drawing.Point(536, 6)
        Me.gpoPosicion.Name = "gpoPosicion"
        Me.gpoPosicion.Size = New System.Drawing.Size(438, 50)
        Me.gpoPosicion.TabIndex = 5
        Me.gpoPosicion.TabStop = False
        Me.gpoPosicion.Text = "Ubicaci�n"
        '
        'txtPosicion
        '
        Me.txtPosicion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPosicion.Location = New System.Drawing.Point(360, 20)
        Me.txtPosicion.MaxLength = 10
        Me.txtPosicion.Name = "txtPosicion"
        Me.txtPosicion.Size = New System.Drawing.Size(33, 20)
        Me.txtPosicion.TabIndex = 84
        Me.txtPosicion.Tag = "1"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label13.Location = New System.Drawing.Point(304, 22)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(50, 13)
        Me.Label13.TabIndex = 85
        Me.Label13.Text = "Posici�n:"
        '
        'txtTipoUni
        '
        Me.txtTipoUni.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTipoUni.Location = New System.Drawing.Point(194, 20)
        Me.txtTipoUni.MaxLength = 10
        Me.txtTipoUni.Name = "txtTipoUni"
        Me.txtTipoUni.ReadOnly = True
        Me.txtTipoUni.Size = New System.Drawing.Size(90, 20)
        Me.txtTipoUni.TabIndex = 82
        Me.txtTipoUni.Tag = "1"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label12.Location = New System.Drawing.Point(157, 20)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(31, 13)
        Me.Label12.TabIndex = 83
        Me.Label12.Text = "Tipo:"
        '
        'txtUniTrans
        '
        Me.txtUniTrans.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtUniTrans.Location = New System.Drawing.Point(63, 20)
        Me.txtUniTrans.MaxLength = 10
        Me.txtUniTrans.Name = "txtUniTrans"
        Me.txtUniTrans.Size = New System.Drawing.Size(90, 20)
        Me.txtUniTrans.TabIndex = 80
        Me.txtUniTrans.Tag = "1"
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label33.Location = New System.Drawing.Point(6, 23)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(44, 13)
        Me.Label33.TabIndex = 81
        Me.Label33.Text = "Unidad:"
        '
        'GrupoCaptura
        '
        Me.GrupoCaptura.Controls.Add(Me.Label16)
        Me.GrupoCaptura.Controls.Add(Me.cmdBuscaClavellantaProd)
        Me.GrupoCaptura.Controls.Add(Me.txtidProductoLlanta)
        Me.GrupoCaptura.Controls.Add(Me.Label15)
        Me.GrupoCaptura.Controls.Add(Me.txtObserva)
        Me.GrupoCaptura.Controls.Add(Me.btnGenerar)
        Me.GrupoCaptura.Controls.Add(Me.Label14)
        Me.GrupoCaptura.Controls.Add(Me.txtRFDI)
        Me.GrupoCaptura.Controls.Add(Me.Label10)
        Me.GrupoCaptura.Controls.Add(Me.txtCosto)
        Me.GrupoCaptura.Controls.Add(Me.Label11)
        Me.GrupoCaptura.Controls.Add(Me.txtFactura)
        Me.GrupoCaptura.Controls.Add(Me.Label6)
        Me.GrupoCaptura.Controls.Add(Me.txtProfundidad)
        Me.GrupoCaptura.Controls.Add(Me.Label5)
        Me.GrupoCaptura.Controls.Add(Me.txtMedida)
        Me.GrupoCaptura.Controls.Add(Me.txtLlantaP1)
        Me.GrupoCaptura.Controls.Add(Me.txtLlantaP4)
        Me.GrupoCaptura.Controls.Add(Me.txtLlantaP3)
        Me.GrupoCaptura.Controls.Add(Me.txtLlantaP2)
        Me.GrupoCaptura.Controls.Add(Me.label1)
        Me.GrupoCaptura.Controls.Add(Me.cmdBuscaClave)
        Me.GrupoCaptura.Controls.Add(Me.txtidLlanta)
        Me.GrupoCaptura.Controls.Add(Me.Label9)
        Me.GrupoCaptura.Controls.Add(Me.dtpFecha)
        Me.GrupoCaptura.Controls.Add(Me.Label7)
        Me.GrupoCaptura.Controls.Add(Me.cmdBuscaClaveEmp)
        Me.GrupoCaptura.Controls.Add(Me.txtidEmpresa)
        Me.GrupoCaptura.Controls.Add(Me.txtRazonSocial)
        Me.GrupoCaptura.Controls.Add(Me.txtidMarca)
        Me.GrupoCaptura.Controls.Add(Me.Label4)
        Me.GrupoCaptura.Controls.Add(Me.cmdBuscaClaveMar)
        Me.GrupoCaptura.Controls.Add(Me.txtNombreMarca)
        Me.GrupoCaptura.Controls.Add(Me.cmbEstatus)
        Me.GrupoCaptura.Controls.Add(Me.Label8)
        Me.GrupoCaptura.Controls.Add(Me.Label3)
        Me.GrupoCaptura.Controls.Add(Me.cmdBuscaClaveTPLla)
        Me.GrupoCaptura.Controls.Add(Me.txtidTipoLLanta)
        Me.GrupoCaptura.Controls.Add(Me.txtDescripTLLA)
        Me.GrupoCaptura.Controls.Add(Me.label2)
        Me.GrupoCaptura.Controls.Add(Me.txtDise�o)
        Me.GrupoCaptura.Location = New System.Drawing.Point(6, 6)
        Me.GrupoCaptura.Name = "GrupoCaptura"
        Me.GrupoCaptura.Size = New System.Drawing.Size(524, 403)
        Me.GrupoCaptura.TabIndex = 4
        Me.GrupoCaptura.TabStop = False
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label16.Location = New System.Drawing.Point(3, 199)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(62, 13)
        Me.Label16.TabIndex = 113
        Me.Label16.Text = "Id Producto"
        '
        'txtidProductoLlanta
        '
        Me.txtidProductoLlanta.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtidProductoLlanta.Location = New System.Drawing.Point(76, 194)
        Me.txtidProductoLlanta.Name = "txtidProductoLlanta"
        Me.txtidProductoLlanta.Size = New System.Drawing.Size(69, 20)
        Me.txtidProductoLlanta.TabIndex = 110
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label15.Location = New System.Drawing.Point(3, 333)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(53, 13)
        Me.Label15.TabIndex = 109
        Me.Label15.Text = "Observa.:"
        '
        'txtObserva
        '
        Me.txtObserva.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObserva.Location = New System.Drawing.Point(76, 328)
        Me.txtObserva.MaxLength = 150
        Me.txtObserva.Multiline = True
        Me.txtObserva.Name = "txtObserva"
        Me.txtObserva.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtObserva.Size = New System.Drawing.Size(442, 42)
        Me.txtObserva.TabIndex = 108
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label14.Location = New System.Drawing.Point(3, 381)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(35, 13)
        Me.Label14.TabIndex = 106
        Me.Label14.Text = "RFDI:"
        '
        'txtRFDI
        '
        Me.txtRFDI.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRFDI.Location = New System.Drawing.Point(73, 376)
        Me.txtRFDI.MaxLength = 50
        Me.txtRFDI.Name = "txtRFDI"
        Me.txtRFDI.ReadOnly = True
        Me.txtRFDI.Size = New System.Drawing.Size(393, 20)
        Me.txtRFDI.TabIndex = 105
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label10.Location = New System.Drawing.Point(293, 281)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(37, 13)
        Me.Label10.TabIndex = 104
        Me.Label10.Text = "Costo:"
        '
        'txtCosto
        '
        Me.txtCosto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCosto.Location = New System.Drawing.Point(354, 278)
        Me.txtCosto.MaxLength = 50
        Me.txtCosto.Name = "txtCosto"
        Me.txtCosto.Size = New System.Drawing.Size(112, 20)
        Me.txtCosto.TabIndex = 103
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label11.Location = New System.Drawing.Point(3, 280)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(46, 13)
        Me.Label11.TabIndex = 102
        Me.Label11.Text = "Factura:"
        '
        'txtFactura
        '
        Me.txtFactura.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtFactura.Location = New System.Drawing.Point(76, 275)
        Me.txtFactura.MaxLength = 50
        Me.txtFactura.Name = "txtFactura"
        Me.txtFactura.Size = New System.Drawing.Size(150, 20)
        Me.txtFactura.TabIndex = 101
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label6.Location = New System.Drawing.Point(3, 251)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(67, 13)
        Me.Label6.TabIndex = 100
        Me.Label6.Text = "Profundidad:"
        '
        'txtProfundidad
        '
        Me.txtProfundidad.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtProfundidad.Location = New System.Drawing.Point(76, 249)
        Me.txtProfundidad.MaxLength = 50
        Me.txtProfundidad.Name = "txtProfundidad"
        Me.txtProfundidad.Size = New System.Drawing.Size(112, 20)
        Me.txtProfundidad.TabIndex = 99
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label5.Location = New System.Drawing.Point(296, 229)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(45, 13)
        Me.Label5.TabIndex = 98
        Me.Label5.Text = "Medida:"
        '
        'txtMedida
        '
        Me.txtMedida.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMedida.Location = New System.Drawing.Point(357, 226)
        Me.txtMedida.MaxLength = 50
        Me.txtMedida.Name = "txtMedida"
        Me.txtMedida.Size = New System.Drawing.Size(112, 20)
        Me.txtMedida.TabIndex = 97
        '
        'txtLlantaP1
        '
        Me.txtLlantaP1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtLlantaP1.Location = New System.Drawing.Point(247, 22)
        Me.txtLlantaP1.MaxLength = 10
        Me.txtLlantaP1.Name = "txtLlantaP1"
        Me.txtLlantaP1.Size = New System.Drawing.Size(25, 20)
        Me.txtLlantaP1.TabIndex = 96
        '
        'txtLlantaP4
        '
        Me.txtLlantaP4.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtLlantaP4.Location = New System.Drawing.Point(365, 22)
        Me.txtLlantaP4.MaxLength = 10
        Me.txtLlantaP4.Name = "txtLlantaP4"
        Me.txtLlantaP4.Size = New System.Drawing.Size(39, 20)
        Me.txtLlantaP4.TabIndex = 95
        '
        'txtLlantaP3
        '
        Me.txtLlantaP3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtLlantaP3.Location = New System.Drawing.Point(328, 21)
        Me.txtLlantaP3.MaxLength = 10
        Me.txtLlantaP3.Name = "txtLlantaP3"
        Me.txtLlantaP3.Size = New System.Drawing.Size(25, 20)
        Me.txtLlantaP3.TabIndex = 94
        '
        'txtLlantaP2
        '
        Me.txtLlantaP2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtLlantaP2.Location = New System.Drawing.Point(278, 22)
        Me.txtLlantaP2.MaxLength = 10
        Me.txtLlantaP2.Name = "txtLlantaP2"
        Me.txtLlantaP2.Size = New System.Drawing.Size(44, 20)
        Me.txtLlantaP2.TabIndex = 93
        '
        'label1
        '
        Me.label1.AutoSize = True
        Me.label1.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.label1.Location = New System.Drawing.Point(3, 27)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(53, 13)
        Me.label1.TabIndex = 92
        Me.label1.Text = "ID Llanta:"
        '
        'txtidLlanta
        '
        Me.txtidLlanta.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtidLlanta.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtidLlanta.ForeColor = System.Drawing.Color.Blue
        Me.txtidLlanta.Location = New System.Drawing.Point(76, 22)
        Me.txtidLlanta.MaxLength = 10
        Me.txtidLlanta.Name = "txtidLlanta"
        Me.txtidLlanta.Size = New System.Drawing.Size(112, 22)
        Me.txtidLlanta.TabIndex = 90
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label9.Location = New System.Drawing.Point(3, 92)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(40, 13)
        Me.Label9.TabIndex = 85
        Me.Label9.Text = "Fecha:"
        '
        'dtpFecha
        '
        Me.dtpFecha.Location = New System.Drawing.Point(76, 90)
        Me.dtpFecha.Name = "dtpFecha"
        Me.dtpFecha.Size = New System.Drawing.Size(225, 20)
        Me.dtpFecha.TabIndex = 84
        Me.dtpFecha.Value = New Date(2016, 4, 27, 0, 0, 0, 0)
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label7.Location = New System.Drawing.Point(3, 62)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(51, 13)
        Me.Label7.TabIndex = 52
        Me.Label7.Text = "Empresa:"
        '
        'txtidEmpresa
        '
        Me.txtidEmpresa.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtidEmpresa.Location = New System.Drawing.Point(76, 57)
        Me.txtidEmpresa.Name = "txtidEmpresa"
        Me.txtidEmpresa.Size = New System.Drawing.Size(69, 20)
        Me.txtidEmpresa.TabIndex = 6
        '
        'txtRazonSocial
        '
        Me.txtRazonSocial.Enabled = False
        Me.txtRazonSocial.Location = New System.Drawing.Point(197, 57)
        Me.txtRazonSocial.Name = "txtRazonSocial"
        Me.txtRazonSocial.ReadOnly = True
        Me.txtRazonSocial.Size = New System.Drawing.Size(275, 20)
        Me.txtRazonSocial.TabIndex = 9
        '
        'txtidMarca
        '
        Me.txtidMarca.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtidMarca.Location = New System.Drawing.Point(76, 126)
        Me.txtidMarca.Name = "txtidMarca"
        Me.txtidMarca.Size = New System.Drawing.Size(69, 20)
        Me.txtidMarca.TabIndex = 8
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label4.Location = New System.Drawing.Point(3, 131)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(40, 13)
        Me.Label4.TabIndex = 40
        Me.Label4.Text = "Marca:"
        '
        'txtNombreMarca
        '
        Me.txtNombreMarca.Enabled = False
        Me.txtNombreMarca.Location = New System.Drawing.Point(194, 130)
        Me.txtNombreMarca.Name = "txtNombreMarca"
        Me.txtNombreMarca.ReadOnly = True
        Me.txtNombreMarca.Size = New System.Drawing.Size(243, 20)
        Me.txtNombreMarca.TabIndex = 12
        '
        'cmbEstatus
        '
        Me.cmbEstatus.AutoCompleteCustomSource.AddRange(New String() {"Regularizaci�n", "Vivienda"})
        Me.cmbEstatus.FormattingEnabled = True
        Me.cmbEstatus.Items.AddRange(New Object() {"ACTIVO", "INACTIVO"})
        Me.cmbEstatus.Location = New System.Drawing.Point(76, 301)
        Me.cmbEstatus.Name = "cmbEstatus"
        Me.cmbEstatus.Size = New System.Drawing.Size(112, 21)
        Me.cmbEstatus.TabIndex = 14
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label8.Location = New System.Drawing.Point(3, 306)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(45, 13)
        Me.Label8.TabIndex = 36
        Me.Label8.Text = "Estatus:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label3.Location = New System.Drawing.Point(3, 169)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(63, 13)
        Me.Label3.TabIndex = 20
        Me.Label3.Text = "Tipo Llanta:"
        '
        'txtidTipoLLanta
        '
        Me.txtidTipoLLanta.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtidTipoLLanta.Location = New System.Drawing.Point(76, 164)
        Me.txtidTipoLLanta.Name = "txtidTipoLLanta"
        Me.txtidTipoLLanta.Size = New System.Drawing.Size(69, 20)
        Me.txtidTipoLLanta.TabIndex = 3
        '
        'txtDescripTLLA
        '
        Me.txtDescripTLLA.Enabled = False
        Me.txtDescripTLLA.Location = New System.Drawing.Point(194, 168)
        Me.txtDescripTLLA.Name = "txtDescripTLLA"
        Me.txtDescripTLLA.ReadOnly = True
        Me.txtDescripTLLA.Size = New System.Drawing.Size(275, 20)
        Me.txtDescripTLLA.TabIndex = 5
        '
        'label2
        '
        Me.label2.AutoSize = True
        Me.label2.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.label2.Location = New System.Drawing.Point(3, 228)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(43, 13)
        Me.label2.TabIndex = 16
        Me.label2.Text = "Dise�o:"
        '
        'txtDise�o
        '
        Me.txtDise�o.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDise�o.Location = New System.Drawing.Point(76, 223)
        Me.txtDise�o.MaxLength = 50
        Me.txtDise�o.Name = "txtDise�o"
        Me.txtDise�o.Size = New System.Drawing.Size(150, 20)
        Me.txtDise�o.TabIndex = 5
        '
        'TabPage2
        '
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(980, 415)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Ubicaci�n"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'cmdBuscaClavellantaProd
        '
        Me.cmdBuscaClavellantaProd.Image = CType(resources.GetObject("cmdBuscaClavellantaProd.Image"), System.Drawing.Image)
        Me.cmdBuscaClavellantaProd.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdBuscaClavellantaProd.Location = New System.Drawing.Point(156, 186)
        Me.cmdBuscaClavellantaProd.Name = "cmdBuscaClavellantaProd"
        Me.cmdBuscaClavellantaProd.Size = New System.Drawing.Size(32, 32)
        Me.cmdBuscaClavellantaProd.TabIndex = 111
        Me.cmdBuscaClavellantaProd.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'btnGenerar
        '
        Me.btnGenerar.Image = Global.Fletera.My.Resources.Resources._1487050303_Add
        Me.btnGenerar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnGenerar.Location = New System.Drawing.Point(307, 83)
        Me.btnGenerar.Name = "btnGenerar"
        Me.btnGenerar.Size = New System.Drawing.Size(46, 41)
        Me.btnGenerar.TabIndex = 107
        Me.btnGenerar.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cmdBuscaClave
        '
        Me.cmdBuscaClave.Image = CType(resources.GetObject("cmdBuscaClave.Image"), System.Drawing.Image)
        Me.cmdBuscaClave.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdBuscaClave.Location = New System.Drawing.Point(197, 17)
        Me.cmdBuscaClave.Name = "cmdBuscaClave"
        Me.cmdBuscaClave.Size = New System.Drawing.Size(32, 32)
        Me.cmdBuscaClave.TabIndex = 91
        Me.cmdBuscaClave.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cmdBuscaClaveEmp
        '
        Me.cmdBuscaClaveEmp.Image = CType(resources.GetObject("cmdBuscaClaveEmp.Image"), System.Drawing.Image)
        Me.cmdBuscaClaveEmp.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdBuscaClaveEmp.Location = New System.Drawing.Point(159, 52)
        Me.cmdBuscaClaveEmp.Name = "cmdBuscaClaveEmp"
        Me.cmdBuscaClaveEmp.Size = New System.Drawing.Size(32, 32)
        Me.cmdBuscaClaveEmp.TabIndex = 7
        Me.cmdBuscaClaveEmp.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cmdBuscaClaveMar
        '
        Me.cmdBuscaClaveMar.Image = CType(resources.GetObject("cmdBuscaClaveMar.Image"), System.Drawing.Image)
        Me.cmdBuscaClaveMar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdBuscaClaveMar.Location = New System.Drawing.Point(156, 118)
        Me.cmdBuscaClaveMar.Name = "cmdBuscaClaveMar"
        Me.cmdBuscaClaveMar.Size = New System.Drawing.Size(32, 32)
        Me.cmdBuscaClaveMar.TabIndex = 9
        Me.cmdBuscaClaveMar.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cmdBuscaClaveTPLla
        '
        Me.cmdBuscaClaveTPLla.Image = CType(resources.GetObject("cmdBuscaClaveTPLla.Image"), System.Drawing.Image)
        Me.cmdBuscaClaveTPLla.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdBuscaClaveTPLla.Location = New System.Drawing.Point(156, 156)
        Me.cmdBuscaClaveTPLla.Name = "cmdBuscaClaveTPLla"
        Me.cmdBuscaClaveTPLla.Size = New System.Drawing.Size(32, 32)
        Me.cmdBuscaClaveTPLla.TabIndex = 4
        Me.cmdBuscaClaveTPLla.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'btnMnuAltas
        '
        Me.btnMnuAltas.ForeColor = System.Drawing.Color.Red
        Me.btnMnuAltas.Image = CType(resources.GetObject("btnMnuAltas.Image"), System.Drawing.Image)
        Me.btnMnuAltas.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuAltas.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuAltas.Name = "btnMnuAltas"
        Me.btnMnuAltas.Size = New System.Drawing.Size(44, 39)
        Me.btnMnuAltas.Text = "&ALTAS"
        Me.btnMnuAltas.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnMnuAltas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuModificar
        '
        Me.btnMnuModificar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuModificar.Image = CType(resources.GetObject("btnMnuModificar.Image"), System.Drawing.Image)
        Me.btnMnuModificar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuModificar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuModificar.Name = "btnMnuModificar"
        Me.btnMnuModificar.Size = New System.Drawing.Size(74, 39)
        Me.btnMnuModificar.Text = "&MODIFICAR"
        Me.btnMnuModificar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnMnuModificar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuEliminar
        '
        Me.btnMnuEliminar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuEliminar.Image = CType(resources.GetObject("btnMnuEliminar.Image"), System.Drawing.Image)
        Me.btnMnuEliminar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuEliminar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuEliminar.Name = "btnMnuEliminar"
        Me.btnMnuEliminar.Size = New System.Drawing.Size(39, 39)
        Me.btnMnuEliminar.Text = "&BAJA"
        Me.btnMnuEliminar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuReporte
        '
        Me.btnMnuReporte.ForeColor = System.Drawing.Color.Red
        Me.btnMnuReporte.Image = CType(resources.GetObject("btnMnuReporte.Image"), System.Drawing.Image)
        Me.btnMnuReporte.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuReporte.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuReporte.Name = "btnMnuReporte"
        Me.btnMnuReporte.Size = New System.Drawing.Size(59, 39)
        Me.btnMnuReporte.Text = "&REPORTE"
        Me.btnMnuReporte.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuOk
        '
        Me.btnMnuOk.ForeColor = System.Drawing.Color.Red
        Me.btnMnuOk.Image = CType(resources.GetObject("btnMnuOk.Image"), System.Drawing.Image)
        Me.btnMnuOk.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuOk.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuOk.Name = "btnMnuOk"
        Me.btnMnuOk.Size = New System.Drawing.Size(56, 39)
        Me.btnMnuOk.Text = "&GRABAR"
        Me.btnMnuOk.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuCancelar
        '
        Me.btnMnuCancelar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuCancelar.Image = CType(resources.GetObject("btnMnuCancelar.Image"), System.Drawing.Image)
        Me.btnMnuCancelar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuCancelar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuCancelar.Name = "btnMnuCancelar"
        Me.btnMnuCancelar.Size = New System.Drawing.Size(71, 39)
        Me.btnMnuCancelar.Text = "&CANCELAR"
        Me.btnMnuCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuSalir
        '
        Me.btnMnuSalir.ForeColor = System.Drawing.Color.Red
        Me.btnMnuSalir.Image = CType(resources.GetObject("btnMnuSalir.Image"), System.Drawing.Image)
        Me.btnMnuSalir.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuSalir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuSalir.Name = "btnMnuSalir"
        Me.btnMnuSalir.Size = New System.Drawing.Size(41, 39)
        Me.btnMnuSalir.Text = "&SALIR"
        Me.btnMnuSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'frmCatLlantas
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(1171, 519)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.GridTodos)
        Me.Controls.Add(Me.MeStatus1)
        Me.Controls.Add(Me.ToolStripMenu)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmCatLlantas"
        Me.Text = "Catal�go de Unidades de Llantas"
        Me.ToolStripMenu.ResumeLayout(False)
        Me.ToolStripMenu.PerformLayout()
        CType(Me.GridTodos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.GpoPosiciones.ResumeLayout(False)
        Me.gpoPosicion.ResumeLayout(False)
        Me.gpoPosicion.PerformLayout()
        Me.GrupoCaptura.ResumeLayout(False)
        Me.GrupoCaptura.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

#End Region
    Sub New(ByVal vTag As String)
        InitializeComponent()
        _Tag = vTag
    End Sub

    Public Sub New(con As String, v As Boolean, nomUsuario As String)
        _con = con
        BD = New CapaDatos.UtilSQL(con, "Juan")
        InitializeComponent()
        _v = v
        _Usuario = nomUsuario
    End Sub
#Region "MenusToolStrip"
    Private Sub LimpiaVariables()
        SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")

    End Sub
    Private Sub LimpiaCampos()
        txtidLlanta.Text = ""
        txtidTipoLLanta.Text = ""
        txtDescripTLLA.Text = ""
        txtDise�o.Text = ""
        txtidEmpresa.Text = ""
        txtRazonSocial.Text = ""
        txtidMarca.Text = ""
        txtNombreMarca.Text = ""
        cmbEstatus.SelectedIndex = 0
        txtMedida.Text = ""
        txtProfundidad.Text = ""
        txtFactura.Text = ""
        txtCosto.Text = ""

        txtUniTrans.Text = ""
        txtTipoUni.Text = ""
        txtPosicion.Text = ""

        LimpiaVariables()
    End Sub

    Private Sub ActivaCampos(ByVal valor As Boolean, ByVal vOpcion As TipoOpcActivaCampos)
        If vOpcion = TipoOpcActivaCampos.tOpcDESHABTODOS Then
            txtidLlanta.Enabled = False
            txtidTipoLLanta.Enabled = False
            'txtnomTipoUniTras.Enabled = False
            txtDise�o.Enabled = False
            txtMedida.Enabled = False
            txtProfundidad.Enabled = False
            txtFactura.Enabled = False
            txtCosto.Enabled = False

            txtidMarca.Enabled = False
            cmbEstatus.Enabled = False
            txtidEmpresa.Enabled = False
            dtpFecha.Enabled = False

            gpoPosicion.Enabled = False
            GpoPosiciones.Enabled = False

            txtUniTrans.Enabled = False
            txtTipoUni.Enabled = False
            txtPosicion.Enabled = False

        ElseIf vOpcion = TipoOpcActivaCampos.tOpcINICIALIZA Then
            txtidLlanta.Enabled = Not valor
            txtidTipoLLanta.Enabled = valor
            'txtnomTipoUniTras.Enabled = valor
            txtDise�o.Enabled = valor
            txtMedida.Enabled = valor
            txtProfundidad.Enabled = valor
            txtFactura.Enabled = valor
            txtCosto.Enabled = valor

            txtidMarca.Enabled = valor
            cmbEstatus.Enabled = valor
            txtidEmpresa.Enabled = valor
            dtpFecha.Enabled = valor

            If OpcionForma = TipoOpcionForma.tOpcInsertar Or OpcionForma = TipoOpcionForma.tOpcModificar Then
                gpoPosicion.Enabled = valor
                GpoPosiciones.Enabled = valor
                txtUniTrans.Enabled = valor
                txtTipoUni.Enabled = valor
                txtPosicion.Enabled = valor
            Else
                gpoPosicion.Enabled = False
                GpoPosiciones.Enabled = False
                txtUniTrans.Enabled = False
                txtTipoUni.Enabled = False
                txtPosicion.Enabled = False

            End If
        End If
    End Sub


    Private Sub ActivaBotones(ByVal valor As Boolean, Optional ByVal vOpcion As TipoOpcActivaBoton = TipoOpcActivaBoton.tOpcInicializa)
        If vOpcion = TipoOpcActivaBoton.tOpcConsulta Then
            btnMnuOk.Enabled = False
            btnMnuCancelar.Enabled = valor
            btnMnuSalir.Enabled = Not valor

            btnMnuAltas.Enabled = Not valor
            btnMnuModificar.Enabled = Not valor

            btnMnuEliminar.Enabled = Not valor


            btnMnuReporte.Enabled = Not valor
        ElseIf vOpcion = TipoOpcActivaBoton.tOpcInicializa Then
            btnMnuOk.Enabled = valor
            btnMnuCancelar.Enabled = valor
            btnMnuSalir.Enabled = Not valor

            btnMnuAltas.Enabled = Not valor
            btnMnuModificar.Enabled = Not valor
            btnMnuEliminar.Enabled = Not valor
            btnMnuReporte.Enabled = Not valor

            txtMenuBusca.Enabled = Not valor
            GridTodos.Enabled = Not valor
        ElseIf vOpcion = TipoOpcActivaBoton.tOpcInsertar Then
            btnMnuOk.Enabled = valor
            btnMnuCancelar.Enabled = valor
            btnMnuSalir.Enabled = Not valor

            btnMnuAltas.Enabled = Not valor
            btnMnuModificar.Enabled = Not valor
            btnMnuEliminar.Enabled = Not valor
            btnMnuReporte.Enabled = Not valor

            btnGenerar.Enabled = valor
        End If
    End Sub

    Private Sub btnMnuAltas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuAltas.Click
        'ALTAS
        '15/FEB/2016
        'If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Alta) = False Then
        '    MessageBox.Show("No tiene los permisos Necesarios para poder dar de Alta en el Catalogo de Servicios!!!",
        '        "Acceso Denegado a Alta de Servicios", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    OpcionForma = TipoOpcionForma.tOpcConsultar
        '    Exit Sub
        'End If
        OpcionForma = TipoOpcionForma.tOpcInsertar
        LimpiaCampos()
        ActivaCampos(True, TipoOpcActivaCampos.tOpcINICIALIZA)
        ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)

        If bEsIdentidad Then
            txtidLlanta.Enabled = False
            txtidTipoLLanta.Focus()
            txtidTipoLLanta.SelectAll()
        Else
            txtidLlanta.Enabled = True
            'txtidLlanta.Focus()

            txtidEmpresa.Focus()
            txtidEmpresa.SelectAll()
        End If
        Status("Ingrese nueva Llanta para guardar", Me)
    End Sub
    Private Sub btnMnuModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuModificar.Click
        'MODIFICAR
        'If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Modificar) = False Then
        '    MessageBox.Show("No tiene los permisos Necesarios para poder Modificar la Informacion del Servicio!!!",
        '           "Acceso Denegado a Modificacion de la Informacion del Servicio", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    OpcionForma = TipoOpcionForma.tOpcConsultar
        '    Exit Sub
        'End If
        If Trim(txtidLlanta.Text) = "" Then
            txtidLlanta.Text = DespliegaGrid(TablaBd, Inicio.CONSTR, CampoLLave)
            CargaForm(txtidLlanta.Text, UCase(TablaBd))
        End If

        OpcionForma = TipoOpcionForma.tOpcModificar
        ActivaCampos(True, TipoOpcActivaCampos.tOpcINICIALIZA)
        ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)

        btnMnuEliminar.Enabled = True
        If bEsIdentidad Then
            txtidLlanta.Enabled = False
            'txtDescripcionUni.Focus()
        Else
            'txtidUnidadTrans.Focus()
        End If
        txtidTipoLLanta.Focus()


        Status("Ingrese Unidad para guardar", Me)
    End Sub
    Private Sub btnMnuEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuEliminar.Click
        'ELIMINAR
        'If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Baja) = False Then
        '    MessageBox.Show("No tiene los permisos Necesarios para poder Eliminar el Servicio!!!",
        '        "Acceso Denegado a Eliminacion de Servicio", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    OpcionForma = TipoOpcionForma.tOpcConsultar
        '    Exit Sub
        'End If
        If txtidLlanta.Text = Llanta.idLlanta Then
            With Llanta
                If MessageBox.Show("Esta completamente seguro que desea dar de Baja a la Llanta: " &
                                   " Con Id: " & .idLlanta & " ????", "Confirma que desea dar de Baja el Servicio: " & .idLlanta & "???",
                                 MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then Exit Sub
                Try
                    .Eliminar() 'Dar de Baja a la Llanta
                    .GuardaMov("BAJA", FormatFecHora(Now, True), _Usuario, txtidLlanta.Text, txtUniTrans.Text, txtPosicion.Text, txtUniTrans.Text, txtPosicion.Text, txtObserva.Text)

                    MessageBox.Show("Llanta: " & .idLlanta & " dada de Baja con exito!!!",
                                    "Llanta dada de Baja con exito", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Status("Llanta: " & .idLlanta & " dad de Baja con exito!!!", Me)
                    ActualizaGrid(txtMenuBusca.Text)
                    txtidLlanta.Text = ""
                    OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
                Catch ex As Exception
                    Status("Error al tratar de dar de Baja a la Llanta: " & ex.Message, Me, Err:=True)
                    MessageBox.Show("Error al Tratar de dar de Baja a la Llanta: " & .idLlanta & vbNewLine &
                                    "Error. " & ex.Message, "Error: " & Err.Number, MessageBoxButtons.OK, MessageBoxIcon.Error)

                End Try
            End With
        End If
    End Sub
    Private Sub btnMnuReporte_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuReporte.Click
        'REPORTE
        'If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Reporte) = False Then
        '    MessageBox.Show("No tiene los permisos Necesarios para poder realizar Reporte de Servicios!!!",
        '        "Acceso Denegado a Reporte de Servicios", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    OpcionForma = TipoOpcionForma.tOpcConsultar
        '    Exit Sub
        'End If
        ToolStripMenu.Enabled = False
        Status("Abriendo Reporte Espere. . . ", Me)
        Me.Cursor = Cursors.WaitCursor
        DespliegaReporte(TablaBd, CONSTR)
        Status("Finalizo el reporte. . . ", Me)
        Me.Cursor = Cursors.Default
        ToolStripMenu.Enabled = True
    End Sub

#End Region
    Private Sub ActualizaGrid(ByVal vidLlanta As String)
        If vidLlanta = "" Then
            GridTodos.DataSource = BD.ExecuteReturn("SELECT idLlanta FROM " & TablaBd, TablaBd)
            Status(GridTodos.RowCount & " Registros", Me)
        Else
            GridTodos.DataSource = BD.ExecuteReturn("SELECT idLlanta FROM " & TablaBd &
                                                " WHERE idLlanta Like '%" & vidLlanta & "%'")
            Status(GridTodos.RowCount & " Registros obtenidos con el filtro %" & vidLlanta & "%", Me)
        End If
    End Sub

    Private Sub OpcionMnuTools(ByVal vOpcion As TipoOpcionForma)
        If vOpcion <> TipoOpcionForma.tOpcImprimir And vOpcion <> TipoOpcionForma.tOpcEliminar Then
            OpcionForma = vOpcion
        End If
        txtMenuBusca.Enabled = True
        GridTodos.Enabled = True
        If vOpcion = TipoOpcionForma.tOpcConsultar Then
            'CONSULTA
            For Each ct As Control In GrupoCaptura.Controls
                If Not ct Is cmdBuscaClave Then
                    ct.Enabled = False
                Else
                    ct.Enabled = True
                End If
            Next
            gpoPosicion.Enabled = False
            GpoPosiciones.Enabled = False

            txtMenuBusca.Enabled = True
            btnMnuAltas.Enabled = True
            btnMnuModificar.Enabled = True
            btnMnuEliminar.Enabled = False
            btnMnuReporte.Enabled = True
            btnMnuOk.Enabled = False
            btnMnuCancelar.Enabled = False

            btnMnuSalir.Enabled = True

            GeneraAutoacompletar()
        End If
    End Sub


    Private Sub txtMenuBusca_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtMenuBusca.KeyPress
        If Asc(e.KeyChar) = 39 Then e.Handled = True : Exit Sub 'Caracter ' Evita errores
        If Asc(e.KeyChar) = 13 Then
            GridTodos.Focus()
            e.Handled = True
        End If
    End Sub

    'Private Sub txtMenuBusca_KeyUp(sender As Object, e As KeyEventArgs) Handles txtMenuBusca.KeyUp
    '    If txtMenuBusca.Text.Trim = "" Then Exit Sub
    '    If e.KeyCode = Keys.Down Or e.KeyCode = Keys.Up Then
    '        GridTodos.Focus()
    '        e.Handled = True
    '        Exit Sub
    '    End If
    '    If e.KeyCode = Keys.Left Or e.KeyCode = Keys.Right Then Exit Sub
    '    ActualizaGrid(txtMenuBusca.Text)
    'End Sub

    Public Function AutoCompletar(ByVal Control As TextBox, ByVal strSql As String, ByVal NomCampo As String, ByVal NomTabla As String, Optional ByVal FiltroSinWhere As String = "")
        Dim objSql As New ToolSQLs
        Dim Coleccion As New AutoCompleteStringCollection
        Dim dt As DataTable = objSql.CreaTabla(NomTabla, strSql, FiltroSinWhere)
        For Each row As DataRow In dt.Rows
            Coleccion.Add(Convert.ToString(row(NomCampo)))
        Next
        With Control

            .AutoCompleteCustomSource = Coleccion
            .AutoCompleteMode = AutoCompleteMode.Suggest
            .AutoCompleteSource = AutoCompleteSource.CustomSource
        End With
        ' Aca le devuelvo los datos a recuperados de la Base
        Return Coleccion
    End Function

    Public Function AutoCompletarUnidadTransp(ByVal Control As TextBox, ByVal Activo As Boolean, ByVal idEmpresa As Integer,
                                              ByVal Tipo As String, Optional ByVal FiltroSinWhere As String = "") As AutoCompleteStringCollection

        Dim Coleccion As New AutoCompleteStringCollection

        UniTrans = New UnidadesTranspClass(0)

        Dim dt As DataTable = UniTrans.TablaUnidadxTipo2(idEmpresa, FiltroSinWhere)
        'Recorrer y cargar los items para el Autocompletado
        For Each row As DataRow In dt.Rows
            Coleccion.Add(Convert.ToString(row("idUnidadTrans")))
        Next
        'Ajustamos el control TextBox o ComboBox para recibir los datos de la siguiente manera.
        With Control

            .AutoCompleteCustomSource = Coleccion
            .AutoCompleteMode = AutoCompleteMode.Suggest
            .AutoCompleteSource = AutoCompleteSource.CustomSource
        End With

        ' Aca le devuelvo los datos a recuperados de la Base
        Return Coleccion
    End Function

    Private Sub GeneraAutoacompletar()
        AutoCompletarUnidadTransp(txtUniTrans, True, 0, "tractor")
        AutoCompletar(txtDise�o, "SELECT DISTINCT Dise�o AS dise�o FROM dbo.CatLLantasProducto", "Dise�o", "Dise�o")
        AutoCompletar(txtProfundidad, "SELECT DISTINCT Profundidad AS Profundidad FROM dbo.CatLLantasProducto", "Profundidad", "Profundidad")
        AutoCompletar(txtMedida, "SELECT DISTINCT Medida AS Medida FROM dbo.CatLLantasProducto", "Medida", "Medida")

    End Sub
    Private Sub frmCatFamilias_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
        If Inicio.CONSTR = "" Then
            Inicio.CONSTR = _con
        End If
        ActivaCampos(True, TipoOpcActivaCampos.tOpcDESHABTODOS)
        'ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)
        GeneraAutoacompletar()
        LlenaCombos()
        ActualizaGrid("")
        GridTodos.Columns(0).Width = GridTodos.Width - 20
        'GridTodos.Columns(1).Visible = False
        txtMenuBusca.Focus()

    End Sub
    Private Sub GridTodos_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles GridTodos.CellEnter
        CargaForm(GridTodos.Rows(GridTodos.CurrentRow.Index).Cells(0).Value, UCase(TablaBd))
    End Sub
    Private Sub GridTodos_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles GridTodos.KeyPress
        If Asc(e.KeyChar) = 8 Then
            txtMenuBusca.Focus()
            e.Handled = True
        End If
    End Sub
    Private Sub CargaForm(ByVal idClave As String, ByVal NomTabla As String)
        Try
            Select Case UCase(NomTabla)
                Case UCase(TablaBd)
                    Llanta = New LlantasClass(idClave)
                    With Llanta
                        txtidLlanta.Text = .idLlanta
                        If .Existe Then

                            txtidEmpresa.Text = .idEmpresa
                            CargaForm(.idEmpresa, "Empresa")
                            dtpFecha.Value = .Fecha

                            llantaProd = New LLantasProductoClass(Llanta.idProductoLlanta)
                            If llantaProd.Existe Then
                                txtidMarca.Text = llantaProd.idMarca
                                CargaForm(llantaProd.idMarca, "Marca")
                                txtDise�o.Text = llantaProd.idDisenio
                                txtMedida.Text = llantaProd.Medida
                                txtProfundidad.Text = llantaProd.Profundidad

                            End If



                            txtidTipoLLanta.Text = .idTipoLLanta
                            CargaForm(.idTipoLLanta, "CatTipoLlanta")

                            txtFactura.Text = .Factura
                            txtCosto.Text = .Costo
                            txtRFDI.Text = .RFDI
                            cmbEstatus.SelectedValue = .Estatus

                            vUnidadOrig = .idUnidadTrans
                            txtUniTrans.Text = .idUnidadTrans
                            txtPosicion.Text = .Posicion
                            vPosUniOrig = .Posicion
                            CargaForm(.idUnidadTrans, "CatUnidadTrans")


                            'txtCIDALMACEN_COM.Text = .CIDALMACEN_COM
                            If OpcionForma = TipoOpcionForma.tOpcInsertar Then
                                Status("La Llanta Con id: " & .idLlanta &
                                       " Ya existe!!!, Favor de modificarlo para poder dar de alta a otra Llanta", Me, Err:=True)
                                MessageBox.Show("La Llanta con id: " & .idLlanta &
                                                " YA EXISTE!!!, Favor de Cambiarlo y volver a intentarlo",
                                                "La Llanta con id: " & .idLlanta & " YA EXISTE!!!", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            End If
                        Else 'No existe 
                            If OpcionForma = TipoOpcionForma.tOpcInsertar Then

                                ActivaCampos(True, TipoOpcActivaCampos.tOpcINICIALIZA)
                                ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)
                                btnMnuOk.Enabled = True
                                txtidTipoLLanta.Focus()
                                Status("Listo para Guardar nueva Unidad con id: " & .idLlanta, Me)
                            End If
                        End If
                    End With
                'Case UCase(TablaBd2)
                '    'aqui()
                '    TipoUniTrans = New TipoUniTransClass(Val(idClave))
                '    With TipoUniTrans
                '        txtidTipoLLanta.Text = .idTipoUniTras
                '        If .Existe Then
                '            txtDescripTLLA.Text = .nomTipoUniTras
                '            txtDise�o.Focus()
                '            'txtDescripcionUni.SelectAll()
                '        Else
                '            MsgBox("El Tipo de Unidad con id: " & txtidTipoLLanta.Text & " No Existe")
                '            txtidTipoLLanta.Focus()
                '            txtidTipoLLanta.SelectAll()
                '        End If
                '    End With
                Case UCase("CatEmpresas")
                    'OK
                    Empresa = New EmpresaClass(Val(idClave))
                    If Empresa.Existe Then
                        txtRazonSocial.Text = Empresa.RazonSocial
                        Prefijo = Empresa.PrefLla
                        'ConsecutivoLlantas
                        Dim ultLlantas As New LlantasClass("0")
                        UltimoConsecutivo = ultLlantas.ConsecutivoLlantas(Val(idClave), dtpFecha.Value.Year, dtpFecha.Value.Month)

                        AutoCompletarUnidadTransp(txtUniTrans, True, Val(idClave), "")
                    End If
                Case UCase("Marca")
                    Marca = New MarcaClass(Val(idClave))
                    If Marca.Existe Then
                        txtNombreMarca.Text = Marca.NOMBREMARCA
                    End If
                Case UCase("CatTipoLlanta")
                    TipoLLanta = New TipoLlantaClass(Val(idClave))
                    If TipoLLanta.Existe Then
                        txtDescripTLLA.Text = TipoLLanta.Descripcion
                    End If
                    '
                Case UCase("CatUnidadTrans")
                    UniTrans = New UnidadesTranspClass(idClave)
                    If UniTrans.Existe Then
                        TipUni = New TipoUniTransClass(UniTrans.idTipoUnidad)
                        If TipUni.Existe Then
                            txtTipoUni.Text = TipUni.Clasificacion
                            ClasLlan = New ClasLlanClass(TipUni.idClasLlan)
                            If ClasLlan.Existe Then
                                DibujaEsquema(TipUni.idClasLlan, TipUni.NoLlantas)
                                If txtPosicion.Text <> "" Then
                                    SeleccionaPosicion(Val(txtPosicion.Text))
                                End If
                            End If
                        End If
                    End If
            End Select
        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try

    End Sub
    Private Function Validar() As Boolean
        Validar = True
        If Not Inicio.ValidandoCampo(Trim(txtidLlanta.Text), TipoDato.TdCadena) Then
            If Not bEsIdentidad Then
                MsgBox(MENSAJE_CLAVE, vbInformation, "Aviso" & Me.Text)
                If txtidLlanta.Enabled Then
                    txtidLlanta.Focus()
                    txtidLlanta.SelectAll()
                End If
                Validar = False
            End If
        ElseIf Not Inicio.ValidandoCampo(Trim(txtDise�o.Text), TipoDato.TdCadena) Then
            MsgBox("La Descripcion de la unidad No Puede Ser Vacio", vbInformation, "Aviso" & Me.Text)
            If txtDise�o.Enabled Then
                txtDise�o.Focus()
                txtDise�o.SelectAll()
            End If
            Validar = False
        ElseIf Not Inicio.ValidandoCampo(Trim(txtidTipoLLanta.Text), TipoDato.TdNumerico) Then
            MsgBox("El Tipo de Unidad no Puede ser Vacio", vbInformation, "Aviso" & Me.Text)
            If txtidTipoLLanta.Enabled Then
                txtidTipoLLanta.Focus()
                txtidTipoLLanta.SelectAll()
            End If
            Validar = False
        End If
    End Function

    'Private Sub txtidAlmacen_EnabledChanged(sender As Object, e As EventArgs) Handles txtidAlmacen.EnabledChanged
    '    cmdBuscaClave.Enabled = txtidAlmacen.Enabled
    'End Sub
    'CONTROLES
    Private Sub txtidUnidadTrans_KeyDown(sender As Object, e As KeyEventArgs)
        If e.KeyCode = Keys.F3 Then
            cmdBuscaClave_Click(sender, e)
        End If

    End Sub
    Private Sub txtidAlmacen_KeyPress(sender As Object, e As KeyPressEventArgs)
        If Asc(e.KeyChar) = 13 Then
            If txtidLlanta.Text.Trim <> "" Then
                CargaForm(txtidLlanta.Text, UCase(TablaBd))
            End If
            e.Handled = True
        End If
    End Sub

    Private Sub cmdBuscaClave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBuscaClave.Click
        Dim idEst As String = DespliegaGrid(UCase(TablaBd), Inicio.CONSTR, CampoLLave)
        If idEst <> "" Then CargaForm(idEst, UCase(TablaBd))
    End Sub

    Private Sub txtNomServicio_MouseDown(sender As Object, e As MouseEventArgs)
        txtDise�o.Focus()
        txtDise�o.SelectAll()
    End Sub

    'Private Sub txtidSucursal_EnabledChanged(sender As Object, e As EventArgs) Handles txtidSucursal.EnabledChanged
    '    cmdBuscaClaveTPU.Enabled = txtidTipoUnidad.Enabled
    'End Sub

    Private Sub txtidTipoUnidad_KeyDown(sender As Object, e As KeyEventArgs)
        If e.KeyCode = Keys.F3 Then
            cmdBuscaClaveTPU_Click(sender, e)
        End If
    End Sub




    Private Sub txtidTipoUnidad_MouseDown(sender As Object, e As MouseEventArgs)
        txtidTipoLLanta.Focus()
        txtidTipoLLanta.SelectAll()
    End Sub

    Private Sub cmdBuscaClaveTPU_Click(sender As Object, e As EventArgs)
        'txtidTipoLLanta.Text = DespliegaGrid(UCase(TablaBd2), Inicio.CONSTR, CampoLLave)
        'txtidTipoUnidad_Leave(sender, e)
    End Sub

    Private Sub cmdCancelar_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        With TryCast(sender, Button)
            If .Enabled Then
                Me.CancelButton = sender
            End If
        End With
    End Sub
    Private Sub cmdTerminar_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        With TryCast(sender, Button)
            If .Enabled Then
                Me.CancelButton = sender
            End If
        End With
    End Sub

    Private Sub btnMnuOk_Click(sender As Object, e As EventArgs) Handles btnMnuOk.Click
        Dim CadCam As String = ""
        Try
            Salida = True
            If Not Validar() Then Exit Sub
            If GridTodos.RowCount = 0 Then
                'Servicio = New ServicioClass(0)
                Llanta = New LlantasClass("")
            End If
            With Llanta
                If OpcionForma = TipoOpcionForma.tOpcModificar Then

                    'CadCam = .GetCambios(txtDise�o.Text, txtidTipoLLanta.Text, txtidMarca.Text, Val(txtidEmpresa.Text), Val(txtidSucursal.Text), cmbEstatus.SelectedValue, Val(txtIdAlmacen.Text))
                    CadCam = .GetCambios(txtidEmpresa.Text, txtidMarca.Text, txtDise�o.Text, txtMedida.Text, txtProfundidad.Text, txtidTipoLLanta.Text,
                                         txtFactura.Text, dtpFecha.Value, txtCosto.Text, txtRFDI.Text, 1, "", dtpFecha.Value.Year, dtpFecha.Value.Month,
                                         cmbEstatus.SelectedValue, txtObserva.Text, dtpFecha.Value)
                    If CadCam <> "" Then
                        'If MessageBox.Show("Esta completamente Seguro que Desea guardar los Cambios Realizados al Estado Con id: " & .IDMARCA & vbNewLine &
                        '                CadCam, "Confirma que desea guardar los Cambios Realizados???", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then Exit Sub
                        Status("Guardando Modificaciones de la Llanta Con id: " & .idLlanta, Me, 1, 2)


                        .Guardar(bEsIdentidad, txtidEmpresa.Text, txtidProductoLlanta.Text, txtidTipoLLanta.Text,
                                         txtFactura.Text, FormatFecHora(dtpFecha.Value, True), txtCosto.Text, txtRFDI.Text, Val(txtPosicion.Text), txtUniTrans.Text, dtpFecha.Value.Year, dtpFecha.Value.Month,
                                         cmbEstatus.SelectedValue, txtObserva.Text, FormatFecHora(dtpFecha.Value, True))

                        If vUnidadOrig <> txtUniTrans.Text Then
                            .GuardaMov("MODI", FormatFecHora(Now, True), _Usuario, txtidLlanta.Text, vUnidadOrig, vPosUniOrig, txtUniTrans.Text, txtPosicion.Text, txtObserva.Text)
                        End If



                        Status("Llanta con Id: " & .idLlanta & " Modificado Con Exito!!!", Me, 2, 2)
                        MessageBox.Show("Llanta con Id: " & .idLlanta & " Modificada con exito!!!",
                                        "Accion terminada con exito!!!", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
                        ActualizaGrid(txtMenuBusca.Text)
                    Else
                        Status("No se detectaron cambios para realizar!!!", Me, Err:=True)
                    End If
                ElseIf OpcionForma = TipoOpcionForma.tOpcInsertar Then
                    .Existe = False
                    '.DescripcionUni = txtDise�o.Text
                    If Not bEsIdentidad Then
                        .idLlanta = txtidLlanta.Text
                    End If
                    Status("Guardando Nueva Llanta: " & .idLlanta & ". . .", Me, 1, 2)
                    '.Guardar(txtDise�o.Text, txtidTipoLLanta.Text, txtidMarca.Text, Val(txtidEmpresa.Text), Val(txtidSucursal.Text), cmbEstatus.SelectedValue, Val(txtIdAlmacen.Text))
                    .Guardar(bEsIdentidad, txtidEmpresa.Text, txtidProductoLlanta.Text, txtidTipoLLanta.Text,
                                         txtFactura.Text, FormatFecHora(dtpFecha.Value, True), txtCosto.Text, txtRFDI.Text, Val(txtPosicion.Text), txtUniTrans.Text, dtpFecha.Value.Year, dtpFecha.Value.Month,
                                         cmbEstatus.SelectedValue, txtObserva.Text, FormatFecHora(dtpFecha.Value, True))

                    .GuardaMov("ALTA", FormatFecHora(Now, True), _Usuario, txtidLlanta.Text, txtUniTrans.Text, txtPosicion.Text, txtUniTrans.Text, txtPosicion.Text, txtObserva.Text)

                    Status("Llanta: " & .idLlanta & " Guardado con Exito!!!", Me, 2, 2)
                    MessageBox.Show("Nueva Llanta: " & .idLlanta & " Guardado con exito!!!",
                                    "Accion terminada con exito!!!", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
                    ActualizaGrid(txtMenuBusca.Text)

                End If
            End With
            Salida = False
        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try
    End Sub


    Private Sub btnMnuCancelar_Click(sender As Object, e As EventArgs) Handles btnMnuCancelar.Click
        If txtidLlanta.Text <> "" Then
            CargaForm(txtidLlanta.Text, UCase(TablaBd))
        End If
        OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
    End Sub

    Private Sub btnMnuSalir_Click(sender As Object, e As EventArgs) Handles btnMnuSalir.Click
        Me.Close()
    End Sub

    Private Sub txtMenuBusca_TextChanged(sender As Object, e As EventArgs) Handles txtMenuBusca.TextChanged
        ActualizaGrid(txtMenuBusca.Text)
    End Sub




    Private Sub txtDuracionHoras_KeyPress(sender As Object, e As KeyPressEventArgs)
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            txtidEmpresa.Focus()
            txtidEmpresa.SelectAll()
        End If
    End Sub



    Private Sub GrupoCaptura_Enter(sender As Object, e As EventArgs)

    End Sub

    Private Function LlenaCombos() As Boolean
        Dim StrSql As String = ""
        LlenaCombos = True
        Try
            DsCombo.Clear()
            StrSql = "select 'ACT' as IdEstatus, 'ACTIVO' as Estatus " &
            "union " &
            "select 'BAJA' as IdEstatus, 'BAJA' as Estatus"

            DsCombo = Inicio.LlenaCombos("IdEstatus", "Estatus", "CatEstatus",
                               TipoConexion.TcSQL, Inicio.CONSTR, Process.GetCurrentProcess.ProcessName,
                               KEY_CHECAERROR, KEY_ESTATUS, , StrSql)
            'DsCombo = BD.ExecuteReturn(StrSql)
            If Not DsCombo Is Nothing Then
                If DsCombo.Tables("CatEstatus").DefaultView.Count > 0 Then
                    cmbEstatus.DataSource = DsCombo.Tables("CatEstatus")
                    cmbEstatus.ValueMember = "IdEstatus"
                    cmbEstatus.DisplayMember = "Estatus"
                End If
            Else
                MsgBox("No se han dado de alta a Estatus ", vbInformation, "Aviso" & Me.Text)
                LlenaCombos = False
                Exit Function
            End If
        Catch ex As Exception

        End Try
    End Function


    Private Sub cmdBuscaClaveMar_Click(sender As Object, e As EventArgs) Handles cmdBuscaClaveMar.Click
        txtidMarca.Text = DespliegaGrid("CatMarcas", Inicio.CONSTR, "idMarca")
        txtidMarca_Leave(sender, e)

    End Sub

    'Private Sub txtidMarca_Leave(sender As Object, e As EventArgs)
    '    If Salida Then Exit Sub
    '    If txtidMarca.Text.Length = 0 Then
    '        txtidMarca.Focus()
    '        txtidMarca.SelectAll()
    '    Else
    '        If txtidMarca.Enabled Then
    '            CargaForm(txtidMarca.Text, "Marca")
    '            'txtidSucursal.Focus()
    '            'txtidSucursal.SelectAll()

    '        End If
    '    End If
    'End Sub

    'Private Sub txtidMarca_KeyDown(sender As Object, e As KeyEventArgs)
    '    If e.KeyCode = Keys.F3 Then
    '        cmdBuscaClaveMar_Click(sender, e)
    '    End If
    'End Sub

    Private Sub cmdBuscaClaveSuc_Click(sender As Object, e As EventArgs)
        'txtidSucursal.Text = DespliegaGrid("CatSucursal", Inicio.CONSTR, "id_sucursal")
        txtidSucursal_Leave(sender, e)
    End Sub

    Private Sub txtidSucursal_Leave(sender As Object, e As EventArgs)
        If Salida Then Exit Sub
        'If txtidSucursal.Text.Length = 0 Then
        '    txtidSucursal.Focus()
        '    txtidSucursal.SelectAll()
        'Else
        '    If txtidSucursal.Enabled Then
        '        CargaForm(txtidSucursal.Text, "Sucursal")
        '        txtIdAlmacen.Focus()
        '        txtIdAlmacen.SelectAll()

        '    End If
        'End If
    End Sub

    'Private Sub txtidSucursal_KeyDown(sender As Object, e As KeyEventArgs) Handles txtidSucursal.KeyDown, txtidTipoUnidad.KeyDown
    '    If e.KeyCode = Keys.F3 Then
    '        cmdBuscaClaveSuc_Click(sender, e)
    '    End If
    'End Sub


    Private Sub txtIdAlmacen_Leave(sender As Object, e As EventArgs)
        'If txtIdAlmacen.Enabled Then
        '    CargaForm(txtIdAlmacen.Text, "Almacen")
        '    cmbEstatus.Focus()


        'End If
    End Sub



    Private Sub txtidUnidadTrans_KeyPress(sender As Object, e As KeyPressEventArgs)
        If Asc(e.KeyChar) = 13 Then
            If txtidLlanta.Text.Trim <> "" Then
                CargaForm(txtidLlanta.Text, UCase(TablaBd))
            End If
            e.Handled = True
        End If
    End Sub

    Private Sub txtidSucursal_KeyDown(sender As Object, e As KeyEventArgs)
        If e.KeyCode = Keys.F3 Then
            cmdBuscaClaveSuc_Click(sender, e)
        End If
    End Sub

    Private Sub txtidTipoUnidad_Leave(sender As Object, e As EventArgs)
        If Salida Then Exit Sub
        If txtidTipoLLanta.Text.Length = 0 Then
            txtidTipoLLanta.Focus()
            txtidTipoLLanta.SelectAll()
        Else
            If txtidTipoLLanta.Enabled Then
                CargaForm(txtidTipoLLanta.Text, "CatTipoUniTrans")
                txtDise�o.Focus()


            End If
        End If
    End Sub


    Private Sub txtidEmpresa_EnabledChanged(sender As Object, e As EventArgs) Handles txtidEmpresa.EnabledChanged
        cmdBuscaClaveEmp.Enabled = txtidEmpresa.Enabled
    End Sub

    Private Sub txtidEmpresa_KeyDown(sender As Object, e As KeyEventArgs) Handles txtidEmpresa.KeyDown
        If e.KeyCode = Keys.F3 Then
            cmdBuscaClaveEmp_Click(sender, e)
        End If
    End Sub

    Private Sub txtidEmpresa_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtidEmpresa.KeyPress
        If txtidEmpresa.Text.Length = 0 Then
            txtidEmpresa.Focus()
        Else
            If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
                Salida = True
                CargaForm(txtidEmpresa.Text, "CatEmpresas")
                Salida = False
                dtpFecha.Focus()
            End If
        End If
    End Sub

    Private Sub txtidEmpresa_Leave(sender As Object, e As EventArgs) Handles txtidEmpresa.Leave
        If Salida Then Exit Sub
        If txtidEmpresa.Text.Length = 0 Then
            txtidEmpresa.Focus()
            txtidEmpresa.SelectAll()
        Else
            If txtidEmpresa.Enabled Then
                CargaForm(txtidEmpresa.Text, "CatEmpresas")
                dtpFecha.Focus()
            End If
        End If
    End Sub

    Private Sub txtidEmpresa_MouseDown(sender As Object, e As MouseEventArgs) Handles txtidEmpresa.MouseDown
        txtidEmpresa.Focus()
        txtidEmpresa.SelectAll()
    End Sub

    Private Sub cmdBuscaClaveEmp_Click(sender As Object, e As EventArgs) Handles cmdBuscaClaveEmp.Click
        txtidEmpresa.Text = DespliegaGrid("CatEmpresas", Inicio.CONSTR, "idEmpresa")
        txtidEmpresa_Leave(sender, e)
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnGenerar.Click
        txtLlantaP1.Text = Prefijo
        txtLlantaP2.Text = dtpFecha.Value.Year.ToString.Substring(2)
        txtLlantaP3.Text = IIf(dtpFecha.Value.Month < 10, "0" & dtpFecha.Value.Month, dtpFecha.Value.Month)
        txtLlantaP4.Text = UltimoConsecutivo
        txtidLlanta.Text = Trim(Prefijo &
        dtpFecha.Value.Year.ToString.Substring(2) &
        IIf(dtpFecha.Value.Month < 10, "0" & dtpFecha.Value.Month, dtpFecha.Value.Month) &
        UltimoConsecutivo)
    End Sub

    Private Sub txtidLlanta_TextChanged(sender As Object, e As EventArgs) Handles txtidLlanta.TextChanged

    End Sub

    Private Sub txtidLlanta_EnabledChanged(sender As Object, e As EventArgs) Handles txtidLlanta.EnabledChanged
        cmdBuscaClave.Enabled = txtidLlanta.Enabled
    End Sub

    Private Sub txtidLlanta_KeyDown(sender As Object, e As KeyEventArgs) Handles txtidLlanta.KeyDown
        If e.KeyCode = Keys.F3 Then
            cmdBuscaClave_Click(sender, e)
        End If
    End Sub

    Private Sub txtidLlanta_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtidLlanta.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If txtidLlanta.Text.Trim <> "" Then
                CargaForm(txtidLlanta.Text, UCase(TablaBd))
                txtidEmpresa.Focus()
            End If
            e.Handled = True
        End If
    End Sub

    Private Sub txtidLlanta_Leave(sender As Object, e As EventArgs) Handles txtidLlanta.Leave

    End Sub

    Private Sub txtidLlanta_MouseDown(sender As Object, e As MouseEventArgs) Handles txtidLlanta.MouseDown
        txtidLlanta.Focus()
        txtidLlanta.SelectAll()
    End Sub

    Private Sub txtidMarca_EnabledChanged(sender As Object, e As EventArgs) Handles txtidMarca.EnabledChanged
        cmdBuscaClaveMar.Enabled = txtidMarca.Enabled
    End Sub

    Private Sub txtidMarca_KeyDown(sender As Object, e As KeyEventArgs) Handles txtidMarca.KeyDown
        If e.KeyCode = Keys.F3 Then
            cmdBuscaClaveMar_Click(sender, e)
        End If
    End Sub

    Private Sub txtidMarca_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtidMarca.KeyPress
        If txtidMarca.Text.Length = 0 Then
            txtidMarca.Focus()
        Else
            If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
                Salida = True
                CargaForm(txtidMarca.Text, "Marca")
                Salida = False
                txtidTipoLLanta.Focus()
            End If
        End If
    End Sub

    Private Sub txtidMarca_Leave(sender As Object, e As EventArgs) Handles txtidMarca.Leave
        If Salida Then Exit Sub
        If txtidMarca.Text.Length = 0 Then
            txtidMarca.Focus()
            txtidMarca.SelectAll()
        Else
            If txtidMarca.Enabled Then
                CargaForm(txtidMarca.Text, "Marca")
                txtidTipoLLanta.Focus()
            End If
        End If
    End Sub

    Private Sub txtidMarca_MouseDown(sender As Object, e As MouseEventArgs) Handles txtidMarca.MouseDown
        txtidMarca.Focus()
        txtidMarca.SelectAll()
    End Sub

    Private Sub cmdBuscaClaveTPLla_Click(sender As Object, e As EventArgs) Handles cmdBuscaClaveTPLla.Click
        txtidTipoLLanta.Text = DespliegaGrid("CatTipoLlanta", Inicio.CONSTR, "idTipoLLanta")
        txtidTipoLLanta_Leave(sender, e)
    End Sub

    Private Sub txtidTipoLLanta_EnabledChanged(sender As Object, e As EventArgs) Handles txtidTipoLLanta.EnabledChanged
        cmdBuscaClaveTPLla.Enabled = txtidTipoLLanta.Enabled
    End Sub

    Private Sub txtidTipoLLanta_KeyDown(sender As Object, e As KeyEventArgs) Handles txtidTipoLLanta.KeyDown
        If e.KeyCode = Keys.F3 Then
            cmdBuscaClaveTPLla_Click(sender, e)
        End If
    End Sub

    Private Sub txtidTipoLLanta_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtidTipoLLanta.KeyPress
        If txtidTipoLLanta.Text.Length = 0 Then
            txtidTipoLLanta.Focus()
        Else
            If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
                Salida = True
                CargaForm(txtidTipoLLanta.Text, "CatTipoLlanta")

                txtDise�o.Focus()
                Salida = False
            End If
        End If
    End Sub

    Private Sub txtidTipoLLanta_MouseDown(sender As Object, e As MouseEventArgs) Handles txtidTipoLLanta.MouseDown
        txtidTipoLLanta.Focus()
        txtidTipoLLanta.SelectAll()
    End Sub

    Private Sub txtidTipoLLanta_Leave(sender As Object, e As EventArgs) Handles txtidTipoLLanta.Leave
        If Salida Then Exit Sub
        If txtidTipoLLanta.Text.Length = 0 Then
            txtidTipoLLanta.Focus()
            txtidTipoLLanta.SelectAll()
        Else
            If txtidTipoLLanta.Enabled Then
                CargaForm(txtidTipoLLanta.Text, "CatTipoLlanta")
                txtDise�o.Focus()
            End If
        End If
    End Sub

    Private Sub txtDise�o_TextChanged(sender As Object, e As EventArgs) Handles txtDise�o.TextChanged

    End Sub



    Private Sub txtDise�o_MouseDown(sender As Object, e As MouseEventArgs) Handles txtDise�o.MouseDown
        txtDise�o.Focus()
        txtDise�o.SelectAll()
    End Sub

    Private Sub txtMedida_TextChanged(sender As Object, e As EventArgs) Handles txtMedida.TextChanged

    End Sub

    Private Sub txtMedida_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtMedida.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            txtProfundidad.Focus()
        End If
    End Sub

    Private Sub txtMedida_MouseDown(sender As Object, e As MouseEventArgs) Handles txtMedida.MouseDown
        txtMedida.Focus()
        txtMedida.SelectAll()
    End Sub

    Private Sub txtProfundidad_TextChanged(sender As Object, e As EventArgs) Handles txtProfundidad.TextChanged

    End Sub

    Private Sub txtProfundidad_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtProfundidad.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            txtFactura.Focus()
        End If
    End Sub

    Private Sub txtProfundidad_MouseDown(sender As Object, e As MouseEventArgs) Handles txtProfundidad.MouseDown
        txtProfundidad.Focus()
        txtProfundidad.SelectAll()
    End Sub

    Private Sub txtFactura_TextChanged(sender As Object, e As EventArgs) Handles txtFactura.TextChanged

    End Sub

    Private Sub txtFactura_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtFactura.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            txtCosto.Focus()
        End If
    End Sub

    Private Sub txtFactura_MouseDown(sender As Object, e As MouseEventArgs) Handles txtFactura.MouseDown
        txtFactura.Focus()
        txtFactura.SelectAll()
    End Sub

    Private Sub txtCosto_TextChanged(sender As Object, e As EventArgs) Handles txtCosto.TextChanged

    End Sub

    Private Sub txtCosto_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCosto.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            cmbEstatus.Focus()
        End If
    End Sub

    Private Sub txtCosto_MouseDown(sender As Object, e As MouseEventArgs) Handles txtCosto.MouseDown
        txtCosto.Focus()
        txtCosto.SelectAll()
    End Sub

    Private Sub cmbEstatus_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbEstatus.SelectedIndexChanged

    End Sub

    Private Sub cmbEstatus_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbEstatus.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            If txtRFDI.Enabled Then
                txtRFDI.Focus()
                txtRFDI.SelectAll()
            Else
                txtUniTrans.Focus()
            End If
        End If
    End Sub

    Private Sub txtUniTrans_TextChanged(sender As Object, e As EventArgs) Handles txtUniTrans.TextChanged

    End Sub

    Private Sub txtUniTrans_Leave(sender As Object, e As EventArgs) Handles txtUniTrans.Leave
        If Salida Then Exit Sub
        If txtUniTrans.Text.Length = 0 Then
            txtUniTrans.Focus()
            txtUniTrans.SelectAll()
        Else
            If txtUniTrans.Enabled Then
                CargaForm(txtUniTrans.Text, "CatUnidadTrans")
                txtPosicion.Focus()
            End If
        End If
    End Sub

    Private Sub txtUniTrans_MouseDown(sender As Object, e As MouseEventArgs) Handles txtUniTrans.MouseDown

    End Sub

    Private Sub txtUniTrans_KeyUp(sender As Object, e As KeyEventArgs) Handles txtUniTrans.KeyUp
        'Cuando tiene AutoCompletar no usa Keypress
        If e.KeyValue = Keys.Enter Then
            Salida = True
            CargaForm(txtUniTrans.Text, "CatUnidadTrans")
            Salida = False
            txtPosicion.Focus()
        End If
    End Sub


    'Private Sub txtidSucursal_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtidSucursal.KeyPress, txtidTipoUnidad.KeyPress
    '    If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
    '        txtIdAlmacen.Focus()
    '        txtIdAlmacen.SelectAll()
    '    End If

    'End Sub
    Private Sub DibujaEsquema(ByVal vidClasLlan As Integer, ByVal TotalLlantas As Integer)
        'Dim vNoEje As Integer
        'Dim vNoLlantas As Integer
        'Dim LlantxLado As Integer
        'Dim NoPosicion As Integer = 0
        'Dim Consecutivo As Integer

        'Dim FactorX As Integer = 71
        'Dim FactorY As Integer = 41

        'Dim PosXL1 As Integer = 29
        'Dim PosXL2 As Integer = 100
        'Dim PosY As Integer = 62

        'ReDim rdbPosicion(TotalLlantas - 1)

        'ClasLlan = New ClasLlanClass(0)
        'TablaEstruc = New DataTable

        'TablaEstruc = ClasLlan.TablaPosLlanta(vidClasLlan)

        'LimpiaPosiciones()

        'If Not TablaEstruc Is Nothing Then
        '    If TablaEstruc.Rows.Count > 0 Then
        '        For i = 0 To TablaEstruc.Rows.Count - 1
        '            vNoEje = TablaEstruc.DefaultView.Item(i).Item("EjeNo")
        '            vNoLlantas = TablaEstruc.DefaultView.Item(i).Item("NoLlantas")
        '            LlantxLado = vNoLlantas / 2



        '            'Posiciones Iniciales
        '            If LlantxLado = 1 Then
        '                PosXL1 = 29
        '                FactorX = 71
        '            Else
        '                PosXL1 = 5
        '                FactorX = 37
        '            End If

        '            For z = 0 To vNoLlantas - 1
        '                rdbPosicion(NoPosicion) = New RadioButton
        '                rdbPosicion(NoPosicion).Name = "pos" & NoPosicion
        '                rdbPosicion(NoPosicion).Text = NoPosicion + 1
        '                rdbPosicion(NoPosicion).AutoSize = True
        '                rdbPosicion(NoPosicion).Checked = False
        '                rdbPosicion(NoPosicion).Size = New System.Drawing.Size(31, 17)
        '                'rdbPosicion(i).Location = New Point(6, 4)
        '                rdbPosicion(NoPosicion).Location = New Point(PosXL1, PosY)

        '                AddHandler rdbPosicion(NoPosicion).Click, AddressOf RadioButtons_Click
        '                AddHandler rdbPosicion(NoPosicion).KeyDown, AddressOf ObjEnter_KeyDown
        '                'PanelControles.Controls.Add(rdbLogico1(i))
        '                GpoPosiciones.Controls.Add(rdbPosicion(NoPosicion))

        '                'PanelLogico1(i).Controls.Add(rdbLogico1(i))

        '                PosXL1 = PosXL1 + FactorX

        '                If z < vNoLlantas - 1 Then
        '                    NoPosicion = NoPosicion + 1
        '                End If


        '            Next
        '            PosY = PosY + FactorY
        '            NoPosicion = NoPosicion + 1


        '        Next

        '    End If
        'End If

        Dim vNoEje As Integer
        Dim vNoLlantas As Integer
        Dim LlantxLado As Integer
        Dim NoPosicion As Integer = 0

        'Dim FactorX As Integer = 71
        Dim FactorX As Integer = 201

        'Dim FactorY As Integer = 41
        Dim FactorY As Integer = 70

        Dim PosX As Integer = 29
        'Dim PosFacLinX As Integer = 201
        Dim PosFacLinX As Integer = 195

        'Dim PosY As Integer = 62
        Dim PosY As Integer = 37

        ReDim rdbPosicion(TotalLlantas - 1)

        LimpiaPosiciones()

        ClasLlan = New ClasLlanClass(0)
        TablaEstruc = New DataTable

        TablaEstruc = ClasLlan.TablaPosLlanta(vidClasLlan)
        If Not TablaEstruc Is Nothing Then
            If TablaEstruc.Rows.Count > 0 Then
                For i = 0 To TablaEstruc.Rows.Count - 1
                    vNoEje = TablaEstruc.DefaultView.Item(i).Item("EjeNo")
                    vNoLlantas = TablaEstruc.DefaultView.Item(i).Item("NoLlantas")
                    LlantxLado = vNoLlantas / 2
                    'Posiciones Iniciales
                    If LlantxLado = 0 Then
                        'PosX = 29
                        'FactorX = 71
                        PosX = 95
                        FactorX = 37
                    ElseIf LlantxLado = 1 Then
                        'PosX = 29
                        'FactorX = 71
                        PosX = 50
                        FactorX = 37
                    Else
                        'PosX = 5
                        'FactorX = 37
                        'PosX = 3
                        PosX = 5
                        'FactorX = 99
                        FactorX = 90
                    End If

                    For z = 0 To vNoLlantas - 1
                        rdbPosicion(NoPosicion) = New RadioButton
                        rdbPosicion(NoPosicion).Name = "pos" & NoPosicion

                        rdbPosicion(NoPosicion).Text = NoPosicion + 1
                        'rdbPosicion(NoPosicion).AutoSize = True
                        rdbPosicion(NoPosicion).Checked = False

                        'rdbPosicion(NoPosicion).Size = New System.Drawing.Size(31, 17)
                        rdbPosicion(NoPosicion).Size = New System.Drawing.Size(95, 64)

                        rdbPosicion(NoPosicion).Image = My.Resources.llanta2
                        rdbPosicion(NoPosicion).TextImageRelation = TextImageRelation.Overlay
                        rdbPosicion(NoPosicion).ImageAlign = ContentAlignment.MiddleCenter

                        'rdbPosicion(i).Location = New Point(6, 4)

                        rdbPosicion(NoPosicion).Location = New Point(PosX, PosY)

                        AddHandler rdbPosicion(NoPosicion).Click, AddressOf RadioButtons_Click
                        AddHandler rdbPosicion(NoPosicion).KeyDown, AddressOf ObjEnter_KeyDown
                        'PanelControles.Controls.Add(rdbLogico1(i))
                        GpoPosiciones.Controls.Add(rdbPosicion(NoPosicion))

                        'PanelLogico1(i).Controls.Add(rdbLogico1(i))

                        PosX = PosX + FactorX

                        If z < vNoLlantas - 1 Then
                            NoPosicion = NoPosicion + 1
                        End If

                        If LlantxLado = 1 Then
                            PosX = PosX + PosFacLinX - FactorX
                        End If

                    Next
                    PosY = PosY + FactorY
                    NoPosicion = NoPosicion + 1
                Next
            End If
        End If



    End Sub

    Private Sub RadioButtons_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        With TryCast(sender, RadioButton)
            txtPosicion.Text = .Text
            For i = 0 To rdbPosicion.Count - 1
                rdbPosicion(i).BackColor = SystemColors.Control
            Next
            .BackColor = Color.Yellow
            'MessageBox.Show("El Label Tiene Nombre: " & .Name & " y Tiene Escrito:" & .Text, "informacion del Label", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End With
    End Sub

    Private Sub ObjEnter_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        If e.KeyCode = 13 Then
            SendKeys.Send("{TAB}")
            e.Handled = True
        End If
    End Sub

    Private Sub SeleccionaPosicion(ByVal Posicion As Integer)



        If Posicion > rdbPosicion.Count Then
            MsgBox("La Estructura del tipo de unidad " & txtTipoUni.Text & " No cuenta con la posicion " & txtPosicion.Text & vbCrLf & "Favor de seleccionar la Posicion Correspondiente", MsgBoxStyle.Information, Me.Text)
            txtPosicion.Text = ""
            For i = 0 To rdbPosicion.Count - 1
                rdbPosicion(i).BackColor = SystemColors.Control
                rdbPosicion(i).Checked = False
            Next

        Else
            rdbPosicion(Posicion - 1).Checked = True
            For i = 0 To rdbPosicion.Count - 1
                rdbPosicion(i).BackColor = SystemColors.Control
            Next
            rdbPosicion(Posicion - 1).BackColor = Color.Yellow

        End If


    End Sub

    Private Sub LimpiaPosiciones()
        'For Each ct As Control In GrupoCaptura.Controls
        '    If TypeOf ct Is RadioButton Then
        '        ct.Dispose()
        '    End If
        'Next

        'Dim num_controles As Int32 = Me.TLPanel_convenio.Controls.Count - 1
        Dim num_controles As Int32 = Me.GpoPosiciones.Controls.Count - 1
        For n As Integer = num_controles To 0 Step -1
            Dim ctrl As Windows.Forms.Control = Me.GpoPosiciones.Controls(n)
            If TypeOf ctrl Is RadioButton Then

            End If
            Me.GpoPosiciones.Controls.Remove(ctrl)
            ctrl.Dispose()
        Next
        DibujaLinea()
    End Sub

    Private Sub DibujaLinea()
        'Dim canvas As New ShapeContainer
        'Dim theLine As New LineShape
        '' Set the form as the parent of the ShapeContainer.
        'canvas.Parent = Me.GpoPosiciones
        '' Set the ShapeContainer as the parent of the LineShape.
        'theLine.Parent = canvas
        '' Set the starting and ending coordinates for the line.
        'theLine.StartPoint = New System.Drawing.Point(70, 34)
        'theLine.EndPoint = New System.Drawing.Point(70, 191)

        Dim canvas As New ShapeContainer
        Dim theLine As New LineShape
        ' Set the form as the parent of the ShapeContainer.
        canvas.Parent = Me.GpoPosiciones
        ' Set the ShapeContainer as the parent of the LineShape.
        theLine.Parent = canvas
        ' Set the starting and ending coordinates for the line.
        theLine.BorderWidth = 10
        theLine.StartPoint = New System.Drawing.Point(183, 25)
        theLine.EndPoint = New System.Drawing.Point(183, 300)
    End Sub

    Private Sub txtPosicion_TextChanged(sender As Object, e As EventArgs) Handles txtPosicion.TextChanged

    End Sub

    Private Sub txtPosicion_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPosicion.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            SeleccionaPosicion(txtPosicion.Text)
        End If
    End Sub

    Private Sub dtpFecha_ValueChanged(sender As Object, e As EventArgs) Handles dtpFecha.ValueChanged

    End Sub

    Private Sub dtpFecha_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dtpFecha.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            If OpcionForma = TipoOpcionForma.tOpcInsertar Then
                btnGenerar.Focus()
            Else
                txtidMarca.Focus()
                txtidMarca.SelectAll()
            End If

        End If
    End Sub

    Private Sub txtDise�o_KeyUp(sender As Object, e As KeyEventArgs) Handles txtDise�o.KeyUp
        If e.KeyValue = Keys.Enter Then
            txtMedida.Focus()
        End If

    End Sub

    Private Sub txtMedida_KeyUp(sender As Object, e As KeyEventArgs) Handles txtMedida.KeyUp
        If e.KeyValue = Keys.Enter Then
            txtProfundidad.Focus()
        End If
    End Sub

    Private Sub txtProfundidad_KeyUp(sender As Object, e As KeyEventArgs) Handles txtProfundidad.KeyUp
        If e.KeyValue = Keys.Enter Then
            txtFactura.Focus()
        End If
    End Sub

    Private Sub txtidTipoLLanta_TextChanged(sender As Object, e As EventArgs) Handles txtidTipoLLanta.TextChanged

    End Sub

    Private Sub txtidMarca_TextChanged(sender As Object, e As EventArgs) Handles txtidMarca.TextChanged

    End Sub
End Class
