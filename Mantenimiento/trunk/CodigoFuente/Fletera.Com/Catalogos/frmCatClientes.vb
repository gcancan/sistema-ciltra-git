'Fecha: 15 / Febrero / 2016
'ROGER GALA PACHECO
'************************************************************************************************************************************
'*  Fecha Modificacion :   15 / Febrero / 2016                                                                                       
'*  
'************************************************************************************************************************************
Public Class frmCatClientes
    Inherits System.Windows.Forms.Form
    Dim OpcionForma As TipoOpcionForma
    Dim TablaBd As String = "CatClientes"
    Dim CampoLLave As String = "IdCliente"
    'Dim TablaBd2 As String = "CatColonia"
    Public WithEvents MeStatus1 As ControlStatus.MeStatus
    Friend WithEvents GridTodos As System.Windows.Forms.DataGridView
    'Private Proveedor As ProveedorClass
    Private Cliente As ClientesClass

    Friend WithEvents btnMnuOk As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuCancelar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuSalir As System.Windows.Forms.ToolStripButton
    Private _Tag As String
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Dim bEsIdentidad As Boolean = True
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtRFCProv As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtDomicilio As System.Windows.Forms.TextBox
    Friend WithEvents cmbidEmpresa As System.Windows.Forms.ComboBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Dim Salida As Boolean
    'Private con As String
    Friend WithEvents cmbcolonia As ComboBox
    Friend WithEvents cmbpais As ComboBox
    Friend WithEvents cmbestado As ComboBox
    Friend WithEvents cmbmunicipio As ComboBox
    Friend WithEvents cmbClave_impuesto As ComboBox
    Friend WithEvents cmbEstatus As ComboBox
    Friend WithEvents Label11 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents txtpenalizacion As TextBox
    Friend WithEvents cmbclave_Fraccion As ComboBox
    Friend WithEvents cmbidImpuestoFlete As ComboBox
    Friend WithEvents Label14 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents Label15 As Label
    Friend WithEvents txtcp As TextBox
    Friend WithEvents txttelefono As TextBox
    Private v As Boolean
    '15/Mar/2017
    Dim DsCombo As New DataSet
    Private Empresa As EmpresaClass
    Private Colonia As ColoniaClass
    Private Impuesto As ImpuestosClass
    Private Fraccion As FraccionesClass
    Private Pais As PaisClass
    Private Ciudad As CiudadClass
    Private Estado As EstadosClass

    Private _con As String
    Private _Usuario As String
    Private _IdUsuario As Integer
    Friend WithEvents Label17 As Label
    Friend WithEvents chkCambioPrecio As CheckBox
    Friend WithEvents Label18 As Label
    Friend WithEvents chkGeneraDoc As CheckBox
    Private _cveEmpresa As Integer

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents GrupoCaptura As System.Windows.Forms.GroupBox
    Friend WithEvents ToolStripMenu As System.Windows.Forms.ToolStrip
    Friend WithEvents txtMenuBusca As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents btnMnuAltas As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuModificar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuEliminar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuReporte As System.Windows.Forms.ToolStripButton
    Friend WithEvents label1 As System.Windows.Forms.Label
    Friend WithEvents label2 As System.Windows.Forms.Label
    Friend WithEvents cmdBuscaClave As System.Windows.Forms.Button
    Friend WithEvents txtIdCliente As System.Windows.Forms.TextBox
    Friend WithEvents txtNombre As System.Windows.Forms.TextBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCatClientes))
        Me.txtIdCliente = New System.Windows.Forms.TextBox()
        Me.label1 = New System.Windows.Forms.Label()
        Me.txtNombre = New System.Windows.Forms.TextBox()
        Me.label2 = New System.Windows.Forms.Label()
        Me.GrupoCaptura = New System.Windows.Forms.GroupBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.chkCambioPrecio = New System.Windows.Forms.CheckBox()
        Me.cmbclave_Fraccion = New System.Windows.Forms.ComboBox()
        Me.cmbidImpuestoFlete = New System.Windows.Forms.ComboBox()
        Me.cmbClave_impuesto = New System.Windows.Forms.ComboBox()
        Me.cmbEstatus = New System.Windows.Forms.ComboBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.cmbpais = New System.Windows.Forms.ComboBox()
        Me.cmbestado = New System.Windows.Forms.ComboBox()
        Me.cmbmunicipio = New System.Windows.Forms.ComboBox()
        Me.cmbcolonia = New System.Windows.Forms.ComboBox()
        Me.cmbidEmpresa = New System.Windows.Forms.ComboBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtcp = New System.Windows.Forms.TextBox()
        Me.txttelefono = New System.Windows.Forms.TextBox()
        Me.txtpenalizacion = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtDomicilio = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtRFCProv = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cmdBuscaClave = New System.Windows.Forms.Button()
        Me.ToolStripMenu = New System.Windows.Forms.ToolStrip()
        Me.txtMenuBusca = New System.Windows.Forms.ToolStripTextBox()
        Me.btnMnuAltas = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuModificar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuEliminar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuReporte = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuOk = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuCancelar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuSalir = New System.Windows.Forms.ToolStripButton()
        Me.MeStatus1 = New ControlStatus.MeStatus()
        Me.GridTodos = New System.Windows.Forms.DataGridView()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.chkGeneraDoc = New System.Windows.Forms.CheckBox()
        Me.GrupoCaptura.SuspendLayout()
        Me.ToolStripMenu.SuspendLayout()
        CType(Me.GridTodos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtIdCliente
        '
        Me.txtIdCliente.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtIdCliente.Location = New System.Drawing.Point(109, 27)
        Me.txtIdCliente.Name = "txtIdCliente"
        Me.txtIdCliente.Size = New System.Drawing.Size(69, 20)
        Me.txtIdCliente.TabIndex = 2
        '
        'label1
        '
        Me.label1.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.label1.Location = New System.Drawing.Point(6, 27)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(94, 20)
        Me.label1.TabIndex = 14
        Me.label1.Text = "id Cliente:"
        '
        'txtNombre
        '
        Me.txtNombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNombre.Location = New System.Drawing.Point(109, 84)
        Me.txtNombre.MaxLength = 255
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.Size = New System.Drawing.Size(540, 20)
        Me.txtNombre.TabIndex = 5
        '
        'label2
        '
        Me.label2.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.label2.Location = New System.Drawing.Point(9, 84)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(91, 20)
        Me.label2.TabIndex = 16
        Me.label2.Text = "Raz�n Social:"
        '
        'GrupoCaptura
        '
        Me.GrupoCaptura.Controls.Add(Me.Label18)
        Me.GrupoCaptura.Controls.Add(Me.chkGeneraDoc)
        Me.GrupoCaptura.Controls.Add(Me.Label17)
        Me.GrupoCaptura.Controls.Add(Me.chkCambioPrecio)
        Me.GrupoCaptura.Controls.Add(Me.cmbclave_Fraccion)
        Me.GrupoCaptura.Controls.Add(Me.cmbidImpuestoFlete)
        Me.GrupoCaptura.Controls.Add(Me.cmbClave_impuesto)
        Me.GrupoCaptura.Controls.Add(Me.cmbEstatus)
        Me.GrupoCaptura.Controls.Add(Me.Label11)
        Me.GrupoCaptura.Controls.Add(Me.Label10)
        Me.GrupoCaptura.Controls.Add(Me.cmbpais)
        Me.GrupoCaptura.Controls.Add(Me.cmbestado)
        Me.GrupoCaptura.Controls.Add(Me.cmbmunicipio)
        Me.GrupoCaptura.Controls.Add(Me.cmbcolonia)
        Me.GrupoCaptura.Controls.Add(Me.cmbidEmpresa)
        Me.GrupoCaptura.Controls.Add(Me.Label14)
        Me.GrupoCaptura.Controls.Add(Me.Label9)
        Me.GrupoCaptura.Controls.Add(Me.Label13)
        Me.GrupoCaptura.Controls.Add(Me.Label16)
        Me.GrupoCaptura.Controls.Add(Me.Label15)
        Me.GrupoCaptura.Controls.Add(Me.Label12)
        Me.GrupoCaptura.Controls.Add(Me.Label8)
        Me.GrupoCaptura.Controls.Add(Me.txtcp)
        Me.GrupoCaptura.Controls.Add(Me.txttelefono)
        Me.GrupoCaptura.Controls.Add(Me.txtpenalizacion)
        Me.GrupoCaptura.Controls.Add(Me.Label7)
        Me.GrupoCaptura.Controls.Add(Me.txtDomicilio)
        Me.GrupoCaptura.Controls.Add(Me.Label6)
        Me.GrupoCaptura.Controls.Add(Me.Label4)
        Me.GrupoCaptura.Controls.Add(Me.Label5)
        Me.GrupoCaptura.Controls.Add(Me.txtRFCProv)
        Me.GrupoCaptura.Controls.Add(Me.Label3)
        Me.GrupoCaptura.Controls.Add(Me.label1)
        Me.GrupoCaptura.Controls.Add(Me.label2)
        Me.GrupoCaptura.Controls.Add(Me.cmdBuscaClave)
        Me.GrupoCaptura.Controls.Add(Me.txtIdCliente)
        Me.GrupoCaptura.Controls.Add(Me.txtNombre)
        Me.GrupoCaptura.Location = New System.Drawing.Point(168, 45)
        Me.GrupoCaptura.Name = "GrupoCaptura"
        Me.GrupoCaptura.Size = New System.Drawing.Size(856, 314)
        Me.GrupoCaptura.TabIndex = 3
        Me.GrupoCaptura.TabStop = False
        '
        'Label17
        '
        Me.Label17.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label17.Location = New System.Drawing.Point(6, 273)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(94, 20)
        Me.Label17.TabIndex = 42
        Me.Label17.Text = "Cambia Precio:"
        '
        'chkCambioPrecio
        '
        Me.chkCambioPrecio.AutoSize = True
        Me.chkCambioPrecio.Location = New System.Drawing.Point(109, 273)
        Me.chkCambioPrecio.Name = "chkCambioPrecio"
        Me.chkCambioPrecio.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.chkCambioPrecio.Size = New System.Drawing.Size(15, 14)
        Me.chkCambioPrecio.TabIndex = 41
        Me.chkCambioPrecio.UseVisualStyleBackColor = True
        '
        'cmbclave_Fraccion
        '
        Me.cmbclave_Fraccion.AutoCompleteCustomSource.AddRange(New String() {"Regularizaci�n", "Vivienda"})
        Me.cmbclave_Fraccion.FormattingEnabled = True
        Me.cmbclave_Fraccion.Items.AddRange(New Object() {"MORAL", "FISICA"})
        Me.cmbclave_Fraccion.Location = New System.Drawing.Point(616, 110)
        Me.cmbclave_Fraccion.Name = "cmbclave_Fraccion"
        Me.cmbclave_Fraccion.Size = New System.Drawing.Size(163, 21)
        Me.cmbclave_Fraccion.TabIndex = 8
        '
        'cmbidImpuestoFlete
        '
        Me.cmbidImpuestoFlete.AutoCompleteCustomSource.AddRange(New String() {"Regularizaci�n", "Vivienda"})
        Me.cmbidImpuestoFlete.FormattingEnabled = True
        Me.cmbidImpuestoFlete.Items.AddRange(New Object() {"MORAL", "FISICA"})
        Me.cmbidImpuestoFlete.Location = New System.Drawing.Point(361, 110)
        Me.cmbidImpuestoFlete.Name = "cmbidImpuestoFlete"
        Me.cmbidImpuestoFlete.Size = New System.Drawing.Size(163, 21)
        Me.cmbidImpuestoFlete.TabIndex = 7
        '
        'cmbClave_impuesto
        '
        Me.cmbClave_impuesto.AutoCompleteCustomSource.AddRange(New String() {"Regularizaci�n", "Vivienda"})
        Me.cmbClave_impuesto.FormattingEnabled = True
        Me.cmbClave_impuesto.Items.AddRange(New Object() {"MORAL", "FISICA"})
        Me.cmbClave_impuesto.Location = New System.Drawing.Point(109, 110)
        Me.cmbClave_impuesto.Name = "cmbClave_impuesto"
        Me.cmbClave_impuesto.Size = New System.Drawing.Size(163, 21)
        Me.cmbClave_impuesto.TabIndex = 6
        '
        'cmbEstatus
        '
        Me.cmbEstatus.AutoCompleteCustomSource.AddRange(New String() {"Regularizaci�n", "Vivienda"})
        Me.cmbEstatus.FormattingEnabled = True
        Me.cmbEstatus.Items.AddRange(New Object() {"MORAL", "FISICA"})
        Me.cmbEstatus.Location = New System.Drawing.Point(340, 137)
        Me.cmbEstatus.Name = "cmbEstatus"
        Me.cmbEstatus.Size = New System.Drawing.Size(184, 21)
        Me.cmbEstatus.TabIndex = 10
        '
        'Label11
        '
        Me.Label11.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label11.Location = New System.Drawing.Point(243, 138)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(91, 20)
        Me.Label11.TabIndex = 40
        Me.Label11.Text = "Estatus:"
        '
        'Label10
        '
        Me.Label10.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label10.Location = New System.Drawing.Point(566, 191)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(94, 20)
        Me.Label10.TabIndex = 38
        Me.Label10.Text = "Ciudad:"
        '
        'cmbpais
        '
        Me.cmbpais.AutoCompleteCustomSource.AddRange(New String() {"Regularizaci�n", "Vivienda"})
        Me.cmbpais.FormattingEnabled = True
        Me.cmbpais.Items.AddRange(New Object() {"MORAL", "FISICA"})
        Me.cmbpais.Location = New System.Drawing.Point(405, 217)
        Me.cmbpais.Name = "cmbpais"
        Me.cmbpais.Size = New System.Drawing.Size(184, 21)
        Me.cmbpais.TabIndex = 16
        '
        'cmbestado
        '
        Me.cmbestado.AutoCompleteCustomSource.AddRange(New String() {"Regularizaci�n", "Vivienda"})
        Me.cmbestado.FormattingEnabled = True
        Me.cmbestado.Items.AddRange(New Object() {"MORAL", "FISICA"})
        Me.cmbestado.Location = New System.Drawing.Point(109, 217)
        Me.cmbestado.Name = "cmbestado"
        Me.cmbestado.Size = New System.Drawing.Size(184, 21)
        Me.cmbestado.TabIndex = 15
        '
        'cmbmunicipio
        '
        Me.cmbmunicipio.AutoCompleteCustomSource.AddRange(New String() {"Regularizaci�n", "Vivienda"})
        Me.cmbmunicipio.FormattingEnabled = True
        Me.cmbmunicipio.Items.AddRange(New Object() {"MORAL", "FISICA"})
        Me.cmbmunicipio.Location = New System.Drawing.Point(666, 190)
        Me.cmbmunicipio.Name = "cmbmunicipio"
        Me.cmbmunicipio.Size = New System.Drawing.Size(184, 21)
        Me.cmbmunicipio.TabIndex = 14
        '
        'cmbcolonia
        '
        Me.cmbcolonia.AutoCompleteCustomSource.AddRange(New String() {"Regularizaci�n", "Vivienda"})
        Me.cmbcolonia.FormattingEnabled = True
        Me.cmbcolonia.Items.AddRange(New Object() {"MORAL", "FISICA"})
        Me.cmbcolonia.Location = New System.Drawing.Point(109, 190)
        Me.cmbcolonia.Name = "cmbcolonia"
        Me.cmbcolonia.Size = New System.Drawing.Size(184, 21)
        Me.cmbcolonia.TabIndex = 12
        '
        'cmbidEmpresa
        '
        Me.cmbidEmpresa.AutoCompleteCustomSource.AddRange(New String() {"Regularizaci�n", "Vivienda"})
        Me.cmbidEmpresa.FormattingEnabled = True
        Me.cmbidEmpresa.Items.AddRange(New Object() {"MORAL", "FISICA"})
        Me.cmbidEmpresa.Location = New System.Drawing.Point(109, 57)
        Me.cmbidEmpresa.Name = "cmbidEmpresa"
        Me.cmbidEmpresa.Size = New System.Drawing.Size(184, 21)
        Me.cmbidEmpresa.TabIndex = 4
        '
        'Label14
        '
        Me.Label14.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label14.Location = New System.Drawing.Point(530, 110)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(91, 20)
        Me.Label14.TabIndex = 34
        Me.Label14.Text = "Fracci�n"
        '
        'Label9
        '
        Me.Label9.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label9.Location = New System.Drawing.Point(9, 62)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(91, 20)
        Me.Label9.TabIndex = 35
        Me.Label9.Text = "Empresa:"
        '
        'Label13
        '
        Me.Label13.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label13.Location = New System.Drawing.Point(278, 110)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(91, 20)
        Me.Label13.TabIndex = 34
        Me.Label13.Text = "Impuesto Flete:"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label16.Location = New System.Drawing.Point(358, 193)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(27, 13)
        Me.Label16.TabIndex = 34
        Me.Label16.Text = "C.P."
        '
        'Label15
        '
        Me.Label15.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label15.Location = New System.Drawing.Point(3, 244)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(91, 20)
        Me.Label15.TabIndex = 34
        Me.Label15.Text = "Tel�fono:"
        '
        'Label12
        '
        Me.Label12.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label12.Location = New System.Drawing.Point(9, 138)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(91, 20)
        Me.Label12.TabIndex = 34
        Me.Label12.Text = "Penalizaci�n"
        '
        'Label8
        '
        Me.Label8.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label8.Location = New System.Drawing.Point(9, 110)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(91, 20)
        Me.Label8.TabIndex = 34
        Me.Label8.Text = "Clave Impuesto"
        '
        'txtcp
        '
        Me.txtcp.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtcp.Location = New System.Drawing.Point(405, 190)
        Me.txtcp.MaxLength = 6
        Me.txtcp.Name = "txtcp"
        Me.txtcp.Size = New System.Drawing.Size(95, 20)
        Me.txtcp.TabIndex = 13
        '
        'txttelefono
        '
        Me.txttelefono.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txttelefono.Location = New System.Drawing.Point(109, 244)
        Me.txttelefono.MaxLength = 60
        Me.txttelefono.Name = "txttelefono"
        Me.txttelefono.Size = New System.Drawing.Size(184, 20)
        Me.txttelefono.TabIndex = 17
        '
        'txtpenalizacion
        '
        Me.txtpenalizacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtpenalizacion.Location = New System.Drawing.Point(109, 138)
        Me.txtpenalizacion.MaxLength = 50
        Me.txtpenalizacion.Name = "txtpenalizacion"
        Me.txtpenalizacion.Size = New System.Drawing.Size(69, 20)
        Me.txtpenalizacion.TabIndex = 9
        '
        'Label7
        '
        Me.Label7.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label7.Location = New System.Drawing.Point(9, 164)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(91, 20)
        Me.Label7.TabIndex = 32
        Me.Label7.Text = "Direcci�n:"
        '
        'txtDomicilio
        '
        Me.txtDomicilio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDomicilio.Location = New System.Drawing.Point(109, 164)
        Me.txtDomicilio.MaxLength = 255
        Me.txtDomicilio.Name = "txtDomicilio"
        Me.txtDomicilio.Size = New System.Drawing.Size(741, 20)
        Me.txtDomicilio.TabIndex = 11
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label6.Location = New System.Drawing.Point(367, 223)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(32, 13)
        Me.Label6.TabIndex = 30
        Me.Label6.Text = "Pa�s:"
        '
        'Label4
        '
        Me.Label4.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label4.Location = New System.Drawing.Point(6, 216)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(94, 20)
        Me.Label4.TabIndex = 28
        Me.Label4.Text = "Estado:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label5.Location = New System.Drawing.Point(655, 84)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(37, 13)
        Me.Label5.TabIndex = 26
        Me.Label5.Text = "R.F.C."
        '
        'txtRFCProv
        '
        Me.txtRFCProv.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRFCProv.Location = New System.Drawing.Point(698, 84)
        Me.txtRFCProv.MaxLength = 13
        Me.txtRFCProv.Name = "txtRFCProv"
        Me.txtRFCProv.Size = New System.Drawing.Size(152, 20)
        Me.txtRFCProv.TabIndex = 8
        '
        'Label3
        '
        Me.Label3.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label3.Location = New System.Drawing.Point(6, 193)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(94, 20)
        Me.Label3.TabIndex = 20
        Me.Label3.Text = "Id Colonia:"
        '
        'cmdBuscaClave
        '
        Me.cmdBuscaClave.Image = CType(resources.GetObject("cmdBuscaClave.Image"), System.Drawing.Image)
        Me.cmdBuscaClave.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdBuscaClave.Location = New System.Drawing.Point(186, 19)
        Me.cmdBuscaClave.Name = "cmdBuscaClave"
        Me.cmdBuscaClave.Size = New System.Drawing.Size(32, 32)
        Me.cmdBuscaClave.TabIndex = 3
        Me.cmdBuscaClave.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'ToolStripMenu
        '
        Me.ToolStripMenu.AutoSize = False
        Me.ToolStripMenu.BackColor = System.Drawing.Color.Silver
        Me.ToolStripMenu.Dock = System.Windows.Forms.DockStyle.None
        Me.ToolStripMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.txtMenuBusca, Me.btnMnuAltas, Me.btnMnuModificar, Me.btnMnuEliminar, Me.btnMnuReporte, Me.btnMnuOk, Me.btnMnuCancelar, Me.btnMnuSalir})
        Me.ToolStripMenu.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow
        Me.ToolStripMenu.Location = New System.Drawing.Point(3, 0)
        Me.ToolStripMenu.Name = "ToolStripMenu"
        Me.ToolStripMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.ToolStripMenu.Size = New System.Drawing.Size(730, 42)
        Me.ToolStripMenu.TabIndex = 1
        Me.ToolStripMenu.Text = "ToolStrip1"
        '
        'txtMenuBusca
        '
        Me.txtMenuBusca.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.txtMenuBusca.Name = "txtMenuBusca"
        Me.txtMenuBusca.Size = New System.Drawing.Size(150, 42)
        Me.txtMenuBusca.ToolTipText = "Filtra Listado"
        '
        'btnMnuAltas
        '
        Me.btnMnuAltas.ForeColor = System.Drawing.Color.Red
        Me.btnMnuAltas.Image = CType(resources.GetObject("btnMnuAltas.Image"), System.Drawing.Image)
        Me.btnMnuAltas.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuAltas.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuAltas.Name = "btnMnuAltas"
        Me.btnMnuAltas.Size = New System.Drawing.Size(44, 39)
        Me.btnMnuAltas.Text = "&ALTAS"
        Me.btnMnuAltas.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnMnuAltas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuModificar
        '
        Me.btnMnuModificar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuModificar.Image = CType(resources.GetObject("btnMnuModificar.Image"), System.Drawing.Image)
        Me.btnMnuModificar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuModificar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuModificar.Name = "btnMnuModificar"
        Me.btnMnuModificar.Size = New System.Drawing.Size(74, 39)
        Me.btnMnuModificar.Text = "&MODIFICAR"
        Me.btnMnuModificar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnMnuModificar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuEliminar
        '
        Me.btnMnuEliminar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuEliminar.Image = CType(resources.GetObject("btnMnuEliminar.Image"), System.Drawing.Image)
        Me.btnMnuEliminar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuEliminar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuEliminar.Name = "btnMnuEliminar"
        Me.btnMnuEliminar.Size = New System.Drawing.Size(64, 39)
        Me.btnMnuEliminar.Text = "&ELIMINAR"
        Me.btnMnuEliminar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuReporte
        '
        Me.btnMnuReporte.ForeColor = System.Drawing.Color.Red
        Me.btnMnuReporte.Image = CType(resources.GetObject("btnMnuReporte.Image"), System.Drawing.Image)
        Me.btnMnuReporte.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuReporte.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuReporte.Name = "btnMnuReporte"
        Me.btnMnuReporte.Size = New System.Drawing.Size(59, 39)
        Me.btnMnuReporte.Text = "&REPORTE"
        Me.btnMnuReporte.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuOk
        '
        Me.btnMnuOk.ForeColor = System.Drawing.Color.Red
        Me.btnMnuOk.Image = Global.Fletera.My.Resources.Resources.Save1
        Me.btnMnuOk.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuOk.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuOk.Name = "btnMnuOk"
        Me.btnMnuOk.Size = New System.Drawing.Size(56, 39)
        Me.btnMnuOk.Text = "&GRABAR"
        Me.btnMnuOk.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuCancelar
        '
        Me.btnMnuCancelar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuCancelar.Image = Global.Fletera.My.Resources.Resources.signos_f_059
        Me.btnMnuCancelar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuCancelar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuCancelar.Name = "btnMnuCancelar"
        Me.btnMnuCancelar.Size = New System.Drawing.Size(71, 39)
        Me.btnMnuCancelar.Text = "&CANCELAR"
        Me.btnMnuCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuSalir
        '
        Me.btnMnuSalir.ForeColor = System.Drawing.Color.Red
        Me.btnMnuSalir.Image = Global.Fletera.My.Resources.Resources.Salida2
        Me.btnMnuSalir.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuSalir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuSalir.Name = "btnMnuSalir"
        Me.btnMnuSalir.Size = New System.Drawing.Size(41, 39)
        Me.btnMnuSalir.Text = "&SALIR"
        Me.btnMnuSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'MeStatus1
        '
        Me.MeStatus1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.MeStatus1.Location = New System.Drawing.Point(0, 369)
        Me.MeStatus1.Margin = New System.Windows.Forms.Padding(4)
        Me.MeStatus1.Name = "MeStatus1"
        Me.MeStatus1.Size = New System.Drawing.Size(1044, 26)
        Me.MeStatus1.TabIndex = 72
        '
        'GridTodos
        '
        Me.GridTodos.AllowUserToAddRows = False
        Me.GridTodos.AllowUserToDeleteRows = False
        Me.GridTodos.AllowUserToResizeRows = False
        Me.GridTodos.BackgroundColor = System.Drawing.Color.White
        Me.GridTodos.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.GridTodos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.GridTodos.Location = New System.Drawing.Point(3, 45)
        Me.GridTodos.MultiSelect = False
        Me.GridTodos.Name = "GridTodos"
        Me.GridTodos.ReadOnly = True
        Me.GridTodos.RowHeadersVisible = False
        Me.GridTodos.Size = New System.Drawing.Size(162, 314)
        Me.GridTodos.TabIndex = 73
        '
        'Label18
        '
        Me.Label18.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label18.Location = New System.Drawing.Point(178, 273)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(94, 20)
        Me.Label18.TabIndex = 44
        Me.Label18.Text = "Genera Doc."
        '
        'chkGeneraDoc
        '
        Me.chkGeneraDoc.AutoSize = True
        Me.chkGeneraDoc.Location = New System.Drawing.Point(281, 273)
        Me.chkGeneraDoc.Name = "chkGeneraDoc"
        Me.chkGeneraDoc.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.chkGeneraDoc.Size = New System.Drawing.Size(15, 14)
        Me.chkGeneraDoc.TabIndex = 43
        Me.chkGeneraDoc.UseVisualStyleBackColor = True
        '
        'frmCatClientes
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(1044, 395)
        Me.Controls.Add(Me.GridTodos)
        Me.Controls.Add(Me.MeStatus1)
        Me.Controls.Add(Me.ToolStripMenu)
        Me.Controls.Add(Me.GrupoCaptura)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmCatClientes"
        Me.Text = "Catal�go de Clientes"
        Me.GrupoCaptura.ResumeLayout(False)
        Me.GrupoCaptura.PerformLayout()
        Me.ToolStripMenu.ResumeLayout(False)
        Me.ToolStripMenu.PerformLayout()
        CType(Me.GridTodos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region
    Sub New(ByVal vTag As String)
        InitializeComponent()
        _Tag = vTag
    End Sub

    'Public Sub New(con As String, v As Boolean)
    '    Me.con = con
    '    BD = New CapaDatos.UtilSQL(con, "Juan")
    '    InitializeComponent()
    '    Me.v = v
    'End Sub
    Public Sub New(con As String, nomUsuario As String, cveEmpresa As Integer, IdUsuario As Integer)
        _con = con
        BD = New CapaDatos.UtilSQL(con, "Juan")
        InitializeComponent()
        _Usuario = nomUsuario
        _cveEmpresa = cveEmpresa

        '_cveEmpresa = cveEmpresa
        _IdUsuario = IdUsuario

    End Sub

#Region "MenusToolStrip"
    Private Sub LimpiaVariables()
        SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")

    End Sub
    Private Sub LimpiaCampos()
        txtIdCliente.Text = ""
        txtNombre.Text = ""
        txtRFCProv.Text = ""
        txtDomicilio.Text = ""
        'cmbidEmpresa.SelectedIndex = 0
        cmbidEmpresa.SelectedValue = _cveEmpresa



        LimpiaVariables()
    End Sub

    Private Sub ActivaCampos(ByVal valor As Boolean, ByVal vOpcion As TipoOpcActivaCampos)
        If vOpcion = TipoOpcActivaCampos.tOpcDESHABTODOS Then
            txtIdCliente.Enabled = False
            txtNombre.Enabled = False
            txtRFCProv.Enabled = False
            txtDomicilio.Enabled = False
            cmbidEmpresa.Enabled = False

            cmbClave_impuesto.Enabled = False
            cmbidImpuestoFlete.Enabled = False
            cmbclave_Fraccion.Enabled = False
            cmbcolonia.Enabled = False
            txtpenalizacion.Enabled = False
            cmbEstatus.Enabled = False

            chkCambioPrecio.Enabled = False
            chkGeneraDoc.Enabled = False


        ElseIf vOpcion = TipoOpcActivaCampos.tOpcINICIALIZA Then
            txtIdCliente.Enabled = Not valor
            txtNombre.Enabled = valor
            txtRFCProv.Enabled = valor
            txtDomicilio.Enabled = valor
            'cmbidEmpresa.Enabled = valor
            cmbidEmpresa.Enabled = False
            cmbClave_impuesto.Enabled = valor
            cmbidImpuestoFlete.Enabled = valor
            cmbclave_Fraccion.Enabled = valor
            cmbcolonia.Enabled = valor
            txtpenalizacion.Enabled = valor
            cmbEstatus.Enabled = valor
            chkCambioPrecio.Enabled = valor
            chkGeneraDoc.Enabled = valor

        End If
    End Sub


    Private Sub ActivaBotones(ByVal valor As Boolean, Optional ByVal vOpcion As TipoOpcActivaBoton = TipoOpcActivaBoton.tOpcInicializa)
        If vOpcion = TipoOpcActivaBoton.tOpcConsulta Then
            btnMnuOk.Enabled = False
            btnMnuCancelar.Enabled = valor
            btnMnuSalir.Enabled = Not valor

            btnMnuAltas.Enabled = Not valor
            btnMnuModificar.Enabled = Not valor
            btnMnuEliminar.Enabled = Not valor
            btnMnuReporte.Enabled = Not valor
        ElseIf vOpcion = TipoOpcActivaBoton.tOpcInicializa Then
            btnMnuOk.Enabled = valor
            btnMnuCancelar.Enabled = valor
            btnMnuSalir.Enabled = Not valor

            btnMnuAltas.Enabled = Not valor
            btnMnuModificar.Enabled = Not valor
            btnMnuEliminar.Enabled = Not valor
            btnMnuReporte.Enabled = Not valor

            txtMenuBusca.Enabled = Not valor
            GridTodos.Enabled = Not valor
        ElseIf vOpcion = TipoOpcActivaBoton.tOpcInsertar Then
            btnMnuOk.Enabled = valor
            btnMnuCancelar.Enabled = valor
            btnMnuSalir.Enabled = Not valor

            btnMnuAltas.Enabled = Not valor
            btnMnuModificar.Enabled = Not valor
            btnMnuEliminar.Enabled = Not valor
            btnMnuReporte.Enabled = Not valor
        End If
    End Sub

    Private Sub btnMnuAltas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuAltas.Click
        'ALTAS
        '15/FEB/2016
        'If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Alta) = False Then
        '    MessageBox.Show("No tiene los permisos Necesarios para poder dar de Alta en el Catalogo de Proveedor!!!",
        '        "Acceso Denegado a Alta de Proveedor", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    OpcionForma = TipoOpcionForma.tOpcConsultar
        '    Exit Sub
        'End If
        OpcionForma = TipoOpcionForma.tOpcInsertar
        LimpiaCampos()
        ActivaCampos(True, TipoOpcActivaCampos.tOpcINICIALIZA)
        ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)

        If bEsIdentidad Then
            txtIdCliente.Enabled = False
            txtNombre.Focus()
        Else
            txtIdCliente.Focus()
        End If
        Status("Ingrese nuevo Cliente para guardar", Me)
    End Sub
    Private Sub btnMnuModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuModificar.Click
        'MODIFICAR
        'If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Modificar) = False Then
        '    MessageBox.Show("No tiene los permisos Necesarios para poder Modificar la Informacion del Proveedor!!!",
        '           "Acceso Denegado a Modificacion de la Informacion del Proveedor", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    OpcionForma = TipoOpcionForma.tOpcConsultar
        '    Exit Sub
        'End If
        If Trim(txtIdCliente.Text) = "" Then
            txtIdCliente.Text = DespliegaGrid(TablaBd, Inicio.CONSTR, CampoLLave)
            CargaForm(txtIdCliente.Text, UCase(TablaBd))
        End If

        OpcionForma = TipoOpcionForma.tOpcModificar
        ActivaCampos(True, TipoOpcActivaCampos.tOpcINICIALIZA)
        ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)

        btnMnuEliminar.Enabled = True
        If bEsIdentidad Then
            txtIdCliente.Enabled = False
            'txtNombre.Focus()
            cmbidEmpresa.Focus()
        Else
            txtIdCliente.Focus()
        End If

        Status("Ingrese Cliente para guardar", Me)
    End Sub
    Private Sub btnMnuEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuEliminar.Click
        'ELIMINAR
        'If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Baja) = False Then
        '    MessageBox.Show("No tiene los permisos Necesarios para poder Eliminar el Proveedor!!!",
        '        "Acceso Denegado a Eliminacion de Proveedor", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    OpcionForma = TipoOpcionForma.tOpcConsultar
        '    Exit Sub
        'End If
        If txtIdCliente.Text = Cliente.IdCliente Then
            With Cliente
                If MessageBox.Show("Esta completamente seguro que desea Eliminar el Cliente: " &
                                   .Nombre & " Con Id: " & .idColonia & " ????", "Confirma que desea eliminar la Colonia: " & .Nombre & "???",
                                 MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then Exit Sub
                Try
                    .Eliminar() 'Elimina el municipio
                    MessageBox.Show("Cliente: " & .Nombre & " Eliminado con exito!!!",
                                    "Cliente Eliminado con exito", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Status("Cliente: " & .Nombre & " Eliminado con exito!!!", Me)
                    ActualizaGrid(txtMenuBusca.Text)
                    txtIdCliente.Text = ""
                    OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
                Catch ex As Exception
                    Status("Error al tratar de eliminar el Cliente: " & ex.Message, Me, Err:=True)
                    MessageBox.Show("Error al Tratar de eliminar el Cliente: " & .Nombre & vbNewLine &
                                    "Error. " & ex.Message, "Error: " & Err.Number, MessageBoxButtons.OK, MessageBoxIcon.Error)

                End Try
            End With
        End If
    End Sub
    Private Sub btnMnuReporte_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuReporte.Click
        'REPORTE
        'If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Reporte) = False Then
        '    MessageBox.Show("No tiene los permisos Necesarios para poder realizar Reporte de Proveedor!!!",
        '        "Acceso Denegado a Reporte de Proveedor", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    OpcionForma = TipoOpcionForma.tOpcConsultar
        '    Exit Sub
        'End If
        ToolStripMenu.Enabled = False
        Status("Abriendo Reporte Espere. . . ", Me)
        Me.Cursor = Cursors.WaitCursor
        DespliegaReporte(TablaBd, CONSTR)
        Status("Finalizo el reporte. . . ", Me)
        Me.Cursor = Cursors.Default
        ToolStripMenu.Enabled = True
    End Sub

#End Region
    Private Sub ActualizaGrid(ByVal vNomAlmacen As String)
        If vNomAlmacen = "" Then
            GridTodos.DataSource = BD.ExecuteReturn("SELECT IdCliente, Nombre FROM " & TablaBd & " where idempresa = " & _cveEmpresa, TablaBd)
            Status(GridTodos.RowCount & " Registros", Me)
        Else
            GridTodos.DataSource = BD.ExecuteReturn("SELECT IdCliente, Nombre FROM " & TablaBd &
                                                " WHERE idempresa = " & _cveEmpresa & " and Nombre Like '%" & vNomAlmacen & "%'")
            Status(GridTodos.RowCount & " Registros obtenidos con el filtro %" & vNomAlmacen & "%", Me)
        End If
    End Sub

    Private Sub OpcionMnuTools(ByVal vOpcion As TipoOpcionForma)
        If vOpcion <> TipoOpcionForma.tOpcImprimir And vOpcion <> TipoOpcionForma.tOpcEliminar Then
            OpcionForma = vOpcion
        End If
        txtMenuBusca.Enabled = True
        GridTodos.Enabled = True
        If vOpcion = TipoOpcionForma.tOpcConsultar Then
            'CONSULTA
            For Each ct As Control In GrupoCaptura.Controls
                If Not ct Is cmdBuscaClave Then
                    ct.Enabled = False
                Else
                    ct.Enabled = True
                End If
            Next
            txtMenuBusca.Enabled = True
            btnMnuAltas.Enabled = True
            btnMnuModificar.Enabled = True
            btnMnuEliminar.Enabled = False
            btnMnuReporte.Enabled = True
            btnMnuOk.Enabled = False
            btnMnuCancelar.Enabled = False

            btnMnuSalir.Enabled = True


        End If
    End Sub


    Private Sub txtMenuBusca_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtMenuBusca.KeyPress
        If Asc(e.KeyChar) = 39 Then e.Handled = True : Exit Sub 'Caracter ' Evita errores
        If Asc(e.KeyChar) = 13 Then
            GridTodos.Focus()
            e.Handled = True
        End If
    End Sub

    'Private Sub txtMenuBusca_KeyUp(sender As Object, e As KeyEventArgs) Handles txtMenuBusca.KeyUp
    '    If txtMenuBusca.Text.Trim = "" Then Exit Sub
    '    If e.KeyCode = Keys.Down Or e.KeyCode = Keys.Up Then
    '        GridTodos.Focus()
    '        e.Handled = True
    '        Exit Sub
    '    End If
    '    If e.KeyCode = Keys.Left Or e.KeyCode = Keys.Right Then Exit Sub
    '    ActualizaGrid(txtMenuBusca.Text)
    'End Sub

    Private Sub frmCatClientes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Inicio.CONSTR = "" Then
            Inicio.CONSTR = _con
        End If
        LlenaCombos()
        OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
        ActualizaGrid("")
        GridTodos.Columns(1).Width = GridTodos.Width - 20
        GridTodos.Columns(0).Visible = False
        txtMenuBusca.Focus()
    End Sub


    Private Sub GridTodos_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles GridTodos.CellEnter
        CargaForm(GridTodos.Rows(GridTodos.CurrentRow.Index).Cells(0).Value, UCase(TablaBd))
    End Sub
    Private Sub GridTodos_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles GridTodos.KeyPress
        If Asc(e.KeyChar) = 8 Then
            txtMenuBusca.Focus()
            e.Handled = True
        End If
    End Sub
    Private Sub CargaForm(ByVal idClave As Integer, ByVal NomTabla As String)
        Try
            Select Case UCase(NomTabla)
                Case UCase(TablaBd)
                    Cliente = New ClientesClass(idClave)
                    With Cliente
                        txtIdCliente.Text = .IdCliente
                        If .Existe Then
                            txtNombre.Text = .Nombre
                            txtRFCProv.Text = .RFC
                            txtDomicilio.Text = .Domicilio
                            cmbidEmpresa.SelectedValue = .idEmpresa
                            'cmbTipoPersona.Text = IIf(.TipoPersona = "M", "MORAL", "FISICA")
                            Colonia = New ColoniaClass(0, .colonia, .municipio)
                            If Colonia.Existe Then
                                cmbcolonia.SelectedValue = Colonia.idColonia
                                cmbmunicipio.SelectedValue = Colonia.idCiudad
                                cmbestado.SelectedValue = Colonia.idEstado
                                cmbpais.SelectedValue = Colonia.idPais
                                txtcp.Text = Colonia.CP
                            End If
                            Impuesto = New ImpuestosClass(.Clave_impuesto)
                            If Impuesto.Existe Then
                                cmbClave_impuesto.SelectedValue = Impuesto.idImpuesto
                            End If
                            Impuesto = New ImpuestosClass(.idImpuestoFlete)
                            If Impuesto.Existe Then
                                cmbidImpuestoFlete.SelectedValue = Impuesto.idImpuesto
                            End If
                            txtpenalizacion.Text = .penalizacion
                            Fraccion = New FraccionesClass(.clave_Fraccion, .idEmpresa)
                            If Fraccion.Existe Then
                                cmbclave_Fraccion.SelectedValue = Fraccion.IdFraccion
                            End If
                            If .Activo Then
                                cmbEstatus.SelectedValue = "ACT"
                            Else
                                cmbEstatus.SelectedValue = "BAJA"
                            End If
                            chkCambioPrecio.Checked = .CambioPrecio
                            chkGeneraDoc.Checked = .GeneraDoc

                            'txtNomColonia.Text = .
                            'txtCIDALMACEN_COM.Text = .CIDALMACEN_COM
                            If OpcionForma = TipoOpcionForma.tOpcInsertar Then
                                Status("El Cliente Con id: " & .idColonia &
                                       " Ya existe!!!, Favor de modificarlo para poder dar de alta a otro Cliente", Me, Err:=True)
                                MessageBox.Show("El Cliente con id: " & .idColonia &
                                                " YA EXISTE!!!, Favor de Cambiarlo y volver a intentarlo",
                                                "El Cliente con id: " & .idColonia & " YA EXISTE!!!", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            End If
                        Else 'No existe 
                            If OpcionForma = TipoOpcionForma.tOpcInsertar Then
                                'For Each Ct As Control In GrupoCaptura.Controls
                                '    If Not Ct Is txtidAlmacen And Not Ct Is cmdBuscaClave Then
                                '        Ct.Enabled = True
                                '    Else
                                '        Ct.Enabled = False
                                '    End If
                                '    If TypeOf (Ct) Is TextBox Then
                                '        If Not Ct Is txtidAlmacen Then
                                '            TryCast(Ct, TextBox).Text = ""
                                '        End If
                                '    End If
                                'Next
                                ActivaCampos(True, TipoOpcActivaCampos.tOpcINICIALIZA)
                                ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)
                                btnMnuOk.Enabled = True
                                txtNombre.Focus()
                                Status("Listo para Guardar nuevo Cliente con id: " & .idColonia, Me)
                            End If
                        End If
                    End With
                Case UCase("CatColonia")
                    Colonia = New ColoniaClass(idClave)
                    If Colonia.Existe Then
                        cmbcolonia.SelectedValue = Colonia.idColonia
                        cmbmunicipio.SelectedValue = Colonia.idCiudad
                        cmbestado.SelectedValue = Colonia.idEstado
                        cmbpais.SelectedValue = Colonia.idPais
                        txtcp.Text = Colonia.CP
                    End If
            End Select



        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try

    End Sub
    Private Function Validar() As Boolean
        Validar = True
        If Not Inicio.ValidandoCampo(Trim(txtIdCliente.Text), TipoDato.TdNumerico) Then
            If Not bEsIdentidad Then
                MsgBox(MENSAJE_CLAVE, vbInformation, "Aviso" & Me.Text)
                If txtIdCliente.Enabled Then
                    txtIdCliente.Focus()
                    txtIdCliente.SelectAll()
                End If
                Validar = False
            End If
        ElseIf Not Inicio.ValidandoCampo(Trim(txtNombre.Text), TipoDato.TdCadena) Then
            MsgBox("El Nombre del Cliente No Puede Ser Vacio", vbInformation, "Aviso" & Me.Text)
            If txtNombre.Enabled Then
                txtNombre.Focus()
                txtNombre.SelectAll()
            End If
            Validar = False
            'ElseIf Not Inicio.ValidandoCampo(Trim(txtidColonia.Text), TipoDato.TdNumerico) Then
            '    MsgBox("La Colonia del Proveedor no Puede ser Vacio", vbInformation, "Aviso" & Me.Text)
            '    If txtidColonia.Enabled Then
            '        txtidColonia.Focus()
            '        txtidColonia.SelectAll()
            '    End If
            '    Validar = False
        End If
    End Function

    'Private Sub txtidAlmacen_EnabledChanged(sender As Object, e As EventArgs) Handles txtidAlmacen.EnabledChanged
    '    cmdBuscaClave.Enabled = txtidAlmacen.Enabled
    'End Sub
    'CONTROLES
    Private Sub txtidAlmacen_KeyDown(sender As Object, e As KeyEventArgs) Handles txtIdCliente.KeyDown
        If e.KeyCode = Keys.F3 Then
            cmdBuscaClave_Click(sender, e)
        End If

    End Sub
    Private Sub txtidAlmacen_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtIdCliente.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If txtIdCliente.Text.Trim <> "" Then
                CargaForm(txtIdCliente.Text, UCase(TablaBd))
            End If
            e.Handled = True
        End If
    End Sub

    Private Sub cmdBuscaClave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBuscaClave.Click
        Dim idEst As String = DespliegaGrid(UCase(TablaBd), Inicio.CONSTR, CampoLLave)
        If idEst <> "" Then CargaForm(idEst, UCase(TablaBd))
    End Sub









    Private Sub cmdCancelar_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        With TryCast(sender, Button)
            If .Enabled Then
                Me.CancelButton = sender
            End If
        End With
    End Sub
    Private Sub cmdTerminar_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        With TryCast(sender, Button)
            If .Enabled Then
                Me.CancelButton = sender
            End If
        End With
    End Sub

    Private Sub btnMnuOk_Click(sender As Object, e As EventArgs) Handles btnMnuOk.Click
        Dim CadCam As String = ""
        Try
            Salida = True
            If Not Validar() Then Exit Sub
            If GridTodos.RowCount = 0 Then
                Cliente = New ClientesClass(0)
            End If
            With Cliente
                If OpcionForma = TipoOpcionForma.tOpcModificar Then
                    'CadCam = .GetCambios(txtRazonSocial.Text, txtRFCProv.Text, txtDireccionProv.Text, txtidColonia.Text, txtEmailProv.Text, cmbTipoPersona.SelectedValue)
                    CadCam = .GetCambios(cmbidEmpresa.SelectedValue, txtNombre.Text, txtDomicilio.Text, txtRFCProv.Text, cmbClave_impuesto.SelectedValue,
                    IIf(cmbEstatus.SelectedValue = "ACT", True, False), cmbidImpuestoFlete.SelectedValue, cmbclave_Fraccion.SelectedValue, txtpenalizacion.Text, cmbestado.Text,
                    cmbpais.Text, cmbmunicipio.Text, cmbcolonia.Text, txtcp.Text, txttelefono.Text, cmbcolonia.SelectedValue, chkCambioPrecio.Checked, chkGeneraDoc.Checked)


                    If CadCam <> "" Then
                        'If MessageBox.Show("Esta completamente Seguro que Desea guardar los Cambios Realizados al Estado Con id: " & .IDMARCA & vbNewLine &
                        '                CadCam, "Confirma que desea guardar los Cambios Realizados???", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then Exit Sub
                        Status("Guardando Modificaciones del Cliente Con id: " & .idColonia, Me, 1, 2)

                        '.Guardar(bEsIdentidad, txtRazonSocial.Text, txtRFCProv.Text, txtDireccionProv.Text, txtidColonia.Text, txtEmailProv.Text, cmbTipoPersona.Text)
                        .Guardar(bEsIdentidad, cmbidEmpresa.SelectedValue, txtNombre.Text, txtDomicilio.Text, txtRFCProv.Text, cmbClave_impuesto.SelectedValue,
                        IIf(cmbEstatus.SelectedValue = "ACT", True, False), cmbidImpuestoFlete.SelectedValue, cmbclave_Fraccion.SelectedValue, txtpenalizacion.Text, cmbestado.Text,
                        cmbpais.Text, cmbmunicipio.Text, cmbcolonia.Text, txtcp.Text, txttelefono.Text, cmbcolonia.SelectedValue, chkCambioPrecio.Checked, chkGeneraDoc.Checked)

                        Status("Cliente con Id: " & .idColonia & " Modificado Con Exito!!!", Me, 2, 2)
                        MessageBox.Show("Cliente con Id: " & .idColonia & " Modificada con exito!!!",
                                        "Accion terminada con exito!!!", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
                        ActualizaGrid(txtMenuBusca.Text)
                    Else
                        Status("No se detectaron cambios para realizar!!!", Me, Err:=True)
                    End If
                ElseIf OpcionForma = TipoOpcionForma.tOpcInsertar Then
                    .Existe = False
                    .Nombre = txtNombre.Text
                    If Not bEsIdentidad Then
                        .idColonia = txtIdCliente.Text
                    End If
                    Status("Guardando Nuevo Cliente: " & txtNombre.Text & ". . .", Me, 1, 2)
                    '.Guardar(bEsIdentidad, txtRazonSocial.Text, txtRFCProv.Text, txtDireccionProv.Text, txtidColonia.Text, txtEmailProv.Text, cmbTipoPersona.Text)
                    .Guardar(bEsIdentidad, cmbidEmpresa.SelectedValue, txtNombre.Text, txtDomicilio.Text, txtRFCProv.Text, cmbClave_impuesto.SelectedValue,
                        IIf(cmbEstatus.SelectedValue = "ACT", True, False), cmbidImpuestoFlete.SelectedValue, cmbclave_Fraccion.SelectedValue, txtpenalizacion.Text, cmbestado.Text,
                        cmbpais.Text, cmbmunicipio.Text, cmbcolonia.Text, txtcp.Text, txttelefono.Text, cmbcolonia.SelectedValue, chkCambioPrecio.Checked, chkGeneraDoc.Checked)

                    Status("Cliente: " & txtNombre.Text & " Guardado con Exito!!!", Me, 2, 2)
                    MessageBox.Show("Nuevo Cliente: " & txtNombre.Text & " Guardado con exito!!!",
                                    "Accion terminada con exito!!!", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
                    ActualizaGrid(txtMenuBusca.Text)

                End If
            End With
            Salida = False
        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try
    End Sub


    Private Sub btnMnuCancelar_Click(sender As Object, e As EventArgs) Handles btnMnuCancelar.Click
        If txtIdCliente.Text <> "" Then
            CargaForm(txtIdCliente.Text, UCase(TablaBd))
        End If
        OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
    End Sub

    Private Sub btnMnuSalir_Click(sender As Object, e As EventArgs) Handles btnMnuSalir.Click
        Me.Close()
    End Sub

    Private Sub txtMenuBusca_TextChanged(sender As Object, e As EventArgs) Handles txtMenuBusca.TextChanged
        ActualizaGrid(txtMenuBusca.Text)
    End Sub

    Private Sub txtNomColonia_TextChanged(sender As Object, e As EventArgs) Handles txtNombre.TextChanged

    End Sub


    Private Sub txtCP_TextChanged(sender As Object, e As EventArgs) Handles txtRFCProv.TextChanged

    End Sub

    'Private Sub cmbTipoPersona_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbidEmpresa.KeyPress, cmbpais.KeyPress, cmbestado.KeyPress, cmbmunicipio.KeyPress
    '    If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
    '        txtRFCProv.Focus()
    '        txtRFCProv.SelectAll()
    '    End If
    'End Sub


    'Private Sub txtEmailProv_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtpenalizacion.KeyPress, txtcp.KeyPress, txttelefono.KeyPress
    '    If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
    '        txtDomicilio.Focus()
    '        txtDomicilio.SelectAll()
    '    End If
    'End Sub





    Private Sub txtDireccionProv_TextChanged(sender As Object, e As EventArgs) Handles txtDomicilio.TextChanged

    End Sub

#Region "Codigo Nuevo"

    Public Function AutoCompletarFracciones(ByVal Control As ComboBox, ByVal bIncluyeTodos As Boolean, ByVal idEmpresa As Integer) As AutoCompleteStringCollection
        Dim filtro As String = ""
        Dim Coleccion As New AutoCompleteStringCollection
        Dim dt As DataTable

        If idEmpresa > 0 Then
            filtro = " idEmpresa = " & idEmpresa
        End If
        Fraccion = New FraccionesClass(0, 0)
        dt = Fraccion.TablaFracciones(False, filtro)

        'Recorrer y cargar los items para el Autocompletado
        For Each row As DataRow In dt.Rows
            Coleccion.Add(Convert.ToString(row("Descripcion")))
        Next
        'Ajustamos el control TextBox o ComboBox para recibir los datos de la siguiente manera.
        With Control
            .DataSource = dt
            .DisplayMember = "Descripcion"
            .ValueMember = "IdFraccion"


            .AutoCompleteCustomSource = Coleccion
            .AutoCompleteMode = AutoCompleteMode.Suggest
            .AutoCompleteSource = AutoCompleteSource.CustomSource
        End With

        ' Aca le devuelvo los datos a recuperados de la Base
        Return Coleccion

    End Function
    Public Function AutoCompletarImpuesto(ByVal Control As ComboBox, ByVal bIncluyeTodos As Boolean) As AutoCompleteStringCollection

        Dim Coleccion As New AutoCompleteStringCollection
        Dim dt As DataTable

        Impuesto = New ImpuestosClass(0)
        dt = Impuesto.TablaImpuestos(bIncluyeTodos)


        'Recorrer y cargar los items para el Autocompletado
        For Each row As DataRow In dt.Rows
            Coleccion.Add(Convert.ToString(row("Descripcion")))
        Next
        'Ajustamos el control TextBox o ComboBox para recibir los datos de la siguiente manera.
        With Control
            .DataSource = dt
            .DisplayMember = "Descripcion"
            .ValueMember = "idImpuesto"


            .AutoCompleteCustomSource = Coleccion
            .AutoCompleteMode = AutoCompleteMode.Suggest
            .AutoCompleteSource = AutoCompleteSource.CustomSource
        End With

        ' Aca le devuelvo los datos a recuperados de la Base
        Return Coleccion

    End Function
    Public Function AutoCompletarColonias(ByVal Control As ComboBox, ByVal bIncluyeTodos As Boolean) As AutoCompleteStringCollection

        Dim Coleccion As New AutoCompleteStringCollection
        Dim dt As DataTable


        Colonia = New ColoniaClass(0)
        dt = Colonia.TablaColonias(bIncluyeTodos)

        'Recorrer y cargar los items para el Autocompletado
        For Each row As DataRow In dt.Rows
            Coleccion.Add(Convert.ToString(row("NomColonia")))
        Next
        'Ajustamos el control TextBox o ComboBox para recibir los datos de la siguiente manera.
        With Control
            .DataSource = dt
            .DisplayMember = "NomColonia"
            .ValueMember = "idColonia"


            .AutoCompleteCustomSource = Coleccion
            .AutoCompleteMode = AutoCompleteMode.Suggest
            .AutoCompleteSource = AutoCompleteSource.CustomSource
        End With

        ' Aca le devuelvo los datos a recuperados de la Base
        Return Coleccion

    End Function
    Public Function AutoCompletarEmpresas(ByVal Control As ComboBox, ByVal bIncluyeTodos As Boolean) As AutoCompleteStringCollection

        Dim Coleccion As New AutoCompleteStringCollection
        Dim dt As DataTable


        Empresa = New EmpresaClass(0)
        dt = Empresa.TablaEmpresas(bIncluyeTodos)

        'Recorrer y cargar los items para el Autocompletado
        For Each row As DataRow In dt.Rows
            Coleccion.Add(Convert.ToString(row("RazonSocial")))
        Next
        'Ajustamos el control TextBox o ComboBox para recibir los datos de la siguiente manera.
        With Control
            .DataSource = dt
            .DisplayMember = "RazonSocial"
            .ValueMember = "idEmpresa"


            .AutoCompleteCustomSource = Coleccion
            .AutoCompleteMode = AutoCompleteMode.Suggest
            .AutoCompleteSource = AutoCompleteSource.CustomSource
        End With

        ' Aca le devuelvo los datos a recuperados de la Base
        Return Coleccion

    End Function
    Private Function LlenaCombos() As Boolean
        Dim StrSql As String = ""
        LlenaCombos = True
        Try
            DsCombo.Clear()
            StrSql = "select 'ACT' as IdEstatus, 'ACTIVO' as Estatus " &
            "union " &
            "select 'BAJA' as IdEstatus, 'BAJA' as Estatus"

            DsCombo = Inicio.LlenaCombos("IdEstatus", "Estatus", "CatEstatus",
                               TipoConexion.TcSQL, Inicio.CONSTR, Process.GetCurrentProcess.ProcessName,
                               KEY_CHECAERROR, KEY_ESTATUS, , StrSql)
            'DsCombo = BD.ExecuteReturn(StrSql)
            If Not DsCombo Is Nothing Then
                If DsCombo.Tables("CatEstatus").DefaultView.Count > 0 Then
                    cmbEstatus.DataSource = DsCombo.Tables("CatEstatus")
                    cmbEstatus.ValueMember = "IdEstatus"
                    cmbEstatus.DisplayMember = "Estatus"
                End If
            Else
                MsgBox("No se han dado de alta a Estatus ", vbInformation, "Aviso" & Me.Text)
                LlenaCombos = False
                Exit Function
            End If

            'Empresas
            AutoCompletarEmpresas(cmbidEmpresa, False)
            'Colonias
            AutoCompletarColonias(cmbcolonia, False)
            'Impuestos
            AutoCompletarImpuesto(cmbClave_impuesto, False)
            AutoCompletarImpuesto(cmbidImpuestoFlete, False)
            'Fracciones
            AutoCompletarFracciones(cmbclave_Fraccion, False, 0)
            'Paises
            AutoCompletarPaises(cmbpais, False)
            'Ciudadades
            AutoCompletarCiudades(cmbmunicipio, False)
            'Estados
            AutoCompletarEstados(cmbestado, False)
        Catch ex As Exception

        End Try
    End Function
    Public Function AutoCompletarPaises(ByVal Control As ComboBox, ByVal bIncluyeTodos As Boolean) As AutoCompleteStringCollection
        Dim Coleccion As New AutoCompleteStringCollection
        Dim dt As DataTable

        Pais = New PaisClass(0)
        dt = Pais.TablaPaises(False, "")

        'Recorrer y cargar los items para el Autocompletado
        For Each row As DataRow In dt.Rows
            Coleccion.Add(Convert.ToString(row("NomPais")))
        Next
        'Ajustamos el control TextBox o ComboBox para recibir los datos de la siguiente manera.
        With Control
            .DataSource = dt
            .DisplayMember = "NomPais"
            .ValueMember = "idPais"


            .AutoCompleteCustomSource = Coleccion
            .AutoCompleteMode = AutoCompleteMode.Suggest
            .AutoCompleteSource = AutoCompleteSource.CustomSource
        End With

        ' Aca le devuelvo los datos a recuperados de la Base
        Return Coleccion

    End Function
    Public Function AutoCompletarCiudades(ByVal Control As ComboBox, ByVal bIncluyeTodos As Boolean) As AutoCompleteStringCollection
        Dim Coleccion As New AutoCompleteStringCollection
        Dim dt As DataTable


        Ciudad = New CiudadClass(0)

        dt = Ciudad.TablaCiudades(False, "")


        'Recorrer y cargar los items para el Autocompletado
        For Each row As DataRow In dt.Rows
            Coleccion.Add(Convert.ToString(row("NomCiudad")))
        Next
        'Ajustamos el control TextBox o ComboBox para recibir los datos de la siguiente manera.
        With Control
            .DataSource = dt
            .DisplayMember = "NomCiudad"
            .ValueMember = "idCiudad"


            .AutoCompleteCustomSource = Coleccion
            .AutoCompleteMode = AutoCompleteMode.Suggest
            .AutoCompleteSource = AutoCompleteSource.CustomSource
        End With

        ' Aca le devuelvo los datos a recuperados de la Base
        Return Coleccion

    End Function
    Public Function AutoCompletarEstados(ByVal Control As ComboBox, ByVal bIncluyeTodos As Boolean) As AutoCompleteStringCollection
        Dim Coleccion As New AutoCompleteStringCollection
        Dim dt As DataTable


        Estado = New EstadosClass(0)

        dt = Estado.TablaEstados(False, "")


        'Recorrer y cargar los items para el Autocompletado
        For Each row As DataRow In dt.Rows
            Coleccion.Add(Convert.ToString(row("NomEstado")))
        Next
        'Ajustamos el control TextBox o ComboBox para recibir los datos de la siguiente manera.
        With Control
            .DataSource = dt
            .DisplayMember = "NomEstado"
            .ValueMember = "idEstado"


            .AutoCompleteCustomSource = Coleccion
            .AutoCompleteMode = AutoCompleteMode.Suggest
            .AutoCompleteSource = AutoCompleteSource.CustomSource
        End With

        ' Aca le devuelvo los datos a recuperados de la Base
        Return Coleccion

    End Function

    Private Sub cmbpais_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbpais.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then

        End If
    End Sub

    Private Sub cmbcolonia_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbcolonia.SelectedIndexChanged

    End Sub

    Private Sub cmbcolonia_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbcolonia.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then


        End If
    End Sub

    Private Sub cmbcolonia_KeyUp(sender As Object, e As KeyEventArgs) Handles cmbcolonia.KeyUp
        If e.KeyCode = Keys.Enter Then
            CargaForm(cmbcolonia.SelectedValue, "CatColonia")
        End If
    End Sub
    Private Sub cmbidEmpresa_KeyUp(sender As Object, e As KeyEventArgs) Handles cmbidEmpresa.KeyUp
        If e.KeyCode = Keys.Enter Then
            txtNombre.Focus()
        End If
    End Sub
    Private Sub txtNomColonia_MouseDown(sender As Object, e As MouseEventArgs) Handles txtNombre.MouseDown
        txtNombre.Focus()
        txtNombre.SelectAll()
    End Sub
    Private Sub txtNomColonia_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtNombre.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            txtRFCProv.Focus()
        End If
    End Sub
    Private Sub txtCP_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtRFCProv.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            cmbClave_impuesto.Focus()
        End If
    End Sub

    Private Sub txtCP_MouseDown(sender As Object, e As MouseEventArgs) Handles txtRFCProv.MouseDown
        txtRFCProv.Focus()
        txtRFCProv.SelectAll()
    End Sub

    Private Sub cmbClave_impuesto_KeyUp(sender As Object, e As KeyEventArgs) Handles cmbClave_impuesto.KeyUp
        If e.KeyCode = Keys.Enter Then
            cmbidImpuestoFlete.Focus()
        End If
    End Sub

    Private Sub cmbidImpuestoFlete_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbidImpuestoFlete.SelectedIndexChanged

    End Sub

    Private Sub cmbidImpuestoFlete_KeyUp(sender As Object, e As KeyEventArgs) Handles cmbidImpuestoFlete.KeyUp
        If e.KeyCode = Keys.Enter Then
            cmbclave_Fraccion.Focus()
        End If
    End Sub

    Private Sub cmbclave_Fraccion_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbclave_Fraccion.SelectedIndexChanged

    End Sub

    Private Sub cmbclave_Fraccion_KeyUp(sender As Object, e As KeyEventArgs) Handles cmbclave_Fraccion.KeyUp
        If e.KeyCode = Keys.Enter Then
            txtpenalizacion.Focus()
        End If
    End Sub

    Private Sub txtpenalizacion_TextChanged(sender As Object, e As EventArgs) Handles txtpenalizacion.TextChanged

    End Sub

    Private Sub txtpenalizacion_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtpenalizacion.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            cmbEstatus.Focus()
        End If
    End Sub

    Private Sub cmbEstatus_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbEstatus.SelectedIndexChanged

    End Sub

    Private Sub cmbEstatus_KeyUp(sender As Object, e As KeyEventArgs) Handles cmbEstatus.KeyUp
        If e.KeyCode = Keys.Enter Then
            txtDomicilio.Focus()
        End If
    End Sub
    Private Sub txtDireccionProv_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtDomicilio.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            cmbcolonia.Focus()
        End If

    End Sub

    Private Sub txtDireccionProv_MouseDown(sender As Object, e As MouseEventArgs) Handles txtDomicilio.MouseDown
        txtDomicilio.Focus()
        txtDomicilio.SelectAll()
    End Sub

#End Region
End Class
