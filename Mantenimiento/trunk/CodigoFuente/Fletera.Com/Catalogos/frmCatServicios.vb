'Fecha: 22 / Febrero / 2016
'ROGER GALA PACHECO
'************************************************************************************************************************************
'*  Fecha Modificacion :   22 / Febrero / 2016                                                                                       
'*  
'************************************************************************************************************************************
Public Class frmCatServicios
    Inherits System.Windows.Forms.Form
    Dim OpcionForma As TipoOpcionForma
    Dim TablaBd As String = "CatServicios"
    Dim CampoLLave As String = "idServicio"
    Dim TablaBd2 As String = "CatTipoServicio"
    Public WithEvents MeStatus1 As ControlStatus.MeStatus
    Friend WithEvents GridTodos As System.Windows.Forms.DataGridView
    Private Servicio As ServicioClass
    Private TipoServ As TipoServClass
    Friend WithEvents btnMnuOk As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuCancelar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuSalir As System.Windows.Forms.ToolStripButton
    Private _Tag As String
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cmdBuscaClaveTPS As System.Windows.Forms.Button
    Friend WithEvents txtidTipoServicio As System.Windows.Forms.TextBox
    Friend WithEvents txtNomTipoServicio As System.Windows.Forms.TextBox
    Dim bEsIdentidad As Boolean = True
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtCadaKms As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtCadaTiempoDias As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtCosto As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtDescripcion As System.Windows.Forms.TextBox
    Friend WithEvents cmbEstatus As System.Windows.Forms.ComboBox
    Dim Salida As Boolean
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents cmdBuscaClaveDiv As System.Windows.Forms.Button
    Friend WithEvents txtidDivision As System.Windows.Forms.TextBox
    Friend WithEvents txtNomDivision As System.Windows.Forms.TextBox
    'Dim DsCombo As New DataTable
    Dim DsCombo As New DataSet
    Dim Division As DivisionClass

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents GrupoCaptura As System.Windows.Forms.GroupBox
    Friend WithEvents ToolStripMenu As System.Windows.Forms.ToolStrip
    Friend WithEvents txtMenuBusca As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents btnMnuAltas As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuModificar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuEliminar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuReporte As System.Windows.Forms.ToolStripButton
    Friend WithEvents label1 As System.Windows.Forms.Label
    Friend WithEvents label2 As System.Windows.Forms.Label
    Friend WithEvents cmdBuscaClave As System.Windows.Forms.Button
    Friend WithEvents txtidServicio As System.Windows.Forms.TextBox
    Friend WithEvents txtNomServicio As System.Windows.Forms.TextBox
    Private con As String
    Private v As Boolean

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCatServicios))
        Me.txtidServicio = New System.Windows.Forms.TextBox()
        Me.label1 = New System.Windows.Forms.Label()
        Me.txtNomServicio = New System.Windows.Forms.TextBox()
        Me.label2 = New System.Windows.Forms.Label()
        Me.GrupoCaptura = New System.Windows.Forms.GroupBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.cmdBuscaClaveDiv = New System.Windows.Forms.Button()
        Me.txtidDivision = New System.Windows.Forms.TextBox()
        Me.txtNomDivision = New System.Windows.Forms.TextBox()
        Me.cmbEstatus = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtCosto = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtDescripcion = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtCadaTiempoDias = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtCadaKms = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cmdBuscaClaveTPS = New System.Windows.Forms.Button()
        Me.txtidTipoServicio = New System.Windows.Forms.TextBox()
        Me.txtNomTipoServicio = New System.Windows.Forms.TextBox()
        Me.cmdBuscaClave = New System.Windows.Forms.Button()
        Me.ToolStripMenu = New System.Windows.Forms.ToolStrip()
        Me.txtMenuBusca = New System.Windows.Forms.ToolStripTextBox()
        Me.btnMnuAltas = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuModificar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuEliminar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuReporte = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuOk = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuCancelar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuSalir = New System.Windows.Forms.ToolStripButton()
        Me.MeStatus1 = New ControlStatus.MeStatus()
        Me.GridTodos = New System.Windows.Forms.DataGridView()
        Me.GrupoCaptura.SuspendLayout()
        Me.ToolStripMenu.SuspendLayout()
        CType(Me.GridTodos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtidServicio
        '
        Me.txtidServicio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtidServicio.Location = New System.Drawing.Point(122, 24)
        Me.txtidServicio.MaxLength = 3
        Me.txtidServicio.Name = "txtidServicio"
        Me.txtidServicio.Size = New System.Drawing.Size(69, 20)
        Me.txtidServicio.TabIndex = 4
        '
        'label1
        '
        Me.label1.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.label1.Location = New System.Drawing.Point(6, 27)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(94, 20)
        Me.label1.TabIndex = 14
        Me.label1.Text = "id Servicio:"
        '
        'txtNomServicio
        '
        Me.txtNomServicio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNomServicio.Location = New System.Drawing.Point(119, 93)
        Me.txtNomServicio.MaxLength = 50
        Me.txtNomServicio.Name = "txtNomServicio"
        Me.txtNomServicio.Size = New System.Drawing.Size(322, 20)
        Me.txtNomServicio.TabIndex = 9
        '
        'label2
        '
        Me.label2.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.label2.Location = New System.Drawing.Point(6, 96)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(91, 20)
        Me.label2.TabIndex = 16
        Me.label2.Text = "Nombre:"
        '
        'GrupoCaptura
        '
        Me.GrupoCaptura.Controls.Add(Me.Label9)
        Me.GrupoCaptura.Controls.Add(Me.cmdBuscaClaveDiv)
        Me.GrupoCaptura.Controls.Add(Me.txtidDivision)
        Me.GrupoCaptura.Controls.Add(Me.txtNomDivision)
        Me.GrupoCaptura.Controls.Add(Me.cmbEstatus)
        Me.GrupoCaptura.Controls.Add(Me.Label8)
        Me.GrupoCaptura.Controls.Add(Me.Label7)
        Me.GrupoCaptura.Controls.Add(Me.txtCosto)
        Me.GrupoCaptura.Controls.Add(Me.Label4)
        Me.GrupoCaptura.Controls.Add(Me.txtDescripcion)
        Me.GrupoCaptura.Controls.Add(Me.Label6)
        Me.GrupoCaptura.Controls.Add(Me.txtCadaTiempoDias)
        Me.GrupoCaptura.Controls.Add(Me.Label5)
        Me.GrupoCaptura.Controls.Add(Me.txtCadaKms)
        Me.GrupoCaptura.Controls.Add(Me.Label3)
        Me.GrupoCaptura.Controls.Add(Me.cmdBuscaClaveTPS)
        Me.GrupoCaptura.Controls.Add(Me.txtidTipoServicio)
        Me.GrupoCaptura.Controls.Add(Me.txtNomTipoServicio)
        Me.GrupoCaptura.Controls.Add(Me.label1)
        Me.GrupoCaptura.Controls.Add(Me.label2)
        Me.GrupoCaptura.Controls.Add(Me.cmdBuscaClave)
        Me.GrupoCaptura.Controls.Add(Me.txtidServicio)
        Me.GrupoCaptura.Controls.Add(Me.txtNomServicio)
        Me.GrupoCaptura.Location = New System.Drawing.Point(168, 45)
        Me.GrupoCaptura.Name = "GrupoCaptura"
        Me.GrupoCaptura.Size = New System.Drawing.Size(524, 292)
        Me.GrupoCaptura.TabIndex = 3
        Me.GrupoCaptura.TabStop = False
        '
        'Label9
        '
        Me.Label9.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label9.Location = New System.Drawing.Point(6, 263)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(94, 20)
        Me.Label9.TabIndex = 40
        Me.Label9.Text = "Divisi�n:"
        '
        'cmdBuscaClaveDiv
        '
        Me.cmdBuscaClaveDiv.Image = CType(resources.GetObject("cmdBuscaClaveDiv.Image"), System.Drawing.Image)
        Me.cmdBuscaClaveDiv.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdBuscaClaveDiv.Location = New System.Drawing.Point(199, 252)
        Me.cmdBuscaClaveDiv.Name = "cmdBuscaClaveDiv"
        Me.cmdBuscaClaveDiv.Size = New System.Drawing.Size(32, 32)
        Me.cmdBuscaClaveDiv.TabIndex = 16
        Me.cmdBuscaClaveDiv.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'txtidDivision
        '
        Me.txtidDivision.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtidDivision.Location = New System.Drawing.Point(122, 260)
        Me.txtidDivision.MaxLength = 3
        Me.txtidDivision.Name = "txtidDivision"
        Me.txtidDivision.Size = New System.Drawing.Size(69, 20)
        Me.txtidDivision.TabIndex = 15
        '
        'txtNomDivision
        '
        Me.txtNomDivision.Enabled = False
        Me.txtNomDivision.Location = New System.Drawing.Point(237, 264)
        Me.txtNomDivision.Name = "txtNomDivision"
        Me.txtNomDivision.ReadOnly = True
        Me.txtNomDivision.Size = New System.Drawing.Size(275, 20)
        Me.txtNomDivision.TabIndex = 17
        '
        'cmbEstatus
        '
        Me.cmbEstatus.AutoCompleteCustomSource.AddRange(New String() {"Regularizaci�n", "Vivienda"})
        Me.cmbEstatus.FormattingEnabled = True
        Me.cmbEstatus.Items.AddRange(New Object() {"ACTIVO", "INACTIVO"})
        Me.cmbEstatus.Location = New System.Drawing.Point(119, 228)
        Me.cmbEstatus.Name = "cmbEstatus"
        Me.cmbEstatus.Size = New System.Drawing.Size(112, 21)
        Me.cmbEstatus.TabIndex = 14
        '
        'Label8
        '
        Me.Label8.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label8.Location = New System.Drawing.Point(3, 231)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(94, 20)
        Me.Label8.TabIndex = 36
        Me.Label8.Text = "Estatus:"
        '
        'Label7
        '
        Me.Label7.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label7.Location = New System.Drawing.Point(3, 205)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(94, 20)
        Me.Label7.TabIndex = 34
        Me.Label7.Text = "Costo Mano Obra:"
        '
        'txtCosto
        '
        Me.txtCosto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCosto.Location = New System.Drawing.Point(119, 202)
        Me.txtCosto.MaxLength = 6
        Me.txtCosto.Name = "txtCosto"
        Me.txtCosto.Size = New System.Drawing.Size(109, 20)
        Me.txtCosto.TabIndex = 13
        Me.txtCosto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label4
        '
        Me.Label4.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label4.Location = New System.Drawing.Point(6, 122)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(91, 20)
        Me.Label4.TabIndex = 32
        Me.Label4.Text = "Descripci�n:"
        '
        'txtDescripcion
        '
        Me.txtDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDescripcion.Location = New System.Drawing.Point(119, 119)
        Me.txtDescripcion.MaxLength = 250
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(393, 20)
        Me.txtDescripcion.TabIndex = 10
        '
        'Label6
        '
        Me.Label6.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label6.Location = New System.Drawing.Point(3, 174)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(94, 20)
        Me.Label6.TabIndex = 30
        Me.Label6.Text = "Cada Dias:"
        '
        'txtCadaTiempoDias
        '
        Me.txtCadaTiempoDias.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCadaTiempoDias.Location = New System.Drawing.Point(119, 171)
        Me.txtCadaTiempoDias.MaxLength = 6
        Me.txtCadaTiempoDias.Name = "txtCadaTiempoDias"
        Me.txtCadaTiempoDias.Size = New System.Drawing.Size(109, 20)
        Me.txtCadaTiempoDias.TabIndex = 12
        Me.txtCadaTiempoDias.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label5
        '
        Me.Label5.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label5.Location = New System.Drawing.Point(3, 148)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(110, 20)
        Me.Label5.TabIndex = 26
        Me.Label5.Text = "Cada Kms:"
        '
        'txtCadaKms
        '
        Me.txtCadaKms.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCadaKms.Location = New System.Drawing.Point(119, 145)
        Me.txtCadaKms.MaxLength = 6
        Me.txtCadaKms.Name = "txtCadaKms"
        Me.txtCadaKms.Size = New System.Drawing.Size(109, 20)
        Me.txtCadaKms.TabIndex = 11
        Me.txtCadaKms.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label3
        '
        Me.Label3.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label3.Location = New System.Drawing.Point(6, 66)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(94, 20)
        Me.Label3.TabIndex = 20
        Me.Label3.Text = "Tipo Serv.:"
        '
        'cmdBuscaClaveTPS
        '
        Me.cmdBuscaClaveTPS.Image = CType(resources.GetObject("cmdBuscaClaveTPS.Image"), System.Drawing.Image)
        Me.cmdBuscaClaveTPS.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdBuscaClaveTPS.Location = New System.Drawing.Point(199, 55)
        Me.cmdBuscaClaveTPS.Name = "cmdBuscaClaveTPS"
        Me.cmdBuscaClaveTPS.Size = New System.Drawing.Size(32, 32)
        Me.cmdBuscaClaveTPS.TabIndex = 7
        Me.cmdBuscaClaveTPS.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'txtidTipoServicio
        '
        Me.txtidTipoServicio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtidTipoServicio.Location = New System.Drawing.Point(122, 63)
        Me.txtidTipoServicio.MaxLength = 3
        Me.txtidTipoServicio.Name = "txtidTipoServicio"
        Me.txtidTipoServicio.Size = New System.Drawing.Size(69, 20)
        Me.txtidTipoServicio.TabIndex = 6
        '
        'txtNomTipoServicio
        '
        Me.txtNomTipoServicio.Enabled = False
        Me.txtNomTipoServicio.Location = New System.Drawing.Point(237, 67)
        Me.txtNomTipoServicio.Name = "txtNomTipoServicio"
        Me.txtNomTipoServicio.ReadOnly = True
        Me.txtNomTipoServicio.Size = New System.Drawing.Size(275, 20)
        Me.txtNomTipoServicio.TabIndex = 8
        '
        'cmdBuscaClave
        '
        Me.cmdBuscaClave.Image = CType(resources.GetObject("cmdBuscaClave.Image"), System.Drawing.Image)
        Me.cmdBuscaClave.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdBuscaClave.Location = New System.Drawing.Point(199, 16)
        Me.cmdBuscaClave.Name = "cmdBuscaClave"
        Me.cmdBuscaClave.Size = New System.Drawing.Size(32, 32)
        Me.cmdBuscaClave.TabIndex = 5
        Me.cmdBuscaClave.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'ToolStripMenu
        '
        Me.ToolStripMenu.AutoSize = False
        Me.ToolStripMenu.BackColor = System.Drawing.Color.Silver
        Me.ToolStripMenu.Dock = System.Windows.Forms.DockStyle.None
        Me.ToolStripMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.txtMenuBusca, Me.btnMnuAltas, Me.btnMnuModificar, Me.btnMnuEliminar, Me.btnMnuReporte, Me.btnMnuOk, Me.btnMnuCancelar, Me.btnMnuSalir})
        Me.ToolStripMenu.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow
        Me.ToolStripMenu.Location = New System.Drawing.Point(3, 0)
        Me.ToolStripMenu.Name = "ToolStripMenu"
        Me.ToolStripMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.ToolStripMenu.Size = New System.Drawing.Size(730, 42)
        Me.ToolStripMenu.TabIndex = 1
        Me.ToolStripMenu.Text = "ToolStrip1"
        '
        'txtMenuBusca
        '
        Me.txtMenuBusca.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.txtMenuBusca.Name = "txtMenuBusca"
        Me.txtMenuBusca.Size = New System.Drawing.Size(150, 42)
        Me.txtMenuBusca.ToolTipText = "Filtra Listado"
        '
        'btnMnuAltas
        '
        Me.btnMnuAltas.ForeColor = System.Drawing.Color.Red
        Me.btnMnuAltas.Image = CType(resources.GetObject("btnMnuAltas.Image"), System.Drawing.Image)
        Me.btnMnuAltas.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuAltas.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuAltas.Name = "btnMnuAltas"
        Me.btnMnuAltas.Size = New System.Drawing.Size(44, 39)
        Me.btnMnuAltas.Text = "&ALTAS"
        Me.btnMnuAltas.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnMnuAltas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuModificar
        '
        Me.btnMnuModificar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuModificar.Image = CType(resources.GetObject("btnMnuModificar.Image"), System.Drawing.Image)
        Me.btnMnuModificar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuModificar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuModificar.Name = "btnMnuModificar"
        Me.btnMnuModificar.Size = New System.Drawing.Size(74, 39)
        Me.btnMnuModificar.Text = "&MODIFICAR"
        Me.btnMnuModificar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnMnuModificar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuEliminar
        '
        Me.btnMnuEliminar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuEliminar.Image = CType(resources.GetObject("btnMnuEliminar.Image"), System.Drawing.Image)
        Me.btnMnuEliminar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuEliminar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuEliminar.Name = "btnMnuEliminar"
        Me.btnMnuEliminar.Size = New System.Drawing.Size(64, 39)
        Me.btnMnuEliminar.Text = "&ELIMINAR"
        Me.btnMnuEliminar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuReporte
        '
        Me.btnMnuReporte.ForeColor = System.Drawing.Color.Red
        Me.btnMnuReporte.Image = CType(resources.GetObject("btnMnuReporte.Image"), System.Drawing.Image)
        Me.btnMnuReporte.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuReporte.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuReporte.Name = "btnMnuReporte"
        Me.btnMnuReporte.Size = New System.Drawing.Size(59, 39)
        Me.btnMnuReporte.Text = "&REPORTE"
        Me.btnMnuReporte.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuOk
        '
        Me.btnMnuOk.ForeColor = System.Drawing.Color.Red
        Me.btnMnuOk.Image = CType(resources.GetObject("btnMnuOk.Image"), System.Drawing.Image)
        Me.btnMnuOk.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuOk.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuOk.Name = "btnMnuOk"
        Me.btnMnuOk.Size = New System.Drawing.Size(56, 39)
        Me.btnMnuOk.Text = "&GRABAR"
        Me.btnMnuOk.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuCancelar
        '
        Me.btnMnuCancelar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuCancelar.Image = CType(resources.GetObject("btnMnuCancelar.Image"), System.Drawing.Image)
        Me.btnMnuCancelar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuCancelar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuCancelar.Name = "btnMnuCancelar"
        Me.btnMnuCancelar.Size = New System.Drawing.Size(71, 39)
        Me.btnMnuCancelar.Text = "&CANCELAR"
        Me.btnMnuCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuSalir
        '
        Me.btnMnuSalir.ForeColor = System.Drawing.Color.Red
        Me.btnMnuSalir.Image = CType(resources.GetObject("btnMnuSalir.Image"), System.Drawing.Image)
        Me.btnMnuSalir.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuSalir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuSalir.Name = "btnMnuSalir"
        Me.btnMnuSalir.Size = New System.Drawing.Size(41, 39)
        Me.btnMnuSalir.Text = "&SALIR"
        Me.btnMnuSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'MeStatus1
        '
        Me.MeStatus1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.MeStatus1.Location = New System.Drawing.Point(0, 371)
        Me.MeStatus1.Margin = New System.Windows.Forms.Padding(4)
        Me.MeStatus1.Name = "MeStatus1"
        Me.MeStatus1.Size = New System.Drawing.Size(910, 26)
        Me.MeStatus1.TabIndex = 72
        '
        'GridTodos
        '
        Me.GridTodos.AllowUserToAddRows = False
        Me.GridTodos.AllowUserToDeleteRows = False
        Me.GridTodos.AllowUserToResizeRows = False
        Me.GridTodos.BackgroundColor = System.Drawing.Color.White
        Me.GridTodos.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.GridTodos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.GridTodos.Location = New System.Drawing.Point(3, 45)
        Me.GridTodos.MultiSelect = False
        Me.GridTodos.Name = "GridTodos"
        Me.GridTodos.ReadOnly = True
        Me.GridTodos.RowHeadersVisible = False
        Me.GridTodos.Size = New System.Drawing.Size(162, 259)
        Me.GridTodos.TabIndex = 73
        '
        'frmCatServicios
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(910, 397)
        Me.Controls.Add(Me.GridTodos)
        Me.Controls.Add(Me.MeStatus1)
        Me.Controls.Add(Me.ToolStripMenu)
        Me.Controls.Add(Me.GrupoCaptura)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmCatServicios"
        Me.Text = "Catal�go de Servicios"
        Me.GrupoCaptura.ResumeLayout(False)
        Me.GrupoCaptura.PerformLayout()
        Me.ToolStripMenu.ResumeLayout(False)
        Me.ToolStripMenu.PerformLayout()
        CType(Me.GridTodos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region
    Sub New(ByVal vTag As String)
        InitializeComponent()
        _Tag = vTag
    End Sub

    Public Sub New(con As String, v As Boolean)
        Me.con = con
        BD = New CapaDatos.UtilSQL(con, "Juan")
        InitializeComponent()
        Me.v = v
    End Sub
#Region "MenusToolStrip"
    Private Sub LimpiaVariables()
        SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")

    End Sub
    Private Sub LimpiaCampos()
        txtidServicio.Text = ""
        txtidTipoServicio.Text = ""
        txtNomServicio.Text = ""
        txtNomTipoServicio.Text = ""
        txtCadaKms.Text = ""
        txtCadaTiempoDias.Text = ""
        txtDescripcion.Text = ""
        cmbEstatus.SelectedIndex = 0
        txtCosto.Text = ""
        txtidDivision.Text = ""
        txtNomDivision.Text = ""
        LimpiaVariables()
    End Sub

    Private Sub ActivaCampos(ByVal valor As Boolean, ByVal vOpcion As TipoOpcActivaCampos)
        If vOpcion = TipoOpcActivaCampos.tOpcDESHABTODOS Then
            txtidServicio.Enabled = False
            txtidTipoServicio.Enabled = False
            txtNomTipoServicio.Enabled = False
            txtNomServicio.Enabled = False
            txtDescripcion.Enabled = False
            txtCadaKms.Enabled = False
            txtCadaTiempoDias.Enabled = False
            cmbEstatus.Enabled = False
            txtCosto.Enabled = False
            txtidDivision.Enabled = False
            txtNomDivision.Enabled = False
        ElseIf vOpcion = TipoOpcActivaCampos.tOpcINICIALIZA Then
            txtidServicio.Enabled = Not valor
            txtidTipoServicio.Enabled = valor
            txtNomTipoServicio.Enabled = valor
            txtNomServicio.Enabled = valor
            txtDescripcion.Enabled = valor
            txtCadaKms.Enabled = valor
            txtCadaTiempoDias.Enabled = valor
            cmbEstatus.Enabled = valor
            txtCosto.Enabled = valor
            txtidDivision.Enabled = valor
            txtNomDivision.Enabled = valor
        End If
    End Sub


    Private Sub ActivaBotones(ByVal valor As Boolean, Optional ByVal vOpcion As TipoOpcActivaBoton = TipoOpcActivaBoton.tOpcInicializa)
        If vOpcion = TipoOpcActivaBoton.tOpcConsulta Then
            btnMnuOk.Enabled = False
            btnMnuCancelar.Enabled = valor
            btnMnuSalir.Enabled = Not valor

            btnMnuAltas.Enabled = Not valor
            btnMnuModificar.Enabled = Not valor
            btnMnuEliminar.Enabled = Not valor
            btnMnuReporte.Enabled = Not valor
        ElseIf vOpcion = TipoOpcActivaBoton.tOpcInicializa Then
            btnMnuOk.Enabled = valor
            btnMnuCancelar.Enabled = valor
            btnMnuSalir.Enabled = Not valor

            btnMnuAltas.Enabled = Not valor
            btnMnuModificar.Enabled = Not valor
            btnMnuEliminar.Enabled = Not valor
            btnMnuReporte.Enabled = Not valor

            txtMenuBusca.Enabled = Not valor
            GridTodos.Enabled = Not valor
        ElseIf vOpcion = TipoOpcActivaBoton.tOpcInsertar Then
            btnMnuOk.Enabled = valor
            btnMnuCancelar.Enabled = valor
            btnMnuSalir.Enabled = Not valor

            btnMnuAltas.Enabled = Not valor
            btnMnuModificar.Enabled = Not valor
            btnMnuEliminar.Enabled = Not valor
            btnMnuReporte.Enabled = Not valor
        End If
    End Sub

    Private Sub btnMnuAltas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuAltas.Click
        'ALTAS
        '15/FEB/2016
        'If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Alta) = False Then
        '    MessageBox.Show("No tiene los permisos Necesarios para poder dar de Alta en el Catalogo de Servicios!!!",
        '        "Acceso Denegado a Alta de Servicios", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    OpcionForma = TipoOpcionForma.tOpcConsultar
        '    Exit Sub
        'End If
        OpcionForma = TipoOpcionForma.tOpcInsertar
        LimpiaCampos()
        ActivaCampos(True, TipoOpcActivaCampos.tOpcINICIALIZA)
        ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)

        If bEsIdentidad Then
            txtidServicio.Enabled = False
            txtidTipoServicio.Focus()
            txtidTipoServicio.SelectAll()
        Else
            txtidServicio.Focus()
        End If
        Status("Ingrese nuevo Servicio para guardar", Me)
    End Sub
    Private Sub btnMnuModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuModificar.Click
        'MODIFICAR
        'If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Modificar) = False Then
        '    MessageBox.Show("No tiene los permisos Necesarios para poder Modificar la Informacion del Servicio!!!",
        '           "Acceso Denegado a Modificacion de la Informacion del Servicio", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    OpcionForma = TipoOpcionForma.tOpcConsultar
        '    Exit Sub
        'End If
        If Trim(txtidServicio.Text) = "" Then
            txtidServicio.Text = DespliegaGrid(TablaBd, Inicio.CONSTR, CampoLLave)
            CargaForm(txtidServicio.Text, UCase(TablaBd))
        End If

        OpcionForma = TipoOpcionForma.tOpcModificar
        ActivaCampos(True, TipoOpcActivaCampos.tOpcINICIALIZA)
        ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)

        btnMnuEliminar.Enabled = True
        If bEsIdentidad Then
            txtidServicio.Enabled = False
            txtNomServicio.Focus()
        Else
            txtidServicio.Focus()
        End If

        Status("Ingrese Servicio para guardar", Me)
    End Sub
    Private Sub btnMnuEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuEliminar.Click
        'ELIMINAR
        'If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Baja) = False Then
        '    MessageBox.Show("No tiene los permisos Necesarios para poder Eliminar el Servicio!!!",
        '        "Acceso Denegado a Eliminacion de Servicio", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    OpcionForma = TipoOpcionForma.tOpcConsultar
        '    Exit Sub
        'End If
        If txtidServicio.Text = Servicio.idServicio Then
            With Servicio
                If MessageBox.Show("Esta completamente seguro que desea Eliminar el Servicio: " &
                                   .NomServicio & " Con Id: " & .idServicio & " ????", "Confirma que desea eliminar el Servicio: " & .NomServicio & "???",
                                 MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then Exit Sub
                Try
                    .Eliminar() 'Elimina el municipio
                    MessageBox.Show("Servicio: " & .NomServicio & " Eliminada con exito!!!",
                                    "Servicio Eliminado con exito", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Status("Servicio: " & .NomServicio & " Eliminado con exito!!!", Me)
                    ActualizaGrid(txtMenuBusca.Text)
                    txtidServicio.Text = ""
                    OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
                Catch ex As Exception
                    Status("Error al tratar de eliminar el Servicio: " & ex.Message, Me, Err:=True)
                    MessageBox.Show("Error al Tratar de eliminar el Servicio: " & .NomServicio & vbNewLine &
                                    "Error. " & ex.Message, "Error: " & Err.Number, MessageBoxButtons.OK, MessageBoxIcon.Error)

                End Try
            End With
        End If
    End Sub
    Private Sub btnMnuReporte_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuReporte.Click
        'REPORTE
        'If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Reporte) = False Then
        '    MessageBox.Show("No tiene los permisos Necesarios para poder realizar Reporte de Servicios!!!",
        '        "Acceso Denegado a Reporte de Servicios", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    OpcionForma = TipoOpcionForma.tOpcConsultar
        '    Exit Sub
        'End If
        ToolStripMenu.Enabled = False
        Status("Abriendo Reporte Espere. . . ", Me)
        Me.Cursor = Cursors.WaitCursor
        DespliegaReporte(TablaBd, CONSTR)
        Status("Finalizo el reporte. . . ", Me)
        Me.Cursor = Cursors.Default
        ToolStripMenu.Enabled = True
    End Sub

#End Region
    Private Sub ActualizaGrid(ByVal vNomServicio As String)
        If vNomServicio = "" Then
            GridTodos.DataSource = BD.ExecuteReturn("SELECT idServicio, NomServicio FROM " & TablaBd, TablaBd)
            Status(GridTodos.RowCount & " Registros", Me)
        Else
            GridTodos.DataSource = BD.ExecuteReturn("SELECT idServicio, NomServicio FROM " & TablaBd &
                                                " WHERE NomServicio Like '%" & vNomServicio & "%'")
            Status(GridTodos.RowCount & " Registros obtenidos con el filtro %" & vNomServicio & "%", Me)
        End If
    End Sub

    Private Sub OpcionMnuTools(ByVal vOpcion As TipoOpcionForma)
        If vOpcion <> TipoOpcionForma.tOpcImprimir And vOpcion <> TipoOpcionForma.tOpcEliminar Then
            OpcionForma = vOpcion
        End If
        txtMenuBusca.Enabled = True
        GridTodos.Enabled = True
        If vOpcion = TipoOpcionForma.tOpcConsultar Then
            'CONSULTA
            For Each ct As Control In GrupoCaptura.Controls
                If Not ct Is cmdBuscaClave Then
                    ct.Enabled = False
                Else
                    ct.Enabled = True
                End If
            Next
            txtMenuBusca.Enabled = True
            btnMnuAltas.Enabled = True
            btnMnuModificar.Enabled = True
            btnMnuEliminar.Enabled = False
            btnMnuReporte.Enabled = True
            btnMnuOk.Enabled = False
            btnMnuCancelar.Enabled = False

            btnMnuSalir.Enabled = True


        End If
    End Sub


    Private Sub txtMenuBusca_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtMenuBusca.KeyPress
        If Asc(e.KeyChar) = 39 Then e.Handled = True : Exit Sub 'Caracter ' Evita errores
        If Asc(e.KeyChar) = 13 Then
            GridTodos.Focus()
            e.Handled = True
        End If
    End Sub

    'Private Sub txtMenuBusca_KeyUp(sender As Object, e As KeyEventArgs) Handles txtMenuBusca.KeyUp
    '    If txtMenuBusca.Text.Trim = "" Then Exit Sub
    '    If e.KeyCode = Keys.Down Or e.KeyCode = Keys.Up Then
    '        GridTodos.Focus()
    '        e.Handled = True
    '        Exit Sub
    '    End If
    '    If e.KeyCode = Keys.Left Or e.KeyCode = Keys.Right Then Exit Sub
    '    ActualizaGrid(txtMenuBusca.Text)
    'End Sub



    Private Sub frmCatFamilias_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
        ActualizaGrid("")
        GridTodos.Columns(1).Width = GridTodos.Width - 20
        GridTodos.Columns(0).Visible = False
        txtMenuBusca.Focus()
        LlenaCombos()
    End Sub
    Private Sub GridTodos_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles GridTodos.CellEnter
        CargaForm(GridTodos.Rows(GridTodos.CurrentRow.Index).Cells(0).Value, UCase(TablaBd))
    End Sub
    Private Sub GridTodos_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles GridTodos.KeyPress
        If Asc(e.KeyChar) = 8 Then
            txtMenuBusca.Focus()
            e.Handled = True
        End If
    End Sub
    Private Sub CargaForm(ByVal idClave As Integer, ByVal NomTabla As String)
        Try
            Select Case UCase(NomTabla)
                Case UCase(TablaBd)
                    Servicio = New ServicioClass(idClave)
                    With Servicio
                        txtidServicio.Text = .idServicio
                        If .Existe Then
                            txtNomServicio.Text = .NomServicio
                            txtDescripcion.Text = .Descripcion
                            txtidTipoServicio.Text = .idTipoServicio
                            CargaForm(.idTipoServicio, TablaBd2)
                            txtCadaKms.Text = .CadaKms
                            txtCadaTiempoDias.Text = .CadaTiempoDias
                            txtCosto.Text = .Costo
                            cmbEstatus.SelectedValue = .Estatus
                            txtidDivision.Text = .idDivision
                            txtNomDivision.Text = .NomDivision

                            'txtCIDALMACEN_COM.Text = .CIDALMACEN_COM
                            If OpcionForma = TipoOpcionForma.tOpcInsertar Then
                                Status("EL Servicio Con id: " & .idServicio & _
                                       " Ya existe!!!, Favor de modificarlo para poder dar de alta a otro Servicio", Me, Err:=True)
                                MessageBox.Show("El Servicio con id: " & .idServicio & _
                                                " YA EXISTE!!!, Favor de Cambiarlo y volver a intentarlo",
                                                "El Servicio con id: " & .idServicio & " YA EXISTE!!!", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            End If
                        Else 'No existe 
                            If OpcionForma = TipoOpcionForma.tOpcInsertar Then

                                ActivaCampos(True, TipoOpcActivaCampos.tOpcINICIALIZA)
                                ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)
                                btnMnuOk.Enabled = True
                                txtNomServicio.Focus()
                                Status("Listo para Guardar nuevo Servicio con id: " & .idServicio, Me)
                            End If
                        End If
                    End With
                Case UCase(TablaBd2)
                    'aqui()
                    TipoServ = New TipoServClass(idClave)
                    With TipoServ
                        txtidTipoServicio.Text = .idTipoServicio
                        If .Existe Then
                            txtNomTipoServicio.Text = .NomTipoServicio
                            txtNomServicio.Focus()
                            txtNomServicio.SelectAll()
                        Else
                            MsgBox("El Tipo de Servicio con id: " & txtidTipoServicio.Text & " No Existe")
                            txtidTipoServicio.Focus()
                            txtidTipoServicio.SelectAll()
                        End If
                    End With
                Case UCase("Division")
                    Division = New DivisionClass(Val(idClave))
                    If Division.Existe Then
                        txtNomDivision.Text = Division.NomDivision
                    End If

            End Select



        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try

    End Sub
    Private Function Validar() As Boolean
        Validar = True
        If Not Inicio.ValidandoCampo(Trim(txtidServicio.Text), TipoDato.TdNumerico) Then
            If Not bEsIdentidad Then
                MsgBox(MENSAJE_CLAVE, vbInformation, "Aviso" & Me.Text)
                If txtidServicio.Enabled Then
                    txtidServicio.Focus()
                    txtidServicio.SelectAll()
                End If
                Validar = False
            End If
        ElseIf Not Inicio.ValidandoCampo(Trim(txtNomServicio.Text), TipoDato.TdCadena) Then
            MsgBox("El Nombre del Servicio No Puede Ser Vacio", vbInformation, "Aviso" & Me.Text)
            If txtNomServicio.Enabled Then
                txtNomServicio.Focus()
                txtNomServicio.SelectAll()
            End If
            Validar = False
        ElseIf Not Inicio.ValidandoCampo(Trim(txtidTipoServicio.Text), TipoDato.TdNumerico) Then
            MsgBox("El Tipo de Servicio no Puede ser Vacio", vbInformation, "Aviso" & Me.Text)
            If txtidTipoServicio.Enabled Then
                txtidTipoServicio.Focus()
                txtidTipoServicio.SelectAll()
            End If
            Validar = False
        End If
    End Function

    'Private Sub txtidAlmacen_EnabledChanged(sender As Object, e As EventArgs) Handles txtidAlmacen.EnabledChanged
    '    cmdBuscaClave.Enabled = txtidAlmacen.Enabled
    'End Sub
    'CONTROLES
    Private Sub txtidAlmacen_KeyDown(sender As Object, e As KeyEventArgs) Handles txtidServicio.KeyDown
        If e.KeyCode = Keys.F3 Then
            cmdBuscaClave_Click(sender, e)
        End If

    End Sub
    Private Sub txtidAlmacen_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtidServicio.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If txtidServicio.Text.Trim <> "" Then
                CargaForm(txtidServicio.Text, UCase(TablaBd))
            End If
            e.Handled = True
        End If
    End Sub
    Private Sub txtNomServicio_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtNomServicio.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            txtDescripcion.Focus()
            txtDescripcion.SelectAll()
        End If
    End Sub

    Private Sub cmdBuscaClave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBuscaClave.Click
        Dim idEst As String = DespliegaGrid(UCase(TablaBd), Inicio.CONSTR, CampoLLave)
        If idEst <> "" Then CargaForm(idEst, UCase(TablaBd))
    End Sub

    Private Sub txtNomServicio_MouseDown(sender As Object, e As MouseEventArgs) Handles txtNomServicio.MouseDown
        txtNomServicio.Focus()
        txtNomServicio.SelectAll()
    End Sub

    Private Sub txtidSucursal_EnabledChanged(sender As Object, e As EventArgs) Handles txtidTipoServicio.EnabledChanged
        cmdBuscaClaveTPS.Enabled = txtidTipoServicio.Enabled
    End Sub

    Private Sub txtidSucursal_KeyDown(sender As Object, e As KeyEventArgs) Handles txtidTipoServicio.KeyDown
        If e.KeyCode = Keys.F3 Then
            'cmdBuscaClaveDiv_Click(sender, e)
            cmdBuscaClaveTPS_Click(sender, e)
        End If
    End Sub

    Private Sub txtidSucursal_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtidTipoServicio.KeyPress
        If txtidTipoServicio.Text.Length = 0 Then
            txtidTipoServicio.Focus()
            txtidTipoServicio.SelectAll()
        Else
            If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
                Salida = True
                CargaForm(txtidTipoServicio.Text, TablaBd2)

                'txtNomServicio.Focus()
                'txtNomServicio.SelectAll()
                Salida = False
            End If
        End If
    End Sub

    Private Sub txtidSucursal_Leave(sender As Object, e As EventArgs) Handles txtidTipoServicio.Leave
        If Salida Then Exit Sub
        If txtidTipoServicio.Text.Length = 0 Then
            txtidTipoServicio.Focus()
            txtidTipoServicio.SelectAll()
        Else
            If txtidTipoServicio.Enabled Then
                CargaForm(txtidTipoServicio.Text, UCase(TablaBd2))

            End If
        End If
    End Sub

    Private Sub txtidSucursal_MouseDown(sender As Object, e As MouseEventArgs) Handles txtidTipoServicio.MouseDown
        txtidTipoServicio.Focus()
        txtidTipoServicio.SelectAll()
    End Sub

    Private Sub cmdBuscaClaveTPS_Click(sender As Object, e As EventArgs) Handles cmdBuscaClaveTPS.Click
        txtidTipoServicio.Text = DespliegaGrid(UCase(TablaBd2), Inicio.CONSTR, CampoLLave)
        txtidSucursal_Leave(sender, e)
    End Sub

    Private Sub cmdCancelar_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        With TryCast(sender, Button)
            If .Enabled Then
                Me.CancelButton = sender
            End If
        End With
    End Sub
    Private Sub cmdTerminar_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        With TryCast(sender, Button)
            If .Enabled Then
                Me.CancelButton = sender
            End If
        End With
    End Sub

    Private Sub btnMnuOk_Click(sender As Object, e As EventArgs) Handles btnMnuOk.Click
        Dim CadCam As String = ""
        Try
            Salida = True
            If Not Validar() Then Exit Sub
            If GridTodos.RowCount = 0 Then
                Servicio = New ServicioClass(0)
            End If
            With Servicio
                If OpcionForma = TipoOpcionForma.tOpcModificar Then
                    CadCam = .GetCambios(txtidTipoServicio.Text, txtNomServicio.Text, txtDescripcion.Text, Val(txtCadaKms.Text), Val(txtCadaTiempoDias.Text), cmbEstatus.SelectedValue, Val(txtCosto.Text), txtidDivision.Text)
                    If CadCam <> "" Then
                        'If MessageBox.Show("Esta completamente Seguro que Desea guardar los Cambios Realizados al Estado Con id: " & .IDMARCA & vbNewLine &
                        '                CadCam, "Confirma que desea guardar los Cambios Realizados???", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then Exit Sub
                        Status("Guardando Modificaciones del Servicio Con id: " & .idServicio, Me, 1, 2)

                        .Guardar(bEsIdentidad, txtidTipoServicio.Text, txtNomServicio.Text, txtDescripcion.Text, Val(txtCadaKms.Text), Val(txtCadaTiempoDias.Text), cmbEstatus.SelectedValue, Val(txtCosto.Text), txtidDivision.Text)
                        Status("Servicio con Id: " & .idServicio & " Modificado Con Exito!!!", Me, 2, 2)
                        MessageBox.Show("Servicio con Id: " & .idServicio & " Modificada con exito!!!",
                                        "Accion terminada con exito!!!", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
                        ActualizaGrid(txtMenuBusca.Text)
                    Else
                        Status("No se detectaron cambios para realizar!!!", Me, Err:=True)
                    End If
                ElseIf OpcionForma = TipoOpcionForma.tOpcInsertar Then
                    .Existe = False
                    .NomServicio = txtNomServicio.Text
                    If Not bEsIdentidad Then
                        .idServicio = txtidServicio.Text
                    End If
                    Status("Guardando Nuevo Servicio: " & txtNomServicio.Text & ". . .", Me, 1, 2)
                    .Guardar(bEsIdentidad, txtidTipoServicio.Text, txtNomServicio.Text, txtDescripcion.Text, Val(txtCadaKms.Text), Val(txtCadaTiempoDias.Text), cmbEstatus.SelectedValue, Val(txtCosto.Text), txtidDivision.Text)
                    Status("Servicio: " & txtNomServicio.Text & " Guardado con Exito!!!", Me, 2, 2)
                    MessageBox.Show("Nuevo Servicio: " & txtNomServicio.Text & " Guardado con exito!!!",
                                    "Accion terminada con exito!!!", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
                    ActualizaGrid(txtMenuBusca.Text)

                End If
            End With
            Salida = False
        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try
    End Sub


    Private Sub btnMnuCancelar_Click(sender As Object, e As EventArgs) Handles btnMnuCancelar.Click
        If txtidServicio.Text <> "" Then
            CargaForm(txtidServicio.Text, UCase(TablaBd))
        End If
        OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
    End Sub

    Private Sub btnMnuSalir_Click(sender As Object, e As EventArgs) Handles btnMnuSalir.Click
        Me.Close()
    End Sub

    Private Sub txtMenuBusca_TextChanged(sender As Object, e As EventArgs) Handles txtMenuBusca.TextChanged
        ActualizaGrid(txtMenuBusca.Text)
    End Sub

    Private Sub txtCP_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCadaKms.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            txtCadaTiempoDias.Focus()
            txtCadaTiempoDias.SelectAll()
        End If
    End Sub

    Private Sub txtCP_MouseDown(sender As Object, e As MouseEventArgs) Handles txtCadaKms.MouseDown
        txtCadaKms.Focus()
        txtCadaKms.SelectAll()
    End Sub

    Private Sub txtDuracionHoras_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCadaTiempoDias.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            txtCosto.Focus()
            txtCosto.SelectAll()
        End If
    End Sub

    Private Sub txtDuracionHoras_MouseDown(sender As Object, e As MouseEventArgs) Handles txtCadaTiempoDias.MouseDown
        txtCadaTiempoDias.Focus()
        txtCadaTiempoDias.SelectAll()
    End Sub

    Private Sub GrupoCaptura_Enter(sender As Object, e As EventArgs) Handles GrupoCaptura.Enter

    End Sub

    Private Function LlenaCombos() As Boolean
        Dim StrSql As String = ""
        If Inicio.CONSTR = "" Then
            Inicio.CONSTR = Me.con
        End If
        LlenaCombos = True
        Try
            DsCombo.Clear()
            StrSql = "select 'ACT' as IdEstatus, 'ACTIVO' as Estatus " & _
            "union " & _
            "select 'INA' as IdEstatus, 'INACTIVO' as Estatus"

            DsCombo = Inicio.LlenaCombos("IdEstatus", "Estatus", "CatEstatus", _
                               TipoConexion.TcSQL, Inicio.CONSTR, Process.GetCurrentProcess.ProcessName, _
                               KEY_CHECAERROR, KEY_ESTATUS, , StrSql)
            'DsCombo = BD.ExecuteReturn(StrSql)
            If Not DsCombo Is Nothing Then
                If DsCombo.Tables("CatEstatus").DefaultView.Count > 0 Then
                    cmbEstatus.DataSource = DsCombo.Tables("CatEstatus")
                    cmbEstatus.ValueMember = "IdEstatus"
                    cmbEstatus.DisplayMember = "Estatus"
                End If
            Else
                MsgBox("No se han dado de alta a Estatus ", vbInformation, "Aviso" & Me.Text)
                LlenaCombos = False
                Exit Function
            End If
        Catch ex As Exception

        End Try
    End Function

    Private Sub txtDescripcion_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtDescripcion.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            txtCadaKms.Focus()
            txtCadaKms.SelectAll()
        End If
    End Sub

    Private Sub txtDescripcion_MouseDown(sender As Object, e As MouseEventArgs) Handles txtDescripcion.MouseDown
        txtDescripcion.Focus()
        txtDescripcion.SelectAll()
    End Sub

    Private Sub txtCosto_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCosto.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            cmbEstatus.Focus()
        End If
    End Sub

    Private Sub txtCosto_MouseDown(sender As Object, e As MouseEventArgs) Handles txtCosto.MouseDown
        txtCosto.Focus()
        txtCosto.SelectAll()
    End Sub

    Private Sub cmbEstatus_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbEstatus.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            txtidDivision.Focus()
            txtidDivision.SelectAll()
            'btnMnuOk_Click(sender, e)
        End If
    End Sub

    Private Sub txtidTipoServicio_TextChanged(sender As Object, e As EventArgs) Handles txtidTipoServicio.TextChanged

    End Sub

    Private Sub cmbEstatus_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbEstatus.SelectedIndexChanged

    End Sub

    Private Sub txtidDivision_EnabledChanged(sender As Object, e As EventArgs) Handles txtidDivision.EnabledChanged
        cmdBuscaClaveDiv.Enabled = txtidDivision.Enabled
    End Sub

    Private Sub txtidDivision_KeyDown(sender As Object, e As KeyEventArgs) Handles txtidDivision.KeyDown
        If e.KeyCode = Keys.F3 Then
            cmdBuscaClaveDiv_Click(sender, e)
        End If
    End Sub

    Private Sub txtidDivision_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtidDivision.KeyPress
        If txtidDivision.Text.Length = 0 Then
            txtidDivision.Focus()
            txtidDivision.SelectAll()
        Else
            If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
                Salida = True
                CargaForm(txtidDivision.Text, UCase("Division"))
                btnMnuOk_Click(sender, e)

                'txtNomServicio.Focus()
                'txtNomServicio.SelectAll()
                Salida = False
            End If
        End If
    End Sub

    Private Sub txtidDivision_Leave(sender As Object, e As EventArgs) Handles txtidDivision.Leave
        If Salida Then Exit Sub
        If txtidDivision.Text.Length = 0 Then
            txtidDivision.Focus()
            txtidDivision.SelectAll()
        Else
            If txtidTipoServicio.Enabled Then
                CargaForm(txtidDivision.Text, UCase("Division"))

            End If
        End If
    End Sub

    Private Sub txtidDivision_MouseDown(sender As Object, e As MouseEventArgs) Handles txtidDivision.MouseDown
        txtidDivision.Focus()
        txtidDivision.SelectAll()
    End Sub

    Private Sub txtidDivision_TextChanged(sender As Object, e As EventArgs) Handles txtidDivision.TextChanged

    End Sub

    Private Sub cmdBuscaClaveDiv_Click(sender As Object, e As EventArgs) Handles cmdBuscaClaveDiv.Click
        txtidDivision.Text = DespliegaGrid(UCase("Catdivision"), Inicio.CONSTR, CampoLLave)
        txtidDivision_Leave(sender, e)
    End Sub
End Class
