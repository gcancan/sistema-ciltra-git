'Fecha: 14 / Septiembre / 2016
'************************************************************************************************************************************
'*  Fecha Modificacion :   14 / Septiembre / 2016                                                                                       
'*  
'************************************************************************************************************************************


Public Class frmRelppROVTipoOrdSer
    Inherits System.Windows.Forms.Form
    Dim OpcionForma As TipoOpcionForma
    Dim TablaBd As String = "RelProvTipOrdSer"
    Dim CampoLLave1 As String = "idTipOrdServ"
    Dim CampoLLave2 As String = "idProveedor"
    Public WithEvents MeStatus1 As ControlStatus.MeStatus
    Friend WithEvents GridTodos As System.Windows.Forms.DataGridView
    'Private Depto As DeptosClass
    Private relacion As RelProvTipOrdSerClass
    Friend WithEvents btnMnuOk As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuCancelar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuSalir As System.Windows.Forms.ToolStripButton
    Private _Tag As String
    Friend WithEvents cmbidProveedor As ComboBox
    Friend WithEvents Label3 As Label
    Friend WithEvents cmbidTipOrdServ As ComboBox
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents chkActivo As CheckBox
    Dim bEsIdentidad As Boolean = False
    Dim DsCombo As New DataSet

    'Dim Marca As MarcaClass
    Dim Prov As ProveedorClass
    Dim tos As TipoServClass



#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents GrupoCaptura As System.Windows.Forms.GroupBox
    Friend WithEvents ToolStripMenu As System.Windows.Forms.ToolStrip
    Friend WithEvents txtMenuBusca As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents btnMnuAltas As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuModificar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuReporte As System.Windows.Forms.ToolStripButton
    Private con As String
    Private v As Boolean

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmRelppROVTipoOrdSer))
        Me.GrupoCaptura = New System.Windows.Forms.GroupBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.chkActivo = New System.Windows.Forms.CheckBox()
        Me.cmbidProveedor = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cmbidTipOrdServ = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.ToolStripMenu = New System.Windows.Forms.ToolStrip()
        Me.txtMenuBusca = New System.Windows.Forms.ToolStripTextBox()
        Me.btnMnuAltas = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuModificar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuReporte = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuOk = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuCancelar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuSalir = New System.Windows.Forms.ToolStripButton()
        Me.MeStatus1 = New ControlStatus.MeStatus()
        Me.GridTodos = New System.Windows.Forms.DataGridView()
        Me.GrupoCaptura.SuspendLayout()
        Me.ToolStripMenu.SuspendLayout()
        CType(Me.GridTodos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GrupoCaptura
        '
        Me.GrupoCaptura.Controls.Add(Me.Label4)
        Me.GrupoCaptura.Controls.Add(Me.chkActivo)
        Me.GrupoCaptura.Controls.Add(Me.cmbidProveedor)
        Me.GrupoCaptura.Controls.Add(Me.Label3)
        Me.GrupoCaptura.Controls.Add(Me.cmbidTipOrdServ)
        Me.GrupoCaptura.Controls.Add(Me.Label5)
        Me.GrupoCaptura.Location = New System.Drawing.Point(168, 45)
        Me.GrupoCaptura.Name = "GrupoCaptura"
        Me.GrupoCaptura.Size = New System.Drawing.Size(524, 222)
        Me.GrupoCaptura.TabIndex = 3
        Me.GrupoCaptura.TabStop = False
        '
        'Label4
        '
        Me.Label4.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label4.Location = New System.Drawing.Point(25, 92)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(94, 20)
        Me.Label4.TabIndex = 44
        Me.Label4.Text = "Activo:"
        '
        'chkActivo
        '
        Me.chkActivo.AutoSize = True
        Me.chkActivo.Location = New System.Drawing.Point(125, 92)
        Me.chkActivo.Name = "chkActivo"
        Me.chkActivo.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.chkActivo.Size = New System.Drawing.Size(15, 14)
        Me.chkActivo.TabIndex = 43
        Me.chkActivo.UseVisualStyleBackColor = True
        '
        'cmbidProveedor
        '
        Me.cmbidProveedor.AutoCompleteCustomSource.AddRange(New String() {"Regularizaci�n", "Vivienda"})
        Me.cmbidProveedor.FormattingEnabled = True
        Me.cmbidProveedor.Location = New System.Drawing.Point(125, 17)
        Me.cmbidProveedor.Name = "cmbidProveedor"
        Me.cmbidProveedor.Size = New System.Drawing.Size(199, 21)
        Me.cmbidProveedor.TabIndex = 41
        '
        'Label3
        '
        Me.Label3.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label3.Location = New System.Drawing.Point(22, 57)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(97, 20)
        Me.Label3.TabIndex = 42
        Me.Label3.Text = "Aplica En:"
        '
        'cmbidTipOrdServ
        '
        Me.cmbidTipOrdServ.AutoCompleteCustomSource.AddRange(New String() {"Regularizaci�n", "Vivienda"})
        Me.cmbidTipOrdServ.FormattingEnabled = True
        Me.cmbidTipOrdServ.Location = New System.Drawing.Point(125, 54)
        Me.cmbidTipOrdServ.Name = "cmbidTipOrdServ"
        Me.cmbidTipOrdServ.Size = New System.Drawing.Size(199, 21)
        Me.cmbidTipOrdServ.TabIndex = 39
        '
        'Label5
        '
        Me.Label5.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label5.Location = New System.Drawing.Point(22, 20)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(97, 20)
        Me.Label5.TabIndex = 40
        Me.Label5.Text = "Proveedor"
        '
        'ToolStripMenu
        '
        Me.ToolStripMenu.AutoSize = False
        Me.ToolStripMenu.BackColor = System.Drawing.Color.Silver
        Me.ToolStripMenu.Dock = System.Windows.Forms.DockStyle.None
        Me.ToolStripMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.txtMenuBusca, Me.btnMnuAltas, Me.btnMnuModificar, Me.btnMnuReporte, Me.btnMnuOk, Me.btnMnuCancelar, Me.btnMnuSalir})
        Me.ToolStripMenu.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow
        Me.ToolStripMenu.Location = New System.Drawing.Point(3, 0)
        Me.ToolStripMenu.Name = "ToolStripMenu"
        Me.ToolStripMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.ToolStripMenu.Size = New System.Drawing.Size(730, 42)
        Me.ToolStripMenu.TabIndex = 1
        Me.ToolStripMenu.Text = "ToolStrip1"
        '
        'txtMenuBusca
        '
        Me.txtMenuBusca.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.txtMenuBusca.Name = "txtMenuBusca"
        Me.txtMenuBusca.Size = New System.Drawing.Size(150, 42)
        Me.txtMenuBusca.ToolTipText = "Filtra Listado"
        '
        'btnMnuAltas
        '
        Me.btnMnuAltas.ForeColor = System.Drawing.Color.Red
        Me.btnMnuAltas.Image = CType(resources.GetObject("btnMnuAltas.Image"), System.Drawing.Image)
        Me.btnMnuAltas.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuAltas.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuAltas.Name = "btnMnuAltas"
        Me.btnMnuAltas.Size = New System.Drawing.Size(44, 39)
        Me.btnMnuAltas.Text = "&ALTAS"
        Me.btnMnuAltas.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnMnuAltas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuModificar
        '
        Me.btnMnuModificar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuModificar.Image = CType(resources.GetObject("btnMnuModificar.Image"), System.Drawing.Image)
        Me.btnMnuModificar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuModificar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuModificar.Name = "btnMnuModificar"
        Me.btnMnuModificar.Size = New System.Drawing.Size(74, 39)
        Me.btnMnuModificar.Text = "&MODIFICAR"
        Me.btnMnuModificar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnMnuModificar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuReporte
        '
        Me.btnMnuReporte.ForeColor = System.Drawing.Color.Red
        Me.btnMnuReporte.Image = CType(resources.GetObject("btnMnuReporte.Image"), System.Drawing.Image)
        Me.btnMnuReporte.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuReporte.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuReporte.Name = "btnMnuReporte"
        Me.btnMnuReporte.Size = New System.Drawing.Size(59, 39)
        Me.btnMnuReporte.Text = "&REPORTE"
        Me.btnMnuReporte.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuOk
        '
        Me.btnMnuOk.ForeColor = System.Drawing.Color.Red
        Me.btnMnuOk.Image = Global.Fletera.My.Resources.Resources.Save1
        Me.btnMnuOk.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuOk.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuOk.Name = "btnMnuOk"
        Me.btnMnuOk.Size = New System.Drawing.Size(56, 39)
        Me.btnMnuOk.Text = "&GRABAR"
        Me.btnMnuOk.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuCancelar
        '
        Me.btnMnuCancelar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuCancelar.Image = Global.Fletera.My.Resources.Resources.signos_f_059
        Me.btnMnuCancelar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuCancelar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuCancelar.Name = "btnMnuCancelar"
        Me.btnMnuCancelar.Size = New System.Drawing.Size(71, 39)
        Me.btnMnuCancelar.Text = "&CANCELAR"
        Me.btnMnuCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuSalir
        '
        Me.btnMnuSalir.ForeColor = System.Drawing.Color.Red
        Me.btnMnuSalir.Image = Global.Fletera.My.Resources.Resources.Salida2
        Me.btnMnuSalir.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuSalir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuSalir.Name = "btnMnuSalir"
        Me.btnMnuSalir.Size = New System.Drawing.Size(41, 39)
        Me.btnMnuSalir.Text = "&SALIR"
        Me.btnMnuSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'MeStatus1
        '
        Me.MeStatus1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.MeStatus1.Location = New System.Drawing.Point(0, 266)
        Me.MeStatus1.Margin = New System.Windows.Forms.Padding(4)
        Me.MeStatus1.Name = "MeStatus1"
        Me.MeStatus1.Size = New System.Drawing.Size(692, 26)
        Me.MeStatus1.TabIndex = 72
        '
        'GridTodos
        '
        Me.GridTodos.AllowUserToAddRows = False
        Me.GridTodos.AllowUserToDeleteRows = False
        Me.GridTodos.AllowUserToResizeRows = False
        Me.GridTodos.BackgroundColor = System.Drawing.Color.White
        Me.GridTodos.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.GridTodos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.GridTodos.Location = New System.Drawing.Point(3, 45)
        Me.GridTodos.MultiSelect = False
        Me.GridTodos.Name = "GridTodos"
        Me.GridTodos.ReadOnly = True
        Me.GridTodos.RowHeadersVisible = False
        Me.GridTodos.Size = New System.Drawing.Size(162, 222)
        Me.GridTodos.TabIndex = 73
        '
        'frmRelppROVTipoOrdSer
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(692, 292)
        Me.Controls.Add(Me.GridTodos)
        Me.Controls.Add(Me.MeStatus1)
        Me.Controls.Add(Me.ToolStripMenu)
        Me.Controls.Add(Me.GrupoCaptura)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmRelppROVTipoOrdSer"
        Me.Text = "Relacion Proveedores - Tipo Orden Servicio"
        Me.GrupoCaptura.ResumeLayout(False)
        Me.GrupoCaptura.PerformLayout()
        Me.ToolStripMenu.ResumeLayout(False)
        Me.ToolStripMenu.PerformLayout()
        CType(Me.GridTodos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region
    Sub New(ByVal vTag As String)
        InitializeComponent()
        _Tag = vTag
    End Sub

    Public Sub New(con As String, v As Boolean)
        InitializeComponent()
        Me.con = con
        BD = New CapaDatos.UtilSQL(con, "Juan")
        Me.v = v
    End Sub
#Region "MenusToolStrip"
    Private Sub LimpiaVariables()
        SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")

    End Sub
    Private Sub LimpiaCampos()
        cmbidProveedor.SelectedIndex = 0
        cmbidTipOrdServ.SelectedIndex = 0

        LimpiaVariables()
    End Sub

    Private Sub ActivaCampos(ByVal valor As Boolean, ByVal vOpcion As TipoOpcActivaCampos)
        If vOpcion = TipoOpcActivaCampos.tOpcDESHABTODOS Then
            cmbidProveedor.Enabled = False
            cmbidTipOrdServ.Enabled = False
            chkActivo.Enabled = False
        ElseIf vOpcion = TipoOpcActivaCampos.tOpcINICIALIZA Then
            cmbidProveedor.Enabled = valor
            cmbidTipOrdServ.Enabled = valor
            chkActivo.Enabled = valor
        End If
    End Sub


    Private Sub ActivaBotones(ByVal valor As Boolean, Optional ByVal vOpcion As TipoOpcActivaBoton = TipoOpcActivaBoton.tOpcInicializa)
        'Si es Verdadero Se activan Ok,Cancelar y Buscar y Terminar Se desactiva
        If vOpcion = TipoOpcActivaBoton.tOpcConsulta Then
            btnMnuOk.Enabled = False
            btnMnuCancelar.Enabled = valor
            'cmdBuscaClave.Enabled = valor
            btnMnuSalir.Enabled = Not valor

            btnMnuAltas.Enabled = Not valor
            btnMnuModificar.Enabled = Not valor
            'btnMnuEliminar.Enabled = Not valor
            btnMnuReporte.Enabled = Not valor


        ElseIf vOpcion = TipoOpcActivaBoton.tOpcInicializa Then
            btnMnuOk.Enabled = valor
            btnMnuCancelar.Enabled = valor
            'cmdBuscaClave.Enabled = valor
            btnMnuSalir.Enabled = Not valor

            btnMnuAltas.Enabled = Not valor
            btnMnuModificar.Enabled = Not valor
            'btnMnuEliminar.Enabled = Not valor
            btnMnuReporte.Enabled = Not valor

            txtMenuBusca.Enabled = Not valor
            GridTodos.Enabled = Not valor
        ElseIf vOpcion = TipoOpcActivaBoton.tOpcInsertar Then
            btnMnuOk.Enabled = valor
            btnMnuCancelar.Enabled = valor
            'cmdBuscaClave.Enabled = False
            btnMnuSalir.Enabled = Not valor

            btnMnuAltas.Enabled = Not valor
            btnMnuModificar.Enabled = Not valor
            'btnMnuEliminar.Enabled = Not valor
            btnMnuReporte.Enabled = Not valor
        End If
    End Sub

    Private Sub btnMnuAltas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuAltas.Click
        'ALTAS
        ' OpcionMnuTools(TipoOpcionForma.tOpcInsertar)
        '15/FEB/2016

        'txtMenuBusca.Enabled = True
        'GridTodos.Enabled = True

        'If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Alta) = False Then
        '    MessageBox.Show("No tiene los permisos Necesarios para poder dar de Alta en el Catalogo de Marcas!!!",
        '        "Acceso Denegado a Alta de Marcas", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    OpcionForma = TipoOpcionForma.tOpcConsultar
        '    Exit Sub


        'For Each ct As Control In GrupoCaptura.Controls
        '    If Not ct Is txtidMarca Then
        '        ct.Enabled = False
        '    Else
        '        ct.Enabled = True
        '    End If
        '    If TypeOf (ct) Is TextBox Then
        '        TryCast(ct, TextBox).Text = ""
        '    End If
        'Next
        'btnMnuAltas.Enabled = False
        'btnMnuModificar.Enabled = False
        'btnMnuEliminar.Enabled = False
        'btnMnuOk.Enabled = False
        'btnMnuCancelar.Enabled = True
        'txtMenuBusca.Enabled = False
        'GridTodos.Enabled = False

        'btnMnuSalir.Enabled = False

        OpcionForma = TipoOpcionForma.tOpcInsertar
        LimpiaCampos()
        ActivaCampos(True, TipoOpcActivaCampos.tOpcINICIALIZA)
        ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)

        If bEsIdentidad Then
            'txtIdDepto.Enabled = False
            'txtNomDepto.Focus()
        Else
            cmbidProveedor.Focus()
        End If

        Status("Ingrese nuevo Departamento para guardar", Me)
    End Sub
    Private Sub btnMnuModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuModificar.Click
        'MODIFICAR
        'OpcionMnuTools(TipoOpcionForma.tOpcModificar)
        'txtsIdColonia_Leave(sender, e)
        'txtsDescripcion.Enabled = False

        'If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Modificar) = False Then
        '    MessageBox.Show("No tiene los permisos Necesarios para poder Modificar la Informacion de la Marca!!!",
        '           "Acceso Denegado a Modificacion de la Informacion de la Marca", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    OpcionForma = TipoOpcionForma.tOpcConsultar
        '    Exit Sub
        'End If

        'btnMnuAltas.Enabled = False
        'btnMnuModificar.Enabled = False
        'btnMnuEliminar.Enabled = True
        'btnMnuCancelar.Enabled = True
        'btnMnuOk.Enabled = True

        'GridTodos.Enabled = False
        'btnMnuSalir.Enabled = False

        'CHECAR
        'If Trim(txtIdDepto.Text) = "" Then
        '    txtIdDepto.Text = DespliegaGrid(TablaBd, Inicio.CONSTR, CampoLLave)
        '    CargaForm(txtIdDepto.Text)
        'End If


        'txtidMarca.Focus()
        ' Status("Formulario Preparado para modificar Marcas", Me)

        OpcionForma = TipoOpcionForma.tOpcModificar
        ActivaCampos(True, TipoOpcActivaCampos.tOpcINICIALIZA)
        ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)

        'btnMnuEliminar.Enabled = True
        If bEsIdentidad Then

        Else
            cmbidProveedor.Focus()
        End If

        Status("Ingrese nuevo Departamento para guardar", Me)
    End Sub
    'Private Sub btnMnuEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    'ELIMINAR
    '    'OpcionMnuTools(TipoOpcionForma.tOpcEliminar)
    '    'txtsIdColonia_Leave(sender, e)
    '    'txtsDescripcion.Enabled = False

    '    'If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Baja) = False Then
    '    '    MessageBox.Show("No tiene los permisos Necesarios para poder Eliminar la Marca!!!",
    '    '        "Acceso Denegado a Eliminacion de Marca", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
    '    '    OpcionForma = TipoOpcionForma.tOpcConsultar
    '    '    Exit Sub
    '    'End If
    '    If cmbidMarca.SelectedValue = relacion.idMarca And cmbidTipOrdServ.SelectedValue = relacion.idTipOrdServ Then
    '        With relacion
    '            If MessageBox.Show("Esta completamente seguro que desea Eliminar la relaci�n : " &
    '                               .NombreMarca & " - " & .NomTipOrdServ & " ????", "Confirma que desea eliminar la Relaci�n : " & .NombreMarca & " - " & .NomTipOrdServ & "???",
    '                             MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then Exit Sub
    '            Try
    '                .Eliminar() 'Elimina el municipio
    '                MessageBox.Show("Departamento: " & .NomDepto & " Eliminado con exito!!!",
    '                                "Departamento Eliminado con exito", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '                Status("Departamento: " & .NomDepto & " Eliminado con exito!!!", Me)
    '                ActualizaGrid(txtMenuBusca.Text)
    '                txtIdDepto.Text = ""
    '                OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
    '            Catch ex As Exception
    '                Status("Error al tratar de eliminar el Departamento: " & ex.Message, Me, Err:=True)
    '                MessageBox.Show("Error al Tratar de eliminar el Departamento: " & .NomDepto & vbNewLine &
    '                                "Error. " & ex.Message, "Error: " & Err.Number, MessageBoxButtons.OK, MessageBoxIcon.Error)

    '            End Try
    '        End With
    '    End If
    'End Sub
    Private Sub btnMnuConsulta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'CONSULTA
        OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
        'txtsIdColonia_Leave(sender, e)
        'txtsNomColonia.Enabled = False
    End Sub
    Private Sub btnMnuReporte_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuReporte.Click
        'REPORTE
        'OpcionMnuTools(TipoOpcionForma.tOpcImprimir)

        'If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Reporte) = False Then
        '    MessageBox.Show("No tiene los permisos Necesarios para poder realizar Reporte de Estados!!!",
        '        "Acceso Denegado a Reportes de Estados", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    OpcionForma = TipoOpcionForma.tOpcConsultar
        '    Exit Sub
        'End If
        ToolStripMenu.Enabled = False
        'comboMnu.SelectedIndex = 4
        Status("Abriendo Reporte Espere. . . ", Me)
        Me.Cursor = Cursors.WaitCursor
        DespliegaReporte(TablaBd, CONSTR)
        Status("Finalizo el reporte. . . ", Me)
        Me.Cursor = Cursors.Default
        ToolStripMenu.Enabled = True
    End Sub

#End Region
    Private Sub ActualizaGrid(ByVal vRazonSocial As String, ByVal NomTipOrdServ As String)
        Dim str As String = "SELECT P.RazonSocial, t.NomTipOrdServ,rel.idProveedor,rel.idTipOrdServ " &
        "FROM RelProvTipOrdSer rel " &
        "INNER JOIN dbo.CatProveedores P ON P.idProveedor = Rel.idProveedor " &
        "INNER JOIN CatTipoOrdServ T ON T.idTipOrdServ = rel.idTipOrdServ"

        If vRazonSocial = "" And NomTipOrdServ = "" Then
            GridTodos.DataSource = BD.ExecuteReturn(str, TablaBd)
            Status(GridTodos.RowCount & " Registros", Me)
        Else
            GridTodos.DataSource = BD.ExecuteReturn(str &
                                                " WHERE RazonSocial Like '%" & vRazonSocial & "%'" &
                                                " AND NomTipOrdServ Like '%" & NomTipOrdServ & "%'")
            'Status(GridTodos.RowCount & " Registros obtenidos con el filtro %" & vNombreMarca & "%", Me)
        End If

    End Sub
    Private Sub OpcionMnuTools(ByVal vOpcion As TipoOpcionForma)
        If vOpcion <> TipoOpcionForma.tOpcImprimir And vOpcion <> TipoOpcionForma.tOpcEliminar Then
            OpcionForma = vOpcion
        End If
        txtMenuBusca.Enabled = True
        GridTodos.Enabled = True
        If vOpcion = TipoOpcionForma.tOpcInsertar Then
            'ALTAS

        ElseIf vOpcion = TipoOpcionForma.tOpcModificar Then
            'MODIFICAR

        ElseIf vOpcion = TipoOpcionForma.tOpcEliminar Then

        ElseIf vOpcion = TipoOpcionForma.tOpcConsultar Then
            'CONSULTA
            For Each ct As Control In GrupoCaptura.Controls
                ct.Enabled = False
                'If Not ct Is cmdBuscaClave Then
                '    ct.Enabled = False
                'Else
                '    ct.Enabled = True
                'End If
            Next
            txtMenuBusca.Enabled = True
            btnMnuAltas.Enabled = True
            btnMnuModificar.Enabled = True
            'btnMnuEliminar.Enabled = False
            btnMnuReporte.Enabled = True
            btnMnuOk.Enabled = False
            btnMnuCancelar.Enabled = False

            btnMnuSalir.Enabled = True

            'Status("Formulario preparado para Consultar los Municipios Existentes!!!", Me)
        ElseIf vOpcion = TipoOpcionForma.tOpcImprimir Then
            'REPORTE

        End If
    End Sub


    Private Sub txtMenuBusca_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtMenuBusca.KeyPress
        If Asc(e.KeyChar) = 39 Then e.Handled = True : Exit Sub 'Caracter ' Evita errores
        If Asc(e.KeyChar) = 13 Then
            GridTodos.Focus()
            e.Handled = True
        End If
    End Sub

    'Private Sub txtMenuBusca_KeyUp(sender As Object, e As KeyEventArgs) Handles txtMenuBusca.KeyUp
    '    If txtMenuBusca.Text.Trim = "" Then Exit Sub
    '    If e.KeyCode = Keys.Down Or e.KeyCode = Keys.Up Then
    '        GridTodos.Focus()
    '        e.Handled = True
    '        Exit Sub
    '    End If
    '    If e.KeyCode = Keys.Left Or e.KeyCode = Keys.Right Then Exit Sub
    '    ActualizaGrid(txtMenuBusca.Text)
    'End Sub



    Private Sub frmc_Estado_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Inicio.CONSTR = "" Then
            Inicio.CONSTR = con
        End If
        LlenaCombos()
        OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
        ActualizaGrid("", "")
        GridTodos.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
        GridTodos.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill


        'GridTodos.Columns(0).Width = GridTodos.Width - 20
        'GridTodos.Columns(1).Width = GridTodos.Width - 20
        GridTodos.Columns(2).Visible = False
        GridTodos.Columns(3).Visible = False
        txtMenuBusca.Focus()
    End Sub
    Private Sub GridTodos_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles GridTodos.CellEnter
        'With GridTodos
        CargaForm(GridTodos.Rows(GridTodos.CurrentRow.Index).Cells(2).Value, GridTodos.Rows(GridTodos.CurrentRow.Index).Cells(3).Value)
        'End With
    End Sub
    Private Sub GridTodos_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles GridTodos.KeyPress
        If Asc(e.KeyChar) = 8 Then
            txtMenuBusca.Focus()
            e.Handled = True
        End If
    End Sub
    Private Sub CargaForm(ByVal vidProveedor As Integer, ByVal vidTipOrdServ As Integer)
        Try

            relacion = New RelProvTipOrdSerClass(vidProveedor, vidTipOrdServ)
            With relacion
                cmbidProveedor.SelectedValue = .idProveedor
                cmbidTipOrdServ.SelectedValue = .idTipOrdServ
                If .Existe Then
                    chkActivo.Checked = .Activo
                    If OpcionForma = TipoOpcionForma.tOpcInsertar Then
                        Status("La Relacion Con id: " & .RazonSocial & " - " & .NomTipOrdServ & " Ya existe!!!, Favor de modificarlo para poder dar de alta a otra Relaci�n", Me, Err:=True)
                        MessageBox.Show("La Relaci�n Con id: " & .RazonSocial & " - " & .NomTipOrdServ & " YA EXISTE!!!, Favor de Cambiarlo y volver a intentarlo",
                                        "La Relaci�n Con id: " & .RazonSocial & " - " & .NomTipOrdServ & " YA EXISTE!!!", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    End If
                Else 'No existe la colonia!!!
                    If OpcionForma = TipoOpcionForma.tOpcInsertar Then
                        For Each Ct As Control In GrupoCaptura.Controls
                            Ct.Enabled = False
                            'If Not Ct Is txtIdDepto And Not Ct Is cmdBuscaClave Then
                            '    Ct.Enabled = True
                            'Else
                            '    Ct.Enabled = False
                            'End If
                            'If TypeOf (Ct) Is TextBox Then
                            '    If Not Ct Is cmbidMarca Then
                            '        TryCast(Ct, TextBox).Text = ""
                            '    End If
                            'End If
                        Next
                        btnMnuOk.Enabled = True
                        'cmdOk.Enabled = True
                        cmbidProveedor.Focus()
                        Status("Listo para Guardar la nueva Departamento con id: " & .RazonSocial & " - " & .NomTipOrdServ, Me)
                    End If
                End If
            End With
        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try

    End Sub
    Private Function Validar() As Boolean
        Validar = True
        If cmbidProveedor.SelectedValue = 0 Then
            MsgBox("El Proveedor No Puede Ser Vacio", vbInformation, "Aviso" & Me.Text)
            If cmbidProveedor.Enabled Then
                cmbidProveedor.Focus()
            End If
            Validar = False
        ElseIf cmbidTipOrdServ.SelectedValue = 0 Then
            MsgBox("Aplica en No Puede Ser Vacio", vbInformation, "Aviso" & Me.Text)
            If cmbidTipOrdServ.Enabled Then
                cmbidTipOrdServ.Focus()
            End If
            Validar = False

        End If

        'If Not Inicio.ValidandoCampo(Trim(cmbidMarca.SelectedValue), TipoDato.TdNumerico) Then
        '    If Not bEsIdentidad Then
        '        MsgBox(MENSAJE_CLAVE, vbInformation, "Aviso" & Me.Text)
        '        If txtIdDepto.Enabled Then
        '            txtIdDepto.Focus()
        '            txtIdDepto.SelectAll()
        '        End If
        '        Validar = False
        '    End If
        'ElseIf Not Inicio.ValidandoCampo(Trim(txtNomDepto.Text), TipoDato.TdCadena) Then
        '    MsgBox("El Nombre del Estado No Puede Ser Vacio", vbInformation, "Aviso" & Me.Text)
        '    If txtNomDepto.Enabled Then
        '        txtNomDepto.Focus()
        '        txtNomDepto.SelectAll()
        '    End If
        '    Validar = False
        'End If
    End Function
    'CONTROLES
    'Private Sub txtsIdEstado_KeyDown(sender As Object, e As KeyEventArgs)
    '    If e.KeyCode = Keys.F3 Then
    '        cmdBuscaClave_Click(sender, e)
    '    End If

    'End Sub
    'Private Sub txtsIdEstado_KeyPress(sender As Object, e As KeyPressEventArgs)
    '    If Asc(e.KeyChar) = 13 Then
    '        If txtIdDepto.Text.Trim <> "" Then
    '            CargaForm(txtIdDepto.Text)
    '        End If
    '        e.Handled = True
    '    End If
    'End Sub
    'Private Sub txtsNomEstado_KeyPress(sender As Object, e As KeyPressEventArgs)
    '    If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
    '        btnMnuOk_Click(sender, e)
    '    End If
    'End Sub

    'BOTONES
    Private Sub cmdCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
    Private Sub cmdTerminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub cmdOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
    'Private Sub cmdBuscaClave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Dim idEst As String = DespliegaGrid(TablaBd, Inicio.CONSTR, CampoLLave)
    '    If idEst <> "" Then CargaForm(idEst)
    'End Sub
    'Handles cmdCancelar.EnabledChanged, cmdTerminar.EnabledChanged
    '        With TryCast(sender, Button)
    '            If .Enabled Then
    '                Me.CancelButton = sender
    '            End If
    '        End With
    '    End Sub
    Private Sub cmdCancelar_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        With TryCast(sender, Button)
            If .Enabled Then
                Me.CancelButton = sender
            End If
        End With
    End Sub
    Private Sub cmdTerminar_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        With TryCast(sender, Button)
            If .Enabled Then
                Me.CancelButton = sender
            End If
        End With
    End Sub

    Private Sub btnMnuOk_Click(sender As Object, e As EventArgs) Handles btnMnuOk.Click
        Dim CadCam As String = ""
        Try
            If Not Validar() Then Exit Sub
            If GridTodos.RowCount = 0 Then
                relacion = New RelProvTipOrdSerClass(0, 0)
            End If
            With relacion
                If OpcionForma = TipoOpcionForma.tOpcModificar Then

                    CadCam = .GetCambios(chkActivo.Checked)
                    If CadCam <> "" Then
                        'If MessageBox.Show("Esta completamente Seguro que Desea guardar los Cambios Realizados al Estado Con id: " & .IDMARCA & vbNewLine &
                        '                CadCam, "Confirma que desea guardar los Cambios Realizados???", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then Exit Sub
                        Status("Guardando Modificaciones de la Relacion : " & .RazonSocial & " - " & .NomTipOrdServ, Me, 1, 2)
                        .Guardar(bEsIdentidad, cmbidTipOrdServ.SelectedValue, cmbidProveedor.SelectedValue, chkActivo.Checked)
                        Status("Relaci�n: " & .RazonSocial & " - " & .NomTipOrdServ & " Modificado Con Exito!!!", Me, 2, 2)
                        MessageBox.Show("Relaci�n : " & .RazonSocial & " - " & .NomTipOrdServ & " Modificada con exito!!!",
                                        "Accion terminada con exito!!!", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
                        ActualizaGrid(txtMenuBusca.Text, "")
                    Else
                        Status("No se detectaron cambios para realizar!!!", Me, Err:=True)
                    End If
                ElseIf OpcionForma = TipoOpcionForma.tOpcInsertar Then
                    .Existe = False
                    .idProveedor = cmbidProveedor.SelectedValue
                    .idTipOrdServ = cmbidTipOrdServ.SelectedValue
                    .Activo = chkActivo.Checked

                    Status("Guardando Nueva Relaci�n: " & cmbidProveedor.Text & " - " & cmbidTipOrdServ.Text & ". . .", Me, 1, 2)
                    .Guardar(bEsIdentidad, cmbidTipOrdServ.SelectedValue, cmbidProveedor.SelectedValue, chkActivo.Checked)
                    Status("Relaci�n: " & .RazonSocial & " - " & .NomTipOrdServ & " Guardado con Exito!!!", Me, 2, 2)
                    MessageBox.Show("Nuevo Relaci�n: " & .RazonSocial & " - " & .NomTipOrdServ & " Guardado con exito!!!",
                                    "Accion terminada con exito!!!", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
                    ActualizaGrid(txtMenuBusca.Text, "")

                End If
            End With

        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try
    End Sub


    Private Sub btnMnuCancelar_Click(sender As Object, e As EventArgs) Handles btnMnuCancelar.Click
        'If txtIdDepto.Text <> "" Then
        '    CargaForm(txtIdDepto.Text)
        'End If
        OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
    End Sub

    Private Sub btnMnuSalir_Click(sender As Object, e As EventArgs) Handles btnMnuSalir.Click
        Me.Close()
    End Sub

    Private Sub txtMenuBusca_Click(sender As Object, e As EventArgs) Handles txtMenuBusca.Click

    End Sub

    Private Sub GridTodos_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles GridTodos.CellContentClick

    End Sub


    Private Sub txtMenuBusca_TextChanged(sender As Object, e As EventArgs) Handles txtMenuBusca.TextChanged
        'DsLista.Tables(TablaBd).DefaultView.RowFilter = Nothing
        'DsLista.Tables(TablaBd).DefaultView.RowFilter = "Descripcion Like '*" & txtMenuBusca.Text & "*'"
        'Status(DsLista.Tables(TablaBd).DefaultView.Count & " Areas Encontradas. . . ", Me)
        'ActualizaLista()

        ActualizaGrid(txtMenuBusca.Text, "")
    End Sub

    Private Sub txtNomDepto_TextChanged(sender As Object, e As EventArgs)

    End Sub
    Private Function LlenaCombos() As Boolean
        Dim StrSql As String = ""
        LlenaCombos = True
        Try

            AutoCompletarProveedores(cmbidProveedor, False)
            AutoCompletarTOS(cmbidTipOrdServ, False)




            'DsCombo.Clear()

            'StrSql = "SELECT m.idMarca,m.NombreMarca FROM dbo.CatMarcas M"

            'DsCombo = Inicio.LlenaCombos("idMarca", "NombreMarca", "CatMarcas",
            '                   TipoConexion.TcSQL, Inicio.CONSTR, Process.GetCurrentProcess.ProcessName,
            '                   KEY_CHECAERROR, KEY_ESTATUS, , StrSql)

            'If Not DsCombo Is Nothing Then
            '    If DsCombo.Tables("CatMarcas").DefaultView.Count > 0 Then
            '        cmbidMarca.DataSource = DsCombo.Tables("CatMarcas")
            '        cmbidMarca.ValueMember = "idMarca"
            '        cmbidMarca.DisplayMember = "NombreMarca"
            '    End If
            'Else
            '    MsgBox("No se han dado de alta a Tipos de Orden de Servicios ", vbInformation, "Aviso" & Me.Text)
            '    LlenaCombos = False
            '    Exit Function
            'End If

            'StrSql = "SELECT tos.idTipOrdServ, tos.NomTipOrdServ FROM CatTipoOrdServ tos"

            'DsCombo = Inicio.LlenaCombos("idTipOrdServ", "NomTipOrdServ", "CatTipoOrdServ",
            '                   TipoConexion.TcSQL, Inicio.CONSTR, Process.GetCurrentProcess.ProcessName,
            '                   KEY_CHECAERROR, KEY_ESTATUS, , StrSql)

            'If Not DsCombo Is Nothing Then
            '    If DsCombo.Tables("CatTipoOrdServ").DefaultView.Count > 0 Then
            '        cmbidTipOrdServ.DataSource = DsCombo.Tables("CatTipoOrdServ")
            '        cmbidTipOrdServ.ValueMember = "idTipOrdServ"
            '        cmbidTipOrdServ.DisplayMember = "NomTipOrdServ"
            '    End If
            'Else
            '    MsgBox("No se han dado de alta a Tipos de Orden de Servicios ", vbInformation, "Aviso" & Me.Text)
            '    LlenaCombos = False
            '    Exit Function
            'End If

        Catch ex As Exception

        End Try
    End Function

    Public Function AutoCompletarProveedores(ByVal Control As ComboBox, ByVal bIncluyeTodos As Boolean) As AutoCompleteStringCollection

        Dim Coleccion As New AutoCompleteStringCollection
        Dim dt As DataTable


        Prov = New ProveedorClass(0)
        dt = Prov.TablaProvedores(bIncluyeTodos)

        'Recorrer y cargar los items para el Autocompletado
        For Each row As DataRow In dt.Rows
            Coleccion.Add(Convert.ToString(row("RazonSocial")))
        Next
        'Ajustamos el control TextBox o ComboBox para recibir los datos de la siguiente manera.
        With Control
            .DataSource = dt
            .DisplayMember = "RazonSocial"
            .ValueMember = "idProveedor"


            .AutoCompleteCustomSource = Coleccion
            .AutoCompleteMode = AutoCompleteMode.Suggest
            .AutoCompleteSource = AutoCompleteSource.CustomSource
        End With

        ' Aca le devuelvo los datos a recuperados de la Base
        Return Coleccion

    End Function

    Public Function AutoCompletarTOS(ByVal Control As ComboBox, ByVal bIncluyeTodos As Boolean) As AutoCompleteStringCollection

        Dim Coleccion As New AutoCompleteStringCollection
        Dim dt As DataTable


        tos = New TipoServClass(0)
        dt = tos.TablaTiposOrdenServicio(bIncluyeTodos)

        'Recorrer y cargar los items para el Autocompletado
        For Each row As DataRow In dt.Rows
            Coleccion.Add(Convert.ToString(row("NomTipOrdServ")))
        Next
        'Ajustamos el control TextBox o ComboBox para recibir los datos de la siguiente manera.
        With Control
            .DataSource = dt
            .DisplayMember = "NomTipOrdServ"
            .ValueMember = "idTipOrdServ"


            .AutoCompleteCustomSource = Coleccion
            .AutoCompleteMode = AutoCompleteMode.Suggest
            .AutoCompleteSource = AutoCompleteSource.CustomSource
        End With

        ' Aca le devuelvo los datos a recuperados de la Base
        Return Coleccion

    End Function



    Private Sub cmbidMarca_KeyUp(sender As Object, e As KeyEventArgs) Handles cmbidProveedor.KeyUp
        If e.KeyCode = Keys.Enter Then
            cmbidTipOrdServ.Focus()

        End If
    End Sub

    Private Sub cmbidTipOrdServ_KeyUp(sender As Object, e As KeyEventArgs) Handles cmbidTipOrdServ.KeyUp
        If e.KeyCode = Keys.Enter Then
            chkActivo.Focus()
        End If
    End Sub

    Private Sub chkActivo_CheckedChanged(sender As Object, e As EventArgs) Handles chkActivo.CheckedChanged

    End Sub

    Private Sub chkActivo_KeyUp(sender As Object, e As KeyEventArgs) Handles chkActivo.KeyUp
        If e.KeyCode = Keys.Enter Then
            btnMnuOk_Click(sender, e)
        End If
    End Sub
End Class
