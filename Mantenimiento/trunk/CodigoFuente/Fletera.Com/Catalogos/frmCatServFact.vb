'Fecha: 22 / Febrero / 2016
'ROGER GALA PACHECO
'************************************************************************************************************************************
'*  Fecha Modificacion :   22 / Febrero / 2016                                                                                       
'*  
'************************************************************************************************************************************
Public Class frmCatServFact
    Inherits System.Windows.Forms.Form
    Dim OpcionForma As TipoOpcionForma
    Dim TablaBd As String = "CatServFact"
    Dim CampoLLave As String = "CCODIGOPRODUCTO"
    'Dim CampoLLave2 As String = "IdCliente"
    'Dim TablaBd2 As String = "CatTipoServicio"
    Public WithEvents MeStatus1 As ControlStatus.MeStatus
    Friend WithEvents GridTodos As System.Windows.Forms.DataGridView

    'Private Servicio As ServicioClass
    Private CatServFact As CatServFactClass

    Private TipoServ As TipoServClass
    Friend WithEvents btnMnuOk As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuCancelar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuSalir As System.Windows.Forms.ToolStripButton
    Private _Tag As String
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtCIDPRODUCTO As System.Windows.Forms.TextBox
    Dim bEsIdentidad As Boolean = True
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtCNOMBREPRODUCTO As System.Windows.Forms.TextBox
    Friend WithEvents cmbTipoViaje As System.Windows.Forms.ComboBox
    Dim Salida As Boolean
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents cmdBuscaEdo As System.Windows.Forms.Button
    Friend WithEvents txtidEstado As System.Windows.Forms.TextBox
    Friend WithEvents txtNomEstado As System.Windows.Forms.TextBox
    'Dim DsCombo As New DataTable
    Dim DsCombo As New DataSet
    Friend WithEvents Label2 As Label
    Friend WithEvents cmdBuscaCli As Button
    Friend WithEvents txtIdCliente As TextBox
    Friend WithEvents txtRazonSocial As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents chkActivo As CheckBox
    Dim Division As DivisionClass
    Dim strSql As String
    Private _cveEmpresa As Integer



#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents GrupoCaptura As System.Windows.Forms.GroupBox
    Friend WithEvents ToolStripMenu As System.Windows.Forms.ToolStrip
    Friend WithEvents txtMenuBusca As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents btnMnuAltas As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuModificar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuEliminar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuReporte As System.Windows.Forms.ToolStripButton
    Friend WithEvents label1 As System.Windows.Forms.Label
    Friend WithEvents cmdBuscaClave As System.Windows.Forms.Button
    Friend WithEvents txtCCODIGOPRODUCTO As System.Windows.Forms.TextBox
    Private con As String
    Private v As Boolean

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCatServFact))
        Me.txtCCODIGOPRODUCTO = New System.Windows.Forms.TextBox()
        Me.label1 = New System.Windows.Forms.Label()
        Me.GrupoCaptura = New System.Windows.Forms.GroupBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.chkActivo = New System.Windows.Forms.CheckBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cmdBuscaCli = New System.Windows.Forms.Button()
        Me.txtIdCliente = New System.Windows.Forms.TextBox()
        Me.txtRazonSocial = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.cmdBuscaEdo = New System.Windows.Forms.Button()
        Me.txtidEstado = New System.Windows.Forms.TextBox()
        Me.txtNomEstado = New System.Windows.Forms.TextBox()
        Me.cmbTipoViaje = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtCNOMBREPRODUCTO = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtCIDPRODUCTO = New System.Windows.Forms.TextBox()
        Me.cmdBuscaClave = New System.Windows.Forms.Button()
        Me.ToolStripMenu = New System.Windows.Forms.ToolStrip()
        Me.txtMenuBusca = New System.Windows.Forms.ToolStripTextBox()
        Me.btnMnuAltas = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuModificar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuEliminar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuReporte = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuOk = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuCancelar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuSalir = New System.Windows.Forms.ToolStripButton()
        Me.MeStatus1 = New ControlStatus.MeStatus()
        Me.GridTodos = New System.Windows.Forms.DataGridView()
        Me.GrupoCaptura.SuspendLayout()
        Me.ToolStripMenu.SuspendLayout()
        CType(Me.GridTodos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtCCODIGOPRODUCTO
        '
        Me.txtCCODIGOPRODUCTO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCCODIGOPRODUCTO.Location = New System.Drawing.Point(119, 24)
        Me.txtCCODIGOPRODUCTO.MaxLength = 3
        Me.txtCCODIGOPRODUCTO.Name = "txtCCODIGOPRODUCTO"
        Me.txtCCODIGOPRODUCTO.Size = New System.Drawing.Size(69, 20)
        Me.txtCCODIGOPRODUCTO.TabIndex = 4
        '
        'label1
        '
        Me.label1.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.label1.Location = New System.Drawing.Point(6, 27)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(94, 20)
        Me.label1.TabIndex = 14
        Me.label1.Text = "Servicio:"
        '
        'GrupoCaptura
        '
        Me.GrupoCaptura.Controls.Add(Me.Label5)
        Me.GrupoCaptura.Controls.Add(Me.chkActivo)
        Me.GrupoCaptura.Controls.Add(Me.Label2)
        Me.GrupoCaptura.Controls.Add(Me.cmdBuscaCli)
        Me.GrupoCaptura.Controls.Add(Me.txtIdCliente)
        Me.GrupoCaptura.Controls.Add(Me.txtRazonSocial)
        Me.GrupoCaptura.Controls.Add(Me.Label9)
        Me.GrupoCaptura.Controls.Add(Me.cmdBuscaEdo)
        Me.GrupoCaptura.Controls.Add(Me.txtidEstado)
        Me.GrupoCaptura.Controls.Add(Me.txtNomEstado)
        Me.GrupoCaptura.Controls.Add(Me.cmbTipoViaje)
        Me.GrupoCaptura.Controls.Add(Me.Label8)
        Me.GrupoCaptura.Controls.Add(Me.Label4)
        Me.GrupoCaptura.Controls.Add(Me.txtCNOMBREPRODUCTO)
        Me.GrupoCaptura.Controls.Add(Me.Label3)
        Me.GrupoCaptura.Controls.Add(Me.txtCIDPRODUCTO)
        Me.GrupoCaptura.Controls.Add(Me.label1)
        Me.GrupoCaptura.Controls.Add(Me.cmdBuscaClave)
        Me.GrupoCaptura.Controls.Add(Me.txtCCODIGOPRODUCTO)
        Me.GrupoCaptura.Location = New System.Drawing.Point(168, 45)
        Me.GrupoCaptura.Name = "GrupoCaptura"
        Me.GrupoCaptura.Size = New System.Drawing.Size(524, 292)
        Me.GrupoCaptura.TabIndex = 3
        Me.GrupoCaptura.TabStop = False
        '
        'Label5
        '
        Me.Label5.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label5.Location = New System.Drawing.Point(6, 228)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(94, 20)
        Me.Label5.TabIndex = 46
        Me.Label5.Text = "Activo:"
        '
        'chkActivo
        '
        Me.chkActivo.AutoSize = True
        Me.chkActivo.Location = New System.Drawing.Point(122, 228)
        Me.chkActivo.Name = "chkActivo"
        Me.chkActivo.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.chkActivo.Size = New System.Drawing.Size(15, 14)
        Me.chkActivo.TabIndex = 45
        Me.chkActivo.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label2.Location = New System.Drawing.Point(6, 104)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(94, 20)
        Me.Label2.TabIndex = 44
        Me.Label2.Text = "Cliente:"
        '
        'cmdBuscaCli
        '
        Me.cmdBuscaCli.Image = CType(resources.GetObject("cmdBuscaCli.Image"), System.Drawing.Image)
        Me.cmdBuscaCli.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdBuscaCli.Location = New System.Drawing.Point(199, 93)
        Me.cmdBuscaCli.Name = "cmdBuscaCli"
        Me.cmdBuscaCli.Size = New System.Drawing.Size(32, 32)
        Me.cmdBuscaCli.TabIndex = 42
        Me.cmdBuscaCli.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'txtIdCliente
        '
        Me.txtIdCliente.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtIdCliente.Location = New System.Drawing.Point(119, 101)
        Me.txtIdCliente.MaxLength = 3
        Me.txtIdCliente.Name = "txtIdCliente"
        Me.txtIdCliente.Size = New System.Drawing.Size(69, 20)
        Me.txtIdCliente.TabIndex = 41
        '
        'txtRazonSocial
        '
        Me.txtRazonSocial.Enabled = False
        Me.txtRazonSocial.Location = New System.Drawing.Point(237, 105)
        Me.txtRazonSocial.Name = "txtRazonSocial"
        Me.txtRazonSocial.ReadOnly = True
        Me.txtRazonSocial.Size = New System.Drawing.Size(275, 20)
        Me.txtRazonSocial.TabIndex = 43
        '
        'Label9
        '
        Me.Label9.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label9.Location = New System.Drawing.Point(6, 198)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(94, 20)
        Me.Label9.TabIndex = 40
        Me.Label9.Text = "Estado:"
        '
        'cmdBuscaEdo
        '
        Me.cmdBuscaEdo.Image = CType(resources.GetObject("cmdBuscaEdo.Image"), System.Drawing.Image)
        Me.cmdBuscaEdo.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdBuscaEdo.Location = New System.Drawing.Point(199, 187)
        Me.cmdBuscaEdo.Name = "cmdBuscaEdo"
        Me.cmdBuscaEdo.Size = New System.Drawing.Size(32, 32)
        Me.cmdBuscaEdo.TabIndex = 16
        Me.cmdBuscaEdo.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'txtidEstado
        '
        Me.txtidEstado.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtidEstado.Location = New System.Drawing.Point(122, 195)
        Me.txtidEstado.MaxLength = 3
        Me.txtidEstado.Name = "txtidEstado"
        Me.txtidEstado.Size = New System.Drawing.Size(69, 20)
        Me.txtidEstado.TabIndex = 15
        '
        'txtNomEstado
        '
        Me.txtNomEstado.Enabled = False
        Me.txtNomEstado.Location = New System.Drawing.Point(237, 199)
        Me.txtNomEstado.Name = "txtNomEstado"
        Me.txtNomEstado.ReadOnly = True
        Me.txtNomEstado.Size = New System.Drawing.Size(275, 20)
        Me.txtNomEstado.TabIndex = 17
        '
        'cmbTipoViaje
        '
        Me.cmbTipoViaje.AutoCompleteCustomSource.AddRange(New String() {"Regularizaci�n", "Vivienda"})
        Me.cmbTipoViaje.FormattingEnabled = True
        Me.cmbTipoViaje.Items.AddRange(New Object() {"ACTIVO", "INACTIVO"})
        Me.cmbTipoViaje.Location = New System.Drawing.Point(119, 157)
        Me.cmbTipoViaje.Name = "cmbTipoViaje"
        Me.cmbTipoViaje.Size = New System.Drawing.Size(112, 21)
        Me.cmbTipoViaje.TabIndex = 14
        '
        'Label8
        '
        Me.Label8.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label8.Location = New System.Drawing.Point(6, 160)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(94, 20)
        Me.Label8.TabIndex = 36
        Me.Label8.Text = "Tipo Viaje:"
        '
        'Label4
        '
        Me.Label4.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label4.Location = New System.Drawing.Point(6, 133)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(91, 20)
        Me.Label4.TabIndex = 32
        Me.Label4.Text = "Descripci�n:"
        '
        'txtCNOMBREPRODUCTO
        '
        Me.txtCNOMBREPRODUCTO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCNOMBREPRODUCTO.Location = New System.Drawing.Point(119, 131)
        Me.txtCNOMBREPRODUCTO.MaxLength = 250
        Me.txtCNOMBREPRODUCTO.Name = "txtCNOMBREPRODUCTO"
        Me.txtCNOMBREPRODUCTO.Size = New System.Drawing.Size(393, 20)
        Me.txtCNOMBREPRODUCTO.TabIndex = 10
        '
        'Label3
        '
        Me.Label3.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label3.Location = New System.Drawing.Point(6, 66)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(110, 20)
        Me.Label3.TabIndex = 20
        Me.Label3.Text = "Codigo COMERCIAL"
        '
        'txtCIDPRODUCTO
        '
        Me.txtCIDPRODUCTO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCIDPRODUCTO.Location = New System.Drawing.Point(119, 63)
        Me.txtCIDPRODUCTO.MaxLength = 3
        Me.txtCIDPRODUCTO.Name = "txtCIDPRODUCTO"
        Me.txtCIDPRODUCTO.Size = New System.Drawing.Size(199, 20)
        Me.txtCIDPRODUCTO.TabIndex = 6
        '
        'cmdBuscaClave
        '
        Me.cmdBuscaClave.Image = CType(resources.GetObject("cmdBuscaClave.Image"), System.Drawing.Image)
        Me.cmdBuscaClave.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdBuscaClave.Location = New System.Drawing.Point(199, 16)
        Me.cmdBuscaClave.Name = "cmdBuscaClave"
        Me.cmdBuscaClave.Size = New System.Drawing.Size(32, 32)
        Me.cmdBuscaClave.TabIndex = 5
        Me.cmdBuscaClave.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'ToolStripMenu
        '
        Me.ToolStripMenu.AutoSize = False
        Me.ToolStripMenu.BackColor = System.Drawing.Color.Silver
        Me.ToolStripMenu.Dock = System.Windows.Forms.DockStyle.None
        Me.ToolStripMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.txtMenuBusca, Me.btnMnuAltas, Me.btnMnuModificar, Me.btnMnuEliminar, Me.btnMnuReporte, Me.btnMnuOk, Me.btnMnuCancelar, Me.btnMnuSalir})
        Me.ToolStripMenu.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow
        Me.ToolStripMenu.Location = New System.Drawing.Point(3, 0)
        Me.ToolStripMenu.Name = "ToolStripMenu"
        Me.ToolStripMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.ToolStripMenu.Size = New System.Drawing.Size(730, 42)
        Me.ToolStripMenu.TabIndex = 1
        Me.ToolStripMenu.Text = "ToolStrip1"
        '
        'txtMenuBusca
        '
        Me.txtMenuBusca.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.txtMenuBusca.Name = "txtMenuBusca"
        Me.txtMenuBusca.Size = New System.Drawing.Size(150, 42)
        Me.txtMenuBusca.ToolTipText = "Filtra Listado"
        '
        'btnMnuAltas
        '
        Me.btnMnuAltas.ForeColor = System.Drawing.Color.Red
        Me.btnMnuAltas.Image = CType(resources.GetObject("btnMnuAltas.Image"), System.Drawing.Image)
        Me.btnMnuAltas.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuAltas.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuAltas.Name = "btnMnuAltas"
        Me.btnMnuAltas.Size = New System.Drawing.Size(46, 39)
        Me.btnMnuAltas.Text = "&ALTAS"
        Me.btnMnuAltas.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnMnuAltas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuModificar
        '
        Me.btnMnuModificar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuModificar.Image = CType(resources.GetObject("btnMnuModificar.Image"), System.Drawing.Image)
        Me.btnMnuModificar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuModificar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuModificar.Name = "btnMnuModificar"
        Me.btnMnuModificar.Size = New System.Drawing.Size(74, 39)
        Me.btnMnuModificar.Text = "&MODIFICAR"
        Me.btnMnuModificar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnMnuModificar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuEliminar
        '
        Me.btnMnuEliminar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuEliminar.Image = CType(resources.GetObject("btnMnuEliminar.Image"), System.Drawing.Image)
        Me.btnMnuEliminar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuEliminar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuEliminar.Name = "btnMnuEliminar"
        Me.btnMnuEliminar.Size = New System.Drawing.Size(64, 39)
        Me.btnMnuEliminar.Text = "&ELIMINAR"
        Me.btnMnuEliminar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnMnuEliminar.Visible = False
        '
        'btnMnuReporte
        '
        Me.btnMnuReporte.ForeColor = System.Drawing.Color.Red
        Me.btnMnuReporte.Image = CType(resources.GetObject("btnMnuReporte.Image"), System.Drawing.Image)
        Me.btnMnuReporte.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuReporte.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuReporte.Name = "btnMnuReporte"
        Me.btnMnuReporte.Size = New System.Drawing.Size(60, 39)
        Me.btnMnuReporte.Text = "&REPORTE"
        Me.btnMnuReporte.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuOk
        '
        Me.btnMnuOk.ForeColor = System.Drawing.Color.Red
        Me.btnMnuOk.Image = CType(resources.GetObject("btnMnuOk.Image"), System.Drawing.Image)
        Me.btnMnuOk.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuOk.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuOk.Name = "btnMnuOk"
        Me.btnMnuOk.Size = New System.Drawing.Size(56, 39)
        Me.btnMnuOk.Text = "&GRABAR"
        Me.btnMnuOk.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuCancelar
        '
        Me.btnMnuCancelar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuCancelar.Image = CType(resources.GetObject("btnMnuCancelar.Image"), System.Drawing.Image)
        Me.btnMnuCancelar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuCancelar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuCancelar.Name = "btnMnuCancelar"
        Me.btnMnuCancelar.Size = New System.Drawing.Size(71, 39)
        Me.btnMnuCancelar.Text = "&CANCELAR"
        Me.btnMnuCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuSalir
        '
        Me.btnMnuSalir.ForeColor = System.Drawing.Color.Red
        Me.btnMnuSalir.Image = CType(resources.GetObject("btnMnuSalir.Image"), System.Drawing.Image)
        Me.btnMnuSalir.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuSalir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuSalir.Name = "btnMnuSalir"
        Me.btnMnuSalir.Size = New System.Drawing.Size(41, 39)
        Me.btnMnuSalir.Text = "&SALIR"
        Me.btnMnuSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'MeStatus1
        '
        Me.MeStatus1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.MeStatus1.Location = New System.Drawing.Point(0, 336)
        Me.MeStatus1.Margin = New System.Windows.Forms.Padding(4)
        Me.MeStatus1.Name = "MeStatus1"
        Me.MeStatus1.Size = New System.Drawing.Size(692, 26)
        Me.MeStatus1.TabIndex = 72
        '
        'GridTodos
        '
        Me.GridTodos.AllowUserToAddRows = False
        Me.GridTodos.AllowUserToDeleteRows = False
        Me.GridTodos.AllowUserToResizeRows = False
        Me.GridTodos.BackgroundColor = System.Drawing.Color.White
        Me.GridTodos.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.GridTodos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.GridTodos.Location = New System.Drawing.Point(3, 45)
        Me.GridTodos.MultiSelect = False
        Me.GridTodos.Name = "GridTodos"
        Me.GridTodos.ReadOnly = True
        Me.GridTodos.RowHeadersVisible = False
        Me.GridTodos.Size = New System.Drawing.Size(162, 284)
        Me.GridTodos.TabIndex = 73
        '
        'frmCatServFact
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(692, 362)
        Me.Controls.Add(Me.GridTodos)
        Me.Controls.Add(Me.MeStatus1)
        Me.Controls.Add(Me.ToolStripMenu)
        Me.Controls.Add(Me.GrupoCaptura)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmCatServFact"
        Me.Text = "Catal�go de Servicios para Facturaci�n"
        Me.GrupoCaptura.ResumeLayout(False)
        Me.GrupoCaptura.PerformLayout()
        Me.ToolStripMenu.ResumeLayout(False)
        Me.ToolStripMenu.PerformLayout()
        CType(Me.GridTodos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region
    Sub New(ByVal vTag As String)
        InitializeComponent()
        _Tag = vTag
    End Sub

    Public Sub New(con As String, v As Boolean, cveEmpresa As Integer)
        Me.con = con
        BD = New CapaDatos.UtilSQL(con, "Juan")
        _cveEmpresa = cveEmpresa
        InitializeComponent()
        Me.v = v
    End Sub
#Region "MenusToolStrip"
    Private Sub LimpiaVariables()
        SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")

    End Sub
    Private Sub LimpiaCampos()
        txtCCODIGOPRODUCTO.Text = ""
        txtCIDPRODUCTO.Text = ""
        txtCNOMBREPRODUCTO.Text = ""
        cmbTipoViaje.SelectedIndex = 0
        txtidEstado.Text = ""
        txtNomEstado.Text = ""
        LimpiaVariables()
    End Sub

    Private Sub ActivaCampos(ByVal valor As Boolean, ByVal vOpcion As TipoOpcActivaCampos)
        If vOpcion = TipoOpcActivaCampos.tOpcDESHABTODOS Then
            txtCCODIGOPRODUCTO.Enabled = False
            txtCIDPRODUCTO.Enabled = False
            txtCNOMBREPRODUCTO.Enabled = False
            cmbTipoViaje.Enabled = False
            txtidEstado.Enabled = False
            txtNomEstado.Enabled = False
        ElseIf vOpcion = TipoOpcActivaCampos.tOpcINICIALIZA Then
            txtCCODIGOPRODUCTO.Enabled = Not valor
            txtCIDPRODUCTO.Enabled = valor
            txtCNOMBREPRODUCTO.Enabled = valor
            cmbTipoViaje.Enabled = valor
            txtidEstado.Enabled = valor
            txtNomEstado.Enabled = valor
        End If
    End Sub


    Private Sub ActivaBotones(ByVal valor As Boolean, Optional ByVal vOpcion As TipoOpcActivaBoton = TipoOpcActivaBoton.tOpcInicializa)
        If vOpcion = TipoOpcActivaBoton.tOpcConsulta Then
            btnMnuOk.Enabled = False
            btnMnuCancelar.Enabled = valor
            btnMnuSalir.Enabled = Not valor

            btnMnuAltas.Enabled = Not valor
            btnMnuModificar.Enabled = Not valor
            btnMnuEliminar.Enabled = Not valor
            btnMnuReporte.Enabled = Not valor
        ElseIf vOpcion = TipoOpcActivaBoton.tOpcInicializa Then
            btnMnuOk.Enabled = valor
            btnMnuCancelar.Enabled = valor
            btnMnuSalir.Enabled = Not valor

            btnMnuAltas.Enabled = Not valor
            btnMnuModificar.Enabled = Not valor
            btnMnuEliminar.Enabled = Not valor
            btnMnuReporte.Enabled = Not valor

            txtMenuBusca.Enabled = Not valor
            GridTodos.Enabled = Not valor
        ElseIf vOpcion = TipoOpcActivaBoton.tOpcInsertar Then
            btnMnuOk.Enabled = valor
            btnMnuCancelar.Enabled = valor
            btnMnuSalir.Enabled = Not valor

            btnMnuAltas.Enabled = Not valor
            btnMnuModificar.Enabled = Not valor
            btnMnuEliminar.Enabled = Not valor
            btnMnuReporte.Enabled = Not valor
        End If
    End Sub

    Private Sub btnMnuAltas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuAltas.Click
        'ALTAS
        '15/FEB/2016
        'If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Alta) = False Then
        '    MessageBox.Show("No tiene los permisos Necesarios para poder dar de Alta en el Catalogo de Servicios!!!",
        '        "Acceso Denegado a Alta de Servicios", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    OpcionForma = TipoOpcionForma.tOpcConsultar
        '    Exit Sub
        'End If
        OpcionForma = TipoOpcionForma.tOpcInsertar
        LimpiaCampos()
        ActivaCampos(True, TipoOpcActivaCampos.tOpcINICIALIZA)
        ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)

        If bEsIdentidad Then
            txtCCODIGOPRODUCTO.Enabled = False
            txtCIDPRODUCTO.Focus()
            txtCIDPRODUCTO.SelectAll()
        Else
            txtCCODIGOPRODUCTO.Focus()
        End If
        Status("Ingrese nuevo Servicio para guardar", Me)
    End Sub
    Private Sub btnMnuModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuModificar.Click
        'MODIFICAR
        'If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Modificar) = False Then
        '    MessageBox.Show("No tiene los permisos Necesarios para poder Modificar la Informacion del Servicio!!!",
        '           "Acceso Denegado a Modificacion de la Informacion del Servicio", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    OpcionForma = TipoOpcionForma.tOpcConsultar
        '    Exit Sub
        'End If
        If Trim(txtCCODIGOPRODUCTO.Text) = "" Then
            txtCCODIGOPRODUCTO.Text = DespliegaGrid(TablaBd, Inicio.CONSTR, CampoLLave)
            CargaForm(txtCCODIGOPRODUCTO.Text, UCase(TablaBd))
        End If

        OpcionForma = TipoOpcionForma.tOpcModificar
        ActivaCampos(True, TipoOpcActivaCampos.tOpcINICIALIZA)
        ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)

        btnMnuEliminar.Enabled = True
        If bEsIdentidad Then
            txtCCODIGOPRODUCTO.Enabled = False
            'txtNomServicio.Focus()
        Else
            txtCCODIGOPRODUCTO.Focus()
        End If

        Status("Ingrese Servicio para guardar", Me)
    End Sub
    Private Sub btnMnuEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuEliminar.Click
        'ELIMINAR
        'If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Baja) = False Then
        '    MessageBox.Show("No tiene los permisos Necesarios para poder Eliminar el Servicio!!!",
        '        "Acceso Denegado a Eliminacion de Servicio", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    OpcionForma = TipoOpcionForma.tOpcConsultar
        '    Exit Sub
        'End If

        '10-FEB-2017
        'If txtCCODIGOPRODUCTO.Text = CatServFact.CCODIGOPRODUCTO Then
        '    With CatServFact
        '        If MessageBox.Show("Esta completamente seguro que desea Eliminar el Servicio: " &
        '                           .NomServicio & " Con Id: " & .idServicio & " ????", "Confirma que desea eliminar el Servicio: " & .NomServicio & "???",
        '                         MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then Exit Sub
        '        Try
        '            .Eliminar() 'Elimina el municipio
        '            MessageBox.Show("Servicio: " & .NomServicio & " Eliminada con exito!!!",
        '                            "Servicio Eliminado con exito", MessageBoxButtons.OK, MessageBoxIcon.Information)
        '            Status("Servicio: " & .NomServicio & " Eliminado con exito!!!", Me)
        '            ActualizaGrid(txtMenuBusca.Text)
        '            txtCCODIGOPRODUCTO.Text = ""
        '            OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
        '        Catch ex As Exception
        '            Status("Error al tratar de eliminar el Servicio: " & ex.Message, Me, Err:=True)
        '            MessageBox.Show("Error al Tratar de eliminar el Servicio: " & .NomServicio & vbNewLine &
        '                            "Error. " & ex.Message, "Error: " & Err.Number, MessageBoxButtons.OK, MessageBoxIcon.Error)

        '        End Try
        '    End With
        'End If
    End Sub
    Private Sub btnMnuReporte_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuReporte.Click
        'REPORTE
        'If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Reporte) = False Then
        '    MessageBox.Show("No tiene los permisos Necesarios para poder realizar Reporte de Servicios!!!",
        '        "Acceso Denegado a Reporte de Servicios", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    OpcionForma = TipoOpcionForma.tOpcConsultar
        '    Exit Sub
        'End If
        ToolStripMenu.Enabled = False
        Status("Abriendo Reporte Espere. . . ", Me)
        Me.Cursor = Cursors.WaitCursor
        DespliegaReporte(TablaBd, CONSTR)
        Status("Finalizo el reporte. . . ", Me)
        Me.Cursor = Cursors.Default
        ToolStripMenu.Enabled = True
    End Sub

#End Region
    Private Sub ActualizaGrid(ByVal vCCODIGOPRODUCTO As String)
        Dim filtroExtra As String
        If vCCODIGOPRODUCTO = "" Then
            If _cveEmpresa > 0 Then
                filtroExtra = " where cli.idempresa = " & _cveEmpresa
            Else
                filtroExtra = ""
            End If

            strSql = "SELECT sf.CCODIGOPRODUCTO, cli.Nombre " &
            "FROM dbo.CatServFact sf " &
            "INNER JOIN dbo.CatClientes cli ON cli.IdCliente = sf.IdCliente " & filtroExtra

            GridTodos.DataSource = BD.ExecuteReturn(strSql, TablaBd)
            Status(GridTodos.RowCount & " Registros", Me)
        Else
            If _cveEmpresa > 0 Then
                filtroExtra = " where cli.idempresa = " & _cveEmpresa & " and sf.CCODIGOPRODUCTO Like '%" & vCCODIGOPRODUCTO & "%'"
            Else
                filtroExtra = " WHERE sf.CCODIGOPRODUCTO Like '%" & vCCODIGOPRODUCTO & "%'"
            End If

            strSql = "SELECT sf.CCODIGOPRODUCTO, cli.Nombre " &
            "FROM dbo.CatServFact sf " &
            "INNER JOIN dbo.CatClientes cli ON cli.IdCliente = sf.IdCliente " & filtroExtra

            GridTodos.DataSource = BD.ExecuteReturn(strSql)

            Status(GridTodos.RowCount & " Registros obtenidos con el filtro %" & vCCODIGOPRODUCTO & "%", Me)
        End If
    End Sub

    Private Sub OpcionMnuTools(ByVal vOpcion As TipoOpcionForma)
        If vOpcion <> TipoOpcionForma.tOpcImprimir And vOpcion <> TipoOpcionForma.tOpcEliminar Then
            OpcionForma = vOpcion
        End If
        txtMenuBusca.Enabled = True
        GridTodos.Enabled = True
        If vOpcion = TipoOpcionForma.tOpcConsultar Then
            'CONSULTA
            For Each ct As Control In GrupoCaptura.Controls
                If Not ct Is cmdBuscaClave Then
                    ct.Enabled = False
                Else
                    ct.Enabled = True
                End If
            Next
            txtMenuBusca.Enabled = True
            btnMnuAltas.Enabled = True
            btnMnuModificar.Enabled = True
            btnMnuEliminar.Enabled = False
            btnMnuReporte.Enabled = True
            btnMnuOk.Enabled = False
            btnMnuCancelar.Enabled = False

            btnMnuSalir.Enabled = True


        End If
    End Sub


    Private Sub txtMenuBusca_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtMenuBusca.KeyPress
        If Asc(e.KeyChar) = 39 Then e.Handled = True : Exit Sub 'Caracter ' Evita errores
        If Asc(e.KeyChar) = 13 Then
            GridTodos.Focus()
            e.Handled = True
        End If
    End Sub

    'Private Sub txtMenuBusca_KeyUp(sender As Object, e As KeyEventArgs) Handles txtMenuBusca.KeyUp
    '    If txtMenuBusca.Text.Trim = "" Then Exit Sub
    '    If e.KeyCode = Keys.Down Or e.KeyCode = Keys.Up Then
    '        GridTodos.Focus()
    '        e.Handled = True
    '        Exit Sub
    '    End If
    '    If e.KeyCode = Keys.Left Or e.KeyCode = Keys.Right Then Exit Sub
    '    ActualizaGrid(txtMenuBusca.Text)
    'End Sub



    Private Sub frmCatFamilias_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
        ActualizaGrid("")
        GridTodos.Columns(1).Width = GridTodos.Width - 20
        GridTodos.Columns(0).Visible = False
        txtMenuBusca.Focus()
        LlenaCombos()
    End Sub
    Private Sub GridTodos_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles GridTodos.CellEnter
        CargaForm(GridTodos.Rows(GridTodos.CurrentRow.Index).Cells(0).Value, UCase(TablaBd))
    End Sub
    Private Sub GridTodos_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles GridTodos.KeyPress
        If Asc(e.KeyChar) = 8 Then
            txtMenuBusca.Focus()
            e.Handled = True
        End If
    End Sub
    Private Sub CargaForm(ByVal idClave As String, ByVal NomTabla As String)
        Try
            Select Case UCase(NomTabla)
                Case UCase(TablaBd)
                    CatServFact = New CatServFactClass(idClave)
                    With CatServFact
                        txtCCODIGOPRODUCTO.Text = .CCODIGOPRODUCTO
                        txtIdCliente.Text = .IdCliente
                        If .Existe Then
                            txtCNOMBREPRODUCTO.Text = .CNOMBREPRODUCTO
                            txtCIDPRODUCTO.Text = .CIDPRODUCTO
                            cmbTipoViaje.SelectedValue = .TipoViaje
                            txtidEstado.Text = .idEstado
                            txtNomEstado.Text = .NomEstado
                            chkActivo.Checked = .Activo

                            'txtCIDALMACEN_COM.Text = .CIDALMACEN_COM
                            If OpcionForma = TipoOpcionForma.tOpcInsertar Then
                                Status("EL Servicio Con id: " & .CCODIGOPRODUCTO &
                                       " Ya existe!!!, Favor de modificarlo para poder dar de alta a otro Servicio", Me, Err:=True)
                                MessageBox.Show("El Servicio con id: " & .CCODIGOPRODUCTO &
                                                " YA EXISTE!!!, Favor de Cambiarlo y volver a intentarlo",
                                                "El Servicio con id: " & .CCODIGOPRODUCTO & " YA EXISTE!!!", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            End If
                        Else 'No existe 
                            If OpcionForma = TipoOpcionForma.tOpcInsertar Then

                                ActivaCampos(True, TipoOpcActivaCampos.tOpcINICIALIZA)
                                ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)
                                btnMnuOk.Enabled = True
                                txtCIDPRODUCTO.Focus()
                                Status("Listo para Guardar nuevo Servicio con id: " & .CCODIGOPRODUCTO, Me)
                            End If
                        End If
                    End With
                    'Case UCase(TablaBd2)
                    '    'aqui()
                    '    TipoServ = New TipoServClass(idClave)
                    '    With TipoServ
                    '        txtCIDPRODUCTO.Text = .idTipoServicio
                    '        If .Existe Then
                    '            'txtNomTipoServicio.Text = .NomTipoServicio
                    '            'txtNomServicio.Focus()
                    '            'txtNomServicio.SelectAll()
                    '        Else
                    '            MsgBox("El Tipo de Servicio con id: " & txtCIDPRODUCTO.Text & " No Existe")
                    '            txtCIDPRODUCTO.Focus()
                    '            txtCIDPRODUCTO.SelectAll()
                    '        End If
                    '    End With
                    'Case UCase("Division")
                    '    Division = New DivisionClass(Val(idClave))
                    '    If Division.Existe Then
                    '        txtNomEstado.Text = Division.NomDivision
                    '    End If

            End Select



        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try

    End Sub
    Private Function Validar() As Boolean
        Validar = True
        If Not Inicio.ValidandoCampo(Trim(txtCCODIGOPRODUCTO.Text), TipoDato.TdNumerico) Then
            If Not bEsIdentidad Then
                MsgBox(MENSAJE_CLAVE, vbInformation, "Aviso" & Me.Text)
                If txtCCODIGOPRODUCTO.Enabled Then
                    txtCCODIGOPRODUCTO.Focus()
                    txtCCODIGOPRODUCTO.SelectAll()
                End If
                Validar = False
            End If
        ElseIf Not Inicio.ValidandoCampo(Trim(txtCNOMBREPRODUCTO.Text), TipoDato.TdCadena) Then
            MsgBox("El Nombre del Servicio No Puede Ser Vacio", vbInformation, "Aviso" & Me.Text)
            If txtCNOMBREPRODUCTO.Enabled Then
                txtCNOMBREPRODUCTO.Focus()
                txtCNOMBREPRODUCTO.SelectAll()
            End If
            Validar = False
        ElseIf Not Inicio.ValidandoCampo(Trim(txtCIDPRODUCTO.Text), TipoDato.TdNumerico) Then
            MsgBox("El Codigo de Servicio no Puede ser Vacio", vbInformation, "Aviso" & Me.Text)
            If txtCIDPRODUCTO.Enabled Then
                txtCIDPRODUCTO.Focus()
                txtCIDPRODUCTO.SelectAll()
            End If
            Validar = False
        End If
    End Function

    'Private Sub txtidAlmacen_EnabledChanged(sender As Object, e As EventArgs) Handles txtidAlmacen.EnabledChanged
    '    cmdBuscaClave.Enabled = txtidAlmacen.Enabled
    'End Sub
    'CONTROLES
    Private Sub txtidAlmacen_KeyDown(sender As Object, e As KeyEventArgs) Handles txtCCODIGOPRODUCTO.KeyDown
        If e.KeyCode = Keys.F3 Then
            cmdBuscaClave_Click(sender, e)
        End If

    End Sub
    Private Sub txtCCODIGOPRODUCTO_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCCODIGOPRODUCTO.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If txtCCODIGOPRODUCTO.Text.Trim <> "" Then
                CargaForm(txtCCODIGOPRODUCTO.Text, UCase(TablaBd))
            End If
            e.Handled = True
        End If
    End Sub

    Private Sub cmdBuscaClave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBuscaClave.Click
        Dim idEst As String = DespliegaGrid(UCase(TablaBd), Inicio.CONSTR, CampoLLave)
        If idEst <> "" Then CargaForm(idEst, UCase(TablaBd))
    End Sub

    'Private Sub txtidSucursal_EnabledChanged(sender As Object, e As EventArgs) Handles txtCIDPRODUCTO.EnabledChanged
    '    cmdBuscaClaveTPS.Enabled = txtCIDPRODUCTO.Enabled
    'End Sub

    Private Sub txtidSucursal_KeyDown(sender As Object, e As KeyEventArgs) Handles txtCIDPRODUCTO.KeyDown
        If e.KeyCode = Keys.F3 Then
            'cmdBuscaClaveDiv_Click(sender, e)
            'cmdBuscaClaveTPS_Click(sender, e)
        End If
    End Sub

    Private Sub txtidSucursal_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCIDPRODUCTO.KeyPress
        If txtCIDPRODUCTO.Text.Length = 0 Then
            txtCIDPRODUCTO.Focus()
            txtCIDPRODUCTO.SelectAll()
        Else
            If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
                Salida = True
                'CargaForm(txtCIDPRODUCTO.Text, TablaBd2)

                'txtNomServicio.Focus()
                'txtNomServicio.SelectAll()
                Salida = False
            End If
        End If
    End Sub

    Private Sub txtidSucursal_Leave(sender As Object, e As EventArgs) Handles txtCIDPRODUCTO.Leave
        If Salida Then Exit Sub
        If txtCIDPRODUCTO.Text.Length = 0 Then
            txtCIDPRODUCTO.Focus()
            txtCIDPRODUCTO.SelectAll()
        Else
            If txtCIDPRODUCTO.Enabled Then
                'CargaForm(txtCIDPRODUCTO.Text, UCase(TablaBd2))

            End If
        End If
    End Sub

    Private Sub txtidSucursal_MouseDown(sender As Object, e As MouseEventArgs) Handles txtCIDPRODUCTO.MouseDown
        txtCIDPRODUCTO.Focus()
        txtCIDPRODUCTO.SelectAll()
    End Sub


    Private Sub cmdCancelar_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        With TryCast(sender, Button)
            If .Enabled Then
                Me.CancelButton = sender
            End If
        End With
    End Sub
    Private Sub cmdTerminar_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        With TryCast(sender, Button)
            If .Enabled Then
                Me.CancelButton = sender
            End If
        End With
    End Sub

    Private Sub btnMnuOk_Click(sender As Object, e As EventArgs) Handles btnMnuOk.Click
        Dim CadCam As String = ""
        Try
            Salida = True
            If Not Validar() Then Exit Sub
            If GridTodos.RowCount = 0 Then
                CatServFact = New CatServFactClass("")
            End If
            With CatServFact
                If OpcionForma = TipoOpcionForma.tOpcModificar Then
                    CadCam = .GetCambios(txtCIDPRODUCTO.Text, txtCNOMBREPRODUCTO.Text, cmbTipoViaje.SelectedValue, txtidEstado.Text, chkActivo.Checked)
                    If CadCam <> "" Then
                        'If MessageBox.Show("Esta completamente Seguro que Desea guardar los Cambios Realizados al Estado Con id: " & .IDMARCA & vbNewLine &
                        '                CadCam, "Confirma que desea guardar los Cambios Realizados???", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then Exit Sub
                        Status("Guardando Modificaciones del Servicio Con id: " & .CCODIGOPRODUCTO, Me, 1, 2)

                        .Guardar(bEsIdentidad, txtCIDPRODUCTO.Text, txtCNOMBREPRODUCTO.Text, cmbTipoViaje.SelectedValue, txtidEstado.Text, chkActivo.Checked)
                        Status("Servicio con Id: " & .CCODIGOPRODUCTO & " Modificado Con Exito!!!", Me, 2, 2)
                        MessageBox.Show("Servicio con Id: " & .CCODIGOPRODUCTO & " Modificada con exito!!!",
                                        "Accion terminada con exito!!!", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
                        ActualizaGrid(txtMenuBusca.Text)
                    Else
                        Status("No se detectaron cambios para realizar!!!", Me, Err:=True)
                    End If
                ElseIf OpcionForma = TipoOpcionForma.tOpcInsertar Then
                    .Existe = False
                    .CNOMBREPRODUCTO = txtCNOMBREPRODUCTO.Text
                    If Not bEsIdentidad Then
                        .CCODIGOPRODUCTO = txtCCODIGOPRODUCTO.Text

                    End If
                    Status("Guardando Nuevo Servicio: " & txtCNOMBREPRODUCTO.Text & ". . .", Me, 1, 2)
                    .Guardar(bEsIdentidad, txtCIDPRODUCTO.Text, txtCNOMBREPRODUCTO.Text, cmbTipoViaje.SelectedValue, txtidEstado.Text, chkActivo.Checked)
                    Status("Servicio: " & txtCNOMBREPRODUCTO.Text & " Guardado con Exito!!!", Me, 2, 2)
                    MessageBox.Show("Nuevo Servicio: " & txtCNOMBREPRODUCTO.Text & " Guardado con exito!!!",
                                    "Accion terminada con exito!!!", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
                    ActualizaGrid(txtMenuBusca.Text)

                End If
            End With
            Salida = False
        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try
    End Sub


    Private Sub btnMnuCancelar_Click(sender As Object, e As EventArgs) Handles btnMnuCancelar.Click
        If txtCCODIGOPRODUCTO.Text <> "" Then
            CargaForm(txtCCODIGOPRODUCTO.Text, UCase(TablaBd))
        End If
        OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
    End Sub

    Private Sub btnMnuSalir_Click(sender As Object, e As EventArgs) Handles btnMnuSalir.Click
        Me.Close()
    End Sub

    Private Sub txtMenuBusca_TextChanged(sender As Object, e As EventArgs) Handles txtMenuBusca.TextChanged
        ActualizaGrid(txtMenuBusca.Text)
    End Sub









    Private Sub GrupoCaptura_Enter(sender As Object, e As EventArgs) Handles GrupoCaptura.Enter

    End Sub

    Private Function LlenaCombos() As Boolean
        Dim StrSql As String = ""
        If Inicio.CONSTR = "" Then
            Inicio.CONSTR = Me.con
        End If
        LlenaCombos = True
        Try
            DsCombo.Clear()

            StrSql = "SELECT 'F' AS TipoViaje, 'FULL' AS NomTipoViaje " &
            "UNION " &
            "SELECT 'S' AS TipoViaje, 'SENCILLO' AS NomTipoViaje " &
            "union " &
            "SELECT 'A' AS TipoViaje, 'AMBOS' AS NomTipoViaje"

            'StrSql = "select 'ACT' as IdEstatus, 'ACTIVO' as Estatus " &
            '"union " &
            '"select 'INA' as IdEstatus, 'INACTIVO' as Estatus"

            DsCombo = Inicio.LlenaCombos("TipoViaje", "NomTipoViaje", "CatTipoViaje",
                               TipoConexion.TcSQL, Inicio.CONSTR, Process.GetCurrentProcess.ProcessName,
                               KEY_CHECAERROR, KEY_ESTATUS, , StrSql)
            'DsCombo = BD.ExecuteReturn(StrSql)
            If Not DsCombo Is Nothing Then
                If DsCombo.Tables("CatTipoViaje").DefaultView.Count > 0 Then
                    cmbTipoViaje.DataSource = DsCombo.Tables("CatTipoViaje")
                    cmbTipoViaje.ValueMember = "TipoViaje"
                    cmbTipoViaje.DisplayMember = "NomTipoViaje"
                End If
            Else
                MsgBox("No se han dado de alta a Tipo de Viaje ", vbInformation, "Aviso" & Me.Text)
                LlenaCombos = False
                Exit Function
            End If
        Catch ex As Exception

        End Try
    End Function


    Private Sub txtDescripcion_MouseDown(sender As Object, e As MouseEventArgs) Handles txtCNOMBREPRODUCTO.MouseDown
        txtCNOMBREPRODUCTO.Focus()
        txtCNOMBREPRODUCTO.SelectAll()
    End Sub





    Private Sub cmbEstatus_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbTipoViaje.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            txtidEstado.Focus()
            txtidEstado.SelectAll()
            'btnMnuOk_Click(sender, e)
        End If
    End Sub

    Private Sub txtidTipoServicio_TextChanged(sender As Object, e As EventArgs) Handles txtCIDPRODUCTO.TextChanged

    End Sub

    Private Sub cmbEstatus_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbTipoViaje.SelectedIndexChanged

    End Sub

    Private Sub txtidDivision_EnabledChanged(sender As Object, e As EventArgs) Handles txtidEstado.EnabledChanged
        cmdBuscaEdo.Enabled = txtidEstado.Enabled
    End Sub

    Private Sub txtidDivision_KeyDown(sender As Object, e As KeyEventArgs) Handles txtidEstado.KeyDown
        If e.KeyCode = Keys.F3 Then
            cmdBuscaClaveDiv_Click(sender, e)
        End If
    End Sub

    Private Sub txtidDivision_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtidEstado.KeyPress
        If txtidEstado.Text.Length = 0 Then
            txtidEstado.Focus()
            txtidEstado.SelectAll()
        Else
            If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
                Salida = True
                CargaForm(txtidEstado.Text, UCase("Division"))
                btnMnuOk_Click(sender, e)

                'txtNomServicio.Focus()
                'txtNomServicio.SelectAll()
                Salida = False
            End If
        End If
    End Sub

    Private Sub txtidDivision_Leave(sender As Object, e As EventArgs) Handles txtidEstado.Leave
        If Salida Then Exit Sub
        If txtidEstado.Text.Length = 0 Then
            txtidEstado.Focus()
            txtidEstado.SelectAll()
        Else
            If txtCIDPRODUCTO.Enabled Then
                CargaForm(txtidEstado.Text, UCase("Division"))

            End If
        End If
    End Sub

    Private Sub txtidDivision_MouseDown(sender As Object, e As MouseEventArgs) Handles txtidEstado.MouseDown
        txtidEstado.Focus()
        txtidEstado.SelectAll()
    End Sub

    Private Sub txtidDivision_TextChanged(sender As Object, e As EventArgs) Handles txtidEstado.TextChanged

    End Sub

    Private Sub cmdBuscaClaveDiv_Click(sender As Object, e As EventArgs) Handles cmdBuscaEdo.Click
        txtidEstado.Text = DespliegaGrid(UCase("Catdivision"), Inicio.CONSTR, CampoLLave)
        txtidDivision_Leave(sender, e)
    End Sub
End Class
