'Fecha: 18 / Febrero / 2016
'ROGER GALA PACHECO
'************************************************************************************************************************************
'*  Fecha Modificacion :   18 / Febrero / 2016                                                                                       
'*  
'************************************************************************************************************************************
Public Class frmCatActividad
    Inherits System.Windows.Forms.Form
    Dim OpcionForma As TipoOpcionForma
    Dim TablaBd As String = "CatActividades"
    Dim CampoLLave As String = "idActividad"
    Dim TablaBd2 As String = "CatFamilia"
    Public WithEvents MeStatus1 As ControlStatus.MeStatus
    Friend WithEvents GridTodos As System.Windows.Forms.DataGridView
    Private Actividad As ActividadesClass
    Private Ciudad As FamiliasClass
    Friend WithEvents btnMnuOk As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuCancelar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuSalir As System.Windows.Forms.ToolStripButton
    Private _Tag As String
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cmdBuscaClaveFam As System.Windows.Forms.Button
    Friend WithEvents txtidFamilia As System.Windows.Forms.TextBox
    Friend WithEvents txtNomFamilia As System.Windows.Forms.TextBox
    Dim bEsIdentidad As Boolean = True
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtNomDivision As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtCostoManoObra As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtDuracionHoras As System.Windows.Forms.TextBox
    Dim Salida As Boolean
    Private _con As String
    Private _v As Boolean



#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents GrupoCaptura As System.Windows.Forms.GroupBox
    Friend WithEvents ToolStripMenu As System.Windows.Forms.ToolStrip
    Friend WithEvents txtMenuBusca As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents btnMnuAltas As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuModificar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuEliminar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuReporte As System.Windows.Forms.ToolStripButton
    Friend WithEvents label1 As System.Windows.Forms.Label
    Friend WithEvents label2 As System.Windows.Forms.Label
    Friend WithEvents cmdBuscaClave As System.Windows.Forms.Button
    Friend WithEvents txtidActividad As System.Windows.Forms.TextBox
    Friend WithEvents txtNombreAct As System.Windows.Forms.TextBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCatActividad))
        Me.txtidActividad = New System.Windows.Forms.TextBox()
        Me.label1 = New System.Windows.Forms.Label()
        Me.txtNombreAct = New System.Windows.Forms.TextBox()
        Me.label2 = New System.Windows.Forms.Label()
        Me.GrupoCaptura = New System.Windows.Forms.GroupBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtDuracionHoras = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtNomDivision = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtCostoManoObra = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cmdBuscaClaveFam = New System.Windows.Forms.Button()
        Me.txtidFamilia = New System.Windows.Forms.TextBox()
        Me.txtNomFamilia = New System.Windows.Forms.TextBox()
        Me.cmdBuscaClave = New System.Windows.Forms.Button()
        Me.ToolStripMenu = New System.Windows.Forms.ToolStrip()
        Me.txtMenuBusca = New System.Windows.Forms.ToolStripTextBox()
        Me.btnMnuAltas = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuModificar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuEliminar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuReporte = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuOk = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuCancelar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuSalir = New System.Windows.Forms.ToolStripButton()
        Me.MeStatus1 = New ControlStatus.MeStatus()
        Me.GridTodos = New System.Windows.Forms.DataGridView()
        Me.GrupoCaptura.SuspendLayout()
        Me.ToolStripMenu.SuspendLayout()
        CType(Me.GridTodos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtidActividad
        '
        Me.txtidActividad.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtidActividad.Location = New System.Drawing.Point(122, 24)
        Me.txtidActividad.MaxLength = 3
        Me.txtidActividad.Name = "txtidActividad"
        Me.txtidActividad.Size = New System.Drawing.Size(69, 20)
        Me.txtidActividad.TabIndex = 4
        '
        'label1
        '
        Me.label1.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.label1.Location = New System.Drawing.Point(6, 27)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(94, 20)
        Me.label1.TabIndex = 14
        Me.label1.Text = "id Actividad:"
        '
        'txtNombreAct
        '
        Me.txtNombreAct.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNombreAct.Location = New System.Drawing.Point(122, 56)
        Me.txtNombreAct.MaxLength = 50
        Me.txtNombreAct.Name = "txtNombreAct"
        Me.txtNombreAct.Size = New System.Drawing.Size(322, 20)
        Me.txtNombreAct.TabIndex = 6
        '
        'label2
        '
        Me.label2.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.label2.Location = New System.Drawing.Point(9, 59)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(91, 20)
        Me.label2.TabIndex = 16
        Me.label2.Text = "Nombre:"
        '
        'GrupoCaptura
        '
        Me.GrupoCaptura.Controls.Add(Me.Label6)
        Me.GrupoCaptura.Controls.Add(Me.txtDuracionHoras)
        Me.GrupoCaptura.Controls.Add(Me.Label4)
        Me.GrupoCaptura.Controls.Add(Me.txtNomDivision)
        Me.GrupoCaptura.Controls.Add(Me.Label5)
        Me.GrupoCaptura.Controls.Add(Me.txtCostoManoObra)
        Me.GrupoCaptura.Controls.Add(Me.Label3)
        Me.GrupoCaptura.Controls.Add(Me.cmdBuscaClaveFam)
        Me.GrupoCaptura.Controls.Add(Me.txtidFamilia)
        Me.GrupoCaptura.Controls.Add(Me.txtNomFamilia)
        Me.GrupoCaptura.Controls.Add(Me.label1)
        Me.GrupoCaptura.Controls.Add(Me.label2)
        Me.GrupoCaptura.Controls.Add(Me.cmdBuscaClave)
        Me.GrupoCaptura.Controls.Add(Me.txtidActividad)
        Me.GrupoCaptura.Controls.Add(Me.txtNombreAct)
        Me.GrupoCaptura.Location = New System.Drawing.Point(300, 45)
        Me.GrupoCaptura.Name = "GrupoCaptura"
        Me.GrupoCaptura.Size = New System.Drawing.Size(524, 222)
        Me.GrupoCaptura.TabIndex = 3
        Me.GrupoCaptura.TabStop = False
        '
        'Label6
        '
        Me.Label6.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label6.Location = New System.Drawing.Point(6, 111)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(94, 20)
        Me.Label6.TabIndex = 30
        Me.Label6.Text = "Duraci�n Horas:"
        '
        'txtDuracionHoras
        '
        Me.txtDuracionHoras.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDuracionHoras.Location = New System.Drawing.Point(122, 108)
        Me.txtDuracionHoras.MaxLength = 6
        Me.txtDuracionHoras.Name = "txtDuracionHoras"
        Me.txtDuracionHoras.Size = New System.Drawing.Size(109, 20)
        Me.txtDuracionHoras.TabIndex = 8
        Me.txtDuracionHoras.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label4
        '
        Me.Label4.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label4.Location = New System.Drawing.Point(6, 175)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(94, 20)
        Me.Label4.TabIndex = 28
        Me.Label4.Text = "Division:"
        '
        'txtNomDivision
        '
        Me.txtNomDivision.Enabled = False
        Me.txtNomDivision.Location = New System.Drawing.Point(122, 172)
        Me.txtNomDivision.Name = "txtNomDivision"
        Me.txtNomDivision.ReadOnly = True
        Me.txtNomDivision.Size = New System.Drawing.Size(275, 20)
        Me.txtNomDivision.TabIndex = 12
        '
        'Label5
        '
        Me.Label5.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label5.Location = New System.Drawing.Point(6, 85)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(110, 20)
        Me.Label5.TabIndex = 26
        Me.Label5.Text = "Costo Mano Obra $:"
        '
        'txtCostoManoObra
        '
        Me.txtCostoManoObra.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCostoManoObra.Location = New System.Drawing.Point(122, 82)
        Me.txtCostoManoObra.MaxLength = 6
        Me.txtCostoManoObra.Name = "txtCostoManoObra"
        Me.txtCostoManoObra.Size = New System.Drawing.Size(109, 20)
        Me.txtCostoManoObra.TabIndex = 7
        Me.txtCostoManoObra.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label3
        '
        Me.Label3.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label3.Location = New System.Drawing.Point(6, 145)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(94, 20)
        Me.Label3.TabIndex = 20
        Me.Label3.Text = "Id Familia:"
        '
        'cmdBuscaClaveFam
        '
        Me.cmdBuscaClaveFam.Image = CType(resources.GetObject("cmdBuscaClaveFam.Image"), System.Drawing.Image)
        Me.cmdBuscaClaveFam.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdBuscaClaveFam.Location = New System.Drawing.Point(199, 134)
        Me.cmdBuscaClaveFam.Name = "cmdBuscaClaveFam"
        Me.cmdBuscaClaveFam.Size = New System.Drawing.Size(32, 32)
        Me.cmdBuscaClaveFam.TabIndex = 10
        Me.cmdBuscaClaveFam.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'txtidFamilia
        '
        Me.txtidFamilia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtidFamilia.Location = New System.Drawing.Point(122, 142)
        Me.txtidFamilia.MaxLength = 3
        Me.txtidFamilia.Name = "txtidFamilia"
        Me.txtidFamilia.Size = New System.Drawing.Size(69, 20)
        Me.txtidFamilia.TabIndex = 9
        '
        'txtNomFamilia
        '
        Me.txtNomFamilia.Enabled = False
        Me.txtNomFamilia.Location = New System.Drawing.Point(237, 146)
        Me.txtNomFamilia.Name = "txtNomFamilia"
        Me.txtNomFamilia.ReadOnly = True
        Me.txtNomFamilia.Size = New System.Drawing.Size(275, 20)
        Me.txtNomFamilia.TabIndex = 11
        '
        'cmdBuscaClave
        '
        Me.cmdBuscaClave.Image = CType(resources.GetObject("cmdBuscaClave.Image"), System.Drawing.Image)
        Me.cmdBuscaClave.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdBuscaClave.Location = New System.Drawing.Point(199, 16)
        Me.cmdBuscaClave.Name = "cmdBuscaClave"
        Me.cmdBuscaClave.Size = New System.Drawing.Size(32, 32)
        Me.cmdBuscaClave.TabIndex = 5
        Me.cmdBuscaClave.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'ToolStripMenu
        '
        Me.ToolStripMenu.AutoSize = False
        Me.ToolStripMenu.BackColor = System.Drawing.Color.Silver
        Me.ToolStripMenu.Dock = System.Windows.Forms.DockStyle.None
        Me.ToolStripMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.txtMenuBusca, Me.btnMnuAltas, Me.btnMnuModificar, Me.btnMnuEliminar, Me.btnMnuReporte, Me.btnMnuOk, Me.btnMnuCancelar, Me.btnMnuSalir})
        Me.ToolStripMenu.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow
        Me.ToolStripMenu.Location = New System.Drawing.Point(3, 0)
        Me.ToolStripMenu.Name = "ToolStripMenu"
        Me.ToolStripMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.ToolStripMenu.Size = New System.Drawing.Size(848, 42)
        Me.ToolStripMenu.TabIndex = 1
        Me.ToolStripMenu.Text = "ToolStrip1"
        '
        'txtMenuBusca
        '
        Me.txtMenuBusca.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.txtMenuBusca.Name = "txtMenuBusca"
        Me.txtMenuBusca.Size = New System.Drawing.Size(150, 42)
        Me.txtMenuBusca.ToolTipText = "Filtra Listado"
        '
        'btnMnuAltas
        '
        Me.btnMnuAltas.ForeColor = System.Drawing.Color.Red
        Me.btnMnuAltas.Image = CType(resources.GetObject("btnMnuAltas.Image"), System.Drawing.Image)
        Me.btnMnuAltas.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuAltas.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuAltas.Name = "btnMnuAltas"
        Me.btnMnuAltas.Size = New System.Drawing.Size(46, 39)
        Me.btnMnuAltas.Text = "&ALTAS"
        Me.btnMnuAltas.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnMnuAltas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuModificar
        '
        Me.btnMnuModificar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuModificar.Image = CType(resources.GetObject("btnMnuModificar.Image"), System.Drawing.Image)
        Me.btnMnuModificar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuModificar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuModificar.Name = "btnMnuModificar"
        Me.btnMnuModificar.Size = New System.Drawing.Size(74, 39)
        Me.btnMnuModificar.Text = "&MODIFICAR"
        Me.btnMnuModificar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnMnuModificar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuEliminar
        '
        Me.btnMnuEliminar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuEliminar.Image = CType(resources.GetObject("btnMnuEliminar.Image"), System.Drawing.Image)
        Me.btnMnuEliminar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuEliminar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuEliminar.Name = "btnMnuEliminar"
        Me.btnMnuEliminar.Size = New System.Drawing.Size(64, 39)
        Me.btnMnuEliminar.Text = "&ELIMINAR"
        Me.btnMnuEliminar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuReporte
        '
        Me.btnMnuReporte.ForeColor = System.Drawing.Color.Red
        Me.btnMnuReporte.Image = CType(resources.GetObject("btnMnuReporte.Image"), System.Drawing.Image)
        Me.btnMnuReporte.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuReporte.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuReporte.Name = "btnMnuReporte"
        Me.btnMnuReporte.Size = New System.Drawing.Size(60, 39)
        Me.btnMnuReporte.Text = "&REPORTE"
        Me.btnMnuReporte.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuOk
        '
        Me.btnMnuOk.ForeColor = System.Drawing.Color.Red
        Me.btnMnuOk.Image = CType(resources.GetObject("btnMnuOk.Image"), System.Drawing.Image)
        Me.btnMnuOk.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuOk.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuOk.Name = "btnMnuOk"
        Me.btnMnuOk.Size = New System.Drawing.Size(56, 39)
        Me.btnMnuOk.Text = "&GRABAR"
        Me.btnMnuOk.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuCancelar
        '
        Me.btnMnuCancelar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuCancelar.Image = CType(resources.GetObject("btnMnuCancelar.Image"), System.Drawing.Image)
        Me.btnMnuCancelar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuCancelar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuCancelar.Name = "btnMnuCancelar"
        Me.btnMnuCancelar.Size = New System.Drawing.Size(71, 39)
        Me.btnMnuCancelar.Text = "&CANCELAR"
        Me.btnMnuCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuSalir
        '
        Me.btnMnuSalir.ForeColor = System.Drawing.Color.Red
        Me.btnMnuSalir.Image = CType(resources.GetObject("btnMnuSalir.Image"), System.Drawing.Image)
        Me.btnMnuSalir.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuSalir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuSalir.Name = "btnMnuSalir"
        Me.btnMnuSalir.Size = New System.Drawing.Size(41, 39)
        Me.btnMnuSalir.Text = "&SALIR"
        Me.btnMnuSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'MeStatus1
        '
        Me.MeStatus1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.MeStatus1.Location = New System.Drawing.Point(0, 266)
        Me.MeStatus1.Margin = New System.Windows.Forms.Padding(4)
        Me.MeStatus1.Name = "MeStatus1"
        Me.MeStatus1.Size = New System.Drawing.Size(836, 26)
        Me.MeStatus1.TabIndex = 72
        '
        'GridTodos
        '
        Me.GridTodos.AllowUserToAddRows = False
        Me.GridTodos.AllowUserToDeleteRows = False
        Me.GridTodos.AllowUserToResizeRows = False
        Me.GridTodos.BackgroundColor = System.Drawing.Color.White
        Me.GridTodos.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.GridTodos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.GridTodos.Location = New System.Drawing.Point(3, 45)
        Me.GridTodos.MultiSelect = False
        Me.GridTodos.Name = "GridTodos"
        Me.GridTodos.ReadOnly = True
        Me.GridTodos.RowHeadersVisible = False
        Me.GridTodos.Size = New System.Drawing.Size(291, 222)
        Me.GridTodos.TabIndex = 73
        '
        'frmCatActividad
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(836, 292)
        Me.Controls.Add(Me.GridTodos)
        Me.Controls.Add(Me.MeStatus1)
        Me.Controls.Add(Me.ToolStripMenu)
        Me.Controls.Add(Me.GrupoCaptura)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmCatActividad"
        Me.Text = "Catal�go de Actividades"
        Me.GrupoCaptura.ResumeLayout(False)
        Me.GrupoCaptura.PerformLayout()
        Me.ToolStripMenu.ResumeLayout(False)
        Me.ToolStripMenu.PerformLayout()
        CType(Me.GridTodos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region
    'Sub New(ByVal vTag As String)
    '    InitializeComponent()
    '    _Tag = vTag
    'End Sub
    Public Sub New(con As String, v As Boolean)
        _con = con
        BD = New CapaDatos.UtilSQL(con, "Juan")
        InitializeComponent()
        _v = v
    End Sub
#Region "MenusToolStrip"
    Private Sub LimpiaVariables()
        SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")

    End Sub
    Private Sub LimpiaCampos()
        txtidActividad.Text = ""
        txtNombreAct.Text = ""
        txtidFamilia.Text = ""
        txtNomFamilia.Text = ""
        txtCostoManoObra.Text = ""
        txtNomDivision.Text = ""
        txtDuracionHoras.Text = ""
        LimpiaVariables()
    End Sub

    Private Sub ActivaCampos(ByVal valor As Boolean, ByVal vOpcion As TipoOpcActivaCampos)
        If vOpcion = TipoOpcActivaCampos.tOpcDESHABTODOS Then
            txtidActividad.Enabled = False
            txtNombreAct.Enabled = False
            txtidFamilia.Enabled = False
            txtCostoManoObra.Enabled = False
            txtDuracionHoras.Enabled = False
        ElseIf vOpcion = TipoOpcActivaCampos.tOpcINICIALIZA Then
            txtidActividad.Enabled = Not valor
            txtNombreAct.Enabled = valor
            txtidFamilia.Enabled = valor
            txtCostoManoObra.Enabled = valor
            txtDuracionHoras.Enabled = valor
        End If
    End Sub


    Private Sub ActivaBotones(ByVal valor As Boolean, Optional ByVal vOpcion As TipoOpcActivaBoton = TipoOpcActivaBoton.tOpcInicializa)
        If vOpcion = TipoOpcActivaBoton.tOpcConsulta Then
            btnMnuOk.Enabled = False
            btnMnuCancelar.Enabled = valor
            btnMnuSalir.Enabled = Not valor

            btnMnuAltas.Enabled = Not valor
            btnMnuModificar.Enabled = Not valor
            btnMnuEliminar.Enabled = Not valor
            btnMnuReporte.Enabled = Not valor
        ElseIf vOpcion = TipoOpcActivaBoton.tOpcInicializa Then
            btnMnuOk.Enabled = valor
            btnMnuCancelar.Enabled = valor
            btnMnuSalir.Enabled = Not valor

            btnMnuAltas.Enabled = Not valor
            btnMnuModificar.Enabled = Not valor
            btnMnuEliminar.Enabled = Not valor
            btnMnuReporte.Enabled = Not valor

            txtMenuBusca.Enabled = Not valor
            GridTodos.Enabled = Not valor
        ElseIf vOpcion = TipoOpcActivaBoton.tOpcInsertar Then
            btnMnuOk.Enabled = valor
            btnMnuCancelar.Enabled = valor
            btnMnuSalir.Enabled = Not valor

            btnMnuAltas.Enabled = Not valor
            btnMnuModificar.Enabled = Not valor
            btnMnuEliminar.Enabled = Not valor
            btnMnuReporte.Enabled = Not valor
        End If
    End Sub

    Private Sub btnMnuAltas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuAltas.Click
        'ALTAS
        '15/FEB/2016
        'If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Alta) = False Then
        '    MessageBox.Show("No tiene los permisos Necesarios para poder dar de Alta en el Catalogo de Actividades!!!",
        '        "Acceso Denegado a Alta de Actividades", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    OpcionForma = TipoOpcionForma.tOpcConsultar
        '    Exit Sub
        'End If
        OpcionForma = TipoOpcionForma.tOpcInsertar
        LimpiaCampos()
        ActivaCampos(True, TipoOpcActivaCampos.tOpcINICIALIZA)
        ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)

        If bEsIdentidad Then
            txtidActividad.Enabled = False
            txtNombreAct.Focus()
        Else
            txtidActividad.Focus()
        End If
        GridTodos.Enabled = False
        Status("Ingrese nueva Actividad para guardar", Me)

    End Sub
    Private Sub btnMnuModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuModificar.Click
        'MODIFICAR
        'If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Modificar) = False Then
        '    MessageBox.Show("No tiene los permisos Necesarios para poder Modificar la Informacion de la Actividad!!!",
        '           "Acceso Denegado a Modificacion de la Informacion de la Actividad", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    OpcionForma = TipoOpcionForma.tOpcConsultar
        '    Exit Sub
        'End If
        If Trim(txtidActividad.Text) = "" Then
            txtidActividad.Text = DespliegaGrid(TablaBd, Inicio.CONSTR, CampoLLave)
            CargaForm(txtidActividad.Text, UCase(TablaBd))
        End If

        OpcionForma = TipoOpcionForma.tOpcModificar
        ActivaCampos(True, TipoOpcActivaCampos.tOpcINICIALIZA)
        ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)

        btnMnuEliminar.Enabled = True
        If bEsIdentidad Then
            txtidActividad.Enabled = False
            txtNombreAct.Focus()
        Else
            txtidActividad.Focus()
        End If
        GridTodos.Enabled = False
        Status("Ingrese Actividad para guardar", Me)
    End Sub
    Private Sub btnMnuEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuEliminar.Click
        'ELIMINAR
        'If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Baja) = False Then
        '    MessageBox.Show("No tiene los permisos Necesarios para poder Eliminar la Actividad!!!",
        '        "Acceso Denegado a Eliminacion de Actividad", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    OpcionForma = TipoOpcionForma.tOpcConsultar
        '    Exit Sub
        'End If
        If txtidActividad.Text = Actividad.idActividad Then
            With Actividad
                If MessageBox.Show("Esta completamente seguro que desea Eliminar la Actividad: " &
                                   .NombreAct & " Con Id: " & .idActividad & " ????", "Confirma que desea eliminar la Actividad: " & .NombreAct & "???",
                                 MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then Exit Sub
                Try
                    .Eliminar() 'Elimina el municipio
                    MessageBox.Show("Actividad: " & .NombreAct & " Eliminada con exito!!!",
                                    "Actividad Eliminado con exito", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Status("Actividad: " & .NombreAct & " Eliminado con exito!!!", Me)
                    ActualizaGrid(txtMenuBusca.Text)
                    txtidActividad.Text = ""
                    OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
                Catch ex As Exception
                    Status("Error al tratar de eliminar la Actividad: " & ex.Message, Me, Err:=True)
                    MessageBox.Show("Error al Tratar de eliminar la Actividad: " & .NombreAct & vbNewLine &
                                    "Error. " & ex.Message, "Error: " & Err.Number, MessageBoxButtons.OK, MessageBoxIcon.Error)

                End Try
            End With
        End If
    End Sub
    Private Sub btnMnuReporte_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuReporte.Click
        'REPORTE
        'If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Reporte) = False Then
        '    MessageBox.Show("No tiene los permisos Necesarios para poder realizar Reporte de Actividad!!!",
        '        "Acceso Denegado a Reporte de Actividad", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    OpcionForma = TipoOpcionForma.tOpcConsultar
        '    Exit Sub
        'End If
        ToolStripMenu.Enabled = False
        Status("Abriendo Reporte Espere. . . ", Me)
        Me.Cursor = Cursors.WaitCursor
        DespliegaReporte(TablaBd, CONSTR)
        Status("Finalizo el reporte. . . ", Me)
        Me.Cursor = Cursors.Default
        ToolStripMenu.Enabled = True
    End Sub

#End Region
    Private Sub ActualizaGrid(ByVal vNomAlmacen As String)
        If vNomAlmacen = "" Then
            GridTodos.DataSource = BD.ExecuteReturn("SELECT idActividad, NombreAct FROM " & TablaBd, TablaBd)
            Status(GridTodos.RowCount & " Registros", Me)
        Else
            GridTodos.DataSource = BD.ExecuteReturn("SELECT idActividad, NombreAct FROM " & TablaBd &
                                                " WHERE NombreAct Like '%" & vNomAlmacen & "%'")
            Status(GridTodos.RowCount & " Registros obtenidos con el filtro %" & vNomAlmacen & "%", Me)
        End If
    End Sub

    Private Sub OpcionMnuTools(ByVal vOpcion As TipoOpcionForma)
        If vOpcion <> TipoOpcionForma.tOpcImprimir And vOpcion <> TipoOpcionForma.tOpcEliminar Then
            OpcionForma = vOpcion
        End If
        txtMenuBusca.Enabled = True
        GridTodos.Enabled = True
        If vOpcion = TipoOpcionForma.tOpcConsultar Then
            'CONSULTA
            For Each ct As Control In GrupoCaptura.Controls
                If Not ct Is cmdBuscaClave Then
                    ct.Enabled = False
                Else
                    ct.Enabled = True
                End If
            Next
            txtMenuBusca.Enabled = True
            btnMnuAltas.Enabled = True
            btnMnuModificar.Enabled = True
            btnMnuEliminar.Enabled = False
            btnMnuReporte.Enabled = True
            btnMnuOk.Enabled = False
            btnMnuCancelar.Enabled = False

            btnMnuSalir.Enabled = True


        End If
    End Sub


    Private Sub txtMenuBusca_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtMenuBusca.KeyPress
        If Asc(e.KeyChar) = 39 Then e.Handled = True : Exit Sub 'Caracter ' Evita errores
        If Asc(e.KeyChar) = 13 Then
            GridTodos.Focus()
            e.Handled = True
        End If
    End Sub

    'Private Sub txtMenuBusca_KeyUp(sender As Object, e As KeyEventArgs) Handles txtMenuBusca.KeyUp
    '    If txtMenuBusca.Text.Trim = "" Then Exit Sub
    '    If e.KeyCode = Keys.Down Or e.KeyCode = Keys.Up Then
    '        GridTodos.Focus()
    '        e.Handled = True
    '        Exit Sub
    '    End If
    '    If e.KeyCode = Keys.Left Or e.KeyCode = Keys.Right Then Exit Sub
    '    ActualizaGrid(txtMenuBusca.Text)
    'End Sub



    Private Sub frmCatFamilias_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Inicio.CONSTR = "" Then
            Inicio.CONSTR = _con
        End If
        OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
        ActualizaGrid("")
        GridTodos.Columns(1).Width = GridTodos.Width - 20
        GridTodos.Columns(0).Visible = False
        txtMenuBusca.Focus()
    End Sub
    Private Sub GridTodos_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles GridTodos.CellEnter
        CargaForm(GridTodos.Rows(GridTodos.CurrentRow.Index).Cells(0).Value, UCase(TablaBd))
    End Sub
    Private Sub GridTodos_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles GridTodos.KeyPress
        If Asc(e.KeyChar) = 8 Then
            txtMenuBusca.Focus()
            e.Handled = True
        End If
    End Sub
    Private Sub CargaForm(ByVal idClave As Integer, ByVal NomTabla As String)
        Try
            Select Case NomTabla
                Case UCase(TablaBd)
                    Actividad = New ActividadesClass(idClave)
                    With Actividad
                        txtidActividad.Text = .idActividad
                        If .Existe Then
                            txtNombreAct.Text = .NombreAct
                            txtidFamilia.Text = .idFamilia
                            txtNomFamilia.Text = .NomFamilia
                            txtCostoManoObra.Text = .CostoManoObra
                            txtDuracionHoras.Text = .DuracionHoras
                            txtNomDivision.Text = .NomDivision

                            'txtCIDALMACEN_COM.Text = .CIDALMACEN_COM
                            If OpcionForma = TipoOpcionForma.tOpcInsertar Then
                                Status("La Actividad Con id: " & .idActividad & _
                                       " Ya existe!!!, Favor de modificarlo para poder dar de alta a otra Actividad", Me, Err:=True)
                                MessageBox.Show("La Actividad con id: " & .idActividad & _
                                                " YA EXISTE!!!, Favor de Cambiarlo y volver a intentarlo",
                                                "La Actividad con id: " & .idActividad & " YA EXISTE!!!", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            End If
                        Else 'No existe 
                            If OpcionForma = TipoOpcionForma.tOpcInsertar Then
                                'For Each Ct As Control In GrupoCaptura.Controls
                                '    If Not Ct Is txtidAlmacen And Not Ct Is cmdBuscaClave Then
                                '        Ct.Enabled = True
                                '    Else
                                '        Ct.Enabled = False
                                '    End If
                                '    If TypeOf (Ct) Is TextBox Then
                                '        If Not Ct Is txtidAlmacen Then
                                '            TryCast(Ct, TextBox).Text = ""
                                '        End If
                                '    End If
                                'Next
                                ActivaCampos(True, TipoOpcActivaCampos.tOpcINICIALIZA)
                                ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)
                                btnMnuOk.Enabled = True
                                txtNombreAct.Focus()
                                Status("Listo para Guardar nueva Actividad con id: " & .idActividad, Me)
                            End If
                        End If
                    End With
                Case UCase(TablaBd2)
                    'aqui()
                    Ciudad = New FamiliasClass(idClave)
                    With Ciudad
                        txtidFamilia.Text = .IdFamilia
                        If .Existe Then
                            txtNomFamilia.Text = .NomFamilia
                            txtNomDivision.Text = .NomDivision
                            'txtNomPais.Text = .NomEstado
                        Else
                            MsgBox("La Familia con id: " & txtidFamilia.Text & " No Existe")
                        End If
                    End With

            End Select



        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try

    End Sub
    Private Function Validar() As Boolean
        Validar = True
        If Not Inicio.ValidandoCampo(Trim(txtidActividad.Text), TipoDato.TdNumerico) Then
            If Not bEsIdentidad Then
                MsgBox(MENSAJE_CLAVE, vbInformation, "Aviso" & Me.Text)
                If txtidActividad.Enabled Then
                    txtidActividad.Focus()
                    txtidActividad.SelectAll()
                End If
                Validar = False
            End If
        ElseIf Not Inicio.ValidandoCampo(Trim(txtNombreAct.Text), TipoDato.TdCadena) Then
            MsgBox("El Nombre de la Actividad No Puede Ser Vacio", vbInformation, "Aviso" & Me.Text)
            If txtNombreAct.Enabled Then
                txtNombreAct.Focus()
                txtNombreAct.SelectAll()
            End If
            Validar = False
        ElseIf Not Inicio.ValidandoCampo(Trim(txtidFamilia.Text), TipoDato.TdNumerico) Then
            MsgBox("La Familia de la Actividad no Puede ser Vacio", vbInformation, "Aviso" & Me.Text)
            If txtidFamilia.Enabled Then
                txtidFamilia.Focus()
                txtidFamilia.SelectAll()
            End If
            Validar = False
        End If
    End Function

    'Private Sub txtidAlmacen_EnabledChanged(sender As Object, e As EventArgs) Handles txtidAlmacen.EnabledChanged
    '    cmdBuscaClave.Enabled = txtidAlmacen.Enabled
    'End Sub
    'CONTROLES
    Private Sub txtidAlmacen_KeyDown(sender As Object, e As KeyEventArgs) Handles txtidActividad.KeyDown
        If e.KeyCode = Keys.F3 Then
            cmdBuscaClave_Click(sender, e)
        End If

    End Sub
    Private Sub txtidAlmacen_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtidActividad.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If txtidActividad.Text.Trim <> "" Then
                CargaForm(txtidActividad.Text, UCase(TablaBd))
            End If
            e.Handled = True
        End If
    End Sub
    Private Sub txtNombreAct_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtNombreAct.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            'btnMnuOk_Click(sender, e)
            'txtidCiudad.Focus()
            'txtidCiudad.SelectAll()
            txtCostoManoObra.Focus()
            txtCostoManoObra.SelectAll()
        End If
    End Sub

    Private Sub cmdBuscaClave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBuscaClave.Click
        Dim idEst As String = DespliegaGrid(UCase(TablaBd), Inicio.CONSTR, CampoLLave)
        If idEst <> "" Then CargaForm(idEst, UCase(TablaBd))
    End Sub

    Private Sub txtNombreAct_MouseDown(sender As Object, e As MouseEventArgs) Handles txtNombreAct.MouseDown
        txtNombreAct.Focus()
        txtNombreAct.SelectAll()
    End Sub

    Private Sub txtidSucursal_EnabledChanged(sender As Object, e As EventArgs) Handles txtidFamilia.EnabledChanged
        cmdBuscaClaveFam.Enabled = txtidFamilia.Enabled
    End Sub

    Private Sub txtidSucursal_KeyDown(sender As Object, e As KeyEventArgs) Handles txtidFamilia.KeyDown
        If e.KeyCode = Keys.F3 Then
            cmdBuscaClaveFam_Click(sender, e)

            'cmdBusccmvaClaveDiv_Click(sender, e)
        End If
    End Sub

    Private Sub txtidSucursal_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtidFamilia.KeyPress
        '    btnMnuOk_Click(sender, e)
        '
        'If Asc(e.KeyChar) = 13 Then
        '    If txtidSucursal.Text.Trim <> "" Then
        '        CargaForm(txtidSucursal.Text, TablaBd2)
        '        txtNomSucursal.Focus()
        '        txtNomSucursal.SelectAll()
        '    End If
        '    e.Handled = True
        'End If
        If txtidFamilia.Text.Length = 0 Then
            txtidFamilia.Focus()
        Else
            If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
                Salida = True
                CargaForm(txtidFamilia.Text, UCase(TablaBd2))
                Salida = False
            End If
        End If
    End Sub

    Private Sub txtidSucursal_Leave(sender As Object, e As EventArgs) Handles txtidFamilia.Leave
        If Salida Then Exit Sub
        If txtidFamilia.Text.Length = 0 Then
            txtidFamilia.Focus()
            txtidFamilia.SelectAll()
        Else
            If txtidFamilia.Enabled Then
                CargaForm(txtidFamilia.Text, UCase(TablaBd2))
                'txtCIDALMACEN_COM.Focus()
                'txtCIDALMACEN_COM.SelectAll()
            End If
        End If
    End Sub

    Private Sub txtidSucursal_MouseDown(sender As Object, e As MouseEventArgs) Handles txtidFamilia.MouseDown
        txtidFamilia.Focus()
        txtidFamilia.SelectAll()
    End Sub

    Private Sub cmdBuscaClaveFam_Click(sender As Object, e As EventArgs) Handles cmdBuscaClaveFam.Click
        txtidFamilia.Text = DespliegaGrid(UCase(TablaBd2), Inicio.CONSTR, CampoLLave)
        txtidSucursal_Leave(sender, e)
    End Sub

    Private Sub cmdCancelar_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        With TryCast(sender, Button)
            If .Enabled Then
                Me.CancelButton = sender
            End If
        End With
    End Sub
    Private Sub cmdTerminar_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        With TryCast(sender, Button)
            If .Enabled Then
                Me.CancelButton = sender
            End If
        End With
    End Sub

    Private Sub btnMnuOk_Click(sender As Object, e As EventArgs) Handles btnMnuOk.Click
        Dim CadCam As String = ""
        Try
            Salida = True
            If Not Validar() Then Exit Sub
            If GridTodos.RowCount = 0 Then
                Actividad = New ActividadesClass(0)
            End If
            With Actividad
                If OpcionForma = TipoOpcionForma.tOpcModificar Then
                    CadCam = .GetCambios(txtNombreAct.Text, txtidFamilia.Text, txtCostoManoObra.Text, txtDuracionHoras.Text)
                    If CadCam <> "" Then
                        'If MessageBox.Show("Esta completamente Seguro que Desea guardar los Cambios Realizados al Estado Con id: " & .IDMARCA & vbNewLine &
                        '                CadCam, "Confirma que desea guardar los Cambios Realizados???", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then Exit Sub
                        Status("Guardando Modificaciones de la Actividad Con id: " & .idActividad, Me, 1, 2)

                        .Guardar(txtNombreAct.Text, txtidFamilia.Text, bEsIdentidad, txtCostoManoObra.Text, txtDuracionHoras.Text)
                        Status("Actividad con Id: " & .idActividad & " Modificado Con Exito!!!", Me, 2, 2)
                        MessageBox.Show("Actividad con Id: " & .idActividad & " Modificada con exito!!!",
                                        "Accion terminada con exito!!!", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
                        ActualizaGrid(txtMenuBusca.Text)
                    Else
                        Status("No se detectaron cambios para realizar!!!", Me, Err:=True)
                    End If
                ElseIf OpcionForma = TipoOpcionForma.tOpcInsertar Then
                    .Existe = False
                    .NombreAct = txtNombreAct.Text
                    If Not bEsIdentidad Then
                        .idActividad = txtidActividad.Text
                    End If
                    Status("Guardando Nueva Actividad: " & txtNombreAct.Text & ". . .", Me, 1, 2)
                    .Guardar(txtNombreAct.Text, txtidFamilia.Text, bEsIdentidad, txtCostoManoObra.Text, txtDuracionHoras.Text)
                    Status("Actividad: " & txtNombreAct.Text & " Guardado con Exito!!!", Me, 2, 2)
                    MessageBox.Show("Nueva Actividad: " & txtNombreAct.Text & " Guardado con exito!!!",
                                    "Accion terminada con exito!!!", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
                    ActualizaGrid(txtMenuBusca.Text)

                End If
            End With
            GridTodos.Enabled = True
            Salida = False
        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try
    End Sub


    Private Sub btnMnuCancelar_Click(sender As Object, e As EventArgs) Handles btnMnuCancelar.Click
        If txtidActividad.Text <> "" Then
            CargaForm(txtidActividad.Text, UCase(TablaBd))
        End If
        OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
    End Sub

    Private Sub btnMnuSalir_Click(sender As Object, e As EventArgs) Handles btnMnuSalir.Click
        Me.Close()
    End Sub

    Private Sub txtMenuBusca_TextChanged(sender As Object, e As EventArgs) Handles txtMenuBusca.TextChanged
        ActualizaGrid(txtMenuBusca.Text)
    End Sub

    Private Sub txtCP_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCostoManoObra.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            txtDuracionHoras.Focus()
            txtDuracionHoras.SelectAll()
        End If
    End Sub

    Private Sub txtCP_MouseDown(sender As Object, e As MouseEventArgs) Handles txtCostoManoObra.MouseDown
        txtCostoManoObra.Focus()
        txtCostoManoObra.SelectAll()
    End Sub

    Private Sub txtDuracionHoras_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtDuracionHoras.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            txtidFamilia.Focus()
            txtidFamilia.SelectAll()
        End If
    End Sub

    Private Sub txtDuracionHoras_MouseDown(sender As Object, e As MouseEventArgs) Handles txtDuracionHoras.MouseDown
        txtDuracionHoras.Focus()
        txtDuracionHoras.SelectAll()
    End Sub

    Private Sub GrupoCaptura_Enter(sender As Object, e As EventArgs) Handles GrupoCaptura.Enter

    End Sub

    Private Sub txtidActividad_TextChanged(sender As Object, e As EventArgs) Handles txtidActividad.TextChanged

    End Sub

    Private Sub txtidFamilia_TextChanged(sender As Object, e As EventArgs) Handles txtidFamilia.TextChanged

    End Sub

    Private Sub txtNombreAct_TextChanged(sender As Object, e As EventArgs) Handles txtNombreAct.TextChanged

    End Sub
End Class
