'Fecha: 14 / Marzo / 2016
'ROGER GALA PACHECO
'************************************************************************************************************************************
'*  Fecha Modificacion :  14 / Marzo / 2016                                                                             
'*  
'************************************************************************************************************************************
Imports System.Threading
Imports Microsoft.VisualBasic.PowerPacks

Public Class frmCatUniTransp
    Inherits System.Windows.Forms.Form
    Dim OpcionForma As TipoOpcionForma
    Dim TablaBd As String = "CatUnidadTrans"
    Dim CampoLLave As String = "idUnidadTrans"
    Dim TablaBd2 As String = "CatTipoUniTrans"
    Public WithEvents MeStatus1 As ControlStatus.MeStatus
    Friend WithEvents GridTodos As System.Windows.Forms.DataGridView
    'Private Servicio As ServicioClass
    'Private TipoServ As TipoServClass

    Private UniTrans As UnidadesTranspClass
    Private TipoUniTrans As TipoUniTransClass
    Private Empresa As EmpresaClass
    Private Almacen As AlmacenClass
    Private Sucursal As SucursalClass
    Private Marca As MarcaClass

    Friend WithEvents btnMnuOk As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuCancelar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuSalir As System.Windows.Forms.ToolStripButton
    Private _Tag As String
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cmdBuscaClaveTPU As System.Windows.Forms.Button
    Friend WithEvents txtidTipoUnidad As System.Windows.Forms.TextBox
    Friend WithEvents txtnomTipoUniTras As System.Windows.Forms.TextBox
    Dim bEsIdentidad As Boolean = False
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents cmbEstatus As System.Windows.Forms.ComboBox
    Dim Salida As Boolean
    Friend WithEvents txtidMarca As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cmdBuscaClaveMar As System.Windows.Forms.Button
    Friend WithEvents txtNombreMarca As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cmdBuscaClaveSuc As System.Windows.Forms.Button
    Friend WithEvents txtidSucursal As System.Windows.Forms.TextBox
    Friend WithEvents txtNomSucursal As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents cmdBuscaClaveAlm As System.Windows.Forms.Button
    Friend WithEvents txtIdAlmacen As System.Windows.Forms.TextBox
    Friend WithEvents txtNomAlmacen As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents cmdBuscaClaveEmp As System.Windows.Forms.Button
    Friend WithEvents txtidEmpresa As System.Windows.Forms.TextBox
    Friend WithEvents txtRazonSocial As System.Windows.Forms.TextBox
    Friend WithEvents txtTonelajeMax As System.Windows.Forms.TextBox
    Friend WithEvents txtNoEjes As System.Windows.Forms.TextBox
    Friend WithEvents txtNoLlantas As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    'Dim DsCombo As New DataTable
    Dim DsCombo As New DataSet
    Private _con As String
    Friend WithEvents gpoPosicion As GroupBox
    Friend WithEvents txtPosicion As TextBox
    Friend WithEvents Label13 As Label
    Friend WithEvents txtTipoUni As TextBox
    Friend WithEvents Label12 As Label
    Friend WithEvents txtUniTrans As TextBox
    Friend WithEvents Label33 As Label
    Friend WithEvents GpoPosiciones As GroupBox
    'Friend WithEvents ShapeContainer2 As PowerPacks.ShapeContainer
    'Friend WithEvents LineShape1 As PowerPacks.LineShape
    Private _v As Boolean

    'llantas
    'Dim UniTrans As UnidadesTranspClass
    Dim TipUni As TipoUniTransClass
    Dim ClasLlan As ClasLlanClass
    Dim rdbPosicion() As RadioButton
    'Dim tt() As ToolTip
    'Dim rdbPosicion() As CheckBox
    Dim tt As New ToolTip
    Dim TablaEstruc As DataTable
    Dim llanta As LlantasClass
    Dim llantaProd As LLantasProductoClass
    Dim tipllanta As TipoLlantaClass
    Dim Disenio As CatDiseniosClass

    Dim TextoToolTip As String = ""
    Dim NomEmpresa As String = ""
    Dim NomMarca As String = ""
    Dim NomTipLlanta As String = ""
    Dim NomDisenio As String
    Dim pos As Integer

    Dim strsql As String



#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents GrupoCaptura As System.Windows.Forms.GroupBox
    Friend WithEvents ToolStripMenu As System.Windows.Forms.ToolStrip
    Friend WithEvents txtMenuBusca As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents btnMnuAltas As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuModificar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuEliminar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuReporte As System.Windows.Forms.ToolStripButton
    Friend WithEvents label1 As System.Windows.Forms.Label
    Friend WithEvents label2 As System.Windows.Forms.Label
    Friend WithEvents cmdBuscaClave As System.Windows.Forms.Button
    Friend WithEvents txtidUnidadTrans As System.Windows.Forms.TextBox
    Friend WithEvents txtDescripcionUni As System.Windows.Forms.TextBox


    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCatUniTransp))
        Me.txtidUnidadTrans = New System.Windows.Forms.TextBox()
        Me.label1 = New System.Windows.Forms.Label()
        Me.txtDescripcionUni = New System.Windows.Forms.TextBox()
        Me.label2 = New System.Windows.Forms.Label()
        Me.GrupoCaptura = New System.Windows.Forms.GroupBox()
        Me.txtTonelajeMax = New System.Windows.Forms.TextBox()
        Me.txtNoEjes = New System.Windows.Forms.TextBox()
        Me.txtNoLlantas = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.cmdBuscaClaveEmp = New System.Windows.Forms.Button()
        Me.txtidEmpresa = New System.Windows.Forms.TextBox()
        Me.txtRazonSocial = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.cmdBuscaClaveAlm = New System.Windows.Forms.Button()
        Me.txtIdAlmacen = New System.Windows.Forms.TextBox()
        Me.txtNomAlmacen = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.cmdBuscaClaveSuc = New System.Windows.Forms.Button()
        Me.txtidSucursal = New System.Windows.Forms.TextBox()
        Me.txtNomSucursal = New System.Windows.Forms.TextBox()
        Me.txtidMarca = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cmdBuscaClaveMar = New System.Windows.Forms.Button()
        Me.txtNombreMarca = New System.Windows.Forms.TextBox()
        Me.cmbEstatus = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cmdBuscaClaveTPU = New System.Windows.Forms.Button()
        Me.txtidTipoUnidad = New System.Windows.Forms.TextBox()
        Me.txtnomTipoUniTras = New System.Windows.Forms.TextBox()
        Me.cmdBuscaClave = New System.Windows.Forms.Button()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.ToolStripMenu = New System.Windows.Forms.ToolStrip()
        Me.txtMenuBusca = New System.Windows.Forms.ToolStripTextBox()
        Me.btnMnuAltas = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuModificar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuEliminar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuReporte = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuOk = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuCancelar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuSalir = New System.Windows.Forms.ToolStripButton()
        Me.MeStatus1 = New ControlStatus.MeStatus()
        Me.GridTodos = New System.Windows.Forms.DataGridView()
        Me.gpoPosicion = New System.Windows.Forms.GroupBox()
        Me.txtPosicion = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtTipoUni = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtUniTrans = New System.Windows.Forms.TextBox()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.GpoPosiciones = New System.Windows.Forms.GroupBox()
        'Me.ShapeContainer2 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        'Me.LineShape1 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.GrupoCaptura.SuspendLayout()
        Me.ToolStripMenu.SuspendLayout()
        CType(Me.GridTodos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gpoPosicion.SuspendLayout()
        Me.GpoPosiciones.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtidUnidadTrans
        '
        Me.txtidUnidadTrans.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtidUnidadTrans.Location = New System.Drawing.Point(122, 24)
        Me.txtidUnidadTrans.MaxLength = 10
        Me.txtidUnidadTrans.Name = "txtidUnidadTrans"
        Me.txtidUnidadTrans.Size = New System.Drawing.Size(140, 20)
        Me.txtidUnidadTrans.TabIndex = 1
        '
        'label1
        '
        Me.label1.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.label1.Location = New System.Drawing.Point(6, 27)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(94, 20)
        Me.label1.TabIndex = 14
        Me.label1.Text = "id Unidad Trans:"
        '
        'txtDescripcionUni
        '
        Me.txtDescripcionUni.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDescripcionUni.Location = New System.Drawing.Point(125, 124)
        Me.txtDescripcionUni.MaxLength = 50
        Me.txtDescripcionUni.Name = "txtDescripcionUni"
        Me.txtDescripcionUni.Size = New System.Drawing.Size(390, 20)
        Me.txtDescripcionUni.TabIndex = 5
        '
        'label2
        '
        Me.label2.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.label2.Location = New System.Drawing.Point(9, 127)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(91, 20)
        Me.label2.TabIndex = 16
        Me.label2.Text = "Descripci�n:"
        '
        'GrupoCaptura
        '
        Me.GrupoCaptura.Controls.Add(Me.txtTonelajeMax)
        Me.GrupoCaptura.Controls.Add(Me.txtNoEjes)
        Me.GrupoCaptura.Controls.Add(Me.txtNoLlantas)
        Me.GrupoCaptura.Controls.Add(Me.Label7)
        Me.GrupoCaptura.Controls.Add(Me.cmdBuscaClaveEmp)
        Me.GrupoCaptura.Controls.Add(Me.txtidEmpresa)
        Me.GrupoCaptura.Controls.Add(Me.txtRazonSocial)
        Me.GrupoCaptura.Controls.Add(Me.Label6)
        Me.GrupoCaptura.Controls.Add(Me.cmdBuscaClaveAlm)
        Me.GrupoCaptura.Controls.Add(Me.txtIdAlmacen)
        Me.GrupoCaptura.Controls.Add(Me.txtNomAlmacen)
        Me.GrupoCaptura.Controls.Add(Me.Label5)
        Me.GrupoCaptura.Controls.Add(Me.cmdBuscaClaveSuc)
        Me.GrupoCaptura.Controls.Add(Me.txtidSucursal)
        Me.GrupoCaptura.Controls.Add(Me.txtNomSucursal)
        Me.GrupoCaptura.Controls.Add(Me.txtidMarca)
        Me.GrupoCaptura.Controls.Add(Me.Label4)
        Me.GrupoCaptura.Controls.Add(Me.cmdBuscaClaveMar)
        Me.GrupoCaptura.Controls.Add(Me.txtNombreMarca)
        Me.GrupoCaptura.Controls.Add(Me.cmbEstatus)
        Me.GrupoCaptura.Controls.Add(Me.Label8)
        Me.GrupoCaptura.Controls.Add(Me.Label3)
        Me.GrupoCaptura.Controls.Add(Me.cmdBuscaClaveTPU)
        Me.GrupoCaptura.Controls.Add(Me.txtidTipoUnidad)
        Me.GrupoCaptura.Controls.Add(Me.txtnomTipoUniTras)
        Me.GrupoCaptura.Controls.Add(Me.label1)
        Me.GrupoCaptura.Controls.Add(Me.label2)
        Me.GrupoCaptura.Controls.Add(Me.cmdBuscaClave)
        Me.GrupoCaptura.Controls.Add(Me.txtidUnidadTrans)
        Me.GrupoCaptura.Controls.Add(Me.txtDescripcionUni)
        Me.GrupoCaptura.Controls.Add(Me.Label11)
        Me.GrupoCaptura.Controls.Add(Me.Label10)
        Me.GrupoCaptura.Controls.Add(Me.Label9)
        Me.GrupoCaptura.Location = New System.Drawing.Point(168, 45)
        Me.GrupoCaptura.Name = "GrupoCaptura"
        Me.GrupoCaptura.Size = New System.Drawing.Size(524, 339)
        Me.GrupoCaptura.TabIndex = 3
        Me.GrupoCaptura.TabStop = False
        '
        'txtTonelajeMax
        '
        Me.txtTonelajeMax.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTonelajeMax.Location = New System.Drawing.Point(447, 93)
        Me.txtTonelajeMax.MaxLength = 6
        Me.txtTonelajeMax.Name = "txtTonelajeMax"
        Me.txtTonelajeMax.ReadOnly = True
        Me.txtTonelajeMax.Size = New System.Drawing.Size(65, 20)
        Me.txtTonelajeMax.TabIndex = 55
        Me.txtTonelajeMax.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtNoEjes
        '
        Me.txtNoEjes.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNoEjes.Location = New System.Drawing.Point(246, 94)
        Me.txtNoEjes.MaxLength = 6
        Me.txtNoEjes.Name = "txtNoEjes"
        Me.txtNoEjes.ReadOnly = True
        Me.txtNoEjes.Size = New System.Drawing.Size(65, 20)
        Me.txtNoEjes.TabIndex = 54
        Me.txtNoEjes.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtNoLlantas
        '
        Me.txtNoLlantas.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNoLlantas.Location = New System.Drawing.Point(79, 93)
        Me.txtNoLlantas.MaxLength = 6
        Me.txtNoLlantas.Name = "txtNoLlantas"
        Me.txtNoLlantas.ReadOnly = True
        Me.txtNoLlantas.Size = New System.Drawing.Size(65, 20)
        Me.txtNoLlantas.TabIndex = 53
        Me.txtNoLlantas.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label7
        '
        Me.Label7.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label7.Location = New System.Drawing.Point(22, 158)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(94, 20)
        Me.Label7.TabIndex = 52
        Me.Label7.Text = "Id Empresa:"
        '
        'cmdBuscaClaveEmp
        '
        Me.cmdBuscaClaveEmp.Image = CType(resources.GetObject("cmdBuscaClaveEmp.Image"), System.Drawing.Image)
        Me.cmdBuscaClaveEmp.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdBuscaClaveEmp.Location = New System.Drawing.Point(202, 150)
        Me.cmdBuscaClaveEmp.Name = "cmdBuscaClaveEmp"
        Me.cmdBuscaClaveEmp.Size = New System.Drawing.Size(32, 32)
        Me.cmdBuscaClaveEmp.TabIndex = 7
        Me.cmdBuscaClaveEmp.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'txtidEmpresa
        '
        Me.txtidEmpresa.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtidEmpresa.Location = New System.Drawing.Point(125, 158)
        Me.txtidEmpresa.Name = "txtidEmpresa"
        Me.txtidEmpresa.Size = New System.Drawing.Size(69, 20)
        Me.txtidEmpresa.TabIndex = 6
        '
        'txtRazonSocial
        '
        Me.txtRazonSocial.Enabled = False
        Me.txtRazonSocial.Location = New System.Drawing.Point(240, 162)
        Me.txtRazonSocial.Name = "txtRazonSocial"
        Me.txtRazonSocial.ReadOnly = True
        Me.txtRazonSocial.Size = New System.Drawing.Size(275, 20)
        Me.txtRazonSocial.TabIndex = 9
        '
        'Label6
        '
        Me.Label6.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label6.Location = New System.Drawing.Point(22, 272)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(94, 20)
        Me.Label6.TabIndex = 48
        Me.Label6.Text = "Id Almacen:"
        '
        'cmdBuscaClaveAlm
        '
        Me.cmdBuscaClaveAlm.Image = CType(resources.GetObject("cmdBuscaClaveAlm.Image"), System.Drawing.Image)
        Me.cmdBuscaClaveAlm.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdBuscaClaveAlm.Location = New System.Drawing.Point(202, 264)
        Me.cmdBuscaClaveAlm.Name = "cmdBuscaClaveAlm"
        Me.cmdBuscaClaveAlm.Size = New System.Drawing.Size(32, 32)
        Me.cmdBuscaClaveAlm.TabIndex = 13
        Me.cmdBuscaClaveAlm.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'txtIdAlmacen
        '
        Me.txtIdAlmacen.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtIdAlmacen.Location = New System.Drawing.Point(125, 272)
        Me.txtIdAlmacen.Name = "txtIdAlmacen"
        Me.txtIdAlmacen.Size = New System.Drawing.Size(69, 20)
        Me.txtIdAlmacen.TabIndex = 12
        '
        'txtNomAlmacen
        '
        Me.txtNomAlmacen.Enabled = False
        Me.txtNomAlmacen.Location = New System.Drawing.Point(240, 276)
        Me.txtNomAlmacen.Name = "txtNomAlmacen"
        Me.txtNomAlmacen.ReadOnly = True
        Me.txtNomAlmacen.Size = New System.Drawing.Size(275, 20)
        Me.txtNomAlmacen.TabIndex = 18
        '
        'Label5
        '
        Me.Label5.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label5.Location = New System.Drawing.Point(22, 234)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(94, 20)
        Me.Label5.TabIndex = 44
        Me.Label5.Text = "Id Sucursal:"
        '
        'cmdBuscaClaveSuc
        '
        Me.cmdBuscaClaveSuc.Image = CType(resources.GetObject("cmdBuscaClaveSuc.Image"), System.Drawing.Image)
        Me.cmdBuscaClaveSuc.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdBuscaClaveSuc.Location = New System.Drawing.Point(202, 226)
        Me.cmdBuscaClaveSuc.Name = "cmdBuscaClaveSuc"
        Me.cmdBuscaClaveSuc.Size = New System.Drawing.Size(32, 32)
        Me.cmdBuscaClaveSuc.TabIndex = 11
        Me.cmdBuscaClaveSuc.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'txtidSucursal
        '
        Me.txtidSucursal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtidSucursal.Location = New System.Drawing.Point(125, 234)
        Me.txtidSucursal.Name = "txtidSucursal"
        Me.txtidSucursal.Size = New System.Drawing.Size(69, 20)
        Me.txtidSucursal.TabIndex = 10
        '
        'txtNomSucursal
        '
        Me.txtNomSucursal.Enabled = False
        Me.txtNomSucursal.Location = New System.Drawing.Point(240, 238)
        Me.txtNomSucursal.Name = "txtNomSucursal"
        Me.txtNomSucursal.ReadOnly = True
        Me.txtNomSucursal.Size = New System.Drawing.Size(275, 20)
        Me.txtNomSucursal.TabIndex = 15
        '
        'txtidMarca
        '
        Me.txtidMarca.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtidMarca.Location = New System.Drawing.Point(125, 196)
        Me.txtidMarca.Name = "txtidMarca"
        Me.txtidMarca.Size = New System.Drawing.Size(69, 20)
        Me.txtidMarca.TabIndex = 8
        '
        'Label4
        '
        Me.Label4.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label4.Location = New System.Drawing.Point(22, 196)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(114, 20)
        Me.Label4.TabIndex = 40
        Me.Label4.Text = "Marca:"
        '
        'cmdBuscaClaveMar
        '
        Me.cmdBuscaClaveMar.Image = CType(resources.GetObject("cmdBuscaClaveMar.Image"), System.Drawing.Image)
        Me.cmdBuscaClaveMar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdBuscaClaveMar.Location = New System.Drawing.Point(202, 188)
        Me.cmdBuscaClaveMar.Name = "cmdBuscaClaveMar"
        Me.cmdBuscaClaveMar.Size = New System.Drawing.Size(32, 32)
        Me.cmdBuscaClaveMar.TabIndex = 9
        Me.cmdBuscaClaveMar.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'txtNombreMarca
        '
        Me.txtNombreMarca.Enabled = False
        Me.txtNombreMarca.Location = New System.Drawing.Point(240, 200)
        Me.txtNombreMarca.Name = "txtNombreMarca"
        Me.txtNombreMarca.ReadOnly = True
        Me.txtNombreMarca.Size = New System.Drawing.Size(275, 20)
        Me.txtNombreMarca.TabIndex = 12
        '
        'cmbEstatus
        '
        Me.cmbEstatus.AutoCompleteCustomSource.AddRange(New String() {"Regularizaci�n", "Vivienda"})
        Me.cmbEstatus.FormattingEnabled = True
        Me.cmbEstatus.Items.AddRange(New Object() {"ACTIVO", "INACTIVO"})
        Me.cmbEstatus.Location = New System.Drawing.Point(125, 302)
        Me.cmbEstatus.Name = "cmbEstatus"
        Me.cmbEstatus.Size = New System.Drawing.Size(112, 21)
        Me.cmbEstatus.TabIndex = 14
        '
        'Label8
        '
        Me.Label8.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label8.Location = New System.Drawing.Point(9, 305)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(94, 20)
        Me.Label8.TabIndex = 36
        Me.Label8.Text = "Estatus:"
        '
        'Label3
        '
        Me.Label3.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label3.Location = New System.Drawing.Point(6, 66)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(94, 20)
        Me.Label3.TabIndex = 20
        Me.Label3.Text = "Tipo Unidad:"
        '
        'cmdBuscaClaveTPU
        '
        Me.cmdBuscaClaveTPU.Image = CType(resources.GetObject("cmdBuscaClaveTPU.Image"), System.Drawing.Image)
        Me.cmdBuscaClaveTPU.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdBuscaClaveTPU.Location = New System.Drawing.Point(199, 55)
        Me.cmdBuscaClaveTPU.Name = "cmdBuscaClaveTPU"
        Me.cmdBuscaClaveTPU.Size = New System.Drawing.Size(32, 32)
        Me.cmdBuscaClaveTPU.TabIndex = 4
        Me.cmdBuscaClaveTPU.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'txtidTipoUnidad
        '
        Me.txtidTipoUnidad.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtidTipoUnidad.Location = New System.Drawing.Point(122, 63)
        Me.txtidTipoUnidad.Name = "txtidTipoUnidad"
        Me.txtidTipoUnidad.Size = New System.Drawing.Size(69, 20)
        Me.txtidTipoUnidad.TabIndex = 3
        '
        'txtnomTipoUniTras
        '
        Me.txtnomTipoUniTras.Enabled = False
        Me.txtnomTipoUniTras.Location = New System.Drawing.Point(237, 67)
        Me.txtnomTipoUniTras.Name = "txtnomTipoUniTras"
        Me.txtnomTipoUniTras.ReadOnly = True
        Me.txtnomTipoUniTras.Size = New System.Drawing.Size(275, 20)
        Me.txtnomTipoUniTras.TabIndex = 5
        '
        'cmdBuscaClave
        '
        Me.cmdBuscaClave.Image = CType(resources.GetObject("cmdBuscaClave.Image"), System.Drawing.Image)
        Me.cmdBuscaClave.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdBuscaClave.Location = New System.Drawing.Point(268, 15)
        Me.cmdBuscaClave.Name = "cmdBuscaClave"
        Me.cmdBuscaClave.Size = New System.Drawing.Size(32, 32)
        Me.cmdBuscaClave.TabIndex = 2
        Me.cmdBuscaClave.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'Label11
        '
        Me.Label11.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label11.Location = New System.Drawing.Point(6, 97)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(110, 20)
        Me.Label11.TabIndex = 56
        Me.Label11.Text = "No. Llantas:"
        '
        'Label10
        '
        Me.Label10.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label10.Location = New System.Drawing.Point(195, 97)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(94, 20)
        Me.Label10.TabIndex = 57
        Me.Label10.Text = "No. Ejes:"
        '
        'Label9
        '
        Me.Label9.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label9.Location = New System.Drawing.Point(369, 97)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(94, 20)
        Me.Label9.TabIndex = 58
        Me.Label9.Text = "Tonelaje M�x. :"
        '
        'ToolStripMenu
        '
        Me.ToolStripMenu.AutoSize = False
        Me.ToolStripMenu.BackColor = System.Drawing.Color.Silver
        Me.ToolStripMenu.Dock = System.Windows.Forms.DockStyle.None
        Me.ToolStripMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.txtMenuBusca, Me.btnMnuAltas, Me.btnMnuModificar, Me.btnMnuEliminar, Me.btnMnuReporte, Me.btnMnuOk, Me.btnMnuCancelar, Me.btnMnuSalir})
        Me.ToolStripMenu.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow
        Me.ToolStripMenu.Location = New System.Drawing.Point(3, 0)
        Me.ToolStripMenu.Name = "ToolStripMenu"
        Me.ToolStripMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.ToolStripMenu.Size = New System.Drawing.Size(1175, 42)
        Me.ToolStripMenu.TabIndex = 1
        Me.ToolStripMenu.Text = "ToolStrip1"
        '
        'txtMenuBusca
        '
        Me.txtMenuBusca.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.txtMenuBusca.Name = "txtMenuBusca"
        Me.txtMenuBusca.Size = New System.Drawing.Size(150, 42)
        Me.txtMenuBusca.ToolTipText = "Filtra Listado"
        '
        'btnMnuAltas
        '
        Me.btnMnuAltas.ForeColor = System.Drawing.Color.Red
        Me.btnMnuAltas.Image = CType(resources.GetObject("btnMnuAltas.Image"), System.Drawing.Image)
        Me.btnMnuAltas.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuAltas.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuAltas.Name = "btnMnuAltas"
        Me.btnMnuAltas.Size = New System.Drawing.Size(44, 39)
        Me.btnMnuAltas.Text = "&ALTAS"
        Me.btnMnuAltas.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnMnuAltas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuModificar
        '
        Me.btnMnuModificar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuModificar.Image = CType(resources.GetObject("btnMnuModificar.Image"), System.Drawing.Image)
        Me.btnMnuModificar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuModificar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuModificar.Name = "btnMnuModificar"
        Me.btnMnuModificar.Size = New System.Drawing.Size(74, 39)
        Me.btnMnuModificar.Text = "&MODIFICAR"
        Me.btnMnuModificar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnMnuModificar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuEliminar
        '
        Me.btnMnuEliminar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuEliminar.Image = CType(resources.GetObject("btnMnuEliminar.Image"), System.Drawing.Image)
        Me.btnMnuEliminar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuEliminar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuEliminar.Name = "btnMnuEliminar"
        Me.btnMnuEliminar.Size = New System.Drawing.Size(64, 39)
        Me.btnMnuEliminar.Text = "&ELIMINAR"
        Me.btnMnuEliminar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnMnuEliminar.Visible = False
        '
        'btnMnuReporte
        '
        Me.btnMnuReporte.ForeColor = System.Drawing.Color.Red
        Me.btnMnuReporte.Image = CType(resources.GetObject("btnMnuReporte.Image"), System.Drawing.Image)
        Me.btnMnuReporte.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuReporte.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuReporte.Name = "btnMnuReporte"
        Me.btnMnuReporte.Size = New System.Drawing.Size(59, 39)
        Me.btnMnuReporte.Text = "&REPORTE"
        Me.btnMnuReporte.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuOk
        '
        Me.btnMnuOk.ForeColor = System.Drawing.Color.Red
        Me.btnMnuOk.Image = CType(resources.GetObject("btnMnuOk.Image"), System.Drawing.Image)
        Me.btnMnuOk.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuOk.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuOk.Name = "btnMnuOk"
        Me.btnMnuOk.Size = New System.Drawing.Size(56, 39)
        Me.btnMnuOk.Text = "&GRABAR"
        Me.btnMnuOk.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuCancelar
        '
        Me.btnMnuCancelar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuCancelar.Image = CType(resources.GetObject("btnMnuCancelar.Image"), System.Drawing.Image)
        Me.btnMnuCancelar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuCancelar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuCancelar.Name = "btnMnuCancelar"
        Me.btnMnuCancelar.Size = New System.Drawing.Size(71, 39)
        Me.btnMnuCancelar.Text = "&CANCELAR"
        Me.btnMnuCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuSalir
        '
        Me.btnMnuSalir.ForeColor = System.Drawing.Color.Red
        Me.btnMnuSalir.Image = CType(resources.GetObject("btnMnuSalir.Image"), System.Drawing.Image)
        Me.btnMnuSalir.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuSalir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuSalir.Name = "btnMnuSalir"
        Me.btnMnuSalir.Size = New System.Drawing.Size(41, 39)
        Me.btnMnuSalir.Text = "&SALIR"
        Me.btnMnuSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'MeStatus1
        '
        Me.MeStatus1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.MeStatus1.Location = New System.Drawing.Point(0, 450)
        Me.MeStatus1.Margin = New System.Windows.Forms.Padding(4)
        Me.MeStatus1.Name = "MeStatus1"
        Me.MeStatus1.Size = New System.Drawing.Size(1157, 26)
        Me.MeStatus1.TabIndex = 72
        '
        'GridTodos
        '
        Me.GridTodos.AllowUserToAddRows = False
        Me.GridTodos.AllowUserToDeleteRows = False
        Me.GridTodos.AllowUserToResizeRows = False
        Me.GridTodos.BackgroundColor = System.Drawing.Color.White
        Me.GridTodos.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.GridTodos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.GridTodos.Location = New System.Drawing.Point(3, 45)
        Me.GridTodos.MultiSelect = False
        Me.GridTodos.Name = "GridTodos"
        Me.GridTodos.ReadOnly = True
        Me.GridTodos.RowHeadersVisible = False
        Me.GridTodos.Size = New System.Drawing.Size(162, 339)
        Me.GridTodos.TabIndex = 73
        '
        'gpoPosicion
        '
        Me.gpoPosicion.Controls.Add(Me.txtPosicion)
        Me.gpoPosicion.Controls.Add(Me.Label13)
        Me.gpoPosicion.Controls.Add(Me.txtTipoUni)
        Me.gpoPosicion.Controls.Add(Me.Label12)
        Me.gpoPosicion.Controls.Add(Me.txtUniTrans)
        Me.gpoPosicion.Controls.Add(Me.Label33)
        Me.gpoPosicion.Location = New System.Drawing.Point(710, 45)
        Me.gpoPosicion.Name = "gpoPosicion"
        Me.gpoPosicion.Size = New System.Drawing.Size(438, 50)
        Me.gpoPosicion.TabIndex = 121
        Me.gpoPosicion.TabStop = False
        Me.gpoPosicion.Text = "Ubicaci�n"
        '
        'txtPosicion
        '
        Me.txtPosicion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPosicion.Location = New System.Drawing.Point(360, 20)
        Me.txtPosicion.MaxLength = 10
        Me.txtPosicion.Name = "txtPosicion"
        Me.txtPosicion.Size = New System.Drawing.Size(33, 20)
        Me.txtPosicion.TabIndex = 84
        Me.txtPosicion.Tag = "1"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label13.Location = New System.Drawing.Point(304, 22)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(50, 13)
        Me.Label13.TabIndex = 85
        Me.Label13.Text = "Posici�n:"
        '
        'txtTipoUni
        '
        Me.txtTipoUni.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTipoUni.Location = New System.Drawing.Point(194, 20)
        Me.txtTipoUni.MaxLength = 10
        Me.txtTipoUni.Name = "txtTipoUni"
        Me.txtTipoUni.ReadOnly = True
        Me.txtTipoUni.Size = New System.Drawing.Size(90, 20)
        Me.txtTipoUni.TabIndex = 82
        Me.txtTipoUni.Tag = "1"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label12.Location = New System.Drawing.Point(157, 20)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(31, 13)
        Me.Label12.TabIndex = 83
        Me.Label12.Text = "Tipo:"
        '
        'txtUniTrans
        '
        Me.txtUniTrans.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtUniTrans.Location = New System.Drawing.Point(63, 20)
        Me.txtUniTrans.MaxLength = 10
        Me.txtUniTrans.Name = "txtUniTrans"
        Me.txtUniTrans.Size = New System.Drawing.Size(90, 20)
        Me.txtUniTrans.TabIndex = 80
        Me.txtUniTrans.Tag = "1"
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label33.Location = New System.Drawing.Point(6, 23)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(44, 13)
        Me.Label33.TabIndex = 81
        Me.Label33.Text = "Unidad:"
        '
        'GpoPosiciones
        '
        'Me.GpoPosiciones.Controls.Add(Me.ShapeContainer2)
        'Me.GpoPosiciones.Location = New System.Drawing.Point(707, 101)
        'Me.GpoPosiciones.Name = "GpoPosiciones"
        'Me.GpoPosiciones.Size = New System.Drawing.Size(385, 349)
        'Me.GpoPosiciones.TabIndex = 120
        'Me.GpoPosiciones.TabStop = False
        'Me.GpoPosiciones.Text = "Posiciones"
        ''
        ''ShapeContainer2
        ''
        'Me.ShapeContainer2.Location = New System.Drawing.Point(3, 16)
        'Me.ShapeContainer2.Margin = New System.Windows.Forms.Padding(0)
        'Me.ShapeContainer2.Name = "ShapeContainer2"
        ''Me.ShapeContainer2.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.LineShape1})
        'Me.ShapeContainer2.Size = New System.Drawing.Size(379, 330)
        'Me.ShapeContainer2.TabIndex = 2
        'Me.ShapeContainer2.TabStop = False
        ''
        ''LineShape1
        ''
        'Me.LineShape1.BorderWidth = 10
        'Me.LineShape1.Name = "LineaPrinc"
        'Me.LineShape1.X1 = 186
        'Me.LineShape1.X2 = 186
        'Me.LineShape1.Y1 = 36
        'Me.LineShape1.Y2 = 280
        '
        'frmCatUniTransp
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(1157, 476)
        Me.Controls.Add(Me.gpoPosicion)
        Me.Controls.Add(Me.GpoPosiciones)
        Me.Controls.Add(Me.GridTodos)
        Me.Controls.Add(Me.MeStatus1)
        Me.Controls.Add(Me.ToolStripMenu)
        Me.Controls.Add(Me.GrupoCaptura)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmCatUniTransp"
        Me.Text = "Catal�go de Unidades de Transporte"
        Me.GrupoCaptura.ResumeLayout(False)
        Me.GrupoCaptura.PerformLayout()
        Me.ToolStripMenu.ResumeLayout(False)
        Me.ToolStripMenu.PerformLayout()
        CType(Me.GridTodos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gpoPosicion.ResumeLayout(False)
        Me.gpoPosicion.PerformLayout()
        Me.GpoPosiciones.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region
    Sub New(ByVal vTag As String)
        InitializeComponent()
        _Tag = vTag
    End Sub

    Public Sub New(con As String, v As Boolean)
        _con = con
        BD = New CapaDatos.UtilSQL(con, "Juan")
        InitializeComponent()
        _v = v
    End Sub
#Region "MenusToolStrip"
    Private Sub LimpiaVariables()
        SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")

    End Sub
    Private Sub LimpiaCampos()
        txtidUnidadTrans.Text = ""
        txtidTipoUnidad.Text = ""
        txtnomTipoUniTras.Text = ""
        txtNoLlantas.Text = ""
        txtNoEjes.Text = ""
        txtTonelajeMax.Text = ""
        txtDescripcionUni.Text = ""
        txtidEmpresa.Text = ""
        txtRazonSocial.Text = ""
        txtidMarca.Text = ""
        txtNombreMarca.Text = ""
        txtidSucursal.Text = ""
        txtNomSucursal.Text = ""
        txtIdAlmacen.Text = ""
        txtNomAlmacen.Text = ""
        cmbEstatus.SelectedIndex = 0
        LimpiaVariables()
    End Sub

    Private Sub ActivaCampos(ByVal valor As Boolean, ByVal vOpcion As TipoOpcActivaCampos)
        If vOpcion = TipoOpcActivaCampos.tOpcDESHABTODOS Then
            txtidUnidadTrans.Enabled = False
            txtidTipoUnidad.Enabled = False
            'txtnomTipoUniTras.Enabled = False
            txtDescripcionUni.Enabled = False
            txtidMarca.Enabled = False
            txtidSucursal.Enabled = False
            txtIdAlmacen.Enabled = False
            cmbEstatus.Enabled = False
            txtidEmpresa.Enabled = False
        ElseIf vOpcion = TipoOpcActivaCampos.tOpcINICIALIZA Then
            txtidUnidadTrans.Enabled = Not valor
            txtidTipoUnidad.Enabled = valor
            'txtnomTipoUniTras.Enabled = valor
            txtDescripcionUni.Enabled = valor
            txtidMarca.Enabled = valor
            txtidSucursal.Enabled = valor
            txtIdAlmacen.Enabled = valor
            cmbEstatus.Enabled = valor
            txtidEmpresa.Enabled = valor
        End If
    End Sub


    Private Sub ActivaBotones(ByVal valor As Boolean, Optional ByVal vOpcion As TipoOpcActivaBoton = TipoOpcActivaBoton.tOpcInicializa)
        If vOpcion = TipoOpcActivaBoton.tOpcConsulta Then
            btnMnuOk.Enabled = False
            btnMnuCancelar.Enabled = valor
            btnMnuSalir.Enabled = Not valor

            btnMnuAltas.Enabled = Not valor
            btnMnuModificar.Enabled = Not valor

            btnMnuEliminar.Enabled = Not valor


            btnMnuReporte.Enabled = Not valor
        ElseIf vOpcion = TipoOpcActivaBoton.tOpcInicializa Then
            btnMnuOk.Enabled = valor
            btnMnuCancelar.Enabled = valor
            btnMnuSalir.Enabled = Not valor

            btnMnuAltas.Enabled = Not valor
            btnMnuModificar.Enabled = Not valor
            btnMnuEliminar.Enabled = Not valor
            btnMnuReporte.Enabled = Not valor

            txtMenuBusca.Enabled = Not valor
            GridTodos.Enabled = Not valor
        ElseIf vOpcion = TipoOpcActivaBoton.tOpcInsertar Then
            btnMnuOk.Enabled = valor
            btnMnuCancelar.Enabled = valor
            btnMnuSalir.Enabled = Not valor

            btnMnuAltas.Enabled = Not valor
            btnMnuModificar.Enabled = Not valor
            btnMnuEliminar.Enabled = Not valor
            btnMnuReporte.Enabled = Not valor
        End If
    End Sub

    Private Sub btnMnuAltas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuAltas.Click
        'ALTAS
        '15/FEB/2016
        'If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Alta) = False Then
        '    MessageBox.Show("No tiene los permisos Necesarios para poder dar de Alta en el Catalogo de Servicios!!!",
        '        "Acceso Denegado a Alta de Servicios", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    OpcionForma = TipoOpcionForma.tOpcConsultar
        '    Exit Sub
        'End If
        OpcionForma = TipoOpcionForma.tOpcInsertar
        LimpiaCampos()
        ActivaCampos(True, TipoOpcActivaCampos.tOpcINICIALIZA)
        ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)

        If bEsIdentidad Then
            txtidUnidadTrans.Enabled = False
            txtidTipoUnidad.Focus()
            txtidTipoUnidad.SelectAll()
        Else
            txtidUnidadTrans.Enabled = True
            txtidUnidadTrans.Focus()
        End If
        GridTodos.Enabled = False

        Status("Ingrese nuevo Servicio para guardar", Me)
    End Sub
    Private Sub btnMnuModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuModificar.Click
        'MODIFICAR
        'If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Modificar) = False Then
        '    MessageBox.Show("No tiene los permisos Necesarios para poder Modificar la Informacion del Servicio!!!",
        '           "Acceso Denegado a Modificacion de la Informacion del Servicio", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    OpcionForma = TipoOpcionForma.tOpcConsultar
        '    Exit Sub
        'End If
        If Trim(txtidUnidadTrans.Text) = "" Then
            txtidUnidadTrans.Text = DespliegaGrid(TablaBd, Inicio.CONSTR, CampoLLave)
            CargaForm(txtidUnidadTrans.Text, UCase(TablaBd))
        End If

        OpcionForma = TipoOpcionForma.tOpcModificar
        ActivaCampos(True, TipoOpcActivaCampos.tOpcINICIALIZA)
        ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)

        btnMnuEliminar.Enabled = True
        If bEsIdentidad Then
            txtidUnidadTrans.Enabled = False
            'txtDescripcionUni.Focus()
        Else
            'txtidUnidadTrans.Focus()
        End If
        txtidTipoUnidad.Focus()


        Status("Ingrese Unidad para guardar", Me)
    End Sub
    Private Sub btnMnuEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuEliminar.Click
        'ELIMINAR
        'If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Baja) = False Then
        '    MessageBox.Show("No tiene los permisos Necesarios para poder Eliminar el Servicio!!!",
        '        "Acceso Denegado a Eliminacion de Servicio", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    OpcionForma = TipoOpcionForma.tOpcConsultar
        '    Exit Sub
        'End If
        If txtidUnidadTrans.Text = UniTrans.iUnidadTrans Then
            With UniTrans
                If MessageBox.Show("Esta completamente seguro que desea Eliminar la unidad: " &
                                   .DescripcionUni & " Con Id: " & .iUnidadTrans & " ????", "Confirma que desea eliminar el Servicio: " & .DescripcionUni & "???",
                                 MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then Exit Sub
                Try
                    .Eliminar() 'Elimina el municipio
                    MessageBox.Show("Servicio: " & .DescripcionUni & " Eliminada con exito!!!",
                                    "Servicio Eliminado con exito", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Status("Servicio: " & .DescripcionUni & " Eliminado con exito!!!", Me)
                    ActualizaGrid(txtMenuBusca.Text)
                    txtidUnidadTrans.Text = ""
                    OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
                Catch ex As Exception
                    Status("Error al tratar de eliminar el Servicio: " & ex.Message, Me, Err:=True)
                    MessageBox.Show("Error al Tratar de eliminar el Servicio: " & .DescripcionUni & vbNewLine &
                                    "Error. " & ex.Message, "Error: " & Err.Number, MessageBoxButtons.OK, MessageBoxIcon.Error)

                End Try
            End With
        End If
    End Sub
    Private Sub btnMnuReporte_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuReporte.Click
        'REPORTE
        'If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Reporte) = False Then
        '    MessageBox.Show("No tiene los permisos Necesarios para poder realizar Reporte de Servicios!!!",
        '        "Acceso Denegado a Reporte de Servicios", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    OpcionForma = TipoOpcionForma.tOpcConsultar
        '    Exit Sub
        'End If
        ToolStripMenu.Enabled = False
        Status("Abriendo Reporte Espere. . . ", Me)
        Me.Cursor = Cursors.WaitCursor
        DespliegaReporte(TablaBd, CONSTR)
        Status("Finalizo el reporte. . . ", Me)
        Me.Cursor = Cursors.Default
        ToolStripMenu.Enabled = True
    End Sub

#End Region
    Private Sub ActualizaGrid(ByVal vidUnidadTrans As String)
        If vidUnidadTrans = "" Then
            GridTodos.DataSource = BD.ExecuteReturn("SELECT idUnidadTrans, descripcionUni FROM " & TablaBd, TablaBd)
            Status(GridTodos.RowCount & " Registros", Me)
        Else
            strsql = "SELECT idUnidadTrans, descripcionUni FROM " & TablaBd &
                " WHERE idUnidadTrans Like '%" & vidUnidadTrans & "%'"
            GridTodos.DataSource = BD.ExecuteReturn(strsql)
            Status(GridTodos.RowCount & " Registros obtenidos con el filtro %" & vidUnidadTrans & "%", Me)
        End If
    End Sub

    Private Sub OpcionMnuTools(ByVal vOpcion As TipoOpcionForma)
        If vOpcion <> TipoOpcionForma.tOpcImprimir And vOpcion <> TipoOpcionForma.tOpcEliminar Then
            OpcionForma = vOpcion
        End If
        txtMenuBusca.Enabled = True
        GridTodos.Enabled = True
        If vOpcion = TipoOpcionForma.tOpcConsultar Then
            'CONSULTA
            For Each ct As Control In GrupoCaptura.Controls
                If Not ct Is cmdBuscaClave Then
                    ct.Enabled = False
                Else
                    ct.Enabled = True
                End If
            Next
            txtMenuBusca.Enabled = True
            btnMnuAltas.Enabled = True
            btnMnuModificar.Enabled = True
            btnMnuEliminar.Enabled = False
            btnMnuReporte.Enabled = True
            btnMnuOk.Enabled = False
            btnMnuCancelar.Enabled = False

            btnMnuSalir.Enabled = True
            GridTodos.Enabled = True

        End If
    End Sub


    Private Sub txtMenuBusca_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtMenuBusca.KeyPress
        If Asc(e.KeyChar) = 39 Then e.Handled = True : Exit Sub 'Caracter ' Evita errores
        If Asc(e.KeyChar) = 13 Then
            GridTodos.Focus()
            e.Handled = True
        End If
    End Sub

    'Private Sub txtMenuBusca_KeyUp(sender As Object, e As KeyEventArgs) Handles txtMenuBusca.KeyUp
    '    If txtMenuBusca.Text.Trim = "" Then Exit Sub
    '    If e.KeyCode = Keys.Down Or e.KeyCode = Keys.Up Then
    '        GridTodos.Focus()
    '        e.Handled = True
    '        Exit Sub
    '    End If
    '    If e.KeyCode = Keys.Left Or e.KeyCode = Keys.Right Then Exit Sub
    '    ActualizaGrid(txtMenuBusca.Text)
    'End Sub



    Private Sub frmCatFamilias_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
        If Inicio.CONSTR = "" Then
            Inicio.CONSTR = _con
        End If
        LlenaCombos()
        ActualizaGrid("")
        GridTodos.Columns(0).Width = GridTodos.Width - 20
        GridTodos.Columns(1).Visible = False
        txtMenuBusca.Focus()

    End Sub
    Private Sub GridTodos_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles GridTodos.CellEnter
        CargaForm(GridTodos.Rows(GridTodos.CurrentRow.Index).Cells(0).Value, UCase(TablaBd))
    End Sub
    Private Sub GridTodos_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles GridTodos.KeyPress
        If Asc(e.KeyChar) = 8 Then
            txtMenuBusca.Focus()
            e.Handled = True
        End If
    End Sub
    Private Sub CargaForm(ByVal idClave As String, ByVal NomTabla As String)
        Try
            Select Case UCase(NomTabla)
                Case UCase(TablaBd)
                    UniTrans = New UnidadesTranspClass(idClave)
                    With UniTrans
                        txtidUnidadTrans.Text = .iUnidadTrans
                        If .Existe Then
                            txtDescripcionUni.Text = .DescripcionUni
                            txtidTipoUnidad.Text = .idTipoUnidad
                            CargaForm(.idTipoUnidad, TablaBd2)
                            txtidSucursal.Text = .idSucursal
                            CargaForm(.idSucursal, "Sucursal")
                            txtIdAlmacen.Text = .idAlmacen
                            CargaForm(.idAlmacen, "Almacen")
                            txtidEmpresa.Text = .idEmpresa
                            CargaForm(.idEmpresa, "Empresa")
                            txtidMarca.Text = .idMarca
                            CargaForm(.idMarca, "Marca")
                            cmbEstatus.SelectedValue = .Estatus

                            CargaLlantasUnidad(.iUnidadTrans)
                            'txtCIDALMACEN_COM.Text = .CIDALMACEN_COM
                            If OpcionForma = TipoOpcionForma.tOpcInsertar Then
                                Status("La Unidad Con id: " & .iUnidadTrans &
                                       " Ya existe!!!, Favor de modificarlo para poder dar de alta a otra Unidad", Me, Err:=True)
                                MessageBox.Show("La Unidad con id: " & .iUnidadTrans &
                                                " YA EXISTE!!!, Favor de Cambiarlo y volver a intentarlo",
                                                "La Unidad con id: " & .iUnidadTrans & " YA EXISTE!!!", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            End If
                        Else 'No existe 
                            If OpcionForma = TipoOpcionForma.tOpcInsertar Then

                                ActivaCampos(True, TipoOpcActivaCampos.tOpcINICIALIZA)
                                ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)
                                btnMnuOk.Enabled = True
                                txtidTipoUnidad.Focus()
                                'txtDescripcionUni.Focus()
                                Status("Listo para Guardar nueva Unidad con id: " & .iUnidadTrans, Me)
                            End If
                        End If
                    End With
                Case UCase(TablaBd2)
                    'aqui()
                    TipoUniTrans = New TipoUniTransClass(Val(idClave))
                    With TipoUniTrans
                        txtidTipoUnidad.Text = .idTipoUniTras
                        If .Existe Then
                            txtnomTipoUniTras.Text = .nomTipoUniTras
                            txtNoLlantas.Text = .NoLlantas
                            txtNoEjes.Text = .NoEjes
                            txtTonelajeMax.Text = .TonelajeMax
                            txtDescripcionUni.Focus()
                            'txtDescripcionUni.SelectAll()
                        Else
                            MsgBox("El Tipo de Unidad con id: " & txtidTipoUnidad.Text & " No Existe")
                            txtidTipoUnidad.Focus()
                            txtidTipoUnidad.SelectAll()
                        End If
                    End With
                Case UCase("Empresa")
                    Empresa = New EmpresaClass(Val(idClave))
                    If Empresa.Existe Then
                        txtRazonSocial.Text = Empresa.RazonSocial
                    End If
                Case UCase("Almacen")
                    Almacen = New AlmacenClass(Val(idClave))
                    If Almacen.Existe Then
                        txtNomAlmacen.Text = Almacen.NomAlmacen
                    End If
                Case UCase("Sucursal")
                    Sucursal = New SucursalClass(Val(idClave))
                    If Sucursal.Existe Then
                        txtNomSucursal.Text = Sucursal.NomSucursal
                    End If
                Case UCase("Marca")
                    Marca = New MarcaClass(Val(idClave))
                    If Marca.Existe Then
                        txtNombreMarca.Text = Marca.NOMBREMARCA
                    End If
                Case UCase("Almacen")
                    Almacen = New AlmacenClass(Val(idClave))
                    If Almacen.Existe Then
                        txtNomAlmacen.Text = Almacen.NomAlmacen
                    End If

            End Select



        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try

    End Sub
    Private Function Validar() As Boolean
        Validar = True
        If Not Inicio.ValidandoCampo(Trim(txtidUnidadTrans.Text), TipoDato.TdCadena) Then
            If Not bEsIdentidad Then
                MsgBox(MENSAJE_CLAVE, vbInformation, "Aviso" & Me.Text)
                If txtidUnidadTrans.Enabled Then
                    txtidUnidadTrans.Focus()
                    txtidUnidadTrans.SelectAll()
                End If
                Validar = False
            End If
        ElseIf Not Inicio.ValidandoCampo(Trim(txtDescripcionUni.Text), TipoDato.TdCadena) Then
            MsgBox("La Descripcion de la unidad No Puede Ser Vacio", vbInformation, "Aviso" & Me.Text)
            If txtDescripcionUni.Enabled Then
                txtDescripcionUni.Focus()
                txtDescripcionUni.SelectAll()
            End If
            Validar = False
        ElseIf Not Inicio.ValidandoCampo(Trim(txtidTipoUnidad.Text), TipoDato.TdNumerico) Then
            MsgBox("El Tipo de Unidad no Puede ser Vacio", vbInformation, "Aviso" & Me.Text)
            If txtidTipoUnidad.Enabled Then
                txtidTipoUnidad.Focus()
                txtidTipoUnidad.SelectAll()
            End If
            Validar = False
        End If
    End Function

    'Private Sub txtidAlmacen_EnabledChanged(sender As Object, e As EventArgs) Handles txtidAlmacen.EnabledChanged
    '    cmdBuscaClave.Enabled = txtidAlmacen.Enabled
    'End Sub
    'CONTROLES
    Private Sub txtidUnidadTrans_KeyDown(sender As Object, e As KeyEventArgs) Handles txtidUnidadTrans.KeyDown
        If e.KeyCode = Keys.F3 Then
            cmdBuscaClave_Click(sender, e)
        End If

    End Sub
    Private Sub txtidAlmacen_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtIdAlmacen.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If txtidUnidadTrans.Text.Trim <> "" Then
                CargaForm(txtidUnidadTrans.Text, UCase(TablaBd))
            End If
            e.Handled = True
        End If
    End Sub
    Private Sub txtNomServicio_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtDescripcionUni.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            txtidEmpresa.Focus()
            txtidEmpresa.SelectAll()
        End If
    End Sub

    Private Sub cmdBuscaClave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBuscaClave.Click
        Dim idEst As String = DespliegaGrid(UCase(TablaBd), Inicio.CONSTR, CampoLLave)
        If idEst <> "" Then CargaForm(idEst, UCase(TablaBd))
    End Sub

    Private Sub txtNomServicio_MouseDown(sender As Object, e As MouseEventArgs) Handles txtDescripcionUni.MouseDown
        txtDescripcionUni.Focus()
        txtDescripcionUni.SelectAll()
    End Sub

    'Private Sub txtidSucursal_EnabledChanged(sender As Object, e As EventArgs) Handles txtidSucursal.EnabledChanged
    '    cmdBuscaClaveTPU.Enabled = txtidTipoUnidad.Enabled
    'End Sub

    Private Sub txtidTipoUnidad_KeyDown(sender As Object, e As KeyEventArgs) Handles txtidTipoUnidad.KeyDown
        If e.KeyCode = Keys.F3 Then
            cmdBuscaClaveTPU_Click(sender, e)
        End If
    End Sub

    Private Sub txtidSucursal_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtidSucursal.KeyPress
        '    btnMnuOk_Click(sender, e)
        '
        'If Asc(e.KeyChar) = 13 Then
        '    If txtidSucursal.Text.Trim <> "" Then
        '        CargaForm(txtidSucursal.Text, TablaBd2)
        '        txtNomSucursal.Focus()
        '        txtNomSucursal.SelectAll()
        '    End If
        '    e.Handled = True
        'End If
        If txtidSucursal.Text.Length = 0 Then
            txtidSucursal.Focus()
            txtidSucursal.SelectAll()
        Else
            If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
                Salida = True
                CargaForm(txtidSucursal.Text, "Sucursal")
                txtIdAlmacen.Focus()
                txtIdAlmacen.SelectAll()

                'txtNomServicio.Focus()
                'txtNomServicio.SelectAll()
                Salida = False
            End If
        End If
    End Sub


    Private Sub txtidTipoUnidad_MouseDown(sender As Object, e As MouseEventArgs) Handles txtidTipoUnidad.MouseDown
        txtidTipoUnidad.Focus()
        txtidTipoUnidad.SelectAll()
    End Sub

    Private Sub cmdBuscaClaveTPU_Click(sender As Object, e As EventArgs) Handles cmdBuscaClaveTPU.Click
        txtidTipoUnidad.Text = DespliegaGrid(UCase(TablaBd2), Inicio.CONSTR, CampoLLave)
        txtidTipoUnidad_Leave(sender, e)
    End Sub

    Private Sub cmdCancelar_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        With TryCast(sender, Button)
            If .Enabled Then
                Me.CancelButton = sender
            End If
        End With
    End Sub
    Private Sub cmdTerminar_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        With TryCast(sender, Button)
            If .Enabled Then
                Me.CancelButton = sender
            End If
        End With
    End Sub

    Private Sub btnMnuOk_Click(sender As Object, e As EventArgs) Handles btnMnuOk.Click
        Dim CadCam As String = ""
        Try
            Salida = True
            If Not Validar() Then Exit Sub
            If GridTodos.RowCount = 0 Then
                'Servicio = New ServicioClass(0)
                UniTrans = New UnidadesTranspClass(0)
            End If
            With UniTrans
                If OpcionForma = TipoOpcionForma.tOpcModificar Then
                    'CadCam = .GetCambios(txtidTipoUnidad.Text, txtDescripcionUni.Text, txtidMarca.Text, Val(txtidSucursal.Text), Val(txtIdAlmacen.Text), cmbEstatus.SelectedValue, Val(txtidEmpresa.Text), 1)
                    CadCam = .GetCambios(txtDescripcionUni.Text, txtidTipoUnidad.Text, txtidMarca.Text, Val(txtidEmpresa.Text), Val(txtidSucursal.Text), cmbEstatus.SelectedValue, Val(txtIdAlmacen.Text))
                    If CadCam <> "" Then
                        'If MessageBox.Show("Esta completamente Seguro que Desea guardar los Cambios Realizados al Estado Con id: " & .IDMARCA & vbNewLine &
                        '                CadCam, "Confirma que desea guardar los Cambios Realizados???", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then Exit Sub
                        Status("Guardando Modificaciones de la Unidad Con id: " & .iUnidadTrans, Me, 1, 2)

                        '.Guardar(bEsIdentidad, txtidTipoUnidad.Text, txtDescripcionUni.Text, txtidMarca.Text, Val(txtidSucursal.Text), Val(txtIdAlmacen.Text), cmbEstatus.SelectedValue, Val(txtidEmpresa.Text))
                        .Guardar(txtDescripcionUni.Text, txtidTipoUnidad.Text, txtidMarca.Text, Val(txtidEmpresa.Text), Val(txtidSucursal.Text), cmbEstatus.SelectedValue, Val(txtIdAlmacen.Text))
                        Status("Unidad con Id: " & .iUnidadTrans & " Modificado Con Exito!!!", Me, 2, 2)
                        MessageBox.Show("Unidad con Id: " & .iUnidadTrans & " Modificada con exito!!!",
                                        "Accion terminada con exito!!!", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
                        ActualizaGrid(txtMenuBusca.Text)
                    Else
                        Status("No se detectaron cambios para realizar!!!", Me, Err:=True)
                    End If
                ElseIf OpcionForma = TipoOpcionForma.tOpcInsertar Then
                    .Existe = False
                    .DescripcionUni = txtDescripcionUni.Text
                    If Not bEsIdentidad Then
                        .iUnidadTrans = txtidUnidadTrans.Text
                    End If
                    Status("Guardando Nueva Unidad: " & txtDescripcionUni.Text & ". . .", Me, 1, 2)
                    '.Guardar(bEsIdentidad, txtidTipoUnidad.Text, txtDescripcionUni.Text, txtidMarca.Text, Val(txtidSucursal.Text), Val(txtIdAlmacen.Text), cmbEstatus.SelectedValue, Val(txtidEmpresa.Text), 1)
                    .Guardar(txtDescripcionUni.Text, txtidTipoUnidad.Text, txtidMarca.Text, Val(txtidEmpresa.Text), Val(txtidSucursal.Text), cmbEstatus.SelectedValue, Val(txtIdAlmacen.Text))
                    Status("Unidad: " & txtDescripcionUni.Text & " Guardado con Exito!!!", Me, 2, 2)
                    MessageBox.Show("Nueva Unidad: " & txtDescripcionUni.Text & " Guardado con exito!!!",
                                    "Accion terminada con exito!!!", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
                    ActualizaGrid(txtMenuBusca.Text)

                End If
            End With
            Salida = False
        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try
    End Sub


    Private Sub btnMnuCancelar_Click(sender As Object, e As EventArgs) Handles btnMnuCancelar.Click
        If txtidUnidadTrans.Text <> "" Then
            CargaForm(txtidUnidadTrans.Text, UCase(TablaBd))
        End If
        OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
    End Sub

    Private Sub btnMnuSalir_Click(sender As Object, e As EventArgs) Handles btnMnuSalir.Click
        Me.Close()
    End Sub

    Private Sub txtMenuBusca_TextChanged(sender As Object, e As EventArgs) Handles txtMenuBusca.TextChanged
        ActualizaGrid(txtMenuBusca.Text)
    End Sub

    Private Sub txtCP_KeyPress(sender As Object, e As KeyPressEventArgs)
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            txtIdAlmacen.Focus()
            txtIdAlmacen.SelectAll()
        End If
    End Sub

    Private Sub txtCP_MouseDown(sender As Object, e As MouseEventArgs)
        txtidSucursal.Focus()
        txtidSucursal.SelectAll()
    End Sub

    Private Sub txtDuracionHoras_KeyPress(sender As Object, e As KeyPressEventArgs)
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            txtidEmpresa.Focus()
            txtidEmpresa.SelectAll()
        End If
    End Sub

    Private Sub txtDuracionHoras_MouseDown(sender As Object, e As MouseEventArgs)
        txtIdAlmacen.Focus()
        txtIdAlmacen.SelectAll()
    End Sub

    Private Sub GrupoCaptura_Enter(sender As Object, e As EventArgs) Handles GrupoCaptura.Enter

    End Sub

    Private Function LlenaCombos() As Boolean
        Dim StrSql As String = ""
        LlenaCombos = True
        Try
            DsCombo.Clear()
            StrSql = "select 'ACT' as IdEstatus, 'ACTIVO' as Estatus " &
            "union " &
            "select 'BAJ' as IdEstatus, 'BAJA' as Estatus"

            DsCombo = Inicio.LlenaCombos("IdEstatus", "Estatus", "CatEstatus",
                               TipoConexion.TcSQL, Inicio.CONSTR, Process.GetCurrentProcess.ProcessName,
                               KEY_CHECAERROR, KEY_ESTATUS, , StrSql)
            'DsCombo = BD.ExecuteReturn(StrSql)
            If Not DsCombo Is Nothing Then
                If DsCombo.Tables("CatEstatus").DefaultView.Count > 0 Then
                    cmbEstatus.DataSource = DsCombo.Tables("CatEstatus")
                    cmbEstatus.ValueMember = "IdEstatus"
                    cmbEstatus.DisplayMember = "Estatus"
                End If
            Else
                MsgBox("No se han dado de alta a Estatus ", vbInformation, "Aviso" & Me.Text)
                LlenaCombos = False
                Exit Function
            End If
        Catch ex As Exception

        End Try
    End Function

    Private Sub txtidMarca_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtidMarca.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            txtidSucursal.Focus()
            txtidSucursal.SelectAll()
        End If
    End Sub

    Private Sub txtidMarca_MouseDown(sender As Object, e As MouseEventArgs) Handles txtidMarca.MouseDown
        txtidMarca.Focus()
        txtidMarca.SelectAll()
    End Sub

    Private Sub txtCosto_KeyPress(sender As Object, e As KeyPressEventArgs)
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            cmbEstatus.Focus()
        End If
    End Sub

    Private Sub txtCosto_MouseDown(sender As Object, e As MouseEventArgs)
        txtidEmpresa.Focus()
        txtidEmpresa.SelectAll()
    End Sub

    Private Sub cmbEstatus_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbEstatus.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            btnMnuOk_Click(sender, e)
        End If
    End Sub

    Private Sub txtidTipoServicio_TextChanged(sender As Object, e As EventArgs) Handles txtidTipoUnidad.TextChanged

    End Sub

    Private Sub txtidEmpresa_TextChanged(sender As Object, e As EventArgs) Handles txtidEmpresa.TextChanged

    End Sub

    Private Sub txtidEmpresa_EnabledChanged(sender As Object, e As EventArgs) Handles txtidEmpresa.EnabledChanged
        cmdBuscaClaveEmp.Enabled = txtidEmpresa.Enabled
    End Sub

    Private Sub txtidMarca_TextChanged(sender As Object, e As EventArgs) Handles txtidMarca.TextChanged

    End Sub

    Private Sub txtidMarca_EnabledChanged(sender As Object, e As EventArgs) Handles txtidMarca.EnabledChanged
        cmdBuscaClaveMar.Enabled = txtidMarca.Enabled
    End Sub

    Private Sub txtidSucursal_TextChanged(sender As Object, e As EventArgs) Handles txtidSucursal.TextChanged

    End Sub

    Private Sub txtidSucursal_EnabledChanged(sender As Object, e As EventArgs) Handles txtidSucursal.EnabledChanged
        cmdBuscaClaveSuc.Enabled = txtidSucursal.Enabled
    End Sub

    Private Sub txtIdAlmacen_TextChanged(sender As Object, e As EventArgs) Handles txtIdAlmacen.TextChanged

    End Sub

    Private Sub txtIdAlmacen_EnabledChanged(sender As Object, e As EventArgs) Handles txtIdAlmacen.EnabledChanged
        cmdBuscaClaveAlm.Enabled = txtIdAlmacen.Enabled
    End Sub

    Private Sub txtidEmpresa_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtidEmpresa.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            txtidMarca.Focus()
            txtidMarca.SelectAll()
        End If
    End Sub

    Private Sub txtidTipoUnidad_EnabledChanged(sender As Object, e As EventArgs) Handles txtidTipoUnidad.EnabledChanged
        cmdBuscaClaveTPU.Enabled = txtidTipoUnidad.Enabled

    End Sub

    Private Sub txtidTipoUnidad_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtidTipoUnidad.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            Salida = True
            CargaForm(txtidTipoUnidad.Text, UCase(TablaBd2))
            Salida = False
            txtDescripcionUni.Focus()
        End If
    End Sub

    Private Sub txtDescripcionUni_TextChanged(sender As Object, e As EventArgs) Handles txtDescripcionUni.TextChanged

    End Sub

    Private Sub txtidEmpresa_MouseDown(sender As Object, e As MouseEventArgs) Handles txtidEmpresa.MouseDown
        txtidEmpresa.Focus()
        txtidEmpresa.SelectAll()

    End Sub

    Private Sub txtidSucursal_MouseDown(sender As Object, e As MouseEventArgs) Handles txtidSucursal.MouseDown
        txtidSucursal.Focus()
        txtidSucursal.SelectAll()

    End Sub

    Private Sub txtIdAlmacen_MouseDown(sender As Object, e As MouseEventArgs) Handles txtIdAlmacen.MouseDown
        txtIdAlmacen.Focus()
        txtIdAlmacen.SelectAll()

    End Sub

    Private Sub cmdBuscaClaveEmp_Click(sender As Object, e As EventArgs) Handles cmdBuscaClaveEmp.Click
        txtidEmpresa.Text = DespliegaGrid("CatEmpresas", Inicio.CONSTR, "idEmpresa")
        txtidEmpresa_Leave(sender, e)
    End Sub

    Private Sub txtidEmpresa_Leave(sender As Object, e As EventArgs) Handles txtidEmpresa.Leave
        If Salida Then Exit Sub
        If txtidEmpresa.Text.Length = 0 Then
            txtidEmpresa.Focus()
            txtidEmpresa.SelectAll()
        Else
            If txtidEmpresa.Enabled Then
                CargaForm(txtidEmpresa.Text, "Empresa")
                txtidMarca.Focus()
                txtidMarca.SelectAll()

            End If
        End If
    End Sub

    Private Sub txtidUnidadTrans_TextChanged(sender As Object, e As EventArgs) Handles txtidUnidadTrans.TextChanged

    End Sub

    Private Sub txtidEmpresa_KeyDown(sender As Object, e As KeyEventArgs) Handles txtidEmpresa.KeyDown
        If e.KeyCode = Keys.F3 Then
            cmdBuscaClaveEmp_Click(sender, e)
        End If
    End Sub

    Private Sub cmdBuscaClaveMar_Click(sender As Object, e As EventArgs) Handles cmdBuscaClaveMar.Click
        txtidMarca.Text = DespliegaGrid("CatMarcas", Inicio.CONSTR, "idMarca")
        txtidMarca_Leave(sender, e)

    End Sub

    Private Sub txtidMarca_Leave(sender As Object, e As EventArgs) Handles txtidMarca.Leave
        If Salida Then Exit Sub
        If txtidMarca.Text.Length = 0 Then
            txtidMarca.Focus()
            txtidMarca.SelectAll()
        Else
            If txtidMarca.Enabled Then
                CargaForm(txtidMarca.Text, "Marca")
                txtidSucursal.Focus()
                txtidSucursal.SelectAll()

            End If
        End If
    End Sub

    Private Sub txtidMarca_KeyDown(sender As Object, e As KeyEventArgs) Handles txtidMarca.KeyDown
        If e.KeyCode = Keys.F3 Then
            cmdBuscaClaveMar_Click(sender, e)
        End If
    End Sub

    Private Sub cmdBuscaClaveSuc_Click(sender As Object, e As EventArgs) Handles cmdBuscaClaveSuc.Click
        txtidSucursal.Text = DespliegaGrid("CatSucursal", Inicio.CONSTR, "id_sucursal")
        txtidSucursal_Leave(sender, e)
    End Sub

    Private Sub txtidSucursal_Leave(sender As Object, e As EventArgs) Handles txtidSucursal.Leave
        If Salida Then Exit Sub
        If txtidSucursal.Text.Length = 0 Then
            txtidSucursal.Focus()
            txtidSucursal.SelectAll()
        Else
            If txtidSucursal.Enabled Then
                CargaForm(txtidSucursal.Text, "Sucursal")
                txtIdAlmacen.Focus()
                txtIdAlmacen.SelectAll()

            End If
        End If
    End Sub

    'Private Sub txtidSucursal_KeyDown(sender As Object, e As KeyEventArgs) Handles txtidSucursal.KeyDown, txtidTipoUnidad.KeyDown
    '    If e.KeyCode = Keys.F3 Then
    '        cmdBuscaClaveSuc_Click(sender, e)
    '    End If
    'End Sub

    Private Sub cmdBuscaClaveAlm_Click(sender As Object, e As EventArgs) Handles cmdBuscaClaveAlm.Click
        txtIdAlmacen.Text = DespliegaGrid("CatAlmacen", Inicio.CONSTR, "idAlmacen")



        txtIdAlmacen_Leave(sender, e)
    End Sub

    Private Sub txtIdAlmacen_Leave(sender As Object, e As EventArgs) Handles txtIdAlmacen.Leave
        If txtIdAlmacen.Enabled Then
            CargaForm(txtIdAlmacen.Text, "Almacen")
            cmbEstatus.Focus()


        End If
    End Sub

    Private Sub txtIdAlmacen_KeyDown(sender As Object, e As KeyEventArgs) Handles txtIdAlmacen.KeyDown
        If e.KeyCode = Keys.F3 Then
            cmdBuscaClaveAlm_Click(sender, e)
        End If
    End Sub

    Private Sub txtidUnidadTrans_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtidUnidadTrans.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If txtidUnidadTrans.Text.Trim <> "" Then
                CargaForm(txtidUnidadTrans.Text, UCase(TablaBd))
            End If
            e.Handled = True
        End If
    End Sub

    Private Sub txtidSucursal_KeyDown(sender As Object, e As KeyEventArgs) Handles txtidSucursal.KeyDown
        If e.KeyCode = Keys.F3 Then
            cmdBuscaClaveSuc_Click(sender, e)
        End If
    End Sub

    Private Sub txtidTipoUnidad_Leave(sender As Object, e As EventArgs) Handles txtidTipoUnidad.Leave
        If Salida Then Exit Sub
        If txtidTipoUnidad.Text.Length = 0 Then
            txtidTipoUnidad.Focus()
            txtidTipoUnidad.SelectAll()
        Else
            If txtidTipoUnidad.Enabled Then
                Salida = True
                CargaForm(txtidTipoUnidad.Text, "CatTipoUniTrans")
                Salida = False
                txtDescripcionUni.Focus()


            End If
        End If
    End Sub

    'Private Sub txtidSucursal_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtidSucursal.KeyPress, txtidTipoUnidad.KeyPress
    '    If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
    '        txtIdAlmacen.Focus()
    '        txtIdAlmacen.SelectAll()
    '    End If

    'End Sub
    'llantas 3/julio/2017
    Private Sub CargaLlantasUnidad(ByVal idUnidad As String)
        txtUniTrans.Text = idUnidad
        UniTrans = New UnidadesTranspClass(idUnidad)
        If UniTrans.Existe Then
            TipUni = New TipoUniTransClass(UniTrans.idTipoUnidad)
            If TipUni.Existe Then
                txtTipoUni.Text = TipUni.Clasificacion
                ClasLlan = New ClasLlanClass(TipUni.idClasLlan)
                If ClasLlan.Existe Then
                    DibujaEsquema(TipUni.idClasLlan, TipUni.NoLlantas)
                    If txtPosicion.Text <> "" Then
                        SeleccionaPosicion(Val(txtPosicion.Text))
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub DibujaEsquema(ByVal vidClasLlan As Integer, ByVal TotalLlantas As Integer)
        Dim vNoEje As Integer
        Dim vNoLlantas As Integer
        Dim LlantxLado As Integer
        Dim NoPosicion As Integer = 0
        Dim TextoToolTip As String = ""

        'Dim FactorX As Integer = 71
        Dim FactorX As Integer = 201

        'Dim FactorY As Integer = 41
        Dim FactorY As Integer = 70

        Dim PosX As Integer = 29
        'Dim PosFacLinX As Integer = 201
        Dim PosFacLinX As Integer = 195

        'Dim PosY As Integer = 62
        Dim PosY As Integer = 37

        ReDim rdbPosicion(TotalLlantas - 1)
        For i = 0 To rdbPosicion.Length - 1
            rdbPosicion(i) = New RadioButton()
        Next
        'ReDim tt(TotalLlantas - 1)

        LimpiaPosiciones()

        ClasLlan = New ClasLlanClass(0)
        TablaEstruc = New DataTable

        TablaEstruc = ClasLlan.TablaPosLlanta(vidClasLlan)
        If Not TablaEstruc Is Nothing Then
            If TablaEstruc.Rows.Count > 0 Then
                For i = 0 To TablaEstruc.Rows.Count - 1
                    vNoEje = TablaEstruc.DefaultView.Item(i).Item("EjeNo")
                    vNoLlantas = TablaEstruc.DefaultView.Item(i).Item("NoLlantas")
                    LlantxLado = vNoLlantas / 2
                    'Posiciones Iniciales
                    If LlantxLado = 0 Then
                        'PosX = 29
                        'FactorX = 71
                        PosX = 95
                        FactorX = 37
                    ElseIf LlantxLado = 1 Then
                        'PosX = 29
                        'FactorX = 71
                        PosX = 50
                        FactorX = 37
                    Else
                        'PosX = 5
                        PosX = 25
                        'FactorX = 99
                        'FactorX = 90
                        FactorX = 95
                    End If

                    For z = 0 To vNoLlantas - 1
                        'rdbPosicion(NoPosicion) = New RadioButton

                        rdbPosicion(NoPosicion).Name = "pos" & NoPosicion
                        'Verificamos Nombre de la llanta()
                        llanta = New LlantasClass((NoPosicion + 1), OpcionLlanta.Posicion, txtUniTrans.Text)
                        If llanta.Existe Then
                            rdbPosicion(NoPosicion).Text = NoPosicion + 1 & " (" & llanta.idLlanta & " )"
                            'TextoToolTip = "idLlanta = " & llanta.idLlanta & vbCrLf &
                            '                "Empresa = " & llanta.idEmpresa & vbCrLf &
                            '                "Marca = " & llanta.idMarca & vbCrLf &
                            '                "Dise�o = " & llanta.Dise�o & vbCrLf &
                            '                "Dise�o = " & llanta.Dise�o & vbCrLf &
                            '                "Medida = " & llanta.Medida & vbCrLf &
                            '                "Profundidad = " & llanta.Profundidad & vbCrLf &
                            '                "TipoLlanta = " & llanta.idTipoLLanta & vbCrLf &
                            '                "idunidad = " & llanta.idUnidadTrans & vbCrLf &
                            '                "Posicion = " & llanta.idUnidadTrans
                        Else
                            rdbPosicion(NoPosicion).Text = NoPosicion + 1

                        End If
                        'rdbPosicion(NoPosicion).AutoSize = True
                        rdbPosicion(NoPosicion).Checked = False

                        'rdbPosicion(NoPosicion).Size = New System.Drawing.Size(31, 17)

                        'rdbPosicion(NoPosicion).Size = New System.Drawing.Size(95, 64)
                        rdbPosicion(NoPosicion).Size = New System.Drawing.Size(80, 64)

                        rdbPosicion(NoPosicion).Image = My.Resources.llanta2
                        rdbPosicion(NoPosicion).TextImageRelation = TextImageRelation.Overlay
                        rdbPosicion(NoPosicion).ImageAlign = ContentAlignment.TopCenter
                        rdbPosicion(NoPosicion).TextAlign = ContentAlignment.BottomCenter
                        rdbPosicion(NoPosicion).ForeColor = Color.Blue


                        'rdbPosicion(i).Location = New Point(6, 4)

                        If z = 0 And vNoLlantas > 2 Then
                            PosX = 5
                        End If
                        rdbPosicion(NoPosicion).Location = New Point(PosX, PosY)

                        AddHandler rdbPosicion(NoPosicion).Click, AddressOf RadioButtons_Click
                        AddHandler rdbPosicion(NoPosicion).KeyDown, AddressOf ObjEnter_KeyDown
                        AddHandler rdbPosicion(NoPosicion).MouseHover, AddressOf RadioButtons_MouseHover
                        'AddHandler rdbPosicion(NoPosicion).MouseEnter, AddressOf RadioButtons_MouseHover
                        'PanelControles.Controls.Add(rdbLogico1(i))
                        GpoPosiciones.Controls.Add(rdbPosicion(NoPosicion))
                        'PanelLogico1(i).Controls.Add(rdbLogico1(i))
                        PosX = PosX + FactorX

                        If z < vNoLlantas - 1 Then
                            NoPosicion = NoPosicion + 1
                        End If

                        If LlantxLado = 1 Then
                            PosX = PosX + PosFacLinX - FactorX
                        End If

                    Next
                    PosY = PosY + FactorY
                    NoPosicion = NoPosicion + 1
                Next
            End If
        End If
    End Sub
    Private Sub LimpiaPosiciones()
        Dim num_controles As Int32 = Me.GpoPosiciones.Controls.Count - 1
        For n As Integer = num_controles To 0 Step -1
            Dim ctrl As Windows.Forms.Control = Me.GpoPosiciones.Controls(n)
            If TypeOf ctrl Is RadioButton Then

            End If
            Me.GpoPosiciones.Controls.Remove(ctrl)
            ctrl.Dispose()
        Next
        DibujaLinea()
    End Sub
    Private Sub DibujaLinea()
        'Dim canvas As New ShapeContainer
        'Dim theLine As New LineShape
        '' Set the form as the parent of the ShapeContainer.
        'canvas.Parent = Me.GpoPosiciones
        '' Set the ShapeContainer as the parent of the LineShape.
        'theLine.Parent = canvas
        '' Set the starting and ending coordinates for the line.
        'theLine.BorderWidth = 10
        'theLine.StartPoint = New System.Drawing.Point(183, 25)
        'theLine.EndPoint = New System.Drawing.Point(183, 300)
    End Sub
    Private Sub RadioButtons_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs)
        With TryCast(sender, RadioButton)
            pos = ObtienePosicion(.Text)
            '//////// llena datos LLANTA /////////////////
            llanta = New LlantasClass(pos, OpcionLlanta.Posicion, txtUniTrans.Text)
            If llanta.Existe Then
                Empresa = New EmpresaClass(llanta.idEmpresa)
                If Empresa.Existe Then
                    NomEmpresa = Empresa.RazonSocial
                End If

                llantaProd = New LLantasProductoClass(llanta.idProductoLlanta)
                If llantaProd.Existe Then
                    Marca = New MarcaClass(llantaProd.idMarca)
                    If Marca.Existe Then
                        NomMarca = Marca.NOMBREMARCA
                    End If
                End If



                tipllanta = New TipoLlantaClass(llanta.idTipoLLanta)
                If tipllanta.Existe Then
                    NomTipLlanta = tipllanta.Descripcion
                End If

                Disenio = New CatDiseniosClass(llantaProd.idDisenio)
                If Disenio.Existe Then
                    NomDisenio = Disenio.Descripcion
                End If

                TextoToolTip = "idLlanta = " & llanta.idLlanta & vbCrLf &
                                "Empresa = " & NomEmpresa & vbCrLf &
                                "Marca = " & NomMarca & vbCrLf &
                                "Dise�o = " & NomDisenio & vbCrLf &
                                "Medida = " & llantaProd.Medida & vbCrLf &
                                "Profundidad = " & llantaProd.Profundidad & vbCrLf &
                                "TipoLlanta = " & NomTipLlanta & vbCrLf &
                                "ubicacion = " & llanta.idUbicacion & vbCrLf &
                                "Posicion = " & llanta.Posicion
            Else

                TextoToolTip = pos
            End If
            tt.IsBalloon = True
            tt.ShowAlways = True
            tt.SetToolTip(sender, TextoToolTip)
            '//////// llena datos LLANTA /////////////////
            Thread.Sleep(2500)
            'threads


        End With
    End Sub
    Private Sub RadioButtons_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        With TryCast(sender, RadioButton)
            pos = ObtienePosicion(.Text)

            txtPosicion.Text = pos
            '//////// llena datos LLANTA /////////////////
            llanta = New LlantasClass(pos, txtUniTrans.Text)
            If llanta.Existe Then
                Empresa = New EmpresaClass(llanta.idEmpresa)
                If Empresa.Existe Then
                    NomEmpresa = Empresa.RazonSocial
                End If
                llantaProd = New LLantasProductoClass(llanta.idProductoLlanta)
                If llantaProd.Existe Then
                    Marca = New MarcaClass(llantaProd.idMarca)
                    If Marca.Existe Then
                        NomMarca = Marca.NOMBREMARCA
                    End If
                End If


                tipllanta = New TipoLlantaClass(llanta.idTipoLLanta)
                If tipllanta.Existe Then
                    NomTipLlanta = tipllanta.Descripcion
                End If

                Disenio = New CatDiseniosClass(llantaProd.idDisenio)
                If Disenio.Existe Then
                    NomDisenio = Disenio.Descripcion
                End If

                TextoToolTip = "idLlanta = " & llanta.idLlanta & vbCrLf &
                                "Empresa = " & NomEmpresa & vbCrLf &
                                "Marca = " & NomMarca & vbCrLf &
                                "Dise�o = " & NomDisenio & vbCrLf &
                                "Medida = " & llantaProd.Medida & vbCrLf &
                                "Profundidad = " & llantaProd.Profundidad & vbCrLf &
                                "TipoLlanta = " & NomTipLlanta & vbCrLf &
                                "Ubicacion = " & llanta.idUbicacion & vbCrLf &
                                "Posicion = " & llanta.Posicion
            Else

                TextoToolTip = pos
            End If
            tt.IsBalloon = True
            tt.ShowAlways = True
            tt.SetToolTip(txtPosicion, TextoToolTip)
            '//////// llena datos LLANTA /////////////////


            For i = 0 To rdbPosicion.Count - 1
                rdbPosicion(i).BackColor = SystemColors.Control
            Next
            .BackColor = Color.Yellow
            'MessageBox.Show("El Label Tiene Nombre: " & .Name & " y Tiene Escrito:" & .Text, "informacion del Label", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End With
    End Sub
    Private Sub ObjEnter_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        If e.KeyCode = 13 Then
            SendKeys.Send("{TAB}")
            e.Handled = True
        End If
    End Sub
    Private Sub SeleccionaPosicion(ByVal Posicion As Integer)
        If Posicion > rdbPosicion.Count Then
            MsgBox("La Estructura del tipo de unidad " & txtTipoUni.Text & " No cuenta con la posicion " & txtPosicion.Text & vbCrLf & "Favor de seleccionar la Posicion Correspondiente", MsgBoxStyle.Information, Me.Text)
            txtPosicion.Text = ""
            For i = 0 To rdbPosicion.Count - 1
                rdbPosicion(i).BackColor = SystemColors.Control
                rdbPosicion(i).Checked = False
            Next

        Else
            rdbPosicion(Posicion - 1).Checked = True
            For i = 0 To rdbPosicion.Count - 1
                rdbPosicion(i).BackColor = SystemColors.Control
            Next
            rdbPosicion(Posicion - 1).BackColor = Color.Yellow

        End If


    End Sub

    Private Function ObtienePosicion(ByVal Cadena As String) As Integer
        Dim PosiblePos As String
        Dim RespuestaPos As Integer = 0

        PosiblePos = Mid(Cadena, 1, 2)
        If IsNumeric(PosiblePos) Then
            RespuestaPos = Val(PosiblePos)
        Else
            PosiblePos = Mid(Cadena, 1, 1)
            If IsNumeric(PosiblePos) Then
                RespuestaPos = Val(PosiblePos)
            End If
        End If

        Return RespuestaPos

    End Function

    Private Sub txtMenuBusca_Click(sender As Object, e As EventArgs) Handles txtMenuBusca.Click

    End Sub
End Class
