'Fecha: 15 / Febrero / 2016
'ROGER GALA PACHECO
'************************************************************************************************************************************
'*  Fecha Modificacion :   15 / Febrero / 2016                                                                                       
'*  
'************************************************************************************************************************************
Public Class frmCatAlmacen
    Inherits System.Windows.Forms.Form
    Dim OpcionForma As TipoOpcionForma
    Dim TablaBd As String = "CatAlmacen"
    Dim CampoLLave As String = "idAlmacen"
    Dim TablaBd2 As String = "CatSucursal"
    Public WithEvents MeStatus1 As ControlStatus.MeStatus
    Friend WithEvents GridTodos As System.Windows.Forms.DataGridView
    Private Almacen As AlmacenClass
    Private Sucursal As SucursalClass
    Private _Tag As String
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cmdBuscaClaveSuc As System.Windows.Forms.Button
    Friend WithEvents txtidSucursal As System.Windows.Forms.TextBox
    Friend WithEvents txtNomSucursal As System.Windows.Forms.TextBox
    Dim bEsIdentidad As Boolean = True
    Friend WithEvents txtCIDALMACEN_COM As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cmdBuscaClaveALm As System.Windows.Forms.Button
    Friend WithEvents txtCNOMBREALMACEN As System.Windows.Forms.TextBox
    Dim Salida As Boolean
    Private con As String
    Friend WithEvents btnMnuSalir As ToolStripButton
    Friend WithEvents btnMnuOk As ToolStripButton
    Friend WithEvents btnMnuCancelar As ToolStripButton
    Private v As Boolean




#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents GrupoCaptura As System.Windows.Forms.GroupBox
    Friend WithEvents ToolStripMenu As System.Windows.Forms.ToolStrip
    Friend WithEvents txtMenuBusca As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents btnMnuAltas As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuModificar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuEliminar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuReporte As System.Windows.Forms.ToolStripButton
    Friend WithEvents label1 As System.Windows.Forms.Label
    Friend WithEvents label2 As System.Windows.Forms.Label
    Friend WithEvents cmdBuscaClave As System.Windows.Forms.Button
    Friend WithEvents txtidAlmacen As System.Windows.Forms.TextBox
    Friend WithEvents txtNomAlmacen As System.Windows.Forms.TextBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCatAlmacen))
        Me.txtidAlmacen = New System.Windows.Forms.TextBox()
        Me.label1 = New System.Windows.Forms.Label()
        Me.txtNomAlmacen = New System.Windows.Forms.TextBox()
        Me.label2 = New System.Windows.Forms.Label()
        Me.GrupoCaptura = New System.Windows.Forms.GroupBox()
        Me.txtCIDALMACEN_COM = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cmdBuscaClaveALm = New System.Windows.Forms.Button()
        Me.txtCNOMBREALMACEN = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cmdBuscaClaveSuc = New System.Windows.Forms.Button()
        Me.txtidSucursal = New System.Windows.Forms.TextBox()
        Me.txtNomSucursal = New System.Windows.Forms.TextBox()
        Me.cmdBuscaClave = New System.Windows.Forms.Button()
        Me.ToolStripMenu = New System.Windows.Forms.ToolStrip()
        Me.txtMenuBusca = New System.Windows.Forms.ToolStripTextBox()
        Me.btnMnuAltas = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuModificar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuEliminar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuReporte = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuOk = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuCancelar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuSalir = New System.Windows.Forms.ToolStripButton()
        Me.MeStatus1 = New ControlStatus.MeStatus()
        Me.GridTodos = New System.Windows.Forms.DataGridView()
        Me.GrupoCaptura.SuspendLayout()
        Me.ToolStripMenu.SuspendLayout()
        CType(Me.GridTodos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtidAlmacen
        '
        Me.txtidAlmacen.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtidAlmacen.Location = New System.Drawing.Point(109, 27)
        Me.txtidAlmacen.Name = "txtidAlmacen"
        Me.txtidAlmacen.Size = New System.Drawing.Size(69, 20)
        Me.txtidAlmacen.TabIndex = 4
        '
        'label1
        '
        Me.label1.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.label1.Location = New System.Drawing.Point(6, 27)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(94, 20)
        Me.label1.TabIndex = 14
        Me.label1.Text = "id Almacen:"
        '
        'txtNomAlmacen
        '
        Me.txtNomAlmacen.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNomAlmacen.Location = New System.Drawing.Point(109, 59)
        Me.txtNomAlmacen.MaxLength = 50
        Me.txtNomAlmacen.Name = "txtNomAlmacen"
        Me.txtNomAlmacen.Size = New System.Drawing.Size(390, 20)
        Me.txtNomAlmacen.TabIndex = 50
        '
        'label2
        '
        Me.label2.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.label2.Location = New System.Drawing.Point(5, 59)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(98, 20)
        Me.label2.TabIndex = 16
        Me.label2.Text = "Nombre Almacen:"
        '
        'GrupoCaptura
        '
        Me.GrupoCaptura.Controls.Add(Me.txtCIDALMACEN_COM)
        Me.GrupoCaptura.Controls.Add(Me.Label4)
        Me.GrupoCaptura.Controls.Add(Me.cmdBuscaClaveALm)
        Me.GrupoCaptura.Controls.Add(Me.txtCNOMBREALMACEN)
        Me.GrupoCaptura.Controls.Add(Me.Label3)
        Me.GrupoCaptura.Controls.Add(Me.cmdBuscaClaveSuc)
        Me.GrupoCaptura.Controls.Add(Me.txtidSucursal)
        Me.GrupoCaptura.Controls.Add(Me.txtNomSucursal)
        Me.GrupoCaptura.Controls.Add(Me.label1)
        Me.GrupoCaptura.Controls.Add(Me.label2)
        Me.GrupoCaptura.Controls.Add(Me.cmdBuscaClave)
        Me.GrupoCaptura.Controls.Add(Me.txtidAlmacen)
        Me.GrupoCaptura.Controls.Add(Me.txtNomAlmacen)
        Me.GrupoCaptura.Location = New System.Drawing.Point(168, 45)
        Me.GrupoCaptura.Name = "GrupoCaptura"
        Me.GrupoCaptura.Size = New System.Drawing.Size(524, 222)
        Me.GrupoCaptura.TabIndex = 3
        Me.GrupoCaptura.TabStop = False
        '
        'txtCIDALMACEN_COM
        '
        Me.txtCIDALMACEN_COM.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCIDALMACEN_COM.Location = New System.Drawing.Point(109, 135)
        Me.txtCIDALMACEN_COM.Name = "txtCIDALMACEN_COM"
        Me.txtCIDALMACEN_COM.Size = New System.Drawing.Size(69, 20)
        Me.txtCIDALMACEN_COM.TabIndex = 22
        '
        'Label4
        '
        Me.Label4.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label4.Location = New System.Drawing.Point(6, 135)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(114, 20)
        Me.Label4.TabIndex = 24
        Me.Label4.Text = "CIDALMACEN:"
        '
        'cmdBuscaClaveALm
        '
        Me.cmdBuscaClaveALm.Image = CType(resources.GetObject("cmdBuscaClaveALm.Image"), System.Drawing.Image)
        Me.cmdBuscaClaveALm.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdBuscaClaveALm.Location = New System.Drawing.Point(186, 127)
        Me.cmdBuscaClaveALm.Name = "cmdBuscaClaveALm"
        Me.cmdBuscaClaveALm.Size = New System.Drawing.Size(32, 32)
        Me.cmdBuscaClaveALm.TabIndex = 23
        Me.cmdBuscaClaveALm.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'txtCNOMBREALMACEN
        '
        Me.txtCNOMBREALMACEN.Enabled = False
        Me.txtCNOMBREALMACEN.Location = New System.Drawing.Point(224, 139)
        Me.txtCNOMBREALMACEN.Name = "txtCNOMBREALMACEN"
        Me.txtCNOMBREALMACEN.ReadOnly = True
        Me.txtCNOMBREALMACEN.Size = New System.Drawing.Size(275, 20)
        Me.txtCNOMBREALMACEN.TabIndex = 21
        '
        'Label3
        '
        Me.Label3.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label3.Location = New System.Drawing.Point(6, 97)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(94, 20)
        Me.Label3.TabIndex = 20
        Me.Label3.Text = "Id Sucursal:"
        '
        'cmdBuscaClaveSuc
        '
        Me.cmdBuscaClaveSuc.Image = CType(resources.GetObject("cmdBuscaClaveSuc.Image"), System.Drawing.Image)
        Me.cmdBuscaClaveSuc.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdBuscaClaveSuc.Location = New System.Drawing.Point(186, 89)
        Me.cmdBuscaClaveSuc.Name = "cmdBuscaClaveSuc"
        Me.cmdBuscaClaveSuc.Size = New System.Drawing.Size(32, 32)
        Me.cmdBuscaClaveSuc.TabIndex = 19
        Me.cmdBuscaClaveSuc.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'txtidSucursal
        '
        Me.txtidSucursal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtidSucursal.Location = New System.Drawing.Point(109, 97)
        Me.txtidSucursal.Name = "txtidSucursal"
        Me.txtidSucursal.Size = New System.Drawing.Size(69, 20)
        Me.txtidSucursal.TabIndex = 18
        '
        'txtNomSucursal
        '
        Me.txtNomSucursal.Enabled = False
        Me.txtNomSucursal.Location = New System.Drawing.Point(224, 101)
        Me.txtNomSucursal.Name = "txtNomSucursal"
        Me.txtNomSucursal.ReadOnly = True
        Me.txtNomSucursal.Size = New System.Drawing.Size(275, 20)
        Me.txtNomSucursal.TabIndex = 17
        '
        'cmdBuscaClave
        '
        Me.cmdBuscaClave.Image = CType(resources.GetObject("cmdBuscaClave.Image"), System.Drawing.Image)
        Me.cmdBuscaClave.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdBuscaClave.Location = New System.Drawing.Point(186, 19)
        Me.cmdBuscaClave.Name = "cmdBuscaClave"
        Me.cmdBuscaClave.Size = New System.Drawing.Size(32, 32)
        Me.cmdBuscaClave.TabIndex = 5
        Me.cmdBuscaClave.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'ToolStripMenu
        '
        Me.ToolStripMenu.AutoSize = False
        Me.ToolStripMenu.BackColor = System.Drawing.Color.Silver
        Me.ToolStripMenu.Dock = System.Windows.Forms.DockStyle.None
        Me.ToolStripMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.txtMenuBusca, Me.btnMnuAltas, Me.btnMnuModificar, Me.btnMnuEliminar, Me.btnMnuReporte, Me.btnMnuOk, Me.btnMnuCancelar, Me.btnMnuSalir})
        Me.ToolStripMenu.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow
        Me.ToolStripMenu.Location = New System.Drawing.Point(3, 0)
        Me.ToolStripMenu.Name = "ToolStripMenu"
        Me.ToolStripMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.ToolStripMenu.Size = New System.Drawing.Size(730, 42)
        Me.ToolStripMenu.TabIndex = 1
        Me.ToolStripMenu.Text = "ToolStrip1"
        '
        'txtMenuBusca
        '
        Me.txtMenuBusca.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.txtMenuBusca.Name = "txtMenuBusca"
        Me.txtMenuBusca.Size = New System.Drawing.Size(150, 42)
        Me.txtMenuBusca.ToolTipText = "Filtra Listado"
        '
        'btnMnuAltas
        '
        Me.btnMnuAltas.ForeColor = System.Drawing.Color.Red
        Me.btnMnuAltas.Image = CType(resources.GetObject("btnMnuAltas.Image"), System.Drawing.Image)
        Me.btnMnuAltas.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuAltas.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuAltas.Name = "btnMnuAltas"
        Me.btnMnuAltas.Size = New System.Drawing.Size(46, 39)
        Me.btnMnuAltas.Text = "&ALTAS"
        Me.btnMnuAltas.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnMnuAltas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuModificar
        '
        Me.btnMnuModificar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuModificar.Image = CType(resources.GetObject("btnMnuModificar.Image"), System.Drawing.Image)
        Me.btnMnuModificar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuModificar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuModificar.Name = "btnMnuModificar"
        Me.btnMnuModificar.Size = New System.Drawing.Size(74, 39)
        Me.btnMnuModificar.Text = "&MODIFICAR"
        Me.btnMnuModificar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnMnuModificar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuEliminar
        '
        Me.btnMnuEliminar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuEliminar.Image = CType(resources.GetObject("btnMnuEliminar.Image"), System.Drawing.Image)
        Me.btnMnuEliminar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuEliminar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuEliminar.Name = "btnMnuEliminar"
        Me.btnMnuEliminar.Size = New System.Drawing.Size(64, 39)
        Me.btnMnuEliminar.Text = "&ELIMINAR"
        Me.btnMnuEliminar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuReporte
        '
        Me.btnMnuReporte.ForeColor = System.Drawing.Color.Red
        Me.btnMnuReporte.Image = CType(resources.GetObject("btnMnuReporte.Image"), System.Drawing.Image)
        Me.btnMnuReporte.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuReporte.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuReporte.Name = "btnMnuReporte"
        Me.btnMnuReporte.Size = New System.Drawing.Size(60, 39)
        Me.btnMnuReporte.Text = "&REPORTE"
        Me.btnMnuReporte.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuOk
        '
        Me.btnMnuOk.ForeColor = System.Drawing.Color.Red
        Me.btnMnuOk.Image = Global.Fletera.My.Resources.Resources.Save1
        Me.btnMnuOk.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuOk.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuOk.Name = "btnMnuOk"
        Me.btnMnuOk.Size = New System.Drawing.Size(56, 39)
        Me.btnMnuOk.Text = "&GRABAR"
        Me.btnMnuOk.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuCancelar
        '
        Me.btnMnuCancelar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuCancelar.Image = Global.Fletera.My.Resources.Resources.signos_f_059
        Me.btnMnuCancelar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuCancelar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuCancelar.Name = "btnMnuCancelar"
        Me.btnMnuCancelar.Size = New System.Drawing.Size(71, 39)
        Me.btnMnuCancelar.Text = "&CANCELAR"
        Me.btnMnuCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuSalir
        '
        Me.btnMnuSalir.ForeColor = System.Drawing.Color.Red
        Me.btnMnuSalir.Image = Global.Fletera.My.Resources.Resources.Salida2
        Me.btnMnuSalir.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuSalir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuSalir.Name = "btnMnuSalir"
        Me.btnMnuSalir.Size = New System.Drawing.Size(41, 39)
        Me.btnMnuSalir.Text = "&SALIR"
        Me.btnMnuSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'MeStatus1
        '
        Me.MeStatus1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.MeStatus1.Location = New System.Drawing.Point(0, 266)
        Me.MeStatus1.Margin = New System.Windows.Forms.Padding(4)
        Me.MeStatus1.Name = "MeStatus1"
        Me.MeStatus1.Size = New System.Drawing.Size(692, 26)
        Me.MeStatus1.TabIndex = 72
        '
        'GridTodos
        '
        Me.GridTodos.AllowUserToAddRows = False
        Me.GridTodos.AllowUserToDeleteRows = False
        Me.GridTodos.AllowUserToResizeRows = False
        Me.GridTodos.BackgroundColor = System.Drawing.Color.White
        Me.GridTodos.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.GridTodos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.GridTodos.Location = New System.Drawing.Point(3, 45)
        Me.GridTodos.MultiSelect = False
        Me.GridTodos.Name = "GridTodos"
        Me.GridTodos.ReadOnly = True
        Me.GridTodos.RowHeadersVisible = False
        Me.GridTodos.Size = New System.Drawing.Size(162, 222)
        Me.GridTodos.TabIndex = 73
        '
        'frmCatAlmacen
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(692, 292)
        Me.Controls.Add(Me.GridTodos)
        Me.Controls.Add(Me.MeStatus1)
        Me.Controls.Add(Me.ToolStripMenu)
        Me.Controls.Add(Me.GrupoCaptura)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmCatAlmacen"
        Me.Text = "Catal�go de Almacenes"
        Me.GrupoCaptura.ResumeLayout(False)
        Me.GrupoCaptura.PerformLayout()
        Me.ToolStripMenu.ResumeLayout(False)
        Me.ToolStripMenu.PerformLayout()
        CType(Me.GridTodos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region
    Sub New(ByVal vTag As String)
        InitializeComponent()
        _Tag = vTag
    End Sub

    Public Sub New(con As String, v As Boolean)
        Me.con = con
        BD = New CapaDatos.UtilSQL(con, "Juan")
        InitializeComponent()
        Me.v = v
    End Sub
#Region "MenusToolStrip"
    Private Sub LimpiaVariables()
        SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")

    End Sub
    Private Sub LimpiaCampos()
        txtidAlmacen.Text = ""
        txtNomAlmacen.Text = ""
        txtidSucursal.Text = ""
        txtNomSucursal.Text = ""
        txtCIDALMACEN_COM.Text = ""
        txtCNOMBREALMACEN.Text = ""
        LimpiaVariables()
    End Sub

    Private Sub ActivaCampos(ByVal valor As Boolean, ByVal vOpcion As TipoOpcActivaCampos)
        If vOpcion = TipoOpcActivaCampos.tOpcDESHABTODOS Then
            txtidAlmacen.Enabled = False
            txtNomAlmacen.Enabled = False
            txtidSucursal.Enabled = False
            txtCIDALMACEN_COM.Enabled = False
        ElseIf vOpcion = TipoOpcActivaCampos.tOpcINICIALIZA Then
            txtidAlmacen.Enabled = Not valor
            txtNomAlmacen.Enabled = valor
            txtidSucursal.Enabled = valor
            txtCIDALMACEN_COM.Enabled = valor
        End If
    End Sub


    Private Sub ActivaBotones(ByVal valor As Boolean, Optional ByVal vOpcion As TipoOpcActivaBoton = TipoOpcActivaBoton.tOpcInicializa)
        If vOpcion = TipoOpcActivaBoton.tOpcConsulta Then
            btnMnuOk.Enabled = False
            btnMnuCancelar.Enabled = valor
            btnMnuSalir.Enabled = Not valor

            btnMnuAltas.Enabled = Not valor
            btnMnuModificar.Enabled = Not valor
            btnMnuEliminar.Enabled = Not valor
            btnMnuReporte.Enabled = Not valor
        ElseIf vOpcion = TipoOpcActivaBoton.tOpcInicializa Then
            btnMnuOk.Enabled = valor
            btnMnuCancelar.Enabled = valor
            btnMnuSalir.Enabled = Not valor

            btnMnuAltas.Enabled = Not valor
            btnMnuModificar.Enabled = Not valor
            btnMnuEliminar.Enabled = Not valor
            btnMnuReporte.Enabled = Not valor

            txtMenuBusca.Enabled = Not valor
            GridTodos.Enabled = Not valor
        ElseIf vOpcion = TipoOpcActivaBoton.tOpcInsertar Then
            btnMnuOk.Enabled = valor
            btnMnuCancelar.Enabled = valor
            btnMnuSalir.Enabled = Not valor

            btnMnuAltas.Enabled = Not valor
            btnMnuModificar.Enabled = Not valor
            btnMnuEliminar.Enabled = Not valor
            btnMnuReporte.Enabled = Not valor
        End If
    End Sub

    Private Sub btnMnuAltas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuAltas.Click
        'ALTAS
        '15/FEB/2016
        'If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Alta) = False Then
        '    MessageBox.Show("No tiene los permisos Necesarios para poder dar de Alta en el Catalogo de Almacen!!!",
        '        "Acceso Denegado a Alta de Almacen", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    OpcionForma = TipoOpcionForma.tOpcConsultar
        '    Exit Sub
        'End If
        OpcionForma = TipoOpcionForma.tOpcInsertar
        LimpiaCampos()
        ActivaCampos(True, TipoOpcActivaCampos.tOpcINICIALIZA)
        ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)

        If bEsIdentidad Then
            txtidAlmacen.Enabled = False
            txtNomAlmacen.Focus()
        Else
            txtidAlmacen.Focus()
        End If
        Status("Ingrese nuevo Almacen para guardar", Me)
    End Sub
    Private Sub btnMnuModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuModificar.Click
        'MODIFICAR
        'If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Modificar) = False Then
        '    MessageBox.Show("No tiene los permisos Necesarios para poder Modificar la Informacion del Almacen!!!",
        '           "Acceso Denegado a Modificacion de la Informacion del Almacen", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    OpcionForma = TipoOpcionForma.tOpcConsultar
        '    Exit Sub
        'End If
        If Trim(txtidAlmacen.Text) = "" Then
            txtidAlmacen.Text = DespliegaGrid(TablaBd, Inicio.CONSTR, CampoLLave)
            CargaForm(txtidAlmacen.Text, UCase(TablaBd))
        End If

        OpcionForma = TipoOpcionForma.tOpcModificar
        ActivaCampos(True, TipoOpcActivaCampos.tOpcINICIALIZA)
        ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)

        btnMnuEliminar.Enabled = True
        If bEsIdentidad Then
            txtidAlmacen.Enabled = False
            txtNomAlmacen.Focus()
        Else
            txtidAlmacen.Focus()
        End If

        Status("Ingrese nuevo Almacen para guardar", Me)
    End Sub
    Private Sub btnMnuEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuEliminar.Click
        'ELIMINAR
        'If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Baja) = False Then
        '    MessageBox.Show("No tiene los permisos Necesarios para poder Eliminar el Almacen!!!",
        '        "Acceso Denegado a Eliminacion de Almacen", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    OpcionForma = TipoOpcionForma.tOpcConsultar
        '    Exit Sub
        'End If
        If txtidAlmacen.Text = Almacen.idAlmacen Then
            With Almacen
                If MessageBox.Show("Esta completamente seguro que desea Eliminar el Almacen: " &
                                   .NomAlmacen & " Con Id: " & .idAlmacen & " ????", "Confirma que desea eliminar el Almacen: " & .NomAlmacen & "???",
                                 MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then Exit Sub
                Try
                    .Eliminar() 'Elimina el municipio
                    MessageBox.Show("Almacen: " & .NomAlmacen & " Eliminada con exito!!!",
                                    "Almacen Eliminado con exito", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Status("Almacen: " & .NomAlmacen & " Eliminado con exito!!!", Me)
                    ActualizaGrid(txtMenuBusca.Text)
                    txtidAlmacen.Text = ""
                    OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
                Catch ex As Exception
                    Status("Error al tratar de eliminar el Almacen: " & ex.Message, Me, Err:=True)
                    MessageBox.Show("Error al Tratar de eliminar el Almacen: " & .NomAlmacen & vbNewLine &
                                    "Error. " & ex.Message, "Error: " & Err.Number, MessageBoxButtons.OK, MessageBoxIcon.Error)

                End Try
            End With
        End If
    End Sub
    Private Sub btnMnuReporte_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuReporte.Click
        'REPORTE
        'If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Reporte) = False Then
        '    MessageBox.Show("No tiene los permisos Necesarios para poder realizar Reporte de Almacen!!!",
        '        "Acceso Denegado a Reporte de Almacen", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    OpcionForma = TipoOpcionForma.tOpcConsultar
        '    Exit Sub
        'End If
        ToolStripMenu.Enabled = False
        Status("Abriendo Reporte Espere. . . ", Me)
        Me.Cursor = Cursors.WaitCursor
        DespliegaReporte(TablaBd, CONSTR)
        Status("Finalizo el reporte. . . ", Me)
        Me.Cursor = Cursors.Default
        ToolStripMenu.Enabled = True
    End Sub

#End Region
    Private Sub ActualizaGrid(ByVal vNomAlmacen As String)
        If vNomAlmacen = "" Then
            GridTodos.DataSource = BD.ExecuteReturn("SELECT idAlmacen, NomAlmacen FROM " & TablaBd, TablaBd)
            Status(GridTodos.RowCount & " Registros", Me)
        Else
            GridTodos.DataSource = BD.ExecuteReturn("SELECT idAlmacen, NomAlmacen FROM " & TablaBd &
                                                " WHERE NomAlmacen Like '%" & vNomAlmacen & "%'")
            Status(GridTodos.RowCount & " Registros obtenidos con el filtro %" & vNomAlmacen & "%", Me)
        End If
    End Sub

    Private Sub OpcionMnuTools(ByVal vOpcion As TipoOpcionForma)
        If vOpcion <> TipoOpcionForma.tOpcImprimir And vOpcion <> TipoOpcionForma.tOpcEliminar Then
            OpcionForma = vOpcion
        End If
        txtMenuBusca.Enabled = True
        GridTodos.Enabled = True
        If vOpcion = TipoOpcionForma.tOpcConsultar Then
            'CONSULTA
            For Each ct As Control In GrupoCaptura.Controls
                If Not ct Is cmdBuscaClave Then
                    ct.Enabled = False
                Else
                    ct.Enabled = True
                End If
            Next
            txtMenuBusca.Enabled = True
            btnMnuAltas.Enabled = True
            btnMnuModificar.Enabled = True
            btnMnuEliminar.Enabled = False
            btnMnuReporte.Enabled = True
            btnMnuOk.Enabled = False
            btnMnuCancelar.Enabled = False

            btnMnuSalir.Enabled = True


        End If
    End Sub


    Private Sub txtMenuBusca_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtMenuBusca.KeyPress
        If Asc(e.KeyChar) = 39 Then e.Handled = True : Exit Sub 'Caracter ' Evita errores
        If Asc(e.KeyChar) = 13 Then
            GridTodos.Focus()
            e.Handled = True
        End If
    End Sub

    'Private Sub txtMenuBusca_KeyUp(sender As Object, e As KeyEventArgs) Handles txtMenuBusca.KeyUp
    '    If txtMenuBusca.Text.Trim = "" Then Exit Sub
    '    If e.KeyCode = Keys.Down Or e.KeyCode = Keys.Up Then
    '        GridTodos.Focus()
    '        e.Handled = True
    '        Exit Sub
    '    End If
    '    If e.KeyCode = Keys.Left Or e.KeyCode = Keys.Right Then Exit Sub
    '    ActualizaGrid(txtMenuBusca.Text)
    'End Sub



    Private Sub frmCatFamilias_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Inicio.CONSTR = "" Then
            Inicio.CONSTR = con
        End If
        OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
        ActualizaGrid("")
        GridTodos.Columns(1).Width = GridTodos.Width - 20
        GridTodos.Columns(0).Visible = False
        txtMenuBusca.Focus()
    End Sub
    Private Sub GridTodos_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles GridTodos.CellEnter
        CargaForm(GridTodos.Rows(GridTodos.CurrentRow.Index).Cells(0).Value, UCase(TablaBd))
    End Sub
    Private Sub GridTodos_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles GridTodos.KeyPress
        If Asc(e.KeyChar) = 8 Then
            txtMenuBusca.Focus()
            e.Handled = True
        End If
    End Sub
    Private Sub CargaForm(ByVal idClave As Integer, ByVal NomTabla As String)
        Try
            Select Case NomTabla
                Case UCase(TablaBd)
                    Almacen = New AlmacenClass(idClave)
                    With Almacen
                        txtidAlmacen.Text = .idAlmacen
                        If .Existe Then
                            txtNomAlmacen.Text = .NomAlmacen
                            txtidSucursal.Text = .idSucursal
                            txtNomSucursal.Text = .NomSucursal
                            txtCIDALMACEN_COM.Text = .CIDALMACEN_COM
                            If OpcionForma = TipoOpcionForma.tOpcInsertar Then
                                Status("El Almacen Con idSucursal: " & .idAlmacen &
                                       " Ya existe!!!, Favor de modificarlo para poder dar de alta a otro Almacen", Me, Err:=True)
                                MessageBox.Show("El Almacen con id: " & .idAlmacen &
                                                " YA EXISTE!!!, Favor de Cambiarlo y volver a intentarlo",
                                                "El Almacen con id: " & .idAlmacen & " YA EXISTE!!!", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            End If
                        Else 'No existe 
                            If OpcionForma = TipoOpcionForma.tOpcInsertar Then
                                'For Each Ct As Control In GrupoCaptura.Controls
                                '    If Not Ct Is txtidAlmacen And Not Ct Is cmdBuscaClave Then
                                '        Ct.Enabled = True
                                '    Else
                                '        Ct.Enabled = False
                                '    End If
                                '    If TypeOf (Ct) Is TextBox Then
                                '        If Not Ct Is txtidAlmacen Then
                                '            TryCast(Ct, TextBox).Text = ""
                                '        End If
                                '    End If
                                'Next
                                ActivaCampos(True, TipoOpcActivaCampos.tOpcINICIALIZA)
                                ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)
                                btnMnuOk.Enabled = True
                                txtNomAlmacen.Focus()
                                Status("Listo para Guardar el nuevo Almacen con id: " & .idSucursal, Me)
                            End If
                        End If
                    End With
                Case UCase(TablaBd2)
                    'aqui()
                    Sucursal = New SucursalClass(idClave)
                    With Sucursal
                        txtidSucursal.Text = .idSucursal
                        If .Existe Then
                            txtNomSucursal.Text = .NomSucursal
                        Else
                            MsgBox("La Sucursal con id: " & txtidSucursal.Text & " No Existe")
                        End If
                    End With

            End Select



        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try

    End Sub
    'Private Function Validar() As Boolean
    '    Validar = True
    '    If Not Inicio.ValidandoCampo(Trim(txtidAlmacen.Text), TipoDato.TdNumerico) Then
    '        If Not bEsIdentidad Then
    '            MsgBox(MENSAJE_CLAVE, vbInformation, "Aviso" & Me.Text)
    '            If txtidAlmacen.Enabled Then
    '                txtidAlmacen.Focus()
    '                txtidAlmacen.SelectAll()
    '            End If
    '            Validar = False
    '        End If
    '    ElseIf Not Inicio.ValidandoCampo(Trim(txtNomAlmacen.Text), TipoDato.TdCadena) Then
    '        MsgBox("El Nombre del Almacen No Puede Ser Vacio", vbInformation, "Aviso" & Me.Text)
    '        If txtNomAlmacen.Enabled Then
    '            txtNomAlmacen.Focus()
    '            txtNomAlmacen.SelectAll()
    '        End If
    '        Validar = False
    '    ElseIf Not Inicio.ValidandoCampo(Trim(txtidSucursal.Text), TipoDato.TdNumerico) Then
    '        MsgBox("La Sucursal del Almacen no Puede ser Vacio", vbInformation, "Aviso" & Me.Text)
    '        If txtidSucursal.Enabled Then
    '            txtidSucursal.Focus()
    '            txtidSucursal.SelectAll()
    '        End If
    '        Validar = False
    '    End If
    'End Function

    'Private Sub txtidAlmacen_EnabledChanged(sender As Object, e As EventArgs) Handles txtidAlmacen.EnabledChanged
    '    cmdBuscaClave.Enabled = txtidAlmacen.Enabled
    'End Sub
    'CONTROLES
    Private Sub txtidAlmacen_KeyDown(sender As Object, e As KeyEventArgs) Handles txtidAlmacen.KeyDown
        If e.KeyCode = Keys.F3 Then
            cmdBuscaClave_Click(sender, e)
        End If

    End Sub
    Private Sub txtidAlmacen_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtidAlmacen.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If txtidAlmacen.Text.Trim <> "" Then
                CargaForm(txtidAlmacen.Text, UCase(TablaBd))
            End If
            e.Handled = True
        End If
    End Sub
    Private Sub txtNomAlmacen_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtNomAlmacen.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            'btnMnuOk_Click(sender, e)
            txtidSucursal.Focus()
            txtidSucursal.SelectAll()
        End If
    End Sub

    Private Sub cmdBuscaClave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBuscaClave.Click
        Dim idEst As String = DespliegaGrid(UCase(TablaBd), Inicio.CONSTR, CampoLLave)
        If idEst <> "" Then CargaForm(idEst, UCase(TablaBd))
    End Sub

    Private Sub txtNomAlmacen_MouseDown(sender As Object, e As MouseEventArgs) Handles txtNomAlmacen.MouseDown
        txtNomAlmacen.Focus()
        txtNomAlmacen.SelectAll()
    End Sub

    Private Sub txtidSucursal_EnabledChanged(sender As Object, e As EventArgs) Handles txtidSucursal.EnabledChanged
        cmdBuscaClaveSuc.Enabled = txtidSucursal.Enabled
    End Sub

    Private Sub txtidSucursal_KeyDown(sender As Object, e As KeyEventArgs) Handles txtidSucursal.KeyDown
        If e.KeyCode = Keys.F3 Then
            cmdBuscaClaveDiv_Click(sender, e)
        End If
    End Sub

    Private Sub txtidSucursal_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtidSucursal.KeyPress
        '    btnMnuOk_Click(sender, e)
        '
        'If Asc(e.KeyChar) = 13 Then
        '    If txtidSucursal.Text.Trim <> "" Then
        '        CargaForm(txtidSucursal.Text, TablaBd2)
        '        txtNomSucursal.Focus()
        '        txtNomSucursal.SelectAll()
        '    End If
        '    e.Handled = True
        'End If
        If txtidSucursal.Text.Length = 0 Then
            txtidSucursal.Focus()
        Else
            If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
                Salida = True
                CargaForm(txtidSucursal.Text, UCase(TablaBd2))
                Salida = False
                txtCIDALMACEN_COM.Focus()
                txtCIDALMACEN_COM.SelectAll()
            End If
        End If
    End Sub

    Private Sub txtidSucursal_Leave(sender As Object, e As EventArgs) Handles txtidSucursal.Leave
        If Salida Then Exit Sub
        If txtidSucursal.Text.Length = 0 Then
            txtidSucursal.Focus()
            txtidSucursal.SelectAll()
        Else
            If txtidSucursal.Enabled Then
                CargaForm(txtidSucursal.Text, UCase(TablaBd2))
                txtCIDALMACEN_COM.Focus()
                txtCIDALMACEN_COM.SelectAll()
            End If
        End If
    End Sub

    Private Sub txtidSucursal_MouseDown(sender As Object, e As MouseEventArgs) Handles txtidSucursal.MouseDown
        txtidSucursal.Focus()
        txtidSucursal.SelectAll()
    End Sub

    Private Sub cmdBuscaClaveDiv_Click(sender As Object, e As EventArgs) Handles cmdBuscaClaveSuc.Click
        txtidSucursal.Text = DespliegaGrid(UCase(TablaBd2), Inicio.CONSTR, CampoLLave)
        txtidSucursal_Leave(sender, e)
    End Sub

    Private Sub cmdCancelar_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        With TryCast(sender, Button)
            If .Enabled Then
                Me.CancelButton = sender
            End If
        End With
    End Sub
    Private Sub cmdTerminar_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        With TryCast(sender, Button)
            If .Enabled Then
                Me.CancelButton = sender
            End If
        End With
    End Sub

    Private Sub btnMnuOk_Click(sender As Object, e As EventArgs) Handles btnMnuOk.Click
        Dim CadCam As String = ""
        Try
            Salida = True
            If Not Validar() Then Exit Sub
            If GridTodos.RowCount = 0 Then
                Almacen = New AlmacenClass(0)
            End If
            With Almacen
                If OpcionForma = TipoOpcionForma.tOpcModificar Then

                    CadCam = .GetCambios(txtNomAlmacen.Text, txtidSucursal.Text, txtCIDALMACEN_COM.Text)
                    If CadCam <> "" Then
                        'If MessageBox.Show("Esta completamente Seguro que Desea guardar los Cambios Realizados al Estado Con id: " & .IDMARCA & vbNewLine &
                        '                CadCam, "Confirma que desea guardar los Cambios Realizados???", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then Exit Sub
                        Status("Guardando Modificaciones del Almacen Con id: " & .idSucursal, Me, 1, 2)
                        .Guardar(txtNomAlmacen.Text, txtidSucursal.Text, bEsIdentidad, txtCIDALMACEN_COM.Text)
                        Status("Almacen con Id: " & .idSucursal & " Modificado Con Exito!!!", Me, 2, 2)
                        MessageBox.Show("Almacen con Id: " & .idSucursal & " Modificada con exito!!!",
                                        "Accion terminada con exito!!!", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
                        ActualizaGrid(txtMenuBusca.Text)
                    Else
                        Status("No se detectaron cambios para realizar!!!", Me, Err:=True)
                    End If
                ElseIf OpcionForma = TipoOpcionForma.tOpcInsertar Then
                    .Existe = False
                    .NomSucursal = txtNomAlmacen.Text
                    If Not bEsIdentidad Then
                        .idSucursal = txtidAlmacen.Text
                    End If
                    Status("Guardando Nuevo Almacen: " & txtNomAlmacen.Text & ". . .", Me, 1, 2)
                    .Guardar(txtNomAlmacen.Text, txtidSucursal.Text, bEsIdentidad, txtCIDALMACEN_COM.Text)
                    Status("Almacen: " & txtNomAlmacen.Text & " Guardado con Exito!!!", Me, 2, 2)
                    MessageBox.Show("Nuevo Almacen: " & txtNomAlmacen.Text & " Guardado con exito!!!",
                                    "Accion terminada con exito!!!", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
                    ActualizaGrid(txtMenuBusca.Text)

                End If
            End With
            Salida = False
        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try
    End Sub


    Private Sub btnMnuCancelar_Click(sender As Object, e As EventArgs) Handles btnMnuCancelar.Click
        If txtidAlmacen.Text <> "" Then
            CargaForm(txtidAlmacen.Text, UCase(TablaBd))
        End If
        OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
    End Sub

    Private Sub btnMnuSalir_Click(sender As Object, e As EventArgs) Handles btnMnuSalir.Click
        Me.Close()
    End Sub

    Private Sub txtMenuBusca_TextChanged(sender As Object, e As EventArgs) Handles txtMenuBusca.TextChanged
        ActualizaGrid(txtMenuBusca.Text)
    End Sub

    Private Function Validar() As Boolean
        Validar = True
        If Not Inicio.ValidandoCampo(Trim(txtidAlmacen.Text), TipoDato.TdNumerico) Then
            If Not bEsIdentidad Then
                MsgBox(MENSAJE_CLAVE, vbInformation, "Aviso" & Me.Text)
                If txtidAlmacen.Enabled Then
                    txtidAlmacen.Focus()
                    txtidAlmacen.SelectAll()
                End If
                Validar = False
            End If
        ElseIf Not Inicio.ValidandoCampo(Trim(txtNomAlmacen.Text), TipoDato.TdCadena) Then
            MsgBox("El Nombre del Almacen No Puede Ser Vacio", vbInformation, "Aviso" & Me.Text)
            If txtNomAlmacen.Enabled Then
                txtNomAlmacen.Focus()
                txtNomAlmacen.SelectAll()
            End If
            Validar = False
        ElseIf Not Inicio.ValidandoCampo(Trim(txtidSucursal.Text), TipoDato.TdNumerico) Then
            MsgBox("La Sucursal del Almacen no Puede ser Vacio", vbInformation, "Aviso" & Me.Text)
            If txtidSucursal.Enabled Then
                txtidSucursal.Focus()
                txtidSucursal.SelectAll()
            End If
            Validar = False
        End If
    End Function

    Private Sub GridTodos_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles GridTodos.CellContentClick

    End Sub


End Class
