'Fecha: 15 / Febrero / 2016
'ROGER GALA PACHECO
'************************************************************************************************************************************
'*  Fecha Modificacion :   15 / Febrero / 2016                                                                                       
'*  
'************************************************************************************************************************************
Public Class frmCatHerramienta
    Inherits System.Windows.Forms.Form
    Dim OpcionForma As TipoOpcionForma
    Dim TablaBd As String = "CatHerramientas"
    Dim CampoLLave As String = "idHerramienta"
    Dim TablaBd2 As String = "CatFamilia"
    Dim TablaBd3 As String = "CatTipoHerramienta"
    Dim TablaBd4 As String = "CatMarcas"
    Public WithEvents MeStatus1 As ControlStatus.MeStatus
    Friend WithEvents GridTodos As System.Windows.Forms.DataGridView
    Private Herramienta As HerramientaClass
    Private Familia As FamiliasClass
    Private TipoHerramienta As TipoHerramClass
    Private Marca As MarcaClass
    Friend WithEvents btnMnuOk As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuCancelar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuSalir As System.Windows.Forms.ToolStripButton
    Private _Tag As String
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cmdBuscaClaveFam As System.Windows.Forms.Button
    Friend WithEvents txtidFamilia As System.Windows.Forms.TextBox
    Friend WithEvents txtNomFamilia As System.Windows.Forms.TextBox
    Dim bEsIdentidad As Boolean = True
    Friend WithEvents txtidTipoHerramienta As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cmdBuscaClaveTPH As System.Windows.Forms.Button
    Friend WithEvents txtNomTipoHerramienta As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtMedida As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtNoPiezas As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtModelo As System.Windows.Forms.TextBox
    Friend WithEvents txtidMarca As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cmdBuscaClaveMar As System.Windows.Forms.Button
    Friend WithEvents txtNombreMarca As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtNomDivision As System.Windows.Forms.TextBox
    Dim Salida As Boolean
    Private con As String
    Private v As Boolean

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents GrupoCaptura As System.Windows.Forms.GroupBox
    Friend WithEvents ToolStripMenu As System.Windows.Forms.ToolStrip
    Friend WithEvents txtMenuBusca As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents btnMnuAltas As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuModificar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuEliminar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuReporte As System.Windows.Forms.ToolStripButton
    Friend WithEvents label1 As System.Windows.Forms.Label
    Friend WithEvents label2 As System.Windows.Forms.Label
    Friend WithEvents cmdBuscaClave As System.Windows.Forms.Button
    Friend WithEvents txtidHerramienta As System.Windows.Forms.TextBox
    Friend WithEvents txtNomHerramienta As System.Windows.Forms.TextBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCatHerramienta))
        Me.txtidHerramienta = New System.Windows.Forms.TextBox()
        Me.label1 = New System.Windows.Forms.Label()
        Me.txtNomHerramienta = New System.Windows.Forms.TextBox()
        Me.label2 = New System.Windows.Forms.Label()
        Me.GrupoCaptura = New System.Windows.Forms.GroupBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtNomDivision = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtMedida = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtNoPiezas = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtModelo = New System.Windows.Forms.TextBox()
        Me.txtidMarca = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.cmdBuscaClaveMar = New System.Windows.Forms.Button()
        Me.txtNombreMarca = New System.Windows.Forms.TextBox()
        Me.txtidTipoHerramienta = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cmdBuscaClaveTPH = New System.Windows.Forms.Button()
        Me.txtNomTipoHerramienta = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cmdBuscaClaveFam = New System.Windows.Forms.Button()
        Me.txtidFamilia = New System.Windows.Forms.TextBox()
        Me.txtNomFamilia = New System.Windows.Forms.TextBox()
        Me.cmdBuscaClave = New System.Windows.Forms.Button()
        Me.ToolStripMenu = New System.Windows.Forms.ToolStrip()
        Me.txtMenuBusca = New System.Windows.Forms.ToolStripTextBox()
        Me.btnMnuAltas = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuModificar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuEliminar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuReporte = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuOk = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuCancelar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuSalir = New System.Windows.Forms.ToolStripButton()
        Me.MeStatus1 = New ControlStatus.MeStatus()
        Me.GridTodos = New System.Windows.Forms.DataGridView()
        Me.GrupoCaptura.SuspendLayout()
        Me.ToolStripMenu.SuspendLayout()
        CType(Me.GridTodos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtidHerramienta
        '
        Me.txtidHerramienta.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtidHerramienta.Location = New System.Drawing.Point(109, 27)
        Me.txtidHerramienta.MaxLength = 3
        Me.txtidHerramienta.Name = "txtidHerramienta"
        Me.txtidHerramienta.Size = New System.Drawing.Size(69, 20)
        Me.txtidHerramienta.TabIndex = 4
        '
        'label1
        '
        Me.label1.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.label1.Location = New System.Drawing.Point(6, 27)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(94, 20)
        Me.label1.TabIndex = 14
        Me.label1.Text = "id Herramienta:"
        '
        'txtNomHerramienta
        '
        Me.txtNomHerramienta.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNomHerramienta.Location = New System.Drawing.Point(109, 59)
        Me.txtNomHerramienta.MaxLength = 50
        Me.txtNomHerramienta.Name = "txtNomHerramienta"
        Me.txtNomHerramienta.Size = New System.Drawing.Size(322, 20)
        Me.txtNomHerramienta.TabIndex = 6
        '
        'label2
        '
        Me.label2.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.label2.Location = New System.Drawing.Point(9, 59)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(91, 20)
        Me.label2.TabIndex = 16
        Me.label2.Text = "Nombre:"
        '
        'GrupoCaptura
        '
        Me.GrupoCaptura.Controls.Add(Me.Label9)
        Me.GrupoCaptura.Controls.Add(Me.txtNomDivision)
        Me.GrupoCaptura.Controls.Add(Me.Label8)
        Me.GrupoCaptura.Controls.Add(Me.txtMedida)
        Me.GrupoCaptura.Controls.Add(Me.Label7)
        Me.GrupoCaptura.Controls.Add(Me.txtNoPiezas)
        Me.GrupoCaptura.Controls.Add(Me.Label6)
        Me.GrupoCaptura.Controls.Add(Me.txtModelo)
        Me.GrupoCaptura.Controls.Add(Me.txtidMarca)
        Me.GrupoCaptura.Controls.Add(Me.Label5)
        Me.GrupoCaptura.Controls.Add(Me.cmdBuscaClaveMar)
        Me.GrupoCaptura.Controls.Add(Me.txtNombreMarca)
        Me.GrupoCaptura.Controls.Add(Me.txtidTipoHerramienta)
        Me.GrupoCaptura.Controls.Add(Me.Label4)
        Me.GrupoCaptura.Controls.Add(Me.cmdBuscaClaveTPH)
        Me.GrupoCaptura.Controls.Add(Me.txtNomTipoHerramienta)
        Me.GrupoCaptura.Controls.Add(Me.Label3)
        Me.GrupoCaptura.Controls.Add(Me.cmdBuscaClaveFam)
        Me.GrupoCaptura.Controls.Add(Me.txtidFamilia)
        Me.GrupoCaptura.Controls.Add(Me.txtNomFamilia)
        Me.GrupoCaptura.Controls.Add(Me.label1)
        Me.GrupoCaptura.Controls.Add(Me.label2)
        Me.GrupoCaptura.Controls.Add(Me.cmdBuscaClave)
        Me.GrupoCaptura.Controls.Add(Me.txtidHerramienta)
        Me.GrupoCaptura.Controls.Add(Me.txtNomHerramienta)
        Me.GrupoCaptura.Location = New System.Drawing.Point(168, 45)
        Me.GrupoCaptura.Name = "GrupoCaptura"
        Me.GrupoCaptura.Size = New System.Drawing.Size(524, 341)
        Me.GrupoCaptura.TabIndex = 3
        Me.GrupoCaptura.TabStop = False
        '
        'Label9
        '
        Me.Label9.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label9.Location = New System.Drawing.Point(6, 130)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(94, 20)
        Me.Label9.TabIndex = 36
        Me.Label9.Text = "Division:"
        '
        'txtNomDivision
        '
        Me.txtNomDivision.Enabled = False
        Me.txtNomDivision.Location = New System.Drawing.Point(109, 127)
        Me.txtNomDivision.Name = "txtNomDivision"
        Me.txtNomDivision.ReadOnly = True
        Me.txtNomDivision.Size = New System.Drawing.Size(275, 20)
        Me.txtNomDivision.TabIndex = 35
        '
        'Label8
        '
        Me.Label8.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label8.Location = New System.Drawing.Point(9, 308)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(91, 20)
        Me.Label8.TabIndex = 34
        Me.Label8.Text = "Medida:"
        '
        'txtMedida
        '
        Me.txtMedida.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMedida.Location = New System.Drawing.Point(109, 308)
        Me.txtMedida.MaxLength = 50
        Me.txtMedida.Name = "txtMedida"
        Me.txtMedida.Size = New System.Drawing.Size(322, 20)
        Me.txtMedida.TabIndex = 18
        '
        'Label7
        '
        Me.Label7.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label7.Location = New System.Drawing.Point(6, 273)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(91, 20)
        Me.Label7.TabIndex = 32
        Me.Label7.Text = "No. Piezas"
        '
        'txtNoPiezas
        '
        Me.txtNoPiezas.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNoPiezas.Location = New System.Drawing.Point(109, 273)
        Me.txtNoPiezas.MaxLength = 50
        Me.txtNoPiezas.Name = "txtNoPiezas"
        Me.txtNoPiezas.Size = New System.Drawing.Size(109, 20)
        Me.txtNoPiezas.TabIndex = 17
        '
        'Label6
        '
        Me.Label6.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label6.Location = New System.Drawing.Point(9, 238)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(91, 20)
        Me.Label6.TabIndex = 30
        Me.Label6.Text = "Modelo:"
        '
        'txtModelo
        '
        Me.txtModelo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtModelo.Location = New System.Drawing.Point(109, 238)
        Me.txtModelo.MaxLength = 50
        Me.txtModelo.Name = "txtModelo"
        Me.txtModelo.Size = New System.Drawing.Size(322, 20)
        Me.txtModelo.TabIndex = 16
        '
        'txtidMarca
        '
        Me.txtidMarca.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtidMarca.Location = New System.Drawing.Point(109, 199)
        Me.txtidMarca.MaxLength = 3
        Me.txtidMarca.Name = "txtidMarca"
        Me.txtidMarca.Size = New System.Drawing.Size(69, 20)
        Me.txtidMarca.TabIndex = 13
        '
        'Label5
        '
        Me.Label5.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label5.Location = New System.Drawing.Point(6, 199)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(114, 20)
        Me.Label5.TabIndex = 28
        Me.Label5.Text = "Marca:"
        '
        'cmdBuscaClaveMar
        '
        Me.cmdBuscaClaveMar.Image = CType(resources.GetObject("cmdBuscaClaveMar.Image"), System.Drawing.Image)
        Me.cmdBuscaClaveMar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdBuscaClaveMar.Location = New System.Drawing.Point(186, 191)
        Me.cmdBuscaClaveMar.Name = "cmdBuscaClaveMar"
        Me.cmdBuscaClaveMar.Size = New System.Drawing.Size(32, 32)
        Me.cmdBuscaClaveMar.TabIndex = 14
        Me.cmdBuscaClaveMar.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'txtNombreMarca
        '
        Me.txtNombreMarca.Enabled = False
        Me.txtNombreMarca.Location = New System.Drawing.Point(224, 203)
        Me.txtNombreMarca.Name = "txtNombreMarca"
        Me.txtNombreMarca.ReadOnly = True
        Me.txtNombreMarca.Size = New System.Drawing.Size(275, 20)
        Me.txtNombreMarca.TabIndex = 15
        '
        'txtidTipoHerramienta
        '
        Me.txtidTipoHerramienta.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtidTipoHerramienta.Location = New System.Drawing.Point(109, 159)
        Me.txtidTipoHerramienta.MaxLength = 3
        Me.txtidTipoHerramienta.Name = "txtidTipoHerramienta"
        Me.txtidTipoHerramienta.Size = New System.Drawing.Size(69, 20)
        Me.txtidTipoHerramienta.TabIndex = 10
        '
        'Label4
        '
        Me.Label4.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label4.Location = New System.Drawing.Point(6, 159)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(114, 20)
        Me.Label4.TabIndex = 24
        Me.Label4.Text = "Tipo Herram.:"
        '
        'cmdBuscaClaveTPH
        '
        Me.cmdBuscaClaveTPH.Image = CType(resources.GetObject("cmdBuscaClaveTPH.Image"), System.Drawing.Image)
        Me.cmdBuscaClaveTPH.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdBuscaClaveTPH.Location = New System.Drawing.Point(186, 151)
        Me.cmdBuscaClaveTPH.Name = "cmdBuscaClaveTPH"
        Me.cmdBuscaClaveTPH.Size = New System.Drawing.Size(32, 32)
        Me.cmdBuscaClaveTPH.TabIndex = 11
        Me.cmdBuscaClaveTPH.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'txtNomTipoHerramienta
        '
        Me.txtNomTipoHerramienta.Enabled = False
        Me.txtNomTipoHerramienta.Location = New System.Drawing.Point(224, 163)
        Me.txtNomTipoHerramienta.Name = "txtNomTipoHerramienta"
        Me.txtNomTipoHerramienta.ReadOnly = True
        Me.txtNomTipoHerramienta.Size = New System.Drawing.Size(275, 20)
        Me.txtNomTipoHerramienta.TabIndex = 12
        '
        'Label3
        '
        Me.Label3.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label3.Location = New System.Drawing.Point(6, 97)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(94, 20)
        Me.Label3.TabIndex = 20
        Me.Label3.Text = "Familia:"
        '
        'cmdBuscaClaveFam
        '
        Me.cmdBuscaClaveFam.Image = CType(resources.GetObject("cmdBuscaClaveFam.Image"), System.Drawing.Image)
        Me.cmdBuscaClaveFam.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdBuscaClaveFam.Location = New System.Drawing.Point(186, 89)
        Me.cmdBuscaClaveFam.Name = "cmdBuscaClaveFam"
        Me.cmdBuscaClaveFam.Size = New System.Drawing.Size(32, 32)
        Me.cmdBuscaClaveFam.TabIndex = 8
        Me.cmdBuscaClaveFam.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'txtidFamilia
        '
        Me.txtidFamilia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtidFamilia.Location = New System.Drawing.Point(109, 97)
        Me.txtidFamilia.MaxLength = 3
        Me.txtidFamilia.Name = "txtidFamilia"
        Me.txtidFamilia.Size = New System.Drawing.Size(69, 20)
        Me.txtidFamilia.TabIndex = 7
        '
        'txtNomFamilia
        '
        Me.txtNomFamilia.Enabled = False
        Me.txtNomFamilia.Location = New System.Drawing.Point(224, 101)
        Me.txtNomFamilia.Name = "txtNomFamilia"
        Me.txtNomFamilia.ReadOnly = True
        Me.txtNomFamilia.Size = New System.Drawing.Size(275, 20)
        Me.txtNomFamilia.TabIndex = 9
        '
        'cmdBuscaClave
        '
        Me.cmdBuscaClave.Image = CType(resources.GetObject("cmdBuscaClave.Image"), System.Drawing.Image)
        Me.cmdBuscaClave.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdBuscaClave.Location = New System.Drawing.Point(186, 19)
        Me.cmdBuscaClave.Name = "cmdBuscaClave"
        Me.cmdBuscaClave.Size = New System.Drawing.Size(32, 32)
        Me.cmdBuscaClave.TabIndex = 5
        Me.cmdBuscaClave.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'ToolStripMenu
        '
        Me.ToolStripMenu.AutoSize = False
        Me.ToolStripMenu.BackColor = System.Drawing.Color.Silver
        Me.ToolStripMenu.Dock = System.Windows.Forms.DockStyle.None
        Me.ToolStripMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.txtMenuBusca, Me.btnMnuAltas, Me.btnMnuModificar, Me.btnMnuEliminar, Me.btnMnuReporte, Me.btnMnuOk, Me.btnMnuCancelar, Me.btnMnuSalir})
        Me.ToolStripMenu.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow
        Me.ToolStripMenu.Location = New System.Drawing.Point(3, 0)
        Me.ToolStripMenu.Name = "ToolStripMenu"
        Me.ToolStripMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.ToolStripMenu.Size = New System.Drawing.Size(730, 42)
        Me.ToolStripMenu.TabIndex = 1
        Me.ToolStripMenu.Text = "ToolStrip1"
        '
        'txtMenuBusca
        '
        Me.txtMenuBusca.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.txtMenuBusca.Name = "txtMenuBusca"
        Me.txtMenuBusca.Size = New System.Drawing.Size(150, 42)
        Me.txtMenuBusca.ToolTipText = "Filtra Listado"
        '
        'btnMnuAltas
        '
        Me.btnMnuAltas.ForeColor = System.Drawing.Color.Red
        Me.btnMnuAltas.Image = CType(resources.GetObject("btnMnuAltas.Image"), System.Drawing.Image)
        Me.btnMnuAltas.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuAltas.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuAltas.Name = "btnMnuAltas"
        Me.btnMnuAltas.Size = New System.Drawing.Size(46, 39)
        Me.btnMnuAltas.Text = "&ALTAS"
        Me.btnMnuAltas.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnMnuAltas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuModificar
        '
        Me.btnMnuModificar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuModificar.Image = CType(resources.GetObject("btnMnuModificar.Image"), System.Drawing.Image)
        Me.btnMnuModificar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuModificar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuModificar.Name = "btnMnuModificar"
        Me.btnMnuModificar.Size = New System.Drawing.Size(74, 39)
        Me.btnMnuModificar.Text = "&MODIFICAR"
        Me.btnMnuModificar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnMnuModificar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuEliminar
        '
        Me.btnMnuEliminar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuEliminar.Image = CType(resources.GetObject("btnMnuEliminar.Image"), System.Drawing.Image)
        Me.btnMnuEliminar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuEliminar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuEliminar.Name = "btnMnuEliminar"
        Me.btnMnuEliminar.Size = New System.Drawing.Size(64, 39)
        Me.btnMnuEliminar.Text = "&ELIMINAR"
        Me.btnMnuEliminar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuReporte
        '
        Me.btnMnuReporte.ForeColor = System.Drawing.Color.Red
        Me.btnMnuReporte.Image = CType(resources.GetObject("btnMnuReporte.Image"), System.Drawing.Image)
        Me.btnMnuReporte.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuReporte.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuReporte.Name = "btnMnuReporte"
        Me.btnMnuReporte.Size = New System.Drawing.Size(60, 39)
        Me.btnMnuReporte.Text = "&REPORTE"
        Me.btnMnuReporte.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuOk
        '
        Me.btnMnuOk.ForeColor = System.Drawing.Color.Red
        Me.btnMnuOk.Image = Global.Fletera.My.Resources.Resources.Save1
        Me.btnMnuOk.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuOk.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuOk.Name = "btnMnuOk"
        Me.btnMnuOk.Size = New System.Drawing.Size(56, 39)
        Me.btnMnuOk.Text = "&GRABAR"
        Me.btnMnuOk.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuCancelar
        '
        Me.btnMnuCancelar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuCancelar.Image = Global.Fletera.My.Resources.Resources.signos_f_059
        Me.btnMnuCancelar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuCancelar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuCancelar.Name = "btnMnuCancelar"
        Me.btnMnuCancelar.Size = New System.Drawing.Size(71, 39)
        Me.btnMnuCancelar.Text = "&CANCELAR"
        Me.btnMnuCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuSalir
        '
        Me.btnMnuSalir.ForeColor = System.Drawing.Color.Red
        Me.btnMnuSalir.Image = Global.Fletera.My.Resources.Resources.Salida2
        Me.btnMnuSalir.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuSalir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuSalir.Name = "btnMnuSalir"
        Me.btnMnuSalir.Size = New System.Drawing.Size(41, 39)
        Me.btnMnuSalir.Text = "&SALIR"
        Me.btnMnuSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'MeStatus1
        '
        Me.MeStatus1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.MeStatus1.Location = New System.Drawing.Point(0, 388)
        Me.MeStatus1.Margin = New System.Windows.Forms.Padding(4)
        Me.MeStatus1.Name = "MeStatus1"
        Me.MeStatus1.Size = New System.Drawing.Size(692, 26)
        Me.MeStatus1.TabIndex = 72
        '
        'GridTodos
        '
        Me.GridTodos.AllowUserToAddRows = False
        Me.GridTodos.AllowUserToDeleteRows = False
        Me.GridTodos.AllowUserToResizeRows = False
        Me.GridTodos.BackgroundColor = System.Drawing.Color.White
        Me.GridTodos.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.GridTodos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.GridTodos.Location = New System.Drawing.Point(3, 45)
        Me.GridTodos.MultiSelect = False
        Me.GridTodos.Name = "GridTodos"
        Me.GridTodos.ReadOnly = True
        Me.GridTodos.RowHeadersVisible = False
        Me.GridTodos.Size = New System.Drawing.Size(162, 341)
        Me.GridTodos.TabIndex = 73
        '
        'frmCatHerramienta
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(692, 414)
        Me.Controls.Add(Me.GridTodos)
        Me.Controls.Add(Me.MeStatus1)
        Me.Controls.Add(Me.ToolStripMenu)
        Me.Controls.Add(Me.GrupoCaptura)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmCatHerramienta"
        Me.Text = "Catal�go de Almacenes"
        Me.GrupoCaptura.ResumeLayout(False)
        Me.GrupoCaptura.PerformLayout()
        Me.ToolStripMenu.ResumeLayout(False)
        Me.ToolStripMenu.PerformLayout()
        CType(Me.GridTodos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region
    Sub New(ByVal vTag As String)
        InitializeComponent()
        _Tag = vTag
    End Sub

    Public Sub New(con As String, v As Boolean)
        Me.con = con
        BD = New CapaDatos.UtilSQL(con, "Juan")
        InitializeComponent()
        Me.v = v
    End Sub


#Region "MenusToolStrip"
    Private Sub LimpiaVariables()
        SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")

    End Sub
    Private Sub LimpiaCampos()
        txtidHerramienta.Text = ""
        txtNomHerramienta.Text = ""
        txtidFamilia.Text = ""
        txtidTipoHerramienta.Text = ""
        txtidMarca.Text = ""
        txtModelo.Text = ""
        txtNoPiezas.Text = ""
        txtMedida.Text = ""

        txtNomFamilia.Text = ""
        txtNomTipoHerramienta.Text = ""
        txtNomDivision.Text = ""
        txtNombreMarca.Text = ""

        LimpiaVariables()
    End Sub

    Private Sub ActivaCampos(ByVal valor As Boolean, ByVal vOpcion As TipoOpcActivaCampos)
        If vOpcion = TipoOpcActivaCampos.tOpcDESHABTODOS Then
            txtidHerramienta.Enabled = False
            txtNomHerramienta.Enabled = False
            txtidFamilia.Enabled = False
            txtidTipoHerramienta.Enabled = False
            txtidMarca.Enabled = False
            txtModelo.Enabled = False
            txtNoPiezas.Enabled = False
            txtMedida.Enabled = False
        ElseIf vOpcion = TipoOpcActivaCampos.tOpcINICIALIZA Then
            txtidHerramienta.Enabled = Not valor
            txtNomHerramienta.Enabled = valor
            txtidFamilia.Enabled = valor
            txtidTipoHerramienta.Enabled = valor
            txtidMarca.Enabled = valor
            txtModelo.Enabled = valor
            txtNoPiezas.Enabled = valor
            txtMedida.Enabled = valor
        End If
    End Sub

    Private Sub ActivaBotones(ByVal valor As Boolean, Optional ByVal vOpcion As TipoOpcActivaBoton = TipoOpcActivaBoton.tOpcInicializa)
        If vOpcion = TipoOpcActivaBoton.tOpcConsulta Then
            btnMnuOk.Enabled = False
            btnMnuCancelar.Enabled = valor
            btnMnuSalir.Enabled = Not valor

            btnMnuAltas.Enabled = Not valor
            btnMnuModificar.Enabled = Not valor
            btnMnuEliminar.Enabled = Not valor
            btnMnuReporte.Enabled = Not valor
        ElseIf vOpcion = TipoOpcActivaBoton.tOpcInicializa Then
            btnMnuOk.Enabled = valor
            btnMnuCancelar.Enabled = valor
            btnMnuSalir.Enabled = Not valor

            btnMnuAltas.Enabled = Not valor
            btnMnuModificar.Enabled = Not valor
            btnMnuEliminar.Enabled = Not valor
            btnMnuReporte.Enabled = Not valor

            txtMenuBusca.Enabled = Not valor
            GridTodos.Enabled = Not valor
        ElseIf vOpcion = TipoOpcActivaBoton.tOpcInsertar Then
            btnMnuOk.Enabled = valor
            btnMnuCancelar.Enabled = valor
            btnMnuSalir.Enabled = Not valor

            btnMnuAltas.Enabled = Not valor
            btnMnuModificar.Enabled = Not valor
            btnMnuEliminar.Enabled = Not valor
            btnMnuReporte.Enabled = Not valor
        End If
    End Sub

    Private Sub btnMnuAltas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuAltas.Click
        'ALTAS
        '15/FEB/2016
        'If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Alta) = False Then
        '    MessageBox.Show("No tiene los permisos Necesarios para poder dar de Alta en el Catalogo de Herramienta!!!",
        '        "Acceso Denegado a Alta de Herramienta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    OpcionForma = TipoOpcionForma.tOpcConsultar
        '    Exit Sub
        'End If
        OpcionForma = TipoOpcionForma.tOpcInsertar
        LimpiaCampos()
        ActivaCampos(True, TipoOpcActivaCampos.tOpcINICIALIZA)
        ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)

        If bEsIdentidad Then
            txtidHerramienta.Enabled = False
            txtNomHerramienta.Focus()
        Else
            txtidHerramienta.Focus()
        End If
        Status("Ingrese nueva Herramienta para guardar", Me)
    End Sub
    Private Sub btnMnuModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuModificar.Click
        'MODIFICAR
        'If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Modificar) = False Then
        '    MessageBox.Show("No tiene los permisos Necesarios para poder Modificar la Informacion del Herramienta!!!",
        '           "Acceso Denegado a Modificacion de la Informacion del Herramienta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    OpcionForma = TipoOpcionForma.tOpcConsultar
        '    Exit Sub
        'End If
        If Trim(txtidHerramienta.Text) = "" Then
            txtidHerramienta.Text = DespliegaGrid(TablaBd, Inicio.CONSTR, CampoLLave)
            CargaForm(txtidHerramienta.Text, UCase(TablaBd))
        End If

        OpcionForma = TipoOpcionForma.tOpcModificar
        ActivaCampos(True, TipoOpcActivaCampos.tOpcINICIALIZA)
        ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)

        btnMnuEliminar.Enabled = True
        If bEsIdentidad Then
            txtidHerramienta.Enabled = False
            txtNomHerramienta.Focus()
        Else
            txtidHerramienta.Focus()
        End If

        Status("Ingrese Herramienta para guardar", Me)
    End Sub
    Private Sub btnMnuEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuEliminar.Click
        'ELIMINAR
        'If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Baja) = False Then
        '    MessageBox.Show("No tiene los permisos Necesarios para poder Eliminar Herramienta!!!",
        '        "Acceso Denegado a Eliminacion de Herramienta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    OpcionForma = TipoOpcionForma.tOpcConsultar
        '    Exit Sub
        'End If
        If txtidHerramienta.Text = Herramienta.idHerramienta Then
            With Herramienta
                If MessageBox.Show("Esta completamente seguro que desea Eliminar la Herramienta: " &
                                   .NomHerramienta & " Con Id: " & .idHerramienta & " ????", "Confirma que desea eliminar la Herramienta: " & .NomHerramienta & "???",
                                 MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then Exit Sub
                Try
                    .Eliminar() 'Elimina el municipio
                    MessageBox.Show("Herramienta: " & .NomHerramienta & " Eliminada con exito!!!",
                                    "Herramienta Eliminada con exito", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Status("Herramienta: " & .NomHerramienta & " Eliminada con exito!!!", Me)
                    ActualizaGrid(txtMenuBusca.Text)
                    txtidHerramienta.Text = ""
                    OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
                Catch ex As Exception
                    Status("Error al tratar de eliminar la Herramienta: " & ex.Message, Me, Err:=True)
                    MessageBox.Show("Error al Tratar de eliminar la Herramienta: " & .NomHerramienta & vbNewLine &
                                    "Error. " & ex.Message, "Error: " & Err.Number, MessageBoxButtons.OK, MessageBoxIcon.Error)

                End Try
            End With
        End If
    End Sub
    Private Sub btnMnuReporte_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuReporte.Click
        'REPORTE
        'If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Reporte) = False Then
        '    MessageBox.Show("No tiene los permisos Necesarios para poder realizar Reporte de Herramienta!!!",
        '        "Acceso Denegado a Reporte de Herramienta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    OpcionForma = TipoOpcionForma.tOpcConsultar
        '    Exit Sub
        'End If
        ToolStripMenu.Enabled = False
        Status("Abriendo Reporte Espere. . . ", Me)
        Me.Cursor = Cursors.WaitCursor
        DespliegaReporte(TablaBd, CONSTR)
        Status("Finalizo el reporte. . . ", Me)
        Me.Cursor = Cursors.Default
        ToolStripMenu.Enabled = True
    End Sub

#End Region
    Private Sub ActualizaGrid(ByVal vNomHerramienta As String)
        If vNomHerramienta = "" Then
            GridTodos.DataSource = BD.ExecuteReturn("SELECT idHerramienta, NomHerramienta FROM " & TablaBd, TablaBd)
            Status(GridTodos.RowCount & " Registros", Me)
        Else
            GridTodos.DataSource = BD.ExecuteReturn("SELECT idHerramienta, NomHerramienta FROM " & TablaBd &
                                                " WHERE NomHerramienta Like '%" & vNomHerramienta & "%'")
            Status(GridTodos.RowCount & " Registros obtenidos con el filtro %" & vNomHerramienta & "%", Me)
        End If
    End Sub

    Private Sub OpcionMnuTools(ByVal vOpcion As TipoOpcionForma)
        If vOpcion <> TipoOpcionForma.tOpcImprimir And vOpcion <> TipoOpcionForma.tOpcEliminar Then
            OpcionForma = vOpcion
        End If
        txtMenuBusca.Enabled = True
        GridTodos.Enabled = True
        If vOpcion = TipoOpcionForma.tOpcConsultar Then
            'CONSULTA
            For Each ct As Control In GrupoCaptura.Controls
                If Not ct Is cmdBuscaClave Then
                    ct.Enabled = False
                Else
                    ct.Enabled = True
                End If
            Next
            txtMenuBusca.Enabled = True
            btnMnuAltas.Enabled = True
            btnMnuModificar.Enabled = True
            btnMnuEliminar.Enabled = False
            btnMnuReporte.Enabled = True
            btnMnuOk.Enabled = False
            btnMnuCancelar.Enabled = False

            btnMnuSalir.Enabled = True


        End If
    End Sub


    Private Sub txtMenuBusca_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtMenuBusca.KeyPress
        If Asc(e.KeyChar) = 39 Then e.Handled = True : Exit Sub 'Caracter ' Evita errores
        If Asc(e.KeyChar) = 13 Then
            GridTodos.Focus()
            e.Handled = True
        End If
    End Sub

    'Private Sub txtMenuBusca_KeyUp(sender As Object, e As KeyEventArgs) Handles txtMenuBusca.KeyUp
    '    If txtMenuBusca.Text.Trim = "" Then Exit Sub
    '    If e.KeyCode = Keys.Down Or e.KeyCode = Keys.Up Then
    '        GridTodos.Focus()
    '        e.Handled = True
    '        Exit Sub
    '    End If
    '    If e.KeyCode = Keys.Left Or e.KeyCode = Keys.Right Then Exit Sub
    '    ActualizaGrid(txtMenuBusca.Text)
    'End Sub



    Private Sub frmCatFamilias_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Inicio.CONSTR = "" Then
            Inicio.CONSTR = con
        End If

        OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
        ActualizaGrid("")
        GridTodos.Columns(1).Width = GridTodos.Width - 20
        GridTodos.Columns(0).Visible = False
        txtMenuBusca.Focus()
    End Sub
    Private Sub GridTodos_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles GridTodos.CellEnter
        CargaForm(GridTodos.Rows(GridTodos.CurrentRow.Index).Cells(0).Value, UCase(TablaBd))
    End Sub
    Private Sub GridTodos_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles GridTodos.KeyPress
        If Asc(e.KeyChar) = 8 Then
            txtMenuBusca.Focus()
            e.Handled = True
        End If
    End Sub
    Private Sub CargaForm(ByVal idClave As Integer, ByVal NomTabla As String)
        Try
            Select Case UCase(NomTabla)
                Case UCase(TablaBd)
                    Herramienta = New HerramientaClass(idClave)
                    With Herramienta
                        txtidHerramienta.Text = .idHerramienta
                        If .Existe Then
                            txtNomHerramienta.Text = .NomHerramienta
                            txtidFamilia.Text = .idFamilia
                            CargaForm(.idFamilia, TablaBd2)
                            txtidTipoHerramienta.Text = .idTipoHerramienta
                            CargaForm(.idTipoHerramienta, TablaBd3)
                            txtidMarca.Text = .idMarca
                            CargaForm(.idMarca, TablaBd4)
                            txtNoPiezas.Text = .NoPiezas
                            txtModelo.Text = .Modelo
                            txtMedida.Text = .Medida

                            If OpcionForma = TipoOpcionForma.tOpcInsertar Then
                                Status("La Herramienta Con idFamilia: " & .idHerramienta & _
                                       " Ya existe!!!, Favor de modificarlo para poder dar de alta a otra Herramienta", Me, Err:=True)
                                MessageBox.Show("La Herramienta con id: " & .idHerramienta & _
                                                " YA EXISTE!!!, Favor de Cambiarlo y volver a intentarlo",
                                                "La Herramienta con id: " & .idHerramienta & " YA EXISTE!!!", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            End If
                        Else 'No existe 
                            If OpcionForma = TipoOpcionForma.tOpcInsertar Then
                                ActivaCampos(True, TipoOpcActivaCampos.tOpcINICIALIZA)
                                ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)
                                btnMnuOk.Enabled = True
                                txtNomHerramienta.Focus()
                                Status("Listo para Guardar el nueva Herramienta con id: " & .idFamilia, Me)
                            End If
                        End If
                    End With
                Case UCase(TablaBd2)
                    'aqui()
                    Familia = New FamiliasClass(idClave)
                    With Familia
                        txtidFamilia.Text = .IdFamilia
                        If .Existe Then
                            txtNomFamilia.Text = .NomFamilia
                            txtNomDivision.Text = .NomDivision
                        Else
                            MsgBox("La Familia con id: " & txtidFamilia.Text & " No Existe")
                        End If
                    End With
                Case UCase(TablaBd3)
                    TipoHerramienta = New TipoHerramClass(idClave)
                    With TipoHerramienta
                        txtidTipoHerramienta.Text = .idTipoHerramienta
                        If .Existe Then
                            txtNomTipoHerramienta.Text = .NomTipoHerramienta
                        Else
                            MsgBox("El Tipo de Herramienta con id: " & txtidFamilia.Text & " No Existe")
                        End If
                    End With
                Case UCase(TablaBd4)
                    Marca = New MarcaClass(idClave)
                    With Marca
                        txtidMarca.Text = .IdMarca
                        If .Existe Then
                            txtNombreMarca.Text = .NOMBREMARCA
                        Else
                            MsgBox("La Marca con id: " & txtidFamilia.Text & " No Existe")
                        End If
                    End With
            End Select



        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try

    End Sub
    Private Function Validar() As Boolean
        Validar = True
        If Not Inicio.ValidandoCampo(Trim(txtidHerramienta.Text), TipoDato.TdNumerico) Then
            If Not bEsIdentidad Then
                MsgBox(MENSAJE_CLAVE, vbInformation, "Aviso" & Me.Text)
                If txtidHerramienta.Enabled Then
                    txtidHerramienta.Focus()
                    txtidHerramienta.SelectAll()
                End If
                Validar = False
            End If
        ElseIf Not Inicio.ValidandoCampo(Trim(txtNomHerramienta.Text), TipoDato.TdCadena) Then
            MsgBox("El Nombre de la Herramienta No Puede Ser Vacio", vbInformation, "Aviso" & Me.Text)
            If txtNomHerramienta.Enabled Then
                txtNomHerramienta.Focus()
                txtNomHerramienta.SelectAll()
            End If
            Validar = False
        ElseIf Not Inicio.ValidandoCampo(Trim(txtidFamilia.Text), TipoDato.TdNumerico) Then
            MsgBox("La Familia de la Herramienta no Puede ser Vacio", vbInformation, "Aviso" & Me.Text)
            If txtidFamilia.Enabled Then
                txtidFamilia.Focus()
                txtidFamilia.SelectAll()
            End If
            Validar = False
        End If
    End Function

    'Private Sub txtidHerramienta_EnabledChanged(sender As Object, e As EventArgs) Handles txtidHerramienta.EnabledChanged
    '    cmdBuscaClave.Enabled = txtidHerramienta.Enabled
    'End Sub
    'CONTROLES
    Private Sub txtidHerramienta_KeyDown(sender As Object, e As KeyEventArgs) Handles txtidHerramienta.KeyDown
        If e.KeyCode = Keys.F3 Then
            cmdBuscaClave_Click(sender, e)
        End If

    End Sub
    Private Sub txtidHerramienta_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtidHerramienta.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If txtidHerramienta.Text.Trim <> "" Then
                CargaForm(txtidHerramienta.Text, UCase(TablaBd))
            End If
            e.Handled = True
        End If
    End Sub
    Private Sub txtNomHerramienta_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtNomHerramienta.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            'btnMnuOk_Click(sender, e)
            txtidFamilia.Focus()
            txtidFamilia.SelectAll()
        End If
    End Sub

    Private Sub cmdBuscaClave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBuscaClave.Click
        Dim idEst As String = DespliegaGrid(UCase(TablaBd), Inicio.CONSTR, CampoLLave)
        If idEst <> "" Then CargaForm(idEst, UCase(TablaBd))
    End Sub

    Private Sub txtNomHerramienta_MouseDown(sender As Object, e As MouseEventArgs) Handles txtNomHerramienta.MouseDown
        txtNomHerramienta.Focus()
        txtNomHerramienta.SelectAll()
    End Sub

    Private Sub txtidFamilia_EnabledChanged(sender As Object, e As EventArgs) Handles txtidFamilia.EnabledChanged
        cmdBuscaClaveFam.Enabled = txtidFamilia.Enabled
    End Sub

    Private Sub txtidFamilia_KeyDown(sender As Object, e As KeyEventArgs) Handles txtidFamilia.KeyDown
        If e.KeyCode = Keys.F3 Then
            cmdBuscaClaveFam_Click(sender, e)
        End If
    End Sub

    Private Sub txtidFamilia_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtidFamilia.KeyPress
        '    btnMnuOk_Click(sender, e)
        '
        'If Asc(e.KeyChar) = 13 Then
        '    If txtidFamilia.Text.Trim <> "" Then
        '        CargaForm(txtidFamilia.Text, TablaBd2)
        '        txtNomSucursal.Focus()
        '        txtNomSucursal.SelectAll()
        '    End If
        '    e.Handled = True
        'End If
        If txtidFamilia.Text.Length = 0 Then
            txtidFamilia.Focus()
        Else
            If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
                Salida = True
                CargaForm(txtidFamilia.Text, TablaBd2)
                Salida = False
                txtidTipoHerramienta.Focus()
                txtidTipoHerramienta.SelectAll()
            End If
        End If
    End Sub

    Private Sub txtidFamilia_Leave(sender As Object, e As EventArgs) Handles txtidFamilia.Leave
        If Salida Then Exit Sub
        If txtidFamilia.Text.Length = 0 Then
            txtidFamilia.Focus()
            txtidFamilia.SelectAll()
        Else
            If txtidFamilia.Enabled Then
                CargaForm(txtidFamilia.Text, TablaBd2)
                txtidTipoHerramienta.Focus()
                txtidTipoHerramienta.SelectAll()
            End If
        End If
    End Sub

    Private Sub txtidFamilia_MouseDown(sender As Object, e As MouseEventArgs) Handles txtidFamilia.MouseDown
        txtidFamilia.Focus()
        txtidFamilia.SelectAll()
    End Sub

    Private Sub cmdBuscaClaveFam_Click(sender As Object, e As EventArgs) Handles cmdBuscaClaveFam.Click
        txtidFamilia.Text = DespliegaGrid(TablaBd2, Inicio.CONSTR, CampoLLave)
        txtidFamilia_Leave(sender, e)
    End Sub

    Private Sub cmdCancelar_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        With TryCast(sender, Button)
            If .Enabled Then
                Me.CancelButton = sender
            End If
        End With
    End Sub
    Private Sub cmdTerminar_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        With TryCast(sender, Button)
            If .Enabled Then
                Me.CancelButton = sender
            End If
        End With
    End Sub

    Private Sub btnMnuOk_Click(sender As Object, e As EventArgs) Handles btnMnuOk.Click
        Dim CadCam As String = ""
        Try
            Salida = True
            If Not Validar() Then Exit Sub
            If GridTodos.RowCount = 0 Then
                Herramienta = New HerramientaClass(0)
            End If
            With Herramienta
                If OpcionForma = TipoOpcionForma.tOpcModificar Then

                    CadCam = .GetCambios(txtNomHerramienta.Text, txtMedida.Text, txtidFamilia.Text, txtidTipoHerramienta.Text, txtidMarca.Text, txtNoPiezas.Text, txtModelo.Text)
                    If CadCam <> "" Then
                        'If MessageBox.Show("Esta completamente Seguro que Desea guardar los Cambios Realizados al Estado Con id: " & .IDMARCA & vbNewLine &
                        '                CadCam, "Confirma que desea guardar los Cambios Realizados???", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then Exit Sub
                        Status("Guardando Modificaciones de la Herramienta Con id: " & .idFamilia, Me, 1, 2)
                        .Guardar(bEsIdentidad, txtNomHerramienta.Text, txtMedida.Text, txtidFamilia.Text, txtidTipoHerramienta.Text, txtidMarca.Text, txtNoPiezas.Text, txtModelo.Text)
                        Status("Herramienta con Id: " & .idFamilia & " Modificado Con Exito!!!", Me, 2, 2)
                        MessageBox.Show("Herramienta con Id: " & .idFamilia & " Modificada con exito!!!",
                                        "Accion terminada con exito!!!", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
                        ActualizaGrid(txtMenuBusca.Text)
                    Else
                        Status("No se detectaron cambios para realizar!!!", Me, Err:=True)
                    End If
                ElseIf OpcionForma = TipoOpcionForma.tOpcInsertar Then
                    .Existe = False
                    .NomHerramienta = txtNomHerramienta.Text
                    If Not bEsIdentidad Then
                        .idFamilia = txtidHerramienta.Text
                    End If
                    Status("Guardando Nueva Herramienta: " & txtNomHerramienta.Text & ". . .", Me, 1, 2)
                    .Guardar(bEsIdentidad, txtNomHerramienta.Text, txtMedida.Text, txtidFamilia.Text, txtidTipoHerramienta.Text, txtidMarca.Text, txtNoPiezas.Text, txtModelo.Text)
                    Status("Herramienta: " & txtNomHerramienta.Text & " Guardado con Exito!!!", Me, 2, 2)
                    MessageBox.Show("Nueva Herramienta: " & txtNomHerramienta.Text & " Guardado con exito!!!",
                                    "Accion terminada con exito!!!", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
                    ActualizaGrid(txtMenuBusca.Text)

                End If
            End With
            Salida = False
        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try
    End Sub


    Private Sub btnMnuCancelar_Click(sender As Object, e As EventArgs) Handles btnMnuCancelar.Click
        If txtidHerramienta.Text <> "" Then
            CargaForm(txtidHerramienta.Text, UCase(TablaBd))
        End If
        OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
    End Sub

    Private Sub btnMnuSalir_Click(sender As Object, e As EventArgs) Handles btnMnuSalir.Click
        Me.Close()
    End Sub

    Private Sub txtMenuBusca_TextChanged(sender As Object, e As EventArgs) Handles txtMenuBusca.TextChanged
        ActualizaGrid(txtMenuBusca.Text)
    End Sub

    Private Sub txtidFamilia_TextChanged(sender As Object, e As EventArgs) Handles txtidFamilia.TextChanged

    End Sub

    Private Sub txtidTipoHerramienta_EnabledChanged(sender As Object, e As EventArgs) Handles txtidTipoHerramienta.EnabledChanged
        cmdBuscaClaveTPH.Enabled = txtidTipoHerramienta.Enabled
    End Sub

    Private Sub txtidTipoHerramienta_KeyDown(sender As Object, e As KeyEventArgs) Handles txtidTipoHerramienta.KeyDown
        If e.KeyCode = Keys.F3 Then
            cmdBuscaClaveTPH_Click(sender, e)
        End If
    End Sub

    Private Sub txtidTipoHerramienta_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtidTipoHerramienta.KeyPress
        If txtidTipoHerramienta.Text.Length = 0 Then
            txtidTipoHerramienta.Focus()
        Else
            If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
                Salida = True
                CargaForm(txtidTipoHerramienta.Text, TablaBd3)
                Salida = False
                txtidMarca.Focus()
                txtidMarca.SelectAll()
            End If
        End If
    End Sub

    Private Sub txtidTipoHerramienta_Leave(sender As Object, e As EventArgs) Handles txtidTipoHerramienta.Leave
        If Salida Then Exit Sub
        If txtidTipoHerramienta.Text.Length = 0 Then
            txtidTipoHerramienta.Focus()
            txtidTipoHerramienta.SelectAll()
        Else
            If txtidTipoHerramienta.Enabled Then
                CargaForm(txtidTipoHerramienta.Text, TablaBd3)
                txtidMarca.Focus()
                txtidMarca.SelectAll()
            End If
        End If
    End Sub

    Private Sub txtidTipoHerramienta_MouseDown(sender As Object, e As MouseEventArgs) Handles txtidTipoHerramienta.MouseDown
        txtidTipoHerramienta.Focus()
        txtidTipoHerramienta.SelectAll()
    End Sub

    Private Sub txtidMarca_EnabledChanged(sender As Object, e As EventArgs) Handles txtidMarca.EnabledChanged
        cmdBuscaClaveMar.Enabled = txtidMarca.Enabled
    End Sub

    Private Sub txtidMarca_KeyDown(sender As Object, e As KeyEventArgs) Handles txtidMarca.KeyDown
        If e.KeyCode = Keys.F3 Then
            cmdBuscaClaveMar_Click(sender, e)
        End If

    End Sub

    Private Sub txtidMarca_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtidMarca.KeyPress
        If txtidMarca.Text.Length = 0 Then
            txtidMarca.Focus()
        Else
            If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
                Salida = True
                CargaForm(txtidMarca.Text, TablaBd4)
                Salida = False
                txtModelo.Focus()
                txtModelo.SelectAll()
            End If
        End If
    End Sub

    Private Sub txtidMarca_Leave(sender As Object, e As EventArgs) Handles txtidMarca.Leave
        If Salida Then Exit Sub
        If txtidMarca.Text.Length = 0 Then
            txtidMarca.Focus()
            txtidMarca.SelectAll()
        Else
            If txtidMarca.Enabled Then
                CargaForm(txtidMarca.Text, TablaBd4)
                txtModelo.Focus()
                txtModelo.SelectAll()
            End If
        End If
    End Sub

    Private Sub txtidMarca_MouseDown(sender As Object, e As MouseEventArgs) Handles txtidMarca.MouseDown
        txtidMarca.Focus()
        txtidMarca.SelectAll()
    End Sub

    Private Sub cmdBuscaClaveTPH_Click(sender As Object, e As EventArgs) Handles cmdBuscaClaveTPH.Click
        txtidTipoHerramienta.Text = DespliegaGrid(UCase(TablaBd3), Inicio.CONSTR, CampoLLave)
        txtidTipoHerramienta_Leave(sender, e)
    End Sub

    Private Sub cmdBuscaClaveMar_Click(sender As Object, e As EventArgs) Handles cmdBuscaClaveMar.Click
        txtidMarca.Text = DespliegaGrid(UCase(TablaBd4), Inicio.CONSTR, CampoLLave)
        txtidMarca_Leave(sender, e)
    End Sub

    Private Sub txtModelo_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtModelo.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            txtNoPiezas.Focus()
            txtNoPiezas.SelectAll()
        End If
    End Sub

    Private Sub txtModelo_MouseDown(sender As Object, e As MouseEventArgs) Handles txtModelo.MouseDown
        txtModelo.Focus()
        txtModelo.SelectAll()
    End Sub

    Private Sub txtNoPiezas_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtNoPiezas.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            txtMedida.Focus()
            txtMedida.SelectAll()
        End If
    End Sub

    Private Sub txtNoPiezas_MouseDown(sender As Object, e As MouseEventArgs) Handles txtNoPiezas.MouseDown
        txtNoPiezas.Focus()
        txtNoPiezas.SelectAll()
    End Sub

    Private Sub txtMedida_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtMedida.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            btnMnuOk_Click(sender, e)
        End If
    End Sub

    Private Sub txtMedida_MouseDown(sender As Object, e As MouseEventArgs) Handles txtMedida.MouseDown
        txtMedida.Focus()
        txtMedida.SelectAll()
    End Sub

End Class
