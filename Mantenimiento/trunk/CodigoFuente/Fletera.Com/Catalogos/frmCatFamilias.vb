'Fecha: 15 / Febrero / 2016
'ROGER GALA PACHECO
'************************************************************************************************************************************
'*  Fecha Modificacion :   15 / Febrero / 2016                                                                                       
'*  
'************************************************************************************************************************************
Public Class frmCatFamilias
    Inherits System.Windows.Forms.Form
    Dim OpcionForma As TipoOpcionForma
    Dim TablaBd As String = "CatFamilia"
    Dim CampoLLave As String = "idFamilia"
    Dim TablaBd2 As String = "CatDivision"
    Public WithEvents MeStatus1 As ControlStatus.MeStatus
    Friend WithEvents GridTodos As System.Windows.Forms.DataGridView
    Private Familia As FamiliasClass
    Private Division As DivisionClass
    Friend WithEvents btnMnuOk As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuCancelar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuSalir As System.Windows.Forms.ToolStripButton
    Private _Tag As String
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cmdBuscaClaveDiv As System.Windows.Forms.Button
    Friend WithEvents txtidDivision As System.Windows.Forms.TextBox
    Friend WithEvents txtNomDivision As System.Windows.Forms.TextBox
    Dim bEsIdentidad As Boolean = True
    Dim Salida As Boolean
    Private con As String
    Private v As Boolean

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents GrupoCaptura As System.Windows.Forms.GroupBox
    Friend WithEvents ToolStripMenu As System.Windows.Forms.ToolStrip
    Friend WithEvents txtMenuBusca As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents btnMnuAltas As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuModificar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuEliminar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuReporte As System.Windows.Forms.ToolStripButton
    Friend WithEvents label1 As System.Windows.Forms.Label
    Friend WithEvents label2 As System.Windows.Forms.Label
    Friend WithEvents cmdBuscaClave As System.Windows.Forms.Button
    Friend WithEvents txtidFamilia As System.Windows.Forms.TextBox
    Friend WithEvents txtNomFamilia As System.Windows.Forms.TextBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCatFamilias))
        Me.txtidFamilia = New System.Windows.Forms.TextBox()
        Me.label1 = New System.Windows.Forms.Label()
        Me.txtNomFamilia = New System.Windows.Forms.TextBox()
        Me.label2 = New System.Windows.Forms.Label()
        Me.GrupoCaptura = New System.Windows.Forms.GroupBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cmdBuscaClaveDiv = New System.Windows.Forms.Button()
        Me.txtidDivision = New System.Windows.Forms.TextBox()
        Me.txtNomDivision = New System.Windows.Forms.TextBox()
        Me.cmdBuscaClave = New System.Windows.Forms.Button()
        Me.ToolStripMenu = New System.Windows.Forms.ToolStrip()
        Me.txtMenuBusca = New System.Windows.Forms.ToolStripTextBox()
        Me.btnMnuAltas = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuModificar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuEliminar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuReporte = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuOk = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuCancelar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuSalir = New System.Windows.Forms.ToolStripButton()
        Me.MeStatus1 = New ControlStatus.MeStatus()
        Me.GridTodos = New System.Windows.Forms.DataGridView()
        Me.GrupoCaptura.SuspendLayout()
        Me.ToolStripMenu.SuspendLayout()
        CType(Me.GridTodos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtidFamilia
        '
        Me.txtidFamilia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtidFamilia.Location = New System.Drawing.Point(109, 27)
        Me.txtidFamilia.MaxLength = 3
        Me.txtidFamilia.Name = "txtidFamilia"
        Me.txtidFamilia.Size = New System.Drawing.Size(69, 20)
        Me.txtidFamilia.TabIndex = 4
        '
        'label1
        '
        Me.label1.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.label1.Location = New System.Drawing.Point(6, 27)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(94, 20)
        Me.label1.TabIndex = 14
        Me.label1.Text = "id Familia:"
        '
        'txtNomFamilia
        '
        Me.txtNomFamilia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNomFamilia.Location = New System.Drawing.Point(109, 59)
        Me.txtNomFamilia.MaxLength = 50
        Me.txtNomFamilia.Name = "txtNomFamilia"
        Me.txtNomFamilia.Size = New System.Drawing.Size(322, 20)
        Me.txtNomFamilia.TabIndex = 6
        '
        'label2
        '
        Me.label2.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.label2.Location = New System.Drawing.Point(9, 59)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(91, 20)
        Me.label2.TabIndex = 16
        Me.label2.Text = "Nombre Familia:"
        '
        'GrupoCaptura
        '
        Me.GrupoCaptura.Controls.Add(Me.Label3)
        Me.GrupoCaptura.Controls.Add(Me.cmdBuscaClaveDiv)
        Me.GrupoCaptura.Controls.Add(Me.txtidDivision)
        Me.GrupoCaptura.Controls.Add(Me.txtNomDivision)
        Me.GrupoCaptura.Controls.Add(Me.label1)
        Me.GrupoCaptura.Controls.Add(Me.label2)
        Me.GrupoCaptura.Controls.Add(Me.cmdBuscaClave)
        Me.GrupoCaptura.Controls.Add(Me.txtidFamilia)
        Me.GrupoCaptura.Controls.Add(Me.txtNomFamilia)
        Me.GrupoCaptura.Location = New System.Drawing.Point(168, 45)
        Me.GrupoCaptura.Name = "GrupoCaptura"
        Me.GrupoCaptura.Size = New System.Drawing.Size(524, 222)
        Me.GrupoCaptura.TabIndex = 3
        Me.GrupoCaptura.TabStop = False
        '
        'Label3
        '
        Me.Label3.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label3.Location = New System.Drawing.Point(6, 97)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(94, 20)
        Me.Label3.TabIndex = 20
        Me.Label3.Text = "Id Divisi�n:"
        '
        'cmdBuscaClaveDiv
        '
        Me.cmdBuscaClaveDiv.Image = CType(resources.GetObject("cmdBuscaClaveDiv.Image"), System.Drawing.Image)
        Me.cmdBuscaClaveDiv.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdBuscaClaveDiv.Location = New System.Drawing.Point(186, 89)
        Me.cmdBuscaClaveDiv.Name = "cmdBuscaClaveDiv"
        Me.cmdBuscaClaveDiv.Size = New System.Drawing.Size(32, 32)
        Me.cmdBuscaClaveDiv.TabIndex = 19
        Me.cmdBuscaClaveDiv.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'txtidDivision
        '
        Me.txtidDivision.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtidDivision.Location = New System.Drawing.Point(109, 97)
        Me.txtidDivision.MaxLength = 3
        Me.txtidDivision.Name = "txtidDivision"
        Me.txtidDivision.Size = New System.Drawing.Size(69, 20)
        Me.txtidDivision.TabIndex = 18
        '
        'txtNomDivision
        '
        Me.txtNomDivision.Enabled = False
        Me.txtNomDivision.Location = New System.Drawing.Point(224, 101)
        Me.txtNomDivision.Name = "txtNomDivision"
        Me.txtNomDivision.ReadOnly = True
        Me.txtNomDivision.Size = New System.Drawing.Size(275, 20)
        Me.txtNomDivision.TabIndex = 17
        '
        'cmdBuscaClave
        '
        Me.cmdBuscaClave.Image = CType(resources.GetObject("cmdBuscaClave.Image"), System.Drawing.Image)
        Me.cmdBuscaClave.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdBuscaClave.Location = New System.Drawing.Point(186, 19)
        Me.cmdBuscaClave.Name = "cmdBuscaClave"
        Me.cmdBuscaClave.Size = New System.Drawing.Size(32, 32)
        Me.cmdBuscaClave.TabIndex = 5
        Me.cmdBuscaClave.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'ToolStripMenu
        '
        Me.ToolStripMenu.AutoSize = False
        Me.ToolStripMenu.BackColor = System.Drawing.Color.Silver
        Me.ToolStripMenu.Dock = System.Windows.Forms.DockStyle.None
        Me.ToolStripMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.txtMenuBusca, Me.btnMnuAltas, Me.btnMnuModificar, Me.btnMnuEliminar, Me.btnMnuReporte, Me.btnMnuOk, Me.btnMnuCancelar, Me.btnMnuSalir})
        Me.ToolStripMenu.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow
        Me.ToolStripMenu.Location = New System.Drawing.Point(3, 0)
        Me.ToolStripMenu.Name = "ToolStripMenu"
        Me.ToolStripMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.ToolStripMenu.Size = New System.Drawing.Size(730, 42)
        Me.ToolStripMenu.TabIndex = 1
        Me.ToolStripMenu.Text = "ToolStrip1"
        '
        'txtMenuBusca
        '
        Me.txtMenuBusca.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.txtMenuBusca.Name = "txtMenuBusca"
        Me.txtMenuBusca.Size = New System.Drawing.Size(150, 42)
        Me.txtMenuBusca.ToolTipText = "Filtra Listado"
        '
        'btnMnuAltas
        '
        Me.btnMnuAltas.ForeColor = System.Drawing.Color.Red
        Me.btnMnuAltas.Image = CType(resources.GetObject("btnMnuAltas.Image"), System.Drawing.Image)
        Me.btnMnuAltas.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuAltas.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuAltas.Name = "btnMnuAltas"
        Me.btnMnuAltas.Size = New System.Drawing.Size(46, 39)
        Me.btnMnuAltas.Text = "&ALTAS"
        Me.btnMnuAltas.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnMnuAltas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuModificar
        '
        Me.btnMnuModificar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuModificar.Image = CType(resources.GetObject("btnMnuModificar.Image"), System.Drawing.Image)
        Me.btnMnuModificar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuModificar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuModificar.Name = "btnMnuModificar"
        Me.btnMnuModificar.Size = New System.Drawing.Size(74, 39)
        Me.btnMnuModificar.Text = "&MODIFICAR"
        Me.btnMnuModificar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnMnuModificar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuEliminar
        '
        Me.btnMnuEliminar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuEliminar.Image = CType(resources.GetObject("btnMnuEliminar.Image"), System.Drawing.Image)
        Me.btnMnuEliminar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuEliminar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuEliminar.Name = "btnMnuEliminar"
        Me.btnMnuEliminar.Size = New System.Drawing.Size(64, 39)
        Me.btnMnuEliminar.Text = "&ELIMINAR"
        Me.btnMnuEliminar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuReporte
        '
        Me.btnMnuReporte.ForeColor = System.Drawing.Color.Red
        Me.btnMnuReporte.Image = CType(resources.GetObject("btnMnuReporte.Image"), System.Drawing.Image)
        Me.btnMnuReporte.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuReporte.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuReporte.Name = "btnMnuReporte"
        Me.btnMnuReporte.Size = New System.Drawing.Size(60, 39)
        Me.btnMnuReporte.Text = "&REPORTE"
        Me.btnMnuReporte.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuOk
        '
        Me.btnMnuOk.ForeColor = System.Drawing.Color.Red
        Me.btnMnuOk.Image = Global.Fletera.My.Resources.Resources.Save1
        Me.btnMnuOk.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuOk.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuOk.Name = "btnMnuOk"
        Me.btnMnuOk.Size = New System.Drawing.Size(56, 39)
        Me.btnMnuOk.Text = "&GRABAR"
        Me.btnMnuOk.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuCancelar
        '
        Me.btnMnuCancelar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuCancelar.Image = Global.Fletera.My.Resources.Resources.signos_f_059
        Me.btnMnuCancelar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuCancelar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuCancelar.Name = "btnMnuCancelar"
        Me.btnMnuCancelar.Size = New System.Drawing.Size(71, 39)
        Me.btnMnuCancelar.Text = "&CANCELAR"
        Me.btnMnuCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuSalir
        '
        Me.btnMnuSalir.ForeColor = System.Drawing.Color.Red
        Me.btnMnuSalir.Image = Global.Fletera.My.Resources.Resources.Salida2
        Me.btnMnuSalir.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuSalir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuSalir.Name = "btnMnuSalir"
        Me.btnMnuSalir.Size = New System.Drawing.Size(41, 39)
        Me.btnMnuSalir.Text = "&SALIR"
        Me.btnMnuSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'MeStatus1
        '
        Me.MeStatus1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.MeStatus1.Location = New System.Drawing.Point(0, 266)
        Me.MeStatus1.Margin = New System.Windows.Forms.Padding(4)
        Me.MeStatus1.Name = "MeStatus1"
        Me.MeStatus1.Size = New System.Drawing.Size(692, 26)
        Me.MeStatus1.TabIndex = 72
        '
        'GridTodos
        '
        Me.GridTodos.AllowUserToAddRows = False
        Me.GridTodos.AllowUserToDeleteRows = False
        Me.GridTodos.AllowUserToResizeRows = False
        Me.GridTodos.BackgroundColor = System.Drawing.Color.White
        Me.GridTodos.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.GridTodos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.GridTodos.Location = New System.Drawing.Point(3, 45)
        Me.GridTodos.MultiSelect = False
        Me.GridTodos.Name = "GridTodos"
        Me.GridTodos.ReadOnly = True
        Me.GridTodos.RowHeadersVisible = False
        Me.GridTodos.Size = New System.Drawing.Size(162, 222)
        Me.GridTodos.TabIndex = 73
        '
        'frmCatFamilias
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(692, 292)
        Me.Controls.Add(Me.GridTodos)
        Me.Controls.Add(Me.MeStatus1)
        Me.Controls.Add(Me.ToolStripMenu)
        Me.Controls.Add(Me.GrupoCaptura)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmCatFamilias"
        Me.Text = "Catal�go de Familias"
        Me.GrupoCaptura.ResumeLayout(False)
        Me.GrupoCaptura.PerformLayout()
        Me.ToolStripMenu.ResumeLayout(False)
        Me.ToolStripMenu.PerformLayout()
        CType(Me.GridTodos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region
    Sub New(ByVal vTag As String)
        InitializeComponent()
        _Tag = vTag
    End Sub

    Public Sub New(con As String, v As Boolean)
        Me.con = con
        BD = New CapaDatos.UtilSQL(con, "Juan")
        InitializeComponent()
        Me.v = v
    End Sub

#Region "MenusToolStrip"
    Private Sub LimpiaVariables()
        SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")

    End Sub
    Private Sub LimpiaCampos()
        txtidFamilia.Text = ""
        txtNomFamilia.Text = ""
        txtidDivision.Text = ""
        txtNomDivision.Text = ""
        LimpiaVariables()
    End Sub

    Private Sub ActivaCampos(ByVal valor As Boolean, ByVal vOpcion As TipoOpcActivaCampos)
        If vOpcion = TipoOpcActivaCampos.tOpcDESHABTODOS Then
            txtidFamilia.Enabled = False
            txtNomFamilia.Enabled = False
            txtidDivision.Enabled = False
        ElseIf vOpcion = TipoOpcActivaCampos.tOpcINICIALIZA Then
            txtidFamilia.Enabled = Not valor
            txtNomFamilia.Enabled = valor
            txtidDivision.Enabled = valor
        End If
    End Sub


    Private Sub ActivaBotones(ByVal valor As Boolean, Optional ByVal vOpcion As TipoOpcActivaBoton = TipoOpcActivaBoton.tOpcInicializa)
        If vOpcion = TipoOpcActivaBoton.tOpcConsulta Then
            btnMnuOk.Enabled = False
            btnMnuCancelar.Enabled = valor
            btnMnuSalir.Enabled = Not valor

            btnMnuAltas.Enabled = Not valor
            btnMnuModificar.Enabled = Not valor
            btnMnuEliminar.Enabled = Not valor
            btnMnuReporte.Enabled = Not valor
        ElseIf vOpcion = TipoOpcActivaBoton.tOpcInicializa Then
            btnMnuOk.Enabled = valor
            btnMnuCancelar.Enabled = valor
            btnMnuSalir.Enabled = Not valor

            btnMnuAltas.Enabled = Not valor
            btnMnuModificar.Enabled = Not valor
            btnMnuEliminar.Enabled = Not valor
            btnMnuReporte.Enabled = Not valor

            txtMenuBusca.Enabled = Not valor
            GridTodos.Enabled = Not valor
        ElseIf vOpcion = TipoOpcActivaBoton.tOpcInsertar Then
            btnMnuOk.Enabled = valor
            btnMnuCancelar.Enabled = valor
            btnMnuSalir.Enabled = Not valor

            btnMnuAltas.Enabled = Not valor
            btnMnuModificar.Enabled = Not valor
            btnMnuEliminar.Enabled = Not valor
            btnMnuReporte.Enabled = Not valor
        End If
    End Sub

    Private Sub btnMnuAltas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuAltas.Click
        'ALTAS
        '15/FEB/2016
        If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Alta) = False Then
            MessageBox.Show("No tiene los permisos Necesarios para poder dar de Alta en el Catalogo de Familias!!!",
                "Acceso Denegado a Alta de Familias", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            OpcionForma = TipoOpcionForma.tOpcConsultar
            Exit Sub
        End If
        OpcionForma = TipoOpcionForma.tOpcInsertar
        LimpiaCampos()
        ActivaCampos(True, TipoOpcActivaCampos.tOpcINICIALIZA)
        ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)

        If bEsIdentidad Then
            txtidFamilia.Enabled = False
            txtNomFamilia.Focus()
        Else
            txtidFamilia.Focus()
        End If
        Status("Ingrese nueva Familia para guardar", Me)
    End Sub
    Private Sub btnMnuModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuModificar.Click
        'MODIFICAR
        If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Modificar) = False Then
            MessageBox.Show("No tiene los permisos Necesarios para poder Modificar la Informacion de la Familias!!!",
                   "Acceso Denegado a Modificacion de la Informacion de la Familias", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            OpcionForma = TipoOpcionForma.tOpcConsultar
            Exit Sub
        End If
        If Trim(txtidFamilia.Text) = "" Then
            txtidFamilia.Text = DespliegaGrid(TablaBd, Inicio.CONSTR, CampoLLave)
            CargaForm(txtidFamilia.Text, UCase(TablaBd))
        End If

        OpcionForma = TipoOpcionForma.tOpcModificar
        ActivaCampos(True, TipoOpcActivaCampos.tOpcINICIALIZA)
        ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)

        btnMnuEliminar.Enabled = True
        If bEsIdentidad Then
            txtidFamilia.Enabled = False
            txtNomFamilia.Focus()
        Else
            txtidFamilia.Focus()
        End If

        Status("Ingrese nueva Familia para guardar", Me)
    End Sub
    Private Sub btnMnuEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuEliminar.Click
        'ELIMINAR
        If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Baja) = False Then
            MessageBox.Show("No tiene los permisos Necesarios para poder Eliminar la Familias!!!",
                "Acceso Denegado a Eliminacion de Familias", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            OpcionForma = TipoOpcionForma.tOpcConsultar
            Exit Sub
        End If
        If txtidFamilia.Text = Familia.IdFamilia Then
            With Familia
                If MessageBox.Show("Esta completamente seguro que desea Eliminar la Familias: " &
                                   .NomFamilia & " Con Id: " & .IdFamilia & " ????", "Confirma que desea eliminar la Familias: " & .NomFamilia & "???",
                                 MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then Exit Sub
                Try
                    .Eliminar() 'Elimina el municipio
                    MessageBox.Show("Familia: " & .NomFamilia & " Eliminada con exito!!!",
                                    "Familia Eliminado con exito", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Status("Familia: " & .NomFamilia & " Eliminado con exito!!!", Me)
                    ActualizaGrid(txtMenuBusca.Text)
                    txtidFamilia.Text = ""
                    OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
                Catch ex As Exception
                    Status("Error al tratar de eliminar la Familia: " & ex.Message, Me, Err:=True)
                    MessageBox.Show("Error al Tratar de eliminar la Familia: " & .NomFamilia & vbNewLine &
                                    "Error. " & ex.Message, "Error: " & Err.Number, MessageBoxButtons.OK, MessageBoxIcon.Error)

                End Try
            End With
        End If
    End Sub
    Private Sub btnMnuReporte_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuReporte.Click
        'REPORTE
        'If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Reporte) = False Then
        '    MessageBox.Show("No tiene los permisos Necesarios para poder realizar Reporte de Familias!!!",
        '        "Acceso Denegado a Reportes de Familias", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    OpcionForma = TipoOpcionForma.tOpcConsultar
        '    Exit Sub
        'End If
        ToolStripMenu.Enabled = False
        Status("Abriendo Reporte Espere. . . ", Me)
        Me.Cursor = Cursors.WaitCursor
        DespliegaReporte(TablaBd, CONSTR)
        Status("Finalizo el reporte. . . ", Me)
        Me.Cursor = Cursors.Default
        ToolStripMenu.Enabled = True
    End Sub

#End Region
    Private Sub ActualizaGrid(ByVal vNomFamilia As String)
        If vNomFamilia = "" Then
            GridTodos.DataSource = BD.ExecuteReturn("SELECT idFamilia, NomFamilia FROM " & TablaBd, TablaBd)
            Status(GridTodos.RowCount & " Registros", Me)
        Else
            GridTodos.DataSource = BD.ExecuteReturn("SELECT idFamilia, NomFamilia FROM " & TablaBd &
                                                " WHERE NomFamilia Like '%" & vNomFamilia & "%'")
            Status(GridTodos.RowCount & " Registros obtenidos con el filtro %" & vNomFamilia & "%", Me)
        End If
    End Sub

    Private Sub OpcionMnuTools(ByVal vOpcion As TipoOpcionForma)
        If vOpcion <> TipoOpcionForma.tOpcImprimir And vOpcion <> TipoOpcionForma.tOpcEliminar Then
            OpcionForma = vOpcion
        End If
        txtMenuBusca.Enabled = True
        GridTodos.Enabled = True
        If vOpcion = TipoOpcionForma.tOpcConsultar Then
            'CONSULTA
            For Each ct As Control In GrupoCaptura.Controls
                If Not ct Is cmdBuscaClave Then
                    ct.Enabled = False
                Else
                    ct.Enabled = True
                End If
            Next
            txtMenuBusca.Enabled = True
            btnMnuAltas.Enabled = True
            btnMnuModificar.Enabled = True
            btnMnuEliminar.Enabled = False
            btnMnuReporte.Enabled = True
            btnMnuOk.Enabled = False
            btnMnuCancelar.Enabled = False

            btnMnuSalir.Enabled = True


        End If
    End Sub


    Private Sub txtMenuBusca_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtMenuBusca.KeyPress
        If Asc(e.KeyChar) = 39 Then e.Handled = True : Exit Sub 'Caracter ' Evita errores
        If Asc(e.KeyChar) = 13 Then
            GridTodos.Focus()
            e.Handled = True
        End If
    End Sub

    'Private Sub txtMenuBusca_KeyUp(sender As Object, e As KeyEventArgs) Handles txtMenuBusca.KeyUp
    '    If txtMenuBusca.Text.Trim = "" Then Exit Sub
    '    If e.KeyCode = Keys.Down Or e.KeyCode = Keys.Up Then
    '        GridTodos.Focus()
    '        e.Handled = True
    '        Exit Sub
    '    End If
    '    If e.KeyCode = Keys.Left Or e.KeyCode = Keys.Right Then Exit Sub
    '    ActualizaGrid(txtMenuBusca.Text)
    'End Sub



    Private Sub frmCatFamilias_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Inicio.CONSTR = "" Then
            Inicio.CONSTR = con
        End If
        OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
        ActualizaGrid("")
        GridTodos.Columns(1).Width = GridTodos.Width - 20
        GridTodos.Columns(0).Visible = False
        txtMenuBusca.Focus()
    End Sub
    Private Sub GridTodos_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles GridTodos.CellEnter
        CargaForm(GridTodos.Rows(GridTodos.CurrentRow.Index).Cells(0).Value, UCase(TablaBd))
    End Sub
    Private Sub GridTodos_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles GridTodos.KeyPress
        If Asc(e.KeyChar) = 8 Then
            txtMenuBusca.Focus()
            e.Handled = True
        End If
    End Sub
    Private Sub CargaForm(ByVal idClave As Integer, ByVal NomTabla As String)
        Try
            Select Case NomTabla
                Case UCase(TablaBd)
                    Familia = New FamiliasClass(idClave)
                    With Familia
                        txtidFamilia.Text = .IdFamilia
                        If .Existe Then
                            txtNomFamilia.Text = .NomFamilia
                            txtidDivision.Text = .idDivision
                            txtNomDivision.Text = .NomDivision
                            If OpcionForma = TipoOpcionForma.tOpcInsertar Then
                                Status("La Familia Con idDivision: " & .IdFamilia & _
                                       " Ya existe!!!, Favor de modificarlo para poder dar de alta a otra Familia", Me, Err:=True)
                                MessageBox.Show("La Familia con id: " & .IdFamilia & " YA EXISTE!!!, Favor de Cambiarlo y volver a intentarlo",
                                                "La Familia con id: " & .IdFamilia & " YA EXISTE!!!", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            End If
                        Else 'No existe 
                            If OpcionForma = TipoOpcionForma.tOpcInsertar Then
                                'For Each Ct As Control In GrupoCaptura.Controls
                                '    If Not Ct Is txtidFamilia And Not Ct Is cmdBuscaClave Then
                                '        Ct.Enabled = True
                                '    Else
                                '        Ct.Enabled = False
                                '    End If
                                '    If TypeOf (Ct) Is TextBox Then
                                '        If Not Ct Is txtidFamilia Then
                                '            TryCast(Ct, TextBox).Text = ""
                                '        End If
                                '    End If
                                'Next
                                ActivaCampos(True, TipoOpcActivaCampos.tOpcINICIALIZA)
                                ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)
                                btnMnuOk.Enabled = True
                                txtNomFamilia.Focus()
                                Status("Listo para Guardar la nueva Familia con id: " & .idDivision, Me)
                            End If
                        End If
                    End With
                Case UCase(TablaBd2)
                    'aqui()
                    Division = New DivisionClass(idClave)
                    With Division
                        txtidDivision.Text = .idDivision
                        If .Existe Then
                            txtNomDivision.Text = .NomDivision
                        Else
                            MsgBox("La Divisi�n con id: " & txtidDivision.Text & " No Existe")
                        End If
                    End With

            End Select



        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try

    End Sub
    Private Function Validar() As Boolean
        Validar = True
        If Not Inicio.ValidandoCampo(Trim(txtidFamilia.Text), TipoDato.TdNumerico) Then
            If Not bEsIdentidad Then
                MsgBox(MENSAJE_CLAVE, vbInformation, "Aviso" & Me.Text)
                If txtidFamilia.Enabled Then
                    txtidFamilia.Focus()
                    txtidFamilia.SelectAll()
                End If
                Validar = False
            End If
        ElseIf Not Inicio.ValidandoCampo(Trim(txtNomFamilia.Text), TipoDato.TdCadena) Then
            MsgBox("El Nombre de la Familia No Puede Ser Vacio", vbInformation, "Aviso" & Me.Text)
            If txtNomFamilia.Enabled Then
                txtNomFamilia.Focus()
                txtNomFamilia.SelectAll()
            End If
            Validar = False
        ElseIf Not Inicio.ValidandoCampo(Trim(txtidDivision.Text), TipoDato.TdNumerico) Then
            MsgBox("La Division de la Familia no Puede ser Vacio", vbInformation, "Aviso" & Me.Text)
            If txtidDivision.Enabled Then
                txtidDivision.Focus()
                txtidDivision.SelectAll()
            End If
            Validar = False
        End If
    End Function

    'Private Sub txtidFamilia_EnabledChanged(sender As Object, e As EventArgs) Handles txtidFamilia.EnabledChanged
    '    cmdBuscaClave.Enabled = txtidFamilia.Enabled
    'End Sub
    'CONTROLES
    Private Sub txtidFamilia_KeyDown(sender As Object, e As KeyEventArgs) Handles txtidFamilia.KeyDown
        If e.KeyCode = Keys.F3 Then
            cmdBuscaClave_Click(sender, e)
        End If

    End Sub
    Private Sub txtidFamilia_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtidFamilia.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If txtidFamilia.Text.Trim <> "" Then
                CargaForm(txtidFamilia.Text, UCase(TablaBd))
            End If
            e.Handled = True
        End If
    End Sub
    Private Sub txtNomFamilia_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtNomFamilia.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            'btnMnuOk_Click(sender, e)
            txtidDivision.Focus()
            txtidDivision.SelectAll()
        End If
    End Sub

    Private Sub cmdBuscaClave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBuscaClave.Click
        Dim idEst As String = DespliegaGrid(UCase(TablaBd), Inicio.CONSTR, CampoLLave)
        If idEst <> "" Then CargaForm(idEst, UCase(TablaBd))
    End Sub

    Private Sub txtNomFamilia_MouseDown(sender As Object, e As MouseEventArgs) Handles txtNomFamilia.MouseDown
        txtNomFamilia.Focus()
        txtNomFamilia.SelectAll()
    End Sub

    Private Sub txtidDivision_EnabledChanged(sender As Object, e As EventArgs) Handles txtidDivision.EnabledChanged
        cmdBuscaClaveDiv.Enabled = txtidDivision.Enabled
    End Sub

    Private Sub txtidDivision_KeyDown(sender As Object, e As KeyEventArgs) Handles txtidDivision.KeyDown
        If e.KeyCode = Keys.F3 Then
            cmdBuscaClaveDiv_Click(sender, e)
        End If
    End Sub

    Private Sub txtidDivision_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtidDivision.KeyPress
        '    btnMnuOk_Click(sender, e)
        '
        'If Asc(e.KeyChar) = 13 Then
        '    If txtidDivision.Text.Trim <> "" Then
        '        CargaForm(txtidDivision.Text, TablaBd2)
        '        txtNomDivision.Focus()
        '        txtNomDivision.SelectAll()
        '    End If
        '    e.Handled = True
        'End If
        If txtidDivision.Text.Length = 0 Then
            txtidDivision.Focus()
        Else
            If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
                Salida = True
                CargaForm(txtidDivision.Text, UCase(TablaBd2))
                Salida = False
                txtNomDivision.Focus()
                txtNomDivision.SelectAll()
            End If
        End If
    End Sub

    Private Sub txtidDivision_Leave(sender As Object, e As EventArgs) Handles txtidDivision.Leave
        If Salida Then Exit Sub
        If txtidDivision.Text.Length = 0 Then
            txtidDivision.Focus()
            txtidDivision.SelectAll()
        Else
            If txtidDivision.Enabled Then
                CargaForm(txtidDivision.Text, UCase(TablaBd2))
                txtNomDivision.Focus()
                txtNomDivision.SelectAll()
            End If
        End If
    End Sub

    Private Sub txtidDivision_MouseDown(sender As Object, e As MouseEventArgs) Handles txtidDivision.MouseDown
        txtidDivision.Focus()
        txtidDivision.SelectAll()
    End Sub

    Private Sub cmdBuscaClaveDiv_Click(sender As Object, e As EventArgs) Handles cmdBuscaClaveDiv.Click
        txtidDivision.Text = DespliegaGrid(UCase(TablaBd2), Inicio.CONSTR, CampoLLave)
        txtidDivision_Leave(sender, e)
    End Sub

    Private Sub cmdCancelar_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        With TryCast(sender, Button)
            If .Enabled Then
                Me.CancelButton = sender
            End If
        End With
    End Sub
    Private Sub cmdTerminar_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        With TryCast(sender, Button)
            If .Enabled Then
                Me.CancelButton = sender
            End If
        End With
    End Sub

    Private Sub btnMnuOk_Click(sender As Object, e As EventArgs) Handles btnMnuOk.Click
        Dim CadCam As String = ""
        Try
            Salida = True
            If Not Validar() Then Exit Sub
            If GridTodos.RowCount = 0 Then
                Familia = New FamiliasClass(0)
            End If
            With Familia
                If OpcionForma = TipoOpcionForma.tOpcModificar Then

                    CadCam = .GetCambios(txtNomFamilia.Text, txtidDivision.Text)
                    If CadCam <> "" Then
                        'If MessageBox.Show("Esta completamente Seguro que Desea guardar los Cambios Realizados al Estado Con id: " & .IDMARCA & vbNewLine &
                        '                CadCam, "Confirma que desea guardar los Cambios Realizados???", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then Exit Sub
                        Status("Guardando Modificaciones de la Familia Con id: " & .idDivision, Me, 1, 2)
                        .Guardar(txtNomFamilia.Text, txtidDivision.Text, bEsIdentidad, txtNomDivision.Text)
                        Status("Familia con Id: " & .idDivision & " Modificado Con Exito!!!", Me, 2, 2)
                        MessageBox.Show("Familia con Id: " & .idDivision & " Modificada con exito!!!",
                                        "Accion terminada con exito!!!", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
                        ActualizaGrid(txtMenuBusca.Text)
                    Else
                        Status("No se detectaron cambios para realizar!!!", Me, Err:=True)
                    End If
                ElseIf OpcionForma = TipoOpcionForma.tOpcInsertar Then
                    .Existe = False
                    .NomDivision = txtNomFamilia.Text
                    If Not bEsIdentidad Then
                        .idDivision = txtidFamilia.Text
                    End If
                    Status("Guardando Nueva Familia: " & txtNomFamilia.Text & ". . .", Me, 1, 2)
                    .Guardar(txtNomFamilia.Text, txtidDivision.Text, bEsIdentidad, txtNomDivision.Text)
                    Status("Familia: " & txtNomFamilia.Text & " Guardado con Exito!!!", Me, 2, 2)
                    MessageBox.Show("Nueva Familia: " & txtNomFamilia.Text & " Guardado con exito!!!",
                                    "Accion terminada con exito!!!", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
                    ActualizaGrid(txtMenuBusca.Text)

                End If
            End With
            Salida = False
        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try
    End Sub


    Private Sub btnMnuCancelar_Click(sender As Object, e As EventArgs) Handles btnMnuCancelar.Click
        If txtidFamilia.Text <> "" Then
            CargaForm(txtidFamilia.Text, UCase(TablaBd))
        End If
        OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
    End Sub

    Private Sub btnMnuSalir_Click(sender As Object, e As EventArgs) Handles btnMnuSalir.Click
        Me.Close()
    End Sub

    Private Sub txtMenuBusca_TextChanged(sender As Object, e As EventArgs) Handles txtMenuBusca.TextChanged
        ActualizaGrid(txtMenuBusca.Text)
    End Sub

    

    Private Sub txtidDivision_TextChanged(sender As Object, e As EventArgs) Handles txtidDivision.TextChanged

    End Sub

    Private Sub txtidFamilia_TextChanged(sender As Object, e As EventArgs) Handles txtidFamilia.TextChanged

    End Sub
End Class
