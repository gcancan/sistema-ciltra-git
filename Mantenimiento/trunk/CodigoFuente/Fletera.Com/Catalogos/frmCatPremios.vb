'Fecha Creacion: 
'Fecha Modificacion : 
Public Class frmCatPremios
    Inherits System.Windows.Forms.Form
    Private _Tag As String
    Dim ConsecutivoAudita As Integer
    Dim indice As Integer = 0
    Dim ArraySql() As String
    Dim Existe As String
    Dim TablaBd As String = "CatPremios"
    Dim CampoLLave As String = "IdPremio"
    Dim DsLista As New DataSet
    Private Audit As New AuditaClass
    Protected DS As New DataSet
    Dim sPosibleError As String
    Dim Salida As Boolean
    Dim OpcionForma As TipoOpcionForma
    Friend WithEvents btnMnuOk As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuCancelar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuSalir As System.Windows.Forms.ToolStripButton
    Dim bEsIdenti As Boolean = True

    Sub New(ByVal vTag As String)
        InitializeComponent()
        _Tag = vTag
    End Sub




#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents ToolStripMenu As System.Windows.Forms.ToolStrip
    Friend WithEvents txtMenuBusca As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents btnMnuAltas As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuModificar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuEliminar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuReporte As System.Windows.Forms.ToolStripButton
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtValorPesos As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtValorPuntos As System.Windows.Forms.TextBox
    Friend WithEvents LtitNomUser As System.Windows.Forms.Label
    Friend WithEvents txtDescripcion As System.Windows.Forms.TextBox
    Friend WithEvents cmdBuscaClave As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtIdPremio As System.Windows.Forms.TextBox
    Friend WithEvents GridTodos As System.Windows.Forms.DataGridView
    Public WithEvents MeStatus1 As ControlStatus.MeStatus
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCatPremios))
        Me.ToolStripMenu = New System.Windows.Forms.ToolStrip()
        Me.txtMenuBusca = New System.Windows.Forms.ToolStripTextBox()
        Me.btnMnuAltas = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuModificar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuEliminar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuReporte = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuOk = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuCancelar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuSalir = New System.Windows.Forms.ToolStripButton()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtValorPesos = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtValorPuntos = New System.Windows.Forms.TextBox()
        Me.LtitNomUser = New System.Windows.Forms.Label()
        Me.txtDescripcion = New System.Windows.Forms.TextBox()
        Me.cmdBuscaClave = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtIdPremio = New System.Windows.Forms.TextBox()
        Me.GridTodos = New System.Windows.Forms.DataGridView()
        Me.MeStatus1 = New ControlStatus.MeStatus()
        Me.ToolStripMenu.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.GridTodos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ToolStripMenu
        '
        Me.ToolStripMenu.AutoSize = False
        Me.ToolStripMenu.BackColor = System.Drawing.Color.Silver
        Me.ToolStripMenu.Dock = System.Windows.Forms.DockStyle.None
        Me.ToolStripMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.txtMenuBusca, Me.btnMnuAltas, Me.btnMnuModificar, Me.btnMnuEliminar, Me.btnMnuReporte, Me.btnMnuOk, Me.btnMnuCancelar, Me.btnMnuSalir})
        Me.ToolStripMenu.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow
        Me.ToolStripMenu.Location = New System.Drawing.Point(3, 0)
        Me.ToolStripMenu.Name = "ToolStripMenu"
        Me.ToolStripMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.ToolStripMenu.Size = New System.Drawing.Size(706, 42)
        Me.ToolStripMenu.TabIndex = 75
        Me.ToolStripMenu.Text = "ToolStrip1"
        '
        'txtMenuBusca
        '
        Me.txtMenuBusca.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.txtMenuBusca.Name = "txtMenuBusca"
        Me.txtMenuBusca.Size = New System.Drawing.Size(150, 42)
        Me.txtMenuBusca.ToolTipText = "Filtra Listado"
        '
        'btnMnuAltas
        '
        Me.btnMnuAltas.ForeColor = System.Drawing.Color.Red
        Me.btnMnuAltas.Image = CType(resources.GetObject("btnMnuAltas.Image"), System.Drawing.Image)
        Me.btnMnuAltas.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuAltas.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuAltas.Name = "btnMnuAltas"
        Me.btnMnuAltas.Size = New System.Drawing.Size(46, 39)
        Me.btnMnuAltas.Text = "&ALTAS"
        Me.btnMnuAltas.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnMnuAltas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuModificar
        '
        Me.btnMnuModificar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuModificar.Image = CType(resources.GetObject("btnMnuModificar.Image"), System.Drawing.Image)
        Me.btnMnuModificar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuModificar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuModificar.Name = "btnMnuModificar"
        Me.btnMnuModificar.Size = New System.Drawing.Size(74, 39)
        Me.btnMnuModificar.Text = "&MODIFICAR"
        Me.btnMnuModificar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnMnuModificar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuEliminar
        '
        Me.btnMnuEliminar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuEliminar.Image = CType(resources.GetObject("btnMnuEliminar.Image"), System.Drawing.Image)
        Me.btnMnuEliminar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuEliminar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuEliminar.Name = "btnMnuEliminar"
        Me.btnMnuEliminar.Size = New System.Drawing.Size(64, 39)
        Me.btnMnuEliminar.Text = "&ELIMINAR"
        Me.btnMnuEliminar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuReporte
        '
        Me.btnMnuReporte.ForeColor = System.Drawing.Color.Red
        Me.btnMnuReporte.Image = CType(resources.GetObject("btnMnuReporte.Image"), System.Drawing.Image)
        Me.btnMnuReporte.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuReporte.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuReporte.Name = "btnMnuReporte"
        Me.btnMnuReporte.Size = New System.Drawing.Size(60, 39)
        Me.btnMnuReporte.Text = "&REPORTE"
        Me.btnMnuReporte.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuOk
        '
        Me.btnMnuOk.ForeColor = System.Drawing.Color.Red
        Me.btnMnuOk.Image = Global.Fletera.Com.My.Resources.Resources.Save1
        Me.btnMnuOk.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuOk.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuOk.Name = "btnMnuOk"
        Me.btnMnuOk.Size = New System.Drawing.Size(56, 39)
        Me.btnMnuOk.Text = "&GRABAR"
        Me.btnMnuOk.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuCancelar
        '
        Me.btnMnuCancelar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuCancelar.Image = Global.Fletera.Com.My.Resources.Resources.signos_f_059
        Me.btnMnuCancelar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuCancelar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuCancelar.Name = "btnMnuCancelar"
        Me.btnMnuCancelar.Size = New System.Drawing.Size(71, 39)
        Me.btnMnuCancelar.Text = "&CANCELAR"
        Me.btnMnuCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuSalir
        '
        Me.btnMnuSalir.ForeColor = System.Drawing.Color.Red
        Me.btnMnuSalir.Image = Global.Fletera.Com.My.Resources.Resources.Salida2
        Me.btnMnuSalir.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuSalir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuSalir.Name = "btnMnuSalir"
        Me.btnMnuSalir.Size = New System.Drawing.Size(41, 39)
        Me.btnMnuSalir.Text = "&SALIR"
        Me.btnMnuSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.txtValorPesos)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.txtValorPuntos)
        Me.GroupBox2.Controls.Add(Me.LtitNomUser)
        Me.GroupBox2.Controls.Add(Me.txtDescripcion)
        Me.GroupBox2.Controls.Add(Me.cmdBuscaClave)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.txtIdPremio)
        Me.GroupBox2.Location = New System.Drawing.Point(171, 45)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(513, 209)
        Me.GroupBox2.TabIndex = 76
        Me.GroupBox2.TabStop = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.ForeColor = System.Drawing.Color.Black
        Me.Label2.Location = New System.Drawing.Point(9, 108)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(66, 13)
        Me.Label2.TabIndex = 83
        Me.Label2.Text = "Valor Pesos:"
        '
        'txtValorPesos
        '
        Me.txtValorPesos.Location = New System.Drawing.Point(79, 105)
        Me.txtValorPesos.Name = "txtValorPesos"
        Me.txtValorPesos.Size = New System.Drawing.Size(132, 20)
        Me.txtValorPesos.TabIndex = 6
        Me.txtValorPesos.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(9, 82)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(70, 13)
        Me.Label1.TabIndex = 82
        Me.Label1.Text = "Valor Puntos:"
        '
        'txtValorPuntos
        '
        Me.txtValorPuntos.Location = New System.Drawing.Point(79, 79)
        Me.txtValorPuntos.Name = "txtValorPuntos"
        Me.txtValorPuntos.Size = New System.Drawing.Size(132, 20)
        Me.txtValorPuntos.TabIndex = 5
        Me.txtValorPuntos.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'LtitNomUser
        '
        Me.LtitNomUser.AutoSize = True
        Me.LtitNomUser.ForeColor = System.Drawing.Color.Black
        Me.LtitNomUser.Location = New System.Drawing.Point(10, 53)
        Me.LtitNomUser.Name = "LtitNomUser"
        Me.LtitNomUser.Size = New System.Drawing.Size(66, 13)
        Me.LtitNomUser.TabIndex = 81
        Me.LtitNomUser.Text = "Descripci�n:"
        '
        'txtDescripcion
        '
        Me.txtDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDescripcion.Location = New System.Drawing.Point(79, 53)
        Me.txtDescripcion.MaxLength = 150
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(414, 20)
        Me.txtDescripcion.TabIndex = 4
        '
        'cmdBuscaClave
        '
        Me.cmdBuscaClave.Image = CType(resources.GetObject("cmdBuscaClave.Image"), System.Drawing.Image)
        Me.cmdBuscaClave.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdBuscaClave.Location = New System.Drawing.Point(179, 15)
        Me.cmdBuscaClave.Name = "cmdBuscaClave"
        Me.cmdBuscaClave.Size = New System.Drawing.Size(32, 32)
        Me.cmdBuscaClave.TabIndex = 3
        Me.cmdBuscaClave.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ForeColor = System.Drawing.Color.Black
        Me.Label3.Location = New System.Drawing.Point(9, 30)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(43, 13)
        Me.Label3.TabIndex = 80
        Me.Label3.Text = "C�digo:"
        '
        'txtIdPremio
        '
        Me.txtIdPremio.Enabled = False
        Me.txtIdPremio.Location = New System.Drawing.Point(79, 27)
        Me.txtIdPremio.Name = "txtIdPremio"
        Me.txtIdPremio.Size = New System.Drawing.Size(94, 20)
        Me.txtIdPremio.TabIndex = 2
        '
        'GridTodos
        '
        Me.GridTodos.AllowUserToAddRows = False
        Me.GridTodos.AllowUserToDeleteRows = False
        Me.GridTodos.AllowUserToResizeRows = False
        Me.GridTodos.BackgroundColor = System.Drawing.Color.White
        Me.GridTodos.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.GridTodos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.GridTodos.Location = New System.Drawing.Point(3, 45)
        Me.GridTodos.MultiSelect = False
        Me.GridTodos.Name = "GridTodos"
        Me.GridTodos.ReadOnly = True
        Me.GridTodos.RowHeadersVisible = False
        Me.GridTodos.Size = New System.Drawing.Size(162, 209)
        Me.GridTodos.TabIndex = 1
        '
        'MeStatus1
        '
        Me.MeStatus1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.MeStatus1.Location = New System.Drawing.Point(0, 262)
        Me.MeStatus1.Margin = New System.Windows.Forms.Padding(4)
        Me.MeStatus1.Name = "MeStatus1"
        Me.MeStatus1.Size = New System.Drawing.Size(692, 30)
        Me.MeStatus1.TabIndex = 118
        '
        'frmCatPremios
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(692, 292)
        Me.Controls.Add(Me.MeStatus1)
        Me.Controls.Add(Me.GridTodos)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.ToolStripMenu)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmCatPremios"
        Me.Text = "Premios"
        Me.ToolStripMenu.ResumeLayout(False)
        Me.ToolStripMenu.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.GridTodos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "Funciones y Procedimientos"
    Private Sub LimpiaVariables()
        ConsecutivoAudita = 0
        indice = 0
        'OpcionForma = ""
        Existe = False
        'RegAModif = ""
        'RegModif = ""
        SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")

    End Sub

    Private Sub LimpiaCampos()
        txtIdPremio.Text = ""
        txtDescripcion.Text = ""
        txtValorPuntos.Text = ""
        txtValorPesos.Text = ""

        LimpiaVariables()
    End Sub

    Private Sub ActivaCampos(ByVal valor As Boolean, ByVal vOpcion As TipoOpcActivaCampos)
        If vOpcion = TipoOpcActivaCampos.tOpcDESHABTODOS Then
            txtIdPremio.Enabled = False
            txtDescripcion.Enabled = False
            txtValorPuntos.Enabled = False
            txtValorPesos.Enabled = False
        ElseIf vOpcion = TipoOpcActivaCampos.tOpcINICIALIZA Then
            txtIdPremio.Enabled = Not valor
            txtDescripcion.Enabled = valor
            txtValorPuntos.Enabled = valor
            txtValorPesos.Enabled = valor

        End If
    End Sub

    Private Sub ActivaBotones(ByVal valor As Boolean, Optional ByVal vOpcion As TipoOpcActivaBoton = TipoOpcActivaBoton.tOpcInicializa)
        'Si es Verdadero Se activan Ok,Cancelar y Buscar y Terminar Se desactiva
        If vOpcion = TipoOpcActivaBoton.tOpcConsulta Then
            btnMnuOk.Enabled = False
            btnMnuCancelar.Enabled = valor
            'cmdBuscaClave.Enabled = valor
            btnMnuSalir.Enabled = Not valor

            btnMnuAltas.Enabled = Not valor
            btnMnuModificar.Enabled = Not valor
            btnMnuEliminar.Enabled = Not valor
            btnMnuReporte.Enabled = Not valor
        ElseIf vOpcion = TipoOpcActivaBoton.tOpcInicializa Then
            btnMnuOk.Enabled = valor
            btnMnuCancelar.Enabled = valor
            'cmdBuscaClave.Enabled = valor
            btnMnuSalir.Enabled = Not valor

            btnMnuAltas.Enabled = Not valor
            btnMnuModificar.Enabled = Not valor
            btnMnuEliminar.Enabled = Not valor
            btnMnuReporte.Enabled = Not valor
        ElseIf vOpcion = TipoOpcActivaBoton.tOpcInsertar Then
            btnMnuOk.Enabled = False
            btnMnuCancelar.Enabled = valor
            'cmdBuscaClave.Enabled = False
            btnMnuSalir.Enabled = Not valor

            btnMnuAltas.Enabled = Not valor
            btnMnuModificar.Enabled = Not valor
            btnMnuEliminar.Enabled = Not valor
            btnMnuReporte.Enabled = Not valor
        End If
    End Sub

    Private Sub Cancelar()
        ActivaCampos(False, TipoOpcActivaCampos.tOpcDESHABTODOS)
        ActivaBotones(False)
        LimpiaCampos()
        LlenaLista()
        ToolStripMenu.Enabled = True
        GridTodos.Enabled = True
    End Sub

    Private Function LlenaDatos(ByVal cCve As String, ByVal dbtabla As String, ByVal Campo As String, ByVal TipoCampo As TipoDato) As String
        Try
            DS = Inicio.ChecaClave(txtIdPremio.Text, Inicio.CONSTR, TipoConexion.TcSQL, dbtabla, Campo, TipoCampo, Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS)
            sPosibleError = GetSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")
            If sPosibleError <> "ERROR" Or sPosibleError = Nothing Then
                SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")
                If Not DS Is Nothing Then
                    If DS.Tables(dbtabla).DefaultView.Count > 0 Then
                        'txtsCveSitProv.Text = DS.Tables(dbtabla).DefaultView.Item(0).Item("sNomDivision")
                        txtDescripcion.Text = DS.Tables(dbtabla).DefaultView.Item(0).Item("Descripcion")
                        txtValorPuntos.Text = DS.Tables(dbtabla).DefaultView.Item(0).Item("ValorPuntos")
                        txtValorPesos.Text = DS.Tables(dbtabla).DefaultView.Item(0).Item("ValorPesos")
                        'RegAModif = LLenaRegistro()
                        Audit.CargaAudita()
                        LlenaDatos = KEY_RCORRECTO
                    Else
                        LlenaDatos = KEY_RINCORRECTO
                    End If
                Else
                    LlenaDatos = KEY_RINCORRECTO
                End If
            Else
                SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")
                LlenaDatos = KEY_RERROR
            End If
        Catch ex As Exception
            SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")
            LlenaDatos = KEY_RERROR
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, Me.Text)
        End Try
    End Function

    Private Function Validar() As Boolean
        Validar = True
        If Not Inicio.ValidandoCampo(Trim(txtIdPremio.Text), TipoDato.TdCadena) Then
            If Not bEsIdenti Then
                MsgBox(MENSAJE_CLAVE, vbInformation, "Aviso" & Me.Text)
                If txtIdPremio.Enabled Then
                    txtIdPremio.Focus()
                    txtIdPremio.SelectAll()
                End If
                Validar = False
            End If
        ElseIf Not Inicio.ValidandoCampo(Trim(txtDescripcion.Text), TipoDato.TdCadena) Then
            MsgBox("La Descripcion No Puede Ser Vacia", vbInformation, "Aviso" & Me.Text)
            If txtDescripcion.Enabled Then
                txtDescripcion.Focus()
                txtDescripcion.SelectAll()
            End If
            Validar = False
        End If
    End Function
#End Region

#Region "Listado"
    Private Sub GridTodos_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles GridTodos.CellEnter
        ActualizaLista()
    End Sub

    Private Sub txtMenuBusca_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtMenuBusca.KeyPress

    End Sub
    Private Sub txtMenuBusca_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtMenuBusca.TextChanged
        'filtrar 
        DsLista.Tables(TablaBd).DefaultView.RowFilter = Nothing
        DsLista.Tables(TablaBd).DefaultView.RowFilter = "Descripcion Like '*" & txtMenuBusca.Text & "*'"
        Status(DsLista.Tables(TablaBd).DefaultView.Count & " Areas Encontradas. . . ", Me)
        ActualizaLista()
    End Sub

    Private Sub ActualizaLista()
        If GridTodos.Rows.Count > 0 Then
            If GridTodos.CurrentRow Is Nothing Then
                Exit Sub
            End If
            'txtsIdConcepto.Text = lstTodos.SelectedValue
            txtIdPremio.Text = GridTodos.Rows(GridTodos.CurrentRow.Index).Cells(0).Value & ""
            'With GridTodos
            '    CargaForm(.Rows(.CurrentRow.Index).Cells(1).Value & "")
            'End With
            Existe = LlenaDatos(txtIdPremio.Text, TablaBd, CampoLLave, TipoDato.TdCadena)
        End If
        'LlenaDatosClaves()
    End Sub

    Private Function LlenaLista() As Boolean
        LlenaLista = False
        Try
            DsLista = Inicio.LlenaCombos(CampoLLave, "Descripcion", TablaBd, _
             TipoConexion.TcSQL, Inicio.CONSTR, Process.GetCurrentProcess.ProcessName, _
             KEY_CHECAERROR, KEY_ESTATUS, "")
            If Not DsLista Is Nothing Then
                If DsLista.Tables(TablaBd).DefaultView.Count > 0 Then
                    GridTodos.DataSource = DsLista.Tables(TablaBd)
                    Status(GridTodos.Rows.Count & " Premios Encontrados. . . ", Me)
                    LlenaLista = True

                    GridTodos.Columns(1).Width = GridTodos.Width - 20
                    GridTodos.Columns(0).Visible = False
                End If
            Else
                GridTodos.DataSource = Nothing
            End If
        Catch ex As Exception

        End Try
    End Function

#End Region



    Private Sub frmCatPremios_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FEspera.Show("Cargando Catalogo de Premios. . .")
        ToolStripMenu.Enabled = True
        ActivaCampos(False, TipoOpcActivaCampos.tOpcDESHABTODOS)
        If LlenaLista() Then
            GridTodos.Columns(1).Width = GridTodos.Width - 20
            GridTodos.Columns(0).Visible = False
        End If

        LimpiaCampos()
        ActivaBotones(False)
        Audit = New AuditaClass({
                                 New ContenidoObjClass(txtIdPremio, CampoLLave, , True),
                                 New ContenidoObjClass(txtDescripcion, "Descripcion", "Descripcion Area"),
                                 New ContenidoObjClass(txtValorPuntos, "ValorPuntos", "Valor Puntos"),
                                 New ContenidoObjClass(txtValorPesos, "ValorPesos", "Valor Pesos")
                             }, TablaBd)
        FEspera.Close()

    End Sub

#Region "Codigo Controles"

    Private Sub txtIdPremio_EnabledChanged(sender As Object, e As EventArgs) Handles txtIdPremio.EnabledChanged
        cmdBuscaClave.Enabled = txtIdPremio.Enabled
    End Sub
    Private Sub txtIdPremio_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtIdPremio.KeyDown
        If e.KeyCode = Keys.F3 Then
            cmdBuscaClave_Click(sender, e)
        End If
    End Sub

    Private Sub txtIdPremio_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtIdPremio.KeyPress
        If txtIdPremio.Text.Length = 0 Then
            txtIdPremio.Focus()
        Else
            If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
                Salida = True
                ''DS = Inicio.ChecaClave(txtsCveSitProv.Text, Inicio.ConStrProg, TipoConexion.TcProgress, "JurCatMotivosDon", "sCveMotDon", TipoDato.TdCadena)
                Existe = LlenaDatos(txtIdPremio.Text, TablaBd, CampoLLave, TipoDato.TdCadena)
                If Existe = KEY_RINCORRECTO Then
                    'If MsgBox("!! Esta seguro que desea Crear El Motivo de Donaci�n : " & txtsCveSitProv.Text & "? !!", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                    If MsgBox("!! El Premio : " & txtIdPremio.Text & " no existe desea Crearla ? !!", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                        Cancelar()
                        Exit Sub
                    End If
                ElseIf Existe = KEY_RCORRECTO Then
                    MsgBox("!! El Premio : " & txtIdPremio.Text & " - " & txtDescripcion.Text & " Ya Existe", vbInformation, "Aviso" & Me.Text)
                    'Cancelar()
                    LimpiaCampos()
                    ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)
                    txtIdPremio.Focus()
                    txtIdPremio.SelectAll()
                    Exit Sub

                ElseIf Existe = KEY_RERROR Then
                    txtIdPremio.Focus()
                    txtIdPremio.SelectAll()
                    Exit Sub
                End If
                ActivaCampos(True, TipoOpcActivaCampos.tOpcINICIALIZA)
                If OpcionForma = TipoOpcionForma.tOpcConsultar Then
                    ActivaBotones(True, TipoOpcActivaBoton.tOpcConsulta)
                Else
                    ActivaBotones(True)
                End If
                txtDescripcion.Focus()
                txtDescripcion.SelectAll()
                Salida = False
            End If
        End If
    End Sub

    Private Sub txtIdPremio_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtIdPremio.Leave
        'If Me.ActiveControl Is Nothing Then Me.ActiveControl = cmdTerminar
        'If Salida Or Me.ActiveControl.Name = cmdCancelar.Name Or Me.ActiveControl.Name = cmdTerminar.Name Then Exit Sub
        'If Salida Or Me.ActiveControl.Name = cmdTerminar.Name Then Exit Sub
        If Salida Then Exit Sub
        If txtIdPremio.Text.Length = 0 Then
            txtIdPremio.Focus()
            txtIdPremio.SelectAll()
        Else
            If txtIdPremio.Enabled Then
                Existe = LlenaDatos(txtIdPremio.Text, TablaBd, CampoLLave, TipoDato.TdCadena)
                If Existe = KEY_RINCORRECTO Then
                    'If MsgBox("!! Esta seguro que desea Crear la Restricci�n : " & txtsCveSitProv.Text & "? !!", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                    If MsgBox("!! El Premio : " & txtIdPremio.Text & " no existe desea Crearla ? !!", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                        Cancelar()
                        Exit Sub
                    End If
                ElseIf Existe = KEY_RCORRECTO Then
                    'If OpcionForma = TipoOpcionForma.tOpcConsultar Or OpcionForma = TipoOpcionForma.tOpcEliminar Then

                    If OpcionForma = TipoOpcionForma.tOpcInsertar Then
                        MsgBox("!! El Premio : " & txtIdPremio.Text & " - " & txtDescripcion.Text & " Ya Existe", vbInformation, "Aviso" & Me.Text)
                        LimpiaCampos()
                        ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)
                        txtIdPremio.Focus()
                        txtIdPremio.SelectAll()
                        Exit Sub
                    End If
                ElseIf Existe = KEY_RERROR Then
                    txtIdPremio.Focus()
                    txtIdPremio.SelectAll()
                    Exit Sub
                End If
                'Salida = True
                ActivaCampos(True, TipoOpcActivaCampos.tOpcINICIALIZA)
                If OpcionForma = TipoOpcionForma.tOpcConsultar Then
                    ActivaBotones(True, TipoOpcActivaBoton.tOpcConsulta)
                Else
                    ActivaBotones(True)
                End If
                txtDescripcion.Focus()
                txtDescripcion.SelectAll()
            End If
        End If
    End Sub


    Private Sub txtDescripcion_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtDescripcion.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            If bEsIdenti Then
                'Checar
                If txtDescripcion.Text.Length = 0 Then
                    txtDescripcion.Focus()
                Else
                    Salida = True
                    ActivaCampos(True, TipoOpcActivaCampos.tOpcINICIALIZA)
                    If OpcionForma = TipoOpcionForma.tOpcConsultar Then
                        ActivaBotones(True, TipoOpcActivaBoton.tOpcConsulta)
                    Else
                        ActivaBotones(True)
                    End If
                    txtValorPuntos.Focus()
                    txtValorPuntos.SelectAll()
                    Salida = False
                End If

            Else
                txtValorPuntos.Focus()
                txtValorPuntos.SelectAll()

            End If
        End If
    End Sub

    Private Sub txtDescripcion_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles txtDescripcion.MouseDown
        txtDescripcion.Focus()
        txtDescripcion.SelectAll()
    End Sub

    Private Sub txtValorPuntos_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtValorPuntos.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            txtValorPesos.Focus()
            txtValorPesos.SelectAll()
        End If

    End Sub

    Private Sub txtValorPuntos_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles txtValorPuntos.MouseDown
        txtValorPuntos.Focus()
        txtValorPuntos.SelectAll()
    End Sub

    Private Sub txtValorPesos_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtValorPesos.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            btnMnuOk_Click(sender, e)
        End If
    End Sub

    Private Sub txtValorPesos_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles txtValorPesos.MouseDown
        txtValorPesos.Focus()
        txtValorPesos.SelectAll()
    End Sub

#End Region

#Region "Codigo Botones"
    'Handles cmdCancelar.EnabledChanged, cmdTerminar.EnabledChanged
    '        With TryCast(sender, Button)
    '            If .Enabled Then
    '                Me.CancelButton = sender
    '            End If
    '        End With
    '    End Sub

    Private Sub cmdCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub cmdTerminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub cmdBuscaClave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBuscaClave.Click
        txtIdPremio.Text = DespliegaGrid(TablaBd, Inicio.CONSTR, CampoLLave)
        txtIdPremio_Leave(sender, e)
    End Sub

    Private Sub cmdOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

#End Region

#Region "Codigo Barra Menu"
    Private Sub OpcionMnuTools(ByVal vOpcion As TipoOpcionForma)
        'ToolStripMenu.Enabled = False
        GridTodos.Enabled = False
        If vOpcion = TipoOpcionForma.tOpcInsertar Then
            'ALTAS 
            'Cancelar()
            'PERMISOS - EN CASO DE NECESITARLO
            'If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Alta) = False Then
            '    MessageBox.Show("No tiene los permisos Necesarios para poder dar de Alta en el Catalogo de Areas!!!",
            '                    "Acceso Denegado a Alta de Areas", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            '    Cancelar()
            '    Exit Sub
            'End If
            'comboMnu.SelectedIndex = 0
            OpcionForma = TipoOpcionForma.tOpcInsertar
            ActivaCampos(False, TipoOpcActivaCampos.tOpcINICIALIZA)
            ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)
            LimpiaCampos()
            If bEsIdenti Then
                Existe = KEY_RINCORRECTO
                txtIdPremio.Enabled = False
                txtDescripcion.Enabled = True
                txtDescripcion.Focus()
            Else
                txtIdPremio.Focus()
            End If

        ElseIf vOpcion = TipoOpcionForma.tOpcModificar Then
            'MODIFICAR
            'ToolStripMenu.Enabled = False
            'PERMISOS - EN CASO DE NECESITARLO
            'If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Modificar) = False Then
            '    MessageBox.Show("No tiene los permisos Necesarios para poder Modificar en el Catalogo de Areas!!!",
            '                    "Acceso Denegado a Modificacion de Areas", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            '    Cancelar()
            '    Exit Sub
            'End If
            'comboMnu.SelectedIndex = 1
            OpcionForma = TipoOpcionForma.tOpcModificar
            ActivaCampos(False, TipoOpcActivaCampos.tOpcINICIALIZA)
            ActivaBotones(True, TipoOpcActivaBoton.tOpcConsulta)
            'txtsidArea.Focus()
            If Trim(txtIdPremio.Text) = "" Then
                txtIdPremio.Text = DespliegaGrid(TablaBd, Inicio.CONSTR, CampoLLave)
            End If
        ElseIf vOpcion = TipoOpcionForma.tOpcEliminar Then
            'ELIMINAR
            'PERMISOS - EN CASO DE NECESITARLO
            'If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Baja) = False Then
            '    MessageBox.Show("No tiene los permisos Necesarios para poder Eliminar en el Catalogo de Areas!!!",
            '                    "Acceso Denegado a Eliminacion de Areas", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            '    Cancelar()
            '    Exit Sub
            'End If
            'comboMnu.SelectedIndex = 2
            OpcionForma = TipoOpcionForma.tOpcEliminar
            ActivaCampos(False, TipoOpcActivaCampos.tOpcINICIALIZA)
            ActivaBotones(True, TipoOpcActivaBoton.tOpcConsulta)
            'txtsidArea.Focus()
            If Trim(txtIdPremio.Text) = "" Then
                txtIdPremio.Text = DespliegaGrid(TablaBd, Inicio.CONSTR, CampoLLave)
            End If

        ElseIf vOpcion = TipoOpcionForma.tOpcConsultar Then
            'CONSULTA
            ToolStripMenu.Enabled = True
            GridTodos.Enabled = True
            'comboMnu.SelectedIndex = 3
            OpcionForma = TipoOpcionForma.tOpcConsultar
            ActivaCampos(False, TipoOpcActivaCampos.tOpcINICIALIZA)
            ActivaBotones(True, TipoOpcActivaBoton.tOpcConsulta)
            If Trim(txtIdPremio.Text) = "" Then
                txtIdPremio.Text = DespliegaGrid(TablaBd, Inicio.CONSTR, CampoLLave)
            End If
        ElseIf vOpcion = TipoOpcionForma.tOpcImprimir Then
            'REPORTE
            'PERMISOS - EN CASO DE NECESITARLO
            'If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Reporte) = False Then
            '    MessageBox.Show("No tiene los permisos Necesarios para poder generar Reportes en el Catalogo de Areas!!!",
            '                    "Acceso Denegado a Generar Reportes en el Catalogo de Areas", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            '    Cancelar()
            '    Exit Sub
            'End If
            ToolStripMenu.Enabled = False
            GridTodos.Enabled = True
            'comboMnu.SelectedIndex = 4
            DespliegaReporte(TablaBd, Inicio.CONSTR)
            ToolStripMenu.Enabled = True
        End If
    End Sub

    Private Sub btnMnuAltas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuAltas.Click
        'ALTAS
        OpcionMnuTools(TipoOpcionForma.tOpcInsertar)
    End Sub

    Private Sub btnMnuModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuModificar.Click
        'MODIFICAR
        OpcionMnuTools(TipoOpcionForma.tOpcModificar)
        txtIdPremio_Leave(sender, e)
    End Sub

    Private Sub btnMnuEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuEliminar.Click
        'ELIMINAR
        OpcionMnuTools(TipoOpcionForma.tOpcEliminar)
        txtIdPremio_Leave(sender, e)
        ActivaCampos(False, TipoOpcActivaCampos.tOpcDESHABTODOS)
        btnMnuOk_Click(sender, e)
    End Sub

    Private Sub btnMnuConsulta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'CONSULTA
        OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
        txtIdPremio_Leave(sender, e)
        txtDescripcion.Enabled = False
    End Sub

    Private Sub btnMnuReporte_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuReporte.Click
        'REPORTE
        OpcionMnuTools(TipoOpcionForma.tOpcImprimir)
    End Sub
#End Region

   

    Private Sub txtDescripcion_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtDescripcion.TextChanged

    End Sub

    Private Sub btnMnuOk_Click(sender As Object, e As EventArgs) Handles btnMnuOk.Click
        Dim strSQL As String
        Dim TipoSentencia As String
        indice = 0
        Try
            If Not Validar() Then Exit Sub
            'RegModif = LLenaRegistro()

            If Existe = KEY_RCORRECTO Then
                'Existe - Actualizar o Borrar
                If OpcionForma = TipoOpcionForma.tOpcModificar Then
                    'OpcionForma = TipoOpcionForma.tOpcModificar
                    Dim CadCambios As String = Audit.GetCambios
                    If CadCambios = "" Then
                        MessageBox.Show("NO se realizaron Modificaciones para Guardar!!!", "No hay Modificaciones para Guardar", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        Exit Sub
                    End If
                    If MessageBox.Show("Esta completamente Seguro que Desea Realiar los Siguientes Cambios:" & vbNewLine &
                                    CadCambios, "Confirma que desea Realizar las Siguientes Modificaciones", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then Exit Sub
                    TipoSentencia = DeterminaNombreSentencia(Fletera.Com.TipoSentencia.TsModificar)

                    strSQL = "UPDATE " & TablaBd & " SET Descripcion = '" & txtDescripcion.Text & _
                    "', ValorPuntos = " & txtValorPuntos.Text & ", ValorPesos= " & txtValorPesos.Text & " WHERE " & CampoLLave & " = " & txtIdPremio.Text

                    ReDim Preserve ArraySql(indice)
                    ArraySql(indice) = strSQL
                    indice += 1
                    ReDim Preserve ArraySql(indice)
                    ArraySql(indice) = Audit.GetSQL(AuditaClass._EnTipoAudita.Modifica)
                    indice += 1
                ElseIf OpcionForma = TipoOpcionForma.tOpcEliminar Then
                    If MsgBox("!! Esta seguro que desea Eliminar el Premio : " & txtIdPremio.Text & " " & txtDescripcion.Text & "? !!", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                        TipoSentencia = DeterminaNombreSentencia(Fletera.Com.TipoSentencia.TsEliminar)

                        strSQL = "DELETE FROM " & TablaBd & " WHERE " & CampoLLave & " = " & txtIdPremio.Text

                        ReDim Preserve ArraySql(indice)
                        ArraySql(indice) = strSQL
                        indice += 1
                        ReDim Preserve ArraySql(indice)
                        ArraySql(indice) = Audit.GetSQL(AuditaClass._EnTipoAudita.Elimina)
                        indice += 1
                    End If
                End If

            ElseIf Existe = KEY_RINCORRECTO Then
                'No Existe - Insertar
                'OpcionForma = TipoOpcionForma.tOpcInsertar
                TipoSentencia = DeterminaNombreSentencia(Fletera.Com.TipoSentencia.TsInsertar)

                If bEsIdenti Then
                    strSQL = "insert into " & TablaBd & " (Descripcion, ValorPuntos, ValorPesos )" & _
                   " values ('" & txtDescripcion.Text & "'," & txtValorPuntos.Text & "," & txtValorPesos.Text & ")"
                Else
                    strSQL = "insert into " & TablaBd & " (" & CampoLLave & ", Descripcion, ValorPuntos, ValorPesos )" & _
                    " values ('" & txtIdPremio.Text & "','" & txtDescripcion.Text & "'," & txtValorPuntos.Text & "," & txtValorPesos.Text & ")"

                End If

                ReDim Preserve ArraySql(indice)
                ArraySql(indice) = strSQL
                indice += 1
                ReDim Preserve ArraySql(indice)
                ArraySql(indice) = Audit.GetSQL(AuditaClass._EnTipoAudita.Inserta)
                indice += 1
            End If

            'If ModAuditaSIA Then
            '    If RegAModif <> RegModif Then
            '        'NumCliente = NumSigEnTablas("NumCliente", "PLAZAS", " and CVEPLAZA = '" & CvePlaza & "'", " where CVEPLAZA = '" & CvePlaza & "'", MyValue)
            '        ConsecutivoAudita = objAudita.NumSigEnTablas(Inicio.ConStr, "nNumConsAudita", "Consecutivos")
            '        If ConsecutivoAudita > 0 Then
            '            strSQL = "insert into sysAudit (nNumConsecutivo,sTabla,sRegistroaModificar,sRegistroModificado,dFecha,sUsuario,sTipoAccion) " & _
            '            " values (" & ConsecutivoAudita & ",'" & TablaBd & "','" & RegAModif & "','" & RegModif & "','" & Configuracion.FormateandoFecha(Now, sFormatoFechaServidor) & _
            '            "','" & UCase(System.Environment.UserName) & "','" & TipoSentencia & "')"

            '            ReDim Preserve ArraySql(indice)
            '            ArraySql(indice) = strSQL
            '            indice += 1
            '        End If
            '    End If
            'End If

            Dim obj As New CapaNegocio.Tablas
            Dim Respuesta As String

            Respuesta = obj.EjecutarSql(Inicio.CONSTR, "SQL", ArraySql, Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, indice)

            If Respuesta = "EFECTUADO" Then
                If OpcionForma = TipoOpcionForma.tOpcModificar Then
                    MsgBox("Premio actualizado satisfactoriamente", MsgBoxStyle.Exclamation, Me.Text)
                ElseIf OpcionForma = TipoOpcionForma.tOpcEliminar Then
                    MsgBox("Premio eliminado con exito!!!", MsgBoxStyle.Exclamation, Me.Text)
                ElseIf OpcionForma = TipoOpcionForma.tOpcInsertar Then
                    MsgBox("Actualizacion efectuada satisfactoriamente", MsgBoxStyle.Exclamation, Me.Text)
                End If
            ElseIf Respuesta = "NOEFECTUADO" Then

            ElseIf Respuesta = "ERROR" Then
                indice = 0
                SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")
            End If

        Catch ex As Exception
            indice = 0
            ConsecutivoAudita = 0
            LimpiaVariables()
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try
        Cancelar()
    End Sub

    Private Sub btnMnuCancelar_Click(sender As Object, e As EventArgs) Handles btnMnuCancelar.Click
        Cancelar()
    End Sub

    Private Sub btnMnuSalir_Click(sender As Object, e As EventArgs) Handles btnMnuSalir.Click
        Me.Close()
    End Sub

    Private Sub txtIdPremio_TextChanged(sender As Object, e As EventArgs) Handles txtIdPremio.TextChanged

    End Sub

    Private Sub txtMenuBusca_Click(sender As Object, e As EventArgs) Handles txtMenuBusca.Click

    End Sub

    Private Sub GroupBox2_Enter(sender As Object, e As EventArgs) Handles GroupBox2.Enter

    End Sub
End Class
