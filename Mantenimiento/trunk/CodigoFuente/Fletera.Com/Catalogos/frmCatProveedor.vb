'Fecha: 15 / Febrero / 2016
'ROGER GALA PACHECO
'************************************************************************************************************************************
'*  Fecha Modificacion :   15 / Febrero / 2016                                                                                       
'*  
'************************************************************************************************************************************
Public Class frmCatProveedor
    Inherits System.Windows.Forms.Form
    Dim OpcionForma As TipoOpcionForma
    Dim TablaBd As String = "CatProveedores"
    Dim CampoLLave As String = "idProveedor"
    Dim TablaBd2 As String = "CatColonia"
    Public WithEvents MeStatus1 As ControlStatus.MeStatus
    Friend WithEvents GridTodos As System.Windows.Forms.DataGridView
    Private Proveedor As ProveedorClass
    Private Colonia As ColoniaClass
    Friend WithEvents btnMnuOk As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuCancelar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuSalir As System.Windows.Forms.ToolStripButton
    Private _Tag As String
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cmdBuscaClaveSuc As System.Windows.Forms.Button
    Friend WithEvents txtidColonia As System.Windows.Forms.TextBox
    Friend WithEvents txtNomColonia As System.Windows.Forms.TextBox
    Dim bEsIdentidad As Boolean = True
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtNomPais As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtNomEstado As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtRFCProv As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtEmailProv As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtDireccionProv As System.Windows.Forms.TextBox
    Friend WithEvents cmbTipoPersona As System.Windows.Forms.ComboBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtNomCiudad As System.Windows.Forms.TextBox
    Dim Salida As Boolean
    Private con As String
    Private v As Boolean

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents GrupoCaptura As System.Windows.Forms.GroupBox
    Friend WithEvents ToolStripMenu As System.Windows.Forms.ToolStrip
    Friend WithEvents txtMenuBusca As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents btnMnuAltas As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuModificar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuEliminar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuReporte As System.Windows.Forms.ToolStripButton
    Friend WithEvents label1 As System.Windows.Forms.Label
    Friend WithEvents label2 As System.Windows.Forms.Label
    Friend WithEvents cmdBuscaClave As System.Windows.Forms.Button
    Friend WithEvents txtidProveedor As System.Windows.Forms.TextBox
    Friend WithEvents txtRazonSocial As System.Windows.Forms.TextBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCatProveedor))
        Me.txtidProveedor = New System.Windows.Forms.TextBox()
        Me.label1 = New System.Windows.Forms.Label()
        Me.txtRazonSocial = New System.Windows.Forms.TextBox()
        Me.label2 = New System.Windows.Forms.Label()
        Me.GrupoCaptura = New System.Windows.Forms.GroupBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtNomCiudad = New System.Windows.Forms.TextBox()
        Me.cmbTipoPersona = New System.Windows.Forms.ComboBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtEmailProv = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtDireccionProv = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtNomPais = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtNomEstado = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtRFCProv = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cmdBuscaClaveSuc = New System.Windows.Forms.Button()
        Me.txtidColonia = New System.Windows.Forms.TextBox()
        Me.txtNomColonia = New System.Windows.Forms.TextBox()
        Me.cmdBuscaClave = New System.Windows.Forms.Button()
        Me.ToolStripMenu = New System.Windows.Forms.ToolStrip()
        Me.txtMenuBusca = New System.Windows.Forms.ToolStripTextBox()
        Me.btnMnuAltas = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuModificar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuEliminar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuReporte = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuOk = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuCancelar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuSalir = New System.Windows.Forms.ToolStripButton()
        Me.MeStatus1 = New ControlStatus.MeStatus()
        Me.GridTodos = New System.Windows.Forms.DataGridView()
        Me.GrupoCaptura.SuspendLayout()
        Me.ToolStripMenu.SuspendLayout()
        CType(Me.GridTodos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtidProveedor
        '
        Me.txtidProveedor.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtidProveedor.Location = New System.Drawing.Point(109, 27)
        Me.txtidProveedor.MaxLength = 3
        Me.txtidProveedor.Name = "txtidProveedor"
        Me.txtidProveedor.Size = New System.Drawing.Size(69, 20)
        Me.txtidProveedor.TabIndex = 4
        '
        'label1
        '
        Me.label1.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.label1.Location = New System.Drawing.Point(6, 27)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(94, 20)
        Me.label1.TabIndex = 14
        Me.label1.Text = "id Proveedor:"
        '
        'txtRazonSocial
        '
        Me.txtRazonSocial.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRazonSocial.Location = New System.Drawing.Point(109, 59)
        Me.txtRazonSocial.MaxLength = 50
        Me.txtRazonSocial.Name = "txtRazonSocial"
        Me.txtRazonSocial.Size = New System.Drawing.Size(322, 20)
        Me.txtRazonSocial.TabIndex = 6
        '
        'label2
        '
        Me.label2.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.label2.Location = New System.Drawing.Point(9, 59)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(91, 20)
        Me.label2.TabIndex = 16
        Me.label2.Text = "Raz�n Social:"
        '
        'GrupoCaptura
        '
        Me.GrupoCaptura.Controls.Add(Me.Label10)
        Me.GrupoCaptura.Controls.Add(Me.txtNomCiudad)
        Me.GrupoCaptura.Controls.Add(Me.cmbTipoPersona)
        Me.GrupoCaptura.Controls.Add(Me.Label9)
        Me.GrupoCaptura.Controls.Add(Me.Label8)
        Me.GrupoCaptura.Controls.Add(Me.txtEmailProv)
        Me.GrupoCaptura.Controls.Add(Me.Label7)
        Me.GrupoCaptura.Controls.Add(Me.txtDireccionProv)
        Me.GrupoCaptura.Controls.Add(Me.Label6)
        Me.GrupoCaptura.Controls.Add(Me.txtNomPais)
        Me.GrupoCaptura.Controls.Add(Me.Label4)
        Me.GrupoCaptura.Controls.Add(Me.txtNomEstado)
        Me.GrupoCaptura.Controls.Add(Me.Label5)
        Me.GrupoCaptura.Controls.Add(Me.txtRFCProv)
        Me.GrupoCaptura.Controls.Add(Me.Label3)
        Me.GrupoCaptura.Controls.Add(Me.cmdBuscaClaveSuc)
        Me.GrupoCaptura.Controls.Add(Me.txtidColonia)
        Me.GrupoCaptura.Controls.Add(Me.txtNomColonia)
        Me.GrupoCaptura.Controls.Add(Me.label1)
        Me.GrupoCaptura.Controls.Add(Me.label2)
        Me.GrupoCaptura.Controls.Add(Me.cmdBuscaClave)
        Me.GrupoCaptura.Controls.Add(Me.txtidProveedor)
        Me.GrupoCaptura.Controls.Add(Me.txtRazonSocial)
        Me.GrupoCaptura.Location = New System.Drawing.Point(168, 45)
        Me.GrupoCaptura.Name = "GrupoCaptura"
        Me.GrupoCaptura.Size = New System.Drawing.Size(524, 314)
        Me.GrupoCaptura.TabIndex = 3
        Me.GrupoCaptura.TabStop = False
        '
        'Label10
        '
        Me.Label10.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label10.Location = New System.Drawing.Point(6, 232)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(94, 20)
        Me.Label10.TabIndex = 38
        Me.Label10.Text = "Ciudad:"
        '
        'txtNomCiudad
        '
        Me.txtNomCiudad.Enabled = False
        Me.txtNomCiudad.Location = New System.Drawing.Point(109, 232)
        Me.txtNomCiudad.Name = "txtNomCiudad"
        Me.txtNomCiudad.ReadOnly = True
        Me.txtNomCiudad.Size = New System.Drawing.Size(275, 20)
        Me.txtNomCiudad.TabIndex = 14
        '
        'cmbTipoPersona
        '
        Me.cmbTipoPersona.AutoCompleteCustomSource.AddRange(New String() {"Regularizaci�n", "Vivienda"})
        Me.cmbTipoPersona.FormattingEnabled = True
        Me.cmbTipoPersona.Items.AddRange(New Object() {"MORAL", "FISICA"})
        Me.cmbTipoPersona.Location = New System.Drawing.Point(109, 85)
        Me.cmbTipoPersona.Name = "cmbTipoPersona"
        Me.cmbTipoPersona.Size = New System.Drawing.Size(184, 21)
        Me.cmbTipoPersona.TabIndex = 7
        '
        'Label9
        '
        Me.Label9.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label9.Location = New System.Drawing.Point(9, 90)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(91, 20)
        Me.Label9.TabIndex = 35
        Me.Label9.Text = "Tipo Persona:"
        '
        'Label8
        '
        Me.Label8.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label8.Location = New System.Drawing.Point(9, 138)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(91, 20)
        Me.Label8.TabIndex = 34
        Me.Label8.Text = "Email:"
        '
        'txtEmailProv
        '
        Me.txtEmailProv.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtEmailProv.Location = New System.Drawing.Point(109, 138)
        Me.txtEmailProv.MaxLength = 50
        Me.txtEmailProv.Name = "txtEmailProv"
        Me.txtEmailProv.Size = New System.Drawing.Size(322, 20)
        Me.txtEmailProv.TabIndex = 9
        '
        'Label7
        '
        Me.Label7.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label7.Location = New System.Drawing.Point(9, 167)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(91, 20)
        Me.Label7.TabIndex = 32
        Me.Label7.Text = "Direcci�n:"
        '
        'txtDireccionProv
        '
        Me.txtDireccionProv.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDireccionProv.Location = New System.Drawing.Point(109, 167)
        Me.txtDireccionProv.MaxLength = 50
        Me.txtDireccionProv.Name = "txtDireccionProv"
        Me.txtDireccionProv.Size = New System.Drawing.Size(390, 20)
        Me.txtDireccionProv.TabIndex = 10
        '
        'Label6
        '
        Me.Label6.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label6.Location = New System.Drawing.Point(6, 284)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(94, 20)
        Me.Label6.TabIndex = 30
        Me.Label6.Text = "Pa�s:"
        '
        'txtNomPais
        '
        Me.txtNomPais.Enabled = False
        Me.txtNomPais.Location = New System.Drawing.Point(109, 284)
        Me.txtNomPais.Name = "txtNomPais"
        Me.txtNomPais.ReadOnly = True
        Me.txtNomPais.Size = New System.Drawing.Size(275, 20)
        Me.txtNomPais.TabIndex = 16
        '
        'Label4
        '
        Me.Label4.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label4.Location = New System.Drawing.Point(6, 258)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(94, 20)
        Me.Label4.TabIndex = 28
        Me.Label4.Text = "Estado:"
        '
        'txtNomEstado
        '
        Me.txtNomEstado.Enabled = False
        Me.txtNomEstado.Location = New System.Drawing.Point(109, 258)
        Me.txtNomEstado.Name = "txtNomEstado"
        Me.txtNomEstado.ReadOnly = True
        Me.txtNomEstado.Size = New System.Drawing.Size(275, 20)
        Me.txtNomEstado.TabIndex = 15
        '
        'Label5
        '
        Me.Label5.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label5.Location = New System.Drawing.Point(6, 112)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(94, 20)
        Me.Label5.TabIndex = 26
        Me.Label5.Text = "R.F.C."
        '
        'txtRFCProv
        '
        Me.txtRFCProv.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRFCProv.Location = New System.Drawing.Point(109, 112)
        Me.txtRFCProv.MaxLength = 13
        Me.txtRFCProv.Name = "txtRFCProv"
        Me.txtRFCProv.Size = New System.Drawing.Size(109, 20)
        Me.txtRFCProv.TabIndex = 8
        '
        'Label3
        '
        Me.Label3.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label3.Location = New System.Drawing.Point(6, 202)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(94, 20)
        Me.Label3.TabIndex = 20
        Me.Label3.Text = "Id Colonia:"
        '
        'cmdBuscaClaveSuc
        '
        Me.cmdBuscaClaveSuc.Image = CType(resources.GetObject("cmdBuscaClaveSuc.Image"), System.Drawing.Image)
        Me.cmdBuscaClaveSuc.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdBuscaClaveSuc.Location = New System.Drawing.Point(186, 194)
        Me.cmdBuscaClaveSuc.Name = "cmdBuscaClaveSuc"
        Me.cmdBuscaClaveSuc.Size = New System.Drawing.Size(32, 32)
        Me.cmdBuscaClaveSuc.TabIndex = 12
        Me.cmdBuscaClaveSuc.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'txtidColonia
        '
        Me.txtidColonia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtidColonia.Location = New System.Drawing.Point(109, 202)
        Me.txtidColonia.MaxLength = 3
        Me.txtidColonia.Name = "txtidColonia"
        Me.txtidColonia.Size = New System.Drawing.Size(69, 20)
        Me.txtidColonia.TabIndex = 11
        '
        'txtNomColonia
        '
        Me.txtNomColonia.Enabled = False
        Me.txtNomColonia.Location = New System.Drawing.Point(224, 206)
        Me.txtNomColonia.Name = "txtNomColonia"
        Me.txtNomColonia.ReadOnly = True
        Me.txtNomColonia.Size = New System.Drawing.Size(275, 20)
        Me.txtNomColonia.TabIndex = 13
        '
        'cmdBuscaClave
        '
        Me.cmdBuscaClave.Image = CType(resources.GetObject("cmdBuscaClave.Image"), System.Drawing.Image)
        Me.cmdBuscaClave.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdBuscaClave.Location = New System.Drawing.Point(186, 19)
        Me.cmdBuscaClave.Name = "cmdBuscaClave"
        Me.cmdBuscaClave.Size = New System.Drawing.Size(32, 32)
        Me.cmdBuscaClave.TabIndex = 5
        Me.cmdBuscaClave.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'ToolStripMenu
        '
        Me.ToolStripMenu.AutoSize = False
        Me.ToolStripMenu.BackColor = System.Drawing.Color.Silver
        Me.ToolStripMenu.Dock = System.Windows.Forms.DockStyle.None
        Me.ToolStripMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.txtMenuBusca, Me.btnMnuAltas, Me.btnMnuModificar, Me.btnMnuEliminar, Me.btnMnuReporte, Me.btnMnuOk, Me.btnMnuCancelar, Me.btnMnuSalir})
        Me.ToolStripMenu.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow
        Me.ToolStripMenu.Location = New System.Drawing.Point(3, 0)
        Me.ToolStripMenu.Name = "ToolStripMenu"
        Me.ToolStripMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.ToolStripMenu.Size = New System.Drawing.Size(730, 42)
        Me.ToolStripMenu.TabIndex = 1
        Me.ToolStripMenu.Text = "ToolStrip1"
        '
        'txtMenuBusca
        '
        Me.txtMenuBusca.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.txtMenuBusca.Name = "txtMenuBusca"
        Me.txtMenuBusca.Size = New System.Drawing.Size(150, 42)
        Me.txtMenuBusca.ToolTipText = "Filtra Listado"
        '
        'btnMnuAltas
        '
        Me.btnMnuAltas.ForeColor = System.Drawing.Color.Red
        Me.btnMnuAltas.Image = CType(resources.GetObject("btnMnuAltas.Image"), System.Drawing.Image)
        Me.btnMnuAltas.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuAltas.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuAltas.Name = "btnMnuAltas"
        Me.btnMnuAltas.Size = New System.Drawing.Size(46, 39)
        Me.btnMnuAltas.Text = "&ALTAS"
        Me.btnMnuAltas.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnMnuAltas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuModificar
        '
        Me.btnMnuModificar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuModificar.Image = CType(resources.GetObject("btnMnuModificar.Image"), System.Drawing.Image)
        Me.btnMnuModificar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuModificar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuModificar.Name = "btnMnuModificar"
        Me.btnMnuModificar.Size = New System.Drawing.Size(74, 39)
        Me.btnMnuModificar.Text = "&MODIFICAR"
        Me.btnMnuModificar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnMnuModificar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuEliminar
        '
        Me.btnMnuEliminar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuEliminar.Image = CType(resources.GetObject("btnMnuEliminar.Image"), System.Drawing.Image)
        Me.btnMnuEliminar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuEliminar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuEliminar.Name = "btnMnuEliminar"
        Me.btnMnuEliminar.Size = New System.Drawing.Size(64, 39)
        Me.btnMnuEliminar.Text = "&ELIMINAR"
        Me.btnMnuEliminar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuReporte
        '
        Me.btnMnuReporte.ForeColor = System.Drawing.Color.Red
        Me.btnMnuReporte.Image = CType(resources.GetObject("btnMnuReporte.Image"), System.Drawing.Image)
        Me.btnMnuReporte.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuReporte.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuReporte.Name = "btnMnuReporte"
        Me.btnMnuReporte.Size = New System.Drawing.Size(60, 39)
        Me.btnMnuReporte.Text = "&REPORTE"
        Me.btnMnuReporte.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuOk
        '
        Me.btnMnuOk.ForeColor = System.Drawing.Color.Red
        Me.btnMnuOk.Image = Global.Fletera.My.Resources.Resources.Save1
        Me.btnMnuOk.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuOk.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuOk.Name = "btnMnuOk"
        Me.btnMnuOk.Size = New System.Drawing.Size(56, 39)
        Me.btnMnuOk.Text = "&GRABAR"
        Me.btnMnuOk.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuCancelar
        '
        Me.btnMnuCancelar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuCancelar.Image = Global.Fletera.My.Resources.Resources.signos_f_059
        Me.btnMnuCancelar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuCancelar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuCancelar.Name = "btnMnuCancelar"
        Me.btnMnuCancelar.Size = New System.Drawing.Size(71, 39)
        Me.btnMnuCancelar.Text = "&CANCELAR"
        Me.btnMnuCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuSalir
        '
        Me.btnMnuSalir.ForeColor = System.Drawing.Color.Red
        Me.btnMnuSalir.Image = Global.Fletera.My.Resources.Resources.Salida2
        Me.btnMnuSalir.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuSalir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuSalir.Name = "btnMnuSalir"
        Me.btnMnuSalir.Size = New System.Drawing.Size(41, 39)
        Me.btnMnuSalir.Text = "&SALIR"
        Me.btnMnuSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'MeStatus1
        '
        Me.MeStatus1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.MeStatus1.Location = New System.Drawing.Point(0, 359)
        Me.MeStatus1.Margin = New System.Windows.Forms.Padding(4)
        Me.MeStatus1.Name = "MeStatus1"
        Me.MeStatus1.Size = New System.Drawing.Size(692, 26)
        Me.MeStatus1.TabIndex = 72
        '
        'GridTodos
        '
        Me.GridTodos.AllowUserToAddRows = False
        Me.GridTodos.AllowUserToDeleteRows = False
        Me.GridTodos.AllowUserToResizeRows = False
        Me.GridTodos.BackgroundColor = System.Drawing.Color.White
        Me.GridTodos.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.GridTodos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.GridTodos.Location = New System.Drawing.Point(3, 45)
        Me.GridTodos.MultiSelect = False
        Me.GridTodos.Name = "GridTodos"
        Me.GridTodos.ReadOnly = True
        Me.GridTodos.RowHeadersVisible = False
        Me.GridTodos.Size = New System.Drawing.Size(162, 314)
        Me.GridTodos.TabIndex = 73
        '
        'frmCatProveedor
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(692, 385)
        Me.Controls.Add(Me.GridTodos)
        Me.Controls.Add(Me.MeStatus1)
        Me.Controls.Add(Me.ToolStripMenu)
        Me.Controls.Add(Me.GrupoCaptura)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmCatProveedor"
        Me.Text = "Catal�go de Proveedores"
        Me.GrupoCaptura.ResumeLayout(False)
        Me.GrupoCaptura.PerformLayout()
        Me.ToolStripMenu.ResumeLayout(False)
        Me.ToolStripMenu.PerformLayout()
        CType(Me.GridTodos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region
    Sub New(ByVal vTag As String)
        InitializeComponent()
        _Tag = vTag
    End Sub




    Public Sub New(con As String, v As Boolean)
        Me.con = con
        BD = New CapaDatos.UtilSQL(con, "Juan")
        InitializeComponent()
        Me.v = v
    End Sub
#Region "MenusToolStrip"
    Private Sub LimpiaVariables()
        SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")

    End Sub
    Private Sub LimpiaCampos()
        txtidProveedor.Text = ""
        txtRazonSocial.Text = ""
        txtidColonia.Text = ""
        txtRFCProv.Text = ""
        txtDireccionProv.Text = ""
        txtEmailProv.Text = ""
        cmbTipoPersona.SelectedIndex = 0
        txtNomColonia.Text = ""
        txtNomEstado.Text = ""
        txtNomPais.Text = ""
        LimpiaVariables()
    End Sub

    Private Sub ActivaCampos(ByVal valor As Boolean, ByVal vOpcion As TipoOpcActivaCampos)
        If vOpcion = TipoOpcActivaCampos.tOpcDESHABTODOS Then
            txtidProveedor.Enabled = False
            txtRazonSocial.Enabled = False
            txtidColonia.Enabled = False
            txtRFCProv.Enabled = False
            txtDireccionProv.Enabled = False
            txtEmailProv.Enabled = False
            cmbTipoPersona.Enabled = False
        ElseIf vOpcion = TipoOpcActivaCampos.tOpcINICIALIZA Then
            txtidProveedor.Enabled = Not valor
            txtRazonSocial.Enabled = valor
            txtidColonia.Enabled = valor
            txtRFCProv.Enabled = valor
            txtDireccionProv.Enabled = valor
            txtEmailProv.Enabled = valor
            cmbTipoPersona.Enabled = valor

        End If
    End Sub


    Private Sub ActivaBotones(ByVal valor As Boolean, Optional ByVal vOpcion As TipoOpcActivaBoton = TipoOpcActivaBoton.tOpcInicializa)
        If vOpcion = TipoOpcActivaBoton.tOpcConsulta Then
            btnMnuOk.Enabled = False
            btnMnuCancelar.Enabled = valor
            btnMnuSalir.Enabled = Not valor

            btnMnuAltas.Enabled = Not valor
            btnMnuModificar.Enabled = Not valor
            btnMnuEliminar.Enabled = Not valor
            btnMnuReporte.Enabled = Not valor
        ElseIf vOpcion = TipoOpcActivaBoton.tOpcInicializa Then
            btnMnuOk.Enabled = valor
            btnMnuCancelar.Enabled = valor
            btnMnuSalir.Enabled = Not valor

            btnMnuAltas.Enabled = Not valor
            btnMnuModificar.Enabled = Not valor
            btnMnuEliminar.Enabled = Not valor
            btnMnuReporte.Enabled = Not valor

            txtMenuBusca.Enabled = Not valor
            GridTodos.Enabled = Not valor
        ElseIf vOpcion = TipoOpcActivaBoton.tOpcInsertar Then
            btnMnuOk.Enabled = valor
            btnMnuCancelar.Enabled = valor
            btnMnuSalir.Enabled = Not valor

            btnMnuAltas.Enabled = Not valor
            btnMnuModificar.Enabled = Not valor
            btnMnuEliminar.Enabled = Not valor
            btnMnuReporte.Enabled = Not valor
        End If
    End Sub

    Private Sub btnMnuAltas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuAltas.Click
        'ALTAS
        '15/FEB/2016
        'If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Alta) = False Then
        '    MessageBox.Show("No tiene los permisos Necesarios para poder dar de Alta en el Catalogo de Proveedor!!!",
        '        "Acceso Denegado a Alta de Proveedor", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    OpcionForma = TipoOpcionForma.tOpcConsultar
        '    Exit Sub
        'End If
        OpcionForma = TipoOpcionForma.tOpcInsertar
        LimpiaCampos()
        ActivaCampos(True, TipoOpcActivaCampos.tOpcINICIALIZA)
        ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)

        If bEsIdentidad Then
            txtidProveedor.Enabled = False
            txtRazonSocial.Focus()
        Else
            txtidProveedor.Focus()
        End If
        Status("Ingrese nuevo Proveedor para guardar", Me)
    End Sub
    Private Sub btnMnuModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuModificar.Click
        'MODIFICAR
        'If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Modificar) = False Then
        '    MessageBox.Show("No tiene los permisos Necesarios para poder Modificar la Informacion del Proveedor!!!",
        '           "Acceso Denegado a Modificacion de la Informacion del Proveedor", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    OpcionForma = TipoOpcionForma.tOpcConsultar
        '    Exit Sub
        'End If
        If Trim(txtidProveedor.Text) = "" Then
            txtidProveedor.Text = DespliegaGrid(TablaBd, Inicio.CONSTR, CampoLLave)
            CargaForm(txtidProveedor.Text, UCase(TablaBd))
        End If

        OpcionForma = TipoOpcionForma.tOpcModificar
        ActivaCampos(True, TipoOpcActivaCampos.tOpcINICIALIZA)
        ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)

        btnMnuEliminar.Enabled = True
        If bEsIdentidad Then
            txtidProveedor.Enabled = False
            txtRazonSocial.Focus()
        Else
            txtidProveedor.Focus()
        End If

        Status("Ingrese Proveedor para guardar", Me)
    End Sub
    Private Sub btnMnuEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuEliminar.Click
        'ELIMINAR
        'If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Baja) = False Then
        '    MessageBox.Show("No tiene los permisos Necesarios para poder Eliminar el Proveedor!!!",
        '        "Acceso Denegado a Eliminacion de Proveedor", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    OpcionForma = TipoOpcionForma.tOpcConsultar
        '    Exit Sub
        'End If
        If txtidProveedor.Text = Proveedor.idColonia Then
            With Proveedor
                If MessageBox.Show("Esta completamente seguro que desea Eliminar el Proveedor: " &
                                   .RazonSocial & " Con Id: " & .idColonia & " ????", "Confirma que desea eliminar la Colonia: " & .RazonSocial & "???",
                                 MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then Exit Sub
                Try
                    .Eliminar() 'Elimina el municipio
                    MessageBox.Show("Proveedor: " & .RazonSocial & " Eliminado con exito!!!",
                                    "Proveedor Eliminado con exito", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Status("Proveedor: " & .RazonSocial & " Eliminado con exito!!!", Me)
                    ActualizaGrid(txtMenuBusca.Text)
                    txtidProveedor.Text = ""
                    OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
                Catch ex As Exception
                    Status("Error al tratar de eliminar el Proveedor: " & ex.Message, Me, Err:=True)
                    MessageBox.Show("Error al Tratar de eliminar el Proveedor: " & .RazonSocial & vbNewLine &
                                    "Error. " & ex.Message, "Error: " & Err.Number, MessageBoxButtons.OK, MessageBoxIcon.Error)

                End Try
            End With
        End If
    End Sub
    Private Sub btnMnuReporte_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuReporte.Click
        'REPORTE
        'If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Reporte) = False Then
        '    MessageBox.Show("No tiene los permisos Necesarios para poder realizar Reporte de Proveedor!!!",
        '        "Acceso Denegado a Reporte de Proveedor", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    OpcionForma = TipoOpcionForma.tOpcConsultar
        '    Exit Sub
        'End If
        ToolStripMenu.Enabled = False
        Status("Abriendo Reporte Espere. . . ", Me)
        Me.Cursor = Cursors.WaitCursor
        DespliegaReporte(TablaBd, CONSTR)
        Status("Finalizo el reporte. . . ", Me)
        Me.Cursor = Cursors.Default
        ToolStripMenu.Enabled = True
    End Sub

#End Region
    Private Sub ActualizaGrid(ByVal vNomAlmacen As String)
        If vNomAlmacen = "" Then
            GridTodos.DataSource = BD.ExecuteReturn("SELECT idProveedor, RazonSocial FROM " & TablaBd, TablaBd)
            Status(GridTodos.RowCount & " Registros", Me)
        Else
            GridTodos.DataSource = BD.ExecuteReturn("SELECT idProveedor, RazonSocial FROM " & TablaBd &
                                                " WHERE RazonSocial Like '%" & vNomAlmacen & "%'")
            Status(GridTodos.RowCount & " Registros obtenidos con el filtro %" & vNomAlmacen & "%", Me)
        End If
    End Sub

    Private Sub OpcionMnuTools(ByVal vOpcion As TipoOpcionForma)
        If vOpcion <> TipoOpcionForma.tOpcImprimir And vOpcion <> TipoOpcionForma.tOpcEliminar Then
            OpcionForma = vOpcion
        End If
        txtMenuBusca.Enabled = True
        GridTodos.Enabled = True
        If vOpcion = TipoOpcionForma.tOpcConsultar Then
            'CONSULTA
            For Each ct As Control In GrupoCaptura.Controls
                If Not ct Is cmdBuscaClave Then
                    ct.Enabled = False
                Else
                    ct.Enabled = True
                End If
            Next
            txtMenuBusca.Enabled = True
            btnMnuAltas.Enabled = True
            btnMnuModificar.Enabled = True
            btnMnuEliminar.Enabled = False
            btnMnuReporte.Enabled = True
            btnMnuOk.Enabled = False
            btnMnuCancelar.Enabled = False

            btnMnuSalir.Enabled = True


        End If
    End Sub


    Private Sub txtMenuBusca_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtMenuBusca.KeyPress
        If Asc(e.KeyChar) = 39 Then e.Handled = True : Exit Sub 'Caracter ' Evita errores
        If Asc(e.KeyChar) = 13 Then
            GridTodos.Focus()
            e.Handled = True
        End If
    End Sub

    'Private Sub txtMenuBusca_KeyUp(sender As Object, e As KeyEventArgs) Handles txtMenuBusca.KeyUp
    '    If txtMenuBusca.Text.Trim = "" Then Exit Sub
    '    If e.KeyCode = Keys.Down Or e.KeyCode = Keys.Up Then
    '        GridTodos.Focus()
    '        e.Handled = True
    '        Exit Sub
    '    End If
    '    If e.KeyCode = Keys.Left Or e.KeyCode = Keys.Right Then Exit Sub
    '    ActualizaGrid(txtMenuBusca.Text)
    'End Sub

    Private Sub frmCatProveedor_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Inicio.CONSTR = "" Then
            Inicio.CONSTR = con
        End If
        OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
        ActualizaGrid("")
        GridTodos.Columns(1).Width = GridTodos.Width - 20
        GridTodos.Columns(0).Visible = False
        txtMenuBusca.Focus()
    End Sub
    Private Sub GridTodos_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles GridTodos.CellEnter
        CargaForm(GridTodos.Rows(GridTodos.CurrentRow.Index).Cells(0).Value, UCase(TablaBd))
    End Sub
    Private Sub GridTodos_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles GridTodos.KeyPress
        If Asc(e.KeyChar) = 8 Then
            txtMenuBusca.Focus()
            e.Handled = True
        End If
    End Sub
    Private Sub CargaForm(ByVal idClave As Integer, ByVal NomTabla As String)
        Try
            Select Case NomTabla
                Case UCase(TablaBd)
                    Proveedor = New ProveedorClass(idClave)
                    With Proveedor
                        txtidProveedor.Text = .idColonia
                        If .Existe Then
                            txtRazonSocial.Text = .RazonSocial
                            txtidColonia.Text = .idColonia
                            txtRFCProv.Text = .RFCProv
                            txtDireccionProv.Text = .DireccionProv
                            txtEmailProv.Text = .EmailProv
                            cmbTipoPersona.Text = IIf(.TipoPersona = "M", "MORAL", "FISICA")
                            Colonia = New ColoniaClass(.idColonia)
                            If Colonia.Existe Then
                                txtNomColonia.Text = Colonia.NomColonia
                                txtNomCiudad.Text = Colonia.NomCiudad
                                txtNomEstado.Text = Colonia.NomEstado
                                txtNomPais.Text = Colonia.NomPais
                            End If
                            'txtNomColonia.Text = .
                            'txtCIDALMACEN_COM.Text = .CIDALMACEN_COM
                            If OpcionForma = TipoOpcionForma.tOpcInsertar Then
                                Status("El Proveedor Con id: " & .idColonia & _
                                       " Ya existe!!!, Favor de modificarlo para poder dar de alta a otro Proveedor", Me, Err:=True)
                                MessageBox.Show("El Proveedor con id: " & .idColonia & _
                                                " YA EXISTE!!!, Favor de Cambiarlo y volver a intentarlo",
                                                "El Proveedor con id: " & .idColonia & " YA EXISTE!!!", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            End If
                        Else 'No existe 
                            If OpcionForma = TipoOpcionForma.tOpcInsertar Then
                                'For Each Ct As Control In GrupoCaptura.Controls
                                '    If Not Ct Is txtidAlmacen And Not Ct Is cmdBuscaClave Then
                                '        Ct.Enabled = True
                                '    Else
                                '        Ct.Enabled = False
                                '    End If
                                '    If TypeOf (Ct) Is TextBox Then
                                '        If Not Ct Is txtidAlmacen Then
                                '            TryCast(Ct, TextBox).Text = ""
                                '        End If
                                '    End If
                                'Next
                                ActivaCampos(True, TipoOpcActivaCampos.tOpcINICIALIZA)
                                ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)
                                btnMnuOk.Enabled = True
                                txtRazonSocial.Focus()
                                Status("Listo para Guardar nuevo Proveedor con id: " & .idColonia, Me)
                            End If
                        End If
                    End With
                Case UCase(TablaBd2)
                    'aqui()
                    Colonia = New ColoniaClass(idClave)
                    With Colonia
                        txtidColonia.Text = .idColonia
                        If .Existe Then
                            txtNomColonia.Text = .NomColonia
                            txtNomCiudad.Text = .NomCiudad
                            txtNomEstado.Text = .NomEstado
                            txtNomPais.Text = .NomPais
                        Else
                            MsgBox("La Colonia con id: " & txtidColonia.Text & " No Existe")
                        End If
                    End With

            End Select



        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try

    End Sub
    Private Function Validar() As Boolean
        Validar = True
        If Not Inicio.ValidandoCampo(Trim(txtidProveedor.Text), TipoDato.TdNumerico) Then
            If Not bEsIdentidad Then
                MsgBox(MENSAJE_CLAVE, vbInformation, "Aviso" & Me.Text)
                If txtidProveedor.Enabled Then
                    txtidProveedor.Focus()
                    txtidProveedor.SelectAll()
                End If
                Validar = False
            End If
        ElseIf Not Inicio.ValidandoCampo(Trim(txtRazonSocial.Text), TipoDato.TdCadena) Then
            MsgBox("El Nombre del Proveedor No Puede Ser Vacio", vbInformation, "Aviso" & Me.Text)
            If txtRazonSocial.Enabled Then
                txtRazonSocial.Focus()
                txtRazonSocial.SelectAll()
            End If
            Validar = False
        ElseIf Not Inicio.ValidandoCampo(Trim(txtidColonia.Text), TipoDato.TdNumerico) Then
            MsgBox("La Colonia del Proveedor no Puede ser Vacio", vbInformation, "Aviso" & Me.Text)
            If txtidColonia.Enabled Then
                txtidColonia.Focus()
                txtidColonia.SelectAll()
            End If
            Validar = False
        End If
    End Function

    'Private Sub txtidAlmacen_EnabledChanged(sender As Object, e As EventArgs) Handles txtidAlmacen.EnabledChanged
    '    cmdBuscaClave.Enabled = txtidAlmacen.Enabled
    'End Sub
    'CONTROLES
    Private Sub txtidAlmacen_KeyDown(sender As Object, e As KeyEventArgs) Handles txtidProveedor.KeyDown
        If e.KeyCode = Keys.F3 Then
            cmdBuscaClave_Click(sender, e)
        End If

    End Sub
    Private Sub txtidAlmacen_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtidProveedor.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If txtidProveedor.Text.Trim <> "" Then
                CargaForm(txtidProveedor.Text, UCase(TablaBd))
            End If
            e.Handled = True
        End If
    End Sub
    Private Sub txtNomColonia_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtRazonSocial.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            cmbTipoPersona.Focus()
        End If
    End Sub

    Private Sub cmdBuscaClave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBuscaClave.Click
        Dim idEst As String = DespliegaGrid(UCase(TablaBd), Inicio.CONSTR, CampoLLave)
        If idEst <> "" Then CargaForm(idEst, UCase(TablaBd))
    End Sub

    Private Sub txtNomColonia_MouseDown(sender As Object, e As MouseEventArgs) Handles txtRazonSocial.MouseDown
        txtRazonSocial.Focus()
        txtRazonSocial.SelectAll()
    End Sub

    Private Sub txtidSucursal_EnabledChanged(sender As Object, e As EventArgs) Handles txtidColonia.EnabledChanged
        cmdBuscaClaveSuc.Enabled = txtidColonia.Enabled
    End Sub

    Private Sub txtidSucursal_KeyDown(sender As Object, e As KeyEventArgs) Handles txtidColonia.KeyDown
        If e.KeyCode = Keys.F3 Then
            cmdBuscaClaveDiv_Click(sender, e)
        End If
    End Sub

    Private Sub txtidSucursal_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtidColonia.KeyPress
        '    btnMnuOk_Click(sender, e)
        '
        'If Asc(e.KeyChar) = 13 Then
        '    If txtidSucursal.Text.Trim <> "" Then
        '        CargaForm(txtidSucursal.Text, TablaBd2)
        '        txtNomSucursal.Focus()
        '        txtNomSucursal.SelectAll()
        '    End If
        '    e.Handled = True
        'End If
        If txtidColonia.Text.Length = 0 Then
            txtidColonia.Focus()
        Else
            If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
                Salida = True
                CargaForm(txtidColonia.Text, UCase(TablaBd2))
                Salida = False
                'txtCIDALMACEN_COM.Focus()
                'txtCIDALMACEN_COM.SelectAll()
            End If
        End If
    End Sub

    Private Sub txtidSucursal_Leave(sender As Object, e As EventArgs) Handles txtidColonia.Leave
        If Salida Then Exit Sub
        If txtidColonia.Text.Length = 0 Then
            txtidColonia.Focus()
            txtidColonia.SelectAll()
        Else
            If txtidColonia.Enabled Then
                CargaForm(txtidColonia.Text, UCase(TablaBd2))
                'txtCIDALMACEN_COM.Focus()
                'txtCIDALMACEN_COM.SelectAll()
            End If
        End If
    End Sub

    Private Sub txtidSucursal_MouseDown(sender As Object, e As MouseEventArgs) Handles txtidColonia.MouseDown
        txtidColonia.Focus()
        txtidColonia.SelectAll()
    End Sub

    Private Sub cmdBuscaClaveDiv_Click(sender As Object, e As EventArgs) Handles cmdBuscaClaveSuc.Click
        txtidColonia.Text = DespliegaGrid(UCase(TablaBd2), Inicio.CONSTR, CampoLLave)
        txtidSucursal_Leave(sender, e)
    End Sub

    Private Sub cmdCancelar_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        With TryCast(sender, Button)
            If .Enabled Then
                Me.CancelButton = sender
            End If
        End With
    End Sub
    Private Sub cmdTerminar_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        With TryCast(sender, Button)
            If .Enabled Then
                Me.CancelButton = sender
            End If
        End With
    End Sub

    Private Sub btnMnuOk_Click(sender As Object, e As EventArgs) Handles btnMnuOk.Click
        Dim CadCam As String = ""
        Try
            Salida = True
            If Not Validar() Then Exit Sub
            If GridTodos.RowCount = 0 Then
                Proveedor = New ProveedorClass(0)
            End If
            With Proveedor
                If OpcionForma = TipoOpcionForma.tOpcModificar Then
                    CadCam = .GetCambios(txtRazonSocial.Text, txtRFCProv.Text, txtDireccionProv.Text, txtidColonia.Text, txtEmailProv.Text, cmbTipoPersona.SelectedValue)
                    If CadCam <> "" Then
                        'If MessageBox.Show("Esta completamente Seguro que Desea guardar los Cambios Realizados al Estado Con id: " & .IDMARCA & vbNewLine &
                        '                CadCam, "Confirma que desea guardar los Cambios Realizados???", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then Exit Sub
                        Status("Guardando Modificaciones del Proveedor Con id: " & .idColonia, Me, 1, 2)

                        .Guardar(bEsIdentidad, txtRazonSocial.Text, txtRFCProv.Text, txtDireccionProv.Text, txtidColonia.Text, txtEmailProv.Text, cmbTipoPersona.Text)
                        Status("Proveedor con Id: " & .idColonia & " Modificado Con Exito!!!", Me, 2, 2)
                        MessageBox.Show("Proveedor con Id: " & .idColonia & " Modificada con exito!!!",
                                        "Accion terminada con exito!!!", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
                        ActualizaGrid(txtMenuBusca.Text)
                    Else
                        Status("No se detectaron cambios para realizar!!!", Me, Err:=True)
                    End If
                ElseIf OpcionForma = TipoOpcionForma.tOpcInsertar Then
                    .Existe = False
                    .RazonSocial = txtRazonSocial.Text
                    If Not bEsIdentidad Then
                        .idColonia = txtidProveedor.Text
                    End If
                    Status("Guardando Nuevo Proveedor: " & txtRazonSocial.Text & ". . .", Me, 1, 2)
                    .Guardar(bEsIdentidad, txtRazonSocial.Text, txtRFCProv.Text, txtDireccionProv.Text, txtidColonia.Text, txtEmailProv.Text, cmbTipoPersona.Text)
                    Status("Colonia: " & txtRazonSocial.Text & " Guardado con Exito!!!", Me, 2, 2)
                    MessageBox.Show("Nuevo Proveedor: " & txtRazonSocial.Text & " Guardado con exito!!!",
                                    "Accion terminada con exito!!!", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
                    ActualizaGrid(txtMenuBusca.Text)

                End If
            End With
            Salida = False
        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try
    End Sub


    Private Sub btnMnuCancelar_Click(sender As Object, e As EventArgs) Handles btnMnuCancelar.Click
        If txtidProveedor.Text <> "" Then
            CargaForm(txtidProveedor.Text, UCase(TablaBd))
        End If
        OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
    End Sub

    Private Sub btnMnuSalir_Click(sender As Object, e As EventArgs) Handles btnMnuSalir.Click
        Me.Close()
    End Sub

    Private Sub txtMenuBusca_TextChanged(sender As Object, e As EventArgs) Handles txtMenuBusca.TextChanged
        ActualizaGrid(txtMenuBusca.Text)
    End Sub

    Private Sub txtNomColonia_TextChanged(sender As Object, e As EventArgs) Handles txtRazonSocial.TextChanged

    End Sub

    Private Sub txtCP_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtRFCProv.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            txtEmailProv.Focus()
            txtEmailProv.SelectAll()
        End If
    End Sub

    Private Sub txtCP_MouseDown(sender As Object, e As MouseEventArgs) Handles txtRFCProv.MouseDown
        txtRFCProv.Focus()
        txtRFCProv.SelectAll()
    End Sub

    Private Sub txtCP_TextChanged(sender As Object, e As EventArgs) Handles txtRFCProv.TextChanged

    End Sub

    Private Sub cmbTipoPersona_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbTipoPersona.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            txtRFCProv.Focus()
            txtRFCProv.SelectAll()
        End If
    End Sub

    Private Sub cmbTipoPersona_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbTipoPersona.SelectedIndexChanged

    End Sub

    Private Sub txtEmailProv_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtEmailProv.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            txtDireccionProv.Focus()
            txtDireccionProv.SelectAll()
        End If
    End Sub

    Private Sub txtEmailProv_MouseDown(sender As Object, e As MouseEventArgs) Handles txtEmailProv.MouseDown
        txtEmailProv.Focus()
        txtEmailProv.SelectAll()
    End Sub

    Private Sub txtEmailProv_TextChanged(sender As Object, e As EventArgs) Handles txtEmailProv.TextChanged

    End Sub

    Private Sub txtidColonia_TextChanged(sender As Object, e As EventArgs) Handles txtidColonia.TextChanged

    End Sub

    Private Sub GridTodos_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles GridTodos.CellContentClick

    End Sub

    Private Sub txtDireccionProv_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtDireccionProv.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            txtidColonia.Focus()
            txtidColonia.SelectAll()
        End If

    End Sub

    Private Sub txtDireccionProv_MouseDown(sender As Object, e As MouseEventArgs) Handles txtDireccionProv.MouseDown
        txtDireccionProv.Focus()
        txtDireccionProv.SelectAll()
    End Sub

    Private Sub txtDireccionProv_TextChanged(sender As Object, e As EventArgs) Handles txtDireccionProv.TextChanged

    End Sub
End Class
