'Fecha: 15 / Febrero / 2016
'ROGER GALA PACHECO
'************************************************************************************************************************************
'*  Fecha Modificacion :   15 / Febrero / 2016                                                                                       
'*  
'************************************************************************************************************************************
Public Class frmCatPersonal
    Inherits System.Windows.Forms.Form
    Dim OpcionForma As TipoOpcionForma
    Dim TablaBd As String = "CatPersonal"
    Dim CampoLLave As String = "idPersonal"
    Dim TablaBd2 As String = "CatColonia"
    Private Colonia As ColoniaClass
    Private Personal As PersonalClass
    Private Empresa As EmpresaClass
    Private Depto As DeptosClass
    Private Puesto As PuestosClass


    Private _Tag As String
    Dim bEsIdentidad As Boolean = True
    Dim Salida As Boolean

    Dim DsCombo As New DataSet

    Dim StrSql As String
    Dim indice As Integer = 0
    Dim ArraySql() As String
    Dim ObjSql As New ToolSQLs

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Public WithEvents MeStatus1 As ControlStatus.MeStatus
    Friend WithEvents GridTodos As System.Windows.Forms.DataGridView
    Friend WithEvents btnMnuOk As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuCancelar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuSalir As System.Windows.Forms.ToolStripButton
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cmdBuscaClaveCol As System.Windows.Forms.Button
    Friend WithEvents txtidColonia As System.Windows.Forms.TextBox
    Friend WithEvents txtNomColonia As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtNomPais As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtNomEstado As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtNombre As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtTelefonoPersonal As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtDireccion As System.Windows.Forms.TextBox
    Friend WithEvents cmbEstatus As System.Windows.Forms.ComboBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtNomCiudad As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents cmdBuscaClaveEmp As System.Windows.Forms.Button
    Friend WithEvents txtidEmpresa As System.Windows.Forms.TextBox
    Friend WithEvents txtRazonSocial As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents cmdBuscaClaveDep As System.Windows.Forms.Button
    Friend WithEvents txtidDepto As System.Windows.Forms.TextBox
    Friend WithEvents txtNomDepto As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txtPrecioHora As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents cmdBuscaClavePue As System.Windows.Forms.Button
    Friend WithEvents txtidPuesto As System.Windows.Forms.TextBox
    Friend WithEvents txtNomPuesto As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txtApellidoMat As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtApellidoPat As System.Windows.Forms.TextBox
    Friend WithEvents GrupoCaptura As System.Windows.Forms.GroupBox
    Friend WithEvents ToolStripMenu As System.Windows.Forms.ToolStrip
    Friend WithEvents txtMenuBusca As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents btnMnuAltas As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuModificar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuEliminar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuReporte As System.Windows.Forms.ToolStripButton
    Friend WithEvents label1 As System.Windows.Forms.Label
    Friend WithEvents label2 As System.Windows.Forms.Label
    Friend WithEvents cmdBuscaClave As System.Windows.Forms.Button
    Friend WithEvents txtidPersonal As System.Windows.Forms.TextBox
    Friend WithEvents txtNombreCompleto As System.Windows.Forms.TextBox
    Private con As String
    Private v As Boolean

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCatPersonal))
        Me.txtidPersonal = New System.Windows.Forms.TextBox()
        Me.label1 = New System.Windows.Forms.Label()
        Me.txtNombreCompleto = New System.Windows.Forms.TextBox()
        Me.label2 = New System.Windows.Forms.Label()
        Me.GrupoCaptura = New System.Windows.Forms.GroupBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.cmdBuscaClaveDep = New System.Windows.Forms.Button()
        Me.txtidDepto = New System.Windows.Forms.TextBox()
        Me.txtNomDepto = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txtPrecioHora = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.cmdBuscaClavePue = New System.Windows.Forms.Button()
        Me.txtidPuesto = New System.Windows.Forms.TextBox()
        Me.txtNomPuesto = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtApellidoMat = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtApellidoPat = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.cmdBuscaClaveEmp = New System.Windows.Forms.Button()
        Me.txtidEmpresa = New System.Windows.Forms.TextBox()
        Me.txtRazonSocial = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtNomCiudad = New System.Windows.Forms.TextBox()
        Me.cmbEstatus = New System.Windows.Forms.ComboBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtTelefonoPersonal = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtDireccion = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtNomPais = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtNomEstado = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtNombre = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cmdBuscaClaveCol = New System.Windows.Forms.Button()
        Me.txtidColonia = New System.Windows.Forms.TextBox()
        Me.txtNomColonia = New System.Windows.Forms.TextBox()
        Me.cmdBuscaClave = New System.Windows.Forms.Button()
        Me.ToolStripMenu = New System.Windows.Forms.ToolStrip()
        Me.txtMenuBusca = New System.Windows.Forms.ToolStripTextBox()
        Me.btnMnuAltas = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuModificar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuEliminar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuReporte = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuOk = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuCancelar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuSalir = New System.Windows.Forms.ToolStripButton()
        Me.MeStatus1 = New ControlStatus.MeStatus()
        Me.GridTodos = New System.Windows.Forms.DataGridView()
        Me.GrupoCaptura.SuspendLayout()
        Me.ToolStripMenu.SuspendLayout()
        CType(Me.GridTodos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtidPersonal
        '
        Me.txtidPersonal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtidPersonal.Location = New System.Drawing.Point(109, 27)
        Me.txtidPersonal.MaxLength = 0
        Me.txtidPersonal.Name = "txtidPersonal"
        Me.txtidPersonal.Size = New System.Drawing.Size(69, 20)
        Me.txtidPersonal.TabIndex = 2
        '
        'label1
        '
        Me.label1.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.label1.Location = New System.Drawing.Point(6, 27)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(94, 20)
        Me.label1.TabIndex = 14
        Me.label1.Text = "No. Personal:"
        '
        'txtNombreCompleto
        '
        Me.txtNombreCompleto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNombreCompleto.Location = New System.Drawing.Point(109, 138)
        Me.txtNombreCompleto.MaxLength = 50
        Me.txtNombreCompleto.Name = "txtNombreCompleto"
        Me.txtNombreCompleto.ReadOnly = True
        Me.txtNombreCompleto.Size = New System.Drawing.Size(484, 20)
        Me.txtNombreCompleto.TabIndex = 10
        '
        'label2
        '
        Me.label2.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.label2.Location = New System.Drawing.Point(9, 138)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(104, 20)
        Me.label2.TabIndex = 16
        Me.label2.Text = "Nombre Completo:"
        '
        'GrupoCaptura
        '
        Me.GrupoCaptura.Controls.Add(Me.Label16)
        Me.GrupoCaptura.Controls.Add(Me.cmdBuscaClaveDep)
        Me.GrupoCaptura.Controls.Add(Me.txtidDepto)
        Me.GrupoCaptura.Controls.Add(Me.txtNomDepto)
        Me.GrupoCaptura.Controls.Add(Me.Label15)
        Me.GrupoCaptura.Controls.Add(Me.txtPrecioHora)
        Me.GrupoCaptura.Controls.Add(Me.Label14)
        Me.GrupoCaptura.Controls.Add(Me.cmdBuscaClavePue)
        Me.GrupoCaptura.Controls.Add(Me.txtidPuesto)
        Me.GrupoCaptura.Controls.Add(Me.txtNomPuesto)
        Me.GrupoCaptura.Controls.Add(Me.Label13)
        Me.GrupoCaptura.Controls.Add(Me.txtApellidoMat)
        Me.GrupoCaptura.Controls.Add(Me.Label12)
        Me.GrupoCaptura.Controls.Add(Me.txtApellidoPat)
        Me.GrupoCaptura.Controls.Add(Me.Label11)
        Me.GrupoCaptura.Controls.Add(Me.cmdBuscaClaveEmp)
        Me.GrupoCaptura.Controls.Add(Me.txtidEmpresa)
        Me.GrupoCaptura.Controls.Add(Me.txtRazonSocial)
        Me.GrupoCaptura.Controls.Add(Me.Label10)
        Me.GrupoCaptura.Controls.Add(Me.txtNomCiudad)
        Me.GrupoCaptura.Controls.Add(Me.cmbEstatus)
        Me.GrupoCaptura.Controls.Add(Me.Label9)
        Me.GrupoCaptura.Controls.Add(Me.Label8)
        Me.GrupoCaptura.Controls.Add(Me.txtTelefonoPersonal)
        Me.GrupoCaptura.Controls.Add(Me.Label7)
        Me.GrupoCaptura.Controls.Add(Me.txtDireccion)
        Me.GrupoCaptura.Controls.Add(Me.Label6)
        Me.GrupoCaptura.Controls.Add(Me.txtNomPais)
        Me.GrupoCaptura.Controls.Add(Me.Label4)
        Me.GrupoCaptura.Controls.Add(Me.txtNomEstado)
        Me.GrupoCaptura.Controls.Add(Me.Label5)
        Me.GrupoCaptura.Controls.Add(Me.txtNombre)
        Me.GrupoCaptura.Controls.Add(Me.Label3)
        Me.GrupoCaptura.Controls.Add(Me.cmdBuscaClaveCol)
        Me.GrupoCaptura.Controls.Add(Me.txtidColonia)
        Me.GrupoCaptura.Controls.Add(Me.txtNomColonia)
        Me.GrupoCaptura.Controls.Add(Me.label1)
        Me.GrupoCaptura.Controls.Add(Me.cmdBuscaClave)
        Me.GrupoCaptura.Controls.Add(Me.txtidPersonal)
        Me.GrupoCaptura.Controls.Add(Me.txtNombreCompleto)
        Me.GrupoCaptura.Controls.Add(Me.label2)
        Me.GrupoCaptura.Location = New System.Drawing.Point(168, 45)
        Me.GrupoCaptura.Name = "GrupoCaptura"
        Me.GrupoCaptura.Size = New System.Drawing.Size(641, 508)
        Me.GrupoCaptura.TabIndex = 3
        Me.GrupoCaptura.TabStop = False
        '
        'Label16
        '
        Me.Label16.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label16.Location = New System.Drawing.Point(6, 102)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(94, 20)
        Me.Label16.TabIndex = 56
        Me.Label16.Text = "Departamento:"
        '
        'cmdBuscaClaveDep
        '
        Me.cmdBuscaClaveDep.Image = CType(resources.GetObject("cmdBuscaClaveDep.Image"), System.Drawing.Image)
        Me.cmdBuscaClaveDep.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdBuscaClaveDep.Location = New System.Drawing.Point(186, 94)
        Me.cmdBuscaClaveDep.Name = "cmdBuscaClaveDep"
        Me.cmdBuscaClaveDep.Size = New System.Drawing.Size(32, 32)
        Me.cmdBuscaClaveDep.TabIndex = 8
        Me.cmdBuscaClaveDep.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'txtidDepto
        '
        Me.txtidDepto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtidDepto.Location = New System.Drawing.Point(109, 102)
        Me.txtidDepto.MaxLength = 3
        Me.txtidDepto.Name = "txtidDepto"
        Me.txtidDepto.Size = New System.Drawing.Size(69, 20)
        Me.txtidDepto.TabIndex = 7
        '
        'txtNomDepto
        '
        Me.txtNomDepto.Enabled = False
        Me.txtNomDepto.Location = New System.Drawing.Point(224, 106)
        Me.txtNomDepto.Name = "txtNomDepto"
        Me.txtNomDepto.ReadOnly = True
        Me.txtNomDepto.Size = New System.Drawing.Size(369, 20)
        Me.txtNomDepto.TabIndex = 9
        '
        'Label15
        '
        Me.Label15.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label15.Location = New System.Drawing.Point(3, 262)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(94, 20)
        Me.Label15.TabIndex = 52
        Me.Label15.Text = "Precio Hora;"
        '
        'txtPrecioHora
        '
        Me.txtPrecioHora.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPrecioHora.Location = New System.Drawing.Point(106, 262)
        Me.txtPrecioHora.MaxLength = 3
        Me.txtPrecioHora.Name = "txtPrecioHora"
        Me.txtPrecioHora.Size = New System.Drawing.Size(69, 20)
        Me.txtPrecioHora.TabIndex = 16
        '
        'Label14
        '
        Me.Label14.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label14.Location = New System.Drawing.Point(6, 227)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(94, 20)
        Me.Label14.TabIndex = 50
        Me.Label14.Text = "Id Puesto:"
        '
        'cmdBuscaClavePue
        '
        Me.cmdBuscaClavePue.Image = CType(resources.GetObject("cmdBuscaClavePue.Image"), System.Drawing.Image)
        Me.cmdBuscaClavePue.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdBuscaClavePue.Location = New System.Drawing.Point(186, 219)
        Me.cmdBuscaClavePue.Name = "cmdBuscaClavePue"
        Me.cmdBuscaClavePue.Size = New System.Drawing.Size(32, 32)
        Me.cmdBuscaClavePue.TabIndex = 15
        Me.cmdBuscaClavePue.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'txtidPuesto
        '
        Me.txtidPuesto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtidPuesto.Location = New System.Drawing.Point(109, 227)
        Me.txtidPuesto.MaxLength = 3
        Me.txtidPuesto.Name = "txtidPuesto"
        Me.txtidPuesto.Size = New System.Drawing.Size(69, 20)
        Me.txtidPuesto.TabIndex = 14
        '
        'txtNomPuesto
        '
        Me.txtNomPuesto.Enabled = False
        Me.txtNomPuesto.Location = New System.Drawing.Point(224, 231)
        Me.txtNomPuesto.Name = "txtNomPuesto"
        Me.txtNomPuesto.ReadOnly = True
        Me.txtNomPuesto.Size = New System.Drawing.Size(369, 20)
        Me.txtNomPuesto.TabIndex = 49
        '
        'Label13
        '
        Me.Label13.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label13.Location = New System.Drawing.Point(303, 190)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(94, 20)
        Me.Label13.TabIndex = 46
        Me.Label13.Text = "Ap. Materno:"
        '
        'txtApellidoMat
        '
        Me.txtApellidoMat.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtApellidoMat.Location = New System.Drawing.Point(406, 190)
        Me.txtApellidoMat.MaxLength = 13
        Me.txtApellidoMat.Name = "txtApellidoMat"
        Me.txtApellidoMat.Size = New System.Drawing.Size(187, 20)
        Me.txtApellidoMat.TabIndex = 13
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label12.Location = New System.Drawing.Point(6, 190)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(66, 13)
        Me.Label12.TabIndex = 44
        Me.Label12.Text = "Ap. Paterno:"
        '
        'txtApellidoPat
        '
        Me.txtApellidoPat.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtApellidoPat.Location = New System.Drawing.Point(106, 190)
        Me.txtApellidoPat.MaxLength = 13
        Me.txtApellidoPat.Name = "txtApellidoPat"
        Me.txtApellidoPat.Size = New System.Drawing.Size(187, 20)
        Me.txtApellidoPat.TabIndex = 12
        '
        'Label11
        '
        Me.Label11.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label11.Location = New System.Drawing.Point(6, 65)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(94, 20)
        Me.Label11.TabIndex = 42
        Me.Label11.Text = "Id Empresa:"
        '
        'cmdBuscaClaveEmp
        '
        Me.cmdBuscaClaveEmp.Image = CType(resources.GetObject("cmdBuscaClaveEmp.Image"), System.Drawing.Image)
        Me.cmdBuscaClaveEmp.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdBuscaClaveEmp.Location = New System.Drawing.Point(186, 57)
        Me.cmdBuscaClaveEmp.Name = "cmdBuscaClaveEmp"
        Me.cmdBuscaClaveEmp.Size = New System.Drawing.Size(32, 32)
        Me.cmdBuscaClaveEmp.TabIndex = 5
        Me.cmdBuscaClaveEmp.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'txtidEmpresa
        '
        Me.txtidEmpresa.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtidEmpresa.Location = New System.Drawing.Point(109, 65)
        Me.txtidEmpresa.MaxLength = 3
        Me.txtidEmpresa.Name = "txtidEmpresa"
        Me.txtidEmpresa.Size = New System.Drawing.Size(69, 20)
        Me.txtidEmpresa.TabIndex = 4
        '
        'txtRazonSocial
        '
        Me.txtRazonSocial.Enabled = False
        Me.txtRazonSocial.Location = New System.Drawing.Point(224, 69)
        Me.txtRazonSocial.Name = "txtRazonSocial"
        Me.txtRazonSocial.ReadOnly = True
        Me.txtRazonSocial.Size = New System.Drawing.Size(369, 20)
        Me.txtRazonSocial.TabIndex = 6
        '
        'Label10
        '
        Me.Label10.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label10.Location = New System.Drawing.Point(6, 388)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(94, 20)
        Me.Label10.TabIndex = 38
        Me.Label10.Text = "Ciudad:"
        '
        'txtNomCiudad
        '
        Me.txtNomCiudad.Enabled = False
        Me.txtNomCiudad.Location = New System.Drawing.Point(109, 388)
        Me.txtNomCiudad.Name = "txtNomCiudad"
        Me.txtNomCiudad.ReadOnly = True
        Me.txtNomCiudad.Size = New System.Drawing.Size(275, 20)
        Me.txtNomCiudad.TabIndex = 23
        '
        'cmbEstatus
        '
        Me.cmbEstatus.AutoCompleteCustomSource.AddRange(New String() {"Regularizaci�n", "Vivienda"})
        Me.cmbEstatus.FormattingEnabled = True
        Me.cmbEstatus.Location = New System.Drawing.Point(315, 257)
        Me.cmbEstatus.Name = "cmbEstatus"
        Me.cmbEstatus.Size = New System.Drawing.Size(184, 21)
        Me.cmbEstatus.TabIndex = 17
        '
        'Label9
        '
        Me.Label9.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label9.Location = New System.Drawing.Point(215, 262)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(91, 20)
        Me.Label9.TabIndex = 35
        Me.Label9.Text = "Estatus:"
        '
        'Label8
        '
        Me.Label8.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label8.Location = New System.Drawing.Point(9, 294)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(91, 20)
        Me.Label8.TabIndex = 34
        Me.Label8.Text = "Telefono:"
        '
        'txtTelefonoPersonal
        '
        Me.txtTelefonoPersonal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTelefonoPersonal.Location = New System.Drawing.Point(109, 294)
        Me.txtTelefonoPersonal.MaxLength = 50
        Me.txtTelefonoPersonal.Name = "txtTelefonoPersonal"
        Me.txtTelefonoPersonal.Size = New System.Drawing.Size(109, 20)
        Me.txtTelefonoPersonal.TabIndex = 18
        '
        'Label7
        '
        Me.Label7.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label7.Location = New System.Drawing.Point(9, 323)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(91, 20)
        Me.Label7.TabIndex = 32
        Me.Label7.Text = "Direcci�n:"
        '
        'txtDireccion
        '
        Me.txtDireccion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDireccion.Location = New System.Drawing.Point(109, 323)
        Me.txtDireccion.MaxLength = 50
        Me.txtDireccion.Name = "txtDireccion"
        Me.txtDireccion.Size = New System.Drawing.Size(484, 20)
        Me.txtDireccion.TabIndex = 19
        '
        'Label6
        '
        Me.Label6.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label6.Location = New System.Drawing.Point(6, 440)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(94, 20)
        Me.Label6.TabIndex = 30
        Me.Label6.Text = "Pa�s:"
        '
        'txtNomPais
        '
        Me.txtNomPais.Enabled = False
        Me.txtNomPais.Location = New System.Drawing.Point(109, 440)
        Me.txtNomPais.Name = "txtNomPais"
        Me.txtNomPais.ReadOnly = True
        Me.txtNomPais.Size = New System.Drawing.Size(275, 20)
        Me.txtNomPais.TabIndex = 25
        '
        'Label4
        '
        Me.Label4.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label4.Location = New System.Drawing.Point(6, 414)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(94, 20)
        Me.Label4.TabIndex = 28
        Me.Label4.Text = "Estado:"
        '
        'txtNomEstado
        '
        Me.txtNomEstado.Enabled = False
        Me.txtNomEstado.Location = New System.Drawing.Point(109, 414)
        Me.txtNomEstado.Name = "txtNomEstado"
        Me.txtNomEstado.ReadOnly = True
        Me.txtNomEstado.Size = New System.Drawing.Size(275, 20)
        Me.txtNomEstado.TabIndex = 24
        '
        'Label5
        '
        Me.Label5.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label5.Location = New System.Drawing.Point(3, 164)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(94, 20)
        Me.Label5.TabIndex = 26
        Me.Label5.Text = "Nombre(s):"
        '
        'txtNombre
        '
        Me.txtNombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNombre.Location = New System.Drawing.Point(106, 164)
        Me.txtNombre.MaxLength = 13
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.Size = New System.Drawing.Size(187, 20)
        Me.txtNombre.TabIndex = 11
        '
        'Label3
        '
        Me.Label3.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label3.Location = New System.Drawing.Point(6, 358)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(94, 20)
        Me.Label3.TabIndex = 20
        Me.Label3.Text = "Id Colonia:"
        '
        'cmdBuscaClaveCol
        '
        Me.cmdBuscaClaveCol.Image = CType(resources.GetObject("cmdBuscaClaveCol.Image"), System.Drawing.Image)
        Me.cmdBuscaClaveCol.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdBuscaClaveCol.Location = New System.Drawing.Point(186, 350)
        Me.cmdBuscaClaveCol.Name = "cmdBuscaClaveCol"
        Me.cmdBuscaClaveCol.Size = New System.Drawing.Size(32, 32)
        Me.cmdBuscaClaveCol.TabIndex = 21
        Me.cmdBuscaClaveCol.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'txtidColonia
        '
        Me.txtidColonia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtidColonia.Location = New System.Drawing.Point(109, 358)
        Me.txtidColonia.MaxLength = 3
        Me.txtidColonia.Name = "txtidColonia"
        Me.txtidColonia.Size = New System.Drawing.Size(69, 20)
        Me.txtidColonia.TabIndex = 20
        '
        'txtNomColonia
        '
        Me.txtNomColonia.Enabled = False
        Me.txtNomColonia.Location = New System.Drawing.Point(224, 362)
        Me.txtNomColonia.Name = "txtNomColonia"
        Me.txtNomColonia.ReadOnly = True
        Me.txtNomColonia.Size = New System.Drawing.Size(275, 20)
        Me.txtNomColonia.TabIndex = 22
        '
        'cmdBuscaClave
        '
        Me.cmdBuscaClave.Image = CType(resources.GetObject("cmdBuscaClave.Image"), System.Drawing.Image)
        Me.cmdBuscaClave.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdBuscaClave.Location = New System.Drawing.Point(186, 19)
        Me.cmdBuscaClave.Name = "cmdBuscaClave"
        Me.cmdBuscaClave.Size = New System.Drawing.Size(32, 32)
        Me.cmdBuscaClave.TabIndex = 3
        Me.cmdBuscaClave.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'ToolStripMenu
        '
        Me.ToolStripMenu.AutoSize = False
        Me.ToolStripMenu.BackColor = System.Drawing.Color.Silver
        Me.ToolStripMenu.Dock = System.Windows.Forms.DockStyle.None
        Me.ToolStripMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.txtMenuBusca, Me.btnMnuAltas, Me.btnMnuModificar, Me.btnMnuEliminar, Me.btnMnuReporte, Me.btnMnuOk, Me.btnMnuCancelar, Me.btnMnuSalir})
        Me.ToolStripMenu.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow
        Me.ToolStripMenu.Location = New System.Drawing.Point(3, 0)
        Me.ToolStripMenu.Name = "ToolStripMenu"
        Me.ToolStripMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.ToolStripMenu.Size = New System.Drawing.Size(832, 42)
        Me.ToolStripMenu.TabIndex = 1
        Me.ToolStripMenu.Text = "ToolStrip1"
        '
        'txtMenuBusca
        '
        Me.txtMenuBusca.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.txtMenuBusca.Name = "txtMenuBusca"
        Me.txtMenuBusca.Size = New System.Drawing.Size(150, 42)
        Me.txtMenuBusca.ToolTipText = "Filtra Listado"
        '
        'btnMnuAltas
        '
        Me.btnMnuAltas.ForeColor = System.Drawing.Color.Red
        Me.btnMnuAltas.Image = CType(resources.GetObject("btnMnuAltas.Image"), System.Drawing.Image)
        Me.btnMnuAltas.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuAltas.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuAltas.Name = "btnMnuAltas"
        Me.btnMnuAltas.Size = New System.Drawing.Size(44, 39)
        Me.btnMnuAltas.Text = "&ALTAS"
        Me.btnMnuAltas.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnMnuAltas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuModificar
        '
        Me.btnMnuModificar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuModificar.Image = CType(resources.GetObject("btnMnuModificar.Image"), System.Drawing.Image)
        Me.btnMnuModificar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuModificar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuModificar.Name = "btnMnuModificar"
        Me.btnMnuModificar.Size = New System.Drawing.Size(74, 39)
        Me.btnMnuModificar.Text = "&MODIFICAR"
        Me.btnMnuModificar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnMnuModificar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuEliminar
        '
        Me.btnMnuEliminar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuEliminar.Image = CType(resources.GetObject("btnMnuEliminar.Image"), System.Drawing.Image)
        Me.btnMnuEliminar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuEliminar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuEliminar.Name = "btnMnuEliminar"
        Me.btnMnuEliminar.Size = New System.Drawing.Size(64, 39)
        Me.btnMnuEliminar.Text = "&ELIMINAR"
        Me.btnMnuEliminar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuReporte
        '
        Me.btnMnuReporte.ForeColor = System.Drawing.Color.Red
        Me.btnMnuReporte.Image = CType(resources.GetObject("btnMnuReporte.Image"), System.Drawing.Image)
        Me.btnMnuReporte.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuReporte.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuReporte.Name = "btnMnuReporte"
        Me.btnMnuReporte.Size = New System.Drawing.Size(59, 39)
        Me.btnMnuReporte.Text = "&REPORTE"
        Me.btnMnuReporte.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuOk
        '
        Me.btnMnuOk.ForeColor = System.Drawing.Color.Red
        Me.btnMnuOk.Image = Global.Fletera.My.Resources.Resources.Save1
        Me.btnMnuOk.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuOk.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuOk.Name = "btnMnuOk"
        Me.btnMnuOk.Size = New System.Drawing.Size(56, 39)
        Me.btnMnuOk.Text = "&GRABAR"
        Me.btnMnuOk.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuCancelar
        '
        Me.btnMnuCancelar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuCancelar.Image = Global.Fletera.My.Resources.Resources.signos_f_059
        Me.btnMnuCancelar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuCancelar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuCancelar.Name = "btnMnuCancelar"
        Me.btnMnuCancelar.Size = New System.Drawing.Size(71, 39)
        Me.btnMnuCancelar.Text = "&CANCELAR"
        Me.btnMnuCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuSalir
        '
        Me.btnMnuSalir.ForeColor = System.Drawing.Color.Red
        Me.btnMnuSalir.Image = Global.Fletera.My.Resources.Resources.Salida2
        Me.btnMnuSalir.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuSalir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuSalir.Name = "btnMnuSalir"
        Me.btnMnuSalir.Size = New System.Drawing.Size(41, 39)
        Me.btnMnuSalir.Text = "&SALIR"
        Me.btnMnuSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'MeStatus1
        '
        Me.MeStatus1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.MeStatus1.Location = New System.Drawing.Point(0, 560)
        Me.MeStatus1.Margin = New System.Windows.Forms.Padding(4)
        Me.MeStatus1.Name = "MeStatus1"
        Me.MeStatus1.Size = New System.Drawing.Size(835, 26)
        Me.MeStatus1.TabIndex = 72
        '
        'GridTodos
        '
        Me.GridTodos.AllowUserToAddRows = False
        Me.GridTodos.AllowUserToDeleteRows = False
        Me.GridTodos.AllowUserToResizeRows = False
        Me.GridTodos.BackgroundColor = System.Drawing.Color.White
        Me.GridTodos.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.GridTodos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.GridTodos.Location = New System.Drawing.Point(3, 45)
        Me.GridTodos.MultiSelect = False
        Me.GridTodos.Name = "GridTodos"
        Me.GridTodos.ReadOnly = True
        Me.GridTodos.RowHeadersVisible = False
        Me.GridTodos.Size = New System.Drawing.Size(162, 508)
        Me.GridTodos.TabIndex = 73
        '
        'frmCatPersonal
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(835, 586)
        Me.Controls.Add(Me.GridTodos)
        Me.Controls.Add(Me.MeStatus1)
        Me.Controls.Add(Me.ToolStripMenu)
        Me.Controls.Add(Me.GrupoCaptura)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmCatPersonal"
        Me.Text = "Catal�go de Personal"
        Me.GrupoCaptura.ResumeLayout(False)
        Me.GrupoCaptura.PerformLayout()
        Me.ToolStripMenu.ResumeLayout(False)
        Me.ToolStripMenu.PerformLayout()
        CType(Me.GridTodos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region


    Sub New(ByVal vTag As String)
        InitializeComponent()
        _Tag = vTag
    End Sub

    Public Sub New(con As String, v As Boolean)
        InitializeComponent()
        Me.con = con
        BD = New CapaDatos.UtilSQL(con, "Juan")
        Inicio.CONSTR = con
        Me.v = v
    End Sub
#Region "MenusToolStrip"
    Private Sub LimpiaVariables()
        SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")

    End Sub
    Private Sub LimpiaCampos()
        txtidPersonal.Text = ""
        txtidEmpresa.Text = ""
        txtidDepto.Text = ""
        txtNombreCompleto.Text = ""
        txtNombre.Text = ""
        txtApellidoPat.Text = ""
        txtApellidoMat.Text = ""
        txtidPuesto.Text = ""
        txtPrecioHora.Text = ""
        cmbEstatus.SelectedIndex = 0
        txtTelefonoPersonal.Text = ""
        txtDireccion.Text = ""
        txtidColonia.Text = ""

        txtRazonSocial.Text = ""
        txtNomDepto.Text = ""
        txtNomPuesto.Text = ""
        txtNomColonia.Text = ""
        txtNomCiudad.Text = ""
        txtNomEstado.Text = ""
        txtNomPais.Text = ""

        LimpiaVariables()
    End Sub

    Private Sub ActivaCampos(ByVal valor As Boolean, ByVal vOpcion As TipoOpcActivaCampos)
        If vOpcion = TipoOpcActivaCampos.tOpcDESHABTODOS Then
            txtidPersonal.Enabled = False
            txtidEmpresa.Enabled = False
            txtidDepto.Enabled = False
            txtNombreCompleto.Enabled = False
            txtNombre.Enabled = False
            txtApellidoPat.Enabled = False
            txtApellidoMat.Enabled = False
            txtidPuesto.Enabled = False
            txtPrecioHora.Enabled = False
            cmbEstatus.Enabled = False
            txtTelefonoPersonal.Enabled = False
            txtDireccion.Enabled = False
            txtidColonia.Enabled = False

        ElseIf vOpcion = TipoOpcActivaCampos.tOpcINICIALIZA Then
            txtidPersonal.Enabled = Not valor
            txtidEmpresa.Enabled = valor
            txtidDepto.Enabled = valor
            txtNombreCompleto.Enabled = valor
            txtNombre.Enabled = valor
            txtApellidoPat.Enabled = valor
            txtApellidoMat.Enabled = valor
            txtidPuesto.Enabled = valor
            txtPrecioHora.Enabled = valor
            cmbEstatus.Enabled = valor
            txtTelefonoPersonal.Enabled = valor
            txtDireccion.Enabled = valor
            txtidColonia.Enabled = valor


        End If
    End Sub


    Private Sub ActivaBotones(ByVal valor As Boolean, Optional ByVal vOpcion As TipoOpcActivaBoton = TipoOpcActivaBoton.tOpcInicializa)
        If vOpcion = TipoOpcActivaBoton.tOpcConsulta Then
            btnMnuOk.Enabled = False
            btnMnuCancelar.Enabled = valor
            btnMnuSalir.Enabled = Not valor

            btnMnuAltas.Enabled = Not valor
            btnMnuModificar.Enabled = Not valor
            btnMnuEliminar.Enabled = Not valor
            btnMnuReporte.Enabled = Not valor
        ElseIf vOpcion = TipoOpcActivaBoton.tOpcInicializa Then
            btnMnuOk.Enabled = valor
            btnMnuCancelar.Enabled = valor
            btnMnuSalir.Enabled = Not valor

            btnMnuAltas.Enabled = Not valor
            btnMnuModificar.Enabled = Not valor
            btnMnuEliminar.Enabled = Not valor
            btnMnuReporte.Enabled = Not valor

            txtMenuBusca.Enabled = Not valor
            GridTodos.Enabled = Not valor
        ElseIf vOpcion = TipoOpcActivaBoton.tOpcInsertar Then
            btnMnuOk.Enabled = valor
            btnMnuCancelar.Enabled = valor
            btnMnuSalir.Enabled = Not valor

            btnMnuAltas.Enabled = Not valor
            btnMnuModificar.Enabled = Not valor
            btnMnuEliminar.Enabled = Not valor
            btnMnuReporte.Enabled = Not valor
        End If
    End Sub

    Private Sub btnMnuAltas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuAltas.Click
        'ALTAS
        '15/FEB/2016


        'If UserConectado.TieneAcceso(0, UsuarioClass.enOpcionTag.Alta) = False Then
        '    MessageBox.Show("No tiene los permisos Necesarios para poder dar de Alta en el Catalogo de Proveedor!!!",
        '        "Acceso Denegado a Alta de Proveedor", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    OpcionForma = TipoOpcionForma.tOpcConsultar
        '    Exit Sub
        'End If
        OpcionForma = TipoOpcionForma.tOpcInsertar
        LimpiaCampos()
        ActivaCampos(True, TipoOpcActivaCampos.tOpcINICIALIZA)
        ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)
        GridTodos.Enabled = False


        If bEsIdentidad Then
            txtidPersonal.Enabled = False
            txtNombreCompleto.Focus()
        Else
            txtidPersonal.Focus()
        End If
        Status("Ingrese nuevo Personal para guardar", Me)
    End Sub
    Private Sub btnMnuModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuModificar.Click
        'MODIFICAR
        'If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Modificar) = False Then
        '    MessageBox.Show("No tiene los permisos Necesarios para poder Modificar la Informacion del Proveedor!!!",
        '           "Acceso Denegado a Modificacion de la Informacion del Proveedor", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    OpcionForma = TipoOpcionForma.tOpcConsultar
        '    Exit Sub
        'End If
        If Trim(txtidPersonal.Text) = "" Then
            txtidPersonal.Text = DespliegaGrid(TablaBd, Inicio.CONSTR, CampoLLave)
            CargaForm(txtidPersonal.Text, UCase(TablaBd))
        End If

        OpcionForma = TipoOpcionForma.tOpcModificar
        ActivaCampos(True, TipoOpcActivaCampos.tOpcINICIALIZA)
        ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)

        GridTodos.Enabled = False
        btnMnuEliminar.Enabled = True
        If bEsIdentidad Then
            txtidPersonal.Enabled = False
            txtidEmpresa.Focus()
            txtidEmpresa.SelectAll()
            'txtNombreCompleto.Focus()
        Else
            txtidPersonal.Focus()
        End If

        Status("Ingrese Personal para guardar", Me)
    End Sub
    Private Sub btnMnuEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuEliminar.Click
        'ELIMINAR
        'If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Baja) = False Then
        '    MessageBox.Show("No tiene los permisos Necesarios para poder Eliminar el Proveedor!!!",
        '        "Acceso Denegado a Eliminacion de Proveedor", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    OpcionForma = TipoOpcionForma.tOpcConsultar
        '    Exit Sub
        'End If
        If txtidPersonal.Text = Personal.idPersonal Then
            With Personal
                If MessageBox.Show("Esta completamente seguro que desea Eliminar el Personal: " &
                                   .NombreCompleto & " Con Id: " & .idPersonal & " ????", "Confirma que desea eliminar el Personal: " & .NombreCompleto & "???",
                                 MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then Exit Sub
                Try
                    .Eliminar() 'Elimina el municipio
                    MessageBox.Show("Personal: " & .NombreCompleto & " Eliminado con exito!!!",
                                    "Personal Eliminado con exito", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Status("Personal: " & .NombreCompleto & " Eliminado con exito!!!", Me)
                    ActualizaGrid(txtMenuBusca.Text)
                    txtidPersonal.Text = ""
                    OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
                Catch ex As Exception
                    Status("Error al tratar de eliminar el Personal: " & ex.Message, Me, Err:=True)
                    MessageBox.Show("Error al Tratar de eliminar el Personal: " & .NombreCompleto & vbNewLine &
                                    "Error. " & ex.Message, "Error: " & Err.Number, MessageBoxButtons.OK, MessageBoxIcon.Error)

                End Try
            End With
        End If
    End Sub
    Private Sub btnMnuReporte_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuReporte.Click
        'REPORTE
        'If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Reporte) = False Then
        '    MessageBox.Show("No tiene los permisos Necesarios para poder realizar Reporte de Proveedor!!!",
        '        "Acceso Denegado a Reporte de Proveedor", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    OpcionForma = TipoOpcionForma.tOpcConsultar
        '    Exit Sub
        'End If
        ToolStripMenu.Enabled = False
        Status("Abriendo Reporte Espere. . . ", Me)
        Me.Cursor = Cursors.WaitCursor
        DespliegaReporte(TablaBd, CONSTR)
        Status("Finalizo el reporte. . . ", Me)
        Me.Cursor = Cursors.Default
        ToolStripMenu.Enabled = True
    End Sub

#End Region
    Private Sub ActualizaGrid(ByVal vNombreCompleto As String)
        If vNombreCompleto = "" Then
            GridTodos.DataSource = BD.ExecuteReturn("SELECT idPersonal, NombreCompleto FROM " & TablaBd, TablaBd)
            Status(GridTodos.RowCount & " Registros", Me)
        Else
            GridTodos.DataSource = BD.ExecuteReturn("SELECT idPersonal, NombreCompleto FROM " & TablaBd &
                                                " WHERE NombreCompleto Like '%" & vNombreCompleto & "%'")
            Status(GridTodos.RowCount & " Registros obtenidos con el filtro %" & vNombreCompleto & "%", Me)
        End If
    End Sub

    Private Sub OpcionMnuTools(ByVal vOpcion As TipoOpcionForma)
        If vOpcion <> TipoOpcionForma.tOpcImprimir And vOpcion <> TipoOpcionForma.tOpcEliminar Then
            OpcionForma = vOpcion
        End If
        txtMenuBusca.Enabled = True
        GridTodos.Enabled = True
        If vOpcion = TipoOpcionForma.tOpcConsultar Then
            'CONSULTA
            For Each ct As Control In GrupoCaptura.Controls
                If Not ct Is cmdBuscaClave Then
                    ct.Enabled = False
                Else
                    ct.Enabled = True
                End If
            Next
            txtMenuBusca.Enabled = True
            btnMnuAltas.Enabled = True
            btnMnuModificar.Enabled = True
            btnMnuEliminar.Enabled = False
            btnMnuReporte.Enabled = True
            btnMnuOk.Enabled = False
            btnMnuCancelar.Enabled = False

            btnMnuSalir.Enabled = True


        End If
    End Sub

    Private Sub txtMenuBusca_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtMenuBusca.KeyPress
        If Asc(e.KeyChar) = 39 Then e.Handled = True : Exit Sub 'Caracter ' Evita errores
        If Asc(e.KeyChar) = 13 Then
            GridTodos.Focus()
            e.Handled = True
        End If
    End Sub

    Private Sub frmCatProveedor_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Inicio.CONSTR = "" Then
            Inicio.CONSTR = con
        End If
        OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
        LlenaCombos()
        ActualizaGrid("")
        GridTodos.Columns(1).Width = GridTodos.Width - 20
        GridTodos.Columns(0).Visible = False
        txtMenuBusca.Focus()
    End Sub

    Private Function LlenaCombos() As Boolean
        Dim StrSql As String = ""
        LlenaCombos = True
        Try
            DsCombo.Clear()
            'StrSql = "SELECT DISTINCT per.Estatus, " &
            '"CASE WHEN per.Estatus = 'BAJ' THEN 'BAJA' ELSE 'ACTIVO' END AS NomEstatus " &
            '"FROM CatPersonal per"

            StrSql = "SELECT 'ACT' AS Estatus, 'ACTIVO' AS NomEstatus UNION SELECT 'BAJ' AS Estatus, 'BAJA' AS NomEstatus"


            DsCombo = Inicio.LlenaCombos("Estatus", "NomEstatus", "CatEstatus", _
                               TipoConexion.TcSQL, Inicio.CONSTR, Process.GetCurrentProcess.ProcessName, _
                               KEY_CHECAERROR, KEY_ESTATUS, , StrSql)
            'DsCombo = BD.ExecuteReturn(StrSql)
            If Not DsCombo Is Nothing Then
                If DsCombo.Tables("CatEstatus").DefaultView.Count > 0 Then
                    cmbEstatus.DataSource = DsCombo.Tables("CatEstatus")
                    cmbEstatus.ValueMember = "Estatus"
                    cmbEstatus.DisplayMember = "NomEstatus"
                End If
            Else
                MsgBox("No se han dado de alta a Estatus ", vbInformation, "Aviso" & Me.Text)
                LlenaCombos = False
                Exit Function
            End If
        Catch ex As Exception

        End Try
    End Function

    Private Sub GridTodos_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles GridTodos.CellEnter
        CargaForm(GridTodos.Rows(GridTodos.CurrentRow.Index).Cells(0).Value, UCase(TablaBd))
    End Sub

    Private Sub GridTodos_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles GridTodos.KeyPress
        If Asc(e.KeyChar) = 8 Then
            txtMenuBusca.Focus()
            e.Handled = True
        End If
    End Sub

    Private Sub CargaForm(ByVal idClave As Integer, ByVal NomTabla As String)
        Try
            Select Case UCase(NomTabla)
                Case UCase(TablaBd)
                    Personal = New PersonalClass(idClave)
                    With Personal
                        txtidPersonal.Text = .idPersonal
                        If .Existe Then
                            txtidEmpresa.Text = .idEmpresa
                            Empresa = New EmpresaClass(.idEmpresa)
                            If Empresa.Existe Then
                                txtRazonSocial.Text = Empresa.RazonSocial
                            End If

                            txtidDepto.Text = .idDepto
                            Depto = New DeptosClass(.idDepto)
                            If Depto.Existe Then
                                txtNomDepto.Text = Depto.NomDepto
                            End If

                            Dim vNombre As String = ""
                            Dim Nombre() As String = Split(.NombreCompleto, " ")

                            For i As Integer = 0 To Nombre.Length - 1
                                If i = 0 Then
                                    txtApellidoPat.Text = IIf(.ApellidoPat <> "", .ApellidoPat, Nombre(i))
                                ElseIf i = 1 Then
                                    txtApellidoMat.Text = IIf(.ApellidoMat <> "", .ApellidoMat, Nombre(i))
                                ElseIf i > 1 Then
                                    vNombre = IIf(.Nombre <> "", .Nombre, vNombre & " " & Nombre(i))
                                End If
                            Next
                            txtNombre.Text = Trim(vNombre)

                            txtNombreCompleto.Text = IIf(.NombreCompleto <> "", .NombreCompleto, txtNombre.Text & " " & txtApellidoPat.Text & " " & txtApellidoMat.Text)


                            txtidPuesto.Text = .idPuesto
                            Puesto = New PuestosClass(.idPuesto)
                            If Puesto.Existe Then
                                txtNomPuesto.Text = Puesto.NomPuesto
                            End If

                            txtPrecioHora.Text = .PrecioHora
                            'cmbEstatus.Text = IIf(.TipoPersona = "M", "MORAL", "FISICA")
                            cmbEstatus.SelectedValue = .Estatus
                            txtTelefonoPersonal.Text = .TelefonoPersonal
                            txtDireccion.Text = .Direccion

                            txtidColonia.Text = .idColonia
                            Colonia = New ColoniaClass(.idColonia)
                            If Colonia.Existe Then
                                txtNomColonia.Text = Colonia.NomColonia
                                txtNomCiudad.Text = Colonia.NomCiudad
                                txtNomEstado.Text = Colonia.NomEstado
                                txtNomPais.Text = Colonia.NomPais
                            End If
                            'txtNomColonia.Text = .
                            'txtCIDALMACEN_COM.Text = .CIDALMACEN_COM
                            If OpcionForma = TipoOpcionForma.tOpcInsertar Then
                                Status("El Personal Con id: " & .idPersonal & _
                                       " Ya existe!!!, Favor de modificarlo para poder dar de alta a otro Personal", Me, Err:=True)
                                MessageBox.Show("El Personal con id: " & .idColonia & _
                                                " YA EXISTE!!!, Favor de Cambiarlo y volver a intentarlo",
                                                "El Personal con id: " & .idColonia & " YA EXISTE!!!", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            End If
                        Else 'No existe 
                            If OpcionForma = TipoOpcionForma.tOpcInsertar Then
                                'For Each Ct As Control In GrupoCaptura.Controls
                                '    If Not Ct Is txtidAlmacen And Not Ct Is cmdBuscaClave Then
                                '        Ct.Enabled = True
                                '    Else
                                '        Ct.Enabled = False
                                '    End If
                                '    If TypeOf (Ct) Is TextBox Then
                                '        If Not Ct Is txtidAlmacen Then
                                '            TryCast(Ct, TextBox).Text = ""
                                '        End If
                                '    End If
                                'Next
                                ActivaCampos(True, TipoOpcActivaCampos.tOpcINICIALIZA)
                                ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)
                                btnMnuOk.Enabled = True
                                txtNombreCompleto.Focus()
                                Status("Listo para Guardar nuevo Personal con id: " & .idColonia, Me)
                            End If
                        End If
                    End With
                Case UCase(TablaBd2)
                    'aqui()
                    Colonia = New ColoniaClass(idClave)
                    With Colonia
                        txtidColonia.Text = .idColonia
                        If .Existe Then
                            txtNomColonia.Text = .NomColonia
                            txtNomCiudad.Text = .NomCiudad
                            txtNomEstado.Text = .NomEstado
                            txtNomPais.Text = .NomPais
                        Else
                            MsgBox("La Colonia con id: " & txtidColonia.Text & " No Existe")
                        End If
                    End With
                Case UCase("Empresa")
                    'aqui()
                    Empresa = New EmpresaClass(idClave)
                    With Empresa
                        txtidEmpresa.Text = .idEmpresa
                        If .Existe Then
                            txtRazonSocial.Text = .RazonSocial
                        Else
                            MsgBox("La Empresa con id: " & txtidEmpresa.Text & " No Existe")
                        End If
                    End With
                Case UCase("depto")
                    Depto = New DeptosClass(idClave)
                    With Depto
                        txtidDepto.Text = .IdDepto
                        If .Existe Then
                            txtNomDepto.Text = .NomDepto
                        Else
                            MsgBox("El Departamento con id: " & txtidDepto.Text & " No Existe")
                        End If

                    End With
                Case UCase("Puesto")
                    Puesto = New PuestosClass(idClave)
                    With Puesto
                        txtidPuesto.Text = .idPuesto
                        If .Existe Then
                            txtNomPuesto.Text = .NomPuesto
                        Else
                            MsgBox("El Puesto con id: " & txtidPuesto.Text & " No Existe")
                        End If

                    End With



            End Select



        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try

    End Sub
    Private Function Validar() As Boolean
        Validar = True
        'If Not Inicio.ValidandoCampo(Trim(txtidPersonal.Text), TipoDato.TdNumerico) Then
        '    If Not bEsIdentidad Then
        '        MsgBox(MENSAJE_CLAVE, vbInformation, "Aviso" & Me.Text)
        '        If txtidPersonal.Enabled Then
        '            txtidPersonal.Focus()
        '            txtidPersonal.SelectAll()
        '        End If
        '        Validar = False
        '    End If
        'Else
        If Not Inicio.ValidandoCampo(Trim(txtNombreCompleto.Text), TipoDato.TdCadena) Then
            MsgBox("El Nombre del Personal No Puede Ser Vacio", vbInformation, "Aviso" & Me.Text)
            If txtNombreCompleto.Enabled Then
                txtNombreCompleto.Focus()
                txtNombreCompleto.SelectAll()
            End If
            Validar = False
        ElseIf Not Inicio.ValidandoCampo(Trim(txtidColonia.Text), TipoDato.TdNumerico) Then
            MsgBox("La Colonia del Personal no Puede ser Vacio", vbInformation, "Aviso" & Me.Text)
            If txtidColonia.Enabled Then
                txtidColonia.Focus()
                txtidColonia.SelectAll()
            End If
            Validar = False
        ElseIf Not Inicio.ValidandoCampo(Trim(cmbEstatus.SelectedValue <> ""), TipoDato.TdCadena) Then
            MsgBox("El estatus no Puede ser Vacio", vbInformation, "Aviso" & Me.Text)
            If cmbEstatus.Enabled Then
                cmbEstatus.Focus()
                cmbEstatus.SelectAll()
            End If
            Validar = False

        End If
    End Function

    Private Sub txtidPersonal_EnabledChanged(sender As Object, e As EventArgs) Handles txtidPersonal.EnabledChanged

    End Sub

    'Private Sub txtidAlmacen_EnabledChanged(sender As Object, e As EventArgs) Handles txtidAlmacen.EnabledChanged
    '    cmdBuscaClave.Enabled = txtidAlmacen.Enabled
    'End Sub
    'CONTROLES
    Private Sub txtidAlmacen_KeyDown(sender As Object, e As KeyEventArgs) Handles txtidPersonal.KeyDown
        If e.KeyCode = Keys.F3 Then
            cmdBuscaClave_Click(sender, e)
        End If

    End Sub
    Private Sub txtidPersonal_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtidPersonal.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If txtidPersonal.Text.Trim <> "" Then
                CargaForm(txtidPersonal.Text, UCase(TablaBd))
            End If
            e.Handled = True
        End If
    End Sub
    Private Sub txtNomColonia_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtNombreCompleto.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            cmbEstatus.Focus()
        End If
    End Sub

    Private Sub cmdBuscaClave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBuscaClave.Click
        If Inicio.CONSTR = "" Then
            Inicio.CONSTR = con
        End If
        Dim idEst As String = DespliegaGrid(UCase(TablaBd), Inicio.CONSTR, CampoLLave)
        If idEst <> "" Then CargaForm(idEst, UCase(TablaBd))
    End Sub

    Private Sub txtNomColonia_MouseDown(sender As Object, e As MouseEventArgs) Handles txtNombreCompleto.MouseDown
        txtNombreCompleto.Focus()
        txtNombreCompleto.SelectAll()
    End Sub

    Private Sub txtidColonia_EnabledChanged(sender As Object, e As EventArgs) Handles txtidColonia.EnabledChanged
        cmdBuscaClaveCol.Enabled = txtidColonia.Enabled
    End Sub

    Private Sub txtidColonia_KeyDown(sender As Object, e As KeyEventArgs) Handles txtidColonia.KeyDown
        If e.KeyCode = Keys.F3 Then
            cmdBuscaClaveDiv_Click(sender, e)
        End If
    End Sub

    Private Sub txtidColonia_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtidColonia.KeyPress
        If txtidColonia.Text.Length = 0 Then
            txtidColonia.Focus()
        Else
            If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
                Salida = True
                CargaForm(txtidColonia.Text, UCase(TablaBd2))
                Salida = False
                'txtCIDALMACEN_COM.Focus()
                'txtCIDALMACEN_COM.SelectAll()
            End If
        End If
    End Sub

    Private Sub txtidColonia_Leave(sender As Object, e As EventArgs) Handles txtidColonia.Leave
        If Salida Then Exit Sub
        If txtidColonia.Text.Length = 0 Then
            txtidColonia.Focus()
            txtidColonia.SelectAll()
        Else
            If txtidColonia.Enabled Then
                CargaForm(txtidColonia.Text, UCase(TablaBd2))
                'txtCIDALMACEN_COM.Focus()
                'txtCIDALMACEN_COM.SelectAll()
            End If
        End If
    End Sub

    Private Sub txtidColonia_MouseDown(sender As Object, e As MouseEventArgs) Handles txtidColonia.MouseDown
        txtidColonia.Focus()
        txtidColonia.SelectAll()
    End Sub

    Private Sub cmdBuscaClaveDiv_Click(sender As Object, e As EventArgs) Handles cmdBuscaClaveCol.Click
        If Inicio.CONSTR = "" Then
            Inicio.CONSTR = con
        End If
        txtidColonia.Text = DespliegaGrid(UCase(TablaBd2), Inicio.CONSTR, CampoLLave)
        txtidColonia_Leave(sender, e)
    End Sub

    Private Sub cmdCancelar_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        With TryCast(sender, Button)
            If .Enabled Then
                Me.CancelButton = sender
            End If
        End With
    End Sub
    Private Sub cmdTerminar_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        With TryCast(sender, Button)
            If .Enabled Then
                Me.CancelButton = sender
            End If
        End With
    End Sub

    Private Function IniciarSesionSDK() As Boolean
        frmLoginSDK.Close()
        If frmLoginSDK.ShowDialog = Windows.Forms.DialogResult.OK Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function GrabaAgenteComercial() As String
        Dim Resp As String = "ERROR"
        '1.- Verificar que el Puesto Exista
        If IniciarSesionSDK() = False Then End

        lError = fActualizaClasificacion(1, 1, "PUESTO")
        If lError <> 0 Then
            FEspera.Close()
            DespliegaError(lError)
            fCierraEmpresa()
            fTerminaSDK()
        Else
            'Creo que esta mal
            'lError = fBuscaValorClasif(1, 1, Trim(txtNomPuesto.Text))
            lError = fBuscaValorClasif(1, 1, Trim(txtidPuesto.Text))
            If lError <> 0 Then
                'No Existe
                If lError <> 3 Then
                    FEspera.Close()
                    DespliegaError(lError)
                    fCierraEmpresa()
                    fTerminaSDK()

                Else
                    'DespliegaError(lError)
                    lError = fInsertaValorClasif()
                    If lError <> 0 Then
                        FEspera.Close()
                        DespliegaError(lError)
                        fCierraEmpresa()
                        fTerminaSDK()
                    Else
                        lError = fSetDatoValorClasif("CIDCLASIFICACION", 1)
                        If lError <> 0 Then
                            FEspera.Close()
                            DespliegaError(lError)
                            fCierraEmpresa()
                            fTerminaSDK()

                        Else
                            lError = fSetDatoValorClasif("CVALORCLASIFICACION", txtNomPuesto.Text)
                            If lError <> 0 Then
                                FEspera.Close()
                                DespliegaError(lError)
                                fCierraEmpresa()
                                fTerminaSDK()
                            Else
                                lError = fSetDatoValorClasif("CCODIGOVALORCLASIFICACION", IIf(CInt(txtidPuesto.Text) + 1 < 10, "0" & CInt(txtidPuesto.Text) + 1, CInt(txtidPuesto.Text) + 1))
                                If lError <> 0 Then
                                    FEspera.Close()
                                    DespliegaError(lError)
                                    fCierraEmpresa()
                                    fTerminaSDK()

                                Else
                                    lError = fGuardaValorClasif()
                                    If lError <> 0 Then
                                        FEspera.Close()
                                        DespliegaError(lError)
                                        fCierraEmpresa()
                                        fTerminaSDK()
                                    Else
                                        'Ahora se Graba el Agente
                                        lError = fBuscaAgente(txtNombre.Text & " " & txtApellidoPat.Text & " " & txtApellidoMat.Text)
                                        If lError <> 3 Then
                                            FEspera.Close()
                                            DespliegaError(lError)
                                            fCierraEmpresa()
                                            fTerminaSDK()
                                            'AQui debe de estar
                                        Else
                                            lError = fInsertaAgente()
                                            If lError <> 0 Then
                                                FEspera.Close()
                                                DespliegaError(lError)
                                                fCierraEmpresa()
                                                fTerminaSDK()
                                            Else
                                                'Se agrega Codigo
                                                lError = fSetDatoAgente("CCODIGOAGENTE", IIf(CInt(txtidPersonal.Text) + 1 < 10, "0" & CInt(txtidPersonal.Text) + 1, CInt(txtidPersonal.Text) + 1))
                                                If lError <> 0 Then
                                                    FEspera.Close()
                                                    DespliegaError(lError)
                                                    fCierraEmpresa()
                                                    fTerminaSDK()
                                                Else
                                                    'Se agrega Nombre
                                                    lError = fSetDatoAgente("CNOMBREAGENTE", txtNombre.Text & " " & txtApellidoPat.Text & " " & txtApellidoMat.Text)
                                                    If lError <> 0 Then
                                                        FEspera.Close()
                                                        DespliegaError(lError)
                                                        fCierraEmpresa()
                                                        fTerminaSDK()
                                                    Else
                                                        'Se agrega Puesto Con Update
                                                        StrSql = ObjSql.ActualizaCampoTabla("admAgentes", "CIDVALORCLASIFICACION1", TipoDato.TdNumerico, txtidPuesto.Text, _
                                                        "CCODIGOAGENTE", TipoDato.TdCadena, IIf(CInt(txtidPersonal.Text) + 1 < 10, "0" & CInt(txtidPersonal.Text) + 1, CInt(txtidPersonal.Text) + 1))
                                                        ReDim Preserve ArraySql(indice)
                                                        ArraySql(indice) = StrSql
                                                        indice += 1


                                                        'lError = fSetDatoAgente("CIDVALORCLASIFICACION1", txtidPuesto.Text)
                                                        If lError <> 0 Then
                                                            FEspera.Close()
                                                            DespliegaError(lError)
                                                            fCierraEmpresa()
                                                            fTerminaSDK()
                                                        Else
                                                            'GUARDA CAMBIOS
                                                            lError = fGuardaAgente()
                                                            If lError <> 0 Then
                                                                FEspera.Close()
                                                                DespliegaError(lError)
                                                                fCierraEmpresa()
                                                                fTerminaSDK()
                                                            Else
                                                                'Se Actualiza Manual
                                                                If indice > 0 Then
                                                                    Dim obj As New CapaNegocio.Tablas
                                                                    Dim Respuesta As String
                                                                    Respuesta = obj.EjecutarSql(Inicio.CONSTR_COM, "SQL", ArraySql, Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, indice)
                                                                    If Respuesta = "EFECTUADO" Then
                                                                        'MsgBox("Se Efectuo correctamente la migracion de Productos", MsgBoxStyle.Exclamation, Me.Text)
                                                                        'FEspera.Close()
                                                                        Resp = "CORRECTO"
                                                                    End If
                                                                End If
                                                            End If
                                                        End If
                                                    End If

                                                End If
                                            End If
                                            'aqui
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If


            Else
                'SI Existe ???? - NO SE DA DE ALTA

            End If
        End If
        Return Resp
    End Function




    Private Sub btnMnuOk_Click(sender As Object, e As EventArgs) Handles btnMnuOk.Click
        Dim CadCam As String = ""
        Try
            Salida = True
            If Not Validar() Then Exit Sub
            If GridTodos.RowCount = 0 Then
                Personal = New PersonalClass(0)
            End If
            With Personal
                If OpcionForma = TipoOpcionForma.tOpcModificar Then
                    CadCam = .GetCambios(Val(txtidPersonal.Text), txtidEmpresa.Text, txtNombreCompleto.Text, txtNombre.Text, txtApellidoPat.Text, txtApellidoMat.Text, txtidPuesto.Text, txtPrecioHora.Text, txtDireccion.Text, txtidColonia.Text, txtTelefonoPersonal.Text, cmbEstatus.SelectedValue, txtidDepto.Text, False)
                    If CadCam <> "" Then
                        'If MessageBox.Show("Esta completamente Seguro que Desea guardar los Cambios Realizados al Estado Con id: " & .IDMARCA & vbNewLine &
                        '                CadCam, "Confirma que desea guardar los Cambios Realizados???", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then Exit Sub
                        Status("Guardando Modificaciones del Proveedor Con id: " & .idColonia, Me, 1, 2)

                        .Guardar(bEsIdentidad, txtidPersonal.Text, txtidEmpresa.Text, txtNombreCompleto.Text, txtNombre.Text, txtApellidoPat.Text, txtApellidoMat.Text, txtidPuesto.Text, txtPrecioHora.Text, txtDireccion.Text, txtidColonia.Text, txtTelefonoPersonal.Text, cmbEstatus.SelectedValue, txtidDepto.Text, False)

                        'Primero Checar si es usuario con Comercial
                        If bEsUserComercial Then
                            'If GrabaAgenteComercial() = "CORRECTO" Then
                            '    FEspera.Show("Agente: " & txtidPersonal.Text & " " & txtNombre.Text & " " & _
                            '    txtApellidoPat.Text & " " & txtApellidoMat.Text & " Migrado a Comercial")
                            'End If
                        End If


                        Status("Personal con Id: " & .idPersonal & " Modificado Con Exito!!!", Me, 2, 2)
                        MessageBox.Show("Personal con Id: " & .idPersonal & " Modificada con exito!!!",
                                        "Accion terminada con exito!!!", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
                        ActualizaGrid(txtMenuBusca.Text)
                    Else
                        Status("No se detectaron cambios para realizar!!!", Me, Err:=True)
                    End If
                ElseIf OpcionForma = TipoOpcionForma.tOpcInsertar Then
                    .Existe = False
                    .NombreCompleto = txtNombreCompleto.Text
                    If Not bEsIdentidad Then
                        .idColonia = txtidPersonal.Text
                    End If
                    Status("Guardando Nuevo Proveedor: " & txtNombreCompleto.Text & ". . .", Me, 1, 2)
                    .Guardar(bEsIdentidad, Val(txtidPersonal.Text), txtidEmpresa.Text, txtNombreCompleto.Text, txtNombre.Text, txtApellidoPat.Text, txtApellidoMat.Text, txtidPuesto.Text, txtPrecioHora.Text, txtDireccion.Text, txtidColonia.Text, txtTelefonoPersonal.Text, cmbEstatus.SelectedValue, txtidDepto.Text, False)

                    'Primero Checar si es usuario con Comercial
                    If bEsUserComercial Then
                        'If GrabaAgenteComercial() = "CORRECTO" Then
                        '    FEspera.Show("Agente: " & txtidPersonal.Text & " " & txtNombre.Text & " " & _
                        '    txtApellidoPat.Text & " " & txtApellidoMat.Text & " Migrado a Comercial")

                        'End If
                    End If

                    Status("Personal con Id: " & .idPersonal & " Modificado Con Exito!!!", Me, 2, 2)
                    MessageBox.Show("Personal con Id: " & .idPersonal & " Modificada con exito!!!",
                                    "Accion terminada con exito!!!", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
                    ActualizaGrid(txtMenuBusca.Text)
                    GridTodos.Enabled = True

                End If
            End With
            Salida = False
        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try
    End Sub


    Private Sub btnMnuCancelar_Click(sender As Object, e As EventArgs) Handles btnMnuCancelar.Click
        If txtidPersonal.Text <> "" Then
            CargaForm(txtidPersonal.Text, UCase(TablaBd))
        End If
        OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
    End Sub

    Private Sub btnMnuSalir_Click(sender As Object, e As EventArgs) Handles btnMnuSalir.Click
        Me.Close()
    End Sub

    Private Sub txtMenuBusca_TextChanged(sender As Object, e As EventArgs) Handles txtMenuBusca.TextChanged
        ActualizaGrid(txtMenuBusca.Text)
    End Sub

    Private Sub txtNomColonia_TextChanged(sender As Object, e As EventArgs) Handles txtNombreCompleto.TextChanged

    End Sub













    Private Sub txtidEmpresa_EnabledChanged(sender As Object, e As EventArgs) Handles txtidEmpresa.EnabledChanged
        cmdBuscaClaveEmp.Enabled = txtidEmpresa.Enabled
    End Sub

    Private Sub txtidEmpresa_KeyDown(sender As Object, e As KeyEventArgs) Handles txtidEmpresa.KeyDown
        If e.KeyCode = Keys.F3 Then
            cmdBuscaClaveEmp_Click(sender, e)
        End If
    End Sub

    Private Sub txtidEmpresa_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtidEmpresa.KeyPress
        If txtidEmpresa.Text.Length = 0 Then
            txtidEmpresa.Focus()
        Else
            If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
                Salida = True
                CargaForm(txtidEmpresa.Text, "Empresa")
                Salida = False
                txtidDepto.Focus()
                txtidDepto.SelectAll()
            End If
        End If
    End Sub

    Private Sub txtidEmpresa_Leave(sender As Object, e As EventArgs) Handles txtidEmpresa.Leave
        If Salida Then Exit Sub
        If txtidEmpresa.Text.Length = 0 Then
            txtidEmpresa.Focus()
            txtidEmpresa.SelectAll()
        Else
            If txtidEmpresa.Enabled Then
                CargaForm(txtidEmpresa.Text, "Empresa")
                txtidDepto.Focus()
                txtidDepto.SelectAll()
            End If
        End If
    End Sub

    Private Sub txtidEmpresa_MouseDown(sender As Object, e As MouseEventArgs) Handles txtidEmpresa.MouseDown
        txtidEmpresa.Focus()
        txtidEmpresa.SelectAll()
    End Sub


    Private Sub txtidEmpresa_TextChanged(sender As Object, e As EventArgs) Handles txtidEmpresa.TextChanged

    End Sub

    Private Sub cmdBuscaClaveEmp_Click(sender As Object, e As EventArgs) Handles cmdBuscaClaveEmp.Click
        If Inicio.CONSTR = "" Then
            Inicio.CONSTR = con
        End If
        txtidEmpresa.Text = DespliegaGrid(UCase("CatEmpresas"), Inicio.CONSTR, CampoLLave)
        txtidEmpresa_Leave(sender, e)
    End Sub

    Private Sub txtidDepto_EnabledChanged(sender As Object, e As EventArgs) Handles txtidDepto.EnabledChanged
        cmdBuscaClaveDep.Enabled = txtidDepto.Enabled
    End Sub

    Private Sub txtidDepto_KeyDown(sender As Object, e As KeyEventArgs) Handles txtidDepto.KeyDown
        If e.KeyCode = Keys.F3 Then
            cmdBuscaClaveDep_Click(sender, e)
        End If
    End Sub

    Private Sub txtidDepto_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtidDepto.KeyPress
        If txtidDepto.Text.Length = 0 Then
            txtidDepto.Focus()
        Else
            If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
                Salida = True
                CargaForm(txtidDepto.Text, "Depto")
                Salida = False
                txtNombre.Focus()
                txtNombre.SelectAll()
            End If
        End If
    End Sub

    Private Sub txtidDepto_Leave(sender As Object, e As EventArgs) Handles txtidDepto.Leave
        If Salida Then Exit Sub
        If txtidDepto.Text.Length = 0 Then
            txtidDepto.Focus()
            txtidDepto.SelectAll()
        Else
            If txtidDepto.Enabled Then
                CargaForm(txtidDepto.Text, "Depto")
                txtNombre.Focus()
                txtNombre.SelectAll()
            End If
        End If
    End Sub

    Private Sub txtidDepto_MouseDown(sender As Object, e As MouseEventArgs) Handles txtidDepto.MouseDown
        txtidDepto.Focus()
        txtidDepto.SelectAll()
    End Sub

    Private Sub cmdBuscaClaveDep_Click(sender As Object, e As EventArgs) Handles cmdBuscaClaveDep.Click
        If Inicio.CONSTR = "" Then
            Inicio.CONSTR = con
        End If
        txtidDepto.Text = DespliegaGrid("CatDeptos", Inicio.CONSTR, CampoLLave)
        txtidDepto_Leave(sender, e)
    End Sub

    Private Sub NomCompleto()
        txtNombreCompleto.Text = txtNombre.Text & " " & txtApellidoPat.Text & " " & txtApellidoMat.Text
    End Sub


    Private Sub txtNombre_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtNombre.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            NomCompleto()
            txtApellidoPat.Focus()
            txtApellidoPat.SelectAll()
        End If
    End Sub

    Private Sub txtNombre_MouseDown(sender As Object, e As MouseEventArgs) Handles txtNombre.MouseDown
        txtNombre.Focus()
        txtNombre.SelectAll()
    End Sub

    Private Sub txtApellidoPat_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtApellidoPat.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            NomCompleto()
            txtApellidoMat.Focus()
            txtApellidoMat.SelectAll()
        End If
    End Sub

    Private Sub txtApellidoPat_MouseDown(sender As Object, e As MouseEventArgs) Handles txtApellidoPat.MouseDown
        txtApellidoPat.Focus()
        txtApellidoPat.SelectAll()

    End Sub

    Private Sub txtApellidoMat_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtApellidoMat.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            NomCompleto()
            txtidPuesto.Focus()
            txtidPuesto.SelectAll()
        End If
    End Sub

    Private Sub txtApellidoMat_MouseDown(sender As Object, e As MouseEventArgs) Handles txtApellidoMat.MouseDown
        txtApellidoMat.Focus()
        txtApellidoMat.SelectAll()
    End Sub

    Private Sub txtidPuesto_EnabledChanged(sender As Object, e As EventArgs) Handles txtidPuesto.EnabledChanged
        cmdBuscaClavePue.Enabled = txtidPuesto.Enabled
    End Sub

    Private Sub txtidPuesto_KeyDown(sender As Object, e As KeyEventArgs) Handles txtidPuesto.KeyDown
        If e.KeyCode = Keys.F3 Then
            cmdBuscaClavePue_Click(sender, e)
        End If
    End Sub

    Private Sub txtidPuesto_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtidPuesto.KeyPress
        If txtidPuesto.Text.Length = 0 Then
            txtidPuesto.Focus()
        Else
            If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
                Salida = True
                CargaForm(txtidPuesto.Text, "Puesto")
                Salida = False
                txtPrecioHora.Focus()
                txtPrecioHora.SelectAll()
            End If
        End If
    End Sub

    Private Sub txtidPuesto_Leave(sender As Object, e As EventArgs) Handles txtidPuesto.Leave
        If Salida Then Exit Sub
        If txtidPuesto.Text.Length = 0 Then
            txtidPuesto.Focus()
            txtidPuesto.SelectAll()
        Else
            If txtidPuesto.Enabled Then
                CargaForm(txtidPuesto.Text, "Puesto")
                txtPrecioHora.Focus()
                txtPrecioHora.SelectAll()
            End If
        End If
    End Sub

    Private Sub txtidPuesto_MouseDown(sender As Object, e As MouseEventArgs) Handles txtidPuesto.MouseDown
        txtidPuesto.Focus()
        txtidPuesto.SelectAll()
    End Sub

    Private Sub cmdBuscaClavePue_Click(sender As Object, e As EventArgs) Handles cmdBuscaClavePue.Click
        If Inicio.CONSTR = "" Then
            Inicio.CONSTR = con
        End If
        txtidPuesto.Text = DespliegaGrid("CatPuestos", Inicio.CONSTR, CampoLLave)
        txtidPuesto_Leave(sender, e)
    End Sub

    Private Sub txtPrecioHora_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPrecioHora.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            cmbEstatus.Focus()
        End If
    End Sub

    Private Sub txtPrecioHora_MouseDown(sender As Object, e As MouseEventArgs) Handles txtPrecioHora.MouseDown
        txtPrecioHora.Focus()
        txtPrecioHora.SelectAll()
    End Sub

    Private Sub cmbEstatus_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbEstatus.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            txtTelefonoPersonal.Focus()
            txtTelefonoPersonal.SelectAll()
        End If
    End Sub

    Private Sub txtTelefonoPersonal_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtTelefonoPersonal.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            txtDireccion.Focus()
            txtDireccion.SelectAll()
        End If
    End Sub

    Private Sub txtTelefonoPersonal_MouseDown(sender As Object, e As MouseEventArgs) Handles txtTelefonoPersonal.MouseDown
        txtTelefonoPersonal.Focus()
        txtTelefonoPersonal.SelectAll()
    End Sub

    Private Sub txtDireccionProv_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtDireccion.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            txtidColonia.Focus()
            txtidColonia.SelectAll()
        End If

    End Sub

    Private Sub txtDireccionProv_MouseDown(sender As Object, e As MouseEventArgs) Handles txtDireccion.MouseDown
        txtDireccion.Focus()
        txtDireccion.SelectAll()
    End Sub

    Private Sub txtidColonia_TextChanged(sender As Object, e As EventArgs) Handles txtidColonia.TextChanged

    End Sub

    Private Sub txtidPersonal_TextChanged(sender As Object, e As EventArgs) Handles txtidPersonal.TextChanged

    End Sub

    Private Sub txtNombre_TextChanged(sender As Object, e As EventArgs) Handles txtNombre.TextChanged
        NomCompleto()
    End Sub

    Private Sub txtApellidoPat_TextChanged(sender As Object, e As EventArgs) Handles txtApellidoPat.TextChanged
        NomCompleto()
    End Sub

    Private Sub txtApellidoMat_TextChanged(sender As Object, e As EventArgs) Handles txtApellidoMat.TextChanged
        NomCompleto()
    End Sub
End Class
