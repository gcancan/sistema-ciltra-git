'Fecha: 18 / Febrero / 2016
'ROGER GALA PACHECO
'************************************************************************************************************************************
'*  Fecha Modificacion :   18 / Febrero / 2016                                                                                       
'*  
'************************************************************************************************************************************
Public Class frmCatCombExtra
    Inherits System.Windows.Forms.Form
    Dim OpcionForma As TipoOpcionForma
    Dim TablaBd As String = "CatTipoOsComb"
    Dim CampoLLave As String = "idTipoOSComb"
    Public WithEvents MeStatus1 As ControlStatus.MeStatus
    Friend WithEvents GridTodos As System.Windows.Forms.DataGridView
    'Private TipoUnidad As TipoUniTransClass
    Private CombExtra As CatTipoOsCombClass

    Friend WithEvents btnMnuOk As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuCancelar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuSalir As System.Windows.Forms.ToolStripButton
    Private _Tag As String
    Dim bEsIdentidad As Boolean = True
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents chkRequiereViaje As System.Windows.Forms.CheckBox
    Dim Salida As Boolean
    Private con As String
    Friend WithEvents Label5 As Label
    Friend WithEvents cmbTipoUnidad As ComboBox
    Private v As Boolean
    Dim DsCombo As New DataSet

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents GrupoCaptura As System.Windows.Forms.GroupBox
    Friend WithEvents ToolStripMenu As System.Windows.Forms.ToolStrip
    Friend WithEvents txtMenuBusca As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents btnMnuAltas As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuModificar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuEliminar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuReporte As System.Windows.Forms.ToolStripButton
    Friend WithEvents label1 As System.Windows.Forms.Label
    Friend WithEvents label2 As System.Windows.Forms.Label
    Friend WithEvents cmdBuscaClave As System.Windows.Forms.Button
    Friend WithEvents txtidTipoOSComb As System.Windows.Forms.TextBox
    Friend WithEvents txtNomTipoOSComb As System.Windows.Forms.TextBox

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCatCombExtra))
        Me.txtidTipoOSComb = New System.Windows.Forms.TextBox()
        Me.label1 = New System.Windows.Forms.Label()
        Me.txtNomTipoOSComb = New System.Windows.Forms.TextBox()
        Me.label2 = New System.Windows.Forms.Label()
        Me.GrupoCaptura = New System.Windows.Forms.GroupBox()
        Me.cmbTipoUnidad = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.chkRequiereViaje = New System.Windows.Forms.CheckBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.cmdBuscaClave = New System.Windows.Forms.Button()
        Me.ToolStripMenu = New System.Windows.Forms.ToolStrip()
        Me.txtMenuBusca = New System.Windows.Forms.ToolStripTextBox()
        Me.btnMnuAltas = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuModificar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuEliminar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuReporte = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuOk = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuCancelar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuSalir = New System.Windows.Forms.ToolStripButton()
        Me.MeStatus1 = New ControlStatus.MeStatus()
        Me.GridTodos = New System.Windows.Forms.DataGridView()
        Me.GrupoCaptura.SuspendLayout()
        Me.ToolStripMenu.SuspendLayout()
        CType(Me.GridTodos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtidTipoOSComb
        '
        Me.txtidTipoOSComb.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtidTipoOSComb.Location = New System.Drawing.Point(122, 24)
        Me.txtidTipoOSComb.MaxLength = 3
        Me.txtidTipoOSComb.Name = "txtidTipoOSComb"
        Me.txtidTipoOSComb.Size = New System.Drawing.Size(69, 20)
        Me.txtidTipoOSComb.TabIndex = 2
        '
        'label1
        '
        Me.label1.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.label1.Location = New System.Drawing.Point(6, 27)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(94, 20)
        Me.label1.TabIndex = 14
        Me.label1.Text = "id Tipo:"
        '
        'txtNomTipoOSComb
        '
        Me.txtNomTipoOSComb.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNomTipoOSComb.Location = New System.Drawing.Point(122, 56)
        Me.txtNomTipoOSComb.MaxLength = 50
        Me.txtNomTipoOSComb.Name = "txtNomTipoOSComb"
        Me.txtNomTipoOSComb.Size = New System.Drawing.Size(322, 20)
        Me.txtNomTipoOSComb.TabIndex = 4
        '
        'label2
        '
        Me.label2.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.label2.Location = New System.Drawing.Point(9, 59)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(91, 20)
        Me.label2.TabIndex = 16
        Me.label2.Text = "Nombre:"
        '
        'GrupoCaptura
        '
        Me.GrupoCaptura.Controls.Add(Me.cmbTipoUnidad)
        Me.GrupoCaptura.Controls.Add(Me.Label4)
        Me.GrupoCaptura.Controls.Add(Me.chkRequiereViaje)
        Me.GrupoCaptura.Controls.Add(Me.Label5)
        Me.GrupoCaptura.Controls.Add(Me.label1)
        Me.GrupoCaptura.Controls.Add(Me.label2)
        Me.GrupoCaptura.Controls.Add(Me.cmdBuscaClave)
        Me.GrupoCaptura.Controls.Add(Me.txtidTipoOSComb)
        Me.GrupoCaptura.Controls.Add(Me.txtNomTipoOSComb)
        Me.GrupoCaptura.Location = New System.Drawing.Point(168, 45)
        Me.GrupoCaptura.Name = "GrupoCaptura"
        Me.GrupoCaptura.Size = New System.Drawing.Size(524, 222)
        Me.GrupoCaptura.TabIndex = 3
        Me.GrupoCaptura.TabStop = False
        '
        'cmbTipoUnidad
        '
        Me.cmbTipoUnidad.AutoCompleteCustomSource.AddRange(New String() {"Regularizaci�n", "Vivienda"})
        Me.cmbTipoUnidad.FormattingEnabled = True
        Me.cmbTipoUnidad.Items.AddRange(New Object() {"ACTIVO", "INACTIVO"})
        Me.cmbTipoUnidad.Location = New System.Drawing.Point(122, 82)
        Me.cmbTipoUnidad.Name = "cmbTipoUnidad"
        Me.cmbTipoUnidad.Size = New System.Drawing.Size(112, 21)
        Me.cmbTipoUnidad.TabIndex = 5
        '
        'Label4
        '
        Me.Label4.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label4.Location = New System.Drawing.Point(6, 110)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(94, 20)
        Me.Label4.TabIndex = 34
        Me.Label4.Text = "Requiere Viaje:"
        '
        'chkRequiereViaje
        '
        Me.chkRequiereViaje.AutoSize = True
        Me.chkRequiereViaje.Location = New System.Drawing.Point(122, 110)
        Me.chkRequiereViaje.Name = "chkRequiereViaje"
        Me.chkRequiereViaje.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.chkRequiereViaje.Size = New System.Drawing.Size(15, 14)
        Me.chkRequiereViaje.TabIndex = 6
        Me.chkRequiereViaje.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label5.Location = New System.Drawing.Point(6, 85)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(110, 20)
        Me.Label5.TabIndex = 26
        Me.Label5.Text = "Tipo Unidad:"
        '
        'cmdBuscaClave
        '
        Me.cmdBuscaClave.Image = CType(resources.GetObject("cmdBuscaClave.Image"), System.Drawing.Image)
        Me.cmdBuscaClave.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdBuscaClave.Location = New System.Drawing.Point(199, 16)
        Me.cmdBuscaClave.Name = "cmdBuscaClave"
        Me.cmdBuscaClave.Size = New System.Drawing.Size(32, 32)
        Me.cmdBuscaClave.TabIndex = 3
        Me.cmdBuscaClave.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'ToolStripMenu
        '
        Me.ToolStripMenu.AutoSize = False
        Me.ToolStripMenu.BackColor = System.Drawing.Color.Silver
        Me.ToolStripMenu.Dock = System.Windows.Forms.DockStyle.None
        Me.ToolStripMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.txtMenuBusca, Me.btnMnuAltas, Me.btnMnuModificar, Me.btnMnuEliminar, Me.btnMnuReporte, Me.btnMnuOk, Me.btnMnuCancelar, Me.btnMnuSalir})
        Me.ToolStripMenu.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow
        Me.ToolStripMenu.Location = New System.Drawing.Point(3, 0)
        Me.ToolStripMenu.Name = "ToolStripMenu"
        Me.ToolStripMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.ToolStripMenu.Size = New System.Drawing.Size(730, 42)
        Me.ToolStripMenu.TabIndex = 1
        Me.ToolStripMenu.Text = "ToolStrip1"
        '
        'txtMenuBusca
        '
        Me.txtMenuBusca.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.txtMenuBusca.Name = "txtMenuBusca"
        Me.txtMenuBusca.Size = New System.Drawing.Size(150, 42)
        Me.txtMenuBusca.ToolTipText = "Filtra Listado"
        '
        'btnMnuAltas
        '
        Me.btnMnuAltas.ForeColor = System.Drawing.Color.Red
        Me.btnMnuAltas.Image = CType(resources.GetObject("btnMnuAltas.Image"), System.Drawing.Image)
        Me.btnMnuAltas.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuAltas.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuAltas.Name = "btnMnuAltas"
        Me.btnMnuAltas.Size = New System.Drawing.Size(46, 39)
        Me.btnMnuAltas.Text = "&ALTAS"
        Me.btnMnuAltas.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnMnuAltas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuModificar
        '
        Me.btnMnuModificar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuModificar.Image = CType(resources.GetObject("btnMnuModificar.Image"), System.Drawing.Image)
        Me.btnMnuModificar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuModificar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuModificar.Name = "btnMnuModificar"
        Me.btnMnuModificar.Size = New System.Drawing.Size(74, 39)
        Me.btnMnuModificar.Text = "&MODIFICAR"
        Me.btnMnuModificar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnMnuModificar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuEliminar
        '
        Me.btnMnuEliminar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuEliminar.Image = CType(resources.GetObject("btnMnuEliminar.Image"), System.Drawing.Image)
        Me.btnMnuEliminar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuEliminar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuEliminar.Name = "btnMnuEliminar"
        Me.btnMnuEliminar.Size = New System.Drawing.Size(64, 39)
        Me.btnMnuEliminar.Text = "&ELIMINAR"
        Me.btnMnuEliminar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuReporte
        '
        Me.btnMnuReporte.ForeColor = System.Drawing.Color.Red
        Me.btnMnuReporte.Image = CType(resources.GetObject("btnMnuReporte.Image"), System.Drawing.Image)
        Me.btnMnuReporte.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuReporte.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuReporte.Name = "btnMnuReporte"
        Me.btnMnuReporte.Size = New System.Drawing.Size(60, 39)
        Me.btnMnuReporte.Text = "&REPORTE"
        Me.btnMnuReporte.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuOk
        '
        Me.btnMnuOk.ForeColor = System.Drawing.Color.Red
        Me.btnMnuOk.Image = Global.Fletera.My.Resources.Resources.Save1
        Me.btnMnuOk.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuOk.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuOk.Name = "btnMnuOk"
        Me.btnMnuOk.Size = New System.Drawing.Size(56, 39)
        Me.btnMnuOk.Text = "&GRABAR"
        Me.btnMnuOk.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuCancelar
        '
        Me.btnMnuCancelar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuCancelar.Image = Global.Fletera.My.Resources.Resources.signos_f_059
        Me.btnMnuCancelar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuCancelar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuCancelar.Name = "btnMnuCancelar"
        Me.btnMnuCancelar.Size = New System.Drawing.Size(71, 39)
        Me.btnMnuCancelar.Text = "&CANCELAR"
        Me.btnMnuCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuSalir
        '
        Me.btnMnuSalir.ForeColor = System.Drawing.Color.Red
        Me.btnMnuSalir.Image = Global.Fletera.My.Resources.Resources.Salida2
        Me.btnMnuSalir.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuSalir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuSalir.Name = "btnMnuSalir"
        Me.btnMnuSalir.Size = New System.Drawing.Size(41, 39)
        Me.btnMnuSalir.Text = "&SALIR"
        Me.btnMnuSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'MeStatus1
        '
        Me.MeStatus1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.MeStatus1.Location = New System.Drawing.Point(0, 266)
        Me.MeStatus1.Margin = New System.Windows.Forms.Padding(4)
        Me.MeStatus1.Name = "MeStatus1"
        Me.MeStatus1.Size = New System.Drawing.Size(692, 26)
        Me.MeStatus1.TabIndex = 72
        '
        'GridTodos
        '
        Me.GridTodos.AllowUserToAddRows = False
        Me.GridTodos.AllowUserToDeleteRows = False
        Me.GridTodos.AllowUserToResizeRows = False
        Me.GridTodos.BackgroundColor = System.Drawing.Color.White
        Me.GridTodos.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.GridTodos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.GridTodos.Location = New System.Drawing.Point(3, 45)
        Me.GridTodos.MultiSelect = False
        Me.GridTodos.Name = "GridTodos"
        Me.GridTodos.ReadOnly = True
        Me.GridTodos.RowHeadersVisible = False
        Me.GridTodos.Size = New System.Drawing.Size(162, 222)
        Me.GridTodos.TabIndex = 73
        '
        'frmCatCombExtra
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(692, 292)
        Me.Controls.Add(Me.GridTodos)
        Me.Controls.Add(Me.MeStatus1)
        Me.Controls.Add(Me.ToolStripMenu)
        Me.Controls.Add(Me.GrupoCaptura)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmCatCombExtra"
        Me.Text = "Catal�go de Combustible Extra"
        Me.GrupoCaptura.ResumeLayout(False)
        Me.GrupoCaptura.PerformLayout()
        Me.ToolStripMenu.ResumeLayout(False)
        Me.ToolStripMenu.PerformLayout()
        CType(Me.GridTodos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region
    Sub New(ByVal vTag As String)
        InitializeComponent()
        _Tag = vTag
    End Sub

    Public Sub New(con As String, v As Boolean)
        Me.con = con
        BD = New CapaDatos.UtilSQL(con, "Juan")
        InitializeComponent()
        Me.v = v
    End Sub
#Region "MenusToolStrip"
    Private Sub LimpiaVariables()
        SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")

    End Sub
    Private Sub LimpiaCampos()
        txtidTipoOSComb.Text = ""
        txtNomTipoOSComb.Text = ""
        chkRequiereViaje.Checked = False
        cmbTipoUnidad.SelectedIndex = 0
        'txtNoLlantas.Text = ""
        'txtNoEjes.Text = ""
        'txtTonelajeMax.Text = ""
        LimpiaVariables()
    End Sub
    Private Function LlenaCombos() As Boolean
        Dim StrSql As String = ""
        LlenaCombos = True
        Try
            DsCombo.Clear()

            StrSql = "select 'RE' as TipoUnidad, 'REMOLQUE' as nomTipoUnidad " &
            "UNION " &
            "select 'TC' as TipoUnidad, 'TRACTOR' as nomTipoUnidad"

            DsCombo = Inicio.LlenaCombos("IdEstatus", "Estatus", "CatTipoUni",
                               TipoConexion.TcSQL, Inicio.CONSTR, Process.GetCurrentProcess.ProcessName,
                               KEY_CHECAERROR, KEY_ESTATUS, , StrSql)
            'DsCombo = BD.ExecuteReturn(StrSql)
            If Not DsCombo Is Nothing Then
                If DsCombo.Tables("CatTipoUni").DefaultView.Count > 0 Then
                    cmbTipoUnidad.DataSource = DsCombo.Tables("CatTipoUni")
                    cmbTipoUnidad.ValueMember = "TipoUnidad"
                    cmbTipoUnidad.DisplayMember = "nomTipoUnidad"
                End If
            Else
                MsgBox("No se han dado de alta a Estatus ", vbInformation, "Aviso" & Me.Text)
                LlenaCombos = False
                Exit Function
            End If
        Catch ex As Exception

        End Try
    End Function

    Private Sub ActivaCampos(ByVal valor As Boolean, ByVal vOpcion As TipoOpcActivaCampos)
        If vOpcion = TipoOpcActivaCampos.tOpcDESHABTODOS Then
            txtidTipoOSComb.Enabled = False
            txtNomTipoOSComb.Enabled = False
            'txtNoLlantas.Enabled = False
            'txtNoEjes.Enabled = False
            'txtTonelajeMax.Enabled = False
            chkRequiereViaje.Enabled = False
        ElseIf vOpcion = TipoOpcActivaCampos.tOpcINICIALIZA Then
            txtidTipoOSComb.Enabled = Not valor
            txtNomTipoOSComb.Enabled = valor
            'txtNoLlantas.Enabled = valor
            'txtNoEjes.Enabled = valor
            'txtTonelajeMax.Enabled = valor
            chkRequiereViaje.Enabled = valor
        End If
    End Sub


    Private Sub ActivaBotones(ByVal valor As Boolean, Optional ByVal vOpcion As TipoOpcActivaBoton = TipoOpcActivaBoton.tOpcInicializa)
        If vOpcion = TipoOpcActivaBoton.tOpcConsulta Then
            btnMnuOk.Enabled = False
            btnMnuCancelar.Enabled = valor
            btnMnuSalir.Enabled = Not valor

            btnMnuAltas.Enabled = Not valor
            btnMnuModificar.Enabled = Not valor
            btnMnuEliminar.Enabled = Not valor
            btnMnuReporte.Enabled = Not valor
        ElseIf vOpcion = TipoOpcActivaBoton.tOpcInicializa Then
            btnMnuOk.Enabled = valor
            btnMnuCancelar.Enabled = valor
            btnMnuSalir.Enabled = Not valor

            btnMnuAltas.Enabled = Not valor
            btnMnuModificar.Enabled = Not valor
            btnMnuEliminar.Enabled = Not valor
            btnMnuReporte.Enabled = Not valor

            txtMenuBusca.Enabled = Not valor
            GridTodos.Enabled = Not valor
        ElseIf vOpcion = TipoOpcActivaBoton.tOpcInsertar Then
            btnMnuOk.Enabled = valor
            btnMnuCancelar.Enabled = valor
            btnMnuSalir.Enabled = Not valor

            btnMnuAltas.Enabled = Not valor
            btnMnuModificar.Enabled = Not valor
            btnMnuEliminar.Enabled = Not valor
            btnMnuReporte.Enabled = Not valor
        End If
    End Sub

    Private Sub btnMnuAltas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuAltas.Click
        'ALTAS
        '15/FEB/2016
        'If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Alta) = False Then
        '    MessageBox.Show("No tiene los permisos Necesarios para poder dar de Alta en el Catalogo de Tipo de Unidades de Trasporte!!!",
        '        "Acceso Denegado a Alta de Tipo de Unidades de Trasporte", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    OpcionForma = TipoOpcionForma.tOpcConsultar
        '    Exit Sub
        'End If
        OpcionForma = TipoOpcionForma.tOpcInsertar
        LimpiaCampos()
        ActivaCampos(True, TipoOpcActivaCampos.tOpcINICIALIZA)
        ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)

        If bEsIdentidad Then
            txtidTipoOSComb.Enabled = False
            txtNomTipoOSComb.Focus()
        Else
            txtidTipoOSComb.Focus()
        End If
        Status("Ingrese nuevo Tipo de Unidades de Trasporte para guardar", Me)
    End Sub

    Private Sub btnMnuModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuModificar.Click
        'MODIFICAR
        'If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Modificar) = False Then
        '    MessageBox.Show("No tiene los permisos Necesarios para poder Modificar la Informacion de Tipo de Unidades de Trasporte!!!",
        '           "Acceso Denegado a Modificacion de la Informacion de Tipo de Unidades de Trasporte", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    OpcionForma = TipoOpcionForma.tOpcConsultar
        '    Exit Sub
        'End If
        If Trim(txtidTipoOSComb.Text) = "" Then
            txtidTipoOSComb.Text = DespliegaGrid(TablaBd, Inicio.CONSTR, CampoLLave)
            CargaForm(txtidTipoOSComb.Text, UCase(TablaBd))
        End If

        OpcionForma = TipoOpcionForma.tOpcModificar
        ActivaCampos(True, TipoOpcActivaCampos.tOpcINICIALIZA)
        ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)

        btnMnuEliminar.Enabled = True
        If bEsIdentidad Then
            txtidTipoOSComb.Enabled = False
            txtNomTipoOSComb.Focus()
        Else
            txtidTipoOSComb.Focus()
        End If

        Status("Ingrese Tipo de Unidades de Trasporte para guardar", Me)
    End Sub
    Private Sub btnMnuEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuEliminar.Click
        'ELIMINAR
        'If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Baja) = False Then
        '    MessageBox.Show("No tiene los permisos Necesarios para poder Eliminar el Tipo de Unidades de Trasporte!!!",
        '        "Acceso Denegado a Eliminacion de Tipo de Unidades de Trasporte", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    OpcionForma = TipoOpcionForma.tOpcConsultar
        '    Exit Sub
        'End If
        If txtidTipoOSComb.Text = CombExtra.idTipoOSComb Then
            With CombExtra
                If MessageBox.Show("Esta completamente seguro que desea Eliminar el Tipo de combustible extra: " &
                                   .NomTipoOSComb & " Con Id: " & .idTipoOSComb & " ????", "Confirma que desea eliminar el Tipo de combustible extra: " & .NomTipoOSComb & "???",
                                 MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then Exit Sub
                Try
                    '.Eliminar() 'Elimina el municipio
                    MessageBox.Show("Tipo de combustible extra: " & .NomTipoOSComb & " Eliminada con exito!!!",
                                    "Tipo de combustible extra Eliminado con exito", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Status("Tipo de combustible extra: " & .NomTipoOSComb & " Eliminado con exito!!!", Me)
                    ActualizaGrid(txtMenuBusca.Text)
                    txtidTipoOSComb.Text = ""
                    OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
                Catch ex As Exception
                    Status("Error al tratar de eliminar el Tipo de combustible extra: " & ex.Message, Me, Err:=True)
                    MessageBox.Show("Error al Tratar de eliminar el Tipo de combustible extra: " & .NomTipoOSComb & vbNewLine &
                                    "Error. " & ex.Message, "Error: " & Err.Number, MessageBoxButtons.OK, MessageBoxIcon.Error)

                End Try
            End With
        End If
    End Sub
    Private Sub btnMnuReporte_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuReporte.Click
        'REPORTE
        'If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Reporte) = False Then
        '    MessageBox.Show("No tiene los permisos Necesarios para poder realizar Reporte de Tipo de Unidades de Trasporte!!!",
        '        "Acceso Denegado a Reporte de Tipo de Unidades de Trasporte", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    OpcionForma = TipoOpcionForma.tOpcConsultar
        '    Exit Sub
        'End If
        ToolStripMenu.Enabled = False
        Status("Abriendo Reporte Espere. . . ", Me)
        Me.Cursor = Cursors.WaitCursor
        DespliegaReporte(TablaBd, CONSTR)
        Status("Finalizo el reporte. . . ", Me)
        Me.Cursor = Cursors.Default
        ToolStripMenu.Enabled = True
    End Sub

#End Region
    Private Sub ActualizaGrid(ByVal vNomTipoOSComb As String)
        If vNomTipoOSComb = "" Then
            GridTodos.DataSource = BD.ExecuteReturn("SELECT idTipoOSComb, NomTipoOSComb FROM " & TablaBd, TablaBd)
            Status(GridTodos.RowCount & " Registros", Me)
        Else
            GridTodos.DataSource = BD.ExecuteReturn("SELECT idTipoOSComb, NomTipoOSComb FROM " & TablaBd &
                                                " WHERE NomTipoOSComb Like '%" & vNomTipoOSComb & "%'")
            Status(GridTodos.RowCount & " Registros obtenidos con el filtro %" & vNomTipoOSComb & "%", Me)
        End If
    End Sub

    Private Sub OpcionMnuTools(ByVal vOpcion As TipoOpcionForma)
        If vOpcion <> TipoOpcionForma.tOpcImprimir And vOpcion <> TipoOpcionForma.tOpcEliminar Then
            OpcionForma = vOpcion
        End If
        txtMenuBusca.Enabled = True
        GridTodos.Enabled = True
        If vOpcion = TipoOpcionForma.tOpcConsultar Then
            'CONSULTA
            For Each ct As Control In GrupoCaptura.Controls
                If Not ct Is cmdBuscaClave Then
                    ct.Enabled = False
                Else
                    ct.Enabled = True
                End If
            Next
            txtMenuBusca.Enabled = True
            btnMnuAltas.Enabled = True
            btnMnuModificar.Enabled = True
            btnMnuEliminar.Enabled = False
            btnMnuReporte.Enabled = True
            btnMnuOk.Enabled = False
            btnMnuCancelar.Enabled = False

            btnMnuSalir.Enabled = True


        End If
    End Sub


    Private Sub txtMenuBusca_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtMenuBusca.KeyPress
        If Asc(e.KeyChar) = 39 Then e.Handled = True : Exit Sub 'Caracter ' Evita errores
        If Asc(e.KeyChar) = 13 Then
            GridTodos.Focus()
            e.Handled = True
        End If
    End Sub

    'Private Sub txtMenuBusca_KeyUp(sender As Object, e As KeyEventArgs) Handles txtMenuBusca.KeyUp
    '    If txtMenuBusca.Text.Trim = "" Then Exit Sub
    '    If e.KeyCode = Keys.Down Or e.KeyCode = Keys.Up Then
    '        GridTodos.Focus()
    '        e.Handled = True
    '        Exit Sub
    '    End If
    '    If e.KeyCode = Keys.Left Or e.KeyCode = Keys.Right Then Exit Sub
    '    ActualizaGrid(txtMenuBusca.Text)
    'End Sub



    Private Sub frmCatFamilias_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Inicio.CONSTR = "" Then
            Inicio.CONSTR = con
        End If
        LlenaCombos()
        OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
        ActualizaGrid("")
        GridTodos.Columns(1).Width = GridTodos.Width - 20
        GridTodos.Columns(0).Visible = False
        txtMenuBusca.Focus()
    End Sub
    Private Sub GridTodos_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles GridTodos.CellEnter
        CargaForm(GridTodos.Rows(GridTodos.CurrentRow.Index).Cells(0).Value, UCase(TablaBd))
    End Sub
    Private Sub GridTodos_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles GridTodos.KeyPress
        If Asc(e.KeyChar) = 8 Then
            txtMenuBusca.Focus()
            e.Handled = True
        End If
    End Sub
    Private Sub CargaForm(ByVal idClave As Integer, ByVal NomTabla As String)
        Try
            Select Case NomTabla
                Case UCase(TablaBd)
                    CombExtra = New CatTipoOsCombClass(idClave)
                    With CombExtra
                        txtidTipoOSComb.Text = .idTipoOSComb
                        If .Existe Then
                            txtNomTipoOSComb.Text = .NomTipoOSComb
                            chkRequiereViaje.Checked = .RequiereViaje
                            cmbTipoUnidad.SelectedValue = .TipoUnidad
                            If OpcionForma = TipoOpcionForma.tOpcInsertar Then
                                Status("El Tipo de commbustible extra Con id: " & .idTipoOSComb &
                                       " Ya existe!!!, Favor de modificarlo para poder dar de alta a otro Tipo de commbustible extra", Me, Err:=True)
                                MessageBox.Show("El Tipo de commbustible extra con id: " & .idTipoOSComb &
                                                " YA EXISTE!!!, Favor de Cambiarlo y volver a intentarlo",
                                                "EL Tipo de commbustible extra con id: " & .idTipoOSComb & " YA EXISTE!!!", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            End If
                        Else
                            If OpcionForma = TipoOpcionForma.tOpcInsertar Then
                                ActivaCampos(True, TipoOpcActivaCampos.tOpcINICIALIZA)
                                ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)
                                btnMnuOk.Enabled = True
                                txtNomTipoOSComb.Focus()
                                Status("Listo para Guardar nuevo Tipo de Combustible Extra con id: " & .idTipoOSComb, Me)
                            End If
                        End If
                    End With




            End Select



        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try

    End Sub
    Private Function Validar() As Boolean
        Validar = True
        If Not Inicio.ValidandoCampo(Trim(txtidTipoOSComb.Text), TipoDato.TdNumerico) Then
            If Not bEsIdentidad Then
                MsgBox(MENSAJE_CLAVE, vbInformation, "Aviso" & Me.Text)
                If txtidTipoOSComb.Enabled Then
                    txtidTipoOSComb.Focus()
                    txtidTipoOSComb.SelectAll()
                End If
                Validar = False
            End If
        ElseIf Not Inicio.ValidandoCampo(Trim(txtNomTipoOSComb.Text), TipoDato.TdCadena) Then
            MsgBox("El Nombre del Tipo de combustible extra No Puede Ser Vacio", vbInformation, "Aviso" & Me.Text)
            If txtNomTipoOSComb.Enabled Then
                txtNomTipoOSComb.Focus()
                txtNomTipoOSComb.SelectAll()
            End If
            Validar = False
            'ElseIf Not Inicio.ValidandoCampo(Trim(txtidFamilia.Text), TipoDato.TdNumerico) Then
            '    MsgBox("La Familia de la Actividad no Puede ser Vacio", vbInformation, "Aviso" & Me.Text)
            '    If txtidFamilia.Enabled Then
            '        txtidFamilia.Focus()
            '        txtidFamilia.SelectAll()
            '    End If
            '    Validar = False
        End If
    End Function

    'Private Sub txtidAlmacen_EnabledChanged(sender As Object, e As EventArgs) Handles txtidAlmacen.EnabledChanged
    '    cmdBuscaClave.Enabled = txtidAlmacen.Enabled
    'End Sub
    'CONTROLES
    Private Sub txtidAlmacen_KeyDown(sender As Object, e As KeyEventArgs) Handles txtidTipoOSComb.KeyDown
        If e.KeyCode = Keys.F3 Then
            cmdBuscaClave_Click(sender, e)
        End If

    End Sub
    Private Sub txtidAlmacen_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtidTipoOSComb.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If txtidTipoOSComb.Text.Trim <> "" Then
                CargaForm(txtidTipoOSComb.Text, UCase(TablaBd))
            End If
            e.Handled = True
        End If
    End Sub
    Private Sub txtnomTipoUniTras_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtNomTipoOSComb.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            cmbTipoUnidad.Focus()
            'txtNoLlantas.Focus()
            'txtNoLlantas.SelectAll()
        End If
    End Sub

    Private Sub cmdBuscaClave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBuscaClave.Click
        If Inicio.CONSTR = "" Then
            Inicio.CONSTR = Me.con
        End If
        Dim idEst As String = DespliegaGrid(UCase(TablaBd), Inicio.CONSTR, CampoLLave)
        If idEst <> "" Then CargaForm(idEst, UCase(TablaBd))
    End Sub

    Private Sub txtnomTipoUniTras_MouseDown(sender As Object, e As MouseEventArgs) Handles txtNomTipoOSComb.MouseDown
        txtNomTipoOSComb.Focus()
        txtNomTipoOSComb.SelectAll()
    End Sub
    Private Sub cmdCancelar_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        With TryCast(sender, Button)
            If .Enabled Then
                Me.CancelButton = sender
            End If
        End With
    End Sub
    Private Sub cmdTerminar_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        With TryCast(sender, Button)
            If .Enabled Then
                Me.CancelButton = sender
            End If
        End With
    End Sub

    Private Sub btnMnuOk_Click(sender As Object, e As EventArgs) Handles btnMnuOk.Click
        Dim CadCam As String = ""
        Try
            Salida = True
            If Not Validar() Then Exit Sub
            If GridTodos.RowCount = 0 Then
                CombExtra = New CatTipoOsCombClass(0)
            End If
            With CombExtra
                If OpcionForma = TipoOpcionForma.tOpcModificar Then
                    'CadCam = .GetCambios(txtnomTipoUniTras.Text, txtNoEjes.Text, txtNoLlantas.Text, txtTonelajeMax.Text, chkbCombustible.Checked, txtidClasLlan.Text)
                    CadCam = .GetCambios(txtNomTipoOSComb.Text, chkRequiereViaje.Checked, cmbTipoUnidad.SelectedValue)
                    If CadCam <> "" Then
                        'If MessageBox.Show("Esta completamente Seguro que Desea guardar los Cambios Realizados al Estado Con id: " & .IDMARCA & vbNewLine &
                        '                CadCam, "Confirma que desea guardar los Cambios Realizados???", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then Exit Sub
                        Status("Guardando Modificaciones del Tipo de combustible extra Con id: " & .idTipoOSComb, Me, 1, 2)
                        '.Guardar(txtnomTipoUniTras.Text, txtNoEjes.Text, bEsIdentidad, txtNoLlantas.Text, txtTonelajeMax.Text, chkbCombustible.Checked, txtidClasLlan.Text)
                        .Guardar(bEsIdentidad, txtNomTipoOSComb.Text, chkRequiereViaje.Checked, cmbTipoUnidad.SelectedValue)
                        Status("Tipo de combustible extra con Id: " & .idTipoOSComb & " Modificado Con Exito!!!", Me, 2, 2)
                        MessageBox.Show("Tipo de combustible extra con Id: " & .idTipoOSComb & " Modificada con exito!!!",
                                        "Accion terminada con exito!!!", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
                        ActualizaGrid(txtMenuBusca.Text)
                    Else
                        Status("No se detectaron cambios para realizar!!!", Me, Err:=True)
                    End If
                ElseIf OpcionForma = TipoOpcionForma.tOpcInsertar Then
                    .Existe = False
                    .NomTipoOSComb = txtNomTipoOSComb.Text
                    If Not bEsIdentidad Then
                        .idTipoOSComb = txtidTipoOSComb.Text
                    End If
                    Status("Guardando Nuevo Tipo de combustible extra: " & txtNomTipoOSComb.Text & ". . .", Me, 1, 2)
                    '.Guardar(txtnomTipoUniTras.Text, txtNoEjes.Text, bEsIdentidad, txtNoLlantas.Text, txtTonelajeMax.Text, chkbCombustible.Checked, txtidClasLlan.Text)
                    .Guardar(bEsIdentidad, txtNomTipoOSComb.Text, chkRequiereViaje.Checked, cmbTipoUnidad.SelectedValue)
                    Status("Tipo de combustible extra: " & txtNomTipoOSComb.Text & " Guardado con Exito!!!", Me, 2, 2)
                    MessageBox.Show("Tipo de combustible extra: " & txtNomTipoOSComb.Text & " Guardado con exito!!!",
                                    "Accion terminada con exito!!!", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
                    ActualizaGrid(txtMenuBusca.Text)

                End If
            End With
            Salida = False
        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try
    End Sub


    Private Sub btnMnuCancelar_Click(sender As Object, e As EventArgs) Handles btnMnuCancelar.Click
        If txtidTipoOSComb.Text <> "" Then
            CargaForm(txtidTipoOSComb.Text, UCase(TablaBd))
        End If
        OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
    End Sub

    Private Sub btnMnuSalir_Click(sender As Object, e As EventArgs) Handles btnMnuSalir.Click
        Me.Close()
    End Sub

    Private Sub txtMenuBusca_TextChanged(sender As Object, e As EventArgs) Handles txtMenuBusca.TextChanged
        ActualizaGrid(txtMenuBusca.Text)
    End Sub










    Private Sub GrupoCaptura_Enter(sender As Object, e As EventArgs) Handles GrupoCaptura.Enter

    End Sub

    Private Sub txtidTipoOSComb_TextChanged(sender As Object, e As EventArgs) Handles txtidTipoOSComb.TextChanged

    End Sub

    Private Sub txtNomTipoOSComb_TextChanged(sender As Object, e As EventArgs) Handles txtNomTipoOSComb.TextChanged

    End Sub

    Private Sub cmbTipoUnidad_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbTipoUnidad.SelectedIndexChanged

    End Sub

    Private Sub cmbTipoUnidad_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbTipoUnidad.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            chkRequiereViaje.Focus()
        End If
    End Sub

    Private Sub chkRequiereViaje_CheckedChanged(sender As Object, e As EventArgs) Handles chkRequiereViaje.CheckedChanged

    End Sub

    Private Sub chkRequiereViaje_KeyPress(sender As Object, e As KeyPressEventArgs) Handles chkRequiereViaje.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then

            btnMnuOk_Click(sender, e)

        End If
    End Sub
End Class
