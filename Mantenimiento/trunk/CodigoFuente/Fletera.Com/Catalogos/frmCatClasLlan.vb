'Fecha: 18 / Febrero / 2016
'ROGER GALA PACHECO
'************************************************************************************************************************************
'*  Fecha Modificacion :   18 / Febrero / 2016                                                                                       
'*  
'************************************************************************************************************************************
Imports Microsoft.VisualBasic.PowerPacks
Public Class frmCatClasLlan
    Inherits System.Windows.Forms.Form
    Dim OpcionForma As TipoOpcionForma
    Dim TablaBd As String = "CatClasLlan"
    Dim CampoLLave As String = "idClasLlan"
    Public WithEvents MeStatus1 As ControlStatus.MeStatus
    Friend WithEvents GridTodos As System.Windows.Forms.DataGridView
    'Private TipoUnidad As TipoUniTransClass
    Private ClasLlan As ClasLlanClass
    Friend WithEvents btnMnuOk As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuCancelar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuSalir As System.Windows.Forms.ToolStripButton
    Private _Tag As String
    Dim bEsIdentidad As Boolean = True
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtNoLlantas As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtNoEjes As System.Windows.Forms.TextBox
    Dim Salida As Boolean
    Private con As String
    Private v As Boolean
    Friend WithEvents gpoPosicion As GroupBox
    Friend WithEvents Label13 As Label
    Friend WithEvents txtTotEjes As TextBox
    Friend WithEvents Label33 As Label
    Friend WithEvents btnAgrega As Button
    Friend WithEvents btnDesAgrega As Button
    Friend WithEvents txtNumLlan As NumericUpDown
    Friend WithEvents txtNumEje As NumericUpDown
    Friend WithEvents lstRelacion As ListView
    Friend WithEvents colNoEje As ColumnHeader
    Friend WithEvents colNoLlantas As ColumnHeader
    Private _Usuario As String

    Private TablaEstruct As DataTable
    Private TablaDibujaEstruc As DataTable

    Dim rdbPosicion() As RadioButton

    Dim vTotEjes As Integer = 0
    Friend WithEvents txtTotLlan As TextBox
    Friend WithEvents GpoPosiciones As GroupBox
    'Friend WithEvents ShapeContainer1 As ShapeContainer
    'Friend WithEvents LineaPrinc As LineShape
    Dim vTotLlan As Integer = 0


#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents GrupoCaptura As System.Windows.Forms.GroupBox
    Friend WithEvents ToolStripMenu As System.Windows.Forms.ToolStrip
    Friend WithEvents txtMenuBusca As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents btnMnuAltas As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuModificar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuEliminar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuReporte As System.Windows.Forms.ToolStripButton
    Friend WithEvents label1 As System.Windows.Forms.Label
    Friend WithEvents label2 As System.Windows.Forms.Label
    Friend WithEvents cmdBuscaClave As System.Windows.Forms.Button
    Friend WithEvents txtidClasLlan As System.Windows.Forms.TextBox
    Friend WithEvents txtNombreClasLlan As System.Windows.Forms.TextBox

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCatClasLlan))
        Me.txtidClasLlan = New System.Windows.Forms.TextBox()
        Me.label1 = New System.Windows.Forms.Label()
        Me.txtNombreClasLlan = New System.Windows.Forms.TextBox()
        Me.label2 = New System.Windows.Forms.Label()
        Me.GrupoCaptura = New System.Windows.Forms.GroupBox()
        Me.gpoPosicion = New System.Windows.Forms.GroupBox()
        Me.txtTotLlan = New System.Windows.Forms.TextBox()
        Me.btnAgrega = New System.Windows.Forms.Button()
        Me.btnDesAgrega = New System.Windows.Forms.Button()
        Me.txtNumLlan = New System.Windows.Forms.NumericUpDown()
        Me.txtNumEje = New System.Windows.Forms.NumericUpDown()
        Me.lstRelacion = New System.Windows.Forms.ListView()
        Me.colNoEje = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colNoLlantas = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtTotEjes = New System.Windows.Forms.TextBox()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtNoEjes = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtNoLlantas = New System.Windows.Forms.TextBox()
        Me.ToolStripMenu = New System.Windows.Forms.ToolStrip()
        Me.txtMenuBusca = New System.Windows.Forms.ToolStripTextBox()
        Me.MeStatus1 = New ControlStatus.MeStatus()
        Me.GridTodos = New System.Windows.Forms.DataGridView()
        Me.GpoPosiciones = New System.Windows.Forms.GroupBox()
        'Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        'Me.LineaPrinc = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.btnMnuAltas = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuModificar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuEliminar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuReporte = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuOk = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuCancelar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuSalir = New System.Windows.Forms.ToolStripButton()
        Me.cmdBuscaClave = New System.Windows.Forms.Button()
        Me.GrupoCaptura.SuspendLayout()
        Me.gpoPosicion.SuspendLayout()
        CType(Me.txtNumLlan, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNumEje, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ToolStripMenu.SuspendLayout()
        CType(Me.GridTodos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GpoPosiciones.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtidClasLlan
        '
        Me.txtidClasLlan.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtidClasLlan.Location = New System.Drawing.Point(122, 24)
        Me.txtidClasLlan.Name = "txtidClasLlan"
        Me.txtidClasLlan.Size = New System.Drawing.Size(69, 20)
        Me.txtidClasLlan.TabIndex = 4
        '
        'label1
        '
        Me.label1.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.label1.Location = New System.Drawing.Point(6, 27)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(94, 20)
        Me.label1.TabIndex = 14
        Me.label1.Text = "id Tipo:"
        '
        'txtNombreClasLlan
        '
        Me.txtNombreClasLlan.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNombreClasLlan.Location = New System.Drawing.Point(122, 56)
        Me.txtNombreClasLlan.MaxLength = 50
        Me.txtNombreClasLlan.Name = "txtNombreClasLlan"
        Me.txtNombreClasLlan.Size = New System.Drawing.Size(322, 20)
        Me.txtNombreClasLlan.TabIndex = 6
        '
        'label2
        '
        Me.label2.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.label2.Location = New System.Drawing.Point(6, 59)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(91, 20)
        Me.label2.TabIndex = 16
        Me.label2.Text = "Nombre:"
        '
        'GrupoCaptura
        '
        Me.GrupoCaptura.Controls.Add(Me.GpoPosiciones)
        Me.GrupoCaptura.Controls.Add(Me.gpoPosicion)
        Me.GrupoCaptura.Controls.Add(Me.Label6)
        Me.GrupoCaptura.Controls.Add(Me.txtNoEjes)
        Me.GrupoCaptura.Controls.Add(Me.Label5)
        Me.GrupoCaptura.Controls.Add(Me.txtNoLlantas)
        Me.GrupoCaptura.Controls.Add(Me.label1)
        Me.GrupoCaptura.Controls.Add(Me.label2)
        Me.GrupoCaptura.Controls.Add(Me.cmdBuscaClave)
        Me.GrupoCaptura.Controls.Add(Me.txtidClasLlan)
        Me.GrupoCaptura.Controls.Add(Me.txtNombreClasLlan)
        Me.GrupoCaptura.Location = New System.Drawing.Point(168, 45)
        Me.GrupoCaptura.Name = "GrupoCaptura"
        Me.GrupoCaptura.Size = New System.Drawing.Size(611, 508)
        Me.GrupoCaptura.TabIndex = 3
        Me.GrupoCaptura.TabStop = False
        '
        'gpoPosicion
        '
        Me.gpoPosicion.Controls.Add(Me.txtTotLlan)
        Me.gpoPosicion.Controls.Add(Me.btnAgrega)
        Me.gpoPosicion.Controls.Add(Me.btnDesAgrega)
        Me.gpoPosicion.Controls.Add(Me.txtNumLlan)
        Me.gpoPosicion.Controls.Add(Me.txtNumEje)
        Me.gpoPosicion.Controls.Add(Me.lstRelacion)
        Me.gpoPosicion.Controls.Add(Me.Label13)
        Me.gpoPosicion.Controls.Add(Me.txtTotEjes)
        Me.gpoPosicion.Controls.Add(Me.Label33)
        Me.gpoPosicion.Location = New System.Drawing.Point(442, 153)
        Me.gpoPosicion.Name = "gpoPosicion"
        Me.gpoPosicion.Size = New System.Drawing.Size(159, 252)
        Me.gpoPosicion.TabIndex = 31
        Me.gpoPosicion.TabStop = False
        Me.gpoPosicion.Text = "Ubicaci�n"
        '
        'txtTotLlan
        '
        Me.txtTotLlan.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTotLlan.Location = New System.Drawing.Point(83, 212)
        Me.txtTotLlan.MaxLength = 10
        Me.txtTotLlan.Name = "txtTotLlan"
        Me.txtTotLlan.ReadOnly = True
        Me.txtTotLlan.Size = New System.Drawing.Size(58, 20)
        Me.txtTotLlan.TabIndex = 90
        Me.txtTotLlan.Tag = "1"
        Me.txtTotLlan.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'btnAgrega
        '
        Me.btnAgrega.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAgrega.Location = New System.Drawing.Point(120, 69)
        Me.btnAgrega.Name = "btnAgrega"
        Me.btnAgrega.Size = New System.Drawing.Size(31, 27)
        Me.btnAgrega.TabIndex = 89
        Me.btnAgrega.Text = "+"
        Me.btnAgrega.UseVisualStyleBackColor = True
        '
        'btnDesAgrega
        '
        Me.btnDesAgrega.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDesAgrega.Location = New System.Drawing.Point(83, 69)
        Me.btnDesAgrega.Name = "btnDesAgrega"
        Me.btnDesAgrega.Size = New System.Drawing.Size(31, 27)
        Me.btnDesAgrega.TabIndex = 89
        Me.btnDesAgrega.Text = "-"
        Me.btnDesAgrega.UseVisualStyleBackColor = True
        '
        'txtNumLlan
        '
        Me.txtNumLlan.Location = New System.Drawing.Point(84, 43)
        Me.txtNumLlan.Name = "txtNumLlan"
        Me.txtNumLlan.Size = New System.Drawing.Size(69, 20)
        Me.txtNumLlan.TabIndex = 88
        '
        'txtNumEje
        '
        Me.txtNumEje.Location = New System.Drawing.Point(83, 16)
        Me.txtNumEje.Name = "txtNumEje"
        Me.txtNumEje.Size = New System.Drawing.Size(69, 20)
        Me.txtNumEje.TabIndex = 87
        '
        'lstRelacion
        '
        Me.lstRelacion.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colNoEje, Me.colNoLlantas})
        Me.lstRelacion.Location = New System.Drawing.Point(9, 102)
        Me.lstRelacion.MultiSelect = False
        Me.lstRelacion.Name = "lstRelacion"
        Me.lstRelacion.Size = New System.Drawing.Size(137, 104)
        Me.lstRelacion.TabIndex = 86
        Me.lstRelacion.UseCompatibleStateImageBehavior = False
        Me.lstRelacion.View = System.Windows.Forms.View.Details
        '
        'colNoEje
        '
        Me.colNoEje.Text = "No. Eje"
        '
        'colNoLlantas
        '
        Me.colNoLlantas.Text = "No. Llantas"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label13.Location = New System.Drawing.Point(6, 50)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(61, 13)
        Me.Label13.TabIndex = 85
        Me.Label13.Text = "No. Llantas"
        '
        'txtTotEjes
        '
        Me.txtTotEjes.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTotEjes.Location = New System.Drawing.Point(9, 212)
        Me.txtTotEjes.MaxLength = 10
        Me.txtTotEjes.Name = "txtTotEjes"
        Me.txtTotEjes.ReadOnly = True
        Me.txtTotEjes.Size = New System.Drawing.Size(58, 20)
        Me.txtTotEjes.TabIndex = 82
        Me.txtTotEjes.Tag = "1"
        Me.txtTotEjes.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label33.Location = New System.Drawing.Point(6, 23)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(42, 13)
        Me.Label33.TabIndex = 81
        Me.Label33.Text = "No. Eje"
        '
        'Label6
        '
        Me.Label6.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label6.Location = New System.Drawing.Point(6, 95)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(94, 20)
        Me.Label6.TabIndex = 30
        Me.Label6.Text = "No. Ejes:"
        '
        'txtNoEjes
        '
        Me.txtNoEjes.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNoEjes.Location = New System.Drawing.Point(122, 92)
        Me.txtNoEjes.Name = "txtNoEjes"
        Me.txtNoEjes.Size = New System.Drawing.Size(109, 20)
        Me.txtNoEjes.TabIndex = 7
        Me.txtNoEjes.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label5
        '
        Me.Label5.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label5.Location = New System.Drawing.Point(6, 130)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(110, 20)
        Me.Label5.TabIndex = 26
        Me.Label5.Text = "No. Llantas:"
        '
        'txtNoLlantas
        '
        Me.txtNoLlantas.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNoLlantas.Location = New System.Drawing.Point(122, 127)
        Me.txtNoLlantas.Name = "txtNoLlantas"
        Me.txtNoLlantas.Size = New System.Drawing.Size(109, 20)
        Me.txtNoLlantas.TabIndex = 8
        Me.txtNoLlantas.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'ToolStripMenu
        '
        Me.ToolStripMenu.AutoSize = False
        Me.ToolStripMenu.BackColor = System.Drawing.Color.Silver
        Me.ToolStripMenu.Dock = System.Windows.Forms.DockStyle.None
        Me.ToolStripMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.txtMenuBusca, Me.btnMnuAltas, Me.btnMnuModificar, Me.btnMnuEliminar, Me.btnMnuReporte, Me.btnMnuOk, Me.btnMnuCancelar, Me.btnMnuSalir})
        Me.ToolStripMenu.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow
        Me.ToolStripMenu.Location = New System.Drawing.Point(3, 0)
        Me.ToolStripMenu.Name = "ToolStripMenu"
        Me.ToolStripMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.ToolStripMenu.Size = New System.Drawing.Size(793, 42)
        Me.ToolStripMenu.TabIndex = 1
        Me.ToolStripMenu.Text = "ToolStrip1"
        '
        'txtMenuBusca
        '
        Me.txtMenuBusca.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.txtMenuBusca.Name = "txtMenuBusca"
        Me.txtMenuBusca.Size = New System.Drawing.Size(150, 42)
        Me.txtMenuBusca.ToolTipText = "Filtra Listado"
        '
        'MeStatus1
        '
        Me.MeStatus1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.MeStatus1.Location = New System.Drawing.Point(0, 574)
        Me.MeStatus1.Margin = New System.Windows.Forms.Padding(4)
        Me.MeStatus1.Name = "MeStatus1"
        Me.MeStatus1.Size = New System.Drawing.Size(791, 26)
        Me.MeStatus1.TabIndex = 72
        '
        'GridTodos
        '
        Me.GridTodos.AllowUserToAddRows = False
        Me.GridTodos.AllowUserToDeleteRows = False
        Me.GridTodos.AllowUserToResizeRows = False
        Me.GridTodos.BackgroundColor = System.Drawing.Color.White
        Me.GridTodos.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.GridTodos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.GridTodos.Location = New System.Drawing.Point(3, 45)
        Me.GridTodos.MultiSelect = False
        Me.GridTodos.Name = "GridTodos"
        Me.GridTodos.ReadOnly = True
        Me.GridTodos.RowHeadersVisible = False
        Me.GridTodos.Size = New System.Drawing.Size(162, 508)
        Me.GridTodos.TabIndex = 73
        '
        'GpoPosiciones
        '
        'Me.GpoPosiciones.Controls.Add(Me.ShapeContainer1)
        'Me.GpoPosiciones.Location = New System.Drawing.Point(9, 153)
        'Me.GpoPosiciones.Name = "GpoPosiciones"
        'Me.GpoPosiciones.Size = New System.Drawing.Size(427, 349)
        'Me.GpoPosiciones.TabIndex = 42
        'Me.GpoPosiciones.TabStop = False
        'Me.GpoPosiciones.Text = "Posiciones"
        ''
        ''ShapeContainer1
        ''
        'Me.ShapeContainer1.Location = New System.Drawing.Point(3, 16)
        'Me.ShapeContainer1.Margin = New System.Windows.Forms.Padding(0)
        'Me.ShapeContainer1.Name = "ShapeContainer1"
        'Me.ShapeContainer1.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.LineaPrinc})
        'Me.ShapeContainer1.Size = New System.Drawing.Size(421, 330)
        'Me.ShapeContainer1.TabIndex = 2
        'Me.ShapeContainer1.TabStop = False
        '
        'LineaPrinc
        '
        'Me.LineaPrinc.BorderWidth = 10
        'Me.LineaPrinc.Name = "LineaPrinc"
        'Me.LineaPrinc.X1 = 186
        'Me.LineaPrinc.X2 = 186
        'Me.LineaPrinc.Y1 = 36
        'Me.LineaPrinc.Y2 = 280
        '
        'btnMnuAltas
        '
        Me.btnMnuAltas.ForeColor = System.Drawing.Color.Red
        Me.btnMnuAltas.Image = CType(resources.GetObject("btnMnuAltas.Image"), System.Drawing.Image)
        Me.btnMnuAltas.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuAltas.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuAltas.Name = "btnMnuAltas"
        Me.btnMnuAltas.Size = New System.Drawing.Size(46, 39)
        Me.btnMnuAltas.Text = "&ALTAS"
        Me.btnMnuAltas.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnMnuAltas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuModificar
        '
        Me.btnMnuModificar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuModificar.Image = CType(resources.GetObject("btnMnuModificar.Image"), System.Drawing.Image)
        Me.btnMnuModificar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuModificar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuModificar.Name = "btnMnuModificar"
        Me.btnMnuModificar.Size = New System.Drawing.Size(74, 39)
        Me.btnMnuModificar.Text = "&MODIFICAR"
        Me.btnMnuModificar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnMnuModificar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuEliminar
        '
        Me.btnMnuEliminar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuEliminar.Image = CType(resources.GetObject("btnMnuEliminar.Image"), System.Drawing.Image)
        Me.btnMnuEliminar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuEliminar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuEliminar.Name = "btnMnuEliminar"
        Me.btnMnuEliminar.Size = New System.Drawing.Size(64, 39)
        Me.btnMnuEliminar.Text = "&ELIMINAR"
        Me.btnMnuEliminar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnMnuEliminar.Visible = False
        '
        'btnMnuReporte
        '
        Me.btnMnuReporte.ForeColor = System.Drawing.Color.Red
        Me.btnMnuReporte.Image = CType(resources.GetObject("btnMnuReporte.Image"), System.Drawing.Image)
        Me.btnMnuReporte.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuReporte.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuReporte.Name = "btnMnuReporte"
        Me.btnMnuReporte.Size = New System.Drawing.Size(60, 39)
        Me.btnMnuReporte.Text = "&REPORTE"
        Me.btnMnuReporte.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuOk
        '
        Me.btnMnuOk.ForeColor = System.Drawing.Color.Red
        Me.btnMnuOk.Image = Global.Fletera.My.Resources.Resources.Save1
        Me.btnMnuOk.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuOk.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuOk.Name = "btnMnuOk"
        Me.btnMnuOk.Size = New System.Drawing.Size(56, 39)
        Me.btnMnuOk.Text = "&GRABAR"
        Me.btnMnuOk.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuCancelar
        '
        Me.btnMnuCancelar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuCancelar.Image = Global.Fletera.My.Resources.Resources.signos_f_059
        Me.btnMnuCancelar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuCancelar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuCancelar.Name = "btnMnuCancelar"
        Me.btnMnuCancelar.Size = New System.Drawing.Size(71, 39)
        Me.btnMnuCancelar.Text = "&CANCELAR"
        Me.btnMnuCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuSalir
        '
        Me.btnMnuSalir.ForeColor = System.Drawing.Color.Red
        Me.btnMnuSalir.Image = Global.Fletera.My.Resources.Resources.Salida2
        Me.btnMnuSalir.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuSalir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuSalir.Name = "btnMnuSalir"
        Me.btnMnuSalir.Size = New System.Drawing.Size(41, 39)
        Me.btnMnuSalir.Text = "&SALIR"
        Me.btnMnuSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'cmdBuscaClave
        '
        Me.cmdBuscaClave.Image = CType(resources.GetObject("cmdBuscaClave.Image"), System.Drawing.Image)
        Me.cmdBuscaClave.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdBuscaClave.Location = New System.Drawing.Point(199, 16)
        Me.cmdBuscaClave.Name = "cmdBuscaClave"
        Me.cmdBuscaClave.Size = New System.Drawing.Size(32, 32)
        Me.cmdBuscaClave.TabIndex = 5
        Me.cmdBuscaClave.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'frmCatClasLlan
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(791, 600)
        Me.Controls.Add(Me.GridTodos)
        Me.Controls.Add(Me.MeStatus1)
        Me.Controls.Add(Me.ToolStripMenu)
        Me.Controls.Add(Me.GrupoCaptura)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmCatClasLlan"
        Me.Text = "Catal�go de Clasificacion de Llantas"
        Me.GrupoCaptura.ResumeLayout(False)
        Me.GrupoCaptura.PerformLayout()
        Me.gpoPosicion.ResumeLayout(False)
        Me.gpoPosicion.PerformLayout()
        CType(Me.txtNumLlan, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNumEje, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ToolStripMenu.ResumeLayout(False)
        Me.ToolStripMenu.PerformLayout()
        CType(Me.GridTodos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GpoPosiciones.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region
    Sub New(ByVal vTag As String)
        InitializeComponent()
        _Tag = vTag
    End Sub

    Public Sub New(con As String, v As Boolean, nomUsuario As String)
        Me.con = con
        BD = New CapaDatos.UtilSQL(con, "Juan")
        InitializeComponent()
        _Usuario = nomUsuario
        Me.v = v
    End Sub
#Region "MenusToolStrip"
    Private Sub LimpiaVariables()
        SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")

    End Sub
    Private Sub LimpiaCampos()
        txtidClasLlan.Text = ""
        txtNombreClasLlan.Text = ""
        txtNoLlantas.Text = ""
        txtNoEjes.Text = ""
        LimpiaVariables()
    End Sub

    Private Sub ActivaCampos(ByVal valor As Boolean, ByVal vOpcion As TipoOpcActivaCampos)
        If vOpcion = TipoOpcActivaCampos.tOpcDESHABTODOS Then
            txtidClasLlan.Enabled = False
            txtNombreClasLlan.Enabled = False
            txtNoLlantas.Enabled = False
            txtNoEjes.Enabled = False
        ElseIf vOpcion = TipoOpcActivaCampos.tOpcINICIALIZA Then
            txtidClasLlan.Enabled = Not valor
            txtNombreClasLlan.Enabled = valor
            txtNoLlantas.Enabled = valor
            txtNoEjes.Enabled = valor

            If OpcionForma = TipoOpcionForma.tOpcInsertar Then
                gpoPosicion.Enabled = True
                GpoPosiciones.Enabled = True
            ElseIf OpcionForma = TipoOpcionForma.tOpcModificar Then
                gpoPosicion.Enabled = True
                GpoPosiciones.Enabled = True

            Else
                gpoPosicion.Enabled = False
                GpoPosiciones.Enabled = False

            End If
        End If
    End Sub


    Private Sub ActivaBotones(ByVal valor As Boolean, Optional ByVal vOpcion As TipoOpcActivaBoton = TipoOpcActivaBoton.tOpcInicializa)
        If vOpcion = TipoOpcActivaBoton.tOpcConsulta Then
            btnMnuOk.Enabled = False
            btnMnuCancelar.Enabled = valor
            btnMnuSalir.Enabled = Not valor

            btnMnuAltas.Enabled = Not valor
            btnMnuModificar.Enabled = Not valor
            btnMnuEliminar.Enabled = Not valor
            btnMnuReporte.Enabled = Not valor
        ElseIf vOpcion = TipoOpcActivaBoton.tOpcInicializa Then
            btnMnuOk.Enabled = valor
            btnMnuCancelar.Enabled = valor
            btnMnuSalir.Enabled = Not valor

            btnMnuAltas.Enabled = Not valor
            btnMnuModificar.Enabled = Not valor
            btnMnuEliminar.Enabled = Not valor
            btnMnuReporte.Enabled = Not valor

            txtMenuBusca.Enabled = Not valor
            GridTodos.Enabled = Not valor
        ElseIf vOpcion = TipoOpcActivaBoton.tOpcInsertar Then
            btnMnuOk.Enabled = valor
            btnMnuCancelar.Enabled = valor
            btnMnuSalir.Enabled = Not valor

            btnMnuAltas.Enabled = Not valor
            btnMnuModificar.Enabled = Not valor
            btnMnuEliminar.Enabled = Not valor
            btnMnuReporte.Enabled = Not valor
        End If
    End Sub

    Private Sub btnMnuAltas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuAltas.Click
        'ALTAS
        '15/FEB/2016
        'If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Alta) = False Then
        '    MessageBox.Show("No tiene los permisos Necesarios para poder dar de Alta en el Catalogo de Tipo de Unidades de Trasporte!!!",
        '        "Acceso Denegado a Alta de Tipo de Unidades de Trasporte", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    OpcionForma = TipoOpcionForma.tOpcConsultar
        '    Exit Sub
        'End If
        OpcionForma = TipoOpcionForma.tOpcInsertar
        LimpiaCampos()
        ActivaCampos(True, TipoOpcActivaCampos.tOpcINICIALIZA)
        ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)

        If bEsIdentidad Then
            txtidClasLlan.Enabled = False
            txtNombreClasLlan.Focus()
        Else
            txtidClasLlan.Focus()
        End If
        Status("Ingrese nueva Clasificaci�n de Llantas para guardar", Me)
    End Sub

    Private Sub btnMnuModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuModificar.Click
        'MODIFICAR
        'If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Modificar) = False Then
        '    MessageBox.Show("No tiene los permisos Necesarios para poder Modificar la Informacion de Tipo de Unidades de Trasporte!!!",
        '           "Acceso Denegado a Modificacion de la Informacion de Tipo de Unidades de Trasporte", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    OpcionForma = TipoOpcionForma.tOpcConsultar
        '    Exit Sub
        'End If
        If Trim(txtidClasLlan.Text) = "" Then
            txtidClasLlan.Text = DespliegaGrid(TablaBd, Inicio.CONSTR, CampoLLave)
            CargaForm(txtidClasLlan.Text, UCase(TablaBd))
        End If

        OpcionForma = TipoOpcionForma.tOpcModificar
        ActivaCampos(True, TipoOpcActivaCampos.tOpcINICIALIZA)
        ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)

        btnMnuEliminar.Enabled = True
        If bEsIdentidad Then
            txtidClasLlan.Enabled = False
            txtNombreClasLlan.Focus()
        Else
            txtidClasLlan.Focus()
        End If
        GridTodos.Enabled = False


        Status("Ingrese Clasificaci�n de Llantas para guardar", Me)
    End Sub
    Private Sub btnMnuEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuEliminar.Click
        'ELIMINAR
        'If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Baja) = False Then
        '    MessageBox.Show("No tiene los permisos Necesarios para poder Eliminar el Tipo de Unidades de Trasporte!!!",
        '        "Acceso Denegado a Eliminacion de Tipo de Unidades de Trasporte", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    OpcionForma = TipoOpcionForma.tOpcConsultar
        '    Exit Sub
        'End If
        'If txtidTipoUniTras.Text = TipoUnidad.idTipoUniTras Then
        '    With TipoUnidad
        '        If MessageBox.Show("Esta completamente seguro que desea Eliminar el Tipo de Unidades de Trasporte: " &
        '                           .nomTipoUniTras & " Con Id: " & .idTipoUniTras & " ????", "Confirma que desea eliminar el Tipo de Unidades de Trasporte: " & .nomTipoUniTras & "???",
        '                         MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then Exit Sub
        '        Try
        '            .Eliminar() 'Elimina el municipio
        '            MessageBox.Show("Tipo de Unidades de Trasporte: " & .nomTipoUniTras & " Eliminada con exito!!!",
        '                            "Tipo de Unidades de Trasporte Eliminado con exito", MessageBoxButtons.OK, MessageBoxIcon.Information)
        '            Status("Tipo de Unidades de Trasporte: " & .nomTipoUniTras & " Eliminado con exito!!!", Me)
        '            ActualizaGrid(txtMenuBusca.Text)
        '            txtidTipoUniTras.Text = ""
        '            OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
        '        Catch ex As Exception
        '            Status("Error al tratar de eliminar el Tipo de Unidades de Trasporte: " & ex.Message, Me, Err:=True)
        '            MessageBox.Show("Error al Tratar de eliminar el Tipo de Unidades de Trasporte: " & .nomTipoUniTras & vbNewLine &
        '                            "Error. " & ex.Message, "Error: " & Err.Number, MessageBoxButtons.OK, MessageBoxIcon.Error)

        '        End Try
        '    End With
        'End If
    End Sub
    Private Sub btnMnuReporte_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuReporte.Click
        'REPORTE
        'If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Reporte) = False Then
        '    MessageBox.Show("No tiene los permisos Necesarios para poder realizar Reporte de Tipo de Unidades de Trasporte!!!",
        '        "Acceso Denegado a Reporte de Tipo de Unidades de Trasporte", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    OpcionForma = TipoOpcionForma.tOpcConsultar
        '    Exit Sub
        'End If
        ToolStripMenu.Enabled = False
        Status("Abriendo Reporte Espere. . . ", Me)
        Me.Cursor = Cursors.WaitCursor
        DespliegaReporte(TablaBd, CONSTR)
        Status("Finalizo el reporte. . . ", Me)
        Me.Cursor = Cursors.Default
        ToolStripMenu.Enabled = True
    End Sub

#End Region
    Private Sub ActualizaGrid(ByVal vNombreClasLlan As String)
        If vNombreClasLlan = "" Then
            GridTodos.DataSource = BD.ExecuteReturn("SELECT idClasLlan, NombreClasLlan FROM " & TablaBd, TablaBd)
            Status(GridTodos.RowCount & " Registros", Me)
        Else
            GridTodos.DataSource = BD.ExecuteReturn("SELECT idClasLlan, NombreClasLlan FROM " & TablaBd &
                                                " WHERE NombreClasLlan Like '%" & vNombreClasLlan & "%'")
            Status(GridTodos.RowCount & " Registros obtenidos con el filtro %" & vNombreClasLlan & "%", Me)
        End If
    End Sub

    Private Sub OpcionMnuTools(ByVal vOpcion As TipoOpcionForma)
        If vOpcion <> TipoOpcionForma.tOpcImprimir And vOpcion <> TipoOpcionForma.tOpcEliminar Then
            OpcionForma = vOpcion
        End If
        txtMenuBusca.Enabled = True
        GridTodos.Enabled = True
        If vOpcion = TipoOpcionForma.tOpcConsultar Then
            'CONSULTA
            For Each ct As Control In GrupoCaptura.Controls
                If Not ct Is cmdBuscaClave Then
                    ct.Enabled = False
                Else
                    ct.Enabled = True
                End If
            Next
            txtMenuBusca.Enabled = True
            btnMnuAltas.Enabled = True
            btnMnuModificar.Enabled = True
            btnMnuEliminar.Enabled = False
            btnMnuReporte.Enabled = True
            btnMnuOk.Enabled = False
            btnMnuCancelar.Enabled = False

            btnMnuSalir.Enabled = True


        End If
    End Sub


    Private Sub txtMenuBusca_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtMenuBusca.KeyPress
        If Asc(e.KeyChar) = 39 Then e.Handled = True : Exit Sub 'Caracter ' Evita errores
        If Asc(e.KeyChar) = 13 Then
            GridTodos.Focus()
            e.Handled = True
        End If
    End Sub

    'Private Sub txtMenuBusca_KeyUp(sender As Object, e As KeyEventArgs) Handles txtMenuBusca.KeyUp
    '    If txtMenuBusca.Text.Trim = "" Then Exit Sub
    '    If e.KeyCode = Keys.Down Or e.KeyCode = Keys.Up Then
    '        GridTodos.Focus()
    '        e.Handled = True
    '        Exit Sub
    '    End If
    '    If e.KeyCode = Keys.Left Or e.KeyCode = Keys.Right Then Exit Sub
    '    ActualizaGrid(txtMenuBusca.Text)
    'End Sub



    Private Sub frmCatFamilias_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Inicio.CONSTR = "" Then
            Inicio.CONSTR = con
        End If
        OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
        ActualizaGrid("")
        GridTodos.Columns(1).Width = GridTodos.Width - 20
        GridTodos.Columns(0).Visible = False
        txtMenuBusca.Focus()
    End Sub
    Private Sub GridTodos_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles GridTodos.CellEnter
        CargaForm(GridTodos.Rows(GridTodos.CurrentRow.Index).Cells(0).Value, UCase(TablaBd))
    End Sub
    Private Sub GridTodos_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles GridTodos.KeyPress
        If Asc(e.KeyChar) = 8 Then
            txtMenuBusca.Focus()
            e.Handled = True
        End If
    End Sub

    Private Sub ActualizaMaximosEjes(ByVal vEjes As Integer)
        If vEjes > 0 Then
            txtNumEje.Minimum = 1
            txtNumEje.Maximum = vEjes
        Else
            txtNumEje.Enabled = False
        End If
    End Sub

    Private Sub ActualizaMaximosLlantas(ByVal vLlantas As Integer)
        If vLlantas > 0 Then
            txtNumLlan.Minimum = 1
            txtNumLlan.Maximum = vLlantas
        Else
            txtNumLlan.Enabled = False
        End If
    End Sub
    Private Sub CargaForm(ByVal idClave As Integer, ByVal NomTabla As String)
        Try
            Select Case NomTabla
                Case UCase(TablaBd)
                    ClasLlan = New ClasLlanClass(Val(idClave))
                    With ClasLlan
                        txtidClasLlan.Text = .idClasLlan
                        If .Existe Then
                            txtNombreClasLlan.Text = .NombreClasLlan
                            txtNoLlantas.Text = .NoLLantas
                            txtNoEjes.Text = .NoEjes

                            'Para Manejo de Estructura

                            TablaEstruct = New DataTable
                            TablaEstruct = ClasLlan.TablaPosLlanta(.idClasLlan)
                            If Not TablaEstruct Is Nothing Then
                                If TablaEstruct.Rows.Count > 0 Then
                                    DibujaEsquema(.idClasLlan, .NoLLantas)
                                    'Agrega al listvie
                                    'For i = 0
                                    lstRelacion.Items.Clear()
                                    For i = 0 To TablaEstruct.Rows.Count - 1
                                        Dim array(2) As String
                                        array(0) = TablaEstruct.DefaultView.Item(i).Item("EjeNo")
                                        array(1) = TablaEstruct.DefaultView.Item(i).Item("NoLlantas")
                                        Dim lvl As New ListViewItem(array)
                                        lvl.Tag = array
                                        lstRelacion.Items.Add(lvl)
                                    Next
                                    TotalesxLsView()
                                End If
                            Else

                            End If
                            ActualizaMaximosEjes(.NoEjes)
                            ActualizaMaximosLlantas(.NoLLantas)



                            'txtCIDALMACEN_COM.Text = .CIDALMACEN_COM
                            If OpcionForma = TipoOpcionForma.tOpcInsertar Then
                                Status("La Clasificacion de llanta Con id: " & .idClasLlan &
                                               " Ya existe!!!, Favor de modificarlo para poder dar de alta a otra Clasificacion de llanta", Me, Err:=True)
                                MessageBox.Show("Clasificacion de llanta con id: " & .idClasLlan &
                                                        " YA EXISTE!!!, Favor de Cambiarlo y volver a intentarlo",
                                                        "Clasificacion de llanta con id: " & .idClasLlan & " YA EXISTE!!!", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            End If
                        Else 'No existe 
                            If OpcionForma = TipoOpcionForma.tOpcInsertar Then
                                'For Each Ct As Control In GrupoCaptura.Controls
                                '    If Not Ct Is txtidAlmacen And Not Ct Is cmdBuscaClave Then
                                '        Ct.Enabled = True
                                '    Else
                                '        Ct.Enabled = False
                                '    End If
                                '    If TypeOf (Ct) Is TextBox Then
                                '        If Not Ct Is txtidAlmacen Then
                                '            TryCast(Ct, TextBox).Text = ""
                                '        End If
                                '    End If
                                'Next
                                ActivaCampos(True, TipoOpcActivaCampos.tOpcINICIALIZA)
                                ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)
                                btnMnuOk.Enabled = True
                                txtNombreClasLlan.Focus()
                                Status("Listo para Guardar nueva Clasificacion de llanta con id: " & .idClasLlan, Me)
                            End If
                        End If
                    End With
            End Select



        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try

    End Sub
    Private Function Validar() As Boolean
        Validar = True
        If Not Inicio.ValidandoCampo(Trim(txtidClasLlan.Text), TipoDato.TdNumerico) Then
            If Not bEsIdentidad Then
                MsgBox(MENSAJE_CLAVE, vbInformation, "Aviso" & Me.Text)
                If txtidClasLlan.Enabled Then
                    txtidClasLlan.Focus()
                    txtidClasLlan.SelectAll()
                End If
                Validar = False
            End If
        ElseIf Not Inicio.ValidandoCampo(Trim(txtNombreClasLlan.Text), TipoDato.TdCadena) Then
            MsgBox("El Nombre del Tipo de Unidades de Trasporte No Puede Ser Vacio", vbInformation, "Aviso" & Me.Text)
            If txtNombreClasLlan.Enabled Then
                txtNombreClasLlan.Focus()
                txtNombreClasLlan.SelectAll()
            End If
            Validar = False
            'ElseIf txtNoEjes.Text = "" Then

        ElseIf Not Inicio.ValidandoCampo(Trim(txtNoEjes.Text), TipoDato.TdNumerico) Then
            MsgBox("El No. de Ejes No Puede Ser Vacio", vbInformation, "Aviso" & Me.Text)
            If txtNoEjes.Enabled Then
                txtNoEjes.Focus()
                txtNoEjes.SelectAll()
            End If
            Validar = False
        ElseIf Not Inicio.ValidandoCampo(Trim(txtNoLlantas.Text), TipoDato.TdNumerico) Then
            MsgBox("El No. de Llantas No Puede Ser Vacio", vbInformation, "Aviso" & Me.Text)
            If txtNoLlantas.Enabled Then
                txtNoLlantas.Focus()
                txtNoLlantas.SelectAll()
            End If
            Validar = False
        End If
    End Function

    'Private Sub txtidAlmacen_EnabledChanged(sender As Object, e As EventArgs) Handles txtidAlmacen.EnabledChanged
    '    cmdBuscaClave.Enabled = txtidAlmacen.Enabled
    'End Sub
    'CONTROLES
    Private Sub txtidAlmacen_KeyDown(sender As Object, e As KeyEventArgs) Handles txtidClasLlan.KeyDown
        If e.KeyCode = Keys.F3 Then
            cmdBuscaClave_Click(sender, e)
        End If

    End Sub
    Private Sub txtidAlmacen_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtidClasLlan.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If txtidClasLlan.Text.Trim <> "" Then
                CargaForm(txtidClasLlan.Text, UCase(TablaBd))
            End If
            e.Handled = True
        End If
    End Sub
    Private Sub txtnomTipoUniTras_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtNombreClasLlan.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            txtNoEjes.Focus()
            txtNoEjes.SelectAll()

        End If
    End Sub

    Private Sub cmdBuscaClave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBuscaClave.Click
        If Inicio.CONSTR = "" Then
            Inicio.CONSTR = Me.con
        End If
        Dim idEst As String = DespliegaGrid(UCase(TablaBd), Inicio.CONSTR, CampoLLave)
        If idEst <> "" Then CargaForm(idEst, UCase(TablaBd))
    End Sub

    Private Sub txtnomTipoUniTras_MouseDown(sender As Object, e As MouseEventArgs) Handles txtNombreClasLlan.MouseDown
        txtNombreClasLlan.Focus()
        txtNombreClasLlan.SelectAll()
    End Sub
    Private Sub cmdCancelar_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        With TryCast(sender, Button)
            If .Enabled Then
                Me.CancelButton = sender
            End If
        End With
    End Sub
    Private Sub cmdTerminar_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        With TryCast(sender, Button)
            If .Enabled Then
                Me.CancelButton = sender
            End If
        End With
    End Sub

    Private Sub btnMnuOk_Click(sender As Object, e As EventArgs) Handles btnMnuOk.Click
        Dim CadCam As String = ""
        Try
            Salida = True
            If Not Validar() Then Exit Sub
            If GridTodos.RowCount = 0 Then
                ClasLlan = New ClasLlanClass(0)
            Else
                ClasLlan = New ClasLlanClass(txtidClasLlan.Text)
            End If
            With ClasLlan
                If OpcionForma = TipoOpcionForma.tOpcModificar Then
                    'CadCam = .GetCambios(txtnomTipoUniTras.Text, txtNoEjes.Text, txtNoLlantas.Text, txtTonelajeMax.Text, chkbCombustible.Checked, txtidClasLlan.Text)
                    CadCam = .GetCambios(txtNombreClasLlan.Text, txtNoEjes.Text, txtNoLlantas.Text)
                    If CadCam <> "" Then
                        'If MessageBox.Show("Esta completamente Seguro que Desea guardar los Cambios Realizados al Estado Con id: " & .IDMARCA & vbNewLine &
                        '                CadCam, "Confirma que desea guardar los Cambios Realizados???", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then Exit Sub
                        Status("Guardando Modificaciones de la Clasificacion de llantas con id: " & .idClasLlan, Me, 1, 2)
                        .Guardar(bEsIdentidad, txtNombreClasLlan.Text, txtNoEjes.Text, txtNoLlantas.Text, _Usuario)
                        Status("Clasificacion de llantas con Id: " & .idClasLlan & " Modificado Con Exito!!!", Me, 2, 2)
                        MessageBox.Show("Clasificacion de llantas con Id: " & .idClasLlan & " Modificada con exito!!!",
                                    "Accion terminada con exito!!!", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
                        ActualizaGrid(txtMenuBusca.Text)
                    Else
                        Status("No se detectaron cambios para realizar!!!", Me, Err:=True)
                    End If
                    .GuardaRelacion(_Usuario, lstRelacion, txtidClasLlan.Text)
                    OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
                    ActualizaGrid(txtMenuBusca.Text)

                ElseIf OpcionForma = TipoOpcionForma.tOpcInsertar Then
                        .Existe = False
                    .NombreClasLlan = txtNombreClasLlan.Text
                    If Not bEsIdentidad Then
                        .idClasLlan = txtidClasLlan.Text
                    End If

                    Status("Guardando Nuevo Clasificacion de llantas: " & txtNombreClasLlan.Text & ". . .", Me, 1, 2)
                    .Guardar(bEsIdentidad, txtNombreClasLlan.Text, txtNoEjes.Text, txtNoLlantas.Text, _Usuario)
                    .GuardaRelacion(_Usuario, lstRelacion, txtNombreClasLlan.Text)
                    Status("Clasificacion de llantas: " & txtNombreClasLlan.Text & " Guardado con Exito!!!", Me, 2, 2)
                    MessageBox.Show("Clasificacion de llantas: " & txtNombreClasLlan.Text & " Guardado con exito!!!",
                                    "Accion terminada con exito!!!", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
                    ActualizaGrid(txtMenuBusca.Text)

                End If
            End With
            Salida = False
        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try
    End Sub


    Private Sub btnMnuCancelar_Click(sender As Object, e As EventArgs) Handles btnMnuCancelar.Click
        If txtidClasLlan.Text <> "" Then
            CargaForm(txtidClasLlan.Text, UCase(TablaBd))
        End If
        OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
        GridTodos.Enabled = True
    End Sub

    Private Sub btnMnuSalir_Click(sender As Object, e As EventArgs) Handles btnMnuSalir.Click
        Me.Close()
    End Sub

    Private Sub txtMenuBusca_TextChanged(sender As Object, e As EventArgs) Handles txtMenuBusca.TextChanged
        ActualizaGrid(txtMenuBusca.Text)
    End Sub

    Private Sub txtCP_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtNoLlantas.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            'txtNoEjes.Focus()
            'txtNoEjes.SelectAll()

        End If
    End Sub

    Private Sub txtCP_MouseDown(sender As Object, e As MouseEventArgs) Handles txtNoLlantas.MouseDown
        txtNoLlantas.Focus()
        txtNoLlantas.SelectAll()
    End Sub

    Private Sub txtNoEjes_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtNoEjes.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            txtNoLlantas.Focus()
            txtNoLlantas.SelectAll()
        End If
    End Sub

    Private Sub txtDuracionHoras_MouseDown(sender As Object, e As MouseEventArgs) Handles txtNoEjes.MouseDown
        txtNoEjes.Focus()
        txtNoEjes.SelectAll()
    End Sub


    Private Sub txtTonelajeMax_KeyPress(sender As Object, e As KeyPressEventArgs)
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            btnMnuOk_Click(sender, e)
        End If

    End Sub

    Private Sub txtNoEjes_TextChanged(sender As Object, e As EventArgs) Handles txtNoEjes.TextChanged

    End Sub

    Private Sub txtNombreClasLlan_TextChanged(sender As Object, e As EventArgs) Handles txtNombreClasLlan.TextChanged

    End Sub

    Private Sub btnAgrega_Click(sender As Object, e As EventArgs) Handles btnAgrega.Click
        'lstRelacion

        Dim array(2) As String
        array(0) = txtNumEje.Value.ToString
        array(1) = txtNumLlan.Value.ToString
        Dim indice As Integer = 0

        Dim lvl As New ListViewItem(array)
        lvl.Tag = array

        For Each ls As ListViewItem In lstRelacion.Items
            Dim ar(2) As String
            ar = TryCast(ls.Tag, String())
            If ar(0) = array(0) Then
                lstRelacion.Items(indice) = lvl
                DibujaEsquema(txtidClasLlan.Text, txtNoLlantas.Text, lstRelacion)
                Exit Sub
            End If
            indice = indice + 1
        Next
        lstRelacion.Items.Add(lvl)
        'lstRelacion.Refresh()

        DibujaEsquema(txtidClasLlan.Text, txtNoLlantas.Text, lstRelacion)
        'DibujaEsquemaLsv(txtNoLlantas.Text, lstRelacion)

    End Sub

    Private Sub btnDesAgrega_Click(sender As Object, e As EventArgs) Handles btnDesAgrega.Click
        'If lstRelacion.SelectedItems(0) Then
        '    lstRelacion.Items.Remove(lstRelacion.SelectedItems(0))
        'End If
        For Each eachItem As ListViewItem In lstRelacion.SelectedItems
            lstRelacion.Items.Remove(eachItem)
        Next
        TotalesxLsView()
    End Sub

    'Private Sub DibujaEsquemaLsv(ByVal TotalLlantas As Integer, ByVal lsv As ListView)
    '    Dim vNoEje As Integer
    '    Dim vNoLlantas As Integer
    '    Dim LlantxLado As Integer
    '    Dim NoPosicion As Integer = 0

    '    Dim FactorX As Integer = 71
    '    Dim FactorY As Integer = 41

    '    Dim PosXL1 As Integer = 29
    '    'Dim PosXL2 As Integer = 100
    '    Dim PosY As Integer = 62

    '    ReDim rdbPosicion(TotalLlantas - 1)
    '    LimpiaPosiciones()

    '    For Each ls As ListViewItem In lstRelacion.Items
    '        Dim ar(2) As String
    '        ar = TryCast(ls.Tag, String())
    '        vNoEje = ar(0)
    '        vNoLlantas = ar(1)
    '        LlantxLado = vNoLlantas / 2
    '        If LlantxLado = 1 Then
    '            PosXL1 = 29
    '            FactorX = 71
    '        Else
    '            PosXL1 = 5
    '            FactorX = 37
    '        End If
    '        For z = 0 To vNoLlantas - 1
    '            rdbPosicion(NoPosicion) = New RadioButton
    '            rdbPosicion(NoPosicion).Name = "pos" & NoPosicion
    '            rdbPosicion(NoPosicion).Text = NoPosicion + 1
    '            rdbPosicion(NoPosicion).AutoSize = True
    '            rdbPosicion(NoPosicion).Checked = False
    '            rdbPosicion(NoPosicion).Size = New System.Drawing.Size(31, 17)
    '            'rdbPosicion(i).Location = New Point(6, 4)
    '            rdbPosicion(NoPosicion).Location = New Point(PosXL1, PosY)

    '            AddHandler rdbPosicion(NoPosicion).Click, AddressOf RadioButtons_Click
    '            AddHandler rdbPosicion(NoPosicion).KeyDown, AddressOf ObjEnter_KeyDown
    '            'PanelControles.Controls.Add(rdbLogico1(i))
    '            GpoPosiciones.Controls.Add(rdbPosicion(NoPosicion))

    '            'PanelLogico1(i).Controls.Add(rdbLogico1(i))

    '            PosXL1 = PosXL1 + FactorX

    '            If z < vNoLlantas - 1 Then
    '                NoPosicion = NoPosicion + 1
    '            End If


    '        Next
    '        PosY = PosY + FactorY
    '        NoPosicion = NoPosicion + 1

    '    Next
    '    TotalesxLsView()
    'End Sub
    Private Sub DibujaEsquema(ByVal vidClasLlan As Integer, ByVal TotalLlantas As Integer, Optional ByVal lsv As ListView = Nothing)
        Dim vNoEje As Integer
        Dim vNoLlantas As Integer
        Dim LlantxLado As Integer
        Dim NoPosicion As Integer = 0

        'Dim FactorX As Integer = 71
        Dim FactorX As Integer = 201

        'Dim FactorY As Integer = 41
        Dim FactorY As Integer = 70

        Dim PosX As Integer = 29
        'Dim PosFacLinX As Integer = 201
        Dim PosFacLinX As Integer = 195

        'Dim PosY As Integer = 62
        Dim PosY As Integer = 37

        ReDim rdbPosicion(TotalLlantas - 1)

        LimpiaPosiciones()

        If lsv IsNot Nothing Then

            If lsv.Items.Count > 0 Then
                For Each ls As ListViewItem In lstRelacion.Items
                    Dim ar(2) As String
                    ar = TryCast(ls.Tag, String())
                    vNoEje = ar(0)
                    vNoLlantas = ar(1)
                    LlantxLado = vNoLlantas / 2
                    If LlantxLado = 0 Then
                        'PosX = 29
                        'FactorX = 71
                        PosX = 95
                        FactorX = 37
                    ElseIf LlantxLado = 1 Then
                        'PosX = 29
                        'FactorX = 71
                        PosX = 50
                        FactorX = 37
                    Else
                        'PosX = 5
                        'FactorX = 37
                        'PosX = 3
                        PosX = 5
                        'FactorX = 99
                        FactorX = 90
                    End If

                    For z = 0 To vNoLlantas - 1
                        rdbPosicion(NoPosicion) = New RadioButton
                        rdbPosicion(NoPosicion).Name = "pos" & NoPosicion
                        rdbPosicion(NoPosicion).Text = NoPosicion + 1
                        rdbPosicion(NoPosicion).AutoSize = False
                        rdbPosicion(NoPosicion).Checked = False
                        'rdbPosicion(NoPosicion).Size = New System.Drawing.Size(31, 17)
                        rdbPosicion(NoPosicion).Size = New System.Drawing.Size(95, 64)
                        'rdbPosicion(i).Location = New Point(6, 4)
                        rdbPosicion(NoPosicion).Location = New Point(PosX, PosY)

                        rdbPosicion(NoPosicion).Image = My.Resources.llanta2
                        rdbPosicion(NoPosicion).TextImageRelation = TextImageRelation.Overlay
                        rdbPosicion(NoPosicion).ImageAlign = ContentAlignment.MiddleCenter

                        AddHandler rdbPosicion(NoPosicion).Click, AddressOf RadioButtons_Click
                        AddHandler rdbPosicion(NoPosicion).KeyDown, AddressOf ObjEnter_KeyDown
                        'PanelControles.Controls.Add(rdbLogico1(i))
                        GpoPosiciones.Controls.Add(rdbPosicion(NoPosicion))

                        'PanelLogico1(i).Controls.Add(rdbLogico1(i))

                        PosX = PosX + FactorX

                        If z < vNoLlantas - 1 Then
                            NoPosicion = NoPosicion + 1
                        End If

                        If LlantxLado = 1 Then
                            PosX = PosX + PosFacLinX - FactorX
                        End If

                    Next
                    PosY = PosY + FactorY
                    NoPosicion = NoPosicion + 1

                Next


            End If
            TotalesxLsView()
        Else
            ClasLlan = New ClasLlanClass(0)
            TablaDibujaEstruc = New DataTable

            TablaDibujaEstruc = ClasLlan.TablaPosLlanta(vidClasLlan)
            If Not TablaDibujaEstruc Is Nothing Then
                If TablaDibujaEstruc.Rows.Count > 0 Then
                    For i = 0 To TablaDibujaEstruc.Rows.Count - 1
                        vNoEje = TablaDibujaEstruc.DefaultView.Item(i).Item("EjeNo")
                        vNoLlantas = TablaDibujaEstruc.DefaultView.Item(i).Item("NoLlantas")
                        LlantxLado = vNoLlantas / 2
                        'Posiciones Iniciales
                        If LlantxLado = 0 Then
                            'PosX = 29
                            'FactorX = 71
                            PosX = 95
                            FactorX = 37
                        ElseIf LlantxLado = 1 Then
                            'PosX = 29
                            'FactorX = 71
                            PosX = 50
                            FactorX = 37
                        Else
                            'PosX = 5
                            'FactorX = 37
                            'PosX = 3
                            PosX = 5
                            'FactorX = 99
                            FactorX = 90
                        End If

                        For z = 0 To vNoLlantas - 1
                            rdbPosicion(NoPosicion) = New RadioButton
                            rdbPosicion(NoPosicion).Name = "pos" & NoPosicion

                            rdbPosicion(NoPosicion).Text = NoPosicion + 1
                            'rdbPosicion(NoPosicion).AutoSize = True
                            rdbPosicion(NoPosicion).Checked = False

                            'rdbPosicion(NoPosicion).Size = New System.Drawing.Size(31, 17)
                            rdbPosicion(NoPosicion).Size = New System.Drawing.Size(95, 64)

                            rdbPosicion(NoPosicion).Image = My.Resources.llanta2
                            rdbPosicion(NoPosicion).TextImageRelation = TextImageRelation.Overlay
                            rdbPosicion(NoPosicion).ImageAlign = ContentAlignment.MiddleCenter

                            'rdbPosicion(i).Location = New Point(6, 4)

                            rdbPosicion(NoPosicion).Location = New Point(PosX, PosY)

                            AddHandler rdbPosicion(NoPosicion).Click, AddressOf RadioButtons_Click
                            AddHandler rdbPosicion(NoPosicion).KeyDown, AddressOf ObjEnter_KeyDown
                            'PanelControles.Controls.Add(rdbLogico1(i))
                            GpoPosiciones.Controls.Add(rdbPosicion(NoPosicion))

                            'PanelLogico1(i).Controls.Add(rdbLogico1(i))

                            PosX = PosX + FactorX

                            If z < vNoLlantas - 1 Then
                                NoPosicion = NoPosicion + 1
                            End If

                            If LlantxLado = 1 Then
                                PosX = PosX + PosFacLinX - FactorX
                            End If

                        Next
                        PosY = PosY + FactorY
                        NoPosicion = NoPosicion + 1
                    Next
                End If
            End If
        End If



    End Sub

    Private Sub LimpiaPosiciones()
        'For Each ct As Control In GrupoCaptura.Controls
        '    If TypeOf ct Is RadioButton Then
        '        ct.Dispose()
        '    End If
        'Next

        'Dim num_controles As Int32 = Me.TLPanel_convenio.Controls.Count - 1
        Dim num_controles As Int32 = Me.GpoPosiciones.Controls.Count - 1
        For n As Integer = num_controles To 0 Step -1
            Dim ctrl As Windows.Forms.Control = Me.GpoPosiciones.Controls(n)
            If TypeOf ctrl Is RadioButton Then

            End If
            Me.GpoPosiciones.Controls.Remove(ctrl)
            ctrl.Dispose()
        Next
        DibujaLinea()
    End Sub
    Private Sub RadioButtons_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        With TryCast(sender, RadioButton)
            'txtPosicion.Text = .Text
            For i = 0 To rdbPosicion.Count - 1
                rdbPosicion(i).BackColor = SystemColors.Control
            Next
            .BackColor = Color.Yellow
            'MessageBox.Show("El Label Tiene Nombre: " & .Name & " y Tiene Escrito:" & .Text, "informacion del Label", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End With
    End Sub

    Private Sub ObjEnter_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        If e.KeyCode = 13 Then
            SendKeys.Send("{TAB}")
            e.Handled = True
        End If
    End Sub
    Private Sub DibujaLinea()
        'Dim canvas As New ShapeContainer
        'Dim theLine As New LineShape
        ' Set the form as the parent of the ShapeContainer.
        'canvas.Parent = Me.GpoPosiciones
        '' Set the ShapeContainer as the parent of the LineShape.
        'theLine.Parent = canvas
        '' Set the starting and ending coordinates for the line.
        'theLine.BorderWidth = 10
        'theLine.StartPoint = New System.Drawing.Point(183, 25)
        'theLine.EndPoint = New System.Drawing.Point(183, 300)
    End Sub

    Private Sub SeleccionaElementoLsView()
        For Each ls As ListViewItem In lstRelacion.SelectedItems
            Dim ar(2) As String
            ar = TryCast(ls.Tag, String())
            txtNumEje.Value = ar(0)
            txtNumLlan.Value = ar(1)
        Next
    End Sub

    Private Sub lstRelacion_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstRelacion.SelectedIndexChanged
        SeleccionaElementoLsView()
        TotalesxLsView()
    End Sub

    Private Sub TotalesxLsView()
        vTotEjes = 0
        vTotLlan = 0
        For Each ls As ListViewItem In lstRelacion.Items
            Dim ar(2) As String
            ar = TryCast(ls.Tag, String())
            vTotEjes = vTotEjes + 1
            vTotLlan = vTotLlan + Val(ar(1))
        Next

        txtTotLlan.Text = vTotLlan
        txtTotEjes.Text = vTotEjes
    End Sub

End Class
