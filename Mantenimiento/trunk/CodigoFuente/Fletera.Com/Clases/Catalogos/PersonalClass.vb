﻿Public Class PersonalClass
    'idPersonal	        vidPersonal	        _idPersonal
    'idEmpresa	        vidEmpresa	        _idEmpresa
    'NombreCompleto	    vNombreCompleto	    _NombreCompleto
    'Nombre	            vNombre	            _Nombre
    'ApellidoPat	    vApellidoPat	    _ApellidoPat
    'ApellidoMat	    vApellidoMat	    _ApellidoMat
    'idPuesto	        vidPuesto	        _idPuesto
    'PrecioHora	        vPrecioHora	        _PrecioHora
    'Direccion	        vDireccion	        _Direccion
    'idColonia	        vidColonia	        _idColonia
    'TelefonoPersonal	vTelefonoPersonal	_TelefonoPersonal
    'Estatus	        vEstatus	        _Estatus
    'idDepto	        vidDepto	        _idDepto
    'EsTaller	        vEsTaller	        _EsTaller

    Private _idPersonal As Integer
    Private _idEmpresa As Integer
    Private _NombreCompleto As String
    Private _Nombre As String
    Private _ApellidoPat As String
    Private _ApellidoMat As String
    Private _idPuesto As Integer
    Private _PrecioHora As Double
    Private _Direccion As String
    Private _idColonia As Integer
    Private _TelefonoPersonal As String
    Private _Estatus As String
    Private _idDepto As Integer
    Private _EsTaller As Boolean

    Private _Existe As Boolean
    Private _TablaBd As String = "CatPersonal"

    Dim StrSql As String
    Dim DsClase As New DataTable
    Dim MyTabla As DataTable = New DataTable(_TablaBd)

#Region "Constructor"
    Sub New(ByVal vidPersonal As Integer)
        _idPersonal = vidPersonal

        StrSql = "select Per.idPersonal, " & _
        "Per.idEmpresa, " & _
        "Per.NombreCompleto, " & _
        "Per.Nombre, " & _
        "Per.ApellidoPat, " & _
        "Per.ApellidoMat, " & _
        "Per.idPuesto, " & _
        "isnull(Per.PrecioHora,0.0) as PrecioHora, " & _
        "Per.Direccion, " & _
        "Per.idColonia, " & _
        "Per.TelefonoPersonal, " & _
        "Per.Estatus, " & _
        "Per.idDepto, " & _
        "isnull(Per.EsTaller,0) as EsTaller " & _
        "from " & _TablaBd & " Per " & _
        "where Per.idPersonal = " & vidPersonal

        DsClase = BD.ExecuteReturn(StrSql)
        If DsClase.Rows.Count > 0 Then
            With DsClase.Rows(0)
                _idPersonal = .Item("idPersonal")
                _idEmpresa = .Item("idEmpresa")
                _NombreCompleto = .Item("NombreCompleto") & ""
                _Nombre = .Item("Nombre") & ""
                _ApellidoPat = .Item("ApellidoPat") & ""
                _ApellidoMat = .Item("ApellidoMat") & ""
                _idPuesto = .Item("idPuesto")
                _PrecioHora = .Item("PrecioHora")
                _Direccion = .Item("Direccion") & ""
                _idColonia = .Item("idColonia")
                _TelefonoPersonal = .Item("TelefonoPersonal") & ""
                _Estatus = .Item("Estatus") & ""
                _idDepto = .Item("idDepto")
                _EsTaller = .Item("EsTaller")

            End With
            _Existe = True
        Else
            _Existe = False
        End If
    End Sub

    Sub New(ByVal vidPersonal As Integer, ByVal vidEmpresa As Integer, ByVal vNombreCompleto As String, ByVal vNombre As String, _
            ByVal vApellidoPat As String, ByVal vApellidoMat As String, ByVal vidPuesto As Integer, ByVal vPrecioHora As Double, _
            ByVal vDireccion As String, ByVal vidColonia As Integer, ByVal vTelefonoPersonal As String, ByVal vEstatus As String, _
            ByVal vidDepto As Integer, ByVal vEsTaller As Boolean)
        _idPersonal = vidPersonal
        _idEmpresa = vidEmpresa
        _NombreCompleto = vNombreCompleto
        _Nombre = vNombre
        _ApellidoPat = vApellidoPat
        _ApellidoMat = vApellidoMat
        _idPuesto = vidPuesto
        _PrecioHora = vPrecioHora
        _Direccion = vDireccion
        _idColonia = vidColonia
        _TelefonoPersonal = vTelefonoPersonal
        _Estatus = vEstatus
        _idDepto = vidDepto
        _EsTaller = vEsTaller
        _Existe = True
    End Sub
#End Region

#Region "Propiedades"
    Public Property idPersonal As Integer
        Get
            Return _idPersonal
        End Get
        Set(ByVal value As Integer)
            _idPersonal = value
        End Set
    End Property
    Public Property idEmpresa As Integer
        Get
            Return _idEmpresa
        End Get
        Set(ByVal value As Integer)
            _idEmpresa = value
        End Set
    End Property
    Public Property NombreCompleto As String
        Get
            Return _NombreCompleto
        End Get
        Set(ByVal value As String)
            _NombreCompleto = value
        End Set
    End Property
    Public Property Nombre As String
        Get
            Return _Nombre
        End Get
        Set(ByVal value As String)
            _Nombre = value
        End Set
    End Property
    Public Property ApellidoPat As String
        Get
            Return _ApellidoPat
        End Get
        Set(ByVal value As String)
            _ApellidoPat = value
        End Set
    End Property
    Public Property ApellidoMat As String
        Get
            Return _ApellidoMat
        End Get
        Set(ByVal value As String)
            _ApellidoMat = value
        End Set
    End Property

    Public Property idPuesto As Integer
        Get
            Return _idPuesto
        End Get
        Set(ByVal value As Integer)
            _idPuesto = value
        End Set
    End Property
    Public Property PrecioHora As Double
        Get
            Return _PrecioHora
        End Get
        Set(ByVal value As Double)
            _PrecioHora = value
        End Set
    End Property
    Public Property Direccion As String
        Get
            Return _Direccion
        End Get
        Set(ByVal value As String)
            _Direccion = value
        End Set
    End Property
    Public Property idColonia As Integer
        Get
            Return _idColonia
        End Get
        Set(ByVal value As Integer)
            _idColonia = value
        End Set
    End Property
    Public Property TelefonoPersonal As String
        Get
            Return _TelefonoPersonal
        End Get
        Set(ByVal value As String)
            _TelefonoPersonal = value
        End Set
    End Property
    Public Property Estatus As String
        Get
            Return _Estatus
        End Get
        Set(ByVal value As String)
            _Estatus = value
        End Set
    End Property
    Public Property idDepto As Integer
        Get
            Return _idDepto
        End Get
        Set(ByVal value As Integer)
            _idDepto = value
        End Set
    End Property
    Public Property EsTaller As Boolean
        Get
            Return _EsTaller
        End Get
        Set(ByVal value As Boolean)
            _EsTaller = value
        End Set
    End Property
    Public Property Existe As Boolean
        Get
            Return _Existe
        End Get
        Set(ByVal value As Boolean)
            _Existe = value
        End Set
    End Property

#End Region

#Region "Funciones"
    Public Function GetCambios(ByVal vidPersonal As Integer, ByVal vidEmpresa As Integer, ByVal vNombreCompleto As String, ByVal vNombre As String, _
            ByVal vApellidoPat As String, ByVal vApellidoMat As String, ByVal vidPuesto As Integer, ByVal vPrecioHora As Double, _
            ByVal vDireccion As String, ByVal vidColonia As Integer, ByVal vTelefonoPersonal As String, ByVal vEstatus As String, _
            ByVal vidDepto As Integer, ByVal vEsTaller As Boolean) As String
        Dim CadCam As String = ""

        If _idPersonal <> vidPersonal Then
            CadCam += "idPersonal: '" & _idPersonal & "' Cambia a [" & vidPersonal & "],"
        End If
        If _idEmpresa <> vidEmpresa Then
            CadCam += "idEmpresa: '" & _idEmpresa & "' Cambia a [" & vidEmpresa & "],"
        End If
        If _NombreCompleto <> vNombreCompleto Then
            CadCam += "NombreCompleto: '" & _NombreCompleto & "' Cambia a [" & vNombreCompleto & "],"
        End If
        If _Nombre <> vNombre Then
            CadCam += "Nombre: '" & _Nombre & "' Cambia a [" & vNombre & "],"
        End If
        If _ApellidoPat <> vApellidoPat Then
            CadCam += "ApellidoPat: '" & _ApellidoPat & "' Cambia a [" & vApellidoPat & "],"
        End If
        If _ApellidoMat <> vApellidoMat Then
            CadCam += "ApellidoMat: '" & _ApellidoMat & "' Cambia a [" & vApellidoMat & "],"
        End If

        If _idPuesto <> vidPuesto Then
            CadCam += "idPuesto: '" & _idPuesto & "' Cambia a [" & vidPuesto & "],"
        End If
        If _PrecioHora <> vPrecioHora Then
            CadCam += "PrecioHora: '" & _PrecioHora & "' Cambia a [" & vPrecioHora & "],"
        End If
        If _Direccion <> vDireccion Then
            CadCam += "Direccion: '" & _Direccion & "' Cambia a [" & vDireccion & "],"
        End If
        If _idColonia <> vidColonia Then
            CadCam += "idColonia: '" & _idColonia & "' Cambia a [" & vidColonia & "],"
        End If
        If _TelefonoPersonal <> vTelefonoPersonal Then
            CadCam += "TelefonoPersonal: '" & _TelefonoPersonal & "' Cambia a [" & vTelefonoPersonal & "],"
        End If
        If _Estatus <> vEstatus Then
            CadCam += "Estatus: '" & _Estatus & "' Cambia a [" & vEstatus & "],"
        End If
        If _idDepto <> vidDepto Then
            CadCam += "idDepto: '" & _idDepto & "' Cambia a [" & vidDepto & "],"
        End If
        If _EsTaller <> vEsTaller Then
            CadCam += "EsTaller: '" & _EsTaller & "' Cambia a [" & vEsTaller & "],"
        End If

        If CadCam <> "" Then
            CadCam = Mid(CadCam, 1, CadCam.Length - 1)
        End If
        Return CadCam
    End Function
    Public Sub Guardar(ByVal bEsIdentidad As Boolean, ByVal vidPersonal As Integer, ByVal vidEmpresa As Integer, ByVal vNombreCompleto As String, _
    ByVal vNombre As String, ByVal vApellidoPat As String, ByVal vApellidoMat As String, ByVal vidPuesto As Integer, ByVal vPrecioHora As Double, _
    ByVal vDireccion As String, ByVal vidColonia As Integer, ByVal vTelefonoPersonal As String, ByVal vEstatus As String, _
    ByVal vidDepto As Integer, ByVal vEsTaller As Boolean)
        Dim sSql As String = "", _CadValAnt As String = "", _CadValNue As String = ""
        If _Existe Then
            If _idEmpresa <> vidEmpresa Then
                sSql += "idEmpresa = " & vidEmpresa & ","
                _CadValAnt += "idEmpresa =" & vidEmpresa & ","
            End If
            If _NombreCompleto <> vNombreCompleto Then
                'sSql += "NombreCompleto = '" & vNombreCompleto & "',"
                '_CadValAnt += "NombreCompleto =" & vNombreCompleto & ","

                sSql += "NombreCompleto = '" & vNombre & " " & vApellidoPat & " " & vApellidoMat & "',"
                _CadValAnt += "NombreCompleto =" & vNombre & " " & vApellidoPat & " " & vApellidoMat & ","

            End If
            If _Nombre <> vNombre Then
                sSql += "Nombre = '" & vNombre & "',"
                _CadValAnt += "Nombre =" & vNombre & ","
            End If
            If _ApellidoPat <> vApellidoPat Then
                sSql += "ApellidoPat = '" & vApellidoPat & "',"
                _CadValAnt += "ApellidoPat =" & vApellidoPat & ","
            End If
            If _ApellidoMat <> vApellidoMat Then
                sSql += "ApellidoMat = '" & vApellidoMat & "',"
                _CadValAnt += "ApellidoMat =" & vApellidoMat & ","
            End If
            If _idPuesto <> vidPuesto Then
                sSql += "idPuesto = " & vidPuesto & ","
                _CadValAnt += "idPuesto =" & vidPuesto & ","
            End If
            If _PrecioHora <> vPrecioHora Then
                sSql += "PrecioHora = " & vPrecioHora & ","
                _CadValAnt += "PrecioHora =" & vPrecioHora & ","
            End If
            If _Direccion <> vDireccion Then
                sSql += "Direccion = '" & vDireccion & "',"
                _CadValAnt += "Direccion =" & vDireccion & ","
            End If
            If _idColonia <> vidColonia Then
                sSql += "idColonia = " & vidColonia & ","
                _CadValAnt += "idColonia =" & vidColonia & ","
            End If
            If _TelefonoPersonal <> vTelefonoPersonal Then
                sSql += "TelefonoPersonal = '" & vTelefonoPersonal & "',"
                _CadValAnt += "TelefonoPersonal =" & vTelefonoPersonal & ","
            End If
            If _Estatus <> vEstatus Then
                sSql += "Estatus = '" & vEstatus & "',"
                _CadValAnt += "Estatus =" & vEstatus & ","
            End If
            If _idDepto <> vidDepto Then
                sSql += "idDepto = " & vidDepto & ","
                _CadValAnt += "idDepto =" & vidDepto & ","
            End If
            If _EsTaller <> vEsTaller Then
                sSql += "EsTaller = " & vEsTaller & ","
                _CadValAnt += "EsTaller =" & vEsTaller & ","
            End If

            If sSql <> "" Then
                sSql = Mid(sSql, 1, sSql.Length - 1) 'Para quitar la Coma ','
                _CadValAnt = "idPersonal=" & _idPersonal & "," & _CadValAnt
                _CadValAnt = Mid(_CadValAnt, 1, _CadValAnt.Length - 1)
                _CadValNue = Replace(sSql, "'", "")

                sSql = "UPDATE " & _TablaBd & " SET " & sSql & " WHERE idPersonal = " & _idPersonal
                Try
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                    BD.Execute(sSql, True)
                    Audita(_TablaBd, _CadValAnt, _CadValNue, EnTipAccion.Modifica)
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
                Catch ex As Exception
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                    Throw New Exception(ex.Message)
                End Try
            End If
        Else
            If bEsIdentidad Then
                sSql = "INSERT INTO " & _TablaBd & "(idEmpresa,NombreCompleto,Nombre,ApellidoPat,ApellidoMat,idPuesto,PrecioHora, Direccion, idColonia, TelefonoPersonal, Estatus, idDepto, EsTaller) " & _
                "VALUES(" & vidEmpresa & ",'" & vNombreCompleto & "','" & vNombre & "','" & vApellidoPat & "','" & vApellidoMat & _
                "'," & vidPuesto & "," & vPrecioHora & ",'" & vDireccion & "'," & vidColonia & ",'" & vTelefonoPersonal & "','" & vEstatus & "'," & vidDepto & "," & IIf(vEsTaller, 1, 0) & ")"

                _CadValNue = "idEmpresa = " & vidEmpresa & "NombreCompleto = " & vNombreCompleto & _
                "Nombre = " & vNombre & "ApellidoPat = " & vApellidoPat & "ApellidoMat = " & vApellidoMat & "idPuesto = " & vidPuesto & _
                "PrecioHora = " & vPrecioHora & "Direccion = " & vDireccion & "idColonia = " & vidColonia & "TelefonoPersonal = " & vTelefonoPersonal & _
                "Estatus = " & vEstatus & "idDepto = " & vidDepto & "EsTaller = " & IIf(vEsTaller, 1, 0)

            Else
                sSql = "INSERT INTO " & _TablaBd & "(idPersonal,idEmpresa,NombreCompleto,Nombre,ApellidoPat,ApellidoMat,idPuesto,PrecioHora, Direccion, idColonia, TelefonoPersonal, Estatus, idDepto, EsTaller) " & _
                "VALUES(" & vidPersonal & "," & vidEmpresa & ",'" & vNombreCompleto & "','" & vNombre & "','" & vApellidoPat & "','" & vApellidoMat & _
                "'," & vidPuesto & "," & vPrecioHora & ",'" & vDireccion & "'," & vidColonia & ",'" & vTelefonoPersonal & "','" & vEstatus & "'," & vidDepto & "," & IIf(vEsTaller, 1, 0) & ")"

                _CadValNue = "idPersonal=" & vidPersonal & "idEmpresa = " & vidEmpresa & "NombreCompleto = " & vNombreCompleto & _
                "Nombre = " & vNombre & "ApellidoPat = " & vApellidoPat & "ApellidoMat = " & vApellidoMat & "idPuesto = " & vidPuesto & _
                "PrecioHora = " & vPrecioHora & "Direccion = " & vDireccion & "idColonia = " & vidColonia & "TelefonoPersonal = " & vTelefonoPersonal & _
                "Estatus = " & vEstatus & "idDepto = " & vidDepto & "EsTaller = " & IIf(vEsTaller, 1, 0)
            End If

            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, "", _CadValNue, EnTipAccion.Inserta)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
        End If
        _idPersonal = vidPersonal
        _idEmpresa = vidEmpresa
        _NombreCompleto = vNombreCompleto
        _Nombre = vNombre
        _ApellidoPat = vApellidoPat
        _ApellidoMat = vApellidoMat
        _idPuesto = vidPuesto
        _PrecioHora = vPrecioHora
        _Direccion = vDireccion
        _idColonia = vidColonia
        _TelefonoPersonal = vTelefonoPersonal
        _Estatus = vEstatus
        _idDepto = vidDepto
        _EsTaller = vEsTaller

    End Sub
    Public Sub Eliminar()
        If _Existe Then
            Dim sSql As String = "", _CadValAnt As String = ""
            _CadValAnt = "idPersonal=" & _idPersonal & "idEmpresa = " & _idEmpresa & "NombreCompleto = " & _NombreCompleto & _
                "Nombre = " & _Nombre & "ApellidoPat = " & _ApellidoPat & "ApellidoMat = " & _ApellidoMat & "idPuesto = " & _idPuesto & _
                "PrecioHora = " & _PrecioHora & "Direccion = " & _Direccion & "idColonia = " & _idColonia & "TelefonoPersonal = " & _TelefonoPersonal & _
                "Estatus = " & _Estatus & "idDepto = " & _idDepto & "EsTaller = " & _EsTaller

            sSql = "DELETE FROM " & _TablaBd & " WHERE idPersonal = " & _idPersonal
            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, _CadValAnt, "", EnTipAccion.Elimina)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
            _Existe = False
        Else
            'se supone que nunca debe entrar aqui. . .
            MessageBox.Show("El Persona con Id: " & _idPersonal & " No existe por lo tanto no se puede Eliminar!!!",
                            "No se puede eliminar el Personal porque no Existe", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub
#End Region
#Region "Consultas"
    Public Function TablaRelPersonalRolOrdServ(ByVal Activo As Boolean, ByVal idEmpresa As Integer,
     ByVal idTipOrdServ As Integer, ByVal idRol As Integer, Optional ByVal FiltroSinWhere As String = "") As DataTable
        MyTabla.Rows.Clear()

        StrSql = "SELECT  per.idPersonal, " &
       "per.idEmpresa, " &
       "per.NombreCompleto, " &
       "per.idPuesto, " &
       "p.NomPuesto " &
       "FROM dbo.CatPersonal per " &
       "INNER JOIN dbo.CatPuestos p ON p.idPuesto = per.idPuesto " &
       "WHERE per.idDepto IN (SELECT DISTINCT idDepto FROM dbo.RelPersonalRolOrdServ WHERE idTipOrdServ = " & idTipOrdServ & " AND idRol = " & idRol & ") " &
       "AND per.idPuesto IN (SELECT DISTINCT idPuesto FROM dbo.RelPersonalRolOrdServ WHERE idTipOrdServ = " & idTipOrdServ & " AND idRol = " & idRol & ")" & FiltroSinWhere

        MyTabla = BD.ExecuteReturn(StrSql)

        Return MyTabla
    End Function

    Public Function TablaAutorizaSolServ(ByVal Activo As Boolean, ByVal idEmpresa As Integer,
                                         ByVal Puestos As String, ByVal Deptos As String) As DataTable
        MyTabla.Rows.Clear()

        'StrSql = "SELECT idPersonal, idEmpresa, NombreCompleto FROM dbo.CatPersonal " &
        '"WHERE idPuesto = 7 AND Estatus = '" & IIf(Activo, "ACT", "BAJ") & "'" & IIf(idEmpresa > 0, " and idEmpresa = " & idEmpresa, "")

        '"AND per.idDepto in (" & Deptos & ") " &

        StrSql = "SELECT  per.idPersonal, " &
        "per.idEmpresa, " &
        "per.NombreCompleto " &
        "FROM dbo.CatPersonal per " &
        "WHERE per.Estatus = '" & IIf(Activo, "ACT", "BAJ") & "' " &
        IIf(Puestos <> "", " AND per.idPuesto in (" & Puestos & ") ", "") &
        IIf(Deptos <> "", "AND per.idDepto in (" & Deptos & ") ", "") &
        IIf(idEmpresa > 0, " AND per.idEmpresa = " & idEmpresa, "")

        '"INNER JOIN dbo.CatDeptos dep ON per.idDepto = dep.IdDepto " &
        '"INNER JOIN dbo.CatPuestos pue ON per.idPuesto = pue.idPuesto " &


        MyTabla = BD.ExecuteReturn(StrSql)

        Return MyTabla

    End Function


    Public Function TablaChoferesCombo(ByVal Activo As Boolean, ByVal idEmpresa As Integer) As DataTable
        MyTabla.Rows.Clear()

        StrSql = "SELECT idPersonal, idEmpresa, NombreCompleto FROM dbo.CatPersonal " &
        "WHERE idPuesto in (7,36,37) AND Estatus = '" & IIf(Activo, "ACT", "BAJ") & "'" &
        IIf(idEmpresa > 0, " and idEmpresa = " & idEmpresa, "")

        MyTabla = BD.ExecuteReturn(StrSql)

        Return MyTabla

    End Function

    Public Function TablaPersonalCombo(ByVal FiltroSinWhere As String) As DataTable
        'Dim Filtro As String = ""
        MyTabla.Rows.Clear()


        StrSql = " select 0 as idpersonal, 0 as idempresa, '<NINGUNO>' as nombrecompleto, " & _
        " 0 as idpuesto, 'SIN PUESTO' as Nompuesto, " & _
        " 0 as idDepto, '' nomdepto " & _
        " UNION  " & _
        "select per.idpersonal, per.idempresa, Per.nombrecompleto, " & _
        "per.idpuesto, pue.Nompuesto, " & _
        "per.idDepto, dep.nomdepto " & _
        "from CatPersonal Per " & _
        "inner join catpuestos pue on per.idPuesto = pue.idPuesto " & _
        "inner join catdeptos dep on per.idDepto = dep.idDepto " & _
        IIf(FiltroSinWhere <> "", "where " & FiltroSinWhere, "") & " order by nomdepto,idpuesto,nombrecompleto"

        MyTabla = BD.ExecuteReturn(StrSql)

        Return MyTabla

    End Function
    Public Function TablaPersonalComboSinNinguno(ByVal FiltroSinWhere As String) As DataTable
        'Dim Filtro As String = ""
        MyTabla.Rows.Clear()


        StrSql = "select per.idpersonal, per.idempresa, Per.nombrecompleto, " &
        "per.idpuesto, pue.Nompuesto, " &
        "per.idDepto, dep.nomdepto " &
        "from CatPersonal Per " &
        "inner join catpuestos pue on per.idPuesto = pue.idPuesto " &
        "inner join catdeptos dep on per.idDepto = dep.idDepto " &
        IIf(FiltroSinWhere <> "", "where " & FiltroSinWhere, "") & " order by nomdepto,idpuesto,nombrecompleto"

        MyTabla = BD.ExecuteReturn(StrSql)

        Return MyTabla

    End Function
    Public Function TablaMecanicoOCupado(ByVal vEstatusOS As String) As DataTable
        StrSql = "SELECT OT.IDORDENTRABAJO, OT.IDORDENSER, " & _
        "DOS.FECHADIAG, " & _
        "OT.NOTARECEPCION AS FALLA, " & _
        "OT.IDFAMILIA,FAM.NOMFAMILIA,DIV.NOMDIVISION, " & _
        "CAB.IDUNIDADTRANS " & _
        "FROM DETRECEPCIONOS OT " & _
        "INNER JOIN DETORDENSERVICIO DOS ON OT.IDORDENTRABAJO = DOS.IDORDENTRABAJO " & _
        "INNER JOIN CATFAMILIA FAM ON OT.IDFAMILIA = FAM.IDFAMILIA " & _
        "INNER JOIN CATDIVISION DIV ON FAM.IDDIVISION = DIV.IDDIVISION " & _
        "INNER JOIN CABORDENSERVICIO CAB ON OT.IDORDENSER = CAB.IDORDENSER " & _
        "WHERE OT.IDPERSONALRESP = " & _idPersonal & _
        " AND DOS.ESTATUS = '" & vEstatusOS & "'"

        '" AND DOS.ESTATUS = 'DIAG'"


        MyTabla = BD.ExecuteReturn(StrSql)

        Return MyTabla
    End Function
#End Region
End Class
