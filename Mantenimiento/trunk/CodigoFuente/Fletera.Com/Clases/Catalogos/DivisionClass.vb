﻿Public Class DivisionClass
    Private _idDivision As Integer
    Private _NomDivision As String

    Private _Existe As Boolean
    Private _TablaBd As String = "CatDivision"

    Dim StrSql As String
    Dim DsClase As New DataTable
    Dim MyTabla As DataTable = New DataTable(_TablaBd)
#Region "Constructor"
    Sub New(ByVal vIdDivision As Integer)
        'Dim DtEst As New DataTable
        _idDivision = vIdDivision
        StrSql = "select idDivision,NomDivision from " & _TablaBd & " where idDivision = " & vIdDivision

        DsClase = BD.ExecuteReturn(StrSql)
        If DsClase.Rows.Count > 0 Then
            With DsClase.Rows(0)
                _NomDivision = .Item("NomDivision") & ""
            End With
            _Existe = True
        Else
            _Existe = False
        End If

    End Sub
    Sub New(ByVal vIdMarca As Integer, ByVal vNombreMarca As String)
        _idDivision = vIdMarca
        _NomDivision = vNombreMarca
        _Existe = True
    End Sub
#End Region
#Region "Propiedades"
    'Public ReadOnly Property idDivision As Integer
    '    Get
    '        Return _idDivision
    '    End Get
    'End Property
    Public Property idDivision As Integer
        Get
            Return _idDivision
        End Get
        Set(ByVal value As Integer)
            _idDivision = value
        End Set
    End Property
    Public Property NomDivision As String
        Get
            Return _NomDivision
        End Get
        Set(ByVal value As String)
            _NomDivision = value
        End Set
    End Property
    'Public ReadOnly Property Existe As Boolean
    '    Get
    '        Return _Existe
    '    End Get
    'End Property
    Public Property Existe As Boolean
        Get
            Return _Existe
        End Get
        Set(ByVal value As Boolean)
            _Existe = value
        End Set
    End Property

#End Region
#Region "Funciones"
    Public Function GetCambios(ByVal vNomDivision As String) As String
        Dim CadCam As String = ""
        If _NomDivision <> vNomDivision Then
            CadCam += "Nombre Division: '" & _NomDivision & "' Cambia a [" & vNomDivision & "],"
        End If
        If CadCam <> "" Then
            CadCam = Mid(CadCam, 1, CadCam.Length - 1)
        End If
        Return CadCam
    End Function
    Public Sub Guardar(ByVal vNomDivision As String, ByVal bEsIdentidad As Boolean)
        Dim sSql As String = "", _CadValAnt As String = "", _CadValNue As String = ""
        If _Existe Then
            If _NomDivision <> vNomDivision Then
                sSql += "NomDivision = '" & vNomDivision & "',"
                _CadValAnt += "NomDivision =" & vNomDivision & ","
            End If

            If sSql <> "" Then
                sSql = Mid(sSql, 1, sSql.Length - 1) 'Para quitar la Coma ','
                _CadValAnt = "idDivision=" & _idDivision & "," & _CadValAnt
                _CadValAnt = Mid(_CadValAnt, 1, _CadValAnt.Length - 1)
                _CadValNue = Replace(sSql, "'", "")
                sSql = "UPDATE " & _TablaBd & " SET " & sSql & " WHERE idDivision = '" & _idDivision & "'"
                Try
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                    BD.Execute(sSql, True)
                    Audita(_TablaBd, _CadValAnt, _CadValNue, EnTipAccion.Modifica)
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
                Catch ex As Exception
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                    Throw New Exception(ex.Message)
                End Try
            End If
        Else
            If bEsIdentidad Then
                sSql = "INSERT INTO " & _TablaBd & "(NomDivision) VALUES('" & _NomDivision & "')"

                _CadValNue = "NomDivision=" & _NomDivision
            Else

                sSql = "INSERT INTO " & _TablaBd & "(idDivision,NomDivision) VALUES(" &
                "'" & _idDivision & "','" & _NomDivision & "')"

                _CadValNue = "idDivision=" & _idDivision & ",NomDivision=" & _NomDivision
            End If

            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, "", _CadValNue, EnTipAccion.Inserta)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
        End If
        _NomDivision = vNomDivision
    End Sub
    Public Sub Eliminar()
        If _Existe Then
            Dim sSql As String = "", _CadValAnt As String = ""
            _CadValAnt = "idDivision=" & _idDivision & " , NomDivision =" & _NomDivision
            sSql = "DELETE FROM " & _TablaBd & " WHERE idDivision = '" & _idDivision & "'"
            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, _CadValAnt, "", EnTipAccion.Elimina)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
            _Existe = False
        Else
            'se supone que nunca debe entrar aqui. . .
            MessageBox.Show("La División con Id: " & _idDivision & " No existe por lo tanto no se puede Eliminar!!!",
                            "No se puede eliminar la División porque no Existe", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub
#End Region
#Region "Consultas"
    Public Function TablaDivisiones(ByVal FiltroSinWhere As String) As DataTable
        'Dim Filtro As String = ""
        MyTabla.Rows.Clear()

        StrSql = "SELECT div.idDivision, " &
        "div.NomDivision " &
        "FROM dbo.CatDivision div " & IIf(FiltroSinWhere = "", "", " WHERE " & FiltroSinWhere) &
        " ORDER BY idDivision"

        '"WHERE idDivision > 0 " &

        MyTabla = BD.ExecuteReturn(StrSql)

        Return MyTabla

    End Function
#End Region
End Class
