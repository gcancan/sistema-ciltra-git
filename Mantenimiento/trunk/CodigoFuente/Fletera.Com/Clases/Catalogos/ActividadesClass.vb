﻿Public Class ActividadesClass
    'idActividad				vidActividad 				_idActividad
    'NombreAct				vNombreAct 					_NombreAct
    'CostoManoObra			vCostoManoObra 				_CostoManoObra 
    'idFamilia				vidFamilia 					_idFamilia
    'DuracionHoras			vDuracionHoras 				_DuracionHoras 

    'NomFamilia				vNomFamilia 				_NomFamilia 
    'idDivision				vidDivision 				_idDivision 
    'NomDivision				vNomDivision 				_NomDivision
    Private _idActividad As Integer
    Private _NombreAct As String
    Private _CostoManoObra As Double
    Private _idFamilia As Integer
    Private _NomFamilia As String
    Private _DuracionHoras As Double

    Private _idDivision As Integer
    Private _NomDivision As String
    Private _idEnum As Integer

    Private _Existe As Boolean
    Private _TablaBd As String = "CatActividades"

    Dim StrSql As String
    Dim DsClase As New DataTable
    Dim MyTabla As DataTable = New DataTable(_TablaBd)

#Region "Constructor"
    Sub New(ByVal vidActividad As Integer)
        _idActividad = vidActividad

        StrSql = "select act.idActividad, act.NombreAct, isnull(act.CostoManoObra,0) as CostoManoObra, isnull(act.DuracionHoras,0) as DuracionHoras, " &
        "act.idFamilia, fam.NomFamilia, fam.idDivision, div.NomDivision, isnull(idEnum,0) as idEnum " &
        "from " & _TablaBd & " Act " &
        "inner join CatFamilia Fam on act.idFamilia = fam.idFamilia " &
        "inner join CatDivision Div on fam.idDivision = div.idDivision " &
        "where act.idActividad = " & vidActividad

        DsClase = BD.ExecuteReturn(StrSql)
        If DsClase.Rows.Count > 0 Then
            With DsClase.Rows(0)
                _NombreAct = .Item("NombreAct") & ""
                _idFamilia = .Item("idFamilia")
                _NomFamilia = .Item("NomFamilia")
                _CostoManoObra = .Item("CostoManoObra")
                _DuracionHoras = .Item("DuracionHoras")
                _idDivision = .Item("idDivision")
                _DuracionHoras = .Item("DuracionHoras")
                _NomDivision = .Item("NomDivision")
                _idEnum = .Item("idEnum")
            End With
            _Existe = True
        Else
            _Existe = False
        End If

    End Sub
    Sub New(ByVal vidActividad As Integer, ByVal vNombreAct As String, ByVal vidFamilia As Integer, ByVal vNomFamilia As String,
            ByVal vCostoManoObra As Double, ByVal vDuracionHoras As Double, ByVal vidDivision As Integer,
            ByVal vNomDivision As String, ByVal vidEnum As Integer)
        _idActividad = vidActividad
        _NombreAct = vNombreAct
        _idFamilia = vidFamilia
        _NomFamilia = vNomFamilia
        _CostoManoObra = vCostoManoObra
        _DuracionHoras = vDuracionHoras
        _idDivision = vidDivision
        _NomDivision = vNomDivision
        _idEnum = vidEnum
        _Existe = True
    End Sub
#End Region

#Region "Propiedades"
    'Public ReadOnly Property idDivision As Integer
    '    Get
    '        Return _idFamilia
    '    End Get
    'End Property
    Public Property idActividad As Integer
        Get
            Return _idActividad
        End Get
        Set(ByVal value As Integer)
            _idActividad = value
        End Set
    End Property
    Public Property NombreAct As String
        Get
            Return _NombreAct
        End Get
        Set(ByVal value As String)
            _NombreAct = value
        End Set
    End Property
    Public Property idFamilia As Integer
        Get
            Return _idFamilia
        End Get
        Set(ByVal value As Integer)
            _idFamilia = value
        End Set
    End Property
    Public Property NomFamilia As String
        Get
            Return _NomFamilia
        End Get
        Set(ByVal value As String)
            _NomFamilia = value
        End Set
    End Property
    Public Property CostoManoObra As Double
        Get
            Return _CostoManoObra
        End Get
        Set(ByVal value As Double)
            _CostoManoObra = value
        End Set
    End Property
    Public Property DuracionHoras As Double
        Get
            Return _DuracionHoras
        End Get
        Set(ByVal value As Double)
            _DuracionHoras = value
        End Set
    End Property
    Public Property idDivision As Integer
        Get
            Return _idDivision
        End Get
        Set(ByVal value As Integer)
            _idDivision = value
        End Set
    End Property
    Public Property NomDivision As String
        Get
            Return _NomDivision
        End Get
        Set(ByVal value As String)
            _NomDivision = value
        End Set
    End Property
    Public Property idEnum As Integer
        Get
            Return _idEnum
        End Get
        Set(ByVal value As Integer)
            _idEnum = value
        End Set
    End Property

    Public Property Existe As Boolean
        Get
            Return _Existe
        End Get
        Set(ByVal value As Boolean)
            _Existe = value
        End Set
    End Property

#End Region

#Region "Funciones"
    Public Function GetCambios(ByVal vNombreAct As String, ByVal vidFamilia As Integer, ByVal vCostoManoObra As Double,
    ByVal vDuracionHoras As Double) As String
        ', ByVal vidEnum As Integer
        Dim CadCam As String = ""
        If _NombreAct <> vNombreAct Then
            CadCam += "NombreAct: '" & _NombreAct & "' Cambia a [" & vNombreAct & "],"
        End If
        If _idFamilia <> vidFamilia Then
            CadCam += "idFamilia: '" & _idFamilia & "' Cambia a [" & vidFamilia & "],"
        End If
        'If _NomFamilia <> vNomFamilia Then
        '    CadCam += "NomFamilia: '" & _NomFamilia & "' Cambia a [" & vNomFamilia & "],"
        'End If
        If _CostoManoObra <> vCostoManoObra Then
            CadCam += "CostoManoObra: '" & _CostoManoObra & "' Cambia a [" & vCostoManoObra & "],"
        End If
        If _DuracionHoras <> vDuracionHoras Then
            CadCam += "DuracionHoras: '" & _DuracionHoras & "' Cambia a [" & vDuracionHoras & "],"
        End If
        'If _idEnum <> vidEnum Then
        '    CadCam += "idEnum: '" & _idEnum & "' Cambia a [" & vidEnum & "],"
        'End If

        If CadCam <> "" Then
            CadCam = Mid(CadCam, 1, CadCam.Length - 1)
        End If
        Return CadCam
    End Function
    Public Sub Guardar(ByVal vNombreAct As String, ByVal vidFamilia As Integer, ByVal bEsIdentidad As Boolean,
    ByVal vCostoManoObra As Double, ByVal vDuracionHoras As Double)
        ', ByVal vidEnum As Integer
        Dim sSql As String = "", _CadValAnt As String = "", _CadValNue As String = ""
        If _Existe Then
            If _NombreAct <> vNombreAct Then
                sSql += "NombreAct = '" & vNombreAct & "',"
                _CadValAnt += "NombreAct =" & vNombreAct & ","
            End If
            If _idFamilia <> vidFamilia Then
                sSql += "idFamilia = " & vidFamilia & ","
                _CadValAnt += "idFamilia =" & vidFamilia & ","
            End If
            If _CostoManoObra <> vCostoManoObra Then
                sSql += "CostoManoObra = " & vCostoManoObra & ","
                _CadValAnt += "CostoManoObra =" & vCostoManoObra & ","
            End If
            If _DuracionHoras <> vDuracionHoras Then
                sSql += "DuracionHoras = " & vDuracionHoras & ","
                _CadValAnt += "DuracionHoras =" & vDuracionHoras & ","
            End If
            'If _idEnum <> vidEnum Then
            '    sSql += "idEnum = " & vidEnum & ","
            '    _CadValAnt += "idEnum =" & vidEnum & ","
            'End If

            If sSql <> "" Then
                sSql = Mid(sSql, 1, sSql.Length - 1) 'Para quitar la Coma ','
                _CadValAnt = "idActividad=" & _idActividad & "," & _CadValAnt
                _CadValAnt = Mid(_CadValAnt, 1, _CadValAnt.Length - 1)
                _CadValNue = Replace(sSql, "'", "")

                sSql = "UPDATE " & _TablaBd & " SET " & sSql & " WHERE idActividad = '" & _idActividad & "'"
                Try
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                    BD.Execute(sSql, True)
                    Audita(_TablaBd, _CadValAnt, _CadValNue, EnTipAccion.Modifica)
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
                Catch ex As Exception
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                    Throw New Exception(ex.Message)
                End Try
            End If
        Else
            If bEsIdentidad Then
                'sSql = "INSERT INTO " & _TablaBd & "(NombreAct,idFamilia,CostoManoObra,DuracionHoras,idEnum) " &
                '"VALUES('" & vNombreAct & "'," & vidFamilia & "," & vCostoManoObra & "," & vDuracionHoras & "," & vidEnum & ")"
                sSql = "INSERT INTO " & _TablaBd & "(NombreAct,idFamilia,CostoManoObra,DuracionHoras) " &
                "VALUES('" & vNombreAct & "'," & vidFamilia & "," & vCostoManoObra & "," & vDuracionHoras & ")"

                _CadValNue = "NombreAct=" & vNombreAct & "idFamilia = " & vidFamilia & "CostoManoObra = " & vCostoManoObra & "DuracionHoras = " & vDuracionHoras '& "idEnum = " & vidEnum
            Else
                'sSql = "INSERT INTO " & _TablaBd & "(idActividad,NombreAct,idFamilia,CostoManoObra,DuracionHoras,idEnum) " &
                '"VALUES(" & _idActividad & ",'" & vNombreAct & "'," & vidFamilia & "," & vCostoManoObra &
                '"," & vDuracionHoras & "," & vidEnum & ")"
                sSql = "INSERT INTO " & _TablaBd & "(idActividad,NombreAct,idFamilia,CostoManoObra,DuracionHoras) " &
                "VALUES(" & _idActividad & ",'" & vNombreAct & "'," & vidFamilia & "," & vCostoManoObra &
                "," & vDuracionHoras & ")"

                _CadValNue = "idActividad=" & _idActividad & "NombreAct=" & vNombreAct & "idFamilia = " & vidFamilia & "CostoManoObra = " & vCostoManoObra & "DuracionHoras = " & vDuracionHoras '& "idEnum = " & vidEnum
            End If

            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, "", _CadValNue, EnTipAccion.Inserta)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
        End If
        _NombreAct = vNombreAct
        _idFamilia = vidFamilia
        _DuracionHoras = vDuracionHoras
        _CostoManoObra = vCostoManoObra
    End Sub
    Public Sub Eliminar()
        If _Existe Then
            Dim sSql As String = "", _CadValAnt As String = ""
            _CadValAnt = "idActividad=" & _idActividad & " , NomAlmacen =" & _NombreAct
            sSql = "DELETE FROM " & _TablaBd & " WHERE idActividad = " & _idActividad
            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, _CadValAnt, "", EnTipAccion.Elimina)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
            _Existe = False
        Else
            'se supone que nunca debe entrar aqui. . .
            MessageBox.Show("La Actividad con Id: " & _idActividad & " No existe por lo tanto no se puede Eliminar!!!",
                            "No se puede eliminar la Actividad porque no Existe", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub
#End Region

#Region "Consultas"
    Public Function TablaActividades(ByVal FiltroSinWhere As String) As DataTable
        'Dim Filtro As String = ""
        MyTabla.Rows.Clear()

        StrSql = "SELECT act.idActividad, " &
        "act.NombreAct, " &
        "act.CostoManoObra, " &
        "act.idFamilia, " &
        "fam.NomFamilia, " &
        "fam.idDivision, " &
        "div.NomDivision, " &
        "act.DuracionHoras, " &
        "isnull(act.idEnum,0) as idEnum " &
        "FROM dbo.CatActividades Act " &
        "INNER JOIN dbo.CatFamilia fam ON act.idFamilia = fam.idFamilia " &
        "INNER JOIN dbo.CatDivision div ON fam.idDivision = div.idDivision " &
        IIf(FiltroSinWhere = "", "", " WHERE " & FiltroSinWhere)

        '"WHERE idDivision > 0 " &

        MyTabla = BD.ExecuteReturn(StrSql)

        Return MyTabla

    End Function
#End Region
End Class
