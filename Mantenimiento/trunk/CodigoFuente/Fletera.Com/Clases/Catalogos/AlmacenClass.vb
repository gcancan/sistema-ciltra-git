﻿Public Class AlmacenClass
    'idAlmacen			vidAlmacen				_idAlmacen
    'NomAlmacen			vNomAlmacen				_NomAlmacen
    'idSucursal			vidSucursal				_idSucursal
    'NomSucursal		vNomSucursal			_NomSucursal
    'CIDALMACEN_COM		vCIDALMACEN_COM			_CIDALMACEN_COM

    Private _idAlmacen As Integer
    Private _NomAlmacen As String
    Private _idSucursal As Integer
    Private _NomSucursal As String
    Private _CIDALMACEN_COM As Integer

    Private _Existe As Boolean
    Private _TablaBd As String = "CatAlmacen"

    Dim StrSql As String
    Dim DsClase As New DataTable
    Dim MyTabla As DataTable = New DataTable(_TablaBd)
#Region "Constructor"
    Sub New(ByVal vidAlmacen As Integer)
        'Dim DtEst As New DataTable
        _idAlmacen = vidAlmacen

        StrSql = "select alm.idAlmacen, alm.NomAlmacen, alm.idSucursal, suc.NomSucursal, alm.CIDALMACEN_COM " & _
        "from " & _TablaBd & " Alm " & _
        "LEFT join CatSucursal Suc on alm.idSucursal = suc.id_sucursal " & _
        "where alm.idAlmacen = " & vidAlmacen


        DsClase = BD.ExecuteReturn(StrSql)
        If DsClase.Rows.Count > 0 Then
            With DsClase.Rows(0)
                _NomAlmacen = .Item("NomAlmacen") & ""
                _idSucursal = .Item("idSucursal")
                _NomSucursal = .Item("NomSucursal")
                _CIDALMACEN_COM = .Item("CIDALMACEN_COM")
            End With
            _Existe = True
        Else
            _Existe = False
        End If

    End Sub
    Sub New(ByVal vidAlmacen As Integer, ByVal vNomAlmacen As String, ByVal vidSucursal As Integer, ByVal vNomSucursal As String, ByVal vCIDALMACEN_COM As Integer)
        _idAlmacen = vNomAlmacen
        _NomAlmacen = vNomAlmacen
        _idSucursal = vidSucursal
        _NomSucursal = vNomSucursal
        _CIDALMACEN_COM = vCIDALMACEN_COM
        _Existe = True
    End Sub
#End Region
#Region "Propiedades"
    'Public ReadOnly Property idDivision As Integer
    '    Get
    '        Return _idSucursal
    '    End Get
    'End Property
    Public Property idAlmacen As Integer
        Get
            Return _idAlmacen
        End Get
        Set(ByVal value As Integer)
            _idAlmacen = value
        End Set
    End Property
    Public Property NomAlmacen As String
        Get
            Return _NomAlmacen
        End Get
        Set(ByVal value As String)
            _NomAlmacen = value
        End Set
    End Property
    Public Property idSucursal As Integer
        Get
            Return _idSucursal
        End Get
        Set(ByVal value As Integer)
            _idSucursal = value
        End Set
    End Property
    Public Property NomSucursal As String
        Get
            Return _NomSucursal
        End Get
        Set(ByVal value As String)
            _NomSucursal = value
        End Set
    End Property
    Public Property CIDALMACEN_COM As Integer
        Get
            Return _CIDALMACEN_COM
        End Get
        Set(ByVal value As Integer)
            _CIDALMACEN_COM = value
        End Set
    End Property
    'Public ReadOnly Property Existe As Boolean
    '    Get
    '        Return _Existe
    '    End Get
    'End Property
    Public Property Existe As Boolean
        Get
            Return _Existe
        End Get
        Set(ByVal value As Boolean)
            _Existe = value
        End Set
    End Property

#End Region
#Region "Funciones"
    Public Function GetCambios(ByVal vNomAlmacen As String, ByVal vidSucursal As Integer, ByVal vCIDALMACEN_COM As Integer) As String
        Dim CadCam As String = ""
        If _NomAlmacen <> vNomAlmacen Then
            CadCam += "NomAlmacen: '" & _NomAlmacen & "' Cambia a [" & vNomAlmacen & "],"
        End If
        If _idSucursal <> vidSucursal Then
            CadCam += "idSucursal: '" & _idSucursal & "' Cambia a [" & vidSucursal & "],"
        End If
        'If _NomSucursal <> vNomSucursal Then
        '    CadCam += "NomSucursal: '" & _NomSucursal & "' Cambia a [" & vNomSucursal & "],"
        'End If
        If _CIDALMACEN_COM <> vCIDALMACEN_COM Then
            CadCam += "CIDALMACEN_COM: '" & _CIDALMACEN_COM & "' Cambia a [" & vCIDALMACEN_COM & "],"
        End If
        If CadCam <> "" Then
            CadCam = Mid(CadCam, 1, CadCam.Length - 1)
        End If
        Return CadCam
    End Function
    Public Sub Guardar(ByVal vNomAlmacen As String, ByVal vidSucursal As Integer, ByVal bEsIdentidad As Boolean, ByVal vCIDALMACEN_COM As Integer)
        Dim sSql As String = "", _CadValAnt As String = "", _CadValNue As String = ""
        If _Existe Then
            If _NomAlmacen <> vNomAlmacen Then
                sSql += "NomAlmacen = '" & vNomAlmacen & "',"
                _CadValAnt += "NomAlmacen =" & vNomAlmacen & ","
            End If
            If _idSucursal <> vidSucursal Then
                sSql += "idSucursal = " & vidSucursal & ","
                _CadValAnt += "idSucursal =" & vidSucursal & ","
            End If
            If _CIDALMACEN_COM <> vCIDALMACEN_COM Then
                sSql += "CIDALMACEN_COM = " & vCIDALMACEN_COM & ","
                _CadValAnt += "CIDALMACEN_COM =" & vCIDALMACEN_COM & ","
            End If

            If sSql <> "" Then
                sSql = Mid(sSql, 1, sSql.Length - 1) 'Para quitar la Coma ','
                _CadValAnt = "idAlmacen=" & _idAlmacen & "," & _CadValAnt
                _CadValAnt = Mid(_CadValAnt, 1, _CadValAnt.Length - 1)
                _CadValNue = Replace(sSql, "'", "")

                sSql = "UPDATE " & _TablaBd & " SET " & sSql & " WHERE idAlmacen = '" & _idAlmacen & "'"
                Try
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                    BD.Execute(sSql, True)
                    Audita(_TablaBd, _CadValAnt, _CadValNue, EnTipAccion.Modifica)
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
                Catch ex As Exception
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                    Throw New Exception(ex.Message)
                End Try
            End If
        Else
            If bEsIdentidad Then
                sSql = "INSERT INTO " & _TablaBd & "(NomAlmacen,idSucursal,CIDALMACEN_COM) VALUES('" & vNomAlmacen & "'," & vidSucursal & "," & vCIDALMACEN_COM & ")"

                _CadValNue = "NomAlmacen=" & vNomAlmacen & "idSucursal = " & vidSucursal & "CIDALMACEN_COM = " & vCIDALMACEN_COM
            Else

                sSql = "INSERT INTO " & _TablaBd & "(idAlmacen,NomAlmacen,idSucursal,CIDALMACEN_COM) VALUES(" &
                "'" & _idAlmacen & "','" & vNomAlmacen & "'," & vidSucursal & "," & vCIDALMACEN_COM & ")"

                _CadValNue = "idAlmacen=" & _idAlmacen & ",NomAlmacen=" & vNomAlmacen & ",idDivision=" & vidSucursal & "CIDALMACEN_COM = " & vCIDALMACEN_COM
            End If

            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, "", _CadValNue, EnTipAccion.Inserta)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
        End If
        _NomAlmacen = vNomAlmacen
        _idSucursal = vidSucursal
        '_NomSucursal = vNomSucursal
        _CIDALMACEN_COM = vCIDALMACEN_COM
    End Sub
    Public Sub Eliminar()
        If _Existe Then
            Dim sSql As String = "", _CadValAnt As String = ""
            _CadValAnt = "idAlmacen=" & _idAlmacen & " , NomAlmacen =" & _NomAlmacen
            sSql = "DELETE FROM " & _TablaBd & " WHERE idAlmacen = '" & _idAlmacen & "'"
            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, _CadValAnt, "", EnTipAccion.Elimina)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
            _Existe = False
        Else
            'se supone que nunca debe entrar aqui. . .
            MessageBox.Show("El Almacen con Id: " & _idAlmacen & " No existe por lo tanto no se puede Eliminar!!!",
                            "No se puede eliminar el Almacen porque no Existe", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub
#End Region
#Region "Consultas"
    Public Function TablaAlmacenes(ByVal bIncluyeTodos As Boolean, Optional ByVal FiltroSinWhere As String = "") As DataTable
        Dim filtro As String = ""
        MyTabla.Rows.Clear()

        If FiltroSinWhere <> "" Then
            filtro = " where " & FiltroSinWhere
        End If

        If bIncluyeTodos Then
            StrSql = "SELECT 0 AS idAlmacen,'TODOS' AS NomAlmacen " &
            "UNION " &
            "SELECT idAlmacen,NomAlmacen FROM dbo." & _TablaBd & " " & filtro &
            " ORDER BY idAlmacen"
        Else
            StrSql = "SELECT idAlmacen,NomAlmacen FROM dbo." & _TablaBd & " " & filtro &
            " ORDER BY idAlmacen"
        End If
        MyTabla = BD.ExecuteReturn(StrSql)

        Return MyTabla

    End Function
#End Region

End Class
