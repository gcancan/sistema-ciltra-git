﻿Public Class DeptosClass
    'IdDepto	vIdDepto	_IdDepto
    'NomDepto	vNomDepto	_NomDepto

    Private _IdDepto As Integer
    Private _NomDepto As String

    Private _Existe As Boolean
    Private _TablaBd As String = "CatDeptos"

    Dim StrSql As String
    Dim DsClase As New DataTable

#Region "Constructor"
    Sub New(ByVal vIdDepto As Integer)
        'Dim DtEst As New DataTable
        _IdDepto = vIdDepto
        StrSql = "select IdDepto,NomDepto from " & _TablaBd & " where IdDepto = " & vIdDepto

        DsClase = BD.ExecuteReturn(StrSql)
        If DsClase.Rows.Count > 0 Then
            With DsClase.Rows(0)
                _NomDepto = .Item("NomDepto") & ""
            End With
            _Existe = True
        Else
            _Existe = False
        End If

    End Sub
    Sub New(ByVal vIdDepto As Integer, ByVal vNomDepto As String)
        _IdDepto = vIdDepto
        _NomDepto = vNomDepto
        _Existe = True
    End Sub
#End Region
#Region "Propiedades"
    Public Property IdDepto As Integer
        Get
            Return _IdDepto
        End Get
        Set(ByVal value As Integer)
            _IdDepto = value
        End Set
    End Property
    Public Property NomDepto As String
        Get
            Return _NomDepto
        End Get
        Set(ByVal value As String)
            _NomDepto = value
        End Set
    End Property
    Public Property Existe As Boolean
        Get
            Return _Existe
        End Get
        Set(ByVal value As Boolean)
            _Existe = value
        End Set
    End Property

#End Region

#Region "Funciones"
    Public Function GetCambios(ByVal vNomDepto As String) As String
        Dim CadCam As String = ""
        If _NomDepto <> vNomDepto Then
            CadCam += "Nombre Puesto: '" & _NomDepto & "' Cambia a [" & vNomDepto & "],"
        End If
        If CadCam <> "" Then
            CadCam = Mid(CadCam, 1, CadCam.Length - 1)
        End If
        Return CadCam
    End Function

    Public Sub Guardar(ByVal vNomDepto As String, ByVal bEsIdentidad As Boolean)
        Dim sSql As String = "", _CadValAnt As String = "", _CadValNue As String = ""
        If _Existe Then
            If _NomDepto <> vNomDepto Then
                sSql += "NomDepto = '" & vNomDepto & "',"
                _CadValAnt += "NomDepto =" & vNomDepto & ","
            End If

            If sSql <> "" Then
                sSql = Mid(sSql, 1, sSql.Length - 1) 'Para quitar la Coma ','
                _CadValAnt = "IdDepto=" & _IdDepto & "," & _CadValAnt
                _CadValAnt = Mid(_CadValAnt, 1, _CadValAnt.Length - 1)
                _CadValNue = Replace(sSql, "'", "")
                sSql = "UPDATE " & _TablaBd & " SET " & sSql & " WHERE IdDepto = '" & _IdDepto & "'"
                Try
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                    BD.Execute(sSql, True)
                    Audita(_TablaBd, _CadValAnt, _CadValNue, EnTipAccion.Modifica)
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
                Catch ex As Exception
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                    Throw New Exception(ex.Message)
                End Try
            End If
        Else
            If bEsIdentidad Then
                sSql = "INSERT INTO " & _TablaBd & "(NomDepto) VALUES('" & _NomDepto & "')"

                _CadValNue = "NomDepto=" & _NomDepto
            Else

                sSql = "INSERT INTO " & _TablaBd & "(IdDepto,NomDepto) VALUES(" &
                "'" & _IdDepto & "','" & _NomDepto & "')"

                _CadValNue = "IdDepto=" & _IdDepto & ",NomDepto=" & _NomDepto
            End If

            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, "", _CadValNue, EnTipAccion.Inserta)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
        End If
        _NomDepto = vNomDepto
    End Sub

    Public Sub Eliminar()
        If _Existe Then
            Dim sSql As String = "", _CadValAnt As String = ""
            _CadValAnt = "IdDepto=" & _IdDepto & " , NomDepto =" & _NomDepto
            sSql = "DELETE FROM " & _TablaBd & " WHERE IdDepto = '" & _IdDepto & "'"
            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, _CadValAnt, "", EnTipAccion.Elimina)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
            _Existe = False
        Else
            'se supone que nunca debe entrar aqui. . .
            MessageBox.Show("El tipo de Departamento con Id: " & _IdDepto & " No existe por lo tanto no se puede Eliminar!!!",
                            "No se puede eliminar el tipo de Departamento porque no Existe", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub
#End Region

End Class
