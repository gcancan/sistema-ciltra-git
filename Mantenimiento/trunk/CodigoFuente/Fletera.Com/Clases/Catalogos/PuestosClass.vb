﻿Public Class PuestosClass
    Private _idPuesto As Integer
    Private _NomPuesto As String

    Private _Existe As Boolean
    Private _TablaBd As String = "CatPuestos"

    Dim StrSql As String
    Dim DsClase As New DataTable
#Region "Constructor"
    Sub New(ByVal vIdPuesto As Integer)
        'Dim DtEst As New DataTable
        _idpuesto = vIdPuesto
        StrSql = "select idPuesto,NomPuesto from " & _TablaBd & " where idPuesto = " & vIdPuesto

        DsClase = BD.ExecuteReturn(StrSql)
        If DsClase.Rows.Count > 0 Then
            With DsClase.Rows(0)
                _NomPuesto = .Item("NomPuesto") & ""
            End With
            _Existe = True
        Else
            _Existe = False
        End If

    End Sub
    Sub New(ByVal vIdPuesto As Integer, ByVal vNomPuesto As String)
        _idpuesto = vIdPuesto
        _NomPuesto = vNomPuesto
        _Existe = True
    End Sub
#End Region

#Region "Propiedades"
    Public Property idPuesto As Integer
        Get
            Return _idPuesto
        End Get
        Set(ByVal value As Integer)
            _idPuesto = value
        End Set
    End Property
    Public Property NomPuesto As String
        Get
            Return _NomPuesto
        End Get
        Set(ByVal value As String)
            _NomPuesto = value
        End Set
    End Property
    Public Property Existe As Boolean
        Get
            Return _Existe
        End Get
        Set(ByVal value As Boolean)
            _Existe = value
        End Set
    End Property

#End Region

#Region "Funciones"
    Public Function GetCambios(ByVal vNomPuesto As String) As String
        Dim CadCam As String = ""
        If _NomPuesto <> vNomPuesto Then
            CadCam += "Nombre Puesto: '" & _NomPuesto & "' Cambia a [" & vNomPuesto & "],"
        End If
        If CadCam <> "" Then
            CadCam = Mid(CadCam, 1, CadCam.Length - 1)
        End If
        Return CadCam
    End Function
    Public Sub Guardar(ByVal vNomPuesto As String, ByVal bEsIdentidad As Boolean)
        Dim sSql As String = "", _CadValAnt As String = "", _CadValNue As String = ""
        If _Existe Then
            If _NomPuesto <> vNomPuesto Then
                sSql += "NomPuesto = '" & vNomPuesto & "',"
                _CadValAnt += "NomPuesto =" & vNomPuesto & ","
            End If

            If sSql <> "" Then
                sSql = Mid(sSql, 1, sSql.Length - 1) 'Para quitar la Coma ','
                _CadValAnt = "idPuesto=" & _idPuesto & "," & _CadValAnt
                _CadValAnt = Mid(_CadValAnt, 1, _CadValAnt.Length - 1)
                _CadValNue = Replace(sSql, "'", "")
                sSql = "UPDATE " & _TablaBd & " SET " & sSql & " WHERE idPuesto = '" & _idPuesto & "'"
                Try
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                    BD.Execute(sSql, True)
                    Audita(_TablaBd, _CadValAnt, _CadValNue, EnTipAccion.Modifica)
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
                Catch ex As Exception
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                    Throw New Exception(ex.Message)
                End Try
            End If
        Else
            If bEsIdentidad Then
                sSql = "INSERT INTO " & _TablaBd & "(NomPuesto) VALUES('" & _NomPuesto & "')"

                _CadValNue = "NomPuesto=" & _NomPuesto
            Else

                sSql = "INSERT INTO " & _TablaBd & "(idPuesto,NomPuesto) VALUES(" &
                "'" & _idPuesto & "','" & _NomPuesto & "')"

                _CadValNue = "idPuesto=" & _idPuesto & ",NomPuesto=" & _NomPuesto
            End If

            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, "", _CadValNue, EnTipAccion.Inserta)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
        End If
        _NomPuesto = vNomPuesto
    End Sub
    Public Sub Eliminar()
        If _Existe Then
            Dim sSql As String = "", _CadValAnt As String = ""
            _CadValAnt = "idPuesto=" & _idPuesto & " , NomPuesto =" & _NomPuesto
            sSql = "DELETE FROM " & _TablaBd & " WHERE idPuesto = '" & _idPuesto & "'"
            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, _CadValAnt, "", EnTipAccion.Elimina)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
            _Existe = False
        Else
            'se supone que nunca debe entrar aqui. . .
            MessageBox.Show("El tipo de Puesto con Id: " & _idPuesto & " No existe por lo tanto no se puede Eliminar!!!",
                            "No se puede eliminar el tipo de Puesto porque no Existe", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub
#End Region
End Class
