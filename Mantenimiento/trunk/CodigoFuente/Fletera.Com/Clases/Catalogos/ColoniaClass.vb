﻿Public Class ColoniaClass
    'idColonia			vidColonia				_idColonia 
    'NomColonia			vNomColonia 			_NomColonia
    'CP					vCP 					_CP
    'idCiudad			vidCiudad 				_idCiudad
    ''NomCiudad			vNomCiudad				_NomCiudad
    'idEstado			vidEstado				_idEstado
    'NomEstado			vNomEstado				_NomEstado
    'idPais				vidPais					_idPais
    'NomPais				vNomPais				_NomPais

    Private _idColonia As Integer
    Private _NomColonia As String
    Private _idCiudad As Integer
    Private _CP As String

    Private _NomCiudad As String
    Private _idEstado As Integer
    Private _NomEstado As String
    Private _idPais As Integer
    Private _NomPais As String

    Private _Existe As Boolean
    Private _TablaBd As String = "CatColonia"

    Dim StrSql As String
    Dim DsClase As New DataTable
    Dim MyTabla As DataTable = New DataTable(_TablaBd)
#Region "Constructor"
    Sub New(ByVal vidColonia As Integer, Optional ByVal NomColonia As String = "", Optional ByVal NomCiudad As String = "")
        'Dim DtEst As New DataTable
        Dim filtro As String
        Dim sComando As String = ""

        If NomColonia <> "" And NomCiudad <> "" Then
            filtro = "WHERE col.NomColonia = '" & NomColonia & "' AND ciu.NomCiudad = '" & NomCiudad & "'"
            sComando = " DISTINCT "
        ElseIf NomColonia <> "" Then
            filtro = " where col.NomColonia = '" & NomColonia & "'"
            sComando = " DISTINCT "
        Else
            filtro = " where col.idColonia = " & vidColonia

        End If
        _idColonia = vidColonia


        StrSql = "select " & sComando & "col.idColonia, col.NomColonia, col.CP, " &
        "col.idCiudad, ciu.NomCiudad, " &
        "ciu.idEstado, est.NomEstado, " &
        "est.idPais, pais.NomPais " &
        "from " & _TablaBd & " col " &
        "left join CatCiudad ciu on col.idCiudad = ciu.idCiudad " &
        "left join CatEstados est on ciu.idEstado = est.idEstado " &
        "left join CatPais pais on est.idPais = pais.idPais " & filtro


        DsClase = BD.ExecuteReturn(StrSql)
        If DsClase.Rows.Count > 0 Then
            With DsClase.Rows(0)
                If NomColonia <> "" Then
                    _idColonia = .Item("idColonia") & ""
                End If

                _NomColonia = .Item("NomColonia") & ""
                _idCiudad = .Item("idCiudad")
                _NomCiudad = .Item("NomCiudad")
                _CP = .Item("CP")
                '_NomCiudad = .Item("NomCiudad")
                _idEstado = .Item("idEstado")
                _NomEstado = .Item("NomEstado")
                _idPais = .Item("idPais")
                _NomPais = .Item("NomPais")
            End With
            _Existe = True
        Else
            _Existe = False
        End If

    End Sub
    Sub New(ByVal vidColonia As Integer, ByVal vNomColonia As String, ByVal vCP As String, ByVal vidCiudad As Integer, _
            ByVal vNomCiudad As String, ByVal vidEstado As Integer, ByVal vNomEstado As String, ByVal vidPais As Integer, _
            ByVal vNomPais As String)
        _idColonia = vidColonia
        _NomColonia = vNomColonia
        _CP = vCP
        _idCiudad = vidCiudad
        _NomCiudad = vNomCiudad
        _idEstado = vidEstado
        _NomEstado = vNomEstado
        _idPais = vidPais
        _NomPais = vNomPais

        _Existe = True
    End Sub
#End Region
#Region "Propiedades"
    'Public ReadOnly Property idDivision As Integer
    '    Get
    '        Return _idCiudad
    '    End Get
    'End Property
    Public Property idColonia As Integer
        Get
            Return _idColonia
        End Get
        Set(ByVal value As Integer)
            _idColonia = value
        End Set
    End Property
    Public Property NomColonia As String
        Get
            Return _NomColonia
        End Get
        Set(ByVal value As String)
            _NomColonia = value
        End Set
    End Property
    Public Property CP As String
        Get
            Return _CP
        End Get
        Set(ByVal value As String)
            _CP = value
        End Set
    End Property
    Public Property idCiudad As Integer
        Get
            Return _idCiudad
        End Get
        Set(ByVal value As Integer)
            _idCiudad = value
        End Set
    End Property
    Public Property NomCiudad As String
        Get
            Return _NomCiudad
        End Get
        Set(ByVal value As String)
            _NomCiudad = value
        End Set
    End Property
    Public Property idEstado As Integer
        Get
            Return _idEstado
        End Get
        Set(ByVal value As Integer)
            _idEstado = value
        End Set
    End Property
    Public Property NomEstado As String
        Get
            Return _NomEstado
        End Get
        Set(ByVal value As String)
            _NomEstado = value
        End Set
    End Property
    Public Property idPais As Integer
        Get
            Return _idPais
        End Get
        Set(ByVal value As Integer)
            _idPais = value
        End Set
    End Property
    Public Property NomPais As String
        Get
            Return _NomPais
        End Get
        Set(ByVal value As String)
            _NomPais = value
        End Set
    End Property
    'Public ReadOnly Property Existe As Boolean
    '    Get
    '        Return _Existe
    '    End Get
    'End Property
    Public Property Existe As Boolean
        Get
            Return _Existe
        End Get
        Set(ByVal value As Boolean)
            _Existe = value
        End Set
    End Property

#End Region
#Region "Funciones"
    'Public Function GetCambios(ByVal vNomColonia As String, ByVal vCP As String, ByVal vidCiudad As Integer, ByVal vNomCiudad As String, _
    '    ByVal vidEstado As Integer, ByVal vNomEstado As String, ByVal vidPais As Integer, ByVal vNomPais As String) As String
    Public Function GetCambios(ByVal vNomColonia As String, ByVal vCP As String, ByVal vidCiudad As Integer) As String

        Dim CadCam As String = ""
        If _NomColonia <> vNomColonia Then
            CadCam += "NomColonia: '" & _NomColonia & "' Cambia a [" & vNomColonia & "],"
        End If
        If _CP <> vCP Then
            CadCam += "CP: '" & _CP & "' Cambia a [" & vCP & "],"
        End If
        If _idCiudad <> vidCiudad Then
            CadCam += "idCiudad: '" & _idCiudad & "' Cambia a [" & vidCiudad & "],"
        End If
        'If _NomCiudad <> vNomCiudad Then
        '    CadCam += "NomCiudad: '" & _NomCiudad & "' Cambia a [" & vNomCiudad & "],"
        'End If
        'If _idEstado <> vidEstado Then
        '    CadCam += "idEstado: '" & _idEstado & "' Cambia a [" & vidEstado & "],"
        'End If
        'If _NomEstado <> vNomEstado Then
        '    CadCam += "NomEstado: '" & _NomEstado & "' Cambia a [" & vNomEstado & "],"
        'End If
        'If _idPais <> vidPais Then
        '    CadCam += "idPais: '" & _idPais & "' Cambia a [" & vidPais & "],"
        'End If
        'If _NomPais <> vNomPais Then
        '    CadCam += "NomPais: '" & _NomPais & "' Cambia a [" & vNomPais & "],"
        'End If
        If CadCam <> "" Then
            CadCam = Mid(CadCam, 1, CadCam.Length - 1)
        End If
        Return CadCam
    End Function
    'Public Sub Guardar(ByVal vNomColonia As String, ByVal vCP As String, ByVal vidCiudad As Integer, ByVal bEsIdentidad As Boolean, _
    'ByVal vNomCiudad As String, ByVal vidEstado As Integer, ByVal vNomEstado As String, ByVal vidPais As Integer, ByVal vNomPais As String)
    Public Sub Guardar(ByVal vNomColonia As String, ByVal vCP As String, ByVal vidCiudad As Integer, ByVal bEsIdentidad As Boolean)

        Dim sSql As String = "", _CadValAnt As String = "", _CadValNue As String = ""
        If _Existe Then
            If _NomColonia <> vNomColonia Then
                sSql += "NomColonia = '" & vNomColonia & "',"
                _CadValAnt += "NomColonia =" & vNomColonia & ","
            End If
            If _CP <> vCP Then
                sSql += "CP = '" & vCP & "',"
                _CadValAnt += "CP =" & vCP & ","
            End If
            If _idCiudad <> vidCiudad Then
                sSql += "idCiudad = " & vidCiudad & ","
                _CadValAnt += "idCiudad =" & vidCiudad & ","
            End If
            'If _NomCiudad <> vNomCiudad Then
            '    sSql += "NomCiudad = '" & vNomCiudad & "',"
            '    _CadValAnt += "NomCiudad =" & vNomCiudad & ","
            'End If
            'If _idEstado <> vidEstado Then
            '    sSql += "idEstado = " & vidEstado & ","
            '    _CadValAnt += "idEstado =" & vidEstado & ","
            'End If
            'If _NomEstado <> vNomEstado Then
            '    sSql += "NomEstado = '" & vNomEstado & "',"
            '    _CadValAnt += "NomEstado =" & vNomEstado & ","
            'End If
            'If _idPais <> vidPais Then
            '    sSql += "idPais = " & vidPais & ","
            '    _CadValAnt += "idPais =" & vidPais & ","
            'End If
            'If _NomPais <> vNomPais Then
            '    sSql += "NomPais = '" & vNomPais & "',"
            '    _CadValAnt += "NomPais =" & vNomPais & ","
            'End If

            If sSql <> "" Then
                sSql = Mid(sSql, 1, sSql.Length - 1) 'Para quitar la Coma ','
                _CadValAnt = "idColonia=" & _idColonia & "," & _CadValAnt
                _CadValAnt = Mid(_CadValAnt, 1, _CadValAnt.Length - 1)
                _CadValNue = Replace(sSql, "'", "")

                sSql = "UPDATE " & _TablaBd & " SET " & sSql & " WHERE idColonia = " & _idColonia
                Try
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                    BD.Execute(sSql, True)
                    Audita(_TablaBd, _CadValAnt, _CadValNue, EnTipAccion.Modifica)
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
                Catch ex As Exception
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                    Throw New Exception(ex.Message)
                End Try
            End If
        Else
            If bEsIdentidad Then
                sSql = "INSERT INTO " & _TablaBd & "(NomColonia,idCiudad,CP) VALUES('" & vNomColonia & "'," & vidCiudad & ",'" & vCP & "')"

                _CadValNue = "NomColonia=" & vNomColonia & "idCiudad = " & vidCiudad & "CP = " & vCP
            Else
                sSql = "INSERT INTO " & _TablaBd & "(idColonia,NomColonia,idCiudad,CP) VALUES " & _
                "(" & _idColonia & ",'" & vNomColonia & "'," & vidCiudad & ",'" & vCP & "')"

                _CadValNue = "idColonia=" & _idColonia & "NomColonia=" & vNomColonia & "idCiudad = " & vidCiudad & "CP = " & vCP
            End If

            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, "", _CadValNue, EnTipAccion.Inserta)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
        End If
        _NomColonia = vNomColonia
        _CP = vCP
        _idCiudad = vidCiudad
        '_NomCiudad = vNomCiudad
        '_idEstado = vidEstado
        '_NomEstado = vNomEstado
        '_idPais = vidPais
        '_NomPais = vNomPais
    End Sub
    Public Sub Eliminar()
        If _Existe Then
            Dim sSql As String = "", _CadValAnt As String = ""
            _CadValAnt = "idColonia=" & _idColonia & " , NomColonia =" & _NomColonia & " , CP =" & _CP
            sSql = "DELETE FROM " & _TablaBd & " WHERE idColonia = '" & _idColonia & "'"
            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, _CadValAnt, "", EnTipAccion.Elimina)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
            _Existe = False
        Else
            'se supone que nunca debe entrar aqui. . .
            MessageBox.Show("La Colonia con Id: " & _idColonia & " No existe por lo tanto no se puede Eliminar!!!",
                            "No se puede eliminar la Colonia porque no Existe", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub
#End Region

#Region "Consultas"
    Public Function TablaColonias(ByVal bIncluyeTodos As Boolean) As DataTable
        MyTabla.Rows.Clear()

        If bIncluyeTodos Then
            StrSql = "SELECT 0 AS idColonia,'TODAS' AS NomColonia,'' as CP, 0 as idCiudad  UNION SELECT idColonia,NomColonia,CP,idCiudad FROM dbo.CatColonia ORDER BY idColonia"
        Else
            StrSql = "SELECT idColonia,NomColonia,CP,idCiudad FROM dbo.CatColonia ORDER BY idColonia"
        End If
        MyTabla = BD.ExecuteReturn(StrSql)

        Return MyTabla

    End Function
#End Region


End Class
