﻿Public Class CiudadClass
    'idCiudad       vidCiudad
    'NomCiudad      vNomCiudad
    'idEstado       vidEstado
    'NomEstado      vNomEstado
    Private _idCiudad As Integer
    Private _NomCiudad As String
    Private _idEstado As Integer
    Private _NomEstado As String
    Private _NomPais As String

    Private _Existe As Boolean
    Private _TablaBd As String = "CatCiudad"

    Dim StrSql As String
    Dim DsClase As New DataTable
    Dim MyTabla As DataTable = New DataTable(_TablaBd)
#Region "Constructor"
    Sub New(ByVal vidCiudad As Integer)
        'Dim DtEst As New DataTable
        _idCiudad = vidCiudad

        StrSql = "select ciu.idCiudad , ciu.NomCiudad , ciu.idEstado , est.NomEstado, p.NomPais " & _
        "from " & _TablaBd & " ciu " & _
        "inner join catestados Est on ciu.idEstado = est.idEstado " & _
        "INNER JOIN dbo.CatPais p ON est.idPais = p.idPais " &
        "where ciu.idCiudad = " & vidCiudad

        DsClase = BD.ExecuteReturn(StrSql)
        If DsClase.Rows.Count > 0 Then
            With DsClase.Rows(0)
                _NomCiudad = .Item("NomCiudad") & ""
                _idEstado = .Item("idEstado")
                _NomEstado = .Item("NomEstado")
                _NomPais = .Item("NomPais")
            End With
            _Existe = True
        Else
            _Existe = False
        End If

    End Sub
    Sub New(ByVal vidCiudad As Integer, ByVal vNomCiudad As String, ByVal vidEstado As Integer, ByVal vNomEstado As String, ByVal vNomPais As String)
        _idCiudad = vidCiudad
        _NomCiudad = vNomCiudad
        _idEstado = vidEstado
        _NomEstado = vNomEstado
        _NomPais = vNomPais
        _Existe = True
    End Sub
#End Region
#Region "Propiedades"
    'Public ReadOnly Property idDivision As Integer
    '    Get
    '        Return _idEstado
    '    End Get
    'End Property
    Public Property idCiudad As Integer
        Get
            Return _idCiudad
        End Get
        Set(ByVal value As Integer)
            _idCiudad = value
        End Set
    End Property
    Public Property NomCiudad As String
        Get
            Return _NomCiudad
        End Get
        Set(ByVal value As String)
            _NomCiudad = value
        End Set
    End Property
    Public Property idEstado As Integer
        Get
            Return _idEstado
        End Get
        Set(ByVal value As Integer)
            _idEstado = value
        End Set
    End Property
    Public Property NomEstado As String
        Get
            Return _NomEstado
        End Get
        Set(ByVal value As String)
            _NomEstado = value
        End Set
    End Property
    Public Property NomPais As String
        Get
            Return _NomPais
        End Get
        Set(ByVal value As String)
            _NomPais = value
        End Set
    End Property
    'Public ReadOnly Property Existe As Boolean
    '    Get
    '        Return _Existe
    '    End Get
    'End Property
    Public Property Existe As Boolean
        Get
            Return _Existe
        End Get
        Set(ByVal value As Boolean)
            _Existe = value
        End Set
    End Property

#End Region
#Region "Funciones"
    Public Function GetCambios(ByVal vNomCiudad As String, ByVal vidEstado As Integer) As String
        Dim CadCam As String = ""
        If _NomCiudad <> vNomCiudad Then
            CadCam += "Nombre Ciudad: '" & _NomCiudad & "' Cambia a [" & vNomCiudad & "],"
        End If
        If _idEstado <> vidEstado Then
            CadCam += "id Estado: '" & _idEstado & "' Cambia a [" & vidEstado & "],"
        End If

        If CadCam <> "" Then
            CadCam = Mid(CadCam, 1, CadCam.Length - 1)
        End If
        Return CadCam
    End Function
    Public Sub Guardar(ByVal vNomCiudad As String, ByVal vidEstado As Integer, ByVal bEsIdentidad As Boolean, ByVal vNomEstado As String)
        Dim sSql As String = "", _CadValAnt As String = "", _CadValNue As String = ""
        If _Existe Then
            If _NomCiudad <> vNomCiudad Then
                sSql += "NomCiudad = '" & vNomCiudad & "',"
                _CadValAnt += "NomCiudad =" & vNomCiudad & ","
            End If
            If _idEstado <> vidEstado Then
                sSql += "idEstado = " & vidEstado & ","
                _CadValAnt += "idEstado =" & vidEstado & ","
            End If
            If sSql <> "" Then
                sSql = Mid(sSql, 1, sSql.Length - 1) 'Para quitar la Coma ','
                _CadValAnt = "idCiudad=" & _idCiudad & "," & _CadValAnt
                _CadValAnt = Mid(_CadValAnt, 1, _CadValAnt.Length - 1)
                _CadValNue = Replace(sSql, "'", "")

                sSql = "UPDATE " & _TablaBd & " SET " & sSql & " WHERE idCiudad = '" & _idCiudad & "'"
                Try
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                    BD.Execute(sSql, True)
                    Audita(_TablaBd, _CadValAnt, _CadValNue, EnTipAccion.Modifica)
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
                Catch ex As Exception
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                    Throw New Exception(ex.Message)
                End Try
            End If
        Else
            If bEsIdentidad Then
                sSql = "INSERT INTO " & _TablaBd & "(NomCiudad,idEstado) VALUES('" & vNomCiudad & "'," & vidEstado & ")"

                _CadValNue = "NomCiudad=" & vNomCiudad & "idEstado = " & vidEstado
            Else

                sSql = "INSERT INTO " & _TablaBd & "(idCiudad,NomCiudad,idEstado) VALUES(" &
                "'" & _idCiudad & "','" & vNomCiudad & "'," & vidEstado & ")"

                _CadValNue = "idCiudad=" & _idCiudad & ",NomCiudad=" & vNomCiudad & ",idEstado=" & vidEstado
            End If

            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, "", _CadValNue, EnTipAccion.Inserta)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
        End If
        _NomCiudad = vNomCiudad
        _idEstado = vidEstado
        _NomEstado = vNomEstado
    End Sub
    Public Sub Eliminar()
        If _Existe Then
            Dim sSql As String = "", _CadValAnt As String = ""
            _CadValAnt = "idCiudad=" & _idCiudad & " , NomCiudad =" & _NomCiudad
            sSql = "DELETE FROM " & _TablaBd & " WHERE idCiudad = '" & _idCiudad & "'"
            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, _CadValAnt, "", EnTipAccion.Elimina)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
            _Existe = False
        Else
            'se supone que nunca debe entrar aqui. . .
            MessageBox.Show("La Ciudad con Id: " & _idCiudad & " No existe por lo tanto no se puede Eliminar!!!",
                            "No se puede eliminar la Ciudad porque no Existe", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub
#End Region

#Region "Consultas"
    Public Function TablaCiudades(ByVal bIncluyeTodos As Boolean, Optional ByVal FiltroSinWhere As String = "") As DataTable
        Dim filtro As String = ""
        MyTabla.Rows.Clear()

        If FiltroSinWhere <> "" Then
            filtro = " where " & FiltroSinWhere
        End If

        If bIncluyeTodos Then
            StrSql = "SELECT 0 AS idCiudad,'TODAS' AS NomCiudad,0 AS idEstado " &
            "union " &
            "SELECT idCiudad,NomCiudad,idEstado FROM dbo.CatCiudad " & filtro &
            " ORDER BY idCiudad"
        Else
            StrSql = "SELECT idCiudad,NomCiudad,idEstado FROM dbo.CatCiudad " & filtro &
            " ORDER BY idCiudad"

        End If
        MyTabla = BD.ExecuteReturn(StrSql)


        Return MyTabla

    End Function
#End Region
End Class
