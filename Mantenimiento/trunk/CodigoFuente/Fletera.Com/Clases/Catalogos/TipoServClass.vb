﻿Public Class TipoServClass
    'idTipoServicio
    'NomTipoServicio

    Private _idTipoServicio As Integer
    Private _NomTipoServicio As String

    Private _Existe As Boolean
    Private _TablaBd As String = "CatTipoServicio"

    Dim StrSql As String
    Dim DsClase As New DataTable
    Dim MyTabla As DataTable = New DataTable(_TablaBd)

#Region "Constructor"
    Sub New(ByVal vidTipoServicio As Integer)
        'Dim DtEst As New DataTable
        _idTipoServicio = vidTipoServicio
        StrSql = "select idTipoServicio,NomTipoServicio from " & _TablaBd & " where idTipoServicio = " & vidTipoServicio

        DsClase = BD.ExecuteReturn(StrSql)
        If DsClase.Rows.Count > 0 Then
            With DsClase.Rows(0)
                _NomTipoServicio = .Item("NomTipoServicio") & ""
            End With
            _Existe = True
        Else
            _Existe = False
        End If

    End Sub
    Sub New(ByVal FiltroSinWhere As String)
        StrSql = "select idTipoServicio,NomTipoServicio from " & _TablaBd & IIf(FiltroSinWhere = "", "", " WHERE " & FiltroSinWhere)

        DsClase = BD.ExecuteReturn(StrSql)
        If DsClase.Rows.Count > 0 Then
            With DsClase.Rows(0)
                _idTipoServicio = .Item("idTipoServicio")
                _NomTipoServicio = .Item("NomTipoServicio") & ""
            End With
            _Existe = True
        Else
            _Existe = False
        End If

    End Sub

    Sub New(ByVal vidTipoServicio As Integer, ByVal vNomTipoServicio As String)
        _idTipoServicio = vidTipoServicio
        _NomTipoServicio = vNomTipoServicio
        _Existe = True
    End Sub
#End Region
#Region "Propiedades"
    'Public ReadOnly Property idDivision As Integer
    '    Get
    '        Return _idDivision
    '    End Get
    'End Property
    Public Property idTipoServicio As Integer
        Get
            Return _idTipoServicio
        End Get
        Set(ByVal value As Integer)
            _idTipoServicio = value
        End Set
    End Property
    Public Property NomTipoServicio As String
        Get
            Return _NomTipoServicio
        End Get
        Set(ByVal value As String)
            _NomTipoServicio = value
        End Set
    End Property
    'Public ReadOnly Property Existe As Boolean
    '    Get
    '        Return _Existe
    '    End Get
    'End Property
    Public Property Existe As Boolean
        Get
            Return _Existe
        End Get
        Set(ByVal value As Boolean)
            _Existe = value
        End Set
    End Property

#End Region
#Region "Funciones"
    Public Function GetCambios(ByVal vNomTipoServicio As String) As String
        Dim CadCam As String = ""
        If _NomTipoServicio <> vNomTipoServicio Then
            CadCam += "Nombre Tipo Servicio: '" & _NomTipoServicio & "' Cambia a [" & vNomTipoServicio & "],"
        End If
        If CadCam <> "" Then
            CadCam = Mid(CadCam, 1, CadCam.Length - 1)
        End If
        Return CadCam
    End Function
    Public Sub Guardar(ByVal vNomTipoServicio As String, ByVal bEsIdentidad As Boolean)
        Dim sSql As String = "", _CadValAnt As String = "", _CadValNue As String = ""
        If _Existe Then
            If _NomTipoServicio <> vNomTipoServicio Then
                sSql += "NomTipoServicio = '" & vNomTipoServicio & "',"
                _CadValAnt += "NomTipoServicio =" & vNomTipoServicio & ","
            End If

            If sSql <> "" Then
                sSql = Mid(sSql, 1, sSql.Length - 1) 'Para quitar la Coma ','
                _CadValAnt = "idTipoServicio=" & _idTipoServicio & "," & _CadValAnt
                _CadValAnt = Mid(_CadValAnt, 1, _CadValAnt.Length - 1)
                _CadValNue = Replace(sSql, "'", "")
                sSql = "UPDATE " & _TablaBd & " SET " & sSql & " WHERE idTipoServicio = '" & _idTipoServicio & "'"
                Try
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                    BD.Execute(sSql, True)
                    Audita(_TablaBd, _CadValAnt, _CadValNue, EnTipAccion.Modifica)
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
                Catch ex As Exception
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                    Throw New Exception(ex.Message)
                End Try
            End If
        Else
            If bEsIdentidad Then
                sSql = "INSERT INTO " & _TablaBd & "(NomTipoServicio) VALUES('" & _NomTipoServicio & "')"

                _CadValNue = "NomTipoServicio=" & _NomTipoServicio
            Else

                sSql = "INSERT INTO " & _TablaBd & "(idTipoServicio,NomTipoServicio) VALUES(" &
                "'" & _idTipoServicio & "','" & _NomTipoServicio & "')"

                _CadValNue = "idTipoServicio=" & _idTipoServicio & ",NomTipoServicio=" & _NomTipoServicio
            End If

            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, "", _CadValNue, EnTipAccion.Inserta)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
        End If
        _NomTipoServicio = vNomTipoServicio
    End Sub
    Public Sub Eliminar()
        If _Existe Then
            Dim sSql As String = "", _CadValAnt As String = ""
            _CadValAnt = "idTipoServicio=" & _idTipoServicio & " , NomTipoServicio =" & _NomTipoServicio
            sSql = "DELETE FROM " & _TablaBd & " WHERE idTipoServicio = '" & _idTipoServicio & "'"
            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, _CadValAnt, "", EnTipAccion.Elimina)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
            _Existe = False
        Else
            'se supone que nunca debe entrar aqui. . .
            MessageBox.Show("El tipo de Servicio con Id: " & _idTipoServicio & " No existe por lo tanto no se puede Eliminar!!!",
                            "No se puede eliminar el tipo de Servicio porque no Existe", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub
#End Region
#Region "Consultas"
    Public Function TablaTipoServicio_RelTipOrdServ(ByVal idTipOrdServ As Integer) As DataTable
        MyTabla.Rows.Clear()

        StrSql = "SELECT * FROM dbo.CatTipoServicio TS " &
        "INNER JOIN RelTipOrdServ R ON TS.idTipoServicio = r.IdRelacion AND  r.NomCatalogo = 'CatTipoServicio' " &
        "WHERE r.idTipOrdServ = 3"

        MyTabla = BD.ExecuteReturn(StrSql)

        Return MyTabla

    End Function
    Public Function TablaTiposOrdenServicio(ByVal FiltroSinWhere As String) As DataTable
        'Dim Filtro As String = ""
        MyTabla.Rows.Clear()

        StrSql = "SELECT idTipOrdServ,NomTipOrdServ,bOrdenTrab FROM CatTipoOrdServ ORDER BY NUMORDEN"

        'StrSql = " select 0 as idpersonal, 0 as idempresa, '<NINGUNO>' as nombrecompleto, " & _
        '" 0 as idpuesto, 'SIN PUESTO' as Nompuesto, " & _
        '" 0 as idDepto, '' nomdepto " & _
        '" UNION  " & _
        '"select per.idpersonal, per.idempresa, Per.nombrecompleto, " & _
        '"per.idpuesto, pue.Nompuesto, " & _
        '"per.idDepto, dep.nomdepto " & _
        '"from CatPersonal Per " & _
        '"inner join catpuestos pue on per.idPuesto = pue.idPuesto " & _
        '"inner join catdeptos dep on per.idDepto = dep.idDepto " & _
        'IIf(FiltroSinWhere <> "", "where " & FiltroSinWhere, "") & " order by nomdepto,idpuesto,nombrecompleto"

        MyTabla = BD.ExecuteReturn(StrSql)

        Return MyTabla

    End Function
    Public Function TablaCatTipoServicio(ByVal FiltroSinWhere As String) As DataTable
        'Dim Filtro As String = ""
        MyTabla.Rows.Clear()

        StrSql = "SELECT idTipoServicio,NomTipoServicio FROM CatTipoServicio"

        'StrSql = " select 0 as idpersonal, 0 as idempresa, '<NINGUNO>' as nombrecompleto, " & _
        '" 0 as idpuesto, 'SIN PUESTO' as Nompuesto, " & _
        '" 0 as idDepto, '' nomdepto " & _
        '" UNION  " & _
        '"select per.idpersonal, per.idempresa, Per.nombrecompleto, " & _
        '"per.idpuesto, pue.Nompuesto, " & _
        '"per.idDepto, dep.nomdepto " & _
        '"from CatPersonal Per " & _
        '"inner join catpuestos pue on per.idPuesto = pue.idPuesto " & _
        '"inner join catdeptos dep on per.idDepto = dep.idDepto " & _
        'IIf(FiltroSinWhere <> "", "where " & FiltroSinWhere, "") & " order by nomdepto,idpuesto,nombrecompleto"

        MyTabla = BD.ExecuteReturn(StrSql)

        Return MyTabla

    End Function
#End Region
End Class
