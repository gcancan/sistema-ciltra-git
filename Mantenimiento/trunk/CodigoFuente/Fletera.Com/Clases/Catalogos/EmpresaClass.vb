﻿Public Class EmpresaClass
    'idEmpresa				vidEmpresa
    'RazonSocial				vRazonSocial
    'Representante			vRepresentante
    'Direccion				vDireccion
    'idColonia				vidColonia
    'Telefono				vTelefono
    'Fax						vFax
    'MovilRepresentante		vMovilRepresentante
    'CIDEMPRESA_COM			vCIDEMPRESA_COM
    Private _idEmpresa As Integer
    Private _RazonSocial As String
    Private _Representante As String
    Private _Direccion As String
    Private _idColonia As Integer
    Private _Telefono As String
    Private _Fax As String
    Private _MovilRepresentante As String
    Private _CIDEMPRESA_COM As Integer
    Private _PrefLla As String
    Private _NomServidor As String

    Private _NomBaseDatosCOM As String

    Private _Existe As Boolean
    Private _TablaBd As String = "CatEmpresas"

    Dim StrSql As String
    Dim DsClase As New DataTable
    Dim MyTabla As DataTable = New DataTable(_TablaBd)
#Region "Constructor"
    Sub New(ByVal vidEmpresa As Integer)
        'Dim DtEst As New DataTable
        _idEmpresa = vidEmpresa

        StrSql = "select emp.idEmpresa, emp.RazonSocial, emp.Direccion, " &
        "emp.idColonia, col.NomColonia, col.CP, isnull(emp.NomBaseDatosCOM,'') as NomBaseDatosCOM, " &
        "isnull(col.idCiudad,0) as idCiudad, ciu.NomCiudad, ciu.idEstado, est.NomEstado, est.idPais, pais.NomPais, " &
        "emp.Telefono, emp.Fax, emp.Representante, emp.MovilRepresentante,isnull(emp.CIDEMPRESA_COM,0) as CIDEMPRESA_COM, isnull(emp.PrefLla,'') as PrefLla, NomServidor " &
        "from " & _TablaBd & " emp " &
        "left join catcolonia col on emp.idColonia = col.idColonia " &
        "left join CatCiudad ciu on col.idCiudad = ciu.idCiudad " &
        "left join CatEstados est on ciu.idEstado = est.idEstado " &
        "left join CatPais pais on est.idPais = pais.idPais " &
        "where emp.idEmpresa = " & vidEmpresa


        DsClase = BD.ExecuteReturn(StrSql)
        If DsClase.Rows.Count > 0 Then
            With DsClase.Rows(0)
                _RazonSocial = .Item("RazonSocial") & ""
                _Representante = .Item("Representante")
                _Direccion = .Item("Direccion")
                _idColonia = .Item("idColonia")
                _Telefono = .Item("Telefono")
                _Fax = .Item("Fax")
                _MovilRepresentante = .Item("MovilRepresentante")
                _CIDEMPRESA_COM = .Item("CIDEMPRESA_COM")
                _PrefLla = .Item("PrefLla")
                _NomBaseDatosCOM = .Item("NomBaseDatosCOM")
                _NomServidor = .Item("NomServidor")
            End With
            _Existe = True
        Else
            _Existe = False
        End If

    End Sub
    Sub New(ByVal vidEmpresa As Integer, ByVal vRazonSocial As String, ByVal vRepresentante As String, ByVal vDireccion As String _
            , ByVal vidColonia As Integer, ByVal vTelefono As String, ByVal vFax As String, ByVal vMovilRepresentante As String _
            , ByVal vCIDEMPRESA_COM As Integer, ByVal vPrefLla As String, ByVal vNomBaseDatosCOM As String, ByVal vNomServidor As String)
        _idEmpresa = vidEmpresa
        _RazonSocial = vRazonSocial
        _Representante = vRepresentante
        _Direccion = vDireccion
        _idColonia = vidColonia
        _Telefono = vTelefono
        _Fax = vFax
        _MovilRepresentante = vMovilRepresentante
        _CIDEMPRESA_COM = vCIDEMPRESA_COM
        _PrefLla = vPrefLla
        _NomBaseDatosCOM = vNomBaseDatosCOM
        _NomServidor = vNomServidor
        _Existe = True
    End Sub
#End Region

#Region "Propiedades"
    'Public ReadOnly Property idDivision As Integer
    '    Get
    '        Return _idEmpresa
    '    End Get
    'End Property
    Public Property idEmpresa As Integer
        Get
            Return _idEmpresa
        End Get
        Set(ByVal value As Integer)
            _idEmpresa = value
        End Set
    End Property
    Public Property RazonSocial As String
        Get
            Return _RazonSocial
        End Get
        Set(ByVal value As String)
            _RazonSocial = value
        End Set
    End Property
    Public Property Representante As String
        Get
            Return _Representante
        End Get
        Set(ByVal value As String)
            _Representante = value
        End Set
    End Property
    Public Property Direccion As String
        Get
            Return _Direccion
        End Get
        Set(ByVal value As String)
            _Direccion = value
        End Set
    End Property
    Public Property idColonia As Integer
        Get
            Return _idColonia
        End Get
        Set(ByVal value As Integer)
            _idColonia = value
        End Set
    End Property
    Public Property Telefono As String
        Get
            Return _Telefono
        End Get
        Set(ByVal value As String)
            _Telefono = value
        End Set
    End Property
    Public Property Fax As String
        Get
            Return _Fax
        End Get
        Set(ByVal value As String)
            _Fax = value
        End Set
    End Property
    Public Property MovilRepresentante As String
        Get
            Return _MovilRepresentante
        End Get
        Set(ByVal value As String)
            _MovilRepresentante = value
        End Set
    End Property
    Public Property CIDEMPRESA_COM As Integer
        Get
            Return _CIDEMPRESA_COM
        End Get
        Set(ByVal value As Integer)
            _CIDEMPRESA_COM = value
        End Set
    End Property
    'Public ReadOnly Property Existe As Boolean
    '    Get
    '        Return _Existe
    '    End Get
    'End Property
    Public Property PrefLla As String
        Get
            Return _PrefLla
        End Get
        Set(ByVal value As String)
            _PrefLla = value
        End Set
    End Property
    Public Property NomBaseDatosCOM As String
        Get
            Return _NomBaseDatosCOM
        End Get
        Set(ByVal value As String)
            _NomBaseDatosCOM = value
        End Set
    End Property


    Public Property NomServidor As String
        Get
            Return _NomServidor
        End Get
        Set(ByVal value As String)
            _NomServidor = value
        End Set
    End Property

    Public Property Existe As Boolean
        Get
            Return _Existe
        End Get
        Set(ByVal value As Boolean)
            _Existe = value
        End Set
    End Property

#End Region

#Region "Funciones"
    Public Function GetCambios(ByVal vRazonSocial As String, ByVal vRepresentante As String, ByVal vDireccion As String,
    ByVal vidColonia As Integer, ByVal vTelefono As String, ByVal vFax As String, ByVal vMovilRepresentante As String,
    ByVal vCIDEMPRESA_COM As Integer, ByVal vPrefLla As String, ByVal vNomBaseDatosCOM As String) As String

        Dim CadCam As String = ""
        If _RazonSocial <> vRazonSocial Then
            CadCam += "Razon Social: '" & _RazonSocial & "' Cambia a [" & vRazonSocial & "],"
        End If
        If _Representante <> vRepresentante Then
            CadCam += "Representante: '" & _Representante & "' Cambia a [" & vRepresentante & "],"
        End If
        If _Direccion <> vDireccion Then
            CadCam += "Direccion: '" & _Direccion & "' Cambia a [" & vDireccion & "],"
        End If
        If _idColonia <> vidColonia Then
            CadCam += "Id Colonia: '" & _idColonia & "' Cambia a [" & vidColonia & "],"
        End If
        If _Telefono <> vTelefono Then
            CadCam += "Telefono: '" & _Telefono & "' Cambia a [" & vTelefono & "],"
        End If
        If _Fax <> vFax Then
            CadCam += "Fax: '" & _Fax & "' Cambia a [" & vFax & "],"
        End If
        If _MovilRepresentante <> vMovilRepresentante Then
            CadCam += "Movil Representante: '" & _MovilRepresentante & "' Cambia a [" & vMovilRepresentante & "],"
        End If
        If _CIDEMPRESA_COM <> vCIDEMPRESA_COM Then
            CadCam += "CIDEMPRESA_COM: '" & _CIDEMPRESA_COM & "' Cambia a [" & vCIDEMPRESA_COM & "],"
        End If
        If _PrefLla <> vPrefLla Then
            CadCam += "PrefLla: '" & _PrefLla & "' Cambia a [" & vPrefLla & "],"
        End If
        If _NomBaseDatosCOM <> vNomBaseDatosCOM Then
            CadCam += "NomBaseDatosCOM: '" & _NomBaseDatosCOM & "' Cambia a [" & vNomBaseDatosCOM & "],"
        End If

        If CadCam <> "" Then
            CadCam = Mid(CadCam, 1, CadCam.Length - 1)
        End If
        Return CadCam
    End Function
    Public Sub Guardar(ByVal vRazonSocial As String, ByVal vRepresentante As String, ByVal bEsIdentidad As Boolean,
    ByVal vDireccion As String, ByVal vidColonia As Integer, ByVal vTelefono As String, ByVal vFax As String,
    ByVal vMovilRepresentante As String, ByVal vCIDEMPRESA_COM As Integer, ByVal vPrefLla As String, ByVal vNomBaseDatosCOM As String)

        Dim sSql As String = "", _CadValAnt As String = "", _CadValNue As String = ""
        If _Existe Then
            If _RazonSocial <> vRazonSocial Then
                sSql += "NomSucursal = '" & vRazonSocial & "',"
                _CadValAnt += "NomSucursal =" & vRazonSocial & ","
            End If
            If _Representante <> vRepresentante Then
                sSql += "Representante = '" & vRepresentante & "',"
                _CadValAnt += "Representante =" & vRepresentante & ","
            End If
            If _Direccion <> vDireccion Then
                sSql += "Direccion = '" & vDireccion & "',"
                _CadValAnt += "Direccion =" & vDireccion & ","
            End If
            If _idColonia <> vidColonia Then
                sSql += "idColonia = " & vidColonia & ","
                _CadValAnt += "idColonia =" & vidColonia & ","
            End If
            If _Telefono <> vTelefono Then
                sSql += "Telefono = '" & vTelefono & "',"
                _CadValAnt += "Telefono =" & vTelefono & ","
            End If
            If _Fax <> vFax Then
                sSql += "Fax = '" & vFax & "',"
                _CadValAnt += "Fax =" & vFax & ","
            End If
            If _MovilRepresentante <> vMovilRepresentante Then
                sSql += "MovilRepresentante = '" & vMovilRepresentante & "',"
                _CadValAnt += "MovilRepresentante =" & vMovilRepresentante & ","
            End If
            If _CIDEMPRESA_COM <> vCIDEMPRESA_COM Then
                sSql += "CIDEMPRESA_COM = '" & vCIDEMPRESA_COM & "',"
                _CadValAnt += "CIDEMPRESA_COM =" & vCIDEMPRESA_COM & ","
            End If
            If _PrefLla <> vPrefLla Then
                sSql += "PrefLla = " & vPrefLla & ","
                _CadValAnt += "PrefLla =" & vPrefLla & ","
            End If
            If _NomBaseDatosCOM <> vNomBaseDatosCOM Then
                sSql += "NomBaseDatosCOM = '" & vNomBaseDatosCOM & "',"
                _CadValAnt += "NomBaseDatosCOM =" & vNomBaseDatosCOM & ","
            End If

            If sSql <> "" Then
                sSql = Mid(sSql, 1, sSql.Length - 1) 'Para quitar la Coma ','
                _CadValAnt = "idEmpresa=" & _idEmpresa & "," & _CadValAnt
                _CadValAnt = Mid(_CadValAnt, 1, _CadValAnt.Length - 1)
                _CadValNue = Replace(sSql, "'", "")

                sSql = "UPDATE " & _TablaBd & " SET " & sSql & " WHERE idEmpresa = '" & _idEmpresa & "'"
                Try
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                    BD.Execute(sSql, True)
                    Audita(_TablaBd, _CadValAnt, _CadValNue, EnTipAccion.Modifica)
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
                Catch ex As Exception
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                    Throw New Exception(ex.Message)
                End Try
            End If
        Else
            If bEsIdentidad Then
                sSql = "INSERT INTO " & _TablaBd & "(RazonSocial,Representante,CIDEMPRESA_COM,Direccion,idColonia,Telefono,Fax,MovilRepresentante,PrefLla,NomBaseDatosCOM) " &
                "VALUES('" & vRazonSocial & "','" & vRepresentante & "'," & vCIDEMPRESA_COM & ",'" & vDireccion & "'," & vidColonia & ",'" & vTelefono & "','" & vFax & "','" & vMovilRepresentante & "','" & vPrefLla & "','" & vNomBaseDatosCOM & "')"

                _CadValNue = "RazonSocial=" & vRazonSocial &
                "Representante = " & vRepresentante &
                "Direccion = " & vDireccion &
                "idColonia = " & vidColonia &
                "Telefono = " & vTelefono &
                "Fax = " & vFax &
                "MovilRepresentante = " & vMovilRepresentante &
                "PrefLla = " & vPrefLla &
                "CIDEMPRESA_COM = " & vCIDEMPRESA_COM &
                "NomBaseDatosCOM = " & vNomBaseDatosCOM
            Else


                sSql = "INSERT INTO " & _TablaBd & "(idEmpresa,RazonSocial,Representante,CIDEMPRESA_COM,Direccion,idColonia,Telefono,Fax,MovilRepresentante,PrefLla,NomBaseDatosCOM) " &
                "VALUES(" & _idEmpresa & ",'" & vRazonSocial & "','" & vRepresentante & "'," & vCIDEMPRESA_COM & ",'" & vDireccion & "'," & vidColonia & ",'" & vTelefono & "','" & vFax & "','" & vMovilRepresentante & "','" & vPrefLla & "','" & vNomBaseDatosCOM & "')"

                _CadValNue = "idEmpresa=" & _idEmpresa &
                "RazonSocial=" & vRazonSocial &
                "Representante = " & vRepresentante &
                "Direccion = " & vDireccion &
                "idColonia = " & vidColonia &
                "Telefono = " & vTelefono &
                "Fax = " & vFax &
                "MovilRepresentante = " & vMovilRepresentante &
                "PrefLla = " & vPrefLla &
                "CIDEMPRESA_COM = " & vCIDEMPRESA_COM &
                "NomBaseDatosCOM = " & vNomBaseDatosCOM

            End If

            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, "", _CadValNue, EnTipAccion.Inserta)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
        End If
        _RazonSocial = vRazonSocial
        _Representante = vRepresentante
        _Direccion = vDireccion
        _idColonia = vidColonia
        _Telefono = vTelefono
        _Fax = vFax
        _MovilRepresentante = vMovilRepresentante
        _CIDEMPRESA_COM = vCIDEMPRESA_COM
        _NomBaseDatosCOM = vNomBaseDatosCOM
        _PrefLla = vPrefLla

    End Sub
    Public Sub Eliminar()
        If _Existe Then
            Dim sSql As String = "", _CadValAnt As String = ""
            '_CadValAnt = "idEmpresa=" & _idEmpresa & " , NomSucursal =" & _RazonSocial

            _CadValAnt = "idEmpresa=" & _idEmpresa &
              "RazonSocial=" & _RazonSocial &
              "Representante = " & _Representante &
              "Direccion = " & _Direccion &
              "idColonia = " & _idColonia &
              "Telefono = " & _Telefono &
              "Fax = " & _Fax &
              "MovilRepresentante = " & _MovilRepresentante &
               "PrefLla = " & _PrefLla &
              "CIDEMPRESA_COM = " & _CIDEMPRESA_COM &
              "NomBaseDatosCOM = " & _NomBaseDatosCOM

            sSql = "DELETE FROM " & _TablaBd & " WHERE idEmpresa = '" & _idEmpresa & "'"
            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, _CadValAnt, "", EnTipAccion.Elimina)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
            _Existe = False
        Else
            'se supone que nunca debe entrar aqui. . .
            MessageBox.Show("La Empresa con Id: " & _idEmpresa & " No existe por lo tanto no se puede Eliminar!!!",
                            "No se puede eliminar la Empresa porque no Existe", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub
#End Region

    Public Function TablaEmpresas(ByVal bIncluyeTodos As Boolean) As DataTable
        MyTabla.Rows.Clear()

        If bIncluyeTodos Then
            StrSql = "SELECT 0 AS idEmpresa,'TODAS' AS RazonSocial UNION SELECT idEmpresa,RazonSocial FROM dbo.CatEmpresas ORDER BY IDEMPRESA"
        Else
            StrSql = "SELECT idEmpresa,RazonSocial FROM dbo.CatEmpresas ORDER BY IDEMPRESA"
        End If




        MyTabla = BD.ExecuteReturn(StrSql)

        Return MyTabla

    End Function
End Class
