﻿Public Class MarcaClass
    Private _idMarca As Integer
    Private _NombreMarca As String

    Private _Existe As Boolean
    Private _TablaBd As String = "CatMarcas"

    Dim StrSql As String
    Dim DsClase As New DataTable
    Dim MyTabla As DataTable = New DataTable(_TablaBd)
#Region "Constructor"
    Sub New(ByVal vIdMarca As Integer)
        'Dim DtEst As New DataTable
        _idMarca = vIdMarca
        StrSql = "select idMarca,NombreMarca from " & _TablaBd & " where idMarca = " & vIdMarca

        DsClase = BD.ExecuteReturn(StrSql)
        If DsClase.Rows.Count > 0 Then
            With DsClase.Rows(0)
                _NombreMarca = .Item("NombreMarca") & ""
            End With
            _Existe = True
        Else
            _Existe = False
        End If

    End Sub
    Sub New(ByVal vIdMarca As Integer, ByVal vNombreMarca As String)
        _idMarca = vIdMarca
        _NombreMarca = vNombreMarca
        _Existe = True
    End Sub
#End Region
#Region "Propiedades"
    'Public ReadOnly Property IDMARCA As Integer
    '    Get
    '        Return _idMarca
    '    End Get
    'End Property
    Public Property IdMarca As Integer
        Get
            Return _idMarca
        End Get
        Set(ByVal value As Integer)
            _idMarca = value
        End Set
    End Property
    Public Property NOMBREMARCA As String
        Get
            Return _NombreMarca
        End Get
        Set(ByVal value As String)
            _NombreMarca = value
        End Set
    End Property
    'Public ReadOnly Property Existe As Boolean
    '    Get
    '        Return _Existe
    '    End Get
    'End Property
    Public Property Existe As Boolean
        Get
            Return _Existe
        End Get
        Set(ByVal value As Boolean)
            _Existe = value
        End Set
    End Property

#End Region
#Region "Funciones"
    Public Function GetCambios(ByVal vNombreMarca As String) As String
        Dim CadCam As String = ""
        If _NombreMarca <> vNombreMarca Then
            CadCam += "Nombre Marca: '" & _NombreMarca & "' Cambia a [" & vNombreMarca & "],"
        End If
        If CadCam <> "" Then
            CadCam = Mid(CadCam, 1, CadCam.Length - 1)
        End If
        Return CadCam
    End Function
    Public Sub Guardar(ByVal vNombreMarca As String, ByVal bEsIdentidad As Boolean)
        Dim sSql As String = "", _CadValAnt As String = "", _CadValNue As String = ""
        If _Existe Then
            If _NombreMarca <> vNombreMarca Then
                sSql += "NombreMarca = '" & vNombreMarca & "',"
                _CadValAnt += "NombreMarca =" & vNombreMarca & ","
            End If

            If sSql <> "" Then
                sSql = Mid(sSql, 1, sSql.Length - 1) 'Para quitar la Coma ','
                _CadValAnt = "idMarca=" & _idMarca & "," & _CadValAnt
                _CadValAnt = Mid(_CadValAnt, 1, _CadValAnt.Length - 1)
                _CadValNue = Replace(sSql, "'", "")
                sSql = "UPDATE " & _TablaBd & " SET " & sSql & " WHERE idMarca = '" & _idMarca & "'"
                Try
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                    BD.Execute(sSql, True)
                    Audita(_TablaBd, _CadValAnt, _CadValNue, EnTipAccion.Modifica)
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
                Catch ex As Exception
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                    Throw New Exception(ex.Message)
                End Try
            End If
        Else
            If bEsIdentidad Then
                sSql = "INSERT INTO " & _TablaBd & "(NombreMarca) VALUES('" & _NombreMarca & "')"

            Else

                sSql = "INSERT INTO " & _TablaBd & "(idMarca,NombreMarca) VALUES(" &
                "'" & _idMarca & "','" & _NombreMarca & "')"

            End If
            _CadValNue = "idMarca=" & _idMarca & ",NombreMarca=" & _NombreMarca
            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, "", _CadValNue, EnTipAccion.Inserta)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
        End If
        _NombreMarca = vNombreMarca
    End Sub
    Public Sub Eliminar()
        If _Existe Then
            Dim sSql As String = "", _CadValAnt As String = ""
            _CadValAnt = "idMarca=" & _idMarca & " , NomEstado =" & _NombreMarca
            sSql = "DELETE FROM " & _TablaBd & " WHERE idMarca = '" & _idMarca & "'"
            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, _CadValAnt, "", EnTipAccion.Elimina)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
            _Existe = False
        Else
            'se supone que nunca debe entrar aqui. . .
            MessageBox.Show("La Marca con Id: " & _idMarca & " No existe por lo tanto no se puede Eliminar!!!",
                            "No se puede eliminar la marca porque no Existe", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub
#End Region

#Region "Consultas"
    Public Function TablaMarcas(ByVal bIncluyeTodos As Boolean, Optional ByVal FiltroSinWhere As String = "") As DataTable
        Dim filtro As String = ""
        MyTabla.Rows.Clear()

        If FiltroSinWhere <> "" Then
            filtro = " where " & FiltroSinWhere
        End If

        If bIncluyeTodos Then
            StrSql = "SELECT 0 AS idMarca,'TODOS' AS NombreMarca " &
            "UNION " &
            "SELECT idMarca,NombreMarca FROM dbo." & _TablaBd & " " & filtro &
            " ORDER BY idMarca"
        Else
            StrSql = "SELECT idMarca,NombreMarca FROM dbo." & _TablaBd & " " & filtro &
            " ORDER BY idMarca"
        End If
        MyTabla = BD.ExecuteReturn(StrSql)

        Return MyTabla

    End Function
#End Region


End Class
