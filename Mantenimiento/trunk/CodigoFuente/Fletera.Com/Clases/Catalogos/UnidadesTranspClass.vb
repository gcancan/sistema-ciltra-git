﻿Public Class UnidadesTranspClass
    'idUnidadTrans			vidUnidadTrans 			    _idUnidadTrans
    'DescripcionUni			vDescripcionUni 			_DescripcionUni 
    'idTipoUnidad			vidTipoUnidad 				_idTipoUnidad 
    'idMarca				vidMarca 					_idMarca 
    'idEmpresa				vidEmpresa 					_idEmpresa 
    'idSucursal				vidSucursal 				_idSucursal 
    'idAlmacen				vidAlmacen 					_idAlmacen 
    'Estatus				vEstatus 					_Estatus
    Private _idUnidadTrans As String
    Private _idTipoUnidad As Integer
    Private _DescripcionUni As String
    Private _idMarca As Integer
    Private _idEmpresa As Integer
    Private _idSucursal As Double
    Private _Estatus As String
    Private _idAlmacen As Double
    Private _UltOT As Integer
    Private _estatusUbicacion As Integer
    Private _estatusUbicacionAnt As Integer


    Private _Existe As Boolean
    Private _TablaBd As String = "CatUnidadTrans"

    Dim StrSql As String
    Dim DsClase As New DataTable

    Dim MyTabla As DataTable = New DataTable(_TablaBd)
#Region "Constructor"
    Sub New(ByVal vidUnidadTrans As Integer, ByVal Pref As String)
        Dim ValoraBuscar As String = ""

        If Len(CStr(vidUnidadTrans)) >= 3 Then
            ValoraBuscar = Pref & vidUnidadTrans
        ElseIf Len(CStr(vidUnidadTrans)) = 2 Then
            ValoraBuscar = Pref & "0" & vidUnidadTrans
        ElseIf Len(CStr(vidUnidadTrans)) = 1 Then
            ValoraBuscar = Pref & "00" & vidUnidadTrans
        End If


        _idUnidadTrans = vidUnidadTrans

        StrSql = "select uni.idUnidadTrans, " &
        "uni.descripcionUni, " &
        "uni.idTipoUnidad, " &
        "uni.idMarca, " &
        "uni.idEmpresa, " &
        "uni.idSucursal, " &
        "uni.idAlmacen, " &
        "uni.Estatus, " &
         "uni.estatusUbicacion, " &
         "uni.estatusUbicacionAnt, " &
        "isnull(uni.UltOT,0) as UltOT  " &
        "from " & _TablaBd & " uni " &
        "where uni.idUnidadTrans = '" & ValoraBuscar & "'"

        DsClase = BD.ExecuteReturn(StrSql)
        If DsClase.Rows.Count > 0 Then
            With DsClase.Rows(0)
                _idUnidadTrans = .Item("idUnidadTrans")
                _idTipoUnidad = .Item("idTipoUnidad")
                _DescripcionUni = .Item("DescripcionUni") & ""
                _idMarca = .Item("idMarca")
                _idEmpresa = .Item("idEmpresa")
                _idSucursal = .Item("idSucursal")
                _Estatus = .Item("Estatus")
                _idAlmacen = .Item("idAlmacen")
                _UltOT = .Item("UltOT")
                _estatusUbicacion = .Item("estatusUbicacion")
                _estatusUbicacionAnt = .Item("estatusUbicacionAnt")
            End With
            _Existe = True
        Else
            _Existe = False
        End If
    End Sub

    Sub New(ByVal vidUnidadTrans As String)
        _idUnidadTrans = vidUnidadTrans

        StrSql = "select uni.idUnidadTrans, " &
        "uni.descripcionUni, " &
        "uni.idTipoUnidad, " &
        "uni.idMarca, " &
        "uni.idEmpresa, " &
        "uni.idSucursal, " &
        "uni.idAlmacen, " &
        "uni.Estatus, " &
        "uni.estatusUbicacion, " &
        "uni.estatusUbicacionAnt, " &
        "isnull(uni.UltOT,0) as UltOT  " &
        "from " & _TablaBd & " uni " &
        "where uni.idUnidadTrans = '" & vidUnidadTrans & "'"

        DsClase = BD.ExecuteReturn(StrSql)
        If DsClase.Rows.Count > 0 Then
            With DsClase.Rows(0)
                _idTipoUnidad = .Item("idTipoUnidad")
                _DescripcionUni = .Item("DescripcionUni") & ""
                _idMarca = .Item("idMarca")
                _idEmpresa = .Item("idEmpresa")
                _idSucursal = .Item("idSucursal")
                _Estatus = .Item("Estatus")
                _idAlmacen = .Item("idAlmacen")
                _UltOT = .Item("UltOT")
                _estatusUbicacion = .Item("estatusUbicacion")
                _estatusUbicacionAnt = .Item("estatusUbicacionAnt")
            End With
            _Existe = True
        Else
            _Existe = False
        End If

    End Sub
    Sub New(ByVal vidUnidadTrans As String, ByVal vDescripcionUni As String, ByVal vidTipoUnidad As Integer, ByVal vidMarca As Integer,
            ByVal vidEmpresa As Integer, ByVal vidSucursal As Integer, ByVal vEstatus As String,
            ByVal vidAlmacen As Integer, ByVal vUltOT As Integer, ByVal vestatusUbicacion As Integer,
            ByVal vestatusUbicacionAnt As Integer)
        _idUnidadTrans = vidUnidadTrans
        _idTipoUnidad = vidTipoUnidad
        _DescripcionUni = vDescripcionUni
        _idMarca = vidMarca
        _idEmpresa = vidEmpresa
        _idSucursal = vidSucursal
        _Estatus = vEstatus
        _idAlmacen = vidAlmacen
        _UltOT = vUltOT
        _estatusUbicacion = vestatusUbicacion
        _estatusUbicacionAnt = vestatusUbicacionAnt
        _Existe = True
    End Sub
#End Region
#Region "Propiedades"
    'Public ReadOnly Property idDivision As Integer
    '    Get
    '        Return _idTipoServicio
    '    End Get
    'End Property
    Public Property iUnidadTrans As String
        Get
            Return _idUnidadTrans
        End Get
        Set(ByVal value As String)
            _idUnidadTrans = value
        End Set
    End Property
    Public Property idTipoUnidad As Integer
        Get
            Return _idTipoUnidad
        End Get
        Set(ByVal value As Integer)
            _idTipoUnidad = value
        End Set
    End Property
    Public Property DescripcionUni As String
        Get
            Return _DescripcionUni
        End Get
        Set(ByVal value As String)
            _DescripcionUni = value
        End Set
    End Property
    Public Property idMarca As Integer
        Get
            Return _idMarca
        End Get
        Set(ByVal value As Integer)
            _idMarca = value
        End Set
    End Property
    Public Property idEmpresa As Integer
        Get
            Return _idEmpresa
        End Get
        Set(ByVal value As Integer)
            _idEmpresa = value
        End Set
    End Property
    Public Property idSucursal As Integer
        Get
            Return _idSucursal
        End Get
        Set(ByVal value As Integer)
            _idSucursal = value
        End Set
    End Property
    Public Property Estatus As String
        Get
            Return _Estatus
        End Get
        Set(ByVal value As String)
            _Estatus = value
        End Set
    End Property
    Public Property idAlmacen As Integer
        Get
            Return _idAlmacen
        End Get
        Set(ByVal value As Integer)
            _idAlmacen = value
        End Set
    End Property
    Public Property UltOT As Integer
        Get
            Return _UltOT
        End Get
        Set(ByVal value As Integer)
            _UltOT = value
        End Set
    End Property
    Public Property estatusUbicacion As String
        Get
            Return _estatusUbicacion
        End Get
        Set(ByVal value As String)
            _estatusUbicacion = value
        End Set
    End Property

    Public Property estatusUbicacionAnt As String
        Get
            Return _estatusUbicacionAnt
        End Get
        Set(ByVal value As String)
            _estatusUbicacionAnt = value
        End Set
    End Property
    Public Property Existe As Boolean
        Get
            Return _Existe
        End Get
        Set(ByVal value As Boolean)
            _Existe = value
        End Set
    End Property

#End Region
#Region "Funciones"
    Public Function GetCambios(ByVal vDescripcionUni As String, ByVal vidTipoUnidad As Integer, ByVal vidMarca As Integer,
    ByVal vidEmpresa As Integer, ByVal vidSucursal As Integer, ByVal vEstatus As String, ByVal vidAlmacen As Integer) As String
        Dim CadCam As String = ""
        If _idTipoUnidad <> vidTipoUnidad Then
            CadCam += "idTipoServicio: '" & _idTipoUnidad & "' Cambia a [" & vidTipoUnidad & "],"
        End If
        If _DescripcionUni <> vDescripcionUni Then
            CadCam += "NomServicio: '" & _DescripcionUni & "' Cambia a [" & vDescripcionUni & "],"
        End If
        If _idMarca <> vidMarca Then
            CadCam += "Descripcion: '" & _idMarca & "' Cambia a [" & vidMarca & "],"
        End If
        If _idEmpresa <> vidEmpresa Then
            CadCam += "CadaKms: '" & _idEmpresa & "' Cambia a [" & vidEmpresa & "],"
        End If
        If _idSucursal <> vidSucursal Then
            CadCam += "CadaTiempoDias: '" & _idSucursal & "' Cambia a [" & vidSucursal & "],"
        End If
        If _Estatus <> vEstatus Then
            CadCam += "Estatus: '" & _Estatus & "' Cambia a [" & vEstatus & "],"
        End If
        If _idAlmacen <> vidAlmacen Then
            CadCam += "Costo: '" & _idAlmacen & "' Cambia a [" & vidAlmacen & "],"
        End If
        'If _UltOS <> vUltOs Then
        '    CadCam += "Costo: '" & _UltOS & "' Cambia a [" & vUltOs & "],"
        'End If

        If CadCam <> "" Then
            CadCam = Mid(CadCam, 1, CadCam.Length - 1)
        End If
        Return CadCam
    End Function
    Public Sub Guardar(ByVal vDescripcionUni As String, ByVal vidTipoUnidad As Integer, ByVal vidMarca As Integer,
    ByVal vidEmpresa As Integer, ByVal vidSucursal As Integer, ByVal vEstatus As String, ByVal vidAlmacen As Integer)
        Dim sSql As String = "", _CadValAnt As String = "", _CadValNue As String = ""
        If _Existe Then
            If _idTipoUnidad <> vidTipoUnidad Then
                sSql += "idTipoUnidad = " & vidTipoUnidad & ","
                _CadValAnt += "idTipoUnidad =" & vidTipoUnidad & ","
            End If
            If _DescripcionUni <> vDescripcionUni Then
                sSql += "DescripcionUni = '" & vDescripcionUni & "',"
                _CadValAnt += "DescripcionUni =" & vDescripcionUni & ","
            End If
            If _idMarca <> vidMarca Then
                sSql += "idMarca = '" & vidMarca & "',"
                _CadValAnt += "idMarca =" & vidMarca & ","
            End If
            If _idEmpresa <> vidEmpresa Then
                sSql += "idEmpresa = " & vidEmpresa & ","
                _CadValAnt += "idEmpresa =" & vidEmpresa & ","
            End If

            If _idSucursal <> vidSucursal Then
                sSql += "idSucursal = " & vidSucursal & ","
                _CadValAnt += "idSucursal =" & vidSucursal & ","
            End If
            If _Estatus <> vEstatus Then
                sSql += "Estatus = '" & vEstatus & "',"
                _CadValAnt += "Estatus =" & vEstatus & ","
            End If
            If _idAlmacen <> vidAlmacen Then
                sSql += "idAlmacen = " & vidAlmacen & ","
                _CadValAnt += "idAlmacen =" & vidAlmacen & ","
            End If
            'If _UltOS <> vUltOs Then
            '    sSql += "UltOS = " & vUltOs & ","
            '    _CadValAnt += "UltOS =" & vUltOs & ","
            'End If

            If sSql <> "" Then
                sSql = Mid(sSql, 1, sSql.Length - 1) 'Para quitar la Coma ','
                _CadValAnt = "idUnidadTrans=" & _idUnidadTrans & "," & _CadValAnt
                _CadValAnt = Mid(_CadValAnt, 1, _CadValAnt.Length - 1)
                _CadValNue = Replace(sSql, "'", "")

                sSql = "UPDATE " & _TablaBd & " SET " & sSql & " WHERE idUnidadTrans = '" & _idUnidadTrans & "'"
                Try
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                    BD.Execute(sSql, True)
                    Audita(_TablaBd, _CadValAnt, _CadValNue, EnTipAccion.Modifica)
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
                Catch ex As Exception
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                    Throw New Exception(ex.Message)
                End Try
            End If
        Else
            sSql = "INSERT INTO " & _TablaBd & "(idUnidadTrans,idTipoUnidad,DescripcionUni,idMarca,idEmpresa,idSucursal,Estatus,idAlmacen) " & _
            "VALUES('" & _idUnidadTrans & "'," & vidTipoUnidad & ",'" & vDescripcionUni & "'," & vidMarca & "," & vidEmpresa & "," & vidSucursal & _
            ",'" & vEstatus & "'," & vidAlmacen & ")"

            _CadValNue = "idUnidadTrans=" & _idUnidadTrans & "idTipoUnidad=" & vidTipoUnidad & "DescripcionUni = " & vDescripcionUni & "idMarca = " & vidMarca & _
            "idEmpresa = " & vidEmpresa & "idSucursal = " & vidSucursal & "Estatus = " & vEstatus & "idAlmacen = " & vidAlmacen

            'If bEsIdentidad Then
            '    sSql = "INSERT INTO " & _TablaBd & "(idTipoServicio,NomServicio,Descripcion,CadaKms,CadaTiempoDias,Estatus,Costo) " & _
            '    "VALUES(" & vidTipoServicio & ",'" & vNomServicio & "','" & vDescripcion & "'," & vCadaKms & "," & vCadaTiempoDias & _
            '    ",'" & vEstatus & "'," & vCosto & ")"

            '    _CadValNue = "idTipoServicio=" & vidTipoServicio & "NomServicio = " & vNomServicio & "Descripcion = " & vDescripcion & _
            '    "CadaKms = " & vCadaKms & "CadaTiempoDias = " & vCadaTiempoDias & "Estatus = " & vEstatus & "Costo = " & vCosto

            'Else
            '    sSql = "INSERT INTO " & _TablaBd & "(idServicio,idTipoServicio,NomServicio,Descripcion,CadaKms,CadaTiempoDias,Estatus,Costo) " & _
            '    "VALUES(" & _idUnidadTrans & "," & vidTipoServicio & ",'" & vNomServicio & "','" & vDescripcion & "'," & vCadaKms & "," & vCadaTiempoDias & _
            '    ",'" & vEstatus & "'," & vCosto & ")"

            '    _CadValNue = "idServicio=" & _idUnidadTrans & "idTipoServicio=" & vidTipoServicio & "NomServicio = " & vNomServicio & "Descripcion = " & vDescripcion & _
            '    "CadaKms = " & vCadaKms & "CadaTiempoDias = " & vCadaTiempoDias & "Estatus = " & vEstatus & "Costo = " & vCosto
            'End If

            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, "", _CadValNue, EnTipAccion.Inserta)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
        End If
        _idTipoUnidad = vidTipoUnidad
        _DescripcionUni = vDescripcionUni
        _idMarca = vidMarca
        _idEmpresa = vidEmpresa
        _idSucursal = vidSucursal
        _Estatus = vEstatus
        _idAlmacen = vidAlmacen
    End Sub
    Public Sub Eliminar()
        If _Existe Then
            Dim sSql As String = "", _CadValAnt As String = ""
            _CadValAnt = "idUnidadTrans=" & _idUnidadTrans & "idTipoUnidad=" & _idTipoUnidad & "DescripcionUni = " & _DescripcionUni & _
                "idMarca = " & _idMarca & "idEmpresa = " & _idEmpresa & "idSucursal = " & _idSucursal & _
                "Estatus = " & _Estatus & "idAlmacen = " & _idAlmacen

            sSql = "DELETE FROM " & _TablaBd & " WHERE idUnidadTrans = " & _idUnidadTrans
            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, _CadValAnt, "", EnTipAccion.Elimina)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
            _Existe = False
        Else
            'se supone que nunca debe entrar aqui. . .
            MessageBox.Show("La Unidad de Transporte con Id: " & _idUnidadTrans & " No existe por lo tanto no se puede Eliminar!!!",
                            "No se puede eliminar el Servicio porque no Existe", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub
#End Region
#Region "Consultas"
    Public Function TablaUnidadxTipo(ByVal Activo As Boolean, ByVal idEmpresa As Integer, ByVal Tipo As String, Optional ByVal FiltroSinWhere As String = "") As DataTable
        MyTabla.Rows.Clear()

        StrSql = "SELECT uni.*, tip.clasificacion FROM dbo.CatUnidadTrans uni " &
        "INNER JOIN dbo.CatTipoUniTrans tip ON uni.idTipoUnidad = tip.idTipoUniTras " &
        "WHERE tip.TipoUso = '" & Tipo & "' " &
        "AND uni.Estatus = '" & IIf(Activo, "ACT", "BAJ") & "'" &
        IIf(idEmpresa > 0, " and idEmpresa = " & idEmpresa, "") &
        FiltroSinWhere

        MyTabla = BD.ExecuteReturn(StrSql)

        Return MyTabla


    End Function
    Public Function TablaUnidadesTodas(ByVal idEmpresa As Integer, Optional ByVal FiltroSinWhere As String = "") As DataTable
        MyTabla.Rows.Clear()

        StrSql = "SELECT uni.*, tip.clasificacion FROM dbo.CatUnidadTrans uni " &
        "INNER JOIN dbo.CatTipoUniTrans tip ON uni.idTipoUnidad = tip.idTipoUniTras " &
        IIf(idEmpresa > 0, " WHERE idEmpresa = " & idEmpresa, "") &
        FiltroSinWhere

        MyTabla = BD.ExecuteReturn(StrSql)

        Return MyTabla


    End Function

    Public Function TablaUnidadxTipo2(ByVal idEmpresa As Integer, Optional ByVal FiltroSinWhere As String = "") As DataTable
        MyTabla.Rows.Clear()

        StrSql = "SELECT uni.*, tip.clasificacion FROM dbo.CatUnidadTrans uni " &
        "INNER JOIN dbo.CatTipoUniTrans tip ON uni.idTipoUnidad = tip.idTipoUniTras " &
        IIf(idEmpresa > 0, " where idEmpresa = " & idEmpresa, "") &
        FiltroSinWhere

        MyTabla = BD.ExecuteReturn(StrSql)

        Return MyTabla


    End Function

    Public Function UltKilomValidado(ByVal IDUNIDADTRANS As String) As Integer
        Dim UltKm As Integer = 0

        StrSql = "SELECT TOP 1 RK.KMFINAL FROM DBO.REGISTROSKILOMETROS RK " &
    "WHERE RK.IDUNIDADTRANS = '" & IDUNIDADTRANS & "' " &
    "AND RK.VALIDADO = 1 AND RK.ACTIVO = 1 " &
    "ORDER BY FECHA DESC"

        DsClase = BD.ExecuteReturn(StrSql)
        If DsClase.Rows.Count > 0 Then
            With DsClase.Rows(0)
                UltKm = .Item("KMFINAL")
            End With
        End If

        Return UltKm
    End Function


    Public Function ULtimoElementoViaje(ByVal IDUNIDADTRANS As String,
                                        ByVal Elemento As OpcTipoElementoViaje) As String

        Dim Resp As String

        'StrSql = "SELECT TOP 1 idOperador FROM dbo.CabGuia " &
        '"WHERE idTractor = '" & IDUNIDADTRANS & "' ORDER BY FechaHoraViaje DESC "

        StrSql = "SELECT TOP 1 " &
        "c.FechaHoraViaje, " &
        "c.NumGuiaId, " &
        "c.idTractor, " &
        "c.idOperador, p.NombreCompleto, " &
        "ISNULL(c.idRemolque1,'') AS idRemolque1, " &
        "ISNULL(c.idDolly,'') AS idDolly, " &
        "ISNULL(c.idRemolque2,'') AS idRemolque2 " &
        "FROM dbo.CabGuia C " &
        "INNER JOIN dbo.CatPersonal P ON c.idOperador = p.idPersonal " &
        "WHERE idTractor = '" & IDUNIDADTRANS & "' ORDER BY FechaHoraViaje DESC"

        DsClase = BD.ExecuteReturn(StrSql)
        If DsClase.Rows.Count > 0 Then
            With DsClase.Rows(0)
                Select Case Elemento
                    Case OpcTipoElementoViaje.Dolly
                        Resp = .Item("idDolly")
                    Case OpcTipoElementoViaje.Operador
                        Resp = .Item("idOperador")
                    Case OpcTipoElementoViaje.Remolque1
                        Resp = .Item("idRemolque1")
                    Case OpcTipoElementoViaje.Remolque2
                        Resp = .Item("idRemolque2")
                    Case OpcTipoElementoViaje.Tractor
                        Resp = .Item("idTractor")
                    Case Else

                End Select

            End With
        End If

        Return Resp
    End Function

#End Region
End Class
