﻿Public Class TipoUniTransClass
    'idTipoUniTras			vidTipoUniTras 				_idTipoUniTras
    'nomTipoUniTras			vnomTipoUniTras 			_nomTipoUniTras 
    'NoLlantas				vNoLlantas 					_NoLlantas 
    'NoEjes					vNoEjes 					_NoEjes
    'TonelajeMax			vTonelajeMax 				_TonelajeMax
    Private _idTipoUniTras As Integer
    Private _NomTipoUniTras As String
    Private _NoLlantas As Double
    Private _NoEjes As Integer
    Private _TonelajeMax As String
    Private _bCombustible As Boolean
    Private _Clasificacion As String
    Private _idClasLlan As Integer

    Private _Existe As Boolean
    Private _TablaBd As String = "CatTipoUniTrans"

    Dim StrSql As String
    Dim DsClase As New DataTable
#Region "Constructor"
    Sub New(ByVal vidActividad As Integer)
        _idTipoUniTras = vidActividad

        StrSql = "select ctut.idTipoUniTras, isnull(ctut.nomTipoUniTras,'') as nomTipoUniTras, isnull(CTUT.NoLlantas,0) as NoLlantas, " &
        "isnull(ctut.NoEjes,0) as NoEjes, isnull(ctut.TonelajeMax,0) as TonelajeMax, isnull(ctut.bCombustible,0) as bCombustible, " &
        "isnull(ctut.clasificacion,'') as clasificacion,isnull(idClasLlan,0) as idClasLlan " &
        "from " & _TablaBd & " CTUT " &
        "where CTUT.idTipoUniTras = " & vidActividad

        DsClase = BD.ExecuteReturn(StrSql)
        If DsClase.Rows.Count > 0 Then
            With DsClase.Rows(0)
                _NomTipoUniTras = .Item("nomTipoUniTras") & ""
                _NoEjes = .Item("NoEjes")
                _TonelajeMax = .Item("TonelajeMax")
                _NoLlantas = .Item("NoLlantas")
                _bCombustible = .Item("bCombustible")
                _Clasificacion = .Item("clasificacion")
                _idClasLlan = .Item("idClasLlan")
                '
            End With
            _Existe = True
        Else
            _Existe = False
        End If

    End Sub
    Sub New(ByVal vidTipoUniTras As Integer, ByVal vnomTipoUniTras As String, ByVal vNoLlantas As Integer, ByVal vNoEjes As Integer,
            ByVal vTonelajeMax As Double, ByVal vbCombustible As Boolean, ByVal vClasificacion As String, ByVal vidClasLlan As Integer)
        _idTipoUniTras = vidTipoUniTras
        _NomTipoUniTras = vnomTipoUniTras
        _NoEjes = vNoEjes
        _TonelajeMax = vTonelajeMax
        _NoLlantas = vNoLlantas
        _bCombustible = vbCombustible
        _Clasificacion = vClasificacion
        _idClasLlan = vidClasLlan
        _Existe = True
    End Sub
#End Region
#Region "Propiedades"
    'Public ReadOnly Property idDivision As Integer
    '    Get
    '        Return _NoEjes
    '    End Get
    'End Property
    Public Property idTipoUniTras As Integer
        Get
            Return _idTipoUniTras
        End Get
        Set(ByVal value As Integer)
            _idTipoUniTras = value
        End Set
    End Property
    Public Property nomTipoUniTras As String
        Get
            Return _NomTipoUniTras
        End Get
        Set(ByVal value As String)
            _NomTipoUniTras = value
        End Set
    End Property
    Public Property NoEjes As Integer
        Get
            Return _NoEjes
        End Get
        Set(ByVal value As Integer)
            _NoEjes = value
        End Set
    End Property
    Public Property TonelajeMax As Double
        Get
            Return _TonelajeMax
        End Get
        Set(ByVal value As Double)
            _TonelajeMax = value
        End Set
    End Property
    Public Property NoLlantas As Integer
        Get
            Return _NoLlantas
        End Get
        Set(ByVal value As Integer)
            _NoLlantas = value
        End Set
    End Property
    'Public ReadOnly Property Existe As Boolean
    '    Get
    '        Return _Existe
    '    End Get
    'End Property
    Public Property bCombustible As Boolean
        Get
            Return _bCombustible
        End Get
        Set(ByVal value As Boolean)
            _bCombustible = value
        End Set
    End Property
    Public Property Clasificacion As String
        Get
            Return _Clasificacion
        End Get
        Set(ByVal value As String)
            _Clasificacion = value
        End Set
    End Property
    Public Property idClasLlan As Integer
        Get
            Return _idClasLlan
        End Get
        Set(ByVal value As Integer)
            _idClasLlan = value
        End Set
    End Property

    Public Property Existe As Boolean
        Get
            Return _Existe
        End Get
        Set(ByVal value As Boolean)
            _Existe = value
        End Set
    End Property

#End Region
#Region "Funciones"
    Public Function GetCambios(ByVal vnomTipoUniTras As String, ByVal vNoEjes As Integer, ByVal vNoLlantas As Integer,
    ByVal vTonelajeMax As Double, ByVal vbCombustible As Boolean, ByVal vidClasLlan As Integer) As String
        Dim CadCam As String = ""
        If _NomTipoUniTras <> vnomTipoUniTras Then
            CadCam += "nomTipoUniTras: '" & _NomTipoUniTras & "' Cambia a [" & vnomTipoUniTras & "],"
        End If
        If _NoEjes <> vNoEjes Then
            CadCam += "NoEjes: '" & _NoEjes & "' Cambia a [" & vNoEjes & "],"
        End If
        If _NoLlantas <> vNoLlantas Then
            CadCam += "NoLlantas: '" & _NoLlantas & "' Cambia a [" & vNoLlantas & "],"
        End If
        If _TonelajeMax <> vTonelajeMax Then
            CadCam += "TonelajeMax: '" & _TonelajeMax & "' Cambia a [" & vTonelajeMax & "],"
        End If
        If _bCombustible <> vbCombustible Then
            CadCam += "Combustible: '" & _bCombustible & "' Cambia a [" & vbCombustible & "],"
        End If
        If _idClasLlan <> vidClasLlan Then
            CadCam += "idClasLlan: '" & _idClasLlan & "' Cambia a [" & vidClasLlan & "],"
        End If


        If CadCam <> "" Then
            CadCam = Mid(CadCam, 1, CadCam.Length - 1)
        End If
        Return CadCam
    End Function
    Public Sub Guardar(ByVal vnomTipoUniTras As String, ByVal vNoEjes As Integer, ByVal bEsIdentidad As Boolean,
                       ByVal vNoLlantas As Integer, ByVal vTonelajeMax As Double, ByVal vbCombustible As Boolean, ByVal vidClasLlan As Integer)
        Dim sSql As String = "", _CadValAnt As String = "", _CadValNue As String = ""
        If _Existe Then
            If _NomTipoUniTras <> vnomTipoUniTras Then
                sSql += "nomTipoUniTras = '" & vnomTipoUniTras & "',"
                _CadValAnt += "nomTipoUniTras =" & vnomTipoUniTras & ","
            End If
            If _NoEjes <> vNoEjes Then
                sSql += "NoEjes = " & vNoEjes & ","
                _CadValAnt += "NoEjes =" & vNoEjes & ","
            End If
            If _NoLlantas <> vNoLlantas Then
                sSql += "NoLlantas = " & vNoLlantas & ","
                _CadValAnt += "NoLlantas =" & vNoLlantas & ","
            End If
            If _TonelajeMax <> vTonelajeMax Then
                sSql += "TonelajeMax = " & vTonelajeMax & ","
                _CadValAnt += "TonelajeMax =" & vTonelajeMax & ","
            End If
            If _bCombustible <> vbCombustible Then
                sSql += "bCombustible = " & IIf(vbCombustible, 1, 0) & ","
                _CadValAnt += "bCombustible =" & IIf(vbCombustible, 1, 0) & ","
            End If
            If _idClasLlan <> vidClasLlan Then
                sSql += "idClasLlan = " & IIf(vidClasLlan, 1, 0) & ","
                _CadValAnt += "idClasLlan =" & IIf(vidClasLlan, 1, 0) & ","
            End If

            If sSql <> "" Then
                sSql = Mid(sSql, 1, sSql.Length - 1) 'Para quitar la Coma ','
                _CadValAnt = "idTipoUniTras=" & _idTipoUniTras & "," & _CadValAnt
                _CadValAnt = Mid(_CadValAnt, 1, _CadValAnt.Length - 1)
                _CadValNue = Replace(sSql, "'", "")

                sSql = "UPDATE " & _TablaBd & " SET " & sSql & " WHERE idTipoUniTras = '" & _idTipoUniTras & "'"
                Try
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                    BD.Execute(sSql, True)
                    Audita(_TablaBd, _CadValAnt, _CadValNue, EnTipAccion.Modifica)
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
                Catch ex As Exception
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                    Throw New Exception(ex.Message)
                End Try
            End If
        Else
            If bEsIdentidad Then
                sSql = "INSERT INTO " & _TablaBd & "(nomTipoUniTras,NoLlantas,NoEjes,TonelajeMax,bCombustible) " &
                "VALUES('" & vnomTipoUniTras & "'," & vNoLlantas & "," & vNoEjes & "," & vTonelajeMax &
                "," & IIf(vbCombustible, 1, 0) & ")"

                _CadValNue = "nomTipoUniTras=" & vnomTipoUniTras & "NoLlantas = " & vNoLlantas & "NoEjes = " & vNoEjes & "TonelajeMax = " & vTonelajeMax & "bCombustible = " & vbCombustible
            Else
                sSql = "INSERT INTO " & _TablaBd & "(idTipoUniTras,nomTipoUniTras,NoLlantas,NoEjes,TonelajeMax,bCombustible) " &
                "VALUES(" & _idTipoUniTras & ",'" & vnomTipoUniTras & "'," & vNoLlantas & "," & vNoEjes & "," & vTonelajeMax & "," & IIf(vbCombustible, 1, 0) & ")"

                _CadValNue = "idTipoUniTras=" & _idTipoUniTras & "nomTipoUniTras=" & vnomTipoUniTras & "NoLlantas = " & vNoLlantas & "NoEjes = " & vNoEjes & "TonelajeMax = " & vTonelajeMax & "bCombustible = " & vbCombustible
            End If

            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, "", _CadValNue, EnTipAccion.Inserta)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
        End If
        _NomTipoUniTras = vnomTipoUniTras
        _NoEjes = vNoEjes
        _TonelajeMax = vTonelajeMax
        _NoLlantas = vNoLlantas
        _idClasLlan = vidClasLlan
    End Sub
    Public Sub Eliminar()
        If _Existe Then
            Dim sSql As String = "", _CadValAnt As String = ""
            _CadValAnt = "idTipoUniTras=" & _idTipoUniTras & " , nomTipoUniTras =" & _NomTipoUniTras
            sSql = "DELETE FROM " & _TablaBd & " WHERE idTipoUniTras = " & _idTipoUniTras
            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, _CadValAnt, "", EnTipAccion.Elimina)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
            _Existe = False
        Else
            'se supone que nunca debe entrar aqui. . .
            MessageBox.Show("El Tipo de Unidad con Id: " & _idTipoUniTras & " No existe por lo tanto no se puede Eliminar!!!",
                            "No se puede eliminar el tipo de Unidad porque no Existe", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub
#End Region

End Class
