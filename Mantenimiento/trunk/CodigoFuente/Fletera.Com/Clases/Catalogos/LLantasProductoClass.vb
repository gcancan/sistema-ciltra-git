﻿Public Class LLantasProductoClass
    Private _idProductoLlanta As Integer
    Private _idMarca As Integer
    Private _idDisenio As Integer
    Private _Medida As String
    Private _Profundidad As Double
    Private _Activo As Boolean

    Private _Existe As Boolean
    Private _TablaBd As String = "CatLLantasProducto"

    Dim StrSql As String
    Dim DsClase As New DataTable
    Dim MyTabla As DataTable = New DataTable(_TablaBd)

#Region "Constructor"
    Sub New(ByVal vidLlanta As Integer)
        _idProductoLlanta = vidLlanta

        StrSql = "SELECT lla.idProductoLlanta, " &
        "lla.idMarca, " &
        "isnull(lla.idDisenio,0) as idDisenio, " &
        "lla.Medida, " &
        "lla.Profundidad, " &
        "lla.Activo " &
        "FROM dbo." & _TablaBd & " lla " &
        "where lla.idProductoLlanta = " & vidLlanta

        DsClase = BD.ExecuteReturn(StrSql)
        If DsClase.Rows.Count > 0 Then
            With DsClase.Rows(0)
                '_idProductoLlanta = .Item("idLlanta")
                _idMarca = .Item("idMarca")
                _idDisenio = .Item("idDisenio")
                _Medida = .Item("Medida")
                _Profundidad = .Item("Profundidad")
                _Activo = .Item("Activo")

            End With
            _Existe = True
        Else
            _Existe = False
        End If

    End Sub



    Sub New(ByVal vidLlanta As String, ByVal vidMarca As Integer, ByVal vidDisenio As Integer,
    ByVal vMedida As String, ByVal vProfundidad As Double, ByVal vActivo As Boolean)
        _idProductoLlanta = vidLlanta
        _idMarca = vidMarca
        _idDisenio = vidDisenio
        _Medida = vMedida
        _Profundidad = vProfundidad
        _Activo = vActivo
        _Existe = True

    End Sub
#End Region

#Region "Propiedades"
    Public Property idProductoLlanta As Integer
        Get
            Return _idProductoLlanta
        End Get
        Set(ByVal value As Integer)
            _idProductoLlanta = value
        End Set
    End Property

    Public Property idMarca As Integer
        Get
            Return _idMarca
        End Get
        Set(ByVal value As Integer)
            _idMarca = value
        End Set
    End Property
    Public Property idDisenio As String
        Get
            Return _idDisenio
        End Get
        Set(ByVal value As String)
            _idDisenio = value
        End Set
    End Property

    Public Property Medida As String
        Get
            Return _Medida
        End Get
        Set(ByVal value As String)
            _Medida = value
        End Set
    End Property

    Public Property Profundidad As Double
        Get
            Return _Profundidad
        End Get
        Set(ByVal value As Double)
            _Profundidad = value
        End Set
    End Property
    Public Property Activo As Boolean
        Get
            Return _Activo
        End Get
        Set(ByVal value As Boolean)
            _Activo = value
        End Set
    End Property

    Public Property Existe As Boolean
        Get
            Return _Existe
        End Get
        Set(ByVal value As Boolean)
            _Existe = value
        End Set
    End Property


#End Region

#Region "Funciones"
    Public Function GetCambios(ByVal vidMarca As Integer, ByVal vidDisenio As Integer,
    ByVal vMedida As String, ByVal vProfundidad As Double, ByVal vActivo As Boolean) As String
        Dim CadCam As String = ""
        If _Medida <> vMedida Then
            CadCam += "Medida: '" & _Medida & "' Cambia a [" & vMedida & "],"
        End If
        If _idMarca <> vidMarca Then
            CadCam += "idMarca: '" & _idMarca & "' Cambia a [" & vidMarca & "],"
        End If
        If _idDisenio <> vidDisenio Then
            CadCam += "idDisenio: '" & _idDisenio & "' Cambia a [" & vidDisenio & "],"
        End If
        If _Profundidad <> vProfundidad Then
            CadCam += "Profundidad: '" & _Profundidad & "' Cambia a [" & vProfundidad & "],"
        End If
        If _Activo <> vActivo Then
            CadCam += "Activo: '" & _Activo & "' Cambia a [" & vActivo & "],"
        End If
        If CadCam <> "" Then
            CadCam = Mid(CadCam, 1, CadCam.Length - 1)
        End If
        Return CadCam

    End Function

    Public Sub Guardar(ByVal bEsIdentidad As Boolean, ByVal vidMarca As Integer, ByVal vidDisenio As Integer,
    ByVal vMedida As String, ByVal vProfundidad As Double, ByVal vActivo As Boolean)
        Dim sSql As String = "", _CadValAnt As String = "", _CadValNue As String = ""

        If _Existe Then
            If _Medida <> vMedida Then
                sSql += "Medida = '" & vMedida & "',"
                _CadValAnt += "Medida =" & vMedida & ","
            End If
            If _idDisenio <> vidDisenio Then
                sSql += "idDisenio = " & vidDisenio & ","
                _CadValAnt += "idDisenio =" & vidDisenio & ","
            End If
            If _Profundidad <> vProfundidad Then
                sSql += "Profundidad = " & vProfundidad & ","
                _CadValAnt += "Profundidad =" & vProfundidad & ","
            End If
            If _idMarca <> vidMarca Then
                sSql += "idMarca = " & vidMarca & ","
                _CadValAnt += "idMarca =" & vidMarca & ","
            End If
            If _Activo <> vActivo Then
                sSql += "Activo = '" & vActivo & "',"
                _CadValAnt += "Activo =" & vActivo & ","
            End If

            If sSql <> "" Then
                sSql = Mid(sSql, 1, sSql.Length - 1) 'Para quitar la Coma ','
                _CadValAnt = "idProductoLlanta= " & _idProductoLlanta & "," & _CadValAnt
                _CadValAnt = Mid(_CadValAnt, 1, _CadValAnt.Length - 1)
                _CadValNue = Replace(sSql, "'", "")

                sSql = "UPDATE " & _TablaBd & " SET " & sSql & " WHERE idProductoLlanta = " & _idProductoLlanta
                Try
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                    BD.Execute(sSql, True)
                    Audita(_TablaBd, _CadValAnt, _CadValNue, EnTipAccion.Modifica)
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
                Catch ex As Exception
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                    Throw New Exception(ex.Message)
                End Try
            End If

        Else
            If bEsIdentidad Then
                sSql = "insert into " & _TablaBd & " (idMarca,idDisenio,Medida,Profundidad,Activo) " &
                "values (" & vidMarca & "," & vidDisenio & ",'" & vMedida & "'," & vProfundidad & "," & IIf(vActivo, 1, 0) & ")"

            Else
                sSql = "insert into " & _TablaBd & " (idProductoLlanta,idMarca,idDisenio,Medida,Profundidad,Activo) " &
                "values (" & _idProductoLlanta & "," & vidMarca & "," & vidDisenio & ",'" & vMedida & "'," & vProfundidad &
                "," & IIf(vActivo, 1, 0) & ")"

            End If
            _CadValNue = "idMarca=" & vidMarca & "," &
                "Medida = " & vMedida & "," &
                "idDisenio = " & vidDisenio & "," &
                "Profundidad = " & vProfundidad & "," &
                "idMarca = " & vidMarca & "," &
                "Activo = " & _Activo

            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, "", _CadValNue, EnTipAccion.Inserta)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
        End If
        _idMarca = vidMarca
        _idDisenio = vidDisenio
        _Medida = vMedida
        _Profundidad = vProfundidad
        _Activo = vActivo

    End Sub

    Public Sub Eliminar()
        If _Existe Then
            Dim sSql As String = "", _CadValAnt As String = ""
            '_CadValAnt = "idEmpresa=" & _idHerramienta & " , NomSucursal =" & _NomHerramienta

            _CadValAnt = "idDisenio=" & _idDisenio & "," &
                "Medida = " & _Medida & "," &
                "idDesenio = " & _idDisenio & "," &
                "Profundidad = " & _Profundidad & "," &
                "idMarca = " & _idMarca & "," &
                "Activo = " & _Activo

            'sSql = "DELETE FROM " & _TablaBd & " WHERE idLlanta = '" & _idProductoLlanta & "'"
            sSql = "UPDATE " & _TablaBd & " SET  Activo = 0  WHERE idDisenio = " & _idDisenio
            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, _CadValAnt, "", EnTipAccion.Elimina)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
            _Existe = False
        Else
            'se supone que nunca debe entrar aqui. . .
            MessageBox.Show("La Llanta con Id: " & _idProductoLlanta & " No existe por lo tanto no se puede Eliminar!!!",
                            "No se puede eliminar la Llanta porque no Existe", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub
#End Region



#Region "Consultas"
    Public Function TablaLLantasProducto(ByVal bIncluyeTodos As Boolean, Optional ByVal FiltroSinWhere As String = "") As DataTable
        Dim filtro As String = ""
        MyTabla.Rows.Clear()

        If FiltroSinWhere <> "" Then
            filtro = " where " & FiltroSinWhere
        End If

        If bIncluyeTodos Then
            StrSql = "SELECT 0 AS idProductoLlanta,'TODOS' AS Descripcion " &
            "UNION " &
            "SELECT llaPro.idProductoLlanta, " &
            "(m.NombreMarca + ' - ' + d.Descripcion) AS Descripcion " &
            "FROM dbo.CatLLantasProducto llaPro " &
            "INNER JOIN dbo.CatMarcas M ON M.idMarca = llaPro.idMarca " &
            "INNER JOIN dbo.CatDisenios D ON D.idDisenio = llaPro.idDisenio" &
            " ORDER BY idProductoLlanta"


        Else
            'GRCC 16 / 4 / 2018 correccion de bug
            StrSql = "SELECT llaPro.idProductoLlanta, " &
            "(m.NombreMarca + ' - ' + d.Descripcion + ' - ' + llaPro.Medida ) AS Descripcion " &
            "FROM dbo.CatLLantasProducto llaPro " &
            "INNER JOIN dbo.CatMarcas M ON M.idMarca = llaPro.idMarca " &
            "INNER JOIN dbo.CatDisenios D ON D.idDisenio = llaPro.idDisenio" &
            " ORDER BY idProductoLlanta"
        End If
        MyTabla = BD.ExecuteReturn(StrSql)

        Return MyTabla

    End Function
    Public Function TablaLlantasCombo(ByVal FiltroSinWhere As String) As DataTable
        'Dim Filtro As String = ""
        MyTabla.Rows.Clear()

        StrSql = "SELECT idLlanta,idEmpresa, idUnidadTrans,Posicion " &
        " FROM dbo.CatLlantas " & IIf(FiltroSinWhere = "", "", " WHERE " & FiltroSinWhere) &
        " ORDER BY Posicion"

        '"WHERE idDivision > 0 " &

        MyTabla = BD.ExecuteReturn(StrSql)

        Return MyTabla

    End Function
#End Region
End Class
