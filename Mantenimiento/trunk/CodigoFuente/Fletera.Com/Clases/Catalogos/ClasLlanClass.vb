﻿Public Class ClasLlanClass
    Private _idClasLlan As Integer
    Private _NombreClasLlan As String
    Private _NoEjes As Integer
    Private _NoLLantas As Integer

    Private _Existe As Boolean
    Private _TablaBd As String = "CatClasLlan"
    Dim StrSql As String
    Dim DsClase As New DataTable
    Dim MyTabla As DataTable = New DataTable(_TablaBd)

#Region "Constructor"
    Sub New(ByVal vidClasLlan As Integer)
        _idClasLlan = vidClasLlan

        StrSql = "SELECT idClasLlan, " &
        "NombreClasLlan, " &
        "NoEjes, " &
        "NoLLantas " &
        "FROM " & _TablaBd &
        " WHERE idClasLlan = " & vidClasLlan

        DsClase = BD.ExecuteReturn(StrSql)
        If DsClase.Rows.Count > 0 Then
            With DsClase.Rows(0)
                _NombreClasLlan = .Item("NombreClasLlan")
                _NoEjes = .Item("NoEjes")
                _NoLLantas = .Item("NoLLantas")

            End With
            _Existe = True
        Else
            _Existe = False
        End If
    End Sub

    Sub New(ByVal vidClasLlan As String, ByVal vNombreClasLlan As Integer, ByVal vNoEjes As Integer, ByVal vNoLLantas As String)
        _idClasLlan = vidClasLlan
        _NombreClasLlan = vNombreClasLlan
        _NoEjes = vNoEjes
        _NoLLantas = vNoLLantas
        _Existe = True
    End Sub

#End Region

#Region "Propiedades"
    Public Property idClasLlan As Integer
        Get
            Return _idClasLlan
        End Get
        Set(ByVal value As Integer)
            _idClasLlan = value
        End Set
    End Property
    Public Property NombreClasLlan As String
        Get
            Return _NombreClasLlan
        End Get
        Set(ByVal value As String)
            _NombreClasLlan = value
        End Set
    End Property

    Public Property NoEjes As Integer
        Get
            Return _NoEjes
        End Get
        Set(ByVal value As Integer)
            _NoEjes = value
        End Set
    End Property
    Public Property NoLLantas As Integer
        Get
            Return _NoLLantas
        End Get
        Set(ByVal value As Integer)
            _NoLLantas = value
        End Set
    End Property
    Public Property Existe As Boolean
        Get
            Return _Existe
        End Get
        Set(ByVal value As Boolean)
            _Existe = value
        End Set
    End Property
#End Region

#Region "Funciones"
    Public Function GetCambios(ByVal vNombreClasLlan As String, ByVal vNoEjes As Integer, ByVal vNoLLantas As Integer)
        Dim CadCam As String = ""

        If _NombreClasLlan <> vNombreClasLlan Then
            CadCam += "NombreClasLlan: '" & _NombreClasLlan & "' Cambia a [" & vNombreClasLlan & "],"
        End If
        If _NoEjes <> vNoEjes Then
            CadCam += "NoEjes: '" & _NoEjes & "' Cambia a [" & vNoEjes & "],"
        End If
        If _NoLLantas <> vNoLLantas Then
            CadCam += "NoLLantas: '" & _NoLLantas & "' Cambia a [" & vNoLLantas & "],"
        End If

        If CadCam <> "" Then
            CadCam = Mid(CadCam, 1, CadCam.Length - 1)
        End If

        Return CadCam
    End Function

    Public Sub Guardar(ByVal bEsIdentidad As Boolean, ByVal vNombreClasLlan As String, ByVal vNoEjes As Integer, ByVal vNoLLantas As Integer, ByVal Usuario As String)
        Dim sSql As String = "", _CadValAnt As String = "", _CadValNue As String = ""

        If _Existe Then
            If _NombreClasLlan <> vNombreClasLlan Then
                sSql += "NombreClasLlan = '" & vNombreClasLlan & "',"
                _CadValAnt += "NombreClasLlan =" & vNombreClasLlan & ","
            End If
            If _NoEjes <> vNoEjes Then
                sSql += "NoEjes = '" & vNoEjes & "',"
                _CadValAnt += "NoEjes =" & vNoEjes & ","
            End If
            If _NoLLantas <> vNoLLantas Then
                sSql += "NoLLantas = '" & vNoLLantas & "',"
                _CadValAnt += "NoLLantas =" & vNoLLantas & ","
            End If

            If sSql <> "" Then
                sSql = Mid(sSql, 1, sSql.Length - 1) 'Para quitar la Coma ','
                _CadValAnt = "idClasLlan= " & _idClasLlan & "," & _CadValAnt
                _CadValAnt = Mid(_CadValAnt, 1, _CadValAnt.Length - 1)
                _CadValNue = Replace(sSql, "'", "")

                sSql = "UPDATE " & _TablaBd & " SET " & sSql & " WHERE idClasLlan = '" & _idClasLlan & "'"
                Try
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                    BD.Execute(sSql, True)
                    Audita(_TablaBd, _CadValAnt, _CadValNue, EnTipAccion.Modifica, Now, Usuario)
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
                Catch ex As Exception
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                    Throw New Exception(ex.Message)
                End Try
            End If
        Else
            If bEsIdentidad Then
                StrSql = "INSERT INTO " & _TablaBd & " (NombreClasLlan,NoEjes,NoLLantas) VALUES ('" & vNombreClasLlan & "'," & vNoEjes & "," & vNoLLantas & ")"

            Else
                StrSql = "INSERT INTO " & _TablaBd & " (idClasLlan,NombreClasLlan,NoEjes,NoLLantas) VALUES (" & _idClasLlan &
                ",'" & vNombreClasLlan & "'," & vNoEjes & "," & vNoLLantas & ")"
            End If

            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, "", _CadValNue, EnTipAccion.Inserta)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try

        End If
        _NombreClasLlan = vNombreClasLlan
        _NoEjes = vNoEjes
        _NoLLantas = vNoLLantas


    End Sub

    Public Sub Eliminar(ByVal Usuario As String)
        If _Existe Then
            Dim sSql As String = "", _CadValAnt As String = ""
            '_CadValAnt = "idEmpresa=" & _idHerramienta & " , NomSucursal =" & _NomHerramienta

            _CadValAnt = "NombreClasLlan=" & _NombreClasLlan &
                "NoEjes=" & _NoEjes &
                "NoLLantas = " & _NoLLantas



            sSql = "DELETE FROM " & _TablaBd & " WHERE idClasLlan = '" & _idClasLlan & "'"
            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, _CadValAnt, "", EnTipAccion.Elimina, Now, Usuario)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
            _Existe = False
        Else
            'se supone que nunca debe entrar aqui. . .
            MessageBox.Show("La Clasificacion de Llanta con Id: " & _idClasLlan & " No existe por lo tanto no se puede Eliminar!!!",
                            "No se puede eliminar la Clasificacion de Llanta porque no Existe", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub

    Public Sub GuardaRelacion(ByVal Usuario As String, ByVal lsview As ListView, ByVal vidClasLlan As Integer)
        Dim strSql As String
        Dim strSql2 As String
        Dim dtRelacion As DataTable
        Dim Clas As New ClasLlanClass(0)
        Dim _CadVal As String
        'Dim EjeNo, NoLlantas As Integer
        Dim Contador As Integer = 0

        dtRelacion = Clas.TablaPosLlanta(vidClasLlan)
        If dtRelacion IsNot Nothing Then
            'Actualizar
            If dtRelacion.Rows.Count > 0 Then
                strSql2 = "DELETE dbo.RelEjeLlanta WHERE idClasLlan = " & vidClasLlan

                For Each ls As ListViewItem In lsview.Items
                    Dim ar(2) As String
                    ar = TryCast(ls.Tag, String())

                    'For i = 0 To dtRelacion.Rows.Count - 1
                    _CadVal = "idClasLlan= " & vidClasLlan & " ,EjeNo = " & ar(0) & ", NoLlantas = " & ar(1)

                        strSql = "INSERT INTO RelEjeLlanta(idClasLlan,EjeNo,NoLlantas) VALUES (" & vidClasLlan & "," & ar(0) & "," & ar(1) & ")"
                    'If ar(0) = dtRelacion.DefaultView.Item(i).Item("EjeNo") Then
                    '    'Actualizar
                    '    strSql = "UPDATE dbo.RelEjeLlanta SET NoLlantas = " & ar(1) & " WHERE idClasLlan = " & vidClasLlan & " and EjeNo = " & ar(0)
                    '    Exit For
                    'Else
                    '    'Insertar
                    '    strSql = "INSERT INTO RelEjeLlanta(idClasLlan,EjeNo,NoLlantas) VALUES (" & vidClasLlan & "," & ar(0) & "," & ar(1) & ")"
                    '    Exit For
                    'End If


                    'Next

                    Try
                        BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                        If Contador = 0 Then
                            BD.Execute(strSql2, True)
                        End If
                        BD.Execute(strSql, True)
                        Contador = Contador + 1
                        Audita(_TablaBd, _CadVal, "", EnTipAccion.Elimina, Now, Usuario)
                        BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
                    Catch ex As Exception
                        BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                        Throw New Exception(ex.Message)
                    End Try
                Next
            End If
        Else
            'Insertar
            For Each ls As ListViewItem In lsview.Items
                Dim ar(2) As String
                ar = TryCast(ls.Tag, String())
                strSql = "INSERT INTO RelEjeLlanta(idClasLlan,EjeNo,NoLlantas) VALUES (" & vidClasLlan & "," & ar(0) & "," & ar(1) & ")"
                _CadVal = "idClasLlan= " & vidClasLlan & " ,EjeNo = " & ar(0) & ", NoLlantas = " & ar(1)

                Try
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                    BD.Execute(strSql, True)
                    Audita(_TablaBd, _CadVal, "", EnTipAccion.Elimina, Now, Usuario)
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
                Catch ex As Exception
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                    Throw New Exception(ex.Message)
                End Try
            Next

        End If



    End Sub
#End Region

    Public Function TablaPosLlanta(ByVal vidClasLlan As Integer) As DataTable
        MyTabla.Rows.Clear()

        StrSql = "SELECT idClasLlan,EjeNo,NoLlantas " &
        "FROM dbo.RelEjeLlanta " &
        "WHERE idClasLlan = " & vidClasLlan &
        " ORDER BY EjeNo"


        MyTabla = BD.ExecuteReturn(StrSql)

        Return MyTabla


    End Function
End Class
