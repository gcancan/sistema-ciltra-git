﻿Public Class LlantasClass
    Private _idSisLlanta As Integer
    Private _NoSerie As String
    Private _idCondicion As Integer

    Private _idLlanta As String
    Private _idEmpresa As Integer
    Private _idProductoLlanta As Integer
    Private _idTipoLLanta As Integer
    Private _Factura As String
    Private _Fecha As DateTime
    Private _Costo As Double
    Private _RFDI As String
    Private _Posicion As Integer
    Private _idtipoUbicacion As Integer
    Private _idUbicacion As String
    Private _Año As Integer
    Private _Mes As Integer
    Private _Activo As Boolean
    Private _Observaciones As String
    Private _UltFecha As DateTime
    Private _ProfundidadAct As Integer
    Private _idDisenioRev As Integer

    Private _Existe As Boolean
    Private _TablaBd As String = "Catllantas"

    Dim StrSql As String
    Dim DsClase As New DataTable
    Dim MyTabla As DataTable = New DataTable(_TablaBd)

#Region "Constructor"
    Sub New(ByVal vValor As String, ByVal opcion As OpcionLlanta, Optional ByVal vValor2 As String = "")
        Dim filtro As String = ""
        If opcion = OpcionLlanta.idSisLlanta Then
            _idSisLlanta = vValor
            filtro = " where lla.idSisLlanta = " & Val(vValor)
        ElseIf opcion = OpcionLlanta.idLlanta Then
            _idLlanta = vValor
            filtro = " where lla.idLlanta = '" & vValor & "'"
        ElseIf opcion = OpcionLlanta.NoSerie Then
            _NoSerie = vValor
            filtro = " where lla.NoSerie = '" & vValor & "'"
        ElseIf opcion = OpcionLlanta.Posicion Then
            _Posicion = Val(vValor)
            _idUbicacion = vValor2
            filtro = " where lla.idUbicacion = '" & vValor2 & "' and lla.Posicion = " & Val(vValor)
        End If

        StrSql = "SELECT lla.idSisLlanta, " &
        " lla.idLlanta, " &
        "isnull(lla.NoSerie,'') as NoSerie, " &
        "lla.idEmpresa, " &
        "lla.idProductoLlanta, " &
        "lla.idTipoLLanta, " &
        "lla.Factura, " &
        "lla.Fecha, " &
        "lla.Costo, " &
        "lla.RFDI, " &
        "lla.Posicion, " &
        "lla.idtipoUbicacion, " &
        "lla.idUbicacion, " &
        "lla.Año, " &
        "lla.mes, " &
        "isnull(lla.Activo,0) as Activo, " &
        "isnull(lla.Observaciones,'') as Observaciones, " &
        "isnull(lla.idCondicion,'') as idCondicion, " &
        "isnull(lla.ProfundidadAct,0) as ProfundidadAct, " &
        "isnull(lla.idDisenioRev,0) as idDisenioRev, " &
        "lla.UltFecha " &
        "FROM dbo." & _TablaBd & " lla " & filtro


        DsClase = BD.ExecuteReturn(StrSql)
        If DsClase.Rows.Count > 0 Then
            With DsClase.Rows(0)
                If opcion = OpcionLlanta.idSisLlanta Then
                    _idLlanta = .Item("idLlanta")
                    _NoSerie = .Item("NoSerie")
                ElseIf opcion = OpcionLlanta.idLlanta Then
                    _idSisLlanta = .Item("idSisLlanta")
                    _NoSerie = .Item("NoSerie")
                ElseIf opcion = OpcionLlanta.NoSerie Then
                    _idSisLlanta = .Item("idSisLlanta")
                    _idLlanta = .Item("idLlanta")
                ElseIf opcion = OpcionLlanta.Posicion Then
                    _idSisLlanta = .Item("idSisLlanta")
                    _idLlanta = .Item("idLlanta")
                    _NoSerie = .Item("NoSerie")
                End If
                _idEmpresa = .Item("idEmpresa")
                _idProductoLlanta = .Item("idProductoLlanta")
                _idTipoLLanta = .Item("idTipoLLanta")
                _Factura = .Item("Factura")
                _Fecha = .Item("Fecha")
                _Costo = .Item("Costo")
                _RFDI = .Item("RFDI")
                _Posicion = .Item("Posicion")
                _idtipoUbicacion = .Item("idtipoUbicacion")
                _idUbicacion = .Item("idUbicacion")
                _Año = .Item("Año")
                _Mes = .Item("Mes")
                _Activo = .Item("Activo")
                _Observaciones = .Item("Observaciones")
                _idCondicion = .Item("idCondicion")
                _UltFecha = .Item("UltFecha")
                _ProfundidadAct = .Item("ProfundidadAct")
                _idDisenioRev = .Item("idDisenioRev")

            End With
            _Existe = True
        Else
            _Existe = False
        End If

    End Sub
    'Sub New(ByVal vidLlanta As String)


    '    StrSql = "SELECT lla.idSisLlanta, " &
    '    " lla.idLlanta, " &
    '    "isnull(lla.NoSerie,'') as NoSerie, " &
    '    "lla.idEmpresa, " &
    '    "lla.idProductoLlanta, " &
    '    "lla.idTipoLLanta, " &
    '    "lla.Factura, " &
    '    "lla.Fecha, " &
    '    "lla.Costo, " &
    '    "lla.RFDI, " &
    '    "lla.Posicion, " &
    '    "lla.idtipoUbicacion, " &
    '    "lla.idUbicacion, " &
    '    "lla.Año, " &
    '    "lla.mes, " &
    '    "isnull(lla.Activo,0) as Activo, " &
    '    "isnull(lla.Observaciones,'') as Observaciones, " &
    '     "isnull(lla.idCondicion,'') as idCondicion, " &
    '    "isnull(lla.ProfundidadAct,0) as ProfundidadAct, " &
    '    "isnull(lla.idDisenioRev,0) as idDisenioRev, " &
    '    "lla.UltFecha " &
    '    "FROM dbo." & _TablaBd & " lla " &
    '    "where lla.idLlanta = '" & vidLlanta & "'"

    '    DsClase = BD.ExecuteReturn(StrSql)
    '    If DsClase.Rows.Count > 0 Then
    '        With DsClase.Rows(0)
    '            '_idLlanta = .Item("idLlanta")
    '            _idSisLlanta = .Item("idSisLlanta")
    '            _NoSerie = .Item("NoSerie")
    '            _idEmpresa = .Item("idEmpresa")
    '            _idProductoLlanta = .Item("idProductoLlanta")
    '            _idTipoLLanta = .Item("idTipoLLanta")
    '            _Factura = .Item("Factura")
    '            _Fecha = .Item("Fecha")
    '            _Costo = .Item("Costo")
    '            _RFDI = .Item("RFDI")
    '            _Posicion = .Item("Posicion")
    '            _idtipoUbicacion = .Item("idtipoUbicacion")
    '            _idUbicacion = .Item("idUbicacion")
    '            _Año = .Item("Año")
    '            _Mes = .Item("Mes")
    '            _Activo = .Item("Activo")
    '            _Observaciones = .Item("Observaciones")
    '            _idCondicion = .Item("idCondicion")
    '            _UltFecha = .Item("UltFecha")
    '            _ProfundidadAct = .Item("ProfundidadAct")
    '            _idDisenioRev = .Item("idDisenioRev")


    '        End With
    '        _Existe = True
    '    Else
    '        _Existe = False
    '    End If

    'End Sub

    'Sub New(ByVal vNoSerie As String)
    '    _NoSerie = vNoSerie

    '    StrSql = "SELECT lla.idSisLlanta, " &
    '    " lla.idLlanta, " &
    '    "isnull(lla.NoSerie,'') as NoSerie, " &
    '    "lla.idEmpresa, " &
    '    "lla.idProductoLlanta, " &
    '    "lla.idTipoLLanta, " &
    '    "lla.Factura, " &
    '    "lla.Fecha, " &
    '    "lla.Costo, " &
    '    "lla.RFDI, " &
    '    "lla.Posicion, " &
    '    "lla.idtipoUbicacion, " &
    '    "lla.idUbicacion, " &
    '    "lla.Año, " &
    '    "lla.mes, " &
    '    "isnull(lla.Activo,0) as Activo, " &
    '    "isnull(lla.Observaciones,'') as Observaciones, " &
    '     "isnull(lla.idCondicion,'') as idCondicion, " &
    '    "isnull(lla.ProfundidadAct,0) as ProfundidadAct, " &
    '    "isnull(lla.idDisenioRev,0) as idDisenioRev, " &
    '    "lla.UltFecha " &
    '    "FROM dbo." & _TablaBd & " lla " &
    '    "where isnull(lla.NoSerie,'') = '" & vNoSerie & "'"

    '    DsClase = BD.ExecuteReturn(StrSql)
    '    If DsClase.Rows.Count > 0 Then
    '        With DsClase.Rows(0)
    '            _idLlanta = .Item("idLlanta")
    '            _idSisLlanta = .Item("idSisLlanta")
    '            '_NoSerie = .Item("NoSerie")
    '            _idEmpresa = .Item("idEmpresa")
    '            _idProductoLlanta = .Item("idProductoLlanta")
    '            _idTipoLLanta = .Item("idTipoLLanta")
    '            _Factura = .Item("Factura")
    '            _Fecha = .Item("Fecha")
    '            _Costo = .Item("Costo")
    '            _RFDI = .Item("RFDI")
    '            _Posicion = .Item("Posicion")
    '            _idtipoUbicacion = .Item("idtipoUbicacion")
    '            _idUbicacion = .Item("idUbicacion")
    '            _Año = .Item("Año")
    '            _Mes = .Item("Mes")
    '            _Activo = .Item("Activo")
    '            _Observaciones = .Item("Observaciones")
    '            _idCondicion = .Item("idCondicion")
    '            _UltFecha = .Item("UltFecha")
    '            _ProfundidadAct = .Item("ProfundidadAct")
    '            _idDisenioRev = .Item("idDisenioRev")


    '        End With
    '        _Existe = True
    '    Else
    '        _Existe = False
    '    End If
    'End Sub
    'Sub New(ByVal vPosicion As Integer, ByVal vidUnidadTrans As String)
    '    _Posicion = vPosicion
    '    _idUbicacion = vidUnidadTrans

    '    StrSql = "SELECT lla.idLlanta, " &
    '    "isnull(lla.NoSerie,'') as NoSerie, " &
    '    "lla.idSisLlanta, " &
    '    "lla.idEmpresa, " &
    '    "lla.idProductoLlanta, " &
    '    "lla.idTipoLLanta, " &
    '    "lla.Factura, " &
    '    "lla.Fecha, " &
    '    "lla.Costo, " &
    '    "lla.RFDI, " &
    '    "lla.Posicion, " &
    '    "lla.idtipoUbicacion, " &
    '    "lla.idUbicacion, " &
    '    "lla.Año, " &
    '    "lla.mes, " &
    '    "lla.Estatus, " &
    '    "isnull(lla.Observaciones,'') as Observaciones, " &
    '    "isnull(lla.idCondicion,'') as idCondicion, " &
    '    "isnull(lla.ProfundidadAct,0) as ProfundidadAct, " &
    '    "isnull(lla.idDisenioRev,0) as idDisenioRev, " &
    '    "lla.UltFecha " &
    '    "FROM dbo." & _TablaBd & " lla " &
    '    "where lla.idUnidadTrans = '" & vidUnidadTrans & "' and lla.Posicion = " & vPosicion

    '    DsClase = BD.ExecuteReturn(StrSql)
    '    If DsClase.Rows.Count > 0 Then
    '        With DsClase.Rows(0)

    '            '_idEmpresa = .Item("idEmpresa")
    '            '_idProductoLlanta = .Item("idProductoLlanta")
    '            '_idTipoLLanta = .Item("idTipoLLanta")
    '            '_Factura = .Item("Factura")
    '            '_Fecha = .Item("Fecha")
    '            '_Costo = .Item("Costo")
    '            '_RFDI = .Item("RFDI")
    '            '_Año = .Item("Año")
    '            '_Mes = .Item("Mes")
    '            '_Activo = .Item("Activo")
    '            '_Observaciones = .Item("Observaciones")
    '            '_UltFecha = .Item("UltFecha")

    '            _idLlanta = .Item("idLlanta")
    '            _NoSerie = .Item("NoSerie")
    '            _idCondicion = .Item("_idCondicion")
    '            _idEmpresa = .Item("idEmpresa")
    '            _idProductoLlanta = .Item("idProductoLlanta")
    '            _idTipoLLanta = .Item("idTipoLLanta")
    '            _Factura = .Item("Factura")
    '            _Fecha = .Item("Fecha")
    '            _Costo = .Item("Costo")
    '            _RFDI = .Item("RFDI")
    '            _Posicion = .Item("Posicion")
    '            _idtipoUbicacion = .Item("idtipoUbicacion")
    '            _idUbicacion = .Item("idUbicacion")
    '            _Año = .Item("Año")
    '            _Mes = .Item("Mes")
    '            _Activo = .Item("Activo")
    '            _Observaciones = .Item("Observaciones")
    '            _UltFecha = .Item("UltFecha")
    '            _ProfundidadAct = .Item("ProfundidadAct")
    '            _idDisenioRev = .Item("idDisenioRev")

    '        End With
    '        _Existe = True
    '    Else
    '        _Existe = False
    '    End If

    'End Sub

    Sub New(ByVal vidLlanta As String, ByVal vidEmpresa As Integer, ByVal vidProductoLlanta As Integer,
    ByVal vidTipoLLanta As Integer, ByVal vFactura As String,
    ByVal vFecha As DateTime, ByVal vCosto As Double, ByVal vRFDI As String, ByVal vPosicion As Integer,
    ByVal vidUnidadTrans As String, ByVal vAño As Integer, ByVal vMes As Integer, ByVal vActivo As Boolean,
    ByVal vObservaciones As String, ByVal vUltFecha As DateTime, ByVal vidtipoUbicacion As Integer,
    ByVal vProfundidadAct As Decimal, ByVal vidDisenioRev As Integer)
        _idLlanta = vidLlanta
        _idEmpresa = vidEmpresa
        _idProductoLlanta = vidProductoLlanta
        _idTipoLLanta = vidTipoLLanta
        _Factura = vFactura
        _Fecha = vFecha
        _Costo = vCosto
        _RFDI = vRFDI
        _Posicion = vPosicion
        _idtipoUbicacion = vidtipoUbicacion
        _idUbicacion = vidUnidadTrans
        _Año = vAño
        _Mes = vMes
        _Activo = vActivo
        _Observaciones = vObservaciones
        _UltFecha = vUltFecha
        _ProfundidadAct = vProfundidadAct
        _idDisenioRev = vidDisenioRev
        _Existe = True

    End Sub
#End Region

#Region "Propiedades"
    Public Property idSisLlanta As Integer
        Get
            Return _idSisLlanta
        End Get
        Set(ByVal value As Integer)
            _idSisLlanta = value
        End Set
    End Property
    Public Property idLlanta As String
        Get
            Return _idLlanta
        End Get
        Set(ByVal value As String)
            _idLlanta = value
        End Set
    End Property

    Public Property NoSerie As String
        Get
            Return _NoSerie
        End Get
        Set(ByVal value As String)
            _NoSerie = value
        End Set
    End Property
    Public Property idEmpresa As Integer
        Get
            Return _idEmpresa
        End Get
        Set(ByVal value As Integer)
            _idEmpresa = value
        End Set
    End Property

    Public Property idProductoLlanta As Integer
        Get
            Return _idProductoLlanta
        End Get
        Set(ByVal value As Integer)
            _idProductoLlanta = value
        End Set
    End Property

    Public Property idTipoLLanta As Integer
        Get
            Return _idTipoLLanta
        End Get
        Set(ByVal value As Integer)
            _idTipoLLanta = value
        End Set
    End Property

    Public Property Factura As String
        Get
            Return _Factura
        End Get
        Set(ByVal value As String)
            _Factura = value
        End Set
    End Property

    Public Property Fecha As DateTime
        Get
            Return _Fecha
        End Get
        Set(ByVal value As DateTime)
            _Fecha = value
        End Set
    End Property

    Public Property Costo As Double
        Get
            Return _Costo
        End Get
        Set(ByVal value As Double)
            _Costo = value
        End Set
    End Property

    Public Property RFDI As String
        Get
            Return _RFDI
        End Get
        Set(ByVal value As String)
            _RFDI = value
        End Set
    End Property

    Public Property Posicion As Integer
        Get
            Return _Posicion
        End Get
        Set(ByVal value As Integer)
            _Posicion = value
        End Set
    End Property
    Public Property idtipoUbicacion As Integer
        Get
            Return _idtipoUbicacion
        End Get
        Set(ByVal value As Integer)
            _idtipoUbicacion = value
        End Set
    End Property
    Public Property idUbicacion As String
        Get
            Return _idUbicacion
        End Get
        Set(ByVal value As String)
            _idUbicacion = value
        End Set
    End Property

    Public Property Año As Integer
        Get
            Return _Año
        End Get
        Set(ByVal value As Integer)
            _Año = value
        End Set
    End Property

    Public Property Mes As Integer
        Get
            Return _Mes
        End Get
        Set(ByVal value As Integer)
            _Mes = value
        End Set
    End Property

    Public Property Activo As Boolean
        Get
            Return _Activo
        End Get
        Set(ByVal value As Boolean)
            _Activo = value
        End Set
    End Property
    Public Property Observaciones As String
        Get
            Return _Observaciones
        End Get
        Set(ByVal value As String)
            _Observaciones = value
        End Set
    End Property

    Public Property UltFecha As DateTime
        Get
            Return _UltFecha
        End Get
        Set(ByVal value As DateTime)
            _UltFecha = value
        End Set
    End Property

    Public Property idCondicion As Integer
        Get
            Return _idCondicion
        End Get
        Set(ByVal value As Integer)
            _idCondicion = value
        End Set
    End Property
    Public Property ProfundidadAct As Decimal
        Get
            Return _ProfundidadAct
        End Get
        Set(ByVal value As Decimal)
            _ProfundidadAct = value
        End Set
    End Property

    Public Property idDisenioRev As Integer
        Get
            Return _idDisenioRev
        End Get
        Set(ByVal value As Integer)
            _idDisenioRev = value
        End Set
    End Property
    Public Property Existe As Boolean
        Get
            Return _Existe
        End Get
        Set(ByVal value As Boolean)
            _Existe = value
        End Set
    End Property


#End Region

#Region "Funciones"
    Public Function GetCambios(ByVal vidEmpresa As Integer, ByVal vidProductoLlanta As Integer, ByVal vDiseño As String,
    ByVal vMedida As String, ByVal vProfundidad As Double, ByVal vidTipoLLanta As Integer, ByVal vFactura As String,
    ByVal vFecha As DateTime, ByVal vCosto As Double, ByVal vRFDI As String, ByVal vPosicion As Integer,
    ByVal vidUbicacion As String, ByVal vAño As Integer, ByVal vMes As Integer, ByVal vActivo As Boolean,
    ByVal vObservaciones As String, ByVal vUltFecha As DateTime, ByVal vidtipoUbicacion As Integer,
    ByVal vidLlanta As String, ByVal vNoSerie As String, ByVal vidCondicion As Integer,
    ByVal vProfundidadAct As Decimal, ByVal vidDisenioRev As Integer) As String
        Dim CadCam As String = ""
        If _ProfundidadAct <> vProfundidadAct Then
            CadCam += "ProfundidadAct: '" & _ProfundidadAct & "' Cambia a [" & vProfundidadAct & "],"
        End If
        If _idDisenioRev <> vidDisenioRev Then
            CadCam += "idDisenioRev: '" & _idDisenioRev & "' Cambia a [" & vidDisenioRev & "],"
        End If
        If _idLlanta <> vidLlanta Then
            CadCam += "idLlanta: '" & _idLlanta & "' Cambia a [" & vidLlanta & "],"
        End If
        If _NoSerie <> vNoSerie Then
            CadCam += "NoSerie: '" & _NoSerie & "' Cambia a [" & vNoSerie & "],"
        End If
        If _idCondicion <> vidCondicion Then
            CadCam += "idCondicion: '" & _idCondicion & "' Cambia a [" & vidCondicion & "],"
        End If
        If _idEmpresa <> vidEmpresa Then
            CadCam += "idEmpresa: '" & _idEmpresa & "' Cambia a [" & vidEmpresa & "],"
        End If
        If _idProductoLlanta <> vidProductoLlanta Then
            CadCam += "idProductoLlanta: '" & _idProductoLlanta & "' Cambia a [" & vidProductoLlanta & "],"
        End If
        If _idTipoLLanta <> vidTipoLLanta Then
            CadCam += "idTipoLLanta: '" & _idTipoLLanta & "' Cambia a [" & vidTipoLLanta & "],"
        End If

        If _Factura <> vFactura Then
            CadCam += "Factura: '" & _Factura & "' Cambia a [" & vFactura & "],"
        End If
        If _Fecha <> vFecha Then
            CadCam += "Fecha: '" & _Fecha & "' Cambia a [" & vFecha & "],"
        End If
        If _Costo <> vCosto Then
            CadCam += "Costo: '" & _Costo & "' Cambia a [" & vCosto & "],"
        End If
        If _RFDI <> vRFDI Then
            CadCam += "RFDI: '" & _RFDI & "' Cambia a [" & vRFDI & "],"
        End If
        If _Posicion <> vPosicion Then
            CadCam += "Posicion: '" & _Posicion & "' Cambia a [" & vPosicion & "],"
        End If
        If _idUbicacion <> vidUbicacion Then
            CadCam += "idUbicacion: '" & _idUbicacion & "' Cambia a [" & vidUbicacion & "],"
        End If
        If _Año <> vAño Then
            CadCam += "Año: '" & _Año & "' Cambia a [" & vAño & "],"
        End If
        If _Observaciones <> vObservaciones Then
            CadCam += "Observaciones: '" & _Observaciones & "' Cambia a [" & vObservaciones & "],"
        End If
        If _Activo <> vActivo Then
            CadCam += "Activo: '" & _Activo & "' Cambia a [" & vActivo & "],"
        End If
        If _Mes <> vMes Then
            CadCam += "Mes: '" & _Mes & "' Cambia a [" & vMes & "],"
        End If
        If _UltFecha <> vUltFecha Then
            CadCam += "UltFecha: '" & _UltFecha & "' Cambia a [" & vUltFecha & "],"
        End If
        If _idtipoUbicacion <> vidtipoUbicacion Then
            CadCam += "idtipoUbicacion: '" & _idtipoUbicacion & "' Cambia a [" & vidtipoUbicacion & "],"
        End If

        If CadCam <> "" Then
            CadCam = Mid(CadCam, 1, CadCam.Length - 1)
        End If
        Return CadCam

    End Function

    'Public Sub GuardaHistorico(ByVal vTipoMov As String, ByVal vFecha As String, ByVal vUsuario As String, ByVal vidLlanta As String,
    '                     ByVal vidUniOrig As String, ByVal vPosUniOrig As Integer, ByVal vidUniAct As String,
    '                     ByVal vPosUniAct As Integer, ByVal vObservaciones As String, ByVal vNoSerie As String,
    '                     ByVal vidCondicion As Integer)
    Public Sub GuardaHistorico(ByVal vidtipoUbicacion As Integer, ByVal vidUbicacion As String,
    ByVal vFecha As DateTime, ByVal vUsuario As String, ByVal vidSisLlanta As Integer,
    ByVal vidLlanta As String, ByVal vEntSal As Boolean, ByVal vidTipoDoc As Integer, ByVal vDocumento As String,
    ByVal vProfundidad As Decimal, ByVal vidCondicion As Integer, ByVal vPosicion As Integer)
        Dim _CadValNue As String

        'StrSql = "INSERT INTO dbo.MovLLantas " &
        '    "( TipoMov ,Fecha ,Usuario ,idLlanta ,idUniOrig ,PosUniOrig ,idUniAct ,PosUniAct,Observaciones,UltFecha)" &
        '    " VALUES " &
        '    "( '" & vTipoMov & "','" & vFecha & "','" & vUsuario & "','" & vidLlanta & "','" & vidUniOrig &
        '    "'," & vPosUniOrig & ",'" & vidUniAct & "'," & vPosUniAct & ",'" & vObservaciones & "')"

        StrSql = "INSERT INTO dbo.LlantasHis ( idtipoUbicacion ,idUbicacion ,Fecha ,Usuario ,idSisLlanta ,idLlanta ,EntSal , " &
            "idTipoDoc ,Documento,Profundidad,idCondicion,Posicion)" &
            "VALUES (" & vidtipoUbicacion & ",'" & vidUbicacion & "','" & FormatFecHora(vFecha, True, True) & "' ,'" & vUsuario &
            "' ," & vidSisLlanta & " ,'" & vidLlanta & "' ," & IIf(vEntSal, 1, 0) & " ," & vidTipoDoc & ",'" & vDocumento &
            "'," & vProfundidad & "," & vidCondicion & "," & vPosicion & ")"


        _CadValNue = "idtipoUbicacion=" & vidtipoUbicacion & "," &
                "idUbicacion=" & vidUbicacion & "," &
                "Fecha = " & vFecha & "," &
                "Usuario = " & vUsuario & "," &
                "idSisLlanta = " & vidSisLlanta & "," &
                "idLlanta = " & vidLlanta & "," &
                "EntSal = " & IIf(vEntSal, 1, 0) & "," &
                "idTipoDoc = " & vidTipoDoc & "," &
                "Profundidad = " & vProfundidad & "," &
                "idCondicion = " & vidCondicion & "," &
                "Documento = " & vDocumento

        Try
            BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
            BD.Execute(StrSql, True)
            Audita("MovLLantas", "", _CadValNue, EnTipAccion.Inserta)
            BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
        Catch ex As Exception
            BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
            Throw New Exception(ex.Message)
        End Try

    End Sub

    Public Sub InsertaMovimientos(ByVal vTipoMov As Integer, ByVal vFecha As DateTime,
    ByVal vUsuario As String, ByVal vidSisLlanta As Integer, ByVal vidtipoUbicacionOrig As Integer, ByVal vidUbicacionOrig As String,
    ByVal vPosicionOrig As Integer, ByVal vidtipoUbicacionDes As Integer, ByVal vidUbicacionDes As String, ByVal vPosicionDes As Integer,
    ByVal vObservaciones As String)

        StrSql = "insert into MovLLantas (idMovLlanta,TipoMov,Fecha,Usuario,idSisLlanta,idtipoUbicacionOrig, " &
        "idUbicacionOrig,PosicionOrig,idtipoUbicacionDes,idUbicacionDes,PosicionDes,Observaciones) values ( " &
        vTipoMov & ",'" & FormatFecHora(vFecha, True, True) & "','" & vUsuario & "'," & vidSisLlanta & "," &
        vidtipoUbicacionOrig & ",'" & vidUbicacionOrig & "'," & vPosicionOrig & "," & vidtipoUbicacionDes & ",'" & vidUbicacionDes & "'," &
        vPosicionDes & ",'" & vObservaciones & "')"
        Try
            BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
            BD.Execute(StrSql, True)
            'Audita(_TablaBd, _CadValAnt, _CadValNue, EnTipAccion.Modifica)
            BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
        Catch ex As Exception
            BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
            Throw New Exception(ex.Message)
        End Try

    End Sub

    Public Sub ActualizaCatLlantas(ByVal idtipoUbicacion As Integer, ByVal idUbicacion As String, ByVal Posicion As Integer,
    ByVal UltFecha As DateTime, ByVal idSisLlanta As Integer, ByVal ProfundidadAct As Decimal)
        StrSql = "UPDATE dbo.CatLlantas SET idtipoUbicacion = " & idtipoUbicacion & ", idUbicacion = '" & idUbicacion &
        "', Posicion = " & Posicion & ", UltFecha = '" & FormatFecHora(UltFecha, True, True) & "',ProfundidadAct = " & ProfundidadAct & " WHERE idSisLlanta = " & idSisLlanta

        Try
            BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
            BD.Execute(StrSql, True)
            'Audita(_TablaBd, _CadValAnt, _CadValNue, EnTipAccion.Modifica)
            BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
        Catch ex As Exception
            BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
            Throw New Exception(ex.Message)
        End Try
    End Sub

    Public Sub Guardar(ByVal bEsIdentidad As Boolean, ByVal vidEmpresa As Integer, ByVal vidProductoLlanta As Integer, ByVal vidTipoLLanta As Integer, ByVal vFactura As String,
    ByVal vFecha As String, ByVal vCosto As Double, ByVal vRFDI As String, ByVal vPosicion As Integer,
    ByVal vidUbicacion As String, ByVal vAño As Integer, ByVal vMes As Integer, ByVal vActivo As Boolean,
    ByVal vObservaciones As String, ByVal vUltFecha As String, ByVal vidtipoUbicacion As Integer,
    ByVal vidLlanta As String, ByVal vNoSerie As String, ByVal vidCondicion As Integer, ByVal vProfundidadAct As Decimal,
    ByVal vidDisenioRev As Integer)

        Dim sSql As String = "", _CadValAnt As String = "", _CadValNue As String = ""

        If _Existe Then
            If _idLlanta <> vidLlanta Then
                sSql += "idLlanta = '" & vidLlanta & "',"
                _CadValAnt += "idLlanta =" & vidLlanta & ","
            End If
            If _NoSerie <> vNoSerie Then
                sSql += "NoSerie = '" & vNoSerie & "',"
                _CadValAnt += "NoSerie =" & vNoSerie & ","
            End If
            If _ProfundidadAct <> vProfundidadAct Then
                sSql += "ProfundidadAct = " & vProfundidadAct & ","
                _CadValAnt += "ProfundidadAct =" & vProfundidadAct & ","
            End If
            If _ProfundidadAct <> vProfundidadAct Then
                sSql += "ProfundidadAct = " & vProfundidadAct & ","
                _CadValAnt += "ProfundidadAct =" & vProfundidadAct & ","
            End If
            If _idCondicion <> vidCondicion Then
                sSql += "idCondicion = " & vidCondicion & ","
                _CadValAnt += "idCondicion =" & vidCondicion & ","
            End If

            If _idEmpresa <> vidEmpresa Then
                sSql += "idEmpresa = " & vidEmpresa & ","
                _CadValAnt += "idEmpresa =" & vidEmpresa & ","
            End If
            If _idProductoLlanta <> vidProductoLlanta Then
                sSql += "idProductoLlanta = " & vidProductoLlanta & ","
                _CadValAnt += "idProductoLlanta =" & vidProductoLlanta & ","
            End If
            If _idTipoLLanta <> vidTipoLLanta Then
                sSql += "idTipoLLanta = " & vidTipoLLanta & ","
                _CadValAnt += "idTipoLLanta =" & vidTipoLLanta & ","
            End If
            If _Factura <> vFactura Then
                sSql += "Factura = '" & vFactura & "',"
                _CadValAnt += "Factura =" & vFactura & ","
            End If
            If _Fecha <> vFecha Then
                sSql += "Fecha = '" & vFecha & "',"
                _CadValAnt += "Fecha =" & vFecha & ","
            End If
            If _Costo <> vCosto Then
                sSql += "Costo = " & vCosto & ","
                _CadValAnt += "Costo =" & vCosto & ","
            End If
            If _RFDI <> vRFDI Then
                sSql += "RFDI = '" & vRFDI & "',"
                _CadValAnt += "RFDI =" & vRFDI & ","
            End If
            If _Posicion <> vPosicion Then
                sSql += "Posicion = " & vPosicion & ","
                _CadValAnt += "Posicion =" & vPosicion & ","
            End If
            If _idUbicacion <> vidUbicacion Then
                sSql += "idUbicacion = '" & vidUbicacion & "',"
                _CadValAnt += "idUbicacion =" & vidUbicacion & ","
            End If
            If _Año <> vAño Then
                sSql += "Año = " & vAño & ","
                _CadValAnt += "Año =" & vAño & ","
            End If
            If _Mes <> vMes Then
                sSql += "Mes = " & vMes & ","
                _CadValAnt += "Mes =" & vMes & ","
            End If

            If _Activo <> vActivo Then
                sSql += "Activo = " & IIf(vActivo, 1, 0) & ","
                _CadValAnt += "Activo =" & IIf(vActivo, 1, 0) & ","
            End If


            If _Observaciones <> vObservaciones Then
                sSql += "Observaciones = '" & vObservaciones & "',"
                _CadValAnt += "Observaciones =" & vObservaciones & ","
            End If

            'vUltFecha
            If _UltFecha <> vUltFecha Then
                sSql += "UltFecha = '" & vUltFecha & "',"
                _CadValAnt += "UltFecha =" & vUltFecha & ","
            End If
            '
            If _idtipoUbicacion <> vidtipoUbicacion Then
                sSql += "idtipoUbicacion = " & vidtipoUbicacion & ","
                _CadValAnt += "idtipoUbicacion =" & vidtipoUbicacion & ","
            End If

            If sSql <> "" Then
                sSql = Mid(sSql, 1, sSql.Length - 1) 'Para quitar la Coma ','
                _CadValAnt = "idLlanta= " & _idLlanta & "," & _CadValAnt
                _CadValAnt = Mid(_CadValAnt, 1, _CadValAnt.Length - 1)
                _CadValNue = Replace(sSql, "'", "")

                sSql = "UPDATE " & _TablaBd & " SET " & sSql & " WHERE idSisLlanta = " & _idSisLlanta
                Try
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                    BD.Execute(sSql, True)
                    Audita(_TablaBd, _CadValAnt, _CadValNue, EnTipAccion.Modifica)
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
                Catch ex As Exception
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                    Throw New Exception(ex.Message)
                End Try
            End If

        Else
            If bEsIdentidad Then
                sSql = "insert into " & _TablaBd & " (idLlanta,idEmpresa,idProductoLlanta,idTipoLLanta, " &
                "Factura,Fecha,Costo,RFDI,Posicion,idUbicacion,Año,Mes,Activo,Observaciones,UltFecha,idtipoUbicacion, " &
                "NoSerie,idCondicion,ProfundidadAct,idDisenioRev) " &
                "values ('" & vidLlanta & "'," & vidEmpresa & "," & vidProductoLlanta & "," & vidTipoLLanta & ",'" &
                vFactura & "','" & vFecha & "'," & vCosto & ",'" & vRFDI & "'," & vPosicion & ",'" & vidUbicacion & "'," & vAño & "," & vMes &
                "," & IIf(vActivo, 1, 0) & ",'" & vObservaciones & "','" & vUltFecha & "'," & vidtipoUbicacion & ",'" & vNoSerie &
                "'," & vidCondicion & "," & vProfundidadAct & "," & vidDisenioRev & ")"
            Else
                sSql = "insert into " & _TablaBd & " (idSisLlanta,idLlanta,idEmpresa,idProductoLlanta,idTipoLLanta, " &
                "Factura,Fecha,Costo,RFDI,Posicion,idUbicacion,Año,Mes,Activo,Observaciones,UltFecha,idtipoUbicacion, " &
                "NoSerie,idCondicion,ProfundidadAct,idDisenioRev) " &
                "values (" & _idSisLlanta & ",'" & vidLlanta & "'," & vidEmpresa & "," & vidProductoLlanta & "," & vidTipoLLanta & ",'" &
                vFactura & "','" & vFecha & "'," & vCosto & ",'" & vRFDI & "'," & vPosicion & ",'" & vidUbicacion & "'," & vAño & "," & vMes &
                "," & IIf(vActivo, 1, 0) & ",'" & vObservaciones & "','" & vUltFecha & "'," & vidtipoUbicacion & ",'" & vNoSerie &
                "'," & vidCondicion & "," & vProfundidadAct & ", " & vidDisenioRev & ")"

            End If
            _CadValNue = "idEmpresa=" & vidEmpresa & "," &
                "idLlanta=" & vidLlanta & "," &
                "NoSerie=" & vNoSerie & "," &
                "idCondicion=" & vidCondicion & "," &
                "idProductoLlanta=" & vidProductoLlanta & "," &
                "Factura = " & vFactura & "," &
                "Fecha = " & vFecha & "," &
                "Costo = " & vCosto & "," &
                "RFDI = " & vRFDI & "," &
                "Posicion = " & vPosicion & "," &
                "idUbicacion = " & vidUbicacion & "," &
                "Año = " & vAño & "," &
                "Mes = " & vMes & "," &
                "Activo = " & IIf(vActivo, 1, 0) & "," &
                "vObservaciones = " & vObservaciones & "," &
                "UltFecha = " & vUltFecha & "," &
                "ProfundidadAct = " & vProfundidadAct & "," &
                "idDisenioRev = " & vidDisenioRev & "," &
                "idtipoUbicacion = " & vidtipoUbicacion

            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, "", _CadValNue, EnTipAccion.Inserta)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
        End If

        _idEmpresa = vidEmpresa
        _idLlanta = vidLlanta
        _NoSerie = vNoSerie
        _idCondicion = vidCondicion
        _idProductoLlanta = vidProductoLlanta
        _idTipoLLanta = vidTipoLLanta
        _Factura = vFactura
        '_Fecha = Convert.ToDateTime(vFecha)
        _Fecha = Now
        _Costo = vCosto
        _RFDI = vRFDI
        _Posicion = vPosicion
        _idtipoUbicacion = vidtipoUbicacion
        _idUbicacion = vidUbicacion
        _Año = vAño
        _Mes = vMes
        _Activo = vActivo
        '_UltFecha = vUltFecha
        _UltFecha = Now
        _idDisenioRev = vidDisenioRev
        _ProfundidadAct = vProfundidadAct


    End Sub

    Public Sub Eliminar()
        If _Existe Then
            Dim sSql As String = "", _CadValAnt As String = ""
            '_CadValAnt = "idEmpresa=" & _idHerramienta & " , NomSucursal =" & _NomHerramienta

            _CadValAnt = "idEmpresa=" & _idEmpresa & "," &
                "idLlanta=" & _idLlanta & "," &
                "idProductoLlanta=" & _idProductoLlanta & "," &
                 "NoSerie=" & _NoSerie & "," &
                "idCondicion=" & _idCondicion & "," &
                "Factura = " & _Factura & "," &
                "Fecha = " & _Fecha & "," &
                "Costo = " & _Costo & "," &
                "RFDI = " & _RFDI & "," &
                "Posicion = " & _Posicion & "," &
                "idUnidadTrans = " & _idUbicacion & "," &
                "Año = " & _Año & "," &
                "Mes = " & _Mes & "," &
                "Activo = " & IIf(_Activo, 1, 0) & "," &
                "Observaciones = " & _Observaciones & "," &
                "UltFecha = " & _UltFecha & "," &
                "ProfundidadAct = " & _ProfundidadAct & "," &
                "idDisenioRev = " & _idDisenioRev & "," &
                "idtipoUbicacion = " & _idtipoUbicacion

            'sSql = "DELETE FROM " & _TablaBd & " WHERE idLlanta = '" & _idLlanta & "'"
            sSql = "UPDATE " & _TablaBd & " SET  Activo = 0  WHERE idSisLlanta = " & _idSisLlanta
            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, _CadValAnt, "", EnTipAccion.Elimina)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
            _Existe = False
        Else
            'se supone que nunca debe entrar aqui. . .
            MessageBox.Show("La Llanta con Id: " & _idLlanta & " No existe por lo tanto no se puede Eliminar!!!",
                            "No se puede eliminar la Llanta porque no Existe", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub
#End Region

    Public Function ConsecutivoLlantas(ByVal idEmpresa As Integer, ByVal Año As Integer, ByVal Mes As Integer) As String
        Dim vultimo As Integer = 0
        Dim Resp As String

        MyTabla.Rows.Clear()

        StrSql = "SELECT (COUNT(*) + 1) as Ultimo FROM dbo.CatLlantas WHERE Año = " & Año & " AND mes = " & Mes & " and idEmpresa = " & idEmpresa

        MyTabla = BD.ExecuteReturn(StrSql)



        If Not MyTabla Is Nothing Then
            If MyTabla.Rows.Count > 0 Then
                vultimo = MyTabla.DefaultView.Item(0).Item("Ultimo")
            End If
        End If

        If vultimo < 10 Then
            Resp = "0" & vultimo
        Else
            Resp = vultimo
        End If

        Return Resp

    End Function

    Public Function TablaLlantasCombo(ByVal FiltroSinWhere As String) As DataTable
        'Dim Filtro As String = ""
        MyTabla.Rows.Clear()

        StrSql = "SELECT idLlanta,idEmpresa, idUbicacion,Posicion " &
        " FROM dbo.CatLlantas " & IIf(FiltroSinWhere = "", "", " WHERE " & FiltroSinWhere) &
        " ORDER BY Posicion"

        '"WHERE idDivision > 0 " &

        MyTabla = BD.ExecuteReturn(StrSql)

        Return MyTabla

    End Function
End Class
