﻿Public Class CatTiposFoliosDinClass
    Private _idTipoFolDin As Integer
    Private _Descripcion As String

    Private _Existe As Boolean
    Private _TablaBd As String = "CatTiposFoliosDin"

    Dim StrSql As String
    Dim DsClase As New DataTable
    Dim MyTabla As DataTable = New DataTable(_TablaBd)

#Region "Constructor"

    Sub New(ByVal vidTipoFolDin As Integer)
        _idTipoFolDin = vidTipoFolDin

        StrSql = "SELECT td.idTipoFolDin, " &
        "td.Descripcion " &
        "FROM dbo." & _TablaBd & " td " &
        "WHERE td.idTipoFolDin = " & vidTipoFolDin

        DsClase = BD.ExecuteReturn(StrSql)
        If DsClase.Rows.Count > 0 Then
            With DsClase.Rows(0)
                _Descripcion = .Item("Descripcion")
            End With
            _Existe = True
        Else
            _Existe = False
        End If

    End Sub

    Sub New(ByVal vidTipoFolDin As Integer, ByVal vDescripcion As String)
        _idTipoFolDin = vidTipoFolDin
        _Descripcion = vDescripcion
        _Existe = True
    End Sub
#End Region

#Region "Propiedades"
    Public Property idTipoFolDin As Integer
        Get
            Return _idTipoFolDin
        End Get
        Set(ByVal value As Integer)
            _idTipoFolDin = value
        End Set
    End Property
    Public Property Descripcion As String
        Get
            Return _Descripcion
        End Get
        Set(ByVal value As String)
            _Descripcion = value
        End Set
    End Property
    Public Property Existe As Boolean
        Get
            Return _Existe
        End Get
        Set(ByVal value As Boolean)
            _Existe = value
        End Set
    End Property
#End Region

#Region "Funciones"
    Public Function GetCambios(ByVal vDescripcion As String) As String
        Dim CadCam As String = ""
        If _Descripcion <> vDescripcion Then
            CadCam += "Descripcion: '" & _Descripcion & "' Cambia a [" & vDescripcion & "],"
        End If
        If CadCam <> "" Then
            CadCam = Mid(CadCam, 1, CadCam.Length - 1)
        End If
        Return CadCam
    End Function
    Public Sub Guardar(ByVal vDescripcion As String, ByVal bEsIdentidad As Boolean)
        Dim sSql As String = "", _CadValAnt As String = "", _CadValNue As String = ""
        If _Existe Then
            If _Descripcion <> vDescripcion Then
                sSql += "Descripcion = '" & vDescripcion & "',"
                _CadValAnt += "Descripcion =" & vDescripcion & ","
            End If

            If sSql <> "" Then
                sSql = Mid(sSql, 1, sSql.Length - 1) 'Para quitar la Coma ','
                _CadValAnt = "idTipoFolDin=" & _idTipoFolDin & "," & _CadValAnt
                _CadValAnt = Mid(_CadValAnt, 1, _CadValAnt.Length - 1)
                _CadValNue = Replace(sSql, "'", "")
                sSql = "UPDATE " & _TablaBd & " SET " & sSql & " WHERE idTipoFolDin = '" & _idTipoFolDin & "'"
                Try
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                    BD.Execute(sSql, True)
                    Audita(_TablaBd, _CadValAnt, _CadValNue, EnTipAccion.Modifica)
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
                Catch ex As Exception
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                    Throw New Exception(ex.Message)
                End Try
            End If
        Else
            If bEsIdentidad Then
                sSql = "INSERT INTO " & _TablaBd & "(Descripcion) VALUES('" & _Descripcion & "')"

                _CadValNue = "Descripcion=" & _Descripcion
            Else

                sSql = "INSERT INTO " & _TablaBd & "(idTipoFolDin,Descripcion) VALUES(" &
                "'" & _idTipoFolDin & "','" & _Descripcion & "')"

                _CadValNue = "idTipoFolDin=" & _idTipoFolDin & ",Descripcion=" & _Descripcion
            End If

            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, "", _CadValNue, EnTipAccion.Inserta)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
        End If
        _Descripcion = vDescripcion
    End Sub
    Public Sub Eliminar()
        If _Existe Then
            Dim sSql As String = "", _CadValAnt As String = ""
            _CadValAnt = "idTipoFolDin=" & _idTipoFolDin & " , Descripcion =" & _Descripcion
            sSql = "DELETE FROM " & _TablaBd & " WHERE idTipoFolDin = '" & _idTipoFolDin & "'"
            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, _CadValAnt, "", EnTipAccion.Elimina)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
            _Existe = False
        Else
            'se supone que nunca debe entrar aqui. . .
            MessageBox.Show("El País con Id: " & _idTipoFolDin & " No existe por lo tanto no se puede Eliminar!!!",
                            "No se puede eliminar el País porque no Existe", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub
#End Region

#Region "Consultas"
    Public Function TablaTiposFolioDinero(ByVal bIncluyeTodos As Boolean, Optional ByVal FiltroSinWhere As String = "") As DataTable
        Dim filtro As String = ""
        MyTabla.Rows.Clear()

        If FiltroSinWhere <> "" Then
            filtro = " where " & FiltroSinWhere
        End If

        If bIncluyeTodos Then
            StrSql = "SELECT 0 AS idTipoFolDin,'TODOS' AS Descripcion " &
            "UNION " &
            "SELECT idTipoFolDin,Descripcion FROM dbo.CatTiposFoliosDin " & filtro &
            " ORDER BY idTipoFolDin"

        Else
            StrSql = "SELECT idTipoFolDin,Descripcion FROM dbo.CatTiposFoliosDin " & filtro &
            " ORDER BY idTipoFolDin"
        End If
        MyTabla = BD.ExecuteReturn(StrSql)

        Return MyTabla

    End Function
#End Region


End Class
