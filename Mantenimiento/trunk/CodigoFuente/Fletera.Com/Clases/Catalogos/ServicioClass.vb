﻿Public Class ServicioClass
    'idServicio				vidServicio 				_idServicio 
    'idTipoServicio			vidTipoServicio 			_idTipoServicio 
    'NomServicio			vNomServicio 				_NomServicio 
    'Descripcion			vDescripcion 				_Descripcion 
    'CadaKms				vCadaKms 					_CadaKms
    'CadaTiempoDias			vCadaTiempoDias 			_CadaTiempoDias 
    'Estatus				vEstatus 					_Estatus
    'Costo					vCosto 						_Costo
    Private _idServicio As Integer
    Private _idTipoServicio As Integer
    Private _NomServicio As String
    Private _Descripcion As String
    Private _CadaKms As Double
    Private _CadaTiempoDias As Double
    Private _Estatus As String
    Private _Costo As Double
    Private _idDivision As Integer
    Private _NomDivision As String

    Private _Existe As Boolean
    Private _TablaBd As String = "CatServicios"

    Dim StrSql As String
    Dim DsClase As New DataTable
    Dim MyTabla As DataTable = New DataTable(_TablaBd)
#Region "Constructor"
    Sub New(ByVal vidServicio As Integer)
        _idServicio = vidServicio

        StrSql = "select ser.idServicio, ser.NomServicio, " & _
        "ser.descripcion, ser.Estatus, " & _
        "ser.idTipoServicio, tip.NomTipoServicio, " & _
        "ser.CadaKms, ser.CadaTiempoDias, ser.costo, ser.idDivision, div.NomDivision " & _
        "from " & _TablaBd & " Ser " & _
        "inner join cattiposervicio tip on ser.idTipoServicio = tip.idTipoServicio " & _
        "inner join catdivision div on ser.iddivision = div.iddivision " & _
        "where ser.idServicio = " & vidServicio

        DsClase = BD.ExecuteReturn(StrSql)
        If DsClase.Rows.Count > 0 Then
            With DsClase.Rows(0)
                _idTipoServicio = .Item("idTipoServicio")
                _NomServicio = .Item("NomServicio") & ""
                _Descripcion = .Item("Descripcion")
                _CadaKms = .Item("CadaKms")
                _CadaTiempoDias = .Item("CadaTiempoDias")
                _Estatus = .Item("Estatus")
                _Costo = .Item("Costo")
                _idDivision = .Item("idDivision")
                _NomDivision = .Item("NomDivision")
            End With
            _Existe = True
        Else
            _Existe = False
        End If

    End Sub
    Sub New(ByVal vidServicio As Integer, ByVal vidTipoServicio As Integer, ByVal vNomServicio As String, ByVal vDescripcion As String, _
            ByVal vCadaKms As Double, ByVal vCadaTiempoDias As Double, ByVal vEstatus As String, ByVal vCosto As Double,
            ByVal idDivision As Integer, ByVal NomDivision As String)
        _idServicio = vidServicio
        _idTipoServicio = vidTipoServicio
        _NomServicio = vNomServicio
        _Descripcion = vDescripcion
        _CadaKms = vCadaKms
        _CadaTiempoDias = vCadaTiempoDias
        _Estatus = vEstatus
        _Costo = vCosto
        _idDivision = idDivision
        _NomDivision = NomDivision
        _Existe = True
    End Sub
#End Region
#Region "Propiedades"
    'Public ReadOnly Property idDivision As Integer
    '    Get
    '        Return _idTipoServicio
    '    End Get
    'End Property
    Public Property idServicio As Integer
        Get
            Return _idServicio
        End Get
        Set(ByVal value As Integer)
            _idServicio = value
        End Set
    End Property
    Public Property idTipoServicio As Integer
        Get
            Return _idTipoServicio
        End Get
        Set(ByVal value As Integer)
            _idTipoServicio = value
        End Set
    End Property
    Public Property NomServicio As String
        Get
            Return _NomServicio
        End Get
        Set(ByVal value As String)
            _NomServicio = value
        End Set
    End Property
    Public Property Descripcion As String
        Get
            Return _Descripcion
        End Get
        Set(ByVal value As String)
            _Descripcion = value
        End Set
    End Property
    Public Property CadaKms As Double
        Get
            Return _CadaKms
        End Get
        Set(ByVal value As Double)
            _CadaKms = value
        End Set
    End Property
    Public Property CadaTiempoDias As Double
        Get
            Return _CadaTiempoDias
        End Get
        Set(ByVal value As Double)
            _CadaTiempoDias = value
        End Set
    End Property
    Public Property Estatus As String
        Get
            Return _Estatus
        End Get
        Set(ByVal value As String)
            _Estatus = value
        End Set
    End Property
    Public Property Costo As Double
        Get
            Return _Costo
        End Get
        Set(ByVal value As Double)
            _Costo = value
        End Set
    End Property
    Public Property idDivision As Integer
        Get
            Return _idDivision
        End Get
        Set(ByVal value As Integer)
            _idDivision = value
        End Set
    End Property
    Public Property NomDivision As String
        Get
            Return _NomDivision
        End Get
        Set(ByVal value As String)
            _NomDivision = value
        End Set
    End Property
    'Public ReadOnly Property Existe As Boolean
    '    Get
    '        Return _Existe
    '    End Get
    'End Property
    Public Property Existe As Boolean
        Get
            Return _Existe
        End Get
        Set(ByVal value As Boolean)
            _Existe = value
        End Set
    End Property

#End Region
#Region "Funciones"
    Public Function GetCambios(ByVal vidTipoServicio As Integer, ByVal vNomServicio As String, ByVal vDescripcion As String, _
            ByVal vCadaKms As Double, ByVal vCadaTiempoDias As Double, ByVal vEstatus As String, ByVal vCosto As Double,
            ByVal vidDivision As Integer) As String
        Dim CadCam As String = ""
        If _idTipoServicio <> vidTipoServicio Then
            CadCam += "idTipoServicio: '" & _idTipoServicio & "' Cambia a [" & vidTipoServicio & "],"
        End If
        If _NomServicio <> vNomServicio Then
            CadCam += "NomServicio: '" & _NomServicio & "' Cambia a [" & vNomServicio & "],"
        End If
        If _Descripcion <> vDescripcion Then
            CadCam += "Descripcion: '" & _Descripcion & "' Cambia a [" & vDescripcion & "],"
        End If
        If _CadaKms <> vCadaKms Then
            CadCam += "CadaKms: '" & _CadaKms & "' Cambia a [" & vCadaKms & "],"
        End If
        If _CadaTiempoDias <> vCadaTiempoDias Then
            CadCam += "CadaTiempoDias: '" & _CadaTiempoDias & "' Cambia a [" & vCadaTiempoDias & "],"
        End If
        If _Estatus <> vEstatus Then
            CadCam += "Estatus: '" & _Estatus & "' Cambia a [" & vEstatus & "],"
        End If
        If _Costo <> vCosto Then
            CadCam += "Costo: '" & _Costo & "' Cambia a [" & vCosto & "],"
        End If
        If _idDivision <> vidDivision Then
            CadCam += "Division: '" & _idDivision & "' Cambia a [" & vidDivision & "],"
        End If

        If CadCam <> "" Then
            CadCam = Mid(CadCam, 1, CadCam.Length - 1)
        End If
        Return CadCam
    End Function
    Public Sub Guardar(ByVal bEsIdentidad As Boolean, ByVal vidTipoServicio As Integer, ByVal vNomServicio As String, ByVal vDescripcion As String, _
            ByVal vCadaKms As Double, ByVal vCadaTiempoDias As Double, ByVal vEstatus As String, ByVal vCosto As Double, ByVal vidDivision As Integer)
        Dim sSql As String = "", _CadValAnt As String = "", _CadValNue As String = ""
        If _Existe Then
            If _idTipoServicio <> vidTipoServicio Then
                sSql += "idTipoServicio = " & vidTipoServicio & ","
                _CadValAnt += "idTipoServicio =" & vidTipoServicio & ","
            End If
            If _NomServicio <> vNomServicio Then
                sSql += "NomServicio = '" & vNomServicio & "',"
                _CadValAnt += "NomServicio =" & vNomServicio & ","
            End If
            If _Descripcion <> vDescripcion Then
                sSql += "Descripcion = '" & vDescripcion & "',"
                _CadValAnt += "Descripcion =" & vDescripcion & ","
            End If
            If _CadaKms <> vCadaKms Then
                sSql += "CadaKms = " & vCadaKms & ","
                _CadValAnt += "CadaKms =" & vCadaKms & ","
            End If

            If _CadaTiempoDias <> vCadaTiempoDias Then
                sSql += "CadaTiempoDias = " & vCadaTiempoDias & ","
                _CadValAnt += "CadaTiempoDias =" & vCadaTiempoDias & ","
            End If
            If _Estatus <> vEstatus Then
                sSql += "Estatus = '" & vEstatus & "',"
                _CadValAnt += "Estatus =" & vEstatus & ","
            End If
            If _Costo <> vCosto Then
                sSql += "Costo = " & vCosto & ","
                _CadValAnt += "Costo =" & vCosto & ","
            End If
            If _idDivision <> vidDivision Then
                sSql += "idDivision = " & vidDivision & ","
                _CadValAnt += "idDivision =" & vidDivision & ","
            End If

            If sSql <> "" Then
                sSql = Mid(sSql, 1, sSql.Length - 1) 'Para quitar la Coma ','
                _CadValAnt = "idServicio=" & _idServicio & "," & _CadValAnt
                _CadValAnt = Mid(_CadValAnt, 1, _CadValAnt.Length - 1)
                _CadValNue = Replace(sSql, "'", "")

                sSql = "UPDATE " & _TablaBd & " SET " & sSql & " WHERE idServicio = '" & _idServicio & "'"
                Try
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                    BD.Execute(sSql, True)
                    Audita(_TablaBd, _CadValAnt, _CadValNue, EnTipAccion.Modifica)
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
                Catch ex As Exception
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                    Throw New Exception(ex.Message)
                End Try
            End If
        Else
            If bEsIdentidad Then
                sSql = "INSERT INTO " & _TablaBd & "(idTipoServicio,NomServicio,Descripcion,CadaKms,CadaTiempoDias,Estatus,Costo,idDivision) " & _
                "VALUES(" & vidTipoServicio & ",'" & vNomServicio & "','" & vDescripcion & "'," & vCadaKms & "," & vCadaTiempoDias & _
                ",'" & vEstatus & "'," & vCosto & "," & vidDivision & ")"

                _CadValNue = "idTipoServicio=" & vidTipoServicio & "NomServicio = " & vNomServicio & "Descripcion = " & vDescripcion & _
                "CadaKms = " & vCadaKms & "CadaTiempoDias = " & vCadaTiempoDias & "Estatus = " & vEstatus & "Costo = " & vCosto & "idDivision = " & vidDivision

            Else
                sSql = "INSERT INTO " & _TablaBd & "(idServicio,idTipoServicio,NomServicio,Descripcion,CadaKms,CadaTiempoDias,Estatus,Costo,idDivision) " & _
                "VALUES(" & _idServicio & "," & vidTipoServicio & ",'" & vNomServicio & "','" & vDescripcion & "'," & vCadaKms & "," & vCadaTiempoDias & _
                ",'" & vEstatus & "'," & vCosto & "," & vidDivision & ")"

                _CadValNue = "idServicio=" & _idServicio & "idTipoServicio=" & vidTipoServicio & "NomServicio = " & vNomServicio & "Descripcion = " & vDescripcion & _
                "CadaKms = " & vCadaKms & "CadaTiempoDias = " & vCadaTiempoDias & "Estatus = " & vEstatus & "Costo = " & vCosto & "idDivision = " & vidDivision
            End If

            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, "", _CadValNue, EnTipAccion.Inserta)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
        End If
        _idTipoServicio = vidTipoServicio
        _NomServicio = vNomServicio
        _Descripcion = vDescripcion
        _CadaKms = vCadaKms
        _CadaTiempoDias = vCadaTiempoDias
        _Estatus = vEstatus
        _Costo = vCosto
    End Sub
    Public Sub Eliminar()
        If _Existe Then
            Dim sSql As String = "", _CadValAnt As String = ""
            _CadValAnt = "idServicio=" & _idServicio & "idTipoServicio=" & _idTipoServicio & "NomServicio = " & _NomServicio & _
                "Descripcion = " & _Descripcion & "CadaKms = " & _CadaKms & "CadaTiempoDias = " & _CadaTiempoDias & _
                "Estatus = " & _Estatus & "Costo = " & _Costo

            sSql = "DELETE FROM " & _TablaBd & " WHERE idServicio = " & _idServicio
            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, _CadValAnt, "", EnTipAccion.Elimina)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
            _Existe = False
        Else
            'se supone que nunca debe entrar aqui. . .
            MessageBox.Show("El Servicio con Id: " & _idServicio & " No existe por lo tanto no se puede Eliminar!!!",
                            "No se puede eliminar el Servicio porque no Existe", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub
#End Region
#Region "Consultas"
    Public Function TablaRelServAct(ByVal FiltroSinWhere As String) As DataTable
        MyTabla.Rows.Clear()

        StrSql = "SELECT rel.idServicio, " &
        "ser.idTipoServicio, " &
        "ser.NomServicio, " &
        "ser.Costo, " &
        "act.NombreAct, " &
        "act.CostoManoObra, " &
        "act.DuracionHoras, " &
        "rel.idActividad, " &
        "fam.idDivision," &
        "div.NomDivision " &
        "FROM dbo.RelServicioActividad rel " &
        "INNER JOIN dbo.CatServicios ser ON rel.idServicio = ser.idServicio " &
        "INNER JOIN dbo.CatActividades act ON rel.idActividad = act.idActividad " &
        "INNER JOIN dbo.CatFamilia fam ON act.idFamilia = fam.idFamilia " &
        "INNER JOIN dbo.CatDivision div ON fam.idDivision = div.idDivision " &
        IIf(FiltroSinWhere = "", "", " where " & FiltroSinWhere)

        '"WHERE rel.idServicio = 1"

        MyTabla = BD.ExecuteReturn(StrSql)

        Return MyTabla

    End Function
    Public Function TablaServicios(ByVal FiltroSinWhere As String) As DataTable
        'Dim Filtro As String = ""
        MyTabla.Rows.Clear()

        StrSql = "SELECT ser.idServicio, " &
        "ser.idTipoServicio, " &
        "ser.NomServicio, " &
        "ser.Descripcion, " &
        "ser.CadaKms, " &
        "ser.CadaTiempoDias, " &
        "ser.Estatus, " &
        "ser.Costo, " &
        "ser.idDivision, " &
        "div.NomDivision " &
        "FROM dbo.CatServicios ser " &
        "LEFT JOIN CatDivision DIV ON SER.IDDIVISION = DIV.IDDIVISION " &
        IIf(FiltroSinWhere = "", "", " where " & FiltroSinWhere)
        '"WHERE ser.idTipoServicio = 1"

        MyTabla = BD.ExecuteReturn(StrSql)

        Return MyTabla

    End Function
    Public Function TablaServicio_RelTipOrdServ(ByVal idTipOrdServ As Integer) As DataTable
        MyTabla.Rows.Clear()

        StrSql = "SELECT * FROM dbo.CatServicios S " &
        "INNER JOIN RelTipOrdServ R ON s.idServicio = r.IdRelacion AND  r.NomCatalogo = 'CatServicios' " &
        "WHERE r.idTipOrdServ = " & idTipOrdServ


        MyTabla = BD.ExecuteReturn(StrSql)

        Return MyTabla

    End Function
#End Region
End Class
