﻿Public Class TipoHerramClass
    'idTipoHerramienta
    'NomTipoHerramienta
    Private _idTipoHerramienta As Integer
    Private _NomTipoHerramienta As String

    Private _Existe As Boolean
    Private _TablaBd As String = "CatTipoHerramienta"

    Dim StrSql As String
    Dim DsClase As New DataTable

#Region "Constructor"
    Sub New(ByVal vidTipoHerramienta As Integer)
        'Dim DtEst As New DataTable
        _idTipoHerramienta = vidTipoHerramienta
        StrSql = "select idTipoHerramienta,NomTipoHerramienta from " & _TablaBd & " where idTipoHerramienta = " & vidTipoHerramienta

        DsClase = BD.ExecuteReturn(StrSql)
        If DsClase.Rows.Count > 0 Then
            With DsClase.Rows(0)
                _NomTipoHerramienta = .Item("NomTipoHerramienta") & ""
            End With
            _Existe = True
        Else
            _Existe = False
        End If

    End Sub
    Sub New(ByVal vidTipoHerramienta As Integer, ByVal vNomTipoHerramienta As String)
        _idTipoHerramienta = vidTipoHerramienta
        _NomTipoHerramienta = vNomTipoHerramienta
        _Existe = True
    End Sub
#End Region

#Region "Propiedades"
    'Public ReadOnly Property idDivision As Integer
    '    Get
    '        Return _idDivision
    '    End Get
    'End Property
    Public Property idTipoHerramienta As Integer
        Get
            Return _idTipoHerramienta
        End Get
        Set(ByVal value As Integer)
            _idTipoHerramienta = value
        End Set
    End Property
    Public Property NomTipoHerramienta As String
        Get
            Return _NomTipoHerramienta
        End Get
        Set(ByVal value As String)
            _NomTipoHerramienta = value
        End Set
    End Property
    'Public ReadOnly Property Existe As Boolean
    '    Get
    '        Return _Existe
    '    End Get
    'End Property
    Public Property Existe As Boolean
        Get
            Return _Existe
        End Get
        Set(ByVal value As Boolean)
            _Existe = value
        End Set
    End Property

#End Region

#Region "Funciones"
    Public Function GetCambios(ByVal vNomTipoHerramienta As String) As String
        Dim CadCam As String = ""
        If _NomTipoHerramienta <> vNomTipoHerramienta Then
            CadCam += "Nombre Tipo Herramienta: '" & _NomTipoHerramienta & "' Cambia a [" & vNomTipoHerramienta & "],"
        End If
        If CadCam <> "" Then
            CadCam = Mid(CadCam, 1, CadCam.Length - 1)
        End If
        Return CadCam
    End Function
    Public Sub Guardar(ByVal vNomTipoHerramienta As String, ByVal bEsIdentidad As Boolean)
        Dim sSql As String = "", _CadValAnt As String = "", _CadValNue As String = ""
        If _Existe Then
            If _NomTipoHerramienta <> vNomTipoHerramienta Then
                sSql += "NomTipoHerramienta = '" & vNomTipoHerramienta & "',"
                _CadValAnt += "NomTipoHerramienta =" & vNomTipoHerramienta & ","
            End If

            If sSql <> "" Then
                sSql = Mid(sSql, 1, sSql.Length - 1) 'Para quitar la Coma ','
                _CadValAnt = "idTipoHerramienta=" & _idTipoHerramienta & "," & _CadValAnt
                _CadValAnt = Mid(_CadValAnt, 1, _CadValAnt.Length - 1)
                _CadValNue = Replace(sSql, "'", "")
                sSql = "UPDATE " & _TablaBd & " SET " & sSql & " WHERE idTipoHerramienta = '" & _idTipoHerramienta & "'"
                Try
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                    BD.Execute(sSql, True)
                    Audita(_TablaBd, _CadValAnt, _CadValNue, EnTipAccion.Modifica)
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
                Catch ex As Exception
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                    Throw New Exception(ex.Message)
                End Try
            End If
        Else
            If bEsIdentidad Then
                sSql = "INSERT INTO " & _TablaBd & "(NomTipoHerramienta) VALUES('" & _NomTipoHerramienta & "')"

                _CadValNue = "NomTipoHerramienta=" & _NomTipoHerramienta
            Else

                sSql = "INSERT INTO " & _TablaBd & "(idTipoHerramienta,NomTipoHerramienta) VALUES(" &
                "'" & _idTipoHerramienta & "','" & _NomTipoHerramienta & "')"

                _CadValNue = "idTipoHerramienta=" & _idTipoHerramienta & ",NomTipoHerramienta=" & _NomTipoHerramienta
            End If

            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, "", _CadValNue, EnTipAccion.Inserta)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
        End If
        _NomTipoHerramienta = vNomTipoHerramienta
    End Sub
    Public Sub Eliminar()
        If _Existe Then
            Dim sSql As String = "", _CadValAnt As String = ""
            _CadValAnt = "idTipoHerramienta=" & _idTipoHerramienta & " , NomTipoHerramienta =" & _NomTipoHerramienta
            sSql = "DELETE FROM " & _TablaBd & " WHERE idTipoHerramienta = '" & _idTipoHerramienta & "'"
            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, _CadValAnt, "", EnTipAccion.Elimina)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
            _Existe = False
        Else
            'se supone que nunca debe entrar aqui. . .
            MessageBox.Show("El tipo de Herramienta con Id: " & _idTipoHerramienta & " No existe por lo tanto no se puede Eliminar!!!",
                            "No se puede eliminar el tipo de Herramienta porque no Existe", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub
#End Region

End Class
