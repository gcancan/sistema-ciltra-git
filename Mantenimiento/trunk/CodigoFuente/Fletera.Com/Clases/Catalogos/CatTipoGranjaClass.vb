﻿Public Class CatTipoGranjaClass
    Private _idTipoGranja As Integer
    Private _Descripcion As String

    Private _Existe As Boolean
    Private _TablaBd As String = "CatTipoGranja"

    Dim StrSql As String
    Dim DsClase As New DataTable
    Dim MyTabla As DataTable = New DataTable(_TablaBd)

#Region "Constructor"
    Sub New(ByVal vidTipoGranja As Integer)
        'Dim DtEst As New DataTable
        _idTipoGranja = vidTipoGranja
        StrSql = "select idTipoGranja,Descripcion from " & _TablaBd & " where idTipoGranja = " & vidTipoGranja

        DsClase = BD.ExecuteReturn(StrSql)
        If DsClase.Rows.Count > 0 Then
            With DsClase.Rows(0)
                _Descripcion = .Item("Descripcion") & ""
            End With
            _Existe = True
        Else
            _Existe = False
        End If

    End Sub
    Sub New(ByVal vidTipoGranja As Integer, ByVal vDescripcion As String)
        _idTipoGranja = vidTipoGranja
        _Descripcion = vDescripcion
        _Existe = True
    End Sub
#End Region
#Region "Propiedades"
    'Public ReadOnly Property idDivision As Integer
    '    Get
    '        Return _idDivision
    '    End Get
    'End Property
    Public Property idTipoGranja As Integer
        Get
            Return _idTipoGranja
        End Get
        Set(ByVal value As Integer)
            _idTipoGranja = value
        End Set
    End Property
    Public Property Descripcion As String
        Get
            Return _Descripcion
        End Get
        Set(ByVal value As String)
            _Descripcion = value
        End Set
    End Property
    'Public ReadOnly Property Existe As Boolean
    '    Get
    '        Return _Existe
    '    End Get
    'End Property
    Public Property Existe As Boolean
        Get
            Return _Existe
        End Get
        Set(ByVal value As Boolean)
            _Existe = value
        End Set
    End Property

#End Region
#Region "Funciones"
    Public Function GetCambios(ByVal vDescripcion As String) As String
        Dim CadCam As String = ""
        If _Descripcion <> vDescripcion Then
            CadCam += "Descripcion: '" & _Descripcion & "' Cambia a [" & vDescripcion & "],"
        End If
        If CadCam <> "" Then
            CadCam = Mid(CadCam, 1, CadCam.Length - 1)
        End If
        Return CadCam
    End Function
    Public Sub Guardar(ByVal vDescripcion As String, ByVal bEsIdentidad As Boolean)
        Dim sSql As String = "", _CadValAnt As String = "", _CadValNue As String = ""
        If _Existe Then
            If _Descripcion <> vDescripcion Then
                sSql += "Descripcion = '" & vDescripcion & "',"
                _CadValAnt += "Descripcion =" & vDescripcion & ","
            End If

            If sSql <> "" Then
                sSql = Mid(sSql, 1, sSql.Length - 1) 'Para quitar la Coma ','
                _CadValAnt = "idTipoGranja=" & _idTipoGranja & "," & _CadValAnt
                _CadValAnt = Mid(_CadValAnt, 1, _CadValAnt.Length - 1)
                _CadValNue = Replace(sSql, "'", "")
                sSql = "UPDATE " & _TablaBd & " SET " & sSql & " WHERE idTipoGranja = '" & _idTipoGranja & "'"
                Try
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                    BD.Execute(sSql, True)
                    Audita(_TablaBd, _CadValAnt, _CadValNue, EnTipAccion.Modifica)
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
                Catch ex As Exception
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                    Throw New Exception(ex.Message)
                End Try
            End If
        Else
            If bEsIdentidad Then
                sSql = "INSERT INTO " & _TablaBd & "(Descripcion) VALUES('" & _Descripcion & "')"

                _CadValNue = "Descripcion=" & _Descripcion
            Else

                sSql = "INSERT INTO " & _TablaBd & "(idTipoGranja,Descripcion) VALUES(" &
                "'" & _idTipoGranja & "','" & _Descripcion & "')"

                _CadValNue = "idTipoGranja=" & _idTipoGranja & ",Descripcion=" & _Descripcion
            End If

            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, "", _CadValNue, EnTipAccion.Inserta)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
        End If
        _Descripcion = vDescripcion
    End Sub
    Public Sub Eliminar()
        If _Existe Then
            Dim sSql As String = "", _CadValAnt As String = ""
            _CadValAnt = "idDivision=" & _idTipoGranja & " , NomDivision =" & _Descripcion
            sSql = "DELETE FROM " & _TablaBd & " WHERE idDivision = '" & _idTipoGranja & "'"
            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, _CadValAnt, "", EnTipAccion.Elimina)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
            _Existe = False
        Else
            'se supone que nunca debe entrar aqui. . .
            MessageBox.Show("El Tipo de Granja con Id: " & _idTipoGranja & " No existe por lo tanto no se puede Eliminar!!!",
                            "No se puede eliminar El Tipo de Granja porque no Existe", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub
#End Region
#Region "Consultas"

    Public Function TablaTiposGRanjas(ByVal bIncluyeTodos As Boolean, Optional ByVal FiltroSinWhere As String = "") As DataTable
        Dim filtro As String = ""
        MyTabla.Rows.Clear()

        If FiltroSinWhere <> "" Then
            filtro = " where " & FiltroSinWhere
        End If

        If bIncluyeTodos Then
            StrSql = "SELECT 0 AS idTipoGranja,'TODOS' AS Descripcion " &
            "UNION " &
            "SELECT idTipoGranja,Descripcion FROM dbo.CatTipoGranja " & filtro &
            " ORDER BY idTipoGranja"
        Else
            StrSql = "SELECT idTipoGranja,Descripcion FROM dbo.CatTipoGranja " & filtro &
            " ORDER BY idTipoGranja"
        End If
        MyTabla = BD.ExecuteReturn(StrSql)

        Return MyTabla

    End Function
#End Region
End Class
