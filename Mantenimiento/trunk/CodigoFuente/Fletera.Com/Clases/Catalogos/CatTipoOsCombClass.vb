﻿Public Class CatTipoOsCombClass
    Private _idTipoOSComb As Integer
    Private _NomTipoOSComb As String
    Private _RequiereViaje As Boolean
    Private _TipoUnidad As String
    'Private _EntSal As Boolean
    'Private _VCExtra As Boolean



    Private _Existe As Boolean
    Private _TablaBd As String = "CatTipoOsComb"
    Dim StrSql As String
    Dim DsClase As New DataTable
    Dim MyTabla As DataTable = New DataTable(_TablaBd)

#Region "Constructor"
    Sub New(ByVal vidTipoOSComb As Integer)
        _idTipoOSComb = vidTipoOSComb

        StrSql = "select idTipoOSComb,NomTipoOSComb,isnull(RequiereViaje,0) as RequiereViaje," &
        " isnull(TipoUnidad,'') as TipoUnidad " &
        "  from " & _TablaBd & " where idTipoOSComb = " & vidTipoOSComb

        DsClase = BD.ExecuteReturn(StrSql)
        If DsClase.Rows.Count > 0 Then
            With DsClase.Rows(0)
                _NomTipoOSComb = .Item("NomTipoOSComb") & ""
                _RequiereViaje = Convert.ToBoolean(.Item("RequiereViaje"))
                _TipoUnidad = .Item("TipoUnidad") & ""
                '_EntSal = Convert.ToBoolean(.Item("EntSal"))
                '_VCExtra = Convert.ToBoolean(.Item("VCExtra"))
            End With
            _Existe = True
        Else
            _Existe = False
        End If
    End Sub

    Sub New(ByVal vidTipoOSComb As Integer, ByVal vNomTipoOSComb As String, ByVal vRequiereViaje As Boolean,
            ByVal vTipoUnidad As String)
        _idTipoOSComb = vidTipoOSComb
        _NomTipoOSComb = vNomTipoOSComb
        _RequiereViaje = vRequiereViaje
        _TipoUnidad = vTipoUnidad
        '_EntSal = vEntSal
        '_VCExtra = vVCExtra
        _Existe = True
    End Sub
#End Region

#Region "Propiedades"
    Public Property idTipoOSComb As Integer
        Get
            Return _idTipoOSComb
        End Get
        Set(ByVal value As Integer)
            _idTipoOSComb = value
        End Set
    End Property
    Public Property NomTipoOSComb As String
        Get
            Return _NomTipoOSComb
        End Get
        Set(ByVal value As String)
            _NomTipoOSComb = value
        End Set
    End Property
    Public Property RequiereViaje As Boolean
        Get
            Return _RequiereViaje
        End Get
        Set(ByVal value As Boolean)
            _RequiereViaje = value
        End Set
    End Property
    Public Property Existe As Boolean
        Get
            Return _Existe
        End Get
        Set(ByVal value As Boolean)
            _Existe = value
        End Set
    End Property
    Public Property TipoUnidad As String
        Get
            Return _TipoUnidad
        End Get
        Set(ByVal value As String)
            _TipoUnidad = value
        End Set
    End Property

    'Public Property EntSal As Boolean
    '    Get
    '        Return _EntSal
    '    End Get
    '    Set(ByVal value As Boolean)
    '        _EntSal = value
    '    End Set
    'End Property
    'Public Property VCExtra As Boolean
    '    Get
    '        Return _VCExtra
    '    End Get
    '    Set(ByVal value As Boolean)
    '        _VCExtra = value
    '    End Set
    'End Property
#End Region

#Region "Funciones"

    Public Function GetCambios(ByVal vNomTipoOSComb As Integer, ByVal vRequiereViaje As String, ByVal vTipoUnidad As DateTime) As String
        Dim CadCam As String = ""
        If _NomTipoOSComb <> vNomTipoOSComb Then
            CadCam += "NomTipoOSComb: '" & _NomTipoOSComb & "' Cambia a [" & vNomTipoOSComb & "],"
        End If
        If _RequiereViaje <> vRequiereViaje Then
            CadCam += "RequiereViaje: '" & _RequiereViaje & "' Cambia a [" & vRequiereViaje & "],"
        End If
        If _TipoUnidad <> vTipoUnidad Then
            CadCam += "TipoUnidad: '" & _TipoUnidad & "' Cambia a [" & vTipoUnidad & "],"
        End If
        If CadCam <> "" Then
            CadCam = Mid(CadCam, 1, CadCam.Length - 1)
        End If
        Return CadCam
    End Function

    Public Sub Guardar(ByVal bEsIdentidad As Boolean, ByVal vNomTipoOSComb As Integer, ByVal vRequiereViaje As String, ByVal vTipoUnidad As DateTime)
        Dim sSql As String = "", _CadValAnt As String = "", _CadValNue As String = ""
        If _Existe Then
            If _NomTipoOSComb <> vNomTipoOSComb Then
                sSql += "NomTipoOSComb = '" & vNomTipoOSComb & "',"
                _CadValAnt += "NomTipoOSComb =" & vNomTipoOSComb & ","
            End If
            If _RequiereViaje <> vRequiereViaje Then
                sSql += "RequiereViaje = " & IIf(vRequiereViaje, 1, 0) & ","
                _CadValAnt += "RequiereViaje =" & IIf(vRequiereViaje, 1, 0) & ","
            End If
            If _TipoUnidad <> vTipoUnidad Then
                sSql += "TipoUnidad = " & vTipoUnidad & ","
                _CadValAnt += "TipoUnidad =" & vTipoUnidad & ","
            End If

            If sSql <> "" Then
                sSql = Mid(sSql, 1, sSql.Length - 1) 'Para quitar la Coma ','
                _CadValAnt = "idTipoOSComb=" & _idTipoOSComb & "," & _CadValAnt
                _CadValAnt = Mid(_CadValAnt, 1, _CadValAnt.Length - 1)
                _CadValNue = Replace(sSql, "'", "")

                sSql = "UPDATE " & _TablaBd & " SET " & sSql & " WHERE idTipoOSComb = " & _idTipoOSComb
                Try
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                    BD.Execute(sSql, True)
                    Audita(_TablaBd, _CadValAnt, _CadValNue, EnTipAccion.Modifica)
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
                Catch ex As Exception
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                    Throw New Exception(ex.Message)
                End Try
            End If
        Else
            If bEsIdentidad Then
                sSql = "INSERT INTO " & _TablaBd & "(NomTipoOSComb,RequiereViaje,TipoUnidad) " &
                "VALUES('" & vNomTipoOSComb & "'," & IIf(vRequiereViaje, 1, 0) & ",'" & vTipoUnidad & "')"

                _CadValNue = "NomTipoOSComb=" & vNomTipoOSComb & "RequiereViaje = " & vRequiereViaje & "TipoUnidad = " & vTipoUnidad
            Else
                sSql = "INSERT INTO " & _TablaBd & "(idTipoOSComb,NomTipoOSComb,RequiereViaje,TipoUnidad) " &
                "VALUES(" & _idTipoOSComb & ",'" & vNomTipoOSComb & "'," & IIf(vRequiereViaje, 1, 0) & ",'" & vTipoUnidad & "')"

                _CadValNue = "idTipoOSComb=" & _idTipoOSComb & "NomTipoOSComb=" & vNomTipoOSComb & "RequiereViaje = " & vRequiereViaje & "TipoUnidad = " & vTipoUnidad
            End If

            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, "", _CadValNue, EnTipAccion.Inserta)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
        End If
        _NomTipoOSComb = vNomTipoOSComb
        _RequiereViaje = vRequiereViaje
        _TipoUnidad = vTipoUnidad

    End Sub
    Public Sub Eliminar()
        If _Existe Then
            Dim sSql As String = "", _CadValAnt As String = ""
            _CadValAnt = "idTipoOSComb=" & _idTipoOSComb & " , NomTipoOSComb =" & _NomTipoOSComb & ", RequiereViaje = " & _RequiereViaje & ", TipoUnidad = " & _TipoUnidad
            sSql = "DELETE FROM " & _TablaBd & " WHERE idTipoOSComb = " & _idTipoOSComb
            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, _CadValAnt, "", EnTipAccion.Elimina)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
            _Existe = False
        Else
            'se supone que nunca debe entrar aqui. . .
            MessageBox.Show("El Tipo de Combustible extra con Id: " & _idTipoOSComb & " No existe por lo tanto no se puede Eliminar!!!",
                            "No se puede eliminar el tipo de combustible extra porque no Existe", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub
#End Region

#Region "Consultas"
    Public Function TablaTiposComb(ByVal FiltroSinWhere As String) As DataTable
        'Dim Filtro As String = ""
        MyTabla.Rows.Clear()

        StrSql = "SELECT IDTIPOOSCOMB,NOMTIPOOSCOMB,isnull(RequiereViaje,0) as RequiereViaje, " &
            " isnull(TipoUnidad,'') as TipoUnidad FROM CATTIPOOSCOMB " & IIf(FiltroSinWhere = "", "", " WHERE " & FiltroSinWhere) &
            " ORDER BY NOMTIPOOSCOMB "


        '"WHERE idDivision > 0 " &

        MyTabla = BD.ExecuteReturn(StrSql)

        Return MyTabla

    End Function
#End Region
End Class
