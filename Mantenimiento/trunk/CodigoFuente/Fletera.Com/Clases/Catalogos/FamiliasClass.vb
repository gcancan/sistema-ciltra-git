﻿Public Class FamiliasClass
    '    idFamilia
    'NomFamilia
    'idDivision
    'NomDivision
    Private _idFamilia As Integer
    Private _NomFamilia As String
    Private _idDivision As Integer
    Private _NomDivision As String

    Private _Existe As Boolean
    Private _TablaBd As String = "CatFamilia"

    Dim StrSql As String
    Dim DsClase As New DataTable
#Region "Constructor"
    Sub New(ByVal vidFamilia As Integer)
        'Dim DtEst As New DataTable
        _idFamilia = vidFamilia

        StrSql = "SELECT FAM.IDFAMILIA, FAM.NOMFAMILIA, FAM.IDDIVISION, DIV.NOMDIVISION " & _
        "FROM " & _TablaBd & " FAM " & _
        "INNER JOIN CATDIVISION DIV ON FAM.IDDIVISION = DIV.IDDIVISION " & _
        "WHERE FAM.IDFAMILIA = " & vidFamilia


        DsClase = BD.ExecuteReturn(StrSql)
        If DsClase.Rows.Count > 0 Then
            With DsClase.Rows(0)
                _NomFamilia = .Item("NOMFAMILIA") & ""
                _idDivision = .Item("IDDIVISION")
                _NomDivision = .Item("NOMDIVISION")
            End With
            _Existe = True
        Else
            _Existe = False
        End If

    End Sub
    Sub New(ByVal vidFamilia As Integer, ByVal vNomFamilia As String, ByVal vidDivision As Integer, ByVal vNomDivision As String)
        _idFamilia = vidFamilia
        _NomFamilia = vNomFamilia
        _idDivision = vidDivision
        _NomDivision = vNomDivision
        _Existe = True
    End Sub
#End Region
#Region "Propiedades"
    'Public ReadOnly Property idDivision As Integer
    '    Get
    '        Return _idDivision
    '    End Get
    'End Property
    Public Property IdFamilia As Integer
        Get
            Return _idFamilia
        End Get
        Set(ByVal value As Integer)
            _idFamilia = value
        End Set
    End Property
    Public Property NomFamilia As String
        Get
            Return _NomFamilia
        End Get
        Set(ByVal value As String)
            _NomFamilia = value
        End Set
    End Property
    Public Property idDivision As Integer
        Get
            Return _idDivision
        End Get
        Set(ByVal value As Integer)
            _idDivision = value
        End Set
    End Property
    Public Property NomDivision As String
        Get
            Return _NomDivision
        End Get
        Set(ByVal value As String)
            _NomDivision = value
        End Set
    End Property
    'Public ReadOnly Property Existe As Boolean
    '    Get
    '        Return _Existe
    '    End Get
    'End Property
    Public Property Existe As Boolean
        Get
            Return _Existe
        End Get
        Set(ByVal value As Boolean)
            _Existe = value
        End Set
    End Property

#End Region
#Region "Funciones"
    Public Function GetCambios(ByVal vNomFamilia As String, ByVal vidDivision As Integer) As String
        Dim CadCam As String = ""
        If _NomFamilia <> vNomFamilia Then
            CadCam += "Nombre Familia: '" & _NomFamilia & "' Cambia a [" & vNomFamilia & "],"
        End If
        If _idDivision <> vidDivision Then
            CadCam += "id División: '" & _idDivision & "' Cambia a [" & vidDivision & "],"
        End If

        If CadCam <> "" Then
            CadCam = Mid(CadCam, 1, CadCam.Length - 1)
        End If
        Return CadCam
    End Function
    Public Sub Guardar(ByVal vNomFamilia As String, ByVal vidDivision As Integer, ByVal bEsIdentidad As Boolean, ByVal vNomDivision As String)
        Dim sSql As String = "", _CadValAnt As String = "", _CadValNue As String = ""
        If _Existe Then
            If _NomFamilia <> vNomFamilia Then
                sSql += "NomFamilia = '" & vNomFamilia & "',"
                _CadValAnt += "NomFamilia =" & vNomFamilia & ","
            End If
            If _idDivision <> vidDivision Then
                sSql += "idDivision = " & vidDivision & ","
                _CadValAnt += "idDivision =" & vidDivision & ","
            End If
            If sSql <> "" Then
                sSql = Mid(sSql, 1, sSql.Length - 1) 'Para quitar la Coma ','
                _CadValAnt = "idFamilia=" & _idFamilia & "," & _CadValAnt
                _CadValAnt = Mid(_CadValAnt, 1, _CadValAnt.Length - 1)
                _CadValNue = Replace(sSql, "'", "")

                sSql = "UPDATE " & _TablaBd & " SET " & sSql & " WHERE idFamilia = '" & _idFamilia & "'"
                Try
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                    BD.Execute(sSql, True)
                    Audita(_TablaBd, _CadValAnt, _CadValNue, EnTipAccion.Modifica)
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
                Catch ex As Exception
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                    Throw New Exception(ex.Message)
                End Try
            End If
        Else
            If bEsIdentidad Then
                sSql = "INSERT INTO " & _TablaBd & "(NomFamilia,idDivision) VALUES('" & vNomFamilia & "'," & vidDivision & ")"

                _CadValNue = "NomFamilia=" & vNomFamilia & "idDivision = " & vidDivision
            Else

                sSql = "INSERT INTO " & _TablaBd & "(idFamilia,NomFamilia,idDivision) VALUES(" &
                "'" & _idFamilia & "','" & vNomFamilia & "'," & vidDivision & ")"

                _CadValNue = "idFamilia=" & _idFamilia & ",NomFamilia=" & vNomFamilia & ",idDivision=" & vidDivision
            End If

            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, "", _CadValNue, EnTipAccion.Inserta)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
        End If
        _NomFamilia = vNomFamilia
        _idDivision = vidDivision
        _NomDivision = vNomDivision
    End Sub
    Public Sub Eliminar()
        If _Existe Then
            Dim sSql As String = "", _CadValAnt As String = ""
            _CadValAnt = "idFamilia=" & _idFamilia & " , NomFamilia =" & _NomFamilia
            sSql = "DELETE FROM " & _TablaBd & " WHERE idFamilia = '" & _idFamilia & "'"
            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, _CadValAnt, "", EnTipAccion.Elimina)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
            _Existe = False
        Else
            'se supone que nunca debe entrar aqui. . .
            MessageBox.Show("La Familia con Id: " & _idFamilia & " No existe por lo tanto no se puede Eliminar!!!",
                            "No se puede eliminar la Familia porque no Existe", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub
#End Region
End Class
