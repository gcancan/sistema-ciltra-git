﻿Public Class ClientesClass
    Private _IdCliente As Integer
    Private _idEmpresa As Integer
    Private _Nombre As String
    Private _Domicilio As String
    Private _RFC As String
    Private _Clave_impuesto As Integer
    Private _Activo As Boolean
    Private _idImpuestoFlete As Integer
    Private _clave_Fraccion As Integer
    Private _penalizacion As Decimal
    Private _estado As String
    Private _pais As String
    Private _municipio As String
    Private _colonia As String
    Private _cp As String
    Private _telefono As String
    Private _idColonia As Integer
    Private _CambioPrecio As Boolean
    Private _GeneraDoc As Boolean


    Private _Existe As Boolean
    Private _TablaBd As String = "CatClientes"

    Dim StrSql As String
    Dim DsClase As New DataTable

    Dim MyTabla As DataTable = New DataTable(_TablaBd)

#Region "Constructor"
    Sub New(ByVal vIdCliente As Integer, Optional ByVal vNombre As String = "")
        Dim Filtro As String = ""

        If vNombre <> "" Then
            Filtro = " where cli.Nombre = " & vIdCliente & "'"
        Else
            Filtro = " where cli.IdCliente = " & vIdCliente
        End If

        StrSql = "SELECT cli.IdCliente, " &
        "cli.idEmpresa, " &
        "cli.Nombre, " &
        "cli.Domicilio, " &
        "cli.RFC, " &
        "cli.Clave_impuesto, " &
        "cli.Activo, " &
        "cli.idImpuestoFlete, " &
        "cli.clave_Fraccion, " &
        "cli.penalizacion, " &
        "cli.estado, " &
        "cli.pais, " &
        "cli.municipio, " &
        "cli.colonia, " &
        "cli.cp, " &
        "cli.telefono, " &
        "isnull(cli.idColonia,0) as idColonia, " &
        "isnull(CambioPrecio,0) as CambioPrecio, " &
        "isnull(GeneraDoc,0) as GeneraDoc " &
        "FROM dbo." & _TablaBd & " cli " & Filtro

        _IdCliente = vIdCliente

        DsClase = BD.ExecuteReturn(StrSql)
        If DsClase.Rows.Count > 0 Then
            With DsClase.Rows(0)
                _idEmpresa = .Item("idEmpresa")
                _Nombre = .Item("Nombre")
                _Domicilio = .Item("Domicilio")
                _RFC = .Item("RFC")
                _Clave_impuesto = .Item("Clave_impuesto")
                _Activo = .Item("Activo")
                _idImpuestoFlete = .Item("idImpuestoFlete")
                _clave_Fraccion = .Item("clave_Fraccion")
                _penalizacion = .Item("penalizacion")
                _estado = .Item("estado")
                _pais = .Item("pais")
                _municipio = .Item("municipio")
                _colonia = .Item("colonia")
                _cp = .Item("cp")
                _telefono = .Item("telefono")
                _idColonia = .Item("idColonia")
                _CambioPrecio = .Item("CambioPrecio")
                _GeneraDoc = .Item("GeneraDoc")
            End With
            _Existe = True
        Else
            _Existe = False
        End If
    End Sub
    Sub New(ByVal vIdCliente As Integer, ByVal vidEmpresa As Integer, ByVal vNombre As String, ByVal vDomicilio As String, ByVal vRFC As String,
        ByVal vClave_impuesto As Integer, ByVal vActivo As Boolean, ByVal vidImpuestoFlete As Integer, ByVal vclave_Fraccion As Integer,
        ByVal vpenalizacion As Decimal, ByVal vestado As String, ByVal vpais As String, ByVal vmunicipio As String,
        ByVal vcolonia As String, ByVal vcp As String, ByVal vtelefono As String, ByVal vidColonia As Integer,
        ByVal vCambioPrecio As Boolean, ByVal vGeneraDoc As Boolean)
        _IdCliente = vIdCliente
        _idEmpresa = vidEmpresa
        _Nombre = vNombre
        _Domicilio = vDomicilio
        _RFC = vRFC
        _Clave_impuesto = vClave_impuesto
        _Activo = vActivo
        _idImpuestoFlete = vidImpuestoFlete
        _clave_Fraccion = vclave_Fraccion
        _penalizacion = vpenalizacion
        _estado = vestado
        _pais = vpais
        _municipio = vmunicipio
        _colonia = vcolonia
        _cp = vcp
        _telefono = vtelefono
        _idColonia = vidColonia
        _CambioPrecio = vCambioPrecio
        _GeneraDoc = vGeneraDoc

        _Existe = True
    End Sub
#End Region

#Region "Propiedades"
    Public Property IdCliente As Integer
        Get
            Return _IdCliente
        End Get
        Set(ByVal value As Integer)
            _IdCliente = value
        End Set
    End Property
    Public Property idEmpresa As Integer
        Get
            Return _idEmpresa
        End Get
        Set(ByVal value As Integer)
            _idEmpresa = value
        End Set
    End Property
    Public Property Nombre As String
        Get
            Return _Nombre
        End Get
        Set(ByVal value As String)
            _Nombre = value
        End Set
    End Property
    Public Property Domicilio As String
        Get
            Return _Domicilio
        End Get
        Set(ByVal value As String)
            _Domicilio = value
        End Set
    End Property
    Public Property RFC As String
        Get
            Return _RFC
        End Get
        Set(ByVal value As String)
            _RFC = value
        End Set
    End Property
    Public Property Clave_impuesto As Integer
        Get
            Return _Clave_impuesto
        End Get
        Set(ByVal value As Integer)
            _Clave_impuesto = value
        End Set
    End Property
    Public Property Activo As Boolean
        Get
            Return _Activo
        End Get
        Set(ByVal value As Boolean)
            _Activo = value
        End Set
    End Property
    Public Property idImpuestoFlete As Integer
        Get
            Return _idImpuestoFlete
        End Get
        Set(ByVal value As Integer)
            _idImpuestoFlete = value
        End Set
    End Property
    Public Property clave_Fraccion As Integer
        Get
            Return _clave_Fraccion
        End Get
        Set(ByVal value As Integer)
            _clave_Fraccion = value
        End Set
    End Property
    Public Property penalizacion As Decimal
        Get
            Return _penalizacion
        End Get
        Set(ByVal value As Decimal)
            _penalizacion = value
        End Set
    End Property
    Public Property estado As String
        Get
            Return _estado
        End Get
        Set(ByVal value As String)
            _estado = value
        End Set
    End Property
    Public Property pais As String
        Get
            Return _pais
        End Get
        Set(ByVal value As String)
            _pais = value
        End Set
    End Property
    Public Property municipio As String
        Get
            Return _municipio
        End Get
        Set(ByVal value As String)
            _municipio = value
        End Set
    End Property
    Public Property colonia As String
        Get
            Return _colonia
        End Get
        Set(ByVal value As String)
            _colonia = value
        End Set
    End Property
    Public Property cp As String
        Get
            Return _cp
        End Get
        Set(ByVal value As String)
            _cp = value
        End Set
    End Property
    Public Property telefono As String
        Get
            Return _telefono
        End Get
        Set(ByVal value As String)
            _telefono = value
        End Set
    End Property
    Public Property idColonia As Integer
        Get
            Return _idColonia
        End Get
        Set(ByVal value As Integer)
            _idColonia = value
        End Set
    End Property
    Public Property CambioPrecio As Boolean
        Get
            Return _CambioPrecio
        End Get
        Set(ByVal value As Boolean)
            _CambioPrecio = value
        End Set
    End Property

    Public Property GeneraDoc As Boolean
        Get
            Return _GeneraDoc
        End Get
        Set(ByVal value As Boolean)
            _GeneraDoc = value
        End Set
    End Property
    Public Property Existe As Boolean
        Get
            Return _Existe
        End Get
        Set(ByVal value As Boolean)
            _Existe = value
        End Set
    End Property
#End Region

#Region "Funciones"
    Public Function GetCambios(ByVal vidEmpresa As Integer, ByVal vNombre As String, ByVal vDomicilio As String, ByVal vRFC As String,
        ByVal vClave_impuesto As Integer, ByVal vActivo As Boolean, ByVal vidImpuestoFlete As Integer, ByVal vclave_Fraccion As Integer,
        ByVal vpenalizacion As Decimal, ByVal vestado As String, ByVal vpais As String, ByVal vmunicipio As String,
        ByVal vcolonia As String, ByVal vcp As String, ByVal vtelefono As String, ByVal vidColonia As Integer,
        ByVal vCambioPrecio As Boolean, ByVal vGeneraDoc As Boolean) As String
        Dim CadCam As String = ""

        If _idEmpresa <> vidEmpresa Then
            CadCam += "idEmpresa: '" & _idEmpresa & "' Cambia a [" & vidEmpresa & "],"
        End If
        If _Nombre <> vNombre Then
            CadCam += "Nombre: '" & _Nombre & "' Cambia a [" & vNombre & "],"
        End If
        If _Domicilio <> vDomicilio Then
            CadCam += "Domicilio: '" & _Domicilio & "' Cambia a [" & vDomicilio & "],"
        End If
        If _RFC <> vRFC Then
            CadCam += "RFC: '" & _RFC & "' Cambia a [" & vRFC & "],"
        End If
        If _Clave_impuesto <> vClave_impuesto Then
            CadCam += "Clave_impuesto: '" & _Clave_impuesto & "' Cambia a [" & vClave_impuesto & "],"
        End If
        If _Activo <> vActivo Then
            CadCam += "Activo: '" & _Activo & "' Cambia a [" & vActivo & "],"
        End If
        If _idImpuestoFlete <> vidImpuestoFlete Then
            CadCam += "idImpuestoFlete: '" & _idImpuestoFlete & "' Cambia a [" & vidImpuestoFlete & "],"
        End If
        If _clave_Fraccion <> vclave_Fraccion Then
            CadCam += "clave_Fraccion: '" & _clave_Fraccion & "' Cambia a [" & vclave_Fraccion & "],"
        End If
        If _penalizacion <> vpenalizacion Then
            CadCam += "penalizacion: '" & _penalizacion & "' Cambia a [" & vpenalizacion & "],"
        End If
        If _estado <> vestado Then
            CadCam += "estado: '" & _estado & "' Cambia a [" & vestado & "],"
        End If
        If _pais <> vpais Then
            CadCam += "pais: '" & _pais & "' Cambia a [" & vpais & "],"
        End If
        If _municipio <> vmunicipio Then
            CadCam += "municipio: '" & _municipio & "' Cambia a [" & vmunicipio & "],"
        End If
        If _colonia <> vcolonia Then
            CadCam += "colonia: '" & _colonia & "' Cambia a [" & vcolonia & "],"
        End If
        If _cp <> vcp Then
            CadCam += "cp: '" & _cp & "' Cambia a [" & vcp & "],"
        End If
        If _telefono <> vtelefono Then
            CadCam += "telefono: '" & _telefono & "' Cambia a [" & vtelefono & "],"
        End If
        If _idColonia <> vidColonia Then
            CadCam += "idColonia: '" & _idColonia & "' Cambia a [" & vidColonia & "],"
        End If
        If _CambioPrecio <> vCambioPrecio Then
            CadCam += "CambioPrecio: '" & _CambioPrecio & "' Cambia a [" & vCambioPrecio & "],"
        End If
        If _GeneraDoc <> vGeneraDoc Then
            CadCam += "GeneraDoc: '" & _GeneraDoc & "' Cambia a [" & vGeneraDoc & "],"
        End If

        If CadCam <> "" Then
            CadCam = Mid(CadCam, 1, CadCam.Length - 1)
        End If

        Return CadCam


    End Function

    Public Sub Guardar(ByVal bEsIdentidad As Boolean, ByVal vidEmpresa As Integer, ByVal vNombre As String, ByVal vDomicilio As String,
        ByVal vRFC As String, ByVal vClave_impuesto As Integer, ByVal vActivo As Boolean, ByVal vidImpuestoFlete As Integer,
        ByVal vclave_Fraccion As Integer, ByVal vpenalizacion As Decimal, ByVal vestado As String, ByVal vpais As String,
        ByVal vmunicipio As String, ByVal vcolonia As String, ByVal vcp As String, ByVal vtelefono As String,
        ByVal vidColonia As Integer, ByVal vCambioPrecio As Boolean, ByVal vGeneraDoc As Boolean)


        Dim sSql As String = "", _CadValAnt As String = "", _CadValNue As String = ""
        If _Existe Then
            If _idEmpresa <> vidEmpresa Then
                sSql += "idEmpresa = " & vidEmpresa & ","
                _CadValAnt += "idEmpresa =" & vidEmpresa & ","
            End If
            If _Nombre <> vNombre Then
                sSql += "Nombre = '" & vNombre & "',"
                _CadValAnt += "Nombre =" & vNombre & ","
            End If
            If _Domicilio <> vDomicilio Then
                sSql += "Domicilio = '" & vDomicilio & "',"
                _CadValAnt += "Domicilio =" & vDomicilio & ","
            End If
            If _RFC <> vRFC Then
                sSql += "RFC = '" & vRFC & "',"
                _CadValAnt += "RFC =" & vRFC & ","
            End If
            If _Clave_impuesto <> vClave_impuesto Then
                sSql += "Clave_impuesto = " & vClave_impuesto & ","
                _CadValAnt += "Clave_impuesto =" & vClave_impuesto & ","
            End If
            If _Activo <> vActivo Then
                sSql += "Activo = " & IIf(vActivo, 1, 0) & ","
                _CadValAnt += "Activo =" & IIf(vActivo, 1, 0) & ","
            End If

            If _idImpuestoFlete <> vidImpuestoFlete Then
                sSql += "idImpuestoFlete = " & vidImpuestoFlete & ","
                _CadValAnt += "idImpuestoFlete =" & vidImpuestoFlete & ","
            End If
            If _clave_Fraccion <> vclave_Fraccion Then
                sSql += "clave_Fraccion = " & vclave_Fraccion & ","
                _CadValAnt += "clave_Fraccion =" & vclave_Fraccion & ","
            End If

            If _penalizacion <> vpenalizacion Then
                sSql += "penalizacion = " & vpenalizacion & ","
                _CadValAnt += "penalizacion =" & vpenalizacion & ","
            End If

            If _estado <> vestado Then
                sSql += "estado = '" & vestado & "',"
                _CadValAnt += "estado =" & vestado & ","
            End If
            If _pais <> vpais Then
                sSql += "pais = '" & vpais & "',"
                _CadValAnt += "pais =" & vpais & ","
            End If
            If _municipio <> vmunicipio Then
                sSql += "municipio = '" & vmunicipio & "',"
                _CadValAnt += "municipio =" & vmunicipio & ","
            End If
            If _colonia <> vcolonia Then
                sSql += "colonia = '" & vcolonia & "',"
                _CadValAnt += "colonia =" & vcolonia & ","
            End If
            If _cp <> vcp Then
                sSql += "cp = '" & vcp & "',"
                _CadValAnt += "cp =" & vcp & ","
            End If
            If _telefono <> vtelefono Then
                sSql += "telefono = '" & vtelefono & "',"
                _CadValAnt += "telefono =" & vtelefono & ","
            End If
            If _idColonia <> vidColonia Then
                sSql += "idColonia = " & vidColonia & ","
                _CadValAnt += "idColonia =" & vidColonia & ","
            End If
            If _CambioPrecio <> vCambioPrecio Then
                sSql += "CambioPrecio = " & IIf(vCambioPrecio, 1, 0) & ","
                _CadValAnt += "CambioPrecio =" & IIf(vCambioPrecio, 1, 0) & ","
            End If
            If _GeneraDoc <> vGeneraDoc Then
                sSql += "GeneraDoc = " & IIf(vGeneraDoc, 1, 0) & ","
                _CadValAnt += "GeneraDoc =" & IIf(vGeneraDoc, 1, 0) & ","
            End If

            If sSql <> "" Then
                sSql = Mid(sSql, 1, sSql.Length - 1) 'Para quitar la Coma ','
                _CadValAnt = "IdCliente=" & _IdCliente & "," & _CadValAnt
                _CadValAnt = Mid(_CadValAnt, 1, _CadValAnt.Length - 1)
                _CadValNue = Replace(sSql, "'", "")

                sSql = "UPDATE " & _TablaBd & " SET " & sSql & " WHERE IdCliente = " & _IdCliente

                Try
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                    BD.Execute(sSql, True)
                    Audita(_TablaBd, _CadValAnt, _CadValNue, EnTipAccion.Modifica)
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
                Catch ex As Exception
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                    Throw New Exception(ex.Message)
                End Try
            End If
        Else
            If bEsIdentidad Then
                sSql = "insert into CatClientes (idEmpresa,Nombre,Domicilio,RFC,Clave_impuesto, " &
                "Activo,idImpuestoFlete,clave_Fraccion,penalizacion,estado,pais,municipio,colonia,cp,telefono,idColonia,CambioPrecio,GeneraDoc) " &
                "values (" & vidEmpresa & ",'" & vNombre & "','" & vDomicilio & "','" & vRFC &
                "'," & vClave_impuesto & "," & IIf(vActivo, 1, 0) & "," & vidImpuestoFlete & ", " & vclave_Fraccion &
                "," & vpenalizacion & ",'" & vestado & "','" & vpais & "','" & vmunicipio & "','" & vcolonia &
                "','" & vcp & "','" & vtelefono & "'," & vidColonia & "," & IIf(vCambioPrecio, 1, 0) & "," & IIf(vGeneraDoc, 1, 0) & ")"

            Else
                sSql = "insert into CatClientes (IdCliente,idEmpresa,Nombre,Domicilio,RFC,Clave_impuesto, " &
                "Activo,idImpuestoFlete,clave_Fraccion,penalizacion,estado,pais,municipio,colonia,cp,telefono,idColonia,CambioPrecio,GeneraDoc) " &
                "values (" & _IdCliente & "," & vidEmpresa & ",'" & vNombre & "','" & vDomicilio & "','" & vRFC &
                "'," & vClave_impuesto & "," & IIf(vActivo, 1, 0) & "," & vidImpuestoFlete & ", " & vclave_Fraccion &
                "," & vpenalizacion & ",'" & vestado & "','" & vpais & "','" & vmunicipio & "','" & vcolonia &
                "','" & vcp & "','" & vtelefono & "'," & vidColonia & "," & IIf(vCambioPrecio, 1, 0) & "," & IIf(vGeneraDoc, 1, 0) & ")"

            End If
            _CadValNue = "IdCliente=" & _IdCliente & ",idEmpresa=" & vidEmpresa & ",Nombre = " & vNombre & ",Domicilio = " & vDomicilio &
            ",RFC = " & vRFC & ",Clave_impuesto = " & vClave_impuesto & ",Activo = " & vActivo & ",idImpuestoFlete = " & vidImpuestoFlete &
            ",clave_Fraccion = " & vclave_Fraccion & ",penalizacion = " & vpenalizacion & ",estado = " & vestado & ",pais = " & vpais &
            ",municipio = " & vmunicipio & ",colonia = " & vcolonia & ",cp = " & vcp & ",telefono = " & vtelefono & ",idColonia = " &
            vidColonia & ",CambioPrecio = " & vCambioPrecio & ",GeneraDoc = " & vGeneraDoc

            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, "", _CadValNue, EnTipAccion.Inserta)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
        End If
    End Sub
    Public Sub Eliminar()
        If _Existe Then
            Dim sSql As String = "", _CadValAnt As String = ""

            _CadValAnt = "IdCliente=" & _IdCliente & ",idEmpresa=" & _idEmpresa & ",Nombre = " & _Nombre & ",Domicilio = " & _Domicilio &
            ",RFC = " & _RFC & ",Clave_impuesto = " & _Clave_impuesto & ",Activo = " & _Activo & ",idImpuestoFlete = " & _idImpuestoFlete &
            ",clave_Fraccion = " & _clave_Fraccion & ",penalizacion = " & _penalizacion & ",estado = " & _estado & ",pais = " & _pais &
            ",municipio = " & _municipio & ",colonia = " & _colonia & ",cp = " & _cp & ",telefono = " & _telefono & ",idColonia = " &
            _idColonia & ",CambioPrecio = " & _CambioPrecio & ",GeneraDoc = " & _GeneraDoc


            sSql = "DELETE FROM " & _TablaBd & " WHERE IdCliente = " & _IdCliente
            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, _CadValAnt, "", EnTipAccion.Elimina)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
            _Existe = False
        Else
            'se supone que nunca debe entrar aqui. . .
            MessageBox.Show("El Cliente con Id: " & _IdCliente & " No existe por lo tanto no se puede Eliminar!!!",
                            "No se puede eliminar el Cliente porque no Existe", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub
#End Region

#Region "Consultas"
    Public Function TablaClientesCombo(ByVal FiltroWhere As String) As DataTable
        MyTabla.Rows.Clear()

        'StrSql = "SELECT idPersonal, idEmpresa, NombreCompleto FROM dbo.CatPersonal " &
        '"WHERE idPuesto in (7,36,37) AND Estatus = '" & IIf(Activo, "ACT", "BAJ") & "'" &
        'IIf(idEmpresa > 0, " and idEmpresa = " & idEmpresa, "")

        StrSql = "SELECT cli.IdCliente, " &
        "cli.Nombre, " &
        "cli.idEmpresa " &
        "FROM dbo.CatClientes cli " & FiltroWhere

        '"WHERE cli.idEmpresa = 1 AND cli.Activo = 1"



        MyTabla = BD.ExecuteReturn(StrSql)

        Return MyTabla

    End Function
#End Region
End Class
