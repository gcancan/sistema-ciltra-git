﻿Public Class ProveedorClass
    'idProveedor				vidProveedor 				_idProveedor
    'RazonSocial				vRazonSocial 				_RazonSocial 
    'RFCProv					vRFCProv 					_RFCProv
    'DireccionProv				vDireccionProv				_DireccionProv
    'EmailProv					vEmailProv					_EmailProv
    'TipoPersona				vTipoPersona 				_TipoPersona
    'idColonia					vidColonia 					_idColonia
    Private _idProveedor As Integer
    Private _RazonSocial As String
    Private _RFCProv As String
    Private _DireccionProv As String
    Private _idColonia As Integer
    Private _EmailProv As String
    Private _TipoPersona As String

    Private _Existe As Boolean
    Private _TablaBd As String = "CatProveedores"

    Dim StrSql As String
    Dim DsClase As New DataTable
    Dim MyTabla As DataTable = New DataTable(_TablaBd)
#Region "Constructor"
    Sub New(ByVal vidProveedor As Integer)
        'Dim DtEst As New DataTable
        _idProveedor = vidProveedor

        'StrSql = "select pro.idProveedor, pro.RazonSocial, pro.RFCProv, pro.TipoPersona, " & _
        '"pro.EmailProv, pro.idColonia, col.NomColonia, col.CP, " & _
        '"col.idCiudad, ciu.NomCiudad, " & _
        '"ciu.idEstado, est.NomEstado, " & _
        '"est.idPais, pais.NomPais " & _
        '"from " & _TablaBd & " Pro " & _
        '"inner join CatColonia col on pro.idColonia = col.idColonia " & _
        '"inner join CatCiudad ciu on col.idCiudad = ciu.idCiudad " & _
        '"inner join CatEstados Est on ciu.idEstado = est.idEstado " & _
        '"inner join CatPais pais on est.idPais = pais.idPais " & _
        '"where pro.idProveedor = " & vidEmpresa

        StrSql = "select pro.idProveedor, pro.RazonSocial,pro.DireccionProv, pro.RFCProv, pro.TipoPersona, " & _
       "pro.EmailProv, pro.idColonia " & _
       "from " & _TablaBd & " Pro " & _
       "where pro.idProveedor = " & vidProveedor


        DsClase = BD.ExecuteReturn(StrSql)
        If DsClase.Rows.Count > 0 Then
            With DsClase.Rows(0)
                _RazonSocial = .Item("RazonSocial") & ""
                _RFCProv = .Item("RFCProv")
                _DireccionProv = .Item("DireccionProv")
                _idColonia = .Item("idColonia")
                _EmailProv = .Item("EmailProv")
                _TipoPersona = .Item("TipoPersona")
            End With
            _Existe = True
        Else
            _Existe = False
        End If

    End Sub
    Sub New(ByVal vidProveedor As Integer, ByVal vRazonSocial As String, ByVal vRFCProv As String, ByVal vDireccionProv As String _
            , ByVal vidColonia As Integer, ByVal vEmailProv As String, ByVal vTipoPersona As String)
        _idProveedor = vidProveedor
        _RazonSocial = vRazonSocial
        _RFCProv = vRFCProv
        _DireccionProv = vDireccionProv
        _idColonia = vidColonia
        _EmailProv = vEmailProv
        _TipoPersona = vTipoPersona
        _Existe = True
    End Sub
#End Region
#Region "Propiedades"
    'Public ReadOnly Property idDivision As Integer
    '    Get
    '        Return _idProveedor
    '    End Get
    'End Property
    Public Property idProveedor As Integer
        Get
            Return _idProveedor
        End Get
        Set(ByVal value As Integer)
            _idProveedor = value
        End Set
    End Property
    Public Property RazonSocial As String
        Get
            Return _RazonSocial
        End Get
        Set(ByVal value As String)
            _RazonSocial = value
        End Set
    End Property
    Public Property RFCProv As String
        Get
            Return _RFCProv
        End Get
        Set(ByVal value As String)
            _RFCProv = value
        End Set
    End Property
    Public Property DireccionProv As String
        Get
            Return _DireccionProv
        End Get
        Set(ByVal value As String)
            _DireccionProv = value
        End Set
    End Property
    Public Property idColonia As Integer
        Get
            Return _idColonia
        End Get
        Set(ByVal value As Integer)
            _idColonia = value
        End Set
    End Property
    Public Property EmailProv As String
        Get
            Return _EmailProv
        End Get
        Set(ByVal value As String)
            _EmailProv = value
        End Set
    End Property
    Public Property TipoPersona As String
        Get
            Return _TipoPersona
        End Get
        Set(ByVal value As String)
            _TipoPersona = value
        End Set
    End Property
    'Public ReadOnly Property Existe As Boolean
    '    Get
    '        Return _Existe
    '    End Get
    'End Property
    Public Property Existe As Boolean
        Get
            Return _Existe
        End Get
        Set(ByVal value As Boolean)
            _Existe = value
        End Set
    End Property

#End Region
#Region "Funciones"
    Public Function GetCambios(ByVal vRazonSocial As String, ByVal vRFCProv As String, _
            ByVal vDireccionProv As String, ByVal vidColonia As Integer, ByVal vEmailProv As String, ByVal vTipoPersona As String) As String

        Dim CadCam As String = ""
        If _RazonSocial <> vRazonSocial Then
            CadCam += "Razon Social: '" & _RazonSocial & "' Cambia a [" & vRazonSocial & "],"
        End If
        If _RFCProv <> vRFCProv Then
            CadCam += "RFCProv: '" & _RFCProv & "' Cambia a [" & vRFCProv & "],"
        End If
        If _DireccionProv <> vDireccionProv Then
            CadCam += "DireccionProv: '" & _DireccionProv & "' Cambia a [" & vDireccionProv & "],"
        End If
        If _idColonia <> vidColonia Then
            CadCam += "Id Colonia: '" & _idColonia & "' Cambia a [" & vidColonia & "],"
        End If
        If _EmailProv <> vEmailProv Then
            CadCam += "EmailProv: '" & _EmailProv & "' Cambia a [" & vEmailProv & "],"
        End If
        If _TipoPersona <> vTipoPersona Then
            CadCam += "TipoPersona: '" & _TipoPersona & "' Cambia a [" & vTipoPersona & "],"
        End If

        If CadCam <> "" Then
            CadCam = Mid(CadCam, 1, CadCam.Length - 1)
        End If
        Return CadCam
    End Function
    '
    Public Sub Guardar(ByVal bEsIdentidad As Boolean, ByVal vRazonSocial As String, ByVal vRFCProv As String, ByVal vDireccionProv As String, _
                       ByVal vidColonia As Integer, ByVal vEmailProv As String, ByVal vTipoPersona As String)

        Dim sSql As String = "", _CadValAnt As String = "", _CadValNue As String = ""
        If _Existe Then
            If _RazonSocial <> vRazonSocial Then
                sSql += "NomSucursal = '" & vRazonSocial & "',"
                _CadValAnt += "NomSucursal =" & vRazonSocial & ","
            End If
            If _RFCProv <> vRFCProv Then
                sSql += "RFCProv = '" & vRFCProv & "',"
                _CadValAnt += "RFCProv =" & vRFCProv & ","
            End If
            If _DireccionProv <> vDireccionProv Then
                sSql += "DireccionProv = '" & vDireccionProv & "',"
                _CadValAnt += "DireccionProv =" & vDireccionProv & ","
            End If
            If _idColonia <> vidColonia Then
                sSql += "idColonia = " & vidColonia & ","
                _CadValAnt += "idColonia =" & vidColonia & ","
            End If
            If _EmailProv <> vEmailProv Then
                sSql += "EmailProv = '" & vEmailProv & "',"
                _CadValAnt += "EmailProv =" & vEmailProv & ","
            End If
            If _TipoPersona <> vTipoPersona Then
                sSql += "TipoPersona = '" & Left(vTipoPersona, 1) & "',"
                _CadValAnt += "TipoPersona =" & Left(vTipoPersona, 1) & ","
            End If

            If sSql <> "" Then
                sSql = Mid(sSql, 1, sSql.Length - 1) 'Para quitar la Coma ','
                _CadValAnt = "idProveedor=" & _idProveedor & "," & _CadValAnt
                _CadValAnt = Mid(_CadValAnt, 1, _CadValAnt.Length - 1)
                _CadValNue = Replace(sSql, "'", "")

                sSql = "UPDATE " & _TablaBd & " SET " & sSql & " WHERE idProveedor = '" & _idProveedor & "'"
                Try
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                    BD.Execute(sSql, True)
                    Audita(_TablaBd, _CadValAnt, _CadValNue, EnTipAccion.Modifica)
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
                Catch ex As Exception
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                    Throw New Exception(ex.Message)
                End Try
            End If
        Else
            If bEsIdentidad Then

                sSql = "INSERT INTO " & _TablaBd & "(RazonSocial, RFCProv, DireccionProv, EmailProv, idColonia, TipoPersona) " & _
                "VALUES('" & vRazonSocial & "','" & vRFCProv & "','" & vDireccionProv & "','" & vEmailProv & "'," & vidColonia & ",'" & Left(vTipoPersona, 1) & "')"

                _CadValNue = "RazonSocial=" & vRazonSocial & _
                "RFCProv = " & vRFCProv & _
                "Direccion = " & vDireccionProv & _
                "idColonia = " & vidColonia & _
                "EmailProv = " & vEmailProv & _
                "TipoPersona = " & vTipoPersona
            Else

                sSql = "INSERT INTO " & _TablaBd & "(idProveedor,RazonSocial, RFCProv, DireccionProv, EmailProv, idColonia, TipoPersona) " & _
                "VALUES(" & _idProveedor & ",'" & vRazonSocial & "','" & vRFCProv & "','" & vDireccionProv & "','" & vEmailProv & "'," & vidColonia & ",'" & Left(vTipoPersona, 1) & "')"

                _CadValNue = "idProveedor = " & _idProveedor & _
                "RazonSocial = " & vRazonSocial & _
                "RFCProv = " & vRFCProv & _
                "Direccion = " & vDireccionProv & _
                "idColonia = " & vidColonia & _
                "EmailProv = " & vEmailProv & _
                "TipoPersona = " & vTipoPersona

            End If

            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, "", _CadValNue, EnTipAccion.Inserta)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
        End If

        _RazonSocial = vRazonSocial
        _RFCProv = vRFCProv
        _DireccionProv = vDireccionProv
        _idColonia = vidColonia
        _EmailProv = vEmailProv
        _TipoPersona = vTipoPersona
    End Sub
    Public Sub Eliminar()
        If _Existe Then
            Dim sSql As String = "", _CadValAnt As String = ""
            '_CadValAnt = "idEmpresa=" & _idProveedor & " , NomSucursal =" & _RazonSocial

            _CadValAnt = "idProveedor = " & _idProveedor & _
                "RazonSocial = " & _RazonSocial & _
                "RFCProv = " & _RFCProv & _
                "Direccion = " & _DireccionProv & _
                "idColonia = " & _idColonia & _
                "EmailProv = " & _EmailProv & _
                "TipoPersona = " & _TipoPersona

            sSql = "DELETE FROM " & _TablaBd & " WHERE idProveedor = '" & _idProveedor & "'"
            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, _CadValAnt, "", EnTipAccion.Elimina)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
            _Existe = False
        Else
            'se supone que nunca debe entrar aqui. . .
            MessageBox.Show("El Proveedor con Id: " & _idProveedor & " No existe por lo tanto no se puede Eliminar!!!",
                            "No se puede eliminar el Proveedor porque no Existe", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub
#End Region
#Region "Consultas"
    Public Function TablaProvedores(ByVal bIncluyeTodos As Boolean, Optional ByVal FiltroSinWhere As String = "") As DataTable
        Dim filtro As String = ""
        MyTabla.Rows.Clear()

        If FiltroSinWhere <> "" Then
            filtro = " where " & FiltroSinWhere
        End If

        If bIncluyeTodos Then
            StrSql = "SELECT 0 AS idProveedor,'TODOS' AS RazonSocial " &
            "UNION " &
            "SELECT idProveedor,RazonSocial FROM dbo." & _TablaBd & " " & filtro &
            " ORDER BY idProveedor"
        Else
            StrSql = "SELECT idProveedor,RazonSocial FROM dbo." & _TablaBd & " " & filtro &
            " ORDER BY idProveedor"
        End If
        MyTabla = BD.ExecuteReturn(StrSql)

        Return MyTabla

    End Function
#End Region
End Class
