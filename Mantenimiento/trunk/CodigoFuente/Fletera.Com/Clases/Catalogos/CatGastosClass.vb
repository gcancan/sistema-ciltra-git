﻿Public Class CatGastosClass
    Private _idGasto As Integer
    Private _NomGasto As String
    Private _Deducible As Boolean
    Dim _ReqComprobante As Boolean
    Dim _ReqAutorizacion As Boolean


    Private _Existe As Boolean
    Private _TablaBd As String = "CatGastos"

    Dim StrSql As String
    Dim DsClase As New DataTable
    Dim MyTabla As DataTable = New DataTable(_TablaBd)

#Region "Constructor"
    Sub New(ByVal vidGasto As Integer)
        _idGasto = vidGasto

        StrSql = "SELECT g.idGasto, " &
        "g.NomGasto, " &
        "g.Deducible,ReqComprobante,ReqAutorizacion " &
        "FROM dbo." & _TablaBd & " G " &
        "WHERE g.idGasto = " & vidGasto

        DsClase = BD.ExecuteReturn(StrSql)
        If DsClase.Rows.Count > 0 Then
            With DsClase.Rows(0)
                _NomGasto = .Item("NomGasto") & ""
                _Deducible = .Item("Deducible")
                _ReqComprobante = .Item("ReqComprobante")
                _ReqAutorizacion = .Item("ReqAutorizacion")
            End With
            _Existe = True
        Else
            _Existe = False
        End If

    End Sub

    Sub New(ByVal vidGasto As Integer, ByVal vNomGasto As String, ByVal vDeducible As Boolean, ByVal vReqComprobante As Boolean,
            ByVal vReqAutorizacion As Boolean)
        _idGasto = vidGasto
        _NomGasto = vNomGasto
        _Deducible = vDeducible
        _ReqComprobante = vReqComprobante
        _ReqAutorizacion = vReqAutorizacion
        _Existe = True
    End Sub
#End Region

#Region "Propiedades"
    Public Property idGasto As Integer
        Get
            Return _idGasto
        End Get
        Set(ByVal value As Integer)
            _idGasto = value
        End Set
    End Property
    Public Property NomGasto As String
        Get
            Return _NomGasto
        End Get
        Set(ByVal value As String)
            _NomGasto = value
        End Set
    End Property
    Public Property Deducible As Boolean
        Get
            Return _Deducible
        End Get
        Set(ByVal value As Boolean)
            _Deducible = value
        End Set
    End Property
    Public Property ReqComprobante As Boolean
        Get
            Return _ReqComprobante
        End Get
        Set(ByVal value As Boolean)
            _ReqComprobante = value
        End Set
    End Property
    Public Property ReqAutorizacion As Boolean
        Get
            Return _ReqAutorizacion
        End Get
        Set(ByVal value As Boolean)
            _ReqAutorizacion = value
        End Set
    End Property
    Public Property Existe As Boolean
        Get
            Return _Existe
        End Get
        Set(ByVal value As Boolean)
            _Existe = value
        End Set
    End Property
#End Region

#Region "Consultas"
    Public Function TablaGastos(ByVal bIncluyeTodos As Boolean, Optional ByVal FiltroSinWhere As String = "") As DataTable
        Dim filtro As String = ""
        MyTabla.Rows.Clear()

        If FiltroSinWhere <> "" Then
            filtro = " where " & FiltroSinWhere
        End If

        If bIncluyeTodos Then
            StrSql = "SELECT 0 AS idGasto,'TODOS' AS NomGasto, 0 as Deducible,0 as ReqComprobante,0 as ReqAutorizacion " &
            "UNION " &
            "SELECT idGasto,NomGasto,Deducible,ReqComprobante,ReqAutorizacion FROM dbo.CatGastos " & filtro &
            " ORDER BY idGasto"

        Else
            StrSql = "SELECT idGasto,NomGasto,Deducible FROM dbo.CatGastos " & filtro &
            " ORDER BY idGasto"
        End If
        MyTabla = BD.ExecuteReturn(StrSql)

        Return MyTabla

    End Function
#End Region


End Class
