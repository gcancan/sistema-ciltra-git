﻿Public Class TipoOrdenClass

    'idTipoOrden
    'NomTipoOrden
    Private _idTipoOrden As Integer
    Private _NomTipoOrden As String

    Private _Existe As Boolean
    Private _TablaBd As String = "CatTipoOrden"

    Dim StrSql As String
    Dim DsClase As New DataTable
#Region "Constructor"
    Sub New(ByVal vidTipoOrden As Integer)
        'Dim DtEst As New DataTable
        _idTipoOrden = vidTipoOrden
        StrSql = "select idTipoOrden,NomTipoOrden from " & _TablaBd & " where idTipoOrden = " & vidTipoOrden

        DsClase = BD.ExecuteReturn(StrSql)
        If DsClase.Rows.Count > 0 Then
            With DsClase.Rows(0)
                _NomTipoOrden = .Item("NomTipoOrden") & ""
            End With
            _Existe = True
        Else
            _Existe = False
        End If

    End Sub
    Sub New(ByVal vidTipoOrden As Integer, ByVal vNomTipoOrden As String)
        _idTipoOrden = vidTipoOrden
        _NomTipoOrden = vNomTipoOrden
        _Existe = True
    End Sub
#End Region
#Region "Propiedades"
    'Public ReadOnly Property idDivision As Integer
    '    Get
    '        Return _idDivision
    '    End Get
    'End Property
    Public Property idTipoOrden As Integer
        Get
            Return _idTipoOrden
        End Get
        Set(ByVal value As Integer)
            _idTipoOrden = value
        End Set
    End Property
    Public Property NomTipoOrden As String
        Get
            Return _NomTipoOrden
        End Get
        Set(ByVal value As String)
            _NomTipoOrden = value
        End Set
    End Property
    'Public ReadOnly Property Existe As Boolean
    '    Get
    '        Return _Existe
    '    End Get
    'End Property
    Public Property Existe As Boolean
        Get
            Return _Existe
        End Get
        Set(ByVal value As Boolean)
            _Existe = value
        End Set
    End Property

#End Region
#Region "Funciones"
    Public Function GetCambios(ByVal vNomTipoOrden As String) As String
        Dim CadCam As String = ""
        If _NomTipoOrden <> vNomTipoOrden Then
            CadCam += "Nombre Tipo Orden: '" & _NomTipoOrden & "' Cambia a [" & vNomTipoOrden & "],"
        End If
        If CadCam <> "" Then
            CadCam = Mid(CadCam, 1, CadCam.Length - 1)
        End If
        Return CadCam
    End Function
    Public Sub Guardar(ByVal vNomTipoOrden As String, ByVal bEsIdentidad As Boolean)
        Dim sSql As String = "", _CadValAnt As String = "", _CadValNue As String = ""
        If _Existe Then
            If _NomTipoOrden <> vNomTipoOrden Then
                sSql += "NomTipoOrden = '" & vNomTipoOrden & "',"
                _CadValAnt += "NomTipoOrden =" & vNomTipoOrden & ","
            End If

            If sSql <> "" Then
                sSql = Mid(sSql, 1, sSql.Length - 1) 'Para quitar la Coma ','
                _CadValAnt = "idTipoOrden=" & _idTipoOrden & "," & _CadValAnt
                _CadValAnt = Mid(_CadValAnt, 1, _CadValAnt.Length - 1)
                _CadValNue = Replace(sSql, "'", "")
                sSql = "UPDATE " & _TablaBd & " SET " & sSql & " WHERE idTipoOrden = '" & _idTipoOrden & "'"
                Try
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                    BD.Execute(sSql, True)
                    Audita(_TablaBd, _CadValAnt, _CadValNue, EnTipAccion.Modifica)
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
                Catch ex As Exception
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                    Throw New Exception(ex.Message)
                End Try
            End If
        Else
            If bEsIdentidad Then
                sSql = "INSERT INTO " & _TablaBd & "(NomTipoOrden) VALUES('" & _NomTipoOrden & "')"

                _CadValNue = "NomTipoOrden=" & _NomTipoOrden
            Else

                sSql = "INSERT INTO " & _TablaBd & "(idTipoOrden,NomTipoOrden) VALUES(" &
                "'" & _idTipoOrden & "','" & _NomTipoOrden & "')"

                _CadValNue = "idTipoOrden=" & _idTipoOrden & ",NomTipoOrden=" & _NomTipoOrden
            End If

            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, "", _CadValNue, EnTipAccion.Inserta)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
        End If
        _NomTipoOrden = vNomTipoOrden
    End Sub
    Public Sub Eliminar()
        If _Existe Then
            Dim sSql As String = "", _CadValAnt As String = ""
            _CadValAnt = "idTipoOrden=" & _idTipoOrden & " , NomTipoOrden =" & _NomTipoOrden
            sSql = "DELETE FROM " & _TablaBd & " WHERE idTipoOrden = '" & _idTipoOrden & "'"
            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, _CadValAnt, "", EnTipAccion.Elimina)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
            _Existe = False
        Else
            'se supone que nunca debe entrar aqui. . .
            MessageBox.Show("El tipo de Orden de Servicio con Id: " & _idTipoOrden & " No existe por lo tanto no se puede Eliminar!!!",
                            "No se puede eliminar el tipo de Orden de Servicio porque no Existe", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub
#End Region
End Class
