﻿Public Class SucursalClass
    'idSucursal     vidSucursal
    'NomSucursal    vNomSucursal
    'idEmpresa      vidEmpresa
    'RazonSocial    vRazonSocial
    Private _idSucursal As Integer
    Private _NomSucursal As String
    Private _idEmpresa As Integer
    Private _RazonSocial As String

    Private _Existe As Boolean
    Private _TablaBd As String = "CatSucursal"

    Dim StrSql As String
    Dim DsClase As New DataTable
#Region "Constructor"
    Sub New(ByVal vidSucursal As Integer)
        'Dim DtEst As New DataTable
        _idSucursal = vidSucursal

        StrSql = "select suc.id_sucursal, suc.NomSucursal, suc.id_empresa, emp.RazonSocial " & _
        "from " & _TablaBd & "  Suc " & _
        "INNER join catempresas emp on suc.id_empresa = emp.idEmpresa  " & _
        "where suc.id_sucursal = " & vidSucursal


        DsClase = BD.ExecuteReturn(StrSql)
        If DsClase.Rows.Count > 0 Then
            With DsClase.Rows(0)
                _NomSucursal = .Item("NomSucursal") & ""
                _idEmpresa = .Item("id_empresa")
                _RazonSocial = .Item("RazonSocial")
            End With
            _Existe = True
        Else
            _Existe = False
        End If

    End Sub
    Sub New(ByVal vidSucursal As Integer, ByVal vNomSucursal As String, ByVal vidEmpresa As Integer, ByVal vRazonSocial As String)
        _idSucursal = vidSucursal
        _NomSucursal = vNomSucursal
        _idEmpresa = vidEmpresa
        _RazonSocial = vRazonSocial
        _Existe = True
    End Sub
#End Region
#Region "Propiedades"
    'Public ReadOnly Property idDivision As Integer
    '    Get
    '        Return _idEmpresa
    '    End Get
    'End Property
    Public Property idSucursal As Integer
        Get
            Return _idSucursal
        End Get
        Set(ByVal value As Integer)
            _idSucursal = value
        End Set
    End Property
    Public Property NomSucursal As String
        Get
            Return _NomSucursal
        End Get
        Set(ByVal value As String)
            _NomSucursal = value
        End Set
    End Property
    Public Property idEmpresa As Integer
        Get
            Return _idEmpresa
        End Get
        Set(ByVal value As Integer)
            _idEmpresa = value
        End Set
    End Property
    Public Property RazonSocial As String
        Get
            Return _RazonSocial
        End Get
        Set(ByVal value As String)
            _RazonSocial = value
        End Set
    End Property
    'Public ReadOnly Property Existe As Boolean
    '    Get
    '        Return _Existe
    '    End Get
    'End Property
    Public Property Existe As Boolean
        Get
            Return _Existe
        End Get
        Set(ByVal value As Boolean)
            _Existe = value
        End Set
    End Property

#End Region
#Region "Funciones"
    Public Function GetCambios(ByVal vNomSucursal As String, ByVal vidEmpresa As Integer) As String
        Dim CadCam As String = ""
        If _NomSucursal <> vNomSucursal Then
            CadCam += "Nombre Sucursal: '" & _NomSucursal & "' Cambia a [" & vNomSucursal & "],"
        End If
        If _idEmpresa <> vidEmpresa Then
            CadCam += "id Empresa: '" & _idEmpresa & "' Cambia a [" & vidEmpresa & "],"
        End If

        If CadCam <> "" Then
            CadCam = Mid(CadCam, 1, CadCam.Length - 1)
        End If
        Return CadCam
    End Function
    Public Sub Guardar(ByVal vNomSucursal As String, ByVal vidEmpresa As Integer, ByVal bEsIdentidad As Boolean, ByVal vRazonSocial As String)
        Dim sSql As String = "", _CadValAnt As String = "", _CadValNue As String = ""
        If _Existe Then
            If _NomSucursal <> vNomSucursal Then
                sSql += "NomSucursal = '" & vNomSucursal & "',"
                _CadValAnt += "NomSucursal =" & vNomSucursal & ","
            End If
            If _idEmpresa <> vidEmpresa Then
                sSql += "id_empresa = " & vidEmpresa & ","
                _CadValAnt += "id_empresa =" & vidEmpresa & ","
            End If
            If sSql <> "" Then
                sSql = Mid(sSql, 1, sSql.Length - 1) 'Para quitar la Coma ','
                _CadValAnt = "idSucursal=" & _idSucursal & "," & _CadValAnt
                _CadValAnt = Mid(_CadValAnt, 1, _CadValAnt.Length - 1)
                _CadValNue = Replace(sSql, "'", "")

                sSql = "UPDATE " & _TablaBd & " SET " & sSql & " WHERE idSucursal = '" & _idSucursal & "'"
                Try
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                    BD.Execute(sSql, True)
                    Audita(_TablaBd, _CadValAnt, _CadValNue, EnTipAccion.Modifica)
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
                Catch ex As Exception
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                    Throw New Exception(ex.Message)
                End Try
            End If
        Else
            If bEsIdentidad Then
                sSql = "INSERT INTO " & _TablaBd & "(NomSucursal,id_empresa) VALUES('" & vNomSucursal & "'," & vidEmpresa & ")"

                _CadValNue = "NomSucursal=" & vNomSucursal & "id_empresa = " & vidEmpresa
            Else

                sSql = "INSERT INTO " & _TablaBd & "(idSucursal,NomSucursal,id_empresa) VALUES(" &
                "'" & _idSucursal & "','" & vNomSucursal & "'," & vidEmpresa & ")"

                _CadValNue = "idSucursal=" & _idSucursal & ",NomSucursal=" & vNomSucursal & ",id_empresa=" & vidEmpresa
            End If

            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, "", _CadValNue, EnTipAccion.Inserta)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
        End If
        _NomSucursal = vNomSucursal
        _idEmpresa = vidEmpresa
        _RazonSocial = vRazonSocial
    End Sub
    Public Sub Eliminar()
        If _Existe Then
            Dim sSql As String = "", _CadValAnt As String = ""
            _CadValAnt = "idSucursal=" & _idSucursal & " , NomSucursal =" & _NomSucursal
            sSql = "DELETE FROM " & _TablaBd & " WHERE idSucursal = '" & _idSucursal & "'"
            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, _CadValAnt, "", EnTipAccion.Elimina)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
            _Existe = False
        Else
            'se supone que nunca debe entrar aqui. . .
            MessageBox.Show("La Sucursal con Id: " & _idSucursal & " No existe por lo tanto no se puede Eliminar!!!",
                            "No se puede eliminar la Sucursal porque no Existe", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub
#End Region

End Class
