﻿Public Class FraccionesClass
    Private _IdFraccion As Integer
    Private _IdEmpresa As Integer
    Private _Descripcion As String

    Private _Existe As Boolean
    Private _TablaBd As String = "CatFracciones"

    Dim StrSql As String
    Dim DsClase As New DataTable

    Dim MyTabla As DataTable = New DataTable(_TablaBd)

#Region "Constructor"

    Sub New(ByVal vIdFraccion As Integer, ByVal vIdEmpresa As Integer)
        Dim filtro As String = ""

        If vIdEmpresa > 0 Then
            filtro = " where IdFraccion = " & vIdFraccion & " and IdEmpresa = " & vIdEmpresa
        Else
            filtro = " where IdFraccion = " & vIdFraccion
        End If

        StrSql = "SELECT IdFraccion, " &
      "IdEmpresa, " &
      "Descripcion " &
      "FROM dbo.CatFracciones " & filtro

        _IdFraccion = vIdFraccion

        DsClase = BD.ExecuteReturn(StrSql)
        If DsClase.Rows.Count > 0 Then
            With DsClase.Rows(0)
                _IdEmpresa = .Item("IdEmpresa")
                _Descripcion = .Item("Descripcion")

            End With
            _Existe = True
        Else
            _Existe = False
        End If


    End Sub

    Sub New(ByVal vIdFraccion As Integer, ByVal vIdEmpresa As Integer, ByVal vDescripcion As String)
        _IdFraccion = vIdFraccion
        _IdEmpresa = vIdEmpresa
        _Descripcion = vDescripcion
        _Existe = True
    End Sub
#End Region

#Region "Propiedades"
    Public Property IdFraccion As Integer
        Get
            Return _IdFraccion
        End Get
        Set(ByVal value As Integer)
            _IdFraccion = value
        End Set
    End Property
    Public Property idEmpresa As Integer
        Get
            Return _IdEmpresa
        End Get
        Set(ByVal value As Integer)
            _IdEmpresa = value
        End Set
    End Property
    Public Property Descripcion As String
        Get
            Return _Descripcion
        End Get
        Set(ByVal value As String)
            _Descripcion = value
        End Set
    End Property
    Public Property Existe As Boolean
        Get
            Return _Existe
        End Get
        Set(ByVal value As Boolean)
            _Existe = value
        End Set
    End Property
#End Region

#Region "Consultas"
    Public Function TablaFracciones(ByVal bIncluyeTodos As Boolean, Optional ByVal FiltroSinWhere As String = "") As DataTable
        Dim filtro As String = ""
        MyTabla.Rows.Clear()

        If FiltroSinWhere <> "" Then
            filtro = " where " & FiltroSinWhere
        End If

        If bIncluyeTodos Then
            StrSql = "SELECT 0 AS IdFraccion,0 AS IdEmpresa,'TODAS' AS Descripcion " &
            "union " &
            "SELECT IdFraccion,IdEmpresa,Descripcion FROM dbo.CatFracciones " & filtro &
            " ORDER BY IdFraccion"

        Else
            StrSql = "SELECT IdFraccion,IdEmpresa,Descripcion FROM dbo.CatFracciones " & filtro &
            " ORDER BY IdFraccion"
        End If
        MyTabla = BD.ExecuteReturn(StrSql)

        Return MyTabla

    End Function
#End Region
End Class
