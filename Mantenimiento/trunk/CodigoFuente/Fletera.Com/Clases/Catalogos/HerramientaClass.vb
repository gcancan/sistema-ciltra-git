﻿Public Class HerramientaClass
    Private _idHerramienta As Integer
    Private _NomHerramienta As String
    Private _Medida As String
    Private _idFamilia As Integer
    Private _idTipoHerramienta As Integer
    Private _idMarca As Integer
    Private _NoPiezas As String
    Private _Modelo As String

    Private _Existe As Boolean
    Private _TablaBd As String = "CatHerramientas"

    Dim StrSql As String
    Dim DsClase As New DataTable

#Region "Constructor"
    Sub New(ByVal vidHerramienta As Integer)
        _idHerramienta = vidHerramienta
        StrSql = "select her.idHerramienta, her.NomHerramienta, her.Medida, " & _
        "her.idFamilia, fam.NomFamilia, fam.idDivision, div.NomDivision, " & _
        "her.idTipoHerramienta, tph.NomTipoHerramienta, " & _
        "her.idMarca, mar.NombreMarca, " & _
        "her.NoPiezas, her.Medida, her.Modelo  " & _
        "from " & _TablaBd & " Her " & _
        "inner join CatFamilia fam on her.idFamilia = fam.idFamilia " & _
        "inner join catdivision div on fam.idDivision = div.iddivision " & _
        "inner join CatTipoHerramienta tph on her.idTipoHerramienta = tph.idTipoHerramienta " & _
        "inner join CatMarcas mar on her.idMarca = mar.idMarca " & _
        "where her.idHerramienta = " & vidHerramienta


        DsClase = BD.ExecuteReturn(StrSql)
        If DsClase.Rows.Count > 0 Then
            With DsClase.Rows(0)
                _NomHerramienta = .Item("NomHerramienta") & ""
                _Medida = .Item("Medida")
                _idFamilia = .Item("idFamilia")
                _idTipoHerramienta = .Item("idTipoHerramienta")
                _idMarca = .Item("idMarca")
                _NoPiezas = .Item("NoPiezas")
                _Modelo = .Item("Modelo")
            End With
            _Existe = True
        Else
            _Existe = False
        End If

    End Sub
    Sub New(ByVal vidHerramienta As Integer, ByVal vNomHerramienta As String, ByVal vMedida As String, ByVal vidFamilia As Integer _
            , ByVal vidTipoHerramienta As Integer, ByVal vidMarca As String, ByVal vNoPiezas As String, ByVal vModelo As String)
        _idHerramienta = vidHerramienta
        _NomHerramienta = vNomHerramienta
        _Medida = vMedida
        _idFamilia = vidFamilia
        _idTipoHerramienta = vidTipoHerramienta
        _idMarca = vidMarca
        _NoPiezas = vNoPiezas
        _Modelo = vModelo
        _Existe = True
    End Sub
#End Region

#Region "Propiedades"
    'Public ReadOnly Property idDivision As Integer
    '    Get
    '        Return _idHerramienta
    '    End Get
    'End Property
    Public Property idHerramienta As Integer
        Get
            Return _idHerramienta
        End Get
        Set(ByVal value As Integer)
            _idHerramienta = value
        End Set
    End Property
    Public Property NomHerramienta As String
        Get
            Return _NomHerramienta
        End Get
        Set(ByVal value As String)
            _NomHerramienta = value
        End Set
    End Property
    Public Property Medida As String
        Get
            Return _Medida
        End Get
        Set(ByVal value As String)
            _Medida = value
        End Set
    End Property
    
    Public Property idFamilia As Integer
        Get
            Return _idFamilia
        End Get
        Set(ByVal value As Integer)
            _idFamilia = value
        End Set
    End Property
    Public Property idTipoHerramienta As Integer
        Get
            Return _idTipoHerramienta
        End Get
        Set(ByVal value As Integer)
            _idTipoHerramienta = value
        End Set
    End Property
    Public Property idMarca As Integer
        Get
            Return _idMarca
        End Get
        Set(ByVal value As Integer)
            _idMarca = value
        End Set
    End Property
    Public Property NoPiezas As String
        Get
            Return _NoPiezas
        End Get
        Set(ByVal value As String)
            _NoPiezas = value
        End Set
    End Property
    Public Property Modelo As String
        Get
            Return _Modelo
        End Get
        Set(ByVal value As String)
            _Modelo = value
        End Set
    End Property
    'Public ReadOnly Property Existe As Boolean
    '    Get
    '        Return _Existe
    '    End Get
    'End Property
    Public Property Existe As Boolean
        Get
            Return _Existe
        End Get
        Set(ByVal value As Boolean)
            _Existe = value
        End Set
    End Property

#End Region

#Region "Funciones"
    Public Function GetCambios(ByVal vNomHerramienta As String, ByVal vMedida As String, ByVal vidFamilia As Integer, _
    ByVal vidTipoHerramienta As Integer, ByVal vidMarca As String, ByVal vNoPiezas As String, ByVal vModelo As String) As String

        Dim CadCam As String = ""
        If _NomHerramienta <> vNomHerramienta Then
            CadCam += "NomHerramienta: '" & _NomHerramienta & "' Cambia a [" & vNomHerramienta & "],"
        End If
        If _Medida <> vMedida Then
            CadCam += "Medida: '" & _Medida & "' Cambia a [" & vMedida & "],"
        End If
        If _Modelo <> vModelo Then
            CadCam += "Modelo: '" & _Modelo & "' Cambia a [" & vModelo & "],"
        End If
        If _idFamilia <> vidFamilia Then
            CadCam += "idFamilia: '" & _idFamilia & "' Cambia a [" & vidFamilia & "],"
        End If
        If _NoPiezas <> vNoPiezas Then
            CadCam += "NoPiezas: '" & _NoPiezas & "' Cambia a [" & vNoPiezas & "],"
        End If
        If _idTipoHerramienta <> vidTipoHerramienta Then
            CadCam += "idTipoHerramienta: '" & _idTipoHerramienta & "' Cambia a [" & vidTipoHerramienta & "],"
        End If
        If _idMarca <> vidMarca Then
            CadCam += "idMarca: '" & _idMarca & "' Cambia a [" & vidMarca & "],"
        End If
        If CadCam <> "" Then
            CadCam = Mid(CadCam, 1, CadCam.Length - 1)
        End If
        Return CadCam
    End Function
    '
    Public Sub Guardar(ByVal bEsIdentidad As Boolean, ByVal vNomHerramienta As String, ByVal vMedida As String, ByVal vidFamilia As Integer, _
    ByVal vidTipoHerramienta As Integer, ByVal vidMarca As String, ByVal vNoPiezas As String, ByVal vModelo As String)

        Dim sSql As String = "", _CadValAnt As String = "", _CadValNue As String = ""
        If _Existe Then
            If _NomHerramienta <> vNomHerramienta Then
                sSql += "NomHerramienta = '" & vNomHerramienta & "',"
                _CadValAnt += "NomHerramienta =" & vNomHerramienta & ","
            End If
            If _Medida <> vMedida Then
                sSql += "Medida = '" & vMedida & "',"
                _CadValAnt += "Medida =" & vMedida & ","
            End If
            If _Modelo <> vModelo Then
                sSql += "Modelo = '" & vModelo & "',"
                _CadValAnt += "Modelo =" & vModelo & ","
            End If
            If _idFamilia <> vidFamilia Then
                sSql += "idFamilia = " & vidFamilia & ","
                _CadValAnt += "idFamilia =" & vidFamilia & ","
            End If
            If _idMarca <> vidMarca Then
                sSql += "idMarca = " & vidMarca & ","
                _CadValAnt += "idMarca =" & vidMarca & ","
            End If
            If _idTipoHerramienta <> vidTipoHerramienta Then
                sSql += "idTipoHerramienta = " & vidTipoHerramienta & ","
                _CadValAnt += "idTipoHerramienta =" & vidTipoHerramienta & ","
            End If
            If _NoPiezas <> vNoPiezas Then
                sSql += "NoPiezas = '" & vNoPiezas & "',"
                _CadValAnt += "NoPiezas =" & vNoPiezas & ","
            End If
          

            If sSql <> "" Then
                sSql = Mid(sSql, 1, sSql.Length - 1) 'Para quitar la Coma ','
                _CadValAnt = "idHerramienta=" & _idHerramienta & "," & _CadValAnt
                _CadValAnt = Mid(_CadValAnt, 1, _CadValAnt.Length - 1)
                _CadValNue = Replace(sSql, "'", "")

                sSql = "UPDATE " & _TablaBd & " SET " & sSql & " WHERE idHerramienta = '" & _idHerramienta & "'"
                Try
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                    BD.Execute(sSql, True)
                    Audita(_TablaBd, _CadValAnt, _CadValNue, EnTipAccion.Modifica)
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
                Catch ex As Exception
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                    Throw New Exception(ex.Message)
                End Try
            End If
        Else
            If bEsIdentidad Then

                sSql = "INSERT INTO " & _TablaBd & "(NomHerramienta, Medida, idFamilia, idTipoHerramienta, idMarca, NoPiezas,Modelo) " & _
                "VALUES('" & vNomHerramienta & "','" & vMedida & "'," & vidFamilia & "," & vidTipoHerramienta & "," & vidMarca & _
                ",'" & vNoPiezas & "','" & vModelo & "')"

                _CadValNue = "NomHerramienta=" & vNomHerramienta & _
                "Medida = " & vMedida & _
                "idFamilia = " & vidFamilia & _
                "idTipoHerramienta = " & vidTipoHerramienta & _
                "idMarca = " & vidMarca & _
                "NoPiezas = " & vNoPiezas & _
                "Modelo = " & vModelo
            Else

                sSql = "INSERT INTO " & _TablaBd & "(idHerramienta,NomHerramienta, Medida, idFamilia, idTipoHerramienta, idMarca, NoPiezas,Modelo) " & _
                  "VALUES(" & _idHerramienta & ",'" & vNomHerramienta & "','" & vMedida & "'," & vidFamilia & "," & vidTipoHerramienta & "," & vidMarca & _
                  ",'" & vNoPiezas & "','" & vModelo & "')"

                _CadValNue = "idHerramienta=" & _idHerramienta & _
                "NomHerramienta=" & vNomHerramienta & _
                "Medida = " & vMedida & _
                "idFamilia = " & vidFamilia & _
                "idTipoHerramienta = " & vidTipoHerramienta & _
                "idMarca = " & vidMarca & _
                "NoPiezas = " & vNoPiezas & _
                "Modelo = " & vModelo

            End If

            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, "", _CadValNue, EnTipAccion.Inserta)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
        End If

        _NomHerramienta = vNomHerramienta
        _Medida = vMedida
        _idFamilia = vidFamilia
        _idTipoHerramienta = vidTipoHerramienta
        _idMarca = vidMarca
        _NoPiezas = vNoPiezas
        _Modelo = vModelo
    End Sub
    Public Sub Eliminar()
        If _Existe Then
            Dim sSql As String = "", _CadValAnt As String = ""
            '_CadValAnt = "idEmpresa=" & _idHerramienta & " , NomSucursal =" & _NomHerramienta

            _CadValAnt = "idHerramienta=" & _idHerramienta & _
                "NomHerramienta=" & _NomHerramienta & _
                "Medida = " & _Medida & _
                "idFamilia = " & _idFamilia & _
                "idTipoHerramienta = " & _idTipoHerramienta & _
                "idMarca = " & _idMarca & _
                "NoPiezas = " & _NoPiezas & _
                "Modelo = " & _Modelo

            sSql = "DELETE FROM " & _TablaBd & " WHERE idHerramienta = '" & _idHerramienta & "'"
            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, _CadValAnt, "", EnTipAccion.Elimina)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
            _Existe = False
        Else
            'se supone que nunca debe entrar aqui. . .
            MessageBox.Show("La Herramienta con Id: " & _idHerramienta & " No existe por lo tanto no se puede Eliminar!!!",
                            "No se puede eliminar la Herramienta porque no Existe", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub
#End Region
End Class
