﻿Public Class CatDiseniosClass

    Private _idDisenio As Integer
        Private _Descripcion As String
        Private _Activo As Boolean

        Private _Existe As Boolean
        Private _TablaBd As String = "CatDisenios"

        Dim StrSql As String
        Dim DsClase As New DataTable
        Dim MyTabla As DataTable = New DataTable(_TablaBd)

#Region "Constructor"
        Sub New(ByVal vidDisenio As Integer)

            Dim filtro As String = " where idDisenio = " & vidDisenio

            StrSql = "SELECT di.idDisenio, " &
            "di.Descripcion, " &
            "di.Activo " &
            "from " & _TablaBd & " di" & filtro

            _idDisenio = vidDisenio
            DsClase = BD.ExecuteReturn(StrSql)
            If DsClase.Rows.Count > 0 Then
                With DsClase.Rows(0)
                    _Descripcion = .Item("Descripcion") & ""
                    _Activo = .Item("Activo")
                End With
                _Existe = True
            Else
                _Existe = False
            End If
        End Sub

        Sub New(ByVal vidDisenio As Integer, ByVal vDescripcion As String, ByVal vActivo As Boolean)
            _idDisenio = vidDisenio
            _Descripcion = vDescripcion
            _Activo = vActivo
            _Existe = True
        End Sub
#End Region

#Region "Propiedades"
        Public Property idDisenio As Integer
            Get
                Return _idDisenio
            End Get
            Set(ByVal value As Integer)
                _idDisenio = value
            End Set
        End Property
        Public Property Descripcion As String
            Get
                Return _Descripcion
            End Get
            Set(ByVal value As String)
                _Descripcion = value
            End Set
        End Property
        Public Property Activo As Boolean
            Get
                Return _Activo
            End Get
            Set(ByVal value As Boolean)
                _Activo = value
            End Set
        End Property
        Public Property Existe As Boolean
            Get
                Return _Existe
            End Get
            Set(ByVal value As Boolean)
                _Existe = value
            End Set
        End Property
#End Region

#Region "Funciones"
        Public Function GetCambios(ByVal vDescripcion As String, ByVal vActivo As Boolean) As String

            Dim CadCam As String = ""

            If _Descripcion <> vDescripcion Then

                CadCam += "Descripcion: '" & _Descripcion & "' Cambia a [" & vDescripcion & "],"
            End If
            If _Activo <> vActivo Then
                CadCam += "Activo: '" & _Activo & "' Cambia a [" & vActivo & "],"
            End If

            If CadCam <> "" Then
                CadCam = Mid(CadCam, 1, CadCam.Length - 1)
            End If
            Return CadCam
        End Function

        Public Sub Guardar(ByVal bEsIdentidad As Boolean, ByVal vDescripcion As String, ByVal vActivo As Boolean)
            Dim sSql As String = "", _CadValAnt As String = "", _CadValNue As String = ""
            If _Existe Then
                If _Descripcion <> vDescripcion Then
                    sSql += "Descripcion = '" & vDescripcion & "',"
                    _CadValAnt += "Descripcion =" & vDescripcion & ","
                End If
                If _Activo <> vActivo Then
                    sSql += "Activo = '" & IIf(vActivo, 1, 0) & "',"
                    _CadValAnt += "Activo =" & vActivo & ","
                End If



                If sSql <> "" Then
                    sSql = Mid(sSql, 1, sSql.Length - 1) 'Para quitar la Coma ','
                    _CadValAnt = "idDisenio=" & _idDisenio & "," & _CadValAnt
                    _CadValAnt = Mid(_CadValAnt, 1, _CadValAnt.Length - 1)
                    _CadValNue = Replace(sSql, "'", "")
                    sSql = "UPDATE " & _TablaBd & " SET " & sSql & " WHERE idDisenio = '" & _idDisenio & "'"
                    Try
                        BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                        BD.Execute(sSql, True)
                        Audita(_TablaBd, _CadValAnt, _CadValNue, EnTipAccion.Modifica)
                        BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
                    Catch ex As Exception
                        BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                        Throw New Exception(ex.Message)
                    End Try
                End If
            Else
                If bEsIdentidad Then
                    sSql = "INSERT INTO " & _TablaBd & "(Descripcion,Activo) VALUES('" & vDescripcion & "'," & IIf(vActivo, 1, 0) & ")"

                    _CadValNue = "Descripcion=" & vDescripcion & ", Activo = " & vActivo
                Else

                    sSql = "INSERT INTO " & _TablaBd & "(idDisenio,Descripcion,Activo) VALUES(" & _idDisenio & "','" & vDescripcion & "'," & IIf(vActivo, 1, 0) & ")"

                    _CadValNue = "idDisenio = " & _idDisenio & ", Descripcion=" & vDescripcion & ",Activo = " & vActivo
                End If

                Try
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                    BD.Execute(sSql, True)
                    Audita(_TablaBd, "", _CadValNue, EnTipAccion.Inserta)
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
                Catch ex As Exception
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                    Throw New Exception(ex.Message)
                End Try
            End If
            _Descripcion = vDescripcion
        End Sub
        Public Sub Eliminar()
            If _Existe Then
                Dim sSql As String = "", _CadValAnt As String = ""
                _CadValAnt = "idDisenio = " & _idDisenio & ", Descripcion=" & _Descripcion & ",Activo = " & _Activo
                sSql = "DELETE FROM " & _TablaBd & " WHERE idDisenio = " & _idDisenio
                Try
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                    BD.Execute(sSql, True)
                    Audita(_TablaBd, _CadValAnt, "", EnTipAccion.Elimina)
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
                Catch ex As Exception
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                    Throw New Exception(ex.Message)
                End Try
                _Existe = False
            Else
                'se supone que nunca debe entrar aqui. . .
                MessageBox.Show("El Diseño con Id: " & _idDisenio & " No existe por lo tanto no se puede Eliminar!!!",
                            "No se puede eliminar el Diseño porque no Existe", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
        End Sub
#End Region
#Region "Consultas"
    Public Function TablaDisenios(ByVal bIncluyeTodos As Boolean, Optional ByVal FiltroSinWhere As String = "") As DataTable
        Dim filtro As String = ""
        MyTabla.Rows.Clear()

        If FiltroSinWhere <> "" Then
            filtro = " where " & FiltroSinWhere
        End If

        If bIncluyeTodos Then
            StrSql = "SELECT 0 AS idDisenio,'TODOS' AS Descripcion " &
            "UNION " &
            "SELECT idDisenio,Descripcion FROM dbo." & _TablaBd & " " & filtro &
            " ORDER BY idDisenio"
        Else
            StrSql = "SELECT idDisenio,Descripcion FROM dbo." & _TablaBd & " " & filtro &
            " ORDER BY idDisenio"
        End If
        MyTabla = BD.ExecuteReturn(StrSql)

        Return MyTabla

    End Function
#End Region
End Class


