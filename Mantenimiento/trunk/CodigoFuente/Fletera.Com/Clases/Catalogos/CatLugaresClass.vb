﻿Public Class CatLugaresClass
    Private _idLugar As Integer
    Private _Descripcion As String
    Private _idTipOrdServ As Integer
    Private _NomTabla As String
    Private _Activo As Boolean

    Private _Existe As Boolean
    Private _TablaBd As String = "CatLugares"

    Dim StrSql As String
    Dim DsClase As New DataTable
    Dim MyTabla As DataTable = New DataTable(_TablaBd)

#Region "Constructor"
    Sub New(ByVal vidLugar As Integer, Optional ByVal vidTipOrdServ As Integer = 0)

        Dim filtro As String
        If vidTipOrdServ > 0 Then
            filtro = " where idLugar = " & vidLugar & " and idTipOrdServ = " & vidTipOrdServ
        Else
            filtro = " where idLugar = " & vidLugar
        End If

        StrSql = "SELECT lu.idLugar, " &
        "lu.Descripcion, " &
        "lu.idTipOrdServ, " &
        "isnull(lu.NomTabla,'') as NomTabla, " &
        "lu.Activo " &
        "from " & _TablaBd & " lu" & filtro

        _idLugar = vidLugar

        DsClase = BD.ExecuteReturn(StrSql)
        If DsClase.Rows.Count > 0 Then
            With DsClase.Rows(0)
                _Descripcion = .Item("Descripcion") & ""
                _idTipOrdServ = .Item("idTipOrdServ")
                _NomTabla = .Item("NomTabla")
                _Activo = .Item("Activo")

            End With
            _Existe = True
        Else
            _Existe = False
        End If
    End Sub

    Sub New(ByVal vidLugar As Integer, ByVal vDescripcion As String, ByVal vidTipOrdServ As Integer, ByVal vActivo As Boolean,
    ByVal vNomTabla As String)
        _idLugar = vidLugar
        _Descripcion = vDescripcion
        _idTipOrdServ = vidTipOrdServ
        _NomTabla = vNomTabla
        _Activo = vActivo
        _Existe = True
    End Sub
#End Region

#Region "Propiedades"
    Public Property idLugar As Integer
        Get
            Return _idLugar
        End Get
        Set(ByVal value As Integer)
            _idLugar = value
        End Set
    End Property
    Public Property Descripcion As String
        Get
            Return _Descripcion
        End Get
        Set(ByVal value As String)
            _Descripcion = value
        End Set
    End Property
    Public Property idTipOrdServ As Integer
        Get
            Return _idTipOrdServ
        End Get
        Set(ByVal value As Integer)
            _idTipOrdServ = value
        End Set
    End Property
    Public Property NomTabla As String
        Get
            Return _NomTabla
        End Get
        Set(ByVal value As String)
            _NomTabla = value
        End Set
    End Property
    Public Property Activo As Boolean
        Get
            Return _Activo
        End Get
        Set(ByVal value As Boolean)
            _Activo = value
        End Set
    End Property
    Public Property Existe As Boolean
        Get
            Return _Existe
        End Get
        Set(ByVal value As Boolean)
            _Existe = value
        End Set
    End Property
#End Region

#Region "Funciones"
    Public Function GetCambios(ByVal vDescripcion As String, ByVal vidTipOrdServ As Integer, ByVal vActivo As Boolean, ByVal vNomTabla As String) As String

        Dim CadCam As String = ""

        If _Descripcion <> vDescripcion Then

            CadCam += "Descripcion: '" & _Descripcion & "' Cambia a [" & vDescripcion & "],"
        End If
        If _idTipOrdServ <> vidTipOrdServ Then
            CadCam += "idTipOrdServ: '" & _idTipOrdServ & "' Cambia a [" & vidTipOrdServ & "],"
        End If
        If _NomTabla <> vNomTabla Then
            CadCam += "NomTabla: '" & _NomTabla & "' Cambia a [" & vNomTabla & "],"
        End If
        If _Activo <> vActivo Then
            CadCam += "Activo: '" & _Activo & "' Cambia a [" & vActivo & "],"
        End If

        If CadCam <> "" Then
            CadCam = Mid(CadCam, 1, CadCam.Length - 1)
        End If
        Return CadCam
    End Function

    Public Sub Guardar(ByVal bEsIdentidad As Boolean, ByVal vDescripcion As String, ByVal vidTipOrdServ As Integer,
                       ByVal vActivo As Boolean, ByVal vNomTabla As String)
        Dim sSql As String = "", _CadValAnt As String = "", _CadValNue As String = ""
        If _Existe Then
            If _Descripcion <> vDescripcion Then
                sSql += "Descripcion = '" & vDescripcion & "',"
                _CadValAnt += "Descripcion =" & vDescripcion & ","
            End If
            If _idTipOrdServ <> vidTipOrdServ Then
                sSql += "idTipOrdServ = " & vidTipOrdServ & ","
                _CadValAnt += "idTipOrdServ =" & vidTipOrdServ & ","
            End If
            If _NomTabla <> vNomTabla Then
                sSql += "NomTabla = '" & vNomTabla & "',"
                _CadValAnt += "NomTabla =" & vNomTabla & ","
            End If
            If _Activo <> vActivo Then
                sSql += "Activo = '" & IIf(vActivo, 1, 0) & "',"
                _CadValAnt += "Activo =" & vActivo & ","
            End If



            If sSql <> "" Then
                sSql = Mid(sSql, 1, sSql.Length - 1) 'Para quitar la Coma ','
                _CadValAnt = "idLugar=" & _idLugar & "," & _CadValAnt
                _CadValAnt = Mid(_CadValAnt, 1, _CadValAnt.Length - 1)
                _CadValNue = Replace(sSql, "'", "")
                sSql = "UPDATE " & _TablaBd & " SET " & sSql & " WHERE idLugar = '" & _idLugar & "'"
                Try
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                    BD.Execute(sSql, True)
                    Audita(_TablaBd, _CadValAnt, _CadValNue, EnTipAccion.Modifica)
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
                Catch ex As Exception
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                    Throw New Exception(ex.Message)
                End Try
            End If
        Else
            If bEsIdentidad Then
                sSql = "INSERT INTO " & _TablaBd & "(Descripcion,idTipOrdServ,Activo,NomTabla) VALUES('" & vDescripcion & "'," & vidTipOrdServ & "," & IIf(vActivo, 1, 0) & ",'" & vNomTabla & "')"

                _CadValNue = "Descripcion=" & vDescripcion & ", idTipOrdServ = " & vidTipOrdServ & ",Activo = " & vActivo & ",NomTabla = " & vNomTabla

            Else

                sSql = "INSERT INTO " & _TablaBd & "(idLugar,Descripcion,idTipOrdServ,Activo,NomTabla) VALUES(" & _idLugar & "','" & vDescripcion & "'," & vidTipOrdServ & "," & IIf(vActivo, 1, 0) & ",'" & vNomTabla & "')"

                _CadValNue = "idLugar = " & _idLugar & ", Descripcion=" & vDescripcion & ", idTipOrdServ = " & vidTipOrdServ &
                    ",Activo = " & vActivo & ",NomTabla = " & vNomTabla
            End If

            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, "", _CadValNue, EnTipAccion.Inserta)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
        End If
        _Descripcion = vDescripcion
    End Sub
    Public Sub Eliminar()
        If _Existe Then
            Dim sSql As String = "", _CadValAnt As String = ""
            _CadValAnt = "idlugar = " & _idLugar & ", Descripcion=" & _Descripcion & ", idTipOrdServ = " & _idTipOrdServ & ",Activo = " & _Activo & ",NomTabla = " & _NomTabla
            sSql = "DELETE FROM " & _TablaBd & " WHERE idlugar = " & _idLugar
            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, _CadValAnt, "", EnTipAccion.Elimina)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
            _Existe = False
        Else
            'se supone que nunca debe entrar aqui. . .
            MessageBox.Show("El Lugar con Id: " & _idLugar & " No existe por lo tanto no se puede Eliminar!!!",
                            "No se puede eliminar el Lugar porque no Existe", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub
#End Region
#Region "Consultas"
    Public Function TablaLugares(ByVal bIncluyeTodos As Boolean, Optional ByVal FiltroSinWhere As String = "") As DataTable
        Dim filtro As String = ""
        MyTabla.Rows.Clear()

        If FiltroSinWhere <> "" Then
            filtro = " where " & FiltroSinWhere
        End If

        If bIncluyeTodos Then
            StrSql = "SELECT 0 AS idLugar,'TODOS' AS Descripcion " &
            "UNION " &
            "SELECT idLugar,Descripcion FROM dbo." & _TablaBd & " " & filtro &
            " ORDER BY idLugar"
        Else
            StrSql = "SELECT idLugar,Descripcion FROM dbo." & _TablaBd & " " & filtro &
            " ORDER BY idLugar"
        End If
        MyTabla = BD.ExecuteReturn(StrSql)

        Return MyTabla

    End Function
#End Region
End Class
