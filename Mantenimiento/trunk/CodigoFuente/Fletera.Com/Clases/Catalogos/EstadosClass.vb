﻿Public Class EstadosClass
    'idEstado       vidEstado
    'NomEstado      vNomEstado
    'idPais         vidPais
    'NomPais        vNomPais
    Private _idEstado As Integer
    Private _NomEstado As String
    Private _idPais As Integer
    Private _NomPais As String

    Private _Existe As Boolean
    Private _TablaBd As String = "catEstados"

    Dim StrSql As String
    Dim DsClase As New DataTable

    Dim MyTabla As DataTable = New DataTable(_TablaBd)
#Region "Constructor"
    Sub New(ByVal vidEstado As Integer)
        'Dim DtEst As New DataTable
        _idEstado = vidEstado

        StrSql = "select est.idEstado, est.NomEstado, est.idPais, pais.NomPais " & _
        "from " & _TablaBd & " Est " & _
        "inner join catpais pais on est.idPais = pais.idPais " & _
        "where est.idEstado = " & vidEstado

        DsClase = BD.ExecuteReturn(StrSql)
        If DsClase.Rows.Count > 0 Then
            With DsClase.Rows(0)
                _NomEstado = .Item("NomEstado") & ""
                _idPais = .Item("idPais")
                _NomPais = .Item("NomPais")
            End With
            _Existe = True
        Else
            _Existe = False
        End If

    End Sub
    Sub New(ByVal vidEstado As Integer, ByVal vNomEstado As String, ByVal vidPais As Integer, ByVal vNomPais As String)
        _idEstado = vidEstado
        _NomEstado = vNomEstado
        _idPais = vidPais
        _NomPais = vNomPais
        _Existe = True
    End Sub
#End Region
#Region "Propiedades"

    Public Property idEstado As Integer
        Get
            Return _idEstado
        End Get
        Set(ByVal value As Integer)
            _idEstado = value
        End Set
    End Property
    Public Property NomEstado As String
        Get
            Return _NomEstado
        End Get
        Set(ByVal value As String)
            _NomEstado = value
        End Set
    End Property
    Public Property idPais As Integer
        Get
            Return _idPais
        End Get
        Set(ByVal value As Integer)
            _idPais = value
        End Set
    End Property
    Public Property NomPais As String
        Get
            Return _NomPais
        End Get
        Set(ByVal value As String)
            _NomPais = value
        End Set
    End Property
    'Public ReadOnly Property Existe As Boolean
    '    Get
    '        Return _Existe
    '    End Get
    'End Property
    Public Property Existe As Boolean
        Get
            Return _Existe
        End Get
        Set(ByVal value As Boolean)
            _Existe = value
        End Set
    End Property

#End Region
#Region "Funciones"
    Public Function GetCambios(ByVal vNomEstado As String, ByVal vidPais As Integer) As String
        Dim CadCam As String = ""
        If _NomEstado <> vNomEstado Then
            CadCam += "Nombre Estado: '" & _NomEstado & "' Cambia a [" & vNomEstado & "],"
        End If
        If _idPais <> vidPais Then
            CadCam += "id Pais: '" & _idPais & "' Cambia a [" & vidPais & "],"
        End If

        If CadCam <> "" Then
            CadCam = Mid(CadCam, 1, CadCam.Length - 1)
        End If
        Return CadCam
    End Function
    Public Sub Guardar(ByVal vNomEstado As String, ByVal vidPais As Integer, ByVal bEsIdentidad As Boolean, ByVal vNomPais As String)
        Dim sSql As String = "", _CadValAnt As String = "", _CadValNue As String = ""
        If _Existe Then
            If _NomEstado <> vNomEstado Then
                sSql += "NomEstado = '" & vNomEstado & "',"
                _CadValAnt += "NomEstado =" & vNomEstado & ","
            End If
            If _idPais <> vidPais Then
                sSql += "idPais = " & vidPais & ","
                _CadValAnt += "idPais =" & vidPais & ","
            End If
            If sSql <> "" Then
                sSql = Mid(sSql, 1, sSql.Length - 1) 'Para quitar la Coma ','
                _CadValAnt = "idEstado=" & _idEstado & "," & _CadValAnt
                _CadValAnt = Mid(_CadValAnt, 1, _CadValAnt.Length - 1)
                _CadValNue = Replace(sSql, "'", "")

                sSql = "UPDATE " & _TablaBd & " SET " & sSql & " WHERE idEstado = '" & _idEstado & "'"
                Try
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                    BD.Execute(sSql, True)
                    Audita(_TablaBd, _CadValAnt, _CadValNue, EnTipAccion.Modifica)
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
                Catch ex As Exception
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                    Throw New Exception(ex.Message)
                End Try
            End If
        Else
            If bEsIdentidad Then
                sSql = "INSERT INTO " & _TablaBd & "(NomEstado,idPais) VALUES('" & vNomEstado & "'," & vidPais & ")"

                _CadValNue = "NomEstado=" & vNomEstado & "idPais = " & vidPais
            Else

                sSql = "INSERT INTO " & _TablaBd & "(idEstado,NomEstado,idPais) VALUES(" &
                "'" & _idEstado & "','" & vNomEstado & "'," & vidPais & ")"

                _CadValNue = "idCiudad=" & _idEstado & ",NomEstado=" & vNomEstado & ",idPais=" & vidPais
            End If

            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, "", _CadValNue, EnTipAccion.Inserta)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
        End If
        _NomEstado = vNomEstado
        _idPais = vidPais
        _NomPais = vNomPais
    End Sub
    Public Sub Eliminar()
        If _Existe Then
            Dim sSql As String = "", _CadValAnt As String = ""
            _CadValAnt = "idEstado=" & _idEstado & " , NomEstado =" & _NomEstado
            sSql = "DELETE FROM " & _TablaBd & " WHERE idEstado = '" & _idEstado & "'"
            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, _CadValAnt, "", EnTipAccion.Elimina)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
            _Existe = False
        Else
            'se supone que nunca debe entrar aqui. . .
            MessageBox.Show("El Estado con Id: " & _idEstado & " No existe por lo tanto no se puede Eliminar!!!",
                            "No se puede eliminar el Estado porque no Existe", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub
#End Region

#Region "Consultas"
    Public Function TablaEstados(ByVal bIncluyeTodos As Boolean, Optional ByVal FiltroSinWhere As String = "") As DataTable
        Dim filtro As String = ""
        MyTabla.Rows.Clear()

        If FiltroSinWhere <> "" Then
            filtro = " where " & FiltroSinWhere
        End If

        If bIncluyeTodos Then
            StrSql = "SELECT 0 AS idEstado,'TODOS' AS NomEstado,0 AS idPais " &
            "UNION " &
            "SELECT idEstado,NomEstado,idPais FROM dbo.CatEstados " & filtro &
            " ORDER BY idEstado"
        Else
            StrSql = "SELECT idEstado,NomEstado,idPais FROM dbo.CatEstados " & filtro &
            " ORDER BY idEstado"
        End If
        MyTabla = BD.ExecuteReturn(StrSql)

        Return MyTabla

    End Function
#End Region
End Class
