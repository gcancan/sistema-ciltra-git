﻿Public Class CatServFactClass

    Private _CIDPRODUCTO As Integer
    Private _CNOMBREPRODUCTO As String
    Private _CCODIGOPRODUCTO As String
    Private _IdCliente As Integer
    Private _RazonSocial As String
    Private _TipoViaje As String
    Private _idEstado As Integer
    Private _NomEstado As String
    Private _Activo As Boolean


    Private _Existe As Boolean
    Private _TablaBd As String = "CatServFact"

    Dim StrSql As String
    Dim DsClase As New DataTable

#Region "Constructor"
    Sub New(ByVal vCCODIGOPRODUCTO As String)
        _CCODIGOPRODUCTO = vCCODIGOPRODUCTO


        StrSql = "SELECT sf.CCODIGOPRODUCTO, " &
        "sf.IdCliente, " &
        "cli.Nombre AS RazonSocial, " &
        "sf.CIDPRODUCTO, " &
        "sf.CNOMBREPRODUCTO, " &
        "sf.TipoViaje, " &
        "sf.idEstado, " &
        "est.NomEstado, " &
        "sf.Activo " &
        "FROM dbo." & _TablaBd & " sf " &
        "INNER JOIN dbo.CatClientes cli ON sf.IdCliente = cli.IdCliente " &
        "INNER JOIN dbo.CatEstados est ON est.idEstado = sf.idEstado " &
        "WHERE sf.CCODIGOPRODUCTO = '" & vCCODIGOPRODUCTO & "'"

        DsClase = BD.ExecuteReturn(StrSql)
        If DsClase.Rows.Count > 0 Then
            With DsClase.Rows(0)
                _CIDPRODUCTO = .Item("CIDPRODUCTO") & ""
                _CNOMBREPRODUCTO = .Item("CNOMBREPRODUCTO") & ""
                _CCODIGOPRODUCTO = .Item("CCODIGOPRODUCTO") & ""
                _IdCliente = .Item("IdCliente") & ""
                _RazonSocial = .Item("RazonSocial") & ""
                _TipoViaje = .Item("TipoViaje") & ""
                _idEstado = .Item("idEstado") & ""
                _NomEstado = .Item("NomEstado") & ""
                _Activo = .Item("Activo") & ""

            End With
            _Existe = True
        Else
            _Existe = False
        End If

    End Sub

    Sub New(ByVal vCIDPRODUCTO As Integer, ByVal vCNOMBREPRODUCTO As String, ByVal vCCODIGOPRODUCTO As String,
    ByVal vIdCliente As Integer, ByVal vTipoViaje As String, ByVal vidEstado As Integer, ByVal vActivo As Boolean)

        _CIDPRODUCTO = vCIDPRODUCTO
        _CNOMBREPRODUCTO = vCNOMBREPRODUCTO
        _CCODIGOPRODUCTO = vCCODIGOPRODUCTO
        _IdCliente = vIdCliente
        _TipoViaje = vTipoViaje
        _idEstado = vidEstado
        _Activo = vActivo

    End Sub

#End Region

#Region "Propiedades"
    Public Property CIDPRODUCTO As Integer
        Get
            Return _CIDPRODUCTO
        End Get
        Set(ByVal value As Integer)
            _CIDPRODUCTO = value
        End Set
    End Property
    Public Property CNOMBREPRODUCTO As String
        Get
            Return _CNOMBREPRODUCTO
        End Get
        Set(ByVal value As String)
            _CNOMBREPRODUCTO = value
        End Set
    End Property

    Public Property CCODIGOPRODUCTO As String
        Get
            Return _CCODIGOPRODUCTO
        End Get
        Set(ByVal value As String)
            _CCODIGOPRODUCTO = value
        End Set
    End Property

    Public Property IdCliente As Integer
        Get
            Return _IdCliente
        End Get
        Set(ByVal value As Integer)
            _IdCliente = value
        End Set
    End Property

    Public Property RazonSocial As String
        Get
            Return _RazonSocial
        End Get
        Set(ByVal value As String)
            _RazonSocial = value
        End Set
    End Property

    Public Property TipoViaje As String
        Get
            Return _TipoViaje
        End Get
        Set(ByVal value As String)
            _TipoViaje = value
        End Set
    End Property

    Public Property idEstado As Integer
        Get
            Return _idEstado
        End Get
        Set(ByVal value As Integer)
            _idEstado = value
        End Set
    End Property

    Public Property NomEstado As String
        Get
            Return _NomEstado
        End Get
        Set(ByVal value As String)
            _NomEstado = value
        End Set
    End Property

    Public Property Activo As Boolean
        Get
            Return _Activo
        End Get
        Set(ByVal value As Boolean)
            _Activo = value
        End Set
    End Property

    Public Property Existe As Boolean
        Get
            Return _Existe
        End Get
        Set(ByVal value As Boolean)
            _Existe = value
        End Set
    End Property
#End Region

#Region "Funciones"
    Public Function GetCambios(ByVal vCIDPRODUCTO As Integer, ByVal vCNOMBREPRODUCTO As String, ByVal vTipoViaje As String,
                               ByVal vidEstado As Integer, ByVal vActivo As Boolean)
        Dim CadCam As String = ""
        If _CIDPRODUCTO <> vCIDPRODUCTO Then
            CadCam += "CIDPRODUCTO: '" & _CIDPRODUCTO & "' Cambia a [" & vCIDPRODUCTO & "],"
        End If

        If _CNOMBREPRODUCTO <> vCNOMBREPRODUCTO Then
            CadCam += "CNOMBREPRODUCTO: '" & _CNOMBREPRODUCTO & "' Cambia a [" & vCNOMBREPRODUCTO & "],"
        End If

        If _TipoViaje <> vTipoViaje Then
            CadCam += "TipoViaje: '" & _TipoViaje & "' Cambia a [" & vTipoViaje & "],"
        End If

        If _idEstado <> vidEstado Then
            CadCam += "idEstado: '" & _idEstado & "' Cambia a [" & vidEstado & "],"
        End If

        If _Activo <> vActivo Then
            CadCam += "Activo: '" & _Activo & "' Cambia a [" & vActivo & "],"
        End If

        If CadCam <> "" Then
            CadCam = Mid(CadCam, 1, CadCam.Length - 1)
        End If
        Return CadCam

    End Function

    Public Sub Guardar(ByVal bEsIdentidad As Boolean, ByVal vCIDPRODUCTO As Integer, ByVal vCNOMBREPRODUCTO As String, ByVal vTipoViaje As String,
                               ByVal vidEstado As Integer, ByVal vActivo As Boolean)

        Dim sSql As String = "", _CadValAnt As String = "", _CadValNue As String = ""
        If _Existe Then
            If _CIDPRODUCTO <> vCIDPRODUCTO Then
                sSql += "CIDPRODUCTO = " & vCIDPRODUCTO & ","
                _CadValAnt += "CIDPRODUCTO =" & vCIDPRODUCTO & ","
            End If

            If _CNOMBREPRODUCTO <> vCNOMBREPRODUCTO Then
                sSql += "CNOMBREPRODUCTO = '" & vCNOMBREPRODUCTO & "',"
                _CadValAnt += "CNOMBREPRODUCTO =" & vCNOMBREPRODUCTO & ","
            End If

            If _TipoViaje <> vTipoViaje Then
                sSql += "TipoViaje = '" & vTipoViaje & "',"
                _CadValAnt += "TipoViaje =" & vTipoViaje & ","
            End If

            If _idEstado <> vidEstado Then
                sSql += "idEstado = " & _idEstado & ","
                _CadValAnt += "idEstado =" & _idEstado & ","
            End If

            If _Activo <> vActivo Then
                sSql += "Activo = " & IIf(vActivo, 1, 0) & ","
                _CadValAnt += "Activo =" & IIf(vActivo, 1, 0) & ","
            End If

            If sSql <> "" Then
                sSql = Mid(sSql, 1, sSql.Length - 1) 'Para quitar la Coma ','
                _CadValAnt = "IdCliente= " & _IdCliente & ", CCODIGOPRODUCTO= " & _CCODIGOPRODUCTO & "," & _CadValAnt
                _CadValAnt = Mid(_CadValAnt, 1, _CadValAnt.Length - 1)
                _CadValNue = Replace(sSql, "'", "")

                sSql = "UPDATE " & _TablaBd & " SET " & sSql & " WHERE CCODIGOPRODUCTO = '" & _CCODIGOPRODUCTO & "' and IdCliente = " & _IdCliente
                Try
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                    BD.Execute(sSql, True)
                    Audita(_TablaBd, _CadValAnt, _CadValNue, EnTipAccion.Modifica)
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
                Catch ex As Exception
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                    Throw New Exception(ex.Message)
                End Try
            End If
        Else
            If bEsIdentidad Then
                StrSql = "INSERT INTO dbo.CatServFact( CIDPRODUCTO ,CNOMBREPRODUCTO ,CCODIGOPRODUCTO ,IdCliente ,TipoViaje ,idEstado, Activo) " &
           "VALUES  ( " & vCIDPRODUCTO & " ,'" & vCNOMBREPRODUCTO & "' ,'" & _CCODIGOPRODUCTO & "' , " & _IdCliente & " , '" & vTipoViaje &
           "' , " & vidEstado & "," & IIf(vActivo, 1, 0) & ")"
            Else
                StrSql = "INSERT INTO dbo.CatServFact( CIDPRODUCTO ,CNOMBREPRODUCTO ,CCODIGOPRODUCTO ,IdCliente ,TipoViaje ,idEstado, Activo) " &
            "VALUES  ( " & vCIDPRODUCTO & " ,'" & vCNOMBREPRODUCTO & "' ,'" & _CCODIGOPRODUCTO & "' , " & _IdCliente & " , '" & vTipoViaje &
            "' , " & vidEstado & "," & IIf(vActivo, 1, 0) & ")"

            End If

            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, "", _CadValNue, EnTipAccion.Inserta)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try

        End If
        _CIDPRODUCTO = vCIDPRODUCTO
        _CNOMBREPRODUCTO = vCNOMBREPRODUCTO
        _TipoViaje = vTipoViaje
        _idEstado = vidEstado
        _Activo = vActivo


    End Sub
#End Region

End Class
