﻿Public Class ImpuestosClass
    Private _idImpuesto As Integer
    Private _IdEmpresa As Integer
    Private _Descripcion As String
    Private _Valor As Decimal
    Private _Activo As Boolean

    Private _Existe As Boolean
    Private _TablaBd As String = "CatImpuestos"

    Dim StrSql As String
    Dim DsClase As New DataTable

    Dim MyTabla As DataTable = New DataTable(_TablaBd)

#Region "Constructor"
    Sub New(ByVal vidImpuesto As Integer)
        Dim filtro As String = " where idImpuesto = " & vidImpuesto

        StrSql = "SELECT idImpuesto, " &
        "IdEmpresa, " &
        "Descripcion, " &
        "Valor, " &
        "Activo " &
        "FROM dbo.CatImpuestos " & filtro

        _idImpuesto = vidImpuesto

        DsClase = BD.ExecuteReturn(StrSql)
        If DsClase.Rows.Count > 0 Then
            With DsClase.Rows(0)
                _IdEmpresa = .Item("IdEmpresa")
                _Descripcion = .Item("Descripcion")
                _Valor = .Item("Valor")
                _Activo = .Item("Activo")

            End With
            _Existe = True
        Else
            _Existe = False
        End If

    End Sub
    Sub New(ByVal vidImpuesto As Integer, ByVal vIdEmpresa As Integer, ByVal vDescripcion As String, ByVal vValor As Decimal, ByVal vActivo As Boolean)
        _idImpuesto = vidImpuesto
        _IdEmpresa = vIdEmpresa
        _Descripcion = vDescripcion
        _Valor = vValor
        _Activo = vActivo
        _Existe = True
    End Sub
#End Region

#Region "Propiedades"
    Public Property idImpuesto As Integer
        Get
            Return _idImpuesto
        End Get
        Set(ByVal value As Integer)
            _idImpuesto = value
        End Set
    End Property
    Public Property idEmpresa As Integer
        Get
            Return _IdEmpresa
        End Get
        Set(ByVal value As Integer)
            _IdEmpresa = value
        End Set
    End Property
    Public Property Descripcion As String
        Get
            Return _Descripcion
        End Get
        Set(ByVal value As String)
            _Descripcion = value
        End Set
    End Property
    Public Property Valor As Decimal
        Get
            Return _Valor
        End Get
        Set(ByVal value As Decimal)
            _Valor = value
        End Set
    End Property

    Public Property Activo As Boolean
        Get
            Return _Activo
        End Get
        Set(ByVal value As Boolean)
            _Activo = value
        End Set
    End Property
    Public Property Existe As Boolean
        Get
            Return _Existe
        End Get
        Set(ByVal value As Boolean)
            _Existe = value
        End Set
    End Property
#End Region
#Region "Funciones"
    Public Function GetCambios(ByVal vIdEmpresa As Integer, ByVal vDescripcion As String, ByVal vValor As Decimal, ByVal vActivo As Boolean) As String
        Dim CadCam As String = ""
        If _IdEmpresa <> vIdEmpresa Then
            CadCam += "idEmpresa: '" & _IdEmpresa & "' Cambia a [" & vIdEmpresa & "],"
        End If
        If _Descripcion <> vDescripcion Then
            CadCam += "Descripcion: '" & _Descripcion & "' Cambia a [" & vDescripcion & "],"
        End If
        If _Valor <> vValor Then
            CadCam += "Valor: '" & _Valor & "' Cambia a [" & vValor & "],"
        End If
        If _Activo <> vActivo Then
            CadCam += "Activo: '" & _Activo & "' Cambia a [" & vActivo & "],"
        End If
        If CadCam <> "" Then
            CadCam = Mid(CadCam, 1, CadCam.Length - 1)
        End If
    End Function

    Public Sub Guardar(ByVal bEsIdentidad As Boolean, ByVal vIdEmpresa As Integer, ByVal vDescripcion As String, ByVal vValor As Decimal, ByVal vActivo As Boolean)
        Dim sSql As String = "", _CadValAnt As String = "", _CadValNue As String = ""
        If _Existe Then
            If _IdEmpresa <> vIdEmpresa Then
                sSql += "idEmpresa = " & vIdEmpresa & ","
                _CadValAnt += "idEmpresa =" & vIdEmpresa & ","
            End If
            If _Descripcion <> vDescripcion Then
                sSql += "Descripcion = '" & vDescripcion & "',"
                _CadValAnt += "Descripcion =" & vDescripcion & ","
            End If
            If _Valor <> vValor Then
                sSql += "Valor = " & vValor & ","
                _CadValAnt += "Valor =" & vValor & ","
            End If
            If _Activo <> vActivo Then
                sSql += "Activo = " & IIf(vActivo, 1, 0) & ","
                _CadValAnt += "Activo =" & IIf(vActivo, 1, 0) & ","
            End If
            If sSql <> "" Then
                sSql = Mid(sSql, 1, sSql.Length - 1) 'Para quitar la Coma ','
                _CadValAnt = "idImpuesto=" & _idImpuesto & "," & _CadValAnt
                _CadValAnt = Mid(_CadValAnt, 1, _CadValAnt.Length - 1)
                _CadValNue = Replace(sSql, "'", "")

                sSql = "UPDATE " & _TablaBd & " SET " & sSql & " WHERE idImpuesto = " & _idImpuesto

                Try
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                    BD.Execute(sSql, True)
                    Audita(_TablaBd, _CadValAnt, _CadValNue, EnTipAccion.Modifica)
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
                Catch ex As Exception
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                    Throw New Exception(ex.Message)
                End Try
            End If
        Else
            If bEsIdentidad Then
                StrSql = "insert into dbo.CatImpuestos (IdEmpresa,Descripcion,Valor,Activo) " &
                "values (" & vIdEmpresa & ",'" & vDescripcion & "'," & vValor & "," & vActivo & ")"
            Else
                StrSql = "insert into dbo.CatImpuestos (idImpuesto,IdEmpresa,Descripcion,Valor,Activo) " &
                "values (" & _idImpuesto & "," & vIdEmpresa & ",'" & vDescripcion & "'," & vValor & "," & vActivo & ")"

            End If
            _CadValNue = "idImpuesto=" & _idImpuesto & "IdEmpresa=" & vIdEmpresa & "Descripcion = " & vDescripcion &
            "Valor = " & vValor & "Activo = " & vActivo

            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, "", _CadValNue, EnTipAccion.Inserta)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try

        End If
    End Sub
    Public Sub Eliminar()
        If _Existe Then
            Dim sSql As String = "", _CadValAnt As String = ""

            _CadValAnt = "idImpuesto=" & _idImpuesto & "IdEmpresa=" & _IdEmpresa & "Descripcion = " & _Descripcion &
            "Valor = " & _Valor & "Activo = " & _Activo

            sSql = "DELETE FROM " & _TablaBd & " WHERE idImpuesto = " & _idImpuesto
            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, _CadValAnt, "", EnTipAccion.Elimina)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
            _Existe = False
        Else
            'se supone que nunca debe entrar aqui. . .
            MessageBox.Show("El Impuesto con Id: " & _idImpuesto & " No existe por lo tanto no se puede Eliminar!!!",
                            "No se puede eliminar el Impuesto porque no Existe", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub

#End Region
#Region "Consultas"
    Public Function TablaImpuestos(ByVal bIncluyeTodos As Boolean) As DataTable
        MyTabla.Rows.Clear()

        If bIncluyeTodos Then
            StrSql = "SELECT 0 AS idImpuesto,0 AS IdEmpresa, 'TODOS' AS Descripcion, 0 AS Valor, 1 AS Activo " &
            "UNION all " &
            "SELECT idImpuesto,IdEmpresa, Descripcion, Valor, Activo FROM dbo.CatImpuestos " &
            "ORDER BY idImpuesto"
        Else
            StrSql = "SELECT idImpuesto,IdEmpresa, Descripcion, Valor, Activo FROM dbo.CatImpuestos " &
            "ORDER BY idImpuesto"

        End If
        MyTabla = BD.ExecuteReturn(StrSql)

        Return MyTabla

    End Function
#End Region

End Class
