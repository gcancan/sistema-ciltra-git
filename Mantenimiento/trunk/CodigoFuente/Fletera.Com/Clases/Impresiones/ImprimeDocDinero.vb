﻿Imports System.Drawing.Printing

Public Class ImprimeDocDinero
    Dim ImpRep As New ImprimeRemision
    Private _opcion As TipoDocDinero
    Private _id As Integer
    Private _BandPreview As Boolean
    Private _banHorizontal As Boolean
    Private _nombreImpresora As String
    Private _cveEmpresa As Integer
    Private _NumGuiaId As Integer

    Dim objSql As New ToolSQLs
    Dim NumRegistros As Integer
    Dim NumRegistrosDET As Integer
    Dim MyTablaImprime As DataTable = New DataTable("IMPRIME")
    Dim DsRep As DataSet
    Dim DsRepDET As DataSet
    Dim myDataRow As DataRow
    Dim myDataColumn As DataColumn
    Private prtSettings As PrinterSettings
    Private prtDoc As PrintDocument

    Dim printFont As New System.Drawing.Font("Courier New", 7)
    Private lineaActual As Integer = 0
    Dim ContPaginas As Integer = 0
    Dim vText As String = "Documento Dinero"
    Dim MyPaperSize As New Printing.PaperSize("MediaCarta", 827, 584.5)

    Sub New(ByVal opcion As TipoDocDinero, ByVal id As Integer, ByVal BandPreview As Boolean,
            ByVal banHorizontal As Boolean, Optional ByVal nombreImpresora As String = "", Optional ByVal NumGuiaId As Integer = 0)
        _opcion = opcion
        _id = id
        _BandPreview = BandPreview
        _banHorizontal = banHorizontal
        _nombreImpresora = nombreImpresora
        _NumGuiaId = NumGuiaId
        'ConfiguraDocumento()

    End Sub

    Public Sub ConfiguraDocumento()
        ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 10)
        ImpRep.FuenteImprimeCampos3 = New System.Drawing.Font("Courier New", 10, FontStyle.Bold)
        ImpRep.FuenteImprimeCampos2 = New Font("Free 3 of 9 Extended", 40)

        ImpRep.Constr = Inicio.CONSTR
        ImpRep.TipoConexion = TipoConexion.TcSQL
        ImpRep.NomTabla = "ORDSERV"

        CreaTablaImprime()
        If _opcion = TipoDocDinero.Recibo Then
            ImpRep.StrSql = objSql.ImpReciboDinero(_id)
        ElseIf _opcion = TipoDocDinero.Folio Then
            ImpRep.StrSql = objSql.ImpFolioDinero(_id)

            If _NumGuiaId > 0 Then
                ImpRep.StrSqlDET = objSql.OrigenDestino(_NumGuiaId)
            End If




            '    reciboDinero = New RecibosDineroClass(0)
            'MyTablaOriDes = reciboDinero.tbOriDes(Viaje.NumGuiaId)

            'cmbOriDes.DataSource = Nothing
            'If MyTablaOriDes.Rows.Count > 0 Then
            '    cmbOriDes.DataSource = MyTablaOriDes
            '    cmbOriDes.DisplayMember = "OriDes"
            '    cmbOriDes.ValueMember = "OriDes"

            'End If

        End If

        NumRegistros = ImpRep.LlenaDataSet
        If NumRegistros > 0 Then
            If _NumGuiaId > 0 Then
                NumRegistrosDET = ImpRep.LlenaDataSetDET
                If NumRegistrosDET > 0 Then
                    ArmaTablaImpresion(_opcion)
                    ImpRep.TablaImprime = MyTablaImprime
                Else
                    'No se encontro Detalle
                End If
            Else
                ArmaTablaImpresion(_opcion)
                ImpRep.TablaImprime = MyTablaImprime

            End If



        End If

        If prtSettings Is Nothing Then
            prtSettings = New PrinterSettings
        End If

        If prtDoc Is Nothing Then
            prtDoc = New PrintDocument
            AddHandler prtDoc.PrintPage, AddressOf prt_PrintPage
        End If

        If ExisteColaImp = KEY_RCORRECTO Then
            If StrColaImpRecibo <> "" Then
                prtSettings.PrinterName = StrColaImpRecibo
                MsgBox(StrColaImpRecibo, MsgBoxStyle.Information, vText)
            End If
        End If

        prtDoc.PrinterSettings = prtSettings

        prtDoc.DefaultPageSettings.Landscape = _banHorizontal
        prtDoc.DefaultPageSettings.PaperSize = MyPaperSize

        If _BandPreview Then
            Dim prtPrev As New PrintPreviewDialog
            prtPrev.Document = prtDoc
            prtPrev.Text = "Previsualizar datos de " & vText
            prtPrev.WindowState = FormWindowState.Maximized
            prtPrev.Document.DefaultPageSettings.Landscape = _banHorizontal
            If _banHorizontal Then
                prtPrev.PrintPreviewControl.Zoom = 1
            Else
                prtPrev.PrintPreviewControl.Zoom = 1.5
            End If

            prtPrev.ShowDialog()
        Else
            prtDoc.PrinterSettings.PrinterName = _nombreImpresora
            prtDoc.Print()
        End If

    End Sub

    Private Sub ArmaTablaImpresion(ByVal opcion As TipoDocDinero)
        Dim campo As Integer
        Dim Descripcion As String
        Dim Valor As String
        Dim PosValorX, PosValorY, PosDescX, PosDescY As Single
        Dim NumAletra As New NumALetras
        Dim EnLetras As String = ""
        Dim Total As Double = 0

        'Dim Y1 As Single = 100
        Dim Y1 As Single = 50

        'Horizontal
        'Dim X1 As Single = 20
        'Dim X1A As Single = 120
        'Dim X1B As Single = 200
        'Dim X2 As Single = 320
        'Dim X3 As Single = 440
        'Dim X3A As Single = 540
        'Dim X4 As Single = 620

        'Vertical
        Dim X1 As Single = 20
        Dim X1A As Single = 120
        Dim X1B As Single = 200
        Dim X2 As Single = 160
        Dim X3 As Single = 220
        Dim X3A As Single = 260
        Dim X4 As Single = 400

        Dim XV1 As Single = 120
        Dim XV2 As Single = 320
        Dim XV3 As Single = 520
        Dim XV3A As Single = 580
        Dim XV4 As Single = 620


        Dim ValPad As Integer = 12
        Dim ObjNumLet As New NumALetras()


        DsRep = New DataSet
        DsRepDET = New DataSet
        'MyTablaImprime = New DataTable("IMPRIME")
        If MyTablaImprime.Rows.Count > 0 Then
            MyTablaImprime.Rows.Clear()
        End If


        DsRep = ImpRep.LlenaDsImprime(ImpRep.NomTabla)
        If DsRep.Tables(ImpRep.NomTabla).DefaultView.Count > 0 Then

            For i As Integer = 0 To DsRep.Tables(ImpRep.NomTabla).DefaultView.Count - 1
                If opcion = TipoDocDinero.Folio Then
                    'NO FOLIO
                    'Codigo de Barras
                    campo += 1
                    Descripcion = ""
                    Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("Folio"))
                    PosDescX = XV4
                    PosDescY = Y1
                    PosValorX = XV4 + 50
                    PosValorY = PosDescY
                    InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 2)

                    'No. Recibo
                    campo += 1
                    Descripcion = "No. FOLIO "
                    Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("Folio"))
                    PosDescX = XV4
                    PosDescY = PosDescY + 50
                    PosValorX = XV4 + 100
                    PosValorY = PosDescY
                    InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

                    'Codigo de Barras
                    campo += 1
                    Descripcion = ""
                    Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("NoRecibo"))
                    PosDescX = XV4
                    PosDescY = PosDescY + 30
                    PosValorX = XV4 + 50
                    PosValorY = PosDescY
                    InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 2)
                ElseIf opcion = TipoDocDinero.Recibo Then
                    'Codigo de Barras
                    campo += 1
                    Descripcion = ""
                    Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("NoRecibo"))
                    PosDescX = XV4
                    PosDescY = Y1
                    PosValorX = XV4 + 50
                    PosValorY = PosDescY
                    InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 2)

                End If
                'No. Recibo
                campo += 1
                Descripcion = "No. RECIBO "
                Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("NoRecibo"))
                PosDescX = XV4
                PosDescY = PosDescY + 50
                PosValorX = XV4 + 100
                PosValorY = PosDescY
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)



                'LINEA 5
                campo += 1
                Descripcion = "FECHA:"
                Valor = CDate(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("Fecha")).ToShortDateString
                PosDescY = PosDescY + 30 : PosValorY = PosDescY
                PosDescX = XV3A : PosValorX = PosDescX + 55
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

                'LINEA 5.1
                campo += 1
                Descripcion = "HORA:"
                Valor = CDate(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("Fecha")).ToShortTimeString
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                PosDescX = XV3A : PosValorX = PosDescX + 45
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

                'EMPRESA
                campo += 1
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                PosDescX = XV3A : PosValorX = XV3A + 70
                'Valor = FormarBarCode(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idOrdenSer"))
                Descripcion = "EMPRESA: "
                Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("RazonSocial"))
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

                'LINEA 2
                campo += 1
                PosDescX = XV1
                'Cambio de Linea
                PosDescY = PosDescY + 30
                'PosValorX = PosDescX + 100
                PosValorX = XV1 + 90
                PosValorY = PosDescY
                Descripcion = "BUENO POR: "
                Valor = Format(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("Importe"), "c")
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

                'importe en letras
                'LINEA 2
                campo += 1
                PosDescX = XV1
                'Cambio de Linea
                PosDescY = PosDescY + 15
                'PosValorX = PosDescX + 100
                PosValorX = XV1 + 90
                PosValorY = PosDescY
                Descripcion = "EN LETRAS: "
                Valor = UCase(ObjNumLet.NumerosALetras(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("Importe"))) & " M.N."
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)


                ''CONCEPTO
                'campo += 1
                'PosDescX = X2
                'PosDescY = PosDescY + 30
                'PosValorX = X2 + 65
                'PosValorY = PosDescY
                'Descripcion = "CONCEPTO: "
                'Valor = ""
                'InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

                If opcion = TipoDocDinero.Recibo Then
                    campo += 1
                    PosDescX = XV1
                    PosDescY = PosDescY + 30
                    PosValorX = XV1 + 80
                    PosValorY = PosDescY
                    Descripcion = "CONCEPTO: "
                    Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("Concepto"))
                    InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

                ElseIf opcion = TipoDocDinero.Folio Then
                    campo += 1
                    PosDescX = XV1
                    PosDescY = PosDescY + 15
                    PosValorX = XV1 + 80
                    PosValorY = PosDescY
                    Descripcion = "CONCEPTO: "
                    Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("TipoDinero"))
                    InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)
                End If

                If opcion = TipoDocDinero.Folio Then
                    'Viaje
                    If DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("NumViaje") > 0 Then



                        campo += 1
                        PosDescX = XV1
                        PosDescY = PosDescY + 30
                        PosValorX = XV1 + 80
                        PosValorY = PosDescY
                        Descripcion = "No. Guia:"
                        Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("NumViaje"))
                        InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

                        campo += 1
                        PosDescX = XV2
                        PosDescY = PosDescY
                        PosValorX = XV2 + 75
                        PosValorY = PosDescY
                        Descripcion = "Tractor:"
                        Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idTractor"))
                        InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

                        campo += 1
                        PosDescX = XV3
                        PosDescY = PosDescY
                        PosValorX = XV3 + 105
                        PosValorY = PosDescY
                        Descripcion = "Remolque 1:"
                        Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idRemolque1"))
                        InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

                        If DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idDolly") <> "" Then
                            campo += 1
                            PosDescX = XV2
                            PosDescY = PosDescY + 15
                            PosValorX = XV2 + 75
                            PosValorY = PosDescY
                            Descripcion = "Dolly:"
                            Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idDolly"))
                            InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

                            campo += 1
                            PosDescX = XV3
                            PosDescY = PosDescY
                            PosValorX = XV3 + 105
                            PosValorY = PosDescY
                            Descripcion = "Remolque 2:"
                            Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idRemolque2"))
                            InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)
                        End If

                        'DETALLE
                        DsRepDET = ImpRep.LlenaDsImprimeDET(ImpRep.NomTabla)
                        If DsRepDET.Tables(ImpRep.NomTabla).DefaultView.Count > 0 Then
                            campo += 1
                            PosDescX = XV1
                            PosDescY = PosDescY + 15
                            PosValorX = XV1 + 75
                            PosValorY = PosDescY
                            Descripcion = "ORIGEN - DESTINO:"
                            Valor = ""
                            InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

                            For j As Integer = 0 To DsRepDET.Tables(ImpRep.NomTabla).DefaultView.Count - 1
                                campo += 1
                                PosDescX = XV1 + 150
                                PosDescY = PosDescY + 15
                                PosValorX = XV1 + 150
                                PosValorY = PosDescY
                                Descripcion = ""
                                Valor = UCase(DsRepDET.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("OriDes"))
                                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)
                            Next
                        End If
                    End If

                End If

                'Recibe
                'LINEA 11
                campo += 1
                PosDescY = PosDescY + 100 : PosValorY = PosDescY
                Descripcion = ""
                Valor = "___________________"
                PosDescX = X2 : PosValorX = PosDescX + 25
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                'PosDescY = PosDescY + 15

                campo += 1
                Descripcion = ""
                Valor = "___________________"
                'PosDescX = X3A : PosValorX = PosDescX + 25
                PosDescX = X4 : PosValorX = PosDescX + 25
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                'PosDescY = PosDescY + 15

                'LINEA 12
                campo += 1
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                Descripcion = ""
                Valor = "    ( RECIBI ) "
                PosDescX = X2 : PosValorX = PosDescX + 35
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                'PosDescY = PosDescY + 15

                campo += 1
                Descripcion = ""
                Valor = "    ( ENTREGO ) "
                PosDescX = X4 : PosValorX = PosDescX + 35
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                'PosDescY = PosDescY + 15

                'LINEA 13
                campo += 1
                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                Descripcion = ""
                Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("NomEmpRec"))
                PosDescX = X2 : PosValorX = PosDescX + 25
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                'PosDescY = PosDescY + 15

                campo += 1
                Descripcion = ""
                Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("NomEmpEnt"))
                PosDescX = X4 : PosValorX = PosDescX + 25
                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                'PosDescY = PosDescY + 15


                'LINEA 3
                'campo += 1
                'Descripcion = ""
                'PosDescY = PosDescY + 15 : PosValorY = PosDescY
                'PosDescX = X2 : PosValorX = X2 + 40
                'Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("NomTipOrdServ"))
                'InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)

                ''LINEA 4
                'campo += 1
                'Descripcion = "ORD.SER."
                'PosDescY = PosDescY + 15 : PosValorY = PosDescY
                'PosDescX = X4 : PosValorX = X4 + 80
                ''Valor = FormarBarCode(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idOrdenSer"))
                'Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idOrdenSer")
                'InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)






                ''LINEA 6
                'campo += 1
                'PosDescY = PosDescY + 30 : PosValorY = PosDescY
                'Descripcion = "TRACTOR: "
                'Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idTractor"))
                'PosDescX = X1 : PosValorX = PosDescX + 70
                'InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

                'campo += 1
                'Descripcion = "REMOLQUE 1:"
                'Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idTolva1"))
                'PosDescX = X2 : PosValorX = PosDescX + 95
                'InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)


                ''LINEA 7
                'campo += 1
                'PosDescY = PosDescY + 15 : PosValorY = PosDescY
                'Descripcion = "DOLLY: "
                'Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idDolly"))
                'PosDescX = X1 : PosValorX = PosDescX + 50
                'InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)

                'campo += 1
                'Descripcion = "REMOLQUE 2:"
                'Valor = UCase(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idTolva2"))
                'PosDescX = X2 : PosValorX = PosDescX + 95
                'InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)


                'LINEA 9
                'campo += 1
                'PosDescY = PosDescY + 30 : PosValorY = PosDescY
                'Descripcion = ""
                'If opcion = TipoDocDinero.Recibo Then
                '    Valor = "REPARACIONES SOLICITADAS"
                'Else
                '    Valor = "DESCRIPCION DEL SERVICIO"
                'End If
                'PosDescX = X2 : PosValorX = PosDescX + 25
                'InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 1)
                'PosDescY = PosDescY + 15 : PosValorY = PosDescY

                'LINEA 10
                'DETALLE
                '    For j As Integer = 0 To DsRep.Tables(ImpRep.NomTabla).DefaultView.Count - 1
                '        If opcion = TipoDocDinero.Recibo Then
                '            If DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NomServicio") <> "" Then
                '                campo += 1
                '                PosDescY = PosDescY + 15 : PosValorY = PosDescY
                '                Descripcion = ""
                '                Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NomServicio")
                '                PosDescX = X1 : PosValorX = PosDescX + 25
                '                InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                '            Else
                '                If DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NombreAct") <> "" Then
                '                    campo += 1
                '                    PosDescY = PosDescY + 15 : PosValorY = PosDescY
                '                    Descripcion = ""
                '                    Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NombreAct")
                '                    PosDescX = X1 : PosValorX = PosDescX + 25
                '                    InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                '                Else
                '                    'salto de linea
                '                    PosDescY = PosDescY + 15 : PosValorY = PosDescY
                '                End If
                '            End If

                '            campo += 1
                '            'PosDescY = PosDescY + 30 : PosValorY = PosDescY
                '            Descripcion = ""
                '            Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NotaRecepcion")
                '            PosDescX = X2 : PosValorX = PosDescX + 25
                '            InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                '        Else
                '            campo += 1
                '            PosDescY = PosDescY + 15 : PosValorY = PosDescY
                '            Descripcion = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("idUnidadTrans")
                '            Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(j).Item("NomServicio")
                '            PosDescX = X2 - 35 : PosValorX = X2 + 25
                '            InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)
                '            'PosDescY = PosDescY + 15

                '        End If
                '    Next






                '    'LINEA 14
                '    'CODIGO DE BARRAS ORDEN SERVICIO
                '    campo += 1
                '    Descripcion = ""
                '    'PosDescY = PosDescY + 15 : PosValorY = PosDescY
                '    PosDescY = PosDescY + 75 : PosValorY = PosDescY
                '    PosDescX = X4 : PosValorX = X4
                '    Valor = FormarBarCode(DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idPadreOrdSer"))
                '    InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 2)

                '    'LINEA 15
                '    campo += 1
                '    Descripcion = "FOLIO: "
                '    PosDescY = PosDescY + 45 : PosValorY = PosDescY
                '    PosDescX = X4 : PosValorX = X4 + 60
                '    Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("idPadreOrdSer")
                '    InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY, 3)

                '    'LINEA 14
                '    'If idContrato = 0 Then
                '    '    '4 LINEAS
                '    '    PosDescY = PosDescY + 60
                '    'End If

                '    'campo += 1
                '    'Descripcion = "RECIBO NO."
                '    'PosDescX = X1 : PosDescY = PosDescY + 15 : PosValorX = PosDescX + 100 : PosValorY = PosDescY
                '    'Valor = DsRep.Tables(ImpRep.NomTabla).DefaultView.Item(i).Item("NUMRECIBO")
                '    'InsertaRegistro(campo, Descripcion, Valor, PosDescX, PosDescY, PosValorX, PosValorY)
            Next
        End If
    End Sub

    Private Sub InsertaRegistro(ByVal Campo As Integer, ByVal Descripcion As String, ByVal Valor As String, ByVal PosDescX As Single,
                           ByVal PosDescY As Single, ByVal PosValX As Single, ByVal PosValY As Single, ByVal vFuente As String)

        myDataRow = MyTablaImprime.NewRow()
        myDataRow("Campo") = Campo
        myDataRow("Descripcion") = Descripcion
        myDataRow("Valor") = Valor
        myDataRow("PosDescX") = PosDescX
        myDataRow("PosDescY") = PosDescY
        myDataRow("PosValX") = PosValX
        myDataRow("PosValY") = PosValY
        myDataRow("Fuente") = vFuente
        MyTablaImprime.Rows.Add(myDataRow)

    End Sub

    Private Function FormarBarCode(ByVal Codigo As String) As String

        Dim Barcode As String = vbEmpty
        'Barcode = Format("*{0}*", Codigo)
        If Len(Codigo) = 1 Then
            Barcode = "*00" & Codigo & "*"
        ElseIf Len(Codigo) = 2 Then
            Barcode = "*0" & Codigo & "*"
        ElseIf Len(Codigo) = 3 Then
            Barcode = "*" & Codigo & "*"
        End If

        Return Barcode
    End Function

    Private Sub prt_PrintPage(ByVal sender As Object, ByVal e As PrintPageEventArgs)
        ' Este evento se produce cada vez que se va a imprimir una página
        Try
            'lineaActual = 0

            Dim yPos As Single = e.MarginBounds.Top
            'Margen Izquierdo
            Dim leftMargin As Single = e.MarginBounds.Left
            'Fuentes
            'Altura de la Linea
            Dim lineHeight As Single = printFont.GetHeight(e.Graphics)


            If _cveEmpresa = 1 Then

                'Dim bmp As New Bitmap(My.Resources.LogoPegaso)
                'ImpRep.Imagen1 = New Bitmap(bmp, bmp.Width / 20, bmp.Height / 20)

                'ImpRep.PosicionImagen1 = New Point(e.MarginBounds.Left - 10, yPos - 30)
                ImpRep.PosicionImagen1 = New Point(e.MarginBounds.Left - 70, yPos - 30)


            End If

            Do
                yPos += lineHeight
                'Aqui se imprime el dato

                yPos = ImpRep.ImprimeLineaRecibo(e, lineaActual)

                lineaActual += 1

            Loop Until yPos >= e.MarginBounds.Bottom - 20 _
                OrElse lineaActual >= NumRegistros


            ContPaginas += 1
            'DS.Tables(vNomTabla).DefaultView.Count



            If lineaActual < NumRegistros Then
                e.HasMorePages = True
            Else
                ContPaginas = 0
                e.HasMorePages = False
            End If
        Catch ex As Exception
            MsgBox(ex.Message, vText)
        End Try

    End Sub
    Public Sub CreaTablaImprime()
        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Int32")
        myDataColumn.ColumnName = "Campo"
        myDataColumn.ReadOnly = False
        myDataColumn.Unique = False
        MyTablaImprime.Columns.Add(myDataColumn)


        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.String")
        myDataColumn.ColumnName = "Descripcion"
        myDataColumn.ReadOnly = False
        myDataColumn.Unique = False
        MyTablaImprime.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.String")
        myDataColumn.ColumnName = "Valor"
        myDataColumn.ReadOnly = False
        myDataColumn.Unique = False
        MyTablaImprime.Columns.Add(myDataColumn)


        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Single")
        myDataColumn.ColumnName = "PosDescX"
        myDataColumn.ReadOnly = False
        myDataColumn.Unique = False
        MyTablaImprime.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Single")
        myDataColumn.ColumnName = "PosDescY"
        myDataColumn.ReadOnly = False
        myDataColumn.Unique = False
        MyTablaImprime.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Single")
        myDataColumn.ColumnName = "PosValX"
        myDataColumn.ReadOnly = False
        myDataColumn.Unique = False
        MyTablaImprime.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Single")
        myDataColumn.ColumnName = "PosValY"
        myDataColumn.ReadOnly = False
        myDataColumn.Unique = False
        MyTablaImprime.Columns.Add(myDataColumn)


        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Single")
        myDataColumn.ColumnName = "Fuente"
        myDataColumn.ReadOnly = False
        myDataColumn.Unique = False
        MyTablaImprime.Columns.Add(myDataColumn)



    End Sub
End Class
