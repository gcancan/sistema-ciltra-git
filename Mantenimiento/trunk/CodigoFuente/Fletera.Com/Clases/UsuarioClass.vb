﻿Public Class UsuarioClass
    Private _idUsuario As String
    Private _Pass As String
    Private _NombreUserGrupo As String
    Private _Grupo As GrupoClass
    Private _EsGrupo As Boolean
    Private _Existe As Boolean
    Private _Activo As Boolean
    'Private _AutorizaDescuento As Boolean
    Private _DtPermisos As New DataTable
    Private WithEvents _TreePermisos As New TreeView 'Tiene los permisos en un Treeview. . .
    Private DtMnuPermisos As New DataTable 'Para poder dibujar el Treeview de Permisos segun el Menu Principal
    Private _TablaBd As String = "Usuarios"
    Dim strSql As String = ""
    Dim DsClase As New DataTable
#Region "Constructor"
    Sub New(ByVal vidUsuario As String)
        Dim DtUser As New DataTable
        'DtUser = BD.ExecuteReturn("SELECT sNomUser,sPassword,sCveGrupo,bActivo,EsGrupo,AutorizaDescuento " &
        '                          "FROM Usuarios " &
        '                          "WHERE sUserId = '" & vidUsuario & "'")

        DtUser = BD.ExecuteReturn("SELECT sNomUser,sPassword,sCveGrupo,bActivo,EsGrupo " &
                          "FROM Usuarios " &
                          "WHERE sUserId = '" & vidUsuario & "'")

        _idUsuario = vidUsuario
        If DtUser.Rows.Count > 0 Then
            With DtUser.Rows(0)
                _NombreUserGrupo = .Item("sNomUser") & ""
                _Pass = .Item("sPassword") & ""
                _Grupo = New GrupoClass(.Item("sCveGrupo") & "")
                Try
                    _Activo = .Item("bActivo")
                Catch ex As Exception
                    _Activo = False
                End Try
                'Try
                '    _AutorizaDescuento = .Item("AutorizaDescuento")
                'Catch ex As Exception
                '    _AutorizaDescuento = False
                'End Try
                Try
                    _EsGrupo = .Item("EsGrupo")
                Catch ex As Exception
                    _EsGrupo = False
                End Try
                strSql = "SELECT TAG,ISNULL(Acceso,'0') Acceso " &
                "FROM MnuAccesos WHERE sUserId = '" & _idUsuario & "'"

                _DtPermisos = BD.ExecuteReturn(strSql)
                CargaArbolPermiso()
            End With
            _Existe = True
        Else
            _Existe = False

        End If
    End Sub
#End Region
#Region "Propiedades"
    Public ReadOnly Property Existe As Boolean
        Get
            Return _Existe
        End Get
    End Property
    Public ReadOnly Property idUsuario As String
        Get
            Return _idUsuario
        End Get
    End Property

    Public Property Pass As String
        Get
            Return _Pass
        End Get
        Set(ByVal value As String)
            _Pass = value
        End Set
    End Property
    Public Property NombreUsuario As String
        Get
            Return _NombreUserGrupo
        End Get
        Set(ByVal value As String)
            _NombreUserGrupo = value
        End Set
    End Property
    Public Property Grupo As GrupoClass
        Get
            Return _Grupo
        End Get
        Set(ByVal value As GrupoClass)
            _Grupo = value
        End Set
    End Property
    Public ReadOnly Property EsGrupo As Boolean
        Get
            Return _EsGrupo
        End Get
    End Property
    Public ReadOnly Property Activo As Boolean
        Get
            Return _Activo
        End Get
    End Property
    'Public ReadOnly Property AutorizaDescuento As Boolean
    '    Get
    '        Return _AutorizaDescuento
    '    End Get
    'End Property

#End Region

#Region "Funciones"
    Public Sub ActualizaPermisos()
        If _EsGrupo = False Then
            If _Grupo.Existe Then
                _Grupo.ActualizaPermisos()
            End If
        End If
        _DtPermisos = BD.ExecuteReturn("SELECT TAG,ISNULL(Acceso,'0') Acceso " &
                                               "FROM MnuAccesos " &
                                               "WHERE sUserId = '" & _idUsuario & "'")
        CargaArbolPermiso()
    End Sub
    Public Function GetCambiosUsuario(ByVal vNombreUsuario As String, ByVal vPass As String, ByVal vClvGrupo As String, _
                                      ByVal vActivo As Boolean, ByVal vArbolPermisos As TreeView, ByVal vAutorizaDescuento As Boolean) As String
        Dim CadCam As String = ""
        If vNombreUsuario <> _NombreUserGrupo Then
            CadCam += "Nombre Usuario: '" & vNombreUsuario & "' Cambia a [" & _NombreUserGrupo & "],"
        End If
        If vPass <> _Pass Then
            CadCam += "Contraseña del Usuario se MODIFICA,"
        End If
        If vClvGrupo <> _Grupo.idGrupo Then
            CadCam += "Grupo de Usuario: '" & _Grupo.idGrupo & "' Cambia a [" & vClvGrupo & "],"
        End If
        'If vAutorizaDescuento <> _AutorizaDescuento Then
        '    CadCam += "Grupo Cambia de '" & IIf(_AutorizaDescuento, "AutorizaDescuento", "NO AutorizaDescuento") & "' A [" & IIf(vAutorizaDescuento, "AutorizaDescuento", "NO AutorizaDescuento") & "],"
        'End If
        If vActivo <> _Activo Then
            CadCam += "Grupo Cambia de '" & IIf(_Activo, "ACTIVO", "INACTIVO") & "' A [" & IIf(vActivo, "ACTIVO", "INACTIVO") & "],"
        End If
        Dim Difarboles As New List(Of String)
        For i As Integer = 0 To vArbolPermisos.Nodes.Count - 1
            Difarboles.AddRange(DiferenciasNodos(vArbolPermisos.Nodes(i), _TreePermisos.Nodes(i)))
        Next
        Dim PartNod() As String
        For i As Integer = 0 To Difarboles.Count - 1
            PartNod = Replace(Replace(Difarboles.Item(i), "(", ""), ")", "").Split({"'", ","}, StringSplitOptions.RemoveEmptyEntries)
            Try
                CadCam += "Nodo: " & getNombreTag(PartNod(1)) & " Cambia de '" & IIf(PartNod(2) = "1", "NO", "SI") & "' A [" & IIf(PartNod(2) = "0", "NO", "SI") & "],"
            Catch ex As Exception
            End Try
        Next
        Return CadCam
    End Function
    Public Sub GuardaUsuario(ByVal vNombreUsuario As String, ByVal vPass As String, ByVal vClvGrupo As String, ByVal vActivo As Boolean, _
                             ByVal vArbolPermisos As TreeView, ByVal vAutorizaDescuento As Boolean)
        Dim sSql As String = "", _CadValAnt As String = "", _CadValNue As String = ""

        If _Existe Then
            Dim vGrpTmp As New GrupoClass(vClvGrupo)
            If vNombreUsuario <> _NombreUserGrupo Then
                sSql += "sNomUser = '" & vNombreUsuario & "',"
                _CadValAnt += "sNomUser =" & _NombreUserGrupo & ","
            End If
            If vPass <> _Pass Then
                sSql += "sPassword = '" & generarClaveSHA1(vPass) & "',"
                _CadValAnt += "sPassword =" & _Pass & ","
            End If
            If vClvGrupo <> _Grupo.idGrupo Then
                sSql += "sCveGrupo = " & IIf(vGrpTmp.Existe, "'" & vGrpTmp.idGrupo & "'", "NULL") & ","
                _CadValAnt += "sCveGrupo " & _Grupo.idGrupo & ","
            End If
            If vActivo <> _Activo Then
                sSql += "bActivo = " & IIf(vActivo, "'1'", "'0'") & ","
                _CadValAnt += "bActivo =" & IIf(_Activo, "'1'", "'0'") & ","
            End If
            'If vAutorizaDescuento <> _AutorizaDescuento Then
            '    sSql += "AutorizaDescuento = " & IIf(vAutorizaDescuento, 1, 0) & ","
            '    _CadValAnt += "AutorizaDescuento =" & IIf(_AutorizaDescuento, 1, 0) & ","

            'End If
            Dim Difarboles As New List(Of String)
            For i As Integer = 0 To vArbolPermisos.Nodes.Count - 1
                Difarboles.AddRange(DiferenciasNodos(vArbolPermisos.Nodes(i), _TreePermisos.Nodes(i)))
            Next
            If sSql <> "" Then
                sSql = Mid(sSql, 1, sSql.Length - 1) 'Para quitar la Coma ','
                _CadValAnt = "sUserId=" & _idUsuario & "," & _CadValAnt
                _CadValAnt = Mid(_CadValAnt, 1, _CadValAnt.Length - 1)
                _CadValNue = Replace(sSql, "'", "")
                sSql = "UPDATE " & _TablaBd & " SET " & sSql & " WHERE sUserId = '" & _idUsuario & "'"
            End If
            If Difarboles.Count > 0 Then
                _CadValNue += ", Modifico la Estructura del Arbol de PERMISOS"
            End If
            Try
                Dim sSqlInPerm As String = ""
                If Difarboles.Count > 0 Then
                    If vGrpTmp.Existe Then
                        sSqlInPerm = GetSqlInsertPermUser(vArbolPermisos, vClvGrupo) 'Quiere decir que Hay q ver diferencias entre permisos del grup y user
                    Else
                        sSqlInPerm = GetSqlInsertGrupPermisos(vArbolPermisos)
                    End If
                End If

                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                If sSql <> "" Then BD.Execute(sSql, True)
                'Agrega los permisos del ARBOL!!!!!**SOlo si hay diferencia
                If Difarboles.Count > 0 Then
                    BD.Execute("DELETE FROM MnuAccesos WHERE sUserId = '" & _idUsuario & "'", True) 'Primero elimina los permisos Estableciados anteriormente
                    BD.Execute(sSqlInPerm, True) 'Ingresa los nuevos permisos!!!

                End If
                Audita(_TablaBd, _CadValAnt, _CadValNue, EnTipAccion.Modifica)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
        Else
            Try
                Dim ssqlGr As String = GetSqlInsertPermUser(vArbolPermisos, vClvGrupo)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute("INSERT INTO Usuarios(sUserId,sNomUser,sPassword,bActivo,sCveGrupo,EsGrupo) VALUES('" &
                        _idUsuario & "','" & vNombreUsuario & "','" & generarClaveSHA1(vPass) & "'," & IIf(vActivo, 1, 0) & ",'" & vClvGrupo & "',0)", True)
                '**************AUDITA**************
                _CadValNue = "sUserId=" & _idUsuario & ",sNomUser" & vNombreUsuario & ",sPassword=" & generarClaveSHA1(vPass) &
                                    ",bActivo=" & IIf(vActivo, "1", "0") & ",EsGrupo= 0"
                Audita(_TablaBd, "", _CadValNue, EnTipAccion.Inserta)
                '**********************************
                BD.Execute(ssqlGr, True)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)

            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
        End If
        _NombreUserGrupo = vNombreUsuario
        _Pass = generarClaveSHA1(vPass)
        _Grupo = New GrupoClass(vClvGrupo)
        _TreePermisos = vArbolPermisos
        _Activo = vActivo
    End Sub
    Public Function GetCambiosGrupo(ByVal vNombreGrupo As String, ByVal vActivo As Boolean, ByVal vArbolPermisos As TreeView) As String
        Dim CadCam As String = ""
        If vNombreGrupo <> _NombreUserGrupo Then
            CadCam += "Nombre Grupo: '" & _NombreUserGrupo & "' Cambia a [" & vNombreGrupo & "],"
        End If
        If vActivo <> _Activo Then
            CadCam += "Grupo Cambia de '" & IIf(_Activo, "ACTIVO", "INACTIVO") & "' A [" & IIf(vActivo, "ACTIVO", "INACTIVO") & "],"
        End If
        Dim Difarboles As New List(Of String)
        For i As Integer = 0 To vArbolPermisos.Nodes.Count - 1
            Difarboles.AddRange(DiferenciasNodos(vArbolPermisos.Nodes(i), _TreePermisos.Nodes(i)))
        Next
        Dim PartNod() As String
        For i As Integer = 0 To Difarboles.Count - 1
            PartNod = Replace(Replace(Difarboles.Item(i), "(", ""), ")", "").Split({"'", ","}, StringSplitOptions.RemoveEmptyEntries)
            Try
                CadCam += "Nodo: " & getNombreTag(PartNod(1)) & " Cambia de '" & IIf(PartNod(2) = "1", "NO", "SI") & "' A [" & IIf(PartNod(2) = "0", "NO", "SI") & "],"
            Catch ex As Exception
            End Try
        Next
        Return CadCam
    End Function
    Public Sub GuardaGrupo(ByVal vNombreGrupo As String, ByVal vActivo As Boolean, ByVal vArbolPermisos As TreeView)
        Dim sSql As String = "", _CadValAnt As String = "", _CadValNue As String = ""
        If _Existe Then
            If vNombreGrupo <> _NombreUserGrupo Then
                sSql += "sNomUser = '" & vNombreGrupo & "',"
                _CadValAnt += "sNomUser =" & _NombreUserGrupo & ","
            End If
            If vActivo <> _Activo Then
                sSql += "bActivo = " & IIf(vActivo, "'1'", "'0'") & ","
                _CadValAnt += "bActivo =" & IIf(_Activo, "'1'", "'0'") & ","
            End If
            Dim Difarboles As New List(Of String)
            For i As Integer = 0 To vArbolPermisos.Nodes.Count - 1
                Difarboles.AddRange(DiferenciasNodos(vArbolPermisos.Nodes(i), _TreePermisos.Nodes(i)))
            Next
            If sSql <> "" Then
                sSql = Mid(sSql, 1, sSql.Length - 1) 'Para quitar la Coma ','
                _CadValAnt = "sUserId=" & _idUsuario & "," & _CadValAnt
                _CadValAnt = Mid(_CadValAnt, 1, _CadValAnt.Length - 1)
                _CadValNue = Replace(sSql, "'", "")
                sSql = "UPDATE " & _TablaBd & " SET " & sSql & " WHERE sUserId = '" & _idUsuario & "'"
            End If
            If Difarboles.Count > 0 Then
                _CadValNue += ", Modifico la Estructura del Arbol de PERMISOS"
            End If
            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                If sSql <> "" Then BD.Execute(sSql, True)
                'Agrega los permisos del ARBOL!!!!!**SOlo si hay diferencia
                If Difarboles.Count > 0 Then
                    BD.Execute("DELETE FROM MnuAccesos WHERE sUserId = '" & _idUsuario & "'", True) 'Primero elimina los permisos Estableciados anteriormente
                    '**************ARBOL DE PERMISOS**************
                    '13/FEB/2016
                    'Solo Funciona en SQl server 2008
                    'BD.Execute(GetSqlInsertGrupPermisos(vArbolPermisos), True) 'Ingresa los nuevos permisos!!!
                    '**********************************
                    Dim ListUltChek As New List(Of String)
                    'Dim sSql As String = ""
                    For i As Integer = 0 To vArbolPermisos.Nodes.Count - 1
                        ListUltChek.AddRange(BuscaCadUltimoCheked(vArbolPermisos.Nodes(i)))
                    Next
                    For i As Integer = 0 To ListUltChek.Count - 1
                        sSql = "INSERT INTO MnuAccesos(sUserId,TAG,Acceso) VALUES ('" & _idUsuario & "','" & ListUltChek(i) & "',1)"
                        BD.Execute(sSql, True)
                    Next
                    '**************ARBOL DE PERMISOS**************
                End If
                Audita(_TablaBd, _CadValAnt, _CadValNue, EnTipAccion.Modifica)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try

        Else
            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)

                strSql = "INSERT INTO Usuarios(sUserId,sNomUser,bActivo,EsGrupo) VALUES('" & _
                        _idUsuario & "','" & vNombreGrupo & "'," & IIf(vActivo, 1, 0) & ",1)"
                BD.Execute(strSql, True)
                '**************AUDITA**************
                _CadValNue = "sUserId=" & _idUsuario & ",sNomUser" & vNombreGrupo & ",bActivo=" & IIf(vActivo, "1", "0") & ",EsGrupo=1"
                Audita(_TablaBd, "", _CadValNue, EnTipAccion.Inserta)
                '**************ARBOL DE PERMISOS**************
                '13/FEB/2016
                'Solo Funciona en SQl server 2008
                'BD.Execute(GetSqlInsertGrupPermisos(vArbolPermisos), True) -> se sustituye por esto...
                '**********************************
                Dim ListUltChek As New List(Of String)
                'Dim sSql As String = ""
                For i As Integer = 0 To vArbolPermisos.Nodes.Count - 1
                    ListUltChek.AddRange(BuscaCadUltimoCheked(vArbolPermisos.Nodes(i)))
                Next
                For i As Integer = 0 To ListUltChek.Count - 1
                    sSql = "INSERT INTO MnuAccesos(sUserId,TAG,Acceso) VALUES ('" & _idUsuario & "','" & ListUltChek(i) & "',1)"
                    BD.Execute(sSql, True)
                Next
                '**************ARBOL DE PERMISOS**************

                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
                _NombreUserGrupo = vNombreGrupo
                _Activo = vActivo
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message & ex.Source)
            End Try
        End If
        _NombreUserGrupo = vNombreGrupo
        _Activo = vActivo
        _TreePermisos = vArbolPermisos
    End Sub
    Public Sub Eliminar()
        If _Existe Then
            Dim sSql As String = "", _CadValAnt As String = ""
            _CadValAnt = "sUserId=" & _idUsuario & ",Pass= " & _Pass & " , sNomUser =" & _NombreUserGrupo & " ,sCveGrupo=" & IIf(_Grupo.Existe, _Grupo.idGrupo, "<SIN GRUPO>") &
                ",EsGrupo=" & IIf(_EsGrupo, "1", "0")
            sSql = "DELETE FROM " & _TablaBd & " WHERE sUserId = '" & _idUsuario & "'"
            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                BD.Execute("DELETE FROM MnuAccesos WHERE sUserId = '" & _idUsuario & "'", True) 'Borra los permisos que tuviera!!!
                Audita(_TablaBd, _CadValAnt, "", EnTipAccion.Elimina)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
            _Existe = False
        Else
            'se supone que nunca debe entrar aqui. . .
            MessageBox.Show("EL Usuario o Grupo con Id: " & _idUsuario & " No existe por lo tanto no se puede Eliminar!!!",
                            "No se puede eliminar el Usuario o Grupo porque no Existe", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub
    Private Function GetSqlInsertPermUser(ByVal vArbolPerm As TreeView, ByVal vidGrupo As String) As String
        Dim _GrupTmp As New GrupoClass(vidGrupo), ResSql As String = "", Res As New List(Of String)
        If _GrupTmp.Existe Then
            For i As Integer = 0 To vArbolPerm.Nodes.Count - 1
                Res.AddRange(DiferenciasNodos(vArbolPerm.Nodes(i), _GrupTmp.ArbolPermGrupo.Nodes(i)))
            Next
        End If
        For i As Integer = 0 To Res.Count - 1
            ResSql += Res.Item(i) & ","
        Next
        If ResSql <> "" Then
            ResSql = "INSERT INTO MnuAccesos(sUserId,TAG,Acceso) VALUES" & Mid(ResSql, 1, ResSql.Length - 1)
        End If
        Return ResSql
    End Function
    Private Function DiferenciasNodos(ByVal vArbol1 As TreeNode, ByVal vArbol2 As TreeNode) As List(Of String)
        Dim ListDif As New List(Of String)
        If vArbol1.Checked <> vArbol2.Checked Then
            ListDif.Add("('" & _idUsuario & "','" & vArbol1.Tag & "'," & IIf(vArbol1.Checked, "1", "0") & ")")
        End If
        For i As Integer = 0 To vArbol1.Nodes.Count - 1
            If vArbol1.Nodes(i).GetNodeCount(False) > 0 Then
                ListDif.AddRange(DiferenciasNodos(vArbol1.Nodes(i), vArbol2.Nodes(i)))
            Else
                If vArbol1.Nodes(i).Checked <> vArbol2.Nodes(i).Checked Then
                    ListDif.Add("('" & _idUsuario & "','" & vArbol1.Nodes(i).Tag & "'," & IIf(vArbol1.Nodes(i).Checked, "1", "0") & ")")
                End If
            End If
        Next
        Return ListDif
    End Function
    Private Function GetSqlInsertGrupPermisos(ByVal vArbolPerm As TreeView) As String
        Dim ListUltChek As New List(Of String), sSql As String = ""
        For i As Integer = 0 To vArbolPerm.Nodes.Count - 1
            ListUltChek.AddRange(BuscaCadUltimoCheked(vArbolPerm.Nodes(i)))
        Next
        For i As Integer = 0 To ListUltChek.Count - 1
            'sSql += "('" & _idUsuario & "','" & ListUltChek(i) & "','1'),"
            'Solo Funciona en SQl server 2008
            'sSql += "('" & _idUsuario & "','" & ListUltChek(i) & "',1), "

            sSql += "INSERT INTO MnuAccesos(sUserId,TAG,Acceso) VALUES ('" & _idUsuario & "','" & ListUltChek(i) & "',1); "
        Next
        'If sSql <> "" Then sSql = "INSERT INTO MnuAccesos(sUserId,TAG,Acceso) VALUES " & Mid(sSql, 1, sSql.Length - 1)
        If sSql <> "" Then sSql = Mid(sSql, 2, sSql.Length - 1)
        Return sSql
    End Function
    Private Function BuscaCadUltimoCheked(ByVal vNodo As TreeNode) As List(Of String)
        Dim ResList As New List(Of String)
        'MessageBox.Show(vNodo.Text & vbNewLine)
        If vNodo.Checked Then
            'If vNodo.Checked Or UCase(vNodo.Name) = UCase("salir") Then
            For i As Integer = 0 To vNodo.GetNodeCount(False) - 1
                If vNodo.Nodes(i).GetNodeCount(False) > 0 Then
                    If vNodo.Nodes(i).Checked Then
                        ResList.AddRange(BuscaCadUltimoCheked(vNodo.Nodes(i)))
                    Else
                        If i = vNodo.GetNodeCount(False) - 1 Then 'Es el Ultimo Elemento
                            Dim BndAgre As Boolean = True
                            For j As Integer = 0 To vNodo.GetNodeCount(False) - 1
                                If vNodo.Nodes(j).Checked = True Then
                                    BndAgre = False
                                    Exit For
                                End If
                            Next
                            If BndAgre Then ResList.Add(vNodo.Nodes(i).Parent.Tag)
                            'If BndAgre Then ResList.Add("('" & Usuario.idUsuario & "','" & vNodo.Nodes(i).Parent.Tag & "','1')")
                        End If
                    End If
                Else
                    If vNodo.Nodes(i).Checked Then
                        ResList.Add(vNodo.Nodes(i).Tag)
                        'ResList.Add("('" & Usuario.idUsuario & "','" & vNodo.Nodes(i).Tag & "','1')")
                    Else
                        If i = vNodo.GetNodeCount(False) - 1 Then 'Es el Ultimo Elemento
                            Dim BndAgre As Boolean = True
                            For j As Integer = 0 To vNodo.GetNodeCount(False) - 1
                                If vNodo.Nodes(j).Checked = True Then
                                    BndAgre = False
                                    Exit For
                                End If
                            Next
                            If BndAgre Then ResList.Add(vNodo.Nodes(i).Parent.Tag)
                            'If BndAgre Then ResList.Add("('" & Usuario.idUsuario & "','" & vNodo.Nodes(i).Parent.Tag & "','1')")
                        End If
                    End If
                    'MessageBox.Show(vNodo.Nodes(i).Text & "*ABCR*")
                End If
            Next
        End If
        Return ResList
    End Function
    Enum enOpcionTag
        Nada = 0
        Alta
        Baja
        Modificar
        Reporte
    End Enum
    Public Function TieneAcceso(ByVal vTag As String, Optional ByVal vTipoAccion As enOpcionTag = enOpcionTag.Nada) As Boolean
        If vTipoAccion <> enOpcionTag.Nada Then
            vTag += "$" & vTipoAccion
        End If
        For i As Integer = 0 To _TreePermisos.Nodes.Count - 1 'Tomado de frmUsuarios
            If vTag = _TreePermisos.Nodes(i).Tag Then
                Return _TreePermisos.Nodes(i).Checked
                Exit For
            End If
            If TieneAccesoNod(_TreePermisos.Nodes(i), vTag) Then
                Return True
            End If
        Next
        Return False
    End Function
    Private Function TieneAccesoNod(ByVal vNodo As TreeNode, ByVal vTag As String) As Boolean
        For i As Integer = 0 To vNodo.GetNodeCount(False) - 1
            If vNodo.Nodes(i).GetNodeCount(False) > 0 Then
                If vNodo.Nodes(i).Tag = vTag Then
                    Return vNodo.Nodes(i).Checked
                    Exit For
                End If
                If TieneAccesoNod(vNodo.Nodes(i), vTag) = True Then
                    Return True
                End If
            Else
                If vTag = vNodo.Nodes(i).Tag Then
                    Return vNodo.Nodes(i).Checked
                End If
            End If
        Next
        Return False
    End Function
    Public Function TieneAccesoUltNodos(ByVal vTag As String) As Boolean
        'Accesos del Grupo o del Usuario!!!, solo si es usuario tendra permisos de grupo
        If _EsGrupo Then
            _DtPermisos.DefaultView.RowFilter = "TAG = '" & vTag & "' AND Acceso = true"
            If _DtPermisos.DefaultView.Count > 0 Then
                Return True
            Else
                Return False
            End If
        Else 'ES UN USUARIO
            If _Grupo.Existe Then
                With _Grupo.DtPermisosGrupo.DefaultView
                    .RowFilter = "TAG = '" & vTag & "' AND Acceso = true"
                    If .Count > 0 Then 'Tiene Acceso en el grupo
                        'Verifica En los Extendidos del Usuario si tiene Permisoo
                        _DtPermisos.DefaultView.RowFilter = "TAG = '" & vTag & "'"
                        If _DtPermisos.DefaultView.Count > 0 Then 'verifica que el Añadido Sea Truo o False y es el que regresara
                            Return CBool(_DtPermisos.DefaultView.Item(0).Item("Acceso"))
                        Else 'Si no lo encuentra no se agrego de mas
                            Return True 'Toma el Permiso del Grupo
                        End If
                    Else 'No tiene Acceso en el Grupo y se verifica en sus permisos
                        _DtPermisos.DefaultView.RowFilter = "TAG = '" & vTag & "' AND Acceso = true"
                        If _DtPermisos.DefaultView.Count > 0 Then 'Si obtuvo acceso. . 
                            Return True
                        Else 'No obtuvo Acceso!!!
                            Return False
                        End If
                    End If
                End With
            Else 'SOLO TIENE ACCESO A SUS PERMISOS PROPIOS
                _DtPermisos.DefaultView.RowFilter = "TAG = '" & vTag & "' AND Acceso = true"
                If _DtPermisos.DefaultView.Count > 0 Then 'Si obtuvo acceso. . 
                    Return True
                Else 'No obtuvo Acceso!!!
                    Return False
                End If
            End If

        End If

    End Function
    Public Function AccesosGrupoUltNodo(ByVal vTag As String) As Boolean
        _Grupo.DtPermisosGrupo.DefaultView.RowFilter = "TAG = '" & vTag & "' AND Acceso = True"
        If _Grupo.DtPermisosGrupo.DefaultView.Count > 0 Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Sub CargaArbolPermiso()
        strSql = "SELECT Mp.TAG, Mp.idTipoPer, Mp.NomForma, Tp.Opcion, Tp.NomOpcion " &
                "FROM MnuPermisos Mp LEFT JOIN " &
                "MnuTiposPermisos Tp ON Mp.idTipoPer = Tp.idTipoPer " &
                "WHERE Tp.Opcion <> 0" 'Jala Todos los Permisos. . . al Cargar
        DtMnuPermisos = BD.ExecuteReturn(strSql)
        DibujaArbolPermisos()
        For i As Integer = 0 To _TreePermisos.Nodes.Count - 1 'Tomado de frmUsuarios
            _TreePermisos.Nodes(i).Checked = False
            CargaNodos(_TreePermisos.Nodes(i))
        Next
    End Sub
    Private Sub DibujaArbolPermisos() 'Tomado de frmUsuarios
        _TreePermisos.Nodes.Clear()
        For j = 0 To FMenu.Menu.MenuItems.Count - 1
            With FMenu.Menu.MenuItems(j)
                If .Tag = "" Then Continue For
                With _TreePermisos.Nodes.Add(FMenu.Menu.MenuItems(j).Text) 'NIVELES PRINCIPALES!!!
                    .Tag = FMenu.Menu.MenuItems(j).Tag
                    .Checked = False
                    GetNodos(FMenu.Menu.MenuItems(j), _TreePermisos.Nodes(j))
                End With
            End With
        Next
    End Sub
    Private Sub GetNodos(ByVal vMenu As MenuItem, ByVal vNodo As TreeNode) 'Tomado de frmUsuarios
        Dim ResMen As String = "", FilItm As Integer = 0
        For Each mycontrol As MenuItem In vMenu.MenuItems
            If mycontrol.IsParent Then
                If mycontrol.Tag = "" Then Continue For
                With vNodo.Nodes.Add(mycontrol.Text)
                    .Tag = mycontrol.Tag
                    .Checked = False
                    GetNodos(mycontrol, vNodo.Nodes(FilItm))
                End With
            Else
                If mycontrol.Tag = "" Then Continue For
                With vNodo.Nodes.Add(mycontrol.Text)
                    .Tag = mycontrol.Tag
                    .Checked = False
                    DtMnuPermisos.DefaultView.RowFilter = "TAG = '" & .Tag & "'"
                    DtMnuPermisos.DefaultView.Sort = "Opcion"
                    For i As Integer = 0 To DtMnuPermisos.DefaultView.Count - 1
                        With vNodo.Nodes(FilItm).Nodes.Add(DtMnuPermisos.DefaultView.Item(i).Item("NomOpcion"))
                            .tag = .parent.tag & "$" & DtMnuPermisos.DefaultView.Item(i).Item("Opcion")
                            .checked = False
                        End With
                    Next
                End With
                FilItm += 1
            End If
        Next
    End Sub
    Private Sub CargaNodos(ByVal vNodo As TreeNode)
        If TieneAccesoUltNodos(vNodo.Tag) Then
            vNodo.Checked = True
        End If
        For i As Integer = 0 To vNodo.GetNodeCount(False) - 1
            If vNodo.Nodes(i).GetNodeCount(False) > 0 Then
                CargaNodos(vNodo.Nodes(i))
            Else
                If TieneAccesoUltNodos(vNodo.Nodes(i).Tag) Then
                    vNodo.Nodes(i).Checked = True
                End If
            End If
        Next
    End Sub
    Private Function getNombreTag(ByVal vTag As String) As String
        Dim TipAccion As String = "", rNomTag As String = ""
        If vTag.Contains("$") Then
            TipAccion = vTag.Substring(vTag.IndexOf("$") + 1)
            vTag = vTag.Substring(0, vTag.IndexOf("$"))

        End If
        For i As Integer = 0 To _TreePermisos.Nodes.Count
            rNomTag = recNombreNodoTag(_TreePermisos.Nodes(i), vTag)
            If rNomTag <> "" Then
                Exit For
            End If
        Next
        Select Case TipAccion
            Case "1"
                TipAccion = "Alta"
            Case "2"
                TipAccion = "Baja"
            Case "3"
                TipAccion = "Modificacion"
            Case "4"
                TipAccion = "Reporte"
            Case Else
                TipAccion = ""
        End Select
        Return rNomTag & IIf(TipAccion <> "", " Apartado " & TipAccion, "")
    End Function
    Private Function recNombreNodoTag(ByVal vNodo As TreeNode, ByVal vTagBuscar As String) As String
        If vNodo.Tag = vTagBuscar Then
            Return vNodo.Text
        End If
        For I As Integer = 0 To vNodo.Nodes.Count - 1
            If vNodo.Nodes(I).GetNodeCount(False) > 0 Then
                Dim rsTmp As String = recNombreNodoTag(vNodo.Nodes(I), vTagBuscar)
                If rsTmp <> "" Then
                    Return rsTmp
                End If
            Else
                If vNodo.Nodes(I).Tag = vTagBuscar Then
                    Return vNodo.Nodes(I).Text
                End If
            End If
        Next
        Return ""
    End Function
#End Region

    Private Sub _TreePermisos_AfterCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles _TreePermisos.AfterCheck
        If e.Node.Checked = False Then
            ' e.Node.ForeColor = Color.Red
            ' e.Node.BackColor = Color.White
            For i As Integer = 0 To e.Node.GetNodeCount(False) - 1
                If e.Node.Nodes(i).Checked Then e.Node.Nodes(i).Checked = False
            Next
        Else
            'e.Node.ForeColor = Color.Black
            'e.Node.BackColor = Color.GreenYellow
            If Not e.Node.Parent Is Nothing Then
                If e.Node.Parent.Checked = False Then e.Node.Parent.Checked = True
            End If
        End If
    End Sub

    Public Function VerificaPermisoEspecial(ByVal usuario As String, ByVal NomPermiso As String) As Boolean
        strSql = "SELECT * FROM dbo.privilegiosUsuario " &
        "WHERE idUsuario = (SELECT idUsuario FROM dbo.usuariosSys WHERE nombreUsuario = '" & usuario & "') " &
        "AND idPrivilegio = (SELECT idPrivilegio FROM dbo.privilegios WHERE clave = '" & NomPermiso & "')"
        DsClase = BD.ExecuteReturn(strSql)
        If DsClase.Rows.Count > 0 Then
            Return True
        Else
            Return False
        End If
    End Function



End Class
