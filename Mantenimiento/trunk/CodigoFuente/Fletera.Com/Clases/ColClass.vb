﻿Public Class ColClass
    Private _Campo As String
    Private _DesCampo As String
    Private _TipoCampo As TipoDato
    Private _TipoCampoFiltra As TipoDato
    Private _BfiltraCampo As Boolean
    Private _BOrdenCampo As Boolean
    Private _BOrdenCampoTipo As TipoOrdenamiento
    '**********************
    'AGREGADO 6/AGOSTO/2013 -> Se Agrego para activar Orden 
    Private _ActivaOrdenCampo As Boolean

    Private _TipControlCampo As TipoControlReport
    Private _BImprimeCampo As Boolean
    Private _DesValorTrue As String
    Private _DesValorFalse As String
    Private _AnchoCampo As Integer
    Private _StrSqlOpc As String
    Private _BSuma As Boolean
    Private _ControlLibre As Control()
    Private _BGrupo As Boolean
    Private _CampoSumaGrupo As String


    '**********************
    'AGREGADO 31/JULIO/2013 -> Se Agrego Prefijo
    Private _Prefijo As String
    Public ReadOnly Property EsControlLibre
        Get
            If _ControlLibre Is Nothing Then
                Return False
            Else
                Return True
            End If
        End Get
    End Property
    '/////////////////////////////////////////////////////////////////////////////////////////
    Private _TipFiltCadena As String
    Enum EnTipoFilCadena
        Obligatorio = 0
        Opcional
    End Enum
    '\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
#Region "Inicializacion"
    Sub New()
        '_BOrdenCampo = EnTipoOrden.Ninguno
        '_BfiltraCampo = False
    End Sub
    Sub New(ByVal vCampo As String, ByVal vDescCampo As String, ByVal vTipoCampo As TipoDato,
            ByVal vBfiltraCampo As Boolean, ByVal vBOrdenCampo As Boolean, ByVal vBOrdenCampoTipo As TipoOrdenamiento,
             ByVal vTipControlCampo As TipoControlReport, ByVal vBImprimeCampo As Boolean, ByVal vAnchoCampo As Integer,
             Optional ByVal vDesValorTrue As String = "", Optional ByVal vDesValorFalse As String = "", Optional ByVal vStrSqlOpc As String = "",
             Optional ByVal vBSuma As Boolean = False, Optional ByVal vTipoFiltroCadena As EnTipoFilCadena = EnTipoFilCadena.Opcional,
             Optional ByVal vBGrupo As Boolean = False, Optional ByVal vBCampoSumaGrupo As String = "", Optional ByVal vPrefijo As String = "",
            Optional ByVal vActivaOrdenCampo As Boolean = False, Optional ByVal vTipoCampoFiltra As TipoDato = TipoDato.TdCadena)



        _Campo = vCampo
        _DesCampo = vDescCampo
        _TipoCampo = vTipoCampo
        _BfiltraCampo = vBfiltraCampo
        _BOrdenCampo = vBOrdenCampo
        _BOrdenCampoTipo = vBOrdenCampoTipo
        _TipControlCampo = vTipControlCampo
        _BImprimeCampo = vBImprimeCampo
        _DesValorTrue = vDesValorTrue
        _DesValorFalse = vDesValorFalse
        _AnchoCampo = vAnchoCampo
        _StrSqlOpc = vStrSqlOpc
        _BSuma = vBSuma
        '/////////////////////////////////////////////////////////////////////
        _TipFiltCadena = vTipoFiltroCadena
        '\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
        _BGrupo = vBGrupo
        _CampoSumaGrupo = vBCampoSumaGrupo
        _Prefijo = vPrefijo
        _ActivaOrdenCampo = vActivaOrdenCampo
        _TipoCampoFiltra = vTipoCampoFiltra
    End Sub
    Sub New(ByVal vDescContLibre As String, ByVal vControlLibre As Control())
        _DesCampo = vDescContLibre
        _ControlLibre = vControlLibre
        _BOrdenCampo = False
        _BfiltraCampo = False
    End Sub
#End Region

#Region "Propiedades"
    Public Property ControlLibre As Control()
        Get
            Return _ControlLibre
        End Get
        Set(ByVal value As Control())
            _ControlLibre = value
        End Set
    End Property
    Public Property Campo As String
        Get
            Return _Campo
        End Get
        Set(ByVal value As String)
            _Campo = value
        End Set
    End Property
    Public Property DesripcionCampo As String
        Get
            Return _DesCampo
        End Get
        Set(ByVal value As String)
            _DesCampo = value
        End Set
    End Property
    Public Property TipoCampo As TipoDato
        Get
            Return _TipoCampo
        End Get
        Set(ByVal value As TipoDato)
            _TipoCampo = value
        End Set
    End Property

    Public Property TipoCampoFiltra As TipoDato
        Get
            Return _TipoCampoFiltra
        End Get
        Set(ByVal value As TipoDato)
            _TipoCampoFiltra = value
        End Set
    End Property
    Public Property BFiltraCampo As Boolean
        Get
            Return _BfiltraCampo
        End Get
        Set(ByVal value As Boolean)
            _BfiltraCampo = value
        End Set
    End Property
    Public Property BOrdenCampo As Boolean
        Get
            Return _BOrdenCampo
        End Get
        Set(ByVal value As Boolean)
            _BOrdenCampo = value
        End Set
    End Property

    Public Property BOrdenCampoTipo As TipoOrdenamiento
        Get
            Return _BOrdenCampoTipo
        End Get
        Set(ByVal value As TipoOrdenamiento)
            _BOrdenCampoTipo = value
        End Set
    End Property
    Public Property TipoControlCampo As TipoControlReport
        Get
            Return _TipControlCampo
        End Get
        Set(ByVal value As TipoControlReport)
            _TipControlCampo = value
        End Set
    End Property
    Public Property BImprimeCampo As Boolean
        Get
            Return _BImprimeCampo
        End Get
        Set(ByVal value As Boolean)
            _BImprimeCampo = value
        End Set
    End Property
    Public Property DesValorFalse As String
        Get
            Return _DesValorFalse
        End Get
        Set(ByVal value As String)
            _DesValorFalse = value
        End Set
    End Property

    Public Property DesValorTrue As String
        Get
            Return _DesValorTrue
        End Get
        Set(ByVal value As String)
            _DesValorTrue = value
        End Set
    End Property

    Public Property AnchoCampo As Integer
        Get
            Return _AnchoCampo
        End Get
        Set(ByVal value As Integer)
            _AnchoCampo = value
        End Set
    End Property

    Public Property StrSqlOpc As String
        Get
            Return _StrSqlOpc
        End Get
        Set(ByVal value As String)
            _StrSqlOpc = value
        End Set
    End Property

    Public Property BSuma As String
        Get
            Return _BSuma
        End Get
        Set(ByVal value As String)
            _BSuma = value
        End Set
    End Property
    Public ReadOnly Property TipofiltroCadena As EnTipoFilCadena
        Get
            Return _TipFiltCadena
        End Get
    End Property
    Public Property BGrupo As Boolean
        Get
            Return _BGrupo
        End Get
        Set(ByVal value As Boolean)
            _BGrupo = value
        End Set
    End Property
    Public Property CampoSumaGrupo As String
        Get
            Return _CampoSumaGrupo
        End Get
        Set(ByVal value As String)
            _CampoSumaGrupo = value
        End Set
    End Property
    Public Property Prefijo As String
        Get
            Return _Prefijo
        End Get
        Set(ByVal value As String)
            _Prefijo = value
        End Set
    End Property
    Public Property ActivaOrdenCampo As Boolean
        Get
            Return _ActivaOrdenCampo
        End Get
        Set(ByVal value As Boolean)
            _ActivaOrdenCampo = value
        End Set
    End Property
#End Region

End Class

