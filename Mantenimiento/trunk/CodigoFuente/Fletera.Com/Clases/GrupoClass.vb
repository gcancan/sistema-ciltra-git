﻿Public Class GrupoClass
    Private _idGrupo As String
    Private _NombreGrupo As String
    Private _Existe As Boolean
    'Private _Externo As Boolean
    'Private _CCODIGOC01_CLI As String
    Private _dtPermisos As New DataTable
    Private WithEvents _TreePermGrupo As New TreeView
    Private DtMnuPermisos As New DataTable
    Dim StrSql As String = ""
#Region "Constructor"
    Sub New(ByVal vIdGrupo As String)
        Dim DtGrup As New DataTable
        'StrSql = "SELECT sNomUser, isnull(CCODIGOC01_CLI,'') as CCODIGOC01_CLI ,isnull(bExterno,0) as bExterno FROM Usuarios WHERE sUserId = '" & vIdGrupo & "'"
        StrSql = "SELECT sNomUser FROM Usuarios WHERE sUserId = '" & vIdGrupo & "'"
        DtGrup = BD.ExecuteReturn(StrSql)
        _idGrupo = vIdGrupo
        If DtGrup.Rows.Count > 0 Then
            With DtGrup.Rows(0)
                _NombreGrupo = .Item("sNomUser") & ""
                ' _CCODIGOC01_CLI = .Item("CCODIGOC01_CLI") & ""
                '_Externo = .Item("bExterno")
            End With
            StrSql = "SELECT TAG,Acceso FROM MnuAccesos WHERE sUserId = '" & vIdGrupo & "'"
            _dtPermisos = BD.ExecuteReturn(StrSql)
            CargaArbolPermiso()
            _Existe = True

        Else
            _NombreGrupo = ""
            _Existe = False
        End If
    End Sub
#End Region
#Region "Propiedades"
    Public ReadOnly Property idGrupo As String
        Get
            Return _idGrupo
        End Get
    End Property
    Public Property NombreGrupo As String
        Get
            Return _NombreGrupo
        End Get
        Set(ByVal value As String)
            _NombreGrupo = value
        End Set
    End Property
    Public ReadOnly Property Existe As Boolean
        Get
            Return _Existe
        End Get
    End Property
    Public ReadOnly Property DtPermisosGrupo As DataTable
        Get
            Return _dtPermisos
        End Get
    End Property
    Public ReadOnly Property ArbolPermGrupo As TreeView
        Get
            Return _TreePermGrupo
        End Get
    End Property
    'Public ReadOnly Property Externo As Boolean
    '    Get
    '        Return _Externo
    '    End Get
    'End Property
    'Public Property CCODIGOC01_CLI As String
    '    Get
    '        Return _CCODIGOC01_CLI
    '    End Get
    '    Set(ByVal value As String)
    '        _CCODIGOC01_CLI = value
    '    End Set
    'End Property
#End Region
    Public Sub ActualizaPermisos()
        If _Existe Then
            _dtPermisos = BD.ExecuteReturn("SELECT TAG,Acceso FROM MnuAccesos WHERE sUserId = '" & _idGrupo & "'")
            CargaArbolPermiso()
        End If
    End Sub
    Private Sub _TreePermGrupo_AfterCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles _TreePermGrupo.AfterCheck
        If e.Node.Checked = False Then
            ' e.Node.ForeColor = Color.Red
            ' e.Node.BackColor = Color.White
            For i As Integer = 0 To e.Node.GetNodeCount(False) - 1
                If e.Node.Nodes(i).Checked Then e.Node.Nodes(i).Checked = False
            Next
        Else
            'e.Node.ForeColor = Color.Black
            'e.Node.BackColor = Color.GreenYellow
            If Not e.Node.Parent Is Nothing Then
                If e.Node.Parent.Checked = False Then e.Node.Parent.Checked = True
            End If
        End If
    End Sub
    Private Sub CargaArbolPermiso()

        StrSql = "SELECT Mp.TAG, Mp.idTipoPer, Mp.NomForma, Tp.Opcion, Tp.NomOpcion " &
        "FROM MnuPermisos Mp LEFT JOIN " &
        "MnuTiposPermisos Tp ON Mp.idTipoPer = Tp.idTipoPer " &
        "WHERE Tp.Opcion <> 0" 'Jala Todos los Permisos. . . al Cargar
        DtMnuPermisos = BD.ExecuteReturn(StrSql)
        DibujaArbolPermisos()
        For i As Integer = 0 To _TreePermGrupo.Nodes.Count - 1 'Tomado de frmUsuarios
            _TreePermGrupo.Nodes(i).Checked = False
            CargaNodos(_TreePermGrupo.Nodes(i))
        Next
    End Sub
    Private Sub DibujaArbolPermisos() 'Tomado de frmUsuarios
        _TreePermGrupo.Nodes.Clear()
        For j = 0 To FMenu.Menu.MenuItems.Count - 1
            With FMenu.Menu.MenuItems(j)
                If .Tag = "" Then Continue For
                With _TreePermGrupo.Nodes.Add(FMenu.Menu.MenuItems(j).Text) 'NIVELES PRINCIPALES!!!
                    .Tag = FMenu.Menu.MenuItems(j).Tag
                    .Checked = False
                    GetNodos(FMenu.Menu.MenuItems(j), _TreePermGrupo.Nodes(j))
                End With
            End With
        Next
    End Sub
    Private Sub GetNodos(ByVal vMenu As MenuItem, ByVal vNodo As TreeNode) 'Tomado de frmUsuarios
        Dim ResMen As String = "", FilItm As Integer = 0
        For Each mycontrol As MenuItem In vMenu.MenuItems
            If mycontrol.IsParent Then
                If mycontrol.Tag = "" Then Continue For
                With vNodo.Nodes.Add(mycontrol.Text)
                    .Tag = mycontrol.Tag
                    .Checked = False
                    GetNodos(mycontrol, vNodo.Nodes(FilItm))
                End With
            Else
                If mycontrol.Tag = "" Then Continue For
                With vNodo.Nodes.Add(mycontrol.Text)
                    .Tag = mycontrol.Tag
                    .Checked = False
                    DtMnuPermisos.DefaultView.RowFilter = "TAG = '" & .Tag & "'"
                    DtMnuPermisos.DefaultView.Sort = "Opcion"
                    For i As Integer = 0 To DtMnuPermisos.DefaultView.Count - 1
                        With vNodo.Nodes(FilItm).Nodes.Add(DtMnuPermisos.DefaultView.Item(i).Item("NomOpcion"))
                            .tag = .parent.tag & "$" & DtMnuPermisos.DefaultView.Item(i).Item("Opcion")
                            .checked = False
                        End With
                    Next
                End With
                FilItm += 1
            End If
        Next
    End Sub
    Private Sub CargaNodos(ByVal vNodo As TreeNode)
        For i As Integer = 0 To vNodo.GetNodeCount(False) - 1
            If vNodo.Nodes(i).GetNodeCount(False) > 0 Then
                CargaNodos(vNodo.Nodes(i))
            Else
                If TieneAccesoUltNodos(vNodo.Nodes(i).Tag) Then
                    vNodo.Nodes(i).Checked = True
                End If
            End If
        Next
    End Sub
    Public Function TieneAccesoUltNodos(ByVal vTag As String) As Boolean
        'Accesos del Grupo o del Usuario!!!, solo si es usuario tendra permisos de grupo
        _dtPermisos.DefaultView.RowFilter = "TAG = '" & vTag & "' AND Acceso = true"
        If _dtPermisos.DefaultView.Count > 0 Then
            Return True
        Else
            Return False
        End If
    End Function
End Class
