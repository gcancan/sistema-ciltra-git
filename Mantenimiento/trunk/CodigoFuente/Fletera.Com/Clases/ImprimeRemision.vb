﻿Public Class ImprimeRemision
    Private vFuenteImprimeCampos As Font
    Private vFuenteImprimeCampos2 As Font
    Private vFuenteImprimeCampos3 As Font
    Private vFuenteImprimeCampos4 As Font
    Private vFuenteImprimeCampos5 As Font

    ' OTROS  ********************************
    Private vConstr As String
    Private vStrSql As String
    Private vStrSqlDET As String
    Private vNomTabla As String
    Private vTipoConexion As TipoConexion
    Private vNomReporte As String
    Private vTablaImprime As DataTable
    Private vImagen1 As Image
    Private vPosicionImagen1 As Point


    'Public DsReporte As New DataSet
    Public DsReporte As DataSet
    Public DsReporteDET As DataSet

#Region "Propiedades"

    Public Property FuenteImprimeCampos() As Font
        Get
            Return vFuenteImprimeCampos
        End Get
        Set(ByVal value As Font)
            vFuenteImprimeCampos = value
        End Set
    End Property
    Public Property FuenteImprimeCampos2() As Font
        Get
            Return vFuenteImprimeCampos2
        End Get
        Set(ByVal value As Font)
            vFuenteImprimeCampos2 = value
        End Set
    End Property
    Public Property FuenteImprimeCampos3() As Font
        Get
            Return vFuenteImprimeCampos3
        End Get
        Set(ByVal value As Font)
            vFuenteImprimeCampos3 = value
        End Set
    End Property
    Public Property FuenteImprimeCampos4() As Font
        Get
            Return vFuenteImprimeCampos4
        End Get
        Set(ByVal value As Font)
            vFuenteImprimeCampos4 = value
        End Set
    End Property

    Public Property FuenteImprimeCampos5() As Font
        Get
            Return vFuenteImprimeCampos5
        End Get
        Set(ByVal value As Font)
            vFuenteImprimeCampos5 = value
        End Set
    End Property

    Public Property TablaImprime() As DataTable
        Get
            Return vTablaImprime
        End Get
        Set(ByVal value As DataTable)
            vTablaImprime = value
        End Set
    End Property

    Public Property Constr() As String
        Get
            Return vConstr
        End Get
        Set(ByVal value As String)
            vConstr = value
        End Set
    End Property

    Public Property StrSql() As String
        Get
            Return vStrSql
        End Get
        Set(ByVal value As String)
            vStrSql = value
        End Set
    End Property

    Public Property StrSqlDET() As String
        Get
            Return vStrSqlDET
        End Get
        Set(ByVal value As String)
            vStrSqlDET = value
        End Set
    End Property

    Public Property NomTabla() As String
        Get
            Return vNomTabla
        End Get
        Set(ByVal value As String)
            vNomTabla = value
        End Set
    End Property

    Public Property TipoConexion() As TipoConexion
        Get
            Return vTipoConexion
        End Get
        Set(ByVal value As TipoConexion)
            vTipoConexion = value
        End Set
    End Property

    Public Property Imagen1() As Image
        Get
            Return vImagen1
        End Get
        Set(ByVal value As Image)
            vImagen1 = value
        End Set
    End Property

    Public Property PosicionImagen1() As Point
        Get
            Return vPosicionImagen1
        End Get
        Set(ByVal value As Point)
            vPosicionImagen1 = value
        End Set
    End Property

#End Region

    Public Function LlenaDsImprime(ByVal Tabla As String) As DataSet
        Dim obj As New CapaNegocio.Tablas
        Dim TipoCon As String = ""
        LlenaDsImprime = Nothing
        Try

            DsReporte = New DataSet
            'DS = obj.CargaDsPorClave(Inicio.ConStrProg, "PROGRESS", "JurCatNotarios", , "nNumNotario")
            If CType(vTipoConexion, TipoConexion) = TipoConexion.TcSQL Then
                TipoCon = "SQL"
            ElseIf CType(vTipoConexion, TipoConexion) = TipoConexion.TcProgress Then
                TipoCon = "PROGRESS"
            ElseIf CType(vTipoConexion, TipoConexion) = TipoConexion.TcFox Then
                TipoCon = "FOX"

            End If
            DsReporte = obj.CargaDsPorSentencia(vConstr, TipoCon, vStrSql, vNomTabla, Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS)

            If Not DsReporte Is Nothing Then
                If DsReporte.Tables(vNomTabla).DefaultView.Count > 0 Then
                    Return DsReporte
                Else
                    'sin datos
                    'Return 0
                End If
            Else
                'Return -1
                'No exiuste
            End If
            If Not obj Is Nothing Then obj = Nothing
        Catch ex As Exception
            If Not obj Is Nothing Then obj = Nothing
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, vNomReporte)
        End Try
    End Function
    Public Function LlenaDsImprimeDET(ByVal Tabla As String) As DataSet
        Dim obj As New CapaNegocio.Tablas
        Dim TipoCon As String = ""
        LlenaDsImprimeDET = Nothing
        Try

            DsReporteDET = New DataSet
            'DS = obj.CargaDsPorClave(Inicio.ConStrProg, "PROGRESS", "JurCatNotarios", , "nNumNotario")
            If CType(vTipoConexion, TipoConexion) = TipoConexion.TcSQL Then
                TipoCon = "SQL"
            ElseIf CType(vTipoConexion, TipoConexion) = TipoConexion.TcProgress Then
                TipoCon = "PROGRESS"
            ElseIf CType(vTipoConexion, TipoConexion) = TipoConexion.TcFox Then
                TipoCon = "FOX"

            End If
            DsReporteDET = obj.CargaDsPorSentencia(vConstr, TipoCon, vStrSqlDET, vNomTabla, Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS)

            If Not DsReporteDET Is Nothing Then
                If DsReporteDET.Tables(vNomTabla).DefaultView.Count > 0 Then
                    Return DsReporteDET
                Else
                    'sin datos
                    'Return 0
                End If
            Else
                'Return -1
                'No exiuste
            End If
            If Not obj Is Nothing Then obj = Nothing
        Catch ex As Exception
            If Not obj Is Nothing Then obj = Nothing
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, vNomReporte)
        End Try
    End Function

    Public Function LlenaDataSet() As Integer
        Dim obj As New CapaNegocio.Tablas
        Dim TipoCon As String = ""
        LlenaDataSet = 0
        Try

            DsReporte = New DataSet
            'DS = obj.CargaDsPorClave(Inicio.ConStrProg, "PROGRESS", "JurCatNotarios", , "nNumNotario")
            If CType(vTipoConexion, TipoConexion) = TipoConexion.TcSQL Then
                TipoCon = "SQL"
            ElseIf CType(vTipoConexion, TipoConexion) = TipoConexion.TcProgress Then
                TipoCon = "PROGRESS"
            ElseIf CType(vTipoConexion, TipoConexion) = TipoConexion.TcFox Then
                TipoCon = "FOX"

            End If
            DsReporte = obj.CargaDsPorSentencia(vConstr, TipoCon, vStrSql, vNomTabla, Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS)

            If Not DsReporte Is Nothing Then
                If DsReporte.Tables(vNomTabla).DefaultView.Count > 0 Then
                    Return DsReporte.Tables(vNomTabla).DefaultView.Count
                Else
                    'sin datos
                    Return 0
                End If
            Else
                Return -1
                'No exiuste
            End If
            If Not obj Is Nothing Then obj = Nothing
        Catch ex As Exception
            If Not obj Is Nothing Then obj = Nothing
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, vNomReporte)
        End Try
    End Function

    Public Function LlenaDataSetDET() As Integer
        Dim obj As New CapaNegocio.Tablas
        Dim TipoCon As String = ""
        LlenaDataSetDET = 0
        Try

            DsReporte = New DataSet
            'DS = obj.CargaDsPorClave(Inicio.ConStrProg, "PROGRESS", "JurCatNotarios", , "nNumNotario")
            If CType(vTipoConexion, TipoConexion) = TipoConexion.TcSQL Then
                TipoCon = "SQL"
            ElseIf CType(vTipoConexion, TipoConexion) = TipoConexion.TcProgress Then
                TipoCon = "PROGRESS"
            ElseIf CType(vTipoConexion, TipoConexion) = TipoConexion.TcFox Then
                TipoCon = "FOX"

            End If
            DsReporteDET = obj.CargaDsPorSentencia(vConstr, TipoCon, vStrSqlDET, vNomTabla, Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS)

            If Not DsReporteDET Is Nothing Then
                If DsReporteDET.Tables(vNomTabla).DefaultView.Count > 0 Then
                    Return DsReporteDET.Tables(vNomTabla).DefaultView.Count
                Else
                    'sin datos
                    Return 0
                End If
            Else
                Return -1
                'No exiuste
            End If
            If Not obj Is Nothing Then obj = Nothing
        Catch ex As Exception
            If Not obj Is Nothing Then obj = Nothing
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, vNomReporte)
        End Try
    End Function
    Public Function ImprimeLineaRecibo(ByVal e As System.Drawing.Printing.PrintPageEventArgs, ByVal Indice As Integer) As Single
        'Dim lineHeight As Single = vFuenteImprimeCampos.GetHeight(e.Graphics)
        Dim PosValorX, PosValorY, PosDescX, PosDescY As Single
        Dim Descripcion As String
        Dim valor As String
        'Dim vfuente As Font
        Dim sFuente As Single

        ''IMPRIME IMAGENES
        If vImagen1 IsNot Nothing Then
            e.Graphics.DrawImage(vImagen1, vPosicionImagen1)
        End If

        'e.Graphics.DrawImage(vImagen2, vPosicionImagen2)
        'yPos += FuenteImprimeCampos.GetHeight(e.Graphics)
        'imprimir Datos

        For i = 0 To vTablaImprime.DefaultView.Count - 1
            Descripcion = vTablaImprime.DefaultView.Item(i).Item("Descripcion").ToString()
            PosDescX = vTablaImprime.DefaultView.Item(i).Item("PosDescX").ToString()
            PosDescY = vTablaImprime.DefaultView.Item(i).Item("PosDescY").ToString()
            sFuente = vTablaImprime.DefaultView.Item(i).Item("Fuente")

            If sFuente = 1 Then
                e.Graphics.DrawString(Descripcion, FuenteImprimeCampos, Brushes.Black, PosDescX, PosDescY)
            ElseIf sFuente = 2 Then
                e.Graphics.DrawString(Descripcion, FuenteImprimeCampos2, Brushes.Black, PosDescX, PosDescY)
            ElseIf sFuente = 3 Then
                e.Graphics.DrawString(Descripcion, FuenteImprimeCampos3, Brushes.Black, PosDescX, PosDescY)
            ElseIf sFuente = 4 Then
                e.Graphics.DrawString(Descripcion, FuenteImprimeCampos4, Brushes.Black, PosDescX, PosDescY)
            ElseIf sFuente = 5 Then
                e.Graphics.DrawString(Descripcion, FuenteImprimeCampos5, Brushes.Black, PosDescX, PosDescY)

            End If

            valor = vTablaImprime.DefaultView.Item(i).Item("valor").ToString()
            PosValorX = vTablaImprime.DefaultView.Item(i).Item("PosValX").ToString()
            PosValorY = vTablaImprime.DefaultView.Item(i).Item("PosValY").ToString()

            If sFuente = 1 Then
                e.Graphics.DrawString(valor, FuenteImprimeCampos, Brushes.Black, PosValorX, PosValorY)
            ElseIf sFuente = 2 Then
                e.Graphics.DrawString(valor, FuenteImprimeCampos2, Brushes.Black, PosValorX, PosValorY)
            ElseIf sFuente = 3 Then
                e.Graphics.DrawString(valor, FuenteImprimeCampos3, Brushes.Black, PosValorX, PosValorY)
            ElseIf sFuente = 4 Then
                e.Graphics.DrawString(valor, FuenteImprimeCampos4, Brushes.Black, PosValorX, PosValorY)
            ElseIf sFuente = 5 Then
                e.Graphics.DrawString(valor, FuenteImprimeCampos5, Brushes.Black, PosValorX, PosValorY)

            End If
        Next



        Return PosDescX
    End Function



End Class
