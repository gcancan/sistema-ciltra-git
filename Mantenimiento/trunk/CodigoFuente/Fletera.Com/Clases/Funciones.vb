﻿Public Class Funciones
    Dim StrSql As String
    Dim DsClase As New DataTable
    Public Function NuevoPrecioUnitario(ByVal Numviaje As Integer, ByVal PrecioTotal As Decimal) As Decimal
        Dim Resultado As Decimal = 0

        'Solo funciona en Microsoft SQL SERVER
        StrSql = "SELECT DBO.fn_NuevoPrecioUnit(" & PrecioTotal & "," & Numviaje & ") as PrecioNew "


        DsClase = BD.ExecuteReturn(StrSql)
        If DsClase.Rows.Count > 0 Then
            With DsClase.Rows(0)
                'Contador = .Item(NomEstatus)
                Resultado = NoNull(.Item("PrecioNew"), "D")
            End With
        End If
        Return Resultado
    End Function

    Public Function NumeroRemisionxViaje(ByVal Numviaje As Integer) As Decimal
        Dim Resultado As Decimal = 0

        StrSql = "SELECT NoRemision FROM dbo.DetGuia WHERE NumGuiaId = " & Numviaje


        DsClase = BD.ExecuteReturn(StrSql)
        If DsClase.Rows.Count > 0 Then
            With DsClase.Rows(0)
                'Contador = .Item(NomEstatus)
                Resultado = NoNull(.Item("NoRemision"), "D")
            End With
        End If
        Return Resultado
    End Function

    Public Function EnumToDataTable(ByVal EnumObject As Type,
   ByVal KeyField As String, ByVal ValueField As String) As DataTable

        Dim oData As DataTable = Nothing
        Dim oRow As DataRow = Nothing
        Dim oColumn As DataColumn = Nothing

        '-------------------------------------------------------------
        ' Sanity check
        If KeyField.Trim() = String.Empty Then
            KeyField = "KEY"
        End If

        If ValueField.Trim() = String.Empty Then
            ValueField = "VALUE"
        End If
        '-------------------------------------------------------------

        '-------------------------------------------------------------
        ' Create the DataTable
        oData = New DataTable

        oColumn = New DataColumn(KeyField, GetType(System.Int32))
        oData.Columns.Add(KeyField)

        oColumn = New DataColumn(ValueField, GetType(System.String))
        oData.Columns.Add(ValueField)
        '-------------------------------------------------------------

        '-------------------------------------------------------------
        ' Add the enum items to the datatable
        For Each iEnumItem As Object In [Enum].GetValues(EnumObject)
            oRow = oData.NewRow()
            oRow(KeyField) = CType(iEnumItem, Int32)
            oRow(ValueField) = StrConv(Replace(iEnumItem.ToString(), "_", " "),
                  VbStrConv.ProperCase)
            oData.Rows.Add(oRow)
        Next
        '-------------------------------------------------------------

        Return oData

    End Function
End Class
