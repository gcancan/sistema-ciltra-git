﻿'//////////////////////////////////
'CLASE AUDITA Version 1.0
'Se tiene que Inicializar en el Evento Load del Formulario que se quiera Auditar,
'En el LLenaDatos se tiene que llamar a la Funcion CargaAudita
'Y Antes de Guardar se Puede Lllamar a GetCambios; Si regresa Vacio Quiere Decir que no hubieron Modificaciones!!!
Public Class AuditaClass
    Private _DescControl As ContenidoObjClass()
    Private _TablaBd As String
    Private Const DelimitadorCadena As String = "$"
    Enum _EnTipoAudita
        Inserta = 0
        Modifica
        Elimina
    End Enum
#Region "Constructor"
    Sub New(ByVal vDescControl As ContenidoObjClass(), ByVal vTablaBd As String)
        _DescControl = vDescControl
        _TablaBd = vTablaBd
    End Sub
    Sub New()
        'SOLO SERVIRA PARA EVITAR ERROR EN EL LOAD!!! pero se debe LLAMAR A LA FUNCION DE ARRIBA o Sino Inicializar
        'CON LAS PROPIEDADES
    End Sub
#End Region
#Region "Propiedades"
    Public Property DescControl As ContenidoObjClass()
        Get
            Return _DescControl
        End Get
        Set(ByVal value As ContenidoObjClass())
            _DescControl = value
        End Set
    End Property
    Public Property TablaBd As String
        Get
            Return _TablaBd
        End Get
        Set(ByVal value As String)
            _TablaBd = value
        End Set
    End Property
#End Region
#Region "Funciones"
    Public Sub CargaAudita()
        If _DescControl Is Nothing Then Exit Sub
        For i As Integer = 0 To _DescControl.Length - 1
            _DescControl(i).CargaContendo()
        Next
    End Sub
    Public Function GetCambios() As String
        Dim CadCam As String = ""
        If _DescControl Is Nothing Then Return ""
        For i As Integer = 0 To _DescControl.Length - 1
            With _DescControl(i)
                If .ContenidoObj <> .GetContenidoObj Then
                    CadCam += .DescripcionCol & ": '" & .ContenidoObj & "' Cambia a [" & .GetContenidoObj & "],"
                End If
            End With
        Next
        If CadCam <> "" Then
            CadCam = Mid(CadCam, 1, CadCam.Length - 1)
        End If
        Return CadCam
    End Function
    Public Sub GuardaAudita(ByVal vTipAudi As _EnTipoAudita)
        Dim ValAnt As String = "", ValNue As String = ""
        If _DescControl Is Nothing Then Throw New Exception("NO SE ha Inicializado Bien la Clase porque No tiene la Descripcion de los Controles a Auditar, Favor de Verificar la PROGRAMACION!!!, consulte al Administrador")
        If vTipAudi = _EnTipoAudita.Modifica Then
            For i As Integer = 0 To _DescControl.Length - 1
                With _DescControl(i)
                    If .EsLlave Then
                        ValAnt = .NombreColTabla & "=" & .ContenidoObj & DelimitadorCadena & ValAnt
                        ValNue = .NombreColTabla & "=" & .GetContenidoObj & DelimitadorCadena & ValNue
                    End If
                    If .ContenidoObj <> .GetContenidoObj Then
                        ValAnt += .NombreColTabla & "=" & .ContenidoObj & DelimitadorCadena
                        ValNue += .NombreColTabla & "=" & .GetContenidoObj & DelimitadorCadena
                    End If
                End With
            Next
        ElseIf vTipAudi = _EnTipoAudita.Inserta Then
            ValAnt = ""
            For i As Integer = 0 To _DescControl.Length - 1
                With _DescControl(i)
                    If .ContenidoObj <> .GetContenidoObj Then
                        ValNue += .NombreColTabla & "=" & .GetContenidoObj & DelimitadorCadena
                    End If
                End With
            Next
        Else 'PARA ELIMINACION
            ValNue = ""
            For i As Integer = 0 To _DescControl.Length - 1
                With _DescControl(i)
                    If .ContenidoObj <> .GetContenidoObj Then
                        ValAnt += .NombreColTabla & "=" & .ContenidoObj & DelimitadorCadena
                    End If
                End With
            Next
        End If
        If ValNue <> "" Then
            ValNue = Mid(ValNue, 1, ValNue.Length - 1)
        End If
        If ValAnt <> "" Then
            ValAnt = Mid(ValAnt, 1, ValAnt.Length - 1)
        End If

        If ValNue <> "" Or ValAnt <> "" Then
            ExecutaAudita(_TablaBd, ValAnt, ValNue, vTipAudi)
        End If

    End Sub
    Public Function GetSQL(ByVal vTipAudi As _EnTipoAudita) As String
        Dim ValAnt As String = "", ValNue As String = ""
        If vTipAudi = _EnTipoAudita.Modifica Then
            For i As Integer = 0 To _DescControl.Length - 1
                With _DescControl(i)
                    If .EsLlave Then
                        ValAnt = .NombreColTabla & "=" & .ContenidoObj & DelimitadorCadena & ValAnt
                        ValNue = .NombreColTabla & "=" & .GetContenidoObj & DelimitadorCadena & ValNue
                    End If
                    If .ContenidoObj <> .GetContenidoObj Then
                        ValAnt += .NombreColTabla & "=" & .ContenidoObj & DelimitadorCadena
                        ValNue += .NombreColTabla & "=" & .GetContenidoObj & DelimitadorCadena
                    End If
                End With
            Next
        ElseIf vTipAudi = _EnTipoAudita.Inserta Then
            ValAnt = ""
            For i As Integer = 0 To _DescControl.Length - 1
                With _DescControl(i)
                    If .ContenidoObj <> .GetContenidoObj Then
                        ValNue += .NombreColTabla & "=" & .GetContenidoObj & DelimitadorCadena
                    End If
                End With
            Next
        Else 'PARA ELIMINACION
            ValNue = ""
            For i As Integer = 0 To _DescControl.Length - 1
                With _DescControl(i)
                    'If .ContenidoObj <> .GetContenidoObj Then
                    ValAnt += .NombreColTabla & "=" & .ContenidoObj & DelimitadorCadena
                    'End If
                End With
            Next
        End If
        If ValNue <> "" Then
            ValNue = Mid(ValNue, 1, ValNue.Length - 1)
        End If
        If ValAnt <> "" Then
            ValAnt = Mid(ValAnt, 1, ValAnt.Length - 1)
        End If

        If ValNue <> "" Or ValAnt <> "" Then
            Return GetSqlAudita(_TablaBd, ValAnt, ValNue, vTipAudi)
        Else
            Return ""
        End If
    End Function
    Private Sub ExecutaAudita(ByVal vTabla As String, ByVal vRegistrosAntes As String, ByVal vRegistrosNuevos As String, ByVal vTipoAccion As _EnTipoAudita, Optional ByVal EsTransaccion As Boolean = False, Optional ByVal vFecha As Date = Nothing, Optional ByVal vUsuario As String = "")
        Dim TipAcc As String = ""
        If vTipoAccion = _EnTipoAudita.Inserta Then
            TipAcc = "INS"
        ElseIf vTipoAccion = _EnTipoAudita.Modifica Then
            TipAcc = "MOD"
        Else
            TipAcc = "DEL"
        End If
        If vFecha.Date.ToString("yyyy/MM/dd") = "0001/01/01" Then vFecha = Now
        If vUsuario = "" Then vUsuario = UserConectado.idUsuario
        BD.Execute("INSERT INTO sysAudit(sTabla,sRegistroaModificar,sRegistroModificado,dFecha,sUsuario,sTipoAccion) " &
                   "VALUES('" & vTabla & "','" & vRegistrosAntes & "','" & vRegistrosNuevos & "','" & FormatFecHora(vFecha, True, True) & "','" & vUsuario & "','" & TipAcc & "')", EsTransaccion)
    End Sub
    Private Function GetSqlAudita(ByVal vTabla As String, ByVal vRegistrosAntes As String, ByVal vRegistrosNuevos As String, ByVal vTipoAccion As _EnTipoAudita, Optional ByVal EsTransaccion As Boolean = False, Optional ByVal vFecha As Date = Nothing, Optional ByVal vUsuario As String = "") As String
        Dim TipAcc As String = ""
        If vTipoAccion = _EnTipoAudita.Inserta Then
            TipAcc = "INS"
        ElseIf vTipoAccion = _EnTipoAudita.Modifica Then
            TipAcc = "MOD"
        Else
            TipAcc = "DEL"
        End If
        If vFecha.Date.ToString("yyyy/MM/dd") = "0001/01/01" Then vFecha = Now
        If vUsuario = "" Then vUsuario = UserConectado.idUsuario
        Return "INSERT INTO sysAudit(sTabla,sRegistroaModificar,sRegistroModificado,dFecha,sUsuario,sTipoAccion) " &
                   "VALUES('" & vTabla & "','" & vRegistrosAntes & "','" & vRegistrosNuevos & "','" & FormatFecHora(vFecha, True, True) & "','" & vUsuario & "','" & TipAcc & "')"
    End Function

#End Region

End Class
Public Class ContenidoObjClass
    'Private _NombreCol As String
    Private _DescripcionCol As String
    Private _Control As Object
    Private _ContenidoObj As String
    Private _NombreColTabla As String
    Private _EsLlave As Boolean
#Region "Constructor"
    ''' <summary>
    ''' Se debe usar un ColAudita por Cada Control(Texttbox,ChekBox, Etc) que se quiera Auditar
    ''' </summary>
    ''' <param name="vControl">Control que se Quiere Auditar Ejeplo TextBox</param>
    ''' <param name="vNomColTabla">Nombre de la Columna en la Base de Datos</param>
    ''' <param name="vDescripcionCol">Parametro Opcional que Sirve para un Texto mas comodo para el Usuario Se muestra en Funcion GetCambios Si se omite Se tima el Nombre de vNomColTabla</param>
    ''' <remarks></remarks>
    Sub New(ByVal vControl As Object, ByVal vNomColTabla As String, Optional ByVal vDescripcionCol As String = "", Optional ByVal vEsLlave As Boolean = False)
        _Control = vControl
        _NombreColTabla = vNomColTabla
        If vDescripcionCol = "" Then
            _DescripcionCol = vNomColTabla
        Else
            _DescripcionCol = vDescripcionCol
        End If
        _EsLlave = vEsLlave
    End Sub
    ''' <summary>
    ''' Se debe usar un ColAudita por Cada Grupo de RadioBotones que se quiera Auditar
    ''' </summary>
    ''' <param name="vGrupoOptions">Cuando se Tiene mas de Una Opcion y Solo la Seleccionada Se Guardara en la Base de Datos</param>
    ''' <param name="vNomColTabla">Nombre de la Columna en la Base de Datos</param>
    ''' <param name="vDescripcionCol">Parametro Opcional que Sirve para un Texto mas comodo para el Usuario Se muestra en Funcion GetCambios Si se omite Se tima el Nombre de vNomColTabla</param>
    ''' <remarks></remarks>
    Sub New(ByVal vGrupoOptions As RadioButton(), ByVal vNomColTabla As String, Optional ByVal vDescripcionCol As String = "")
        _Control = vGrupoOptions
        _NombreColTabla = vNomColTabla
        If vDescripcionCol = "" Then
            _DescripcionCol = vNomColTabla
        Else
            _DescripcionCol = vDescripcionCol
        End If
        _EsLlave = False
    End Sub
    'Sub New(ByVal vGrupoCheckBox As CheckBox(), ByVal vNameColTabla As String, ByVal vDescripcionCol As String)
    '    _Control = vGrupoCheckBox
    '    _DescripcionCol = vDescripcionCol
    '    _NameColTabla = vNameColTabla
    'End Sub
#End Region
#Region "Propiedades"
    Public Property DescripcionCol As String
        Get
            Return _DescripcionCol
        End Get
        Set(ByVal value As String)
            _DescripcionCol = value
        End Set
    End Property
    Public ReadOnly Property EsLlave As Boolean
        Get
            Return _EsLlave
        End Get
    End Property
    Public Property ContenidoObj As String
        Get
            Return _ContenidoObj
        End Get
        Set(ByVal value As String)
            _ContenidoObj = value
        End Set
    End Property
    Public Property NombreColTabla As String
        Get
            Return _NombreColTabla
        End Get
        Set(ByVal value As String)
            _NombreColTabla = value
        End Set
    End Property
#End Region
#Region "Funciones"
    Public Sub CargaContendo()
        _ContenidoObj = GetContenidoObj()
    End Sub
    Public Function GetContenidoObj() As String
        Dim Cont As String = ""
        If TypeOf (_Control) Is TextBox Then
            Cont = TryCast(_Control, TextBox).Text
        ElseIf TypeOf (_Control) Is ComboBox Then
            Cont = TryCast(_Control, ComboBox).Text
        ElseIf TypeOf (_Control) Is NumericUpDown Then
            Cont = CStr(TryCast(_Control, NumericUpDown).Value)
        ElseIf TypeOf (_Control) Is DateTimePicker Then
            Cont = TryCast(_Control, DateTimePicker).Value.ToShortDateString
        ElseIf TypeOf (_Control) Is RadioButton Then
            Cont = CStr(IIf(TryCast(_Control, RadioButton).Checked, "1", "0"))
        ElseIf TypeOf (_Control) Is CheckBox Then
            Cont = CStr(IIf(TryCast(_Control, CheckBox).Checked, "1", "0"))
        ElseIf TypeOf (_Control) Is RadioButton() Then
            Cont = "Ninguno Seleccionado"
            For Each Rd As RadioButton In TryCast(_Control, RadioButton())
                If Rd.Checked Then
                    Cont = Rd.Text '& " Seleccionado"
                    Exit For
                End If
            Next
            'ElseIf TypeOf (_Control) Is CheckBox() Then
            '    Cont = ""
            '    For Each Rd As CheckBox In TryCast(_Control, CheckBox())
            '        If Rd.Checked Then
            '            Cont = Rd.Text & " Seleccionado"
            '        End If
            '    Next
        End If
        Return Cont
    End Function
#End Region

End Class