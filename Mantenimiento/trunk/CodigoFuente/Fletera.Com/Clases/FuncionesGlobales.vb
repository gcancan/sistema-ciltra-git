﻿Imports Microsoft.VisualBasic.Compatibility
Imports System.Text
Imports System.Data.SqlClient

Module FuncionesGlobales
    '_____________________________DECLARACIÓN DE VARIABLES GLOBALES___________________________________
    'Public gRuta As New VB6.FixedLengthString(kLongNombreProducto)
    Public gIdDocto As Integer
    ' Procedimiento MensajeError
    ' Descripción: Procedimiento que recibe recibe un código de error y muestra
    '

    Public Sub DespliegaError(ByRef aError As Integer)
        Dim Res As String = rError(aError)
        MsgBox(IIf(Res.ToString = "", "Mensaje Desconocido ", Res), MsgBoxStyle.Critical, "SDK  COMERCIAL i")
    End Sub

    Public Sub MensajeError(ByRef aError As Integer)
        'Dim aMensaje As String
        Dim aMensaje As StringBuilder

        aMensaje = New StringBuilder(Chr(0), 349)
        ' Recupera el mensaje de error del SDK
        fError(aError, aMensaje, 350)
        MsgBox(IIf(aMensaje.ToString = "", "Mensaje Desconocido ", aMensaje), MsgBoxStyle.Critical, "SDK ADMINPAQ")
    End Sub

    ' Procedimiento para Abrir una Empresa
    'Public Sub AbrirEmpresa(ByVal Ruta As String)
    '    Dim lError As Integer
    '    'MsgBox("fAbreEmpresa(Ruta) " & Ruta)
    '    lError = fAbreEmpresa(Ruta)
    '    'MsgBox(lError)
    '    If lError <> 0 Then
    '        MensajeError(lError)
    '        End
    '    End If

    '    lLicencia = 0
    '    'MsgBox("fInicializaLicenseInfo(lLicencia) " & lLicencia)
    '    lError = fInicializaLicenseInfo(lLicencia) ' 0=AdminPAQ, 1=FACTURA ELECTRÓNICA
    '    'MsgBox(lError)
    '    If lError <> 0 Then
    '        MensajeError(lError)
    '        End
    '    End If

    '    'frmAbreEmpresa.DefInstance.Activa_Botones((True))
    '    'frmAbreEmpresa.DefInstance.btnAbrirEmpresa.Enabled = False

    'End Sub

    ' Procedimiento f_RellenaConBlancos
    ' Descripción: Procedimiento que recibe una cadena y le concatena espacios
    ' y el caracter nulo al final para dejarla de una longitud determinada.
    ' Parametros: aCadena (String), aTamanio (Integer)
    ' Retorna: aCadena con la lungitud indicada y el caracter nulo como finalizador de cadena
    Public Function f_RellenaConBlancos(ByRef aCadena As String, ByRef aTamanio As Short) As String

        Dim lEspacios As String
        Dim lTamanio As Integer

        lEspacios = Space(aTamanio)
        lTamanio = aTamanio - Len(Trim(aCadena)) - 1

        aCadena = Trim(aCadena) & Left(lEspacios, lTamanio) & Chr(0)
        f_RellenaConBlancos = aCadena
    End Function

    'Data Source=LAPHP;Initial Catalog=adFletera_2;Integrated Security=True
    Public Function CadenaConexion2(ByVal Servidor As String, ByVal NomBd As String) As SqlConnectionStringBuilder

        'Return "Data Source=" & Servidor & ";Initial Catalog=" & NomBd.Trim & ";Integrated Security=True"

        Dim MyStringBuilder As New SqlConnectionStringBuilder()
        MyStringBuilder.DataSource = UCase(sNomServidor)
        MyStringBuilder.InitialCatalog = NomBd
        MyStringBuilder.IntegratedSecurity = True

        Return MyStringBuilder
    End Function
End Module
