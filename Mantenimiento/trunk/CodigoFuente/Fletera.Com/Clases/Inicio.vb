﻿Option Explicit On
Imports System.Security.Cryptography
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.Win32
Imports System.IO
Imports System.Windows.Forms
Imports SerialDialog
Imports System.Runtime.InteropServices

#Region "Enums Inicial"

Public Enum opcEstatusUbiUni
    RUTA = 0
    BASE = 1
    TALLER = 2
    LAVADERO = 3
    LLANTAS = 4
    TALLER_EX = 5
    GASOLINERA = 6
    DESCONOCIDO = 10
End Enum

Public Enum opcTipoTicket
    OrdenServ = 1
    PreSalida = 2
    PreOrdenCompra = 3
End Enum
Public Enum OpcTipoElementoViaje
    Tractor = 1
    Remolque1 = 2
    Dolly = 3
    Remolque2 = 4
    Operador = 5

End Enum

Public Enum opTipoEmpleado
    Responsable = 1
    Ayudante1 = 2
    Ayudante2 = 3


End Enum
Public Enum opServLlantas
    NO_APLICA = 0
    DESMONTAR = 1
    MONTAR_UNIDAD = 2
    ROTAR_MISMA_UNIDAD = 3
    CAMBIAR_OTRA_UNIDAD = 4
    CAMBIO_ALMANCEN = 5
    RECHAZADA_PROVEEDOR = 6
End Enum

Public Enum OpcionLlanta
    idSisLlanta = 1
    idLlanta = 2
    NoSerie = 3
    Posicion = 4
End Enum

Public Enum TipoDocumento
    Orden_Compra = 1
    Orden_Servicio = 2
    Orden_Revitalizado = 3
    Orden_Baja = 4
    Rescate = 5
    Inventario = 6
    ServicioInterno = 7

End Enum

Public Enum TipoUbicacionLLa
    ALMACEN = 1
    PROVEEDOR = 2
    UNIDADTRANS = 3
    PILABAJA = 4
End Enum

Public Enum TipoOrdenServicio
    COMBUSTIBLE = 1
    TALLER = 2
    LLANTAS = 3
    LAVADERO = 4
    RESGUARDO = 5
    TODAS = 6
End Enum
Public Enum TipoDocDinero
    Recibo = 0
    Folio = 1
End Enum
Public Enum TipoOpcionEstatus
    SinOpcion = 0
    Terminar = 1
    Entregar = 2
    Cancelar = 3
End Enum

Public Enum TipoOpcionActualiza
    ToNinguno = 0
    ToDecrementa = 1
    ToIncrementa = 2
End Enum
Public Enum TipoDato
    TdNumerico = 0
    TdCadena = 1
    TdFecha = 2
    TdBoolean = 3
    TdFechaHora = 4
    TdHora = 5
    'TdEntero = 5
End Enum

Public Enum TipoConexion
    TcSQL = 0
    TcProgress = 1
    TcFox = 2
End Enum

Public Enum EnTipAccion
    Inserta = 0
    Modifica = 1
    Elimina = 2
End Enum

Public Enum TipoOpcionForma
    tOpcInsertar
    tOpcModificar
    tOpcEliminar
    tOpcConsultar
    tOpcImprimir
    tOpcCancelar
    tOpcPendiente
    tOpcFacturar
    tOpcSalir
End Enum

Public Enum TipoOpcActivaBoton
    tOpcConsulta
    tOpcInicializa
    tOpcInsertar
    tOpcEspecial
    tOpcEditar
End Enum

Public Enum TipoOpcActivaCampos
    tOpcDESHABTODOS
    tOpcINICIALIZA
    tOpcOTRO
    tOpcDEPENDEVALOR
End Enum
Public Enum TipoNumAyudante
    Ayudante1 = 1
    Ayudante2 = 2
End Enum
#End Region '"Enums Inicial"

#Region "Enums"
Public Enum TipotablasPer
    tpChoferes
    tpSupervisores
    tpDespachadores
    tpTrafico
    tpTodos
    TpTaller
    TpAsignaTaller
End Enum


'PARA REPORTES
Public Enum TipoAlineacion
    TdIzquierda = 0
    TdCentrado = 1
    TdDerecha = 2
End Enum

Public Enum TipoOrdenamiento
    Asc = 0
    Desc = 1
End Enum

Public Enum TipoControlReport
    TdCadena = 0
    TdNumerico = 1
    TdRangoNum = 2
    TdFecha = 3
    TdBoolean = 4
    TdLikeCadena = 5
    TdRangoFecha = 6
End Enum

Public Enum TipoSentencia
    TsInsertar = 0
    TsModificar = 1
    TsEliminar = 2
End Enum

Public Enum TipoFacturacion
    TfPegaso = 1
    TfAtlante = 2
    TfGlobal = 3
End Enum

Public Enum MetodoFacturacion
    tmfAgrupado = 1
    tmfGlobal = 2
    tmfDetallado = 3
    tmfGlobalCant = 4
    tmfAgrupadoDia = 5


End Enum

Public Enum OpcionExcelFactura
    NoSAp = 1
    Preliquidacion = 2

End Enum
Public Enum OpcTipoOrdenServ
    Taller = 1
    Lavadero = 2
    Llantas = 3

End Enum

#End Region '"Enums"

Module Inicio
    '*--------------------------------------------------------------------------------------------------------------------
    'VARIABLES INICIO

    Public CONSTR As String = ""
    Public CONSTR_COM As String = ""
    Public ConStrFox As String = ""
    Public CnStrEsp As String = ""

    'en cmdOk_Click de frmLogin se Inicializa
    Public BD As CapaDatos.UtilSQL
    Public BDCOM As CapaDatos.UtilSQL
    Public BDCOMConf As CapaDatos.UtilSQL

    Public UserConectado As UsuarioClass

    Public esPrueba As Boolean
    Dim RegFile As String = ""
    Dim TMSetup As String = ""

    'EMPRESA ADMINPAQ
    Public sRutaEmpresaAdmPAQ As String = ""
    Public sNombreEmpresaAdmPAQ As String = ""
    Public sIdEmpresa As Integer = 0
    Public Parametro_DIRBASE As String = ""

    Public bEsUserComercial As Boolean = True

    Public sNombrePAQ As String = "CONTPAQ I COMERCIAL"
    'Public sNombrePAQ As String = "COMERCIAL"
    Public smensaje As StringBuilder = New StringBuilder(512)
    Public szRegKeySistema As String = "SOFTWARE\\Computación en Acción, SA CV\\CONTPAQ I COMERCIAL"
    Public bActivoSDK As Boolean
    Public bEmpresaSDKAbierta As Boolean
    Public lError As Integer = 0

    'MANEJO DE ERRORES
    Public Const KEY_CHECAERROR As String = "ChecaError"
    Public Const KEY_ESTATUS As String = "Estatus"

    'CONSTANTES DEL SISTEMA
    Public Const KEY_CONFIGURACION As String = "Configuracion"
    Public Const KEY_CONFUSUARIO As String = "Usuario"
    Public Const KEY_CONFIG_USUARIO As String = "SUPERVISOR"

    'POSIBLES RESPUESTAS DE CONSULTA O SENTENCIA SQL
    Public Const KEY_RCORRECTO As String = "CORRECTO"
    Public Const KEY_RINCORRECTO As String = "INCORRECTO"
    Public Const KEY_RERROR As String = "ERROR"

    Public Const KEY_CONFIG_ENCRYPT As String = "RgDsgSoft"

    'PARA REGISTRO
    Public Const KEY_CONFIG_SECCION As String = "Parametro"
    Public Const KEY_CONFIF_KEY As String = "Ubicacion"

    Public UserId, GrupoUser, NomUser, sNomBd, sNomServidor, sNomUsuarioSQL, sPassUsuarioSQL As String
    Public sAutentificacionWindows As Boolean

    'COMERCIAL
    Public sNomBdCOM, sNomServidorCOM, sNomUsuarioSQLCOM, sPassUsuarioSQLCOM As String
    Public sAutentificacionWindowsCOM As Boolean
    Public sRutaBdCOM As String = ""


    Public UbicacionAdminPaq As String
    Public UbicacionSDK As String = ""

    Public idActualizacion As Decimal = 20160213.5
    Public Version As Decimal = 1.0

    'PARAMETROS
    'DATOS EMPRESA
    Public Parametro_Empresa As String
    Public Parametro_Direccion As String
    Public Parametro_RFC As String
    Public Parametro_Telefono As String

    Public Parametro_UsuarioSQL As String
    Public Parametro_PasswordSQL As String

    Public Parametro_CCODIGOA01_SALdefault As String

    Public KeyPathCA As String = "SOFTWARE\"
    Public KeyPathLM As String = "SOFTWARE\RgDsgSoft"
    'HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Computación en Acción, SA CV\AdminPAQ
    'Public KeyPathAdminPAQ As String = "HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Computación en Acción, SA CV\AdminPAQ"
    'Public KeyPathAdminPAQ As String = "SOFTWARE\Wow6432Node\Computación en Acción, SA CV\AdminPAQ"
    Public KeyPathAdminPAQ As String = "SOFTWARE\Computación en Acción, SA CV"
    Public KeyPathWinPath As String = "SYSTEM\ControlSet001\Control\Session Manager\Environment"

    Dim DS As DataSet
    '*--------------------------------------------------------------------------------------------------------------------
    'VARIABLES 
    Public Sep As Char = Application.CurrentCulture.NumberFormat.NumberDecimalSeparator
    Public StrColaImpRecibo As String = ""
    Public Parametro_PassFacturacion As String
    Public Const MENSAJE_CLAVE = "El ID no puede ser Vacio"
    'qUITAR LUEGO
    Public Parametro_CPLAMIGCFD As String
    Public Parametro_GrupoDefault As String
    Public OtroValorGrid As String = ""
    Public ExisteColaImp As String = ""


    Public TiempoUsado As MiStopWath

    <DllImport("kernel32.dll", SetLastError:=True)>
    Public Function GetPrivateProfileString(ByVal lpAppName As String,
                            ByVal lpKeyName As String,
                            ByVal lpDefault As String,
                            ByVal lpReturnedString As StringBuilder,
                            ByVal nSize As Integer,
                            ByVal lpFileName As String) As Integer
    End Function


#Region "Funciones Iniciales"
    Private Function ChecaValorRegistroOculto(ByVal HideFilePath As String) As String
        Dim HideInfo As String
        'HideInfo = FileReadWrite.ReadFile(HideFilePath).Split(";"c)
        HideInfo = FileReadWrite.ReadFile(HideFilePath).ToString
        If HideInfo <> "" Then
            'Return HideInfo(0) & HideInfo(1) & HideInfo(2) & HideInfo(3)
            Return HideInfo
        Else
            Return ""
        End If
    End Function

    Public Function ComprobarLicencia() As Boolean
        Dim _RegFilePath As String = Application.StartupPath & "\RegFile.dll"
        Dim _HideFilePath As String = Application.StartupPath & "\TMSetp.dll"

        ComprobarLicencia = False
        Try

            '*** PENDIENTE *** -> PROCESO PARA ENCRIPTAR LAS CLAVES DE REGISTRO
            'generarClaveSHA1(Trim(txtsPasword.Text))
            'generarClaveSHA1(vPass)
            '        Dim ObjSeg As New Criptografia

            'RegFile = GetSetting(Process.GetCurrentProcess.ProcessName, "RegSoft", "RegFile", "") & ""
            'TMSetup = GetSetting(Process.GetCurrentProcess.ProcessName, "RegSoft", "TMSetup", "") & ""


            If GetSetting(Process.GetCurrentProcess.ProcessName, "RegSoft", "RegFile", "") & "" <> "" Then
                RegFile = Criptografia.Decrypt(GetSetting(Process.GetCurrentProcess.ProcessName, "RegSoft", "RegFile", "") & "", "RgDsgSoft")
            Else
                RegFile = GetSetting(Process.GetCurrentProcess.ProcessName, "RegSoft", "RegFile", "") & ""
            End If

            If GetSetting(Process.GetCurrentProcess.ProcessName, "RegSoft", "TMSetup", "") & "" <> "" Then
                TMSetup = Criptografia.Decrypt(GetSetting(Process.GetCurrentProcess.ProcessName, "RegSoft", "TMSetup", "") & "", "RgDsgSoft")
            Else
                TMSetup = GetSetting(Process.GetCurrentProcess.ProcessName, "RegSoft", "TMSetup", "") & ""
            End If

            'Grabar en Archivo - NEW
            If TMSetup <> "" Then
                FileReadWrite.WriteFile(_HideFilePath, TMSetup)
                'If Not File.Exists(_HideFilePath) Then
                '    FileReadWrite.WriteFile(_HideFilePath, TMSetup)
                'End If
            Else
                If File.Exists(_HideFilePath) Then
                    TMSetup = ChecaValorRegistroOculto(_HideFilePath)
                    'SaveSetting(Process.GetCurrentProcess.ProcessName, "RegSoft", "RegFile", Criptografia.Encrypt(TMSetup, "RgDsgSoft"))
                    SaveSetting(Process.GetCurrentProcess.ProcessName, "RegSoft", "TMSetup", Criptografia.Encrypt(TMSetup, "RgDsgSoft"))
                End If
            End If

            If RegFile <> "" Then
                If Not File.Exists(_RegFilePath) Then
                    FileReadWrite.WriteFile(_RegFilePath, RegFile)
                End If
            Else
                If File.Exists(_RegFilePath) Then
                    RegFile = ChecaValorRegistroOculto(_RegFilePath)
                    'SaveSetting(Process.GetCurrentProcess.ProcessName, "RegSoft", "RegFile", Criptografia.Encrypt(TMSetup, "RgDsgSoft"))
                    SaveSetting(Process.GetCurrentProcess.ProcessName, "RegSoft", "RegFile", Criptografia.Encrypt(RegFile, "RgDsgSoft"))
                End If
            End If
            'MsgBox("LINEA : 433")
            'Dim objLicencia As SerialDialog.TrialMaker
            'Dim objLicencia As New SerialDialog.TrialMaker(Application.ProductName,
            '                RegFile,
            '                TMSetup,
            '                "Para comunicarse con el administrador del Sistema : " & Application.ProductName & vbNewLine &
            '                "Correo Electr                                                                                                 ónico: RgDsgSoft@gmail.com",
            '                30, 500, "751")
            Dim objLicencia As New TrialMaker(Application.ProductName,
                RegFile,
                TMSetup,
                "Para comunicarse con el administrador del Sistema : " & Application.ProductName & " Correo Electrónico: des.SoftNG@gmail.com",
                30, 500, "353", 0)

            'desarrollo@sistemasyseguridad.com.mx
            '"751" identificador 

            'Desarrollo Sistemas -  SISTEMAS Y DE SEGURIDAD <desarrollo@sistemasyseguridad.com.mx>
            '"Para comunicarse con el administrador del Sistema : " & Application.ProductName & " Correo Electrónico: RgDsgSoft@gmail.com",

            'MsgBox(Application.ProductName)
            'MsgBox(RegFile)
            'MsgBox(TMSetup)
            'MsgBox(objLicencia)

            'MsgBox("LINEA : 442")


            Dim miKey As Byte() = {97, 250, 1, 5, 84, 21, 7, 63,
                4, 54, 87, 56, 123, 10, 3, 62,
                7, 9, 20, 36, 37, 21, 101, 57}
            objLicencia.TripleDESKey = miKey

            'Dim RT As  = t.ShowDialog()
            Dim RT As SerialDialog.TrialMaker.RunTypes = objLicencia.ShowDialog()
            If RT <> SerialDialog.TrialMaker.RunTypes.Expired Then
                If RT = SerialDialog.TrialMaker.RunTypes.Full Then
                    Return False
                Else
                    Return True
                End If
            Else
                End 'Ya expiro por eso se cierra la aplicacion
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Se produjo un error, consulte con soporte", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try

    End Function

    Public Sub ImprimeRemision(ByVal bRecibeDatos As Boolean, Optional ByVal NoMovimiento As Integer = 0)
        'Public Sub ImprimeRemision(ByVal bRecibeDatos As Boolean, Optional ByVal Tipo As String = "", Optional ByVal NoMovimiento As Integer = 0)
        Dim frm As New frmReporteador
        If bRecibeDatos Then
            frm.RecibeDatos(NoMovimiento, "")
        End If

        frm.NomTabla = "REMISION"
        frm.BanHorizontal = True
        frm.BanCuadricula = False
        frm.BanImpTotales = True
        frm.BanBorderDetalle = False


        frm.TituloReporte = "Remisión"

        'SELECT NOMOVIMIENTO FROM CABMOVIMIENTOS WHERE  ESTATUS in ('TER') and nomovimiento = nomovimientopadre ORDER BY NOMOVIMIENTO ASC


        'New ColClass("NoMovimiento", "No. Movimiento", TipoDato.TdNumerico, True, False, TipoOrdenamiento.Asc, TipoControlReport.TdCadena, False, 1, , , "SELECT DISTINCT NOMOVIMIENTO FROM CABMOVIMIENTOS WHERE  ESTATUS in ('CAP','TER') ORDER BY NOMOVIMIENTO ASC", , ColClass.EnTipoFilCadena.Obligatorio),
        'New ColClass("TipoMovimiento", "Tipo Movimiento", TipoDato.TdCadena, True, False, TipoOrdenamiento.Asc, TipoControlReport.TdLikeCadena, False, 1, , , , , ColClass.EnTipoFilCadena.Obligatorio, , , "CAB"),
        frm.ConfigRep({New ColClass("NoMovimiento", "No. Traspaso", TipoDato.TdNumerico, True, False, TipoOrdenamiento.Asc, TipoControlReport.TdCadena, False, 1, , , "SELECT NOMOVIMIENTO FROM CABMOVIMIENTOS WHERE  ESTATUS in ('TER') and tipomovimiento = 'TRAS' ORDER BY NOMOVIMIENTO DESC", , ColClass.EnTipoFilCadena.Obligatorio),
                            New ColClass("CCODIGOP01_PRO", "Código", TipoDato.TdCadena, False, False, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 33),
                            New ColClass("CNOMBREP01", "Descripción", TipoDato.TdCadena, False, False, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 200),
                            New ColClass("UNIDAD", "Uni.", TipoDato.TdCadena, False, False, TipoOrdenamiento.Desc, TipoControlReport.TdNumerico, True, 18),
                            New ColClass("Cantidad", "    Cant.", TipoDato.TdNumerico, False, False, TipoOrdenamiento.Desc, TipoControlReport.TdNumerico, True, 40),
                            New ColClass("Costo", "    Costo", TipoDato.TdNumerico, False, False, TipoOrdenamiento.Desc, TipoControlReport.TdNumerico, True, 55),
                            New ColClass("IMPORTE", "   Importe", TipoDato.TdNumerico, False, False, TipoOrdenamiento.Desc, TipoControlReport.TdNumerico, True, 1),
                            New ColClass("No. Copias", {New NumericUpDown With {.DecimalPlaces = 0, .Value = 1, .Width = 50, .Minimum = 1}})
                            })
        frm.ShowDialog()
        frm.Dispose()
    End Sub

    Public Function ChecaClave(ByVal cCve As String, ByVal Constr As String, ByVal TipoCon As TipoConexion, _
ByVal dbtabla As String, ByVal Campo As String, ByVal TipoCampo As TipoDato, ByVal NomAplicacion As String, _
ByVal sErrorLlave As String, ByVal sValorLlave As String, Optional ByVal FiltroEsp As String = "") As DataSet
        Dim Filtro As String = ""
        Dim obj As New CapaNegocio.Tablas
        Dim TipoConex As String
        ChecaClave = Nothing
        Try

            If CType(TipoCampo, TipoDato) = TipoDato.TdCadena Or CType(TipoCampo, TipoDato) = TipoDato.TdFecha Then
                Filtro = Campo & " = '" & cCve & "'"
            ElseIf CType(TipoCampo, TipoDato) = TipoDato.TdNumerico Or CType(TipoCampo, TipoDato) = TipoDato.TdBoolean Then
                Filtro = Campo & " = " & cCve
            End If

            TipoConex = DeterminaNombreConexion(TipoCon)

            'DS = Nothing
            DS = obj.CargaDsPorClave(Constr, TipoConex, dbtabla, Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, Filtro)

            If Not DS Is Nothing Then
                'ChecaClave = False
                If DS.Tables(dbtabla).DefaultView.Count > 0 Then
                    If FiltroEsp <> "" Then
                        DS.Tables(dbtabla).DefaultView.RowFilter = FiltroEsp
                    End If
                    ChecaClave = DS
                End If
            Else
                ChecaClave = Nothing
            End If
            If Not obj Is Nothing Then obj = Nothing
        Catch Ex As Exception
            If Not obj Is Nothing Then obj = Nothing
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & Ex.Message, MsgBoxStyle.Information, dbtabla)
            ChecaClave = Nothing
        End Try
    End Function

    Public Function DeterminaNombreConexion(ByVal TipoCon As TipoConexion) As String
        DeterminaNombreConexion = ""
        If CType(TipoCon, TipoConexion) = TipoConexion.TcProgress Then
            DeterminaNombreConexion = "PROGRESS"
        ElseIf CType(TipoCon, TipoConexion) = TipoConexion.TcSQL Then
            DeterminaNombreConexion = "SQL"
        ElseIf CType(TipoCon, TipoConexion) = TipoConexion.TcFox Then
            DeterminaNombreConexion = "FOX"
        End If
    End Function

    Public Function generarClaveSHA1(ByVal nombre As String) As String
        ' Crear una clave SHA1 como la generada por 
        ' FormsAuthentication.HashPasswordForStoringInConfigFile
        ' Adaptada del ejemplo de la ayuda en la descripción de SHA1 (Clase)
        Dim enc As New UTF8Encoding
        Dim data() As Byte = enc.GetBytes(nombre)
        Dim result() As Byte

        Dim sha As New SHA1CryptoServiceProvider
        ' This is one implementation of the abstract class SHA1.
        result = sha.ComputeHash(data)
        '
        ' Convertir los valores en hexadecimal
        ' cuando tiene una cifra hay que rellenarlo con cero
        ' para que siempre ocupen dos dígitos.
        Dim sb As New StringBuilder
        For i As Integer = 0 To result.Length - 1
            If result(i) < 16 Then
                sb.Append("0")
            End If
            sb.Append(result(i).ToString("x"))
        Next
        '
        Return sb.ToString.ToUpper
    End Function

    Public Sub Audita(ByVal vTabla As String, ByVal vRegistrosAntes As String, ByVal vRegistrosNuevos As String, ByVal vTipoAccion As EnTipAccion, Optional ByVal vFecha As Date = Nothing, Optional ByVal vUsuario As String = "")
        Dim TipAcc As String = ""
        If vTipoAccion = EnTipAccion.Inserta Then
            TipAcc = "INS"
        ElseIf vTipoAccion = EnTipAccion.Modifica Then
            TipAcc = "MOD"
        Else
            TipAcc = "DEL"
        End If
        If vFecha.Date.ToString("yyyy/MM/dd") = "0001/01/01" Then vFecha = Now
        If vUsuario = "" Then vUsuario = NomUser
        BD.Execute("INSERT INTO sysAudit(sTabla,sRegistroaModificar,sRegistroModificado,dFecha,sUsuario,sTipoAccion) " &
                   "VALUES('" & vTabla & "','" & vRegistrosAntes & "','" & vRegistrosNuevos & "','" & FormatFecHora(vFecha, True, True) & "','" & vUsuario & "','" & TipAcc & "')", True)
    End Sub

    Public Function ChecaClavexSentencia(ByVal cCve As String, ByVal Constr As String, ByVal TipoCon As TipoConexion, _
ByVal dbtabla As String, ByVal Campo As String, ByVal TipoCampo As TipoDato, ByVal NomAplicacion As String, _
ByVal sErrorLlave As String, ByVal sValorLlave As String, ByVal StrSql As String, Optional ByVal FiltroEsp As String = "", _
Optional ByVal Filtro2 As String = "", Optional ByVal Orden As String = "", Optional ByVal SoloSQL As Boolean = False) As DataSet
        Dim Filtro As String = ""
        Dim obj As New CapaNegocio.Tablas
        Dim TipoConex As String
        ChecaClavexSentencia = Nothing
        Try

            If CType(TipoCampo, TipoDato) = TipoDato.TdCadena Or CType(TipoCampo, TipoDato) = TipoDato.TdFecha Then
                Filtro = Campo & " = '" & cCve & "'"
            ElseIf CType(TipoCampo, TipoDato) = TipoDato.TdNumerico Or CType(TipoCampo, TipoDato) = TipoDato.TdBoolean Then
                Filtro = Campo & " = " & cCve
            End If

            TipoConex = DeterminaNombreConexion(TipoCon)

            If SoloSQL Then
                StrSql = StrSql & Orden
            Else
                StrSql = StrSql & " Where " & Filtro & Filtro2 & Orden
            End If


            'DS = obj.CargaDsPorClave(Constr, TipoConex, dbtabla, Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, Filtro)

            DS = obj.CargaDsPorSentencia(Constr, TipoConex, StrSql, dbtabla, Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS)

            If Not DS Is Nothing Then
                'ChecaClave = False
                If DS.Tables(dbtabla).DefaultView.Count > 0 Then
                    If FiltroEsp <> "" Then
                        DS.Tables(dbtabla).DefaultView.RowFilter = FiltroEsp
                    End If
                    ChecaClavexSentencia = DS
                End If
            Else
                ChecaClavexSentencia = Nothing
            End If
            If Not obj Is Nothing Then obj = Nothing
        Catch Ex As Exception
            If Not obj Is Nothing Then obj = Nothing
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & Ex.Message, MsgBoxStyle.Information, dbtabla)
            ChecaClavexSentencia = Nothing
        End Try
    End Function

    'Public Sub ChecaValoresRegistro(ByVal AutentificacionWin As Boolean)
    '    Dim util As New CapaNegocio.Parametros
    '    'Dim objCrea As New CreaBaseDatos
    '    'SERVIDOR
    '    If GetSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Nombre del servidor", "") = "" Then
    '        MsgBox("No se ha declarado el servidor de SQL", MsgBoxStyle.Information)
    '        sNomServidor = InputBox("Deme el nombre del servidor SQL:", "Declarando SQL Server")
    '        SaveSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Nombre del servidor", sNomServidor)
    '    Else
    '        sNomServidor = GetSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Nombre del servidor", "") & ""
    '    End If

    '    'BASE DE DATOS
    '    If GetSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Nombre de la Base de Datos", "") = "" Then
    '        MsgBox("No se ha declarado la Base de Datos de SQL", MsgBoxStyle.Information)
    '        sNomBd = InputBox("Deme el nombre de la Base de Datos SQL:", "Declarando SQL Server")
    '        'Checar si Existe Base de Datos, si no Crearla

    '        SaveSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Nombre de la Base de Datos", sNomBd)
    '    Else
    '        sNomBd = GetSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Nombre de la Base de Datos", "") & ""
    '    End If

    '    ''15/ABRIL/2014
    '    ''COLAS DE IMPRESION EN REGISTRO 

    '    ''HKEY_CURRENT_USER\SOFTWARE\.....\CONFIGURACION\
    '    ''FACTURACION
    '    'If GetSetting(Process.GetCurrentProcess.ProcessName, "Configuracion", "ColaFacturaLocal", "") = "" Then
    '    '    MsgBox("No se ha declarado la Cola de Impresión para Facturación", MsgBoxStyle.Information)
    '    '    Registro_ColaFacturaLocal = InputBox("Escriba la cola de impresión para facturación:", "Declarando Cola de Impresión")
    '    '    SaveSetting(Process.GetCurrentProcess.ProcessName, "Configuracion", "ColaFacturaLocal", Registro_ColaFacturaLocal)
    '    'Else
    '    '    Registro_ColaFacturaLocal = GetSetting(Process.GetCurrentProcess.ProcessName, "Configuracion", "ColaFacturaLocal", "") & ""
    '    'End If
    '    ''REMISION
    '    'If GetSetting(Process.GetCurrentProcess.ProcessName, "Configuracion", "ColaRemisionLocal", "") = "" Then
    '    '    MsgBox("No se ha declarado la Cola de Impresión para Remisión", MsgBoxStyle.Information)
    '    '    Registro_ColaRemisionLocal = InputBox("Escriba la cola de impresión para Remisión:", "Declarando Cola de Impresión")
    '    '    SaveSetting(Process.GetCurrentProcess.ProcessName, "Configuracion", "ColaRemisionLocal", Registro_ColaRemisionLocal)
    '    'Else
    '    '    Registro_ColaRemisionLocal = GetSetting(Process.GetCurrentProcess.ProcessName, "Configuracion", "ColaRemisionLocal", "") & ""
    '    'End If
    '    ''PEDIDOS PENDIENTES
    '    'If GetSetting(Process.GetCurrentProcess.ProcessName, "Configuracion", "ColaPedidoLocal", "") = "" Then
    '    '    MsgBox("No se ha declarado la Cola de Impresión para Pedidos Pendientes", MsgBoxStyle.Information)
    '    '    Registro_ColaPedidoLocal = InputBox("Escriba la cola de impresión para Pedidos Pendientes:", "Declarando Cola de Impresión")
    '    '    SaveSetting(Process.GetCurrentProcess.ProcessName, "Configuracion", "ColaPedidoLocal", Registro_ColaPedidoLocal)
    '    'Else
    '    '    Registro_ColaPedidoLocal = GetSetting(Process.GetCurrentProcess.ProcessName, "Configuracion", "ColaPedidoLocal", "") & ""
    '    'End If


    '    'AUTENTIFICACION windows
    '    If GetSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Autentificacion Windows", "") = "" Then
    '        sAutentificacionWindows = AutentificacionWin
    '        SaveSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Autentificacion Windows", sAutentificacionWindows)
    '    Else
    '        If sAutentificacionWindows <> AutentificacionWin Then
    '            sAutentificacionWindows = AutentificacionWin
    '            SaveSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Autentificacion Windows", sAutentificacionWindows)
    '        Else
    '            sAutentificacionWindows = GetSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Autentificacion Windows", False)
    '        End If
    '    End If



    '    If Not AutentificacionWin Then
    '        'USUARIO SQL

    '        If GetSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Nombre del Usuario SQL", "") = "" Then
    '            MsgBox("No se ha declarado el Usuario de SQL", MsgBoxStyle.Information)
    '            sNomUsuarioSQL = InputBox("Deme el nombre del usuario SQL:", "Declarando SQL Server SQL")
    '            SaveSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Nombre del Usuario SQL", sNomUsuarioSQL)
    '        Else
    '            sNomUsuarioSQL = GetSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Nombre del Usuario SQL", "") & ""
    '        End If
    '        'PASSWORD SQL
    '        If GetSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Password del Usuario SQL", "") = "" Then
    '            MsgBox("No se ha declarado el Password del Usuario de SQL", MsgBoxStyle.Information)
    '            sPassUsuarioSQL = InputBox("Deme el Password del usuario SQL(" & sNomUsuarioSQL & ") :", "Declarando SQL Server")
    '            'SaveSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Password del Usuario SQL", sPassUsuarioSQL)
    '            'Encriptado
    '            SaveSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Password del Usuario SQL", Criptografia.Encrypt(Trim(sPassUsuarioSQL), KEY_CONFIG_ENCRYPT))

    '        Else
    '            sPassUsuarioSQL = GetSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Password del Usuario SQL", "") & ""
    '            sPassUsuarioSQL = Criptografia.Decrypt(Trim(sPassUsuarioSQL), KEY_CONFIG_ENCRYPT)
    '            'Parametro_PasswordSQL = Criptografia.Decrypt(Trim(DS.Tables("PARAMETROS").DefaultView.Item(0).Item("PasswordSQL")), KEY_CONFIG_ENCRYPT)
    '            'sPassUsuarioSQL = generarClaveSHA1(Trim(sPassUsuarioSQL))
    '            'generarClaveSHA1(Trim(txtsPasword.Text)) = PassWord
    '        End If
    '        'SE CREA LA CADENA DE CONEXION PRINCIPAL
    '        Inicio.CONSTR = util.CreaConStrSQL(UCase(sNomServidor), sNomBd, False, sNomUsuarioSQL, sPassUsuarioSQL, True)
    '    Else
    '        Inicio.CONSTR = util.CreaConStrSQL(UCase(sNomServidor), sNomBd, True, "", "", True)

    '        Inicio.CONSTR_COM = util.CreaConStrSQL(UCase(sNomServidor & "\compac"), "adPegaso", True, "", "", True)
    '    End If
    '    'MsgBox(Inicio.CONSTR)
    'End Sub

    Public Sub ChecaValoresRegistro(ByVal AutentificacionWin As Boolean, ByVal AutentificacionWinCOM As Boolean, ByVal vBdCOM As String)
        Dim util As New CapaNegocio.Parametros
        'Dim objCrea As New CreaBaseDatos
        'SERVIDORES
        'APLICACION
        If GetSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Nombre del servidor", "") = "" Then
            MsgBox("No se ha declarado el servidor de SQL", MsgBoxStyle.Information)
            sNomServidor = InputBox("Deme el nombre del servidor SQL:", "Declarando SQL Server")
            SaveSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Nombre del servidor", sNomServidor)
        Else
            sNomServidor = GetSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Nombre del servidor", "") & ""
        End If
        'COMERCIAL
        If GetSetting(Process.GetCurrentProcess.ProcessName, "ServerSQLCOM", "Nombre del servidor", "") = "" Then
            MsgBox("No se ha declarado el servidor de SQL Comercial", MsgBoxStyle.Information)
            sNomServidorCOM = InputBox("Deme el nombre del servidor SQL Comercial:", "Declarando SQL Server")
            SaveSetting(Process.GetCurrentProcess.ProcessName, "ServerSQLCOM", "Nombre del servidor", sNomServidorCOM)
        Else
            sNomServidorCOM = GetSetting(Process.GetCurrentProcess.ProcessName, "ServerSQLCOM", "Nombre del servidor", "") & ""
        End If


        'BASE DE DATOS
        'APLICACION
        If GetSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Nombre de la Base de Datos", "") = "" Then
            MsgBox("No se ha declarado la Base de Datos de SQL", MsgBoxStyle.Information)
            sNomBd = InputBox("Deme el nombre de la Base de Datos SQL:", "Declarando SQL Server")
            'Checar si Existe Base de Datos, si no Crearla

            SaveSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Nombre de la Base de Datos", sNomBd)
        Else
            sNomBd = GetSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Nombre de la Base de Datos", "") & ""
        End If
        'COMERCIAL
        If vBdCOM <> "" Then
            sNomBdCOM = vBdCOM
        Else
            If GetSetting(Process.GetCurrentProcess.ProcessName, "ServerSQLCOM", "Nombre de la Base de Datos", "") = "" Then
                MsgBox("No se ha declarado la Base de Datos de SQL COMERCIAL", MsgBoxStyle.Information)
                sNomBdCOM = InputBox("Deme el nombre de la Base de Datos SQL COMERCIAL:", "Declarando SQL Server")
                'Checar si Existe Base de Datos, si no Crearla

                SaveSetting(Process.GetCurrentProcess.ProcessName, "ServerSQLCOM", "Nombre de la Base de Datos", sNomBdCOM)
            Else
                sNomBdCOM = GetSetting(Process.GetCurrentProcess.ProcessName, "ServerSQLCOM", "Nombre de la Base de Datos", "") & ""
            End If
        End If

        If GetSetting(Process.GetCurrentProcess.ProcessName, "ServerSQLCOM", "Ruta de la Base de Datos", "") = "" Then
            If sRutaBdCOM <> "" Then
                SaveSetting(Process.GetCurrentProcess.ProcessName, "ServerSQLCOM", "Ruta de la Base de Datos", sRutaBdCOM)
            Else
                sRutaBdCOM = GetSetting(Process.GetCurrentProcess.ProcessName, "ServerSQLCOM", "Ruta de la Base de Datos", "") & ""
            End If

        Else
            If sRutaBdCOM <> "" And (sRutaBdCOM <> GetSetting(Process.GetCurrentProcess.ProcessName, "ServerSQLCOM", "Ruta de la Base de Datos", "") & "") Then
                SaveSetting(Process.GetCurrentProcess.ProcessName, "ServerSQLCOM", "Ruta de la Base de Datos", sRutaBdCOM)
            Else
                sRutaBdCOM = GetSetting(Process.GetCurrentProcess.ProcessName, "ServerSQLCOM", "Ruta de la Base de Datos", "") & ""
            End If

            'If sRutaBdCOM <> "" Then
            '    SaveSetting(Process.GetCurrentProcess.ProcessName, "ServerSQLCOM", "Ruta de la Base de Datos", sRutaBdCOM)
            'End If
        End If

        'If sRutaBdCOM <> "" Then
        '    SaveSetting(Process.GetCurrentProcess.ProcessName, "ServerSQLCOM", "Ruta de la Base de Datos", sRutaBdCOM)
        'Else
        '    If GetSetting(Process.GetCurrentProcess.ProcessName, "ServerSQLCOM", "Ruta de la Base de Datos", "") = "" Then
        '        sRutaBdCOM = GetSetting(Process.GetCurrentProcess.ProcessName, "ServerSQLCOM", "Ruta de la Base de Datos", "") & ""
        '    End If
        'End If



        ''15/ABRIL/2014
        ''COLAS DE IMPRESION EN REGISTRO 

        ''HKEY_CURRENT_USER\SOFTWARE\.....\CONFIGURACION\
        ''FACTURACION
        'If GetSetting(Process.GetCurrentProcess.ProcessName, "Configuracion", "ColaFacturaLocal", "") = "" Then
        '    MsgBox("No se ha declarado la Cola de Impresión para Facturación", MsgBoxStyle.Information)
        '    Registro_ColaFacturaLocal = InputBox("Escriba la cola de impresión para facturación:", "Declarando Cola de Impresión")
        '    SaveSetting(Process.GetCurrentProcess.ProcessName, "Configuracion", "ColaFacturaLocal", Registro_ColaFacturaLocal)
        'Else
        '    Registro_ColaFacturaLocal = GetSetting(Process.GetCurrentProcess.ProcessName, "Configuracion", "ColaFacturaLocal", "") & ""
        'End If
        ''REMISION
        'If GetSetting(Process.GetCurrentProcess.ProcessName, "Configuracion", "ColaRemisionLocal", "") = "" Then
        '    MsgBox("No se ha declarado la Cola de Impresión para Remisión", MsgBoxStyle.Information)
        '    Registro_ColaRemisionLocal = InputBox("Escriba la cola de impresión para Remisión:", "Declarando Cola de Impresión")
        '    SaveSetting(Process.GetCurrentProcess.ProcessName, "Configuracion", "ColaRemisionLocal", Registro_ColaRemisionLocal)
        'Else
        '    Registro_ColaRemisionLocal = GetSetting(Process.GetCurrentProcess.ProcessName, "Configuracion", "ColaRemisionLocal", "") & ""
        'End If
        ''PEDIDOS PENDIENTES
        'If GetSetting(Process.GetCurrentProcess.ProcessName, "Configuracion", "ColaPedidoLocal", "") = "" Then
        '    MsgBox("No se ha declarado la Cola de Impresión para Pedidos Pendientes", MsgBoxStyle.Information)
        '    Registro_ColaPedidoLocal = InputBox("Escriba la cola de impresión para Pedidos Pendientes:", "Declarando Cola de Impresión")
        '    SaveSetting(Process.GetCurrentProcess.ProcessName, "Configuracion", "ColaPedidoLocal", Registro_ColaPedidoLocal)
        'Else
        '    Registro_ColaPedidoLocal = GetSetting(Process.GetCurrentProcess.ProcessName, "Configuracion", "ColaPedidoLocal", "") & ""
        'End If


        'AUTENTIFICACION windows
        'APLICACION
        If GetSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Autentificacion Windows", "") = "" Then
            sAutentificacionWindows = AutentificacionWin
            SaveSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Autentificacion Windows", sAutentificacionWindows)
        Else
            If sAutentificacionWindows <> AutentificacionWin Then
                sAutentificacionWindows = AutentificacionWin
                SaveSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Autentificacion Windows", sAutentificacionWindows)
            Else
                sAutentificacionWindows = GetSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Autentificacion Windows", False)
            End If
        End If
        'COMERCIAL
        If GetSetting(Process.GetCurrentProcess.ProcessName, "ServerSQLCOM", "Autentificacion Windows", "") = "" Then
            sAutentificacionWindowsCOM = AutentificacionWinCOM
            SaveSetting(Process.GetCurrentProcess.ProcessName, "ServerSQLCOM", "Autentificacion Windows", sAutentificacionWindowsCOM)
        Else
            If sAutentificacionWindowsCOM <> AutentificacionWinCOM Then
                sAutentificacionWindowsCOM = AutentificacionWinCOM
                SaveSetting(Process.GetCurrentProcess.ProcessName, "ServerSQLCOM", "Autentificacion Windows", sAutentificacionWindowsCOM)
            Else
                sAutentificacionWindowsCOM = GetSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Autentificacion Windows", False)
            End If
        End If


        'APLICACION
        If Not AutentificacionWin Then
            'USUARIO SQL

            If GetSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Nombre del Usuario SQL", "") = "" Then
                MsgBox("No se ha declarado el Usuario de SQL", MsgBoxStyle.Information)
                sNomUsuarioSQL = InputBox("Deme el nombre del usuario SQL:", "Declarando SQL Server SQL")
                SaveSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Nombre del Usuario SQL", sNomUsuarioSQL)
            Else
                sNomUsuarioSQL = GetSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Nombre del Usuario SQL", "") & ""
            End If
            'PASSWORD SQL
            If GetSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Password del Usuario SQL", "") = "" Then
                MsgBox("No se ha declarado el Password del Usuario de SQL", MsgBoxStyle.Information)
                sPassUsuarioSQL = InputBox("Deme el Password del usuario SQL(" & sNomUsuarioSQL & ") :", "Declarando SQL Server")
                'SaveSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Password del Usuario SQL", sPassUsuarioSQL)
                'Encriptado
                SaveSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Password del Usuario SQL", Criptografia.Encrypt(Trim(sPassUsuarioSQL), KEY_CONFIG_ENCRYPT))

            Else
                sPassUsuarioSQL = GetSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Password del Usuario SQL", "") & ""
                sPassUsuarioSQL = Criptografia.Decrypt(Trim(sPassUsuarioSQL), KEY_CONFIG_ENCRYPT)
                'Parametro_PasswordSQL = Criptografia.Decrypt(Trim(DS.Tables("PARAMETROS").DefaultView.Item(0).Item("PasswordSQL")), KEY_CONFIG_ENCRYPT)
                'sPassUsuarioSQL = generarClaveSHA1(Trim(sPassUsuarioSQL))
                'generarClaveSHA1(Trim(txtsPasword.Text)) = PassWord
            End If
            'SE CREA LA CADENA DE CONEXION PRINCIPAL
            Inicio.CONSTR = util.CreaConStrSQL(UCase(sNomServidor), sNomBd, False, sNomUsuarioSQL, sPassUsuarioSQL, True)
        Else
            Inicio.CONSTR = util.CreaConStrSQL(UCase(sNomServidor), sNomBd, True, "", "", True)

        End If

        'COMERCIAL
        If Not AutentificacionWinCOM Then
            'USUARIO SQL
            If GetSetting(Process.GetCurrentProcess.ProcessName, "ServerSQLCOM", "Nombre del Usuario SQL", "") = "" Then
                MsgBox("No se ha declarado el Usuario de SQL COMERCIAL", MsgBoxStyle.Information)
                sNomUsuarioSQLCOM = InputBox("Deme el nombre del usuario SQL COMERCIAL:", "Declarando SQL Server SQL")
                SaveSetting(Process.GetCurrentProcess.ProcessName, "ServerSQLCOM", "Nombre del Usuario SQL", sNomUsuarioSQLCOM)
            Else
                sNomUsuarioSQLCOM = GetSetting(Process.GetCurrentProcess.ProcessName, "ServerSQLCOM", "Nombre del Usuario SQL", "") & ""
            End If
            'PASSWORD SQL
            If GetSetting(Process.GetCurrentProcess.ProcessName, "ServerSQLCOM", "Password del Usuario SQL", "") = "" Then
                MsgBox("No se ha declarado el Password del Usuario de SQL COMERCIAL", MsgBoxStyle.Information)
                sPassUsuarioSQLCOM = InputBox("Deme el Password del usuario SQL(" & sNomUsuarioSQLCOM & ") :", "Declarando SQL Server")
                'SaveSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Password del Usuario SQL", sPassUsuarioSQL)
                'Encriptado
                SaveSetting(Process.GetCurrentProcess.ProcessName, "ServerSQLCOM", "Password del Usuario SQL", Criptografia.Encrypt(Trim(sPassUsuarioSQLCOM), KEY_CONFIG_ENCRYPT))

            Else
                sPassUsuarioSQLCOM = GetSetting(Process.GetCurrentProcess.ProcessName, "ServerSQLCOM", "Password del Usuario SQL", "") & ""
                sPassUsuarioSQLCOM = Criptografia.Decrypt(Trim(sPassUsuarioSQLCOM), KEY_CONFIG_ENCRYPT)
                'Parametro_PasswordSQL = Criptografia.Decrypt(Trim(DS.Tables("PARAMETROS").DefaultView.Item(0).Item("PasswordSQL")), KEY_CONFIG_ENCRYPT)
                'sPassUsuarioSQL = generarClaveSHA1(Trim(sPassUsuarioSQL))
                'generarClaveSHA1(Trim(txtsPasword.Text)) = PassWord
            End If
            'SE CREA LA CADENA DE CONEXION PRINCIPAL
            Inicio.CONSTR_COM = util.CreaConStrSQL(UCase(sNomServidorCOM), sNomBdCOM, False, sNomUsuarioSQLCOM, sPassUsuarioSQLCOM, True)
            Inicio.CnStrEsp = util.CreaConStrSQL(UCase(sNomServidorCOM), "CompacWAdmin", False, sNomUsuarioSQLCOM, sPassUsuarioSQLCOM, True)
        Else
            'Inicio.CONSTR = util.CreaConStrSQL(UCase(sNomServidor), sNomBd, True, "", "", True)

            'Inicio.CONSTR_COM = util.CreaConStrSQL(UCase(sNomServidor & "\compac"), "adPegaso", True, "", "", True)
            Inicio.CONSTR_COM = util.CreaConStrSQL(UCase(sNomServidorCOM), sNomBdCOM, True, "", "", True)
            Inicio.CnStrEsp = util.CreaConStrSQL(UCase(sNomServidorCOM), "CompacWAdmin", True, "", "", True)
        End If


        'MsgBox(Inicio.CONSTR)
    End Sub

    'Private Function ChecaValorRutaSDK() As String
    '    Dim ObjReg As New ToolRegistro
    '    Dim Resp As String
    '    ChecaValorRutaSDK = ""
    '    If ObjReg.LeeKey(KeyPathAdminPAQ) Then
    '        'Public KeyPathAdminPAQ As String = "HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Computación en Acción, SA CV\AdminPAQ"
    '        'Resp = ObjReg.LeeLlaveRegistro(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Autentificacion Windows", KeyPathLM)
    '        Resp = ObjReg.LeeLlaveRegistro("", "AdminPAQ", "DIRECTORIOBASE", KeyPathAdminPAQ)
    '        If Resp <> "" Then
    '            Return Resp
    '        Else
    '            Return ""
    '        End If
    '    End If
    'End Function

    Public Function CreaCadenaFox(ByVal Directorio As String) As String
        CreaCadenaFox = ""
        If Directorio <> "" Then
            'Return "Provider = VFPOLEDB;Data Source=" & Directorio & "\"
            Return "Provider = VFPOLEDB;Data Source=" & Directorio & "\;Collating Sequence=general;"
            'Collating Sequence=general;
        Else
            Return "CADENA INVALIDA"
        End If
    End Function

    'Public Function FormatFecHora(ByVal vFecha As DateTime, ByVal FechaUnificada As Boolean,
    '        Optional ByVal IncluyeHora As Boolean = True, Optional ByVal FechaFox As Boolean = False) As String
    Public Function FormatFecHora(ByVal vFecha As DateTime, ByVal FechaUnificada As Boolean,
         ByVal IncluyeHora As Boolean, Optional ByVal FechaFox As Boolean = False) As String

        'Dim strFecha As String = ""
        If FechaFox Then
            If IncluyeHora Then
                Return vFecha.ToString("MM/dd/yyyy HH:mm:ss")
            Else
                'ANTES ojo
                Return vFecha.ToString("MM/dd/yyyy")
                ''Return vFecha.ToString("dd/MM/yyyy")
                'Return vFecha.ToString("yyyy/MM/dd")
            End If
        Else

            If IncluyeHora Then
                'strFecha = vFecha.ToString("dd/MM/yyyy HH:mm:ss")

                If FechaUnificada Then
                    Return vFecha.ToString("yyyyMMdd HH:mm:ss")
                Else
                    Return vFecha.ToString("dd/MM/yyyy HH:mm:ss")
                End If

                'Return
            Else
                If FechaUnificada Then
                    Return vFecha.ToString("yyyyMMdd")
                Else
                    Return vFecha.ToString("dd/MM/yyyy")
                End If
                'Return vFecha.ToString("dd/MM/yyyy")
            End If
        End If

    End Function

    Public Sub Status(ByVal vEstado As String, ByVal Formulario As Object, Optional ByVal ValueBar As Integer = -1, Optional ByVal MaxBar As Integer = 0, Optional ByVal Err As Boolean = False)
        'On Error Resume Next
        If Formulario Is Nothing Then Exit Sub
        If Not TypeOf (Formulario) Is Form Then Exit Sub
        If ValueBar < 0 Then ' kiere decir que no habra barra de estado ya que esta los valores en default. . . .' hace que no sean visibles los controles
            Formulario.MeStatus1.VisibleBarra = False
        Else
            Formulario.MeStatus1.VisibleBarra = True
            Formulario.MeStatus1.TBar.Style = ProgressBarStyle.Continuous
            If MaxBar > 0 And MaxBar <> Formulario.MeStatus1.TBar.Maximum Then 'Esto hara que se inicialize solo una vez. . .
                Formulario.MeStatus1.TBar.Maximum = MaxBar
            End If
            Formulario.MeStatus1.ProgresoBarra(ValueBar)
        End If
        Formulario.MeStatus1.ErrorLabel = Err
        Formulario.MeStatus1.TLStatus.Text = vEstado
    End Sub

    Public Function ValidandoCampo(ByVal Valor As Object, ByVal TipoCampo As TipoDato) As Boolean
        ValidandoCampo = False
        If CType(TipoCampo, TipoDato) = TipoDato.TdCadena Then
            If Valor <> "" Then
                ValidandoCampo = True
            End If
        ElseIf CType(TipoCampo, TipoDato) = TipoDato.TdFecha Then

        ElseIf CType(TipoCampo, TipoDato) = TipoDato.TdNumerico Then
            If Val(Valor) > 0 Then
                ValidandoCampo = True
            End If
        ElseIf CType(TipoCampo, TipoDato) = TipoDato.TdBoolean Then
        End If
    End Function

    'Public Sub ActualizaParametros(ByVal Usuario As String, ByVal Password As String, ByVal Empresa As String, ByVal Direccion As String,
    'ByVal RFC As String, ByVal Telefono As String, ByVal Email As String, ByVal PassEmail As String)

    '    Parametro_UsuarioSQL = Usuario
    '    Parametro_PasswordSQL = Password
    '    Parametro_Empresa = Empresa
    '    Parametro_Direccion = Direccion
    '    Parametro_RFC = RFC
    '    Parametro_Telefono = Telefono
    '    'Parametro_EmailEmpresa = Email
    '    'Parametro_PassEmailEmpresa = PassEmail

    '    'Parametro_ImprimeReg = ImpReg
    '    'Parametro_GrupoDefault = GrupoDefault
    'End Sub
    Public Sub ActualizaParametros(ByVal Usuario As String, ByVal Password As String, ByVal Empresa As String, ByVal Direccion As String,
    ByVal RFC As String, ByVal Telefono As String, ByVal Email As String, ByVal PassEmail As String, _
    ByVal ImpReg As Boolean, ByVal GrupoDefault As String, ByVal IdEmpresa As Integer, ByVal RutaEmpresaAdmPAQ As String, _
    ByVal NombreEmpresaAdmPAQ As String, ByVal CPLAMIGCFD As String, ByVal CCODIGOA01_SALdefault As Integer)

        Parametro_UsuarioSQL = Usuario
        Parametro_PasswordSQL = Password
        Parametro_Empresa = Empresa
        Parametro_Direccion = Direccion
        Parametro_RFC = RFC
        Parametro_Telefono = Telefono

        '12/FEB/2016
        Parametro_CCODIGOA01_SALdefault = CCODIGOA01_SALdefault
        'Parametro_EmailEmpresa = Email
        'Parametro_PassEmailEmpresa = PassEmail
        'Parametro_ImprimeReg = ImpReg
        'Parametro_GrupoDefault = GrupoDefault

        sIdEmpresa = IdEmpresa
        sRutaEmpresaAdmPAQ = RutaEmpresaAdmPAQ
        sNombreEmpresaAdmPAQ = NombreEmpresaAdmPAQ

        'Parametro_CPLAMIGCFD = CPLAMIGCFD

    End Sub

    Public Sub DespliegaReporte(ByVal Tabla As String, ByVal ConStr As String)
        Dim strSql As String = ""
        Dim AnchoNumerico As Integer = 0
        Dim frm As New frmReporteador
        frm.NomTabla = Tabla


        Select Case UCase(Tabla)
            Case UCase("CatTipoOsComb")
                frm.BanHorizontal = True
                frm.BanCuadricula = False
                frm.BanBorderDetalle = False

                frm.TituloReporte = "Reporte de Catalogo de Combustibles Extra"

                frm.ConfigRep({New ColClass("idTipoOSComb", "Clave", TipoDato.TdCadena, False, False, TipoOrdenamiento.Asc, TipoControlReport.TdCadena, True, 45),
                New ColClass("NomTipoOSComb", "Nombre", TipoDato.TdCadena, True, True, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 120,,, ,, ColClass.EnTipoFilCadena.Opcional),
                New ColClass("ReqViaje", "Requiere Viaje", TipoDato.TdCadena, True, False, TipoOrdenamiento.Asc, TipoControlReport.TdCadena, True, 45),
                New ColClass("TipoUnidad", "Tipo Unidad", TipoDato.TdCadena, True, True, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 100,,, ,, ColClass.EnTipoFilCadena.Opcional)})


            Case UCase("CatActividades")
                frm.BanHorizontal = True
                frm.BanCuadricula = False
                frm.BanBorderDetalle = False

                frm.TituloReporte = "Reporte de Catalogo de Actividades"

                frm.ConfigRep({New ColClass("idActividad", "Act.", TipoDato.TdCadena, True, False, TipoOrdenamiento.Asc, TipoControlReport.TdCadena, True, 20,,,,,,,,,, TipoDato.TdNumerico),
                New ColClass("NombreAct", "Nombre", TipoDato.TdCadena, True, False, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 240,,,,,,,,,, TipoDato.TdCadena),
                New ColClass("CostoManoObra", "$ M.O.", TipoDato.TdCadena, False, False, TipoOrdenamiento.Asc, TipoControlReport.TdCadena, True, 35),
                New ColClass("DuracionHoras", "Dur.Hrs.", TipoDato.TdCadena, False, False, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 35),
                New ColClass("NomFamilia", "Familia", TipoDato.TdCadena, False, False, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 100),
                New ColClass("NomDivision", "Division", TipoDato.TdCadena, False, False, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, False, 100)})

            Case UCase("Catciudad")
                frm.BanHorizontal = False
                frm.BanCuadricula = False
                frm.BanBorderDetalle = False
                frm.TituloReporte = "Reporte de Catalogo de Ciudades"
                frm.ConfigRep({New ColClass("idCiudad", "Ciudad", TipoDato.TdCadena, False, False, TipoOrdenamiento.Asc, TipoControlReport.TdCadena, True, 45),
                New ColClass("NomCiudad", "Nombre Ciudad", TipoDato.TdCadena, True, True, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 120,,, "select DISTINCT ciu.idCiudad , ciu.NomCiudad from Catciudad ciu",, ColClass.EnTipoFilCadena.Opcional),
                New ColClass("idEstado", "Estado", TipoDato.TdCadena, False, False, TipoOrdenamiento.Asc, TipoControlReport.TdCadena, True, 45),
                New ColClass("NomEstado", "Nombre Estado", TipoDato.TdCadena, True, True, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 100,,, "select DISTINCT ciu.idEstado , est.NomEstado FROM Catciudad ciu INNER join catestados Est on ciu.idEstado = est.idEstado",, ColClass.EnTipoFilCadena.Opcional)})

            Case UCase("CatColonia")
                frm.BanHorizontal = True
                frm.BanCuadricula = False
                frm.BanBorderDetalle = False
                frm.TituloReporte = "Reporte de Catalogo de Colonias"
                frm.ConfigRep({New ColClass("idColonia", "Colonia", TipoDato.TdCadena, False, False, TipoOrdenamiento.Asc, TipoControlReport.TdCadena, True, 30),
                New ColClass("NomColonia", "   Nombre Colonia", TipoDato.TdCadena, False, True, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 170),
                New ColClass("CP", "C.P.", TipoDato.TdCadena, False, True, TipoOrdenamiento.Asc, TipoControlReport.TdCadena, True, 30),
                New ColClass("NomCiudad", "Ciudad", TipoDato.TdCadena, True, True, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 60,,, "select DISTINCT col.idCiudad, ciu.NomCiudad FROM catcolonia col left join CatCiudad ciu on col.idCiudad = ciu.idCiudad "),
                New ColClass("NomEstado", "Estado", TipoDato.TdCadena, True, True, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 60,,, "select DISTINCT ciu.idEstado, est.NomEstado FROM catcolonia col left join CatCiudad ciu on col.idCiudad = ciu.idCiudad  LEFT join CatEstados est on ciu.idEstado = est.idEstado "),
                New ColClass("NomPais", "País", TipoDato.TdCadena, False, False, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 50)})

            Case UCase("CatDeptos")
                frm.BanHorizontal = True
                frm.BanCuadricula = False
                frm.BanBorderDetalle = False
                frm.TituloReporte = "Reporte de Catalogo de Departamentos"
                frm.ConfigRep({New ColClass("IdDepto", "Departamento", TipoDato.TdCadena, False, True, TipoOrdenamiento.Asc, TipoControlReport.TdCadena, True, 100),
                New ColClass("NomDepto", "Nombre Departamento", TipoDato.TdCadena, False, True, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 200)})



            Case UCase("CatMarcas")
                frm.BanHorizontal = False
                frm.BanCuadricula = False
                frm.BanBorderDetalle = False
                frm.TituloReporte = "Reporte de Catalogo de Marcas"

                frm.ConfigRep({New ColClass("idMarca", "Id Marca", TipoDato.TdCadena, False, True, TipoOrdenamiento.Asc, TipoControlReport.TdCadena, True, 60),
               New ColClass("NombreMarca", "Nombre Marca", TipoDato.TdCadena, False, True, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 100)})


            Case UCase("CatDivision")
                frm.BanHorizontal = False
                frm.BanCuadricula = False
                frm.BanBorderDetalle = False
                frm.TituloReporte = "Reporte de Catalogo de Divisiones"

                frm.ConfigRep({New ColClass("idDivision", "Id División", TipoDato.TdCadena, False, True, TipoOrdenamiento.Asc, TipoControlReport.TdCadena, True, 60),
                New ColClass("NomDivision", "Nombre División", TipoDato.TdCadena, False, True, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 100)})

            Case UCase("CatPais")
                frm.BanHorizontal = False
                frm.BanCuadricula = False
                frm.BanBorderDetalle = False
                frm.TituloReporte = "Reporte de Catalogo de Paises"

                frm.ConfigRep({New ColClass("idPais", "Id País", TipoDato.TdCadena, False, True, TipoOrdenamiento.Asc, TipoControlReport.TdCadena, True, 60),
                New ColClass("NomPais", "Nombre País", TipoDato.TdCadena, False, True, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 100)})

            Case UCase("CatPuestos")
                frm.BanHorizontal = False
                frm.BanCuadricula = False
                frm.BanBorderDetalle = False
                frm.TituloReporte = "Reporte de Catalogo de Paises"

                frm.ConfigRep({New ColClass("idPuesto", "Id Puesto", TipoDato.TdCadena, False, True, TipoOrdenamiento.Asc, TipoControlReport.TdCadena, True, 60),
                New ColClass("NomPuesto", "Nombre Puesto", TipoDato.TdCadena, False, True, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 100)})
            Case UCase("CatTipoHerramienta")
                frm.BanHorizontal = False
                frm.BanCuadricula = False
                frm.BanBorderDetalle = False
                frm.TituloReporte = "Reporte de Catalogo de Tipo de Herramientas"

                frm.ConfigRep({New ColClass("idTipoHerramienta", "Id Tipo", TipoDato.TdCadena, False, True, TipoOrdenamiento.Asc, TipoControlReport.TdCadena, True, 60),
                New ColClass("NomTipoHerramienta", "Descripción", TipoDato.TdCadena, False, True, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 100)})
                'CatTipoOrden
            Case UCase("CatTipoOrden")
                frm.BanHorizontal = False
                frm.BanCuadricula = False
                frm.BanBorderDetalle = False
                frm.TituloReporte = "Reporte de Catalogo de Tipo de Orden"

                frm.ConfigRep({New ColClass("idTipoOrden", "Id Tipo", TipoDato.TdCadena, False, True, TipoOrdenamiento.Asc, TipoControlReport.TdCadena, True, 60),
                New ColClass("NomTipoOrden", "Nombre", TipoDato.TdCadena, False, True, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 100)})
            Case UCase("CatTipoServicio")
                frm.BanHorizontal = False
                frm.BanCuadricula = False
                frm.BanBorderDetalle = False
                frm.TituloReporte = "Reporte de Catalogo de Tipo de Servicios"

                frm.ConfigRep({New ColClass("idTipoServicio", "Id Tipo", TipoDato.TdCadena, False, True, TipoOrdenamiento.Asc, TipoControlReport.TdCadena, True, 60),
                New ColClass("NomTipoServicio", "Nombre", TipoDato.TdCadena, False, True, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 100)})
            Case UCase("CatFamilia")
                frm.BanHorizontal = False
                frm.BanCuadricula = False
                frm.BanBorderDetalle = False
                frm.TituloReporte = "Reporte de Catalogo de Familias"

                frm.ConfigRep({New ColClass("IDFAMILIA", "Familia", TipoDato.TdCadena, False, False, TipoOrdenamiento.Asc, TipoControlReport.TdCadena, True, 45),
                New ColClass("NOMFAMILIA", "Nombre", TipoDato.TdCadena, False, True, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 120),
                New ColClass("IDDIVISION", "División", TipoDato.TdCadena, False, False, TipoOrdenamiento.Asc, TipoControlReport.TdCadena, True, 45),
                New ColClass("NOMDIVISION", "Nombre", TipoDato.TdCadena, True, True, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 100,,, "SELECT DISTINCT FAM.IDDIVISION, DIV.NOMDIVISION FROM CATFAMILIA FAM INNER JOIN CATDIVISION DIV ON FAM.IDDIVISION = DIV.IDDIVISION")})


            Case UCase("catEstados")
                frm.BanHorizontal = False
                frm.BanCuadricula = False
                frm.BanBorderDetalle = False
                frm.TituloReporte = "Reporte de Catalogo de Estados"
                frm.ConfigRep({New ColClass("idEstado", "Estado", TipoDato.TdCadena, False, False, TipoOrdenamiento.Asc, TipoControlReport.TdCadena, True, 45),
                New ColClass("NomEstado", "Nombre Estado", TipoDato.TdCadena, False, True, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 120),
                New ColClass("idPais", "Pais", TipoDato.TdCadena, False, False, TipoOrdenamiento.Asc, TipoControlReport.TdCadena, True, 45),
                New ColClass("NomPais", "Nombre Pais", TipoDato.TdCadena, False, True, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 100)})

            Case UCase("CatSucursal")
                frm.BanHorizontal = False
                frm.BanCuadricula = False
                frm.BanBorderDetalle = False
                frm.TituloReporte = "Reporte de Catalogo de Sucursales"
                frm.ConfigRep({New ColClass("id_sucursal", "Sucursal", TipoDato.TdCadena, False, False, TipoOrdenamiento.Asc, TipoControlReport.TdCadena, True, 45),
                New ColClass("NomSucursal", "Nombre Sucursal", TipoDato.TdCadena, False, True, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 120),
                New ColClass("id_empresa", "Empresa", TipoDato.TdCadena, False, False, TipoOrdenamiento.Asc, TipoControlReport.TdCadena, True, 45),
                New ColClass("RazonSocial", "Nombre Empresa", TipoDato.TdCadena, True, True, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 100,,, "select DISTINCT suc.id_empresa, emp.RazonSocial from CatSucursal Suc inner join catempresas emp ON emp.idEmpresa = Suc.id_empresa")})

            Case UCase("CatAlmacen")
                frm.BanHorizontal = False
                frm.BanCuadricula = False
                frm.BanBorderDetalle = False
                frm.TituloReporte = "Reporte de Catalogo de Almacenes"
                frm.ConfigRep({New ColClass("idAlmacen", "Almacen", TipoDato.TdCadena, True, False, TipoOrdenamiento.Asc, TipoControlReport.TdCadena, True, 45,,,,,,,,,, TipoDato.TdNumerico),
                New ColClass("NomAlmacen", "Nombre Almacen", TipoDato.TdCadena, True, True, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 120,,,,,,,,,, TipoDato.TdCadena),
                New ColClass("idSucursal", "Sucursal", TipoDato.TdCadena, False, False, TipoOrdenamiento.Asc, TipoControlReport.TdCadena, True, 45),
                New ColClass("NomSucursal", "Nombre Sucursal", TipoDato.TdCadena, False, True, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 100)})


            Case UCase("CatTipoUniTrans")
                frm.BanHorizontal = False
                frm.BanCuadricula = False
                frm.BanBorderDetalle = False

                frm.TituloReporte = "Reporte de Catalogo de Tipo de Unidades de Transporte"
                frm.ConfigRep({New ColClass("idTipoUniTras", "Tipo", TipoDato.TdCadena, False, False, TipoOrdenamiento.Asc, TipoControlReport.TdCadena, True, 20),
                New ColClass("nomTipoUniTras", "Nombre", TipoDato.TdCadena, False, True, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 150),
                New ColClass("NoLlantas", "Llantas", TipoDato.TdCadena, False, True, TipoOrdenamiento.Asc, TipoControlReport.TdCadena, True, 35),
                New ColClass("NoEjes", "Ejes", TipoDato.TdCadena, False, True, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 35),
                New ColClass("TonelajeMax", "Tonelaje Máx.", TipoDato.TdCadena, False, True, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 100)})
            Case UCase("CatProveedores")
                frm.BanHorizontal = False
                frm.BanCuadricula = False
                frm.BanBorderDetalle = False

                frm.TituloReporte = "Reporte de Catalogo de Proveedores"
                frm.ConfigRep({New ColClass("idProveedor", "Proveedor", TipoDato.TdCadena, False, False, TipoOrdenamiento.Asc, TipoControlReport.TdCadena, True, 40),
                New ColClass("RazonSocial", "Razon Social", TipoDato.TdCadena, False, True, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 150),
                New ColClass("RFCProv", "RFC", TipoDato.TdCadena, False, True, TipoOrdenamiento.Asc, TipoControlReport.TdCadena, True, 45),
                New ColClass("EmailProv", "Email", TipoDato.TdCadena, False, True, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 35)})

            Case UCase("CatHerramientas")
                frm.BanHorizontal = True
                frm.BanCuadricula = False
                frm.BanBorderDetalle = False

                frm.TituloReporte = "Reporte de Catalogo de Herramientas"
                frm.ConfigRep({New ColClass("idHerramienta", "Id", TipoDato.TdCadena, False, False, TipoOrdenamiento.Asc, TipoControlReport.TdCadena, True, 40),
                New ColClass("NomHerramienta", "Nombre", TipoDato.TdCadena, False, True, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 150),
                New ColClass("NomTipoHerramienta", "Tipo", TipoDato.TdCadena, True, True, TipoOrdenamiento.Asc, TipoControlReport.TdCadena, True, 60,,, "select DISTINCT her.idTipoHerramienta, tph.NomTipoHerramienta FROM CatHerramientas Her inner join CatTipoHerramienta tph on her.idTipoHerramienta = tph.idTipoHerramienta "),
                New ColClass("NombreMarca", "Marca", TipoDato.TdCadena, True, True, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 50,,, "select DISTINCT her.idMarca, mar.NombreMarca FROM CatHerramientas Her inner join CatMarcas mar on her.idMarca = mar.idMarca "),
                New ColClass("Modelo", "Modelo", TipoDato.TdCadena, False, True, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 50),
                New ColClass("Medida", "Medida", TipoDato.TdCadena, False, True, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 50)})

            Case UCase("CatUnidadTrans")
                frm.BanHorizontal = True
                frm.BanCuadricula = False
                frm.BanBorderDetalle = False

                frm.TituloReporte = "Reporte de Catalogo de Unidades de Transporte"
                frm.ConfigRep({New ColClass("idUnidadTrans", "Unidad", TipoDato.TdCadena, True, True, TipoOrdenamiento.Asc, TipoControlReport.TdCadena, True, 40),
                New ColClass("descripcionUni", "Descripción", TipoDato.TdCadena, False, False, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 80),
                New ColClass("idTipoUnidad", "idTipoUnidad", TipoDato.TdNumerico, False, False, TipoOrdenamiento.Asc, TipoControlReport.TdCadena, False, 0),
                New ColClass("nomTipoUniTras", "Tipo Unidad", TipoDato.TdCadena, False, False, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, False, 0),
                New ColClass("clasificacion", "Clasif.", TipoDato.TdCadena, True, False, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 60,,, "SELECT DISTINCT tuni.clasificacion FROM dbo.CatUnidadTrans uni LEFT JOIN dbo.CatTipoUniTrans tuni ON tuni.idTipoUniTras = uni.idTipoUnidad WHERE ISNULL(tuni.clasificacion,'') <> ''"),
                New ColClass("idMarca", "idMarca", TipoDato.TdNumerico, False, False, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, False, 0),
                New ColClass("NombreMarca", "Marca", TipoDato.TdCadena, True, False, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 100,,, "SELECT DISTINCT uni.idMarca,ISNULL(mar.NombreMarca,'') AS NombreMarca FROM dbo.CatUnidadTrans uni LEFT JOIN dbo.CatMarcas mar ON mar.idMarca = uni.idMarca"),
                New ColClass("idEmpresa", "idEmpresa", TipoDato.TdNumerico, False, False, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, False, 0),
                New ColClass("RazonSocial", "Empresa", TipoDato.TdCadena, True, False, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 60,,, "SELECT DISTINCT uni.idEmpresa AS idEmpresa, emp.RazonSocial AS RazonSocial FROM dbo.CatUnidadTrans uni LEFT JOIN dbo.CatEmpresas emp ON emp.idEmpresa = uni.idEmpresa",,,,, "EMP"),
                New ColClass("Estatus", "Estatus", TipoDato.TdCadena, False, False, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 35)})

            Case UCase("CatServicios")
                frm.BanHorizontal = True
                frm.BanCuadricula = False
                frm.BanBorderDetalle = False

                frm.TituloReporte = "Reporte de Catalogo de Servicios"
                frm.ConfigRep({New ColClass("idServicio", "Ser.", TipoDato.TdCadena, False, True, TipoOrdenamiento.Asc, TipoControlReport.TdCadena, True, 25),
                New ColClass("idTipoServicio", "idTipoServicio", TipoDato.TdCadena, False, False, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, False, 0),
                New ColClass("NomTipoServicio", "Tipo Serv.", TipoDato.TdCadena, True, True, TipoOrdenamiento.Asc, TipoControlReport.TdCadena, True, 40,,, "SELECT DISTINCT ser.idTipoServicio,tser.NomTipoServicio FROM dbo.CatServicios ser INNER JOIN dbo.CatTipoServicio tser ON tser.idTipoServicio = ser.idTipoServicio"),
                New ColClass("Descripcion", "Descripcion", TipoDato.TdCadena, False, True, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 150),
                New ColClass("CadaKms", "Cada Km.", TipoDato.TdCadena, False, True, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 40),
                New ColClass("CadaTiempoDias", "Cada Dias", TipoDato.TdCadena, False, True, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 40),
                New ColClass("Estatus", "Estatus", TipoDato.TdCadena, True, False, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 35,,, "SELECT 'ACT' AS Estatus UNION SELECT 'INA' AS Estatus"),
                New ColClass("Costo", "Costo", TipoDato.TdNumerico, False, True, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 46),
                New ColClass("idDivision", "idDivision", TipoDato.TdCadena, True, False, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, False, 0),
                New ColClass("NomDivision", "Division", TipoDato.TdCadena, True, False, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 35,,, "SELECT DISTINCT ser.idDivision, div.NomDivision FROM dbo.CatServicios ser INNER JOIN dbo.CatDivision div ON div.idDivision = ser.idDivision")})


            Case UCase("CatPersonal")
                frm.BanHorizontal = True
                frm.BanCuadricula = False
                frm.BanBorderDetalle = False

                frm.TituloReporte = "Reporte de Catalogo de Personal"

                frm.ConfigRep({New ColClass("idPersonal", "Id", TipoDato.TdCadena, False, False, TipoOrdenamiento.Asc, TipoControlReport.TdCadena, True, 20),
                    New ColClass("idEmpresa", "idEmpresa", TipoDato.TdCadena, False, False, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, False, 0),
                    New ColClass("RazonSocial", "Empresa", TipoDato.TdCadena, True, True, TipoOrdenamiento.Asc, TipoControlReport.TdCadena, True, 60),
                    New ColClass("NombreCompleto", "NombreCompleto", TipoDato.TdCadena, False, False, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, False, 0),
                    New ColClass("Nombre", "Nombres", TipoDato.TdCadena, True, True, TipoOrdenamiento.Desc, TipoControlReport.TdLikeCadena, True, 60),
                    New ColClass("ApellidoPat", "Ap. Pat", TipoDato.TdCadena, True, True, TipoOrdenamiento.Desc, TipoControlReport.TdLikeCadena, True, 50),
                    New ColClass("ApellidoMat", "Ap. Mat", TipoDato.TdCadena, True, True, TipoOrdenamiento.Desc, TipoControlReport.TdLikeCadena, True, 50),
                    New ColClass("idPuesto", "idPuesto", TipoDato.TdNumerico, False, False, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, False, 0),
                    New ColClass("NomPuesto", "Puesto", TipoDato.TdCadena, True, True, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 60,,, "SELECT DISTINCT per.idPuesto,pue.NomPuesto FROM dbo.CatPersonal per INNER JOIN dbo.CatPuestos pue ON pue.idPuesto = per.idPuesto"),
                    New ColClass("PrecioHora", "Prec. Hr.", TipoDato.TdCadena, False, False, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, False, 0),
                    New ColClass("Direccion", "Direccion", TipoDato.TdCadena, False, False, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, False, 0),
                    New ColClass("idColonia", "idColonia", TipoDato.TdCadena, False, False, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, False, 0),
                    New ColClass("NomColonia", "Colonia", TipoDato.TdCadena, True, True, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, False, 0,,, "SELECT DISTINCT per.idColonia,col.NomColonia FROM dbo.CatPersonal per LEFT JOIN dbo.CatColonia col ON col.idColonia = per.idColonia"),
                    New ColClass("CP", "C.P.", TipoDato.TdCadena, True, True, TipoOrdenamiento.Desc, TipoControlReport.TdLikeCadena, False, 35),
                    New ColClass("TelefonoPersonal", "Telefono", TipoDato.TdCadena, False, False, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, False, 0),
                    New ColClass("Estatus", "Estatus", TipoDato.TdCadena, True, True, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 30,,, "SELECT 'ACT' AS Estatus UNION SELECT 'BAJ' AS Estatus"),
                    New ColClass("idDepto", "idDepto", TipoDato.TdCadena, False, False, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, False, 0),
                    New ColClass("NomDepto", "Depto.", TipoDato.TdCadena, True, True, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 40,,, "SELECT DISTINCT per.idDepto,dep.NomDepto FROM dbo.CatPersonal per LEFT JOIN dbo.CatDeptos dep ON dep.IdDepto = per.idDepto"),
                    New ColClass("EsTaller", "EsTaller", TipoDato.TdCadena, False, False, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, False, 0),
                    New ColClass("rfid", "rfid", TipoDato.TdCadena, False, False, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, False, 0)
                    })

            Case UCase("CatTipoLlanta")
                frm.BanHorizontal = False
                frm.BanCuadricula = False
                frm.BanBorderDetalle = False
                frm.TituloReporte = "Reporte de Catalogo de Tipo de LLantas"

                frm.ConfigRep({New ColClass("idTipoLLanta", "Tipo LLanta", TipoDato.TdCadena, False, True, TipoOrdenamiento.Asc, TipoControlReport.TdCadena, True, 60),
               New ColClass("Descripcion", "Descripción", TipoDato.TdCadena, False, True, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 100)})

            Case UCase("CatClasLlan")
                frm.BanHorizontal = False
                frm.BanCuadricula = False
                frm.BanBorderDetalle = False
                frm.TituloReporte = "Reporte de Catalogo de Clasificación de Llantas"

                frm.ConfigRep({New ColClass("idClasLlan", "Id", TipoDato.TdCadena, False, False, TipoOrdenamiento.Asc, TipoControlReport.TdCadena, True, 45),
                New ColClass("NombreClasLlan", "Nombre", TipoDato.TdCadena, False, True, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 150),
                New ColClass("NoEjes", "# Ejes", TipoDato.TdCadena, True, True, TipoOrdenamiento.Asc, TipoControlReport.TdCadena, True, 60,,, "SELECT DISTINCT NoEjes FROM dbo.CatClasLlan",,,,,,, TipoDato.TdNumerico),
                New ColClass("NoLLantas", "# Llantas", TipoDato.TdCadena, True, True, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 60,,, "SELECT DISTINCT NoLLantas FROM dbo.CatClasLlan",,,,,,, TipoDato.TdNumerico)})

            Case UCase("CatLLantasProducto")
                frm.BanHorizontal = False
                frm.BanCuadricula = False
                frm.BanBorderDetalle = False
                frm.TituloReporte = "Reporte de Catalogo de Llantas Productos"
        End Select
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.ShowDialog()
        frm.Dispose()

        'frm.ConfigRep({New ColClass("NOMOVIMIENTO", "No.", TipoDato.TdCadena, False, False, TipoOrdenamiento.Asc, TipoControlReport.TdNumerico, True, 20, , , "SELECT DISTINCT NOMOVIMIENTO FROM CABMOVIMIENTOS WHERE  ESTATUS in ('TER') and nomovimiento = NoMovimientoPadre and bCantCompleta = 0 ORDER BY NOMOVIMIENTO ASC", , ColClass.EnTipoFilCadena.Obligatorio),
        '    New ColClass("FECHAMOVIMIENTO", "  Fecha", TipoDato.TdFecha, True, False, TipoOrdenamiento.Desc, TipoControlReport.TdRangoFecha, True, 60),
        '   New ColClass("CNOMBREC01_TRAS", " Traspaso", TipoDato.TdCadena, False, False, TipoOrdenamiento.Desc, TipoControlReport.TdNumerico, True, 100),
        '   New ColClass("NOMALMSAL", "Alm.Salida", TipoDato.TdCadena, False, False, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 70),
        '   New ColClass("FOLIO_SAL", "Folio", TipoDato.TdCadena, False, False, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 20),
        '   New ColClass("NOMALMENT", "Alm.Entrada", TipoDato.TdCadena, False, False, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 70),
        '  New ColClass("FOLIO_ENT", "Folio", TipoDato.TdCadena, False, False, TipoOrdenamiento.Desc, TipoControlReport.TdNumerico, True, 20),
        '   New ColClass("Detalle", {New CheckBox With {.Checked = False}})
        '   })

        'Case UCase("TRASPFALTANTES"), UCase("TRASPFALTANTESINI")
        '    frm.BanHorizontal = True
        '    frm.BanCuadricula = False
        '    frm.BanBorderDetalle = False
        '    frm.TituloReporte = "Reporte de Traspasos Faltantes"

        '    frm.ConfigRep({New ColClass("NOMOVIMIENTO", "No.", TipoDato.TdCadena, False, False, TipoOrdenamiento.Asc, TipoControlReport.TdNumerico, True, 20, , , "SELECT DISTINCT NOMOVIMIENTO FROM CABMOVIMIENTOS WHERE  ESTATUS in ('TER') and nomovimiento = NoMovimientoPadre and bCantCompleta = 0 ORDER BY NOMOVIMIENTO ASC", , ColClass.EnTipoFilCadena.Obligatorio),
        '        New ColClass("FECHAMOVIMIENTO", "  Fecha", TipoDato.TdFecha, True, False, TipoOrdenamiento.Desc, TipoControlReport.TdRangoFecha, True, 60),
        '       New ColClass("CNOMBREC01_TRAS", " Traspaso", TipoDato.TdCadena, False, False, TipoOrdenamiento.Desc, TipoControlReport.TdNumerico, True, 100),
        '       New ColClass("NOMALMSAL", "Alm.Salida", TipoDato.TdCadena, False, False, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 70),
        '       New ColClass("FOLIO_SAL", "Folio", TipoDato.TdCadena, False, False, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 20),
        '       New ColClass("NOMALMENT", "Alm.Entrada", TipoDato.TdCadena, False, False, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 70),
        '      New ColClass("FOLIO_ENT", "Folio", TipoDato.TdCadena, False, False, TipoOrdenamiento.Desc, TipoControlReport.TdNumerico, True, 20),
        '       New ColClass("Detalle", {New CheckBox With {.Checked = False}})
        '       })
        'If UCase(Tabla) = UCase("TRASPFALTANTES") Then


        '    'ElseIf UCase(Tabla) = UCase("REMISION") Then
        '    '    frm.BanHorizontal = False
        '    '    frm.BanCuadricula = False
        '    '    frm.BanImpTotales = True
        '    '    frm.BanBorderDetalle = True

        '    '    frm.TituloReporte = "Remisión"
        '    '    'New ColClass("Estatus", "Estatus", TipoDato.TdCadena, True, False, TipoOrdenamiento.Asc, TipoControlReport.TdCadena, False, 1, , , "select distinct Estatus  from CabMovimientos"),
        '    '    frm.ConfigRep({New ColClass("TipoMovimiento", "Tipo Movimiento", TipoDato.TdCadena, True, False, TipoOrdenamiento.Asc, TipoControlReport.TdCadena, False, 1, , , "SELECT DISTINCT TipoMovimiento AS TipoMovimiento FROM CABMOVIMIENTOS WHERE TIPOMOVIMIENTO <> 'FAC' AND ESTATUS = 'TER' ORDER BY TipoMovimiento DESC", , ColClass.EnTipoFilCadena.Obligatorio, , , "CAB"),
        '    '                   New ColClass("SERIE", "Serie", TipoDato.TdCadena, True, False, TipoOrdenamiento.Asc, TipoControlReport.TdCadena, False, 1, , , "SELECT DISTINCT NOSERIE AS SERIE FROM CABMOVIMIENTOS WHERE TIPOMOVIMIENTO <> 'FAC' AND ESTATUS = 'TER' ORDER BY NoSerie DESC", , ColClass.EnTipoFilCadena.Obligatorio, , , "CAB"),
        '    '                   New ColClass("IDMOVIMIENTO", "Folio", TipoDato.TdNumerico, True, False, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, False, 10, , , "SELECT DISTINCT IDMOVIMIENTO AS IDMOVIMIENTO FROM CABMOVIMIENTOS WHERE TIPOMOVIMIENTO <> 'FAC' AND ESTATUS = 'TER' ORDER BY IDMOVIMIENTO ASC", , ColClass.EnTipoFilCadena.Obligatorio, , , "CAB"),
        '    '                   New ColClass("CCODIGOP01_PRO", "Código", TipoDato.TdCadena, False, False, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 30),
        '    '                   New ColClass("CNOMBREP01", "Descripción", TipoDato.TdCadena, False, False, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 80),
        '    '                   New ColClass("UNIDAD", "Unidad", TipoDato.TdCadena, False, False, TipoOrdenamiento.Desc, TipoControlReport.TdCadena, True, 30),
        '    '                   New ColClass("Cantidad", "Cant. Pedida", TipoDato.TdNumerico, False, False, TipoOrdenamiento.Desc, TipoControlReport.TdNumerico, True, 50),
        '    '                   New ColClass("PRECIO", "Precio Unit.", TipoDato.TdNumerico, False, False, TipoOrdenamiento.Desc, TipoControlReport.TdNumerico, True, 50),
        '    '                   New ColClass("IMPORTE", "Importe", TipoDato.TdNumerico, False, False, TipoOrdenamiento.Desc, TipoControlReport.TdNumerico, True, 120),
        '    '                   New ColClass("No. Copias", {New NumericUpDown With {.DecimalPlaces = 0, .Value = 1, .Width = 50, .Minimum = 1}})
        '    '                   })
        '    '    'New ColClass("No. Copias", {New NumericUpDown With {.DecimalPlaces = 0, .Value = 1, .Width = 50, .Minimum = 1}})
        'End If

    End Sub

    Public Function DespliegaGrid(ByVal Tabla As String, ByVal ConStr As String, ByVal CampoOrdenar As String, _
                              Optional ByVal FiltroSinWhere As String = "", Optional ByVal ValorEspecial As String = "") As String
        'Optional ByVal ValorEspecial As Integer = 0
        Dim StrSql As String = ""
        Dim frm As New frmgrid
        DespliegaGrid = ""

        Select Case UCase(Tabla)

            Case UCase("COMBEXTRACAP")
                StrSql = "SELECT cce.idVCExtra, TCCE.NomTipoOSComb, CCE.FechaCrea FROM dbo.cargaCombustibleExtra CCE " &
                "INNER JOIN dbo.CatTipoOsComb TCCE ON TCCE.idTipoOSComb = CCE.idTipoOSComb " &
                "WHERE ISNULL(ltCombustible,0) = 0 AND ISNULL(idDespachador,0) =0"
                frm.ConfiguraGrid(StrSql, ConStr, TipoConexion.TcSQL, "Vales de Combustible Extra", Tabla,
            "idVCExtra", "No. Vale", 100, TipoDato.TdCadena,
            "NomTipoOSComb", "Tipo de Vale", 200, TipoDato.TdCadena,
            "FechaCrea", "FEcha", 300, TipoDato.TdFecha, , , , , , , , , , , , , , , , True)



            Case UCase("FolioOS")
                StrSql = "SELECT mos.idPadreOrdSer, mos.FechaCreacion FROM dbo.MasterOrdServ MOS"
                frm.ConfiguraGrid(StrSql, ConStr, TipoConexion.TcSQL, "Folios Orden de Servicio", Tabla,
            "idPadreOrdSer", "FOLIO", 100, TipoDato.TdCadena,
            "FechaCreacion", "Fecha", 300, TipoDato.TdFechaHora, , , , , , , , , , , , , , , , , , , , True)

            Case UCase("UNITRANSTIPO")
                StrSql = "SELECT UNI.idUnidadTrans, UNI.descripcionUni FROM dbo.CatUnidadTrans uni " &
                "INNER JOIN dbo.CatTipoUniTrans tip ON uni.idTipoUnidad = tip.idTipoUniTras " &
                "WHERE " & FiltroSinWhere
                '"WHERE tip.clasificacion = '" & ValorEspecial & "' " & FiltroSinWhere

                frm.ConfiguraGrid(StrSql, ConStr, TipoConexion.TcSQL, "Catalogo de Unidades de Transporte", Tabla,
             "idUnidadTrans", "Unidad Trans.", 100, TipoDato.TdCadena,
             "descripcionUni", "Nombre", 300, TipoDato.TdCadena, , , , , , , , , , , , , , , , , , , , True)



            Case UCase("UNIPROD_COM")
                StrSql = "select UNI.CCLAVEINT,UNI.CNOMBREUNIDAD,UNI.CIDUNIDAD " &
                "from admUnidadesMedidaPeso UNI where uni.cidunidad > 0"
                frm.ConfiguraGrid(StrSql, CONSTR_COM, TipoConexion.TcSQL, "Catalogo de Unidades", Tabla,
             "CCLAVEINT", "Codigo", 80, TipoDato.TdCadena,
             "CNOMBREUNIDAD", "Nombre", 200, TipoDato.TdCadena)

            Case UCase("PROD_COM")
                StrSql = "select CCODIGOPRODUCTO,CNOMBREPRODUCTO,CIDUNIDADBASE,CIDPRODUCTO " &
                "from admProductos " &
                "where CIDPRODUCTO > 0"

                frm.ConfiguraGrid(StrSql, CONSTR_COM, TipoConexion.TcSQL, "Catalogo de Refacciones", Tabla,
             "CCODIGOPRODUCTO", "Codigo", 80, TipoDato.TdCadena,
             "CNOMBREPRODUCTO", "Nombre", 200, TipoDato.TdCadena, , , , , , , , , , , , , , , , , , "Campo2")


            Case UCase("CatMarcas")
                StrSql = "select idMarca, NombreMarca from CATMARCAS"

                frm.ConfiguraGrid(StrSql, ConStr, TipoConexion.TcSQL, "Catalogo de Marcas", Tabla,
                "idMarca", "Marca", 100, TipoDato.TdNumerico,
                "NombreMarca", "Nombre", 250, TipoDato.TdCadena,,,,,,,,,,,,,,,,,, "Campo2")

            Case UCase("CatPais")
                StrSql = "SELECT p.idPais,p.NomPais FROM dbo.CatPais P"

                frm.ConfiguraGrid(StrSql, ConStr, TipoConexion.TcSQL, "Catalogo de Países", Tabla,
              "idPais", "País", 80, TipoDato.TdNumerico,
              "NomPais", "Nombre", 300, TipoDato.TdCadena)

            Case UCase("CatEstados")
                StrSql = "SELECT EST.IDESTADO, EST.NOMESTADO, P.NOMPAIS " &
                "FROM DBO.CATESTADOS EST " &
                "INNER JOIN DBO.CATPAIS P ON EST.IDPAIS = P.IDPAIS"


                frm.ConfiguraGrid(StrSql, ConStr, TipoConexion.TcSQL, "Catalogo de Estados", Tabla,
               "IDESTADO", "Estado", 80, TipoDato.TdNumerico,
               "NOMESTADO", "Nombre", 300, TipoDato.TdCadena,
               "NOMPAIS", "País", 200, TipoDato.TdCadena)
            Case UCase("CatCiudad")
                StrSql = "SELECT ciu.idCiudad, ciu.NomCiudad, est.NomEstado, p.NomPais FROM dbo.CatCiudad ciu " &
                "INNER JOIN dbo.CatEstados Est ON ciu.idEstado = Est.idEstado " &
                "INNER JOIN dbo.CatPais p ON est.idPais = p.idPais"

                frm.ConfiguraGrid(StrSql, ConStr, TipoConexion.TcSQL, "Catalogo de Ciudades", Tabla,
             "idCiudad", "Ciudad", 80, TipoDato.TdNumerico,
             "NomCiudad", "Nombre", 300, TipoDato.TdCadena,
             "NomEstado", "Estado", 200, TipoDato.TdCadena,
             "NomPais", "País", 200, TipoDato.TdCadena)
            Case UCase("CatColonia")
                StrSql = "SELECT col.idColonia, col.NomColonia, col.CP FROM dbo.CatColonia col"
                frm.ConfiguraGrid(StrSql, ConStr, TipoConexion.TcSQL, "Catalogo de Colonias", Tabla,
               "idColonia", "Colonia", 80, TipoDato.TdNumerico,
               "NomColonia", "Nombre", 300, TipoDato.TdCadena,
               "CP", "C.P.", 80, TipoDato.TdCadena)

            Case UCase("CatPuestos")
                StrSql = "SELECT pue.idPuesto, pue.NomPuesto FROM dbo.CatPuestos pue"
                frm.ConfiguraGrid(StrSql, ConStr, TipoConexion.TcSQL, "Catalogo de Puestos", Tabla,
               "idPuesto", "Puesto", 80, TipoDato.TdNumerico,
               "NomPuesto", "Nombre", 300, TipoDato.TdCadena)
            Case UCase("CatDeptos")
                StrSql = "SELECT dep.IdDepto, dep.NomDepto AS Nombre FROM dbo.CatDeptos dep"
                frm.ConfiguraGrid(StrSql, ConStr, TipoConexion.TcSQL, "Catalogo de Departamentos", Tabla,
                "IdDepto", "Depto.", 80, TipoDato.TdNumerico,
                "Nombre", "Nombre", 300, TipoDato.TdCadena)

            Case UCase("CatEmpresas")
                StrSql = "SELECT emp.idEmpresa, emp.RazonSocial FROM dbo.CatEmpresas emp"
                frm.ConfiguraGrid(StrSql, ConStr, TipoConexion.TcSQL, "Catalogo de Empresas", Tabla,
                "idEmpresa", "Empresa", 80, TipoDato.TdNumerico,
                "RazonSocial", "Nombre", 300, TipoDato.TdCadena)

            Case UCase("CatPersonal")
                StrSql = "SELECT per.idPersonal,per.NombreCompleto AS Nombre FROM dbo.CatPersonal per"
                frm.ConfiguraGrid(StrSql, ConStr, TipoConexion.TcSQL, "Catalogo de Personal", Tabla,
                "idPersonal", "Personal", 80, TipoDato.TdNumerico,
                "Nombre", "Nombre", 400, TipoDato.TdCadena)

            Case UCase("Catdivision")
                StrSql = "select idDivision, NomDivision from Catdivision"

                frm.ConfiguraGrid(StrSql, ConStr, TipoConexion.TcSQL, "Catalogo de Divisiones", Tabla,
                "idDivision", "División", 80, TipoDato.TdNumerico,
                "NomDivision", "Nombre", 200, TipoDato.TdCadena)
            Case UCase("CatFamilia")
                StrSql = "SELECT FAM.IDFAMILIA, FAM.NOMFAMILIA, FAM.IDDIVISION, DIV.NOMDIVISION " &
                "FROM CATFAMILIA FAM " &
                "INNER JOIN CATDIVISION DIV ON FAM.IDDIVISION = DIV.IDDIVISION"

                frm.ConfiguraGrid(StrSql, ConStr, TipoConexion.TcSQL, "Catalogo de Divisiones", Tabla,
                "IDFAMILIA", "Familia", 80, TipoDato.TdNumerico,
                "NOMFAMILIA", "Nombre Familia", 200, TipoDato.TdCadena,
                "IDDIVISION", "División", 80, TipoDato.TdNumerico,
                "NOMDIVISION", "Nombre Division", 200, TipoDato.TdCadena)
            Case UCase("CatTipoServicio")
                StrSql = "SELECT ts.idTipoServicio,ts.NomTipoServicio FROM dbo.CatTipoServicio ts"

                frm.ConfiguraGrid(StrSql, ConStr, TipoConexion.TcSQL, "Catalogo de Tipos de Servicio", Tabla,
                "idTipoServicio", "Tipo Servicio", 100, TipoDato.TdNumerico,
                "NomTipoServicio", "Descripción", 200, TipoDato.TdCadena)
            Case UCase("CatServicios")
                StrSql = "SELECT ser.idServicio, " &
                "ser.NomServicio, " &
                "tser.NomTipoServicio, " &
                "div.NomDivision, " &
                "ser.Estatus " &
                "FROM dbo.CatServicios ser " &
                "INNER JOIN dbo.CatTipoServicio tser ON ser.idTipoServicio = tser.idTipoServicio " &
                "INNER JOIN dbo.CatDivision div ON ser.idDivision = div.idDivision"

                frm.ConfiguraGrid(StrSql, ConStr, TipoConexion.TcSQL, "Catalogo de Divisiones", Tabla,
                "idServicio", "Servicio", 80, TipoDato.TdNumerico,
                "NomServicio", "Nombre", 200, TipoDato.TdCadena,
                "NomTipoServicio", "Tipo Servicio", 80, TipoDato.TdNumerico,
                "NomDivision", "Division", 200, TipoDato.TdCadena,
                "Estatus", "Estatus", 200, TipoDato.TdCadena)

            Case UCase("CatUnidadTrans")
                StrSql = "SELECT uni.idUnidadTrans, " &
                "uni.descripcionUni, " &
                "tuni.nomTipoUniTras, " &
                "tuni.clasificacion, " &
                "mar.NombreMarca, " &
                "emp.RazonSocial, " &
                "uni.Estatus " &
                "FROM dbo.CatUnidadTrans uni " &
                "left JOIN dbo.CatTipoUniTrans tuni ON tuni.idTipoUniTras = uni.idTipoUnidad " &
                "left JOIN dbo.CatMarcas mar ON mar.idMarca = uni.idMarca " &
                "left JOIN dbo.CatEmpresas emp ON emp.idEmpresa = uni.idEmpresa"

                frm.ConfiguraGrid(StrSql, ConStr, TipoConexion.TcSQL, "Catalogo de Unidades de Transporte", Tabla,
                "idUnidadTrans", "Unidad", 80, TipoDato.TdCadena,
                "descripcionUni", "Descripcion", 200, TipoDato.TdCadena,
                "clasificacion", "Clasificacion", 80, TipoDato.TdCadena,
                "NombreMarca", "Marca", 100, TipoDato.TdCadena,
                "RazonSocial", "Empresa", 100, TipoDato.TdCadena,
                "Estatus", "Estatus", 50, TipoDato.TdCadena)

            Case UCase("CatTipoUniTrans")
                StrSql = "SELECT tuni.idTipoUniTras, " &
                "tuni.nomTipoUniTras, " &
                "ISNULL(tuni.TonelajeMax,0) AS TonelajeMax, " &
                "ISNULL(tuni.clasificacion,'') AS clasificacion, " &
                "CASE WHEN tuni.bCombustible = 1 THEN 'SI' ELSE 'NO' END AS Combustible " &
                "FROM CatTipoUniTrans tuni"

                frm.ConfiguraGrid(StrSql, ConStr, TipoConexion.TcSQL, "Catalogo de Tipos de Unidades de Transporte", Tabla,
                "idTipoUniTras", "Tipo", 80, TipoDato.TdNumerico,
                "nomTipoUniTras", "Nombre", 200, TipoDato.TdCadena,
                "TonelajeMax", "Tonelaje Max.", 80, TipoDato.TdNumerico,
                "clasificacion", "Clasificación", 100, TipoDato.TdCadena,
                "Combustible", "Combustible", 100, TipoDato.TdCadena,,,,,, "nomTipoUniTras")


            Case UCase("CatSucursal")

                StrSql = "SELECT suc.id_sucursal, " &
                "suc.NomSucursal, " &
                "suc.id_empresa, " &
                "emp.RazonSocial " &
                "FROM dbo.CatSucursal suc " &
                "INNER JOIN dbo.CatEmpresas emp ON emp.idEmpresa = suc.id_empresa"

                frm.ConfiguraGrid(StrSql, ConStr, TipoConexion.TcSQL, "Catalogo de Sucursales", Tabla,
               "id_sucursal", "Sucursal", 80, TipoDato.TdNumerico,
               "NomSucursal", "Nombre", 200, TipoDato.TdCadena,
               "id_empresa", "Empresa", 80, TipoDato.TdNumerico,
               "RazonSocial", "Razón Social", 200, TipoDato.TdCadena)

            Case UCase("CatAlmacen")
                StrSql = "SELECT ALM.idAlmacen, " &
                "ALM.NomAlmacen, " &
                "ALM.idSucursal, " &
                "SUC.NomSucursal, " &
                "EMP.RazonSocial " &
                "FROM dbo.CatAlmacen ALM " &
                "INNER JOIN dbo.CatSucursal SUC ON SUC.id_sucursal = ALM.idSucursal " &
                "INNER JOIN dbo.CatEmpresas EMP ON EMP.idEmpresa = SUC.id_empresa"

                frm.ConfiguraGrid(StrSql, ConStr, TipoConexion.TcSQL, "Catalogo de Almacen", Tabla,
                "idAlmacen", "Almacen", 80, TipoDato.TdNumerico,
                "NomAlmacen", "Nombre", 200, TipoDato.TdCadena,
                "idSucursal", "Sucursal", 80, TipoDato.TdNumerico,
                "NomSucursal", "Nombre Suc.", 150, TipoDato.TdCadena,
                "RazonSocial", "Empresa", 100, TipoDato.TdCadena)


            Case UCase("CatTipoHerramienta")
                StrSql = "SELECT ther.idTipoHerramienta, " &
                "ther.NomTipoHerramienta " &
                "FROM dbo.CatTipoHerramienta ther"

                frm.ConfiguraGrid(StrSql, ConStr, TipoConexion.TcSQL, "Catalogo de Tipos de Herramienta", Tabla,
                "idTipoHerramienta", "Tipo", 100, TipoDato.TdNumerico,
                "NomTipoHerramienta", "Descripción", 200, TipoDato.TdCadena)
            Case UCase("CatClasLlan")
                StrSql = "SELECT idClasLlan, " &
                "NombreClasLlan, " &
                "NoEjes, " &
                "NoLLantas " &
                "FROM dbo.CatClasLlan"


                frm.ConfiguraGrid(StrSql, ConStr, TipoConexion.TcSQL, "Catalogo de Clasificacion de Llantas", Tabla,
               "idClasLlan", "Id", 80, TipoDato.TdNumerico,
               "NombreClasLlan", "Nombre", 200, TipoDato.TdCadena,
               "NoEjes", "# Ejes", 80, TipoDato.TdNumerico,
               "NoLLantas", "# Llantas", 80, TipoDato.TdCadena)


        End Select
        'If UCase(Tabla) = UCase("Productos") Then
        '    StrSql = "SELECT CCODIGOP01, CNOMBREP01, CPRECIO1 FROM MGW10005 where CIDPRODU01 > 0"

        '    frm.ConfiguraGrid(StrSql, ConStr, TipoConexion.TcFox, "Catalogos de Productos", "Productos", _
        ' "CCODIGOP01", "Código", 80, TipoDato.TdCadena, _
        ' "CNOMBREP01", "Nombre", 200, TipoDato.TdCadena, _
        ' "COSTO", "Costo", 80, TipoDato.TdNumerico)

        '    'AQUI()
        'ElseIf UCase(Tabla) = UCase("ProductosGrid") Then
        '    'CUANDO SE CAMBIO Y PARA QUE ????
        '    StrSql = "SELECT DET.CCODIGOP01_PRO  AS CCODIGOP01, PRO.CNOMBREP01 AS CNOMBREP01, DET.COSTO " & _
        '    "FROM DETMOVIMIENTOS DET " & _
        '    "INNER JOIN CATPRODUCTOS PRO ON DET.CCODIGOP01_PRO = PRO.CCODIGOP01 " & _
        '    "WHERE det.NoMovimiento = ( SELECT TOP 1 NOMOVIMIENTO  FROM CABMOVIMIENTOS " & _
        '    "WHERE NOMOVIMIENTOPADRE =  " & Val(ValorEspecial) & "  AND TIPOMOVIMIENTO = 'ENT' AND ESTATUS = 'TER' AND BCANTCOMPLETA = 1 " & _
        '    "ORDER BY FECHAMOVIMIENTO ASC ) " & _
        '    "ORDER BY DET.CONSECUTIVO"

        '    frm.ConfiguraGrid(StrSql, ConStr, TipoConexion.TcSQL, "Catalogos de Productos", "Productos", _
        '   "CCODIGOP01", "Código", 80, TipoDato.TdCadena, _
        '   "CNOMBREP01", "Nombre", 200, TipoDato.TdCadena, _
        '   "COSTO", "Costo", 80, TipoDato.TdNumerico)

        '    'CabVentas
        'ElseIf UCase(Tabla) = UCase("CabVentas") Then

        '    StrSql = "select cab.IdMovimientoDia, " & _
        '     "(cli.Nombre + ' ' + cli.ApellidoPat + ' ' + CLI.ApellidoMat) as cliente, cab.TipoOrden, " & _
        '     "(cab.SubTotalGrav + cab.SubTotalExento + cab.Iva ) as total,cab.Observaciones , cab.NoMovimiento,  cab.Estatus " & _
        '     "from CabMovimientos CAB " & _
        '     "inner join CatClientes CLI on cab.IdCliente = cli.IdCliente " & _
        '     "where (FechaMovimiento >= '" & FormatFecHora(Now, False) & " 00:00:00' and FechaMovimiento <= '" & FormatFecHora(Now, False) & " 23:59:59') " & _
        '     "and TipoMovimiento = 'VTA' " & IIf(FiltroSinWhere <> "", " AND " & FiltroSinWhere, "")

        '    '"and Estatus in ('PEN','PENLOC','CAP')"

        '    frm.ConfiguraGrid(StrSql, ConStr, TipoConexion.TcSQL, "Ordenes Pendientes", "CabMovimientos", _
        '    "IdMovimientoDia", "Orden", 50, TipoDato.TdCadena,
        '    "TipoOrden", "Tipo Orden", 50, TipoDato.TdCadena,
        '    "Estatus", "Estatus", 50, TipoDato.TdCadena,
        '    "cliente", "cliente", 200, TipoDato.TdCadena,
        '    "total", "Total", 60, TipoDato.TdNumerico,
        '    "Observaciones", "Observaciones", 100, TipoDato.TdCadena, , "Campo2")
        'ElseIf UCase(Tabla) = UCase("CabMovimientos") Then

        '    'StrSql = "select cab.IdMovimientoDia, " & _
        '    ' "(cli.Nombre + ' ' + cli.ApellidoPat + ' ' + CLI.ApellidoMat) as cliente, cab.TipoOrden, " & _
        '    ' "(cab.SubTotalGrav + cab.SubTotalExento + cab.Iva ) as total,cab.Observaciones , cab.NoMovimiento " & _
        '    ' "from CabMovimientos CAB " & _
        '    ' "left join CatClientes CLI on cab.IdCliente = cli.IdCliente " & _
        '    ' "where (FechaMovimiento >= '" & FormatFecHora(Now, False) & " 00:00:00' and FechaMovimiento <= '" & FormatFecHora(Now, False) & " 23:59:59') " & _
        '    ' "and TipoMovimiento = 'TRAS' and Estatus in ('PEN','PENLOC','CAP')"

        '    StrSql = "SELECT DISTINCT CAB.NOMOVIMIENTO, CAB.FECHAMOVIMIENTO, " & _
        '    "CAB.CIDALMACEN_SAL, ALMSAL.CNOMBREA01 AS NOMALMSAL, " & _
        '    "CAB.CIDALMACEN_ENT, ALMENT.CNOMBREA01 AS NOMALMENT " & _
        '    "FROM CABMOVIMIENTOS CAB " & _
        '    "INNER JOIN CATALMACENES ALMSAL ON CAB.CIDALMACEN_SAL = ALMSAL.CCODIGOA01 " & _
        '    "INNER JOIN CATALMACENES ALMENT ON CAB.CIDALMACEN_ENT = ALMENT.CCODIGOA01 " & _
        '    IIf(FiltroSinWhere <> "", " WHERE " & FiltroSinWhere, "")

        '    '"WHERE CAB.ESTATUS = 'CAP'" & IIf(FiltroSinWhere <> "", " AND " & FiltroSinWhere, "")

        '    frm.ConfiguraGrid(StrSql, ConStr, TipoConexion.TcSQL, "Traspasos Pendientes", "CabMovimientos", _
        '    "NOMOVIMIENTO", "No Mov", 60, TipoDato.TdNumerico,
        '    "FECHAMOVIMIENTO", "Fecha", 100, TipoDato.TdFechaHora,
        '    "CIDALMACEN_SAL", "Alm. Sal.", 50, TipoDato.TdNumerico,
        '    "NOMALMSAL", "Almacen Salida", 150, TipoDato.TdCadena,
        '    "CIDALMACEN_ENT", "Alm. Ent.", 50, TipoDato.TdNumerico,
        '    "NOMALMENT", "Almacen Entrada", 150, TipoDato.TdCadena, , "Campo2")

        'ElseIf UCase(Tabla) = UCase("CABMOVIMIENTOSFAL") Then
        '    '13/FEB/2016

        '    StrSql = "SELECT DISTINCT CAB.NOMOVIMIENTO, CAB.FECHAMOVIMIENTO, " & _
        '    "CAB.CIDALMACEN_SAL, ALMSAL.CNOMBREA01 AS NOMALMSAL, " & _
        '    "CAB.CIDALMACEN_ENT, ALMENT.CNOMBREA01 AS NOMALMENT, " & _
        '    "CAB.CREFEREN01, CAB.CTEXTOEXTRA1 " & _
        '    "FROM CABMOVIMIENTOS CAB " & _
        '    "INNER JOIN CATALMACENES ALMSAL ON CAB.CIDALMACEN_SAL = ALMSAL.CCODIGOA01 " & _
        '    "INNER JOIN CATALMACENES ALMENT ON CAB.CIDALMACEN_ENT = ALMENT.CCODIGOA01 " & _
        '    IIf(FiltroSinWhere <> "", " WHERE " & FiltroSinWhere, "")

        '    frm.ConfiguraGrid(StrSql, ConStr, TipoConexion.TcSQL, "Traspasos Pendientes", "CabMovimientos", _
        '"NOMOVIMIENTO", "No Mov", 60, TipoDato.TdNumerico,
        '"FECHAMOVIMIENTO", "Fecha", 100, TipoDato.TdFechaHora,
        '"CIDALMACEN_ENT", "Alm. Ent.", 50, TipoDato.TdNumerico,
        '"NOMALMENT", "Almacen Entrada", 150, TipoDato.TdCadena,
        '"CREFEREN01", "Referencia", 50, TipoDato.TdCadena,
        '"CTEXTOEXTRA1", "Texto", 150, TipoDato.TdCadena, , "Campo2")

        'ElseIf UCase(Tabla) = UCase("CatClientes") Then
        '    '

        '    StrSql = "SELECT CLI.IDCLIENTE, ISNULL(CLI.NOMBRE,'') + ' ' + ISNULL(CLI.APELLIDOPAT,'') + ' ' + ISNULL(CLI.APELLIDOMAT,'')  AS NOMBRE, " & _
        '    "CLI.DOMICILIO + ' ' + COL.NOMCOLONIA AS DOMICILIO , CLI.TELEFONO , CLI.REFERENCIA " & _
        '    "FROM CATCLIENTES CLI " & _
        '    "INNER JOIN CATCOLONIAS COL ON CLI.IDCOLONIA = COL.IDCOLONIA"

        '    frm.ConfiguraGrid(StrSql, ConStr, TipoConexion.TcSQL, "Catalogos de Clientes", "Clientes", _
        '    "IDCLIENTE", "Id Cliente", 50, TipoDato.TdCadena,
        '    "NOMBRE", "Nombre", 200, TipoDato.TdCadena,
        '    "DOMICILIO", "Domicilio", 200, TipoDato.TdCadena,
        '    "TELEFONO", "Telefono", 50, TipoDato.TdCadena,
        '    "REFERENCIA", "Referencia", 100, TipoDato.TdCadena, , , , , , "Campo2")

        'ElseIf UCase(Tabla) = UCase("CatDivisiones") Then
        '    StrSql = "select IdDivision, NomDivision from CatDivisiones"

        '    frm.ConfiguraGrid(StrSql, ConStr, TipoConexion.TcSQL, "Catalogos de Divisiones", "CatDivisiones", _
        '    "IdDivision", "División", 80, TipoDato.TdCadena, _
        '    "NomDivision", "Nombre División", 200, TipoDato.TdCadena)


        '    'MOD-GRUPO-DEFAULT (12/MARZO/2014)
        'ElseIf UCase(Tabla) = UCase("ListaPrecios") Then
        '    StrSql = "SELECT LIS.CCODIGOP01_PRO, ISNULL(PROD.CNOMBREP01,'') AS CNOMBREP01  ,LIS.PRECIO " & _
        '    "FROM LISTAPRECIOS LIS " & _
        '    "LEFT JOIN CATPRODUCTOS PROD ON LIS.CCODIGOP01_PRO = PROD.CCODIGOP01 " & _
        '    "WHERE LIS.CCODIGOC01_CLI = '" & ValorEspecial & "'"


        '    frm.ConfiguraGrid(StrSql, ConStr, TipoConexion.TcSQL, "Catalogos de Productos", "Productos", _
        '   "CCODIGOP01_PRO", "Código", 80, TipoDato.TdCadena, _
        '   "CNOMBREP01", "Nombre", 200, TipoDato.TdCadena, _
        '   "PRECIO", "Precio", 80, TipoDato.TdNumerico, , , , , , , , , , , , , , "Campo2")

        'ElseIf UCase(Tabla) = UCase("Almacenes") Then
        '    StrSql = "SELECT CIDALMACEN, CCODIGOA01, ALLTRIM(CNOMBREA01) AS CNOMBREA01 FROM MGW10003 where CIDALMACEN > 0"
        '    frm.ConfiguraGrid(StrSql, ConStr, TipoConexion.TcFox, "Catalogos de Almacenes", "Almacenes", _
        '   "CCODIGOA01", "Código", 150, TipoDato.TdCadena, _
        '   "CNOMBREA01", "Nombre", 250, TipoDato.TdCadena, , , , , , , , , , , , , , , , , , "Campo2")

        'ElseIf UCase(Tabla) = UCase("Unidades") Then
        '    StrSql = "SELECT PRO.CIDUNIDA01 AS CIDUNIDA01, UNI.CNOMBREU01 , UNI.CABREVIA01, 0 AS  NOEQUIVALENTE, 1 AS UNIDADBASE, 1 AS EQUIVALENCIA " & _
        '    "FROM MGW10026 UNI " & _
        '    "INNER JOIN MGW10005 PRO ON UNI.CIDUNIDAD = PRO.CIDUNIDA01 " & _
        '    "WHERE " & FiltroSinWhere & _
        '    "UNION " & _
        '    "SELECT PRO.CIDUNIDA02 AS CIDUNIDA01, UNI.CNOMBREU01 , UNI.CABREVIA01 , 1 AS  NOEQUIVALENTE, 0 AS UNIDADBASE, 0 AS EQUIVALENCIA " & _
        '    "FROM MGW10026 UNI " & _
        '    "INNER JOIN MGW10005 PRO ON UNI.CIDUNIDAD = PRO.CIDUNIDA02 " & _
        '    "WHERE pro.cidunida02 > 0  and " & FiltroSinWhere & _
        '    "UNION " & _
        '    "SELECT UNI.CIDUNIDAD1  AS CIDUNIDA01, CATUNI.CNOMBREU01 , CATUNI.CABREVIA01 , 0 AS  NOEQUIVALENTE, 0 AS UNIDADBASE, UNI.CFACTORC01  AS EQUIVALENCIA " & _
        '    "FROM MGW10027 UNI " & _
        '    "INNER JOIN MGW10005 PRO ON UNI.CIDUNIDAD2 = PRO.CIDUNIDA01 " & _
        '    "INNER JOIN MGW10026 CATUNI ON  UNI.CIDUNIDAD1 = CATUNI.CIDUNIDAD " & _
        '    "WHERE " & FiltroSinWhere & _
        '    " ORDER BY UNIDADBASE DESC"

        '    'frm.ConfiguraGrid(StrSql, ConStr, TipoConexion.TcFox, "Catalogos de Unidades", "Unidades", _
        '    '"CABREVIA01", "Abreviación", 80, TipoDato.TdCadena,
        '    '"CNOMBREU01", "Nombre", 200, TipoDato.TdCadena, _
        '    '"CIDUNIDA01", "Código", 80, TipoDato.TdNumerico)

        '    frm.ConfiguraGrid(StrSql, ConStr, TipoConexion.TcFox, "Catalogos de Unidades", "Unidades", _
        '    "CABREVIA01", "Abreviación", 80, TipoDato.TdCadena,
        '    "CNOMBREU01", "Nombre", 200, TipoDato.TdCadena, , , , , , , , , , , , , , , , , , "Campo2")


        'ElseIf UCase(Tabla) = UCase("CABMOVIMIENTOS") Then


        '    StrSql = "SELECT CAB.NOMOVIMIENTO, CAB.TIPOMOVIMIENTO , CLI.CRAZONSO01, isnull(AGE.CNOMBREA01,'') as CNOMBREA01 ,CAB.FECHAMOVIMIENTO " & _
        '    "FROM CABMOVIMIENTOS CAB " & _
        '    "INNER JOIN CATCLIENTES CLI ON CAB.CCODIGOC01_CLI = CLI.CCODIGOC01 " & _
        '    "left JOIN CATAGENTES AGE ON CAB.CCODIGOA01 = AGE.CCODIGOA01 " & _
        '    " WHERE CAB.ESTATUS = 'CAP'"

        '    frm.ConfiguraGrid(StrSql, ConStr, TipoConexion.TcSQL, "Documentos Pendientes", "CABMOVIMIENTOS", _
        '   "NOMOVIMIENTO", "No. Pre Documento", 60, TipoDato.TdNumerico, _
        '   "TIPOMOVIMIENTO", "Movimiento", 60, TipoDato.TdCadena, _
        '   "CRAZONSO01", "Cliente", 200, TipoDato.TdCadena, _
        '   "CNOMBREA01", "Agente", 100, TipoDato.TdCadena, _
        '   "FECHAMOVIMIENTO", "Fecha", 80, TipoDato.TdFechaHora)

        'ElseIf UCase(Tabla) = UCase("Grupos") Then
        '    StrSql = "select CveGrupo,NombreGrupo, 1 as EsGRupo from CatGrupos"

        '    frm.ConfiguraGrid(StrSql, ConStr, TipoConexion.TcSQL, "Catalogos de Grupos", "Grupos", _
        '  "CveGrupo", "Grupo", 150, TipoDato.TdCadena, _
        '  "NombreGrupo", "Nombre", 250, TipoDato.TdCadena)

        'ElseIf UCase(Tabla) = UCase("Agentes") Then
        '    StrSql = "select ALLTRIM(CCODIGOA01) as CCODIGOA01, ALLTRIM(CNOMBREA01) as CNOMBREA01 from MGW10001 " & IIf(FiltroSinWhere <> "", " WHERE " & FiltroSinWhere, "")

        '    frm.ConfiguraGrid(StrSql, ConStr, TipoConexion.TcFox, "Catalogo de Agentes", "Agentes", _
        '   "CCODIGOA01", "Id Agente", 150, TipoDato.TdCadena, _
        '   "CNOMBREA01", "CNOMBREA01", 250, TipoDato.TdCadena, , , , , , , , , , , , , , , , , , "Campo2")

        'ElseIf UCase(Tabla) = UCase("Clientes") Then
        '    StrSql = "select CCODIGOC01,CIDCLIEN01,CRAZONSO01, 0 as EsGRupo from MGW10002 " & IIf(FiltroSinWhere <> "", " WHERE " & FiltroSinWhere, "")


        '    'frm.ConfiguraGrid(StrSql, ConStr, TipoConexion.TcFox, "Catalogos de Clientes", "Clientes", _
        '    '"CCODIGOC01", "Id Cliente", 80, TipoDato.TdNumerico, _
        '    '"CIDCLIEN01", "Codigo", 80, TipoDato.TdNumerico, _
        '    '"CRAZONSO01", "Nombre", 200, TipoDato.TdCadena)

        '    '3/ABR/2014
        '    frm.ConfiguraGrid(StrSql, ConStr, TipoConexion.TcFox, "Catalogos de Clientes", "Clientes", _
        '     "CCODIGOC01", "Id Cliente", 150, TipoDato.TdCadena,
        '    "CRAZONSO01", "Nombre", 250, TipoDato.TdCadena, , , , , , , , , , , , , , , , , , "Campo2")



        'ElseIf UCase(Tabla) = UCase("Ventas") Then
        '    'StrSql = "SELECT (alltrim(CAB.CSERIEDO01) +  alltrim(str (CAB.CFOLIO))) as FOLIO,CAB.CFECHA, " & _
        '    '"CLI.CCODIGOC01,CAB.CRAZONSO01,CAB.CNETO, VEND.CNOMBREA01 " & _
        '    '"FROM MGW10008.DBF CAB " & _
        '    '"LEFT JOIN MGW10001.DBF VEND ON CAB.CIDAGENTE = VEND.CIDAGENTE " & _
        '    '"LEFT JOIN MGW10002.DBF CLI ON CAB.CIDCLIEN01 = CLI.CIDCLIEN01 " & _
        '    '"WHERE (CAB.CIDDOCUM02 = 3 OR CAB.CIDDOCUM02 = 4) " & IIf(FiltroSinWhere <> "", " AND " & FiltroSinWhere, "")

        '    StrSql = "SELECT (alltrim(CAB.CSERIEDO01) +  alltrim(str (CAB.CFOLIO))) as FOLIO,CAB.CFECHA, " & _
        '    "CLI.CCODIGOC01,CAB.CRAZONSO01,CAB.CNETO, VEND.CNOMBREA01 " & _
        '    "FROM MGW10008.DBF CAB " & _
        '    "LEFT JOIN MGW10001.DBF VEND ON CAB.CIDAGENTE = VEND.CIDAGENTE " & _
        '    "LEFT JOIN MGW10002.DBF CLI ON CAB.CIDCLIEN01 = CLI.CIDCLIEN01 " & _
        '     IIf(FiltroSinWhere <> "", " WHERE " & FiltroSinWhere, "")


        '    frm.ConfiguraGrid(StrSql, ConStr, TipoConexion.TcFox, "Ventas del Cliente", "Ventas", _
        '    "FOLIO", "Folio Venta", 80, TipoDato.TdCadena, _
        '    "CFECHA", "Fecha Venta", 80, TipoDato.TdFecha, _
        '    "CCODIGOC01", "Codigo", 80, TipoDato.TdCadena, _
        '    "CRAZONSO01", "Nombre", 200, TipoDato.TdCadena, _
        '    "CNOMBREA01", "Vendedor", 80, TipoDato.TdCadena, _
        '    "CNETO", "Importe Venta", 80, TipoDato.TdNumerico)

        '    'ElseIf UCase(Tabla) = UCase("CatTarjetas") Then
        '    '    StrSql = "SELECT TAR.IDTARJETA, PRO.DESCRIPCION ,TAR.CCODIGOC01,TAR.CRAZONSO01, TAR.DIRECCION, TAR.EMAIL " & _
        '    '    "FROM CATTARJETAS TAR " & _
        '    '    "INNER JOIN CATPROMOCIONES PRO ON TAR.IDPROMOCION = PRO.IDPROMOCION"

        '    '    frm.ConfiguraGrid(StrSql, ConStr, TipoConexion.TcSQL, "Ventas del Cliente", "Ventas", _
        '    '    "IDTARJETA", "Id Tarjeta", 60, TipoDato.TdCadena, _
        '    '    "DESCRIPCION", "Promocion", 120, TipoDato.TdCadena, _
        '    '    "CCODIGOC01", "Cliente", 60, TipoDato.TdCadena, _
        '    '    "CRAZONSO01", "Nombre", 150, TipoDato.TdCadena, _
        '    '    "DIRECCION", "Dirección", 200, TipoDato.TdCadena, _
        '    '    "EMAIL", "Correo Electronico", 120, TipoDato.TdCadena)


        '    'ElseIf UCase(Tabla) = UCase("puntos") Then

        '    '    StrSql = "select pun.FolioMovimiento, pun.IdTarjeta,TAR.CCODIGOC01,TAR.CRAZONSO01,pun.ImportePuntos " & _
        '    '    "from puntos pun " & _
        '    '    "inner join CatTarjetas TAR on PUN.IdTarjeta = TAR.IdTarjeta where " & FiltroSinWhere

        '    '    frm.ConfiguraGrid(StrSql, ConStr, TipoConexion.TcSQL, "Ventas del Cliente", "Ventas", _
        '    '    "FolioMovimiento", "Folio", 60, TipoDato.TdNumerico, _
        '    '    "IDTARJETA", "Id Tarjeta", 60, TipoDato.TdCadena, _
        '    '    "CCODIGOC01", "Cliente", 60, TipoDato.TdCadena, _
        '    '    "CRAZONSO01", "Nombre", 150, TipoDato.TdCadena, _
        '    '    "ImportePuntos", "Importe", 200, TipoDato.TdCadena)

        '    '
        'ElseIf UCase(Tabla) = UCase("Conceptos") Then
        '    'StrSql = "select CIDCONCE01 as CIDCONCE01, ALLTRIM(CNOMBREC01) as CNOMBREC01 from MGW10006 " & IIf(FiltroSinWhere <> "", " WHERE " & FiltroSinWhere, "")
        '    StrSql = "select CCODIGOC01 as CCODIGOC01, ALLTRIM(CNOMBREC01) as CNOMBREC01 from MGW10006 " & IIf(FiltroSinWhere <> "", " WHERE " & FiltroSinWhere, "")
        '    'CCODIGOC01

        '    'SELECT , FROM  

        '    frm.ConfiguraGrid(StrSql, ConStr, TipoConexion.TcFox, "Catalogo de Conceptos", "Conceptos", _
        '   "CCODIGOC01", "Concepto", 150, TipoDato.TdCadena, _
        '   "CNOMBREC01", "Nombre", 250, TipoDato.TdCadena)
        'End If

        frm.ShowDialog()
        frm.Dispose()
        Try
            If Not IsDBNull(frm.grid.Item(frm.grid.CurrentCell)) Then
                Return Trim(frm.Valor)
            End If
        Catch Ex As Exception
        End Try
    End Function

    Public Function SeleccionaCarpeta() As String
        SeleccionaCarpeta = ""
        Try
            ' Configuración del FolderBrowserDialog  
            Dim FolderName As New FolderBrowserDialog

            With FolderName

                .Reset() ' resetea  

                ' leyenda  
                .Description = " Seleccionar una carpeta "
                ' Path " Mis documentos "  
                .SelectedPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)

                ' deshabilita el botón " crear nueva carpeta "  
                .ShowNewFolderButton = False
                '.RootFolder = Environment.SpecialFolder.Desktop  
                '.RootFolder = Environment.SpecialFolder.StartMenu  

                Dim ret As DialogResult = .ShowDialog ' abre el diálogo  

                ' si se presionó el botón aceptar ...  
                If ret = Windows.Forms.DialogResult.OK Then

                    Return .SelectedPath

                End If


                .Dispose()

            End With
        Catch oe As Exception
            MsgBox(oe.Message, MsgBoxStyle.Critical)
        End Try
    End Function

    Public Sub GrabaRegistroLOCALMACHINE(ByVal NombreClaveSubkey As String, ByVal NomLlave As String, ByVal ValorLLave As String, ByVal KeyPath As String)
        Dim ObjReg As New ToolRegistro

        ObjReg.CreaSubKey(Process.GetCurrentProcess.ProcessName)
        ObjReg.CreaSubSubKey(Process.GetCurrentProcess.ProcessName, NombreClaveSubkey)
        ObjReg.CreaValoresSubKey(Process.GetCurrentProcess.ProcessName, NombreClaveSubkey, NomLlave, ValorLLave, KeyPath)
    End Sub
#End Region 'FUNCIONES iniciales

#Region "Funciones"

    Public Function LlenaCombos(ByVal CampoClave As String, ByVal CampoDescripcion As String, ByVal dbTabla As String, ByVal TipoCon As TipoConexion, _
                               ByVal Constr As String, ByVal NomAplicacion As String, ByVal sErrorLlave As String, ByVal sValorLlave As String, _
                               Optional ByVal FiltroSinWhere As String = "", Optional ByVal strSqlOpc As String = "", Optional ByVal CampoOrden As String = "") As DataSet
        Dim strsql As String = ""
        Dim TipoConex As String = ""
        Dim obj As New CapaNegocio.Tablas
        LlenaCombos = Nothing
        Try
            If strSqlOpc <> "" Then
                strsql = strSqlOpc
            Else
                strsql = "select isnull(" & CampoClave & ",'') as " & CampoClave & ", " & CampoDescripcion & " from " & dbTabla & IIf(FiltroSinWhere = "", "", " where " & FiltroSinWhere) & " order by " & IIf(CampoOrden = "", CampoClave, CampoOrden)
            End If

            TipoConex = DeterminaNombreConexion(TipoCon)

            DS = obj.CargaDsPorSentencia(Constr, TipoConex, strsql, dbTabla, NomAplicacion, sErrorLlave, sValorLlave)
            'DS = obj.CargaDsPorSentencia(vConStr, TipoCon, vstrSql, vNOM_TABLA, Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS)

            If Not DS Is Nothing Then
                'ChecaClave = False
                If DS.Tables(dbTabla).DefaultView.Count > 0 Then
                    LlenaCombos = DS
                End If
            Else
                LlenaCombos = Nothing
            End If
            If Not obj Is Nothing Then obj = Nothing
        Catch ex As Exception
            If Not obj Is Nothing Then obj = Nothing
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, dbTabla)
            LlenaCombos = Nothing
        End Try

    End Function

    Public Function DeterminaNombreTipoOrden(ByVal TipoOrden As TipoOrdenamiento) As String
        DeterminaNombreTipoOrden = ""
        If CType(TipoOrden, TipoOrdenamiento) = TipoOrdenamiento.Asc Then
            DeterminaNombreTipoOrden = "Ascendente"
        ElseIf CType(TipoOrden, TipoOrdenamiento) = TipoOrdenamiento.Desc Then
            DeterminaNombreTipoOrden = "Descendente"
        End If
    End Function

    Public Function DeterminaNombreSentencia(ByVal Tipo As TipoSentencia) As String
        DeterminaNombreSentencia = ""
        If CType(Tipo, TipoSentencia) = TipoSentencia.TsEliminar Then
            DeterminaNombreSentencia = "DEL"
        ElseIf CType(Tipo, TipoSentencia) = TipoSentencia.TsInsertar Then
            DeterminaNombreSentencia = "INS"
        ElseIf CType(Tipo, TipoSentencia) = TipoSentencia.TsModificar Then
            DeterminaNombreSentencia = "MOD"
        End If
    End Function
#End Region '"Funciones"

    Public Function NoNull(ByVal ArVar As Object, ByVal arTipo As String) As Object
        Try
            'para columnas vacias sin datos
            If ArVar.Equals(System.DBNull.Value) Then
                Select Case arTipo
                    Case "A"
                        NoNull = ""
                    Case "N"
                        NoNull = 0
                    Case "D"
                        NoNull = 0
                    Case "F"
                        NoNull = CDate("00:00:0000")
                    Case "DT"
                        NoNull = New DateTime(1, 1, 1)
                    Case Else
                        NoNull = " "
                End Select
                Exit Function
            End If

            If Len(ArVar) > 0 Then
                Select Case arTipo
                    Case "A"
                        NoNull = ArVar
                    Case "N"
                        NoNull = Val(ArVar)
                    Case "D"
                        NoNull = CDec(ArVar)
                    Case "F"
                        If ArVar = "00/00/0000" Then
                            NoNull = ""
                        Else
                            If InStr(ArVar, "/") > 0 Then
                                NoNull = ArVar
                            Else
                                NoNull = Format(ArVar, "00/00/0000")
                            End If
                        End If
                    Case Else
                        NoNull = ArVar
                End Select
            Else
                Select Case arTipo
                    Case "A"
                        NoNull = ""
                    Case "N"
                        NoNull = 0
                    Case "D"
                        NoNull = 0
                    Case "F"
                        NoNull = CDate("00:00:0000")
                    Case Else
                        NoNull = " "
                End Select
            End If
        Catch ex As Exception
            Select Case arTipo
                Case "A"
                    NoNull = ""
                Case "N"
                    NoNull = 0
                Case "D"
                    NoNull = 0
                Case "F"
                    NoNull = CDate("00:00:0000")
                Case Else
                    NoNull = " "
            End Select
        End Try
    End Function

    Public Structure MiStopWath
        Private _StW As Stopwatch
        Private totalSec As Double
        Private totalMin As Double
        Public Sub StartNew()
            _StW = Stopwatch.StartNew
        End Sub
        Public Sub Stopp()
            If IsNothing(_StW) Then StartNew()
            _StW.Reset() 'Deja en Cero y detiene
        End Sub
        Public Sub Pause()
            If IsNothing(_StW) Then StartNew()
            _StW.Stop() 'Solo lo detiene pause
        End Sub
        Public Sub Reanuda()
            If IsNothing(_StW) Then StartNew()
            _StW.Start()
        End Sub
        Public Function GetTiempo(Optional ByVal VDetener As Boolean = True) As String
            Try
                If Not IsNothing(_StW) Then
                    If _StW.IsRunning Then _StW.Stop()
                    totalSec = _StW.ElapsedMilliseconds / 1000
                    totalMin = Math.Truncate(totalSec / 60)
                    totalSec = totalSec - totalMin * 60
                    If totalMin > 0 Then
                        Return String.Format("<En: {0:#,##0.##} min {1:#,##0.0000} seg>", totalMin, totalSec)
                    Else
                        Return String.Format("<En: {1:#,##0.0000} seg>", totalMin, totalSec)
                    End If
                Else
                    Return ""
                End If
            Catch ex As Exception
                Return ex.Message
            End Try
        End Function
    End Structure

    Public Function IntervaloHoras(ByVal vFecha1 As Date, ByVal vFecha2 As Date) As String
        Dim rsInt As String = ""
        Dim SegTot As Integer, Min As Integer, Hor As Integer, Dias As Integer
        SegTot = DateDiff(DateInterval.Second, vFecha1, vFecha2)
        Dias = Math.Truncate(SegTot / 86400)
        SegTot -= Dias * 86400
        Hor = Math.Truncate(SegTot / 3600)
        SegTot -= Hor * 3600
        Min = Math.Truncate(SegTot / 60)
        SegTot -= Min * 60
        If Dias = 1 Then
            rsInt += Dias & " Dia "
        ElseIf Dias <> 0 Then
            rsInt += Dias & " Dias "
        End If
        If Hor > 0 Then
            rsInt += Hor & " H. "
        End If
        If Min > 0 Or rsInt = "" Then
            rsInt += Min & " Min. "
        End If
        If SegTot > 0 Or rsInt = "" Then
            rsInt += SegTot & " Seg."
        End If

        Return rsInt
    End Function
    Public Function IntervaloTiempoCad(ByVal vFecha As Date) As String
        Dim CadFin As String = ""
        Dim NAnio As Long = 0, NMes As Long = 0, NSem As Long = 0, NDias As Long = 0
        Dim FechaTemp1 As Date 'Tiene que ser menor ya que este ira incrementando para igualar a la fecha2
        Dim FechaTemp2 As Date
        If vFecha.ToString("yyyy/MM/dd") = "0001/01/01" Then Return vFecha.ToString("HH:mm:ss tt")
        If vFecha.Date < Now.Date Then
            If vFecha.Date = DateAdd(DateInterval.Day, -1, Now.Date) Then
                Return " AYER"
            End If
            CadFin = " <-Hace "
            FechaTemp1 = vFecha.Date
            FechaTemp2 = Now.Date
        ElseIf vFecha.Date = Now.Date Then
            Return " Es HOY"
        Else
            If vFecha.Date = DateAdd(DateInterval.Day, 1, Now.Date) Then
                Return " MAÑANA"
            End If
            CadFin = " ->Faltan "
            FechaTemp1 = FormatDateTime(Now, DateFormat.ShortDate)
            FechaTemp2 = FormatDateTime(vFecha, DateFormat.ShortDate)
        End If
        NAnio = DateDiff(DateInterval.Year, FechaTemp1, FechaTemp2)
        If DateAdd(DateInterval.Year, NAnio, FechaTemp1) > FechaTemp2 Then
            NAnio -= 1
        End If
        FechaTemp1 = DateAdd(DateInterval.Year, NAnio, FechaTemp1)

        NMes = DateDiff(DateInterval.Month, FechaTemp1, FechaTemp2)
        If DateAdd(DateInterval.Month, NMes, FechaTemp1) > FechaTemp2 Then
            NMes -= 1
        End If
        'CadFin = "Meses: " & NMes
        FechaTemp1 = DateAdd(DateInterval.Month, NMes, FechaTemp1)
        NSem = DateDiff(DateInterval.Weekday, FechaTemp1, FechaTemp2)
        'CadFin += " Semanas: " & NSem
        FechaTemp1 = DateAdd(DateInterval.Day, 7 * NSem, FechaTemp1)
        NDias = DateDiff(DateInterval.Day, FechaTemp1, FechaTemp2)
        'CadFin += " Dias: " & NDias
        If NAnio > 0 Then
            If NAnio = 1 Then
                CadFin += NAnio & " Año, "
            Else
                CadFin += NAnio & " Años, "
            End If
        End If
        If NMes > 0 Then
            If NMes = 1 Then
                CadFin += NMes & " Mes, "
            Else
                CadFin += NMes & " Meses, "
            End If
        End If
        If NSem > 0 Then
            If NSem = 1 Then
                CadFin += NSem & " Semana, "
            Else
                CadFin += NSem & " Semanas, "
            End If
        End If
        If NDias > 0 Then
            If NDias = 1 Then
                CadFin += NDias & " Dia,"
            Else
                CadFin += NDias & " Dias,"
            End If
        End If
        Return Mid(CadFin.Trim, 1, Len(CadFin.Trim) - 1)
    End Function

    Public Function ValidaUbicacionUnidad(ByVal ubicacion As Integer) As Boolean
        Dim opc As opcEstatusUbiUni
        Dim Resp As Boolean
        opc = CType(ubicacion, opcEstatusUbiUni)
        If opc = opcEstatusUbiUni.DESCONOCIDO Or opc = opcEstatusUbiUni.BASE Then
            'Si entra
            Resp = True
        Else
            'No entra
            Resp = False
        End If
        Return Resp
    End Function
End Module


