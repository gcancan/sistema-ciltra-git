﻿Public Class ProductosComClass
    'CIDPRODUCTO	    vCIDPRODUCTO	    _CIDPRODUCTO
    'CCODIGOPRODUCTO	vCCODIGOPRODUCTO	_CCODIGOPRODUCTO
    'CNOMBREPRODUCTO	vCNOMBREPRODUCTO	_CNOMBREPRODUCTO
    'CTIPOPRODUCTO	    vCTIPOPRODUCTO	    _CTIPOPRODUCTO
    'CSTATUSPRODUCTO	vCSTATUSPRODUCTO	_CSTATUSPRODUCTO
    'CIDUNIDADBASE	    vCIDUNIDADBASE	    _CIDUNIDADBASE
    'CCODALTERN	        vCCODALTERN	        _CCODALTERN

    Private _CIDPRODUCTO As Integer
    Private _CCODIGOPRODUCTO As String
    Private _CNOMBREPRODUCTO As String
    Private _CTIPOPRODUCTO As Integer
    Private _CSTATUSPRODUCTO As Integer
    Private _CIDUNIDADBASE As Integer
    Private _CCODALTERN As String
    Private _CPRECIO1 As Decimal




    Private _Existe As Boolean
    Private _ExisteLOC As Boolean
    Private _ExisteLOCServ As Boolean
    Private _TablaBd As String = "admProductos"
    Private _TablaBdLOC As String = "CatProductosCOM"
    Private _TablaBdLOCServ As String = "CatServFact"


    Dim StrSql As String
    Dim DsClase As New DataTable
    Dim MyTabla As DataTable = New DataTable(_TablaBd)

#Region "Constructor"
    Sub New(ByVal vCCODIGOPRODUCTO As String, ByVal CNOMBREPRODUCTO As String)
        _CCODIGOPRODUCTO = vCCODIGOPRODUCTO

        Dim filtro As String = ""

        If vCCODIGOPRODUCTO <> "" Then
            filtro = "where PRO.CCODIGOPRODUCTO = '" & vCCODIGOPRODUCTO & "'"
        ElseIf CNOMBREPRODUCTO <> "" Then
            filtro = "where PRO.CNOMBREPRODUCTO = '" & CNOMBREPRODUCTO & "'"
        End If

        StrSql = "select PRO.CCODIGOPRODUCTO, " &
        "PRO.CNOMBREPRODUCTO, " &
        "PRO.CIDUNIDADBASE, " &
        "PRO.CIDPRODUCTO, " &
        "PRO.CSTATUSPRODUCTO, " &
        "PRO.CCODALTERN, " &
        "PRO.CTIPOPRODUCTO, " &
        "PRO.CPRECIO1 " &
        "from " & _TablaBd & " PRO " &
       filtro

        DsClase = BDCOM.ExecuteReturn(StrSql)
        If DsClase.Rows.Count > 0 Then
            With DsClase.Rows(0)
                _CIDPRODUCTO = .Item("CIDPRODUCTO")
                _CNOMBREPRODUCTO = .Item("CNOMBREPRODUCTO")
                _CTIPOPRODUCTO = .Item("CTIPOPRODUCTO")
                _CSTATUSPRODUCTO = .Item("CSTATUSPRODUCTO")
                _CIDUNIDADBASE = .Item("CIDUNIDADBASE")
                _CCODALTERN = .Item("CCODALTERN")
                _CPRECIO1 = .Item("CPRECIO1")
            End With
            _Existe = True
        Else
            _Existe = False
        End If

        If _Existe Then
            StrSql = "select * from CatProductosCOM " & _
            "where CCODIGOPRODUCTO = '" & vCCODIGOPRODUCTO & "'"
            DsClase = BD.ExecuteReturn(StrSql)
            If DsClase.Rows.Count > 0 Then
                _ExisteLOC = True
            Else
                _ExisteLOC = False
            End If

            StrSql = "select * from " & _TablaBdLOCServ & _
          " where CCODIGOPRODUCTO = '" & vCCODIGOPRODUCTO & "'"
            DsClase = BD.ExecuteReturn(StrSql)
            If DsClase.Rows.Count > 0 Then
                _ExisteLOCServ = True
            Else
                _ExisteLOCServ = False
            End If

        End If
    End Sub
    Sub New(ByVal vCIDPRODUCTO As Integer, ByVal vCCODIGOPRODUCTO As String, ByVal vCNOMBREPRODUCTO As String,
    ByVal vCTIPOPRODUCTO As Integer, ByVal vCSTATUSPRODUCTO As Integer, ByVal vCIDUNIDADBASE As Integer, ByVal vCCODALTERN As String)
        _CIDPRODUCTO = vCIDPRODUCTO
        _CCODIGOPRODUCTO = vCCODIGOPRODUCTO
        _CNOMBREPRODUCTO = vCNOMBREPRODUCTO
        _CTIPOPRODUCTO = vCTIPOPRODUCTO
        _CSTATUSPRODUCTO = vCSTATUSPRODUCTO
        _CIDUNIDADBASE = vCIDUNIDADBASE
        _CCODALTERN = vCCODALTERN
        _Existe = True
    End Sub
#End Region

#Region "Propiedades"
    Public Property CIDPRODUCTO As Integer
        Get
            Return _CIDPRODUCTO
        End Get
        Set(ByVal value As Integer)
            _CIDPRODUCTO = value
        End Set
    End Property
    Public Property CCODIGOPRODUCTO As String
        Get
            Return _CCODIGOPRODUCTO
        End Get
        Set(ByVal value As String)
            _CCODIGOPRODUCTO = value
        End Set
    End Property
    Public Property CNOMBREPRODUCTO As String
        Get
            Return _CNOMBREPRODUCTO
        End Get
        Set(ByVal value As String)
            _CNOMBREPRODUCTO = value
        End Set
    End Property
    Public Property CTIPOPRODUCTO As Integer
        Get
            Return _CTIPOPRODUCTO
        End Get
        Set(ByVal value As Integer)
            _CTIPOPRODUCTO = value
        End Set
    End Property
    Public Property CSTATUSPRODUCTO As Integer
        Get
            Return _CSTATUSPRODUCTO
        End Get
        Set(ByVal value As Integer)
            _CSTATUSPRODUCTO = value
        End Set
    End Property
    Public Property CIDUNIDADBASE As Integer
        Get
            Return _CIDUNIDADBASE
        End Get
        Set(ByVal value As Integer)
            _CIDUNIDADBASE = value
        End Set
    End Property
    Public Property CCODALTERN As String
        Get
            Return _CCODALTERN
        End Get
        Set(ByVal value As String)
            _CCODALTERN = value
        End Set
    End Property

    Public Property CPRECIO1 As Decimal
        Get
            Return _CPRECIO1
        End Get
        Set(ByVal value As Decimal)
            _CPRECIO1 = value
        End Set
    End Property
    Public Property Existe As Boolean
        Get
            Return _Existe
        End Get
        Set(ByVal value As Boolean)
            _Existe = value
        End Set
    End Property
    Public Property ExisteLOC As Boolean
        Get
            Return _ExisteLOC
        End Get
        Set(ByVal value As Boolean)
            _ExisteLOC = value
        End Set
    End Property
    Public Property ExisteLOCServ As Boolean
        Get
            Return _ExisteLOCServ
        End Get
        Set(ByVal value As Boolean)
            _ExisteLOCServ = value
        End Set
    End Property
#End Region

#Region "Funciones"

    'Public Function InsertaProductosCOM()
    '    Dim _CadValAnt As String = "", _CadValNue As String = ""

    '    _CadValAnt += "CCODIGOPRODUCTO =" & _CCODIGOPRODUCTO & ","
    '    _CadValAnt += "CNOMBREPRODUCTO =" & _CNOMBREPRODUCTO & ","
    '    _CadValAnt += "CTIPOPRODUCTO =" & _CTIPOPRODUCTO & ","
    '    _CadValAnt += "CSTATUSPRODUCTO =" & _CSTATUSPRODUCTO & ","
    '    _CadValAnt += "CIDUNIDADBASE =" & _CIDUNIDADBASE & ","
    '    _CadValAnt += "CCODALTERN =" & _CCODALTERN & ","



    '    StrSql = "DECLARE @EXIS AS INT " & _
    '    "SET @EXIS = isnull((select top 1 1 from CatProductosCOM where CIDPRODUCTO = '" & _CIDPRODUCTO & "'),0) " & _
    '    "if @EXIS = 0 " & _
    '    "BEGIN " & _
    '    "insert into CatProductosCOM (CIDPRODUCTO,CCODIGOPRODUCTO, CNOMBREPRODUCTO, CTIPOPRODUCTO, CSTATUSPRODUCTO,CIDUNIDADBASE,CCODALTERN) " & _
    '    "values (" & _CIDPRODUCTO & ",'" & Trim(_CCODIGOPRODUCTO) & _
    '    "','" & Trim(_CNOMBREPRODUCTO) & "'," & _CTIPOPRODUCTO & _
    '    "," & _CSTATUSPRODUCTO & "," & _CIDUNIDADBASE & "," & _CCODALTERN & ") " & _
    '    "end " & _
    '    "else " & _
    '    "begin " & _
    '    "UPDATE CatProductosCOM SET " & _
    '    "CCODIGOPRODUCTO = " & _CCODIGOPRODUCTO & ", " & _
    '    "CNOMBREPRODUCTO = '" & _CNOMBREPRODUCTO & "', " & _
    '    "CTIPOPRODUCTO = " & _CTIPOPRODUCTO & ", " & _
    '    "CSTATUSPRODUCTO = " & _CSTATUSPRODUCTO & ", " & _
    '    "CIDUNIDADBASE = '" & _CIDUNIDADBASE & "', " & _
    '    "CCODALTERN = " & _CCODALTERN & ", " & _
    '    "WHERE CIDPRODUCTO = " & _CIDPRODUCTO & _
    '    "end "
    '    Try
    '        BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
    '        BD.Execute(StrSql, True)
    '        'Audita(_TablaBd, _CadValAnt, _CadValNue, EnTipAccion.Modifica)
    '        BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
    '    Catch ex As Exception
    '        BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
    '        Throw New Exception(ex.Message)
    '    End Try
    'End Function
    Public Function GetCambios(ByVal vCCODIGOPRODUCTO As String, ByVal vCNOMBREPRODUCTO As String, ByVal vCTIPOPRODUCTO As Integer, _
    ByVal vCSTATUSPRODUCTO As Integer, ByVal vCIDUNIDADBASE As Integer, ByVal vCCODALTERN As String) As String
        Dim CadCam As String = ""
        If _CCODIGOPRODUCTO <> vCCODIGOPRODUCTO Then
            CadCam += "CCODIGOPRODUCTO: '" & _CCODIGOPRODUCTO & "' Cambia a [" & vCCODIGOPRODUCTO & "],"
        End If
        If _CNOMBREPRODUCTO <> vCNOMBREPRODUCTO Then
            CadCam += "CNOMBREPRODUCTO: '" & _CNOMBREPRODUCTO & "' Cambia a [" & vCNOMBREPRODUCTO & "],"
        End If
        If _CTIPOPRODUCTO <> vCTIPOPRODUCTO Then
            CadCam += "CTIPOPRODUCTO: '" & _CTIPOPRODUCTO & "' Cambia a [" & vCTIPOPRODUCTO & "],"
        End If
        If _CSTATUSPRODUCTO <> vCSTATUSPRODUCTO Then
            CadCam += "CIDALMACEN_COM: '" & _CSTATUSPRODUCTO & "' Cambia a [" & vCSTATUSPRODUCTO & "],"
        End If
        If _CIDUNIDADBASE <> vCIDUNIDADBASE Then
            CadCam += "CIDUNIDADBASE: '" & _CIDUNIDADBASE & "' Cambia a [" & vCIDUNIDADBASE & "],"
        End If
        If _CCODALTERN <> vCCODALTERN Then
            CadCam += "CCODALTERN: '" & _CCODALTERN & "' Cambia a [" & vCCODALTERN & "],"
        End If

        If CadCam <> "" Then
            CadCam = Mid(CadCam, 1, CadCam.Length - 1)
        End If
        Return CadCam
    End Function

    Public Sub Guardar(ByVal bEsIdentidad As Boolean, ByVal vCCODIGOPRODUCTO As String, ByVal vCNOMBREPRODUCTO As String, ByVal vCTIPOPRODUCTO As Integer,
    ByVal vCSTATUSPRODUCTO As Integer, ByVal vCIDUNIDADBASE As Integer, ByVal vCCODALTERN As String)
        Dim sSql As String = "", _CadValAnt As String = "", _CadValNue As String = ""
        If _ExisteLOC Then
            If _CCODIGOPRODUCTO <> vCCODIGOPRODUCTO Then
                sSql += "CCODIGOPRODUCTO = '" & vCCODIGOPRODUCTO & "',"
                _CadValAnt += "CCODIGOPRODUCTO =" & vCCODIGOPRODUCTO & ","
            End If
            If _CNOMBREPRODUCTO <> vCNOMBREPRODUCTO Then
                sSql += "CNOMBREPRODUCTO = '" & vCNOMBREPRODUCTO & "',"
                _CadValAnt += "CNOMBREPRODUCTO =" & vCNOMBREPRODUCTO & ","
            End If
            If _CTIPOPRODUCTO <> vCTIPOPRODUCTO Then
                sSql += "CTIPOPRODUCTO = " & vCTIPOPRODUCTO & ","
                _CadValAnt += "CTIPOPRODUCTO =" & vCTIPOPRODUCTO & ","
            End If
            If _CSTATUSPRODUCTO <> vCSTATUSPRODUCTO Then
                sSql += "CSTATUSPRODUCTO = " & vCSTATUSPRODUCTO & ","
                _CadValAnt += "CSTATUSPRODUCTO =" & vCSTATUSPRODUCTO & ","
            End If
            If _CIDUNIDADBASE <> vCIDUNIDADBASE Then
                sSql += "CIDUNIDADBASE = " & vCIDUNIDADBASE & ","
                _CadValAnt += "CIDUNIDADBASE =" & vCIDUNIDADBASE & ","
            End If
            If _CCODALTERN <> vCCODALTERN Then
                sSql += "CCODALTERN = '" & vCCODALTERN & "',"
                _CadValAnt += "CCODALTERN =" & vCCODALTERN & ","
            End If

            If sSql <> "" Then
                sSql = Mid(sSql, 1, sSql.Length - 1) 'Para quitar la Coma ','
                _CadValAnt = "CIDPRODUCTO=" & _CIDPRODUCTO & "," & _CadValAnt
                _CadValAnt = Mid(_CadValAnt, 1, _CadValAnt.Length - 1)
                _CadValNue = Replace(sSql, "'", "")

                sSql = "UPDATE " & _TablaBdLOC & " SET " & sSql & " WHERE CIDPRODUCTO = " & _CIDPRODUCTO

                Try
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                    BD.Execute(sSql, True)
                    Audita(_TablaBd, _CadValAnt, _CadValNue, EnTipAccion.Modifica)
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
                Catch ex As Exception
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                    Throw New Exception(ex.Message)
                End Try
            End If
        Else
            If bEsIdentidad Then
                sSql = "INSERT INTO " & _TablaBdLOC & "(CCODIGOPRODUCTO,CNOMBREPRODUCTO,CTIPOPRODUCTO,CSTATUSPRODUCTO,CIDUNIDADBASE,CCODALTERN) " &
                " VALUES('" & vCCODIGOPRODUCTO & "','" & vCNOMBREPRODUCTO & "'," & vCTIPOPRODUCTO & "," & vCSTATUSPRODUCTO & "," & vCIDUNIDADBASE & ",'" & vCCODALTERN & "')"

                _CadValNue = "CCODIGOPRODUCTO=" & vCCODIGOPRODUCTO & "CNOMBREPRODUCTO = " & vCNOMBREPRODUCTO & "CTIPOPRODUCTO = " & vCTIPOPRODUCTO &
                "CTIPOPRODUCTO=" & vCTIPOPRODUCTO & "CSTATUSPRODUCTO = " & vCSTATUSPRODUCTO & "CIDUNIDADBASE = " & vCIDUNIDADBASE & "CCODALTERN = " & vCCODALTERN
            Else
                sSql = "INSERT INTO " & _TablaBdLOC & "(CIDPRODUCTO,CCODIGOPRODUCTO,CNOMBREPRODUCTO,CTIPOPRODUCTO,CSTATUSPRODUCTO,CIDUNIDADBASE,CCODALTERN) " &
                " VALUES(" & _CIDPRODUCTO & ",'" & vCCODIGOPRODUCTO & "','" & vCNOMBREPRODUCTO & "'," & vCTIPOPRODUCTO & "," & vCSTATUSPRODUCTO & "," & vCIDUNIDADBASE & ",'" & vCCODALTERN & "')"

                _CadValNue = "CIDPRODUCTO=" & _CIDPRODUCTO & "CCODIGOPRODUCTO=" & vCCODIGOPRODUCTO & "CNOMBREPRODUCTO = " & vCNOMBREPRODUCTO & "CTIPOPRODUCTO = " & vCTIPOPRODUCTO &
                "CTIPOPRODUCTO=" & vCTIPOPRODUCTO & "CSTATUSPRODUCTO = " & vCSTATUSPRODUCTO & "CIDUNIDADBASE = " & vCIDUNIDADBASE & "CCODALTERN = " & vCCODALTERN

            End If

            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, "", _CadValNue, EnTipAccion.Inserta)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
        End If
        '_CIDPRODUCTO = vCIDPRODUCTO
        _CCODIGOPRODUCTO = vCCODIGOPRODUCTO
        _CNOMBREPRODUCTO = vCNOMBREPRODUCTO
        _CTIPOPRODUCTO = vCTIPOPRODUCTO
        _CSTATUSPRODUCTO = vCSTATUSPRODUCTO
        _CIDUNIDADBASE = vCIDUNIDADBASE
        _CCODALTERN = vCCODALTERN
    End Sub
    Public Function TablaProdServicios(ByVal Filtro As String) As DataTable

        MyTabla.Rows.Clear()

        StrSql = "SELECT CIDPRODUCTO,CCODIGOPRODUCTO,CNOMBREPRODUCTO,CIDUNIDADBASE,CPRECIO1, " &
        "CIMPUESTO1,CIDVALORCLASIFICACION5 FROM admProductos " & IIf(Filtro <> "", " WHERE " & Filtro, "")

        MyTabla = BDCOM.ExecuteReturn(StrSql)

        Return MyTabla

    End Function
#End Region

End Class
