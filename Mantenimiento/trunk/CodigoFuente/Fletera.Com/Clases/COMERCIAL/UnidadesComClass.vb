﻿Public Class UnidadesComClass
    'CIDUNIDAD	    vCIDUNIDAD	    _CIDUNIDAD
    'CNOMBREUNIDAD	vCNOMBREUNIDAD	_CNOMBREUNIDAD
    'CABREVIATURA	vCABREVIATURA	_CABREVIATURA
    'CDESPLIEGUE	vCDESPLIEGUE	_CDESPLIEGUE
    'CCLAVEINT	    vCCLAVEINT	    _CCLAVEINT

    Private _CIDUNIDAD As Integer
    Private _CNOMBREUNIDAD As String
    Private _CABREVIATURA As String
    Private _CDESPLIEGUE As String
    Private _CCLAVEINT As String

    Private _Existe As Boolean
    Private _ExisteLOC As Boolean
    Private _TablaBd As String = "admUnidadesMedidaPeso"
    Private _TablaBdLOC As String = "CatUnidadesCOM"

    Dim StrSql As String
    Dim DsClase As New DataTable
#Region "Constructor"
    Sub New(ByVal vValor As String, ByVal vNomCampo As String, ByVal vTipoDato As TipoDato)
        Dim filtro As String = ""
        If vTipoDato = TipoDato.TdCadena Then
            filtro = "where UNI." & vNomCampo & " = '" & vValor & "'"
        ElseIf vTipoDato = TipoDato.TdNumerico Then
            filtro = "where UNI." & vNomCampo & " = " & vValor
        End If

        StrSql = "SELECT UNI.CIDUNIDAD, " & _
        "UNI.CNOMBREUNIDAD, " & _
        "UNI.CABREVIATURA, " & _
        "UNI.CDESPLIEGUE, " & _
        "UNI.CCLAVEINT " & _
        "from " & _TablaBd & " UNI " & filtro

        DsClase = BDCOM.ExecuteReturn(StrSql)
        If DsClase.Rows.Count > 0 Then
            With DsClase.Rows(0)
                _CIDUNIDAD = .Item("CIDUNIDAD")
                _CNOMBREUNIDAD = .Item("CNOMBREUNIDAD")
                _CABREVIATURA = .Item("CABREVIATURA")
                _CDESPLIEGUE = .Item("CDESPLIEGUE")
                _CCLAVEINT = .Item("CCLAVEINT")
            End With
            _Existe = True


        Else
            _Existe = False
        End If

        If _Existe Then
            StrSql = "select * from CatUnidadesCOM where cidunidad = " & _CIDUNIDAD
            DsClase = BD.ExecuteReturn(StrSql)
            If DsClase.Rows.Count > 0 Then
                _ExisteLOC = True
            Else
                _ExisteLOC = False
            End If

        End If

    End Sub
    Sub New(ByVal vCIDUNIDAD As Integer, ByVal vCNOMBREUNIDAD As String, ByVal vCABREVIATURA As String,
    ByVal vCDESPLIEGUE As String, ByVal vCCLAVEINT As String)
        _CIDUNIDAD = vCIDUNIDAD
        _CNOMBREUNIDAD = vCNOMBREUNIDAD
        _CABREVIATURA = vCABREVIATURA
        _CDESPLIEGUE = vCDESPLIEGUE
        _CCLAVEINT = vCCLAVEINT
        _Existe = True
    End Sub
#End Region
#Region "Propiedades"
    Public Property CIDUNIDAD As Integer
        Get
            Return _CIDUNIDAD
        End Get
        Set(ByVal value As Integer)
            _CIDUNIDAD = value
        End Set
    End Property
    Public Property CNOMBREUNIDAD As String
        Get
            Return _CNOMBREUNIDAD
        End Get
        Set(ByVal value As String)
            _CNOMBREUNIDAD = value
        End Set
    End Property
    Public Property CABREVIATURA As String
        Get
            Return _CABREVIATURA
        End Get
        Set(ByVal value As String)
            _CABREVIATURA = value
        End Set
    End Property
    Public Property CDESPLIEGUE As String
        Get
            Return _CDESPLIEGUE
        End Get
        Set(ByVal value As String)
            _CDESPLIEGUE = value
        End Set
    End Property
    Public Property CCLAVEINT As String
        Get
            Return _CCLAVEINT
        End Get
        Set(ByVal value As String)
            _CCLAVEINT = value
        End Set
    End Property
    Public Property Existe As Boolean
        Get
            Return _Existe
        End Get
        Set(ByVal value As Boolean)
            _Existe = value
        End Set
    End Property
#End Region

    Public Sub Guardar(ByVal bEsIdentidad As Boolean, ByVal vCNOMBREUNIDAD As String, ByVal vCABREVIATURA As String, _
    ByVal vCDESPLIEGUE As String, ByVal vCCLAVEINT As String)
        Dim sSql As String = "", _CadValAnt As String = "", _CadValNue As String = ""
        If _ExisteLOC Then
            If _CNOMBREUNIDAD <> vCNOMBREUNIDAD Then
                sSql += "CNOMBREUNIDAD = '" & vCNOMBREUNIDAD & "',"
                _CadValAnt += "CNOMBREUNIDAD =" & vCNOMBREUNIDAD & ","
            End If
            If _CABREVIATURA <> vCABREVIATURA Then
                sSql += "CABREVIATURA = '" & vCABREVIATURA & "',"
                _CadValAnt += "CABREVIATURA =" & vCABREVIATURA & ","
            End If
            If _CDESPLIEGUE <> vCDESPLIEGUE Then
                sSql += "CDESPLIEGUE = '" & vCDESPLIEGUE & "',"
                _CadValAnt += "CDESPLIEGUE =" & vCDESPLIEGUE & ","
            End If
            If _CCLAVEINT <> vCCLAVEINT Then
                sSql += "CCLAVEINT = '" & vCCLAVEINT & "',"
                _CadValAnt += "CCLAVEINT =" & vCCLAVEINT & ","
            End If

            If sSql <> "" Then
                sSql = Mid(sSql, 1, sSql.Length - 1) 'Para quitar la Coma ','
                _CadValAnt = "CIDUNIDAD=" & _CIDUNIDAD & "," & _CadValAnt
                _CadValAnt = Mid(_CadValAnt, 1, _CadValAnt.Length - 1)
                _CadValNue = Replace(sSql, "'", "")

                sSql = "UPDATE " & _TablaBdLOC & " SET " & sSql & " WHERE CIDUNIDAD = " & _CIDUNIDAD

                Try
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                    BD.Execute(sSql, True)
                    Audita(_TablaBd, _CadValAnt, _CadValNue, EnTipAccion.Modifica)
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
                Catch ex As Exception
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                    Throw New Exception(ex.Message)
                End Try
            End If
        Else
            If bEsIdentidad Then
                sSql = "INSERT INTO " & _TablaBdLOC & "(CNOMBREUNIDAD,CABREVIATURA,CDESPLIEGUE,CCLAVEINT) " & _
                " VALUES('" & vCNOMBREUNIDAD & "','" & vCABREVIATURA & "','" & vCDESPLIEGUE & "','" & vCCLAVEINT & "')"

                _CadValNue = "CNOMBREUNIDAD=" & vCNOMBREUNIDAD & "CABREVIATURA = " & vCABREVIATURA & "CDESPLIEGUE = " & vCDESPLIEGUE & _
                "CCLAVEINT=" & vCCLAVEINT
            Else
                sSql = "INSERT INTO " & _TablaBdLOC & "(CIDUNIDAD,CNOMBREUNIDAD,CABREVIATURA,CDESPLIEGUE,CCLAVEINT) " & _
                " VALUES(" & _CIDUNIDAD & ",'" & vCNOMBREUNIDAD & "','" & vCABREVIATURA & "','" & vCDESPLIEGUE & "','" & vCCLAVEINT & "')"

                _CadValNue = "CIDUNIDAD=" & _CIDUNIDAD & "CNOMBREUNIDAD=" & vCNOMBREUNIDAD & "CABREVIATURA = " & vCABREVIATURA & "CDESPLIEGUE = " & vCDESPLIEGUE & _
                "CCLAVEINT=" & vCCLAVEINT

            End If

            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, "", _CadValNue, EnTipAccion.Inserta)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
        End If
        '_CIDUNIDAD = vCIDUNIDAD
        _CNOMBREUNIDAD = vCNOMBREUNIDAD
        _CABREVIATURA = vCABREVIATURA
        _CDESPLIEGUE = vCDESPLIEGUE
        _CCLAVEINT = vCCLAVEINT
    End Sub
End Class
