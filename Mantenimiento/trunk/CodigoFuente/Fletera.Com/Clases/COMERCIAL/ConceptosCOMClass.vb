﻿Public Class ConceptosCOMClass
    'CIDCONCEPTODOCUMENTO	vCIDCONCEPTODOCUMENTO	_CIDCONCEPTODOCUMENTO
    'CCODIGOCONCEPTO	    vCCODIGOCONCEPTO	    _CCODIGOCONCEPTO
    'CNOMBRECONCEPTO	    vCNOMBRECONCEPTO	    _CNOMBRECONCEPTO
    'CIDDOCUMENTODE	        vCIDDOCUMENTODE	        _CIDDOCUMENTODE
    'CSERIEPOROMISION
    'CNOFOLIO
    'CPLAMIGCFD
    'CRUTAENTREGA
    'CPREFICON

    Private _CIDCONCEPTODOCUMENTO As Integer
    Private _CCODIGOCONCEPTO As String
    Private _CNOMBRECONCEPTO As String
    Private _CIDDOCUMENTODE As Integer
    Private _CDOCTOACREDITO As Boolean
    Private _CSERIEPOROMISION As String
    Private _CNOFOLIO As Integer
    Private _CPLAMIGCFD As String
    Private _CRUTAENTREGA As String
    Private _CPREFICON As String

    Private _Existe As Boolean
    Private _TablaBd As String = "admConceptos"

    Dim StrSql As String
    Dim DsClase As New DataTable

#Region "Constructor"
    Sub New(ByVal vCCODIGOCONCEPTO As String)
        _CCODIGOCONCEPTO = vCCODIGOCONCEPTO

        StrSql = "SELECT con.CIDCONCEPTODOCUMENTO, " & _
        "con.CCODIGOCONCEPTO, " & _
        "con.CNOMBRECONCEPTO, " & _
        "con.CIDDOCUMENTODE, " & _
        "con.CDOCTOACREDITO, " & _
        "con.CSERIEPOROMISION, " & _
        "con.CNOFOLIO, " & _
        "con.CPLAMIGCFD, " & _
        "con.CRUTAENTREGA, " & _
        "con.CPREFIJOCONCEPTO " & _
        "from " & _TablaBd & " con " & _
        "WHERE con.CCODIGOCONCEPTO= '" & vCCODIGOCONCEPTO & "'"

        DsClase = BDCOM.ExecuteReturn(StrSql)
        If DsClase.Rows.Count > 0 Then
            With DsClase.Rows(0)
                _CIDCONCEPTODOCUMENTO = .Item("CIDCONCEPTODOCUMENTO")
                _CCODIGOCONCEPTO = .Item("CCODIGOCONCEPTO")
                _CNOMBRECONCEPTO = Trim(.Item("CNOMBRECONCEPTO"))
                _CIDDOCUMENTODE = .Item("CIDDOCUMENTODE")
                _CDOCTOACREDITO = .Item("CDOCTOACREDITO")
                _CSERIEPOROMISION = .Item("CSERIEPOROMISION")
                _CNOFOLIO = .Item("CNOFOLIO")
                _CPLAMIGCFD = .Item("CPLAMIGCFD")
                _CRUTAENTREGA = .Item("CRUTAENTREGA")
                _CPREFICON = Trim(.Item("CPREFIJOCONCEPTO"))
                _CSERIEPOROMISION = Trim(.Item("CSERIEPOROMISION"))
            End With
            _Existe = True
        Else
            _Existe = False
        End If

        'If _Existe Then
        '    StrSql = "select * from " & _TablaBdLOC & _
        '    "where idcliente = " & CInt(vCCODIGOCLIENTE)
        '    DsClase = BD.ExecuteReturn(StrSql)
        '    If DsClase.Rows.Count > 0 Then
        '        _ExisteLOC = True
        '    Else
        '        _ExisteLOC = False
        '    End If
        'End If
    End Sub
    Sub New(ByVal vCIDCONCEPTODOCUMENTO As Integer, ByVal vCCODIGOCONCEPTO As String, ByVal vCNOMBRECONCEPTO As String,
            ByVal vCIDDOCUMENTODE As Integer, ByVal vCDOCTOACREDITO As Boolean, ByVal vCSERIEPOROMISION As String,
            ByVal vCNOFOLIO As Integer, ByVal vCPLAMIGCFD As String, ByVal vCRUTAENTREGA As String,
            ByVal vCPREFICON As String)
        _CIDCONCEPTODOCUMENTO = vCIDCONCEPTODOCUMENTO
        _CCODIGOCONCEPTO = vCCODIGOCONCEPTO
        _CNOMBRECONCEPTO = vCNOMBRECONCEPTO
        _CIDDOCUMENTODE = vCIDDOCUMENTODE
        _CDOCTOACREDITO = vCDOCTOACREDITO
        _CSERIEPOROMISION = vCSERIEPOROMISION
        _CNOFOLIO = vCNOFOLIO
        _CPLAMIGCFD = vCPLAMIGCFD
        _CRUTAENTREGA = vCRUTAENTREGA
        _CPREFICON = vCPREFICON
        _Existe = True
    End Sub

#End Region
#Region "Propiedades"
    Public Property CIDCONCEPTODOCUMENTO As Integer
        Get
            Return _CIDCONCEPTODOCUMENTO
        End Get
        Set(ByVal value As Integer)
            _CIDCONCEPTODOCUMENTO = value
        End Set
    End Property
    Public Property CCODIGOCONCEPTO As String
        Get
            Return _CCODIGOCONCEPTO
        End Get
        Set(ByVal value As String)
            _CCODIGOCONCEPTO = value
        End Set
    End Property
    Public Property CNOMBRECONCEPTO As String
        Get
            Return _CNOMBRECONCEPTO
        End Get
        Set(ByVal value As String)
            _CNOMBRECONCEPTO = value
        End Set
    End Property
    Public Property CIDDOCUMENTODE As Integer
        Get
            Return _CIDDOCUMENTODE
        End Get
        Set(ByVal value As Integer)
            _CIDDOCUMENTODE = value
        End Set
    End Property
    Public Property CDOCTOACREDITO As Boolean
        Get
            Return _CDOCTOACREDITO
        End Get
        Set(ByVal value As Boolean)
            _CDOCTOACREDITO = value
        End Set
    End Property

    Public Property CSERIEPOROMISION As String
        Get
            Return _CSERIEPOROMISION
        End Get
        Set(ByVal value As String)
            _CSERIEPOROMISION = value
        End Set
    End Property
    Public Property CNOFOLIO As Integer
        Get
            Return _CNOFOLIO
        End Get
        Set(ByVal value As Integer)
            _CNOFOLIO = value
        End Set
    End Property

    Public Property CPLAMIGCFD As String
        Get
            Return _CPLAMIGCFD
        End Get
        Set(ByVal value As String)
            _CPLAMIGCFD = value
        End Set
    End Property

    Public Property CRUTAENTREGA As String
        Get
            Return _CRUTAENTREGA
        End Get
        Set(ByVal value As String)
            _CRUTAENTREGA = value
        End Set
    End Property

    Public Property CPREFICON As String
        Get
            Return _CPREFICON
        End Get
        Set(ByVal value As String)
            _CPREFICON = value
        End Set
    End Property
    Public Property Existe As Boolean
        Get
            Return _Existe
        End Get
        Set(ByVal value As Boolean)
            _Existe = value
        End Set
    End Property
   
#End Region

End Class
