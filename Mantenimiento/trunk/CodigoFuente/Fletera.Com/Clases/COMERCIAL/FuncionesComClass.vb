﻿Public Class FuncionesComClass
    Dim StrSql As String
    Dim DsClase As New DataTable

    Public Function Existencia(ByVal IdProducto As Integer, ByVal Fecha As Date, Optional ByVal Almacen As Integer = 0) As Double
        Dim vExistencia As Double = 0
        StrSql = "select mov.cidproducto, " &
        "sum (case when tip.CAFECTAEXISTENCIA = 1 then (mov.CUNIDADES * 1) when tip.CAFECTAEXISTENCIA = 2 then (mov.CUNIDADES * -1) end) as Existencia " &
        "from admMovimientos mov " &
        "inner join admDocumentos doc on mov.ciddocumento = doc.ciddocumento " &
        "inner join admConceptos con on doc.CIDCONCEPTODOCUMENTO = con.CIDCONCEPTODOCUMENTO " &
        "inner join admDocumentosModelo tip on con.CIDDOCUMENTODE = tip.CIDDOCUMENTODE " &
        "where cidproducto = " & IdProducto &
        "and mov.cfecha <= '" & FormatFecHora(Fecha.ToShortDateString, True, False) & " 23:59:59' " &
        IIf(Almacen > 0, " AND CIDALMACEN = " & Almacen, "") &
        "group by mov.cidproducto"

        'Se checa si hay Cantidades en Presalidas
        DsClase = BDCOM.ExecuteReturn(StrSql)
        If DsClase.Rows.Count > 0 Then
            With DsClase.Rows(0)
                vExistencia = .Item("Existencia")

                'Checar si hay Articulos en PreSalidas
                StrSql = "SELECT DET.IDPRODUCTO, SUM(CANTIDAD) AS CANTIDAD FROM DETPRESALIDA DET " & _
                "INNER JOIN CABPRESALIDA CAB ON DET.IDPRESALIDA = CAB.IDPRESALIDA " & _
                "WHERE CAB.ESTATUS = 'PEN' " & _
                "AND IDPRODUCTO = " & IdProducto & _
                " GROUP BY DET.IDPRODUCTO"

                DsClase = BD.ExecuteReturn(StrSql)
                If DsClase.Rows.Count > 0 Then
                    With DsClase.Rows(0)
                        vExistencia = vExistencia - .Item("CANTIDAD")
                    End With
                End If

            End With
        Else
            'Return 0
        End If
        Return vExistencia
    End Function

    Public Function ChecaCosto(ByVal IdProducto As Integer, ByVal Fecha As Date, Optional ByVal Almacen As Integer = 0) As Double
        Dim VCosto As Double = 0


        StrSql = "SELECT CIDCOSTOH, " &
        "CIDPRODUCTO, " &
        "CIDALMACEN, " &
        "CFECHACOSTOH, " &
        "CCOSTOH, " &
        "CULTIMOCOSTOH, " &
        "CIDMOVIMIENTO, " &
        "CTIMESTAMP " &
        "FROM admCostosHistoricos " &
        "WHERE CIDPRODUCTO = " & IdProducto &
        "and CFECHACOSTOH <= '" & FormatFecHora(Fecha.ToShortDateString, True, False) & " 23:59:59' " &
        IIf(Almacen > 0, " AND CIDALMACEN = " & Almacen, "") &
        "ORDER BY CFECHACOSTOH DESC, CIDMOVIMIENTO DESC"

        DsClase = BDCOM.ExecuteReturn(StrSql)
        If DsClase.Rows.Count > 0 Then
            For i = 0 To DsClase.Rows.Count - 1
                VCosto = DsClase.Rows(i).Item("CCOSTOH")
                If VCosto > 0 Then
                    Exit For
                End If
            Next
        End If
        Return VCosto
    End Function
End Class
