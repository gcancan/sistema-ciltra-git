﻿Public Class ClientesComClass
    'CIDCLIENTEPROVEEDOR	vCIDCLIENTEPROVEEDOR	_CIDCLIENTEPROVEEDOR
    'CCODIGOCLIENTE	        vCCODIGOCLIENTE	        _CCODIGOCLIENTE
    'CRAZONSOCIAL	        vCRAZONSOCIAL	        _CRAZONSOCIAL
    'CRFC	                vCRFC	                _CRFC
    'CTIPOCLIENTE	        vCTIPOCLIENTE	        _CTIPOCLIENTE
    'CESTATUS	            vCESTATUS	            _CESTATUS
    'CLIMITECREDITOCLIENTE	vCLIMITECREDITOCLIENTE	_CLIMITECREDITOCLIENTE
    'CDIASCREDITOCLIENTE	vCDIASCREDITOCLIENTE	_CDIASCREDITOCLIENTE

    Private _CIDCLIENTEPROVEEDOR As Integer
    Private _CCODIGOCLIENTE As String
    Private _CRAZONSOCIAL As String
    Private _CRFC As String
    Private _CTIPOCLIENTE As Boolean
    Private _CESTATUS As Boolean
    Private _CLIMITECREDITOCLIENTE As Double
    Private _CDIASCREDITOCLIENTE As Integer

    Private _Existe As Boolean
    Private _ExisteLOC As Boolean
    Private _TablaBd As String = "admClientes"
    Private _TablaBdLOC As String = "CatClientes"

    Dim StrSql As String
    Dim DsClase As New DataTable

#Region "Constructor"
    Sub New(ByVal vCCODIGOCLIENTE As String)
        _CCODIGOCLIENTE = vCCODIGOCLIENTE

        StrSql = "SELECT cli.CIDCLIENTEPROVEEDOR, " & _
        "cli.CCODIGOCLIENTE, " & _
        "cli.CRAZONSOCIAL, " & _
        "cli.CFECHAALTA, " & _
        "cli.CRFC, " & _
        "cli.CTIPOCLIENTE, " & _
        "cli.CESTATUS, " & _
        "cli.CLIMITECREDITOCLIENTE, " & _
        "cli.CDIASCREDITOCLIENTE " & _
        "from " & _TablaBd & " cli " & _
        "WHERE cli.CCODIGOCLIENTE= '" & vCCODIGOCLIENTE & "'"

        DsClase = BDCOM.ExecuteReturn(StrSql)
        If DsClase.Rows.Count > 0 Then
            With DsClase.Rows(0)
                _CIDCLIENTEPROVEEDOR = .Item("CIDCLIENTEPROVEEDOR")
                _CRAZONSOCIAL = .Item("CRAZONSOCIAL")
                _CRFC = .Item("CRFC")
                _CTIPOCLIENTE = .Item("CTIPOCLIENTE")
                _CESTATUS = .Item("CESTATUS")
                _CLIMITECREDITOCLIENTE = .Item("CLIMITECREDITOCLIENTE")
                _CDIASCREDITOCLIENTE = .Item("CDIASCREDITOCLIENTE")
            End With
            _Existe = True
        Else
            _Existe = False
        End If

        If _Existe Then
            StrSql = "select * from " & _TablaBdLOC & _
            " where idcliente = " & CInt(vCCODIGOCLIENTE)
            DsClase = BD.ExecuteReturn(StrSql)
            If DsClase.Rows.Count > 0 Then
                _ExisteLOC = True
            Else
                _ExisteLOC = False
            End If
        End If
    End Sub
    Sub New(ByVal vCIDCLIENTEPROVEEDOR As Integer, ByVal vCCODIGOCLIENTE As String, ByVal vCRAZONSOCIAL As String, ByVal vCRFC As String,
            ByVal vCTIPOCLIENTE As Boolean, ByVal vCESTATUS As Boolean, ByVal vCLIMITECREDITOCLIENTE As Double, ByVal vCDIASCREDITOCLIENTE As Integer)
        _CIDCLIENTEPROVEEDOR = vCIDCLIENTEPROVEEDOR
        _CCODIGOCLIENTE = vCCODIGOCLIENTE
        _CRAZONSOCIAL = vCRAZONSOCIAL
        _CRFC = vCRFC
        _CTIPOCLIENTE = vCTIPOCLIENTE
        _CESTATUS = vCESTATUS
        _CLIMITECREDITOCLIENTE = vCLIMITECREDITOCLIENTE
        _CDIASCREDITOCLIENTE = vCDIASCREDITOCLIENTE
        _Existe = True
    End Sub

#End Region
#Region "Propiedades"
    Public Property CIDCLIENTEPROVEEDOR As Integer
        Get
            Return _CIDCLIENTEPROVEEDOR
        End Get
        Set(ByVal value As Integer)
            _CIDCLIENTEPROVEEDOR = value
        End Set
    End Property
    Public Property CCODIGOCLIENTE As String
        Get
            Return _CCODIGOCLIENTE
        End Get
        Set(ByVal value As String)
            _CCODIGOCLIENTE = value
        End Set
    End Property
    Public Property CRAZONSOCIAL As String
        Get
            Return _CRAZONSOCIAL
        End Get
        Set(ByVal value As String)
            _CRAZONSOCIAL = value
        End Set
    End Property
    Public Property CRFC As String
        Get
            Return _CRFC
        End Get
        Set(ByVal value As String)
            _CRFC = value
        End Set
    End Property
    Public Property CTIPOCLIENTE As Boolean
        Get
            Return _CTIPOCLIENTE
        End Get
        Set(ByVal value As Boolean)
            _CTIPOCLIENTE = value
        End Set
    End Property
    Public Property CESTATUS As Boolean
        Get
            Return _CESTATUS
        End Get
        Set(ByVal value As Boolean)
            _CESTATUS = value
        End Set
    End Property
    Public Property CLIMITECREDITOCLIENTE As Double
        Get
            Return _CLIMITECREDITOCLIENTE
        End Get
        Set(ByVal value As Double)
            _CLIMITECREDITOCLIENTE = value
        End Set
    End Property

    Public Property CDIASCREDITOCLIENTE As Integer
        Get
            Return _CDIASCREDITOCLIENTE
        End Get
        Set(ByVal value As Integer)
            _CDIASCREDITOCLIENTE = value
        End Set
    End Property

    Public Property Existe As Boolean
        Get
            Return _Existe
        End Get
        Set(ByVal value As Boolean)
            _Existe = value
        End Set
    End Property
    Public Property ExisteLOC As Boolean
        Get
            Return _ExisteLOC
        End Get
        Set(ByVal value As Boolean)
            _ExisteLOC = value
        End Set
    End Property
#End Region
End Class
