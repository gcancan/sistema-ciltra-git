﻿Public Class ImprimeReporte
    ' CAMPOS ********************************
    Private vFuenteEncabCampos As Font
    Private vFuenteEncabCamposOpc As Font
    Private vFuenteImprimeCampos As Font
    Private vFuenteImprimeCamposNeg As Font
    ' CAMPOS ********************************

    ' ENCABEZADO ********************************
    Private vFuenteEncabezado As Font '(tipo, tamaño, estilo)
    Private vFuenteEncabezado2 As Font '(tipo, tamaño, estilo)
    Private vEncabezado1 As String
    Private vAligEncabezado1 As TipoAlineacion
    Private vEncabezado2 As String
    Private vAligEncabezado2 As TipoAlineacion

    Private vFuenteTitulo As Font
    Private vTituloReporte As String
    Private vAligTituloReporte As TipoAlineacion
    'Private vBandAlineacionTitulo As Boolean

    Private vFuenteParametro As Font
    Private vParametro1 As String
    Private vAligParametro1 As TipoAlineacion
    Private vParametro2 As String
    Private vAligParametro2 As TipoAlineacion
    Private vParametro3 As String
    Private vAligParametro3 As TipoAlineacion
    Private vImagen1 As Image
    Private vPosicionImagen1 As Point
    Private vImagen2 As Image
    Private vPosicionImagen2 As Point

    ' PIE DE PAGINA  ********************************
    Private vBandPaginacion As Boolean
    Private vAligPaginacion As TipoAlineacion
    Private vBanSoloPagina As Boolean
    Private vAligSoloPagina As TipoAlineacion
    Private vPieParametro1 As String
    Private vAligPieParametro1 As TipoAlineacion
    Private vPieParametro2 As String
    Private vAligPieParametro2 As TipoAlineacion
    Private vPieParametro3 As String
    Private vAligPieParametro3 As TipoAlineacion
    Private vFuenteLetrasChiquitas As Font
    Private vFuenteLetrasChiquitasEnc As Font
    ' OTROS  ********************************
    Private vConstr As String
    Private vStrSql As String
    Private vStrSqlDet As String
    '
    Private vNomTabla As String
    Private vNomTabla_DET As String
    Private vTipoConexion As TipoConexion
    Private vNomReporte As String
    Private vBandRepEsp As Boolean = False

    'DECLARACION DE VARIABLES INTERNAS
    Public DsReporte As New DataSet
    Public DsReporte_DET As New DataSet
    Private lineaActual As Integer
    Private Factor As Double = 2.3
    Private Factor2 As Double = 3

    Public _ArrColClass() As CamposReporte
    Public _ArrSumaColClass() As Double
    Public _ArrSumaColClassGpo() As Double
    Public vNumColsRep As Integer

    'PARA EDO CUENTA
    Dim SubTotCargos, SubTotAbonos, SubTotMoraPag, SubTotMoraPend As Double

    Dim Band As Boolean = True

    'PARA GRUPOS
    Dim GrupoAct As String = ""
    Dim PosTotGrupo As Integer = 0
    Dim IndiceTotalGrupo As Integer = 0
    Dim CampoSumaGrupo, Campo2 As String

    'PARA ALINEACION
    'Dim AligIzquierda As Single = e.MarginBounds.Left
    'Dim AligCentro As Single = ((e.MarginBounds.Left + e.MarginBounds.Right) / 2) - 20
    'Dim AligDerecha As Single = e.MarginBounds.Right - 50

    Dim AligIzquierda As Single
    Dim AligCentro As Single
    Dim AligDerecha As Single


    'PARA PEDIDOS
    'Dim IdNum As Integer = 0

    'P  R  O  P  I  E  D  A  D  E  S
#Region "Propiedades"
    'vFuenteLetrasChiquitas
    Public Property FuenteLetrasChiquitasEnc() As Font
        Get
            Return vFuenteLetrasChiquitasEnc
        End Get
        Set(ByVal value As Font)
            vFuenteLetrasChiquitasEnc = value
        End Set
    End Property

    Public Property FuenteLetrasChiquitas() As Font
        Get
            Return vFuenteLetrasChiquitas
        End Get
        Set(ByVal value As Font)
            vFuenteLetrasChiquitas = value
        End Set
    End Property

    Public Property FuenteEncabCamposOpc() As Font
        Get
            Return vFuenteEncabCamposOpc
        End Get
        Set(ByVal value As Font)
            vFuenteEncabCamposOpc = value
        End Set
    End Property

    Public Property FuenteEncabCampos() As Font
        Get
            Return vFuenteEncabCampos
        End Get
        Set(ByVal value As Font)
            vFuenteEncabCampos = value
        End Set
    End Property
    Public Property FuenteImprimeCampos() As Font
        Get
            Return vFuenteImprimeCampos
        End Get
        Set(ByVal value As Font)
            vFuenteImprimeCampos = value
        End Set
    End Property

    Public Property FuenteImprimeCamposNeg() As Font
        Get
            Return vFuenteImprimeCamposNeg
        End Get
        Set(ByVal value As Font)
            vFuenteImprimeCamposNeg = value
        End Set
    End Property

    ' ENCABEZADO ********************************
    Public Property FuenteEncabezado() As System.Drawing.Font
        Get
            Return vFuenteEncabezado
        End Get
        Set(ByVal value As System.Drawing.Font)
            vFuenteEncabezado = value
        End Set
    End Property

    Public Property FuenteEncabezado2() As System.Drawing.Font
        Get
            Return vFuenteEncabezado2
        End Get
        Set(ByVal value As System.Drawing.Font)
            vFuenteEncabezado2 = value
        End Set
    End Property


    Public Property Encabezado1() As String
        Get
            Return vEncabezado1
        End Get
        Set(ByVal value As String)
            vEncabezado1 = value
        End Set
    End Property

    Public Property AligEncabezado1() As TipoAlineacion
        Get
            Return vAligEncabezado1
        End Get
        Set(ByVal value As TipoAlineacion)
            vAligEncabezado1 = value
        End Set
    End Property

    Public Property Encabezado2() As String
        Get
            Return vEncabezado2
        End Get
        Set(ByVal value As String)
            vEncabezado2 = value
        End Set
    End Property

    Public Property AligEncabezado2() As TipoAlineacion
        Get
            Return vAligEncabezado2
        End Get
        Set(ByVal value As TipoAlineacion)
            vAligEncabezado2 = value
        End Set
    End Property

    Public Property FuenteTitulo() As Font
        Get
            Return vFuenteTitulo
        End Get
        Set(ByVal value As Font)
            vFuenteTitulo = value
        End Set
    End Property

    Public Property TituloReporte() As String
        Get
            Return vTituloReporte
        End Get
        Set(ByVal value As String)
            vTituloReporte = value
        End Set
    End Property

    'Public Property BandAlineacionTitulo() As Boolean
    '    Get
    '        Return vBandAlineacionTitulo
    '    End Get
    '    Set(ByVal value As Boolean)
    '        vBandAlineacionTitulo = value
    '    End Set
    'End Property

    Public Property AligTituloReporte() As TipoAlineacion
        Get
            Return vAligTituloReporte
        End Get
        Set(ByVal value As TipoAlineacion)
            vAligTituloReporte = value
        End Set
    End Property

    Public Property FuenteParametro() As Font
        Get
            Return vFuenteParametro
        End Get
        Set(ByVal value As Font)
            vFuenteParametro = value
        End Set
    End Property

    Public Property Parametro1() As String
        Get
            Return vParametro1
        End Get
        Set(ByVal value As String)
            vParametro1 = value
        End Set
    End Property

    Public Property AligParametro1() As TipoAlineacion
        Get
            Return vAligParametro1
        End Get
        Set(ByVal value As TipoAlineacion)
            vAligParametro1 = value
        End Set
    End Property

    Public Property Parametro2() As String
        Get
            Return vParametro2
        End Get
        Set(ByVal value As String)
            vParametro2 = value
        End Set
    End Property

    Public Property AligParametro2() As TipoAlineacion
        Get
            Return vAligParametro2
        End Get
        Set(ByVal value As TipoAlineacion)
            vAligParametro2 = value
        End Set
    End Property

    Public Property Parametro3() As String
        Get
            Return vParametro3
        End Get
        Set(ByVal value As String)
            vParametro3 = value
        End Set
    End Property

    Public Property AligParametro3() As TipoAlineacion
        Get
            Return vAligParametro3
        End Get
        Set(ByVal value As TipoAlineacion)
            vAligParametro3 = value
        End Set
    End Property

    Public Property Imagen1() As Image
        Get
            Return vImagen1
        End Get
        Set(ByVal value As Image)
            vImagen1 = value
        End Set
    End Property

    Public Property PosicionImagen1() As Point
        Get
            Return vPosicionImagen1
        End Get
        Set(ByVal value As Point)
            vPosicionImagen1 = value
        End Set
    End Property
    Public Property Imagen2() As Image
        Get
            Return vImagen2
        End Get
        Set(ByVal value As Image)
            vImagen2 = value
        End Set
    End Property

    Public Property PosicionImagen2() As Point
        Get
            Return vPosicionImagen2
        End Get
        Set(ByVal value As Point)
            vPosicionImagen2 = value
        End Set
    End Property

    ' PIE DE PAGINA  ********************************
    Public Property BandPaginacion() As Boolean
        Get
            Return vBandPaginacion
        End Get
        Set(ByVal value As Boolean)
            vBandPaginacion = value
        End Set
    End Property

    Public Property AligPaginacion() As TipoAlineacion
        Get
            Return vAligPaginacion
        End Get
        Set(ByVal value As TipoAlineacion)
            vAligPaginacion = value
        End Set
    End Property

    Public Property BanSoloPagina() As Boolean
        Get
            Return vBanSoloPagina
        End Get
        Set(ByVal value As Boolean)
            vBanSoloPagina = value
        End Set
    End Property

    Public Property AligSoloPagina() As TipoAlineacion
        Get
            Return vAligSoloPagina
        End Get
        Set(ByVal value As TipoAlineacion)
            vAligSoloPagina = value
        End Set
    End Property

    Public Property PieParametro1() As String
        Get
            Return vPieParametro1
        End Get
        Set(ByVal value As String)
            vPieParametro1 = value
        End Set
    End Property

    Public Property AligPieParametro1() As TipoAlineacion
        Get
            Return vAligPieParametro1
        End Get
        Set(ByVal value As TipoAlineacion)
            vAligPieParametro1 = value
        End Set
    End Property

    Public Property PieParametro2() As String
        Get
            Return vPieParametro2
        End Get
        Set(ByVal value As String)
            vPieParametro2 = value
        End Set
    End Property

    Public Property AligPieParametro2() As TipoAlineacion
        Get
            Return vAligPieParametro2
        End Get
        Set(ByVal value As TipoAlineacion)
            vAligPieParametro2 = value
        End Set
    End Property

    Public Property PieParametro3() As String
        Get
            Return vPieParametro3
        End Get
        Set(ByVal value As String)
            vPieParametro3 = value
        End Set
    End Property

    Public Property AligPieParametro3() As TipoAlineacion
        Get
            Return vAligPieParametro3
        End Get
        Set(ByVal value As TipoAlineacion)
            vAligPieParametro3 = value
        End Set
    End Property

    Public Property Constr() As String
        Get
            Return vConstr
        End Get
        Set(ByVal value As String)
            vConstr = value
        End Set
    End Property

    Public Property StrSql() As String
        Get
            Return vStrSql
        End Get
        Set(ByVal value As String)
            vStrSql = value
        End Set
    End Property

    Public Property StrSqlDet() As String
        Get
            Return vStrSqlDet
        End Get
        Set(ByVal value As String)
            vStrSqlDet = value
        End Set
    End Property

    Public Property NomTabla() As String
        Get
            Return vNomTabla
        End Get
        Set(ByVal value As String)
            vNomTabla = value
        End Set
    End Property

    Public Property TipoConexion() As TipoConexion
        Get
            Return vTipoConexion
        End Get
        Set(ByVal value As TipoConexion)
            vTipoConexion = value
        End Set
    End Property

    Public Property NomReporte() As String
        Get
            Return vNomReporte
        End Get
        Set(ByVal value As String)
            vNomReporte = value
        End Set
    End Property

    Public Property BandRepEsp() As Boolean
        Get
            Return vBandRepEsp
        End Get
        Set(ByVal value As Boolean)
            vBandRepEsp = value
        End Set
    End Property

    'vBandRepEsp
#End Region
    'OTRAS FUNCIONES IMPORTANTES
    'Public Sub CargaCampos(ByVal vArrCol() As CamposReporte)
    '    _ArrColClass = vArrCol
    '    vNumColsRep = _ArrColClass.Length
    '    For i As Integer = 0 To _ArrColClass.Length - 1
    '        With _ArrColClass(i)

    '        End With
    '    Next
    'End Sub
    Public Sub CargaCampos2(ByVal vArrCol() As ColClass)

        ReDim _ArrColClass(vArrCol.Length - 1)
        ReDim _ArrSumaColClass(vArrCol.Length - 1)
        ReDim _ArrSumaColClassGpo(vArrCol.Length - 1)
        ReiniciaArreglos()
        For i As Integer = 0 To vArrCol.Length - 1
            With vArrCol(i)
                _ArrColClass(i) = New CamposReporte(.Campo, .DesripcionCampo, .BImprimeCampo, .AnchoCampo, .TipoCampo, .BSuma, .BGrupo, .CampoSumaGrupo)
            End With
        Next

    End Sub

    Public Sub ReiniciaArreglos()
        '_ArrColClass = Nothing
        For i As Integer = 0 To _ArrSumaColClass.Length - 1
            _ArrSumaColClass(i) = 0
            _ArrSumaColClassGpo(i) = 0
        Next
        'ReDim _ArrColClass(0)
        SubTotCargos = 0 : SubTotAbonos = 0 : SubTotMoraPag = 0 : SubTotMoraPend = 0

    End Sub

    Public Function LlenaDsImprime(ByVal Tabla As String) As DataSet
        Dim obj As New CapaNegocio.Tablas
        Dim TipoCon As String = ""
        LlenaDsImprime = Nothing
        Try

            DsReporte.Clear()
            'DS = obj.CargaDsPorClave(Inicio.ConStrProg, "PROGRESS", "JurCatNotarios", , "nNumNotario")
            If CType(vTipoConexion, TipoConexion) = TipoConexion.TcSQL Then
                TipoCon = "SQL"
            ElseIf CType(vTipoConexion, TipoConexion) = TipoConexion.TcProgress Then
                TipoCon = "PROGRESS"
            ElseIf CType(vTipoConexion, TipoConexion) = TipoConexion.TcFox Then
                TipoCon = "FOX"

            End If
            DsReporte = obj.CargaDsPorSentencia(vConstr, TipoCon, vStrSql, vNomTabla, Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS)

            If Not DsReporte Is Nothing Then
                If DsReporte.Tables(vNomTabla).DefaultView.Count > 0 Then
                    Return DsReporte
                Else
                    'sin datos
                    'Return 0
                End If
            Else
                'Return -1
                'No exiuste
            End If
            If Not obj Is Nothing Then obj = Nothing
        Catch ex As Exception
            If Not obj Is Nothing Then obj = Nothing
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, vNomReporte)
        End Try
    End Function

    Public Sub AsignaDataSet(ByVal vDataSet As DataSet)
        Try
            If DsReporte Is Nothing Then DsReporte = New DataSet
            DsReporte.Clear()
            DsReporte = vDataSet
        Catch ex As Exception

        End Try
    End Sub
    Public Function LlenaDataSet() As Integer
        Dim obj As New CapaNegocio.Tablas
        Dim TipoCon As String = ""
        LlenaDataSet = 0
        Try
            If DsReporte Is Nothing Then DsReporte = New DataSet
            DsReporte.Clear()
            'DS = obj.CargaDsPorClave(Inicio.ConStrProg, "PROGRESS", "JurCatNotarios", , "nNumNotario")
            If CType(vTipoConexion, TipoConexion) = TipoConexion.TcSQL Then
                TipoCon = "SQL"
            ElseIf CType(vTipoConexion, TipoConexion) = TipoConexion.TcProgress Then
                TipoCon = "PROGRESS"
            ElseIf CType(vTipoConexion, TipoConexion) = TipoConexion.TcFox Then
                TipoCon = "FOX"

            End If
            DsReporte = obj.CargaDsPorSentencia(vConstr, TipoCon, vStrSql, vNomTabla, Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS)

            If Not DsReporte Is Nothing Then
                If DsReporte.Tables(vNomTabla).DefaultView.Count > 0 Then
                    If NomReporte = "RepEdoCuenta" Or NomReporte = "RepEdoCuentaDet" Then
                        Dim TotSal As Double = 0
                        For i = 0 To DsReporte.Tables(vNomTabla).DefaultView.Count - 1
                            TotSal += DsReporte.Tables(vNomTabla).DefaultView.Item(i).Item("saldo")
                            DsReporte.Tables(vNomTabla).DefaultView.Item(i).Item("saldo") = TotSal
                        Next
                    End If
                    Return DsReporte.Tables(vNomTabla).DefaultView.Count
                Else
                    'sin datos
                    Return 0
                End If
            Else
                Return -1
                'No exiuste
            End If
            If Not obj Is Nothing Then obj = Nothing
        Catch ex As Exception
            If Not obj Is Nothing Then obj = Nothing
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, vNomReporte)
        End Try
    End Function

    Public Function LlenaDataSetDET() As Integer
        Dim obj As New CapaNegocio.Tablas
        Dim TipoCon As String = ""
        Dim vNomTablaDET As String = vNomTabla & "_DET"
        LlenaDataSetDET = 0


        Try
            If DsReporte_DET Is Nothing Then DsReporte_DET = New DataSet
            DsReporte_DET.Clear()
            'DS = obj.CargaDsPorClave(Inicio.ConStrProg, "PROGRESS", "JurCatNotarios", , "nNumNotario")
            If CType(vTipoConexion, TipoConexion) = TipoConexion.TcSQL Then
                TipoCon = "SQL"
            ElseIf CType(vTipoConexion, TipoConexion) = TipoConexion.TcProgress Then
                TipoCon = "PROGRESS"
            ElseIf CType(vTipoConexion, TipoConexion) = TipoConexion.TcFox Then
                TipoCon = "FOX"

            End If
            DsReporte_DET = obj.CargaDsPorSentencia(vConstr, TipoCon, vStrSql, vNomTablaDET, Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS)

            If Not DsReporte_DET Is Nothing Then
                If DsReporte_DET.Tables(vNomTablaDET).DefaultView.Count > 0 Then
                    'If NomReporte = "RepEdoCuenta" Or NomReporte = "RepEdoCuentaDet" Then
                    '    Dim TotSal As Double = 0
                    '    For i = 0 To DsReporte.Tables(vNomTabla).DefaultView.Count - 1
                    '        TotSal += DsReporte.Tables(vNomTabla).DefaultView.Item(i).Item("saldo")
                    '        DsReporte.Tables(vNomTabla).DefaultView.Item(i).Item("saldo") = TotSal
                    '    Next
                    'End If
                    Return DsReporte_DET.Tables(vNomTablaDET).DefaultView.Count
                Else
                    'sin datos
                    Return 0
                End If
            Else
                Return -1
                'No exiuste
            End If
            If Not obj Is Nothing Then obj = Nothing
        Catch ex As Exception
            If Not obj Is Nothing Then obj = Nothing
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, vNomReporte)
        End Try
    End Function

    Private Function MargenCentrado(ByVal e As System.Drawing.Printing.PrintPageEventArgs, ByVal Texto As String, ByVal Fuente As Font) As Single

        '    Function Imp_centrado(texto As String)
        '    Dim ancho As Integer
        '    ancho = Printer.TextWidth(texto) / 2
        '    Printer.CurrentX = Printer.ScaleWidth / 2 - ancho
        '    Printer.Print(texto)
        'End Function

        Dim Medida As SizeF = e.Graphics.MeasureString(Texto, Fuente)
        Dim LeftMedida As Single = e.MarginBounds.Left
        Dim x As Single = e.PageSettings.PrintableArea.Left
        Dim xn As Single = e.PageSettings.PrintableArea.Right
        Dim MedidaPag As Single = e.MarginBounds.Width

        MedidaPag = xn - x

        '{Width = 738.837341 Height = 35.41395}
        '{Width = 422.4718 Height = 35.41395}
        '{Width = 422.4718 Height = 35.41395}


        'MargenCentrado = (MedidaPag / 2) - (Medida.Width / 2)
        MargenCentrado = (MedidaPag - Medida.Width) / 2
        'MargenCentrado = Medida.Width / 2

        If MargenCentrado < LeftMedida Then
            MargenCentrado = LeftMedida
        End If
        'If Medida.Width < mitad + 100 Then
        '    'MargenCentrado = Medida.Width / 2
        '    MargenCentrado = (MedidaPag / 2) - Medida.Width
        'Else
        '    MargenCentrado = Left()

        'End If

    End Function

    'ENCABEZADO
    'Contiene:
    'Nombre de Reporte - o Titulo
    'Datos de Encabezado los parametros que se requieran
    'Imagenes
    'Campos a Imprimir Encabezados o nombre de campos
    'Ejemplo:
    'Codigo  Nombre         Direccion Telefono
    '------  ------------   --------- --------


    'Private Function CalculaAncho(ByVal margenes As Rectangle, ByVal prFont As Font, ByVal AnchoColumna As Integer) As Integer
    '    '------------------------------------------------------------
    '    ' ajustar el ancho de cada columna 
    '    ' según el ancho "requerido" y el "disponible"
    '    '------------------------------------------------------------
    '    ' el número de caracteres por cada línea
    '    Dim lineWidth As Single = margenes.Width
    '    Dim tc As Integer = CInt(lineWidth / prFont.SizeInPoints + 0.5)
    '    Dim anchoTotal = vAnchoCampo1 + vAnchoCampo2 + vAnchoCampo3 + vAnchoCampo4

    '    ' el porcentaje a restar en cada columna
    '    Dim npc As Integer = ((anchoTotal - tc) * 100) \ anchoTotal
    '    If npc < 1 Then npc = 1
    '    'ReDim anchoColumnasImp(anchoColumnas.Length - 1)
    '    '---> Calcula Ancho
    '    Dim Ancho As Integer
    '    Ancho = AnchoColumna - CInt(AnchoColumna * npc / 100) + 1
    '    If Ancho > AnchoColumna Then
    '        Return AnchoColumna
    '    Else
    '        Return Ancho
    '    End If

    '    'For i As Integer = 0 To anchoColumnas.Length - 1
    '    '    Dim j As Integer = anchoColumnas(i) - CInt(anchoColumnas(i) * npc / 100) + 1
    '    '    If j > anchoColumnas(i) Then
    '    '        anchoColumnasImp(i) = anchoColumnas(i)
    '    '    Else
    '    '        anchoColumnasImp(i) = j
    '    '    End If
    '    'Next
    'End Function


    Public Function ImprimeEncabezado(ByVal e As System.Drawing.Printing.PrintPageEventArgs, ByVal yPos As Single, _
    ByVal BandHorizontal As Boolean, ByVal filtroTexto As String, _
    Optional ByVal Parametro As Object = Nothing, Optional ByVal Parametro2 As Object = Nothing) As Single
        'Alineacion
        AligIzquierda = e.MarginBounds.Left
        AligCentro = ((e.MarginBounds.Left + e.MarginBounds.Right) / 2) - 20
        AligDerecha = e.MarginBounds.Right - 50


        Dim Cadena As String = ""
        Dim X1 As Single = e.MarginBounds.Left
        Dim X2 As Single = X1 + 250
        'Dim X3 As Single = X2 + 250
        Dim X3 As Single = X2 + 160 - 20

        'orientacion vertical
        'Dim X3a As Single = X3 - 50
        'Dim X3B As Single = X3 + 140
        'Dim X3C As Single = X3 + 100

        'orientacion Horizonta
        Dim X3a As Single = X3 - 50
        Dim X3B As Single = X3 + 140
        Dim X3C As Single = X3 + 280


        'Para Antiguedad de saldos
        Dim Rango As Integer = CInt(Parametro)
        Dim RangoIni As Integer = CInt(Parametro)
        Dim IniRango As Integer = 1



        'PARAMETROS SEGUN REPORTE
        If NomReporte = "RepAntSaldos" Then
            Band = CBool(Parametro2)
            _ArrColClass(2).BImprimeCampo = Band
        End If


        ImprimeEncabezado = yPos
        Try
            Dim lineHeight As Single = vFuenteEncabezado.GetHeight(e.Graphics)
            Dim leftMargin As Single = e.MarginBounds.Left

            Dim AligCentro As Single = ((e.MarginBounds.Left + e.MarginBounds.Right) / 2)

            'Original
            If BandHorizontal Then
                leftMargin = AligCentro - (Len(Encabezado1) * Factor * Factor2)
            Else
                leftMargin = MargenCentrado(e, Encabezado1, vFuenteEncabezado)
            End If
            e.Graphics.DrawString(Encabezado1, vFuenteEncabezado, Brushes.Black, leftMargin, yPos)



            ''IMPRIME IMAGENES
            e.Graphics.DrawImage(vImagen1, vPosicionImagen1)
            e.Graphics.DrawImage(vImagen2, vPosicionImagen2)
            yPos += vFuenteEncabezado.GetHeight(e.Graphics)

            '
            If BandHorizontal Then
                leftMargin = AligCentro - (Len(Encabezado2) * Factor * Factor2)
            Else
                leftMargin = MargenCentrado(e, Encabezado2, vFuenteEncabezado2)
            End If

            e.Graphics.DrawString(Encabezado2, vFuenteEncabezado2, Brushes.Black, leftMargin, yPos)
            yPos += vFuenteEncabezado.GetHeight(e.Graphics)

            '
            If BandHorizontal Then
                leftMargin = AligCentro - (Len(TituloReporte) * Factor * Factor2)
            Else
                leftMargin = MargenCentrado(e, TituloReporte, vFuenteTitulo)
            End If


            'If vBandAlineacionTitulo Then
            If CType(vAligTituloReporte, TipoAlineacion) = TipoAlineacion.TdIzquierda Then
                leftMargin = AligIzquierda
            ElseIf CType(vAligTituloReporte, TipoAlineacion) = TipoAlineacion.TdCentrado Then
                leftMargin = AligCentro
            ElseIf CType(vAligTituloReporte, TipoAlineacion) = TipoAlineacion.TdDerecha Then
                leftMargin = AligDerecha
            End If
            'End If

            e.Graphics.DrawString(TituloReporte, vFuenteTitulo, Brushes.Black, leftMargin, yPos)
            yPos += vFuenteTitulo.GetHeight(e.Graphics)

            'Aqui Se imprime El Cabecero Opcional
            'If NomReporte = "RepEdoCtaCli" Or NomReporte = "RepEdoCuenta" Or NomReporte = "RepEdoCuentaDet" Then

            If NomReporte = "RepRemision" Or NomReporte = "RepPedidosPen" Then
                Dim ContAuxLineas As Integer = 0
                e.Graphics.DrawLine(Pens.Black, X1, yPos + 15, e.MarginBounds.Right, yPos + 15)
                For i As Integer = 0 To 0

                    'Linea 0
                    yPos += 15
                    If NomReporte = "RepRemision" Then
                        'LINEA 1
                        Cadena = "TRASPASO : " & DsReporte.Tables(vNomTabla).DefaultView.Item(i).Item("CNOMBREC01_TRAS")
                        e.Graphics.DrawString(Cadena, vFuenteEncabCampos, Brushes.Black, X1, yPos)
                        'yPos += 15
                        e.Graphics.DrawLine(Pens.Black, X1, yPos, e.MarginBounds.Right, yPos)
                        Cadena = "No. Movimiento  : " & DsReporte.Tables(vNomTabla).DefaultView.Item(i).Item("NOMOVIMIENTO")
                        e.Graphics.DrawString(Cadena, vFuenteEncabCampos, Brushes.Black, X3C, yPos)

                        'LINEA 2
                        'Mas Amplio la cuadricula
                        'yPos += 15
                        yPos += 20
                        e.Graphics.DrawLine(Pens.Black, X1, yPos, e.MarginBounds.Right, yPos)
                        Cadena = "ALMACEN SALIDA : " & DsReporte.Tables(vNomTabla).DefaultView.Item(i).Item("CIDALMACEN_SAL") & " - " & DsReporte.Tables(vNomTabla).DefaultView.Item(i).Item("NOMALMACEN_SAL")
                        e.Graphics.DrawString(Cadena, vFuenteEncabCampos, Brushes.Black, X1, yPos)

                        'LINEA 3
                        'yPos += 15
                        yPos += 20
                        e.Graphics.DrawLine(Pens.Black, X1, yPos, e.MarginBounds.Right, yPos)
                        Cadena = "CONCEPTO DE SALIDA : " & DsReporte.Tables(vNomTabla).DefaultView.Item(i).Item("CNOMBREC01_SAL")
                        e.Graphics.DrawString(Cadena, vFuenteEncabCampos, Brushes.Black, X1, yPos)
                        'PRUEBA
                        Cadena = "SERIE: " & DsReporte.Tables(vNomTabla).DefaultView.Item(i).Item("serie_sal") & " FOLIO: " & DsReporte.Tables(vNomTabla).DefaultView.Item(i).Item("folio_sal")
                        e.Graphics.DrawString(Cadena, vFuenteEncabCampos, Brushes.Black, X3C, yPos)
                        'Cadena = "FOLIO  : " & DsReporte.Tables(vNomTabla).DefaultView.Item(i).Item("folio_sal")
                        'e.Graphics.DrawString(Cadena, vFuenteEncabCampos, Brushes.Black, X2, yPos)

                        'yPos += 15
                        'e.Graphics.DrawLine(Pens.Black, X1, yPos, e.MarginBounds.Right, yPos)
                        'Cadena = "SERIE  : " & DsReporte.Tables(vNomTabla).DefaultView.Item(i).Item("serie_sal")
                        'e.Graphics.DrawString(Cadena, vFuenteEncabCampos, Brushes.Black, X1, yPos)
                        'Cadena = "FOLIO  : " & DsReporte.Tables(vNomTabla).DefaultView.Item(i).Item("folio_sal")
                        'e.Graphics.DrawString(Cadena, vFuenteEncabCampos, Brushes.Black, X2, yPos)

                        'LINEA 4
                        'yPos += 15
                        yPos += 20
                        e.Graphics.DrawLine(Pens.Black, X1, yPos, e.MarginBounds.Right, yPos)
                        Cadena = "ALMACEN ENTRADA : " & DsReporte.Tables(vNomTabla).DefaultView.Item(i).Item("CIDALMACEN_ENT") & " - " & DsReporte.Tables(vNomTabla).DefaultView.Item(i).Item("NOMALMACEN_ENT")
                        e.Graphics.DrawString(Cadena, vFuenteEncabCampos, Brushes.Black, X1, yPos)

                        'LINEA 5
                        'yPos += 15
                        yPos += 20
                        e.Graphics.DrawLine(Pens.Black, X1, yPos, e.MarginBounds.Right, yPos)
                        Cadena = "CONCEPTO DE ENTRADA : " & DsReporte.Tables(vNomTabla).DefaultView.Item(i).Item("CNOMBREC01_ENT")
                        e.Graphics.DrawString(Cadena, vFuenteEncabCampos, Brushes.Black, X1, yPos)
                        Cadena = "SERIE: " & DsReporte.Tables(vNomTabla).DefaultView.Item(i).Item("serie_ENT") & _
                        " FOLIO: " & DsReporte.Tables(vNomTabla).DefaultView.Item(i).Item("folio_ENT")
                        e.Graphics.DrawString(Cadena, vFuenteEncabCampos, Brushes.Black, X3C, yPos)

                        'yPos += 15
                        'e.Graphics.DrawLine(Pens.Black, X1, yPos, e.MarginBounds.Right, yPos)
                        'Cadena = "SERIE  : " & DsReporte.Tables(vNomTabla).DefaultView.Item(i).Item("serie_ENT")
                        'e.Graphics.DrawString(Cadena, vFuenteEncabCampos, Brushes.Black, X1, yPos)
                        'Cadena = "FOLIO  : " & DsReporte.Tables(vNomTabla).DefaultView.Item(i).Item("folio_ENT")
                        'e.Graphics.DrawString(Cadena, vFuenteEncabCampos, Brushes.Black, X2, yPos)

                    Else
                        Cadena = "ORDEN DE SURTIDO"
                        e.Graphics.DrawString(Cadena, vFuenteEncabCampos, Brushes.Black, X3, yPos)

                        yPos += 15
                        e.Graphics.DrawLine(Pens.Black, X3, yPos, e.MarginBounds.Right, yPos)
                        Cadena = "PEDIDO  : " & DsReporte.Tables(vNomTabla).DefaultView.Item(i).Item("NOMOVIMIENTO")
                        e.Graphics.DrawString(Cadena, vFuenteEncabCampos, Brushes.Black, X3, yPos)

                    End If

                    'LINEA 6
                    'yPos += 15
                    yPos += 20
                    e.Graphics.DrawLine(Pens.Black, X1, yPos, e.MarginBounds.Right, yPos)
                    Cadena = "FECHA: " & DsReporte.Tables(vNomTabla).DefaultView.Item(i).Item("FECHAMOVIMIENTO")
                    e.Graphics.DrawString(Cadena, vFuenteEncabCampos, Brushes.Black, X1, yPos)
                    'yPos += 15

                    'e.Graphics.DrawLine(Pens.Black, X1, yPos, e.MarginBounds.Right, yPos)
                    Cadena = "USUARIO: " & DsReporte.Tables(vNomTabla).DefaultView.Item(i).Item("UsuarioModifica")
                    e.Graphics.DrawString(Cadena, vFuenteEncabCampos, Brushes.Black, X3C, yPos)
                    'yPos += 15
                    yPos += 20

                    'Linea 8
                    Cadena = DsReporte.Tables(vNomTabla).DefaultView.Item(i).Item("REFERENCIA")
                    If Cadena <> "" Then
                        'yPos += 15
                        e.Graphics.DrawLine(Pens.Black, X1, yPos, e.MarginBounds.Right, yPos)

                        Cadena = "REFERENCIA: " & Cadena
                        e.Graphics.DrawString(Cadena, vFuenteEncabCampos, Brushes.Black, X1, yPos)
                        ContAuxLineas += 1
                    End If

                    'e.Graphics.DrawLine(Pens.Black, X1, yPos, e.MarginBounds.Right, yPos)

                    'lineas verticales
                    If NomReporte = "RepRemision" Then
                        'e.Graphics.DrawLine(Pens.Black, X1, yPos - (15 * 6), X1, yPos)
                        'e.Graphics.DrawLine(Pens.Black, e.MarginBounds.Right, yPos - (15 * 6), e.MarginBounds.Right, yPos)
                        e.Graphics.DrawLine(Pens.Black, X1, yPos - (20 * 6), X1, yPos)
                        e.Graphics.DrawLine(Pens.Black, e.MarginBounds.Right, yPos - (20 * 6), e.MarginBounds.Right, yPos)
                    Else
                        e.Graphics.DrawLine(Pens.Black, X1, yPos - (15 * 3), X1, yPos)
                        e.Graphics.DrawLine(Pens.Black, e.MarginBounds.Right, yPos - (15 * 3), e.MarginBounds.Right, yPos)

                    End If
                    'yPos += 15

                    'e.Graphics.DrawLine(Pens.Black, X1, yPos - (15 * 3), X1, yPos + 15)
                    'e.Graphics.DrawLine(Pens.Black, e.MarginBounds.Right, yPos - (15 * 3), e.MarginBounds.Right, yPos + 15)

                    'e.Graphics.DrawLine(Pens.Black, X1, yPos - (15 * ContAuxLineas - 1), X1, yPos + 15)
                    'e.Graphics.DrawLine(Pens.Black, e.MarginBounds.Right, yPos - (15 * ContAuxLineas - 1), e.MarginBounds.Right, yPos + 15)

                    e.Graphics.DrawLine(Pens.Black, X1, yPos - (15 * (ContAuxLineas - 1)), X1, yPos + 15)
                    e.Graphics.DrawLine(Pens.Black, e.MarginBounds.Right, yPos - (15 * (ContAuxLineas - 1)), e.MarginBounds.Right, yPos + 15)


                    'ContAuxLineas = 0
                    'Cadena = "FECHA ULTIMA VTA:" & DsReporte.Tables(vNomTabla).DefaultView.Item(i).Item("FECULTIMACOMPRA")
                    'e.Graphics.DrawString(Cadena, vFuenteEncabCampos, Brushes.Black, X3, yPos)

                    ''Linea 2
                    'yPos += 15

                    'e.Graphics.DrawString(Cadena, vFuenteEncabCampos, Brushes.Black, X1, yPos)
                    'Cadena = "FECHA ULTIMO REGISTRO:" & DsReporte.Tables(vNomTabla).DefaultView.Item(i).Item("FECULTREGISTRO")
                    'e.Graphics.DrawString(Cadena, vFuenteEncabCampos, Brushes.Black, X3, yPos)

                    ''Linea 3
                    'yPos += 15
                    'Cadena = "NOMBRE:" & DsReporte.Tables(vNomTabla).DefaultView.Item(i).Item("CRAZONSO01")
                    'e.Graphics.DrawString(Cadena, vFuenteEncabCampos, Brushes.Black, X1, yPos)
                    'Cadena = "FECHA ULTIMO CANJE:" & DsReporte.Tables(vNomTabla).DefaultView.Item(i).Item("FECULTIMOCANJE")
                    'e.Graphics.DrawString(Cadena, vFuenteEncabCampos, Brushes.Black, X3, yPos)
                Next
            Else
                'Imprimir Filtros
                Cadena = filtroTexto
                e.Graphics.DrawString(Cadena, vFuenteEncabCampos, Brushes.Black, X1, yPos)
            End If

            yPos += vFuenteTitulo.GetHeight(e.Graphics)

            'Dibuja Linea
            e.Graphics.DrawLine(Pens.Black, e.MarginBounds.Left, yPos, e.MarginBounds.Right, yPos)
            e.Graphics.DrawLine(Pens.Black, e.MarginBounds.Left, yPos + 2, e.MarginBounds.Right, yPos + 2)

            yPos += 5

            'imprimir Cabeceros de Campos
            leftMargin = e.MarginBounds.Left

            'If NomReporte = "RepEdoCuenta" Then
            '    'imprimir CARGO (8y9) - ABONO  (10y11)
            '    leftMargin = CalculaPosXTot(10, leftMargin)
            '    Cadena = " C  A  R  G  O  S"
            '    e.Graphics.DrawString(Cadena, vFuenteEncabCampos, Brushes.Black, leftMargin, yPos)

            '    leftMargin = e.MarginBounds.Left
            '    leftMargin = CalculaPosXTot(12, leftMargin)
            '    Cadena = " A  B  O  N  O  S"
            '    e.Graphics.DrawString(Cadena, vFuenteEncabCampos, Brushes.Black, leftMargin, yPos)

            '    yPos += 10
            '    leftMargin = e.MarginBounds.Left
            'End If



            For i As Integer = 0 To _ArrColClass.Length - 1
                With _ArrColClass(i)
                    If .BImprimeCampo Then
                        e.Graphics.DrawString(.DesripcionCampo, vFuenteEncabCampos, Brushes.Black, leftMargin, yPos)
                        'e.Graphics.DrawLine(Pens.Black, e.MarginBounds.Left, yPos, e.MarginBounds.Left, yPos + 10)
                        'e.Graphics.DrawLine(Pens.Black, e.MarginBounds.Left, yPos, e.MarginBounds.Right, yPos)

                        'LINEAS VERTICALES
                        'e.Graphics.DrawLine(Pens.Black, leftMargin, yPos, leftMargin, e.MarginBounds.Right)

                        'If UCase(NomReporte) = UCase("RepAntSaldos") Or UCase(NomReporte) = UCase("RepAntSaldosDet") Then
                        '    If .Campo = "SaldoR1" Or .Campo = "SaldoR2" Or .Campo = "SaldoR3" Or .Campo = "SaldoR4" Then
                        '        Cadena = "    " & IniRango & "-" & Rango
                        '        IniRango = Rango + 1
                        '        Rango = Rango + RangoIni
                        '        e.Graphics.DrawString(Cadena, vFuenteEncabCampos, Brushes.Black, leftMargin, yPos)
                        '    ElseIf .Campo = "SaldoR5" Then
                        '        Cadena = "   > " & Rango - RangoIni
                        '        e.Graphics.DrawString(Cadena, vFuenteEncabCampos, Brushes.Black, leftMargin, yPos)
                        '    Else
                        '        e.Graphics.DrawString(.DesripcionCampo, vFuenteEncabCampos, Brushes.Black, leftMargin, yPos)
                        '    End If


                        '    'If i > 2 And i < 7 Then
                        '    '    If i = 6 Then
                        '    '        Cadena = "   > " & Rango - RangoIni
                        '    '    Else
                        '    '        Cadena = "    " & IniRango & "-" & Rango
                        '    '        IniRango = Rango + 1
                        '    '        Rango = Rango + RangoIni
                        '    '    End If
                        '    '    e.Graphics.DrawString(Cadena, vFuenteEncabCampos, Brushes.Black, leftMargin, yPos)
                        '    '    'leftMargin = leftMargin + (.AnchoCampo * Factor)
                        '    'Else
                        '    '    e.Graphics.DrawString(.DesripcionCampo, vFuenteEncabCampos, Brushes.Black, leftMargin, yPos)
                        '    '    'leftMargin = leftMargin + (.AnchoCampo * Factor)
                        '    'End If
                        'Else
                        '    e.Graphics.DrawString(.DesripcionCampo, vFuenteEncabCampos, Brushes.Black, leftMargin, yPos)
                        '    'leftMargin = leftMargin + (.AnchoCampo * Factor)
                        'End If
                        leftMargin = leftMargin + (.AnchoCampo * Factor)
                    End If
                End With

            Next

            yPos += vFuenteEncabCampos.GetHeight(e.Graphics)

            Return yPos

        Catch ex As Exception
            MsgBox(ex.Message, vNomReporte)
        End Try

    End Function
    'CUERPO
    'Aqui va el vaciado de todos los datos, con sus modificaciones al imprimir
    'De igual forma si se van a llevar Acumulados, sumas, etc.
    Public Sub DibujaLinea(ByVal e As System.Drawing.Printing.PrintPageEventArgs, ByVal yPos As Single, ByVal BanHorizontal As Boolean, ByVal Excedente As Integer)
        Dim leftMargin As Single = e.MarginBounds.Left
        Dim lineHeight As Single = vFuenteImprimeCampos.GetHeight(e.Graphics)
        'Dim leftMargin As Single = yPos
        For i As Integer = 0 To _ArrColClass.Length - 1
            With _ArrColClass(i)
                If .BImprimeCampo Then
                    'linea horizontal
                    e.Graphics.DrawLine(Pens.Black, e.MarginBounds.Left, yPos + lineHeight, e.MarginBounds.Right + Excedente, yPos + lineHeight)


                    'linea vertical
                    'e.Graphics.DrawLine(Pens.Black, leftMargin, yPos, leftMargin, e.MarginBounds.Right)
                    '
                    'Anterior
                    'e.Graphics.DrawLine(Pens.Black, leftMargin, yPos, leftMargin, IIf(BanHorizontal, e.MarginBounds.Bottom, e.MarginBounds.Bottom - 40))

                    'e.Graphics.DrawLine(Pens.Black, leftMargin, yPos, leftMargin, yPos + lineHeight)

                    leftMargin = leftMargin + (.AnchoCampo * Factor)

                End If
            End With
        Next
    End Sub
    Public Function ImprimeLinea(ByVal e As System.Drawing.Printing.PrintPageEventArgs, ByVal yPos As Single, ByVal Indice As Integer, _
                                 ByVal BanCuadricula As Boolean, ByVal BanBorderDetalle As Boolean, ByVal NomCampoDet As String, _
                                 ByVal NomCampoDet2 As String, Optional ByVal Parametro As Object = Nothing, _
                                Optional yPosFinal As Single = 0) As Single
        Dim lineHeight As Single = vFuenteImprimeCampos.GetHeight(e.Graphics)
        Dim leftMargin As Single = e.MarginBounds.Left
        Dim Cadena As String
        Dim j As Integer = 0
        If Indice = 0 Then GrupoAct = ""
        Dim UltimaCol As Integer = _ArrColClass.Length - 1
        Dim NoActual As Integer = 0
        Dim FechaAct As String = ""
        'Dim factorYY As Integer = 20
        Dim ImpDetalle As Boolean = CBool(Parametro)
        'Dim ContLineaActual As Integer = Indice

        Try
            'imprimir Datos
            leftMargin = e.MarginBounds.Left

            For i As Integer = 0 To _ArrColClass.Length - 1
                With _ArrColClass(i)
                    'CHECAMOS SI EL CAMPO ES UN GRUPO
                    Campo2 = .Campo
                    If .BGrupo Then

                        CampoSumaGrupo = .CampoSumaGrupo
                        Cadena = Trim(UCase(DsReporte.Tables(vNomTabla).DefaultView.Item(Indice).Item(.Campo) & ""))
                        If GrupoAct <> Cadena Then
                            'fUNCION IMPRIME TOTAL GRUPO
                            If GrupoAct <> "" Then
                                ImprimeTotalGrupo(e, yPos, i)
                                'ImprimeTotal(e, yPos, 0)
                                yPos += lineHeight
                                yPos += lineHeight
                            End If
                            GrupoAct = Cadena
                            e.Graphics.DrawString(IIf(.TipoCampo = TipoDato.TdNumerico, Cadena.PadLeft(12, " "c), Cadena), vFuenteEncabCampos, Brushes.Black, leftMargin, yPos)
                            yPos += lineHeight
                            yPos += lineHeight
                            'Exit For
                        End If
                    End If
                    If .Campo = "ImporteMov" Then
                        'PosTotGrupo = leftMargin
                        IndiceTotalGrupo = i
                    End If
                    'PARA LLEVAR LA SUMA AUNQUE NO SE IMPRIMA
                    If .BSuma Then
                        If .BGrupo Then
                            'Se tiene que especificar que campo se va sumando para el grupo, en caso de que lo necesite
                            '**********************
                            'AGREGADO 31/JULIO/2013 -> 

                            If .CampoSumaGrupo <> "" Then
                                _ArrSumaColClass(i) += Val(DsReporte.Tables(vNomTabla).DefaultView.Item(Indice).Item(.CampoSumaGrupo))
                                _ArrSumaColClassGpo(i) += Val(DsReporte.Tables(vNomTabla).DefaultView.Item(Indice).Item(.CampoSumaGrupo))
                            Else
                                'nuevo
                                _ArrSumaColClass(i) += Val(DsReporte.Tables(vNomTabla).DefaultView.Item(Indice).Item(.Campo))
                                _ArrSumaColClassGpo(i) += Val(DsReporte.Tables(vNomTabla).DefaultView.Item(Indice).Item(.Campo))
                            End If

                        Else
                            _ArrSumaColClass(i) += Val(DsReporte.Tables(vNomTabla).DefaultView.Item(Indice).Item(.Campo))
                            _ArrSumaColClassGpo(i) += Val(DsReporte.Tables(vNomTabla).DefaultView.Item(Indice).Item(.Campo))
                        End If
                        '_ArrSumaColClass(i) += Val(DsReporte.Tables(vNomTabla).DefaultView.Item(Indice).Item(.Campo))
                        'If NomReporte = "RepEdoCuenta" Or NomReporte = "RepEdoCuentaDet" Then
                        '    'Dim SubTotCargos, SubTotAbonos, SubTotMoraPag, SubTotMoraPend As Double
                        '    If DsReporte.Tables(vNomTabla).DefaultView.Item(Indice).Item("idconcepto") = "200" Then
                        '        'SubTotAbonos += Val(DsReporte.Tables(vNomTabla).DefaultView.Item(Indice).Item("IMPORTEABONO"))
                        '        _ArrSumaColClass(i) += Val(DsReporte.Tables(vNomTabla).DefaultView.Item(Indice).Item(.Campo))
                        '    ElseIf DsReporte.Tables(vNomTabla).DefaultView.Item(Indice).Item("idconcepto") = "201" Then
                        '        'SubTotCargos += Val(DsReporte.Tables(vNomTabla).DefaultView.Item(Indice).Item("IMPORTEMOVIMIENTO"))
                        '        _ArrSumaColClass(i) += Val(DsReporte.Tables(vNomTabla).DefaultView.Item(Indice).Item(.Campo))
                        '    End If
                        '    If .Campo = "SALDO" Then
                        '        _ArrSumaColClass(i) = DsReporte.Tables(vNomTabla).DefaultView.Item(Indice).Item("saldo")
                        '    End If

                        'Else
                        '    _ArrSumaColClass(i) += Val(DsReporte.Tables(vNomTabla).DefaultView.Item(Indice).Item(.Campo))
                        'End If

                        '_ArrSumaColClass(i) += Val(DsReporte.Tables(vNomTabla).DefaultView.Item(Indice).Item(.Campo))

                    End If


                    If .BImprimeCampo Then
                        If .TipoCampo = TipoDato.TdFecha Then
                            Cadena = CDate(DsReporte.Tables(vNomTabla).DefaultView.Item(Indice).Item(.Campo) & "").ToShortDateString
                        ElseIf .TipoCampo = TipoDato.TdFechaHora Then
                            Cadena = CDate(DsReporte.Tables(vNomTabla).DefaultView.Item(Indice).Item(.Campo) & "")
                        ElseIf .TipoCampo = TipoDato.TdHora Then
                            Cadena = CDate(DsReporte.Tables(vNomTabla).DefaultView.Item(Indice).Item(.Campo) & "").ToShortTimeString
                        ElseIf .TipoCampo = TipoDato.TdBoolean Then
                            Cadena = IIf(DsReporte.Tables(vNomTabla).DefaultView.Item(Indice).Item(.Campo), "    *", "")
                        ElseIf .TipoCampo = TipoDato.TdNumerico Then
                            Cadena = Format((DsReporte.Tables(vNomTabla).DefaultView.Item(Indice).Item(.Campo)), "###,###0.00")
                        Else
                            Cadena = DsReporte.Tables(vNomTabla).DefaultView.Item(Indice).Item(.Campo) & ""
                            'Cadena = DsReporte.Tables(0).DefaultView.Item(Indice).Item(.Campo) & ""
                        End If

                        e.Graphics.DrawString(IIf(.TipoCampo = TipoDato.TdNumerico, Cadena.PadLeft(12, " "c), Cadena), vFuenteImprimeCampos, Brushes.Black, leftMargin, yPos)

                        If (Indice = 65 Or Indice = 64) And i = 9 Then
                            'e.Graphics.DrawLine(Pens.Black, leftMargin, yPos, leftMargin, e.MarginBounds.Bottom)
                        End If

                        If BanCuadricula Then

                            'linea horizontal
                            If i = UltimaCol Then
                                e.Graphics.DrawLine(Pens.Black, e.MarginBounds.Left, yPos, e.MarginBounds.Right + 20, yPos)
                            Else
                                e.Graphics.DrawLine(Pens.Black, e.MarginBounds.Left, yPos, e.MarginBounds.Right, yPos)
                            End If

                            'linea Vertical
                            e.Graphics.DrawLine(Pens.Black, leftMargin, yPos, leftMargin, yPos + lineHeight)

                            If i = UltimaCol Then
                                'linea Vertical
                                e.Graphics.DrawLine(Pens.Black, e.MarginBounds.Right + 20, yPos, e.MarginBounds.Right + 20, yPos + lineHeight)

                            End If
                        ElseIf BanBorderDetalle Then
                            'If Indice = 0 Then
                            '    e.Graphics.DrawLine(Pens.Black, leftMargin, yPos, e.MarginBounds.Right + 20, yPos)
                            'End If
                            e.Graphics.DrawLine(Pens.Black, leftMargin, yPos, leftMargin, yPos + lineHeight)
                            If i = UltimaCol - 1 Then
                                'linea Vertical
                                'e.Graphics.DrawLine(Pens.Black, e.MarginBounds.Right + 20, yPos, e.MarginBounds.Right + 20, yPos + lineHeight)
                                e.Graphics.DrawLine(Pens.Black, e.MarginBounds.Right + 20, yPos, e.MarginBounds.Right + 20, yPos + lineHeight)
                            End If

                        Else


                        End If

                        'If NomReporte = "RepEdoCuenta" Or NomReporte = "RepEdoCuentaDet" Then
                        '    If DsReporte.Tables(vNomTabla).DefaultView.Item(Indice).Item("idconcepto") = "201" Then
                        '        e.Graphics.DrawString(IIf(.TipoCampo = TipoDato.TdNumerico, Cadena.PadLeft(12, " "c), Cadena), vFuenteImprimeCamposNeg, Brushes.Black, leftMargin, yPos)
                        '    Else
                        '        e.Graphics.DrawString(IIf(.TipoCampo = TipoDato.TdNumerico, Cadena.PadLeft(12, " "c), Cadena), vFuenteImprimeCampos, Brushes.Black, leftMargin, yPos)
                        '    End If
                        'Else
                        '    'If Band Then

                        '    'End If
                        '    e.Graphics.DrawString(IIf(.TipoCampo = TipoDato.TdNumerico, Cadena.PadLeft(12, " "c), Cadena), vFuenteImprimeCampos, Brushes.Black, leftMargin, yPos)
                        'End If
                        'Campo1 = .Campo

                        leftMargin = leftMargin + (.AnchoCampo * Factor)
                    End If
                End With
            Next

            'Aqui se imprime Detalle (despues de que se imprime toda la linea)
            If NomCampoDet <> "" And ImpDetalle Then
                'ContLineaActual += 1
                'yPos += lineHeight

                vNomTabla_DET = vNomTabla & "_DET"
                NoActual = DsReporte.Tables(vNomTabla).DefaultView.Item(Indice).Item(NomCampoDet)
                'FechaAct = FormatFecHora(DsReporte.Tables(vNomTabla).DefaultView.Item(Indice).Item(NomCampoDet2))
                'FechaAct = DsReporte.Tables(vNomTabla).DefaultView.Item(Indice).Item(NomCampoDet2)
                If DsReporte_DET.Tables(vNomTabla_DET).DefaultView.Count > 0 Then
                    DsReporte_DET.Tables(vNomTabla_DET).DefaultView.RowFilter = Nothing
                    'DsReporte_DET.Tables(vNomTabla_DET).DefaultView.RowFilter = "NoOrden = " & NoActual & " and FECHA = '" & FechaAct & "'"
                    DsReporte_DET.Tables(vNomTabla_DET).DefaultView.RowFilter = "NOMOVIMIENTO = " & NoActual
                    If DsReporte_DET.Tables(vNomTabla_DET).DefaultView.Count > 0 Then
                        'Se imprime Cabeceros del Detalle 
                        yPos += lineHeight
                        leftMargin = e.MarginBounds.Left
                        Cadena = "Código"
                        e.Graphics.DrawString(Cadena, vFuenteLetrasChiquitasEnc, Brushes.Black, leftMargin, yPos)

                        leftMargin = leftMargin + (22 * Factor)
                        Cadena = "     Descripción"
                        e.Graphics.DrawString(Cadena, vFuenteLetrasChiquitasEnc, Brushes.Black, leftMargin, yPos)

                        leftMargin = leftMargin + (200 * Factor)
                        Cadena = "Unidad"
                        e.Graphics.DrawString(Cadena, vFuenteLetrasChiquitasEnc, Brushes.Black, leftMargin, yPos)

                        Select Case UCase(vNomTabla)
                            Case UCase("TRASPFALTANTES")
                                leftMargin = leftMargin + (30 * Factor)
                                Cadena = "Cantidad"
                                e.Graphics.DrawString(Cadena, vFuenteLetrasChiquitasEnc, Brushes.Black, leftMargin, yPos)

                                leftMargin = leftMargin + (30 * Factor)
                                Cadena = "Ingresada"
                                e.Graphics.DrawString(Cadena, vFuenteLetrasChiquitasEnc, Brushes.Black, leftMargin, yPos)
                            Case UCase("TRASPFALTANTESINI")
                                leftMargin = leftMargin + (30 * Factor)
                                Cadena = "Pedida"
                                e.Graphics.DrawString(Cadena, vFuenteLetrasChiquitasEnc, Brushes.Black, leftMargin, yPos)

                                leftMargin = leftMargin + (30 * Factor)
                                Cadena = "Cantidad"
                                e.Graphics.DrawString(Cadena, vFuenteLetrasChiquitasEnc, Brushes.Black, leftMargin, yPos)
                        End Select


                        leftMargin = leftMargin + (30 * Factor)
                        Cadena = "Faltante"
                        e.Graphics.DrawString(Cadena, vFuenteLetrasChiquitasEnc, Brushes.Black, leftMargin, yPos)

                        yPos += lineHeight

                        For k = 0 To DsReporte_DET.Tables(vNomTabla_DET).DefaultView.Count - 1
                            leftMargin = e.MarginBounds.Left
                            'ContLineaActual += 1
                            'leftMargin = leftMargin + (20 * Factor)
                            Cadena = DsReporte_DET.Tables(vNomTabla_DET).DefaultView.Item(k).Item("CCODIGOP01_PRO") & " "
                            e.Graphics.DrawString(Cadena, vFuenteLetrasChiquitas, Brushes.Black, leftMargin, yPos)

                            leftMargin = leftMargin + (22 * Factor)
                            Cadena = DsReporte_DET.Tables(vNomTabla_DET).DefaultView.Item(k).Item("TCNOMBREP01") & " "
                            e.Graphics.DrawString(Cadena, vFuenteLetrasChiquitas, Brushes.Black, leftMargin, yPos)

                            leftMargin = leftMargin + (200 * Factor)
                            Cadena = DsReporte_DET.Tables(vNomTabla_DET).DefaultView.Item(k).Item("CABREVIA01") & " "
                            e.Graphics.DrawString(Cadena, vFuenteLetrasChiquitas, Brushes.Black, leftMargin, yPos)

                            Select Case UCase(vNomTabla)
                                Case UCase("TRASPFALTANTES")
                                    leftMargin = leftMargin + (30 * Factor)
                                    Cadena = DsReporte_DET.Tables(vNomTabla_DET).DefaultView.Item(k).Item("CANTIDAD") & " "
                                    e.Graphics.DrawString(Cadena, vFuenteLetrasChiquitas, Brushes.Black, leftMargin, yPos)

                                    leftMargin = leftMargin + (30 * Factor)
                                    Cadena = DsReporte_DET.Tables(vNomTabla_DET).DefaultView.Item(k).Item("CANTIDADINGRESADA") & " "
                                    e.Graphics.DrawString(Cadena, vFuenteLetrasChiquitas, Brushes.Black, leftMargin, yPos)


                                Case UCase("TRASPFALTANTESINI")
                                    leftMargin = leftMargin + (30 * Factor)
                                    Cadena = DsReporte_DET.Tables(vNomTabla_DET).DefaultView.Item(k).Item("CANTIDADPEDIDA") & " "
                                    e.Graphics.DrawString(Cadena, vFuenteLetrasChiquitas, Brushes.Black, leftMargin, yPos)

                                    leftMargin = leftMargin + (30 * Factor)
                                    Cadena = DsReporte_DET.Tables(vNomTabla_DET).DefaultView.Item(k).Item("CANTIDAD") & " "
                                    e.Graphics.DrawString(Cadena, vFuenteLetrasChiquitas, Brushes.Black, leftMargin, yPos)
                            End Select


                            leftMargin = leftMargin + (30 * Factor)
                            Cadena = DsReporte_DET.Tables(vNomTabla_DET).DefaultView.Item(k).Item("CANTIDADFALTANTE") & " "
                            e.Graphics.DrawString(Cadena, vFuenteLetrasChiquitas, Brushes.Black, leftMargin, yPos)

                            'leftMargin = leftMargin + (30 * Factor)
                            'Cadena = DsReporte_DET.Tables(vNomTabla_DET).DefaultView.Item(k).Item("Importe") & " "
                            'e.Graphics.DrawString(Cadena, vFuenteLetrasChiquitas, Brushes.Black, leftMargin, yPos)

                            yPos += lineHeight

                            If yPos >= yPosFinal Then
                                Exit For
                            End If
                        Next
                    End If
                    DsReporte_DET.Tables(vNomTabla_DET).DefaultView.RowFilter = Nothing
                End If

            End If

            'If NumRegistros_DET Then
            '    If ImpRep.DsReporte_DET.Tables(vNomTabla).DefaultView.Count > 0 Then
            '        ImpRep.DsReporte_DET.Tables(0).DefaultView.RowFilter = Nothing
            '        ImpRep.DsReporte_DET.Tables(0).DefaultView.RowFilter = "NoOrden = " & NoActual
            '        If ImpRep.DsReporte_DET.Tables(vNomTabla).DefaultView.Count > 0 Then
            '            For i = 0 To ImpRep.DsReporte_DET.Tables(0).DefaultView.Count - 1
            '                ImpRep.DsReporte.Tables(0).DefaultView.Item(i).Item("NOORDEN")
            '            Next
            '        End If
            '        ImpRep.DsReporte_DET.Tables(0).DefaultView.RowFilter = Nothing
            '    End If
            'End If






            'yPos += lineHeight
            'e.Graphics.DrawLine(Pens.Black, e.MarginBounds.Left, yPos, e.MarginBounds.Right, yPos)
        Catch ex As Exception
            MsgBox(ex.Message, NomReporte)
        End Try

        Return yPos
    End Function

    'TOTALES
    Private Function CalculaPosXTot(ByVal IndicePos As Single, ByRef leftMargin As Single) As Single
        Dim Posicion As Single = leftMargin
        Dim FactorAncho As Double = 0
        For i As Integer = 0 To IndicePos
            With _ArrColClass(i)
                'leftMargin = leftMargin + (.AnchoCampo * Factor)
                If i = 0 Then
                    Posicion = Posicion
                    FactorAncho = (.AnchoCampo * Factor)
                Else
                    'Posicion = Posicion + (.AnchoCampo * Factor)
                    Posicion = Posicion + FactorAncho
                    FactorAncho = (.AnchoCampo * Factor)
                End If

                If IndicePos = i Then Exit For
            End With
        Next
        CalculaPosXTot = Posicion
    End Function

    'Aqui se imprimen los RESUMENES que se generan en el CUERPO
    Public Function ImprimeResumen(ByVal e As System.Drawing.Printing.PrintPageEventArgs, ByVal yPos As Single, ByVal Indice As Integer) As Single
        yPos += vFuenteTitulo.GetHeight(e.Graphics)
        'Dibuja Linea
        'ANTES - 25/mar/2014
        'e.Graphics.DrawLine(Pens.Black, e.MarginBounds.Left, yPos, e.MarginBounds.Right, yPos)

        'e.Graphics.DrawLine(Pens.Black, e.MarginBounds.Left, yPos + 2, e.MarginBounds.Right, yPos + 2)
        Dim lineHeight As Single = vFuenteImprimeCampos.GetHeight(e.Graphics)
        Dim Cadena As String
        Dim Total As Double
        Dim X1 As Single = e.MarginBounds.Left
        Dim X2, X3, X4, X3A As Single
        Dim Aux As Single
        Dim ValorImp As String
        Dim CadenaObserva As String
        Dim TamanioCadena As Integer
        Dim NumAletra As New NumALetras

        If UCase(NomReporte) = UCase("RepRemision") Then
            'e.Graphics.DrawLine(Pens.Black, e.MarginBounds.Left, yPos, e.MarginBounds.Right, yPos)

            X2 = X1 + 300
            X3 = X2 + 150
            'X4 = X3 + 100 - 20
            X4 = X3 + 330
            X3A = X4 - 150

            'LINEA 1
            CadenaObserva = DsReporte.Tables(vNomTabla).DefaultView.Item(0).Item("OBSERVACIONES")
            TamanioCadena = Len(CadenaObserva)

            ValorImp = "OBSERVACIONES:"
            Cadena = Left(CadenaObserva, 75)
            e.Graphics.DrawString(ValorImp, vFuenteEncabCampos, Brushes.Black, X1, yPos)
            'e.Graphics.DrawString(Cadena, vFuenteEncabCamposOpc, Brushes.Black, X1 + 110, yPos)
            e.Graphics.DrawString(Cadena, vFuenteEncabCamposOpc, Brushes.Black, X1 + 125, yPos)

            e.Graphics.DrawLine(Pens.Black, X3A, yPos, e.MarginBounds.Right, yPos)

            ValorImp = Format((DsReporte.Tables(vNomTabla).DefaultView.Item(0).Item("SUBTOTAL")), "###,###0.00")
            'ValorImp = DsReporte.Tables(vNomTabla).DefaultView.Item(0).Item("SUBTOTAL")
            Cadena = "SUBTOTAL:"
            e.Graphics.DrawString(Cadena, vFuenteEncabCampos, Brushes.Black, X3A, yPos)
            e.Graphics.DrawString(ValorImp.PadLeft(12, " "c), vFuenteEncabCampos, Brushes.Black, X4, yPos)

            'LINEA 2
            yPos += 15
            'CadenaObserva = DsReporte.Tables(vNomTabla).DefaultView.Item(0).Item("OBSERVACIONES")
            'Cadena = Mid(CadenaObserva, 51, 71)
            Cadena = Mid(CadenaObserva, 76, 96)
            e.Graphics.DrawString(Cadena, vFuenteEncabCamposOpc, Brushes.Black, X1, yPos)

            e.Graphics.DrawLine(Pens.Black, X3A, yPos, e.MarginBounds.Right, yPos)
            ValorImp = Format((DsReporte.Tables(vNomTabla).DefaultView.Item(0).Item("DESCUENTO")), "###,###0.00")
            'ValorImp = DsReporte.Tables(vNomTabla).DefaultView.Item(0).Item("DESCUENTO")
            Cadena = "DESCUENTOS"
            e.Graphics.DrawString(Cadena, vFuenteEncabCampos, Brushes.Black, X3A, yPos)
            e.Graphics.DrawString(ValorImp.PadLeft(12, " "c), vFuenteEncabCampos, Brushes.Black, X4, yPos)

            'LINEA 3
            yPos += 15
            'Cadena = Mid(CadenaObserva, 122, 70)
            Cadena = Mid(CadenaObserva, 173, 96)
            e.Graphics.DrawString(Cadena, vFuenteEncabCamposOpc, Brushes.Black, X1, yPos)


            e.Graphics.DrawLine(Pens.Black, X3A, yPos, e.MarginBounds.Right, yPos)
            ValorImp = Format((DsReporte.Tables(vNomTabla).DefaultView.Item(0).Item("IVA")), "###,###0.00")
            'ValorImp = DsReporte.Tables(vNomTabla).DefaultView.Item(0).Item("IVA")
            Cadena = "I.V.A."
            e.Graphics.DrawString(Cadena, vFuenteEncabCampos, Brushes.Black, X3A, yPos)
            e.Graphics.DrawString(ValorImp.PadLeft(12, " "c), vFuenteEncabCampos, Brushes.Black, X4, yPos)

            'LINEA 4
            yPos += 15
            'Cadena = Mid(CadenaObserva, 190, 70)
            Cadena = Mid(CadenaObserva, 269, 96)
            e.Graphics.DrawString(Cadena, vFuenteEncabCamposOpc, Brushes.Black, X1, yPos)

            e.Graphics.DrawLine(Pens.Black, X3A, yPos, e.MarginBounds.Right, yPos)
            Total = DsReporte.Tables(vNomTabla).DefaultView.Item(0).Item("TOTAL")
            ValorImp = Format((DsReporte.Tables(vNomTabla).DefaultView.Item(0).Item("TOTAL")), "###,###0.00")
            'ValorImp = DsReporte.Tables(vNomTabla).DefaultView.Item(0).Item("TOTAL")
            Cadena = "TOTAL"
            e.Graphics.DrawString(Cadena, vFuenteEncabCampos, Brushes.Black, X3A, yPos)
            e.Graphics.DrawString(ValorImp.PadLeft(12, " "c), vFuenteEncabCampos, Brushes.Black, X4, yPos)

            e.Graphics.DrawLine(Pens.Black, X3A, yPos + 15, e.MarginBounds.Right, yPos + 15)
            'LINEAS VERTICALES
            e.Graphics.DrawLine(Pens.Black, X3A, yPos - (15 * 3), X3A, yPos + 15)
            'e.Graphics.DrawLine(Pens.Black, X4, yPos - (15 * 3), X4, yPos + 15)
            'e.Graphics.DrawLine(Pens.Black, e.MarginBounds.Right, yPos, e.MarginBounds.Right, yPos + lineHeight)
            e.Graphics.DrawLine(Pens.Black, e.MarginBounds.Right, yPos - (15 * 3), e.MarginBounds.Right, yPos + 15)

            yPos += 15
            Cadena = "CANTIDAD CON LETRAS:"
            e.Graphics.DrawString(Cadena, vFuenteEncabCampos, Brushes.Black, X1, yPos)

            yPos += 15
            'Cadena = "SON: " & UCase(NumAletra.NumerosALetras(Total)) & " MONEDA NACIONAL"
            Cadena = "SON: " & UCase(NumAletra.NumerosALetras(Total)) & " M.N."
            e.Graphics.DrawString(Cadena, vFuenteEncabCamposOpc, Brushes.Black, X1, yPos)

        ElseIf NomReporte = "RepResPagos" Then
            X2 = X1 + 250
            X3 = X2 + 250
            X4 = X3 + 250
            'LINEA 1
            Cadena = "R E S U M E N "
            e.Graphics.DrawString(Cadena, vFuenteEncabCampos, Brushes.Black, X3, yPos)

            e.Graphics.DrawLine(Pens.Black, e.MarginBounds.Left, yPos, e.MarginBounds.Right, yPos)

            'LINEA 2
            yPos += 15
            Cadena = "SALDO INICIAL"
            e.Graphics.DrawString(Cadena, vFuenteEncabCampos, Brushes.Black, X1, yPos)
            Cadena = "A B O N O S "
            e.Graphics.DrawString(Cadena, vFuenteEncabCampos, Brushes.Black, X2, yPos)
            Cadena = "SALDO ACTUAL"
            e.Graphics.DrawString(Cadena, vFuenteEncabCampos, Brushes.Black, X3, yPos)

            e.Graphics.DrawLine(Pens.Black, e.MarginBounds.Left, yPos, e.MarginBounds.Right, yPos)

            'LINEA 3
            yPos += 15
            Cadena = Format((DsReporte.Tables(vNomTabla).DefaultView.Item(0).Item("ImporteCred")), "###,###0.00")

            e.Graphics.DrawString(Cadena.PadLeft(10, " "c), vFuenteEncabCampos, Brushes.Black, X1, yPos)

            Cadena = Format((DsReporte.Tables(vNomTabla).DefaultView.Item(0).Item("TotalPagado")), "###,###0.00")
            e.Graphics.DrawString(Cadena.PadLeft(10, " "c), vFuenteEncabCampos, Brushes.Black, X2, yPos)

            Cadena = Format((DsReporte.Tables(vNomTabla).DefaultView.Item(0).Item("SaldoActual")), "###,###0.00")
            e.Graphics.DrawString(Cadena.PadLeft(10, " "c), vFuenteEncabCampos, Brushes.Black, X3, yPos)

        ElseIf NomReporte = "RepEdoCuenta" Then
            X2 = X1 + 150
            X3 = X2 + 200
            X4 = X3 + 150

            'LINEA 1
            Cadena = " R E S U M E N "
            e.Graphics.DrawString(Cadena, vFuenteEncabCampos, Brushes.Black, X2, yPos)

            e.Graphics.DrawLine(Pens.Black, e.MarginBounds.Left, yPos, e.MarginBounds.Right, yPos)

            'LINEA 2
            yPos += 15
            Cadena = "C A R G O S "
            e.Graphics.DrawString(Cadena, vFuenteEncabCampos, Brushes.Black, X1, yPos)
            Cadena = "+ M O R A T O R I O S"
            e.Graphics.DrawString(Cadena, vFuenteEncabCampos, Brushes.Black, X2, yPos)
            Cadena = "A B O N O S "
            e.Graphics.DrawString(Cadena, vFuenteEncabCampos, Brushes.Black, X3, yPos)
            Cadena = "S A L D O S"
            e.Graphics.DrawString(Cadena, vFuenteEncabCampos, Brushes.Black, X4, yPos)


            e.Graphics.DrawLine(Pens.Black, e.MarginBounds.Left, yPos, e.MarginBounds.Right, yPos)

            'Dim SubTotCargos, SubTotAbonos, SubTotMoraPag, SubTotMoraPend As Double
            'LINEA 3
            yPos += 15

            'CARGOS es 8 
            Total = _ArrSumaColClass(8)
            Cadena = Format(Total, "###,###0.00")
            e.Graphics.DrawString(Cadena.PadLeft(10, " "c), vFuenteEncabCampos, Brushes.Black, X1, yPos)

            'MORATORIOS ES 9
            Total = _ArrSumaColClass(9)
            Cadena = Format(Total, "###,###0.00")
            e.Graphics.DrawString(Cadena.PadLeft(10, " "c), vFuenteEncabCampos, Brushes.Black, X2, yPos)


            'abonos es 10 + 11
            Total = _ArrSumaColClass(10) + _ArrSumaColClass(11)
            Cadena = Format(Total, "###,###0.00")
            e.Graphics.DrawString(Cadena.PadLeft(10, " "c), vFuenteEncabCampos, Brushes.Black, X3, yPos)

            'saldo es 12
            'Total = _ArrSumaColClass(12)
            Total = _ArrSumaColClass(8) + _ArrSumaColClass(9) - _ArrSumaColClass(10)
            Cadena = Format(Total, "###,###0.00")
            e.Graphics.DrawString(Cadena.PadLeft(10, " "c), vFuenteEncabCampos, Brushes.Black, X4, yPos)
        ElseIf NomReporte = "RepEdoCuentaDet" Then
            X2 = X1 + 350
            X3 = X2 + 350
            X4 = X3 + 350
            Aux = 85

            'LINEA 1
            Cadena = " R E S U M E N "
            e.Graphics.DrawString(Cadena, vFuenteEncabCampos, Brushes.Black, X3, yPos)
            e.Graphics.DrawLine(Pens.Black, e.MarginBounds.Left, yPos, e.MarginBounds.Right, yPos)

            'LINEA 2
            yPos += 15
            Cadena = "C A R G O S "
            e.Graphics.DrawString(Cadena, vFuenteEncabCampos, Brushes.Black, X1, yPos)
            Cadena = "A B O N O S "
            e.Graphics.DrawString(Cadena, vFuenteEncabCampos, Brushes.Black, X2, yPos)
            Cadena = "S A L D O S"
            e.Graphics.DrawString(Cadena, vFuenteEncabCampos, Brushes.Black, X3, yPos)

            e.Graphics.DrawLine(Pens.Black, e.MarginBounds.Left, yPos, e.MarginBounds.Right, yPos)

            'LINEA 3
            yPos += 15
            'CARGOS es 8 
            Cadena = "MENSUALIDAD = "
            e.Graphics.DrawString(Cadena, vFuenteEncabCampos, Brushes.Black, X1, yPos)
            Total = _ArrSumaColClass(8)
            Cadena = Format(Total, "###,###0.00")
            e.Graphics.DrawString(Cadena.PadLeft(10, " "c), vFuenteEncabCampos, Brushes.Black, X1 + Aux, yPos)
            'INTERES ES 10
            Cadena = "INTERES = "
            e.Graphics.DrawString(Cadena, vFuenteEncabCampos, Brushes.Black, X2, yPos)
            Total = _ArrSumaColClass(10)
            Cadena = Format(Total, "###,###0.00")
            e.Graphics.DrawString(Cadena.PadLeft(10, " "c), vFuenteEncabCampos, Brushes.Black, X2 + Aux, yPos)
            'SALDO ES 8 + 9 -13
            Total = _ArrSumaColClass(8) + _ArrSumaColClass(9) - _ArrSumaColClass(13)
            Cadena = Format(Total, "###,###0.00")
            e.Graphics.DrawString(Cadena.PadLeft(10, " "c), vFuenteEncabCampos, Brushes.Black, X3, yPos)

            'LINEA 4
            yPos += 15
            'MORATORIOS ES 9
            Cadena = "MORATORIO = "
            e.Graphics.DrawString(Cadena, vFuenteEncabCampos, Brushes.Black, X1, yPos)
            Total = _ArrSumaColClass(9)
            Cadena = Format(Total, "###,###0.00")
            e.Graphics.DrawString(Cadena.PadLeft(10, " "c), vFuenteEncabCampos, Brushes.Black, X1 + Aux, yPos)
            'GASTOS ES 11
            Cadena = "GASTOS = "
            e.Graphics.DrawString(Cadena, vFuenteEncabCampos, Brushes.Black, X2, yPos)
            Total = _ArrSumaColClass(11)
            Cadena = Format(Total, "###,###0.00")
            e.Graphics.DrawString(Cadena.PadLeft(10, " "c), vFuenteEncabCampos, Brushes.Black, X2 + Aux, yPos)

            'LINEA 5
            yPos += 15
            'TOTAL DE CARGOS ES 8 + 9 
            Cadena = "TOT CARGOS = "
            e.Graphics.DrawString(Cadena, vFuenteEncabCampos, Brushes.Black, X1, yPos)
            Total = _ArrSumaColClass(8) + _ArrSumaColClass(9)
            Cadena = Format(Total, "###,###0.00")
            e.Graphics.DrawString(Cadena.PadLeft(10, " "c), vFuenteEncabCampos, Brushes.Black, X1 + Aux, yPos)
            'CAPITAL ES 12
            Cadena = "CAPITAL = "
            e.Graphics.DrawString(Cadena, vFuenteEncabCampos, Brushes.Black, X2, yPos)
            Total = _ArrSumaColClass(12)
            Cadena = Format(Total, "###,###0.00")
            e.Graphics.DrawString(Cadena.PadLeft(10, " "c), vFuenteEncabCampos, Brushes.Black, X2 + Aux, yPos)

            'LINEA 6
            yPos += 15
            'TOTAL ABONOS ES 13
            Cadena = "TOT ABONOS = "
            e.Graphics.DrawString(Cadena, vFuenteEncabCampos, Brushes.Black, X2, yPos)
            Total = _ArrSumaColClass(13)
            Cadena = Format(Total, "###,###0.00")
            e.Graphics.DrawString(Cadena.PadLeft(10, " "c), vFuenteEncabCampos, Brushes.Black, X2 + Aux, yPos)


            'Cadena = "+ M O R A T O R I O S"
            'e.Graphics.DrawString(Cadena, vFuenteEncabCampos, Brushes.Black, X2, yPos)
            'Cadena = "A B O N O S "
            'e.Graphics.DrawString(Cadena, vFuenteEncabCampos, Brushes.Black, X3, yPos)
            'Cadena = "S A L D O S"
            'e.Graphics.DrawString(Cadena, vFuenteEncabCampos, Brushes.Black, X4, yPos)




            'Dim SubTotCargos, SubTotAbonos, SubTotMoraPag, SubTotMoraPend As Double




            ''abonos es 10 + 11
            'Total = _ArrSumaColClass(10) + _ArrSumaColClass(11)
            'Cadena = Format(Total, "###,###0.00")
            'e.Graphics.DrawString(Cadena.PadLeft(10, " "c), vFuenteEncabCampos, Brushes.Black, X3, yPos)

            ''saldo es 12
            ''Total = _ArrSumaColClass(12)
            'Total = _ArrSumaColClass(8) + _ArrSumaColClass(9) - _ArrSumaColClass(10)
            'Cadena = Format(Total, "###,###0.00")
            'e.Graphics.DrawString(Cadena.PadLeft(10, " "c), vFuenteEncabCampos, Brushes.Black, X4, yPos)

        End If
        'Dibuja Linea
        yPos += 15
        'ANTES - 25/mar/2014
        'e.Graphics.DrawLine(Pens.Black, e.MarginBounds.Left, yPos, e.MarginBounds.Right, yPos)


        'e.Graphics.DrawLine(Pens.Black, e.MarginBounds.Left, yPos + 2, e.MarginBounds.Right, yPos + 2)

        ReiniciaArreglos()
        Return yPos
    End Function
    'Aqui se imprimen los totales que se generan en el CUERPO
    Public Function ImprimeTotal(ByVal e As System.Drawing.Printing.PrintPageEventArgs, ByVal yPos As Single, ByVal Indice As Integer) As Single

        Dim lineHeight As Single = vFuenteImprimeCampos.GetHeight(e.Graphics)
        Dim leftMargin2 As Single = e.MarginBounds.Left
        Dim leftMargin As Single = e.MarginBounds.Left
        Dim Cadena As String

        'imprimir Datos
        leftMargin = e.MarginBounds.Left

        'Dibuja Linea
        yPos += 10 + 5
        e.Graphics.DrawLine(Pens.Black, e.MarginBounds.Left, yPos, e.MarginBounds.Right, yPos)
        e.Graphics.DrawLine(Pens.Black, e.MarginBounds.Left, yPos + 2, e.MarginBounds.Right, yPos + 2)
        yPos += 5

        For i As Integer = 0 To _ArrColClass.Length - 1
            With _ArrColClass(i)
                'If .BImprimeCampo Then
                If .TipoCampo = TipoDato.TdNumerico Then
                    If .BSuma Then
                        'Calcular el lugar donde se debe de 
                        leftMargin = CalculaPosXTot(i, leftMargin2)
                        Cadena = Format((_ArrSumaColClass(i)), "###,###0.00")
                        e.Graphics.DrawString(IIf(.TipoCampo = TipoDato.TdNumerico, Cadena.PadLeft(12, " "c), Cadena), vFuenteImprimeCampos, Brushes.Black, leftMargin, yPos)
                        'leftMargin = leftMargin + (.AnchoCampo * Factor)
                    End If
                End If

                'If .TipoCampo = TipoDato.TdFecha Then
                '    Cadena = CDate(DsReporte.Tables(vNomTabla).DefaultView.Item(Indice).Item(.Campo) & "").ToShortDateString
                'ElseIf .TipoCampo = TipoDato.TdBoolean Then
                '    Cadena = IIf(DsReporte.Tables(vNomTabla).DefaultView.Item(Indice).Item(.Campo), "    *", "")
                'ElseIf .TipoCampo = TipoDato.TdNumerico Then

                '    Cadena = Format((_ArrSumaColClass(i)), "###,###0.00")
                'Else
                '    Cadena = DsReporte.Tables(vNomTabla).DefaultView.Item(Indice).Item(.Campo) & ""
                'End If
                'e.Graphics.DrawString(IIf(.TipoCampo = TipoDato.TdNumerico, Cadena.PadLeft(10, " "c), Cadena), vFuenteImprimeCampos, Brushes.Black, leftMargin, yPos)
                'leftMargin = leftMargin + (.AnchoCampo * Factor)

                'End If
            End With
        Next

        Return yPos
    End Function

    Public Function ImprimeTotalGrupo(ByVal e As System.Drawing.Printing.PrintPageEventArgs, ByVal yPos As Single, ByVal Indice As Integer) As Single
        Dim leftMargin As Single = e.MarginBounds.Left
        Dim leftMargin2 As Single = e.MarginBounds.Left
        Dim lineHeight As Single = vFuenteImprimeCampos.GetHeight(e.Graphics)
        'Dim Cadena, TotalGrupo As String
        Dim Cadena As String

        'Cadena = "TOTAL DEL GRUPO ( " & GrupoAct & " )"
        Cadena = "TOTAL DE ( " & GrupoAct & " )"
        'leftMargin = CalculaPosXTot(0, leftMargin)
        e.Graphics.DrawString(Cadena, vFuenteImprimeCampos, Brushes.Black, leftMargin, yPos)

        'dibuja linea
        e.Graphics.DrawLine(Pens.Black, e.MarginBounds.Left, yPos, e.MarginBounds.Right, yPos)

        yPos += 5
        'ANTESSSSSSS
        'imprime total
        'TotalGrupo = Format((_ArrSumaColClass(Indice)), "###,###0.00")
        'PosTotGrupo = CalculaPosXTot(IndiceTotalGrupo, leftMargin)
        'e.Graphics.DrawString(TotalGrupo.PadLeft(12, " "c), vFuenteImprimeCampos, Brushes.Black, PosTotGrupo, yPos)
        'yPos += lineHeight
        ''yPos += lineHeight
        '_ArrSumaColClass(Indice) = 0

        For i As Integer = 0 To _ArrColClass.Length - 1
            With _ArrColClass(i)
                'If .BImprimeCampo Then
                If .TipoCampo = TipoDato.TdNumerico Then
                    If .BSuma Then
                        'Calcular el lugar donde se debe de 
                        leftMargin = CalculaPosXTot(i, leftMargin2)
                        Cadena = Format((_ArrSumaColClassGpo(i)), "###,###0.00")
                        e.Graphics.DrawString(IIf(.TipoCampo = TipoDato.TdNumerico, Cadena.PadLeft(12, " "c), Cadena), vFuenteImprimeCampos, Brushes.Black, leftMargin, yPos)
                        'leftMargin = leftMargin + (.AnchoCampo * Factor)
                    End If
                End If

                
            End With
        Next

        For i As Integer = 0 To _ArrColClass.Length - 1
            With _ArrColClass(i)
                If .TipoCampo = TipoDato.TdNumerico Then
                    If .BSuma Then
                        _ArrSumaColClassGpo(i) = 0
                    End If
                End If
            End With
            

        Next

        Return yPos
    End Function

    'PIE DE PAGINA
    Public Function PiePagina(ByVal e As System.Drawing.Printing.PrintPageEventArgs, ByVal yPos As Single, _
                              Optional ByVal Pagina As Integer = 0, Optional ByVal NoPaginas As Integer = 0, Optional ByVal Cadena As String = "") As Single
        'Alineacion
        AligIzquierda = e.MarginBounds.Left
        AligCentro = ((e.MarginBounds.Left + e.MarginBounds.Right) / 2) - 20
        AligDerecha = e.MarginBounds.Right - 50

        Dim lineHeight As Single = vFuenteEncabezado.GetHeight(e.Graphics)
        Dim leftMargin As Single = e.MarginBounds.Left

        Dim Cont As Single = 0

        'Dim BandAligIzq As Boolean = False
        'Dim BandAligCen As Boolean = False
        'Dim BandAligDer As Boolean = False

        'If UCase(NomReporte) = UCase("RepRemision") Then
        'AQUI ME QUEDE
        If Cadena <> "" Then
            e.Graphics.DrawString(Cadena, vFuenteLetrasChiquitas, Brushes.Black, AligIzquierda, yPos)
            yPos += 65
        End If


        'End If


        'Dibuja Linea

        e.Graphics.DrawLine(Pens.Black, e.MarginBounds.Left, yPos, e.MarginBounds.Right, yPos)
        e.Graphics.DrawLine(Pens.Black, e.MarginBounds.Left, yPos + 2, e.MarginBounds.Right, yPos + 2)
        yPos += 5

        If vBandPaginacion Then
            'Se pagina 1 de n

            If CType(vAligPaginacion, TipoAlineacion) = TipoAlineacion.TdIzquierda Then
                leftMargin = AligIzquierda
            ElseIf CType(vAligPaginacion, TipoAlineacion) = TipoAlineacion.TdCentrado Then
                leftMargin = AligCentro
            ElseIf CType(vAligPaginacion, TipoAlineacion) = TipoAlineacion.TdDerecha Then
                leftMargin = AligDerecha
            End If
            e.Graphics.DrawString("Pagina " & Pagina & " de " & NoPaginas, vFuenteEncabCampos, Brushes.Black, leftMargin - 40, yPos)
            Cont += 1
        End If

        If vBanSoloPagina Then
            'solo se pone la pagina 1
            If CType(vAligSoloPagina, TipoAlineacion) = TipoAlineacion.TdIzquierda Then
                leftMargin = AligIzquierda
            ElseIf CType(vAligSoloPagina, TipoAlineacion) = TipoAlineacion.TdCentrado Then
                leftMargin = AligCentro
            ElseIf CType(vAligSoloPagina, TipoAlineacion) = TipoAlineacion.TdDerecha Then
                leftMargin = AligDerecha
            End If
            e.Graphics.DrawString("Pagina " & Pagina, vFuenteEncabCampos, Brushes.Black, leftMargin, yPos)
            Cont += 1
        End If

        If vPieParametro1 <> "" Then
            If CType(vAligPieParametro1, TipoAlineacion) = TipoAlineacion.TdIzquierda Then
                leftMargin = AligIzquierda
            ElseIf CType(vAligPieParametro1, TipoAlineacion) = TipoAlineacion.TdCentrado Then
                leftMargin = AligCentro
            ElseIf CType(vAligPieParametro1, TipoAlineacion) = TipoAlineacion.TdDerecha Then
                leftMargin = AligDerecha
            End If
            e.Graphics.DrawString(vPieParametro1, vFuenteEncabCampos, Brushes.Black, leftMargin, yPos)
            Cont += 1
        End If


        If vPieParametro2 <> "" And Cont < 3 Then
            If CType(vAligPieParametro2, TipoAlineacion) = TipoAlineacion.TdIzquierda Then
                leftMargin = AligIzquierda
            ElseIf CType(vAligPieParametro2, TipoAlineacion) = TipoAlineacion.TdCentrado Then
                leftMargin = AligCentro
            ElseIf CType(vAligPieParametro2, TipoAlineacion) = TipoAlineacion.TdDerecha Then
                leftMargin = AligDerecha
            End If
            e.Graphics.DrawString(vPieParametro2, vFuenteEncabCampos, Brushes.Black, leftMargin, yPos)
            Cont += 1
        End If

        If vPieParametro3 <> "" And Cont < 3 Then
            If CType(vAligPieParametro3, TipoAlineacion) = TipoAlineacion.TdIzquierda Then
                leftMargin = AligIzquierda
            ElseIf CType(vAligPieParametro3, TipoAlineacion) = TipoAlineacion.TdCentrado Then
                leftMargin = AligCentro
            ElseIf CType(vAligPieParametro3, TipoAlineacion) = TipoAlineacion.TdDerecha Then
                leftMargin = AligDerecha
            End If
            e.Graphics.DrawString(vPieParametro3, vFuenteEncabCampos, Brushes.Black, leftMargin, yPos)
        End If


        Return yPos
    End Function
    'Lineas Finales 
    'Numero de paginas
    'Fecha o lo que se requiera como parte final de la hoja

End Class
