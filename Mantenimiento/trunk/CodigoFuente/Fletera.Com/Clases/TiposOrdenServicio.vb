﻿Public Class TiposOrdenServicio
    Private _idTipOrdServ As String
    Private _nomTipOrdServ As String
    Private _bOrdenTrab As Boolean
    Private _Existe As Boolean

    Dim MyTabla As DataTable = New DataTable(_TablaBd)
    Private _TablaBd As String = "CatTipoOrdServ"
    Dim StrSql As String
    Dim DsClase As New DataTable

    Public Property idTipoOrden As String
        Get
            Return _idTipOrdServ
        End Get
        Set(ByVal value As String)
            _idTipOrdServ = value
        End Set
    End Property


    Public Property bOrdenTrab As Boolean
        Get
            Return _bOrdenTrab
        End Get
        Set(ByVal value As Boolean)
            _bOrdenTrab = value
        End Set
    End Property

    Public Property nomTipOrdServ As String
        Get
            Return _nomTipOrdServ
        End Get
        Set(ByVal value As String)
            _nomTipOrdServ = value
        End Set
    End Property
    Public Property Existe As Boolean
        Get
            Return _Existe
        End Get
        Set(ByVal value As Boolean)
            _Existe = value
        End Set
    End Property

    Sub New(ByVal vidTipOrdServ As Integer)
        'Dim DtEst As New DataTable
        _idTipOrdServ = vidTipOrdServ


        StrSql = "SELECT tos.idTipOrdServ, " &
        "tos.NomTipOrdServ, " &
        "tos.NumOrden, " &
        "tos.bOrdenTrab " &
        "FROM dbo." & _TablaBd & " TOS " &
        "WHERE tos.idTipOrdServ = " & vidTipOrdServ


        DsClase = BD.ExecuteReturn(StrSql)
        If DsClase.Rows.Count > 0 Then
            With DsClase.Rows(0)
                _nomTipOrdServ = .Item("nomTipOrdServ") & ""
                _bOrdenTrab = .Item("bOrdenTrab")
            End With
            _Existe = True
        Else
            _Existe = False
        End If

    End Sub
    Public Function TablaRelTipOrdServ(ByVal FiltroSinWhere As String) As DataTable
        MyTabla.Rows.Clear()

        StrSql = "SELECT rel.idTipOrdServ, " &
        "rel.NomCatalogo, " &
        "rel.NombreId, " &
        "rel.IdRelacion " &
        "FROM dbo.RelTipOrdServ rel " & IIf(FiltroSinWhere = "", "", " WHERE " & FiltroSinWhere)

        '"WHERE rel.idTipOrdServ = 1"


        MyTabla = BD.ExecuteReturn(StrSql)

        Return MyTabla
    End Function



End Class
