﻿Public Class SAP
    Private _NoViaje As String
    Private _NoSAP As String
    Public Property NoViaje As String
        Get
            Return _NoViaje
        End Get
        Set(ByVal value As String)
            _NoViaje = value
        End Set
    End Property
    Public Property NoSAP As String
        Get
            Return _NoSAP
        End Get
        Set(ByVal value As String)
            _NoSAP = value
        End Set
    End Property

    Public Sub New(ByVal vNoViaje As String, ByVal vNoSAP As String)
        _NoViaje = vNoViaje
        _NoSAP = vNoSAP
    End Sub
End Class
