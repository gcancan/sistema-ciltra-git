﻿Public Class Preliquida
    Private _NoViaje As String
    Private _Precio As String
    Public Property NoViaje As String
        Get
            Return _NoViaje
        End Get
        Set(ByVal value As String)
            _NoViaje = value
        End Set
    End Property
    Public Property Precio As String
        Get
            Return _Precio
        End Get
        Set(ByVal value As String)
            _Precio = value
        End Set
    End Property

    Public Sub New(ByVal vNoViaje As String, ByVal vPrecio As String)
        _NoViaje = vNoViaje
        _Precio = vPrecio
    End Sub
End Class
