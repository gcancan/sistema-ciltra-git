﻿Public Class CamposMigraCOMClass
    'CLASIFICACIONES

    Private _cClasificacionDe As Integer
    Public Property cClasificacionDe As Integer
        Get
            Return _cClasificacionDe
        End Get
        Set(value As Integer)
            _cClasificacionDe = value
        End Set
    End Property

    Private _cNumClasificacion As Integer
    Public Property cNumClasificacion As Integer
        Get
            Return _cNumClasificacion
        End Get
        Set(value As Integer)
            _cNumClasificacion = value
        End Set
    End Property
    Private _cCodigoValorClasificacion As String
    Public Property cCodigoValorClasificacion As String
        Get
            Return _cNumClasificacion
        End Get
        Set(value As String)
            _cNumClasificacion = value
        End Set
    End Property
    Private _cValorClasificacion As String
    Public Property cValorClasificacion As String
        Get
            Return _cValorClasificacion
        End Get
        Set(value As String)
            _cValorClasificacion = value
        End Set
    End Property

    'PRODUCTOS
    Private _CCODIGOPRODUCTO As String
    Public Property CCODIGOPRODUCTO As String
        Get
            Return _CCODIGOPRODUCTO
        End Get
        Set(ByVal value As String)
            _CCODIGOPRODUCTO = value
        End Set
    End Property

    Private _CNOMBREPRODUCTO As String
    Public Property CNOMBREPRODUCTO As String
        Get
            Return _CNOMBREPRODUCTO
        End Get
        Set(ByVal value As String)
            _CNOMBREPRODUCTO = value
        End Set
    End Property

    Private _cCodigoValorClasificacion1 As String
    Public Property cCodigoValorClasificacion1 As String
        Get
            Return _cCodigoValorClasificacion1
        End Get
        Set(ByVal value As String)
            _cCodigoValorClasificacion1 = value
        End Set
    End Property

    Private _CMODELOPRODUCTO As String
    Public Property CMODELOPRODUCTO As String
        Get
            Return _CMODELOPRODUCTO
        End Get
        Set(ByVal value As String)
            _CMODELOPRODUCTO = value
        End Set
    End Property

    Private _CCODIGOALTERNO As String
    Public Property CCODIGOALTERNO As String
        Get
            Return _CCODIGOALTERNO
        End Get
        Set(ByVal value As String)
            _CCODIGOALTERNO = value
        End Set
    End Property




End Class
