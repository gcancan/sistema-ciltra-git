﻿Public Class ComboChek
    Inherits ComboBox
    Private WithEvents _chkCmb As New CheckBox With {.Text = "", .AutoSize = True, .Checked = True}
    Sub New()
        Me.AutoCompleteSource = Windows.Forms.AutoCompleteSource.ListItems
        Me.AutoCompleteMode = Windows.Forms.AutoCompleteMode.SuggestAppend
    End Sub
    Public Property ChekCmb As CheckBox
        Get
            Return _chkCmb
        End Get
        Set(ByVal value As CheckBox)
            _chkCmb = value
        End Set
    End Property
    Private Sub _chkCmb_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _chkCmb.CheckedChanged
        Me.Enabled = _chkCmb.Checked
        If _chkCmb.Checked = False Then
            Me.Text = ""
        End If
    End Sub
    Public Property Checked As Boolean
        Get
            Return _chkCmb.Checked
        End Get
        Set(ByVal value As Boolean)
            _chkCmb.Checked = value
        End Set
    End Property
    
End Class
