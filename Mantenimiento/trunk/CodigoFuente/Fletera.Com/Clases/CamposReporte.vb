﻿Public Class CamposReporte
    Private _Campo As String
    Private _DesCampo As String
    Private _AnchoCampo As Integer
    Private _BImprimeCampo As Boolean
    Private _TipoCampo As TipoDato
    Private _BSuma As Boolean
    Private _BGrupo As Boolean
    Private _CampoSumaGrupo As String
    'Private _BfiltraCampo As Boolean
    'Private _BOrdenCampo As Boolean
    'Private _BOrdenCampoTipo As TipoOrdenamiento
    'Private _TipControlCampo As TipoControlReport
    'Private _DesValorTrue As String
    'Private _DesValorFalse As String
#Region "Inicializacion"
    Sub New()
        '_BOrdenCampo = EnTipoOrden.Ninguno
        '_BfiltraCampo = False
    End Sub
    Sub New(ByVal vCampo As String, ByVal vDescCampo As String, _
        ByVal vBImprimeCampo As Boolean, ByVal vAnchoCampo As Single, ByVal vTipoCampo As TipoDato, ByVal vSuma As Boolean, _
        ByVal vBGrupo As Boolean, ByVal vCampoSumaGrupo As String)
        _Campo = vCampo
        _DesCampo = vDescCampo
        _BImprimeCampo = vBImprimeCampo
        _AnchoCampo = vAnchoCampo
        _TipoCampo = vTipoCampo
        _BSuma = vSuma
        _BGrupo = vBGrupo
        _CampoSumaGrupo = vCampoSumaGrupo
    End Sub
#End Region
#Region "Propiedades"
    Public Property Campo As String
        Get
            Return _Campo
        End Get
        Set(ByVal value As String)
            _Campo = value
        End Set
    End Property
    Public Property DesripcionCampo As String
        Get
            Return _DesCampo
        End Get
        Set(ByVal value As String)
            _DesCampo = value
        End Set
    End Property
    Public Property BImprimeCampo As Boolean
        Get
            Return _BImprimeCampo
        End Get
        Set(ByVal value As Boolean)
            _BImprimeCampo = value
        End Set
    End Property
    Public Property AnchoCampo As Integer
        Get
            Return _AnchoCampo
        End Get
        Set(ByVal value As Integer)
            _AnchoCampo = value
        End Set
    End Property
    Public Property TipoCampo As TipoDato
        Get
            Return _TipoCampo
        End Get
        Set(ByVal value As TipoDato)
            _TipoCampo = value
        End Set
    End Property
    Public Property BSuma As Boolean
        Get
            Return _BSuma
        End Get
        Set(ByVal value As Boolean)
            _BSuma = value
        End Set
    End Property
    Public Property BGrupo As Boolean
        Get
            Return _BGrupo
        End Get
        Set(ByVal value As Boolean)
            _BGrupo = value
        End Set
    End Property
    '
    Public Property CampoSumaGrupo As String
        Get
            Return _CampoSumaGrupo
        End Get
        Set(ByVal value As String)
            _CampoSumaGrupo = value
        End Set
    End Property

#End Region
End Class
