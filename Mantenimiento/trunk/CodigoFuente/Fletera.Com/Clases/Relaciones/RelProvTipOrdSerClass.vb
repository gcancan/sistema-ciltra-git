﻿Public Class RelProvTipOrdSerClass
    Private _idTipOrdServ As Integer
    Private _NomTipOrdServ As String
    Private _idProveedor As Integer
    Private _RazonSocial As String
    Private _Activo As Boolean


    Private _Existe As Boolean
    Private _TablaBd As String = "RelProvTipOrdSer"

    Dim StrSql As String
    Dim DsClase As New DataTable
    Dim MyTabla As DataTable = New DataTable(_TablaBd)

#Region "Constructor"
    Sub New(ByVal vidProveedor As Integer, ByVal vidTipOrdServ As Integer)

        StrSql = "SELECT rel.idTipOrdServ, " &
        "t.NomTipOrdServ, " &
        "rel.idProveedor, " &
        "p.RazonSocial, " &
        "rel.Activo " &
        "FROM " & _TablaBd & " Rel " &
        "INNER JOIN CatTipoOrdServ T ON T.idTipOrdServ = Rel.idTipOrdServ " &
        "INNER JOIN dbo.CatProveedores P ON P.idProveedor = Rel.idProveedor " &
        "WHERE Rel.idTipOrdServ = " & vidTipOrdServ & " AND rel.idProveedor = " & vidProveedor

        _idTipOrdServ = vidTipOrdServ
        _idProveedor = vidProveedor

        DsClase = BD.ExecuteReturn(StrSql)
        If DsClase.Rows.Count > 0 Then
            With DsClase.Rows(0)
                _NomTipOrdServ = .Item("NomTipOrdServ") & ""
                _RazonSocial = .Item("RazonSocial") & ""
                _Activo = .Item("Activo")

            End With
            _Existe = True
        Else
            _Existe = False
        End If
    End Sub

    Sub New(ByVal vidTipOrdServ As Integer, ByVal vNomTipOrdServ As String, ByVal vidProveedor As Integer,
    ByVal vRazonSocial As String, ByVal vActivo As Boolean)
        _idTipOrdServ = vidTipOrdServ
        _NomTipOrdServ = vNomTipOrdServ
        _idProveedor = vidProveedor
        _RazonSocial = vRazonSocial
        _Activo = vActivo
        _Existe = True

    End Sub
#End Region

#Region "Propiedades"
    Public Property idTipOrdServ As Integer
        Get
            Return _idTipOrdServ
        End Get
        Set(ByVal value As Integer)
            _idTipOrdServ = value
        End Set
    End Property
    Public Property NomTipOrdServ As String
        Get
            Return _NomTipOrdServ
        End Get
        Set(ByVal value As String)
            _NomTipOrdServ = value
        End Set
    End Property
    Public Property idProveedor As Integer
        Get
            Return _idProveedor
        End Get
        Set(ByVal value As Integer)
            _idProveedor = value
        End Set
    End Property
    Public Property RazonSocial As String
        Get
            Return _RazonSocial
        End Get
        Set(ByVal value As String)
            _RazonSocial = value
        End Set
    End Property
    Public Property Activo As Boolean
        Get
            Return _Activo
        End Get
        Set(ByVal value As Boolean)
            _Activo = value
        End Set
    End Property
    Public Property Existe As Boolean
        Get
            Return _Existe
        End Get
        Set(ByVal value As Boolean)
            _Existe = value
        End Set
    End Property
#End Region

#Region "Funciones"
    Public Function GetCambios(ByVal vActivo As Boolean) As String

        Dim CadCam As String = ""

        If _Activo <> vActivo Then
            CadCam += "Activo: '" & _Activo & "' Cambia a [" & vActivo & "],"
        End If

        If CadCam <> "" Then
            CadCam = Mid(CadCam, 1, CadCam.Length - 1)
        End If
        Return CadCam
    End Function

    Public Sub Guardar(ByVal bEsIdentidad As Boolean, ByVal vidTipOrdServ As Integer, ByVal vidProveedor As Integer, ByVal vActivo As Boolean)
        Dim sSql As String = "", _CadValAnt As String = "", _CadValNue As String = ""
        If _Existe Then
            'If _idTipOrdServ <> vidTipOrdServ Then
            '    sSql += "idTipOrdServ = " & vidTipOrdServ & ","
            '    _CadValAnt += "idTipOrdServ =" & vidTipOrdServ & ","
            'End If
            'If _idMarca <> vidMarca Then
            '    sSql += "idMarca = " & vidMarca & ","
            '    _CadValAnt += "idMarca =" & vidMarca & ","
            'End If

            If _Activo <> vActivo Then
                sSql += "Activo = '" & IIf(vActivo, 1, 0) & "',"
                _CadValAnt += "Activo =" & vActivo & ","
            End If



            If sSql <> "" Then
                sSql = Mid(sSql, 1, sSql.Length - 1) 'Para quitar la Coma ','
                _CadValAnt = "idProveedor=" & vidProveedor & "," & _CadValAnt
                _CadValAnt = "idTipOrdServ=" & vidTipOrdServ & "," & _CadValAnt

                _CadValAnt = Mid(_CadValAnt, 1, _CadValAnt.Length - 1)
                _CadValNue = Replace(sSql, "'", "")
                sSql = "UPDATE " & _TablaBd & " SET " & sSql & " WHERE idProveedor = " & _idProveedor & " and idTipOrdServ = " & _idTipOrdServ
                Try
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                    BD.Execute(sSql, True)
                    Audita(_TablaBd, _CadValAnt, _CadValNue, EnTipAccion.Modifica)
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
                Catch ex As Exception
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                    Throw New Exception(ex.Message)
                End Try
            End If
        Else
            If bEsIdentidad Then
                sSql = "INSERT INTO " & _TablaBd & "(idTipOrdServ,idProveedor,Activo) VALUES(" & vidTipOrdServ & "," & vidProveedor & "," & IIf(vActivo, 1, 0) & ")"

                _CadValNue = "idTipOrdServ=" & vidTipOrdServ & ", idProveedor = " & vidProveedor & ",Activo = " & vActivo
            Else

                sSql = "INSERT INTO " & _TablaBd & "(idTipOrdServ,idProveedor,Activo) VALUES(" & vidTipOrdServ & "," & vidProveedor & "," & IIf(vActivo, 1, 0) & ")"

                _CadValNue = "idTipOrdServ=" & vidTipOrdServ & ", idProveedor = " & vidProveedor & ",Activo = " & vActivo
            End If

            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, "", _CadValNue, EnTipAccion.Inserta)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
        End If
        _Activo = vActivo
    End Sub
#End Region

#Region "Consultas"
    Public Function TablaProveedoresxTos(ByVal bActivo As Boolean, ByVal tipoOS As TipoOrdenServicio, Optional ByVal FiltroSinWhere As String = "") As DataTable
        Dim filtro As String = ""
        MyTabla.Rows.Clear()

        If FiltroSinWhere <> "" Then
            filtro = " where " & FiltroSinWhere
        End If

        StrSql = "SELECT rel.idTipOrdServ, " &
        "t.NomTipOrdServ, " &
        "rel.idProveedor, " &
        "p.RazonSocial, " &
        "rel.Activo " &
        "FROM " & _TablaBd & " Rel " &
        "INNER JOIN CatTipoOrdServ T ON T.idTipOrdServ = Rel.idTipOrdServ " &
        "INNER JOIN dbo.CatProveedores P ON P.idProveedor = Rel.idProveedor " &
        "WHERE rel.idTipOrdServ = " & tipoOS & " AND rel.Activo = " & IIf(bActivo, 1, 0)

        MyTabla = BD.ExecuteReturn(StrSql)

        Return MyTabla

    End Function
#End Region

End Class
