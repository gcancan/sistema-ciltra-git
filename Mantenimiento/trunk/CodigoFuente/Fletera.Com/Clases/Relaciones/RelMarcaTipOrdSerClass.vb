﻿Public Class RelMarcaTipOrdSerClass
    Private _idTipOrdServ As Integer
    Private _NomTipOrdServ As String
    Private _idMarca As Integer
    Private _NombreMarca As String
    Private _Activo As Boolean

    Private _Existe As Boolean
    Private _TablaBd As String = "RelMarcaTipOrdSer"

    Dim StrSql As String
    Dim DsClase As New DataTable
    Dim MyTabla As DataTable = New DataTable(_TablaBd)

#Region "Constructor"
    Sub New(ByVal vidMarca As Integer, ByVal vidTipOrdServ As Integer)
        _idMarca = vidMarca
        _idTipOrdServ = vidTipOrdServ

        StrSql = "SELECT rel.idMarca, " &
        "m.NombreMarca, " &
        "rel.idTipOrdServ, " &
        "t.NomTipOrdServ, rel.activo " &
        "FROM " & _TablaBd & " rel " &
        "INNER JOIN dbo.CatMarcas M ON M.idMarca = rel.idMarca " &
        "INNER JOIN CatTipoOrdServ T ON T.idTipOrdServ = rel.idTipOrdServ " &
        "WHERE rel.idMarca = " & vidMarca & " AND rel.idTipOrdServ = " & vidTipOrdServ

        DsClase = BD.ExecuteReturn(StrSql)
        If DsClase.Rows.Count > 0 Then
            With DsClase.Rows(0)
                '_idTipOrdServ = .Item("idTipOrdServ")
                _NomTipOrdServ = .Item("NomTipOrdServ") & ""
                '_idMarca = .Item("idMarca")
                _NombreMarca = .Item("NombreMarca") & ""
                _Activo = .Item("Activo")

            End With
            _Existe = True
        Else
            _Existe = False
        End If
    End Sub

    Sub New(ByVal vidTipOrdServ As Integer, ByVal vNomTipOrdServ As String, ByVal vidMarca As Integer, ByVal vNombreMarca As String, ByVal vActivo As Boolean)
        _idTipOrdServ = vidTipOrdServ
        _NomTipOrdServ = vNomTipOrdServ
        _idMarca = vidMarca
        _NombreMarca = vNombreMarca
        _Activo = vActivo

    End Sub
#End Region

#Region "Propiedades"
    Public Property idTipOrdServ As Integer
        Get
            Return _idTipOrdServ
        End Get
        Set(ByVal value As Integer)
            _idTipOrdServ = value
        End Set
    End Property
    Public Property NomTipOrdServ As String
        Get
            Return _NomTipOrdServ
        End Get
        Set(ByVal value As String)
            _NomTipOrdServ = value
        End Set
    End Property
    Public Property idMarca As Integer
        Get
            Return _idMarca
        End Get
        Set(ByVal value As Integer)
            _idMarca = value
        End Set
    End Property
    Public Property NombreMarca As String
        Get
            Return _NombreMarca
        End Get
        Set(ByVal value As String)
            _NombreMarca = value
        End Set
    End Property
    Public Property Activo As Boolean
        Get
            Return _Activo
        End Get
        Set(ByVal value As Boolean)
            _Activo = value
        End Set
    End Property
    Public Property Existe As Boolean
        Get
            Return _Existe
        End Get
        Set(ByVal value As Boolean)
            _Existe = value
        End Set
    End Property
#End Region

#Region "Funciones"
    Public Function GetCambios(ByVal vActivo As Boolean) As String

        Dim CadCam As String = ""

        If _Activo <> vActivo Then
            CadCam += "Activo: '" & _Activo & "' Cambia a [" & vActivo & "],"
        End If

        If CadCam <> "" Then
            CadCam = Mid(CadCam, 1, CadCam.Length - 1)
        End If
        Return CadCam
    End Function

    Public Sub Guardar(ByVal bEsIdentidad As Boolean, ByVal vidTipOrdServ As Integer, ByVal vidMarca As Integer, ByVal vActivo As Boolean)
        Dim sSql As String = "", _CadValAnt As String = "", _CadValNue As String = ""
        If _Existe Then
            'If _idTipOrdServ <> vidTipOrdServ Then
            '    sSql += "idTipOrdServ = " & vidTipOrdServ & ","
            '    _CadValAnt += "idTipOrdServ =" & vidTipOrdServ & ","
            'End If
            'If _idMarca <> vidMarca Then
            '    sSql += "idMarca = " & vidMarca & ","
            '    _CadValAnt += "idMarca =" & vidMarca & ","
            'End If

            If _Activo <> vActivo Then
                sSql += "Activo = '" & IIf(vActivo, 1, 0) & "',"
                _CadValAnt += "Activo =" & vActivo & ","
            End If



            If sSql <> "" Then
                sSql = Mid(sSql, 1, sSql.Length - 1) 'Para quitar la Coma ','
                _CadValAnt = "idMarca=" & vidMarca & "," & _CadValAnt
                _CadValAnt = "idTipOrdServ=" & vidTipOrdServ & "," & _CadValAnt

                _CadValAnt = Mid(_CadValAnt, 1, _CadValAnt.Length - 1)
                _CadValNue = Replace(sSql, "'", "")
                sSql = "UPDATE " & _TablaBd & " SET " & sSql & " WHERE idMarca = " & _idMarca & " and idTipOrdServ = " & _idTipOrdServ
                Try
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                    BD.Execute(sSql, True)
                    Audita(_TablaBd, _CadValAnt, _CadValNue, EnTipAccion.Modifica)
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
                Catch ex As Exception
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                    Throw New Exception(ex.Message)
                End Try
            End If
        Else
            If bEsIdentidad Then
                sSql = "INSERT INTO " & _TablaBd & "(idTipOrdServ,idMarca,Activo) VALUES(" & vidTipOrdServ & "," & vidMarca & "," & IIf(vActivo, 1, 0) & ")"

                _CadValNue = "idTipOrdServ=" & vidTipOrdServ & ", idMarca = " & vidMarca & ",Activo = " & vActivo
            Else

                sSql = "INSERT INTO " & _TablaBd & "(idTipOrdServ,idMarca,Activo) VALUES(" & vidTipOrdServ & "," & vidMarca & "," & IIf(vActivo, 1, 0) & ")"

                _CadValNue = "idTipOrdServ=" & vidTipOrdServ & ", idMarca = " & vidMarca & ",Activo = " & vActivo
            End If

            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, "", _CadValNue, EnTipAccion.Inserta)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
        End If
        _Activo = vActivo
    End Sub
#End Region

#Region "Consultas"
    Public Function TablaMarcasxTos(ByVal bActivo As Boolean, ByVal tipoOS As TipoOrdenServicio, Optional ByVal FiltroSinWhere As String = "") As DataTable
        Dim filtro As String = ""
        MyTabla.Rows.Clear()

        If FiltroSinWhere <> "" Then
            filtro = " where " & FiltroSinWhere
        End If

        StrSql = "SELECT rel.idMarca, " &
        "m.NombreMarca, " &
        "rel.idTipOrdServ, " &
        "t.NomTipOrdServ, " &
        "rel.Activo " &
        "FROM RelMarcaTipOrdSer rel " &
        "INNER JOIN dbo.CatMarcas M ON M.idMarca = rel.idMarca " &
        "INNER JOIN CatTipoOrdServ T ON T.idTipOrdServ = rel.idTipOrdServ " &
        "WHERE rel.idTipOrdServ = " & tipoOS & " AND rel.Activo = " & IIf(bActivo, 1, 0)

        MyTabla = BD.ExecuteReturn(StrSql)

        Return MyTabla

    End Function
#End Region
End Class
