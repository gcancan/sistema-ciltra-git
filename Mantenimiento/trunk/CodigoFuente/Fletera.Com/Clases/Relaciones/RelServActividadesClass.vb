﻿Public Class RelServActividadesClass
    Private _idServicio As Integer
    Private _NomServicio As String
    Private _idActividad As Integer
    Private _NombreAct As String
    Private _Activo As Boolean

    Private _Existe As Boolean
    Private _TablaBd As String = "RelServicioActividad"

    Dim StrSql As String
    Dim DsClase As New DataTable
    Dim MyTabla As DataTable = New DataTable(_TablaBd)

#Region "Constructor"
    Sub New(ByVal vidServicio As Integer, ByVal vidActividad As Integer)
        _idServicio = vidServicio
        _idActividad = vidActividad

        StrSql = "SELECT r.idServicio, " &
        "s.NomServicio, " &
        "r.idActividad, " &
        "a.NombreAct, r.Activo " &
        "FROM dbo.RelServicioActividad r " &
        "INNER JOIN dbo.CatServicios s ON s.idServicio = r.idServicio " &
        "INNER JOIN dbo.CatActividades a ON a.idActividad = r.idActividad " &
        "WHERE r.idServicio = " & vidServicio & " and r.idActividad = " & vidActividad


        DsClase = BD.ExecuteReturn(StrSql)
        If DsClase.Rows.Count > 0 Then
            With DsClase.Rows(0)
                _NomServicio = .Item("NomServicio") & ""
                _NombreAct = .Item("NombreAct") & ""
                _Activo = .Item("Activo")

            End With
            _Existe = True
        Else
            _Existe = False
        End If
    End Sub

    Sub New(ByVal vidServicio As Integer, ByVal vNomServicio As String, ByVal vidActividad As Integer,
            ByVal vNombreAct As String, ByVal vActivo As Boolean)
        _idServicio = vidServicio
        _NomServicio = vNomServicio
        _idActividad = vidActividad
        _NombreAct = vNombreAct
        '_idTipOrdServ = vidTipOrdServ
        '_NomTipOrdServ = vNomTipOrdServ
        '_idMarca = vidMarca
        '_NombreMarca = vNombreMarca
        _Activo = vActivo
        _Existe = True
    End Sub
#End Region

#Region "Propiedades"
    Public Property idServicio As Integer
        Get
            Return _idServicio
        End Get
        Set(ByVal value As Integer)
            _idServicio = value
        End Set
    End Property
    Public Property NomServicio As String
        Get
            Return _NomServicio
        End Get
        Set(ByVal value As String)
            _NomServicio = value
        End Set
    End Property
    Public Property idActividad As Integer
        Get
            Return _idActividad
        End Get
        Set(ByVal value As Integer)
            _idActividad = value
        End Set
    End Property
    Public Property NombreAct As String
        Get
            Return _NombreAct

        End Get
        Set(ByVal value As String)
            _NombreAct = value
        End Set
    End Property
    Public Property Activo As Boolean
        Get
            Return _Activo
        End Get
        Set(ByVal value As Boolean)
            _Activo = value
        End Set
    End Property
    Public Property Existe As Boolean
        Get
            Return _Existe
        End Get
        Set(ByVal value As Boolean)
            _Existe = value
        End Set
    End Property
#End Region

#Region "Funciones"
    Public Function GetCambios(ByVal vActivo As Boolean) As String

        Dim CadCam As String = ""

        If _Activo <> vActivo Then
            CadCam += "Activo: '" & _Activo & "' Cambia a [" & vActivo & "],"
        End If

        If CadCam <> "" Then
            CadCam = Mid(CadCam, 1, CadCam.Length - 1)
        End If
        Return CadCam
    End Function

    Public Sub Guardar(ByVal bEsIdentidad As Boolean, ByVal vidServicio As Integer, ByVal vidActividad As Integer, ByVal vActivo As Boolean)
        Dim sSql As String = "", _CadValAnt As String = "", _CadValNue As String = ""
        If _Existe Then
            'If _idTipOrdServ <> vidTipOrdServ Then
            '    sSql += "idTipOrdServ = " & vidTipOrdServ & ","
            '    _CadValAnt += "idTipOrdServ =" & vidTipOrdServ & ","
            'End If
            'If _idMarca <> vidMarca Then
            '    sSql += "idMarca = " & vidMarca & ","
            '    _CadValAnt += "idMarca =" & vidMarca & ","
            'End If

            If _Activo <> vActivo Then
                sSql += "Activo = " & IIf(vActivo, 1, 0) & ","
                _CadValAnt += "Activo =" & vActivo & ","
            End If



            If sSql <> "" Then
                sSql = Mid(sSql, 1, sSql.Length - 1) 'Para quitar la Coma ','
                _CadValAnt = "idServicio=" & vidServicio & "," & _CadValAnt
                _CadValAnt = "idActividad=" & vidActividad & "," & _CadValAnt

                _CadValAnt = Mid(_CadValAnt, 1, _CadValAnt.Length - 1)
                _CadValNue = Replace(sSql, "'", "")
                sSql = "UPDATE " & _TablaBd & " SET " & sSql & " WHERE idServicio = " & vidServicio & " and idActividad = " & vidActividad
                Try
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                    BD.Execute(sSql, True)
                    Audita(_TablaBd, _CadValAnt, _CadValNue, EnTipAccion.Modifica)
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
                Catch ex As Exception
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                    Throw New Exception(ex.Message)
                End Try
            End If
        Else
            If bEsIdentidad Then
                sSql = "INSERT INTO " & _TablaBd & "(idServicio,idActividad,Activo) VALUES(" & vidServicio & "," & vidActividad & "," & IIf(vActivo, 1, 0) & ")"

                _CadValNue = "idServicio=" & vidServicio & ", idActividad = " & vidActividad & ",Activo = " & vActivo
            Else

                sSql = "INSERT INTO " & _TablaBd & "(idServicio,idActividad,Activo) VALUES(" & vidServicio & "," & vidActividad & "," & IIf(vActivo, 1, 0) & ")"

                _CadValNue = "idServicio=" & vidServicio & ", idActividad = " & vidActividad & ",Activo = " & vActivo
            End If

            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, "", _CadValNue, EnTipAccion.Inserta)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
        End If
        _Activo = vActivo
    End Sub
#End Region

#Region "Consultas"
    Public Function TablaActividadesxServ(ByVal bActivo As Boolean, ByVal vidServicio As Integer, Optional ByVal FiltroSinWhere As String = "") As DataTable
        Dim filtro As String = ""
        MyTabla.Rows.Clear()

        If FiltroSinWhere <> "" Then
            filtro = " where " & FiltroSinWhere
        End If

        'StrSql = "SELECT rel.idMarca, " &
        '"m.NombreMarca, " &
        '"rel.idTipOrdServ, " &
        '"t.NomTipOrdServ, " &
        '"rel.Activo " &
        '"FROM RelMarcaTipOrdSer rel " &
        '"INNER JOIN dbo.CatMarcas M ON M.idMarca = rel.idMarca " &
        '"INNER JOIN CatTipoOrdServ T ON T.idTipOrdServ = rel.idTipOrdServ " &
        '"WHERE rel.idTipOrdServ = " & tipoOS & " AND rel.Activo = " & IIf(bActivo, 1, 0)

        StrSql = "SELECT r.idServicio, " &
        "s.NomServicio, " &
        "r.idActividad, " &
        "a.NombreAct, r.Activo " &
        "FROM dbo.RelServicioActividad r " &
        "INNER JOIN dbo.CatServicios s ON s.idServicio = r.idServicio " &
        "INNER JOIN dbo.CatActividades a ON a.idActividad = r.idActividad " &
        "WHERE r.idServicio = " & vidServicio & " and r.activo = " & IIf(bActivo, 1, 0)

        MyTabla = BD.ExecuteReturn(StrSql)

        Return MyTabla

    End Function
#End Region
End Class
