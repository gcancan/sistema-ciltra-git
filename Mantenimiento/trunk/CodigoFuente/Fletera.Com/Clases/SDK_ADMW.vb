﻿Option Strict Off
Option Explicit On

Imports System.Runtime.InteropServices


Module SDK_ADMW
    'Dim UbicacionSDK As String = "C:\Program Files (x86)\Compac\SDK"


    'Campos de la tablas de Movimientos
    Public Const kMovto_IdMovto As String = "cIdMovimiento"
    Public Const kMovto_NumMovto As String = "cNumeroMovimiento"
    Public Const kMovto_CodProducto As String = "cCodigoProducto"
    Public Const kMovto_CodAlmacen As String = "cCodigoAlmacen"
    Public Const kMovto_CodAlmacenEntrada As String = "cCodigoAlmacenEntrada"
    Public Const kMovto_CodAlmacenSalida As String = "cCodigoAlmacenSalida"
    Public Const kMovto_CodAlmacenConsignacion As String = "cCodigoAlmacenConsignacion"
    Public Const kMovto_Unidades As String = "cUnidades"
    Public Const kMovto_UnidadesNC As String = "cUnidadesNC"
    Public Const kMovto_Precio As String = "cPrecio"
    Public Const kMovto_CostoCapturado As String = "cCostoCapturado"
    Public Const kMovto_CodValorClasificacion As String = "cCodigoValorClasificacion"
    Public Const kMovto_Referencia As String = "cReferencia"
    Public Const kMovto_FechaExtra As String = "cFechaExtra"
    Public Const kMovto_NombreUnidad As String = "cNombreUnidad"
    Public Const kMovto_NombreUnidadNC As String = "cNombreUnidadNC"
    Public Const kMovto_CodClasificacion As String = "cCodigoClasificacion"
    Public Const kMovto_Neto As String = "cNeto"
    Public Const kMovto_Total As String = "cTotal"
    Public Const kMovto_ObservaMov As String = "cObservaMov"
    Public Const kMovto_ComisionVenta As String = "cComVenta"

    Public Const kMovto_PorcDescto1 As String = "cPorcentajeDescuento1"
    Public Const kMovto_ImportDescto1 As String = "cDescuento1"
    Public Const kMovto_PorcDescto2 As String = "cPorcentajeDescuento2"
    Public Const kMovto_ImportDescto2 As String = "cDescuento2"
    Public Const kMovto_PorcDescto3 As String = "cPorcentajeDescuento3"
    Public Const kMovto_ImportDescto3 As String = "cDescuento3"
    Public Const kMovto_PorcDescto4 As String = "cPorcentajeDescuento4"
    Public Const kMovto_ImportDescto4 As String = "cDescuento4"
    Public Const kMovto_PorcDescto5 As String = "cPorcentajeDescuento5"
    Public Const kMovto_ImportDescto5 As String = "cDescuento5"

    ' Campos de la tablas de Documentos
    'CIDDOCUM01
    Public Const kDocumento_IdDocumento As String = "cIdDocumento"
    'Public Const kDocumento_IdDocumento As String = "CIDDOCUM01"
    Public Const kDocumento_CodigoConcepto As String = "cCodigoConcepto"
    Public Const kDocumento_Serie As String = "cSerieDocumento"
    Public Const kDocumento_Folio As String = "cFolio"
    Public Const kDocumento_Fecha As String = "cFecha"
    Public Const kDocumento_CodigoCteProv As String = "cCodigoCteProv"
    Public Const kDocumento_RazonSocial As String = "cRazonSocial"
    Public Const kDocumento_RFC As String = "cRFC"
    Public Const kDocumento_SerieOmision As String = "cSeriePorOmision"
    'Public Const kDocumento_CodigoAgente As String = "cIdAgente"
    Public Const kDocumento_CodigoAgente As String = "cCodigoAgente"
    Public Const kDocumento_FechaVencimiento As String = "cFechaVencimiento"
    Public Const kDocumento_FechaEntRecep As String = "cFechaEntregaRecepcion"
    Public Const kDocumento_FechaProntoPago As String = "cFechaProntoPago"
    Public Const kDocumento_FechaUltimoInteres As String = "cFechaUltimoInteres"
    Public Const kDocumento_IdMoneda As String = "cidMoneda"
    Public Const kDocumento_TipoCambio As String = "cTipoCambio"
    Public Const kDocumento_Referencia As String = "cReferencia"
    Public Const kDocumento_Importe As String = "cImporte"
    Public Const kDocumento_Descuento1 As String = "cDescuentoDoc1"
    Public Const kDocumento_Descuento2 As String = "cDescuentoDoc2"
    Public Const kDocumento_DescProntoPago As String = "cDescuentoProntoPago"
    Public Const kDocumento_InteresMoratorio As String = "cPorcentajeInteres"
    Public Const kDocumento_SisOrigen As String = "cSistOrig"
    Public Const kDocumento_Observaciones As String = "cObservaciones"
    Public Const kDocumento_ConDireccionFiscal As String = "cConDireccionFiscal"
    Public Const kDocumento_ConDireccionEnvio As String = "cConDireccionEnvio"
    Public Const kDocumento_Gasto1 As String = "cGasto1"
    Public Const kDocumento_Gasto2 As String = "cGasto2"
    Public Const kDocumento_Gasto3 As String = "cGasto3"

    '___________________DECLARACIÓN DE CONSTANTES DE LONGITUD____________________
    Public Const kLongNombreProducto As Short = 255 + 1
    Public Const kLongCodigo As Short = 30 + 1
    Public Const kLongNumSerie As Short = 11 + 1
    Public Const kLongFecha As Integer = 23 + 1
    Public Const kLongReferencia As Short = 20 + 1
    Public Const kLongDescripcion As Short = 60 + 1

    Public lLicencia As Byte

    ' Registro para documentos
    Public Structure tDocumento
        'Dim aFolio As Double
        'Dim aNumMoneda As Integer
        'Dim aTipoCambio As Double
        'Dim aImporte As Double ' Importe se asigna solamente cuando se va crear un documento de CxC o CxP, en otro su valor es cero.
        'Dim aDescuentoDoc1 As Double ' No tiene uso, valor de omision = 0
        'Dim aDescuentoDoc2 As Double ' No tiene uso, valor de omision = 0
        'Dim aSistemaOrigen As Integer ' Valor mayor a 5 que indica una aplicación diferente a los PAQ's
        ''Dim aFecha As String ' Formato mm/dd/aaaa. Las diagonales "/" son parte del formato
        'Dim aCodConcepto As String
        'Dim aSerie As String
        'Public aCodigoCteProv As String
        'Public aCodigoAgente As String
        'Public aReferencia As String
        'Dim aAfecta As Byte ' No tiene uso, valor de omision = 0

        ''<VBFixedString(kLongCodigo), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst:=kLongCodigo)>
        ''<VBFixedString(kLongNumSerie), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst:=kLongNumSerie)>
        ''<VBFixedString(kLongFecha), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst:=kLongFecha)> Public aFecha As String ' Formato mm/dd/aaaa. Las diagonales "/" son parte del formato
        '<MarshalAs(UnmanagedType.ByValArray, SizeConst:=kLongFecha)> Dim aFecha As String  '= Space(kLongFecha)
        ''<MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongFecha)> Public aFecha As String
        ''<VBFixedString(kLongCodigo), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst:=kLongCodigo)>
        ''<VBFixedString(kLongCodigo), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst:=kLongCodigo)>
        ''<VBFixedString(kLongReferencia), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst:=kLongReferencia)>
        'Public aGasto1 As Double
        'Public aGasto2 As Double
        'Public aGasto3 As Double

        Public aFolio As Double
        Public aNumMoneda As Integer
        Public aTipoCambio As Double
        Public aImporte As Double
        Public aDescuentoDoc1 As Double
        Public aDescuentoDoc2 As Double
        Public aSistemaOrigen As Integer
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongCodigo)> Public aCodConcepto As String
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongNumSerie)> Public aSerie As String
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongFecha)> Public aFecha As String
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongCodigo)> Public aCodigoCteProv As String
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongCodigo)> Public aCodigoAgente As String
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongReferencia)> Public aReferencia As String
        Public aAfecta As Integer
        Public aGasto1 As Double
        Public aGasto2 As Double
        Public aGasto3 As Double

    End Structure

    ' Registro para Movimientos
    Public Structure tMovimiento
        Dim aConsecutivo As Integer ' Valor inicial debe ser 100 con incrementos de 100 en 100, por si es necesario insertar movtos intermedios
        Dim aUnidades As Double ' En caso de producto con series, lotes y/o pedimentos y carcateristicas este valor es cero
        Dim aPrecio As Double ' Usado para docuementos de venta
        Dim aCosto As Double ' Usado para docuemtnos de compra y/o inventarios
        <VBFixedString(kLongCodigo), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst:=kLongCodigo)> Public aCodProdSer As String
        <VBFixedString(kLongCodigo), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst:=kLongCodigo)> Public aCodAlmacen As String
        <VBFixedString(kLongReferencia), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst:=kLongReferencia)> Public aReferencia As String
        <VBFixedString(kLongCodigo), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst:=kLongCodigo)> Public aCodClasificacion As String ' No tiene uso, valor omision en blancos
    End Structure
    '___________________FUNCIONES SDK____________________

    'vUbicacionSDK
    'Declare Function fInicializaSDK Lib "C:\Program Files (x86)\Compacw\AdminPAQ\MGW_SDK.DLL" () As Integer
    'vUbicacionSDK = vUbicacionSDK
    'Public vUbicacionSDK As String = Inicio.UbicacionSDK & "\MGW_SDK.DLL"

    Public Declare Function fInicializaSDK Lib "MGW_SDK.DLL" () As Integer

    Public Declare Sub fTerminaSDK Lib "MGW_SDK.DLL" ()
    Public Declare Function fSetNombrePAQ Lib "MGW_SDK.DLL" (ByVal aNombrePAQ As String) As Integer

    ' Apertura/Cierre
    Declare Function fAbreEmpresa Lib "MGW_SDK.DLL" (ByVal aError As String) As Integer
    Public Declare Sub fCierraEmpresa Lib "MGW_SDK.DLL" ()
    ' ***** Funciones de Existencias  *****
    ' Bajo Nivel - Lectura/Escritura
    Declare Function fRegresaExistencia Lib "MGW_SDK.DLL" (ByVal aCodigoProducto As String, ByVal aCodigoAlmacen As String, ByVal aAnio As String, ByVal aMes As String, ByVal aDia As String, ByRef aExistencia As Double) As Integer

    Declare Function fRegresaCostoPromedio Lib "MGW_SDK.DLL" (ByVal aCodigoProducto As String, ByVal aCodigoAlmacen As String, ByVal aAnio As String, ByVal aMes As String, ByVal aDia As String, ByRef aCostoPromedio As Double) As Integer
    'fRegresaUltimoCosto
    Declare Function fRegresaUltimoCosto Lib "MGW_SDK.DLL" (ByVal aCodigoProducto As String, ByVal aCodigoAlmacen As String, ByVal aAnio As String, ByVal aMes As String, ByVal aDia As String, ByRef aUltimoCosto As Double) As Integer

    Declare Function fSiguienteFolio Lib "MGW_SDK.DLL" (ByVal aCodigoConcepto As String, ByVal aSerie As String, ByRef aFolio As Double) As Integer

    ' Manejo de errores
    Public Declare Sub fError Lib "MGW_SDK.DLL" (ByVal aNumError As Integer, ByVal aError As String, ByVal aLen As Integer)

    ' ***** Funciones de Empresas *****
    ' Navegación
    Declare Function fPosPrimerEmpresa Lib "MGW_SDK.DLL" (ByRef aIdEmpresa As Integer, ByVal aNombreEmpresa As String, ByVal aDirectorioEmpresa As String) As Integer
    Declare Function fPosSiguienteEmpresa Lib "MGW_SDK.DLL" (ByRef aIdEmpresa As Integer, ByVal aNombreEmpresa As String, ByVal aDirectorioEmpresa As String) As Integer

    ' ***** Funciones de Parámetros *****
    ' Bajo Nivel - Lectura/Escritura
    Declare Function fLeeDatoParametros Lib "MGW_SDK.DLL" (ByVal aCampo As String, ByVal aValor As String, ByVal aLen As Integer) As Integer

    ' Bajo Nivel - Busqueda/Navegación
    Declare Function fBuscaProducto Lib "MGW_SDK.DLL" (ByVal aCodProducto As String) As Integer
    Declare Function fLeeDatoProducto Lib "MGW_SDK.DLL" (ByVal aCampo As String, ByVal aValor As String, ByVal aLen As Integer) As Integer

    ' Alto Nivel - Lectura/Escritura
    'UPGRADE_WARNING: La estructura tDocumento puede requerir que se pasen atributos de cálculo de referencia como argumento en esta instrucción Declare. Haga clic aquí para obtener más información: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1050"'
    Declare Function fAltaDocumento Lib "MGW_SDK.DLL" (ByRef aIdDocumento As Integer, ByRef aDocto As tDocumento) As Integer
    Declare Function fBuscarDocumento Lib "MGW_SDK.DLL" (ByVal aCodigoConcepto As String, ByVal aNumSerie As String, ByVal aFolio As String) As Integer
    'Declare Function fBuscarIdMovimiento Lib "MGW_SDK.DLL" (ByVal aIdMovimiento As Integer)
    '
    ' Alto Nivel - Lectura/Escritura
    'UPGRADE_WARNING: La estructura tMovimiento puede requerir que se pasen atributos de cálculo de referencia como argumento en esta instrucción Declare. Haga clic aquí para obtener más información: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1050"'
    Declare Function fAltaMovimiento Lib "MGW_SDK.DLL" (ByVal aIdDocumento As Integer, ByRef aIdMovimiento As Integer, ByRef aMovimiento As tMovimiento) As Integer

    ' Bajo Nivel - Lectura/Escritura
    Declare Function fInsertarDocumento Lib "MGW_SDK.DLL" () As Integer
    Declare Function fEditarMovimiento Lib "MGW_SDK.DLL" () As Integer

    Declare Function fBorraDocumento Lib "MGW_SDK.DLL" () As Integer
    Declare Function fSetDatoDocumento Lib "MGW_SDK.DLL" (ByVal aCampo As String, ByVal aValor As String) As Integer
    Declare Function fGuardaDocumento Lib "MGW_SDK.DLL" () As Integer

    Declare Function fLeeDatoDocumento Lib "MGW_SDK.DLL" (ByVal aCampo As String, ByVal aValor As String, ByVal aLen As Integer) As Integer
    Declare Function fInsertarMovimiento Lib "MGW_SDK.DLL" () As Integer
    Declare Function fSetDatoMovimiento Lib "MGW_SDK.DLL" (ByVal aCampo As String, ByVal aValor As String) As Integer
    Declare Function fGuardaMovimiento Lib "MGW_SDK.DLL" () As Integer

    ' Funciones para timbrar y entregar documentos
    Public Declare Function fInicializaLicenseInfo Lib "MGW_SDK.DLL" (ByVal lSistema As Byte) As Integer
    'Public Declare Function fInicializaLicenseInfo Lib "MGW_SDK.DLL" (ByVal lSistema As Integer) As Long
    'Public Declare Function fInicializaLicenseInfo Lib "MGW_SDK.DLL" (ByVal lSistema As Short) As Integer
    Public Declare Function fEmitirDocumento Lib "MGW_SDK.DLL" (<MarshalAs(UnmanagedType.LPStr)> ByVal aCodigoConcepto As String, <MarshalAs(UnmanagedType.LPStr)> ByVal aSerie As String, ByVal aFolio As Double, <MarshalAs(UnmanagedType.LPStr)> ByVal aPassword As String, <MarshalAs(UnmanagedType.LPStr)> ByVal aArchivoAdicional As String) As Integer
    Public Declare Function fEntregEnDiscoXML Lib "MGW_SDK.DLL" (<MarshalAs(UnmanagedType.LPStr)> ByVal aCodigoConcepto As String, <MarshalAs(UnmanagedType.LPStr)> ByVal aSerie As String, ByVal aFolio As Double, ByVal aFormato As Integer, <MarshalAs(UnmanagedType.LPStr)> ByVal aFormatoAmig As String) As Integer
    'Declare Function fDocumentoUUID Lib "MGW_SDK.DLL" (ByVal aCodigoConcepto As String, ByVal aSerie As String, ByVal aFolio As Double, ByVal atPtrCFDIUUID As String) As Integer
    Public Declare Function fDocumentoUUID Lib "MGW_SDK.DLL" (ByVal aCodigoConcepto As String, ByVal aNumSerie As String, ByVal aFolio As Double, ByVal aUUID As String) As Integer


    Public Declare Function fAfectaDocto_Param Lib "MGW_SDK.DLL" (<MarshalAs(UnmanagedType.LPStr)> ByVal aCodConcepto As String, <MarshalAs(UnmanagedType.LPStr)> ByVal aSerie As String, ByVal aFolio As Double, ByVal aAfecta As Boolean) As Integer
    '
    Public Declare Function fDocumentoImpreso Lib "MGW_SDK.DLL" (ByVal aImpreso As Boolean) As Integer

    Declare Function fLeeDatoCteProv Lib "MGW_SDK.DLL" (ByVal aCampo As String, ByVal aValor As String, ByVal aLen As Integer) As Integer

End Module

