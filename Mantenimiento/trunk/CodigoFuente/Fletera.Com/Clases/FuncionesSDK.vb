﻿Public Class FuncionesSDK
    Dim lError As Integer = 0
    Dim lError2 As Integer = 0
    Dim DsComboFox As New DataSet
    Dim dbtabla As String = ""
    Dim Consulta As String = ""
    Public Function ChecaCostoPromBDFOX(ByVal Producto As Integer, ByVal Fecha As String, Optional ByVal Almacen As Integer = 0) As Double
        'Lo calcula directo de la Base
        Dim objCosto As New ToolSQLs
        Dim vCostoProm As Double = 0
        Dim Conexion As New OleDb.OleDbConnection(ConStrFox)

        Try
            dbtabla = "COSTOPROM"

            Consulta = objCosto.RegresaCostoPromedio(Producto, Fecha, Almacen)

            Dim Adt As New OleDb.OleDbDataAdapter(Consulta, Conexion)
            DsComboFox.Clear()
            Adt.Fill(DsComboFox, dbtabla)
            If Not DsComboFox Is Nothing Then
                For i = 0 To DsComboFox.Tables(dbtabla).DefaultView.Count - 1
                    vCostoProm = Math.Round(CDbl(DsComboFox.Tables(dbtabla).DefaultView.Item(0).Item("ccostoh")), 4)
                    If vCostoProm > 0 Then
                        Exit For
                    End If
                Next

                'If DsComboFox.Tables(dbtabla).DefaultView.Count > 0 Then
                '    'vCostoProm = Math.Round(CDbl(DsComboFox.Tables(dbtabla).DefaultView.Item(0).Item("cultimoc01")), 4)
                '    'If vCostoProm = 0 Then
                '    '    vCostoProm = Math.Round(CDbl(DsComboFox.Tables(dbtabla).DefaultView.Item(0).Item("ccostoh")), 4)
                '    'End If
                '    vCostoProm = Math.Round(CDbl(DsComboFox.Tables(dbtabla).DefaultView.Item(0).Item("ccostoh")), 4)
                '    If vCostoProm = 0 Then
                '        'vCostoProm = Math.Round(CDbl(DsComboFox.Tables(dbtabla).DefaultView.Item(0).Item("cultimoc01")), 4)
                '    End If

                'End If
            End If
            Return vCostoProm
        Catch ex As Exception
            SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")
            Return vCostoProm
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "ChecaCostoProm")
        End Try
        

    End Function

    'fRegresaExistencia
    Public Function ChecaExistencia(ByVal Producto As String, ByVal Almacen As String, ByVal Anio As String, ByVal Mes As String, ByVal Dia As String) As Double
        Dim vExistencia As Double = 0

        lError = fRegresaExistencia(Producto, Almacen, Anio, Mes, Dia, vExistencia)
        If lError = 0 Then
            Return vExistencia
        Else
            Return 0
        End If
    End Function

    Public Function ChecaCosto(ByVal Producto As String, ByVal Almacen As String, ByVal Anio As String, ByVal Mes As String, ByVal Dia As String) As Double
        Dim aCosto As Double = 0
        'aCosto = f_RellenaConBlancos(aCosto, 255)
        'Producto = f_RellenaConBlancos(Producto, kLongCodigo - 1)
        'Almacen = f_RellenaConBlancos(Almacen, kLongCodigo + 1)
        Dim vCosto As Double = 0


        lError = fRegresaCostoPromedio(Producto, Almacen, Anio, Mes, Dia, vCosto)
        'lError = fRegresaCostoPromedio(Producto, Almacen, Anio, Dia, Mes, vCosto)


        'lError = fRegresaCostoPromedio(Producto, "0", Anio, Mes, Dia, aCosto)
        'lError = fRegresaCostoPromedio(Producto, "1", "2015", "11", "3", aCosto)

        'lError = fRegresaUltimoCosto(Producto, Almacen, Anio, Mes, Dia, aCosto)
        If lError = 0 Then
            'lblExistencia.Text = "Existencia:" & vExistencia

            'aCostoPromedio = Format(aCostoPromedio, "E")
            'aCostoPromedio = Math.Abs(aCostoPromedio)
            'vCosto = Math.Log(aCostoPromedio)

            vCosto = Math.Round(Math.Abs(Math.Log10(aCosto)), 2) * 2

            'aCosto = Math.Round(Math.Log10(Math.Abs(aCosto)), 4)

            'Else
            '    Return aCostoPromedio
        End If
        'Return vCosto
        Return aCosto
    End Function

    Public Function CreaDocumentoSDK(ByVal TipoDocumento As String, ByVal tipoDocto As String, ByVal NoSerie As String, ByVal Folio As String) As Integer
        Dim idDoc As String = ""
        Dim BandError As Boolean = False

        idDoc = f_RellenaConBlancos(idDoc, kLongDescripcion)


        If lError = 0 Then

        Else
            BandError = True
        End If

        lError = fSetDatoDocumento(kDocumento_CodigoConcepto, TipoDocumento)
        If lError = 0 Then
            lError = fSetDatoDocumento(kDocumento_Serie, NoSerie)
            If lError = 0 Then
                lError = fSetDatoDocumento(kDocumento_Folio, Folio)
                If lError = 0 Then
                    'lError = fSetDatoDocumento(kDocumento_Fecha, dtpFechaMov.Value.ToString("MM/dd/yyyy"))
                Else
                    BandError = True
                End If
            Else
                BandError = True
            End If
        Else
            BandError = True
        End If



        If BandError Then
            lError2 = fBorraDocumento()
            If lError2 <> 0 Then
                MensajeError(lError2)
            End If
        End If


        Return lError
    End Function



End Class
