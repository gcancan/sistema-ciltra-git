﻿Imports Microsoft.Win32

Public Enum LugarRegistry
    regClassesRoot = 0
    regLocalMachine = 1
    regCurrentConfig = 2
    regCurrentUser = 3
    regUsers = 4
End Enum

Public Class ToolRegistro
    'Para Registro de Windows Local_Machine
    'Public KeyPathLM As String = "Software\Test"

    'Public KeyPathCA As String = "SOFTWARE\"
    'Public KeyPathLM As String = "SOFTWARE\RgDsgSoft"

    Public Sub CrearKey()
        Try
            Registry.LocalMachine.CreateSubKey(KeyPathLM)
        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "Error en: ToolRegistro")
        End Try

    End Sub

    'Public Sub CreaSubKey(ByVal NombreSubKey As String, ByVal LugarRoot As LugarRegistry, ByVal LugarSubRoot As String)
    Public Sub CreaSubKey(ByVal NombreSubKey As String)

        Dim oRegKey As RegistryKey
        Try
            oRegKey = Registry.LocalMachine.OpenSubKey(KeyPathLM & "\", True)
            oRegKey.CreateSubKey(NombreSubKey)
            'oRegKey = Registry.LocalMachine.CreateSubKey("SOFTWARE\\" & NombreSubKey)
            oRegKey.Close()
        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "Error en: ToolRegistro")
        End Try


    End Sub

    Public Sub CreaValoresSubKey(ByVal NombreSubKey As String, ByVal NombreClaveSubkey As String, ByVal NomLlave As String, ByVal ValorLLave As String, ByVal KeyPath As String)
        Dim oRegKey As RegistryKey
        Try
            
            'Create new values
            oRegKey = Registry.LocalMachine.OpenSubKey(KeyPath & "\" & NombreSubKey & "\" & NombreClaveSubkey, True)
            oRegKey.SetValue(NomLlave, ValorLLave)

            oRegKey.Close()
            'oRegKey.SetValue("Version", "1.0.0.0")
            'oRegKey.SetValue("Author", "Dirk Schuermans")
            'oRegKey.SetValue("Serial", "A1B2C3D4E5F6G7H8I9")

        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "Error en: ToolRegistro")
        End Try
    End Sub

    Public Sub CreaSubSubKey(ByVal NombreSubKey As String, ByVal NombreClaveSubkey As String)
        Dim oRegKey As RegistryKey
        Try
            oRegKey = Registry.LocalMachine.OpenSubKey(KeyPathLM & "\" & NombreSubKey, True)
            oRegKey.CreateSubKey(NombreClaveSubkey)

            ''Create new values
            'oRegKey.SetValue("Version", "1.0.0.0")
            'oRegKey.SetValue("Author", "Dirk Schuermans")
            'oRegKey.SetValue("Serial", "A1B2C3D4E5F6G7H8I9")

            oRegKey.Close()
        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "Error en: ToolRegistro")
        End Try
    End Sub

    Public Function LeeKey(ByVal KeyPath As String) As Boolean
        Dim key As RegistryKey = Registry.LocalMachine.OpenSubKey(KeyPath, False) ' Abrimos para sólo lectura
        If key IsNot Nothing Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function LeeLlaveRegistro(ByVal NombreSubKey As String, ByVal NombreClaveSubkey As String, ByVal NomLlave As String, ByVal KeyPath As String) As String
        Dim oRegKey As RegistryKey
        Dim Resultado As String = ""
        Try
            'Dim runK As RegistryKey = Registry.LocalMachine.OpenSubKey("SOFTWARE\RgDsgSoft\Windows\CurrentVersion\Run", True)

            'oRegKey = Registry.LocalMachine.OpenSubKey(KeyPathLM & "\" & NombreSubKey & "\" & NombreClaveSubkey)
            oRegKey = Registry.LocalMachine.OpenSubKey(KeyPath & "\" & NombreSubKey & "\" & NombreClaveSubkey)

            If oRegKey Is Nothing Then
                Return ""
            Else
                Resultado = (oRegKey.GetValue(NomLlave))
                oRegKey.Close()
                Return Resultado
            End If

        Catch ex As Exception
            Return "ERROR"
        End Try
    End Function

    
    'Public Sub fnWrite(ByVal SubKey As String, ByVal vCampo As String)
    '    'Declare variables
    '    Dim oRegKey As RegistryKey

    '    'Set the subkey to work with
    '    oRegKey = Registry.LocalMachine.OpenSubKey("SOFTWARE", True)


    '    'Create a new subkey
    '    'oRegKey.CreateSubKey("MyApp")
    '    oRegKey.CreateSubKey("MyApp")

    '    'Set the subkey to work with
    '    oRegKey = Registry.LocalMachine.OpenSubKey("SOFTWARE\MyApp", True)

    '    'Create new values
    '    oRegKey.SetValue("Version", "1.0.0.0")
    '    oRegKey.SetValue("Author", "Dirk Schuermans")
    '    oRegKey.SetValue("Serial", "A1B2C3D4E5F6G7H8I9")

    '    'Close the key
    '    oRegKey.Close()
    'End Sub
End Class
