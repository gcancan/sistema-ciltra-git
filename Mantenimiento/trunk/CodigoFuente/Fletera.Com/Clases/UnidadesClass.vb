﻿Public Class UnidadesClass
    Private _CIDUNIDA01 As Integer
    Private _CNOMBREU01 As String
    Private _CABREVIA01 As String

    Private _Existe As Boolean
    Private _TablaBd As String = "CatUnidades"

    Dim StrSql As String
    Dim DsComboFox As New DataSet
#Region "Constructor"
    Sub New(ByVal CIDUNIDAD As Integer)
        Dim Conexion As New OleDb.OleDbConnection(ConStrFox)
        Dim DtCol As New DataTable

        StrSql = "SELECT CIDUNIDAD,CNOMBREU01,CABREVIA01 FROM MGW10026 " & _
        "WHERE CIDUNIDAD = " & CIDUNIDAD

        Dim Adt As New OleDb.OleDbDataAdapter(StrSql, Conexion)
        DsComboFox.Clear()
        Adt.Fill(DsComboFox, _TablaBd)
        If Not DsComboFox Is Nothing Then
            _CIDUNIDA01 = CIDUNIDAD
            If DsComboFox.Tables(_TablaBd).DefaultView.Count > 0 Then
                With DsComboFox.Tables(_TablaBd).DefaultView.Item(0)
                    _CNOMBREU01 = Trim(.Item("CNOMBREU01") & "")
                    _CABREVIA01 = Trim(.Item("CABREVIA01") & "")
                End With
                _Existe = True
            Else
                _Existe = False
            End If
        End If
    End Sub
    Sub New(ByVal vCIDUNIDA01 As Integer, ByVal vCNOMBREU01 As String, ByVal vCABREVIA01 As String)
        _CIDUNIDA01 = vCIDUNIDA01
        _CNOMBREU01 = vCNOMBREU01
        _CABREVIA01 = vCABREVIA01
        _Existe = True
    End Sub
#End Region
#Region "Propiedades"
    Public ReadOnly Property CIDUNIDA01 As String
        Get
            Return _CIDUNIDA01
        End Get
    End Property
    Public Property CNOMBREU01 As String
        Get
            Return _CNOMBREU01
        End Get
        Set(ByVal value As String)
            _CNOMBREU01 = value
        End Set
    End Property
    Public Property CABREVIA01 As String
        Get
            Return _CABREVIA01
        End Get
        Set(ByVal value As String)
            _CABREVIA01 = value
        End Set
    End Property
    Public ReadOnly Property Existe As Boolean
        Get
            Return _Existe
        End Get
    End Property
#End Region
    '#Region "Funciones"
    '    Public Function GetCambios(ByVal vNombreColonia As String, ByVal vCodigoPostal As String, ByVal vPoblacion As PoblacionClass) As String
    '        Dim CadCam As String = ""
    '        If _NombreColonia <> vNombreColonia Then
    '            CadCam += "Nombre Colonia: '" & _NombreColonia & "' Cambia a [" & vNombreColonia & "],"
    '        End If
    '        If _CodigoPostal <> vCodigoPostal Then
    '            CadCam += "Codigo Postal: '" & _CodigoPostal & "' Cambia a [" & vCodigoPostal & "],"
    '        End If
    '        If _Poblacion.idPoblacion <> vPoblacion.idPoblacion Then
    '            CadCam += "Poblacion: '" & _Poblacion.NomPoblacion & "' Cambia a [" & vPoblacion.NomPoblacion & "],"
    '        End If
    '        If CadCam <> "" Then
    '            CadCam = Mid(CadCam, 1, CadCam.Length - 1)
    '        End If
    '        Return CadCam
    '    End Function
    '    Public Sub Guardar(ByVal vNombreColonia As String, ByVal vCodigoPostal As String, ByVal vPoblacion As PoblacionClass)
    '        Dim sSql As String = "", _CadValAnt As String = "", _CadValNue As String = ""
    '        If _Existe Then
    '            If _NombreColonia <> vNombreColonia Then
    '                sSql += "NomColonia = '" & vNombreColonia & "',"
    '                _CadValAnt += "NomColonia =" & _NombreColonia & ","
    '            End If
    '            If _CodigoPostal <> vCodigoPostal Then
    '                sSql += "CodPostal = '" & vCodigoPostal & "'"
    '                _CadValAnt += "CodPostal =" & _CodigoPostal & ","
    '            End If
    '            If _Poblacion.idPoblacion <> vPoblacion.idPoblacion Then
    '                sSql += "idPoblacion ='" & vPoblacion.idPoblacion & "'"
    '                _CadValAnt += "idPoblacion =" & _Poblacion.idPoblacion & ","
    '            End If
    '            If sSql <> "" Then
    '                sSql = Mid(sSql, 1, sSql.Length - 1) 'Para quitar la Coma ','
    '                _CadValAnt = "idColonia=" & _idColonia & "," & _CadValAnt
    '                _CadValAnt = Mid(_CadValAnt, 1, _CadValAnt.Length - 1)
    '                _CadValNue = Replace(sSql, "'", "")
    '                sSql = "UPDATE " & _TablaBd & " SET " & sSql & " WHERE idColonia = '" & _idColonia & "'"
    '                Try
    '                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
    '                    BD.Execute(sSql, True)
    '                    Audita(_TablaBd, _CadValAnt, _CadValNue, EnTipAccion.Modifica)
    '                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
    '                Catch ex As Exception
    '                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
    '                    Throw New Exception(ex.Message)
    '                End Try
    '            End If
    '        Else
    '            sSql = "INSERT INTO c_Colonias(idColonia,NomColonia,CodPostal,idPoblacion) VALUES(" &
    '                "'" & _idColonia & "','" & vNombreColonia & "','" & vCodigoPostal & "','" & vPoblacion.idPoblacion & "')"
    '            _CadValNue = "idColonia=" & _idColonia & ",NomColonia=" & vNombreColonia & ",CodPostal=" & vCodigoPostal & ",idPoblacion=" & vPoblacion.idPoblacion
    '            Try
    '                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
    '                BD.Execute(sSql, True)
    '                Audita(_TablaBd, "", _CadValNue, EnTipAccion.Inserta)
    '                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
    '            Catch ex As Exception
    '                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
    '                Throw New Exception(ex.Message)
    '            End Try
    '        End If
    '    End Sub
    '    Public Sub Eliminar()
    '        If _Existe Then
    '            Dim sSql As String = "", _CadValAnt As String = ""
    '            _CadValAnt = "idColonia=" & _idColonia & " , NomColonia =" & _NombreColonia & ", CodPostal=" & _CodigoPostal & " ,idPoblacion=" & _Poblacion.idPoblacion
    '            sSql = "DELETE FROM " & _TablaBd & " WHERE idColonia = '" & _idColonia & "'"
    '            Try
    '                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
    '                BD.Execute(sSql, True)
    '                Audita(_TablaBd, _CadValAnt, "", EnTipAccion.Elimina)
    '                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
    '            Catch ex As Exception
    '                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
    '                Throw New Exception(ex.Message)
    '            End Try
    '            _Existe = False
    '        Else
    '            'se supone que nunca debe entrar aqui. . .
    '            MessageBox.Show("La Colonia con Id: " & _idColonia & " No existe por lo tanto no se puede Eliminar!!!",
    '                            "No se puede eliminar la Colonia porque no Existe", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '        End If
    '    End Sub
    '#End Region
End Class