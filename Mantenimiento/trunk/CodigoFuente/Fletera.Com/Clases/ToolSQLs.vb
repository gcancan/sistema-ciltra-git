﻿Public Class ToolSQLs

    Dim StrSQL As String = ""
    Dim MyTabla As DataTable

#Region "Diagnostico"
    Public Function InsertaDetOrdenActividadProductos(ByVal idOrdenSer As Integer, ByVal idOrdenActividad As Integer,
    ByVal idProducto As Integer, ByVal idUnidadProd As Integer, ByVal Cantidad As Double, ByVal Costo As Double,
    ByVal Existencia As Double, ByVal CantCompra As Double, ByVal CantSalida As Double) As String

        StrSQL = "insert into DetOrdenActividadProductos  " &
        "(idOrdenSer,idOrdenActividad,idProducto,idUnidadProd,Cantidad,Costo,Existencia,CantCompra,CantSalida)" &
        " values " &
        "(" & idOrdenSer & "," & idOrdenActividad & "," & idProducto & "," & idUnidadProd &
        "," & Cantidad & "," & Costo & "," & Existencia & "," & CantCompra & "," & CantSalida & ")"

        Return StrSQL
    End Function

    Public Function InsertaCabPreOC(ByVal bEsIdentiti As Boolean, ByVal IdPreOC As Integer, ByVal idOrdenTrabajo As Integer,
    ByVal FechaPreOc As Date, ByVal UserSolicita As String, ByVal idAlmacen As Integer, ByVal Estatus As String) As String
        If bEsIdentiti Then
            StrSQL = "insert into CabPreOC (idOrdenTrabajo,FechaPreOc,UserSolicita,idAlmacen,Estatus) " &
            "values (" & idOrdenTrabajo & ",'" & FormatFecHora(FechaPreOc, True, True) & "','" & UserSolicita & "'," & idAlmacen & ",'" & Estatus & "')"
        Else
            StrSQL = "insert into CabPreOC (IdPreOC,idOrdenTrabajo,FechaPreOc,UserSolicita,idAlmacen,Estatus) " &
            "values (" & IdPreOC & "," & idOrdenTrabajo & ",'" & FormatFecHora(FechaPreOc, True, True) & "','" & UserSolicita & "'," & idAlmacen & ",'" & Estatus & "')"

        End If
        Return StrSQL
    End Function

    Public Function InsertaDetbPreOC(ByVal IdPreOC As Integer, ByVal idProducto As Integer, ByVal idUnidadProd As Integer, ByVal CantidadSol As Double)

        'StrSQL = "INSERT INTO DETPREOC (IDPREOC,IDPRODUCTO,IDUNIDADPROD,CANTIDADSOL) " &
        '"VALUES (" & IdPreOC & "," & idProducto & "," & idUnidadProd & "," & CantidadSol & ")"

        StrSQL = "INSERT INTO DETPREOC (IDPREOC,IDPRODUCTO,IDUNIDADPROD,CANTIDADSOL) " &
        "VALUES ((SELECT TOP 1 IdPreOC FROM dbo.CabPreOC ORDER BY IdPreOC DESC)," & idProducto & "," & idUnidadProd & "," & CantidadSol & ")"


        Return StrSQL
    End Function

    Public Function InsertaCabPreSalida(ByVal bEsIdentiti As Boolean, ByVal IdPreSalida As Integer, ByVal idOrdenTrabajo As Integer,
    ByVal FecSolicitud As Date, ByVal UserSolicitud As String, ByVal IdEmpleadoEnc As Integer, ByVal idAlmacen As Integer,
    ByVal Estatus As String, ByVal SubTotal As Double) As String

        If bEsIdentiti Then
            StrSQL = "INSERT INTO CABPRESALIDA (IDORDENTRABAJO,FECSOLICITUD,USERSOLICITUD,IDEMPLEADOENC,IDALMACEN,ESTATUS,SUBTOTAL) " &
            "VALUES (" & idOrdenTrabajo & ",'" & FormatFecHora(FecSolicitud, True, True) & "','" & UserSolicitud &
            "'," & IdEmpleadoEnc & "," & idAlmacen & ",'" & Estatus & "'," & SubTotal & ") "

        Else
            StrSQL = "INSERT INTO CABPRESALIDA (IDPRESALIDA,IDORDENTRABAJO,FECSOLICITUD,USERSOLICITUD,IDEMPLEADOENC,IDALMACEN,ESTATUS,SUBTOTAL) " &
            "VALUES (" & IdPreSalida & "," & idOrdenTrabajo & ",'" & FormatFecHora(FecSolicitud, True, True) & "','" & UserSolicitud &
            "'," & IdEmpleadoEnc & "," & idAlmacen & ",'" & Estatus & "'," & SubTotal & ") "
        End If

        Return StrSQL
    End Function

    Public Function InsertaDetPreSalida(ByVal IdPreSalida As Integer, ByVal idProducto As Integer, ByVal idUnidadProd As Integer, ByVal Cantidad As Integer, ByVal Costo As Integer, ByVal Existencia As String) As String

        'StrSQL = "INSERT INTO DetPreSalida (IDPRESALIDA,IDPRODUCTO,IDUNIDADPROD,CANTIDAD,COSTO,EXISTENCIA) " &
        '"VALUES (" & IdPreSalida & "," & idProducto & "," & idUnidadProd & "," & Cantidad & "," & Costo & "," & Existencia & ")"

        StrSQL = "INSERT INTO DetPreSalida (IDPRESALIDA,IDPRODUCTO,IDUNIDADPROD,CANTIDAD,COSTO,EXISTENCIA) " &
        "VALUES ((SELECT TOP 1 IdPreSalida FROM dbo.CabPreSalida ORDER BY IdPreSalida DESC)," & idProducto & "," & idUnidadProd & "," & Cantidad & "," & Costo & "," & Existencia & ")"


        Return StrSQL
    End Function

#End Region


#Region "Conceptos"
    Public Function InsertaConcepto(ByVal CIDCONCE01 As Integer, ByVal CCODIGOC01 As String, ByVal CNOMBREC01 As String,
                                    ByVal CIDDOCUM01 As Integer, ByVal CPLAMIGCFD As String, ByVal CDOCTOAC01 As Boolean,
                                    ByVal EsExterno As Boolean)

        If EsExterno Then
            StrSQL = "DECLARE @EXIS AS INT " &
           "SET @EXIS = isnull((select top 1 1 from CatConceptosext where CIDCONCE01 = " & CIDCONCE01 & "),0) " &
           "if @EXIS = 0 " &
           "BEGIN " &
           "insert into CatConceptosext (CIDCONCE01,CCODIGOC01,CNOMBREC01,CIDDOCUM01,CPLAMIGCFD,CDOCTOAC01) values (" & CIDCONCE01 &
           ",'" & Trim(CCODIGOC01) & "','" & Trim(CNOMBREC01) & "'," & CIDDOCUM01 & ",'" & CPLAMIGCFD & "'," & IIf(CDOCTOAC01, 1, 0) & ") " &
           "end " &
           "else " &
           "begin " &
           "update CatConceptosext set CIDCONCE01 = 1 where CIDCONCE01 = 1 " &
           "end "

        Else
            ' StrSQL = "DECLARE @EXIS AS INT " & _
            '"SET @EXIS = isnull((select top 1 1 from CatConceptos where CIDCONCE01 = " & CIDCONCE01 & "),0) " & _
            '"if @EXIS = 0 " & _
            '"BEGIN " & _
            '"insert into CatConceptos (CIDCONCE01,CCODIGOC01,CNOMBREC01,CIDDOCUM01,CPLAMIGCFD,CDOCTOAC01) values (" & CIDCONCE01 & _
            '",'" & Trim(CCODIGOC01) & "','" & Trim(CNOMBREC01) & "'," & CIDDOCUM01 & ",'" & CPLAMIGCFD & "'," & IIf(CDOCTOAC01, 1, 0) & ") " & _
            '"end " & _
            '"else " & _
            '"begin " & _
            '"update CatConceptos set CIDCONCE01 = 1 where CIDCONCE01 = 1 " & _
            '"end "


            ' StrSQL = "insert into CatConceptos (CIDCONCE01,CCODIGOC01,CNOMBREC01,CIDDOCUM01,CPLAMIGCFD,CDOCTOAC01) values (" & CIDCONCE01 & _
            '",'" & Trim(CCODIGOC01) & "','" & Trim(CNOMBREC01) & "'," & CIDDOCUM01 & ",'" & CPLAMIGCFD & "'," & IIf(CDOCTOAC01, 1, 0) & ") "

            '12/FEB/2016
            StrSQL = "DECLARE @EXIS AS INT " &
            "SET @EXIS = isnull((select top 1 1 from CatConceptos where CCODIGOC01 = '" & CCODIGOC01 & "'),0) " &
            "if @EXIS = 0 " &
            "BEGIN " &
            "insert into CatConceptos (CIDCONCE01,CCODIGOC01,CNOMBREC01,CIDDOCUM01,CPLAMIGCFD,CDOCTOAC01) values (" & CIDCONCE01 &
            ",'" & Trim(CCODIGOC01) & "','" & Trim(CNOMBREC01) & "'," & CIDDOCUM01 & ",'" & CPLAMIGCFD & "'," & IIf(CDOCTOAC01, 1, 0) & ") " &
            "end " &
            "else " &
            "begin " &
            "update CatConceptos set CCODIGOC01 = '1' where CCODIGOC01 = '1' " &
            "end "

        End If

        Return StrSQL
    End Function

    Public Function BorraTodosConceptos(ByVal EsExterno As Boolean) As String

        If EsExterno Then
            StrSQL = "DELETE CATCONCEPTOSEXT"
        Else
            StrSQL = "DELETE CATCONCEPTOS"
        End If


        Return StrSQL
    End Function
#End Region

#Region "GRUPOS"
    'G R U P O S
    'Public Function BorraDetalleGrupo(ByVal CVEGRUPO As String) As String

    '    StrSQL = "DELETE DETGRUPOS WHERE CVEGRUPO = '" & CVEGRUPO & "'"

    '    Return StrSQL
    'End Function
    ''
    'Public Function BorraGrupo(ByVal CveGrupo As String) As String
    '    StrSQL = "DELETE CATGRUPOS WHERE CVEGRUPO = '" & CveGrupo & "'"
    '    Return StrSQL
    'End Function


    'Public Function CancelaGrupo(ByVal CveGrupo As String, ByVal Usuario As String) As String

    '    StrSQL = "UPDATE CATGRUPOS SET FECHACANCELA = '" & FormatFecHora(Now, True) & _
    '    "', USERCANCELA= '" & Usuario & "', ESTATUS = 'CAN' WHERE CVEGRUPO = '" & CveGrupo & "'"

    '    Return StrSQL
    'End Function

    'Public Function InsertaGrupo(ByVal CveGrupo As String, ByVal NombreGrupo As String, ByVal Usuario As String, ByVal Estatus As String) As String

    '    StrSQL = "INSERT INTO CatGrupos (CveGrupo,NombreGrupo,Estatus,UserCrea,FechaCrea) " & _
    '    " VALUES ('" & CveGrupo & "','" & NombreGrupo & "','" & Estatus & "','" & Usuario & "','" & FormatFecHora(Now, True) & "')"
    '    Return StrSQL
    'End Function

    'Public Function ModificaGrupo(ByVal CveGrupo As String, ByVal NombreGrupo As String, ByVal Usuario As String, ByVal Estatus As String) As String

    '    StrSQL = "update CatGrupos set Estatus = '" & Estatus & "' , NombreGrupo = '" & NombreGrupo & _
    '    "', FechaModifica = '" & FormatFecHora(Now, True) & "', UserModifica= '" & Usuario & _
    '    "' where cvegrupo = '" & CveGrupo & "'"

    '    Return StrSQL

    'End Function

    'Public Function InsertaDetalleGrupo(ByVal CveGrupo As String, ByVal CCODIGOC01_CLI As String) As String
    '    StrSQL = "DECLARE @EXIS AS INT " & _
    '    "SET @EXIS = isnull((select top 1 1 from DetGrupos where CCODIGOC01_CLI = '" & CCODIGOC01_CLI & "' and CveGrupo = '" & CveGrupo & "'),0) " & _
    '    "if @EXIS = 0 " & _
    '    "BEGIN " & _
    '    "insert into DetGrupos (CveGrupo,CCODIGOC01_CLI) values ('" & CveGrupo & "','" & Trim(CCODIGOC01_CLI) & "') " & _
    '    "end " & _
    '    "else " & _
    '    "begin " & _
    '    "update DetGrupos set CveGrupo = '1' where CveGrupo = '1' " & _
    '    "end "

    '    Return StrSQL
    'End Function

    'Public Function BorraListaPreciosxCliente(ByVal CCODIGOC01_CLI As String) As String
    '    StrSQL = "DELETE LISTAPRECIOS WHERE CCODIGOC01_CLI = '" & CCODIGOC01_CLI & "'"
    '    Return StrSQL
    'End Function

    'Public Function InsertaListaPreciosClientexGrupo(ByVal CveGrupo As String, ByVal CCODIGOC01_CLI As String) As String

    '    StrSQL = "INSERT INTO LISTAPRECIOS " & _
    '    "SELECT CCODIGOP01_PRO,'" & CCODIGOC01_CLI & "' AS CCODIGOC01_CLI,CABREVIA01,CIDUNIDA01, " & _
    '    "PRECIO,FECHAVIGENCIA,NOEQUIVALENTE,CIDUNIDA02,CABREVIA02,0 AS ESGRUPO, isnull(CNOMBREP01,'') as CNOMBREP01 " & _
    '    "FROM LISTAPRECIOS WHERE CCODIGOC01_CLI = '" & CveGrupo & "'"

    '    Return StrSQL
    'End Function

    'Public Function InsertaListaPreciosHISClientexGrupo(ByVal CveGrupo As String, ByVal CCODIGOC01_CLI As String, _
    '                                                    ByVal EsGrupo As Boolean, ByVal Accion As String) As String

    '    'StrSQL = "INSERT INTO LISTAPRECIOS " & _
    '    '"SELECT CCODIGOP01_PRO,'" & CCODIGOC01_CLI & "' AS CCODIGOC01_CLI,CABREVIA01,CIDUNIDA01, " & _
    '    '"PRECIO,FECHAVIGENCIA,NOEQUIVALENTE,CIDUNIDA02,CABREVIA02," & IIf(EsGrupo, 1, 0) & " AS ESGRUPO " & _
    '    '"FROM LISTAPRECIOS WHERE CCODIGOC01_CLI = '" & CveGrupo & "'"

    '    StrSQL = "INSERT INTO LISTAPRECIOSHIS " & _
    '    "SELECT CCODIGOP01_PRO,'" & CCODIGOC01_CLI & "' as CCODIGOC01_CLI,GETDATE() AS FECHA,CABREVIA01,CIDUNIDA01,PRECIO, " & _
    '    "FECHAVIGENCIA,'" & Accion & "' AS ACCION,NOEQUIVALENTE,CIDUNIDA02,CABREVIA02," & IIf(EsGrupo, 1, 0) & " as ESGRUPO, isnull(CNOMBREP01,'') as CNOMBREP01 " & _
    '    "FROM LISTAPRECIOS " & _
    '    "WHERE CCODIGOC01_CLI = '" & CveGrupo & "'"


    '    Return StrSQL
    'End Function

#End Region

#Region "ListaPrecios"

    'Public Function BorraListaPreciosClientexGrupo(ByVal CVEGRUPO As String) As String

    '    StrSQL = "DELETE LISTAPRECIOS WHERE CCODIGOC01_CLI IN (SELECT CCODIGOC01_CLI  FROM DETGRUPOS WHERE CVEGRUPO = '" & CVEGRUPO & "')"

    '    Return StrSQL
    'End Function

    'Public Function InsertaListaPrecios(ByVal CCODIGOP01_PRO As String, ByVal CCODIGOC01_CLI As String, _
    'ByVal CIDUNIDA01 As Integer, ByVal Precio As Double, ByVal FechaVigencia As String, _
    'ByVal NoEquivalente As Boolean, ByVal CIDUNIDA02 As Integer, ByVal CABREVIA01 As String, _
    'ByVal CABREVIA02 As String, ByVal EsGrupo As Boolean, ByVal CNOMBREP01 As String) As String

    '    StrSQL = "INSERT INTO LISTAPRECIOS (CCODIGOP01_PRO,CCODIGOC01_CLI,PRECIO,FechaVigencia, " & _
    '    "CIDUNIDA01,NoEquivalente,CIDUNIDA02,CABREVIA01,CABREVIA02, EsGrupo,CNOMBREP01) VALUES ('" & _
    '    CCODIGOP01_PRO & "','" & CCODIGOC01_CLI & "'," & _
    '    Precio & ",'" & FormatFecHora(FechaVigencia, True) & _
    '    "'," & CIDUNIDA01 & "," & IIf(NoEquivalente, 1, 0) & _
    '    "," & CIDUNIDA02 & ",'" & CABREVIA01 & "','" & CABREVIA02 & "'," & IIf(EsGrupo, 1, 0) & ",'" & CNOMBREP01 & "')"


    '    Return StrSQL
    '    '        StrSQL = "INSERT INTO LISTAPRECIOS (CCODIGOP01_PRO,CCODIGOC01_CLI,PRECIO,FechaVigencia,CIDUNIDA01,NoEquivalente) VALUES ('" & _
    '    'grdListado.Item("CCODIGOP01_PRO", i).Value & "','" & txtIdCliente.Text & "'," & _
    '    'grdListado.Item("Precio", i).Value & ",'" & FormatFecHora(grdListado.Item("gFechaVigencia", i).Value, True) & _
    '    '"'," & grdListado.Item("CIDUNIDAD", i).Value & "," & IIf(grdListado.Item("NoEquivalente", i).Value = True, 1, 0) & ")"

    'End Function

    'Public Function InsertaListaPreciosHis(ByVal CCODIGOP01_PRO As String, ByVal CCODIGOC01_CLI As String, _
    'ByVal CIDUNIDA01 As Integer, ByVal Precio As Double, ByVal FechaVigencia As String, _
    'ByVal NoEquivalente As Boolean, ByVal Accion As String, ByVal CIDUNIDA02 As Integer, _
    'ByVal CABREVIA01 As String, ByVal CABREVIA02 As String, ByVal EsGrupo As Boolean, ByVal CNOMBREP01 As String) As String

    '    StrSQL = "INSERT INTO LISTAPRECIOSHIS (CCODIGOP01_PRO,CCODIGOC01_CLI,PRECIO,FECHA,ACCION,FECHAVIGENCIA," & _
    '    "CIDUNIDA01,NOEQUIVALENTE,CIDUNIDA02,CABREVIA01,CABREVIA02,ESGRUPO,CNOMBREP01) " & _
    '    " VALUES ('" & CCODIGOP01_PRO & "','" & CCODIGOC01_CLI & "'," & _
    '    Precio & ",'" & FormatFecHora(Now, True) & "','" & Accion & _
    '    "','" & FormatFecHora(FechaVigencia, True) & "'," & CIDUNIDA01 & _
    '    "," & IIf(NoEquivalente = True, 1, 0) & "," & CIDUNIDA02 & ",'" & CABREVIA01 & _
    '    "','" & CABREVIA02 & "'," & IIf(EsGrupo, 1, 0) & ",'" & Trim(CNOMBREP01) & "')"


    '    Return StrSQL

    'End Function
#End Region




    Public Function BorraDetalleMovto(NoMovimiento As Integer) As String
        StrSQL = "DELETE DETMOVIMIENTOS WHERE NOMOVIMIENTO = " & NoMovimiento

        Return StrSQL
    End Function

    Public Function InsertaAlmacen(ByVal CIDALMACEN As Integer, ByVal CCODIGOA01 As String, ByVal CNOMBREA01 As String) As String

        StrSQL = "DECLARE @EXIS AS INT " &
       "SET @EXIS = isnull((select top 1 1 from CatAlmacenes where CIDALMACEN = " & CIDALMACEN & "),0) " &
       "if @EXIS = 0 " &
       "BEGIN " &
       "insert into CatAlmacenes (CIDALMACEN,CCODIGOA01, CNOMBREA01) values (" & CIDALMACEN &
       ",'" & Trim(CCODIGOA01) & "','" & Trim(CNOMBREA01) & "') " &
       "end " &
       "else " &
       "begin " &
       "update CatAlmacenes set CIDALMACEN = 1 where CIDALMACEN = 1 " &
       "end "
        Return StrSQL
    End Function

    'checar si sirve
    Public Function InsertaUnidad(ByVal CIDUNIDA01 As Integer, ByVal CNOMBREU01 As String, ByVal CABREVIA01 As String)
        StrSQL = "DECLARE @EXIS AS INT " &
        "SET @EXIS = isnull((select top 1 1 from catUnidades where CIDUNIDA01 = " & CIDUNIDA01 & "),0) " &
        "if @EXIS = 0 " &
        "BEGIN " &
        "insert into catUnidades (CIDUNIDA01,CNOMBREU01, CABREVIA01) values (" & CIDUNIDA01 &
        ",'" & Trim(CNOMBREU01) & "','" & Trim(CABREVIA01) & "') " &
        "end " &
        "else " &
        "begin " &
        "update catUnidades set CNOMBREU01 = '" & CNOMBREU01 & "', CABREVIA01 = '" & CABREVIA01 & "' where CIDUNIDA01 = " & CIDUNIDA01 &
        " end "


        Return StrSQL
    End Function

    'checar si sirve
    Public Function InsertaProducto(ByVal CIDPRODU01 As Integer, ByVal CCODIGOP01 As String,
    ByVal CNOMBREP01 As String, ByVal Precio As Double, ByVal CIDUNIDA01 As Integer, ByVal CIDUNIDA02 As Integer,
    ByVal CABREVIA01 As String, ByVal CABREVIA02 As String) As String

        'ORIGINAL
        StrSQL = "DECLARE @EXIS AS INT " &
        "SET @EXIS = isnull((select top 1 1 from CatProductos where CCODIGOP01 = '" & CCODIGOP01 & "'),0) " &
        "if @EXIS = 0 " &
        "BEGIN " &
        "insert into CatProductos (CIDPRODU01,CCODIGOP01, CNOMBREP01, precio, CIDUNIDA01,CIDUNIDA02,CABREVIA01,CABREVIA02) " &
        "values (" & CIDPRODU01 & ",'" & Trim(CCODIGOP01) &
        "','" & Trim(CNOMBREP01) & "'," & Precio &
        "," & CIDUNIDA01 & "," & CIDUNIDA02 & ",'" & CABREVIA01 & "','" & CABREVIA02 & "') " &
        "end " &
        "else " &
        "begin " &
        "UPDATE CATPRODUCTOS SET " &
        "CIDPRODU01 = " & CIDPRODU01 & ", " &
        "CNOMBREP01 = '" & CNOMBREP01 & "', " &
        "PRECIO = " & Precio & ", " &
        "CIDUNIDA01 = " & CIDUNIDA01 & ", " &
        "CABREVIA01 = '" & CABREVIA01 & "', " &
        "CIDUNIDA02 = " & CIDUNIDA02 & ", " &
        "CABREVIA02 = '" & CABREVIA02 & "' " &
        "WHERE CCODIGOP01 = '" & CCODIGOP01 & "' " &
        "end "


        Return StrSQL
    End Function

    Public Function InsertaAgente(ByVal CIDAGENTE As Integer, ByVal CCODIGOA01 As String, ByVal CNOMBREA01 As String) As String
        StrSQL = "DECLARE @EXIS AS INT " &
        "SET @EXIS = isnull((select top 1 1 from CatAgentes where CCODIGOA01 = '" & CCODIGOA01 & "'),0) " &
        "if @EXIS = 0 " &
        "BEGIN " &
        "insert into CatAgentes (CIDAGENTE,CCODIGOA01, CNOMBREA01) " &
        "values (" & CIDAGENTE &
        ",'" & Trim(CCODIGOA01) &
        "','" & Trim(CNOMBREA01) & "') " &
        "end " &
        "else " &
        "begin " &
        "update CatAgentes set CIDAGENTE = 1 where CIDAGENTE = 1 " &
        "end "

        Return StrSQL

    End Function

    ' Public Function BorraCliente(ByVal CIDCLIEN01 As Integer, ByVal CCODIGOC01 As String) As String
    '     StrSQL = "delete from CatClientes where CCODIGOC01 = '" & CCODIGOC01 & "'"

    '     Return StrSQL
    ' End Function

    Public Function InsertaCliente(ByVal CIDCLIEN01 As Integer, ByVal CCODIGOC01 As String, ByVal CRAZONSO01 As String,
    ByVal CRFC As String, ByVal DOMICILIO As String, ByVal CEMAIL As String, ByVal CveGrupo As String, ByVal EsGrupo As Boolean) As String

        StrSQL = "DECLARE @EXIS AS INT " &
        "SET @EXIS = isnull((select top 1 1 from CatClientes where CCODIGOC01 = '" & CCODIGOC01 & "'),0) " &
        "if @EXIS = 0 " &
        "BEGIN " &
        "INSERT INTO CATCLIENTES (CIDCLIEN01,CCODIGOC01,CRAZONSO01,CRFC,DOMICILIO,CEMAIL,CVEGRUPO,ESGRUPO) " &
        "VALUES (" & CIDCLIEN01 & ",'" & CCODIGOC01 & "','" & CRAZONSO01 & "','" & CRFC &
        "','" & DOMICILIO & "','" & CEMAIL & "','" & CveGrupo & "'," & IIf(EsGrupo, 1, 0) &
        " ) end " &
        " begin " &
        " update CATCLIENTES set CCODIGOC01 = '1' where CCODIGOC01 = '1' " &
        " End "

        Return StrSQL
    End Function

    'checar si sirve
    Public Function ActualizaDetalleMovtoPadre(ByVal NoMovimiento As Integer, ByVal CIDUNIDA01 As Integer, ByVal CCODIGOP01_PRO As String, ByVal CantidadIngresada As Double) As String
        StrSQL = "UPDATE DETMOVIMIENTOS SET CANTIDADINGRESADA = isnull(CANTIDADINGRESADA,0) + " & CantidadIngresada &
        " WHERE CIDUNIDA01 = " & CIDUNIDA01 & " AND CCODIGOP01_PRO = '" & CCODIGOP01_PRO & "' AND NOMOVIMIENTO = " & NoMovimiento

        Return StrSQL
    End Function


    Public Function InsertaDetalleMovto(ByVal NoMovimiento As Integer, ByVal IdMovimiento As Integer, ByVal NoSerie As String,
    ByVal TipoMovimiento As String, ByVal CCODIGOP01_PRO As String, ByVal Cantidad As Double, ByVal Precio As Double,
    ByVal PorcIVA As Double, ByVal Costo As Double, ByVal PorcDescto As Double, ByVal ImpDescto As Double,
    ByVal CIDALMACEN As Integer, ByVal Existencia As Double, ByVal CIDUNIDA01 As Integer, ByVal CIDUNIDA01BASE As Integer,
    ByVal CantidadBASE As Double, ByVal tFactorUnidad As Double, ByVal tNoEquivalente As Boolean, ByVal Consecutivo As Integer,
    ByVal CIDUNIDA02 As Integer, ByVal CantidadEQUI As Double, ByVal COBSERVA01 As String) As String
        'CantidadIngresada
        Try
            StrSQL = "INSERT INTO DetMovimientos (NoMovimiento,IdMovimiento,NoSerie,TipoMovimiento,CCODIGOP01_PRO,Cantidad, " &
        "Precio,PorcIVA,Costo,PorcDescto,ImpDescto,CIDALMACEN,Existencia,CIDUNIDA01,CIDUNIDA01BASE,CantidadBASE, " &
        "tFactorUnidad,tNoEquivalente,Consecutivo,CIDUNIDA02,CantidadEQUI,COBSERVA01) " &
        "VALUES (" & NoMovimiento & "," & IdMovimiento & ",'" & NoSerie & "','" & TipoMovimiento &
        "','" & CCODIGOP01_PRO & "'," & Cantidad &
        "," & Precio & "," & PorcIVA & "," & Costo & "," & PorcDescto &
        "," & ImpDescto & "," & CIDALMACEN & "," & Existencia & "," & CIDUNIDA01 &
        "," & CIDUNIDA01BASE & "," & CantidadBASE & "," & tFactorUnidad &
        "," & IIf(tNoEquivalente, 1, 0) & "," & Consecutivo &
        "," & CIDUNIDA02 & "," & CantidadEQUI & ",'" & COBSERVA01 & "')"
        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try



        Return StrSQL
    End Function

    Public Function CancelaDocumento(NoMovimiento As Integer, ByVal UsuarioCancela As String, ByVal FechaCancela As String) As String
        StrSQL = "update CabMovimientos set UsuarioCancela = '" & UsuarioCancela &
        "', FechaCancela = '" & FormatFecHora(FechaCancela, True, True) & "', Estatus = 'CAN' where NoMovimiento = " & NoMovimiento

        Return StrSQL
    End Function

    Public Function ActualizaCabeceroTimbra(ByVal NoMovimiento As Integer, ByVal UsuarioModifica As String, ByVal FechaModifica As String, ByVal TimbraFactura As Boolean)
        StrSQL = "update CabMovimientos set TimbraFactura = " & IIf(TimbraFactura, 1, 0) & ", UsuarioModifica = '" & UsuarioModifica &
        "', FechaModifica = '" & FormatFecHora(FechaModifica, True, True) & "' where NoMovimiento = " & NoMovimiento

        Return StrSQL
    End Function




    ' Public Function ActualizaCabeceroTimbraADMPAQ(ByVal IdMovimiento As Integer, ByVal NoSerie As String, ByVal CIDDOCUM02 As Integer, ByVal UsuarioModifica As String, ByVal FechaModifica As String, ByVal TimbraFactura As Boolean)
    '     StrSQL = "update CabMovimientos set TimbraFactura = " & IIf(TimbraFactura, 1, 0) & ", UsuarioModifica = '" & UsuarioModifica & _
    '     "', FechaModifica = '" & FormatFecHora(FechaModifica, True) & "' where IdMovimiento = " & IdMovimiento & " and  NoSerie = '" & NoSerie & "' and CIDDOCUM02 = " & CIDDOCUM02

    '     Return StrSQL
    ' End Function


    Public Function ActualizaCabeceroModifica(ByVal NoMovimiento As Integer, ByVal UsuarioModifica As String, ByVal FechaModifica As String, ByVal Estatus As String)
        StrSQL = "update CabMovimientos set Estatus = '" & Estatus & "', UsuarioModifica = '" & UsuarioModifica &
        "', FechaModifica = '" & FormatFecHora(FechaModifica, True, True) & "' where NoMovimiento = " & NoMovimiento

        Return StrSQL
    End Function

    Public Function ActualizaCabeceroApliADMPAQ(ByVal NoMovimiento As Integer, ByVal FechaMovimiento As String, ByVal SubTotalGrav As Double,
   ByVal SubTotalExento As Double, ByVal Iva As Double, ByVal Descuento As Double, ByVal UserAutoriza As String,
   ByVal FechaVencimiento As String, ByVal FechaEntrega As String, ByVal UsuarioModifica As String, ByVal FechaModifica As String,
   ByVal CCODIGOA01 As String, ByVal CIDDOCUM02 As Integer, ByVal TipoMovimiento As String,
   ByVal IdMovimiento As Double, ByVal NoSerie As String, ByVal Usuario As String,
   ByVal IdEmpresa As Integer, ByVal Estatus As String, ByVal Vehiculo As String, ByVal Chofer As String,
   ByVal CMETODOPAG As String, ByVal CNUMCTAPAG As String, ByVal CREFEREN01 As String,
   ByVal COBSERVA01 As String, ByVal TimbraFactura As Boolean, ByVal ReqAutDcto As Boolean,
   ByVal bCantCompleta As Boolean, ByVal bTrasDir As Boolean, ByVal CTEXTOEXTRA1 As String, ByVal CTEXTOEXTRA2 As String) As String
        'CREFEREN01
        'COBSERVA01
        ', ByVal bTrasDir As Boolean

        StrSQL = "update CabMovimientos set IdMovimiento = " & IdMovimiento & ", NoSerie = '" & NoSerie &
        "',FechaMovimiento = '" & FormatFecHora(FechaMovimiento, True, True) &
        "', SubTotalGrav = " & SubTotalGrav & ", SubTotalExento = " & SubTotalExento & ", Iva = " & Iva &
        ", Estatus = '" & Estatus & "',Descuento = " & Descuento & ", UserAutoriza = '" & UserAutoriza &
        "',FechaVencimiento = '" & FormatFecHora(FechaVencimiento, True, True) & "', FechaEntrega = '" & FormatFecHora(FechaEntrega, True, True) &
        "', UsuarioModifica = '" & UsuarioModifica & "' ,FechaModifica = '" & FormatFecHora(FechaModifica, True, True) &
        "',CCODIGOA01 = '" & CCODIGOA01 & "',CIDDOCUM02 = " & CIDDOCUM02 & ", TipoMovimiento = '" & TipoMovimiento &
        "', Usuario = '" & Usuario & "',Vehiculo = '" & Vehiculo & "', Chofer = '" & Chofer & "',CMETODOPAG='" & CMETODOPAG &
        "',CNUMCTAPAG= '" & CNUMCTAPAG & "',CREFEREN01= '" & CREFEREN01 & "', COBSERVA01= '" & COBSERVA01 &
        "', TimbraFactura = " & IIf(TimbraFactura, 1, 0) & ", ReqAutDcto = " & IIf(ReqAutDcto, 1, 0) &
        ", bCantCompleta = " & IIf(bCantCompleta, 1, 0) &
        ", bTrasDir = " & IIf(bTrasDir, 1, 0) &
        ", CTEXTOEXTRA1 = '" & CTEXTOEXTRA1 &
        "', CTEXTOEXTRA2 = '" & CTEXTOEXTRA2 &
        "' where NoMovimiento = " & NoMovimiento

        '", bTrasDir = " & IIf(bTrasDir, 1, 0) & _

        Return StrSQL
    End Function

    Public Function ActualizaCabecero(ByVal NoMovimiento As Integer, ByVal FechaMovimiento As String, ByVal SubTotalGrav As Double,
    ByVal SubTotalExento As Double, ByVal Iva As Double, ByVal Descuento As Double, ByVal UserAutoriza As String,
    ByVal FechaVencimiento As String, ByVal FechaEntrega As String, ByVal UsuarioModifica As String, ByVal FechaModifica As String,
    ByVal CCODIGOA01 As String, ByVal CIDDOCUM02 As Integer, ByVal TipoMovimiento As String, ByVal Vehiculo As String, ByVal Chofer As String,
    ByVal CMETODOPAG As String, ByVal CNUMCTAPAG As String, ByVal CREFEREN01 As String, ByVal COBSERVA01 As String,
    ByVal Estatus As String, ByVal TimbraFactura As Boolean, ByVal ReqAutDcto As Boolean, ByVal bCantCompleta As Boolean,
    ByVal bTrasDir As Boolean, ByVal CTEXTOEXTRA1 As String, ByVal CTEXTOEXTRA2 As String) As String

        StrSQL = "update CabMovimientos set FechaMovimiento = '" & FormatFecHora(FechaMovimiento, True, True) &
        "', SubTotalGrav = " & SubTotalGrav & ", SubTotalExento = " & SubTotalExento & ", Iva = " & Iva &
        ", Descuento = " & Descuento & ", UserAutoriza = '" & UserAutoriza &
        "',FechaVencimiento = '" & FormatFecHora(FechaVencimiento, True, True) & "', FechaEntrega = '" & FormatFecHora(FechaEntrega, True, True) &
        "', UsuarioModifica = '" & UsuarioModifica & "' ,FechaModifica = '" & FormatFecHora(FechaModifica, True, True) &
        "',CCODIGOA01 = '" & CCODIGOA01 & "',CIDDOCUM02 = " & CIDDOCUM02 & ", TipoMovimiento = '" & TipoMovimiento &
        "', Vehiculo= '" & Vehiculo & "', Chofer = '" & Chofer & "',CMETODOPAG = '" & CMETODOPAG &
        "',CNUMCTAPAG = '" & CNUMCTAPAG & "',CREFEREN01 = '" & CREFEREN01 & "', COBSERVA01 = '" & COBSERVA01 &
        "', Estatus= '" & Estatus & "', TimbraFactura = " & IIf(TimbraFactura, 1, 0) &
        ", ReqAutDcto = " & IIf(ReqAutDcto, 1, 0) & ", bCantCompleta = " & IIf(bCantCompleta, 1, 0) &
        ", bTrasDir = " & IIf(bTrasDir, 1, 0) &
        ", CTEXTOEXTRA1 = '" & CTEXTOEXTRA1 &
        "', CTEXTOEXTRA2 = '" & CTEXTOEXTRA2 &
        "' where NoMovimiento = " & NoMovimiento

        Return StrSQL
    End Function




    'Public Function InsertaCabeceroMovto(ByVal NoMovimiento As Integer, ByVal IdMovimiento As Integer, ByVal NoSerie As String, _
    'ByVal TipoMovimiento As String, ByVal CCODIGOC01_CLI As String, ByVal FechaMovimiento As String, ByVal SubTotalGrav As Double, _
    'ByVal SubTotalExento As Double, ByVal Iva As Double, ByVal Estatus As String, ByVal Descuento As Double, ByVal CCODIGOA01 As String, _
    'ByVal Usuario As String, ByVal IdEmpresa As Integer, ByVal UserAutoriza As String, ByVal CIDDOCUM02 As Integer, _
    'ByVal FechaVencimiento As String, ByVal FechaEntrega As String, ByVal UsuarioCrea As String, _
    'ByVal FechaCrea As String, ByVal UsuarioModifica As String, ByVal FechaModifica As String, _
    'ByVal CIDMONEDA As Integer, ByVal CTIPOCAM01 As Integer, ByVal Vehiculo As String, ByVal Chofer As String, _
    'ByVal CMETODOPAG As String, ByVal CNUMCTAPAG As String, ByVal CREFEREN01 As String, ByVal COBSERVA01 As String, _
    'ByVal TimbraFactura As Boolean, ByVal EsExterno As Boolean, ByVal ReqAutDcto As Boolean, ByVal CIDALMACEN_SAL As Integer, _
    'ByVal CIDALMACEN_ENT As Integer, ByVal NoMovimientoPadre As Integer, ByVal ConsecutivoMov As Integer, _
    'ByVal bCantCompleta As Boolean, ByVal CantidadMovs As Integer, ByVal bTrasDir As Boolean, ByVal CTEXTOEXTRA1 As String, ByVal CTEXTOEXTRA2 As String) As String

    '    '

    '    StrSQL = "INSERT INTO CabMovimientos ( " & _
    '    "NoMovimiento,IdMovimiento, NoSerie, TipoMovimiento, CCODIGOC01_CLI, FechaMovimiento, " & _
    '    "SubTotalGrav, SubTotalExento, Iva, Estatus, Descuento, CCODIGOA01, " & _
    '    "Usuario, IdEmpresa, UserAutoriza, CIDDOCUM02, FechaVencimiento, " & _
    '    "FechaEntrega, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica,CIDMONEDA, " & _
    '    "CTIPOCAM01,Vehiculo,Chofer,CMETODOPAG,CNUMCTAPAG,CREFEREN01, COBSERVA01, TimbraFactura, " & _
    '    "EsExterno,ReqAutDcto,CIDALMACEN_SAL, CIDALMACEN_ENT,NoMovimientoPadre,ConsecutivoMov,bCantCompleta, " & _
    '    "CantidadMovs,bTrasDir,CTEXTOEXTRA1,CTEXTOEXTRA2 " & _
    '    ") " & _
    '    "VALUES (" & NoMovimiento & "," & IdMovimiento & ",'" & NoSerie & "','" & TipoMovimiento & "','" & CCODIGOC01_CLI & _
    '    "','" & FormatFecHora(FechaMovimiento, True) & "'," & SubTotalGrav & "," & SubTotalExento & _
    '    "," & Iva & ",'" & Estatus & "'," & Descuento & ",'" & CCODIGOA01 & "','" & Usuario & _
    '    "'," & IdEmpresa & ",'" & UserAutoriza & "'," & CIDDOCUM02 & ",'" & FormatFecHora(FechaVencimiento, True) & _
    '    "','" & FormatFecHora(FechaEntrega, True) & "','" & UsuarioCrea & "','" & FormatFecHora(FechaCrea, True) & _
    '    "','" & UsuarioModifica & "','" & FormatFecHora(FechaModifica, True) & "'," & CIDMONEDA & "," & CTIPOCAM01 & _
    '    ",'" & Vehiculo & "','" & Chofer & "','" & CMETODOPAG & "','" & CNUMCTAPAG & "','" & CREFEREN01 & _
    '    "',' " & COBSERVA01 & "'," & IIf(TimbraFactura, 1, 0) & "," & IIf(EsExterno, 1, 0) & _
    '    "," & IIf(ReqAutDcto, 1, 0) & "," & CIDALMACEN_SAL & "," & CIDALMACEN_ENT & _
    '    "," & NoMovimientoPadre & "," & ConsecutivoMov & "," & IIf(bCantCompleta, 1, 0) & "," & _
    '    Val(CantidadMovs) & "," & IIf(bTrasDir, 1, 0) & ",'" & CTEXTOEXTRA1 & "','" & CTEXTOEXTRA2 & "')"

    '    'If Estatus = "TER" Then

    '    'ElseIf Estatus = "CAP" Then

    '    'End If
    '    Return StrSQL
    'End Function
    Public Function InsertaCabeceroMovto(ByVal NoMovimiento As Integer, ByVal IdMovimiento As Integer, ByVal NoSerie As String,
    ByVal TipoMovimiento As String, ByVal CCODIGOC01_CLI As String, ByVal FechaMovimiento As String, ByVal SubTotalGrav As Double,
    ByVal SubTotalExento As Double, ByVal Iva As Double, ByVal Estatus As String, ByVal Descuento As Double, ByVal CCODIGOA01 As String,
    ByVal Usuario As String, ByVal IdEmpresa As Integer, ByVal UserAutoriza As String, ByVal CIDDOCUM02 As Integer,
    ByVal FechaVencimiento As String, ByVal FechaEntrega As String, ByVal UsuarioCrea As String,
    ByVal FechaCrea As String, ByVal UsuarioModifica As String, ByVal FechaModifica As String,
    ByVal CIDMONEDA As Integer, ByVal CTIPOCAM01 As Integer, ByVal Vehiculo As String, ByVal Chofer As String,
    ByVal CMETODOPAG As String, ByVal CNUMCTAPAG As String, ByVal CREFEREN01 As String, ByVal COBSERVA01 As String,
    ByVal TimbraFactura As Boolean, ByVal EsExterno As Boolean, ByVal ReqAutDcto As Boolean, ByVal Retencion As Double,
    ByVal Kilometraje As Double, ByVal CantTotal As Double) As String

        StrSQL = "INSERT INTO CabMovimientos ( " &
        "NoMovimiento,IdMovimiento, NoSerie, TipoMovimiento, CCODIGOC01_CLI, FechaMovimiento, " &
        "SubTotalGrav, SubTotalExento, Iva, Estatus, Descuento, CCODIGOA01, " &
        "Usuario, IdEmpresa, UserAutoriza, CIDDOCUM02, FechaVencimiento, " &
        "FechaEntrega, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica,CIDMONEDA, " &
        "CTIPOCAM01,Vehiculo,Chofer,CMETODOPAG,CNUMCTAPAG,CREFEREN01, COBSERVA01, TimbraFactura,EsExterno,ReqAutDcto, " &
        "Retencion,Kilometraje,CantTotal " &
        ") " &
        "VALUES (" & NoMovimiento & "," & IdMovimiento & ",'" & NoSerie & "','" & TipoMovimiento & "','" & CCODIGOC01_CLI &
        "','" & FormatFecHora(FechaMovimiento, True, True) & "'," & SubTotalGrav & "," & SubTotalExento &
        "," & Iva & ",'" & Estatus & "'," & Descuento & ",'" & CCODIGOA01 & "','" & Usuario &
        "'," & IdEmpresa & ",'" & UserAutoriza & "'," & CIDDOCUM02 & ",'" & FormatFecHora(FechaVencimiento, True, True) &
        "','" & FormatFecHora(FechaEntrega, True, True) & "','" & UsuarioCrea & "','" & FormatFecHora(FechaCrea, True, True) &
        "','" & UsuarioModifica & "','" & FormatFecHora(FechaModifica, True, True) & "'," & CIDMONEDA & "," & CTIPOCAM01 &
        ",'" & Vehiculo & "','" & Chofer & "','" & CMETODOPAG & "','" & CNUMCTAPAG & "','" & CREFEREN01 &
        "',' " & COBSERVA01 & "'," & IIf(TimbraFactura, 1, 0) & "," & IIf(EsExterno, 1, 0) & "," & IIf(ReqAutDcto, 1, 0) &
        "," & Retencion & "," & Kilometraje & "," & CantTotal & ")"

        'If Estatus = "TER" Then

        'ElseIf Estatus = "CAP" Then

        'End If
        Return StrSQL
    End Function

    Public Function RegresaCostoPromedio(ByVal CodProducto As Integer, ByVal fecha As String, Optional ByVal Almacen As Integer = 0)

        '"AND CAB.CFECHA BETWEEN {" & FormatFecHora(CaleFecha1(0).Value.ToShortDateString, False, True) & "} AND {" & FormatFecHora(CaleFecha2(0).Value.ToShortDateString, False, True) & "}" & _
        '" AND CFECHACO01 <= {" & FormatFecHora(fecha, False, True) & "}' " & _

        'StrSQL = "SELECT TOP 1 * FROM MGW10017 " & _

        StrSQL = "SELECT * FROM MGW10017 " &
        " WHERE CIDPRODU01 = " & CodProducto &
        " AND CFECHACO01 <= {" & FormatFecHora(fecha, False, True) & "} " &
        IIf(Almacen > 0, " AND CIDALMACEN = " & Almacen, "") &
        " ORDER BY CFECHACO01 DESC, CIDMOVIM01 DESC"


        Return StrSQL
    End Function

    Public Function RegresaConsecutivo(ByVal NoMovimientoPosible As Integer, ByVal ConStr As String, ByVal Campo As String, ByVal TablaAfecta As String,
    ByVal TablaConsulta As String, Optional ByVal FiltroAfectaConWhere As String = "", Optional ByVal FiltroConsultaConWhere As String = "") As String
        'StrSQL = "DECLARE @NUMSIGUIENTE AS INT " & _
        '"set @numsiguiente = isnull((select isnull(NOMOVIMIENTO,0) as NOMOVIMIENTO from CabMovimientos  where NoMovimiento = 1),0) " & _
        '"if @numsiguiente = 0" & _
        '"begin" & _
        '"     UPDATE PARAMETROS SET NOMOVIMIENTO = ISNULL(NOMOVIMIENTO,0) - 1   WHERE IDPARAMETRO = 1  " & _
        '"end"

        StrSQL = "DECLARE @NUMSIGUIENTE AS INT " &
        "set @numsiguiente = isnull((select isnull(" & Campo & ",0) as " & Campo & " from " & TablaConsulta & " " & FiltroConsultaConWhere & "),0) " &
        "if @numsiguiente = 0" &
        "begin" &
        "     UPDATE " & TablaAfecta & "  SET " & Campo & " = ISNULL(" & Campo & ",0) - 1   " & FiltroAfectaConWhere & " " &
        "end"


        Return StrSQL
    End Function

    'strSql = "DECLARE @NUMSIGUIENTE AS INT " & _
    '      " set @numsiguiente = isnull((select top 1 isnull(" & Campo & ",0) as " & Campo & " from " & TablaConsulta & " " & FiltroConsultaConWhere & " ),0) " & _
    '      "  " & _
    '      " UPDATE " & TablaAfecta & " SET " & Campo & " = ISNULL(@NUMSIGUIENTE,0) + 1 " & _
    '      " " & FiltroAfectaConWhere & " " & _
    '      " SELECT ISNULL(@numsiguiente,0) + 1 AS " & Campo & " ,@@ROWCOUNT AS REGAFECTADOS "
    Public Function ActualizaCabeceroPadre(ByVal NoMovimiento As Integer, ByVal NomCampo As String, ByVal valor As Object,
    ByVal TipoCampo As TipoDato, ByVal OpActualiza As TipoOpcionActualiza, Optional ByVal UnidadIncreDecre As Integer = 1)

        'GENERICO
        Dim Filtro As String = ""

        If TipoCampo = TipoDato.TdBoolean Then
            Filtro = NomCampo & " = " & IIf(valor, 1, 0)
        ElseIf TipoCampo = TipoDato.TdCadena Then
            Filtro = NomCampo & " = '" & IIf(valor, 1, 0) & "'"
        ElseIf TipoCampo = TipoDato.TdNumerico Then
            If OpActualiza = TipoOpcionActualiza.ToNinguno Then
                Filtro = NomCampo & " = " & Val(valor)
            ElseIf OpActualiza = TipoOpcionActualiza.ToIncrementa Then
                Filtro = NomCampo & " = " & NomCampo & " + " & UnidadIncreDecre
            ElseIf OpActualiza = TipoOpcionActualiza.ToDecrementa Then
                Filtro = NomCampo & " = " & NomCampo & " - " & UnidadIncreDecre
            End If

        ElseIf TipoCampo = TipoDato.TdFecha Then
            Filtro = NomCampo & " = '" & FormatFecHora(valor, True, True) & "'"
        End If

        StrSQL = "UPDATE CABMOVIMIENTOS SET " & Filtro & " where NoMovimiento = " & NoMovimiento


        Return StrSQL
    End Function

    Public Function ActualizaCampoTabla(ByVal Tabla As String, ByVal NomCampo As String, ByVal TipoCampo As TipoDato,
    ByVal valorCampo As String, ByVal NomCampoWhere As String, ByVal TipoCampoWhere As TipoDato, ByVal ValorCampoWhere As String)
        Dim filtro As String = ""
        Dim filtroWhere As String = ""

        If TipoCampo = TipoDato.TdBoolean Then
        ElseIf TipoCampo = TipoDato.TdCadena Then
            filtro = NomCampo & " = '" & valorCampo & "'"

        ElseIf TipoCampo = TipoDato.TdNumerico Then
            filtro = NomCampo & " = " & CDbl(valorCampo)
        End If

        If TipoCampoWhere = TipoDato.TdCadena Then
            filtroWhere = NomCampoWhere & " = '" & ValorCampoWhere & "'"
        ElseIf TipoCampoWhere = TipoDato.TdNumerico Then
            filtroWhere = NomCampoWhere & " = " & ValorCampoWhere
        ElseIf TipoCampoWhere = TipoDato.TdFecha Then
            filtroWhere = NomCampoWhere & " = '" & ValorCampoWhere & "'"
        ElseIf TipoCampoWhere = TipoDato.TdFechaHora Then
            filtroWhere = NomCampoWhere & " = '" & ValorCampoWhere & "'"
        ElseIf TipoCampoWhere = TipoDato.TdBoolean Then
            'filtroWhere = NomCampoWhere & " = " & IIf(ValorCampoWhere = "true", 1, 0)
            filtroWhere = NomCampoWhere & " = " & ValorCampoWhere
        End If



        StrSQL = "UPDATE " & Tabla & " SET " & filtro & " WHERE " & filtroWhere


        Return StrSQL
    End Function


    Public Function InsertaCatServFact(ByVal CIDPRODUCTO As Integer, ByVal CNOMBREPRODUCTO As String, ByVal CCODIGOPRODUCTO As String, ByVal IdCliente As Integer, ByVal TipoViaje As String)
        StrSQL = "INSERT INTO dbo.CatServFact ( CIDPRODUCTO ,CNOMBREPRODUCTO,CCODIGOPRODUCTO,IdCliente,TipoViaje) " &
        " VALUES (" & CIDPRODUCTO & ",'" & CNOMBREPRODUCTO & "','" & CCODIGOPRODUCTO & "'," & IdCliente & ",'" & TipoViaje & "')"

        Return StrSQL
    End Function

#Region "ORDEN DE SERVICIO"
    Public Function CambiaEstatusPadre(ByVal idPadreOrdSer As Integer, ByVal Estatus As String)

        StrSQL = "UPDATE dbo.MasterOrdServ SET Estatus = '" & Estatus & "' WHERE idPadreOrdSer = " & idPadreOrdSer

        Return StrSQL


    End Function
    Public Function CancelaPadre(ByVal idPadreOrdSer As Integer, ByVal UserCancela As String, ByVal MotivoCancela As String) As String

        StrSQL = "UPDATE dbo.MasterOrdServ SET Estatus = 'CAN', UserCancela = '" & UserCancela & "', FechaCancela = '" &
            FormatFecHora(Now, True, True) & "', MotivoCancela = '" & MotivoCancela & "' WHERE idPadreOrdSer = " & idPadreOrdSer

        Return StrSQL

    End Function


    Public Function InsertaPadre(ByVal idPadreOrdSer As Integer, ByVal idTractor As String, ByVal idTolva1 As String, ByVal idDolly As String,
    ByVal idTolva2 As String, ByVal idChofer As Integer, ByVal FechaCreacion As String, ByVal UserCrea As String, ByVal Estatus As String) As String

        StrSQL = "INSERT INTO dbo.MasterOrdServ (idPadreOrdSer,idTractor,idTolva1,idDolly,idTolva2,idChofer,FechaCreacion,UserCrea,Estatus) " &
        "VALUES (" & idPadreOrdSer & ",'" & idTractor & "','" & idTolva1 & "','" & idDolly & "','" & idTolva2 & "'," & idChofer &
        ",'" & FechaCreacion & "','" & UserCrea & "','" & Estatus & "')"

        Return StrSQL
    End Function

    Public Function InsertaOrdenServicio(ByVal idOrdenSer As Integer, ByVal IdEmpresa As Integer, ByVal idUnidadTrans As String, ByVal idTipoOrden As Integer,
    ByVal Kilometraje As Integer, ByVal Estatus As String, ByVal idEmpleadoAutoriza As Integer, ByVal fechaCaptura As String,
    ByVal idTipOrdServ As Integer, ByVal idTipoServicio As Integer, ByVal idPadreOrdSer As Integer) As String

        StrSQL = "INSERT INTO dbo.CabOrdenServicio (idOrdenSer,IdEmpresa,idUnidadTrans,idTipoOrden, " &
        "Kilometraje,Estatus,idEmpleadoAutoriza,fechaCaptura,idTipOrdServ, " &
        "idTipoServicio,idPadreOrdSer) " &
        "VALUES (" & idOrdenSer & "," & IdEmpresa & ",'" & idUnidadTrans & "'," & idTipoOrden & "," &
        Kilometraje & ",'" & Estatus & "'," & idEmpleadoAutoriza & ",'" & fechaCaptura & "'," & idTipOrdServ & ", " &
        idTipoServicio & "," & idPadreOrdSer & " ) "


        Return StrSQL
    End Function
    Public Function InsertaOrdenServicioDET(ByVal idOrdenSer As Integer, ByVal idServicio As Integer, ByVal idActividad As Integer,
    ByVal DuracionActHr As Integer, ByVal NoPersonal As Integer, ByVal id_proveedor As Integer,
    ByVal Estatus As String, ByVal CostoManoObra As Double, ByVal CostoProductos As Double,
    ByVal idOrdenTrabajo As Integer, ByVal idDivision As Integer, ByVal idLlanta As String, ByVal Posicion As Integer) As String

        StrSQL = "insert into DetOrdenServicio (idOrdenSer,idServicio,idActividad,DuracionActHr,NoPersonal,id_proveedor, " &
        "Estatus, CostoManoObra, CostoProductos, idOrdenTrabajo, idDivision,idLlanta,PosLLanta) values (" & idOrdenSer & "," & idServicio &
        "," & idActividad & "," & DuracionActHr & "," & NoPersonal & "," & id_proveedor & ",'" &
        Estatus & "'," & CostoManoObra & "," & CostoProductos & "," & idOrdenTrabajo & "," & idDivision & ",'" & idLlanta & "'," & Posicion & " )"

        Return StrSQL
    End Function

    Public Function InsertaOrdenTrabajo(ByVal idOrdenTrabajo As Integer, ByVal idOrdenSer As Integer,
ByVal idFamilia As Integer, ByVal NotaRecepcion As String, ByVal isCancelado As Integer,
ByVal idDivision As Integer, ByVal idPersonalResp As Integer, ByVal idPersonalAyu1 As Integer, ByVal idPersonalAyu2 As Integer) As String

        StrSQL = "insert into DetRecepcionOS (idOrdenTrabajo,idOrdenSer,idFamilia,NotaRecepcion, " &
        "idDivision, idPersonalResp, idPersonalAyu1, idPersonalAyu2) " &
        "values (" & idOrdenTrabajo & "," & idOrdenSer & "," & idFamilia & ",'" & NotaRecepcion & "'," &
        idDivision & "," & idPersonalResp & "," & idPersonalAyu1 & "," & idPersonalAyu2 & ") "

        Return StrSQL
    End Function

#End Region

    Public Function InsertaKM(ByVal idUnidadTrans As String, ByVal kmInicial As Integer, ByVal kmFinal As Integer,
     ByVal fecha As String, ByVal usuario As Integer, ByVal nomTabla As String, ByVal idTabla As Integer,
     ByVal activo As Boolean, ByVal validado As Boolean) As String

        '13/OCT/2016

        StrSQL = "INSERT INTO dbo.registrosKilometros " &
        "(idUnidadTrans,kmInicial,kmFinal,fecha,usuario,nomTabla,idTabla,activo,validado) " &
        "VALUES " &
        "('" & idUnidadTrans & "'," & kmInicial & "," & kmFinal & ",'" & fecha &
        "'," & usuario & ",'" & nomTabla & "'," & idTabla & "," & IIf(activo, 1, 0) & "," & IIf(validado, 1, 0) & ")"


        Return StrSQL
    End Function

    Public Function ImpOrdServSinOT(ByVal idOrdenSer As Integer) As String
        '271
        StrSQL = "SELECT cab.idOrdenSer,CAB.IdEmpresa,CAB.idUnidadTrans,CAB.idTipoOrden,CAB.idPadreOrdSer,CAB.Kilometraje, " &
        "CAB.idTipOrdServ,TOS.NomTipOrdServ, TOS.bOrdenTrab, " &
        "CAB.idTipoServicio,TS.NomTipoServicio, " &
        "DET.idOrdenActividad, DET.idOrdenSer, " &
        "isnull(DET.idServicio,0) as idServicio,SER.idTipoServicio, isnull(SER.NomServicio,'') as NomServicio , " &
        "isnull(DET.idActividad,0) as idActividad,isnull(ACT.NombreAct,'') as NombreAct, " &
        "DET.idOrdenTrabajo, DET.Estatus, emp.RazonSocial,CAB.fechaCaptura, " &
        "PAD.IDTRACTOR,ISNULL(PAD.IDTOLVA1,'') AS IDTOLVA1, ISNULL(PAD.IDDOLLY,'') AS IDDOLLY, ISNULL(PAD.IDTOLVA2,'') AS IDTOLVA2, " &
        "OPE.NombreCompleto AS NomOperador, AUT.NombreCompleto AS NomAutoriza " &
        "FROM dbo.CabOrdenServicio CAB " &
        "INNER JOIN dbo.DetOrdenServicio DET ON cab.idOrdenSer = det.idOrdenSer " &
        "INNER JOIN dbo.CatTipoOrdServ TOS ON CAB.idTipOrdServ = TOS.idTipOrdServ " &
        "INNER JOIN dbo.CatTipoServicio TS ON CAB.idTipoServicio = TS.idTipoServicio " &
        "LEFT JOIN dbo.CatServicios SER ON DET.idServicio = SER.idServicio " &
        "LEFT JOIN dbo.CatActividades ACT ON DET.idActividad = ACT.idActividad " &
        "INNER JOIN dbo.CatEmpresas emp ON cab.IdEmpresa = emp.idEmpresa " &
        "INNER JOIN DBO.MASTERORDSERV PAD ON CAB.IDPADREORDSER = PAD.IDPADREORDSER " &
        "INNER JOIN dbo.CatPersonal OPE ON PAD.idChofer = OPE.idPersonal " &
        "INNER JOIN dbo.CatPersonal AUT ON CAB.idEmpleadoAutoriza = AUT.idPersonal " &
        "WHERE cab.idOrdenSer = " & idOrdenSer

        Return StrSQL
    End Function

    Public Function ImpPreSalida(ByVal IdPreSalida As Integer)
        StrSQL = "SELECT cab.IdPreSalida, CAB.idOrdenTrabajo, DETOS.idOrdenSer, CABOS.IdEmpresa, " &
        "CABOS.idUnidadTrans, tuni.clasificacion,CABOS.idTipoOrden, CABOS.idPadreOrdSer, " &
        "CABOS.idTipOrdServ,TOS.NomTipOrdServ, TOS.bOrdenTrab, " &
        "CABOS.idTipoServicio,TS.NomTipoServicio, " &
        "cab.FecSolicitud AS FEcha, " &
        "detos.DuracionActHr, " &
        "detos.UsuarioAsig,ISNULL(PerUsr.NombreCompleto,'') AS NomAutoriza, " &
        "tra.idPersonalResp,resp.NombreCompleto AS Responsable,PUERESP.NomPuesto AS PuestoResp, " &
        "tra.NotaRecepcion, " &
        "DET.idProducto, DET.idUnidadProd, DET.Cantidad, " &
        "PRO.CNOMBREPRODUCTO, PRO.CCODIGOPRODUCTO " &
        "FROM dbo.CabPreSalida CAB " &
        "INNER JOIN dbo.DetPreSalida DET ON DET.IdPreSalida = CAB.IdPreSalida " &
        "INNER JOIN dbo.DetOrdenServicio DETOS ON DETOS.idOrdenTrabajo = CAB.idOrdenTrabajo " &
        "INNER JOIN dbo.CabOrdenServicio CABOS ON CABOS.idOrdenSer = DETOS.idOrdenSer " &
        "LEFT JOIN dbo.CatTipoOrdServ TOS ON CABOS.idTipOrdServ = TOS.idTipOrdServ " &
        "LEFT JOIN dbo.CatTipoServicio TS ON CABOS.idTipoServicio = TS.idTipoServicio " &
        "LEFT JOIN dbo.DetRecepcionOS TRA ON CABOS.idOrdenSer = tra.idOrdenSer AND DETOS.idOrdenTrabajo = tra.idOrdenTrabajo " &
        "INNER JOIN dbo.CatProductosCOM PRO ON DET.idProducto = PRO.CIDPRODUCTO " &
        "INNER JOIN dbo.CatPersonal RESP ON resp.idPersonal = tra.idPersonalResp " &
        "INNER JOIN dbo.CatPuestos PUERESP ON resp.idPuesto = PUERESP.idPuesto " &
        "INNER JOIN dbo.CatUnidadTrans uni ON uni.idUnidadTrans = CABOS.idUnidadTrans " &
        "INNER JOIN dbo.CatTipoUniTrans Tuni ON Tuni.idTipoUniTras = uni.idTipoUnidad " &
        "LEFT JOIN dbo.usuariosSys usr ON DETOS.UsuarioAsig = usr.nombreUsuario " &
        "LEFT JOIN dbo.CatPersonal PerUsr ON PerUsr.idPersonal = usr.idPersonal " &
        "WHERE CAB.IdPreSalida = " & IdPreSalida

        Return StrSQL

    End Function
    Public Function ImpOrdServConOT(ByVal idOrdenSer As Integer) As String
        '272
        'StrSQL = "SELECT cab.idOrdenSer,CAB.IdEmpresa,CAB.idUnidadTrans,CAB.idTipoOrden,CAB.idPadreOrdSer,CAB.Kilometraje, " &
        '"CAB.idTipOrdServ,TOS.NomTipOrdServ, TOS.bOrdenTrab, " &
        '"CAB.idTipoServicio,TS.NomTipoServicio, " &
        '"DET.idOrdenActividad, DET.idOrdenSer, " &
        '"isnull(DET.idServicio,0) as idServicio,SER.idTipoServicio, isnull(SER.NomServicio,'') as NomServicio , " &
        '"isnull(DET.idActividad,0) as idActividad,isnull(ACT.NombreAct,'') as NombreAct, " &
        '"DET.idOrdenTrabajo, DET.Estatus, " &
        '"TRA.idOrdenTrabajo, TRA.NotaRecepcion,TRA.idDivision, DIV.NomDivision, emp.RazonSocial,CAB.fechaCaptura, " &
        '"PAD.IDTRACTOR,ISNULL(PAD.IDTOLVA1,'') AS IDTOLVA1, ISNULL(PAD.IDDOLLY,'') AS IDDOLLY, ISNULL(PAD.IDTOLVA2,'') AS IDTOLVA2, " &
        '"OPE.NombreCompleto AS NomOperador, AUT.NombreCompleto AS NomAutoriza " &
        '"FROM dbo.CabOrdenServicio CAB " &
        '"INNER JOIN dbo.DetOrdenServicio DET ON cab.idOrdenSer = det.idOrdenSer " &
        '"INNER JOIN dbo.CatTipoOrdServ TOS ON CAB.idTipOrdServ = TOS.idTipOrdServ " &
        '"INNER JOIN dbo.CatTipoServicio TS ON CAB.idTipoServicio = TS.idTipoServicio " &
        '"LEFT JOIN dbo.CatServicios SER ON DET.idServicio = SER.idServicio " &
        '"LEFT JOIN dbo.CatActividades ACT ON DET.idActividad = ACT.idActividad " &
        '"INNER JOIN dbo.DetRecepcionOS TRA ON cab.idOrdenSer = tra.idOrdenSer AND det.idOrdenTrabajo = tra.idOrdenTrabajo " &
        '"INNER JOIN dbo.CatDivision DIV ON TRA.idFamilia = DIV.idDivision " &
        '"INNER JOIN dbo.CatEmpresas emp ON cab.IdEmpresa = emp.idEmpresa " &
        '"INNER JOIN DBO.MASTERORDSERV PAD ON CAB.IDPADREORDSER = PAD.IDPADREORDSER " &
        '"INNER JOIN dbo.CatPersonal OPE ON PAD.idChofer = OPE.idPersonal " &
        '"INNER JOIN dbo.CatPersonal AUT ON CAB.idEmpleadoAutoriza = AUT.idPersonal " &
        '"WHERE cab.idOrdenSer = " & idOrdenSer

        StrSQL = "SELECT cab.idOrdenSer,CAB.IdEmpresa,CAB.idUnidadTrans,CAB.idTipoOrden,CAB.idPadreOrdSer,CAB.Kilometraje, " &
        "CAB.idTipOrdServ,TOS.NomTipOrdServ, TOS.bOrdenTrab, " &
        "CAB.idTipoServicio,TS.NomTipoServicio, " &
        "DET.idOrdenActividad, DET.idOrdenSer, " &
        "isnull(DET.idServicio,0) as idServicio,SER.idTipoServicio, isnull(SER.NomServicio,'') as NomServicio , " &
        "isnull(DET.idActividad,0) as idActividad,isnull(ACT.NombreAct,'') as NombreAct, " &
        "DET.idOrdenTrabajo, DET.Estatus, " &
        "TRA.idOrdenTrabajo, isnull(TRA.NotaRecepcion,'') as NotaRecepcion,TRA.idDivision, DIV.NomDivision, emp.RazonSocial,CAB.fechaCaptura, " &
        "PAD.IDTRACTOR,ISNULL(PAD.IDTOLVA1,'') AS IDTOLVA1, ISNULL(PAD.IDDOLLY,'') AS IDDOLLY, ISNULL(PAD.IDTOLVA2,'') AS IDTOLVA2, " &
        "OPE.NombreCompleto AS NomOperador, isnull(AUT.NombreCompleto,'') AS NomAutoriza " &
        "FROM dbo.CabOrdenServicio CAB " &
        "LEFT JOIN dbo.DetOrdenServicio DET ON cab.idOrdenSer = det.idOrdenSer " &
        "LEFT JOIN dbo.CatTipoOrdServ TOS ON CAB.idTipOrdServ = TOS.idTipOrdServ " &
        "LEFT JOIN dbo.CatTipoServicio TS ON CAB.idTipoServicio = TS.idTipoServicio " &
        "LEFT JOIN dbo.CatServicios SER ON DET.idServicio = SER.idServicio " &
        "LEFT JOIN dbo.CatActividades ACT ON DET.idActividad = ACT.idActividad " &
        "LEFT JOIN dbo.DetRecepcionOS TRA ON cab.idOrdenSer = tra.idOrdenSer AND det.idOrdenTrabajo = tra.idOrdenTrabajo " &
        "LEFT JOIN dbo.CatDivision DIV ON TRA.idFamilia = DIV.idDivision " &
        "LEFT JOIN dbo.CatEmpresas emp ON cab.IdEmpresa = emp.idEmpresa " &
        "LEFT JOIN DBO.MASTERORDSERV PAD ON CAB.IDPADREORDSER = PAD.IDPADREORDSER " &
        "LEFT JOIN dbo.CatPersonal OPE ON PAD.idChofer = OPE.idPersonal " &
        "LEFT JOIN dbo.CatPersonal AUT ON CAB.idEmpleadoAutoriza = AUT.idPersonal " &
        "WHERE cab.idOrdenSer = " & idOrdenSer

        Return StrSQL
    End Function

#Region "Monitores"
    Public Function MonitorServicios(Optional ByVal idTipOrdServ = 0) As String

        StrSQL = "SELECT " &
        "os.fechaCaptura as Fecha, " &
        "'' as TiempoTrasncurrido, " &
        "os.idPadreOrdSer AS Folio, " &
        "os.idOrdenSer AS OrdenServicio, " &
        "emp.RazonSocial as Empresa, " &
        "os.idUnidadTrans AS UniTrans, " &
        "tuni.clasificacion, " &
        "os.Estatus, " &
        "os.FechaRecepcion, " &
        "os.UsuarioRecepcion, " &
        "os.FechaEntregado, " &
        "os.idEmpleadoRecibe, " &
        "tos.NomTipOrdServ, " &
        "com.fechaCombustible as [Fecha Carga], " &
        "ISNULL((pe.Nombre + ' ' + pe.ApellidoPat + ' ' +  pe.ApellidoMat),'') AS despachador, " &
        "CASE WHEN ISNULL(com.idDespachador,0) <> 0 THEN 'FINALIZADO' " &
        "WHEN ISNULL(com.idDespachador,0) = 0 AND isnull(com.idCargaCombustible,0) > 0 THEN 'EN BASE' " &
        "ELSE 'PENDIENTE' END estatus2, " &
        "com.idDespachador, " &
        "com.idCargaCombustible " &
        "FROM dbo.CabOrdenServicio OS " &
        "INNER JOIN dbo.CatTipoOrdServ tos ON tos.idTipOrdServ = OS.idTipOrdServ " &
        "INNER JOIN dbo.CatEmpresas emp ON os.IdEmpresa = emp.idEmpresa " &
        "left JOIN dbo.cargaCombustible com ON os.idOrdenSer = com.idOrdenServ " &
        "LEFT JOIN dbo.CatPersonal pe ON com.idDespachador = pe.idPersonal " &
        "INNER JOIN dbo.CatUnidadTrans uni ON os.idUnidadTrans = uni.idUnidadTrans " &
        "INNER JOIN dbo.CatTipoUniTrans tuni ON uni.idTipoUnidad = tuni.idTipoUniTras " &
        IIf(idTipOrdServ = 0, "", "WHERE os.idTipOrdServ = " & idTipOrdServ)
        '"WHERE os.idTipOrdServ = 1 " &
        '"ORDER BY os.fechaCaptura DESC"

        Return StrSQL

    End Function
    Public Function MonitorEntSal() As String
        StrSQL = "SELECT ma.FechaCreacion AS Fecha, " &
        "'' AS TiempoTrasncurrido, " &
        "ma.idPadreOrdSer AS FOLIO, " &
        "ma.idTractor AS Camion, " &
        "ma.idTolva1 AS Remolque1, " &
        "ma.idDolly AS Dolly, " &
        "ma.idTolva2 AS Remolque2, " &
        "ma.idChofer, " &
        "ISNULL((pe.Nombre + ' ' + pe.ApellidoPat + ' ' +  pe.ApellidoMat),'CHOFER') AS Chofer, " &
        "em.RazonSocial as Empresa, " &
        "CASE WHEN MA.ESTATUS = 'SAL' THEN 'SALIDA' WHEN MA.ESTATUS = 'ENT' THEN 'ENTRADA' WHEN MA.ESTATUS = 'CAP' THEN 'POR ENTRAR'  " &
        "WHEN MA.ESTATUS = 'CAN' THEN 'CANCELADA' END AS Estatus, " &
        "ISNULL(dbo.fn_NomServxID(ma.idPadreOrdSer),'') AS servicios, " &
         "se.fecha AS FechaEntrada, " &
        "se.fechaFin AS FechaSalida, " &
        "ma.FechaCancela, " &
        "upper(MA.ESTATUS) as idEstatus " &
        "FROM dbo.MasterOrdServ ma " &
        "left JOIN dbo.CatPersonal pe ON ma.idChofer = pe.idPersonal " &
        "INNER JOIN dbo.CatUnidadTrans ut ON ma.idTractor = ut.idUnidadTrans " &
        "INNER JOIN dbo.CatEmpresas em ON ut.idEmpresa = em.idEmpresa " &
        "LEFT JOIN dbo.salidasEntradas se ON ma.idPadreOrdSer = se.idPadreOrdSer AND ma.idTractor = se.idUnidadTrans "
        '"ORDER BY MA.FechaCreacion DESC"

        'LEFT JOIN dbo.salidasEntradas se ON ma.idPadreOrdSer = se.idPadreOrdSer
        '
        'IIf(sFiltro = "", "", " where " & sFiltro)


        Return StrSQL
    End Function

    Public Function MonitorEntSalBitacora() As String
        StrSQL = "SELECT bes.fechaEntrada AS [Fecha Entrada], " &
        "bes.fechaSalida AS [Fecha Salida], " &
        "'' AS TiempoTrasncurrido, " &
        "bes.idBitacora AS [Folio Bitacora], " &
        "bes.idTractor AS Camion, " &
        "ISNULL(bes.idTolva1,'') AS Remolque1, " &
        "ISNULL(bes.idDolly,'') AS Dolly, " &
        "ISNULL(bes.idTolva2,'') AS Remolque2, " &
        "bes.idOperador, " &
        "ISNULL((pe.Nombre + ' ' + pe.ApellidoPat + ' ' +  pe.ApellidoMat),'CHOFER') AS Chofer, " &
        "UPPER(fle.RazonSocial) AS Empresa, " &
        "CASE WHEN isnull(BES.fechaSalida,'') = '' THEN 'ENTRADA' ELSE 'SALIDA' END AS Estatus, " &
        "ISNULL((peu.Nombre + ' ' + peu.ApellidoPat + ' ' +  peu.ApellidoMat),'USUARIO') AS Usuario " &
        "FROM dbo.BitacoraEntradasSalidas BES " &
        "LEFT JOIN dbo.CatPersonal pe ON bes.idOperador = pe.idPersonal " &
        "inner JOIN dbo.CatUnidadTrans UT ON bes.idTractor = ut.idUnidadTrans " &
        "INNER JOIN dbo.CatEmpresas fle ON fle.idEmpresa = UT.idEmpresa " &
        "INNER JOIN dbo.usuariosSys usu ON bes.usuario = usu.nombreUsuario " &
        "INNER JOIN dbo.CatPersonal peu ON usu.idPersonal = peu.idPersonal "

        Return StrSQL

    End Function

    Public Function CreaTabla(ByVal vTabla As String, ByVal vStrsql As String, Optional ByVal FiltroSinWhere As String = "") As DataTable
        MyTabla = New DataTable(vTabla)
        MyTabla.Rows.Clear()

        StrSQL = vStrsql & FiltroSinWhere

        MyTabla = BD.ExecuteReturn(StrSQL)

        Return MyTabla


    End Function

    Public Function InsertaCombustibleExtra(ByVal idTipoOSComb As Integer, ByVal idUnidadTrans As String, ByVal FechaCrea As String,
                                            ByVal UserCrea As String, ByVal idTipoCombustible As Integer, ByVal idChofer As Integer,
                                            ByVal NumViaje As Integer) As String
        StrSQL = "INSERT INTO dbo.cargaCombustibleExtra (idTipoOSComb, idUnidadTrans, FechaCrea, UserCrea, idTipoCombustible,idChofer,NumViaje) " &
        "VALUES ( " & idTipoOSComb & ",'" & idUnidadTrans & "','" & FormatFecHora(FechaCrea, True, True) & "','" & UserCrea &
        "'," & idTipoCombustible & "," & idChofer & "," & NumViaje & ")"


        Return StrSQL
    End Function

    Public Function CapturaCombustibleExtra(idVCExtra, fechaCombustible, ltCombustible, precioCombustible, importe, idDespachador) As String

        StrSQL = "update dbo.cargaCombustibleExtra SET " &
            "fechaCombustible = '" & FormatFecHora(fechaCombustible, True, True) & "', " &
            "ltCombustible = " & ltCombustible & ", " &
            "precioCombustible = " & precioCombustible & ", " &
            "importe = " & importe & ", " &
            "idDespachador = " & idDespachador &
            " where idVCExtra = " & idVCExtra

        Return StrSQL
    End Function

    Public Function InsertaRecibosDinero(ByVal vFecha As String, ByVal vidEmpleadoEnt As Integer,
    ByVal vidEmpleadoRec As Integer, ByVal vEstatus As String, ByVal vImporte As Decimal,
    ByVal vImporteComp As Decimal, ByVal Concepto As String)
        StrSQL = "insert into RecibosDinero (Fecha,idEmpleadoEnt,idEmpleadoRec,Estatus,Importe,ImporteComp,Concepto) values " +
        "('" & FormatFecHora(vFecha, True, True) & "'," & vidEmpleadoEnt & "," & vidEmpleadoRec &
        ",'" & vEstatus & "'," & vImporte & "," & vImporteComp & ",'" & Concepto & "')"

        Return StrSQL

    End Function

    Public Function InsertaFoliosDinero(ByVal NoRecibo As Integer, ByVal Fecha As DateTime,
    ByVal idEmpleadoEnt As Integer, ByVal idEmpleadoRec As Integer, ByVal Estatus As String, ByVal Importe As Decimal,
    ByVal ImporteComp As Decimal, ByVal NumViaje As Integer, ByVal idTipoFolDin As Integer, ByVal idEmpresa As Integer)

        StrSQL = "insert into FoliosDinero (NoRecibo,Fecha,idEmpleadoEnt,idEmpleadoRec,Estatus,Importe,ImporteComp,NumViaje,idTipoFolDin,idEmpresa) " &
        "values (" & NoRecibo & ",'" & FormatFecHora(Fecha, True, True) & "'," & idEmpleadoEnt & "," & idEmpleadoRec & ",'" & Estatus &
        "'," & Importe & "," & ImporteComp & "," & NumViaje & "," & idTipoFolDin & "," & idEmpresa & ") "

        Return StrSQL
    End Function



#End Region

#Region "DINERO"
    Public Function CancelaFacturaMovimiento(ByVal NoMoviento As Integer, ByVal UsuarioCancela As String)
        StrSQL = "UPDATE dbo.CabMovimientos SET Estatus = 'CAN', " &
                "UsuarioCancela = '" & UsuarioCancela &
                "',FechaCancela = GETDATE() " &
                "WHERE NoMovimiento = " & NoMoviento

        Return StrSQL
    End Function

    Public Function ActivaViajeFacturaCan(ByVal Factura As String, ByVal Estatus As String)
        StrSQL = "UPDATE dbo.CabGuia Set " &
        "EstatusGuia = '" & Estatus & "', " &
        "NoSerie = '', " &
        "NoFolio = 0 " &
        "WHERE (NoSerie + CAST(NoFolio AS VARCHAR(50))) = '" & Factura & "'"

        Return StrSQL

    End Function

    Public Function ImpReciboDinero(ByVal NoRecibo As Integer)
        StrSQL = "SELECT rd.NoRecibo, " &
        "rd.idEmpresa, " &
        "em.RazonSocial, " &
        "rd.Fecha, " &
        "rd.idEmpleadoEnt, " &
        "pe.NombreCompleto as NomEmpEnt, " &
        "rd.idEmpleadoRec, " &
        "pr.NombreCompleto as NomEmpRec, " &
        "rd.Estatus, " &
        "rd.Importe, " &
        "rd.ImporteComp, " &
        "(rd.Importe - rd.ImporteComp) AS Saldo, " &
        "isnull(rd.Concepto,'') as Concepto " &
        "FROM dbo.RecibosDinero RD " &
        "INNER JOIN dbo.CatEmpresas em ON em.idEmpresa = RD.idEmpresa " &
        "INNER JOIN dbo.CatPersonal pe ON pe.idPersonal = rd.idEmpleadoEnt " &
        "INNER JOIN dbo.CatPersonal pr ON pr.idPersonal = rd.idEmpleadoRec " &
        "WHERE rd.NoRecibo = " & NoRecibo

        Return StrSQL

    End Function

    Public Function ImpFolioDinero(ByVal Folio As Integer)
        StrSQL = "SELECT fd.Folio, " &
        "fd.NoRecibo, " &
        "fd.idTipoFolDin, " &
        "td.Descripcion AS TipoDinero, " &
        "fd.idEmpresa, " &
        "em.RazonSocial, " &
        "fd.Fecha, " &
        "fd.idEmpleadoEnt, " &
        "pe.NombreCompleto AS NomEmpEnt, " &
        "fd.idEmpleadoRec, " &
        "pr.NombreCompleto AS NomEmpRec, " &
        "fd.Estatus, " &
        "fd.Importe, " &
        "fd.ImporteComp, " &
        "fd.NumViaje, " &
        "vi.idOperador, " &
        "vi.idTractor, vi.idRemolque1, ISNULL(vi.idDolly,'') AS idDolly, ISNULL(vi.idRemolque2,'') AS idRemolque2 " &
        "FROM dbo.FoliosDinero FD " &
        "INNER JOIN dbo.CatEmpresas em ON em.idEmpresa = FD.idEmpresa " &
        "INNER JOIN dbo.CatPersonal pe ON pe.idPersonal = FD.idEmpleadoEnt " &
        "INNER JOIN dbo.CatPersonal pr ON pr.idPersonal = FD.idEmpleadoRec " &
        "INNER JOIN dbo.CatTiposFoliosDin td ON td.idTipoFolDin = FD.idTipoFolDin " &
        "left JOIN dbo.CabGuia vi ON vi.NumGuiaId = FD.NumViaje " &
        "WHERE fd.Folio = " & Folio

        Return StrSQL


    End Function

    Public Function OrigenDestino(ByVal NumGuiaId As Integer) As String
        StrSQL = "SELECT DISTINCT (Ori.nombreOrigen + '-' + dest.Descripcion) as OriDes " &
         "FROM dbo.DetGuia det " &
         "INNER JOIN dbo.CabGuia cab ON Cab.NumGuiaId = Det.NumGuiaId " &
         "INNER JOIN dbo.origen Ori ON det.idOrigen = ori.idOrigen " &
         "INNER JOIN dbo.trafico_plazas_cat Dest ON det.IdPlazaTraf = dest.Clave " &
         "WHERE Cab.NumGuiaId = " & NumGuiaId

        Return StrSQL

    End Function

    Public Function InsertaComprobanteFolio(ByVal Folio As Integer, ByVal NoDocumento As String, ByVal idGasto As Integer,
    ByVal Fecha As DateTime, ByVal Importe As Decimal, ByVal IVA As Decimal, ByVal Deducible As Boolean, ByVal Autoriza As Integer,
    ByVal Observaciones As String, ByVal ReqComprobante As Boolean, ByVal ReqAutorizacion As Boolean)
        StrSQL = "INSERT INTO dbo.CompGastosFolios " &
        "( Folio ,NoDocumento ,idGasto ,Fecha ,Importe ,IVA , " &
        "ReqComprobante ,Autoriza ,Observaciones,ReqAutorizacion,Deducible) " &
        "values (" & Folio & ",'" & NoDocumento & "'," & idGasto & ",'" & FormatFecHora(Fecha, True, True) & "'," & Importe & "," & IVA &
        "," & IIf(ReqComprobante, 1, 0) & "," & Autoriza & ",'" & Observaciones & "'," & IIf(ReqAutorizacion, 1, 0) & "," & IIf(Deducible, 1, 0) & ")"

        Return StrSQL
    End Function


#End Region


    Public Function RecepcionaCabOrdenServicio(ByVal idOrdenSer As Integer, ByVal FechaRecepcion As DateTime,
    ByVal idEmpleadoEntrega As Integer, ByVal UsuarioRecepcion As String)

        StrSQL = "UPDATE dbo.CabOrdenServicio SET FechaRecepcion = '" & FormatFecHora(FechaRecepcion, True, True) &
        "',idEmpleadoEntrega = " & idEmpleadoEntrega & ", UsuarioRecepcion = '" & UsuarioRecepcion &
        "', Estatus = 'REC' WHERE idOrdenSer = " & idOrdenSer

        Return StrSQL
    End Function

    Public Function RecepcionaDetOrdenServicio(ByVal idOrdenSer As Integer)

        StrSQL = "UPDATE dbo.DetOrdenServicio SET Estatus = 'REC' WHERE idOrdenSer = " & idOrdenSer & " and Estatus = 'PRE'"

        Return StrSQL

    End Function

    Public Function InsertaLlantasHis(ByVal vidtipoUbicacion As Integer, ByVal vidUbicacion As String,
ByVal vFecha As DateTime, ByVal vUsuario As String, ByVal vidSisLlanta As Integer, ByVal vidLlanta As String, ByVal vEntSal As Boolean,
ByVal vidTipoDoc As Integer, ByVal vDocumento As String)
        'EntSal = True - Entrada = 1
        'EntSal = false - salida = 0

        '" & 
        '& "

        StrSQL = "INSERT INTO dbo.LlantasHis ( idtipoUbicacion ,idUbicacion ,Fecha ,Usuario ,idSisLlanta ,idLlanta ,EntSal , " &
            "idTipoDoc ,Documento)" &
            "VALUES (" & vidtipoUbicacion & "," & vidUbicacion & ",'" & FormatFecHora(vFecha, True, True) & "' ,'" & vUsuario &
            "' ," & vidSisLlanta & " ,'" & vidLlanta & "' ," & IIf(vEntSal, 1, 0) & " ," & vidTipoDoc & ",'" & vDocumento & "')"

        Return StrSQL
    End Function

    Public Function InsertaMovimientos(ByVal vTipoMov As Integer, ByVal vFecha As DateTime,
   ByVal vUsuario As String, ByVal vidSisLlanta As Integer, ByVal vidtipoUbicacionOrig As Integer, ByVal vidUbicacionOrig As String,
   ByVal vPosicionOrig As Integer, ByVal vidtipoUbicacionDes As Integer, ByVal vidUbicacionDes As String, ByVal vPosicionDes As Integer,
   ByVal vObservaciones As String) As String

        StrSQL = "insert into MovLLantas (TipoMov,Fecha,Usuario,idSisLlanta,idtipoUbicacionOrig, " &
        "idUbicacionOrig,PosicionOrig,idtipoUbicacionDes,idUbicacionDes,PosicionDes,Observaciones) values ( " &
        vTipoMov & ",'" & FormatFecHora(vFecha, True, True) & "','" & vUsuario & "'," & vidSisLlanta & "," &
        vidtipoUbicacionOrig & ",'" & vidUbicacionOrig & "'," & vPosicionOrig & "," & vidtipoUbicacionDes & ",'" & vidUbicacionDes & "'," &
        vPosicionDes & ",'" & vObservaciones & "')"

        Return StrSQL
    End Function

    'Public Function ActualizaCatLlantas(ByVal idtipoUbicacion As Integer, ByVal idUbicacion As String, ByVal Posicion As Integer,
    'ByVal UltFecha As DateTime, ByVal idSisLlanta As Integer) As String
    '    StrSQL = "UPDATE dbo.CatLlantas SET idtipoUbicacion = " & idtipoUbicacion & ", idUbicacion = '" & idUbicacion &
    '    "', Posicion = " & Posicion & ", UltFecha = '" & FormatFecHora(UltFecha, True) & "' WHERE idSisLlanta = " & idSisLlanta

    '    Return StrSQL
    'End Function

    Public Function GuardaHistorico(ByVal vidtipoUbicacion As Integer, ByVal vidUbicacion As String,
    ByVal vFecha As DateTime, ByVal vUsuario As String, ByVal vidSisLlanta As Integer,
    ByVal vidLlanta As String, ByVal vEntSal As Boolean, ByVal vidTipoDoc As Integer, ByVal vDocumento As String,
    ByVal vProfundidad As Decimal, ByVal vidCondicion As Integer) As String

        StrSQL = "INSERT INTO dbo.LlantasHis ( idtipoUbicacion ,idUbicacion ,Fecha ,Usuario ,idSisLlanta ,idLlanta ,EntSal , " &
            "idTipoDoc ,Documento,Profundidad,idCondicion)" &
            "VALUES (" & vidtipoUbicacion & ",'" & vidUbicacion & "','" & FormatFecHora(vFecha, True, True) & "' ,'" & vUsuario &
            "' ," & vidSisLlanta & " ,'" & vidLlanta & "' ," & IIf(vEntSal, 1, 0) & " ," & vidTipoDoc & ",'" & vDocumento &
            "'," & vProfundidad & "," & vidCondicion & ")"

        Return StrSQL

    End Function

    Public Function ActualizaCatLlantas(ByVal idtipoUbicacion As Integer, ByVal idUbicacion As String, ByVal Posicion As Integer,
    ByVal UltFecha As DateTime, ByVal idSisLlanta As Integer) As String

        StrSQL = "UPDATE dbo.CatLlantas SET idtipoUbicacion = " & idtipoUbicacion & ", idUbicacion = '" & idUbicacion &
        "', Posicion = " & Posicion & ", UltFecha = '" & FormatFecHora(UltFecha, True, True) & "' WHERE idSisLlanta = " & idSisLlanta

        Return StrSQL
    End Function

    Public Function AsignaCabOrdenServicio(ByVal FechaAsignado As DateTime, ByVal idOrdenSer As Integer) As String
        StrSQL = "UPDATE dbo.CabOrdenServicio SET Estatus = 'ASIG', FechaAsignado = '" &
        FormatFecHora(FechaAsignado, True, True) & "' WHERE idOrdenSer = " & idOrdenSer

        Return StrSQL
    End Function

    Public Function AsignaDetOrdenServicio(ByVal FechaAsignado As DateTime, ByVal idOrdenSer As Integer,
    ByVal idOrdenTrabajo As Integer, ByVal UsuarioAsig As String) As String
        StrSQL = "UPDATE dbo.DetOrdenServicio SET  UsuarioAsig = ' " & UsuarioAsig & "', Estatus = 'ASIG', FechaAsig = '" &
        FormatFecHora(FechaAsignado, True, True) & "' WHERE idOrdenSer = " & idOrdenSer & " AND idOrdenTrabajo = " & idOrdenTrabajo

        Return StrSQL
    End Function

    Public Function AsignaDetRecepcionOS(ByVal idPersonalResp As Integer, ByVal idPersonalAyu1 As Integer,
    ByVal idPersonalAyu2 As Integer, ByVal idOrdenSer As Integer, ByVal idOrdenTrabajo As Integer)
        StrSQL = "UPDATE dbo.DetRecepcionOS SET idPersonalResp = " & idPersonalResp & ", idPersonalAyu1 = " & idPersonalAyu1 &
        ", idPersonalAyu2 = " & idPersonalAyu2 & " WHERE idOrdenSer = " & idOrdenSer & " AND idOrdenTrabajo = " & idOrdenTrabajo

        Return StrSQL

    End Function
    Public Function DiagTerCabOrdenServicio(ByVal FechaTerminado As DateTime, ByVal idOrdenSer As Integer) As String

        StrSQL = "UPDATE dbo.CabOrdenServicio SET FechaDiagnostico = '" & FormatFecHora(FechaTerminado, True, True) &
        "', Estatus = 'TER', FechaTerminado = '" & FormatFecHora(FechaTerminado, True, True) & "' WHERE idOrdenSer = " & idOrdenSer

        Return StrSQL
    End Function

    Public Function DiagCabOrdenServicio(ByVal FechaTerminado As DateTime, ByVal idOrdenSer As Integer) As String

        StrSQL = "UPDATE dbo.CabOrdenServicio SET FechaDiagnostico = '" & FormatFecHora(FechaTerminado, True, True) &
        "', Estatus = 'DIAG' WHERE idOrdenSer = " & idOrdenSer

        Return StrSQL
    End Function

    Public Function TerCabOrdenServicio(ByVal FechaTerminado As DateTime, ByVal idOrdenSer As Integer) As String

        StrSQL = "UPDATE dbo.CabOrdenServicio SET Estatus = 'TER', FechaTerminado = '" & FormatFecHora(FechaTerminado, True, True) & "' WHERE idOrdenSer = " & idOrdenSer

        Return StrSQL
    End Function


    Public Function DiagTerDetOrdenServicio(ByVal FechaTerminado As DateTime, ByVal idOrdenSer As Integer,
    ByVal idOrdenTrabajo As Integer) As String

        StrSQL = "UPDATE dbo.DetOrdenServicio SET FechaDiag = '" & FormatFecHora(FechaTerminado, True, True) &
        "', FechaTerminado = '" & FormatFecHora(FechaTerminado, True, True) &
        "', Estatus = 'TER' WHERE idOrdenSer = " & idOrdenSer & " AND idOrdenTrabajo = " & idOrdenTrabajo

        Return StrSQL
    End Function

    Public Function DiagDetOrdenServicio(ByVal FechaTerminado As DateTime, ByVal idOrdenSer As Integer,
    ByVal idOrdenTrabajo As Integer, ByVal DuracionActHr As Decimal, ByVal NotaDiagnostico As String,
    ByVal CostoManoObra As Decimal, ByVal CostoProductos As Decimal, ByVal CIDPRODUCTO_SERV As Integer,
    ByVal Cantidad_CIDPRODUCTO_SERV As Decimal, ByVal Precio_CIDPRODUCTO_SERV As Decimal) As String

        StrSQL = "UPDATE dbo.DetOrdenServicio SET FechaDiag = '" & FormatFecHora(FechaTerminado, True, True) &
        "', Estatus = 'DIAG', DuracionActHr = " & DuracionActHr & ", NotaDiagnostico = '" & NotaDiagnostico &
        "', CostoManoObra = " & CostoManoObra & ", CostoProductos = " & CostoProductos &
        ", CIDPRODUCTO_SERV = " & CIDPRODUCTO_SERV & ", Cantidad_CIDPRODUCTO_SERV = " & Cantidad_CIDPRODUCTO_SERV & ", " &
        " Precio_CIDPRODUCTO_SERV = " & Precio_CIDPRODUCTO_SERV &
        " WHERE idOrdenSer = " & idOrdenSer & " AND idOrdenTrabajo = " & idOrdenTrabajo




        Return StrSQL
    End Function

    Public Function TerDetOrdenServicio(ByVal FechaTerminado As DateTime, ByVal idOrdenSer As Integer,
    ByVal idOrdenTrabajo As Integer) As String

        StrSQL = "UPDATE dbo.DetOrdenServicio SET FechaTerminado = '" & FormatFecHora(FechaTerminado, True, True) &
        "', Estatus = 'TER' WHERE idOrdenSer = " & idOrdenSer & " AND idOrdenTrabajo = " & idOrdenTrabajo

        Return StrSQL
    End Function

    Public Function EntregaCabOrdenServicio(ByVal idOrdenSer As Integer, ByVal FechaEntregado As DateTime,
    ByVal idEmpleadoRecibe As Integer, ByVal UsuarioEntrega As String)

        StrSQL = "UPDATE dbo.CabOrdenServicio SET FechaEntregado = '" & FormatFecHora(FechaEntregado, True, True) &
        "', NotaFinal = '',idEmpleadoRecibe = " & idEmpleadoRecibe & ", Estatus = 'ENT', UsuarioEntrega = '" & UsuarioEntrega &
        "' WHERE idOrdenSer = " & idOrdenSer

        Return StrSQL
    End Function

    Public Function DeterminaPreSalidas(ByVal idOrdenSer As Integer)
        StrSQL = "SELECT PRE.IdPreSalida FROM dbo.CabPreSalida PRE " &
        "INNER JOIN dbo.DetOrdenServicio DOS ON DOS.idOrdenTrabajo = PRE.idOrdenTrabajo " &
        "WHERE DOS.idOrdenSer = " & idOrdenSer

        Return StrSQL


    End Function

    Public Function ActualizaCabPreSalida(ByVal IdPreSalida As Integer, ByVal UserEntrega As String,
    ByVal IdPersonalRec As Integer, ByVal EntregaCambio As Boolean, ByVal idPreOC As Integer,
    ByVal Estatus As String)
        StrSQL = "UPDATE dbo.CabPreSalida SET ESTATUS = '" & Estatus & "', FecEntrega = GETDATE(), UserEntrega = '" &
        UserEntrega & "', " & "IdPersonalRec = " & IdPersonalRec &
        ", EntregaCambio = " & IIf(EntregaCambio, 1, 0) & ", idPreOC = " & idPreOC &
        " WHERE IdPreSalida = " & IdPreSalida

        Return StrSQL

    End Function

    Public Function ActualizaDetPreSalida(ByVal IdPreSalida As Integer, ByVal idProducto As Integer,
    ByVal Cantidad As Decimal)

        StrSQL = "UPDATE dbo.DetPreSalida SET CantidadEnt = " & Cantidad & " WHERE IdPreSalida = 
        " & IdPreSalida & " AND idProducto = " & idProducto

        Return StrSQL

    End Function


    Public Function ActualizaEstatusUbicacion(ByVal idUnidadTrans As String, ByVal IdEstatus As Integer, ByVal CampoEstatus As String)
        StrSQL = "UPDATE dbo.CatUnidadTrans SET " & CampoEstatus & " = " & IdEstatus & " WHERE idUnidadTrans = '" & idUnidadTrans & "'"

        Return StrSQL
    End Function

    Public Function InsertaKardexEntSalUnid(ByVal idUnidadTrans As String,
    ByVal tipoMovimiento As Integer, ByVal lugar As String, ByVal descripcion As String,
    ByVal folio As Integer)
        StrSQL = "INSERT INTO dbo.kardexEntSalUnid " &
        "        ( idUnidadTrans ,fecha ,tipoMovimiento ,lugar ,descripcion ,folio) " &
        "VALUES  ( '" & idUnidadTrans & "', getdate()," & tipoMovimiento &
        ",'" & lugar & "','" & descripcion & "'," & folio & ")"

        Return StrSQL
    End Function

    Public Function VerificaFaltantes(ByVal idUnidadTrans As String)
        StrSQL = "SELECT * FROM dbo.DetOrdenServicio d " &
        "INNER JOIN dbo.CabOrdenServicio c ON c.idOrdenSer = d.idOrdenSer " &
        "WHERE FaltInsumo = 1  " &
        "AND c.idUnidadTrans IN ('" & idUnidadTrans & "')"


        Return StrSQL

    End Function





End Class