﻿Option Strict Off
Option Explicit On
Imports System.Runtime.InteropServices
Imports System.Text

Module SDK_Comercial

    Public lLicencia As Byte

#Region "CONSTANTES"
    Public Const kLongFecha As Integer = 24
    Public Const kLongSerie As Integer = 12
    Public Const kLongCodigo As Integer = 31
    Public Const kLongNombre As Integer = 61
    Public Const kLongReferencia As Integer = 21
    Public Const kLongDescripcion As Integer = 61
    Public Const kLongCuenta As Integer = 101
    Public Const kLongMensaje As Integer = 3001
    Public Const kLongNombreProducto As Integer = 256
    Public Const kLongAbreviatura As Integer = 4
    Public Const kLongCodValorClasif As Integer = 4
    Public Const kLongDenComercial As Integer = 51
    Public Const kLongRepLegal As Integer = 51
    Public Const kLongTextoExtra As Integer = 51
    Public Const kLongRFC As Integer = 21
    Public Const kLongCURP As Integer = 21
    Public Const kLongDesCorta As Integer = 21
    Public Const kLongNumeroExtInt As Integer = 7
    Public Const kLongNumeroExpandido As Integer = 31
    Public Const kLongCodigoPostal As Integer = 7
    Public Const kLongTelefono As Integer = 16
    Public Const kLongEmailWeb As Integer = 51

    Public Const kLongSelloSat As Integer = 176
    Public Const kLonSerieCertSAT As Integer = 21
    Public Const kLongFechaHora As Integer = 36
    Public Const kLongSelloCFDI As Integer = 176
    Public Const kLongCadOrigComplSAT As Integer = 501
    Public Const kLongitudUUID As Integer = 37
    Public Const kLongitudRegimen As Integer = 101
    Public Const kLongitudMoneda As Integer = 61
    Public Const kLongitudFolio As Integer = 17
    Public Const kLongitudMonto As Integer = 31
    Public Const kLogitudLugarExpedicion As Integer = 401

    Public Const kDocumento_IdDocumento As String = "cIdDocumento"
    Public Const kDocumento_CodigoConcepto As String = "cCodigoConcepto"
    Public Const kDocumento_Serie As String = "cSerieDocumento"
    Public Const kDocumento_Folio As String = "cFolio"
    Public Const kDocumento_Fecha As String = "cFecha"
    Public Const kDocumento_CodigoCteProv As String = "cCodigoCteProv"
    'Public Const kDocumento_CodigoCteProv As String = "CIDCLIENTEPROVEEDOR"

    Public Const kDocumento_RazonSocial As String = "cRazonSocial"
    Public Const kDocumento_RFC As String = "cRFC"
    Public Const kDocumento_SerieOmision As String = "cSeriePorOmision"
    Public Const kDocumento_CodigoAgente As String = "cCodigoAgente"
    Public Const kDocumento_FechaVencimiento As String = "cFechaVencimiento"
    Public Const kDocumento_FechaEntRecep As String = "cFechaEntregaRecepcion"
    Public Const kDocumento_FechaProntoPago As String = "cFechaProntoPago"
    Public Const kDocumento_FechaUltimoInteres As String = "cFechaUltimoInteres"
    'Public Const kDocumento_IdMoneda As String = "cidMoneda"
    Public Const kDocumento_IdMoneda As String = "CIDMONEDA"

    'Public Const kDocumento_TipoCambio As String = "cTipoCambio"
    Public Const kDocumento_TipoCambio As String = "CTIPOCAMBIO"

    Public Const kDocumento_Referencia As String = "cReferencia"
    Public Const kDocumento_Importe As String = "cImporte"
    Public Const kDocumento_Descuento1 As String = "cDescuentoDoc1"
    Public Const kDocumento_Descuento2 As String = "cDescuentoDoc2"
    Public Const kDocumento_DescProntoPago As String = "cDescuentoProntoPago"
    Public Const kDocumento_InteresMoratorio As String = "cPorcentajeInteres"
    Public Const kDocumento_SisOrigen As String = "cSistOrig"
    Public Const kDocumento_Observaciones As String = "cObservaciones"
    Public Const kDocumento_ConDireccionFiscal As String = "cConDireccionFiscal"
    Public Const kDocumento_ConDireccionEnvio As String = "cConDireccionEnvio"
    Public Const kDocumento_Gasto1 As String = "cGasto1"
    Public Const kDocumento_Gasto2 As String = "cGasto2"
    Public Const kDocumento_Gasto3 As String = "cGasto3"


    Public Const kMovto_CodProducto As String = "cCodigoProducto"
    Public Const kMovto_CodAlmacen As String = "cCodigoAlmacen"
    Public Const kMovto_Precio As String = "cPrecio"
    Public Const kMovto_Unidades As String = "cUnidades"
    Public Const kMovto_ObservaMov As String = "cObservaMov"


#End Region

#Region "ESTRUCTURAS"
    ' Eestructura de documentos
    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi, Pack:=4)> _
    Public Structure tDocumento

        Public aFolio As [Double]
        Public aNumMoneda As Integer
        Public aTipoCambio As [Double]
        Public aImporte As [Double]
        Public aDescuentoDoc1 As [Double]
        Public aDescuentoDoc2 As [Double]
        Public aSistemaOrigen As Integer
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongCodigo)> _
        Public aCodConcepto As [String]
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongSerie)> _
        Public aSerie As [String]
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongFecha)> _
        Public aFecha As [String]
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongCodigo)> _
        Public aCodigoCteProv As [String]
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongCodigo)> _
        Public aCodigoAgente As [String]
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongReferencia)> _
        Public aReferencia As [String]
        Public aAfecta As Integer
        Public aGasto1 As Integer
        Public aGasto2 As Integer
        Public aGasto3 As Integer

    End Structure

    ' Eestructura de movimiento
    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi, Pack:=4)> _
    Public Structure tMovimiento
        Public aConsecutivo As Integer
        Public aUnidades As [Double]
        Public aPrecio As [Double]
        Public aCosto As [Double]
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongCodigo)> _
        Public aCodProdSer As [String]
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongCodigo)> _
        Public aCodAlmacen As [String]
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongReferencia)> _
        Public aReferencia As [String]
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongCodigo)> _
        Public aCodClasificacion As [String]


        ''COBSERVAMOV
        ' [MarshalAs(UnmanagedType.ByValTStr, SizeConst = constantes.kLongReferencia)]
        '    public String aReferencia;
    End Structure

    ' Estructura de cliente Provedor
    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi, Pack:=4)> _
    Public Structure tCteProv
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongCodigo)> _
        Public cCodigoCliente As [String]
        '[ kLongCodigo + 1 ];
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongNombre)> _
        Public cRazonSocial As [String]
        '[ kLongNombre + 1 ];
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongFecha)> _
        Public cFechaAlta As [String]
        '[ kLongFecha + 1 ];
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongRFC)> _
        Public cRFC As [String]
        '[ kLongRFC + 1 ];
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongCURP)> _
        Public cCURP As [String]
        '[ kLongCURP + 1 ];
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongDenComercial)> _
        Public cDenComercial As [String]
        '[ kLongDenComercial + 1 ];
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongRepLegal)> _
        Public cRepLegal As [String]
        '[ kLongRepLegal + 1 ];
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongNombre)> _
        Public cNombreMoneda As [String]
        '[ kLongNombre + 1 ];
        Public cListaPreciosCliente As Integer
        Public cDescuentoMovto As Double
        Public cBanVentaCredito As Integer
        ' 0 = No se permite venta a crédito, 1 = Se permite venta a crédito
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongCodValorClasif)> _
        Public cCodigoValorClasificacionCliente1 As [String]
        '[ kLongCodValorClasif + 1 ];
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongCodValorClasif)> _
        Public cCodigoValorClasificacionCliente2 As [String]
        '[ kLongCodValorClasif + 1 ];
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongCodValorClasif)> _
        Public cCodigoValorClasificacionCliente3 As [String]
        '[ kLongCodValorClasif + 1 ];
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongCodValorClasif)> _
        Public cCodigoValorClasificacionCliente4 As [String]
        '[ kLongCodValorClasif + 1 ];
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongCodValorClasif)> _
        Public cCodigoValorClasificacionCliente5 As [String]
        '[ kLongCodValorClasif + 1 ];
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongCodValorClasif)> _
        Public cCodigoValorClasificacionCliente6 As [String]
        '[ kLongCodValorClasif + 1 ];
        Public cTipoCliente As Integer
        ' 1 - Cliente, 2 - Cliente/Proveedor, 3 - Proveedor
        Public cEstatus As Integer
        ' 0. Inactivo, 1. Activo
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongFecha)> _
        Public cFechaBaja As [String]
        '[ kLongFecha + 1 ];
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongFecha)> _
        Public cFechaUltimaRevision As [String]
        '[ kLongFecha + 1 ];
        Public cLimiteCreditoCliente As Double
        Public cDiasCreditoCliente As Integer
        Public cBanExcederCredito As Integer
        ' 0 = No se permite exceder crédito, 1 = Se permite exceder el crédito
        Public cDescuentoProntoPago As Double
        Public cDiasProntoPago As Integer
        Private cInteresMoratorio As Double
        Public cDiaPago As Integer
        Public cDiasRevision As Integer
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongDesCorta)> _
        Public cMensajeria As [String]
        '[ kLongDesCorta + 1 ];
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongDescripcion)> _
        Public cCuentaMensajeria As [String]
        '[ kLongDescripcion + 1 ];
        Public cDiasEmbarqueCliente As Integer
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongCodigo)> _
        Public cCodigoAlmacen As [String]
        '[ kLongCodigo + 1 ];
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongCodigo)> _
        Public cCodigoAgenteVenta As [String]
        '[ kLongCodigo + 1 ];
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongCodigo)> _
        Public cCodigoAgenteCobro As [String]
        '[ kLongCodigo + 1 ];
        Public cRestriccionAgente As Integer
        Public cImpuesto1 As Double
        Public cImpuesto2 As Double
        Public cImpuesto3 As Double
        Public cRetencionCliente1 As Double
        Public cRetencionCliente2 As Double
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongCodValorClasif)> _
        Public cCodigoValorClasificacionProveedor1 As [String]
        '[ kLongCodValorClasif + 1 ];
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongCodValorClasif)> _
        Public cCodigoValorClasificacionProveedor2 As [String]
        '[ kLongCodValorClasif + 1 ];
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongCodValorClasif)> _
        Public cCodigoValorClasificacionProveedor3 As [String]
        '[ kLongCodValorClasif + 1 ];
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongCodValorClasif)> _
        Public cCodigoValorClasificacionProveedor4 As [String]
        '[ kLongCodValorClasif + 1 ];
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongCodValorClasif)> _
        Public cCodigoValorClasificacionProveedor5 As [String]
        '[ kLongCodValorClasif + 1 ];
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongCodValorClasif)> _
        Public cCodigoValorClasificacionProveedor6 As [String]
        '[ kLongCodValorClasif + 1 ];
        Public cLimiteCreditoProveedor As Double
        Public cDiasCreditoProveedor As Integer
        Public cTiempoEntrega As Integer
        Public cDiasEmbarqueProveedor As Integer
        Public cImpuestoProveedor1 As Double
        Public cImpuestoProveedor2 As Double
        Public cImpuestoProveedor3 As Double
        Public cRetencionProveedor1 As Double
        Public cRetencionProveedor2 As Double
        Public cBanInteresMoratorio As Integer
        ' 0 = No se le calculan intereses moratorios al cliente, 1 = Si se le calculan intereses moratorios al cliente.
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongTextoExtra)> _
        Public cTextoExtra1 As [String]
        '[ kLongTextoExtra + 1 ];
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongTextoExtra)> _
        Public cTextoExtra2 As [String]
        '[ kLongTextoExtra + 1 ];
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongTextoExtra)> _
        Public cTextoExtra3 As [String]
        '[ kLongTextoExtra + 1 ];
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongTextoExtra)> _
        Public cFechaExtra As [String]
        '[ kLongFecha + 1 ];
        Public cImporteExtra1 As Double
        Public cImporteExtra2 As Double
        Public cImporteExtra3 As Double
        Public cImporteExtra4 As Double

    End Structure

    'Estrutura de productos
    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi, Pack:=4)> _
    Public Structure tProduto
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongCodigo)> _
        Public cCodigoProducto As String

        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongNombre)> _
        Public cNombreProducto As String

        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongNombreProducto)> _
        Public cDescripcionProducto As String

        Public cTipoProducto As Integer
        ' 1 = Producto, 2 = Paquete, 3 = Servicio

        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongFecha)> _
        Public cFechaAltaProducto As String

        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongFecha)> _
        Public cFechaBaja As String

        Public cStatusProducto As Integer
        ' 0 - Baja Lógica, 1 - Alta

        Public cControlExistencia As Integer

        Public cMetodoCosteo As Integer
        ' 1 = Costo Promedio en Base a Entradas, 2 = Costo Promedio en Base a Entradas Almacen, 3 = Último costo, 4 = UEPS, 5 = PEPS, 6 = Costo específico, 7 = Costo Estandar

        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongCodigo)> _
        Public cCodigoUnidadBase As String

        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongCodigo)> _
        Public cCodigoUnidadNoConvertible As String

        Public cPrecio1 As Double
        Public cPrecio2 As Double
        Public cPrecio3 As Double
        Public cPrecio4 As Double
        Public cPrecio5 As Double
        Public cPrecio6 As Double
        Public cPrecio7 As Double
        Public cPrecio8 As Double
        Public cPrecio9 As Double
        Public cPrecio10 As Double
        Public cImpuesto1 As Double
        Public cImpuesto2 As Double
        Public cImpuesto3 As Double
        Public cRetencion1 As Double
        Public cRetencion2 As Double

        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongNombre)> _
        Public cNombreCaracteristica1 As String

        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongNombre)> _
        Public cNombreCaracteristica2 As String

        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongNombre)> _
        Public cNombreCaracteristica3 As String

        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongCodValorClasif)> _
        Public cCodigoValorClasificacion1 As String

        '[ kLongCodValorClasif + 1 ];
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongCodValorClasif)> _
        Public cCodigoValorClasificacion2 As String

        '[ kLongCodValorClasif + 1 ];
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongCodValorClasif)> _
        Public cCodigoValorClasificacion3 As String

        '[ kLongCodValorClasif + 1 ];
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongCodValorClasif)> _
        Public cCodigoValorClasificacion4 As String

        '[ kLongCodValorClasif + 1 ];
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongCodValorClasif)> _
        Public cCodigoValorClasificacion5 As String

        '[ kLongCodValorClasif + 1 ];
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongCodValorClasif)> _
        Public cCodigoValorClasificacion6 As String

        '[ kLongCodValorClasif + 1 ];
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongTextoExtra)> _
        Public cTextoExtra1 As String

        '[ kLongTextoExtra + 1 ];
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongTextoExtra)> _
        Public cTextoExtra2 As String

        '[ kLongTextoExtra + 1 ];
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongTextoExtra)> _
        Public cTextoExtra3 As String

        '[ kLongTextoExtra + 1 ];
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongFecha)> _
        Public cFechaExtra As String

        '[ kLongFecha + 1 ];
        Public cImporteExtra1 As Double
        Public cImporteExtra2 As Double
        Public cImporteExtra3 As Double
        Public cImporteExtra4 As Double
    End Structure

    ' Estructura de Valor de Clasificacion
   
    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi, Pack:=4)> _
    Public Structure tValorClasificacion
        'Public cClasificacionDe As Integer
        Public cNumClasificacion As Integer
        '<MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongCodValorClasif)> _
        'Public cCodigoValorClasificacion As [String]
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=kLongDescripcion)> _
        Public cValorClasificacion As [String]
    End Structure
#End Region

#Region "METODOS DE WINDOWS"

    Public Declare Function SetCurrentDirectory Lib "KERNEL32" (ByVal pPtrDirActual As String) As Integer

#End Region

#Region "METODOS DE CONEXION"
    Public Declare Sub fTerminaSDK Lib "MGWServicios.DLL" ()

    Public Declare Function fSetNombrePAQ Lib "MGWServicios.DLL" (ByVal aNombrePAQ As String) As Integer

    Public Declare Function fAbreEmpresa Lib "MGWServicios.DLL" (ByVal Directorio As String) As Integer

    Public Declare Sub fCierraEmpresa Lib "MGWServicios.DLL" ()
#End Region

#Region "EMPRESAS"
    Declare Function fPosPrimerEmpresa Lib "MGWServicios.DLL" (ByRef aIdEmpresa As Integer, ByVal aNombreEmpresa As String, ByVal aDirectorioEmpresa As String) As Integer
    Declare Function fPosSiguienteEmpresa Lib "MGWServicios.DLL" (ByRef aIdEmpresa As Integer, ByVal aNombreEmpresa As String, ByVal aDirectorioEmpresa As String) As Integer
#End Region

#Region "METODOS DE ERRORES"

    Public Declare Function fError Lib "MGWServicios.DLL" (ByVal NumeroError As Integer, ByVal Mensaje As StringBuilder, ByVal Longitud As Integer) As Integer

    Public Function rError(iError As Integer) As String
        Dim sMensaje As New StringBuilder(512)

        If iError <> 0 Then

            fError(iError, sMensaje, 512)
        End If
        Return sMensaje.ToString()
    End Function
#End Region

#Region "METODOS DE PRODUCTOS"
    Public Declare Function fAltaProducto Lib "MGWServicios.DLL" (ByRef aIdProducto As Integer, ByRef astProducto As tProduto) As Integer

    Public Declare Function fBuscaProducto Lib "MGWServicios.DLL" (ByVal aCodProducto As String) As Integer

    Public Declare Function fEditaProducto Lib "MGWServicios.DLL" () As Integer

    Public Declare Function fSetDatoProducto Lib "MGWServicios.DLL" (ByVal aCampo As String, ByVal aValor As String) As Integer

    Public Declare Function fGuardaProducto Lib "MGWServicios.DLL" () As Integer

    Public Declare Function fEliminarProducto Lib "MGWServicios.DLL" (ByVal aCodigoProducto As String) As Integer
#End Region

#Region "METODOS DE DOCUMENTOS"
    Public Declare Function fAltaDocumento Lib "MGWServicios.DLL" (ByRef aIdDocumento As Int32, ByRef atDocumento As tDocumento) As Int32


    'Public Declare Function fSiguienteFolio Lib "MGWServicios.DLL" (<MarshalAs(UnmanagedType.LPStr, SizeConst:=kLongCodigo)> ByVal aCodigoConcepto As String,
    '                                                                <MarshalAs(UnmanagedType.LPStr, SizeConst:=kLongSerie)> ByVal aSerie As StringBuilder,
    '                                                                ByRef aFolio As Double) As Int32
    Declare Function fSiguienteFolio Lib "MGWServicios.DLL" (ByVal aCodigoConcepto As String, ByRef aSerie As String, ByRef aFolio As Double) As Integer
#End Region

#Region "METODOS DE MOVIMIENTOS"
    Public Declare Function fAltaMovimiento Lib "MGWServicios.DLL" (ByVal aIdDocumento As Int32, ByRef aIdMovimiento As Int32, ByRef atMovimiento As tMovimiento) As Int32

    Public Declare Function fBuscarDocumento Lib "MGWServicios.DLL" (ByVal aCodConcepto As String, ByVal aSerie As String, ByVal aFolio As String) As Integer

    Public Declare Function fEditarDocumento Lib "MGWServicios.DLL" () As Integer

    'Public Declare Function fSetDatoDocumento Lib "MGWServicios.DLL" (ByVal aCampo As String, ByVal aValor As String) As Integer
    Declare Function fSetDatoDocumento Lib "MGWServicios.DLL" (ByVal aCampo As String, ByVal aValor As String) As Integer

    'Public Declare Function fLeeDatoDocumento Lib "MGWServicios.DLL" (ByVal aCampo As String, ByVal aValor As StringBuilder, ByVal aLen As Integer) As Integer
    Public Declare Function fLeeDatoDocumento Lib "MGWServicios.DLL" (ByVal aCampo As String, ByVal aValor As String, ByVal aLen As Integer) As Integer


    Public Declare Function fGuardaDocumento Lib "MGWServicios.DLL" () As Integer

    Public Declare Function fBuscarIdMovimiento Lib "MGWServicios.DLL" (ByVal aIdMovimiento As Int32) As Integer

    Public Declare Function fEditarMovimiento Lib "MGWServicios.DLL" () As Integer

    Public Declare Function fInsertarMovimiento Lib "MGWServicios.DLL" () As Integer

    Public Declare Function fGuardaMovimiento Lib "MGWServicios.DLL" () As Integer

    Public Declare Function fBorraDocumento Lib "MGWServicios.DLL" () As Integer



    Public Declare Function fSetDatoMovimiento Lib "MGWServicios.DLL" (ByVal aCampo As String, ByVal aValor As String) As Integer

    Public Declare Function fLeeDatoMovimiento Lib "MGWServicios.DLL" (ByVal aCampo As String, ByVal aValor As StringBuilder, ByVal aLen As Integer) As Integer

    Declare Function fInsertarDocumento Lib "MGWServicios.DLL" () As Integer

    Public Declare Function fAfectaDocto_Param Lib "MGWServicios.DLL" (<MarshalAs(UnmanagedType.LPStr)> ByVal aCodConcepto As String, <MarshalAs(UnmanagedType.LPStr)> ByVal aSerie As String, ByVal aFolio As Double, ByVal aAfecta As Boolean) As Integer

    Public Declare Function fEmitirDocumento Lib "MGWServicios.DLL" (<MarshalAs(UnmanagedType.LPStr)> ByVal aCodigoConcepto As String, <MarshalAs(UnmanagedType.LPStr)> ByVal aSerie As String, ByVal aFolio As Double, <MarshalAs(UnmanagedType.LPStr)> ByVal aPassword As String, <MarshalAs(UnmanagedType.LPStr)> ByVal aArchivoAdicional As String) As Integer

    Public Declare Function fDocumentoUUID Lib "MGWServicios.DLL" (ByVal aCodigoConcepto As String, ByVal aNumSerie As String, ByVal aFolio As Double, ByVal aUUID As String) As Integer

    Public Declare Function fEntregEnDiscoXML Lib "MGWServicios.DLL" (<MarshalAs(UnmanagedType.LPStr)> ByVal aCodigoConcepto As String, <MarshalAs(UnmanagedType.LPStr)> ByVal aSerie As String, ByVal aFolio As Double, ByVal aFormato As Integer, <MarshalAs(UnmanagedType.LPStr)> ByVal aFormatoAmig As String) As Integer

    Public Declare Function fInicializaLicenseInfo Lib "MGWServicios.DLL" (ByVal lSistema As Byte) As Integer

#End Region

#Region "METODOS DE CLIENTES"
    Public Declare Function fAltaCteProv Lib "MGWServicios.DLL" (ByRef aIdCliente As Integer, ByRef astCliente As tCteProv) As Integer

    Public Declare Function fBuscaCteProv Lib "MGWServicios.DLL" (ByVal aCodCteProv As String) As Integer

    Public Declare Function fLeeDatoCteProv Lib "MGWServicios.DLL" (ByVal aCampo As String, ByVal aValr As StringBuilder, ByVal aLen As Integer) As Integer

    Public Declare Function fEditaCteProv Lib "MGWServicios.DLL" () As Integer

    Public Declare Function fSetDatoCteProv Lib "MGWServicios.DLL" (ByVal aCampo As String, ByVal aValor As String) As Integer

    Public Declare Function fGuardaCteProv Lib "MGWServicios.DLL" () As Integer

    Public Declare Function fBorraCteProv Lib "MGWServicios.DLL" (ByVal aCodCteProv As String) As Integer

#End Region

#Region "CLASIFICACIONES"

    Public Declare Function fActualizaClasificacion Lib "MGWServicios.DLL" (ByVal aClasificacionDe As Integer, ByVal aNumClasificacion As Integer, ByVal aNombreClasificacion As String) As Integer

    Public Declare Function fLlenaRegistroValorClasif Lib "MGWServicios.DLL" (ByVal astValorClasif As tValorClasificacion) As Integer

    Public Declare Function fAltaValorClasif Lib "MGWServicios.DLL" (ByRef aIdValorClasif As Integer, ByVal astValorClasif As tValorClasificacion) As Integer
    '

    'BAJO NIVEL

    Public Declare Function fBuscaValorClasif Lib "MGWServicios.DLL" (ByVal aClasificacionDe As Integer, ByVal aNumClasificacion As Integer, ByVal aCodValorClasif As String) As Integer

    Public Declare Function fBuscaIdValorClasif Lib "MGWServicios.DLL" (ByVal aIdValorClasif As Integer) As Integer
    Public Declare Function fInsertaValorClasif Lib "MGWServicios.DLL" () As Integer
    Public Declare Function fSetDatoValorClasif Lib "MGWServicios.DLL" (ByVal aCampo As String, ByVal aValor As String) As Integer
    Public Declare Function fGuardaValorClasif Lib "MGWServicios.DLL" () As Integer

    Public Declare Function fLeeDatoValorClasif Lib "MGWServicios.DLL" (ByVal aCampo As String, ByRef aValor As String, ByVal aLen As Integer) As Integer

#End Region

#Region "AGENTES"
    Public Declare Function fBuscaAgente Lib "MGWServicios.DLL" (ByVal aCodigoAgente As String) As Integer

    Public Declare Function fInsertaAgente Lib "MGWServicios.DLL" () As Integer

    Public Declare Function fGuardaAgente Lib "MGWServicios.DLL" () As Integer

    Public Declare Function fSetDatoAgente Lib "MGWServicios.DLL" (ByVal aCampo As String, ByVal aValor As String) As Integer
#End Region


End Module

