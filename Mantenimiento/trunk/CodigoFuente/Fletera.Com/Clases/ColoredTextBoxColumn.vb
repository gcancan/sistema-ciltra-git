Public Class ColoredTextBoxColumn
    'Public Class ColoredTextBoxColumn
    Inherits DataGridTextBoxColumn

    Protected Overloads Overrides Sub Paint(ByVal graph As Graphics, ByVal rectbounds As Rectangle, _
    ByVal curmngrSrc As CurrencyManager, ByVal RowNumber As Integer, ByVal ForecolorBrush As Brush, _
    ByVal backcolorBrush As Brush, ByVal AlignmentRight As Boolean)
        Dim drv As DataView = CType(curmngrSrc.List, DataView)
        'backcolorBrush = Brushes.LightBlue

        backcolorBrush = Brushes.SteelBlue
        ForecolorBrush = Brushes.Yellow

        MyBase.Paint(graph, rectbounds, curmngrSrc, RowNumber, backcolorBrush, ForecolorBrush, AlignmentRight)
    End Sub
    'End Class
End Class
