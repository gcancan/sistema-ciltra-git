﻿Public Class OrdenTrabajoClass
    'idOrdenTrabajo	    vidOrdenTrabajo	    _idOrdenTrabajo
    'idOrdenSer	        vidOrdenSer	        _idOrdenSer
    'idFamilia	        vidFamilia	        _idFamilia
    'NotaRecepcion	    vNotaRecepcion	    _NotaRecepcion
    'isCancelado	    visCancelado	    _isCancelado
    'motivoCancelacion	vmotivoCancelacion	_motivoCancelacion
    'idDivision	        vidDivision	        _idDivision
    'idPersonalResp	    vidPersonalResp	    _idPersonalResp
    'idPersonalAyu1	    vidPersonalAyu1	    _idPersonalAyu1
    'idPersonalAyu2	    vidPersonalAyu2	    _idPersonalAyu2

    Private _idOrdenTrabajo As Integer
    Private _idOrdenSer As Integer
    Private _idFamilia As Integer
    Private _NotaRecepcion As String
    Private _isCancelado As Boolean
    Private _motivoCancelacion As String
    Private _idDivision As String
    Private _idPersonalResp As Integer
    Private _idPersonalAyu1 As Integer
    Private _idPersonalAyu2 As Integer

    Private _Existe As Boolean
    Private _TablaBd As String = "DetRecepcionOS"

    Dim StrSql As String
    Dim DsClase As New DataTable
    Dim MyTabla As DataTable = New DataTable(_TablaBd)

#Region "Constructor"
    Sub New(ByVal vidOrdenTrabajo As String)
        _idOrdenTrabajo = vidOrdenTrabajo

        StrSql = "select OT.idOrdenTrabajo, " & _
        "OT.idOrdenSer, " & _
        "isnull(OT.idFamilia,0) as idFamilia, " & _
        "isnull(OT.NotaRecepcion,'') as NotaRecepcion, " & _
        "isnull(OT.idDivision,0) as idDivision, " & _
        "isnull(OT.idPersonalResp,0) as idPersonalResp, " & _
        "isnull(OT.idPersonalAyu1,0) as idPersonalAyu1, " & _
        "isnull(OT.idPersonalAyu2,0) as idPersonalAyu2, " & _
        "isnull(OT.isCancelado,'') as isCancelado, " & _
        "isnull(OT.motivoCancelacion,'') as motivoCancelacion " & _
        "from " & _TablaBd & " OT " & _
        "where OT.idOrdenTrabajo = " & vidOrdenTrabajo

        DsClase = BD.ExecuteReturn(StrSql)
        If DsClase.Rows.Count > 0 Then
            With DsClase.Rows(0)
                _idOrdenSer = .Item("idOrdenSer")
                _idFamilia = .Item("idFamilia") & ""
                _NotaRecepcion = .Item("NotaRecepcion")
                _isCancelado = .Item("isCancelado")
                _motivoCancelacion = .Item("motivoCancelacion")
                _idDivision = .Item("idDivision")
                _idPersonalResp = .Item("idPersonalResp")
                _idPersonalAyu1 = .Item("idPersonalAyu1")
                _idPersonalAyu2 = .Item("idPersonalAyu2")
            End With
            _Existe = True
        Else
            _Existe = False
        End If

    End Sub
    Sub New(ByVal vidOrdenTrabajo As Integer, ByVal vidOrdenSer As Integer, ByVal vidFamilia As Integer,
            ByVal vNotaRecepcion As String, _
            ByVal visCancelado As Boolean, ByVal vmotivoCancelacion As String, ByVal vidDivision As Integer, _
            ByVal vidPersonalResp As Integer, ByVal vidPersonalAyu1 As Integer, ByVal vidPersonalAyu2 As Integer)
        _idOrdenTrabajo = vidOrdenTrabajo
        _idOrdenSer = vidOrdenSer
        _idFamilia = vidFamilia
        _NotaRecepcion = vNotaRecepcion
        _isCancelado = visCancelado
        _motivoCancelacion = vmotivoCancelacion
        _idDivision = vidDivision
        _idPersonalResp = vidPersonalResp
        _idPersonalAyu1 = vidPersonalAyu1
        _idPersonalAyu2 = vidPersonalAyu2
        _Existe = True
    End Sub
#End Region

#Region "Propiedades"
    Public Property idOrdenTrabajo As Integer
        Get
            Return _idOrdenTrabajo
        End Get
        Set(ByVal value As Integer)
            _idOrdenTrabajo = value
        End Set
    End Property
    Public Property idOrdenSer As Integer
        Get
            Return _idOrdenSer
        End Get
        Set(ByVal value As Integer)
            _idOrdenSer = value
        End Set
    End Property
    Public Property idFamilia As Integer
        Get
            Return _idFamilia
        End Get
        Set(ByVal value As Integer)
            _idFamilia = value
        End Set
    End Property
    Public Property NotaRecepcion As String
        Get
            Return _NotaRecepcion
        End Get
        Set(ByVal value As String)
            _NotaRecepcion = value
        End Set
    End Property
    Public Property isCancelado As Boolean
        Get
            Return _isCancelado
        End Get
        Set(ByVal value As Boolean)
            _isCancelado = value
        End Set
    End Property
    Public Property motivoCancelacion As String
        Get
            Return _motivoCancelacion
        End Get
        Set(ByVal value As String)
            _motivoCancelacion = value
        End Set
    End Property
    Public Property idDivision As Integer
        Get
            Return _idDivision
        End Get
        Set(ByVal value As Integer)
            _idDivision = value
        End Set
    End Property
    Public Property idPersonalResp As Integer
        Get
            Return _idPersonalResp
        End Get
        Set(ByVal value As Integer)
            _idPersonalResp = value
        End Set
    End Property
    Public Property idPersonalAyu1 As Integer
        Get
            Return _idPersonalAyu1
        End Get
        Set(ByVal value As Integer)
            _idPersonalAyu1 = value
        End Set
    End Property
    Public Property idPersonalAyu2 As Integer
        Get
            Return _idPersonalAyu2
        End Get
        Set(ByVal value As Integer)
            _idPersonalAyu2 = value
        End Set
    End Property
    Public Property Existe As Boolean
        Get
            Return _Existe
        End Get
        Set(ByVal value As Boolean)
            _Existe = value
        End Set
    End Property
#End Region

#Region "Funciones"
    Public Function ConsultaActualizaDato(ByVal vValor As String, ByVal vNomCampo As String, ByVal vTipoDato As TipoDato) As String
        StrSql = ""
        Dim Filtro As String = ""
        If vTipoDato = TipoDato.TdCadena Then
            Filtro = "'" & vValor & "'"
        ElseIf vTipoDato = TipoDato.TdFecha Then
            Filtro = "'" & FormatFecHora(vValor, True, True) & "'"
        ElseIf vTipoDato = TipoDato.TdNumerico Then
            Filtro = vValor
        End If

        StrSql = "update " & _TablaBd & " SET " & vNomCampo & " = " & Filtro & " where idOrdenTrabajo = " & _idOrdenTrabajo

        Return StrSql
    End Function

    Public Function tbEmpleadoAsignado(ByVal idPersonal As Integer, ByVal opcion As opTipoEmpleado) As DataTable
        Dim filtro As String = ""
        MyTabla.Rows.Clear()

        Select Case opcion
            Case opTipoEmpleado.Responsable
                filtro = " WHERE dros.idPersonalResp = " & idPersonal & " AND cab.Estatus in ('ASIG','DIAG')"
            Case opTipoEmpleado.Ayudante1
                filtro = " WHERE dros.idPersonalAyu1 = " & idPersonal & " AND cab.Estatus in ('ASIG','DIAG')"
            Case opTipoEmpleado.Ayudante2
                filtro = " WHERE dros.idPersonalAyu2 = " & idPersonal & " AND cab.Estatus in ('ASIG','DIAG')"
        End Select

        StrSql = "SELECT dros.idOrdenSer,cab.Estatus, " &
        "dros.idOrdenTrabajo " &
        "FROM dbo.DetRecepcionOS dros " &
        "INNER JOIN dbo.CabOrdenServicio cab ON cab.idOrdenSer = dros.idOrdenSer " & filtro

        MyTabla = BD.ExecuteReturn(StrSql)

        Return MyTabla
    End Function


#End Region
End Class
