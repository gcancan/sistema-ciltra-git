﻿Public Class PreSalidaClass
    Private _IdPreSalida As Integer
    Private _idOrdenTrabajo As Integer
    Private _FecSolicitud As DateTime
    Private _UserSolicitud As String
    Private _NombreCompleto As String
    Private _IdEmpleadoEnc As Integer
    Private _idAlmacen As Integer
    Private _Estatus As String
    Private _SubTotal As Decimal
    Private _FecCancela As DateTime
    Private _UserCancela As String
    Private _MotivoCancela As String
    Private _FecEntrega As DateTime
    Private _UserEntrega As String
    Private _IdPersonalRec As Integer
    Private _EntregaCambio As Boolean

    Private _Existe As Boolean
    Private _TablaBd As String = "CabPreSalida"
    Dim StrSql As String
    Dim DsClase As New DataTable
    Dim MyTabla As DataTable = New DataTable(_TablaBd)

#Region "Constructor"
    Sub New(ByVal vIdPreSalida As Integer)
        _IdPreSalida = vIdPreSalida

        StrSql = "SELECT cab.IdPreSalida, " &
        "cab.idOrdenTrabajo, " &
        "cab.FecSolicitud, " &
        "cab.UserSolicitud, " &
        "cab.IdEmpleadoEnc, " &
        "per.NombreCompleto, " &
        "cab.idAlmacen, " &
        "cab.Estatus, " &
        "cab.SubTotal, " &
        "isnull(cab.FecCancela,getdate()) as FecCancela, " &
        "isnull(cab.UserCancela,'') as UserCancela, " &
        "isnull(cab.MotivoCancela,'') as MotivoCancela, " &
        "isnull(cab.FecEntrega,getdate()) as FecEntrega, " &
        "isnull(cab.UserEntrega,'') as UserEntrega, " &
        "isnull(cab.IdPersonalRec,'') as IdPersonalRec, " &
        "isnull(cab.EntregaCambio,'') as EntregaCambio, " &
        "ot.NotaRecepcion " &
        "from " & _TablaBd & " cab " &
        "left JOIN dbo.CatPersonal Per  ON cab.IdEmpleadoEnc = Per.idPersonal " &
        "INNER JOIN dbo.DetRecepcionOS OT ON OT.idOrdenTrabajo = cab.idOrdenTrabajo " &
        "where cab.IdPreSalida = " & vIdPreSalida

        DsClase = BD.ExecuteReturn(StrSql)
        If DsClase.Rows.Count > 0 Then
            With DsClase.Rows(0)
                _IdPreSalida = .Item("IdPreSalida")
                _idOrdenTrabajo = .Item("idOrdenTrabajo")
                _FecSolicitud = .Item("FecSolicitud")
                _UserSolicitud = .Item("UserSolicitud")
                _IdEmpleadoEnc = .Item("IdEmpleadoEnc")
                _NombreCompleto = .Item("NombreCompleto")
                _idAlmacen = .Item("idAlmacen")
                _Estatus = .Item("Estatus")
                _SubTotal = .Item("SubTotal")
                _FecCancela = .Item("FecCancela")
                _UserCancela = .Item("UserCancela")
                _MotivoCancela = .Item("MotivoCancela")
                _FecEntrega = .Item("FecEntrega")
                _UserEntrega = .Item("UserEntrega")
                _IdPersonalRec = .Item("IdPersonalRec")
                _EntregaCambio = .Item("EntregaCambio")
            End With
            _Existe = True
        Else
            _Existe = False
        End If

    End Sub
    Sub New(ByVal vIdPreSalida As Integer, ByVal vidOrdenTrabajo As Integer, ByVal vFecSolicitud As DateTime,
    ByVal vUserSolicitud As String, ByVal vIdEmpleadoEnc As Integer, ByVal vidAlmacen As Integer,
    ByVal vEstatus As String, ByVal vSubTotal As Decimal, ByVal vFecCancela As DateTime,
    ByVal vUserCancela As String, ByVal vMotivoCancela As String, ByVal vFecEntrega As DateTime,
    ByVal vUserEntrega As String, ByVal vIdPersonalRec As Integer, ByVal vEntregaCambio As Boolean)
        _IdPreSalida = vIdPreSalida
        _idOrdenTrabajo = vidOrdenTrabajo
        _FecSolicitud = vFecSolicitud
        _UserSolicitud = vUserSolicitud
        _IdEmpleadoEnc = vIdEmpleadoEnc
        _idAlmacen = vidAlmacen
        _Estatus = vEstatus
        _SubTotal = vSubTotal
        _FecCancela = vFecCancela
        _UserCancela = vUserCancela
        _MotivoCancela = vMotivoCancela
        _FecEntrega = vFecEntrega
        _UserEntrega = vUserEntrega
        _IdPersonalRec = vIdPersonalRec
        _EntregaCambio = vEntregaCambio

        _Existe = True
    End Sub
#End Region

#Region "Propiedades"
    Public Property IdPreSalida As Integer
        Get
            Return _IdPreSalida
        End Get
        Set(ByVal value As Integer)
            _IdPreSalida = value
        End Set
    End Property
    Public Property idOrdenTrabajo As Integer
        Get
            Return _idOrdenTrabajo
        End Get
        Set(ByVal value As Integer)
            _idOrdenTrabajo = value
        End Set
    End Property
    Public Property FecSolicitud As DateTime
        Get
            Return _FecSolicitud
        End Get
        Set(ByVal value As DateTime)
            _FecSolicitud = value
        End Set
    End Property

    Public Property UserSolicitud As String
        Get
            Return _UserSolicitud
        End Get
        Set(ByVal value As String)
            _UserSolicitud = value
        End Set
    End Property
    Public Property IdEmpleadoEnc As Integer
        Get
            Return _IdEmpleadoEnc
        End Get
        Set(ByVal value As Integer)
            _IdEmpleadoEnc = value
        End Set
    End Property
    Public Property NombreCompleto As String
        Get
            Return _NombreCompleto
        End Get
        Set(ByVal value As String)
            _NombreCompleto = value
        End Set
    End Property
    Public Property idAlmacen As Integer
        Get
            Return _idAlmacen
        End Get
        Set(ByVal value As Integer)
            _idAlmacen = value
        End Set
    End Property
    Public Property Estatus As String
        Get
            Return _Estatus
        End Get
        Set(ByVal value As String)
            _Estatus = value
        End Set
    End Property

    Public Property SubTotal As Decimal
        Get
            Return _SubTotal
        End Get
        Set(ByVal value As Decimal)
            _SubTotal = value
        End Set
    End Property
    Public Property FecCancela As DateTime
        Get
            Return _FecCancela
        End Get
        Set(ByVal value As DateTime)
            _FecCancela = value
        End Set
    End Property

    Public Property UserCancela As String
        Get
            Return _UserCancela
        End Get
        Set(ByVal value As String)
            _UserCancela = value
        End Set
    End Property
    Public Property MotivoCancela As String
        Get
            Return _MotivoCancela
        End Get
        Set(ByVal value As String)
            _MotivoCancela = value
        End Set
    End Property

    Public Property FecEntrega As DateTime
        Get
            Return _FecEntrega
        End Get
        Set(ByVal value As DateTime)
            _FecEntrega = value
        End Set
    End Property

    Public Property UserEntrega As String
        Get
            Return _UserEntrega
        End Get
        Set(ByVal value As String)
            _UserEntrega = value
        End Set
    End Property

    Public Property IdPersonalRec As Integer
        Get
            Return _IdPersonalRec
        End Get
        Set(ByVal value As Integer)
            _IdPersonalRec = value
        End Set
    End Property
    Public Property EntregaCambio As Boolean
        Get
            Return _EntregaCambio
        End Get
        Set(ByVal value As Boolean)
            _EntregaCambio = value
        End Set
    End Property
    Public Property Existe As Boolean
        Get
            Return _Existe
        End Get
        Set(ByVal value As Boolean)
            _Existe = value
        End Set
    End Property
#End Region
    Public Function tbPreSalidas(ByVal filtroSinWhere As String, ByVal OrderBySin As String) As DataTable
        If filtroSinWhere <> "" Then
            filtroSinWhere = "WHERE " & filtroSinWhere & " "
        End If

        If OrderBySin <> "" Then
            OrderBySin = "ORDER BY " & OrderBySin
        End If

        StrSql = "SELECT cab.IdPreSalida, " &
        "cab.idOrdenTrabajo, " &
        "ot.idOrdenSer, " &
        "cab.FecSolicitud, " &
        "cab.IdEmpleadoEnc, " &
        "per.NombreCompleto, " &
        "ot.NotaRecepcion, " &
        "cab.UserSolicitud, " &
        "perus.NombreCompleto as NomSolicita " &
        "FROM dbo.CabPreSalida cab " &
        "left JOIN dbo.CatPersonal Per  ON cab.IdEmpleadoEnc = Per.idPersonal " &
        "LEFT JOIN dbo.usuariosSys us ON us.nombreUsuario = cab.UserSolicitud " &
        "LEFT JOIN dbo.CatPersonal perus ON perus.idPersonal = us.idPersonal " &
        "INNER JOIN dbo.DetRecepcionOS OT ON OT.idOrdenTrabajo = cab.idOrdenTrabajo " & filtroSinWhere & OrderBySin

        MyTabla = BD.ExecuteReturn(StrSql)

        Return MyTabla

    End Function
    Public Function tbDetPreSalidaxId(ByVal IdPreSalida As Integer) As DataTable
        StrSql = "SELECT CAST(0 AS bit) as Entregar, det.IdPreSalida, " &
                "DET.Cantidad, " &
                "DET.idProducto, " &
                "DET.idUnidadProd, " &
                "pro.CNOMBREPRODUCTO, " &
                "pro.CCODIGOPRODUCTO, " &
                "isnull(DET.CantidadEnt,0) as CantidadEnt, " &
                "det.Costo " &
                "FROM dbo.DetPreSalida DET " &
                "INNER JOIN dbo.CabPreSalida cab ON cab.IdPreSalida = DET.IdPreSalida " &
                "INNER JOIN dbo.CatProductosCOM pro ON DET.idProducto = pro.CIDPRODUCTO " &
                "WHERE det.IdPreSalida =  " & IdPreSalida

        MyTabla = BD.ExecuteReturn(StrSql)

        Return MyTabla

    End Function

    Public Function tbPreSalidasPREOC(ByVal filtroSinWhere As String, ByVal OrderBySin As String) As DataTable
        '

        If filtroSinWhere <> "" Then
            filtroSinWhere = " WHERE " & filtroSinWhere & " "
        End If

        If OrderBySin <> "" Then
            OrderBySin = " ORDER BY " & OrderBySin
        End If

        StrSql = "SELECT c.IdPreOC, " &
        "c.idOrdenTrabajo, " &
        "dos.idOrdenSer, " &
        "c.FechaPreOc, " &
        "dros.idPersonalResp, " &
        "pres.NombreCompleto AS NomPersonalResp, " &
        "dros.NotaRecepcion, " &
        "usr.NombreCompleto AS NomSolicita " &
        "FROM dbo.CabPreOC c " &
        "INNER JOIN dbo.DetOrdenServicio dos ON c.idOrdenTrabajo = dos.idOrdenTrabajo " &
        "INNER JOIN dbo.DetRecepcionOS dros ON dros.idOrdenSer = dos.idOrdenSer AND dros.idOrdenTrabajo = c.idOrdenTrabajo " &
        "INNER JOIN dbo.CatPersonal pres ON dros.idPersonalResp = pres.idPersonal " &
        "INNER JOIN dbo.usuariosSys us ON c.UserSolicita = us.nombreUsuario " &
        "INNER JOIN dbo.CatPersonal usr ON us.idPersonal = usr.idPersonal " &
        filtroSinWhere & OrderBySin


        MyTabla = BD.ExecuteReturn(StrSql)

        Return MyTabla

    End Function

    Public Function tbDetPreSalidaPreOCxId(ByVal IdPreOC As Integer) As DataTable
        'StrSql = "SELECT d.IdPreOC, " &
        '"d.CantidadSol, " &
        '"d.idProducto, " &
        '"p.CCODIGOPRODUCTO, " &
        '"p.CNOMBREPRODUCTO " &
        '"FROM dbo.DetPreOC d " &
        '"INNER JOIN dbo.CabPreOC c ON c.IdPreOC = d.IdPreOC " &
        '"INNER JOIN dbo.CatProductosCOM p ON d.idProducto = p.CIDPRODUCTO " &
        '"WHERE d.IdPreOC =  " & IdPreOC

        StrSql = "SELECT  CAST(0 AS BIT) AS Entregar , " &
        "d.IdPreOC , " &
        "d.CantidadSol AS Cantidad, " &
        "d.idProducto , " &
        "ISNULL(d.idUnidadProd,0) AS idUnidadProd, " &
        "p.CCODIGOPRODUCTO , " &
        "p.CNOMBREPRODUCTO, " &
        "d.CantidadSol AS CantidadEnt, " &
        "p.precio AS Costo " &
        "FROM    dbo.DetPreOC d " &
        "INNER JOIN dbo.CabPreOC c ON c.IdPreOC = d.IdPreOC " &
        "INNER JOIN dbo.CatProductosCOM p ON d.idProducto = p.CIDPRODUCTO " &
        "WHERE d.IdPreOC =  " & IdPreOC


        MyTabla = BD.ExecuteReturn(StrSql)

        Return MyTabla

    End Function
End Class
