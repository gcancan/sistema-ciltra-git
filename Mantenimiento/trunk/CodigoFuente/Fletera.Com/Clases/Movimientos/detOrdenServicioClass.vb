﻿Public Class detOrdenServicioClass
    'idOrdenSer	            vidOrdenSer	            _idOrdenSer
    'idOrdenActividad	    vidOrdenActividad	    _idOrdenActividad
    'idServicio	            vidServicio	            _idServicio
    'idActividad	        vidActividad	        _idActividad
    'DuracionActHr	        vDuracionActHr	        _DuracionActHr
    'NoPersonal	            vNoPersonal	            _NoPersonal
    'id_proveedor	        vid_proveedor	        _id_proveedor
    'Estatus	            vEstatus	            _Estatus
    'CostoManoObra	        vCostoManoObra	        _CostoManoObra
    'CostoProductos	        vCostoProductos	        _CostoProductos
    'FechaDiag	            vFechaDiag	            _FechaDiag
    'UsuarioDiag	        vUsuarioDiag	        _UsuarioDiag
    'FechaAsig	            vFechaAsig	            _FechaAsig
    'UsuarioAsig	        vUsuarioAsig	        _UsuarioAsig
    'FechaPausado	        vFechaPausado	        _FechaPausado
    'NotaPausado	        vNotaPausado	        _NotaPausado
    'FechaTerminado	        vFechaTerminado	        _FechaTerminado
    'UsuarioTerminado	    vUsuarioTerminado	    _UsuarioTerminado
    'idOrdenTrabajo	        vidOrdenTrabajo	        _idOrdenTrabajo

    Private _idOrdenSer As Integer
    Private _idOrdenActividad As Integer
    Private _idServicio As Integer
    Private _idActividad As Integer
    Private _DuracionActHr As Double
    Private _NoPersonal As Integer
    Private _id_proveedor As Integer
    Private _Estatus As String
    Private _CostoManoObra As Double
    Private _CostoProductos As Double
    Private _FechaDiag As Date
    Private _UsuarioDiag As String
    Private _FechaAsig As Date
    Private _UsuarioAsig As String
    Private _FechaPausado As Date
    Private _NotaPausado As String
    Private _FechaTerminado As Date
    Private _UsuarioTerminado As String
    Private _idOrdenTrabajo As Integer

    Private _idLlanta As String
    Private _PosLlanta As Integer
    Private _NotaDiagnostico As String

    Private _Existe As Boolean
    Private _TablaBd As String = "DetOrdenServicio"

    Dim StrSql As String
    Dim DsClase As New DataTable
    Dim MyTabla As DataTable = New DataTable(_TablaBd)

#Region "Constructor"
    Sub New(ByVal vidOrdenTrabajo As String)

        StrSql = "select idOrdenSer, " &
        "idOrdenActividad, " &
        "isnull(idServicio,0) as idServicio, " &
        "isnull(idActividad,0) as idActividad, " &
        "DuracionActHr, " &
        "NoPersonal, " &
        "id_proveedor, " &
        "Estatus, " &
        "CostoManoObra, " &
        "CostoProductos, " &
        "isnull(FechaDiag,getdate()) as FechaDiag, " &
        "isnull(UsuarioDiag, '') as UsuarioDiag , " &
        "isnull(FechaAsig,getdate()) as FechaAsig, " &
        "isnull(UsuarioAsig, '') as UsuarioAsig , " &
        "isnull(FechaPausado,getdate()) as FechaPausado, " &
        "isnull(NotaPausado,'') as NotaPausado , " &
        "isnull(FechaTerminado,getdate()) as FechaTerminado, " &
        "isnull(UsuarioTerminado, '') as UsuarioTerminado , " &
        "idOrdenTrabajo,  " &
        "isnull(idLlanta,'') as idLlanta,  " &
        "isnull(PosLlanta,0) as PosLlanta,  " &
        "isnull(NotaDiagnostico,'') as NotaDiagnostico  " &
        "from " & _TablaBd & " detOS " &
        "where detOS.idOrdenTrabajo = " & vidOrdenTrabajo

        DsClase = BD.ExecuteReturn(StrSql)
        If DsClase.Rows.Count > 0 Then
            With DsClase.Rows(0)
                _idOrdenSer = .Item("idOrdenSer")
                _idOrdenActividad = .Item("idOrdenActividad")
                _idServicio = .Item("idServicio")
                _idActividad = .Item("idActividad")
                _DuracionActHr = .Item("DuracionActHr")
                _NoPersonal = .Item("NoPersonal")
                _id_proveedor = .Item("id_proveedor")
                _Estatus = .Item("Estatus")
                _CostoManoObra = .Item("CostoManoObra")
                _CostoProductos = .Item("CostoProductos")
                _FechaDiag = .Item("FechaDiag")
                _UsuarioDiag = .Item("UsuarioDiag")
                _FechaAsig = .Item("FechaAsig")
                _UsuarioAsig = .Item("UsuarioAsig")
                _FechaPausado = .Item("FechaPausado")
                _NotaPausado = .Item("NotaPausado")
                _FechaTerminado = .Item("FechaTerminado")
                _UsuarioTerminado = .Item("UsuarioTerminado")
                _idOrdenTrabajo = .Item("idOrdenTrabajo")

                _idLlanta = .Item("idLlanta")
                _PosLlanta = .Item("PosLlanta")
                _NotaDiagnostico = .Item("NotaDiagnostico")

            End With
            _Existe = True
        Else
            _Existe = False
        End If

    End Sub
    Sub New(ByVal vidOrdenSer As Integer, ByVal vidOrdenActividad As Integer, ByVal vidServicio As Integer,
            ByVal vidActividad As Integer, ByVal vDuracionActHr As Double, ByVal vNoPersonal As Integer,
            ByVal vid_proveedor As Integer,
            ByVal vEstatus As String, ByVal vCostoManoObra As Double, ByVal vCostoProductos As Double,
            ByVal vFechaDiag As Date, ByVal vUsuarioDiag As String, ByVal vFechaAsig As Date,
            ByVal vUsuarioAsig As String, ByVal vFechaPausado As Date, ByVal vNotaPausado As String,
            ByVal vFechaTerminado As Date, ByVal vUsuarioTerminado As String, ByVal vidOrdenTrabajo As Integer,
            ByVal vidLlanta As String, ByVal vPosLlanta As Integer, ByVal vNotaDiagnostico As String)

        _idOrdenSer = vidOrdenSer
        _idOrdenActividad = vidOrdenActividad
        _idServicio = vidServicio
        _idActividad = vidActividad
        _DuracionActHr = vDuracionActHr
        _NoPersonal = vNoPersonal
        _id_proveedor = vid_proveedor
        _Estatus = vEstatus
        _CostoManoObra = vCostoManoObra
        _CostoProductos = vCostoProductos
        _FechaDiag = vFechaDiag
        _UsuarioDiag = vUsuarioDiag
        _FechaAsig = vFechaAsig
        _UsuarioAsig = vUsuarioAsig
        _FechaPausado = vFechaPausado
        _NotaPausado = vNotaPausado
        _FechaTerminado = vFechaTerminado
        _UsuarioTerminado = vUsuarioTerminado
        _idOrdenTrabajo = vidOrdenTrabajo

        _idLlanta = vidLlanta
        _PosLlanta = vPosLlanta
        _NotaDiagnostico = vNotaDiagnostico



        _Existe = True
    End Sub
#End Region

#Region "Propiedades"
    Public Property idOrdenSer As Integer
        Get
            Return _idOrdenSer
        End Get
        Set(ByVal value As Integer)
            _idOrdenSer = value
        End Set
    End Property
    Public Property idOrdenActividad As Integer
        Get
            Return _idOrdenActividad
        End Get
        Set(ByVal value As Integer)
            _idOrdenActividad = value
        End Set
    End Property
    Public Property idServicio As Integer
        Get
            Return _idServicio
        End Get
        Set(ByVal value As Integer)
            _idServicio = value
        End Set
    End Property
    Public Property idActividad As Integer
        Get
            Return _idActividad
        End Get
        Set(ByVal value As Integer)
            _idActividad = value
        End Set
    End Property
    Public Property DuracionActHr As Double
        Get
            Return _DuracionActHr
        End Get
        Set(ByVal value As Double)
            _DuracionActHr = value
        End Set
    End Property
    Public Property NoPersonal As Integer
        Get
            Return _NoPersonal
        End Get
        Set(ByVal value As Integer)
            _NoPersonal = value
        End Set
    End Property
    Public Property id_proveedor As Integer
        Get
            Return _id_proveedor
        End Get
        Set(ByVal value As Integer)
            _id_proveedor = value
        End Set
    End Property
    Public Property Estatus As String
        Get
            Return _Estatus
        End Get
        Set(ByVal value As String)
            _Estatus = value
        End Set
    End Property
    Public Property CostoManoObra As Double
        Get
            Return _CostoManoObra
        End Get
        Set(ByVal value As Double)
            _CostoManoObra = value
        End Set
    End Property
    Public Property CostoProductos As Double
        Get
            Return _CostoProductos
        End Get
        Set(ByVal value As Double)
            _CostoProductos = value
        End Set
    End Property
    Public Property FechaDiag As Date
        Get
            Return _FechaDiag
        End Get
        Set(ByVal value As Date)
            _FechaDiag = value
        End Set
    End Property
    Public Property UsuarioDiag As String
        Get
            Return _UsuarioDiag
        End Get
        Set(ByVal value As String)
            _UsuarioDiag = value
        End Set
    End Property
    Public Property FechaAsig As Date
        Get
            Return _FechaAsig
        End Get
        Set(ByVal value As Date)
            _FechaAsig = value
        End Set
    End Property
    Public Property UsuarioAsig As String
        Get
            Return _UsuarioAsig
        End Get
        Set(ByVal value As String)
            _UsuarioAsig = value
        End Set
    End Property
    Public Property FechaPausado As Date
        Get
            Return _FechaPausado
        End Get
        Set(ByVal value As Date)
            _FechaPausado = value
        End Set
    End Property
    Public Property NotaPausado As String
        Get
            Return _NotaPausado
        End Get
        Set(ByVal value As String)
            _NotaPausado = value
        End Set
    End Property
    Public Property FechaTerminado As Date
        Get
            Return _FechaTerminado
        End Get
        Set(ByVal value As Date)
            _FechaTerminado = value
        End Set
    End Property
    Public Property UsuarioTerminado As String
        Get
            Return _UsuarioTerminado
        End Get
        Set(ByVal value As String)
            _UsuarioTerminado = value
        End Set
    End Property
    Public Property idOrdenTrabajo As Integer
        Get
            Return _idOrdenTrabajo
        End Get
        Set(ByVal value As Integer)
            _idOrdenTrabajo = value
        End Set
    End Property

    Public Property idLlanta As String
        Get
            Return _idLlanta
        End Get
        Set(ByVal value As String)
            _idLlanta = value
        End Set
    End Property

    Public Property PosLlanta As Integer
        Get
            Return _PosLlanta
        End Get
        Set(ByVal value As Integer)
            _PosLlanta = value
        End Set
    End Property

    Public Property NotaDiagnostico As String
        Get
            Return _NotaDiagnostico
        End Get
        Set(ByVal value As String)
            _NotaDiagnostico = value
        End Set
    End Property

    Public Property Existe As Boolean
        Get
            Return _Existe
        End Get
        Set(ByVal value As Boolean)
            _Existe = value
        End Set
    End Property
#End Region

#Region "Funciones"
    Public Function ConsultaActualizaDato(ByVal vValor As String, ByVal vNomCampo As String, ByVal vTipoDato As TipoDato) As String
        StrSql = ""
        Dim Filtro As String = ""
        If vTipoDato = TipoDato.TdCadena Then
            Filtro = "'" & vValor & "'"
        ElseIf vTipoDato = TipoDato.TdFecha Or vTipoDato = TipoDato.TdFechaHora Then
            Filtro = "'" & FormatFecHora(vValor, True, True) & "'"
        ElseIf vTipoDato = TipoDato.TdNumerico Then
            Filtro = vValor
        End If

        StrSql = "update " & _TablaBd & " SET " & vNomCampo & " = " & Filtro & " where idOrdenActividad = " & _idOrdenActividad

        Return StrSql
    End Function

    Public Function CargaOrdSerxUni(ByVal vidUnidadTrans As String) As DataTable
        MyTabla.Rows.Clear()

        StrSql = "select os.idOrdenSer, " & _
        "os.FechaRecepcion, " & _
        "os.idTipoOrden, " & _
        "os.Kilometraje, " & _
        "os.idEmpleadoAutoriza, " & _
        "os.idEmpleadoEntrega, " & _
        "os.UsuarioRecepcion, " & _
        "os.UsuarioEntrega " & _
        "from CabOrdenServicio OS " & _
        "where idUnidadTrans = '" & vidUnidadTrans & "'" & _
        "order by idOrdenSer"

        MyTabla = BD.ExecuteReturn(StrSql)

        'If MyTabla.Rows.Count > 0 Then
        '    Return MyTabla
        'End If
        Return MyTabla
    End Function

    Public Function CargaOrdTrabxUni(ByVal vidUnidadTrans As String, Optional ByVal vidOrdenSer As Integer = 0) As DataTable
        Dim Filtro As String = ""
        MyTabla.Rows.Clear()

        If vidOrdenSer > 0 Then
            Filtro = " where ot.idOrdenSer = " & vidOrdenSer
        End If
        StrSql = "select OT.idOrdenTrabajo, " & _
        "ot.idOrdenSer, " & _
        "det.UsuarioAsig, " & _
        "ot.idPersonalResp, " & _
        "det.DuracionActHr, " & _
        "det.FechaTerminado " & _
        "from DetRecepcionOS OT " & _
        "inner join detOrdenServicio det on ot.idOrdenSer = det.idOrdenSer and ot.idOrdenTrabajo = det.idOrdenTrabajo " & _
        "inner join CabOrdenServicio OS on ot.idOrdenSer = os.idOrdenSer and os.idUnidadTrans = '" & vidUnidadTrans & _
        "'" & Filtro & _
        " order by ot.idOrdenSer "
        MyTabla = BD.ExecuteReturn(StrSql, , CONSTR)

        Return MyTabla
    End Function
#End Region
End Class
