﻿Public Class FoliosDineroClass
    Private _Folio As Integer
    Private _NoRecibo As Integer
    Private _Fecha As DateTime
    Private _idEmpleadoEnt As Integer
    Private _idEmpleadoRec As Integer
    Private _Estatus As String
    Private _Importe As Decimal
    Private _ImporteComp As Decimal
    Private _NumViaje As Integer

    Private _Existe As Boolean
    Private _TablaBd As String = "FoliosDinero"

    Dim StrSql As String
    Dim DsClase As New DataTable
    Dim MyTabla As DataTable = New DataTable(_TablaBd)

#Region "Constructor"
    Sub New(ByVal vFolio As Integer)
        _Folio = vFolio

        StrSql = "SELECT FD.Folio,FD.NoRecibo,FD.Fecha,FD.idEmpleadoEnt, " +
        "FD.idEmpleadoRec,FD.Estatus,FD.Importe,FD.ImporteComp,FD.NumViaje " +
       "from " + _TablaBd & " FD " +
        "where FD.Folio = " & vFolio

        DsClase = BD.ExecuteReturn(StrSql)
        If DsClase.Rows.Count > 0 Then
            With DsClase.Rows(0)
                _Folio = .Item("Folio")
                _NoRecibo = .Item("NoRecibo")
                _Fecha = .Item("Fecha")
                _idEmpleadoEnt = .Item("idEmpleadoEnt")
                _idEmpleadoRec = .Item("idEmpleadoRec")
                _Estatus = .Item("Estatus")
                _Importe = .Item("Importe")
                _ImporteComp = .Item("ImporteComp")
                _NumViaje = .Item("NumViaje")
            End With
            _Existe = True
        Else
            _Existe = False
        End If

    End Sub
    Sub New(ByVal vFolio As Integer, ByVal vNoRecibo As Integer, ByVal vFecha As DateTime, ByVal vidEmpleadoEnt As Integer,
    ByVal vidEmpleadoRec As Integer, ByVal vEstatus As String, ByVal vImporte As Decimal, ByVal vImporteComp As Decimal,
    ByVal vNumViaje As Integer)
        _Folio = vFolio
        _NoRecibo = vNoRecibo
        _Fecha = vFecha
        _idEmpleadoEnt = vidEmpleadoEnt
        _idEmpleadoRec = vidEmpleadoRec
        _Estatus = vEstatus
        _Importe = vImporte
        _ImporteComp = vImporteComp
        _NumViaje = vNumViaje
        _Existe = True
    End Sub

#End Region

#Region "Propiedades"
    Public Property Folio As Integer
        Get
            Return _Folio
        End Get
        Set(ByVal value As Integer)
            _Folio = value
        End Set
    End Property
    Public Property NoRecibo As Integer
        Get
            Return _NoRecibo
        End Get
        Set(ByVal value As Integer)
            _NoRecibo = value
        End Set
    End Property
    Public Property Fecha As DateTime
        Get
            Return _Fecha
        End Get
        Set(ByVal value As DateTime)
            _Fecha = value
        End Set
    End Property

    Public Property idEmpleadoEnt As Integer
        Get
            Return _idEmpleadoEnt
        End Get
        Set(ByVal value As Integer)
            _idEmpleadoEnt = value
        End Set
    End Property
    Public Property idEmpleadoRec As Integer
        Get
            Return _idEmpleadoRec
        End Get
        Set(ByVal value As Integer)
            _idEmpleadoRec = value
        End Set
    End Property
    Public Property Estatus As String
        Get
            Return _Estatus
        End Get
        Set(ByVal value As String)
            _Estatus = value
        End Set
    End Property
    Public Property Importe As Decimal
        Get
            Return _Importe
        End Get
        Set(ByVal value As Decimal)
            _Importe = value
        End Set
    End Property
    Public Property ImporteComp As Decimal
        Get
            Return _ImporteComp
        End Get
        Set(ByVal value As Decimal)
            _ImporteComp = value
        End Set
    End Property
    Public Property NumViaje As String
        Get
            Return _NumViaje
        End Get
        Set(ByVal value As String)
            _NumViaje = value
        End Set
    End Property
    Public Property Existe As Boolean
        Get
            Return _Existe
        End Get
        Set(ByVal value As Boolean)
            _Existe = value
        End Set
    End Property
#End Region

#Region "Funciones"

#End Region
End Class
