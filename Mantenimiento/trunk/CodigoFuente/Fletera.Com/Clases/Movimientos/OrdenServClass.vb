﻿Public Class OrdenServClass

    Private _idOrdenSer As Integer
    Private _idPadreOrdSer As Integer
    Private _IdEmpresa As Integer
    Private _idUnidadTrans As String
    Private _idTipoOrden As Integer
    Private _FechaRecepcion As Date
    Private _idEmpleadoEntrega As Integer
    Private _UsuarioRecepcion As String
    Private _Kilometraje As Double
    Private _Estatus As String
    Private _FechaDiagnostico As Date
    Private _FechaAsignado As Date
    Private _FechaTerminado As Date
    Private _FechaEntregado As Date
    Private _UsuarioEntrega As String
    Private _idEmpleadoRecibe As Integer
    Private _NotaFinal As String
    Private _usuarioCan As String
    Private _idEmpleadoAutoriza As Integer
    Private _fechaCaptura As Date

    Private _Existe As Boolean
    Private _TablaBd As String = "CabOrdenServicio"


    Dim StrSql As String
    Dim DsClase As New DataTable
    Dim MyTabla As DataTable = New DataTable(_TablaBd)

#Region "Constructor"
    Sub New(ByVal vidOrdenSer As Integer)
        _idOrdenSer = vidOrdenSer
        StrSql = "select OS.idOrdenSer, " &
        "OS.idPadreOrdSer, " &
        "OS.IdEmpresa, " &
        "OS.idUnidadTrans, " &
        "OS.idTipoOrden, " &
        "isnull(OS.FechaRecepcion,getdate()) as FechaRecepcion, " &
        "isnull(OS.idEmpleadoEntrega,0) as idEmpleadoEntrega, " &
        "isnull(OS.UsuarioRecepcion,'') as UsuarioRecepcion, " &
        "OS.Kilometraje, " &
        "OS.Estatus, " &
        "isnull(OS.FechaDiagnostico,getdate()) as FechaDiagnostico, " &
        "isnull(OS.FechaAsignado,getdate()) as FechaAsignado, " &
        "isnull(OS.FechaTerminado,getdate()) as FechaTerminado, " &
        "isnull(OS.FechaEntregado,getdate()) as FechaEntregado, " &
        "isnull(OS.UsuarioEntrega,'') as UsuarioEntrega, " &
        "isnull(OS.idEmpleadoRecibe,0) as idEmpleadoRecibe, " &
        "isnull(OS.NotaFinal,'') as NotaFinal, " &
        "isnull(OS.usuarioCan,0) as usuarioCan, " &
        "isnull(OS.idEmpleadoAutoriza,0) as idEmpleadoAutoriza, " &
        "isnull(OS.fechaCaptura,getdate()) as fechaCaptura " &
        "from " & _TablaBd & " OS " &
        "where OS.idOrdenSer = " & vidOrdenSer


        DsClase = BD.ExecuteReturn(StrSql)
        If DsClase.Rows.Count > 0 Then
            With DsClase.Rows(0)
                _idPadreOrdSer = .Item("idPadreOrdSer")
                _idOrdenSer = .Item("idOrdenSer")
                _IdEmpresa = .Item("IdEmpresa") & ""
                _idUnidadTrans = .Item("idUnidadTrans")
                _idTipoOrden = .Item("idTipoOrden")
                _FechaRecepcion = .Item("FechaRecepcion")
                _idEmpleadoEntrega = .Item("idEmpleadoEntrega")
                _UsuarioRecepcion = .Item("UsuarioRecepcion")
                _Kilometraje = .Item("Kilometraje")
                _Estatus = .Item("Estatus")
                _FechaDiagnostico = .Item("FechaDiagnostico")
                _FechaAsignado = .Item("FechaAsignado")
                _FechaTerminado = .Item("FechaTerminado")
                _FechaEntregado = .Item("FechaEntregado")
                _UsuarioEntrega = .Item("UsuarioEntrega")
                _idEmpleadoRecibe = .Item("idEmpleadoRecibe")
                _NotaFinal = .Item("NotaFinal")
                _usuarioCan = .Item("usuarioCan")
                _idEmpleadoAutoriza = .Item("idEmpleadoAutoriza")
                _fechaCaptura = .Item("fechaCaptura")

            End With
            _Existe = True
        Else
            _Existe = False
        End If

    End Sub
    Sub New(ByVal vidOrdenSer As Integer, ByVal vIdEmpresa As Integer, ByVal vidUnidadTrans As String,
            ByVal vidTipoOrden As Integer, ByVal vFechaRecepcion As Date, ByVal vidEmpleadoEntrega As Integer,
            ByVal vUsuarioRecepcion As String,
            ByVal vKilometraje As Double, ByVal vEstatus As String, ByVal vFechaDiagnostico As Date,
            ByVal vFechaAsignado As Date, ByVal vFechaTerminado As Date, ByVal vFechaEntregado As Date,
            ByVal vUsuarioEntrega As String, ByVal vidEmpleadoRecibe As Integer, ByVal vNotaFinal As String,
            ByVal vusuarioCan As String, ByVal vidEmpleadoAutoriza As Integer, ByVal vfechaCaptura As Date,
            ByVal vidPadreOrdSer As Integer)

        _idOrdenSer = vidOrdenSer
        _IdEmpresa = vIdEmpresa
        _idUnidadTrans = vidUnidadTrans
        _idTipoOrden = vidTipoOrden
        _FechaRecepcion = vFechaRecepcion
        _idEmpleadoEntrega = vidEmpleadoEntrega
        _UsuarioRecepcion = vUsuarioRecepcion
        _Kilometraje = vKilometraje
        _Estatus = vEstatus
        _FechaDiagnostico = vFechaDiagnostico
        _FechaAsignado = vFechaAsignado
        _FechaTerminado = vFechaTerminado
        _FechaEntregado = vFechaEntregado
        _UsuarioEntrega = vUsuarioEntrega
        _idEmpleadoRecibe = vidEmpleadoRecibe
        _NotaFinal = vNotaFinal
        _usuarioCan = vusuarioCan
        _idEmpleadoAutoriza = vidEmpleadoAutoriza
        _fechaCaptura = vfechaCaptura
        _idPadreOrdSer = vidPadreOrdSer
        _Existe = True
    End Sub
#End Region
#Region "Propiedades"
    Public Property idOrdenSer As Integer
        Get
            Return _idOrdenSer
        End Get
        Set(ByVal value As Integer)
            _idOrdenSer = value
        End Set
    End Property

    Public Property idPadreOrdSer As Integer
        Get
            Return _idPadreOrdSer
        End Get
        Set(ByVal value As Integer)
            _idPadreOrdSer = value
        End Set
    End Property
    Public Property IdEmpresa As Integer
        Get
            Return _IdEmpresa
        End Get
        Set(ByVal value As Integer)
            _IdEmpresa = value
        End Set
    End Property
    Public Property idUnidadTrans As String
        Get
            Return _idUnidadTrans
        End Get
        Set(ByVal value As String)
            _idUnidadTrans = value
        End Set
    End Property
    Public Property idTipoOrden As Integer
        Get
            Return _idTipoOrden
        End Get
        Set(ByVal value As Integer)
            _idTipoOrden = value
        End Set
    End Property
    Public Property FechaRecepcion As Date
        Get
            Return _FechaRecepcion
        End Get
        Set(ByVal value As Date)
            _FechaRecepcion = value
        End Set
    End Property
    Public Property idEmpleadoEntrega As Integer
        Get
            Return _idEmpleadoEntrega
        End Get
        Set(ByVal value As Integer)
            _idEmpleadoEntrega = value
        End Set
    End Property
    Public Property UsuarioRecepcion As String
        Get
            Return _UsuarioRecepcion
        End Get
        Set(ByVal value As String)
            _UsuarioRecepcion = value
        End Set
    End Property
    Public Property Kilometraje As Double
        Get
            Return _Kilometraje
        End Get
        Set(ByVal value As Double)
            _Kilometraje = value
        End Set
    End Property
    Public Property Estatus As String
        Get
            Return _Estatus
        End Get
        Set(ByVal value As String)
            _Estatus = value
        End Set
    End Property
    Public Property FechaDiagnostico As Date
        Get
            Return _FechaDiagnostico
        End Get
        Set(ByVal value As Date)
            _FechaDiagnostico = value
        End Set
    End Property
    Public Property FechaAsignado As Date
        Get
            Return _FechaAsignado
        End Get
        Set(ByVal value As Date)
            _FechaAsignado = value
        End Set
    End Property
    Public Property FechaTerminado As Date
        Get
            Return _FechaTerminado
        End Get
        Set(ByVal value As Date)
            _FechaTerminado = value
        End Set
    End Property
    Public Property FechaEntregado As Date
        Get
            Return _FechaEntregado
        End Get
        Set(ByVal value As Date)
            _FechaEntregado = value
        End Set
    End Property
    Public Property UsuarioEntrega As String
        Get
            Return _UsuarioEntrega
        End Get
        Set(ByVal value As String)
            _UsuarioEntrega = value
        End Set
    End Property
    Public Property idEmpleadoRecibe As Integer
        Get
            Return _idEmpleadoRecibe
        End Get
        Set(ByVal value As Integer)
            _idEmpleadoRecibe = value
        End Set
    End Property
    Public Property NotaFinal As String
        Get
            Return _NotaFinal
        End Get
        Set(ByVal value As String)
            _NotaFinal = value
        End Set
    End Property
    Public Property usuarioCan As String
        Get
            Return _usuarioCan
        End Get
        Set(ByVal value As String)
            _usuarioCan = value
        End Set
    End Property
    Public Property idEmpleadoAutoriza As Integer
        Get
            Return _idEmpleadoAutoriza
        End Get
        Set(ByVal value As Integer)
            _idEmpleadoAutoriza = value
        End Set
    End Property
    Public Property fechaCaptura As Date
        Get
            Return _fechaCaptura
        End Get
        Set(ByVal value As Date)
            _fechaCaptura = value
        End Set
    End Property
    Public Property Existe As Boolean
        Get
            Return _Existe
        End Get
        Set(ByVal value As Boolean)
            _Existe = value
        End Set
    End Property
#End Region

#Region "Funciones"
    Public Function ContEstatusxOS(ByVal NomEstatus As String) As Integer
        Dim Contador As Integer = 0

        'Solo funciona en Microsoft SQL SERVER

        StrSql = "SELECT * FROM " & _
        "( " & _
        "SELECT COUNT(ESTATUS) AS TOTAL, ESTATUS FROM DETORDENSERVICIO WHERE IDORDENSER = " & _idOrdenSer & " GROUP BY ESTATUS " & _
        "UNION " & _
        "SELECT COUNT(ESTATUS) AS TOTAL, 'TODAS' AS ESTATUS FROM DETORDENSERVICIO WHERE IDORDENSER = " & _idOrdenSer & " " & _
        ") AS T " & _
        "PIVOT (SUM(T.TOTAL) FOR T.ESTATUS IN ([ASIG],[DIAG],[TER],[TODAS])) PVT"

        DsClase = BD.ExecuteReturn(StrSql)
        If DsClase.Rows.Count > 0 Then
            With DsClase.Rows(0)
                'Contador = .Item(NomEstatus)
                Contador = NoNull(.Item(NomEstatus), "D")
            End With
        End If
        Return Contador
    End Function

    Public Function ConsultaActualizaDato(ByVal vValor As String, ByVal vNomCampo As String, ByVal vTipoDato As TipoDato) As String
        StrSql = ""
        Dim Filtro As String = ""
        If vTipoDato = TipoDato.TdCadena Then
            Filtro = "'" & vValor & "'"
        ElseIf vTipoDato = TipoDato.TdFecha Then
            Filtro = "'" & FormatFecHora(vValor, True, True) & "'"
        ElseIf vTipoDato = TipoDato.TdNumerico Then
            Filtro = vValor
        End If

        StrSql = "update " & _TablaBd & " SET " & vNomCampo & " = " & Filtro & " where idOrdenSer = " & _idOrdenSer

        Return StrSql
    End Function

    Public Sub ActualizaDato(ByVal vValor As String, ByVal vNomCampo As String, ByVal vTipoDato As TipoDato)
        Dim Filtro As String = ""
        If vTipoDato = TipoDato.TdCadena Then
            Filtro = "'" & vValor & "'"
        ElseIf vTipoDato = TipoDato.TdFecha Then
            Filtro = "'" & FormatFecHora(vValor, True, True) & "'"
        ElseIf vTipoDato = TipoDato.TdNumerico Then
            Filtro = vValor
        End If

        StrSql = "update " & _TablaBd & " SET " & vNomCampo & " = " & Filtro & " where idOrdenSer = " & _idOrdenSer

        Try
            BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
            BD.Execute(StrSql, True)
            'Audita(_TablaBd, _CadValAnt, _CadValNue, EnTipAccion.Modifica)
            BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
        Catch ex As Exception
            BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
            Throw New Exception(ex.Message)
        End Try


    End Sub

    Public Function tbDetOrdSerxID(ByVal idOrdenSer As Integer) As DataTable

        StrSql = "SELECT dos.idOrdenSer AS CLAVE, " &
        "dos.idOrdenTrabajo AS idOrdenTrabajo, " &
        "ot.NotaRecepcion AS NotaRecepcion, " &
        "div.NomDivision AS NomDivision, " &
        "dos.idOrdenSer AS idOrdenSer, " &
        "ot.idDivision AS idDivision, " &
        "dos.idServicio AS idServicio, " &
        "dos.idActividad as idActividad, " &
        "dos.DuracionActHr  AS DuracionActHr, " &
        "dos.Estatus  AS Estatus, " &
        "cabos.idUnidadTrans AS IdUnidadTransp, " &
        "cabos.idTipoServicio AS idTipoServicio, " &
        "dos.idOrdenActividad " &
        "FROM dbo.DetOrdenServicio DOS " &
        "left JOIN dbo.DetRecepcionOS OT ON dos.idOrdenSer = ot.idOrdenSer AND dos.idOrdenTrabajo = ot.idOrdenTrabajo " &
        "INNER JOIN dbo.CatDivision DIV ON ot.idDivision = div.idDivision " &
        "INNER JOIN dbo.CabOrdenServicio CABOS ON dos.idOrdenSer = CABOS.idOrdenSer " &
        "WHERE dos.idOrdenSer = " & idOrdenSer

        MyTabla = BD.ExecuteReturn(StrSql)

        Return MyTabla

    End Function


    Public Function VerificaPadrexTOS(ByVal Padre As Integer, ByVal TOS As Integer) As Boolean
        Dim Resp As Boolean = False

        StrSql = "SELECT * FROM dbo.CabOrdenServicio WHERE idPadreOrdSer = " & Padre & " AND idTipOrdServ = " & TOS
        DsClase = BD.ExecuteReturn(StrSql)
        If DsClase.Rows.Count > 0 Then
            'With DsClase.Rows(0)
            '    'Contador = .Item(NomEstatus)
            '    Contador = NoNull(.Item(NomEstatus), "D")
            'End With
            Resp = True
        End If
        Return Resp
    End Function

    Public Function tbOrdenesServ(Optional ByVal filtroConWhere As String = "") As DataTable

        StrSql = "SELECT mos.idOrdenSer, mos.fechaCaptura FROM dbo.CabOrdenServicio MOS " & filtroConWhere


        MyTabla = BD.ExecuteReturn(StrSql)

        Return MyTabla

    End Function
    'Public Sub Guardar(ByVal vIdEmpresa As String, ByVal vidUnidadTrans As Integer, ByVal vidTipoOrden As Integer,
    'ByVal vFechaRecepcion As Integer, ByVal vidEmpleadoEntrega As Integer, ByVal vUsuarioRecepcion As String, ByVal vidAlmacen As Integer,
    'ByVal vUltOs As Integer)
    '    Dim sSql As String = "", _CadValAnt As String = "", _CadValNue As String = ""
    '    If _Existe Then
    '        If _idTipoUnidad <> vidTipoUnidad Then
    '            sSql += "idTipoUnidad = " & vidTipoUnidad & ","
    '            _CadValAnt += "idTipoUnidad =" & vidTipoUnidad & ","
    '        End If
    '        If _DescripcionUni <> vDescripcionUni Then
    '            sSql += "DescripcionUni = '" & vDescripcionUni & "',"
    '            _CadValAnt += "DescripcionUni =" & vDescripcionUni & ","
    '        End If
    '        If _idMarca <> vidMarca Then
    '            sSql += "idMarca = '" & vidMarca & "',"
    '            _CadValAnt += "idMarca =" & vidMarca & ","
    '        End If
    '        If _IdEmpresa <> vIdEmpresa Then
    '            sSql += "idEmpresa = " & vIdEmpresa & ","
    '            _CadValAnt += "idEmpresa =" & vIdEmpresa & ","
    '        End If

    '        If _idSucursal <> vidSucursal Then
    '            sSql += "idSucursal = " & vidSucursal & ","
    '            _CadValAnt += "idSucursal =" & vidSucursal & ","
    '        End If
    '        If _Estatus <> vEstatus Then
    '            sSql += "Estatus = '" & vEstatus & "',"
    '            _CadValAnt += "Estatus =" & vEstatus & ","
    '        End If
    '        If _idAlmacen <> vidAlmacen Then
    '            sSql += "idAlmacen = " & vidAlmacen & ","
    '            _CadValAnt += "idAlmacen =" & vidAlmacen & ","
    '        End If
    '        'If _UltOS <> vUltOs Then
    '        '    sSql += "UltOS = " & vUltOs & ","
    '        '    _CadValAnt += "UltOS =" & vUltOs & ","
    '        'End If

    '        If sSql <> "" Then
    '            sSql = Mid(sSql, 1, sSql.Length - 1) 'Para quitar la Coma ','
    '            _CadValAnt = "idUnidadTrans=" & _idUnidadTrans & "," & _CadValAnt
    '            _CadValAnt = Mid(_CadValAnt, 1, _CadValAnt.Length - 1)
    '            _CadValNue = Replace(sSql, "'", "")

    '            sSql = "UPDATE " & _TablaBd & " SET " & sSql & " WHERE idUnidadTrans = '" & _idUnidadTrans & "'"
    '            Try
    '                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
    '                BD.Execute(sSql, True)
    '                Audita(_TablaBd, _CadValAnt, _CadValNue, EnTipAccion.Modifica)
    '                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
    '            Catch ex As Exception
    '                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
    '                Throw New Exception(ex.Message)
    '            End Try
    '        End If
    '    Else
    '        sSql = "INSERT INTO " & _TablaBd & "(idUnidadTrans,idTipoUnidad,DescripcionUni,idMarca,idEmpresa,idSucursal,Estatus,idAlmacen) " & _
    '        "VALUES('" & _idUnidadTrans & "'," & vidTipoUnidad & ",'" & vDescripcionUni & "'," & vidMarca & "," & vIdEmpresa & "," & vidSucursal & _
    '        ",'" & vEstatus & "'," & vidAlmacen & ")"

    '        _CadValNue = "idUnidadTrans=" & _idUnidadTrans & "idTipoUnidad=" & vidTipoUnidad & "DescripcionUni = " & vDescripcionUni & "idMarca = " & vidMarca & _
    '        "idEmpresa = " & vIdEmpresa & "idSucursal = " & vidSucursal & "Estatus = " & vEstatus & "idAlmacen = " & vidAlmacen

    '        'If bEsIdentidad Then
    '        '    sSql = "INSERT INTO " & _TablaBd & "(idTipoServicio,NomServicio,Descripcion,CadaKms,CadaTiempoDias,Estatus,Costo) " & _
    '        '    "VALUES(" & vidTipoServicio & ",'" & vNomServicio & "','" & vDescripcion & "'," & vCadaKms & "," & vCadaTiempoDias & _
    '        '    ",'" & vEstatus & "'," & vCosto & ")"

    '        '    _CadValNue = "idTipoServicio=" & vidTipoServicio & "NomServicio = " & vNomServicio & "Descripcion = " & vDescripcion & _
    '        '    "CadaKms = " & vCadaKms & "CadaTiempoDias = " & vCadaTiempoDias & "Estatus = " & vEstatus & "Costo = " & vCosto

    '        'Else
    '        '    sSql = "INSERT INTO " & _TablaBd & "(idServicio,idTipoServicio,NomServicio,Descripcion,CadaKms,CadaTiempoDias,Estatus,Costo) " & _
    '        '    "VALUES(" & _idUnidadTrans & "," & vidTipoServicio & ",'" & vNomServicio & "','" & vDescripcion & "'," & vCadaKms & "," & vCadaTiempoDias & _
    '        '    ",'" & vEstatus & "'," & vCosto & ")"

    '        '    _CadValNue = "idServicio=" & _idUnidadTrans & "idTipoServicio=" & vidTipoServicio & "NomServicio = " & vNomServicio & "Descripcion = " & vDescripcion & _
    '        '    "CadaKms = " & vCadaKms & "CadaTiempoDias = " & vCadaTiempoDias & "Estatus = " & vEstatus & "Costo = " & vCosto
    '        'End If

    '        Try
    '            BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
    '            BD.Execute(sSql, True)
    '            Audita(_TablaBd, "", _CadValNue, EnTipAccion.Inserta)
    '            BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
    '        Catch ex As Exception
    '            BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
    '            Throw New Exception(ex.Message)
    '        End Try
    '    End If
    '    _idTipoUnidad = vidTipoUnidad
    '    _DescripcionUni = vDescripcionUni
    '    _idMarca = vidMarca
    '    _IdEmpresa = vIdEmpresa
    '    _idSucursal = vidSucursal
    '    _Estatus = vEstatus
    '    _idAlmacen = vidAlmacen
    'End Sub
#End Region

End Class
