﻿Public Class FacturaClass
    Private _NoSerie As String
    Private _NoFolio As Integer
    Private _NoMovimiento As Integer
    Private _idCliente As Integer
    Private _IdEmpresa As Integer
    Private _SubTotalGrav As Decimal
    Private _SubTotalExento As Decimal
    Private _Retencion As Decimal
    Private _Iva As Decimal
    Private _FechaMovimiento As DateTime
    Private _Usuario As String
    Private _CantTotal As Decimal
    Private _Estatus As String
    Private _FechaVencimiento As DateTime

    Private _Existe As Boolean
    Private _TablaBd As String = "CabMovimientos"

    Dim StrSql As String
    Dim DsClase As New DataTable
    Dim MyTabla As DataTable = New DataTable(_TablaBd)

#Region "Constructor"
    Sub New(ByVal vFactura As String)


        StrSql = "SELECT fac.NoSerie, " &
        "fac.IdMovimiento as NoFolio, " &
        "fac.CCODIGOC01_CLI, " &
        "fac.IdEmpresa, " &
        "fac.SubTotalGrav, " &
        "fac.SubTotalExento, " &
        "fac.Iva, " &
        "fac.Retencion, " &
        "fac.FechaMovimiento, " &
        "fac.FechaVencimiento, " &
        "fac.Usuario, " &
        "fac.Estatus, " &
        "fac.CantTotal, " &
        "fac.NoMovimiento " &
        "FROM dbo." & _TablaBd & " fac " &
        "WHERE (fac.NoSerie + CAST(fac.IdMovimiento AS VARCHAR(50))) = '" & vFactura & "'"

        DsClase = BD.ExecuteReturn(StrSql)
        If DsClase.Rows.Count > 0 Then
            With DsClase.Rows(0)
                _NoSerie = .Item("NoSerie")
                _NoFolio = .Item("NoFolio")
                _idCliente = .Item("CCODIGOC01_CLI")
                _IdEmpresa = .Item("IdEmpresa")
                _SubTotalGrav = .Item("SubTotalGrav")
                _SubTotalExento = .Item("SubTotalExento")
                _Retencion = .Item("Retencion")
                _Iva = .Item("Iva")
                _FechaMovimiento = .Item("FechaMovimiento")
                _Usuario = .Item("Usuario")
                _CantTotal = .Item("CantTotal")
                _Estatus = .Item("Estatus")
                _FechaVencimiento = .Item("FechaVencimiento")
                _NoMovimiento = .Item("NoMovimiento")
            End With
            _Existe = True
        Else
            _Existe = False
        End If

    End Sub

    Sub New(ByVal vNoSerie As String, ByVal vNoFolio As Integer, ByVal vidCliente As Integer, ByVal vIdEmpresa As Integer,
    ByVal vSubTotalGrav As Decimal, ByVal vSubTotalExento As Decimal, ByVal vRetencion As Decimal, ByVal vIva As Decimal,
    ByVal vFechaMovimiento As DateTime, ByVal vUsuario As String, ByVal vCantTotal As Decimal, ByVal vEstatus As String,
    ByVal vFechaVencimiento As DateTime, ByVal vNoMovimiento As Integer)
        _NoSerie = vNoSerie
        _NoFolio = vNoFolio
        _idCliente = vidCliente
        _IdEmpresa = vIdEmpresa
        _SubTotalGrav = vSubTotalGrav
        _SubTotalExento = vSubTotalExento
        _Retencion = vRetencion
        _Iva = vIva
        _FechaMovimiento = vFechaMovimiento
        _Usuario = vUsuario
        _CantTotal = vCantTotal
        _Estatus = vEstatus
        _FechaVencimiento = vFechaVencimiento
        _NoMovimiento = vNoMovimiento
        _Existe = True
    End Sub
#End Region

#Region "Propiedades"
    Public Property NoSerie As String
        Get
            Return _NoSerie
        End Get
        Set(ByVal value As String)
            _NoSerie = value
        End Set
    End Property

    Public Property NoFolio As Integer
        Get
            Return _NoFolio
        End Get
        Set(ByVal value As Integer)
            _NoFolio = value
        End Set
    End Property
    Public Property NoMovimiento As Integer
        Get
            Return _NoMovimiento
        End Get
        Set(ByVal value As Integer)
            _NoMovimiento = value
        End Set
    End Property

    Public Property idCliente As Integer
        Get
            Return _idCliente
        End Get
        Set(ByVal value As Integer)
            _idCliente = value
        End Set
    End Property
    Public Property IdEmpresa As Integer
        Get
            Return _IdEmpresa
        End Get
        Set(ByVal value As Integer)
            _IdEmpresa = value
        End Set
    End Property
    Public Property SubTotalGrav As Decimal
        Get
            Return _SubTotalGrav
        End Get
        Set(ByVal value As Decimal)
            _SubTotalGrav = value
        End Set
    End Property
    Public Property SubTotalExento As Decimal
        Get
            Return _SubTotalExento
        End Get
        Set(ByVal value As Decimal)
            _SubTotalExento = value
        End Set
    End Property
    Public Property Retencion As Decimal
        Get
            Return _Retencion
        End Get
        Set(ByVal value As Decimal)
            _Retencion = value
        End Set
    End Property
    Public Property Iva As Decimal
        Get
            Return _Iva
        End Get
        Set(ByVal value As Decimal)
            _Iva = value
        End Set
    End Property
    Public Property FechaMovimiento As DateTime
        Get
            Return _FechaMovimiento
        End Get
        Set(ByVal value As DateTime)
            _FechaMovimiento = value
        End Set
    End Property
    Public Property Usuario As String
        Get
            Return _Usuario
        End Get
        Set(ByVal value As String)
            _Usuario = value
        End Set
    End Property
    Public Property CantTotal As Decimal
        Get
            Return _CantTotal
        End Get
        Set(ByVal value As Decimal)
            _CantTotal = value
        End Set
    End Property


    Public Property FechaVencimiento As DateTime
        Get
            Return _FechaVencimiento
        End Get
        Set(ByVal value As DateTime)
            _FechaVencimiento = value
        End Set
    End Property
    Public Property Estatus As String
        Get
            Return _Estatus
        End Get
        Set(ByVal value As String)
            _Estatus = value
        End Set
    End Property
    Public Property Existe As Boolean
        Get
            Return _Existe
        End Get
        Set(ByVal value As Boolean)
            _Existe = value
        End Set
    End Property
#End Region

#Region "Consultas"
    Public Function TablaFacturasViajes(ByVal Empresa As Integer) As DataTable
        MyTabla.Rows.Clear()

        'StrSql = "SELECT idPersonal, idEmpresa, NombreCompleto FROM dbo.CatPersonal " &
        '"WHERE idPuesto in (7,36,37) AND Estatus = '" & IIf(Activo, "ACT", "BAJ") & "'" &
        'IIf(idEmpresa > 0, " and idEmpresa = " & idEmpresa, "")

        StrSql = "SELECT (NoSerie + CAST(IdMovimiento AS VARCHAR(50))) AS Factura, " &
        "NoSerie AS Serie, IdMovimiento AS Folio FROM dbo.CabMovimientos " &
        "WHERE (NoSerie + CAST(IdMovimiento AS VARCHAR(50))) IN ( " &
        "SELECT DISTINCT (NoSerie + CAST(NoFolio AS VARCHAR(50)) ) AS factura " &
        "FROM dbo.CabGuia WHERE NoFolio > 0 AND IdEmpresa =" & Empresa & ") " &
        "AND Estatus <> 'CAN'"





        MyTabla = BD.ExecuteReturn(StrSql)

        Return MyTabla

    End Function

    Public Function DetalleFactura(ByVal NoMovimiento As Integer) As DataTable
        MyTabla.Rows.Clear()

        StrSql = "SELECT det.NoMovimiento, " &
        "det.Consecutivo, " &
        "det.NoSerie, " &
        "det.IdMovimiento AS NoFolio, " &
        "det.CCODIGOP01_PRO AS serv, " &
        "prod.CNOMBREPRODUCTO, " &
        "det.Cantidad, " &
        "det.Precio, " &
        "(det.Cantidad * det.Precio) AS Importe, " &
        "(((det.Cantidad * det.Precio)) * (det.PorcIVA/100)) AS IVA, " &
        "(((det.Cantidad * det.Precio)) * (det.PorcRet/100)) AS Retencion, " &
        "(((det.Cantidad * det.Precio))+((((det.Cantidad * det.Precio)) * (det.PorcIVA/100)))-((((det.Cantidad * det.Precio)) * (det.PorcRet/100)))) AS TOTAL, " &
        "det.COBSERVA01 AS Texto " &
        "FROM dbo.DetMovimientos det " &
        "INNER JOIN CatServFact prod ON det.CCODIGOP01_PRO = prod.CCODIGOPRODUCTO " &
        "WHERE NoMovimiento = " & NoMovimiento

        MyTabla = BD.ExecuteReturn(StrSql)

        Return MyTabla

    End Function
    Public Function ViajesFactura(ByVal vFactura As String) As DataTable
        MyTabla.Rows.Clear()

        StrSql = "SELECT g.SerieGuia +  CAST(g.NumGuiaId AS VARCHAR(50)) AS viaje, " &
        "g.idTractor, " &
        "g.idRemolque1, " &
        "g.idDolly, " &
        "g.idRemolque2, " &
        "g.FechaHoraViaje, " &
        "g.TipoViaje " &
        "FROM dbo.CabGuia g " &
        "WHERE (NoSerie + CAST(NoFolio AS VARCHAR(50))) = '" & vFactura & "'"

        MyTabla = BD.ExecuteReturn(StrSql)

        Return MyTabla

    End Function
#End Region

End Class
