﻿Public Class RecibosDineroClass

    Private _NoRecibo As Integer
    Private _Fecha As DateTime
    Private _idEmpleadoEnt As Integer
    Private _idEmpleadoRec As Integer
    Private _Estatus As String
    Private _Importe As Decimal
    Private _ImporteComp As Decimal
    Private _Concepto As String
    Private _IdEmpresa As Integer


    Private _Existe As Boolean
    Private _TablaBd As String = "RecibosDinero"

    Dim StrSql As String
    Dim DsClase As New DataTable
    Dim MyTabla As DataTable = New DataTable(_TablaBd)

#Region "Constructor"
    Sub New(ByVal vNoRecibo As Integer)
        _NoRecibo = vNoRecibo
        StrSql = "SELECT RD.NoRecibo,RD.Fecha, " +
        "RD.idEmpleadoEnt,RD.idEmpleadoRec, " +
        "RD.Estatus,RD.Importe,RD.ImporteComp,isnull(Concepto,'') as Concepto, " +
        "isnull(idEmpresa,0) as idEmpresa " +
       "from " + _TablaBd & " RD " +
        "where RD.NoRecibo = " & vNoRecibo

        DsClase = BD.ExecuteReturn(StrSql)
        If DsClase.Rows.Count > 0 Then
            With DsClase.Rows(0)
                _NoRecibo = .Item("NoRecibo")
                _Fecha = .Item("Fecha")
                _idEmpleadoEnt = .Item("idEmpleadoEnt")
                _idEmpleadoRec = .Item("idEmpleadoRec")
                _Estatus = .Item("Estatus")
                _Importe = .Item("Importe")
                _ImporteComp = .Item("ImporteComp")
                _Concepto = .Item("Concepto")
                _IdEmpresa = .Item("IdEmpresa")
            End With
            _Existe = True
        Else
            _Existe = False
        End If

    End Sub
    Sub New(ByVal vNoRecibo As Integer, ByVal vFecha As DateTime, ByVal vidEmpleadoEnt As Integer,
    ByVal vidEmpleadoRec As Integer, ByVal vEstatus As String, ByVal vImporte As Decimal,
    ByVal vImporteComp As Decimal, ByVal vConcepto As String, ByVal vIdEmpresa As Integer)
        _NoRecibo = vNoRecibo
        _Fecha = vFecha
        _idEmpleadoEnt = vidEmpleadoEnt
        _idEmpleadoRec = vidEmpleadoRec
        _Estatus = vEstatus
        _Importe = vImporte
        _ImporteComp = vImporteComp
        _Concepto = vConcepto
        _IdEmpresa = vIdEmpresa
        _Existe = True
    End Sub
#End Region

#Region "Propiedades"
    Public Property NoRecibo As Integer
        Get
            Return _NoRecibo
        End Get
        Set(ByVal value As Integer)
            _NoRecibo = value
        End Set
    End Property
    Public Property Fecha As DateTime
        Get
            Return _Fecha
        End Get
        Set(ByVal value As DateTime)
            _Fecha = value
        End Set
    End Property

    Public Property idEmpleadoEnt As Integer
        Get
            Return _idEmpleadoEnt
        End Get
        Set(ByVal value As Integer)
            _idEmpleadoEnt = value
        End Set
    End Property

    Public Property IdEmpresa As Integer
        Get
            Return _IdEmpresa
        End Get
        Set(ByVal value As Integer)
            _IdEmpresa = value
        End Set
    End Property
    Public Property idEmpleadoRec As Integer
        Get
            Return _idEmpleadoRec
        End Get
        Set(ByVal value As Integer)
            _idEmpleadoRec = value
        End Set
    End Property
    Public Property Estatus As String
        Get
            Return _Estatus
        End Get
        Set(ByVal value As String)
            _Estatus = value
        End Set
    End Property
    Public Property Importe As Decimal
        Get
            Return _Importe
        End Get
        Set(ByVal value As Decimal)
            _Importe = value
        End Set
    End Property
    Public Property ImporteComp As Decimal
        Get
            Return _ImporteComp
        End Get
        Set(ByVal value As Decimal)
            _ImporteComp = value
        End Set
    End Property
    Public Property Concepto As String
        Get
            Return _Concepto
        End Get
        Set(ByVal value As String)
            _Concepto = value
        End Set
    End Property
    Public Property Existe As Boolean
        Get
            Return _Existe
        End Get
        Set(ByVal value As Boolean)
            _Existe = value
        End Set
    End Property
#End Region

    Public Function tbRecibosDinero(Optional ByVal filtroConWhere As String = "") As DataTable

        'StrSql = "SELECT mos.idPadreOrdSer, mos.FechaCreacion FROM dbo.MasterOrdServ MOS " & filtroConWhere
        StrSql = "SELECT NoRecibo,Fecha,Importe,Estatus FROM dbo.RecibosDinero " & filtroConWhere

        MyTabla = BD.ExecuteReturn(StrSql)

        Return MyTabla

    End Function


    Public Function tbOriDes(ByVal NumGuiaId As Integer) As DataTable

        'StrSql = "SELECT mos.idPadreOrdSer, mos.FechaCreacion FROM dbo.MasterOrdServ MOS " & filtroConWhere
        StrSql = "SELECT DISTINCT (Ori.nombreOrigen + '-' + dest.Descripcion) as OriDes " &
         "FROM dbo.DetGuia det " &
         "INNER JOIN dbo.CabGuia cab ON Cab.NumGuiaId = Det.NumGuiaId " &
         "INNER JOIN dbo.origen Ori ON det.idOrigen = ori.idOrigen " &
         "INNER JOIN dbo.trafico_plazas_cat Dest ON det.IdPlazaTraf = dest.Clave " &
         "WHERE Cab.NumGuiaId = " & NumGuiaId

        MyTabla = BD.ExecuteReturn(StrSql)

        Return MyTabla

    End Function





End Class
