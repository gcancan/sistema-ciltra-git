﻿Public Class MasterOrdServClass
    'idPadreOrdSer	_idPadreOrdSer	vidPadreOrdSer
    'idTractor	    _idTractor	    vidTractor
    'idTolva1	    _idTolva1	    vidTolva1
    'idDolly	    _idDolly	    vidDolly
    'idTolva2	    _idTolva2	    vidTolva2
    'idChofer	    _idChofer	    vidChofer
    'FechaCreacion	_FechaCreacion	vFechaCreacion
    'UserCrea	    _UserCrea	    vUserCrea

    Private _idPadreOrdSer As Integer
    Private _idTractor As String
    Private _idTolva1 As String
    Private _idDolly As String
    Private _idTolva2 As String
    Private _idChofer As Integer
    Private _FechaCreacion As Date
    Private _UserCrea As String
    Private _Kilometraje As Integer
    Private _idEmpleadoAutoriza As Integer

    '10/ENE/2017
    Private _UserModifica As String
    Private _FechaModifica As Date
    Private _UserCancela As String
    Private _FechaCancela As Date
    Private _MotivoCancela As String
    Private _Estatus As String

    Private _Existe As Boolean
    Private _TablaBd As String = "MasterOrdServ"

    Dim StrSql As String
    Dim DsClase As New DataTable
    Dim DsClase2 As New DataTable
    Dim MyTabla As DataTable = New DataTable(_TablaBd)

#Region "Constructor"

    Sub New(ByVal idPadreOrdSer As Integer)
        _idPadreOrdSer = idPadreOrdSer

        StrSql = "SELECT mos.idPadreOrdSer, " &
        "mos.idTractor, " &
        "isnull(mos.idTolva1,'') as idTolva1, " &
        "isnull(mos.idDolly,'') as idDolly, " &
        "isnull(mos.idTolva2,'') as idTolva2, " &
        "mos.idChofer, mos.FechaCreacion, " &
        "isnull(mos.UserCrea,'') as UserCrea ," &
        "isnull(mos.UserModifica,'') as UserModifica," &
        "isnull(mos.FechaModifica,getdate()) as FechaModifica," &
        "isnull(mos.UserCancela,'') as UserCancela," &
        "isnull(mos.FechaCancela,getdate()) as FechaCancela," &
        "isnull(mos.MotivoCancela,'') as MotivoCancela," &
        "isnull(mos.Estatus,'') as Estatus " &
        "FROM " & _TablaBd & " MOS " &
        "WHERE mos.idPadreOrdSer = " & idPadreOrdSer

        DsClase = BD.ExecuteReturn(StrSql)
        If DsClase.Rows.Count > 0 Then
            With DsClase.Rows(0)
                _idTractor = .Item("idTractor")
                _idTolva1 = .Item("idTolva1")
                _idDolly = .Item("idDolly")
                _idTolva2 = .Item("idTolva2")
                _idChofer = .Item("idChofer")
                _FechaCreacion = .Item("FechaCreacion")
                _UserCrea = .Item("UserCrea")
                _UserModifica = .Item("UserModifica")
                _FechaModifica = .Item("FechaModifica")
                _UserCancela = .Item("UserCancela")
                _FechaCancela = .Item("FechaCancela")
                _MotivoCancela = .Item("MotivoCancela")
                _Estatus = .Item("Estatus")

            End With

            StrSql = "SELECT DISTINCT cabos.Kilometraje, " &
              "cabos.idEmpleadoAutoriza " &
              "FROM dbo.CabOrdenServicio CABOS " &
              "WHERE idPadreOrdSer = " & idPadreOrdSer

            DsClase2 = BD.ExecuteReturn(StrSql)
            If DsClase2.Rows.Count > 0 Then
                With DsClase2.Rows(0)
                    _Kilometraje = .Item("Kilometraje")
                    _idEmpleadoAutoriza = .Item("idEmpleadoAutoriza")
                End With

            End If

            _Existe = True
        Else
            _Existe = False
        End If

    End Sub

    Sub New(ByVal vidPadreOrdSer As String, ByVal vidTractor As String, ByVal vidTolva1 As String, ByVal vidDolly As String,
    ByVal vidTolva2 As String, ByVal vidChofer As String, ByVal vFechaCreacion As String, ByVal vUserCrea As String,
    ByVal vUserModifica As String, ByVal vFechaModifica As String, ByVal vUserCancela As String, ByVal vFechaCancela As String,
    ByVal vMotivoCancela As String, ByVal vEstatus As String)
        _idPadreOrdSer = vidPadreOrdSer
        _idTractor = vidTractor
        _idTolva1 = vidTolva1
        _idDolly = vidDolly
        _idTolva2 = vidTolva2
        _idChofer = vidChofer
        _FechaCreacion = vFechaCreacion
        _UserCrea = vUserCrea
        _UserModifica = vUserModifica
        _FechaModifica = vFechaModifica
        _UserCancela = vUserCancela
        _FechaCancela = vFechaCancela
        _MotivoCancela = vMotivoCancela
        _Estatus = vEstatus
        _Existe = True
    End Sub
#End Region

#Region "Propiedades"
    Public Property idPadreOrdSer As Integer
        Get
            Return _idPadreOrdSer
        End Get
        Set(ByVal value As Integer)
            _idPadreOrdSer = value
        End Set
    End Property
    Public Property idTractor As String
        Get
            Return _idTractor
        End Get
        Set(ByVal value As String)
            _idTractor = value
        End Set
    End Property
    Public Property idTolva1 As String
        Get
            Return _idTolva1
        End Get
        Set(ByVal value As String)
            _idTolva1 = value
        End Set
    End Property
    Public Property idDolly As String
        Get
            Return _idDolly
        End Get
        Set(ByVal value As String)
            _idDolly = value
        End Set
    End Property
    Public Property idTolva2 As String
        Get
            Return _idTolva2
        End Get
        Set(ByVal value As String)
            _idTolva2 = value
        End Set
    End Property
    Public Property idChofer As Integer
        Get
            Return _idChofer
        End Get
        Set(ByVal value As Integer)
            _idChofer = value
        End Set
    End Property
    Public Property FechaCreacion As Date
        Get
            Return _FechaCreacion
        End Get
        Set(ByVal value As Date)
            _FechaCreacion = value
        End Set
    End Property
    Public Property UserCrea As String
        Get
            Return _UserCrea
        End Get
        Set(ByVal value As String)
            _UserCrea = value
        End Set
    End Property
    Public Property Kilometraje As Integer
        Get
            Return _Kilometraje
        End Get
        Set(ByVal value As Integer)
            _Kilometraje = value
        End Set
    End Property
    Public Property idEmpleadoAutoriza As Integer
        Get
            Return _idEmpleadoAutoriza
        End Get
        Set(ByVal value As Integer)
            _idEmpleadoAutoriza = value
        End Set
    End Property

    Public Property FechaModifica As Date
        Get
            Return _FechaModifica
        End Get
        Set(ByVal value As Date)
            _FechaModifica = value
        End Set
    End Property
    Public Property UserModifica As String
        Get
            Return _UserModifica
        End Get
        Set(ByVal value As String)
            _UserModifica = value
        End Set
    End Property

    Public Property FechaCancela As Date
        Get
            Return _FechaCancela
        End Get
        Set(ByVal value As Date)
            _FechaCancela = value
        End Set
    End Property
    Public Property UserCancela As String
        Get
            Return _UserCancela
        End Get
        Set(ByVal value As String)
            _UserCancela = value
        End Set
    End Property

    Public Property MotivoCancela As String
        Get
            Return _MotivoCancela
        End Get
        Set(ByVal value As String)
            _MotivoCancela = value
        End Set
    End Property
    Public Property Estatus As String
        Get
            Return _Estatus
        End Get
        Set(ByVal value As String)
            _Estatus = value
        End Set
    End Property

    Public Property Existe As Boolean
        Get
            Return _Existe
        End Get
        Set(ByVal value As Boolean)
            _Existe = value
        End Set
    End Property
#End Region

#Region "Funciones"


    Public Function ChecaEstatusPRE(ByVal vidPadreOrdSer As Integer) As Boolean
        StrSql = "SELECT SUM(CASE WHEN Estatus = 'PRE' THEN 0 ELSE 1 END) AS checaPRE " &
        "FROM dbo.CabOrdenServicio " &
        "WHERE idPadreOrdSer = " & vidPadreOrdSer

        DsClase = BD.ExecuteReturn(StrSql)
        If DsClase.Rows.Count > 0 Then
            With DsClase.Rows(0)
                If .Item("checaPRE") = 0 Then
                    Return True
                Else
                    Return False
                End If
            End With
        Else
            Return False
        End If

    End Function

    Public Function tbTipoOrdSerxPadre(ByVal vidPadreOrdSer As Integer) As DataTable
        StrSql = "SELECT distinct cabos.idTipOrdServ, tos.NomTipOrdServ " &
        "FROM dbo.CabOrdenServicio CABOS " &
        "INNER JOIN dbo.CatTipoOrdServ TOS ON cabos.idTipOrdServ = tos.idTipOrdServ " &
        "WHERE idPadreOrdSer = " & vidPadreOrdSer

        MyTabla = BD.ExecuteReturn(StrSql)

        Return MyTabla
    End Function

    Public Function tbPadreOS(Optional ByVal filtroConWhere As String = "") As DataTable

        StrSql = "SELECT mos.idPadreOrdSer, mos.FechaCreacion FROM dbo.MasterOrdServ MOS " & filtroConWhere


        MyTabla = BD.ExecuteReturn(StrSql)

        Return MyTabla

    End Function
    Public Function tbOrdSerxPadrexTipoOrdServ(ByVal vidPadreOrdSer As Integer, ByVal idTipOrdServ As Integer,
    Optional ByVal FiltroEsp As String = "") As DataTable
        MyTabla.Rows.Clear()

        StrSql = "SELECT cab.idOrdenSer AS CLAVE, " &
        "ROW_NUMBER() OVER(ORDER BY idOrdenSer DESC) AS Consecutivo, " &
        "cab.idOrdenSer, " &
        "cab.fechaCaptura, " &
        "cab.idTipOrdServ, " &
        "TOS.NomTipOrdServ, " &
        "TOS.bOrdenTrab, " &
        "cab.idUnidadTrans, " &
        "uni.idTipoUnidad, " &
        "TUNI.nomTipoUniTras, " &
        "cab.Kilometraje, " &
        "cab.idEmpleadoAutoriza, " &
        "cab.IdEmpresa, " &
        "emp.RazonSocial, " &
        "cab.idTipoServicio, " &
        "tser.NomTipoServicio, " &
        "cab.idPadreOrdSer, " &
        "cab.Estatus ," &
        "isnull(cab.FechaRecepcion,getdate()) as FechaRecepcion, " &
        "isnull(cab.FechaAsignado,getdate()) as FechaAsignado, " &
        "isnull(cab.FechaDiagnostico,getdate()) as FechaDiagnostico, " &
        "isnull(cab.FechaTerminado,getdate()) as FechaTerminado " &
        "FROM dbo.CabOrdenServicio cab " &
        "INNER JOIN dbo.CatTipoOrdServ TOS ON CAB.idTipOrdServ = TOS.idTipOrdServ  " &
        "INNER JOIN dbo.CatUnidadTrans UNI ON cab.idUnidadTrans = uni.idUnidadTrans " &
        "INNER JOIN dbo.CatTipoUniTrans TUNI ON UNI.idTipoUnidad = TUNI.idTipoUniTras " &
        "INNER JOIN dbo.CatEmpresas emp ON cab.IdEmpresa = emp.idEmpresa " &
        "left JOIN dbo.CatTipoServicio TSER ON cab.idTipoServicio = tser.idTipoServicio " &
        "WHERE cab.idPadreOrdSer =  " & vidPadreOrdSer & " and cab.idTipOrdServ = " & idTipOrdServ & FiltroEsp

        '"AND cab.Estatus = '" & Estatus & "'"


        MyTabla = BD.ExecuteReturn(StrSql)

        'If MyTabla.Rows.Count > 0 Then
        '    Return MyTabla
        'End If
        Return MyTabla
    End Function
    Public Function tbOrdSerxPadre(ByVal vidPadreOrdSer As Integer) As DataTable
        MyTabla.Rows.Clear()

        StrSql = "SELECT cab.idOrdenSer AS CLAVE, " &
        "ROW_NUMBER() OVER(ORDER BY idOrdenSer DESC) AS Consecutivo, " &
        "cab.idOrdenSer, " &
        "cab.fechaCaptura, " &
        "cab.idTipOrdServ, " &
        "TOS.NomTipOrdServ, " &
        "TOS.bOrdenTrab, " &
        "cab.idUnidadTrans, " &
        "uni.idTipoUnidad, " &
        "TUNI.nomTipoUniTras, " &
        "cab.Kilometraje, " &
        "cab.idEmpleadoAutoriza, " &
        "cab.IdEmpresa, " &
        "emp.RazonSocial, " &
        "cab.idTipoServicio, " &
        "tser.NomTipoServicio, " &
        "cab.idPadreOrdSer, " &
        "cab.Estatus " &
        "FROM dbo.CabOrdenServicio cab " &
        "INNER JOIN dbo.CatTipoOrdServ TOS ON CAB.idTipOrdServ = TOS.idTipOrdServ  " &
        "INNER JOIN dbo.CatUnidadTrans UNI ON cab.idUnidadTrans = uni.idUnidadTrans " &
        "INNER JOIN dbo.CatTipoUniTrans TUNI ON UNI.idTipoUnidad = TUNI.idTipoUniTras " &
        "INNER JOIN dbo.CatEmpresas emp ON cab.IdEmpresa = emp.idEmpresa " &
        "left JOIN dbo.CatTipoServicio TSER ON cab.idTipoServicio = tser.idTipoServicio " &
        "WHERE cab.idPadreOrdSer =  " & vidPadreOrdSer

        '"AND cab.Estatus = '" & Estatus & "'"


        MyTabla = BD.ExecuteReturn(StrSql)

        'If MyTabla.Rows.Count > 0 Then
        '    Return MyTabla
        'End If
        Return MyTabla
    End Function

    Public Function tbDetOrdSerxPadre(ByVal vidPadreOrdSer As Integer, Optional ByVal FiltroEsp As String = "") As DataTable

        StrSql = "SELECT DOS.IDORDENSER AS CLAVE, " &
        "DOS.IDORDENSER AS IDORDENSER, " &
        "ISNULL(DOS.IDSERVICIO,0) AS IDSERVICIO, " &
        "ISNULL(SER.NOMSERVICIO,'') AS NOMSERVICIO, " &
        "DOS.IDACTIVIDAD AS IDACTIVIDAD, " &
        "ISNULL(ACT.NOMBREACT,'') AS  NOMBREACT, " &
        "DOS.IDORDENTRABAJO AS IDORDENTRABAJO, " &
        "ISNULL(DOS.IDDIVISION,0) AS IDDIVISION, " &
        "ISNULL(DIV.NOMDIVISION,'') AS NOMDIVISION, " &
        "DOS.DURACIONACTHR  AS DURACIONACTHR, " &
        "ISNULL(OT.NOTARECEPCION,'') AS NOTARECEPCION, " &
        "ISNULL(OT.IDDIVISION,0) AS IDDIVISIONOT, " &
        "ISNULL(DIVOT.NOMDIVISION,'') AS NOMDIVISIONOT, " &
        "DOS.idOrdenActividad, " &
        "isnull(DOS.idLlanta,'') as idLlanta, " &
        "isnull(DOS.PosLLanta,0) as Posicion, " &
        "ISNULL(ot.idPersonalResp,0) AS idPersonalResp, " &
        "ISNULL(ot.idPersonalAyu1,0) AS idPersonalAyu1, " &
        "ISNULL(ot.idPersonalAyu2,0) AS idPersonalAyu2, " &
        "ISNULL(dos.Estatus,'') AS Estatus, " &
        "ISNULL(ACT.idEnum,0) AS idEnum, isnull(DOS.FaltInsumo,0) as FaltInsumo, " &
        "DOS.CIDPRODUCTO_SERV,DOS.Cantidad_CIDPRODUCTO_SERV,DOS.Precio_CIDPRODUCTO_SERV, " &
        "ISNULL(p.CCODIGOPRODUCTO,'') as CCODIGOPRODUCTO, ISNULL(p.CNOMBREPRODUCTO,'') as CNOMBREPRODUCTO " &
        "FROM DBO.DETORDENSERVICIO DOS " &
        "LEFT JOIN DBO.CATSERVICIOS SER ON DOS.IDSERVICIO = SER.IDSERVICIO " &
        "LEFT JOIN DBO.CATACTIVIDADES ACT ON DOS.IDACTIVIDAD = ACT.IDACTIVIDAD " &
        "INNER JOIN DBO.CABORDENSERVICIO CABOS ON DOS.IDORDENSER = CABOS.IDORDENSER " &
        "LEFT JOIN DBO.CATDIVISION DIV ON DOS.IDDIVISION = DIV.IDDIVISION " &
        "LEFT JOIN DBO.DETRECEPCIONOS OT ON OT.IDORDENSER = DOS.IDORDENSER AND OT.IDORDENTRABAJO = DOS.IDORDENTRABAJO " &
        "LEFT JOIN DBO.CATDIVISION DIVOT ON OT.IDDIVISION = DIVOT.IDDIVISION " &
        "LEFT JOIN dbo.CatProductosCOM p ON DOS.CIDPRODUCTO_SERV = p.CIDPRODUCTO " &
        "WHERE CABOS.IDPADREORDSER = " & vidPadreOrdSer & FiltroEsp

        '"ISNULL(OT.idopServLlantas,'') AS idopServLlantas, " &
        MyTabla = BD.ExecuteReturn(StrSql)

        Return MyTabla

    End Function






#End Region


End Class
