﻿Public Class CombustibleExtraClass
    Private _idVCExtra As Integer
    Private _idTipoOSComb As Integer
    Private _idUnidadTrans As String
    Private _FechaCrea As DateTime
    Private _UserCrea As String
    Private _idTipoCombustible As Integer
    Private _fechaCombustible As DateTime
    Private _ltCombustible As Decimal
    Private _precioCombustible As Decimal
    Private _importe As Decimal
    Private _idDespachador As Integer
    Private _idCargaCombustible As Integer
    Private _idChofer As Integer
    Private _NumViaje As Integer

    Private _Existe As Boolean
    Private _TablaBd As String = "cargaCombustibleExtra"


    Dim StrSql As String
    Dim DsClase As New DataTable
    Dim MyTabla As DataTable = New DataTable(_TablaBd)

#Region "Constructor"
    Sub New(ByVal vidVCExtra As Integer)
        _idVCExtra = vidVCExtra

        StrSql = "SELECT ce.idVCExtra, " &
        "ce.idTipoOSComb, " &
        "ce.idUnidadTrans, " &
        "ce.FechaCrea, " &
        "ce.UserCrea, " &
        "ce.idTipoCombustible, " &
        "isnull(ce.fechaCombustible,getdate()) as fechaCombustible, " &
        "ISNULL(ce.ltCombustible,0) AS ltCombustible, " &
        "ISNULL(ce.precioCombustible,0) AS precioCombustible, " &
        "ISNULL(ce.importe,0) AS importe, " &
        "ISNULL(ce.idDespachador,0) AS idDespachador, " &
        "ISNULL(ce.idCargaCombustible,0) AS idCargaCombustible, " &
        "ce.idchofer, " &
        "isnull(ce.NumViaje,0) as NumViaje " &
        "FROM dbo." & _TablaBd & " CE " &
        "WHERE ce.idVCExtra = " & vidVCExtra

        DsClase = BD.ExecuteReturn(StrSql)
        If DsClase.Rows.Count > 0 Then
            With DsClase.Rows(0)
                _idTipoOSComb = .Item("idTipoOSComb")
                _idUnidadTrans = .Item("idUnidadTrans")
                _FechaCrea = .Item("FechaCrea")
                _UserCrea = .Item("UserCrea")
                _idTipoCombustible = .Item("idTipoCombustible")
                _fechaCombustible = .Item("fechaCombustible")
                _ltCombustible = .Item("ltCombustible")
                _precioCombustible = .Item("precioCombustible")
                _importe = .Item("importe")
                _idDespachador = .Item("idDespachador")
                _idCargaCombustible = .Item("idCargaCombustible")
                _idChofer = .Item("idChofer")
                _NumViaje = .Item("NumViaje")
            End With
            _Existe = True
        Else
            _Existe = False
        End If



    End Sub

    Sub New(ByVal vidVCExtra As Integer, ByVal vidTipoOSComb As Integer, ByVal vidUnidadTrans As String, ByVal vFechaCrea As DateTime,
ByVal vUserCrea As String, ByVal vidTipoCombustible As Integer, ByVal vfechaCombustible As DateTime, ByVal vltCombustible As Decimal,
ByVal vprecioCombustible As Decimal, ByVal vimporte As Decimal, ByVal vidDespachador As Integer, ByVal vidCargaCombustible As Integer,
ByVal vidChofer As Integer, ByVal vNumViaje As Integer)
        _idVCExtra = vidVCExtra
        _idTipoOSComb = vidTipoOSComb
        _idUnidadTrans = vidUnidadTrans
        _FechaCrea = vFechaCrea
        _UserCrea = vUserCrea
        _idTipoCombustible = vidTipoCombustible
        _fechaCombustible = vfechaCombustible
        _ltCombustible = vltCombustible
        _precioCombustible = vprecioCombustible
        _importe = vimporte
        _idDespachador = vidDespachador
        _idCargaCombustible = vidCargaCombustible
        _idChofer = vidChofer
        _NumViaje = vNumViaje

        _Existe = True
    End Sub
#End Region

#Region "Propiedades"
    Public Property idVCExtra As Integer
        Get
            Return _idVCExtra
        End Get
        Set(ByVal value As Integer)
            _idVCExtra = value
        End Set
    End Property
    Public Property idTipoOSComb As Integer
        Get
            Return _idTipoOSComb
        End Get
        Set(ByVal value As Integer)
            _idTipoOSComb = value
        End Set
    End Property
    Public Property idUnidadTrans As String
        Get
            Return _idUnidadTrans
        End Get
        Set(ByVal value As String)
            _idUnidadTrans = value
        End Set
    End Property
    Public Property FechaCrea As Date
        Get
            Return _FechaCrea
        End Get
        Set(ByVal value As Date)
            _FechaCrea = value
        End Set
    End Property
    Public Property UserCrea As String
        Get
            Return _UserCrea
        End Get
        Set(ByVal value As String)
            _UserCrea = value
        End Set
    End Property
    Public Property idTipoCombustible As Integer
        Get
            Return _idTipoCombustible
        End Get
        Set(ByVal value As Integer)
            _idTipoCombustible = value
        End Set
    End Property
    Public Property fechaCombustible As Date
        Get
            Return _fechaCombustible
        End Get
        Set(ByVal value As Date)
            _fechaCombustible = value
        End Set
    End Property
    Public Property ltCombustible As Decimal
        Get
            Return _ltCombustible
        End Get
        Set(ByVal value As Decimal)
            _ltCombustible = value
        End Set
    End Property
    Public Property precioCombustible As Decimal
        Get
            Return _precioCombustible
        End Get
        Set(ByVal value As Decimal)
            _precioCombustible = value
        End Set
    End Property
    Public Property importe As Decimal
        Get
            Return _importe
        End Get
        Set(ByVal value As Decimal)
            _importe = value
        End Set
    End Property
    Public Property idDespachador As Integer
        Get
            Return _idDespachador
        End Get
        Set(ByVal value As Integer)
            _idDespachador = value
        End Set
    End Property
    Public Property idCargaCombustible As Integer
        Get
            Return _idCargaCombustible
        End Get
        Set(ByVal value As Integer)
            _idCargaCombustible = value
        End Set
    End Property
    Public Property NumViaje As Integer
        Get
            Return _NumViaje
        End Get
        Set(ByVal value As Integer)
            _NumViaje = value
        End Set
    End Property
    Public Property idChofer As Integer
        Get
            Return _idChofer
        End Get
        Set(ByVal value As Integer)
            _idChofer = value
        End Set
    End Property
    Public Property Existe As Boolean
        Get
            Return _Existe
        End Get
        Set(ByVal value As Boolean)
            _Existe = value
        End Set
    End Property
#End Region

#Region "Funciones"

    Public Function UltPrecioCombustible() As Decimal
        Dim Resp As Decimal

        StrSql = "SELECT TOP 1 precio FROM dbo.preciosCombustible ORDER BY fecha DESC"
        DsClase = BD.ExecuteReturn(StrSql)
        If DsClase.Rows.Count > 0 Then
            With DsClase.Rows(0)
                Resp = .Item("precio")
                'Resp = NoNull(.Item(NomEstatus), "D")
            End With
        End If
        Return Resp
    End Function
#End Region
End Class
