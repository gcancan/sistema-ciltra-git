﻿Public Class ProductosExpClass
    Private _CCODIGOP01 As String
    Private _CIDPRODU01 As Integer

    Private _CNOMBREP01 As String
    Private _Precio As Double

    Private _CIDUNIDA01 As Integer
    Private _CABREVIA01 As String
    Private _CNOMBREU01 As String

    Private _CIDUNIDA02 As Integer
    Private _CABREVIA02 As String
    Private _CNOMBREU02 As String

    Private _CSTATUSP01 As Boolean

    Private _Existe As Boolean
    Private _TablaBd As String = "CATPRODUCTOS"

    Dim StrSql As String
    Dim DsComboFox As New DataSet

#Region "Constructor"
    Sub New(ByVal vCCODIGOP01 As String)
        Dim Conexion As New OleDb.OleDbConnection(ConStrFox)
        'Dim DtCol As New DataTable

        StrSql = "SELECT CIDPRODU01,CCODIGOP01, CNOMBREP01,CPRECIO1,PROD.CIDUNIDA01, " & _
        "UNI1.CABREVIA01,UNI1.CNOMBREU01 as CNOMBREU01 ,PROD.CIDUNIDA02,UNI2.CABREVIA01 as CABREVIA02,UNI2.CNOMBREU01 as CNOMBREU02, CSTATUSP01 " & _
        "FROM MGW10005 PROD " & _
        "INNER JOIN MGW10026 UNI1 ON PROD.CIDUNIDA01 = UNI1.CIDUNIDAD " & _
        "INNER JOIN MGW10026 UNI2 ON PROD.CIDUNIDA02 = UNI2.CIDUNIDAD " & _
        "WHERE CCODIGOP01 = '" & vCCODIGOP01 & "'"

        'Dim Adt As New OleDb.OleDbDataAdapter(StrSql, Conexion)
        'Adt.Fill(DtCol, _TablaBd)

        Dim Adt As New OleDb.OleDbDataAdapter(StrSql, Conexion)
        DsComboFox.Clear()
        Adt.Fill(DsComboFox, _TablaBd)

        If Not DsComboFox Is Nothing Then
            _CCODIGOP01 = vCCODIGOP01
            If DsComboFox.Tables(_TablaBd).DefaultView.Count > 0 Then
                'DsComboFox.Tables(dbtabla).DefaultView.Item(0).Item("CIDUNIDA01")
                With DsComboFox.Tables(_TablaBd).DefaultView.Item(0)
                    _CIDPRODU01 = .Item("CIDPRODU01") & ""
                    _CNOMBREP01 = Trim(.Item("CNOMBREP01") & "")
                    _Precio = .Item("CPRECIO1") & ""

                    _CIDUNIDA01 = .Item("CIDUNIDA01") & ""
                    _CABREVIA01 = Trim(.Item("CABREVIA01") & "")
                    _CNOMBREU01 = Trim(.Item("CNOMBREU01") & "")

                    _CIDUNIDA02 = .Item("CIDUNIDA02") & ""
                    _CABREVIA02 = Trim(.Item("CABREVIA02") & "")
                    _CNOMBREU02 = Trim(.Item("CNOMBREU02") & "")

                    _CSTATUSP01 = .Item("CSTATUSP01")
                End With
                _Existe = True
            Else
                _Existe = False
            End If
        End If

      
    End Sub
    Sub New(ByVal vCCODIGOP01 As String, ByVal vCIDPRODU01 As Integer, ByVal vCNOMBREP01 As String,
            ByVal vPrecio As Double, ByVal vCIDUNIDA01 As Integer, ByVal vCABREVIA01 As String,
            ByVal vCIDUNIDA02 As Integer, ByVal vCABREVIA02 As String, ByVal vCNOMBREU01 As String,
            ByVal vCNOMBREU02 As String, ByVal CSTATUSP01 As Boolean)
        _CCODIGOP01 = vCCODIGOP01
        _CIDPRODU01 = vCIDPRODU01
        _CNOMBREP01 = vCNOMBREP01
        _Precio = vPrecio

        _CIDUNIDA01 = vCIDUNIDA01
        _CABREVIA01 = vCABREVIA01
        _CNOMBREU01 = vCNOMBREU01

        _CIDUNIDA02 = vCIDUNIDA02
        _CABREVIA02 = vCABREVIA02
        _CNOMBREU02 = vCNOMBREU02

        _CSTATUSP01 = CSTATUSP01

        _Existe = True
    End Sub
#End Region
#Region "Propiedades"
    Public ReadOnly Property CCODIGOP01 As String
        Get
            Return _CCODIGOP01
        End Get
    End Property
    Public Property CIDPRODU01 As Integer
        Get
            Return _CIDPRODU01
        End Get
        Set(ByVal value As Integer)
            _CIDPRODU01 = value
        End Set
    End Property
    Public Property CNOMBREP01 As String
        Get
            Return _CNOMBREP01
        End Get
        Set(ByVal value As String)
            _CNOMBREP01 = value
        End Set
    End Property
    Public Property Precio As Double
        Get
            Return _Precio
        End Get
        Set(ByVal value As Double)
            _Precio = value
        End Set
    End Property
    Public Property CIDUNIDA01 As Integer
        Get
            Return _CIDUNIDA01
        End Get
        Set(ByVal value As Integer)
            _CIDUNIDA01 = value
        End Set
    End Property
    Public Property CABREVIA01 As String
        Get
            Return _CABREVIA01
        End Get
        Set(ByVal value As String)
            _CABREVIA01 = value
        End Set
    End Property
    Public Property CNOMBREU01 As String
        Get
            Return _CNOMBREU01
        End Get
        Set(ByVal value As String)
            _CNOMBREU01 = value
        End Set
    End Property
    Public Property CIDUNIDA02 As Integer
        Get
            Return _CIDUNIDA02
        End Get
        Set(ByVal value As Integer)
            _CIDUNIDA02 = value
        End Set
    End Property
    Public Property CABREVIA02 As String
        Get
            Return _CABREVIA02
        End Get
        Set(ByVal value As String)
            _CABREVIA02 = value
        End Set
    End Property
    Public Property CNOMBREU02 As String
        Get
            Return _CNOMBREU02
        End Get
        Set(ByVal value As String)
            _CNOMBREU02 = value
        End Set
    End Property

    Public Property CSTATUSP01 As Boolean
        Get
            Return _CSTATUSP01
        End Get
        Set(ByVal value As Boolean)
            _CSTATUSP01 = value
        End Set
    End Property

    Public ReadOnly Property Existe As Boolean
        Get
            Return _Existe
        End Get
    End Property
#End Region
#Region "Funciones"
    'Public Function GetCambios(ByVal vNombreColonia As String, ByVal vCodigoPostal As String, ByVal vPoblacion As PoblacionClass) As String
    '    Dim CadCam As String = ""
    '    If _NombreColonia <> vNombreColonia Then
    '        CadCam += "Nombre Colonia: '" & _NombreColonia & "' Cambia a [" & vNombreColonia & "],"
    '    End If
    '    If _CodigoPostal <> vCodigoPostal Then
    '        CadCam += "Codigo Postal: '" & _CodigoPostal & "' Cambia a [" & vCodigoPostal & "],"
    '    End If
    '    If _Poblacion.idPoblacion <> vPoblacion.idPoblacion Then
    '        CadCam += "Poblacion: '" & _Poblacion.NomPoblacion & "' Cambia a [" & vPoblacion.NomPoblacion & "],"
    '    End If
    '    If CadCam <> "" Then
    '        CadCam = Mid(CadCam, 1, CadCam.Length - 1)
    '    End If
    '    Return CadCam
    'End Function

    Public Sub Guardar(ByVal vCCODIGOP01 As String, ByVal vCIDPRODU01 As Integer, ByVal vCNOMBREP01 As String,
            ByVal vPrecio As Double, ByVal vCIDUNIDA01 As Integer, ByVal vCABREVIA01 As String,
            ByVal vCIDUNIDA02 As Integer, ByVal vCABREVIA02 As String)

        Dim sSql As String = "", _CadValAnt As String = "", _CadValNue As String = ""
        If _Existe Then
            If _CNOMBREP01 <> vCNOMBREP01 Then
                sSql += "CNOMBREP01 = '" & vCNOMBREP01 & "',"
                _CadValAnt += "CNOMBREP01 =" & vCNOMBREP01 & ","
            End If
            If _Precio <> vPrecio Then
                sSql += "Precio = " & vPrecio
                _CadValAnt += "Precio =" & vPrecio & ","
            End If
            If _CIDUNIDA01 <> vCIDUNIDA01 Then
                sSql += "CIDUNIDA01 = " & vCIDUNIDA01
                _CadValAnt += "CIDUNIDA01 =" & vCIDUNIDA01 & ","
            End If
            If _CABREVIA01 <> vCABREVIA01 Then
                sSql += "CABREVIA01 = '" & vCABREVIA01 & "',"
                _CadValAnt += "CABREVIA01 =" & vCABREVIA01 & ","
            End If

            If _CIDUNIDA02 <> vCIDUNIDA02 Then
                sSql += "CIDUNIDA02 = " & vCIDUNIDA02
                _CadValAnt += "CIDUNIDA02 =" & vCIDUNIDA02 & ","
            End If
            If _CABREVIA02 <> vCABREVIA02 Then
                sSql += "CABREVIA02 = '" & vCABREVIA02 & "',"
                _CadValAnt += "CABREVIA02 =" & vCABREVIA02 & ","
            End If

            If sSql <> "" Then
                sSql = Mid(sSql, 1, sSql.Length - 1) 'Para quitar la Coma ','
                _CadValAnt = "CCODIGOP01=" & _CCODIGOP01 & "," & _CadValAnt
                _CadValAnt = Mid(_CadValAnt, 1, _CadValAnt.Length - 1)
                _CadValNue = Replace(sSql, "'", "")
                sSql = "UPDATE " & _TablaBd & " SET " & sSql & " WHERE CCODIGOP01 = '" & _CCODIGOP01 & "'"
                Try
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                    BD.Execute(sSql, True)
                    Audita(_TablaBd, _CadValAnt, _CadValNue, EnTipAccion.Modifica)
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
                Catch ex As Exception
                    BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                    Throw New Exception(ex.Message)
                End Try
            End If
        Else
            sSql = "INSERT INTO " & _TablaBd & "(CIDPRODU01,CCODIGOP01,CNOMBREP01,Precio,CIDUNIDA01,CABREVIA01,CIDUNIDA02,CABREVIA02) VALUES(" &
                _CIDPRODU01 & ",'" & _CCODIGOP01 & "','" & _CNOMBREP01 & "'," & _Precio & "," & _CIDUNIDA01 & ",'" & _CABREVIA01 & "'," & _CIDUNIDA02 & ",'" & _CABREVIA02 & "')"

            _CadValNue = "CCODIGOP01=" & _CCODIGOP01 & ",CIDPRODU01=" & _CIDPRODU01 & ",CNOMBREP01=" & _CNOMBREP01 & ",Precio=" & _Precio & _
            ",CIDUNIDA01 = " & _CIDUNIDA01 & ",CABREVIA01 = " & _CABREVIA01 & ",CIDUNIDA02 = " & _CIDUNIDA02 & ",CABREVIA02 = " & _CABREVIA02
            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, "", _CadValNue, EnTipAccion.Inserta)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
        End If
    End Sub
    Public Sub Eliminar()
        If _Existe Then
            Dim sSql As String = "", _CadValAnt As String = ""
            '_CadValAnt = "CCODIGOP01=" & _CCODIGOP01 & " , NomColonia =" & _NombreColonia & ", CodPostal=" & _CodigoPostal & " ,idPoblacion=" & _Poblacion.idPoblacion
            _CadValAnt = "CCODIGOP01=" & _CCODIGOP01 & ",CIDPRODU01=" & _CIDPRODU01 & ",CNOMBREP01=" & _CNOMBREP01 & ",Precio=" & _Precio & _
            ",CIDUNIDA01 = " & _CIDUNIDA01 & ",CABREVIA01 = " & _CABREVIA01 & ",CIDUNIDA02 = " & _CIDUNIDA02 & ",CABREVIA02 = " & _CABREVIA02

            sSql = "DELETE FROM " & _TablaBd & " WHERE CCODIGOP01 = '" & _CCODIGOP01 & "'"
            Try
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Inicia)
                BD.Execute(sSql, True)
                Audita(_TablaBd, _CadValAnt, "", EnTipAccion.Elimina)
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Finaliza)
            Catch ex As Exception
                BD.Transaccion(CapaDatos.UtilSQL.EnTransaccionEstado.Deshace)
                Throw New Exception(ex.Message)
            End Try
            _Existe = False
        Else
            'se supone que nunca debe entrar aqui. . .
            MessageBox.Show("El Producto con Id: " & _CCODIGOP01 & " No existe por lo tanto no se puede Eliminar!!!",
                            "No se puede eliminar el Producto porque no Existe", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub
#End Region
End Class