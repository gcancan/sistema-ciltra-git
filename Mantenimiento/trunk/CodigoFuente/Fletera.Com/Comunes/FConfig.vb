﻿Public Class FConfig

    Private Sub FConfig_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Process.GetCurrentProcess.ProcessName
        'txtNomServ.Text = GetSetting("SoporteTec", "ServerSQL", "Nombre del servidor", "") & ""
        'txtBD.Text = GetSetting("SoporteTec", "BaseSQL", "Nombre de la Base de Datos", "") & ""
        Dim arrServ() As String = My.Settings.Servidores.Split({"|"}, StringSplitOptions.RemoveEmptyEntries)
        For i As Integer = 0 To arrServ.Length - 1
            txtNomServ.Items.Add(arrServ(i))
        Next
        Dim arrBd() As String = My.Settings.BDS.Split({"|"}, StringSplitOptions.RemoveEmptyEntries)
        For i As Integer = 0 To arrBd.Length - 1
            txtBD.Items.Add(arrBd(i))
        Next
        txtNomServ.Text = GetSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Nombre del servidor", "") & ""
        'SaveSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Nombre de la Base de Datos", txtBD.Text)
        txtBD.Text = GetSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Nombre de la Base de Datos", "") & ""

    End Sub

    Private Sub BtnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        If txtNomServ.Text.Trim = "" Then
            MessageBox.Show("No se puede quedar vacio el Nombre del Servidor, Favor de verificarlo!!!",
                            "Error de campo Vacio", MessageBoxButtons.OK, MessageBoxIcon.Error)
            txtNomServ.Focus()
            Exit Sub
        End If
        If txtNomServ.Text.Trim = "" Then
            MessageBox.Show("No se puede quedar vacio la Base de Datos del Servidor, Favor de verificarlo!!!",
                            "Error de campo Vacio", MessageBoxButtons.OK, MessageBoxIcon.Error)
            txtBD.Focus()
            Exit Sub
        End If
        If Not txtNomServ.Items.Contains(txtNomServ.Text) Then
            txtNomServ.Items.Add(txtNomServ.Text)
        End If
        Dim CadTmp As String = ""
        For i As Integer = 0 To txtNomServ.Items.Count - 1
            CadTmp += txtNomServ.Items(i) & "|"
        Next
        My.Settings.Servidores = CadTmp
        If Not txtBD.Items.Contains(txtBD.Text) Then
            txtBD.Items.Add(txtBD.Text)
        End If
        CadTmp = ""
        For i As Integer = 0 To txtBD.Items.Count - 1
            CadTmp += txtBD.Items(i) & "|"
        Next
        My.Settings.BDS = CadTmp
        My.Settings.Save()

        Dim ObjReg As New ToolRegistro

        'PARA GUARDAR EN EL LOCALMACHINE
        'GrabaRegistroLOCALMACHINE("ServerSQL", "Nombre del servidor", txtNomServ.Text, KeyPathLM)
        'sNomServidor = ObjReg.LeeLlaveRegistro(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Nombre del servidor", KeyPathLM)

        'GrabaRegistroLOCALMACHINE("ServerSQL", "Nombre de la Base de Datos", txtBD.Text, KeyPathLM)
        'sNomBd = ObjReg.LeeLlaveRegistro(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Nombre de la Base de Datos", KeyPathLM)

        'RUTA DEL REGISTRO POR DEFAULT
        SaveSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Nombre del servidor", txtNomServ.Text)
        sNomServidor = GetSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Nombre del servidor", "") & ""

        SaveSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Nombre de la Base de Datos", txtBD.Text)
        sNomBd = GetSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Nombre de la Base de Datos", "") & ""


        'SaveSetting("SoporteTec", "ServerSQL", "Nombre del servidor", txtNomServ.Text)
        'SaveSetting("SoporteTec", "BaseSQL", "Nombre de la Base de Datos", txtBD.Text)
        MessageBox.Show("Datos guardados con exito!!!", "Accion terminada", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Me.Close()
    End Sub

    Private Sub txtNomServ_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs)
        TryCast(sender, TextBox).SelectAll()
    End Sub
    Private Sub txtBD_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Asc(e.KeyChar) = 13 Then
            SendKeys.Send("{TAB}")
            e.Handled = True
        End If
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

    Private Sub txtNomServ_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles _
                txtNomServ.KeyPress, txtBD.KeyPress
        e.KeyChar = UCase(e.KeyChar)
    End Sub

    Private Sub txtNomServ_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNomServ.SelectedIndexChanged

    End Sub
End Class