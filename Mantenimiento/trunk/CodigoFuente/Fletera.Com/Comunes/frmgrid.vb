'***************************************
'*  FORMA REALIZADA POR : 
'*        FECHA CREACION:
'* FORMA MODIFICADA POR : 
'*    FECHA MODIFICACION: 
'********************************************
Option Explicit On 
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Public Class frmgrid
    Inherits System.Windows.Forms.Form
    Public hit As System.Windows.Forms.DataGrid.HitTestInfo
    Public Valor As String
    Public Valor2 As String
    Dim vCampo1, vDescCampo1, vCampo2, vDescCampo2, vCampo3, vDescCampo3, vCampo4, vDescCampo4, vCampo5, vDescCampo5, vCampo6, vDescCampo6 As String
    Dim vSizeCampo1, vSizeCampo2, vSizeCampo3, vSizeCampo4, vSizeCampo5, vSizeCampo6 As Int16
    Dim vTipoCampo1, vTipoCampo2, vTipoCampo3, vTipoCampo4, vTipoCampo5, vTipoCampo6 As TipoDato
    Dim vNombreGrid, vNOM_TABLA, vstrSql, vConStr As String
    Dim vTipoConexion As TipoConexion
    '3/ABRIL/2014
    Dim vNomCampoDefault As String
    '22/SEPTIEMBRE/2016
    Dim vGridDefault As Boolean
    Friend WithEvents rbtnCampo6 As System.Windows.Forms.RadioButton

    Protected DS As New DataSet
    'Protected vDsEnvio As New DataSet

    Dim vOtroIndiceValor As Integer


#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents grid As MyDataGrid.MyDataGrid
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents rbtnCampo1 As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnCampo2 As System.Windows.Forms.RadioButton
    Friend WithEvents txtFilter As System.Windows.Forms.TextBox
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnFilter As System.Windows.Forms.Button
    Friend WithEvents rbtnCampo5 As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnCampo4 As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnCampo3 As System.Windows.Forms.RadioButton
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmgrid))
        Me.grid = New MyDataGrid.MyDataGrid()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.rbtnCampo6 = New System.Windows.Forms.RadioButton()
        Me.rbtnCampo5 = New System.Windows.Forms.RadioButton()
        Me.rbtnCampo4 = New System.Windows.Forms.RadioButton()
        Me.rbtnCampo3 = New System.Windows.Forms.RadioButton()
        Me.rbtnCampo1 = New System.Windows.Forms.RadioButton()
        Me.txtFilter = New System.Windows.Forms.TextBox()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnFilter = New System.Windows.Forms.Button()
        Me.rbtnCampo2 = New System.Windows.Forms.RadioButton()
        CType(Me.grid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'grid
        '
        Me.grid.DataMember = ""
        Me.grid.HeaderForeColor = System.Drawing.SystemColors.ControlText
        resources.ApplyResources(Me.grid, "grid")
        Me.grid.Name = "grid"
        Me.grid.ReadOnly = True
        Me.grid.TabStop = False
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        resources.ApplyResources(Me.Panel1, "Panel1")
        Me.Panel1.Name = "Panel1"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.rbtnCampo6)
        Me.GroupBox1.Controls.Add(Me.rbtnCampo5)
        Me.GroupBox1.Controls.Add(Me.rbtnCampo4)
        Me.GroupBox1.Controls.Add(Me.rbtnCampo3)
        Me.GroupBox1.Controls.Add(Me.rbtnCampo1)
        Me.GroupBox1.Controls.Add(Me.txtFilter)
        Me.GroupBox1.Controls.Add(Me.btnSalir)
        Me.GroupBox1.Controls.Add(Me.btnFilter)
        Me.GroupBox1.Controls.Add(Me.rbtnCampo2)
        resources.ApplyResources(Me.GroupBox1, "GroupBox1")
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.TabStop = False
        '
        'rbtnCampo6
        '
        resources.ApplyResources(Me.rbtnCampo6, "rbtnCampo6")
        Me.rbtnCampo6.Name = "rbtnCampo6"
        '
        'rbtnCampo5
        '
        resources.ApplyResources(Me.rbtnCampo5, "rbtnCampo5")
        Me.rbtnCampo5.Name = "rbtnCampo5"
        '
        'rbtnCampo4
        '
        resources.ApplyResources(Me.rbtnCampo4, "rbtnCampo4")
        Me.rbtnCampo4.Name = "rbtnCampo4"
        '
        'rbtnCampo3
        '
        resources.ApplyResources(Me.rbtnCampo3, "rbtnCampo3")
        Me.rbtnCampo3.Name = "rbtnCampo3"
        '
        'rbtnCampo1
        '
        Me.rbtnCampo1.Checked = True
        resources.ApplyResources(Me.rbtnCampo1, "rbtnCampo1")
        Me.rbtnCampo1.Name = "rbtnCampo1"
        Me.rbtnCampo1.TabStop = True
        '
        'txtFilter
        '
        Me.txtFilter.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        resources.ApplyResources(Me.txtFilter, "txtFilter")
        Me.txtFilter.Name = "txtFilter"
        '
        'btnSalir
        '
        Me.btnSalir.DialogResult = System.Windows.Forms.DialogResult.Cancel
        resources.ApplyResources(Me.btnSalir, "btnSalir")
        Me.btnSalir.Name = "btnSalir"
        '
        'btnFilter
        '
        resources.ApplyResources(Me.btnFilter, "btnFilter")
        Me.btnFilter.Name = "btnFilter"
        '
        'rbtnCampo2
        '
        resources.ApplyResources(Me.rbtnCampo2, "rbtnCampo2")
        Me.rbtnCampo2.Name = "rbtnCampo2"
        '
        'frmgrid
        '
        resources.ApplyResources(Me, "$this")
        Me.CancelButton = Me.btnSalir
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.grid)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "frmgrid"
        CType(Me.grid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Public Sub ConfiguraGrid(ByVal StrSQl As String, ByVal ConStr As String, ByVal TipoConexion As TipoConexion, ByVal NombreGrid As String, ByVal NOM_TABLA As String, _
    ByVal Campo1 As String, ByVal DescCampo1 As String, ByVal SizeCampo1 As Int16, ByVal TipoCampo1 As TipoDato, _
    ByVal Campo2 As String, ByVal DescCampo2 As String, ByVal SizeCampo2 As Int16, ByVal TipoCampo2 As TipoDato, _
    Optional ByVal Campo3 As String = "", Optional ByVal DescCampo3 As String = "", Optional ByVal SizeCampo3 As Int16 = 0, Optional ByVal TipoCampo3 As TipoDato = TipoDato.TdCadena, _
    Optional ByVal Campo4 As String = "", Optional ByVal DescCampo4 As String = "", Optional ByVal SizeCampo4 As Int16 = 0, Optional ByVal TipoCampo4 As TipoDato = TipoDato.TdCadena, _
    Optional ByVal Campo5 As String = "", Optional ByVal DescCampo5 As String = "", Optional ByVal SizeCampo5 As Int16 = 0, Optional ByVal TipoCampo5 As TipoDato = TipoDato.TdCadena, _
    Optional ByVal Campo6 As String = "", Optional ByVal DescCampo6 As String = "", Optional ByVal SizeCampo6 As Int16 = 0, Optional ByVal TipoCampo6 As TipoDato = TipoDato.TdCadena, _
    Optional ByVal DsEnvio As DataSet = Nothing, Optional ByVal NomCampoDefault As String = "Campo1", _
    Optional ByVal OtroIndiceValor As Integer = 0, Optional ByVal GridDefault As Boolean = False)
        'Campo 1
        vCampo1 = Campo1
        vDescCampo1 = DescCampo1
        vSizeCampo1 = SizeCampo1
        vTipoCampo1 = TipoCampo1

        'Campo 2
        vCampo2 = Campo2
        vDescCampo2 = DescCampo2
        vSizeCampo2 = SizeCampo2
        vTipoCampo2 = TipoCampo2

        'Campo 3
        vCampo3 = Campo3
        vDescCampo3 = DescCampo3
        vSizeCampo3 = SizeCampo3
        vTipoCampo3 = TipoCampo3

        'Campo 4
        vCampo4 = Campo4
        vDescCampo4 = DescCampo4
        vSizeCampo4 = SizeCampo4
        vTipoCampo4 = TipoCampo4

        'Campo 5
        vCampo5 = Campo5
        vDescCampo5 = DescCampo5
        vSizeCampo5 = SizeCampo5
        vTipoCampo5 = TipoCampo5

        'Campo 6
        vCampo6 = Campo6
        vDescCampo6 = DescCampo6
        vSizeCampo6 = SizeCampo6
        vTipoCampo6 = TipoCampo6

        'Otros
        vNombreGrid = NombreGrid
        vNOM_TABLA = NOM_TABLA
        vstrSql = StrSQl
        vConStr = ConStr
        vTipoConexion = TipoConexion
        'DS = DsEnvio
        If Not DsEnvio Is Nothing Then
            If DsEnvio.Tables(NOM_TABLA).DefaultView.Count > 0 Then
                DS = DsEnvio
            End If
            'Else
            '    DS.
        End If
        vNomCampoDefault = NomCampoDefault
        vGridDefault = GridDefault

        'OTRO VALOR
        vOtroIndiceValor = OtroIndiceValor

    End Sub



    Private Function setToDoListGridHeader(ByVal dg As DataGrid) As DataGrid

        Dim gs As New DataGridTableStyle
        gs.MappingName = vNOM_TABLA

        'DataGridTextBoxColumn
        Dim Col1 As New ColoredTextBoxColumn
        'Dim Col1 As New DataGridTextBoxColumn
        Col1.TextBox.Enabled = False
        Col1.HeaderText = vDescCampo1
        Col1.MappingName = vCampo1
        Col1.Width = vSizeCampo1
        If CType(vTipoCampo1, TipoDato) = TipoDato.TdNumerico Then
            Col1.Alignment = HorizontalAlignment.Right
        ElseIf CType(vTipoCampo1, TipoDato) = TipoDato.TdFecha Then
            Col1.Format = "d"
        ElseIf CType(vTipoCampo1, TipoDato) = TipoDato.TdFechaHora Then
            Col1.Format = "g"
        End If

        gs.GridColumnStyles.Add(Col1)

        Dim Col2 As New ColoredTextBoxColumn
        'Dim Col2 As New DataGridTextBoxColumn
        Col2.TextBox.Enabled = False
        Col2.HeaderText = vDescCampo2
        Col2.MappingName = vCampo2
        Col2.Width = vSizeCampo2
        Col2.TextBox.Visible = True
        If CType(vTipoCampo2, TipoDato) = TipoDato.TdNumerico Then
            Col2.Alignment = HorizontalAlignment.Right
        ElseIf CType(vTipoCampo2, TipoDato) = TipoDato.TdFecha Then
            Col2.Format = "d"
        ElseIf CType(vTipoCampo2, TipoDato) = TipoDato.TdFechaHora Then
            Col2.Format = "g"
        End If

        gs.GridColumnStyles.Add(Col2)

        If vSizeCampo3 > 0 Then
            Dim Col3 As New ColoredTextBoxColumn
            'Dim Col3 As New DataGridTextBoxColumn
            Col3.TextBox.Enabled = False
            Col3.HeaderText = vDescCampo3
            Col3.MappingName = vCampo3
            Col3.Width = vSizeCampo3
            Col3.TextBox.Visible = True
            If CType(vTipoCampo3, TipoDato) = TipoDato.TdNumerico Then
                Col3.Alignment = HorizontalAlignment.Right
            ElseIf CType(vTipoCampo3, TipoDato) = TipoDato.TdFecha Then
                Col3.Format = "d"
            ElseIf CType(vTipoCampo3, TipoDato) = TipoDato.TdFechaHora Then
                Col3.Format = "g"
            End If

            gs.GridColumnStyles.Add(Col3)
        End If

        If vSizeCampo4 > 0 Then
            Dim Col4 As New ColoredTextBoxColumn
            'Dim Col4 As New DataGridTextBoxColumn
            Col4.TextBox.Enabled = False
            Col4.HeaderText = vDescCampo4
            Col4.MappingName = vCampo4
            Col4.Width = vSizeCampo4
            Col4.TextBox.Visible = True
            If CType(vTipoCampo4, TipoDato) = TipoDato.TdNumerico Then
                Col4.Alignment = HorizontalAlignment.Right
            ElseIf CType(vTipoCampo4, TipoDato) = TipoDato.TdFecha Then
                Col4.Format = "d"
            ElseIf CType(vTipoCampo4, TipoDato) = TipoDato.TdFechaHora Then
                Col4.Format = "g"
            End If

            gs.GridColumnStyles.Add(Col4)
        End If

        If vSizeCampo5 > 0 Then
            Dim Col5 As New ColoredTextBoxColumn
            'Dim Col5 As New DataGridTextBoxColumn
            Col5.TextBox.Enabled = False
            Col5.HeaderText = vDescCampo5
            Col5.MappingName = vCampo5
            Col5.Width = vSizeCampo5
            Col5.TextBox.Visible = True
            If CType(vTipoCampo5, TipoDato) = TipoDato.TdNumerico Then
                Col5.Alignment = HorizontalAlignment.Right
            ElseIf CType(vTipoCampo5, TipoDato) = TipoDato.TdFecha Then
                Col5.Format = "d"
            ElseIf CType(vTipoCampo5, TipoDato) = TipoDato.TdFechaHora Then
                Col5.Format = "g"
            End If
            gs.GridColumnStyles.Add(Col5)
        End If

        If vSizeCampo6 > 0 Then
            Dim Col6 As New ColoredTextBoxColumn
            'Dim Col6 As New DataGridTextBoxColumn
            Col6.TextBox.Enabled = False
            Col6.HeaderText = vDescCampo6
            Col6.MappingName = vCampo6
            Col6.Width = vSizeCampo6
            Col6.TextBox.Visible = True
            'If TipoCampo5 =
            If CType(vTipoCampo6, TipoDato) = TipoDato.TdNumerico Then
                Col6.Alignment = HorizontalAlignment.Right
            ElseIf CType(vTipoCampo6, TipoDato) = TipoDato.TdFecha Then
                Col6.Format = "d"
            ElseIf CType(vTipoCampo6, TipoDato) = TipoDato.TdFechaHora Then
                Col6.Format = "g"
            End If
            gs.GridColumnStyles.Add(Col6)
        End If

        dg.TableStyles.Add(gs)
        Return dg
    End Function
    Private Sub CampoDefault(ByVal Campo As String)
        Select Case UCase(Campo)
            Case UCase("Campo1")
                rbtnCampo1.Checked = True
            Case UCase("Campo2")
                rbtnCampo2.Checked = True
            Case UCase("Campo3")
                rbtnCampo3.Checked = True
            Case UCase("Campo4")
                rbtnCampo4.Checked = True
            Case UCase("Campo5")
                rbtnCampo5.Checked = True
            Case UCase("Campo6")
                rbtnCampo6.Checked = True

        End Select

    End Sub

    Private Sub frmgrid_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try

            If Not DS Is Nothing Then
                'If DS.Tables(vNOM_TABLA).DefaultView.Count > 0 Then
                If DS.Tables.Count() > 0 Then
                    GroupBox1.Focus()
                    grid = setToDoListGridHeader(grid)
                    grid.DataSource = DS.Tables(vNOM_TABLA)
                    grid.Visible = True
                    txtFilter.Focus()
                Else
                    CargarGrid()
                End If
            Else
                CargarGrid()
            End If

            CargaFiltros()

           



            Me.Width = vSizeCampo1 + vSizeCampo2 + vSizeCampo3 + vSizeCampo4 + vSizeCampo5 + vSizeCampo6 + 100
            grid.Width = vSizeCampo1 + vSizeCampo2 + vSizeCampo3 + vSizeCampo4 + vSizeCampo5 + vSizeCampo6 + 50
            Panel1.Width = vSizeCampo1 + vSizeCampo2 + vSizeCampo3 + vSizeCampo4 + vSizeCampo5 + vSizeCampo6 + 66
            Me.Text = vNombreGrid

            If vGridDefault Then
                grid.Focus()
            Else
                CampoDefault(vNomCampoDefault)
                txtFilter.Focus()
            End If


            '

        Catch Ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "No se encontro la informacion " & vbCrLf & "Error: " & Ex.Message, MsgBoxStyle.Information, Me.Text & " - SQL Server: ")
            HabilitaCampos(False)
        End Try
    End Sub

    Sub HabilitaCampos(ByVal MiValor As Boolean)
        btnFilter.Enabled = MiValor
        txtFilter.Enabled = MiValor
        rbtnCampo1.Enabled = MiValor
        rbtnCampo2.Enabled = MiValor
        rbtnCampo3.Enabled = MiValor
        rbtnCampo4.Enabled = MiValor
        rbtnCampo5.Enabled = MiValor
        rbtnCampo6.Enabled = MiValor
        grid.Enabled = MiValor
        btnSalir.Focus()
        btnSalir.Select()
    End Sub

    Private Sub txtFilter_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtFilter.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            e.Handled = True
            Dim ex As System.EventArgs = Nothing
            btnFilter_Click(sender, ex)
        End If
    End Sub

    Private Sub txtFilter_MouseDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnFilter.MouseDown
        txtFilter.SelectAll()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub frmgrid_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.TextChanged
        If sender.Text = "Enter Capturado" Then
            sender.Text = vNombreGrid
            Try
                If DS Is Nothing Then
                    Valor = ""
                ElseIf DS.Tables(vNOM_TABLA) Is Nothing Then
                    Valor = ""
                Else
                    Valor = Trim(grid.Item(grid.CurrentCell.RowNumber, 0))
                    If vOtroIndiceValor > 0 Then
                        Valor2 = Trim(grid.Item(grid.CurrentCell.RowNumber, vOtroIndiceValor))
                    End If
                End If
            Catch ex As Exception
                Valor = ""
            End Try
            Me.Close()
        End If
    End Sub

    Private Sub btnFilter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFilter.Click
        Try
            If txtFilter.Text = "" Then
                '3/ABRIL/2014
                DS.Tables(vNOM_TABLA).DefaultView.RowFilter = Nothing
                grid.Focus()
                Exit Try
            End If


            If DS Is Nothing Then
                Exit Sub
            ElseIf DS.Tables(vNOM_TABLA) Is Nothing Then
                Exit Sub
            End If

            'Const messagebox As String = "Filter"
            Debug.Assert(Not DS.Tables(vNOM_TABLA) Is Nothing, "No se cargaron los datos")

            With DS.Tables(vNOM_TABLA)
                ' FILTRO X CAMPO1
                If rbtnCampo1.Checked Then
                    '3/ABRIL/2014
                    .DefaultView.RowFilter = Nothing
                    If CType(vTipoCampo1, TipoDato) = TipoDato.TdCadena Or CType(vTipoCampo1, TipoDato) = TipoDato.TdFecha Then
                        '.DefaultView.RowFilter = vCampo1 & " Like '" & txtFilter.Text & "'"
                        '.DefaultView.RowFilter = vCampo1 & " = '" & txtFilter.Text & "'"
                        .DefaultView.RowFilter = "" & vCampo1 & " Like '*" & txtFilter.Text & "*'"
                    ElseIf CType(vTipoCampo1, TipoDato) = TipoDato.TdBoolean Or CType(vTipoCampo1, TipoDato) = TipoDato.TdNumerico Then
                        If txtFilter.Text = "" Then
                            .DefaultView.RowFilter = Nothing
                        Else
                            .DefaultView.RowFilter = "" & vCampo1 & " = " & txtFilter.Text
                        End If
                    End If
                    'grid.Focus()
                    'Exit Sub
                End If
                ' FILTRO X CAMPO2
                If rbtnCampo2.Checked Then
                    If CType(vTipoCampo2, TipoDato) = TipoDato.TdCadena Or CType(vTipoCampo2, TipoDato) = TipoDato.TdFecha Then
                        '.DefaultView.RowFilter = "" & vCampo2 & " Like '%" & txtFilter.Text & "%'"
                        .DefaultView.RowFilter = "" & vCampo2 & " Like '*" & txtFilter.Text & "*'"
                    ElseIf CType(vTipoCampo2, TipoDato) = TipoDato.TdBoolean Or CType(vTipoCampo2, TipoDato) = TipoDato.TdNumerico Then
                        If txtFilter.Text = "" Then
                            .DefaultView.RowFilter = Nothing
                        Else
                            .DefaultView.RowFilter = "" & vCampo2 & " = " & txtFilter.Text
                        End If
                    End If
                End If
                ' FILTRO X CAMPO3
                If vCampo3 <> "" Then
                    If rbtnCampo3.Checked Then
                        If CType(vTipoCampo3, TipoDato) = TipoDato.TdCadena Or CType(vTipoCampo3, TipoDato) = TipoDato.TdFecha Then
                            '.DefaultView.RowFilter = "" & vCampo3 & " Like '%" & txtFilter.Text & "%'"
                            .DefaultView.RowFilter = "" & vCampo3 & " Like '*" & txtFilter.Text & "*'"
                        ElseIf CType(vTipoCampo3, TipoDato) = TipoDato.TdBoolean Or CType(vTipoCampo3, TipoDato) = TipoDato.TdNumerico Then
                            If txtFilter.Text = "" Then
                                .DefaultView.RowFilter = Nothing
                            Else
                                .DefaultView.RowFilter = "" & vCampo3 & " = " & txtFilter.Text
                            End If
                        End If
                    End If
                End If
                ' FILTRO X CAMPO4
                If vCampo4 <> "" Then
                    If rbtnCampo4.Checked Then
                        If CType(vTipoCampo4, TipoDato) = TipoDato.TdCadena Or CType(vTipoCampo4, TipoDato) = TipoDato.TdFecha Then
                            '.DefaultView.RowFilter = "" & vCampo4 & " Like '%" & txtFilter.Text & "%'"
                            .DefaultView.RowFilter = "" & vCampo4 & " Like '*" & txtFilter.Text & "*'"
                        ElseIf CType(vTipoCampo4, TipoDato) = TipoDato.TdBoolean Or CType(vTipoCampo4, TipoDato) = TipoDato.TdNumerico Then
                            If txtFilter.Text = "" Then
                                .DefaultView.RowFilter = Nothing
                            Else
                                .DefaultView.RowFilter = "" & vCampo4 & " = " & txtFilter.Text
                            End If
                        End If
                    End If
                End If
                ' FILTRO X CAMPO5
                If vCampo5 <> "" Then
                    If rbtnCampo5.Checked Then
                        If CType(vTipoCampo5, TipoDato) = TipoDato.TdCadena Or CType(vTipoCampo5, TipoDato) = TipoDato.TdFecha Then
                            '.DefaultView.RowFilter = "" & vCampo5 & " Like '%" & txtFilter.Text & "%'"
                            .DefaultView.RowFilter = "" & vCampo5 & " Like '*" & txtFilter.Text & "*'"
                        ElseIf CType(vTipoCampo5, TipoDato) = TipoDato.TdBoolean Or CType(vTipoCampo5, TipoDato) = TipoDato.TdNumerico Then
                            If txtFilter.Text = "" Then
                                .DefaultView.RowFilter = Nothing
                            Else
                                .DefaultView.RowFilter = "" & vCampo5 & " = " & txtFilter.Text
                            End If
                        End If
                    End If
                End If
                ' FILTRO X CAMPO 6
                If vCampo6 <> "" Then
                    If rbtnCampo6.Checked Then
                        If CType(vTipoCampo6, TipoDato) = TipoDato.TdCadena Or CType(vTipoCampo6, TipoDato) = TipoDato.TdFecha Then
                            '.DefaultView.RowFilter = "" & vCampo6 & " Like '%" & txtFilter.Text & "%'"
                            .DefaultView.RowFilter = "" & vCampo6 & " Like '*" & txtFilter.Text & "*'"
                        ElseIf CType(vTipoCampo6, TipoDato) = TipoDato.TdBoolean Or CType(vTipoCampo6, TipoDato) = TipoDato.TdNumerico Then
                            If txtFilter.Text = "" Then
                                .DefaultView.RowFilter = Nothing
                            Else
                                .DefaultView.RowFilter = "" & vCampo6 & " = " & txtFilter.Text
                            End If
                        End If
                    End If
                End If
                If .DefaultView.Count = 0 Then
                    grid.DataSource = .DefaultView
                    MsgBox("!!No hay concidencias!!", MsgBoxStyle.Exclamation, Me.Text)
                    txtFilter.Focus()
                    txtFilter.SelectAll()
                Else
                    grid.DataSource = .DefaultView
                    grid.Focus()
                End If

            End With
            'filtro x Campo2

        Catch ex As Exception
            MsgBox("No se logro realizar el filtro solicitado" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, Me.Text)
        End Try
    End Sub

    Private Sub rbtnCampo1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbtnCampo1.Click
        txtFilter.Focus()
        txtFilter.SelectAll()
    End Sub

    Private Sub rbtnCampo2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbtnCampo2.Click
        txtFilter.Focus()
        txtFilter.SelectAll()
    End Sub

    Private Sub rbtnCampo3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbtnCampo3.Click
        txtFilter.Focus()
        txtFilter.SelectAll()
    End Sub

    Private Sub rbtnCampo4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbtnCampo4.Click
        txtFilter.Focus()
        txtFilter.SelectAll()
    End Sub

    Private Sub rbtnCampo5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbtnCampo5.Click
        txtFilter.Focus()
        txtFilter.SelectAll()
    End Sub

    Private Sub rbtnCampo6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbtnCampo6.Click
        txtFilter.Focus()
        txtFilter.SelectAll()
    End Sub

    Private Sub grdColonia_MouseDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles grid.MouseDown
        Try
            If DS Is Nothing Then
                Exit Sub
            ElseIf DS.Tables(vNOM_TABLA) Is Nothing Then
                Exit Sub
            Else
                Dim myGrid As DataGrid = CType(sender, DataGrid)
                'Dim str As String
                If myGrid.CurrentRowIndex < 0 Then Exit Sub
                'If IIf(hit.Row.GetType.ToString Is "Nothing", 0, hit.Row) Then Exit Sub
                hit = myGrid.HitTest(e.X, e.Y)
                Select Case hit.Type
                    Case System.Windows.Forms.DataGrid.HitTestType.Cell
                        If hit.Column = 0 Then
                            Valor = myGrid.Item(hit.Row, 0)
                            If vOtroIndiceValor > 0 Then
                                Valor2 = myGrid.Item(hit.Row, vOtroIndiceValor)
                            End If
                            Me.Close()
                        End If
                End Select
            End If
        Catch ex As Exception
        End Try
    End Sub
    '
    Private Sub CargarGrid()
        Dim obj As New CapaNegocio.Tablas
        Dim TipoCon As String = ""
        Try
            DS.Clear()
            'DS = obj.CargaDsPorClave(Inicio.ConStrProg, "PROGRESS", "JurCatNotarios", , "nNumNotario")
            If CType(vTipoConexion, TipoConexion) = TipoConexion.TcSQL Then
                TipoCon = "SQL"
            ElseIf CType(vTipoConexion, TipoConexion) = TipoConexion.TcProgress Then
                TipoCon = "PROGRESS"
            ElseIf CType(vTipoConexion, TipoConexion) = TipoConexion.TcFox Then
                TipoCon = "FOX"

            End If
            DS = obj.CargaDsPorSentencia(vConStr, TipoCon, vstrSql, vNOM_TABLA, Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS)

            If Not DS Is Nothing Then
                If DS.Tables(vNOM_TABLA).DefaultView.Count > 0 Then
                    GroupBox1.Focus()
                    grid = setToDoListGridHeader(grid)
                    grid.DataSource = DS.Tables(vNOM_TABLA)
                    grid.Visible = True
                    txtFilter.Focus()
                Else
                    'sin datos
                End If
            Else
                'No exiuste
                grid.DataSource = Nothing
            End If
            If Not obj Is Nothing Then obj = Nothing
        Catch ex As Exception
            If Not obj Is Nothing Then obj = Nothing
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, Me.Text)
        End Try
    End Sub

    Private Sub CargaFiltros()
        rbtnCampo1.Text = rbtnCampo1.Text & vDescCampo1
        rbtnCampo2.Text = rbtnCampo2.Text & vDescCampo2

        If vCampo3 <> "" Then
            rbtnCampo3.Visible = True
            rbtnCampo3.Text = rbtnCampo3.Text & vDescCampo3
        End If

        If vCampo4 <> "" Then
            rbtnCampo4.Visible = True
            rbtnCampo4.Text = rbtnCampo4.Text & vDescCampo4
        End If

        If vCampo5 <> "" Then
            rbtnCampo5.Visible = True
            rbtnCampo5.Text = rbtnCampo5.Text & vDescCampo5
        End If

        If vCampo6 <> "" Then
            rbtnCampo6.Visible = True
            rbtnCampo6.Text = rbtnCampo6.Text & vDescCampo5
        End If
    End Sub

    Private Sub txtFilter_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFilter.Leave
        grid.Focus()
    End Sub

    Private Sub txtFilter_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtFilter.TextChanged
        'DsLista.Tables(TablaBd).DefaultView.RowFilter = Nothing
        'DsLista.Tables(TablaBd).DefaultView.RowFilter = "NomDescripcion Like '*" & txtMenuBusca.Text & "*'"
        Dim NumCampo As Int16 = 0

        If Not DS Is Nothing Then

            '4/ABRIL/2014
            DS.Tables(vNOM_TABLA).DefaultView.RowFilter = Nothing
            If DS.Tables(vNOM_TABLA).DefaultView.Count > 0 Then


                With DS.Tables(vNOM_TABLA)
                    .DefaultView.RowFilter = Nothing

                    'NumCampo = Val(Microsoft.VisualBasic.Mid(rbtnCampo1.Name, 10, 1))
                    If rbtnCampo1.Checked Then
                        If CType(vTipoCampo1, TipoDato) = TipoDato.TdCadena Or CType(vTipoCampo1, TipoDato) = TipoDato.TdFecha Then
                            '.DefaultView.RowFilter = vCampo1 & " Like '" & txtFilter.Text & "'"
                            '.DefaultView.RowFilter = vCampo1 & " = '" & txtFilter.Text & "'"
                            .DefaultView.RowFilter = "" & vCampo1 & " Like '*" & txtFilter.Text & "*'"
                        ElseIf CType(vTipoCampo1, TipoDato) = TipoDato.TdBoolean Or CType(vTipoCampo1, TipoDato) = TipoDato.TdNumerico Then
                            If txtFilter.Text = "" Then
                                .DefaultView.RowFilter = Nothing
                            Else
                                .DefaultView.RowFilter = "" & vCampo1 & " = " & txtFilter.Text
                            End If
                        End If
                        Exit Sub
                    End If
                    If rbtnCampo2.Checked Then
                        If CType(vTipoCampo2, TipoDato) = TipoDato.TdCadena Or CType(vTipoCampo2, TipoDato) = TipoDato.TdFecha Then
                            '.DefaultView.RowFilter = vCampo1 & " Like '" & txtFilter.Text & "'"
                            '.DefaultView.RowFilter = vCampo1 & " = '" & txtFilter.Text & "'"
                            .DefaultView.RowFilter = "" & vCampo2 & " Like '*" & txtFilter.Text & "*'"
                        ElseIf CType(vTipoCampo2, TipoDato) = TipoDato.TdBoolean Or CType(vTipoCampo2, TipoDato) = TipoDato.TdNumerico Then
                            If txtFilter.Text = "" Then
                                .DefaultView.RowFilter = Nothing
                            Else
                                .DefaultView.RowFilter = "" & vCampo2 & " = " & txtFilter.Text
                            End If
                        End If
                        Exit Sub
                    End If

                    If rbtnCampo3.Checked Then
                        If CType(vTipoCampo3, TipoDato) = TipoDato.TdCadena Or CType(vTipoCampo3, TipoDato) = TipoDato.TdFecha Then
                            .DefaultView.RowFilter = "" & vCampo3 & " Like '*" & txtFilter.Text & "*'"
                        ElseIf CType(vTipoCampo3, TipoDato) = TipoDato.TdBoolean Or CType(vTipoCampo3, TipoDato) = TipoDato.TdNumerico Then
                            If txtFilter.Text = "" Then
                                .DefaultView.RowFilter = Nothing
                            Else
                                .DefaultView.RowFilter = "" & vCampo3 & " = " & txtFilter.Text
                            End If
                        End If
                        Exit Sub
                    End If
                    If rbtnCampo4.Checked Then
                        If CType(vTipoCampo4, TipoDato) = TipoDato.TdCadena Or CType(vTipoCampo4, TipoDato) = TipoDato.TdFecha Then
                            .DefaultView.RowFilter = "" & vCampo4 & " Like '*" & txtFilter.Text & "*'"
                        ElseIf CType(vTipoCampo4, TipoDato) = TipoDato.TdBoolean Or CType(vTipoCampo4, TipoDato) = TipoDato.TdNumerico Then
                            If txtFilter.Text = "" Then
                                .DefaultView.RowFilter = Nothing
                            Else
                                .DefaultView.RowFilter = "" & vCampo4 & " = " & txtFilter.Text
                            End If
                        End If
                        Exit Sub
                    End If
                    If rbtnCampo5.Checked Then
                        If CType(vTipoCampo5, TipoDato) = TipoDato.TdCadena Or CType(vTipoCampo5, TipoDato) = TipoDato.TdFecha Then
                            .DefaultView.RowFilter = "" & vCampo5 & " Like '*" & txtFilter.Text & "*'"
                        ElseIf CType(vTipoCampo5, TipoDato) = TipoDato.TdBoolean Or CType(vTipoCampo5, TipoDato) = TipoDato.TdNumerico Then
                            If txtFilter.Text = "" Then
                                .DefaultView.RowFilter = Nothing
                            Else
                                .DefaultView.RowFilter = "" & vCampo5 & " = " & txtFilter.Text
                            End If
                        End If
                        Exit Sub
                    End If
                    If rbtnCampo6.Checked Then
                        If CType(vTipoCampo6, TipoDato) = TipoDato.TdCadena Or CType(vTipoCampo6, TipoDato) = TipoDato.TdFecha Then
                            .DefaultView.RowFilter = "" & vCampo6 & " Like '*" & txtFilter.Text & "*'"
                        ElseIf CType(vTipoCampo6, TipoDato) = TipoDato.TdBoolean Or CType(vTipoCampo6, TipoDato) = TipoDato.TdNumerico Then
                            If txtFilter.Text = "" Then
                                .DefaultView.RowFilter = Nothing
                            Else
                                .DefaultView.RowFilter = "" & vCampo6 & " = " & txtFilter.Text
                            End If
                        End If
                        Exit Sub
                    End If

                End With
            End If
        End If
    End Sub

    Private Sub grid_Navigate(ByVal sender As System.Object, ByVal ne As System.Windows.Forms.NavigateEventArgs) Handles grid.Navigate

    End Sub
End Class
