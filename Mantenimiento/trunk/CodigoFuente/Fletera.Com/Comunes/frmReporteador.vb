'Fecha Creacion: 15 / Marzo / 2012
Imports System.Drawing.Printing
Imports System.Math
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports System.IO
Imports Microsoft.Office.Interop

Public Class frmReporteador
    Inherits System.Windows.Forms.Form
    Private prtSettings As PrinterSettings
    Private prtDoc As PrintDocument
    Private lineaActual As Integer = 0
    Private NumRegistros As Integer
    Private NumRegistros_DET As Integer
    Private ImpRep As ImprimeReporte
    Dim printFont As New System.Drawing.Font("Courier New", 7)
    'Dim TABLA As String = "CatEquipos"
    Dim NumPaginas As Integer = 0
    Dim ContPaginas As Integer = 0
    Friend WithEvents PanelControles As System.Windows.Forms.Panel
    'Dim NumerodeLineas As Double = 78
    Dim NumerodeLineas As Double
    Dim Tama�oLogico As Integer = 0
    Dim strLogico As String = ""
    'Dim TABLA As String = "CatMarcas"
    Dim myDataColumn As DataColumn
    Dim MyTablaOrden As DataTable = New DataTable("ORDEN")
    Dim myDataRow As DataRow

    Dim indice As Integer
    Dim vActivo As Boolean
    Dim vColumna As String
    Dim vOrden As String
    Dim vPosicion As Int16
    Dim vCampo As String

    Dim vActivo2 As Boolean
    Dim vColumna2 As String
    Dim vOrden2 As String
    Dim vPosicion2 As Int16
    Dim vCampo2 As String

    'Filtros y Order by
    Dim strSqlGlobal As String
    Dim strSqlGlobalDet As String
    Dim strFiltroEsp1 As String
    Dim strFiltroEsp2 As String
    Dim strSqlGlobalParte2 As String
    Private strFiltroSinWhere As String
    Private strOrdenaCampos As String

    Dim DsLista As New DataSet
    Dim DsLista2 As New DataSet
    Dim DsRep As New DataSet
    '*********************************************************************************
    'A R R E G L O  D E  C L A S E  C O L C L A S S
    Private _ArrColClass() As ColClass

    Dim LDescripcion() As Label
    Dim CmbCadena() As ComboChek, btnAgre() As Button, CmbFiltroIn() As ComboBox 'Arreglo de comboBox
    Dim txtLike() As TextBox
    Dim txtNumerico1() As TextBox, txtNumerico2() As TextBox
    Dim CaleFecha1() As DateTimePicker
    Dim CaleFecha2() As DateTimePicker
    Dim PanelLogico1() As Panel
    Dim GrupoLogico1() As GroupBox
    Dim rdbLogico1() As RadioButton
    Dim rdbLogico2() As RadioButton
    Dim CmbFiltro() As ComboBox
    'Dim ObjCtrlEsp() As Object
    Dim ObjCnt() As Object

    'Ordena
    Dim LDescripcionOrd() As Label
    Dim PanelOrdena1() As Panel
    Dim chkOrdena1() As CheckBox
    Dim rdbOrdena1() As RadioButton
    Dim rdbOrdena2() As RadioButton
    Dim btnArriba1() As Button
    Dim btnAbajo2() As Button

    Public vNomTabla As String
    'Public vNomTablaDET As String = 
    Public vValor1 As String = ""
    Public vValor2 As String = ""
    Public vValor3 As String = ""

    Friend WithEvents GridDatos As System.Windows.Forms.DataGridView
    Private vTituloReporte As String
    Private vbanHorizontal As Boolean
    Private vbanCuadricula As Boolean

    Friend WithEvents GrupoExtras As System.Windows.Forms.GroupBox
    Friend WithEvents PanelCntExtras As System.Windows.Forms.Panel
    Private vbanImpTotales As Boolean
    Private vBanderaGrupo As Boolean
    Friend WithEvents cmdOk As System.Windows.Forms.Button
    Friend WithEvents btnImportar As System.Windows.Forms.Button
    Friend WithEvents cmdReporte As System.Windows.Forms.Button
    Friend WithEvents cmdTerminar As System.Windows.Forms.Button
    Friend WithEvents cmdBorrar As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Activo As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents Columna As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Orden As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents Posicion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Campo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Prefijo As System.Windows.Forms.DataGridViewTextBoxColumn


    Dim MyTablaImprime As DataTable

    '**********************
    'AGREGADO 6/AGOSTO/2013 -> Texto para Imprimir Filtro
    Dim VTextoFiltro As String = ""
    Dim VTextoFiltroAux As String = ""

    '27/MARZO/2014
    Dim NumRenglones As Integer
    Dim NumRenglonXHoja As Integer

    '3/ABRIL/2014
    Dim vBanBorderDetalle As Boolean
    'PROPIEDADES


    Dim NoOrden As Integer = 0
    Dim NoActual As Integer = 0

#Region "Propiedades"
    Public Property NomTabla() As String
        Get
            Return vNomTabla
        End Get
        Set(ByVal value As String)
            vNomTabla = value
        End Set
    End Property

    Public Property TituloReporte() As String
        Get
            Return vTituloReporte
        End Get
        Set(ByVal value As String)
            vTituloReporte = value
        End Set
    End Property

    Public Property BanHorizontal() As Boolean
        Get
            Return vbanHorizontal
        End Get
        Set(ByVal value As Boolean)
            vbanHorizontal = value
        End Set
    End Property

    Public Property BanCuadricula() As Boolean
        Get
            Return vbanCuadricula
        End Get
        Set(ByVal value As Boolean)
            vbanCuadricula = value
        End Set
    End Property

    Public Property BanBorderDetalle() As Boolean
        Get
            Return vBanBorderDetalle
        End Get
        Set(ByVal value As Boolean)
            vBanBorderDetalle = value
        End Set
    End Property

    Public Property BanImpTotales() As Boolean
        Get
            Return vbanImpTotales
        End Get
        Set(ByVal value As Boolean)
            vbanImpTotales = value
        End Set
    End Property

    Public Property BanGrupo() As Boolean
        Get
            Return vBanderaGrupo
        End Get
        Set(ByVal value As Boolean)
            vBanderaGrupo = value
        End Set
    End Property


    'vBanderaGrupo
#End Region
#Region " C�digo generado por el Dise�ador de Windows Forms "
    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents MenuImporta As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents AcrobatpdfToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExcelxlsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents WebhtmlToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnArriba As System.Windows.Forms.Button
    Friend WithEvents btnAbajo As System.Windows.Forms.Button
    Friend WithEvents grupoOrdena As System.Windows.Forms.GroupBox
    Friend WithEvents GridOrden As System.Windows.Forms.DataGridView
    Friend WithEvents grupoFiltra As System.Windows.Forms.GroupBox
    Friend WithEvents btnPreview As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmReporteador))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.MenuImporta = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.AcrobatpdfToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExcelxlsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.WebhtmlToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.grupoFiltra = New System.Windows.Forms.GroupBox()
        Me.PanelControles = New System.Windows.Forms.Panel()
        Me.btnPreview = New System.Windows.Forms.Button()
        Me.grupoOrdena = New System.Windows.Forms.GroupBox()
        Me.btnAbajo = New System.Windows.Forms.Button()
        Me.GridOrden = New System.Windows.Forms.DataGridView()
        Me.Activo = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.Columna = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Orden = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.Posicion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Campo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Prefijo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnArriba = New System.Windows.Forms.Button()
        Me.GridDatos = New System.Windows.Forms.DataGridView()
        Me.GrupoExtras = New System.Windows.Forms.GroupBox()
        Me.PanelCntExtras = New System.Windows.Forms.Panel()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.cmdBorrar = New System.Windows.Forms.Button()
        Me.cmdTerminar = New System.Windows.Forms.Button()
        Me.cmdReporte = New System.Windows.Forms.Button()
        Me.btnImportar = New System.Windows.Forms.Button()
        Me.cmdOk = New System.Windows.Forms.Button()
        Me.MenuImporta.SuspendLayout()
        Me.grupoFiltra.SuspendLayout()
        Me.grupoOrdena.SuspendLayout()
        CType(Me.GridOrden, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridDatos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GrupoExtras.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuImporta
        '
        Me.MenuImporta.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AcrobatpdfToolStripMenuItem, Me.ExcelxlsToolStripMenuItem, Me.WebhtmlToolStripMenuItem})
        Me.MenuImporta.Name = "MenuImporta"
        Me.MenuImporta.Size = New System.Drawing.Size(146, 70)
        '
        'AcrobatpdfToolStripMenuItem
        '
        Me.AcrobatpdfToolStripMenuItem.Name = "AcrobatpdfToolStripMenuItem"
        Me.AcrobatpdfToolStripMenuItem.Size = New System.Drawing.Size(145, 22)
        Me.AcrobatpdfToolStripMenuItem.Text = "Acrobat (pdf)"
        Me.AcrobatpdfToolStripMenuItem.Visible = False
        '
        'ExcelxlsToolStripMenuItem
        '
        Me.ExcelxlsToolStripMenuItem.Name = "ExcelxlsToolStripMenuItem"
        Me.ExcelxlsToolStripMenuItem.Size = New System.Drawing.Size(145, 22)
        Me.ExcelxlsToolStripMenuItem.Text = "Excel (xls)"
        Me.ExcelxlsToolStripMenuItem.Visible = False
        '
        'WebhtmlToolStripMenuItem
        '
        Me.WebhtmlToolStripMenuItem.Name = "WebhtmlToolStripMenuItem"
        Me.WebhtmlToolStripMenuItem.Size = New System.Drawing.Size(145, 22)
        Me.WebhtmlToolStripMenuItem.Text = "Web (html)"
        Me.WebhtmlToolStripMenuItem.Visible = False
        '
        'grupoFiltra
        '
        Me.grupoFiltra.Controls.Add(Me.PanelControles)
        Me.grupoFiltra.Location = New System.Drawing.Point(12, -2)
        Me.grupoFiltra.Name = "grupoFiltra"
        Me.grupoFiltra.Size = New System.Drawing.Size(455, 151)
        Me.grupoFiltra.TabIndex = 20
        Me.grupoFiltra.TabStop = False
        Me.grupoFiltra.Text = "Filtrar por:"
        '
        'PanelControles
        '
        Me.PanelControles.AutoScroll = True
        Me.PanelControles.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelControles.Location = New System.Drawing.Point(3, 16)
        Me.PanelControles.Name = "PanelControles"
        Me.PanelControles.Size = New System.Drawing.Size(449, 132)
        Me.PanelControles.TabIndex = 0
        '
        'btnPreview
        '
        Me.btnPreview.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPreview.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnPreview.Location = New System.Drawing.Point(-658, 420)
        Me.btnPreview.Name = "btnPreview"
        Me.btnPreview.Size = New System.Drawing.Size(112, 23)
        Me.btnPreview.TabIndex = 18
        Me.btnPreview.Text = "Vista preliminar"
        '
        'grupoOrdena
        '
        Me.grupoOrdena.Controls.Add(Me.btnAbajo)
        Me.grupoOrdena.Controls.Add(Me.GridOrden)
        Me.grupoOrdena.Controls.Add(Me.btnArriba)
        Me.grupoOrdena.Location = New System.Drawing.Point(15, 283)
        Me.grupoOrdena.Name = "grupoOrdena"
        Me.grupoOrdena.Size = New System.Drawing.Size(453, 151)
        Me.grupoOrdena.TabIndex = 23
        Me.grupoOrdena.TabStop = False
        Me.grupoOrdena.Text = "Ordenar por:"
        '
        'btnAbajo
        '
        Me.btnAbajo.Image = CType(resources.GetObject("btnAbajo.Image"), System.Drawing.Image)
        Me.btnAbajo.Location = New System.Drawing.Point(389, 91)
        Me.btnAbajo.Name = "btnAbajo"
        Me.btnAbajo.Size = New System.Drawing.Size(58, 49)
        Me.btnAbajo.TabIndex = 26
        Me.btnAbajo.UseVisualStyleBackColor = True
        '
        'GridOrden
        '
        Me.GridOrden.AllowUserToResizeRows = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.GridOrden.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.GridOrden.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.GridOrden.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Activo, Me.Columna, Me.Orden, Me.Posicion, Me.Campo, Me.Prefijo})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.GridOrden.DefaultCellStyle = DataGridViewCellStyle2
        Me.GridOrden.Location = New System.Drawing.Point(6, 16)
        Me.GridOrden.Name = "GridOrden"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.GridOrden.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.GridOrden.RowHeadersWidth = 31
        Me.GridOrden.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.GridOrden.Size = New System.Drawing.Size(381, 124)
        Me.GridOrden.TabIndex = 25
        '
        'Activo
        '
        Me.Activo.HeaderText = ""
        Me.Activo.Name = "Activo"
        Me.Activo.Width = 30
        '
        'Columna
        '
        Me.Columna.HeaderText = "Columna"
        Me.Columna.Name = "Columna"
        Me.Columna.Width = 150
        '
        'Orden
        '
        Me.Orden.HeaderText = "Orden"
        Me.Orden.Items.AddRange(New Object() {"Ascendente", "Descendente"})
        Me.Orden.Name = "Orden"
        Me.Orden.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Orden.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'Posicion
        '
        Me.Posicion.HeaderText = "Posicion"
        Me.Posicion.Name = "Posicion"
        Me.Posicion.Visible = False
        '
        'Campo
        '
        Me.Campo.HeaderText = "Campo"
        Me.Campo.Name = "Campo"
        Me.Campo.Visible = False
        '
        'Prefijo
        '
        Me.Prefijo.HeaderText = "Prefijo"
        Me.Prefijo.Name = "Prefijo"
        Me.Prefijo.Visible = False
        '
        'btnArriba
        '
        Me.btnArriba.Image = CType(resources.GetObject("btnArriba.Image"), System.Drawing.Image)
        Me.btnArriba.Location = New System.Drawing.Point(389, 16)
        Me.btnArriba.Name = "btnArriba"
        Me.btnArriba.Size = New System.Drawing.Size(58, 49)
        Me.btnArriba.TabIndex = 25
        Me.btnArriba.UseVisualStyleBackColor = True
        '
        'GridDatos
        '
        Me.GridDatos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.GridDatos.Location = New System.Drawing.Point(491, 202)
        Me.GridDatos.Name = "GridDatos"
        Me.GridDatos.Size = New System.Drawing.Size(174, 83)
        Me.GridDatos.TabIndex = 24
        Me.GridDatos.Visible = False
        '
        'GrupoExtras
        '
        Me.GrupoExtras.Controls.Add(Me.PanelCntExtras)
        Me.GrupoExtras.Location = New System.Drawing.Point(12, 155)
        Me.GrupoExtras.Name = "GrupoExtras"
        Me.GrupoExtras.Size = New System.Drawing.Size(458, 122)
        Me.GrupoExtras.TabIndex = 25
        Me.GrupoExtras.TabStop = False
        Me.GrupoExtras.Text = "Opciones Extras:"
        '
        'PanelCntExtras
        '
        Me.PanelCntExtras.AutoScroll = True
        Me.PanelCntExtras.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelCntExtras.Location = New System.Drawing.Point(3, 16)
        Me.PanelCntExtras.Name = "PanelCntExtras"
        Me.PanelCntExtras.Size = New System.Drawing.Size(452, 103)
        Me.PanelCntExtras.TabIndex = 0
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.cmdBorrar)
        Me.GroupBox1.Controls.Add(Me.cmdTerminar)
        Me.GroupBox1.Controls.Add(Me.cmdReporte)
        Me.GroupBox1.Controls.Add(Me.btnImportar)
        Me.GroupBox1.Controls.Add(Me.cmdOk)
        Me.GroupBox1.Location = New System.Drawing.Point(15, 440)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(455, 92)
        Me.GroupBox1.TabIndex = 17
        Me.GroupBox1.TabStop = False
        '
        'cmdBorrar
        '
        Me.cmdBorrar.Image = CType(resources.GetObject("cmdBorrar.Image"), System.Drawing.Image)
        Me.cmdBorrar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdBorrar.Location = New System.Drawing.Point(94, 16)
        Me.cmdBorrar.Name = "cmdBorrar"
        Me.cmdBorrar.Size = New System.Drawing.Size(88, 67)
        Me.cmdBorrar.TabIndex = 10
        Me.cmdBorrar.Text = "&Impresora"
        Me.cmdBorrar.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cmdTerminar
        '
        Me.cmdTerminar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdTerminar.Image = CType(resources.GetObject("cmdTerminar.Image"), System.Drawing.Image)
        Me.cmdTerminar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdTerminar.Location = New System.Drawing.Point(358, 16)
        Me.cmdTerminar.Name = "cmdTerminar"
        Me.cmdTerminar.Size = New System.Drawing.Size(88, 67)
        Me.cmdTerminar.TabIndex = 13
        Me.cmdTerminar.Text = "&Terminar"
        Me.cmdTerminar.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cmdReporte
        '
        Me.cmdReporte.Image = CType(resources.GetObject("cmdReporte.Image"), System.Drawing.Image)
        Me.cmdReporte.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdReporte.Location = New System.Drawing.Point(270, 16)
        Me.cmdReporte.Name = "cmdReporte"
        Me.cmdReporte.Size = New System.Drawing.Size(88, 67)
        Me.cmdReporte.TabIndex = 12
        Me.cmdReporte.Text = "&Configuraci{on"
        Me.cmdReporte.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'btnImportar
        '
        Me.btnImportar.ContextMenuStrip = Me.MenuImporta
        Me.btnImportar.Image = Global.Fletera.My.Resources.Resources._1460375269_excel
        Me.btnImportar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnImportar.Location = New System.Drawing.Point(182, 16)
        Me.btnImportar.Name = "btnImportar"
        Me.btnImportar.Size = New System.Drawing.Size(88, 67)
        Me.btnImportar.TabIndex = 11
        Me.btnImportar.Text = "&Excel:"
        Me.btnImportar.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cmdOk
        '
        Me.cmdOk.Image = Global.Fletera.My.Resources.Resources.Pantalla
        Me.cmdOk.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdOk.Location = New System.Drawing.Point(6, 16)
        Me.cmdOk.Name = "cmdOk"
        Me.cmdOk.Size = New System.Drawing.Size(88, 67)
        Me.cmdOk.TabIndex = 9
        Me.cmdOk.Text = "&Pantalla"
        Me.cmdOk.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'frmReporteador
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.CancelButton = Me.cmdTerminar
        Me.ClientSize = New System.Drawing.Size(483, 544)
        Me.Controls.Add(Me.btnPreview)
        Me.Controls.Add(Me.GrupoExtras)
        Me.Controls.Add(Me.grupoFiltra)
        Me.Controls.Add(Me.GridDatos)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.grupoOrdena)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmReporteador"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Reporteador"
        Me.MenuImporta.ResumeLayout(False)
        Me.grupoFiltra.ResumeLayout(False)
        Me.grupoOrdena.ResumeLayout(False)
        CType(Me.GridOrden, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridDatos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GrupoExtras.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Function CalculaNumPaginas() As Integer
        Dim NumPD As Double
        Dim numpI As Double
        If BanHorizontal Then
            NumerodeLineas = 35
        Else
            'NumerodeLineas = 78
            NumerodeLineas = 70
            'NumerodeLineas = 50
        End If

        If NumRenglonXHoja > 0 Then
            NumPD = (NumRegistros / NumRenglonXHoja)
        Else
            NumPD = (NumRegistros / NumerodeLineas)
        End If


        numpI = Round(NumPD, 0)

        If NumPD > numpI Then
            NumPaginas = NumPD + 1
        Else
            NumPaginas = NumPD
        End If

        Return NumPaginas
    End Function
    Private Sub Imprimir(ByVal BandPreview As Boolean)
        Try

            ' MANDAR A LA IMPRESORA O MOSTRAR EL PRINTPREVIEW
            '
            prtSettings = New PrinterSettings
            'If prtSettings Is Nothing Then
            '    prtSettings = New PrinterSettings
            'End If
            '
            'PARA SELECCIONAR LA IMPRESORA DESPUES LO CHECAMOS
            'If chkSelAntes.Checked Then
            '    If seleccionarImpresora() = False Then Return
            'End If
            '
            If prtDoc Is Nothing Then
                prtDoc = New PrintDocument
                AddHandler prtDoc.PrintPage, AddressOf prt_PrintPage
            End If

            'ExisteColaImp = LlenaDatos("1", "ColaImpresion", "IdParametro", TipoDato.TdNumerico, Inicio.CONSTR)

            'If ExisteColaImp = KEY_RCORRECTO Then
            '    If StrColaImpRecibo <> "" Then
            '        prtSettings.PrinterName = StrColaImpRecibo
            '        MsgBox(StrColaImpRecibo, MsgBoxStyle.Information, Me.Text)
            '    End If
            'End If
            'Dim rango As Integer =


            ' ''COLAS DE IMPRESION
            ''If vNomTabla = UCase("PEDIDOSPEN") Then
            ''    If Registro_ColaPedidoLocal <> "" Then
            ''        prtSettings.PrinterName = Registro_ColaPedidoLocal
            ''    End If
            ''    prtSettings.Copies = GetContenidoControl(0, 0)
            ''ElseIf vNomTabla = UCase("REMISION") Then
            ''    If Registro_ColaRemisionLocal <> "" Then
            ''        prtSettings.PrinterName = Registro_ColaRemisionLocal
            ''    End If
            ''    prtSettings.Copies = GetContenidoControl(0, 0)
            ''End If



            'prtDoc.DefaultPageSettings.Landscape = vbanHorizontal
            '
            ' resetear la l�nea actual
            lineaActual = 0
            '
            ' la configuraci�n a usar en la impresi�n
            prtDoc.PrinterSettings = prtSettings

            '
            'prtDoc.PrinterSettings.Copies = GetContenidoControl(0, 0)
            If BandPreview Then
                Dim prtPrev As New PrintPreviewDialog
                prtPrev.Document = prtDoc
                prtPrev.Text = "Previsualizar datos de " & Me.Text
                prtPrev.WindowState = FormWindowState.Maximized
                prtPrev.Document.DefaultPageSettings.Landscape = vbanHorizontal
                'prtPrev.PrintPreviewControl.Zoom = 1
                If BanHorizontal Then
                    prtPrev.PrintPreviewControl.Zoom = 1
                Else
                    prtPrev.PrintPreviewControl.Zoom = 1.5
                End If

                'prtPrev.MdiParent = Me
                prtPrev.ShowDialog()

                'Dim frm As New frmCatDeptos
                'frm.MdiParent = Me
                'frm.Show()
                'prtPrev.AutoSizeMod
            Else
                prtDoc.DefaultPageSettings.Landscape = vbanHorizontal


                prtDoc.Print()
            End If
        Catch ex As Exception
            MsgBox(ex.Message, Me.Text)
        End Try

    End Sub

    Private Sub prt_PrintPage(ByVal sender As Object, ByVal e As PrintPageEventArgs)
        Dim parametro As Object = Nothing
        Dim parametro2 As Object = Nothing
        Dim NombreCampo As String = ""
        Dim NombreCampo2 As String = ""

        ' Este evento se produce cada vez que se va a imprimir una p�gina
        Try
            'lineaActual = 0
            Dim Cadena As String = ""

            Dim yPos As Single = e.MarginBounds.Top
            'Margen Izquierdo
            Dim leftMargin As Single = e.MarginBounds.Left
            'Fuentes
            'Altura de la Linea
            Dim lineHeight As Single = printFont.GetHeight(e.Graphics)

            '**********************
            'AGREGADO 31/JULIO/2013 -> Reinicia Arreglos
            If lineaActual = 0 Then
                ImpRep.ReiniciaArreglos()
            End If


            'Imgenes DE Encabezado
            'ImpRep.Imagen1 = My.Resources.maharajalogo1

            'Dim bmp As New Bitmap(My.Resources.maharajalogo1)

            'Dim bmp As New Bitmap(My.Resources.softNg)

            'OJOOOOOO
            Dim bmp As New Bitmap(My.Resources.logo_ciltra_transparente1)
            'ImpRep.Imagen1 = New Bitmap(bmp, bmp.Width / 2, bmp.Height / 2)
            'ImpRep.Imagen1 = New Bitmap(bmp, bmp.Width / 10, bmp.Height / 10)
            ImpRep.Imagen1 = New Bitmap(bmp, bmp.Width / 2, bmp.Height / 2)
            'ImpRep.Imagen1.Width
            ImpRep.PosicionImagen1 = New Point(e.MarginBounds.Left - 10, yPos - 30)

            '
            'ImpRep.Imagen2 = My.Resources.softNg
            'ImpRep.PosicionImagen2 = New Point(e.MarginBounds.Right - 80, yPos)


            Dim bmp2 As New Bitmap(My.Resources.softNg)
            'OJOOOOOOOO
            ImpRep.Imagen2 = New Bitmap(bmp2, bmp2.Width / 1, bmp2.Height / 1)
            ImpRep.PosicionImagen2 = New Point(e.MarginBounds.Right - 100, yPos - 30)

            'Se imprime Encabezado
            'If UCase(vNomTabla) = UCase("AntSaldos") Or UCase(vNomTabla) = UCase("AntSaldosDet") Then
            'If UCase(vNomTabla) = UCase("TRASPFALTANTES") Then
            '    'parametro = GetContenidoControl(0, 0)
            '    ''parametro2 = TryCast(_ArrColClass(9).ControlLibre(0), CheckBox).Checked
            '    'parametro2 = TryCast(GetControl(1, 0), CheckBox).Checked

            '    parametro = TryCast(GetControl(0, 0), CheckBox).Checked
            'End If

            Select Case UCase(vNomTabla)
                Case UCase("TRASPFALTANTES"), UCase("TRASPFALTANTESINI")
                    parametro = TryCast(GetControl(0, 0), CheckBox).Checked

            End Select

            yPos = ImpRep.ImprimeEncabezado(e, yPos, vbanHorizontal, VTextoFiltro, parametro, parametro2)
            If BanBorderDetalle Then
                e.Graphics.DrawLine(Pens.Black, leftMargin, yPos + lineHeight, e.MarginBounds.Right + 20, yPos + lineHeight)
                'ImpRep.DibujaLinea(e, yPos, BanHorizontal, 20)
            End If
            'Se imprime Cuerpo
            Do
                ''O R I G I N A L
                'yPos += lineHeight
                ''Aqui se imprime el dato
                'yPos = ImpRep.ImprimeLinea(e, yPos, lineaActual, BanCuadricula, BanBorderDetalle)
                'lineaActual += 1

                yPos += lineHeight
                'Aqui se imprime el dato

                'If UCase(vNomTabla) = UCase("TRASPFALTANTES") Then
                '    If NumRegistros_DET > 0 Then
                '        NombreCampo = "NOMOVIMIENTO"
                '        NombreCampo2 = ""
                '    End If

                'End If
                Select Case UCase(vNomTabla)
                    Case UCase("TRASPFALTANTES"), UCase("TRASPFALTANTESINI")
                        If NumRegistros_DET > 0 Then
                            NombreCampo = "NOMOVIMIENTO"
                            NombreCampo2 = ""
                        End If

                End Select

                yPos = ImpRep.ImprimeLinea(e, yPos, lineaActual, BanCuadricula, BanBorderDetalle, NombreCampo, NombreCampo2, parametro, IIf(BanHorizontal, e.MarginBounds.Bottom - 35, e.MarginBounds.Bottom - 60))

                'NoActual = ImpRep.DsReporte.Tables(vNomTabla).DefaultView.Item(lineaActual).Item("NOORDEN")
                'If NumRegistros_DET > 0 Then
                '    If ImpRep.DsReporte_DET.Tables(vNomTabla).DefaultView.Count > 0 Then
                '        ImpRep.DsReporte_DET.Tables(0).DefaultView.RowFilter = Nothing
                '        ImpRep.DsReporte_DET.Tables(0).DefaultView.RowFilter = "NoOrden = " & NoActual
                '        If ImpRep.DsReporte_DET.Tables(vNomTabla).DefaultView.Count > 0 Then
                '            For i = 0 To ImpRep.DsReporte_DET.Tables(0).DefaultView.Count - 1
                '                ImpRep.DsReporte.Tables(0).DefaultView.Item(i).Item("NOORDEN")
                '            Next
                '        End If
                '        ImpRep.DsReporte_DET.Tables(0).DefaultView.RowFilter = Nothing
                '    End If
                'End If


                lineaActual += 1

                'yPos += lineHeight

                'If UCase(vNomTabla) = UCase("ORDENES") Then
                '    If lineaActual = 0 Then
                '        NoOrden = 0
                '        NoActual = 0
                '    End If
                '    NoActual = ImpRep.DsReporte.Tables(vNomTabla).DefaultView.Item(lineaActual).Item("NOORDEN")
                '    If NoOrden <> NoActual Then
                '        'Aqui se imprime el dato
                '        yPos = ImpRep.ImprimeLinea(e, yPos, lineaActual, BanCuadricula, BanBorderDetalle)
                '        NoOrden = NoActual
                '        lineaActual += 1
                '    Else
                '        lineaActual += 1
                '    End If
                'Else
                '    'Aqui se imprime el dato
                '    yPos = ImpRep.ImprimeLinea(e, yPos, lineaActual, BanCuadricula, BanBorderDetalle)
                '    lineaActual += 1
                'End If

            Loop Until yPos >= IIf(BanHorizontal, e.MarginBounds.Bottom - 35, e.MarginBounds.Bottom - 60) _
                OrElse (lineaActual >= NumRegistros OrElse IIf(NumRenglonXHoja > 0, lineaActual > NumRenglones, False))
            'OrElse (lineaActual >= NumRegistros OrElse lineaActual > NumRenglones)

            'e.MarginBounds.Bottom - 20
            If BanCuadricula Or BanBorderDetalle Then
                ImpRep.DibujaLinea(e, yPos, BanHorizontal, 20)
            End If

            ContPaginas += 1


            If BanImpTotales And lineaActual = NumRegistros Then
                yPos += lineHeight
                If BanGrupo Then
                    'yPos = ImpRep.ImprimeTotal(e, yPos, 0)
                    'yPos = ImpRep.ImprimeTotal(e, yPos, 0)
                    'yPos = ImpRep.ImprimeResumen(e, yPos, 0)
                    yPos = ImpRep.ImprimeTotalGrupo(e, yPos, 0)
                    yPos = ImpRep.ImprimeTotal(e, yPos, 0)
                Else
                    If UCase(vNomTabla) = UCase("Remision") Then
                        'yPos = ImpRep.ImprimeResumen(e, yPos, 0)
                    Else
                        yPos = ImpRep.ImprimeTotal(e, yPos, 0)
                        'yPos = ImpRep.ImprimeResumen(e, yPos, 0)

                    End If
                End If


            End If


            'yPos = e.MarginBounds.Bottom + 20


            If UCase(vNomTabla) = UCase("Remision") Then
                If BanHorizontal Then
                    'yPos = e.MarginBounds.Bottom - 40
                    yPos = e.MarginBounds.Bottom
                Else
                    'yPos = e.MarginBounds.Bottom - 40
                    yPos = e.MarginBounds.Bottom - 120
                    'yPos = e.MarginBounds.Bottom + 20
                End If
                If BanImpTotales And lineaActual = NumRegistros Then
                    'yPos += lineHeight
                    'yPos = yPos - 76
                    yPos = yPos - 99

                    yPos = ImpRep.ImprimeResumen(e, yPos, 0)
                    'If BanGrupo Then
                    '    yPos = ImpRep.ImprimeTotalGrupo(e, yPos, 0)
                    'End If

                    'If UCase(vNomTabla) = UCase("Remision") Then
                    '    yPos = ImpRep.ImprimeResumen(e, yPos, 0)
                    'Else
                    '    yPos = ImpRep.ImprimeTotal(e, yPos, 0)
                    '    yPos = ImpRep.ImprimeResumen(e, yPos, 0)

                    'End If
                End If


                Cadena = "Debo (emos) y pagare (mos) incondicionalmente a la orden de " & Parametro_Empresa & " en esta ciudad el dia " & CDate(ImpRep.DsReporte.Tables(vNomTabla).DefaultView.Item(0).Item("FECHAMOVIMIENTO")).ToShortDateString & " la cantidad de $ " & Format((ImpRep.DsReporte.Tables(vNomTabla).DefaultView.Item(0).Item("TOTAL")), "###,###0.00") & " M.N" & vbCrLf &
                         "importe del (los) articulo (s) arriba especificado (s) que he (mos) recibido a mi (nuestra) entera satisfacci�n si no fuera pagado a su vencimiento causara" & vbCrLf &
                         "inter�s moratorio de ____ % que rige este pagare mercantil." & vbCrLf &
                         "Todo cheque devuelto causara un recargo del ____ % sobre valor del mismo, de acuerdo con el articulo 193 de la ley general de titulos y operaciones de cr�dito." & vbCrLf &
                         "" & vbCrLf &
                         "" & vbCrLf &
                         "                                                                                                                     __________________________________" & vbCrLf &
                         "                                                                                                                                    FIRMA"
                Cadena = ""
            Else
                If BanHorizontal Then
                    'yPos = e.MarginBounds.Bottom - 40
                    yPos = e.MarginBounds.Bottom
                Else
                    yPos = e.MarginBounds.Bottom - 40
                End If
            End If
            yPos = ImpRep.PiePagina(e, yPos, ContPaginas, NumPaginas, Cadena)


            If lineaActual < NumRegistros Then
                If NumRenglonXHoja > 0 Then
                    NumRenglones = NumRenglones + NumRenglonXHoja

                End If
                e.HasMorePages = True
            Else
                'NumPaginas = 0
                lineaActual = 0
                ContPaginas = 0
                NumRenglones = NumRenglonXHoja
                e.HasMorePages = False
            End If
        Catch ex As Exception
            MsgBox(ex.Message, Me.Text)
        End Try

    End Sub

    Private Sub CreaReportes(ByVal Tabla As String)
        strSqlGlobal = ""
        strSqlGlobalDet = ""
        ImpRep.FuenteEncabezado = New System.Drawing.Font("Arial", 12, FontStyle.Bold)
        'ImpRep.Encabezado1 = Parametro_Empresa
        'ImpRep.FuenteEncabezado = New Drawing.Font(Font.Name = "Arial", 15, Font.Bold)
        'ImpRep.Encabezado1 = "SECRETARIA DE SEGURIDAD PUBLICA - SSP (YUCAT�N)"
        'ImpRep.Encabezado2 = "Sistema iBancos"
        'ImpRep.Encabezado2 = "Sistema Lealtad de Clientes"
        ImpRep.FuenteEncabezado2 = New System.Drawing.Font("Arial", 10, FontStyle.Bold)
        'ImpRep.Encabezado2 = Parametro_Direccion


        'ImpRep.TituloReporte = UCase(vTituloReporte)

        'ImpRep.BandAlineacionTitulo = True

        'Fuente Para Encabezados y Datos
        'ImpRep.FuenteEncabCampos = New System.Drawing.Font("Courier New", 8, FontStyle.Bold)
        'ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 7)
        'ImpRep.FuenteImprimeCamposNeg = New System.Drawing.Font("Courier New", 7, FontStyle.Bold)

        ImpRep.FuenteEncabCampos = New System.Drawing.Font("Courier New", 9, FontStyle.Bold)
        ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 8)
        ImpRep.FuenteImprimeCamposNeg = New System.Drawing.Font("Courier New", 8, FontStyle.Bold)

        ImpRep.Constr = Inicio.CONSTR
        ImpRep.TipoConexion = TipoConexion.TcSQL

        'Para Pie de Pagina

        ImpRep.AligPaginacion = TipoAlineacion.TdDerecha
        ImpRep.BanSoloPagina = False
        ImpRep.AligSoloPagina = TipoAlineacion.TdDerecha
        ImpRep.PieParametro1 = Now
        ImpRep.AligPieParametro1 = TipoAlineacion.TdIzquierda
        ImpRep.PieParametro2 = UserId & " " & NomUser
        ImpRep.AligPieParametro2 = TipoAlineacion.TdCentrado
        'SI BANDPAGINACION = TRUE ENTONCES EL PARAMETRO3 NO SE IMPRIME
        ImpRep.BandPaginacion = True
        ImpRep.BanSoloPagina = False
        ImpRep.PieParametro3 = "Parametro3"
        ImpRep.AligPieParametro3 = TipoAlineacion.TdDerecha

        ImpRep.NomTabla = Tabla


        '**************************************************************************************************************************
        'Se pasa el Arreglo de ColClass en el Sub CargaCampos2 Se hace el Filtro . .CHEKAR!!!. . . 
        '**************************************************************************************************************************
        ImpRep.CargaCampos2(_ArrColClass)

        Select Case UCase(Tabla)
            Case UCase("CatDeptos")
                ImpRep.Encabezado1 = Parametro_Empresa
                ImpRep.Encabezado2 = Parametro_Direccion
                'ImpRep.TituloReporte = Parametro_Direccion
                ImpRep.TituloReporte = "Cat�logo de Departamentos"

                ImpRep.AligTituloReporte = TipoAlineacion.TdCentrado
                ImpRep.FuenteTitulo = New System.Drawing.Font("Arial", 10, FontStyle.Bold)

                ImpRep.FuenteEncabCampos = New System.Drawing.Font("Courier New", 11, FontStyle.Bold)
                ImpRep.FuenteEncabCamposOpc = New System.Drawing.Font("Courier New", 7)

                ImpRep.NomReporte = "RepCatDeptos"
                ImpRep.BandRepEsp = False
                ImpRep.FuenteLetrasChiquitas = New System.Drawing.Font("Arial", 5, FontStyle.Bold)
                'ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 7.5)
                ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 9)

                NumRenglonXHoja = 55
                NumRenglones = NumRenglonXHoja

                strSqlGlobal = "SELECT dep.IdDepto, " &
                "dep.NomDepto " &
                "FROM dbo.CatDeptos dep"

            Case UCase("CatMarcas")
                ImpRep.Encabezado1 = Parametro_Empresa
                ImpRep.Encabezado2 = "R.F.C. : " & Parametro_RFC
                'ImpRep.TituloReporte = Parametro_Direccion
                ImpRep.TituloReporte = "Cat�logo de Marcas"

                ImpRep.AligTituloReporte = TipoAlineacion.TdCentrado
                ImpRep.FuenteTitulo = New System.Drawing.Font("Arial", 10, FontStyle.Bold)

                ImpRep.FuenteEncabCampos = New System.Drawing.Font("Courier New", 11, FontStyle.Bold)
                ImpRep.FuenteEncabCamposOpc = New System.Drawing.Font("Courier New", 7)

                ImpRep.NomReporte = "RepCatMarcas"
                ImpRep.BandRepEsp = False
                ImpRep.FuenteLetrasChiquitas = New System.Drawing.Font("Arial", 5, FontStyle.Bold)
                'ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 7.5)
                ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 9)

                NumRenglonXHoja = 55
                NumRenglones = NumRenglonXHoja

                strSqlGlobal = "SELECT IDMARCA, NOMBREMARCA FROM CATMARCAS"

            Case UCase("CatDivision")
                ImpRep.Encabezado1 = Parametro_Empresa
                ImpRep.Encabezado2 = "R.F.C. : " & Parametro_RFC
                'ImpRep.TituloReporte = Parametro_Direccion
                ImpRep.TituloReporte = "Cat�logo de Divisiones"

                ImpRep.AligTituloReporte = TipoAlineacion.TdCentrado
                ImpRep.FuenteTitulo = New System.Drawing.Font("Arial", 10, FontStyle.Bold)

                ImpRep.FuenteEncabCampos = New System.Drawing.Font("Courier New", 11, FontStyle.Bold)
                ImpRep.FuenteEncabCamposOpc = New System.Drawing.Font("Courier New", 7)

                ImpRep.NomReporte = "RepCatDivision"
                ImpRep.BandRepEsp = False
                ImpRep.FuenteLetrasChiquitas = New System.Drawing.Font("Arial", 5, FontStyle.Bold)
                'ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 7.5)
                ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 9)

                NumRenglonXHoja = 55
                NumRenglones = NumRenglonXHoja

                strSqlGlobal = "SELECT IDDIVISION, NOMDIVISION FROM CATDIVISION"
            Case UCase("CatPais")
                ImpRep.Encabezado1 = Parametro_Empresa
                ImpRep.Encabezado2 = "R.F.C. : " & Parametro_RFC
                'ImpRep.TituloReporte = Parametro_Direccion
                ImpRep.TituloReporte = "Cat�logo de Paises"

                ImpRep.AligTituloReporte = TipoAlineacion.TdCentrado
                ImpRep.FuenteTitulo = New System.Drawing.Font("Arial", 10, FontStyle.Bold)

                ImpRep.FuenteEncabCampos = New System.Drawing.Font("Courier New", 11, FontStyle.Bold)
                ImpRep.FuenteEncabCamposOpc = New System.Drawing.Font("Courier New", 7)

                ImpRep.NomReporte = "RepCatPais"
                ImpRep.BandRepEsp = False
                ImpRep.FuenteLetrasChiquitas = New System.Drawing.Font("Arial", 5, FontStyle.Bold)
                'ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 7.5)
                ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 9)

                NumRenglonXHoja = 55
                NumRenglones = NumRenglonXHoja

                strSqlGlobal = "SELECT IDPAIS, NOMPAIS FROM CATPAIS"
            Case UCase("CatPuestos")
                ImpRep.Encabezado1 = Parametro_Empresa
                ImpRep.Encabezado2 = "R.F.C. : " & Parametro_RFC
                'ImpRep.TituloReporte = Parametro_Direccion
                ImpRep.TituloReporte = "Cat�logo de Puestos"

                ImpRep.AligTituloReporte = TipoAlineacion.TdCentrado
                ImpRep.FuenteTitulo = New System.Drawing.Font("Arial", 10, FontStyle.Bold)

                ImpRep.FuenteEncabCampos = New System.Drawing.Font("Courier New", 11, FontStyle.Bold)
                ImpRep.FuenteEncabCamposOpc = New System.Drawing.Font("Courier New", 7)

                ImpRep.NomReporte = "RepCatPuestos"
                ImpRep.BandRepEsp = False
                ImpRep.FuenteLetrasChiquitas = New System.Drawing.Font("Arial", 5, FontStyle.Bold)
                'ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 7.5)
                ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 9)

                NumRenglonXHoja = 55
                NumRenglones = NumRenglonXHoja

                strSqlGlobal = "SELECT IDPUESTO, NOMPUESTO FROM CATPUESTOS"
            Case UCase("CatTipoHerramienta")
                ImpRep.Encabezado1 = Parametro_Empresa
                ImpRep.Encabezado2 = "R.F.C. : " & Parametro_RFC
                'ImpRep.TituloReporte = Parametro_Direccion
                ImpRep.TituloReporte = "Cat�logo de Tipo de Herramientas"

                ImpRep.AligTituloReporte = TipoAlineacion.TdCentrado
                ImpRep.FuenteTitulo = New System.Drawing.Font("Arial", 10, FontStyle.Bold)

                ImpRep.FuenteEncabCampos = New System.Drawing.Font("Courier New", 11, FontStyle.Bold)
                ImpRep.FuenteEncabCamposOpc = New System.Drawing.Font("Courier New", 7)

                ImpRep.NomReporte = "RepCatTipoHerramienta"
                ImpRep.BandRepEsp = False
                ImpRep.FuenteLetrasChiquitas = New System.Drawing.Font("Arial", 5, FontStyle.Bold)
                'ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 7.5)
                ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 9)

                NumRenglonXHoja = 55
                NumRenglones = NumRenglonXHoja

                strSqlGlobal = "SELECT idTipoHerramienta, NomTipoHerramienta FROM CatTipoHerramienta"

                'CatTipoOrden
            Case UCase("CatTipoOrden")
                ImpRep.Encabezado1 = Parametro_Empresa
                ImpRep.Encabezado2 = "R.F.C. : " & Parametro_RFC
                'ImpRep.TituloReporte = Parametro_Direccion
                ImpRep.TituloReporte = "Cat�logo de Tipo de Orden de Servicio"

                ImpRep.AligTituloReporte = TipoAlineacion.TdCentrado
                ImpRep.FuenteTitulo = New System.Drawing.Font("Arial", 10, FontStyle.Bold)

                ImpRep.FuenteEncabCampos = New System.Drawing.Font("Courier New", 11, FontStyle.Bold)
                ImpRep.FuenteEncabCamposOpc = New System.Drawing.Font("Courier New", 7)

                ImpRep.NomReporte = "RepCatTipoOrden"
                ImpRep.BandRepEsp = False
                ImpRep.FuenteLetrasChiquitas = New System.Drawing.Font("Arial", 5, FontStyle.Bold)
                'ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 7.5)
                ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 9)

                NumRenglonXHoja = 55
                NumRenglones = NumRenglonXHoja

                strSqlGlobal = "SELECT idTipoOrden, NomTipoOrden FROM CatTipoOrden"
            Case UCase("CatTipoServicio")
                ImpRep.Encabezado1 = Parametro_Empresa
                ImpRep.Encabezado2 = "R.F.C. : " & Parametro_RFC
                'ImpRep.TituloReporte = Parametro_Direccion
                ImpRep.TituloReporte = "Cat�logo de Tipo de Servicios"

                ImpRep.AligTituloReporte = TipoAlineacion.TdCentrado
                ImpRep.FuenteTitulo = New System.Drawing.Font("Arial", 10, FontStyle.Bold)

                ImpRep.FuenteEncabCampos = New System.Drawing.Font("Courier New", 11, FontStyle.Bold)
                ImpRep.FuenteEncabCamposOpc = New System.Drawing.Font("Courier New", 7)

                ImpRep.NomReporte = "RepCatTipoServicio"
                ImpRep.BandRepEsp = False
                ImpRep.FuenteLetrasChiquitas = New System.Drawing.Font("Arial", 5, FontStyle.Bold)
                'ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 7.5)
                ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 9)

                NumRenglonXHoja = 55
                NumRenglones = NumRenglonXHoja

                strSqlGlobal = "SELECT idTipoServicio, NomTipoServicio FROM CatTipoServicio"
            Case UCase("CatFamilia")
                ImpRep.Encabezado1 = Parametro_Empresa
                ImpRep.Encabezado2 = "R.F.C. : " & Parametro_RFC
                'ImpRep.TituloReporte = Parametro_Direccion
                ImpRep.TituloReporte = "Cat�logo de Familias"

                ImpRep.AligTituloReporte = TipoAlineacion.TdCentrado
                ImpRep.FuenteTitulo = New System.Drawing.Font("Arial", 10, FontStyle.Bold)

                ImpRep.FuenteEncabCampos = New System.Drawing.Font("Courier New", 11, FontStyle.Bold)
                ImpRep.FuenteEncabCamposOpc = New System.Drawing.Font("Courier New", 7)

                ImpRep.NomReporte = "RepCatFamilia"
                ImpRep.BandRepEsp = False
                ImpRep.FuenteLetrasChiquitas = New System.Drawing.Font("Arial", 5, FontStyle.Bold)
                'ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 7.5)
                ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 9)

                NumRenglonXHoja = 55
                NumRenglones = NumRenglonXHoja

                strSqlGlobal = "SELECT FAM.IDFAMILIA, FAM.NOMFAMILIA, FAM.IDDIVISION, DIV.NOMDIVISION " &
                "FROM CATFAMILIA FAM " &
                "INNER JOIN CATDIVISION DIV ON FAM.IDDIVISION = DIV.IDDIVISION"
            Case UCase("Catciudad")
                ImpRep.Encabezado1 = Parametro_Empresa
                ImpRep.Encabezado2 = "R.F.C. : " & Parametro_RFC
                'ImpRep.TituloReporte = Parametro_Direccion
                ImpRep.TituloReporte = "Cat�logo de Ciudades"

                ImpRep.AligTituloReporte = TipoAlineacion.TdCentrado
                ImpRep.FuenteTitulo = New System.Drawing.Font("Arial", 10, FontStyle.Bold)

                ImpRep.FuenteEncabCampos = New System.Drawing.Font("Courier New", 11, FontStyle.Bold)
                ImpRep.FuenteEncabCamposOpc = New System.Drawing.Font("Courier New", 7)

                ImpRep.NomReporte = "RepCatciudad"
                ImpRep.BandRepEsp = False
                ImpRep.FuenteLetrasChiquitas = New System.Drawing.Font("Arial", 5, FontStyle.Bold)
                'ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 7.5)
                ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 9)

                NumRenglonXHoja = 55
                NumRenglones = NumRenglonXHoja

                strSqlGlobal = "select ciu.idCiudad , ciu.NomCiudad , ciu.idEstado , est.NomEstado " &
                "from Catciudad ciu " &
                "inner join catestados Est on ciu.idEstado = est.idEstado "

            Case UCase("catEstados")
                ImpRep.Encabezado1 = Parametro_Empresa
                ImpRep.Encabezado2 = "R.F.C. : " & Parametro_RFC
                'ImpRep.TituloReporte = Parametro_Direccion
                ImpRep.TituloReporte = "Cat�logo de Estados"

                ImpRep.AligTituloReporte = TipoAlineacion.TdCentrado
                ImpRep.FuenteTitulo = New System.Drawing.Font("Arial", 10, FontStyle.Bold)

                ImpRep.FuenteEncabCampos = New System.Drawing.Font("Courier New", 11, FontStyle.Bold)
                ImpRep.FuenteEncabCamposOpc = New System.Drawing.Font("Courier New", 7)

                ImpRep.NomReporte = "RepCatEstados"
                ImpRep.BandRepEsp = False
                ImpRep.FuenteLetrasChiquitas = New System.Drawing.Font("Arial", 5, FontStyle.Bold)
                'ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 7.5)
                ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 9)

                NumRenglonXHoja = 55
                NumRenglones = NumRenglonXHoja

                strSqlGlobal = "select est.idEstado, est.NomEstado, est.idPais, pais.NomPais " &
                "from catEstados Est " &
                "inner join catpais pais on est.idPais = pais.idPais "
            Case UCase("CatSucursal")
                ImpRep.Encabezado1 = Parametro_Empresa
                ImpRep.Encabezado2 = "R.F.C. : " & Parametro_RFC
                'ImpRep.TituloReporte = Parametro_Direccion
                ImpRep.TituloReporte = "Cat�logo de Sucursales"

                ImpRep.AligTituloReporte = TipoAlineacion.TdCentrado
                ImpRep.FuenteTitulo = New System.Drawing.Font("Arial", 10, FontStyle.Bold)

                ImpRep.FuenteEncabCampos = New System.Drawing.Font("Courier New", 11, FontStyle.Bold)
                ImpRep.FuenteEncabCamposOpc = New System.Drawing.Font("Courier New", 7)

                ImpRep.NomReporte = "RepCatSucursal"
                ImpRep.BandRepEsp = False
                ImpRep.FuenteLetrasChiquitas = New System.Drawing.Font("Arial", 5, FontStyle.Bold)
                'ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 7.5)
                ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 9)

                NumRenglonXHoja = 55
                NumRenglones = NumRenglonXHoja

                strSqlGlobal = "select suc.id_sucursal, suc.NomSucursal, suc.id_empresa, emp.RazonSocial " &
                "from CatSucursal Suc " &
                "inner join catempresas emp on suc.id_empresa = emp.idEmpresa "

            Case UCase("CatAlmacen")
                ImpRep.Encabezado1 = Parametro_Empresa
                ImpRep.Encabezado2 = "R.F.C. : " & Parametro_RFC
                'ImpRep.TituloReporte = Parametro_Direccion
                ImpRep.TituloReporte = "Cat�logo de Almacenes"

                ImpRep.AligTituloReporte = TipoAlineacion.TdCentrado
                ImpRep.FuenteTitulo = New System.Drawing.Font("Arial", 10, FontStyle.Bold)

                ImpRep.FuenteEncabCampos = New System.Drawing.Font("Courier New", 11, FontStyle.Bold)
                ImpRep.FuenteEncabCamposOpc = New System.Drawing.Font("Courier New", 7)

                ImpRep.NomReporte = "RepCatAlmacen"
                ImpRep.BandRepEsp = False
                ImpRep.FuenteLetrasChiquitas = New System.Drawing.Font("Arial", 5, FontStyle.Bold)
                'ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 7.5)
                ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 9)

                NumRenglonXHoja = 55
                NumRenglones = NumRenglonXHoja

                strSqlGlobal = "select alm.idAlmacen, alm.NomAlmacen, alm.idSucursal, suc.NomSucursal, alm.CIDALMACEN_COM " &
                "from CatAlmacen Alm " &
                "LEFT JOIN CatSucursal Suc ON Suc.id_sucursal = Alm.idSucursal"
            Case UCase("CatColonia")
                ImpRep.Encabezado1 = Parametro_Empresa
                ImpRep.Encabezado2 = Parametro_Direccion
                'ImpRep.TituloReporte = Parametro_Direccion
                ImpRep.TituloReporte = "Cat�logo de Almacenes"

                ImpRep.AligTituloReporte = TipoAlineacion.TdCentrado
                ImpRep.FuenteTitulo = New System.Drawing.Font("Arial", 10, FontStyle.Bold)

                ImpRep.FuenteEncabCampos = New System.Drawing.Font("Courier New", 11, FontStyle.Bold)
                ImpRep.FuenteEncabCamposOpc = New System.Drawing.Font("Courier New", 7)

                ImpRep.NomReporte = "RepCatColonia"
                ImpRep.BandRepEsp = False
                ImpRep.FuenteLetrasChiquitas = New System.Drawing.Font("Arial", 5, FontStyle.Bold)
                'ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 7.5)
                ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 9)

                NumRenglonXHoja = 55
                NumRenglones = NumRenglonXHoja

                strSqlGlobal = "select col.idColonia, col.NomColonia, col.CP, " &
                "col.idCiudad, ciu.NomCiudad, " &
                "ciu.idEstado, est.NomEstado, " &
                "est.idPais, pais.NomPais " &
                "from catcolonia col " &
                "left join CatCiudad ciu on col.idCiudad = ciu.idCiudad " &
                "left join CatEstados est on ciu.idEstado = est.idEstado " &
                "left join CatPais pais on est.idPais = pais.idPais "
            Case UCase("CatActividades")
                ImpRep.Encabezado1 = Parametro_Empresa
                ImpRep.Encabezado2 = Parametro_Direccion
                'ImpRep.TituloReporte = Parametro_Direccion
                ImpRep.TituloReporte = "Cat�logo de Actividades"

                ImpRep.AligTituloReporte = TipoAlineacion.TdCentrado
                ImpRep.FuenteTitulo = New System.Drawing.Font("Arial", 10, FontStyle.Bold)

                ImpRep.FuenteEncabCampos = New System.Drawing.Font("Courier New", 11, FontStyle.Bold)
                ImpRep.FuenteEncabCamposOpc = New System.Drawing.Font("Courier New", 7)

                ImpRep.NomReporte = "RepCatActividades"
                ImpRep.BandRepEsp = False
                ImpRep.FuenteLetrasChiquitas = New System.Drawing.Font("Arial", 5, FontStyle.Bold)
                'ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 7.5)
                ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 9)

                NumRenglonXHoja = 55
                NumRenglones = NumRenglonXHoja

                strSqlGlobal = "select act.idActividad, act.NombreAct, isnull(act.CostoManoObra,0) as CostoManoObra, isnull(act.DuracionHoras,0) as DuracionHoras, " &
                "act.idFamilia, fam.NomFamilia, fam.idDivision, div.NomDivision " &
                "from CatActividades Act " &
                "inner join CatFamilia Fam on act.idFamilia = fam.idFamilia " &
                "inner join CatDivision Div on fam.idDivision = div.idDivision "

            Case UCase("CatTipoUniTrans")
                ImpRep.Encabezado1 = Parametro_Empresa
                ImpRep.Encabezado2 = Parametro_Direccion
                'ImpRep.TituloReporte = Parametro_Direccion
                ImpRep.TituloReporte = "Cat�logo de Tipo de Unidades de Transporte"

                ImpRep.AligTituloReporte = TipoAlineacion.TdCentrado
                ImpRep.FuenteTitulo = New System.Drawing.Font("Arial", 10, FontStyle.Bold)

                ImpRep.FuenteEncabCampos = New System.Drawing.Font("Courier New", 11, FontStyle.Bold)
                ImpRep.FuenteEncabCamposOpc = New System.Drawing.Font("Courier New", 7)

                ImpRep.NomReporte = "RepCatTipoUniTrans"
                ImpRep.BandRepEsp = False
                ImpRep.FuenteLetrasChiquitas = New System.Drawing.Font("Arial", 5, FontStyle.Bold)
                'ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 7.5)
                ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 9)

                NumRenglonXHoja = 55
                NumRenglones = NumRenglonXHoja

                strSqlGlobal = "select ctut.idTipoUniTras, ctut.nomTipoUniTras, CTUT.NoLlantas, " &
                "ctut.NoEjes, ctut.TonelajeMax " &
                "from CatTipoUniTrans CTUT "

            Case UCase("CatProveedores")
                ImpRep.Encabezado1 = Parametro_Empresa
                ImpRep.Encabezado2 = Parametro_Direccion
                'ImpRep.TituloReporte = Parametro_Direccion
                ImpRep.TituloReporte = "Cat�logo de Proveedores"

                ImpRep.AligTituloReporte = TipoAlineacion.TdCentrado
                ImpRep.FuenteTitulo = New System.Drawing.Font("Arial", 10, FontStyle.Bold)

                ImpRep.FuenteEncabCampos = New System.Drawing.Font("Courier New", 11, FontStyle.Bold)
                ImpRep.FuenteEncabCamposOpc = New System.Drawing.Font("Courier New", 7)

                ImpRep.NomReporte = "RepCatProveedores"
                ImpRep.BandRepEsp = False
                ImpRep.FuenteLetrasChiquitas = New System.Drawing.Font("Arial", 5, FontStyle.Bold)
                'ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 7.5)
                ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 9)

                NumRenglonXHoja = 55
                NumRenglones = NumRenglonXHoja

                strSqlGlobal = "select pro.idProveedor, pro.RazonSocial, pro.RFCProv, pro.TipoPersona, " &
                "pro.EmailProv, pro.idColonia, col.NomColonia, col.CP, " &
                "col.idCiudad, ciu.NomCiudad, " &
                "ciu.idEstado, est.NomEstado, " &
                "est.idPais, pais.NomPais " &
                "from CatProveedores Pro " &
                "inner join CatColonia col on pro.idColonia = col.idColonia " &
                "inner join CatCiudad ciu on col.idCiudad = ciu.idCiudad " &
                "inner join CatEstados Est on ciu.idEstado = est.idEstado " &
                "inner join CatPais pais on est.idPais = pais.idPais"

            Case UCase("CatHerramientas")
                ImpRep.Encabezado1 = Parametro_Empresa
                ImpRep.Encabezado2 = Parametro_Direccion
                'ImpRep.TituloReporte = Parametro_Direccion
                ImpRep.TituloReporte = "Cat�logo de Herramientas"

                ImpRep.AligTituloReporte = TipoAlineacion.TdCentrado
                ImpRep.FuenteTitulo = New System.Drawing.Font("Arial", 10, FontStyle.Bold)

                ImpRep.FuenteEncabCampos = New System.Drawing.Font("Courier New", 11, FontStyle.Bold)
                ImpRep.FuenteEncabCamposOpc = New System.Drawing.Font("Courier New", 7)

                ImpRep.NomReporte = "RepCatHerramientas"
                ImpRep.BandRepEsp = False
                ImpRep.FuenteLetrasChiquitas = New System.Drawing.Font("Arial", 5, FontStyle.Bold)
                'ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 7.5)
                ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 9)

                NumRenglonXHoja = 55
                NumRenglones = NumRenglonXHoja

                strSqlGlobal = "select her.idHerramienta, her.NomHerramienta, her.Medida, " &
                "her.idFamilia, fam.NomFamilia, fam.idDivision, div.NomDivision, " &
                "her.idTipoHerramienta, tph.NomTipoHerramienta, " &
                "her.idMarca, mar.NombreMarca, " &
                "her.NoPiezas, her.Medida, her.modelo " &
                "from CatHerramientas Her " &
                "inner join CatFamilia fam on her.idFamilia = fam.idFamilia " &
                "inner join catdivision div on fam.idDivision = div.iddivision " &
                "inner join CatTipoHerramienta tph on her.idTipoHerramienta = tph.idTipoHerramienta " &
                "inner join CatMarcas mar on her.idMarca = mar.idMarca "

            Case UCase("CatServicios")
                ImpRep.Encabezado1 = Parametro_Empresa
                ImpRep.Encabezado2 = Parametro_Direccion
                'ImpRep.TituloReporte = Parametro_Direccion
                ImpRep.TituloReporte = "Cat�logo de Servicios"

                ImpRep.AligTituloReporte = TipoAlineacion.TdCentrado
                ImpRep.FuenteTitulo = New System.Drawing.Font("Arial", 10, FontStyle.Bold)

                ImpRep.FuenteEncabCampos = New System.Drawing.Font("Courier New", 11, FontStyle.Bold)
                ImpRep.FuenteEncabCamposOpc = New System.Drawing.Font("Courier New", 7)

                ImpRep.NomReporte = "RepCatServicios"
                ImpRep.BandRepEsp = False
                ImpRep.FuenteLetrasChiquitas = New System.Drawing.Font("Arial", 5, FontStyle.Bold)
                'ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 7.5)
                ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 9)

                NumRenglonXHoja = 55
                NumRenglones = NumRenglonXHoja


                strSqlGlobal = "SELECT ser.idServicio, " &
                "ser.idTipoServicio, " &
                "tser.NomTipoServicio, " &
                "ser.Descripcion, " &
                "ser.CadaKms, " &
                "ser.CadaTiempoDias, " &
                "ser.Estatus, " &
                "ser.Costo, " &
                "ser.idDivision, " &
                "div.NomDivision " &
                "FROM dbo.CatServicios ser " &
                "INNER JOIN dbo.CatTipoServicio tser ON tser.idTipoServicio = ser.idTipoServicio " &
                "INNER JOIN dbo.CatDivision div ON div.idDivision = ser.idDivision"

            Case UCase("CatPersonal")
                ImpRep.Encabezado1 = Parametro_Empresa
                ImpRep.Encabezado2 = Parametro_Direccion
                'ImpRep.TituloReporte = Parametro_Direccion
                ImpRep.TituloReporte = "Cat�logo de Personal"

                ImpRep.AligTituloReporte = TipoAlineacion.TdCentrado
                ImpRep.FuenteTitulo = New System.Drawing.Font("Arial", 10, FontStyle.Bold)

                ImpRep.FuenteEncabCampos = New System.Drawing.Font("Courier New", 11, FontStyle.Bold)
                ImpRep.FuenteEncabCamposOpc = New System.Drawing.Font("Courier New", 7)

                ImpRep.NomReporte = "RepCatPersonal"
                ImpRep.BandRepEsp = False
                ImpRep.FuenteLetrasChiquitas = New System.Drawing.Font("Arial", 5, FontStyle.Bold)
                'ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 7.5)
                ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 9)

                NumRenglonXHoja = 55
                NumRenglones = NumRenglonXHoja


                strSqlGlobal = "SELECT per.idPersonal, " &
                "per.idEmpresa, " &
                "emp.RazonSocial, " &
                "per.NombreCompleto, " &
                "per.Nombre, " &
                "per.ApellidoPat, " &
                "per.ApellidoMat, " &
                "per.idPuesto, " &
                "pue.NomPuesto, " &
                "per.PrecioHora, " &
                "per.Direccion, " &
                "per.idColonia, " &
                "col.NomColonia, " &
                "col.CP, " &
                "per.TelefonoPersonal, " &
                "per.Estatus, " &
                "per.idDepto, " &
                "dep.NomDepto, " &
                "per.EsTaller, " &
                "per.rfid " &
                "FROM dbo.CatPersonal per " &
                "INNER JOIN dbo.CatEmpresas emp ON emp.idEmpresa = per.idEmpresa " &
                "INNER JOIN dbo.CatPuestos pue ON pue.idPuesto = per.idPuesto " &
                "left JOIN dbo.CatColonia col ON col.idColonia = per.idColonia " &
                "left JOIN dbo.CatDeptos dep ON dep.IdDepto = per.idDepto "



            Case UCase("CatUnidadTrans")
                ImpRep.Encabezado1 = Parametro_Empresa
                ImpRep.Encabezado2 = Parametro_Direccion
                'ImpRep.TituloReporte = Parametro_Direccion
                ImpRep.TituloReporte = "Cat�logo de Unidades de Transporte"

                ImpRep.AligTituloReporte = TipoAlineacion.TdCentrado
                ImpRep.FuenteTitulo = New System.Drawing.Font("Arial", 10, FontStyle.Bold)

                ImpRep.FuenteEncabCampos = New System.Drawing.Font("Courier New", 11, FontStyle.Bold)
                ImpRep.FuenteEncabCamposOpc = New System.Drawing.Font("Courier New", 7)

                ImpRep.NomReporte = "RepCatUnidadTrans"
                ImpRep.BandRepEsp = True
                ImpRep.FuenteLetrasChiquitas = New System.Drawing.Font("Arial", 5, FontStyle.Bold)
                'ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 7.5)
                ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 9)

                NumRenglonXHoja = 55
                NumRenglones = NumRenglonXHoja

                'strSqlGlobal = "SELECT uni.idUnidadTrans, " &
                '"uni.descripcionUni, " &
                '"uni.idTipoUnidad, " &
                '"tuni.nomTipoUniTras, " &
                '"tuni.clasificacion, " &
                '"uni.idMarca, " &
                '"mar.NombreMarca, " &
                '"uni.idEmpresa, " &
                '"emp.RazonSocial, " &
                '"uni.Estatus " &
                '"FROM dbo.CatUnidadTrans uni " &
                '"left JOIN dbo.CatTipoUniTrans tuni ON tuni.idTipoUniTras = uni.idTipoUnidad " &
                '"left JOIN dbo.CatMarcas mar ON mar.idMarca = uni.idMarca " &
                '"left JOIN dbo.CatEmpresas emp ON emp.idEmpresa = uni.idEmpresa"

                '
            Case UCase("CatTipoLlanta")
                ImpRep.Encabezado1 = Parametro_Empresa
                ImpRep.Encabezado2 = "R.F.C. : " & Parametro_RFC
                'ImpRep.TituloReporte = Parametro_Direccion
                ImpRep.TituloReporte = "Cat�logo de Tipo de Llantas"

                ImpRep.AligTituloReporte = TipoAlineacion.TdCentrado
                ImpRep.FuenteTitulo = New System.Drawing.Font("Arial", 10, FontStyle.Bold)

                ImpRep.FuenteEncabCampos = New System.Drawing.Font("Courier New", 11, FontStyle.Bold)
                ImpRep.FuenteEncabCamposOpc = New System.Drawing.Font("Courier New", 7)

                ImpRep.NomReporte = "RepCatTipoLlanta"
                ImpRep.BandRepEsp = False
                ImpRep.FuenteLetrasChiquitas = New System.Drawing.Font("Arial", 5, FontStyle.Bold)
                'ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 7.5)
                ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 9)

                NumRenglonXHoja = 55
                NumRenglones = NumRenglonXHoja

                strSqlGlobal = "SELECT  idTipoLLanta, Descripcion FROM dbo.CatTipoLlanta"

            Case UCase("CatClasLlan")
                ImpRep.Encabezado1 = Parametro_Empresa
                ImpRep.Encabezado2 = "R.F.C. : " & Parametro_RFC
                'ImpRep.TituloReporte = Parametro_Direccion
                ImpRep.TituloReporte = "Cat�logo de Clasificacion de Llantas"

                ImpRep.AligTituloReporte = TipoAlineacion.TdCentrado
                ImpRep.FuenteTitulo = New System.Drawing.Font("Arial", 10, FontStyle.Bold)

                ImpRep.FuenteEncabCampos = New System.Drawing.Font("Courier New", 11, FontStyle.Bold)
                ImpRep.FuenteEncabCamposOpc = New System.Drawing.Font("Courier New", 7)

                ImpRep.NomReporte = "RepCatClasLlan"
                ImpRep.BandRepEsp = False
                ImpRep.FuenteLetrasChiquitas = New System.Drawing.Font("Arial", 5, FontStyle.Bold)
                'ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 7.5)
                ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 9)

                NumRenglonXHoja = 55
                NumRenglones = NumRenglonXHoja

                strSqlGlobal = "SELECT idClasLlan,NombreClasLlan,NoEjes, NoLLantas FROM dbo.CatClasLlan "
            Case UCase("CatTipoOsComb")
                ImpRep.Encabezado1 = Parametro_Empresa
                ImpRep.Encabezado2 = "R.F.C. : " & Parametro_RFC
                'ImpRep.TituloReporte = Parametro_Direccion
                ImpRep.TituloReporte = "Cat�logo de Tipo de Combustible Extra"

                ImpRep.AligTituloReporte = TipoAlineacion.TdCentrado
                ImpRep.FuenteTitulo = New System.Drawing.Font("Arial", 10, FontStyle.Bold)

                ImpRep.FuenteEncabCampos = New System.Drawing.Font("Courier New", 11, FontStyle.Bold)
                ImpRep.FuenteEncabCamposOpc = New System.Drawing.Font("Courier New", 7)

                ImpRep.NomReporte = "RepCatTipoOsComb"
                ImpRep.BandRepEsp = False
                ImpRep.FuenteLetrasChiquitas = New System.Drawing.Font("Arial", 5, FontStyle.Bold)
                'ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 7.5)
                ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 9)

                NumRenglonXHoja = 55
                NumRenglones = NumRenglonXHoja

                strSqlGlobal = "SELECT tce.idTipoOSComb, " &
        "tce.NomTipoOSComb, " &
        "CASE WHEN tce.RequiereViaje = 1 THEN 'SI' ELSE 'NO' END ReqViaje, " &
        "CASE WHEN tce.TipoUnidad = 'RE' THEN 'REMOLQUE' WHEN tce.TipoUnidad = 'TC' THEN 'TRACTOR' END AS TipoUnidad " &
        "FROM CatTipoOsComb tce"


        End Select

        ImpRep.StrSql = strSqlGlobal

        'Case UCase("REMISION")
        '    '14/DIC/2015
        '    'ImpRep.Encabezado1 = Parametro_Empresa
        '    'ImpRep.Encabezado2 = "R.F.C. : " & Parametro_RFC
        '    'ImpRep.TituloReporte = Parametro_Direccion
        '    ImpRep.Encabezado1 = ""
        '    ImpRep.Encabezado2 = ""
        '    ImpRep.TituloReporte = ""


        '    ImpRep.AligTituloReporte = TipoAlineacion.TdIzquierda
        '    ImpRep.FuenteTitulo = New System.Drawing.Font("Arial", 10, FontStyle.Bold)

        '    ImpRep.FuenteEncabCampos = New System.Drawing.Font("Courier New", 11, FontStyle.Bold)
        '    ImpRep.FuenteEncabCamposOpc = New System.Drawing.Font("Courier New", 7)

        '    ImpRep.NomReporte = "RepRemision"
        '    ImpRep.BandRepEsp = True
        '    ImpRep.FuenteLetrasChiquitas = New System.Drawing.Font("Arial", 5, FontStyle.Bold)
        '    'ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 7.5)
        '    ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 9)

        '    NumRenglonXHoja = 28
        '    NumRenglones = NumRenglonXHoja
        'Case UCase("TRASPFALTANTES"), UCase("TRASPFALTANTESINI")
        '    ImpRep.Encabezado1 = ""
        '    ImpRep.Encabezado2 = ""
        '    If UCase(Tabla) = UCase("TRASPFALTANTES") Then
        '        ImpRep.TituloReporte = "REPORTE DE TRASPASOS FALTANTES"
        '    ElseIf UCase(Tabla) = UCase("TRASPFALTANTESINI") Then
        '        ImpRep.TituloReporte = "REPORTE DE TRASPASOS FALTANTES INICIALES"
        '    End If

        '    ImpRep.AligTituloReporte = TipoAlineacion.TdCentrado

        '    ImpRep.FuenteTitulo = New System.Drawing.Font("Arial", 10, FontStyle.Bold)
        '    'ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 9)
        '    ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 7)
        '    ImpRep.FuenteLetrasChiquitas = New System.Drawing.Font("Courier New", 6.5)
        '    ImpRep.FuenteLetrasChiquitasEnc = New System.Drawing.Font("Courier New", 6.5, FontStyle.Bold)

        '    ImpRep.FuenteEncabCamposOpc = New System.Drawing.Font("Courier New", 7)

        '    ImpRep.NomReporte = "Rep" & Tabla
        '    ImpRep.BandRepEsp = True
        '    'ImpRep.FuenteLetrasChiquitas = New System.Drawing.Font("Arial", 5, FontStyle.Bold)

        '    NumRenglonXHoja = 55
        '    NumRenglones = NumRenglonXHoja

        'If UCase(Tabla) = UCase("CatPremios") Then
        '    'strSqlGlobal = "select IdPremio,Descripcion,ValorPuntos,ValorPesos from CatPremios"

        '    'ImpRep.NomReporte = "RepCatPremios"

        'ElseIf UCase(Tabla) = UCase("CatPromociones") Then
        '    'strSqlGlobal = "SELECT IDPROMOCION,DESCRIPCION, " & _
        '    '"DESCR.NOMDESCRIPCION ,FECHAINICIAL,FECHAFINAL,CAMBIAPREMIO " & _
        '    '"FROM CATPROMOCIONES PRO " & _
        '    '"INNER JOIN DESCRIPCIONES DESCR ON PRO.IDTIPO = DESCR.IDDESCRIPCION AND DESCR.TIPO = 'TIPPRO'"
        'ElseIf UCase(Tabla) = UCase("CLIENTES") Then
        '    'strSqlGlobal = "SELECT TAR.IDTARJETA, TAR.CCODIGOC01, TAR.CRAZONSO01, TAR.TELEFONO, TAR.EMAIL FROM CATTARJETAS TAR"

        '    'ImpRep.NomReporte = "RepClientes"
        'ElseIf UCase(Tabla) = UCase("EDOCTACLI") Then

        '    'ImpRep.NomReporte = "RepEdoCtaCli"
        '    'ImpRep.BandRepEsp = True

        '    '3/ABRIL/2014
        'ElseIf UCase(Tabla) = UCase("MOVTOS") Then
        '    'ImpRep.Encabezado2 = "R.F.C. : " & Parametro_RFC
        '    'ImpRep.TituloReporte = Parametro_Direccion
        '    'ImpRep.AligTituloReporte = TipoAlineacion.TdIzquierda
        '    'ImpRep.FuenteTitulo = New System.Drawing.Font("Arial", 10, FontStyle.Bold)

        '    'ImpRep.FuenteEncabCamposOpc = New System.Drawing.Font("Courier New", 7)

        '    'ImpRep.NomReporte = "RepMovtos"
        '    'ImpRep.BandRepEsp = True
        '    'ImpRep.FuenteLetrasChiquitas = New System.Drawing.Font("Arial", 5, FontStyle.Bold)
        '    'ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 9)

        '    'NumRenglonXHoja = 45
        '    'NumRenglones = NumRenglonXHoja
        '    'PEDIDOSPEN
        'ElseIf UCase(Tabla) = UCase("ORDXEST") Then
        '    'ImpRep.Encabezado2 = "R.F.C. : " & Parametro_RFC
        '    'ImpRep.TituloReporte = Parametro_Direccion
        '    'ImpRep.AligTituloReporte = TipoAlineacion.TdIzquierda
        '    'ImpRep.FuenteTitulo = New System.Drawing.Font("Arial", 10, FontStyle.Bold)

        '    'ImpRep.FuenteEncabCamposOpc = New System.Drawing.Font("Courier New", 7)

        '    'ImpRep.NomReporte = "RepOrdXEst"
        '    'ImpRep.BandRepEsp = True
        '    'ImpRep.FuenteLetrasChiquitas = New System.Drawing.Font("Arial", 5, FontStyle.Bold)
        '    'ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 9)

        '    'NumRenglonXHoja = 45
        '    'NumRenglones = NumRenglonXHoja
        'ElseIf UCase(Tabla) = UCase("ORDENES") Then
        '    'ImpRep.Encabezado2 = "R.F.C. : " & Parametro_RFC
        '    'ImpRep.TituloReporte = Parametro_Direccion
        '    'ImpRep.AligTituloReporte = TipoAlineacion.TdIzquierda
        '    'ImpRep.FuenteTitulo = New System.Drawing.Font("Arial", 10, FontStyle.Bold)

        '    'ImpRep.FuenteEncabCamposOpc = New System.Drawing.Font("Courier New", 7)

        '    'ImpRep.NomReporte = "RepOrdenes"
        '    'ImpRep.BandRepEsp = True
        '    'ImpRep.FuenteLetrasChiquitas = New System.Drawing.Font("Arial", 5, FontStyle.Bold)
        '    'ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 9)

        '    'NumRenglonXHoja = 45
        '    'NumRenglones = NumRenglonXHoja
        'ElseIf UCase(Tabla) = UCase("PRODMASVEND") Then
        '    'ImpRep.Encabezado2 = "R.F.C. : " & Parametro_RFC
        '    'ImpRep.TituloReporte = Parametro_Direccion
        '    'ImpRep.AligTituloReporte = TipoAlineacion.TdIzquierda
        '    'ImpRep.FuenteTitulo = New System.Drawing.Font("Arial", 10, FontStyle.Bold)

        '    'ImpRep.FuenteEncabCamposOpc = New System.Drawing.Font("Courier New", 7)

        '    'ImpRep.NomReporte = "RepProdMasVend"
        '    'ImpRep.BandRepEsp = True
        '    'ImpRep.FuenteLetrasChiquitas = New System.Drawing.Font("Arial", 5, FontStyle.Bold)
        '    'ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 9)

        '    'NumRenglonXHoja = 45
        '    'NumRenglones = NumRenglonXHoja
        'ElseIf UCase(Tabla) = UCase("PRODVEND") Then
        '    'ImpRep.Encabezado2 = "R.F.C. : " & Parametro_RFC
        '    'ImpRep.TituloReporte = Parametro_Direccion
        '    'ImpRep.AligTituloReporte = TipoAlineacion.TdIzquierda
        '    'ImpRep.FuenteTitulo = New System.Drawing.Font("Arial", 10, FontStyle.Bold)

        '    'ImpRep.FuenteEncabCamposOpc = New System.Drawing.Font("Courier New", 7)

        '    'ImpRep.NomReporte = "RepProdVend"
        '    'ImpRep.BandRepEsp = True
        '    'ImpRep.FuenteLetrasChiquitas = New System.Drawing.Font("Arial", 5, FontStyle.Bold)
        '    'ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 9)

        '    'NumRenglonXHoja = 45
        '    'NumRenglones = NumRenglonXHoja
        'ElseIf UCase(Tabla) = UCase("REMISION") Then
        '    '14/DIC/2015
        '    'ImpRep.Encabezado1 = Parametro_Empresa
        '    'ImpRep.Encabezado2 = "R.F.C. : " & Parametro_RFC
        '    'ImpRep.TituloReporte = Parametro_Direccion
        '    ImpRep.Encabezado1 = ""
        '    ImpRep.Encabezado2 = ""
        '    ImpRep.TituloReporte = ""


        '    ImpRep.AligTituloReporte = TipoAlineacion.TdIzquierda
        '    ImpRep.FuenteTitulo = New System.Drawing.Font("Arial", 10, FontStyle.Bold)

        '    ImpRep.FuenteEncabCampos = New System.Drawing.Font("Courier New", 11, FontStyle.Bold)
        '    ImpRep.FuenteEncabCamposOpc = New System.Drawing.Font("Courier New", 7)

        '    ImpRep.NomReporte = "RepRemision"
        '    ImpRep.BandRepEsp = True
        '    ImpRep.FuenteLetrasChiquitas = New System.Drawing.Font("Arial", 5, FontStyle.Bold)
        '    'ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 7.5)
        '    ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 9)

        '    NumRenglonXHoja = 28
        '    NumRenglones = NumRenglonXHoja
        'ElseIf UCase(Tabla) = UCase("TRASPFALTANTES") Then
        '    'ImpRep.Encabezado2 = "R.F.C. : " & Parametro_RFC
        '    'ImpRep.TituloReporte = Parametro_Direccion

        '    ImpRep.Encabezado1 = ""
        '    ImpRep.Encabezado2 = ""
        '    ImpRep.TituloReporte = "REPORTE DE TRASPASOS FALTANTES"
        '    ImpRep.AligTituloReporte = TipoAlineacion.TdCentrado

        '    ImpRep.FuenteTitulo = New System.Drawing.Font("Arial", 10, FontStyle.Bold)
        '    'ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 9)
        '    ImpRep.FuenteImprimeCampos = New System.Drawing.Font("Courier New", 7)
        '    ImpRep.FuenteLetrasChiquitas = New System.Drawing.Font("Courier New", 6.5)
        '    ImpRep.FuenteLetrasChiquitasenc = New System.Drawing.Font("Courier New", 6.5, FontStyle.Bold)

        '    ImpRep.FuenteEncabCamposOpc = New System.Drawing.Font("Courier New", 7)

        '    ImpRep.NomReporte = "RepTraspFaltantes"
        '    ImpRep.BandRepEsp = True
        '    'ImpRep.FuenteLetrasChiquitas = New System.Drawing.Font("Arial", 5, FontStyle.Bold)

        '    NumRenglonXHoja = 55
        '    NumRenglones = NumRenglonXHoja



        'ElseIf UCase(Tabla) = UCase("LISTAERROR") Then
        '    ImpRep.TituloReporte = UCase(vTituloReporte)
        '    ImpRep.AligTituloReporte = TipoAlineacion.TdCentrado
        '    ImpRep.FuenteTitulo = New System.Drawing.Font("Arial", 10, FontStyle.Bold)
        '    'ImpRep.TituloReporte = "PEDIDO"
        '    'ImpRep.BandRepEsp = True
        '    ImpRep.NomReporte = "RepListaError"
        'End If

    End Sub

    Private Sub CreaTabla(ByVal vnomtabla As String, ByVal CampoId As String, ByVal CampoIdTipo As String, ByVal CampoDesc As String, ByVal CampoDescTipo2 As String)
        MyTablaImprime = New DataTable(vnomtabla)

        myDataColumn = New DataColumn
        'myDataColumn.DataType = System.Type.GetType("System.Int32")
        myDataColumn.DataType = System.Type.GetType(CampoIdTipo)
        myDataColumn.ColumnName = CampoId
        myDataColumn.ReadOnly = False
        myDataColumn.Unique = False
        MyTablaImprime.Columns.Add(myDataColumn)


        myDataColumn = New DataColumn
        'myDataColumn.DataType = System.Type.GetType("System.String")
        myDataColumn.DataType = System.Type.GetType(CampoDescTipo2)
        myDataColumn.ColumnName = CampoDesc
        myDataColumn.ReadOnly = False
        myDataColumn.Unique = False
        MyTablaImprime.Columns.Add(myDataColumn)
    End Sub

    Public Sub RecibeDatos(ByVal Valor1 As String, ByVal Valor2 As String, Optional ByRef vDataSet As DataTable = Nothing, Optional ByVal Valor3 As String = "")
        'Public Sub RecibeDatos(ByVal Valor1 As String, ByVal NomTabla As String, ByVal vConStr As String)
        'Inicio.DespliegaReporte(vNomTabla, vConStr)
        'vNomTabla = NomTabla
        vValor1 = Valor1
        vValor2 = Valor2
        vValor3 = Valor3
        'DsRep.Tables(vNomTabla).DataSet = vDataSet
        'DsRep = vDataSet
        DsRep.Clear()
        'DsRep.Dispose()
        'DsRep = New DataSet
        'DataTable dt1 = new DataTable();
        Dim dt1 As New DataTable()
        If Not vDataSet Is Nothing Then
            dt1 = vDataSet.Copy
            DsRep.Tables.Add(dt1)

        End If

        'ImpRep = New ImprimeReporte
        'CreaReportes(vNomTabla)
        'Me.Text = Me.Text & " - " & vTituloReporte
    End Sub


    Private Sub frmReporteador_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try


            ImpRep = New ImprimeReporte
            CreaReportes(vNomTabla)
            Me.Text = Me.Text & " - " & vTituloReporte
            If vNomTabla = UCase("PEDIDOSPEN") Then
                If vValor1 <> "" Then
                    CmbFiltro(0).Enabled = False
                    'CmbCadena(0).SelectedValue = CInt(vValor1)
                    'CmbCadena(0).Enabled = False
                    txtNumerico1(0).Text = CInt(vValor1)
                    txtNumerico1(0).Enabled = False

                    'CmbCadena(0).SelectedValue = vValor2
                    'GetControl(1, 0).Text =
                End If
            ElseIf vNomTabla = UCase("REMISION") Then
                If vValor1 <> "" Then
                    'CmbCadena(1).SelectedValue = CInt(vValor2)
                    'CmbCadena(1).Enabled = False
                    'txtNumerico1(2).Text = CInt(vValor2)
                    'txtNumerico1(2).Enabled = False
                    'txtLike(0).Text = vValor1
                    'txtLike(0).Enabled = False
                    'txtLike(1).Text = vValor3
                    'txtLike(1).Enabled = False
                    'CmbFiltro(2).Enabled = False
                    'txtNumerico1(0).Enabled = False
                    'CmbCadena(0).Enabled = False
                    'If vValor3 <> "" Then
                    '    'txtNumerico1(0).Text = CInt(vValor3)
                    '    CmbCadena(0).SelectedValue = vValor3
                    'End If
                    CmbCadena(0).SelectedValue = vValor1
                    CmbCadena(0).Enabled = False
                End If

                'txt(0).Text = vValor1
            End If
        Catch ex As Exception
            'MsgBox("error AQUI")
            'On Error Resume Next
            MessageBox.Show("Error: " & ex.Message, "Error: " & Err.Number, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    'DENTRO DEL CONFIGREP ( ANT ANT)
    '_ArrColClass = vArrCol
    'ReDim LDescripcion(_ArrColClass.Length - 1)
    'ReDim CmbCadena(_ArrColClass.Length - 1)
    'ReDim txtLike(_ArrColClass.Length - 1)
    'ReDim txtNumerico1(_ArrColClass.Length - 1)
    'ReDim txtNumerico2(_ArrColClass.Length - 1)
    'ReDim CaleFecha1(_ArrColClass.Length - 1)
    'ReDim CaleFecha2(_ArrColClass.Length - 1)
    'ReDim rdbLogico1(_ArrColClass.Length - 1)
    'ReDim rdbLogico2(_ArrColClass.Length - 1)
    'ReDim PanelLogico1(_ArrColClass.Length - 1)
    'ReDim GrupoLogico1(_ArrColClass.Length - 1)

    'Dim _TopMarg As Integer = 10, _Espaciado As Integer = 25
    'Dim _IniSegundaFila As Integer = 155


    'Try
    '    CreaTablaOrden()
    '    PanelControles.Controls.Clear()
    '    Dim ipos As Integer = 0
    '    For i As Integer = 0 To _ArrColClass.Length - 1
    '        With _ArrColClass(i)

    '            If .BOrdenCampo Then
    '                'CREA GRID DE ORDENAMINETO
    '                myDataRow = MyTablaOrden.NewRow()
    '                If i = 0 Then
    '                    myDataRow("Activo") = True
    '                Else
    '                    myDataRow("Activo") = False
    '                End If

    '                myDataRow("Columna") = .DesripcionCampo
    '                'myDataRow("Orden") = "Ascendente"
    '                myDataRow("Orden") = DeterminaNombreTipoOrden(.BOrdenCampoTipo)
    '                myDataRow("Campo") = .Campo
    '                myDataRow("Posicion") = i + 1
    '                MyTablaOrden.Rows.Add(myDataRow)
    '            End If
    '            If .BFiltraCampo Then


    '                'L A B E L S
    '                LDescripcion(i) = New Label
    '                LDescripcion(i).AutoSize = True
    '                LDescripcion(i).Name = "L" & .Campo
    '                LDescripcion(i).Text = .DesripcionCampo
    '                LDescripcion(i).ForeColor = Color.Black
    '                LDescripcion(i).Location = New Point(5, _TopMarg + _Espaciado * ipos)
    '                'AGREGA EVENTO
    '                AddHandler LDescripcion(i).Click, AddressOf Labels_Click
    '                PanelControles.Controls.Add(LDescripcion(i))

    '                'CADENA
    '                If .TipoControlCampo = TipoControlReport.TdCadena Then
    '                    CmbCadena(i) = New ComboBox 'Se agarra el Ultimo Valor del Arreglo
    '                    CmbCadena(i).Name = "CmbCad" & .Campo
    '                    CmbCadena(i).Text = "cmbCadena"
    '                    'CmbCadena(i).Items.AddRange({"Filtro1", "Filtro2", "Filtro3", "Filtro4", "Etc"})
    '                    'LlenaCombo
    '                    DsLista = Inicio.LlenaCombos(.Campo, "Orden", .Campo, TipoConexion.TcSQL, Inicio.CONSTR, Process.GetCurrentProcess.ProcessName, _
    '                                                 KEY_CHECAERROR, KEY_ESTATUS, "", IIf(.StrSqlOpc <> "", .StrSqlOpc, DeterminaStrCombo(.Campo, vNomTabla)))

    '                    If Not DsLista Is Nothing Then
    '                        If DsLista.Tables(.Campo).DefaultView.Count > 0 Then
    '                            CmbCadena(i).DataSource = DsLista.Tables(.Campo)
    '                            CmbCadena(i).ValueMember = "orden"
    '                            CmbCadena(i).DisplayMember = .Campo

    '                        End If
    '                    End If

    '                    CmbCadena(i).ForeColor = Color.Black
    '                    CmbCadena(i).Location = New Point(_IniSegundaFila, _TopMarg - 5 + _Espaciado * ipos)
    '                    CmbCadena(i).Size = New System.Drawing.Size(200, 21)
    '                    AddHandler CmbCadena(i).SelectedIndexChanged, AddressOf Combos_SelectedIndexChanged
    '                    PanelControles.Controls.Add(CmbCadena(i))
    '                ElseIf .TipoControlCampo = TipoControlReport.TdLikeCadena Then
    '                    txtLike(i) = New TextBox 'Se agarra el Ultimo Valor del Arreglo
    '                    txtLike(i).Name = "txtLike" & .Campo
    '                    txtLike(i).Text = "txtLike"
    '                    txtLike(i).Width = 150
    '                    txtLike(i).ForeColor = Color.Black
    '                    txtLike(i).Location = New Point(_IniSegundaFila, _TopMarg - 5 + _Espaciado * ipos)
    '                    AddHandler txtLike(i).Click, AddressOf TxtLike_Click 'Agrega evento Click
    '                    PanelControles.Controls.Add(txtLike(i)) 'Agrega el Control al Panel
    '                ElseIf .TipoControlCampo = TipoControlReport.TdFecha Then
    '                    CaleFecha1(i) = New DateTimePicker
    '                    CaleFecha1(i).Name = "Cale" & .Campo
    '                    CaleFecha1(i).Value = Now
    '                    CaleFecha1(i).Format = DateTimePickerFormat.Long
    '                    CaleFecha1(i).CalendarForeColor = Color.Black
    '                    CaleFecha1(i).Location = New Point(_IniSegundaFila, _TopMarg - 5 + _Espaciado * ipos)
    '                    CaleFecha1(i).Size = New System.Drawing.Size(200, 20)

    '                    AddHandler CaleFecha1(i).ValueChanged, AddressOf DateTimePickers_ValueChanged
    '                    PanelControles.Controls.Add(CaleFecha1(i))

    '                    Dim LEntre As New Label
    '                    LEntre.Name = "LEntre"
    '                    LEntre.Text = " AL "
    '                    LEntre.AutoSize = True
    '                    LEntre.Location = New Point(_IniSegundaFila + CaleFecha1(i).Width, (_TopMarg - 5 + _Espaciado * ipos) + 5)
    '                    PanelControles.Controls.Add(LEntre)

    '                    CaleFecha2(i) = New DateTimePicker
    '                    CaleFecha2(i).Name = "Cale2" & .Campo
    '                    CaleFecha2(i).Value = Now
    '                    CaleFecha2(i).Format = DateTimePickerFormat.Long
    '                    CaleFecha2(i).CalendarForeColor = Color.Black
    '                    CaleFecha2(i).Location = New Point(_IniSegundaFila + 25 + CaleFecha1(i).Width, _TopMarg - 5 + _Espaciado * ipos)
    '                    CaleFecha2(i).Size = New System.Drawing.Size(200, 20)

    '                    AddHandler CaleFecha2(i).ValueChanged, AddressOf DateTimePickers_ValueChanged
    '                    PanelControles.Controls.Add(CaleFecha2(i))
    '                ElseIf .TipoControlCampo = TipoControlReport.TdBoolean Then
    '                    'GrupoLogico1(i) = New GroupBox
    '                    'GrupoLogico1(i).Name = "Grupo" & .Campo
    '                    'GrupoLogico1(i).Text = ""
    '                    'GrupoLogico1(i).Size = New System.Drawing.Size(152, 25)
    '                    'GrupoLogico1(i).Location = New Point(_IniSegundaFila, _TopMarg - 5 + _Espaciado * i)
    '                    'PanelControles.Controls.Add(GrupoLogico1(i))
    '                    strLogico = .DesValorTrue
    '                    Tama�oLogico = Len(strLogico)
    '                    strLogico = .DesValorFalse
    '                    Tama�oLogico = Tama�oLogico + Len(strLogico)

    '                    PanelLogico1(i) = New Panel
    '                    PanelLogico1(i).Name = "Panel" & .Campo
    '                    PanelLogico1(i).Text = ""
    '                    'PanelLogico1(i).Size = New System.Drawing.Size(152, 25)
    '                    PanelLogico1(i).Size = New System.Drawing.Size(Tama�oLogico * 10, 25)
    '                    PanelLogico1(i).Location = New Point(_IniSegundaFila, _TopMarg - 5 + _Espaciado * ipos)
    '                    PanelControles.Controls.Add(PanelLogico1(i))

    '                    rdbLogico1(i) = New RadioButton
    '                    rdbLogico1(i).Name = "rdb" & .Campo
    '                    rdbLogico1(i).Text = .DesValorTrue
    '                    rdbLogico1(i).AutoSize = True
    '                    rdbLogico1(i).Checked = True
    '                    'rdbLogico1(i).Location = New Point(_IniSegundaFila, _TopMarg - 5 + _Espaciado * i)
    '                    rdbLogico1(i).Size = New System.Drawing.Size(75, 17)
    '                    rdbLogico1(i).Location = New Point(6, 4)
    '                    '75, 17
    '                    AddHandler rdbLogico1(i).Click, AddressOf RadioButtons_Click
    '                    'PanelControles.Controls.Add(rdbLogico1(i))
    '                    PanelLogico1(i).Controls.Add(rdbLogico1(i))

    '                    rdbLogico2(i) = New RadioButton
    '                    rdbLogico2(i).Name = "rdb" & .Campo
    '                    rdbLogico2(i).Text = .DesValorFalse
    '                    rdbLogico2(i).AutoSize = True
    '                    rdbLogico2(i).Checked = False
    '                    rdbLogico2(i).Size = New System.Drawing.Size(75, 17)
    '                    'rdbLogico2(i).Location = New Point(85, 4)
    '                    rdbLogico2(i).Location = New Point(15 + rdbLogico1(i).Width, 4)
    '                    AddHandler rdbLogico2(i).Click, AddressOf RadioButtons_Click
    '                    'PanelControles.Controls.Add(rdbLogico2(i))
    '                    PanelLogico1(i).Controls.Add(rdbLogico2(i))




    '                ElseIf .TipoControlCampo = TipoControlReport.TdNumerico Then
    '                    txtNumerico1(i) = New TextBox
    '                    txtNumerico1(i).Name = "txtNum" & .Campo
    '                    txtNumerico1(i).Text = ""
    '                    txtNumerico1(i).ForeColor = Color.Black
    '                    txtNumerico1(i).Location = New Point(_IniSegundaFila, _TopMarg - 5 + _Espaciado * ipos)
    '                    AddHandler txtNumerico1(i).KeyPress, AddressOf TxtNum_KeyPress
    '                    PanelControles.Controls.Add(txtNumerico1(i))
    '                ElseIf .TipoControlCampo = TipoControlReport.TdRangoNum Then
    '                    txtNumerico1(i) = New TextBox
    '                    txtNumerico1(i).Name = "txtRng1" & .Campo
    '                    txtNumerico1(i).Text = "Rango1"
    '                    txtNumerico1(i).ForeColor = Color.Black
    '                    txtNumerico1(i).Location = New Point(_IniSegundaFila, _TopMarg - 5 + _Espaciado * ipos)
    '                    AddHandler txtNumerico1(i).KeyPress, AddressOf TxtNum_KeyPress
    '                    PanelControles.Controls.Add(txtNumerico1(i))

    '                    Dim LEntre As New Label
    '                    LEntre.Name = "LEntre"
    '                    LEntre.Text = "Y"
    '                    LEntre.AutoSize = True
    '                    LEntre.Location = New Point(_IniSegundaFila + txtNumerico1(i).Width, _TopMarg - 5 + _Espaciado * ipos)
    '                    PanelControles.Controls.Add(LEntre)

    '                    txtNumerico2(i) = New TextBox
    '                    txtNumerico2(i).Name = "txtRng2" & .Campo
    '                    txtNumerico2(i).Text = "Rango2"
    '                    txtNumerico2(i).ForeColor = Color.Black
    '                    txtNumerico2(i).Location = New Point(_IniSegundaFila + 15 + txtNumerico1(i).Width, _TopMarg - 5 + _Espaciado * ipos)
    '                    AddHandler txtNumerico2(i).KeyPress, AddressOf TxtNum_KeyPress
    '                    PanelControles.Controls.Add(txtNumerico2(i))
    '                End If
    '            Else
    '                Continue For
    '            End If

    '        End With
    '        ipos += 1
    '    Next
    '    LlenaGrid()
    'Catch ex As Exception
    '    MessageBox.Show("Error: " & ex.Message, "Error: " & Err.Number, MessageBoxButtons.OK, MessageBoxIcon.Error)
    'End Try

    Public Sub ConfigRep(ByVal vArrCol() As ColClass)
        'IIf(.TipofiltroCadena =ColClass.EnTipoFilCadena.Obligatorio,True,false )

        _ArrColClass = vArrCol
        ReDim LDescripcion(_ArrColClass.Length - 1)
        ReDim CmbCadena(_ArrColClass.Length - 1)
        ReDim btnAgre(_ArrColClass.Length - 1)
        ReDim CmbFiltroIn(_ArrColClass.Length - 1)
        ReDim txtLike(_ArrColClass.Length - 1)
        ReDim txtNumerico1(_ArrColClass.Length - 1)
        ReDim txtNumerico2(_ArrColClass.Length - 1)
        ReDim CaleFecha1(_ArrColClass.Length - 1)
        ReDim CaleFecha2(_ArrColClass.Length - 1)
        ReDim rdbLogico1(_ArrColClass.Length - 1)
        ReDim rdbLogico2(_ArrColClass.Length - 1)
        ReDim PanelLogico1(_ArrColClass.Length - 1)
        ReDim GrupoLogico1(_ArrColClass.Length - 1)
        ReDim CmbFiltro(_ArrColClass.Length - 1)
        ReDim ObjCnt(-1)
        Dim _TopMarg As Integer = 10, _Espaciado As Integer = 25
        Dim _IniSegundaFila As Integer = 155
        Try
            CreaTablaOrden()
            PanelControles.Controls.Clear()
            Dim ipos As Integer = 0, iPos2 As Integer = 0 'El 2 es para controles Especiales
            Dim BndOrdena As Boolean = False, BndFiltra As Boolean = False, BndContLibre As Boolean = False
            For i As Integer = 0 To _ArrColClass.Length - 1
                With _ArrColClass(i)
                    If .BOrdenCampo Then
                        'CREA GRID DE ORDENAMINETO
                        BndOrdena = True
                        myDataRow = MyTablaOrden.NewRow()

                        If .ActivaOrdenCampo Then
                            myDataRow("Activo") = True
                        Else
                            myDataRow("Activo") = False
                        End If
                        'If i = 0 Then
                        '    myDataRow("Activo") = True
                        'Else
                        '    myDataRow("Activo") = False
                        'End If
                        myDataRow("Columna") = .DesripcionCampo
                        'myDataRow("Orden") = "Ascendente"
                        myDataRow("Orden") = DeterminaNombreTipoOrden(.BOrdenCampoTipo)
                        myDataRow("Campo") = .Campo
                        myDataRow("Prefijo") = .Prefijo
                        myDataRow("Posicion") = i + 1
                        MyTablaOrden.Rows.Add(myDataRow)
                    End If
                    If .EsControlLibre Then
                        BndContLibre = True
                        LDescripcion(i) = New Label
                        LDescripcion(i).AutoSize = True
                        LDescripcion(i).Name = "L" & .Campo
                        LDescripcion(i).Text = .DesripcionCampo
                        LDescripcion(i).ForeColor = Color.Black
                        LDescripcion(i).Location = New Point(5, _TopMarg + _Espaciado * iPos2)
                        PanelCntExtras.Controls.Add(LDescripcion(i))
                        ReDim Preserve ObjCnt(ObjCnt.Length)
                        ObjCnt(ObjCnt.Length - 1) = .ControlLibre
                        For j As Integer = 0 To .ControlLibre.Length - 1
                            If j > 0 Then
                                .ControlLibre(j).Location = New Point(.ControlLibre(j - 1).Location.X + .ControlLibre(j - 1).Width, _TopMarg - 5 + _Espaciado * iPos2)
                            Else
                                .ControlLibre(j).Location = New Point(_IniSegundaFila, _TopMarg - 5 + _Espaciado * iPos2)
                            End If
                            'AddHandler CmbCadena(i).SelectedIndexChanged, AddressOf Combos_SelectedIndexChanged
                            'AddHandler CmbCadena(i).KeyDown, AddressOf ObjEnter_KeyDown
                            AddHandler .ControlLibre(j).KeyDown, AddressOf ObjEnter_KeyDown
                            PanelCntExtras.Controls.Add(.ControlLibre(j))
                        Next
                        iPos2 += 1
                    ElseIf .BFiltraCampo Then
                        'L A B E L S
                        BndFiltra = True
                        LDescripcion(i) = New Label
                        LDescripcion(i).AutoSize = True
                        LDescripcion(i).Name = "L" & .Campo
                        LDescripcion(i).Text = .DesripcionCampo
                        LDescripcion(i).ForeColor = Color.Black
                        LDescripcion(i).Location = New Point(5, _TopMarg + _Espaciado * ipos)
                        'AGREGA EVENTO
                        AddHandler LDescripcion(i).Click, AddressOf Labels_Click
                        PanelControles.Controls.Add(LDescripcion(i))
                        '********** C A D E N A
                        If .TipoControlCampo = TipoControlReport.TdCadena Then
                            '////////////////////////Cambia de ComboBox  a ComboChek//////////////////////////////////////////////////////
                            CmbCadena(i) = New ComboChek 'Se agarra el Ultimo Valor del Arreglo
                            CmbCadena(i).ChekCmb.Name = i.ToString
                            '\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
                            CmbCadena(i).Name = "CmbCad" & .Campo
                            'CmbCadena(i).Name = .Campo
                            CmbCadena(i).Text = "cmbCadena"
                            'CmbCadena(i).Items.AddRange({"Filtro1", "Filtro2", "Filtro3", "Filtro4", "Etc"})
                            'LlenaCombo
                            DsLista = Inicio.LlenaCombos(.Campo, "Orden", i.ToString, TipoConexion.TcSQL, Inicio.CONSTR, Process.GetCurrentProcess.ProcessName,
                                                         KEY_CHECAERROR, KEY_ESTATUS, "", IIf(.StrSqlOpc <> "", .StrSqlOpc, DeterminaStrCombo(.Campo, vNomTabla)))

                            If Not DsLista Is Nothing Then
                                If DsLista.Tables(i.ToString).DefaultView.Count > 0 Then
                                    CmbCadena(i).DataSource = DsLista.Tables(i.ToString)
                                    CmbCadena(i).ValueMember = DsLista.Tables(i.ToString).Columns(0).ColumnName
                                    CmbCadena(i).DisplayMember = .Campo

                                    'CreaTabla(.Campo, DsLista.Tables(.Campo).Columns(0).ColumnName, IIf(.TipoCampo = TipoDato.TdCadena, "System.String", "System.Int32"), .Campo, IIf(.TipoCampo = TipoDato.TdCadena, "System.String", "System.Int32"))
                                    'DsLista2.Clone = DsLista.Tables(.Campo).DataSet
                                    'DsLista2.Tables(.Campo).Clear()
                                End If
                            Else
                                CmbCadena(i).DataSource = Nothing
                                CmbCadena(i).ValueMember = .Campo
                                CmbCadena(i).DisplayMember = .Campo
                                CmbCadena(i).Enabled = False
                            End If
                            'CmbCadena(i).ForeColor = Color.Black
                            'CmbCadena(i).Location = New Point(_IniSegundaFila, _TopMarg - 5 + _Espaciado * ipos)
                            '//////////////////////////////////////LOCATION DEL CHECKBOX////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                            CmbCadena(i).ChekCmb.Location = New Point(_IniSegundaFila, _TopMarg - 2 + _Espaciado * ipos)
                            CmbCadena(i).ForeColor = Color.Black
                            CmbCadena(i).Location = New Point(_IniSegundaFila + 15, _TopMarg - 5 + _Espaciado * ipos)
                            '/////////////////////////////////////////////////////////////////////////////////////////////////////////
                            CmbCadena(i).Size = New System.Drawing.Size(200, 21)
                            'GoTo brinco1

                            '12/MAYO/2014
                            '/////////////////////////////////////////////////////////////////////////////////////////////////////////
                            'btnAgre(i) = New Button
                            'With btnAgre(i)
                            '    .Name = i & "" 'NO CAMBIAR EL NOMBRE
                            '    .Text = "->"
                            '    .Location = New Point(CmbCadena(i).Location.X + CmbCadena(i).Width, CmbCadena(i).Location.Y)
                            '    .Size = New Size(30, 21)
                            'End With



                            'DsLista2 = DsLista.Clone
                            'DsLista2.Tables.Add(DsLista.Tables(i))

                            'For j = 0 To DsLista.Tables.Count - 1
                            '    MsgBox(DsLista.Tables(j).TableName)
                            'Next

                            CmbFiltroIn(i) = New ComboBox
                            If Not DsLista Is Nothing Then
                                DsLista2.Tables.Add(DsLista.Tables(i.ToString).Clone)

                                CmbFiltroIn(i).DataSource = DsLista2.Tables(i.ToString)
                                CmbFiltroIn(i).ValueMember = DsLista2.Tables(i.ToString).Columns(0).ColumnName
                                CmbFiltroIn(i).DisplayMember = .Campo
                            Else
                                CmbFiltroIn(i).Enabled = False
                                'btnAgre(i).Enabled = False
                            End If


                            'DsLista2.Tables.CopyTo(DsLista.Tables(i))
                            'DsLista2.Tables(i).Clear()


                            'CmbFiltroIn(i).Name = .Campo

                            With CmbFiltroIn(i)
                                ''quite aqui checar
                                '.Name = "CmbFilIn" & i
                                .Name = i
                                .DropDownStyle = ComboBoxStyle.DropDownList
                                '13/mayo/2014
                                '.Location = New Point(btnAgre(i).Location.X + btnAgre(i).Width, btnAgre(i).Location.Y)
                                .Size = New Size(21, 21)
                                .DropDownWidth = 200
                                .Visible = False


                            End With
                            'AddHandler btnAgre(i).Click, AddressOf BtnAgr_Click
                            AddHandler CmbFiltroIn(i).KeyDown, AddressOf CmbFiltroIn_KeyDown
                            '/////////////////////////////////////////////////////////////////////////////////////////////////////////
                            'brinco1:
                            AddHandler CmbCadena(i).SelectedIndexChanged, AddressOf Combos_SelectedIndexChanged
                            AddHandler CmbCadena(i).KeyDown, AddressOf ObjEnter_KeyDown
                            '///////////////////////////////////////////////////////////////////////////////////
                            AddHandler CmbCadena(i).ChekCmb.CheckedChanged, AddressOf chkCmb_CheckedChanged
                            PanelControles.Controls.Add(CmbCadena(i).ChekCmb)
                            If .TipofiltroCadena = ColClass.EnTipoFilCadena.Obligatorio Then
                                CmbCadena(i).ChekCmb.Visible = False
                                CmbCadena(i).Checked = True
                            Else
                                'CmbCadena(i).ChekCmb.Visible = True
                                CmbCadena(i).Checked = False
                            End If
                            '///////////////////////////////////////////////////////////////////////////////////
                            PanelControles.Controls.Add(CmbCadena(i))
                            PanelControles.Controls.Add(btnAgre(i))
                            PanelControles.Controls.Add(CmbFiltroIn(i))

                        ElseIf .TipoControlCampo = TipoControlReport.TdLikeCadena Then
                            txtLike(i) = New TextBox 'Se agarra el Ultimo Valor del Arreglo
                            txtLike(i).Name = "txtLike" & .Campo
                            txtLike(i).Text = ""
                            txtLike(i).Width = 150
                            txtLike(i).ForeColor = Color.Black
                            txtLike(i).Location = New Point(_IniSegundaFila, _TopMarg - 5 + _Espaciado * ipos)
                            AddHandler txtLike(i).Click, AddressOf TxtLike_Click 'Agrega evento Click
                            AddHandler txtLike(i).KeyPress, AddressOf TxtEnter_KeyPress
                            PanelControles.Controls.Add(txtLike(i)) 'Agrega el Control al Panel
                        ElseIf .TipoControlCampo = TipoControlReport.TdFecha Then
                            CmbFiltro(i) = New ComboBox
                            CmbFiltro(i).Items.AddRange({"=", ">", "<", "<>"})
                            CmbFiltro(i).Name = "cmbFiltro" & .Campo
                            CmbFiltro(i).DropDownStyle = ComboBoxStyle.DropDownList
                            CmbFiltro(i).SelectedIndex = 0
                            CmbFiltro(i).Width = 40
                            CmbFiltro(i).Location = New Point(_IniSegundaFila, _TopMarg - 5 + _Espaciado * ipos)

                            CaleFecha1(i) = New DateTimePicker
                            CaleFecha1(i).Name = "Cale" & .Campo
                            CaleFecha1(i).Value = Now
                            CaleFecha1(i).ShowCheckBox = True
                            'CaleFecha1(i).Checked = False
                            CaleFecha1(i).Checked = IIf(.TipofiltroCadena = ColClass.EnTipoFilCadena.Obligatorio, True, False)
                            CaleFecha1(i).Format = DateTimePickerFormat.Long
                            CaleFecha1(i).CalendarForeColor = Color.Black
                            CaleFecha1(i).Location = New Point(_IniSegundaFila + CmbFiltro(i).Width, _TopMarg - 5 + _Espaciado * ipos)
                            CaleFecha1(i).Size = New System.Drawing.Size(215, 20)

                            AddHandler CaleFecha1(i).ValueChanged, AddressOf DateTimePickers_ValueChanged
                            AddHandler CaleFecha1(i).KeyDown, AddressOf ObjEnter_KeyDown
                            PanelControles.Controls.Add(CmbFiltro(i))
                            PanelControles.Controls.Add(CaleFecha1(i))
                        ElseIf .TipoControlCampo = TipoControlReport.TdRangoFecha Then
                            CaleFecha1(i) = New DateTimePicker
                            CaleFecha1(i).Name = "Cale" & .Campo
                            CaleFecha1(i).Value = Now
                            CaleFecha1(i).ShowCheckBox = True
                            CaleFecha1(i).Checked = IIf(.TipofiltroCadena = ColClass.EnTipoFilCadena.Obligatorio, True, False)
                            CaleFecha1(i).Format = DateTimePickerFormat.Custom
                            CaleFecha1(i).CustomFormat = "dd/MMM/yyyy"
                            CaleFecha1(i).CalendarForeColor = Color.Black
                            CaleFecha1(i).Location = New Point(_IniSegundaFila, _TopMarg - 5 + _Espaciado * ipos)
                            CaleFecha1(i).Size = New System.Drawing.Size(125, 20)

                            AddHandler CaleFecha1(i).ValueChanged, AddressOf DateTimePickers_ValueChanged
                            AddHandler CaleFecha1(i).KeyDown, AddressOf ObjEnter_KeyDown
                            PanelControles.Controls.Add(CaleFecha1(i))

                            Dim LEntre As New Label
                            LEntre.Name = "LEntre"
                            LEntre.Text = " AL "
                            LEntre.AutoSize = True
                            LEntre.Location = New Point(_IniSegundaFila + CaleFecha1(i).Width, (_TopMarg - 5 + _Espaciado * ipos) + 5)
                            PanelControles.Controls.Add(LEntre)

                            CaleFecha2(i) = New DateTimePicker
                            CaleFecha2(i).Name = "Cale2" & .Campo
                            CaleFecha2(i).Value = Now
                            CaleFecha2(i).Format = DateTimePickerFormat.Custom
                            CaleFecha2(i).CustomFormat = "dd/MMM/yyyy"
                            CaleFecha2(i).CalendarForeColor = Color.Black
                            CaleFecha2(i).Location = New Point(_IniSegundaFila + LEntre.Width + CaleFecha1(i).Width, _TopMarg - 5 + _Espaciado * ipos)
                            CaleFecha2(i).Size = New System.Drawing.Size(110, 20)

                            AddHandler CaleFecha2(i).ValueChanged, AddressOf DateTimePickers_ValueChanged
                            AddHandler CaleFecha2(i).KeyDown, AddressOf ObjEnter_KeyDown
                            PanelControles.Controls.Add(CaleFecha2(i))
                        ElseIf .TipoControlCampo = TipoControlReport.TdBoolean Then
                            'GrupoLogico1(i) = New GroupBox
                            'GrupoLogico1(i).Name = "Grupo" & .Campo
                            'GrupoLogico1(i).Text = ""
                            'GrupoLogico1(i).Size = New System.Drawing.Size(152, 25)
                            'GrupoLogico1(i).Location = New Point(_IniSegundaFila, _TopMarg - 5 + _Espaciado * i)
                            'PanelControles.Controls.Add(GrupoLogico1(i))
                            strLogico = .DesValorTrue
                            Tama�oLogico = Len(strLogico)
                            strLogico = .DesValorFalse
                            Tama�oLogico = Tama�oLogico + Len(strLogico)

                            PanelLogico1(i) = New Panel
                            PanelLogico1(i).Name = "Panel" & .Campo
                            PanelLogico1(i).Text = ""
                            'PanelLogico1(i).Size = New System.Drawing.Size(152, 25)
                            PanelLogico1(i).Size = New System.Drawing.Size(Tama�oLogico * 10, 25)
                            PanelLogico1(i).Location = New Point(_IniSegundaFila, _TopMarg - 5 + _Espaciado * ipos)
                            PanelControles.Controls.Add(PanelLogico1(i))

                            rdbLogico1(i) = New RadioButton
                            rdbLogico1(i).Name = "rdb" & .Campo
                            rdbLogico1(i).Text = .DesValorTrue
                            rdbLogico1(i).AutoSize = True
                            rdbLogico1(i).Checked = True
                            'rdbLogico1(i).Location = New Point(_IniSegundaFila, _TopMarg - 5 + _Espaciado * i)
                            rdbLogico1(i).Size = New System.Drawing.Size(75, 17)
                            rdbLogico1(i).Location = New Point(6, 4)
                            '75, 17
                            AddHandler rdbLogico1(i).Click, AddressOf RadioButtons_Click
                            AddHandler rdbLogico1(i).KeyDown, AddressOf ObjEnter_KeyDown
                            'PanelControles.Controls.Add(rdbLogico1(i))
                            PanelLogico1(i).Controls.Add(rdbLogico1(i))

                            rdbLogico2(i) = New RadioButton
                            rdbLogico2(i).Name = "rdb" & .Campo
                            rdbLogico2(i).Text = .DesValorFalse
                            rdbLogico2(i).AutoSize = True
                            rdbLogico2(i).Checked = False
                            rdbLogico2(i).Size = New System.Drawing.Size(75, 17)
                            'rdbLogico2(i).Location = New Point(85, 4)
                            rdbLogico2(i).Location = New Point(15 + rdbLogico1(i).Width, 4)
                            AddHandler rdbLogico2(i).Click, AddressOf RadioButtons_Click
                            AddHandler rdbLogico2(i).KeyDown, AddressOf ObjEnter_KeyDown
                            'PanelControles.Controls.Add(rdbLogico2(i))
                            PanelLogico1(i).Controls.Add(rdbLogico2(i))
                        ElseIf .TipoControlCampo = TipoControlReport.TdNumerico Then
                            CmbFiltro(i) = New ComboBox
                            CmbFiltro(i).Items.AddRange({"=", ">", "<", "<>"})
                            CmbFiltro(i).Name = "cmbFiltro" & .Campo
                            CmbFiltro(i).DropDownStyle = ComboBoxStyle.DropDownList
                            CmbFiltro(i).SelectedIndex = 0
                            CmbFiltro(i).Width = 40
                            CmbFiltro(i).Location = New Point(_IniSegundaFila, _TopMarg - 5 + _Espaciado * ipos)
                            txtNumerico1(i) = New TextBox
                            txtNumerico1(i).Name = "txtNum" & .Campo
                            txtNumerico1(i).Text = ""
                            txtNumerico1(i).ForeColor = Color.Black
                            txtNumerico1(i).Location = New Point(_IniSegundaFila + CmbFiltro(i).Width, _TopMarg - 5 + _Espaciado * ipos)
                            AddHandler txtNumerico1(i).KeyPress, AddressOf TxtNum_KeyPress
                            AddHandler txtNumerico1(i).KeyPress, AddressOf TxtEnter_KeyPress
                            AddHandler CmbFiltro(i).KeyDown, AddressOf ObjEnter_KeyDown
                            PanelControles.Controls.Add(CmbFiltro(i))
                            PanelControles.Controls.Add(txtNumerico1(i))
                        ElseIf .TipoControlCampo = TipoControlReport.TdRangoNum Then
                            txtNumerico1(i) = New TextBox
                            txtNumerico1(i).Name = "txtRng1" & .Campo
                            txtNumerico1(i).Text = ""
                            txtNumerico1(i).ForeColor = Color.Black
                            txtNumerico1(i).Location = New Point(_IniSegundaFila, _TopMarg - 5 + _Espaciado * ipos)
                            AddHandler txtNumerico1(i).KeyPress, AddressOf TxtNum_KeyPress
                            AddHandler txtNumerico1(i).KeyPress, AddressOf TxtEnter_KeyPress
                            PanelControles.Controls.Add(txtNumerico1(i))

                            Dim LEntre As New Label
                            LEntre.Name = "LEntre"
                            LEntre.Text = "Y"
                            LEntre.AutoSize = True
                            LEntre.Location = New Point(_IniSegundaFila + txtNumerico1(i).Width, _TopMarg - 5 + _Espaciado * ipos)
                            PanelControles.Controls.Add(LEntre)

                            txtNumerico2(i) = New TextBox
                            txtNumerico2(i).Name = "txtRng2" & .Campo
                            txtNumerico2(i).Text = ""
                            txtNumerico2(i).ForeColor = Color.Black
                            txtNumerico2(i).Location = New Point(_IniSegundaFila + 15 + txtNumerico1(i).Width, _TopMarg - 5 + _Espaciado * ipos)
                            AddHandler txtNumerico2(i).KeyPress, AddressOf TxtNum_KeyPress
                            AddHandler txtNumerico2(i).KeyPress, AddressOf TxtEnter_KeyPress
                            PanelControles.Controls.Add(txtNumerico2(i))
                        End If
                    Else
                        Continue For
                    End If

                End With
                ipos += 1
            Next
            'configurar el ordena para  que no sea visible
            Dim ValHei As Integer = GroupBox1.Height + 50, ValLY As Integer = 5

            If BndFiltra Then
                ValHei += grupoFiltra.Height
                grupoFiltra.Visible = True
                grupoFiltra.Location = New Point(grupoFiltra.Location.X, ValLY)
                ValLY = grupoFiltra.Location.Y + grupoFiltra.Height
            Else
                grupoFiltra.Visible = False
            End If
            If BndContLibre Then
                ValHei += GrupoExtras.Height
                GrupoExtras.Visible = True
                GrupoExtras.Location = New Point(GrupoExtras.Location.X, ValLY)
                ValLY = GrupoExtras.Location.Y + GrupoExtras.Height
            Else
                GrupoExtras.Visible = False
            End If
            If BndOrdena Then
                grupoOrdena.Visible = True
                ValHei += grupoOrdena.Height
                grupoOrdena.Location = New Point(grupoOrdena.Location.X, ValLY)
            Else
                grupoOrdena.Visible = False
            End If
            Me.Height = ValHei
            LlenaGrid()
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error: " & Err.Number, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try



    End Sub


#Region "Eventos De los Arreglos de Controles"
    'L A B E L S
    Private Sub Labels_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        With TryCast(sender, Label)
            'MessageBox.Show("El Label Tiene Nombre: " & .Name & " y Tiene Escrito:" & .Text, "informacion del Label", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End With
    End Sub
    'C O M B O S
    Private Sub Combos_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        With TryCast(sender, ComboBox)
            'MessageBox.Show("El Combo Tiene Nombre: " & .Name & " y Tiene Escrito:" & .Text, "informacion del Combo", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End With
    End Sub

    'D A T E T I M E P I C K E R
    Private Sub DateTimePickers_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        With TryCast(sender, DateTimePicker)
            'MessageBox.Show("El Calendario Tiene Nombre: " & .Name & " y Tiene Fecha:" & .Text, "informacion del Calendario", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End With
    End Sub
    'TextBox
    Private Sub TxtLike_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        With TryCast(sender, TextBox)
            'MessageBox.Show("Se clickeo el Textbox: " & .Name & " Contenido: " & .Text)
        End With
    End Sub

    Private Sub TxtNum_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Not IsNumeric(e.KeyChar) And Asc(e.KeyChar) <> 8 Then
            e.Handled = True

            'MessageBox.Show("Es un campo donde solo se admiten Numeros!!!" & vbNewLine & _
            '               "Nombre: " & TryCast(sender, TextBox).Name & "  Texto: " & TryCast(sender, TextBox).Text, "SOlo numeros", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End If

    End Sub
    'R A D I O B U T T O N
    Private Sub RadioButtons_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        With TryCast(sender, RadioButton)
            'MessageBox.Show("El Label Tiene Nombre: " & .Name & " y Tiene Escrito:" & .Text, "informacion del Label", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End With
    End Sub
    'C H E C K B O X S

    Private Sub CheckBoxs_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        With TryCast(sender, CheckBox)
            'MessageBox.Show("El Label Tiene Nombre: " & .Name & " y Tiene Escrito:" & .Text, "informacion del Label", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End With
    End Sub

    Private Sub TxtEnter_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Asc(e.KeyChar) = 13 Then
            SendKeys.Send("{TAB}")
            e.Handled = True
        End If
    End Sub
    Private Sub ObjEnter_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        If e.KeyCode = 13 Then
            SendKeys.Send("{TAB}")
            e.Handled = True
        End If
    End Sub

    Private Sub BtnAgr_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        With TryCast(sender, Button)
            If Not CmbFiltroIn(.Name).Items.Contains(CmbCadena(.Name).Text) And CmbCadena(.Name).Text <> "TODOS" Then
                Dim MyDataRow As DataRow = DsLista2.Tables(.Name).NewRow()

                MyDataRow(DsLista2.Tables(.Name).Columns(0).ColumnName) = CmbCadena(.Name).SelectedValue
                MyDataRow(DsLista2.Tables(.Name).Columns(1).ColumnName) = CmbCadena(.Name).Text

                DsLista2.Tables(.Name).Rows.Add(MyDataRow)



                'myDataRow = MyTablaOrden.NewRow()
                'myDataRow("Columna") = CmbCadena(Name).ValueMember
                'myDataRow("Campo") = CmbCadena(Name).DisplayMember
                'MyTablaOrden.Rows.Add(myDataRow)
                'DsLista2.Tables(.Name).Rows.Add()
                'DsLista2.Tables(.Name).DefaultView 

                CmbFiltroIn(.Name).Visible = True
                'CmbFiltroIn(.Name).Items.Add(CmbCadena(.Name).SelectedValue)
            End If
        End With

    End Sub
    Private Sub CmbFiltroIn_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        Try
            If e.KeyCode = Keys.Delete Then
                With TryCast(sender, ComboBox)
                    If .SelectedIndex < 0 Then Exit Sub
                    DsLista2.Tables(.Name).Rows.Item(.SelectedIndex).Delete()

                    .Items.Remove(.Items(.SelectedIndex))
                    If .Items.Count <= 0 Then
                        .Visible = False
                    End If
                    e.Handled = True
                End With
            End If
        Catch ex As Exception
            If TryCast(sender, ComboBox).Items.Count <= 0 Then
                TryCast(sender, ComboBox).Visible = False
            End If
            TryCast(sender, ComboBox).SelectedIndex = -1
        End Try
    End Sub
    Private Sub chkCmb_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        With TryCast(sender, CheckBox)
            'btnAgre(.Name).Enabled = .Checked
            CmbFiltroIn(.Name).Enabled = .Checked
            If .Checked Then
                CmbCadena(.Name).Focus()
                CmbCadena(.Name).SelectedIndex = 1
            Else
                'CmbCadena(.Name).SelectedIndex = 0
            End If
        End With
    End Sub
#End Region

    Public Function GetContenidoControl(ByVal vFila As Integer, Optional ByVal vColumna As Integer = 0) As String
        Dim CntExt As Control()
        Try
            CntExt = TryCast(ObjCnt(vFila), Control())
            Return CntExt(vColumna).Text
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error: " & Err.Number, MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return ""
        End Try


    End Function
    Public Function GetControl(ByVal vFila As Integer, ByVal vColumna As Integer) As Control
        Dim CntExt As Control()
        Try
            CntExt = TryCast(ObjCnt(vFila), Control())
            Return CntExt(vColumna)
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error: " & Err.Number, MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return Nothing
        End Try
    End Function
    Private Sub cmdTerminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdTerminar.Click
        Me.Close()
    End Sub

    Private Sub CreaTablaOrden()

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Boolean")
        myDataColumn.ColumnName = "Activo"
        myDataColumn.ReadOnly = False
        myDataColumn.Unique = False
        ' Add the column to the table.
        MyTablaOrden.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.String")
        myDataColumn.ColumnName = "Columna"
        myDataColumn.ReadOnly = False
        myDataColumn.Unique = False
        ' Add the column to the table.
        MyTablaOrden.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.String")
        myDataColumn.ColumnName = "Orden"
        myDataColumn.ReadOnly = False
        myDataColumn.Unique = False
        ' Add the column to the table.
        MyTablaOrden.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Int16")
        myDataColumn.ColumnName = "Posicion"
        myDataColumn.ReadOnly = True
        myDataColumn.Unique = False
        ' Add the Column to the DataColumnCollection.
        MyTablaOrden.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.String")
        myDataColumn.ColumnName = "Campo"
        myDataColumn.ReadOnly = False
        myDataColumn.Unique = False
        ' Add the column to the table.
        MyTablaOrden.Columns.Add(myDataColumn)

        '**********************
        'AGREGADO 31/JULIO/2013 -> Campo agregado

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.String")
        myDataColumn.ColumnName = "Prefijo"
        myDataColumn.ReadOnly = False
        myDataColumn.Unique = False
        ' Add the column to the table.
        MyTablaOrden.Columns.Add(myDataColumn)

    End Sub

    Private Sub LlenaGrid()
        'GridOrden.DataSource = MyTablaOrden
        'For i As Integer = 1 To MyTablaOrden.Rows.Count - 1
        '    GridOrden.Item(0, i - 1).Value = i
        'Next
        GridOrden.Rows.Clear()

        For i = 0 To MyTablaOrden.DefaultView.Count - 1
            GridOrden.Rows.Add()
            GridOrden.Item("Activo", i).Value = MyTablaOrden.DefaultView.Item(i).Item("Activo").ToString
            GridOrden.Item("Columna", i).Value = MyTablaOrden.DefaultView.Item(i).Item("Columna").ToString
            GridOrden.Item("Orden", i).Value = MyTablaOrden.DefaultView.Item(i).Item("Orden").ToString
            GridOrden.Item("Posicion", i).Value = MyTablaOrden.DefaultView.Item(i).Item("Posicion").ToString
            GridOrden.Item("Campo", i).Value = MyTablaOrden.DefaultView.Item(i).Item("Campo").ToString
            '**********************
            'AGREGADO 31/JULIO/2013 -> Campo agregado
            GridOrden.Item("Prefijo", i).Value = MyTablaOrden.DefaultView.Item(i).Item("Prefijo").ToString

        Next

        GridOrden.AllowUserToAddRows = False
        GridOrden.AllowUserToResizeColumns = True
    End Sub

    Private Sub btnArriba_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnArriba.Click

        indice = GridOrden.CurrentRow.Index

        If indice = 0 Then
            Exit Sub
        End If

        'Dim dr As DataRow = MyTablaOrden.NewRow

        'dr(0) = GridOrden.Rows(indice).Cells("Columna").Value
        'dr(1) = GridOrden.Rows(indice).Cells("Orden").Value
        'dr(2) = GridOrden.Rows(indice).Cells("Posicion").Value
        'dr(3) = GridOrden.Rows(indice).Cells("Campo").Value

        'ACTUAL
        vActivo = GridOrden.Item("Activo", indice).Value
        vColumna = GridOrden.Item("Columna", indice).Value
        vOrden = GridOrden.Item("Orden", indice).Value
        vPosicion = GridOrden.Item("Posicion", indice).Value
        vCampo = GridOrden.Item("Campo", indice).Value

        'LA DE ARRIBA
        vActivo2 = GridOrden.Item("Activo", indice - 1).Value
        vColumna2 = GridOrden.Item("Columna", indice - 1).Value
        vOrden2 = GridOrden.Item("Orden", indice - 1).Value
        vPosicion2 = GridOrden.Item("Posicion", indice - 1).Value
        vCampo2 = GridOrden.Item("Campo", indice - 1).Value

        'MyTablaOrden.Rows.RemoveAt(indice)
        'GridOrden.Rows.RemoveAt(indice)
        'GridOrden.Rows.RemoveAt(indice - 1)

        'MyTablaOrden.Rows.InsertAt(dr, indice - 1)
        'GridOrden.Rows.Add()
        GridOrden.Item("Activo", indice - 1).Value = vActivo
        GridOrden.Item("Columna", indice - 1).Value = vColumna
        GridOrden.Item("Orden", indice - 1).Value = vOrden
        GridOrden.Item("Posicion", indice - 1).Value = vPosicion2
        GridOrden.Item("Campo", indice - 1).Value = vCampo

        'GridOrden.Rows.Add()
        GridOrden.Item("Activo", indice).Value = vActivo2
        GridOrden.Item("Columna", indice).Value = vColumna2
        GridOrden.Item("Orden", indice).Value = vOrden2
        GridOrden.Item("Posicion", indice).Value = vPosicion
        GridOrden.Item("Campo", indice).Value = vCampo2



        GridOrden.CurrentCell = GridOrden.Rows(indice - 1).Cells(0)


    End Sub

    Private Sub btnAbajo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAbajo.Click


        indice = GridOrden.CurrentRow.Index

        If indice + 1 = GridOrden.Rows.Count - 1 Then
            Exit Sub
        End If

        'actual
        vActivo = GridOrden.Item("Activo", indice).Value
        vColumna = GridOrden.Item("Columna", indice).Value
        vOrden = GridOrden.Item("Orden", indice).Value
        vPosicion = GridOrden.Item("Posicion", indice).Value
        vCampo = GridOrden.Item("Campo", indice).Value

        'LA DE ABAJO
        vActivo2 = GridOrden.Item("Activo", indice + 1).Value
        vColumna2 = GridOrden.Item("Columna", indice + 1).Value
        vOrden2 = GridOrden.Item("Orden", indice + 1).Value
        vPosicion2 = GridOrden.Item("Posicion", indice + 1).Value
        vCampo2 = GridOrden.Item("Campo", indice + 1).Value

        'GridOrden.Rows.RemoveAt(indice)
        'GridOrden.Rows.RemoveAt(indice + 1)

        'GridOrden.Rows.Add()
        GridOrden.Item("Activo", indice + 1).Value = vActivo
        GridOrden.Item("Columna", indice + 1).Value = vColumna
        GridOrden.Item("Orden", indice + 1).Value = vOrden
        GridOrden.Item("Posicion", indice + 1).Value = vPosicion2
        GridOrden.Item("Campo", indice + 1).Value = vCampo

        'GridOrden.Rows.Add()
        GridOrden.Item("Activo", indice).Value = vActivo2
        GridOrden.Item("Columna", indice).Value = vColumna2
        GridOrden.Item("Orden", indice).Value = vOrden2
        GridOrden.Item("Posicion", indice).Value = vPosicion
        GridOrden.Item("Campo", indice).Value = vCampo2

        GridOrden.CurrentCell = GridOrden.Rows(indice + 1).Cells(0)
    End Sub

    Private Function GetFiltroCadena(ByVal vCampo As String, ByVal vIndex As Integer,
    Optional ByVal TipoDeDato As String = "", Optional ByVal Prefijo As String = "",
    Optional TipofiltroCadena As ColClass.EnTipoFilCadena = ColClass.EnTipoFilCadena.Opcional) As String
        '**********************
        'AGREGADO 31/JULIO/2013 -> se Agrego el Tipo de TipoDeDato y Prefijo
        '**********************
        VTextoFiltroAux = ""
        Dim CadIn As String = ""
        Dim CadInAux As String = ""
        Dim BandFiltra As Boolean = True
        'Dim TipDATO As TipoDato
        If CmbFiltroIn(vIndex).Items.Count > 0 Then
            For j As Integer = 0 To CmbFiltroIn(vIndex).Items.Count - 1
                'CadIn += "'" & CmbFiltroIn(vIndex).Items(j) & "',"




                If TipoDeDato = TipoDato.TdNumerico Then
                    CadIn += DsLista2.Tables(vIndex.ToString).DefaultView.Item(j).Item(CmbFiltroIn(vIndex).ValueMember) & ","
                ElseIf TipoDeDato = TipoDato.TdCadena Then
                    CadIn += "'" & DsLista2.Tables(vIndex.ToString).DefaultView.Item(j).Item(CmbFiltroIn(vIndex).ValueMember) & "',"
                End If
                CadInAux += DsLista2.Tables(vIndex.ToString).DefaultView.Item(j).Item(CmbFiltroIn(vIndex).ValueMember) & ","
                'DsCorridaFin.Tables(TablaBd).DefaultView.Item(i).Item("idCliente")
            Next
            CadIn = Mid(CadIn, 1, CadIn.Length - 1)
            CadInAux = Mid(CadInAux, 1, CadInAux.Length - 1)

            VTextoFiltroAux = "(" & CadInAux & ")"
            Return IIf(Prefijo <> "", Prefijo & "." & vCampo, vCampo) & " IN(" & CadIn & ")"

        Else

            If CmbCadena(vIndex).Text <> "" Then
                If TipofiltroCadena = ColClass.EnTipoFilCadena.Opcional Then
                    If Not CmbCadena(vIndex).Checked Then
                        BandFiltra = False
                    End If
                End If

                If BandFiltra Then
                    If TipoDeDato = TipoDato.TdCadena Then
                        Return IIf(Prefijo <> "", Prefijo & "." & vCampo, vCampo) & " = '" & CmbCadena(vIndex).Text & "'"
                    ElseIf TipoDeDato = TipoDato.TdNumerico Then
                        Return IIf(Prefijo <> "", Prefijo & "." & vCampo, vCampo) & " = " & CmbCadena(vIndex).Text
                    End If

                End If


            Else
                Return ""
            End If
        End If
    End Function
    Private Function DeterminaFiltros() As String
        DeterminaFiltros = ""
        VTextoFiltro = ""
        Dim Str As String = ""
        '**********************
        'AGREGADO 31/JULIO/2013
        Dim StrAux As String = ""
        '**********************

        Try
            For i As Integer = 0 To _ArrColClass.Length - 1
                With _ArrColClass(i)
                    If .TipofiltroCadena = ColClass.EnTipoFilCadena.Obligatorio Then

                    End If
                    If .BFiltraCampo Then
                        If Not .ControlLibre Is Nothing Then Continue For
                        If .TipoControlCampo = TipoControlReport.TdCadena Then 'Se Creo un Combo
                            'MessageBox.Show("Filtro: " & CmbCadena(i).Text, "NombreCampo: " & .Campo & vbNewLine & "Descripcion: " & .DesripcionCampo)

                            If Str = "" Then
                                If Trim(CmbCadena(i).Text) <> "TODOS" Then
                                    ' Str = .Campo & " = " & IIf(.TipoCampo = TipoDato.TdCadena Or .TipoCampo = TipoDato.TdFecha, "'", "") & CmbCadena(i).SelectedValue & IIf(.TipoCampo = TipoDato.TdCadena Or .TipoCampo = TipoDato.TdFecha, "'", "")
                                    'OJO 1/FEB/2017
                                    'Str = GetFiltroCadena(.Campo, i, .TipoCampo, .Prefijo)
                                    Str = GetFiltroCadena(.Campo, i, .TipoCampoFiltra, .Prefijo, .TipofiltroCadena)
                                End If
                            Else
                                If Trim(CmbCadena(i).Text) <> "TODOS" Then
                                    ' Str = Str & " and " & .Campo & " = " & IIf(.TipoCampo = TipoDato.TdCadena Or .TipoCampo = TipoDato.TdFecha, "'", "") & CmbCadena(i).SelectedValue & IIf(.TipoCampo = TipoDato.TdCadena Or .TipoCampo = TipoDato.TdFecha, "'", "")

                                    'Str = Str & " and " & GetFiltroCadena(.Campo, i, .TipoCampo)

                                    '**********************
                                    'AGREGADO 31/JULIO/2013

                                    StrAux = GetFiltroCadena(.Campo, i, .TipoCampoFiltra, .Prefijo, .TipofiltroCadena)
                                    If StrAux <> "" Then
                                        Str = Str & " and " & StrAux
                                    End If

                                    If VTextoFiltro = "" Then
                                        VTextoFiltro = .DesripcionCampo & VTextoFiltroAux
                                    Else
                                        VTextoFiltro = VTextoFiltro & " ; " & .DesripcionCampo & VTextoFiltroAux
                                    End If
                                    '**********************

                                End If
                            End If
                        ElseIf .TipoControlCampo = TipoControlReport.TdLikeCadena Then
                            'MessageBox.Show("Filtro: " & txtLike(i).Text, "NombreCampo: " & .Campo & vbNewLine & "Descripcion: " & .DesripcionCampo)
                        ElseIf .TipoControlCampo = TipoControlReport.TdNumerico Then
                            If Str = "" Then
                                If Val(txtNumerico1(i).Text) > 0 Then
                                    Str = .Campo & " = " & txtNumerico1(i).Text
                                End If
                            Else
                                'If txtNumerico1(i).Text > 0 Or txtNumerico1(i).Text <> "" Then
                                If Val(txtNumerico1(i).Text) > 0 Then
                                    If Str <> "" Then
                                        Str = Str & " and " & .Campo & " = " & txtNumerico1(i).Text
                                    Else
                                        Str = .Campo & " = " & txtNumerico1(i).Text
                                    End If
                                End If
                            End If
                            'MessageBox.Show("Filtro: " & txtNumerico1(i).Text, "NombreCampo: " & .Campo & vbNewLine & "Descripcion: " & .DesripcionCampo)
                        ElseIf .TipoControlCampo = TipoControlReport.TdRangoNum Then
                            'MessageBox.Show("Filtro: " & txtNumerico1(i).Text & " Hacia: " & txtNumerico2(i).Text, "NombreCampo: " & .Campo & vbNewLine & "Descripcion: " & .DesripcionCampo)
                            If txtNumerico1(i).Text.Trim <> "" And txtNumerico2(i).Text.Trim <> "" Then
                                If Val(txtNumerico1(i).Text) > Val(txtNumerico2(i).Text) Then
                                    Dim Rsp As String = txtNumerico1(i).Text
                                    txtNumerico1(i).Text = txtNumerico2(i).Text
                                    txtNumerico2(i).Text = Rsp
                                End If
                                If Str <> "" Then
                                    Str += " AND " & .Campo & " >= " & Val(txtNumerico1(i).Text) & " AND " & .Campo & " <= " & Val(txtNumerico2(i).Text)
                                Else
                                    Str += .Campo & " >= " & Val(txtNumerico1(i).Text) & " AND " & .Campo & " <= " & Val(txtNumerico2(i).Text)
                                End If
                            End If
                        ElseIf .TipoControlCampo = TipoControlReport.TdFecha Then
                            If CaleFecha1(i).Checked Then
                                If Str <> "" Then
                                    Str += " AND " & .Campo & " " & CmbFiltro(i).Text & " " & CaleFecha1(i).Value.Date
                                Else
                                    Str += .Campo & " " & CmbFiltro(i).Text & " " & CaleFecha1(i).Value.Date
                                End If
                            End If
                        ElseIf .TipoControlCampo = TipoControlReport.TdRangoFecha Then
                            'If txtNumerico1(i).Text.Trim <> "" And txtNumerico2(i).Text.Trim <> "" Then
                            If CaleFecha1(i).Checked Then
                                If CaleFecha1(i).Value.Date > CaleFecha2(i).Value.Date Then
                                    Dim Rsp As Date = CaleFecha1(i).Value.Date
                                    CaleFecha1(i).Value = CaleFecha2(i).Value.Date
                                    CaleFecha2(i).Value = Rsp.Date
                                End If
                                '**********************
                                'AGREGADO 31/JULIO/2013 -> SE AGREGO PREFIJO
                                If Str <> "" Then
                                    Str += " AND " & IIf(.Prefijo <> "", .Prefijo & "." & .Campo, .Campo) & " >= '" & CaleFecha1(i).Value.Date & " 00:00:00' AND " & IIf(.Prefijo <> "", .Prefijo & "." & .Campo, .Campo) & " <= '" & CaleFecha2(i).Value.Date & " 23:59:59' "
                                Else
                                    Str += IIf(.Prefijo <> "", .Prefijo & "." & .Campo, .Campo) & " >= '" & CaleFecha1(i).Value.Date & " 00:00:00' AND " & IIf(.Prefijo <> "", .Prefijo & "." & .Campo, .Campo) & " <= '" & CaleFecha2(i).Value.Date & " 23:59:59' "
                                End If
                                If VTextoFiltro = "" Then
                                    VTextoFiltro = .DesripcionCampo & " : del " & CaleFecha1(i).Value.Date & " al " & CaleFecha2(i).Value.Date
                                Else
                                    VTextoFiltro = VTextoFiltro & " ; " & .DesripcionCampo & " : del " & CaleFecha1(i).Value.Date & " al " & CaleFecha2(i).Value.Date
                                End If

                            End If

                            'End If
                            'MessageBox.Show("Filtro: " & CaleFecha1(i).Text & " Hacia: " & CaleFecha2(i).Text, "NombreCampo: " & .Campo & vbNewLine & "Descripcion: " & .DesripcionCampo)
                        ElseIf .TipoControlCampo = TipoControlReport.TdBoolean Then
                            If rdbLogico1(i).Checked Or rdbLogico2(i).Checked Then
                                If Str = "" Then
                                    Str = .Campo & " = " & IIf(rdbLogico1(i).Checked, "1", "0")
                                Else
                                    If Str <> "" Then
                                        Str = Str & " and " & .Campo & " = " & IIf(rdbLogico1(i).Checked, "1", "0")
                                    Else
                                        Str = .Campo & " = " & IIf(rdbLogico1(i).Checked, "1", "0")
                                    End If
                                End If
                            End If
                        End If
                    End If
                End With
            Next

        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try

        Return Str
    End Function

    Private Function DeterminaOrden() As String
        Dim Str As String = ""


        For i = 0 To GridOrden.Rows.Count - 1
            If GridOrden.Item("Activo", i).Value Then
                'If Str = "" Then
                '    Str = GridOrden.Item("Campo", i).Value & " " & IIf(GridOrden.Item("Orden", i).Value = "Ascendente", "ASC", "DESC")
                'Else
                'IIf(GridOrden.Item("Prefijo", i).Value <> "", GridOrden.Item("Prefijo", i).Value & "." & GridOrden.Item("Campo", i).Value, GridOrden.Item("Campo", i).Value)

                'If Trim(Str) <> "" Then
                '    Str = Str & ", " & GridOrden.Item("Campo", i).Value & " " & IIf(GridOrden.Item("Orden", i).Value = "Ascendente", "ASC", "DESC")
                'Else
                '    Str = GridOrden.Item("Campo", i).Value & " " & IIf(GridOrden.Item("Orden", i).Value = "Ascendente", "ASC", "DESC")
                'End If

                If Trim(Str) <> "" Then
                    Str = Str & ", " & IIf(GridOrden.Item("Prefijo", i).Value <> "", GridOrden.Item("Prefijo", i).Value & "." & GridOrden.Item("Campo", i).Value, GridOrden.Item("Campo", i).Value) & " " & IIf(GridOrden.Item("Orden", i).Value = "Ascendente", "ASC", "DESC")
                Else
                    Str = IIf(GridOrden.Item("Prefijo", i).Value <> "", GridOrden.Item("Prefijo", i).Value & "." & GridOrden.Item("Campo", i).Value, GridOrden.Item("Campo", i).Value) & " " & IIf(GridOrden.Item("Orden", i).Value = "Ascendente", "ASC", "DESC")
                End If


                'End If
            End If
        Next

        'Str = "Order by " & Str

        Return Str
    End Function

    Private Function DeterminaStrCombo(ByVal Campo As String, ByVal Tabla As String) As String
        Dim strSql As String

        '"IF (SELECT COUNT(*) FROM ##TABLA) > 0 DROP TABLE ##TABLA " & _

        strSql = "IF EXISTS (SELECT * FROM sysobjects WHERE name='##TABLA') DROP TABLE ##TABLA " &
        "CREATE TABLE ##TABLA (" & Campo & " varchar(100)) " &
        "insert into ##TABLA " &
        "select distinct " & Campo & " AS " & Campo & " from " & Tabla & " " &
        "select 'TODOS' AS " & Campo & ", 0 as Orden " &
        "UNION " &
        "select " & Campo & " AS " & Campo & ",  ROW_NUMBER() OVER (order BY " & Campo & ")  AS 'Orden' " &
        "from ##TABLA order by orden"

        'strSql = "select 'TODOS' AS " & Campo & ", 0 as Orden " & _
        '    " UNION " & _
        '    " select DISTINCT " & Campo & " AS " & Campo & ",  ROW_NUMBER() OVER (ORDER BY " & Campo & ") AS 'Orden' from " & Tabla & " order by orden"

        Return strSql
    End Function

    Private Function FiltrosRepEspeciales(ByVal vCampo As String, ByVal vIndice As Integer) As String
        Dim FiltroGen As String
        FiltroGen = GetFiltroCadena(vCampo, vIndice, )
        If FiltroGen <> "" Then
            FiltroGen = " AND " & FiltroGen
        Else
            FiltroGen = IIf(CmbCadena(vIndice).Text = " TODOS", "", " AND " & vCampo & " = '" & CmbCadena(vIndice).SelectedValue & "' ")
        End If

        Return FiltroGen
    End Function


    Private Function CreaSQLConsulta(ByVal NomRep As String, ByVal FiltroSinWhere As String, Optional ByVal OrdenaCampos As String = "") As String
        Dim Filtro As String = ""
        Dim FiltroIn As String = ""
        Dim FechaCorte As String = ""


        'Dim vFechaMovimiento As String
        Dim vFiltro As String = ""

        CreaSQLConsulta = ""

        Select Case UCase(NomRep)
            Case UCase("RepCatUnidadTrans")
                'FILTROS
                vFiltro = ""
                If CmbCadena(8).Enabled Then
                    vValor1 = CmbCadena(8).SelectedValue
                    vFiltro = " where uni.idEmpresa = " & vValor1
                End If

                If CmbCadena(4).Enabled Then
                    vValor1 = CmbCadena(4).SelectedValue
                    If vFiltro <> "" Then
                        vFiltro = vFiltro & " and tuni.clasificacion = '" & vValor1 & "'"
                    Else
                        vFiltro = " where tuni.clasificacion = '" & vValor1 & "'"
                    End If

                End If

                If CmbCadena(6).Enabled Then
                    vValor1 = CmbCadena(6).SelectedValue
                    If vFiltro <> "" Then
                        vFiltro = vFiltro & " and uni.idMarca = " & vValor1
                    Else
                        vFiltro = " where uni.idMarca = " & vValor1
                    End If

                End If



                strSqlGlobal = "SELECT uni.idUnidadTrans, " &
                "uni.descripcionUni, " &
                "uni.idTipoUnidad, " &
                "tuni.nomTipoUniTras, " &
                "tuni.clasificacion, " &
                "uni.idMarca, " &
                "mar.NombreMarca, " &
                "uni.idEmpresa, " &
                "emp.RazonSocial, " &
                "uni.Estatus " &
                "FROM dbo.CatUnidadTrans uni " &
                "left JOIN dbo.CatTipoUniTrans tuni ON tuni.idTipoUniTras = uni.idTipoUnidad " &
                "left JOIN dbo.CatMarcas mar ON mar.idMarca = uni.idMarca " &
                "left JOIN dbo.CatEmpresas emp ON emp.idEmpresa = uni.idEmpresa" & vFiltro



            Case UCase("RepCatMarcas")
                strSqlGlobal = "SELECT IDMARCA, NOMBREMARCA FROM CATMARCAS"
                'Case UCase("RepRemision")
                '    If vValor1 = "" Then
                '        'TIPO DE DOCUMENTO
                '        'vValor1 = CmbCadena(0).SelectedValue
                '        'ID MOVIMIENTO
                '        vValor1 = CInt(CmbCadena(0).SelectedValue)
                '        'SERIE
                '        'vValor3 = CmbCadena(1).SelectedValue

                '    End If
                '    '    strSqlGlobal = "DECLARE @ULTIMAENTRADA INT " & _
                '    '"SET @ULTIMAENTRADA = (SELECT NoEntrada FROM CABMOVIMIENTOS WHERE NOMOVIMIENTO =  " & Val(vValor1) & " )  " & _
                '    '"DECLARE @ULTIMASALIDA INT  " & _
                '    '"SET @ULTIMASALIDA = (SELECT NoSalida FROM CABMOVIMIENTOS WHERE NOMOVIMIENTO =  " & Val(vValor1) & " )  " & _
                '    '"SELECT CAB.NOMOVIMIENTO,CAB.NOSERIE AS SERIE, CAB.IDMOVIMIENTO AS FOLIO, CAB.FECHAMOVIMIENTO, CONC_TRAS.CNOMBREC01 AS CNOMBREC01_TRAS, " & _
                '    '"CAB.CCODIGOC01_CLI AS CLIENTE, '' AS CRAZONSO01, ''  AS DOMICILIO, '' AS CRFC, " & _
                '    '"CAB.CREFEREN01 AS REFERENCIA, CAB.COBSERVA01 AS OBSERVACIONES, " & _
                '    '"CAB.SUBTOTALEXENTO , CAB.SUBTOTALGRAV, CAB.IVA, CAB.DESCUENTO, " & _
                '    '"(CAB.SUBTOTALEXENTO + CAB.SUBTOTALGRAV) AS SUBTOTAL, " & _
                '    '"(((CAB.SUBTOTALEXENTO + CAB.SUBTOTALGRAV)-(CAB.DESCUENTO))+ CAB.IVA) AS TOTAL, CAB.CIDDOCUM02, CAB.TIPOMOVIMIENTO, " & _
                '    '"DET.CCODIGOP01_PRO, PROD.CNOMBREP01, DET.CIDUNIDA01 ,UNI.CABREVIA01 AS UNIDAD, UNI.CNOMBREU01, " & _
                '    '"CAST(DET.CANTIDAD AS INT) AS CANTIDAD , DET.COSTO , (DET.CANTIDAD * DET.COSTO) AS IMPORTE, DET.CONSECUTIVO, " & _
                '    '"CAB_ENT.NOMOVIMIENTO AS NOMOVENT,CAB_ENT.CIDDOCUM02 AS CIDDOCUM02_ENT,CAB_ENT.NOSERIE AS SERIE_ENT, CAB_ENT.IDMOVIMIENTO AS FOLIO_ENT, CONC_ENT.CNOMBREC01 AS CNOMBREC01_ENT, " & _
                '    '"CAB.CIDALMACEN_SAL , ALM_SAL.CNOMBREA01 AS NOMALMACEN_SAL, " & _
                '    '"CAB_SAL.NOMOVIMIENTO AS NOMOVSAL,CAB_SAL.CIDDOCUM02 AS CIDDOCUM02_SAL,CAB_SAL.NOSERIE AS SERIE_SAL, CAB_SAL.IDMOVIMIENTO AS FOLIO_SAL, CONC_SAL.CNOMBREC01 AS CNOMBREC01_SAL, " & _
                '    '"CAB.CIDALMACEN_ENT , ALM_ENT.CNOMBREA01 AS NOMALMACEN_ENT,cab.UsuarioModifica " & _
                '    '"FROM CABMOVIMIENTOS CAB " & _
                '    '"INNER JOIN CABMOVIMIENTOS CAB_ENT ON CAB.NOMOVIMIENTOPADRE = CAB_ENT.NOMOVIMIENTOPADRE AND CAB_ENT.NOMOVIMIENTO = @ULTIMAENTRADA " & _
                '    '"INNER JOIN CABMOVIMIENTOS CAB_SAL ON CAB.NOMOVIMIENTOPADRE = CAB_SAL.NOMOVIMIENTOPADRE AND CAB_SAL.NOMOVIMIENTO = @ULTIMASALIDA " & _
                '    '"INNER JOIN DETMOVIMIENTOS DET ON DET.NOMOVIMIENTO = @ULTIMAENTRADA " & _
                '    '"INNER JOIN CATPRODUCTOS PROD ON DET.CCODIGOP01_PRO = PROD.CCODIGOP01 " & _
                '    '"INNER JOIN CATUNIDADES UNI ON DET.CIDUNIDA01 = UNI.CIDUNIDA01 " & _
                '    '"INNER JOIN CATCONCEPTOS CONC_SAL ON CAB_SAL.CIDDOCUM02 = CONC_SAL.CCODIGOC01  " & _
                '    '"INNER JOIN CATCONCEPTOS CONC_ENT ON CAB_ENT.CIDDOCUM02 = CONC_ENT.CCODIGOC01  " & _
                '    '"INNER JOIN CATCONCEPTOS CONC_TRAS ON CAB.CIDDOCUM02 = CONC_TRAS.CCODIGOC01  " & _
                '    '"INNER JOIN CATALMACENES ALM_SAL ON CAB.CIDALMACEN_SAL = ALM_SAL.CCODIGOA01  " & _
                '    '"INNER JOIN CATALMACENES ALM_ENT ON CAB.CIDALMACEN_ENT  = ALM_ENT.CCODIGOA01  " & _
                '    '"WHERE CAB.TIPOMOVIMIENTO = 'TRAS' AND CAB.NOMOVIMIENTO = " & Val(vValor1) & _
                '    '"ORDER BY DET.CONSECUTIVO"

                '    '11/FEB/2016

                '    strSqlGlobal = "SELECT CAB.NOMOVIMIENTO,CAB.NOSERIE AS SERIE, CAB.IDMOVIMIENTO AS FOLIO, CAB.FECHAMOVIMIENTO, CONC_TRAS.CNOMBREC01 AS CNOMBREC01_TRAS, " & _
                '    "CAB.CCODIGOC01_CLI AS CLIENTE, '' AS CRAZONSO01, ''  AS DOMICILIO, '' AS CRFC, " & _
                '    "CAB.CREFEREN01 AS REFERENCIA, CAB.COBSERVA01 AS OBSERVACIONES, " & _
                '    "CAB.SUBTOTALEXENTO , CAB.SUBTOTALGRAV, CAB.IVA, CAB.DESCUENTO, " & _
                '    "(CAB.SUBTOTALEXENTO + CAB.SUBTOTALGRAV) AS SUBTOTAL, " & _
                '    "(((CAB.SUBTOTALEXENTO + CAB.SUBTOTALGRAV)-(CAB.DESCUENTO))+ CAB.IVA) AS TOTAL, CAB.CIDDOCUM02, CAB.TIPOMOVIMIENTO, " & _
                '    "DET.CCODIGOP01_PRO, PROD.CNOMBREP01, DET.CIDUNIDA01 ,UNI.CABREVIA01 AS UNIDAD, UNI.CNOMBREU01, " & _
                '    "CAST(DET.CANTIDAD AS INT) AS CANTIDAD , DET.COSTO , (DET.CANTIDAD * DET.COSTO) AS IMPORTE, DET.CONSECUTIVO, " & _
                '    "CAB_ENT.NOMOVIMIENTO AS NOMOVENT,CAB_ENT.CIDDOCUM02 AS CIDDOCUM02_ENT,CAB_ENT.NOSERIE AS SERIE_ENT, CAB_ENT.IDMOVIMIENTO AS FOLIO_ENT, CONC_ENT.CNOMBREC01 AS CNOMBREC01_ENT, " & _
                '    "CAB.CIDALMACEN_SAL , ALM_SAL.CNOMBREA01 AS NOMALMACEN_SAL, " & _
                '    "CAB_SAL.NOMOVIMIENTO AS NOMOVSAL,CAB_SAL.CIDDOCUM02 AS CIDDOCUM02_SAL,CAB_SAL.NOSERIE AS SERIE_SAL, CAB_SAL.IDMOVIMIENTO AS FOLIO_SAL, CONC_SAL.CNOMBREC01 AS CNOMBREC01_SAL, " & _
                '    "CAB.CIDALMACEN_ENT , ALM_ENT.CNOMBREA01 AS NOMALMACEN_ENT,cab.UsuarioModifica " & _
                '    "FROM CABMOVIMIENTOS CAB " & _
                '    "INNER JOIN CABMOVIMIENTOS CAB_ENT ON CAB.NOMOVIMIENTOPADRE = CAB_ENT.NOMOVIMIENTOPADRE AND CAB_ENT.NOMOVIMIENTO = cab.noentrada " & _
                '    "INNER JOIN CABMOVIMIENTOS CAB_SAL ON CAB.NOMOVIMIENTOPADRE = CAB_SAL.NOMOVIMIENTOPADRE AND CAB_SAL.NOMOVIMIENTO = cab.nosalida " & _
                '    "INNER JOIN DETMOVIMIENTOS DET ON DET.NOMOVIMIENTO = cab.UltimaEntrada " & _
                '    "INNER JOIN CATPRODUCTOS PROD ON DET.CCODIGOP01_PRO = PROD.CCODIGOP01 " & _
                '    "INNER JOIN CATUNIDADES UNI ON DET.CIDUNIDA01 = UNI.CIDUNIDA01 " & _
                '    "INNER JOIN CATCONCEPTOS CONC_SAL ON CAB_SAL.CIDDOCUM02 = CONC_SAL.CCODIGOC01 " & _
                '    "INNER JOIN CATCONCEPTOS CONC_ENT ON CAB_ENT.CIDDOCUM02 = CONC_ENT.CCODIGOC01 " & _
                '    "INNER JOIN CATCONCEPTOS CONC_TRAS ON CAB.CIDDOCUM02 = CONC_TRAS.CCODIGOC01 " & _
                '    "INNER JOIN CATALMACENES ALM_SAL ON CAB.CIDALMACEN_SAL = ALM_SAL.CCODIGOA01 " & _
                '    "INNER JOIN CATALMACENES ALM_ENT ON CAB.CIDALMACEN_ENT  = ALM_ENT.CCODIGOA01 " & _
                '    "WHERE CAB.TIPOMOVIMIENTO = 'TRAS' AND CAB.NOMOVIMIENTO = " & Val(vValor1) & _
                '    " ORDER BY DET.CONSECUTIVO"



                '    vValor1 = ""
                'Case UCase("RepTraspFaltantes")
                '    If CaleFecha1(1).Checked Then
                '        vFiltro = ""
                '        vFiltro = vFiltro & " AND CAB.FECHAMOVIMIENTO >= '" & CaleFecha1(1).Value.ToShortDateString & " 00:00:00' AND CAB.FECHAMOVIMIENTO <= '" & CaleFecha2(1).Value.ToShortDateString & " 23:59:59'"
                '    End If
                '    strSqlGlobal = "SELECT CAB.NOMOVIMIENTO ,CAB.FECHAMOVIMIENTO, " & _
                '    "CAB.CIDDOCUM02 AS CIDDOCUM02_TRAS,CONC_TRAS.CNOMBREC01 AS CNOMBREC01_TRAS, " & _
                '    "(CAST(CAB_SAL.CIDALMACEN_SAL AS VARCHAR(4)) + '-' +  ALM_SAL.CNOMBREA01) AS NOMALMSAL, " & _
                '    "CAB.ULTIMASALIDA as NOSALIDA, (CAB_SAL.NOSERIE + '-' + CAST(CAB_SAL.IDMOVIMIENTO AS VARCHAR(10))) AS FOLIO_SAL, " & _
                '    "CAB_SAL.CIDDOCUM02 AS CIDDOCUM02_SAL, CONC_SAL.CNOMBREC01 AS CNOMBREC01_SAL, " & _
                '    "(CAST(CAB_ENT.CIDALMACEN_ENT AS VARCHAR(4)) + '-' + ALM_ENT.CNOMBREA01) AS NOMALMENT, " & _
                '    "CAB.ULTIMAENTRADA as NOENTRADA , (CAB_ENT.NOSERIE + '-' + CAST(CAB_ENT.IDMOVIMIENTO AS VARCHAR(10))) AS FOLIO_ENT, " & _
                '    "CAB_ENT.CIDDOCUM02 AS CIDDOCUM02_ENT, CONC_ENT.CNOMBREC01 AS CNOMBREC01_ENT " & _
                '    "FROM CABMOVIMIENTOS CAB " & _
                '    "INNER JOIN CATALMACENES ALM_SAL ON CAB.CIDALMACEN_SAL = ALM_SAL.CCODIGOA01 " & _
                '    "INNER JOIN CATALMACENES ALM_ENT ON CAB.CIDALMACEN_ENT = ALM_ENT.CCODIGOA01 " & _
                '    "INNER JOIN CABMOVIMIENTOS CAB_SAL ON CAB.ULTIMASALIDA = CAB_SAL.NOMOVIMIENTO " & _
                '    "INNER JOIN CABMOVIMIENTOS CAB_ENT ON CAB.ULTIMAENTRADA  = CAB_ENT.NOMOVIMIENTO " & _
                '    "INNER JOIN CATCONCEPTOS CONC_SAL ON CAB_SAL.CIDDOCUM02 = CONC_SAL.CCODIGOC01 " & _
                '    "INNER JOIN CATCONCEPTOS CONC_ENT ON CAB_ENT.CIDDOCUM02 = CONC_ENT.CCODIGOC01 " & _
                '    "INNER JOIN CATCONCEPTOS CONC_TRAS ON CAB.CIDDOCUM02 = CONC_TRAS.CCODIGOC01 " & _
                '    "WHERE CAB.NOMOVIMIENTO = CAB.NOMOVIMIENTOPADRE AND CAB.BCANTCOMPLETA = 0 " & _
                '    "AND CAB.ESTATUS = 'TER' AND ISNULL(CAB.BELICANTFALT,0) = 0 AND ISNULL(CAB.BTRASDIR,0) = 0 " & vFiltro & _
                '    " ORDER BY CAB.FECHAMOVIMIENTO DESC"

                '    strSqlGlobalDet = "SELECT CAB.NOMOVIMIENTO, " & _
                '    "DET.CCODIGOP01_PRO, PRO.CNOMBREP01 AS TCNOMBREP01,UNI.CABREVIA01, " & _
                '    "DET.CANTIDAD, DET.CANTIDADINGRESADA , (DET.CANTIDAD - DET.CANTIDADINGRESADA) AS CANTIDADFALTANTE " & _
                '    "FROM CABMOVIMIENTOS CAB " & _
                '    "INNER JOIN DETMOVIMIENTOS DET ON CAB.NOMOVIMIENTO = DET.NOMOVIMIENTO " & _
                '    "INNER JOIN CATPRODUCTOS PRO ON DET.CCODIGOP01_PRO = PRO.CCODIGOP01 " & _
                '    "INNER JOIN CATUNIDADES UNI ON DET.CIDUNIDA01 = UNI.CIDUNIDA01 " & _
                '    "WHERE CAB.NOMOVIMIENTO = CAB.NOMOVIMIENTOPADRE AND CAB.BCANTCOMPLETA = 0 AND CAB.ESTATUS = 'TER' " & _
                '    "AND ISNULL(CAB.BTRASDIR,0) = 0 AND ISNULL(CAB.BELICANTFALT,0) = 0 AND DET.CANTIDAD - DET.CANTIDADINGRESADA > 0 " & _
                '    "ORDER BY CAB.NOMOVIMIENTO,DET.CONSECUTIVO"

                'Case UCase("RepTraspFaltantesini")
                '    If CaleFecha1(1).Checked Then
                '        vFiltro = ""
                '        vFiltro = vFiltro & " AND CAB.FECHAMOVIMIENTO >= '" & CaleFecha1(1).Value.ToShortDateString & " 00:00:00' AND CAB.FECHAMOVIMIENTO <= '" & CaleFecha2(1).Value.ToShortDateString & " 23:59:59'"
                '    End If
                '    strSqlGlobal = "SELECT distinct CAB.NOMOVIMIENTO ,CAB.FECHAMOVIMIENTO, " & _
                '    "CAB.CIDDOCUM02 AS CIDDOCUM02_TRAS,CONC_TRAS.CNOMBREC01 AS CNOMBREC01_TRAS, " & _
                '    "(CAST(CAB.CIDALMACEN_SAL AS VARCHAR(4)) + '-' +  ALM_SAL.CNOMBREA01) AS NOMALMSAL, " & _
                '    "CAB.NOSALIDA, (CAB_SAL.NOSERIE + '-' + CAST(CAB_SAL.IDMOVIMIENTO AS VARCHAR(10))) AS FOLIO_SAL, " & _
                '    "CAB_SAL.CIDDOCUM02 AS CIDDOCUM02_SAL, CONC_SAL.CNOMBREC01 AS CNOMBREC01_SAL, " & _
                '    "(CAST(CAB.CIDALMACEN_ENT AS VARCHAR(4)) + '-' + ALM_ENT.CNOMBREA01) AS NOMALMENT, " & _
                '    "CAB.NOENTRADA , (CAB_ENT.NOSERIE + '-' + CAST(CAB_ENT.IDMOVIMIENTO AS VARCHAR(10))) AS FOLIO_ENT, " & _
                '    "CAB_ENT.CIDDOCUM02 AS CIDDOCUM02_ENT, CONC_ENT.CNOMBREC01 AS CNOMBREC01_ENT " & _
                '    "FROM CABMOVIMIENTOS CAB " & _
                '    "INNER JOIN DETMOVIMIENTOS DET ON CAB.NOMOVIMIENTO = DET.NOMOVIMIENTO " & _
                '    "INNER JOIN CATALMACENES ALM_SAL ON CAB.CIDALMACEN_SAL = ALM_SAL.CCODIGOA01 " & _
                '    "INNER JOIN CATALMACENES ALM_ENT ON CAB.CIDALMACEN_ENT = ALM_ENT.CCODIGOA01 " & _
                '    "INNER JOIN CABMOVIMIENTOS CAB_SAL ON CAB.ULTIMASALIDA = CAB_SAL.NOMOVIMIENTO " & _
                '    "INNER JOIN CABMOVIMIENTOS CAB_ENT ON CAB.ULTIMAENTRADA  = CAB_ENT.NOMOVIMIENTO " & _
                '    "INNER JOIN CATCONCEPTOS CONC_SAL ON CAB_SAL.CIDDOCUM02 = CONC_SAL.CCODIGOC01 " & _
                '    "INNER JOIN CATCONCEPTOS CONC_ENT ON CAB_ENT.CIDDOCUM02 = CONC_ENT.CCODIGOC01 " & _
                '    "INNER JOIN CATCONCEPTOS CONC_TRAS ON CAB.CIDDOCUM02 = CONC_TRAS.CCODIGOC01 " & _
                '    "WHERE CAB.NOMOVIMIENTO = CAB.NOMOVIMIENTOPADRE AND ISNULL(CAB.BELICANTPED,0) = 0 " & _
                '    "AND CAB.ESTATUS = 'TER' AND ISNULL(CAB.BTRASDIR,0) = 0 " & vFiltro & _
                '    " AND DET.CANTIDADPEDIDA > DET.CANTIDAD " & _
                '    " ORDER BY CAB.FECHAMOVIMIENTO DESC"

                '    strSqlGlobalDet = "SELECT CAB.NOMOVIMIENTO, " & _
                '    "DET.CCODIGOP01_PRO, PRO.CNOMBREP01 AS TCNOMBREP01,UNI.CABREVIA01, " & _
                '    "DET.CANTIDAD, DET.CANTIDADPEDIDA , (DET.CANTIDADPEDIDA - DET.CANTIDAD) AS CANTIDADFALTANTE " & _
                '    "FROM CABMOVIMIENTOS CAB " & _
                '    "INNER JOIN DETMOVIMIENTOS DET ON CAB.NOMOVIMIENTO = DET.NOMOVIMIENTO " & _
                '    "INNER JOIN CATPRODUCTOS PRO ON DET.CCODIGOP01_PRO = PRO.CCODIGOP01 " & _
                '    "INNER JOIN CATUNIDADES UNI ON DET.CIDUNIDA01 = UNI.CIDUNIDA01 " & _
                '    "WHERE CAB.NOMOVIMIENTO = CAB.NOMOVIMIENTOPADRE AND ISNULL(CAB.BELICANTPED,0) = 0 AND CAB.ESTATUS = 'TER' " & _
                '    "AND ISNULL(CAB.BTRASDIR,0) = 0 AND DET.CANTIDADPEDIDA > DET.CANTIDAD " & vFiltro & _
                '    "ORDER BY CAB.NOMOVIMIENTO,DET.CONSECUTIVO"

                '    '"AND ISNULL(CAB.BTRASDIR,0) = 0 AND DET.CANTIDAD - DET.CANTIDADINGRESADA > 0 " & vFiltro & _
        End Select



        'If UCase(NomRep) = UCase("RepPedidosPen") Then
        '    '"CLI.DOMICILIO + '&&&&&&&&&&' + CLI.DOMICILIO  as DOMICILIO,CLI.CRFC, CLI.CEMAIL, " & _
        '    'If vValor1 = "" Then
        '    '    vValor1 = CInt(CmbCadena(0).SelectedValue)
        '    'End If

        '    'strSqlGlobal = "SELECT MOV.NOMOVIMIENTO AS NOMOVIMIENTO ,MOV.CCODIGOC01_CLI as cliente,CLI.CRAZONSO01, " & _
        '    '"CLI.DOMICILIO as DOMICILIO,CLI.CRFC, CLI.CEMAIL, " & _
        '    '"MOV.FECHAMOVIMIENTO,MOV.SUBTOTALGRAV,MOV.SUBTOTALEXENTO,MOV.IVA, MOV.DESCUENTO, " & _
        '    '"((MOV.SUBTOTALGRAV+MOV.SUBTOTALEXENTO+MOV.IVA)- MOV.DESCUENTO) AS IMPTOTAL, " & _
        '    '"MOV.CCODIGOA01,AGE.CNOMBREA01,MOV.VEHICULO, MOV.CHOFER, MOV.ESTATUS, " & _
        '    '"DET.CONSECUTIVO, " & _
        '    '"DET.CCODIGOP01_PRO, PROD.CNOMBREP01, " & _
        '    '"DET.CANTIDAD, " & _
        '    '"DET.CIDUNIDA01, UNI.CNOMBREU01, UNI.CABREVIA01, " & _
        '    '"'' AS CANTIDADSURTIDA, '' AS NOCAMARA, DET.TNOEQUIVALENTE, " & _
        '    '"DET.CIDUNIDA02 AS CIDUNIDA02 ,  ISNULL(UNIBASE.CNOMBREU01,'') AS CNOMBREU01UNI2 , " & _
        '    '"ISNULL(UNIBASE.CABREVIA01,'') AS CABREVIA01UNI2,mov.NoSerie as serie,mov.IdMovimiento as folio, MOV.CREFEREN01 AS REFERENCIA " & _
        '    '"FROM DETMOVIMIENTOS DET " & _
        '    '"INNER JOIN CABMOVIMIENTOS MOV ON DET.NOMOVIMIENTO = MOV.NOMOVIMIENTO  " & _
        '    '"INNER JOIN CATPRODUCTOS PROD ON DET.CCODIGOP01_PRO = PROD.CCODIGOP01 " & _
        '    '"INNER JOIN CATUNIDADES UNI ON DET.CIDUNIDA01 = UNI.CIDUNIDA01 " & _
        '    '"LEFT JOIN CATUNIDADES UNIBASE ON DET.CIDUNIDA02 = UNIBASE.CIDUNIDA01  " & _
        '    '"INNER JOIN CATCLIENTES CLI ON MOV.CCODIGOC01_CLI = CLI.CCODIGOC01  " & _
        '    '"INNER JOIN CATAGENTES AGE ON MOV.CCODIGOA01 = AGE.CCODIGOA01 " & _
        '    '" WHERE mov.NOMOVIMIENTO = " & Val(vValor1) & " order by det.consecutivo"
        'ElseIf UCase(NomRep) = UCase("RepMovtos") Then
        '    ''Dim vTipoMovimiento As String = ""
        '    ''Dim vESTATUS As String = ""
        '    ''Dim vTipoOrden As String = ""



        '    'If CmbCadena(0).Enabled Then
        '    '    'vTipoMovimiento = CmbCadena(0).SelectedValue
        '    '    If CmbCadena(0).SelectedValue <> "" Then
        '    '        vFiltro = "CAB.TipoMovimiento = '" & CmbCadena(0).SelectedValue & "'"
        '    '    End If

        '    'End If
        '    'If CmbCadena(1).Enabled Then
        '    '    'vESTATUS = CmbCadena(1).SelectedValue
        '    '    If CmbCadena(1).SelectedValue <> "" Then
        '    '        If vFiltro <> "" Then
        '    '            vFiltro = vFiltro & " AND CAB.ESTATUS = '" & CmbCadena(1).SelectedValue & "'"
        '    '        Else
        '    '            vFiltro = "CAB.ESTATUS = '" & CmbCadena(1).SelectedValue & "'"
        '    '        End If
        '    '    End If
        '    'End If
        '    'If CmbCadena(4).Enabled Then
        '    '    'vTipoOrden = CmbCadena(4).SelectedValue
        '    '    If CmbCadena(4).SelectedValue <> "" Then
        '    '        If vFiltro <> "" Then
        '    '            vFiltro = vFiltro & " AND CAB.TipoOrden = '" & CmbCadena(4).SelectedValue & "'"
        '    '        Else
        '    '            vFiltro = "CAB.TipoOrden = '" & CmbCadena(4).SelectedValue & "'"
        '    '        End If
        '    '    End If
        '    'End If

        '    'If CaleFecha1(7).Checked Then
        '    '    vFechaMovimiento = CaleFecha1(7).Value
        '    '    If vFiltro <> "" Then
        '    '        'and (FechaMovimiento >= '03/12/2014 00:00:00' and FechaMovimiento <= '03/12/2014 23:59:59')
        '    '        vFiltro = vFiltro & " AND CAB.FechaMovimiento >= '" & CaleFecha1(7).Value.ToShortDateString & " 00:00:00' AND CAB.FechaMovimiento <= '" & CaleFecha2(7).Value.ToShortDateString & " 23:59:59'"
        '    '    Else
        '    '        vFiltro = vFiltro & "CAB.FechaMovimiento >= '" & CaleFecha1(7).Value.ToShortDateString & " 00:00:00' AND CAB.FechaMovimiento <= '" & CaleFecha2(7).Value.ToShortDateString & " 23:59:59'"
        '    '    End If
        '    'End If

        '    'strSqlGlobal = "SELECT CAB.NOMOVIMIENTO,CAB.IDMOVIMIENTODIA, " & _
        '    '"CAB.TIPOMOVIMIENTO,TIPMOV.DESCRIPCION  AS NOMTIPOMOVIMIENTO, " & _
        '    '"CAB.IDCLIENTE,(CLI.NOMBRE + ' ' + CLI.APELLIDOPAT + ' ' + CLI.APELLIDOMAT ) AS NOMCLIENTE, " & _
        '    '"CAB.ESTATUS, " & _
        '    '"CAB.TIPOORDEN,ISNULL(TIPORD.DESCRIPCION,'') AS NOMTIPOORDEN, " & _
        '    '"CAB.FECHAMOVIMIENTO,CAB.DESCUENTO,CAB.SUBTOTALGRAV,CAB.SUBTOTALEXENTO,CAB.IVA, " & _
        '    '"(CAB.SUBTOTALGRAV+CAB.SUBTOTALEXENTO+CAB.IVA-CAB.DESCUENTO) AS TOTAL, " & _
        '    '"CAB.NUMCORTEID, COR.NUMCORTE , COR.ESTATUS , COR.FECCORTE " & _
        '    '"FROM CABMOVIMIENTOS CAB " & _
        '    '"INNER JOIN CATCLIENTES CLI ON CAB.IDCLIENTE = CLI.IDCLIENTE " & _
        '    '"LEFT JOIN CATDESCRIPCIONES TIPORD ON CAB.TIPOORDEN = TIPORD.CVEDESCRIPCION AND TIPORD.TIPODESCRIPCION = 'TIPORD' " & _
        '    '"INNER JOIN CABCORTES COR ON CAB.NUMCORTEID = COR.NUMCORTEID " & _
        '    '"INNER JOIN CATDESCRIPCIONES TIPMOV ON CAB.TIPOMOVIMIENTO  = TIPMOV.CVEDESCRIPCION AND TIPMOV.TIPODESCRIPCION = 'TIPMOV' " & _
        '    'IIf(vFiltro <> "", " WHERE " & vFiltro, "")

        'ElseIf UCase(NomRep) = UCase("RepOrdXEst") Then
        '    'vFiltro = " TIPOMOVIMIENTO = 'VTA'"
        '    'If CaleFecha1(0).Checked Then
        '    '    vFechaMovimiento = CaleFecha1(0).Value
        '    '    If vFiltro <> "" Then
        '    '        'and (FechaMovimiento >= '03/12/2014 00:00:00' and FechaMovimiento <= '03/12/2014 23:59:59')
        '    '        vFiltro = vFiltro & " AND MOV.FECHAMOVIMIENTO >= '" & CaleFecha1(0).Value.ToShortDateString & " 00:00:00' AND MOV.FECHAMOVIMIENTO <= '" & CaleFecha2(0).Value.ToShortDateString & " 23:59:59'"
        '    '    Else
        '    '        vFiltro = vFiltro & "MOV.FECHAMOVIMIENTO >= '" & CaleFecha1(0).Value.ToShortDateString & " 00:00:00' AND MOV.FECHAMOVIMIENTO <= '" & CaleFecha2(0).Value.ToShortDateString & " 23:59:59'"
        '    '    End If
        '    'End If
        '    'If CmbCadena(1).Enabled Then
        '    '    If CmbCadena(1).SelectedValue <> "" Then
        '    '        If vFiltro <> "" Then
        '    '            vFiltro = vFiltro & " AND mov.TipoOrden = '" & CmbCadena(1).SelectedValue & "'"
        '    '        Else
        '    '            vFiltro = "mov.TipoOrden = '" & CmbCadena(1).SelectedValue & "'"
        '    '        End If
        '    '    End If
        '    'End If
        '    'If CmbCadena(3).Enabled Then
        '    '    If CmbCadena(3).SelectedValue <> "" Then
        '    '        If vFiltro <> "" Then
        '    '            vFiltro = vFiltro & " AND NomEstatus = '" & CmbCadena(3).SelectedValue & "'"
        '    '        Else
        '    '            vFiltro = "NomEstatus = '" & CmbCadena(3).SelectedValue & "'"
        '    '        End If
        '    '    End If
        '    'End If

        '    'strSqlGlobal = "SELECT CAST(MOV.FECHAMOVIMIENTO AS DATE)  AS FECHA , " & _
        '    '"MOV.TIPOORDEN, MOV.ESTATUS,  EST.DESCRIPCION AS NOMESTATUS, " & _
        '    '"COUNT(IDMOVIMIENTODIA ) AS NUMPEDIDOS, " & _
        '    '"SUM(MOV.SUBTOTALGRAV + MOV.SUBTOTALEXENTO + MOV.IVA ) AS TOTAL " & _
        '    '"FROM CABMOVIMIENTOS MOV " & _
        '    '"INNER JOIN CATDESCRIPCIONES EST ON MOV.ESTATUS = EST.CVEDESCRIPCION AND EST.TIPODESCRIPCION = 'ESV' " & IIf(vFiltro <> "", " WHERE " & vFiltro, "") & _
        '    '" GROUP BY CAST(MOV.FECHAMOVIMIENTO AS DATE),MOV.TIPOORDEN,MOV.ESTATUS,EST.DESCRIPCION"

        '    ''            "WHERE TIPOMOVIMIENTO = 'VTA' " & _
        '    ''"AND MOV.FECHAMOVIMIENTO >= '7/9/2015 00:00:00' AND MOV.FECHAMOVIMIENTO <= '7/9/2015 23:59:59' " & _
        'ElseIf UCase(NomRep) = UCase("RepProdMasVend") Then
        '    'vFiltro = " TIPOMOVIMIENTO = 'VTA'"
        '    'If CaleFecha1(0).Checked Then
        '    '    vFechaMovimiento = CaleFecha1(0).Value
        '    '    If vFiltro <> "" Then
        '    '        'and (FechaMovimiento >= '03/12/2014 00:00:00' and FechaMovimiento <= '03/12/2014 23:59:59')
        '    '        vFiltro = vFiltro & " AND CAB.FECHAMOVIMIENTO >= '" & CaleFecha1(0).Value.ToShortDateString & " 00:00:00' AND CAB.FECHAMOVIMIENTO <= '" & CaleFecha2(0).Value.ToShortDateString & " 23:59:59'"
        '    '    Else
        '    '        vFiltro = vFiltro & "CAB.FECHAMOVIMIENTO >= '" & CaleFecha1(0).Value.ToShortDateString & " 00:00:00' AND CAB.FECHAMOVIMIENTO <= '" & CaleFecha2(0).Value.ToShortDateString & " 23:59:59'"
        '    '    End If
        '    'End If
        '    'If CmbCadena(2).Enabled Then
        '    '    If CmbCadena(2).SelectedValue <> "" Then
        '    '        If vFiltro <> "" Then
        '    '            vFiltro = vFiltro & " AND DET.IDPROD = '" & CmbCadena(2).SelectedValue & "'"
        '    '        Else
        '    '            vFiltro = "DET.IDPROD = '" & CmbCadena(2).SelectedValue & "'"
        '    '        End If
        '    '    End If
        '    'End If
        '    'If CmbCadena(4).Enabled Then
        '    '    If CmbCadena(4).SelectedValue <> "" Then
        '    '        If vFiltro <> "" Then
        '    '            vFiltro = vFiltro & " AND UNI.NOMUNIDAD = '" & CmbCadena(4).SelectedValue & "'"
        '    '        Else
        '    '            vFiltro = "UNI.NOMUNIDAD = '" & CmbCadena(4).SelectedValue & "'"
        '    '        End If
        '    '    End If
        '    'End If

        '    'strSqlGlobal = "SELECT CAST(CAB.FECHAMOVIMIENTO AS DATE)  AS FECHA,DET.IDPROD, PROD.NOMBREPROD, DET.IDUNIDAD,UNI.NOMUNIDAD, " & _
        '    '"COUNT(DET.CANTIDAD ) AS CANTIDAD,SUM((DET.CANTIDAD * DET.PRECIO)) AS TOTAL " & _
        '    '"FROM DETMOVIMIENTOS DET " & _
        '    '"INNER JOIN CABMOVIMIENTOS CAB ON DET.NOMOVIMIENTO = CAB.NOMOVIMIENTO " & _
        '    '"INNER JOIN CATPRODUCTOS PROD ON DET.IDPROD = PROD.IDPROD " & _
        '    '"INNER JOIN CATUNIDADES UNI ON DET.IDUNIDAD = UNI.IDUNIDAD " & IIf(vFiltro <> "", " WHERE " & vFiltro, "") & _
        '    '" GROUP BY CAST(CAB.FECHAMOVIMIENTO AS DATE),DET.IDPROD, PROD.NOMBREPROD,  DET.IDUNIDAD, UNI.NOMUNIDAD " & IIf(OrdenaCampos <> "", " ORDER BY " & OrdenaCampos & " , PROD.NOMBREPROD, UNI.NOMUNIDAD", " ORDER BY CANTIDAD DESC , PROD.NOMBREPROD, UNI.NOMUNIDAD")



        '    ''strSqlGlobal = "SELECT CAST(CAB.FECHAMOVIMIENTO AS DATE)  AS FECHA,DET.IDPROD, PROD.NOMBREPROD, DET.IDUNIDAD,UNI.NOMUNIDAD, " & _
        '    ''"COUNT(DET.CANTIDAD ) AS CANTIDAD  " & _
        '    ''"FROM DETMOVIMIENTOS DET " & _
        '    ''"INNER JOIN CABMOVIMIENTOS CAB ON DET.NOMOVIMIENTO = CAB.NOMOVIMIENTO " & _
        '    ''"INNER JOIN CATPRODUCTOS PROD ON DET.IDPROD = PROD.IDPROD " & _
        '    ''"INNER JOIN CATUNIDADES UNI ON DET.IDUNIDAD = UNI.IDUNIDAD " & IIf(vFiltro <> "", " WHERE " & vFiltro, "") & _
        '    ''" GROUP BY CAST(CAB.FECHAMOVIMIENTO AS DATE),DET.IDPROD, PROD.NOMBREPROD,  DET.IDUNIDAD, UNI.NOMUNIDAD " & _
        '    ''"ORDER BY CANTIDAD DESC, PROD.NOMBREPROD, UNI.NOMUNIDAD"

        'ElseIf UCase(NomRep) = UCase("RepProdVend") Then
        '    'vFiltro = " TIPOMOVIMIENTO = 'VTA'"
        '    'If CaleFecha1(0).Checked Then
        '    '    vFechaMovimiento = CaleFecha1(0).Value
        '    '    If vFiltro <> "" Then
        '    '        'and (FechaMovimiento >= '03/12/2014 00:00:00' and FechaMovimiento <= '03/12/2014 23:59:59')
        '    '        vFiltro = vFiltro & " AND CAB.FECHAMOVIMIENTO >= '" & CaleFecha1(0).Value.ToShortDateString & " 00:00:00' AND CAB.FECHAMOVIMIENTO <= '" & CaleFecha2(0).Value.ToShortDateString & " 23:59:59'"
        '    '    Else
        '    '        vFiltro = vFiltro & "CAB.FECHAMOVIMIENTO >= '" & CaleFecha1(0).Value.ToShortDateString & " 00:00:00' AND CAB.FECHAMOVIMIENTO <= '" & CaleFecha2(0).Value.ToShortDateString & " 23:59:59'"
        '    '    End If
        '    'End If
        '    'If CmbCadena(2).Enabled Then
        '    '    If CmbCadena(2).SelectedValue <> "" Then
        '    '        If vFiltro <> "" Then
        '    '            vFiltro = vFiltro & " AND DET.IDPROD = '" & CmbCadena(2).SelectedValue & "'"
        '    '        Else
        '    '            vFiltro = "DET.IDPROD = '" & CmbCadena(2).SelectedValue & "'"
        '    '        End If
        '    '    End If
        '    'End If
        '    'If CmbCadena(4).Enabled Then
        '    '    If CmbCadena(4).SelectedValue <> "" Then
        '    '        If vFiltro <> "" Then
        '    '            vFiltro = vFiltro & " AND UNI.NOMUNIDAD = '" & CmbCadena(4).SelectedValue & "'"
        '    '        Else
        '    '            vFiltro = "UNI.NOMUNIDAD = '" & CmbCadena(4).SelectedValue & "'"
        '    '        End If
        '    '    End If
        '    'End If
        '    'If CmbCadena(8).Enabled Then
        '    '    If CmbCadena(8).SelectedValue <> "" Then
        '    '        If vFiltro <> "" Then
        '    '            If CmbCadena(8).SelectedValue = "NO" Then
        '    '                vFiltro = vFiltro & " AND DET.ESCOMPLEMENTO = 0 "
        '    '            Else
        '    '                vFiltro = vFiltro & " AND DET.ESCOMPLEMENTO = 1 "
        '    '            End If

        '    '        Else
        '    '            If CmbCadena(8).SelectedValue = "NO" Then
        '    '                vFiltro = "DET.ESCOMPLEMENTO = 0"
        '    '            Else
        '    '                vFiltro = "DET.ESCOMPLEMENTO = 1"
        '    '            End If

        '    '        End If
        '    '    End If
        '    'End If

        '    'strSqlGlobal = "SELECT CAST(CAB.FECHAMOVIMIENTO AS DATE)  AS FECHA ,DET.IDPROD, PROD.NOMBREPROD, DET.IDUNIDAD, UNI.NOMUNIDAD, " & _
        '    '"SUM(DET.CANTIDAD) AS CANTIDAD, SUM(DET.PRECIO) AS PRECIO, SUM((DET.CANTIDAD * DET.PRECIO)) AS TOTAL, " & _
        '    '"DET.ESCOMPLEMENTO, CASE   DET.ESCOMPLEMENTO WHEN 1 THEN 'SI' ELSE 'NO' END AS ESCOMP " & _
        '    '"FROM DETMOVIMIENTOS DET " & _
        '    '"INNER JOIN CABMOVIMIENTOS CAB ON DET.NOMOVIMIENTO = CAB.NOMOVIMIENTO " & _
        '    '"INNER JOIN CATPRODUCTOS PROD ON DET.IDPROD = PROD.IDPROD " & _
        '    '"INNER JOIN CATUNIDADES UNI ON DET.IDUNIDAD = UNI.IDUNIDAD " & IIf(vFiltro <> "", " WHERE " & vFiltro, "") & _
        '    '" GROUP BY CAST(CAB.FECHAMOVIMIENTO AS DATE),DET.IDPROD, PROD.NOMBREPROD, DET.ESCOMPLEMENTO  ,DET.IDUNIDAD, UNI.NOMUNIDAD " & _
        '    '"ORDER BY FECHA,PROD.NOMBREPROD,DET.IDUNIDAD"

        '    ''            "WHERE CAB.TIPOMOVIMIENTO = 'VTA' " & _
        '    ''"AND CAB.FECHAMOVIMIENTO >= '7/9/2015 00:00:00' AND CAB.FECHAMOVIMIENTO <= '7/9/2015 23:59:59' " & _
        'ElseIf UCase(NomRep) = UCase("RepTraspFaltantes") Then
        '    If CaleFecha1(1).Checked Then
        '        'vFechaMovimiento = CaleFecha1(0).Value
        '        vFiltro = ""
        '        vFiltro = vFiltro & " AND CAB.FECHAMOVIMIENTO >= '" & CaleFecha1(1).Value.ToShortDateString & " 00:00:00' AND CAB.FECHAMOVIMIENTO <= '" & CaleFecha2(1).Value.ToShortDateString & " 23:59:59'"
        '        'If vFiltro <> "" Then
        '        '    vFiltro = vFiltro & " AND CAB.FECHAMOVIMIENTO >= '" & CaleFecha1(1).Value.ToShortDateString & " 00:00:00' AND CAB.FECHAMOVIMIENTO <= '" & CaleFecha2(1).Value.ToShortDateString & " 23:59:59'"
        '        'Else
        '        '    vFiltro = vFiltro & "CAB.FECHAMOVIMIENTO >= '" & CaleFecha1(1).Value.ToShortDateString & " 00:00:00' AND CAB.FECHAMOVIMIENTO <= '" & CaleFecha2(1).Value.ToShortDateString & " 23:59:59'"
        '        'End If
        '    End If
        '    strSqlGlobal = "SELECT CAB.NOMOVIMIENTO ,CAB.FECHAMOVIMIENTO, " & _
        '    "CAB.CIDDOCUM02 AS CIDDOCUM02_TRAS,CONC_TRAS.CNOMBREC01 AS CNOMBREC01_TRAS, " & _
        '    "(CAST(CAB.CIDALMACEN_SAL AS VARCHAR(4)) + '-' +  ALM_SAL.CNOMBREA01) AS NOMALMSAL, " & _
        '    "CAB.NOSALIDA, (CAB_SAL.NOSERIE + '-' + CAST(CAB_SAL.IDMOVIMIENTO AS VARCHAR(10))) AS FOLIO_SAL, " & _
        '    "CAB_SAL.CIDDOCUM02 AS CIDDOCUM02_SAL, CONC_SAL.CNOMBREC01 AS CNOMBREC01_SAL, " & _
        '    "(CAST(CAB.CIDALMACEN_ENT AS VARCHAR(4)) + '-' + ALM_ENT.CNOMBREA01) AS NOMALMENT, " & _
        '    "CAB.NOENTRADA , (CAB_ENT.NOSERIE + '-' + CAST(CAB_ENT.IDMOVIMIENTO AS VARCHAR(10))) AS FOLIO_ENT, " & _
        '    "CAB_ENT.CIDDOCUM02 AS CIDDOCUM02_ENT, CONC_ENT.CNOMBREC01 AS CNOMBREC01_ENT " & _
        '    "FROM CABMOVIMIENTOS CAB " & _
        '    "INNER JOIN CATALMACENES ALM_SAL ON CAB.CIDALMACEN_SAL = ALM_SAL.CCODIGOA01 " & _
        '    "INNER JOIN CATALMACENES ALM_ENT ON CAB.CIDALMACEN_ENT = ALM_ENT.CCODIGOA01 " & _
        '    "INNER JOIN CABMOVIMIENTOS CAB_SAL ON CAB.ULTIMASALIDA = CAB_SAL.NOMOVIMIENTO " & _
        '    "INNER JOIN CABMOVIMIENTOS CAB_ENT ON CAB.ULTIMAENTRADA  = CAB_ENT.NOMOVIMIENTO " & _
        '    "INNER JOIN CATCONCEPTOS CONC_SAL ON CAB_SAL.CIDDOCUM02 = CONC_SAL.CCODIGOC01 " & _
        '    "INNER JOIN CATCONCEPTOS CONC_ENT ON CAB_ENT.CIDDOCUM02 = CONC_ENT.CCODIGOC01 " & _
        '    "INNER JOIN CATCONCEPTOS CONC_TRAS ON CAB.CIDDOCUM02 = CONC_TRAS.CCODIGOC01 " & _
        '    "WHERE CAB.NOMOVIMIENTO = CAB.NOMOVIMIENTOPADRE AND CAB.BCANTCOMPLETA = 0 " & _
        '    "AND CAB.ESTATUS = 'TER' AND ISNULL(CAB.BTRASDIR,0) = 0 " & vFiltro & _
        '    " ORDER BY CAB.FECHAMOVIMIENTO DESC"

        '    strSqlGlobalDet = "SELECT CAB.NOMOVIMIENTO, " & _
        '    "DET.CCODIGOP01_PRO, PRO.CNOMBREP01 AS TCNOMBREP01,UNI.CABREVIA01, " & _
        '    "DET.CANTIDAD, DET.CANTIDADINGRESADA , (DET.CANTIDAD - DET.CANTIDADINGRESADA) AS CANTIDADFALTANTE " & _
        '    "FROM CABMOVIMIENTOS CAB " & _
        '    "INNER JOIN DETMOVIMIENTOS DET ON CAB.NOMOVIMIENTO = DET.NOMOVIMIENTO " & _
        '    "INNER JOIN CATPRODUCTOS PRO ON DET.CCODIGOP01_PRO = PRO.CCODIGOP01 " & _
        '    "INNER JOIN CATUNIDADES UNI ON DET.CIDUNIDA01 = UNI.CIDUNIDA01 " & _
        '    "WHERE CAB.NOMOVIMIENTO = CAB.NOMOVIMIENTOPADRE AND CAB.BCANTCOMPLETA = 0 AND CAB.ESTATUS = 'TER' " & _
        '    "AND ISNULL(CAB.BTRASDIR,0) = 0 AND DET.CANTIDAD - DET.CANTIDADINGRESADA > 0 " & _
        '    "ORDER BY CAB.NOMOVIMIENTO,DET.CONSECUTIVO"

        '    'vFiltro = " CAB.TIPOMOVIMIENTO = 'VTA'"
        '    'If CaleFecha1(0).Checked Then
        '    '    vFechaMovimiento = CaleFecha1(0).Value
        '    '    If vFiltro <> "" Then
        '    '        'and (FechaMovimiento >= '03/12/2014 00:00:00' and FechaMovimiento <= '03/12/2014 23:59:59')
        '    '        vFiltro = vFiltro & " AND CAB.FECHAMOVIMIENTO >= '" & CaleFecha1(0).Value.ToShortDateString & " 00:00:00' AND CAB.FECHAMOVIMIENTO <= '" & CaleFecha2(0).Value.ToShortDateString & " 23:59:59'"
        '    '    Else
        '    '        vFiltro = vFiltro & "CAB.FECHAMOVIMIENTO >= '" & CaleFecha1(0).Value.ToShortDateString & " 00:00:00' AND CAB.FECHAMOVIMIENTO <= '" & CaleFecha2(0).Value.ToShortDateString & " 23:59:59'"
        '    '    End If
        '    'End If


        '    'strSqlGlobal = "SELECT CAST(CAB.FECHAMOVIMIENTO AS DATE)  AS FECHAGPO , CAB.FECHAMOVIMIENTO AS FECHA, CAB.IDMOVIMIENTODIA AS NOORDEN,CAB.TIPOORDEN, " & _
        '    '"(CAST(CAB.IDCLIENTE AS VARCHAR(10)) + ' - ' + CLI.NOMBRE + ' ' + CLI.APELLIDOPAT + ' ' +CLI.APELLIDOMAT) AS CLIENTE, " & _
        '    '"EST.DESCRIPCION AS ESTATUS, (CAB.SUBTOTALGRAV + CAB.SUBTOTALEXENTO + CAB.IVA ) AS TOTAL " & _
        '    '"FROM CABMOVIMIENTOS CAB " & _
        '    '"INNER JOIN CATCLIENTES CLI ON CAB.IDCLIENTE = CLI.IDCLIENTE " & _
        '    '"INNER JOIN CATDESCRIPCIONES EST ON CAB.ESTATUS = EST.CVEDESCRIPCION AND EST.TIPODESCRIPCION = 'ESV' " & IIf(vFiltro <> "", " WHERE " & vFiltro, "") & _
        '    '" ORDER BY CAB.FECHAMOVIMIENTO, CAB.IDMOVIMIENTODIA"

        '    'If TryCast(GetControl(0, 0), CheckBox).Checked Then
        '    '    '    strSqlGlobal = "SELECT CAST(CAB.FECHAMOVIMIENTO AS DATE)  AS FECHAGPO , CAB.FECHAMOVIMIENTO AS FECHA, CAB.IDMOVIMIENTODIA AS NOORDEN,CAB.TIPOORDEN, " & _
        '    '    '"(CAST(CAB.IDCLIENTE AS VARCHAR(10)) + ' - ' + CLI.NOMBRE + ' ' + CLI.APELLIDOPAT + ' ' +CLI.APELLIDOMAT) AS CLIENTE, " & _
        '    '    '"EST.DESCRIPCION AS ESTATUS, (CAB.SUBTOTALGRAV + CAB.SUBTOTALEXENTO + CAB.IVA ) AS TOTAL, " & _
        '    '    '"DET.IDPROD, PROD.NOMBREPROD, DET.IDUNIDAD, UNI.NOMUNIDAD, " & _
        '    '    '"SUM(DET.CANTIDAD) AS CANTIDAD, DET.PRECIO , SUM((DET.CANTIDAD * DET.PRECIO)) AS IMPORTE " & _
        '    '    '"FROM CABMOVIMIENTOS CAB " & _
        '    '    '"INNER JOIN DETMOVIMIENTOS DET ON CAB.NOMOVIMIENTO = DET.NOMOVIMIENTO " & _
        '    '    '"INNER JOIN CATCLIENTES CLI ON CAB.IDCLIENTE = CLI.IDCLIENTE " & _
        '    '    '"INNER JOIN CATDESCRIPCIONES EST ON CAB.ESTATUS = EST.CVEDESCRIPCION AND EST.TIPODESCRIPCION = 'ESV' " & _
        '    '    '"INNER JOIN CATPRODUCTOS PROD ON DET.IDPROD = PROD.IDPROD " & _
        '    '    '"INNER JOIN CATUNIDADES UNI ON DET.IDUNIDAD = UNI.IDUNIDAD " & IIf(vFiltro <> "", " WHERE " & vFiltro, "") & _
        '    '    '" GROUP BY CAB.IDMOVIMIENTODIA, DET.IDPROD, PROD.NOMBREPROD, DET.IDUNIDAD, UNI.NOMUNIDAD,DET.PRECIO, " & _
        '    '    '"CAST(CAB.FECHAMOVIMIENTO AS DATE),CAB.FECHAMOVIMIENTO,CAB.TIPOORDEN,CAB.IDCLIENTE,CLI.NOMBRE,CLI.APELLIDOPAT,CLI.APELLIDOMAT,EST.DESCRIPCION, " & _
        '    '    '"CAB.SUBTOTALGRAV,CAB.SUBTOTALEXENTO,CAB.IVA " & _
        '    '    '"ORDER BY CAB.FECHAMOVIMIENTO, CAB.IDMOVIMIENTODIA"

        '    '    'Aqui va el detalle
        '    '    strSqlGlobalDet = "SELECT CAB.FECHAMOVIMIENTO AS FECHA,CAB.IDMOVIMIENTODIA AS NOORDEN, DET.IDPROD, PROD.NOMBREPROD, DET.IDUNIDAD, UNI.NOMUNIDAD, " & _
        '    '     "SUM(DET.CANTIDAD) AS CANTIDAD, DET.PRECIO , SUM((DET.CANTIDAD * DET.PRECIO)) AS IMPORTE " & _
        '    '     "FROM DETMOVIMIENTOS DET " & _
        '    '     "INNER JOIN CABMOVIMIENTOS CAB ON DET.NOMOVIMIENTO = CAB.NOMOVIMIENTO " & _
        '    '     "INNER JOIN CATPRODUCTOS PROD ON DET.IDPROD = PROD.IDPROD " & _
        '    '     "INNER JOIN CATUNIDADES UNI ON DET.IDUNIDAD = UNI.IDUNIDAD " & IIf(vFiltro <> "", " WHERE " & vFiltro, "") & _
        '    '     "GROUP BY CAB.FECHAMOVIMIENTO,CAB.IDMOVIMIENTODIA, DET.IDPROD, PROD.NOMBREPROD, DET.IDUNIDAD, UNI.NOMUNIDAD,DET.PRECIO " & _
        '    '     "ORDER BY CAB.IDMOVIMIENTODIA,DET.IDPROD, DET.IDUNIDAD"
        '    'Else
        '    '    strSqlGlobalDet = ""
        '    'End If



        'ElseIf UCase(NomRep) = UCase("RepRemision") Then
        '    '"CAB.CCODIGOC01_CLI AS CLIENTE, CLI.CRAZONSO01, CLI.DOMICILIO + '&&&&&&&&&&' + CLI.DOMICILIO as DOMICILIO, CLI.CRFC, " & _
        '    If vValor1 = "" Then
        '        'TIPO DE DOCUMENTO
        '        'vValor1 = CmbCadena(0).SelectedValue
        '        'ID MOVIMIENTO
        '        vValor1 = CInt(CmbCadena(0).SelectedValue)
        '        'SERIE
        '        'vValor3 = CmbCadena(1).SelectedValue

        '    End If


        '    'DET.CANTIDAD

        '    'strSqlGlobal = "DECLARE @ULTIMAENTRADA INT " & _
        '    '"SET @ULTIMAENTRADA = (SELECT ULTIMAENTRADA FROM CABMOVIMIENTOS WHERE NOMOVIMIENTO = " & Val(vValor2) & ") " & _
        '    '"DECLARE @ULTIMASALIDA INT " & _
        '    '"SET @ULTIMASALIDA = (SELECT ULTIMASALIDA FROM CABMOVIMIENTOS WHERE NOMOVIMIENTO = " & Val(vValor2) & ") " & _
        '    '"SELECT CAB.NOMOVIMIENTO,CAB.NOSERIE AS SERIE, CAB.IDMOVIMIENTO AS FOLIO, CAB.FECHAMOVIMIENTO, CONC_TRAS.CNOMBREC01 AS CNOMBREC01_TRAS, " & _
        '    '"CAB.CCODIGOC01_CLI AS CLIENTE, '' AS CRAZONSO01, ''  AS DOMICILIO, '' AS CRFC, " & _
        '    '"CAB.CREFEREN01 AS REFERENCIA, CAB.COBSERVA01 AS OBSERVACIONES, " & _
        '    '"CAB.SUBTOTALEXENTO , CAB.SUBTOTALGRAV, CAB.IVA, CAB.DESCUENTO, " & _
        '    '"(CAB.SUBTOTALEXENTO + CAB.SUBTOTALGRAV) AS SUBTOTAL, " & _
        '    '"(((CAB.SUBTOTALEXENTO + CAB.SUBTOTALGRAV)-(CAB.DESCUENTO))+ CAB.IVA) AS TOTAL, CAB.CIDDOCUM02, CAB.TIPOMOVIMIENTO, " & _
        '    '"DET.CCODIGOP01_PRO, PROD.CNOMBREP01, DET.CIDUNIDA01 ,UNI.CABREVIA01 AS UNIDAD, UNI.CNOMBREU01, " & _
        '    '"CAST(DET.CANTIDAD AS INT) AS CANTIDAD , DET.COSTO , (DET.CANTIDAD * DET.COSTO) AS IMPORTE, DET.CONSECUTIVO, " & _
        '    '"CAB_ENT.NOMOVIMIENTO AS NOMOVENT,CAB_ENT.CIDDOCUM02 AS CIDDOCUM02_ENT,CAB_ENT.NOSERIE AS SERIE_ENT, CAB_ENT.IDMOVIMIENTO AS FOLIO_ENT, CONC_ENT.CNOMBREC01 AS CNOMBREC01_ENT, " & _
        '    '"CAB.CIDALMACEN_SAL , ALM_SAL.CNOMBREA01 AS NOMALMACEN_SAL,  " & _
        '    '"CAB_SAL.NOMOVIMIENTO AS NOMOVSAL,CAB_SAL.CIDDOCUM02 AS CIDDOCUM02_SAL,CAB_SAL.NOSERIE AS SERIE_SAL, CAB_SAL.IDMOVIMIENTO AS FOLIO_SAL, CONC_SAL.CNOMBREC01 AS CNOMBREC01_SAL, " & _
        '    '"CAB.CIDALMACEN_ENT , ALM_ENT.CNOMBREA01 AS NOMALMACEN_ENT  " & _
        '    '"FROM CABMOVIMIENTOS CAB " & _
        '    '"INNER JOIN CABMOVIMIENTOS CAB_ENT ON CAB.NOMOVIMIENTO = CAB_ENT.NOMOVIMIENTOPADRE AND CAB_ENT.NOMOVIMIENTO = @ULTIMAENTRADA " & _
        '    '"INNER JOIN CABMOVIMIENTOS CAB_SAL ON CAB.NOMOVIMIENTO = CAB_SAL.NOMOVIMIENTOPADRE AND CAB_SAL.NOMOVIMIENTO = @ULTIMASALIDA " & _
        '    '"INNER JOIN DETMOVIMIENTOS DET ON CAB_SAL.NOMOVIMIENTO = DET.NOMOVIMIENTO AND CAB_SAL.NOMOVIMIENTO = @ULTIMASALIDA " & _
        '    '"INNER JOIN CATPRODUCTOS PROD ON DET.CCODIGOP01_PRO = PROD.CCODIGOP01 " & _
        '    '"INNER JOIN CATUNIDADES UNI ON DET.CIDUNIDA01 = UNI.CIDUNIDA01 " & _
        '    '"INNER JOIN CATCONCEPTOS CONC_SAL ON CAB_SAL.CIDDOCUM02 = CONC_SAL.CCODIGOC01 " & _
        '    '"INNER JOIN CATCONCEPTOS CONC_ENT ON CAB_ENT.CIDDOCUM02 = CONC_ENT.CCODIGOC01 " & _
        '    '"INNER JOIN CATCONCEPTOS CONC_TRAS ON CAB.CIDDOCUM02 = CONC_TRAS.CCODIGOC01 " & _
        '    '"INNER JOIN CATALMACENES ALM_SAL ON CAB.CIDALMACEN_SAL = ALM_SAL.CCODIGOA01 " & _
        '    '"INNER JOIN CATALMACENES ALM_ENT ON CAB.CIDALMACEN_ENT  = ALM_ENT.CCODIGOA01 " & _
        '    '"WHERE CAB.TIPOMOVIMIENTO = '" & vValor1 & "' AND CAB.NOMOVIMIENTO =  " & Val(vValor2) & _
        '    '"ORDER BY DET.CONSECUTIVO"

        '    '8/DICIEMBRE /2015
        '    strSqlGlobal = "DECLARE @ULTIMAENTRADA INT " & _
        '    "SET @ULTIMAENTRADA = (SELECT NoEntrada FROM CABMOVIMIENTOS WHERE NOMOVIMIENTO =  " & Val(vValor1) & " )  " & _
        '    "DECLARE @ULTIMASALIDA INT  " & _
        '    "SET @ULTIMASALIDA = (SELECT NoSalida FROM CABMOVIMIENTOS WHERE NOMOVIMIENTO =  " & Val(vValor1) & " )  " & _
        '    "SELECT CAB.NOMOVIMIENTO,CAB.NOSERIE AS SERIE, CAB.IDMOVIMIENTO AS FOLIO, CAB.FECHAMOVIMIENTO, CONC_TRAS.CNOMBREC01 AS CNOMBREC01_TRAS, " & _
        '    "CAB.CCODIGOC01_CLI AS CLIENTE, '' AS CRAZONSO01, ''  AS DOMICILIO, '' AS CRFC, " & _
        '    "CAB.CREFEREN01 AS REFERENCIA, CAB.COBSERVA01 AS OBSERVACIONES, " & _
        '    "CAB.SUBTOTALEXENTO , CAB.SUBTOTALGRAV, CAB.IVA, CAB.DESCUENTO, " & _
        '    "(CAB.SUBTOTALEXENTO + CAB.SUBTOTALGRAV) AS SUBTOTAL, " & _
        '    "(((CAB.SUBTOTALEXENTO + CAB.SUBTOTALGRAV)-(CAB.DESCUENTO))+ CAB.IVA) AS TOTAL, CAB.CIDDOCUM02, CAB.TIPOMOVIMIENTO, " & _
        '    "DET.CCODIGOP01_PRO, PROD.CNOMBREP01, DET.CIDUNIDA01 ,UNI.CABREVIA01 AS UNIDAD, UNI.CNOMBREU01, " & _
        '    "CAST(DET.CANTIDAD AS INT) AS CANTIDAD , DET.COSTO , (DET.CANTIDAD * DET.COSTO) AS IMPORTE, DET.CONSECUTIVO, " & _
        '    "CAB_ENT.NOMOVIMIENTO AS NOMOVENT,CAB_ENT.CIDDOCUM02 AS CIDDOCUM02_ENT,CAB_ENT.NOSERIE AS SERIE_ENT, CAB_ENT.IDMOVIMIENTO AS FOLIO_ENT, CONC_ENT.CNOMBREC01 AS CNOMBREC01_ENT, " & _
        '    "CAB.CIDALMACEN_SAL , ALM_SAL.CNOMBREA01 AS NOMALMACEN_SAL, " & _
        '    "CAB_SAL.NOMOVIMIENTO AS NOMOVSAL,CAB_SAL.CIDDOCUM02 AS CIDDOCUM02_SAL,CAB_SAL.NOSERIE AS SERIE_SAL, CAB_SAL.IDMOVIMIENTO AS FOLIO_SAL, CONC_SAL.CNOMBREC01 AS CNOMBREC01_SAL, " & _
        '    "CAB.CIDALMACEN_ENT , ALM_ENT.CNOMBREA01 AS NOMALMACEN_ENT,cab.UsuarioModifica " & _
        '    "FROM CABMOVIMIENTOS CAB " & _
        '    "INNER JOIN CABMOVIMIENTOS CAB_ENT ON CAB.NOMOVIMIENTOPADRE = CAB_ENT.NOMOVIMIENTOPADRE AND CAB_ENT.NOMOVIMIENTO = @ULTIMAENTRADA " & _
        '    "INNER JOIN CABMOVIMIENTOS CAB_SAL ON CAB.NOMOVIMIENTOPADRE = CAB_SAL.NOMOVIMIENTOPADRE AND CAB_SAL.NOMOVIMIENTO = @ULTIMASALIDA " & _
        '    "INNER JOIN DETMOVIMIENTOS DET ON DET.NOMOVIMIENTO = @ULTIMAENTRADA " & _
        '    "INNER JOIN CATPRODUCTOS PROD ON DET.CCODIGOP01_PRO = PROD.CCODIGOP01 " & _
        '    "INNER JOIN CATUNIDADES UNI ON DET.CIDUNIDA01 = UNI.CIDUNIDA01 " & _
        '    "INNER JOIN CATCONCEPTOS CONC_SAL ON CAB_SAL.CIDDOCUM02 = CONC_SAL.CCODIGOC01  " & _
        '    "INNER JOIN CATCONCEPTOS CONC_ENT ON CAB_ENT.CIDDOCUM02 = CONC_ENT.CCODIGOC01  " & _
        '    "INNER JOIN CATCONCEPTOS CONC_TRAS ON CAB.CIDDOCUM02 = CONC_TRAS.CCODIGOC01  " & _
        '    "INNER JOIN CATALMACENES ALM_SAL ON CAB.CIDALMACEN_SAL = ALM_SAL.CCODIGOA01  " & _
        '    "INNER JOIN CATALMACENES ALM_ENT ON CAB.CIDALMACEN_ENT  = ALM_ENT.CCODIGOA01  " & _
        '    "WHERE CAB.TIPOMOVIMIENTO = 'TRAS' AND CAB.NOMOVIMIENTO = " & Val(vValor1) & _
        '    "ORDER BY DET.CONSECUTIVO"

        '    vValor1 = ""
        'End If


        Return strSqlGlobal

        'If UCase(NomRep) = UCase("RepEdoCtaCli") Then
        '    strFiltroEsp1 = " where idContrato = '" & CmbCadena(0).Text & "'"
        '    strSqlGlobal = "declare @NumPagos as VARCHAR(10); declare @NumPago as VARCHAR(10) " & _
        '    "set @NumPagos = (select NumPagos from m_Contratos " & strFiltroEsp1 & ") " & _
        '    "select con.idContrato, CON.idCliente, " & _
        '    "CASE WHEN CLI.PMORAL = 1 THEN CLI.EMPRAZONSOCIAL  ELSE CLI.NOMBRES + ' ' + CLI.APEPATERNO + ' ' + CLI.APEMATERNO  END AS NOMCLIENTE, " & _
        '    "ISNULL(PROG.NOMBREPROG,'') AS PROGRAMA, " & _
        '    "ISNULL(CON.IDTIPOCREDITO,'') + ISNULL(TIPCRE.DESCRIPCION,'') AS TIPCRE,CON.FechaContrato, " & _
        '    "CON.LoteSuperficie, CON.LoteCalle, CON.LoteNoExt AS pREDIO, CON.LoteCruzamiento,con.ImporteCred, " & _
        '    "CON.SaldoActual, CON.TotalPagado,CON.PagoMensCAp, CON.ImporteEnganche, CON.Numpagos, " & _
        '    "abo.NumRecibo,ABO.FechaTrans,abo.NumAbono, CAR.NoPagare, CAR.NoPagareOrig, (CAR.NoPagareOrig + '/' + @NumPagos) as Mensualidad, " & _
        '    "ABO.ImporteInteres, ABO.ImporteCapital, ABO.ImporteGastos, ABO.ImporteSeguro, ABO.ImporteSubsidio,abo.EsMoratorio,abo.DiasMoratorio, " & _
        '    "CASE WHEN abo.EsMoratorio = 1 THEN ABO.ImportePago  ELSE 0  END AS ImporteMora, " & _
        '    "CASE WHEN abo.EsMoratorio = 1 THEN 0  ELSE ABO.ImportePago  END AS ImportePago " & _
        '    "from m_Contratos CON " & _
        '    "inner join c_Clientes CLI on CON.idCliente = CLI.idCliente " & _
        '    "inner join m_Abonos ABO on con.idContrato = ABO.idContrato " & _
        '    "inner join m_Cargos CAR on ABO.NumAbono = CAR.NoPagare and ABO.idContrato = CAR.idContrato  " & _
        '    "LEFT JOIN C_PROGRAMAS PROG ON CON.IDPROGRAMA = PROG.IDPROGRAMA " & _
        '    "LEFT JOIN C_TIPOCREDITO TIPCRE ON PROG.IDTIPOCREDITO = TIPCRE.IDTIPOCREDITO " & _
        '    "where con.idContrato = '" & CmbCadena(0).Text & "'" & _
        '    "order by CAR.NoPagareOrig, CAR.NoPagare " & OrdenaCampos

        '    strSqlGlobal = "CREATE TABLE ##PUNTOS (FECHA  SMALLDATETIME,FOLIOMOVIMIENTO  INT, IMPORTEVENTA  DECIMAL(18,2), " & _
        '    "CARGO DECIMAL(18,2),ABONO  DECIMAL(18,2),SALDO  DECIMAL(18,2),ESCANJE  BIT,IDTARJETA  VARCHAR(25), " & _
        '    "CRAZONSO01 VARCHAR(150),FECULTIMACOMPRA  SMALLDATETIME,FECULTIMOCANJE  SMALLDATETIME, " & _
        '    "FECULTREGISTRO  SMALLDATETIME, CCODIGOC01 VARCHAR(30), SALDOACT  DECIMAL(18,2)  ) " & _
        '    "insert into ##Puntos " & _
        '    "SELECT PUN.Fecha, PUN.FolioMovimiento, PUN.ImporteVenta, PUN.ImportePuntos AS cARGO, 0 AS aBONO, 0 as SAldo, PUN.EsCanje, " & _
        '    "PUN.IdTarjeta,TAR.CRAZONSO01, tar.FecUltimaCompra ,TAR.FecUltimoCanje,TAR.FecUltRegistro, tar.CCODIGOC01,pun.SaldoAct " & _
        '    "FROM PUNTOS PUN " & _
        '    "left join CatTarjetas TAR on PUN.IdTarjeta = TAR.IdTarjeta WHERE PUN.EsCanje = 0 and " & FiltroSinWhere & _
        '    "union " & _
        '    "SELECT PUN.Fecha, PUN.FolioMovimiento, PUN.ImporteVenta, 0 AS cARGO, PUN.ImportePuntos AS aBONO, 0 as SAldo, PUN.EsCanje, " & _
        '    "PUN.IdTarjeta,TAR.CRAZONSO01, tar.FecUltimaCompra ,TAR.FecUltimoCanje,TAR.FecUltRegistro, tar.CCODIGOC01,PUN.SaldoAct " & _
        '    "FROM PUNTOS PUN " & _
        '    "left join CatTarjetas TAR on PUN.IdTarjeta = TAR.IdTarjeta WHERE PUN.EsCanje = 1 and " & FiltroSinWhere & _
        '    "order by PUN.fecha asc " & _
        '    "DECLARE @Cargo AS decimal(18,4) set @Cargo = 0 " & _
        '    "DECLARE @Abono AS decimal(18,4) set @Abono = 0 " & _
        '    "DECLARE @Saldo AS decimal(18,4) set @Saldo = 0 " & _
        '    "DECLARE @SaldoTot AS decimal(18,4) set @SaldoTot = 0 " & _
        '    "DECLARE @SaldoAct AS decimal(18,4) set @SaldoAct = 0 " & _
        '    "declare @folio as int set @folio = 0 " & _
        '    "declare micursor cursor for select folioMovimiento, cargo,abono,saldo,saldoact from ##Puntos as PUN order by fecha " & _
        '    "open micursor " & _
        '    "FETCH NEXT FROM micursor " & _
        '    "into @folio,@Cargo,@Abono,@Saldo,@SaldoAct " & _
        '    "set @SaldoTot = @SaldoAct " & _
        '    " WHILE (@@FETCH_STATUS = 0) " & _
        '    " BEGIN " & _
        '    "	set @SaldoTot = @SaldoTot + (@Cargo- @Abono) " & _
        '    "	update ##Puntos set SAldo = @SaldoTot where folioMovimiento = @folio " & _
        '    "	FETCH NEXT FROM micursor " & _
        '    "	into @folio,@Cargo,@Abono,@Saldo,@SaldoAct " & _
        '    " END " & _
        '    " CLOSE micursor " & _
        '    " DEALLOCATE micursor " & _
        '    " select * from ##Puntos as PUN " & _
        '    " drop table ##Puntos"

        'ElseIf UCase(NomRep) = UCase("RepSalxCli") Then

        '    '"WHERE AND PUN.ESCANJE = 0 AND (PUN.FECHA >= '23/07/2013 00:00:00' AND PUN.FECHA < '31/07/2013 23:59:59')  " & _
        '    '"WHERE (PUN.FECHA >= '23/07/2013 00:00:00' AND PUN.FECHA < '31/07/2013 23:59:59') AND PUN.ESCANJE = 1 " & _

        '    strSqlGlobal = "CREATE TABLE ##PUNTOSSAL (IDTARJETA  VARCHAR(25), CRAZONSO01 VARCHAR(150),FECHA  SMALLDATETIME,SALDOINICIAL  DECIMAL(18,2), " & _
        '    "CARGO DECIMAL(18,2),ABONO  DECIMAL(18,2),SALDO  DECIMAL(18,2),SALDOACT  DECIMAL(18,2)) " & _
        '    "INSERT INTO ##PUNTOSSAL " & _
        '    "SELECT PUN.IDTARJETA, TAR.CRAZONSO01, PUN.FECHA, " & _
        '    "0 AS SALDOINICIAL, SUM(PUN.IMPORTEPUNTOS) AS CARGO, 0 AS ABONO, 0 AS SALDO,PUN.SALDOACT " & _
        '    "FROM PUNTOS PUN " & _
        '    "left JOIN CATTARJETAS TAR ON PUN.IDTARJETA = TAR.IDTARJETA " & _
        '    "WHERE PUN.ESCANJE = 0 AND  " & FiltroSinWhere & _
        '    " GROUP BY PUN.IDTARJETA, TAR.CRAZONSO01, PUN.FECHA, PUN.ESCANJE,PUN.SALDOACT " & _
        '    "UNION " & _
        '    "SELECT PUN.IDTARJETA, TAR.CRAZONSO01, PUN.FECHA, " & _
        '    "0 AS SALDOINICIAL, 0 AS CARGO, SUM(PUN.IMPORTEPUNTOS) AS ABONO, 0 AS SALDO,PUN.SALDOACT " & _
        '    "FROM PUNTOS PUN " & _
        '    "left JOIN CATTARJETAS TAR ON PUN.IDTARJETA = TAR.IDTARJETA " & _
        '    "WHERE PUN.ESCANJE = 1 AND " & FiltroSinWhere & _
        '    " GROUP BY PUN.IDTARJETA, TAR.CRAZONSO01, PUN.FECHA, PUN.ESCANJE,PUN.SALDOACT " & _
        '    "ORDER BY PUN.IDTARJETA, PUN.FECHA " & _
        '    "CREATE TABLE ##PUNTOSSALFIN (IDTARJETA  VARCHAR(25), CRAZONSO01 VARCHAR(150),SALDOINICIAL  DECIMAL(18,2), " & _
        '    "CARGO DECIMAL(18,2),ABONO  DECIMAL(18,2),SALDO  DECIMAL(18,2)) " & _
        '    "INSERT INTO ##PUNTOSSALFIN " & _
        '    "SELECT PUN.IDTARJETA, PUN.CRAZONSO01 , 0 AS SALDOINICIAL, " & _
        '    "SUM(PUN.CARGO) AS CARGO, SUM(PUN.ABONO) AS ABONO, " & _
        '    "SUM(PUN.CARGO) - SUM(PUN.ABONO) AS SALDO " & _
        '    "FROM ##PUNTOSSAL PUN " & _
        '    "GROUP BY PUN.IDTARJETA, PUN.CRAZONSO01 " & _
        '    "DECLARE @IDTARJETA AS VARCHAR(25) " & _
        '    "DECLARE @IDTARJETAANT AS VARCHAR(25) SET @IDTARJETAANT = 0 " & _
        '    "DECLARE @SALDOACT AS DECIMAL(18,4) " & _
        '    "DECLARE MICURSOR CURSOR FOR SELECT PUN.IDTARJETA, PUN.SALDOACT FROM ##PUNTOSSAL AS PUN ORDER BY PUN.IDTARJETA, PUN.FECHA " & _
        '    "OPEN MICURSOR " & _
        '    "FETCH NEXT FROM MICURSOR " & _
        '    "INTO @IDTARJETA,@SALDOACT " & _
        '    " WHILE (@@FETCH_STATUS = 0) " & _
        '    " BEGIN " & _
        '    "	IF 	@IDTARJETAANT <> @IDTARJETA " & _
        '    "	BEGIN " & _
        '    "	UPDATE ##PUNTOSSALFIN SET SALDOINICIAL = @SALDOACT WHERE IDTARJETA = @IDTARJETA " & _
        '    "	END " & _
        '    "	SET @IDTARJETAANT = @IDTARJETA " & _
        '    "	FETCH NEXT FROM MICURSOR " & _
        '    "	INTO @IDTARJETA,@SALDOACT " & _
        '    " END " & _
        '    " CLOSE MICURSOR  " & _
        '    " DEALLOCATE MICURSOR " & _
        '    "SELECT PUN.IDTARJETA,PUN.CRAZONSO01,PUN.SALDOINICIAL, PUN.CARGO ,PUN.ABONO , (PUN.SALDOINICIAL+ PUN.SALDO ) AS SALDO FROM ##PUNTOSSALFIN PUN " & _
        '    "DROP TABLE ##PUNTOSSAL " & _
        '    "DROP TABLE ##PUNTOSSALFIN "

        'ElseIf UCase(NomRep) = UCase("RepRecordVen") Then
        '    Dim BandHora As Boolean = TryCast(GetControl(0, 0), DateTimePicker).Checked
        '    If BandHora Then
        '        Dim HoraIni As Date = TryCast(GetControl(0, 0), DateTimePicker).Value.ToShortTimeString
        '        Dim HoraFin As Date = TryCast(GetControl(1, 0), DateTimePicker).Value.ToShortTimeString

        '        FiltroSinWhere = FiltroSinWhere & "DATEPART (hour,Pun.Fecha ) >= " & DatePart(DateInterval.Hour, HoraIni) & " and DATEPART (hour,Pun.Fecha ) <= " & DatePart(DateInterval.Hour, HoraFin)

        '        'If FiltroSinWhere = "" Then
        '        '    FiltroSinWhere = "DATEPART (hour,Pun.Fecha ) >= " & DatePart(DateInterval.Hour, HoraIni) & " and DATEPART (hour,Pun.Fecha ) <= " & DatePart(DateInterval.Hour, HoraFin)
        '        'Else

        '        'End If
        '    End If

        '    strSqlGlobal = "SELECT PUN.IDTARJETA, TAR.CRAZONSO01 , PUN.FOLIOVENTA ,isnull(pun.CRAZONSO01,'') as cliente, " & _
        '    "PUN.IMPORTEVENTA, PUN.IMPORTEPUNTOS, PUN.FECHA as FECHA, PUN.FECHAVENTA, isnull(PUN.NomVendedor,'') as NomVendedor  " & _
        '    "FROM PUNTOS PUN " & _
        '    "left JOIN CATTARJETAS TAR ON PUN.IDTARJETA = TAR.IDTARJETA " & _
        '    "WHERE PUN.ESCANJE = 0 " & IIf(FiltroSinWhere <> "", " AND " & FiltroSinWhere, "") & IIf(OrdenaCampos = "", "", " ORDER BY " & OrdenaCampos)

        '    '" ORDER BY PUN.FECHA "
        'ElseIf UCase(NomRep) = UCase("RepOpexDia") Then
        '    strSqlGlobal = "select PUN.IdTarjeta,TAR.CRAZONSO01, PUN.Fecha, PUN.FolioMovimiento, " & _
        '    "case when PUN.EsCanje = 0 then 'REGISTRO' else 'CANJE' end as Concepto, " & _
        '    "PUN.ImporteVenta,PUN.ImportePuntos " & _
        '    "from puntos PUN " & _
        '    "left join CatTarjetas TAR on PUN.IdTarjeta = TAR.IdTarjeta " & _
        '    "WHERE " & FiltroSinWhere & _
        '    "order by PUN.fecha "
        'ElseIf UCase(NomRep) = UCase("RepOpexcON") Then
        '    'strSqlGlobal = "select case when PUN.EsCanje = 0 then 'REGISTRO' else 'CANJE' end as Concepto, " & _
        '    '"PUN.IdTarjeta, TAR.CRAZONSO01, PUN.ImporteVenta, PUN.ImportePuntos, PUN.Fecha " & _
        '    '"from puntos PUN " & _
        '    '"inner join CatTarjetas TAR on PUN.IdTarjeta = TAR.IdTarjeta " & _
        '    '"WHERE " & FiltroSinWhere & _
        '    '"order by Concepto,PUN.Fecha "

        '    strSqlGlobal = "CREATE TABLE ##PUNTOS (CONCEPTO VARCHAR(20),IDTARJETA VARCHAR(25), CRAZONSO01 VARCHAR(150), IMPORTEVENTA DECIMAL(18,4), " & _
        '    "IMPORTEPUNTOS DECIMAL(18,4), FECHA DATETIME) " & _
        '    "INSERT INTO ##PUNTOS " & _
        '    "SELECT CASE WHEN PUN.ESCANJE = 0 THEN 'REGISTRO' ELSE 'CANJE' END AS CONCEPTO, " & _
        '    "PUN.IDTARJETA, TAR.CRAZONSO01, PUN.IMPORTEVENTA, PUN.IMPORTEPUNTOS, PUN.FECHA " & _
        '    "FROM PUNTOS PUN left JOIN CATTARJETAS TAR ON PUN.IDTARJETA = TAR.IDTARJETA " & _
        '    "SELECT PUN.CONCEPTO as GRUPO, PUN.CONCEPTO,PUN.IDTARJETA, PUN.CRAZONSO01,PUN.IMPORTEVENTA , PUN.IMPORTEPUNTOS " & _
        '    "FROM ##PUNTOS PUN " & _
        '    "WHERE " & FiltroSinWhere & _
        '    "ORDER BY PUN.CONCEPTO,PUN.FECHA " & _
        '    "DROP TABLE ##PUNTOS"

        '    '"WHERE  PUN.FECHA >= '01/07/2013 00:00:00' AND PUN.FECHA <= '31/07/2013 23:59:59' " & _
        '    '"AND CONCEPTO IN('CANJE') " & _


        'End If




    End Function


    Private Function LlenaNumRegistros() As Integer
        LlenaNumRegistros = 0

        Dim strSqlComp As String = ""
        Dim NumReg As Integer
        Try


            'If DsRep.DefaultView 
            If DsRep.Tables.Count > 0 Then
                If DsRep.Tables(vNomTabla).DefaultView.Count > 0 Then
                    NumReg = DsRep.Tables(vNomTabla).DefaultView.Count
                    ImpRep.AsignaDataSet(DsRep)
                    'Else
                    '    strOrdenaCampos = DeterminaOrden()
                    '    If ImpRep.BandRepEsp Then
                    '        strFiltroSinWhere = DeterminaFiltros()
                    '        ImpRep.StrSql = CreaSQLConsulta(ImpRep.NomReporte, strFiltroSinWhere, strOrdenaCampos)
                    '    Else
                    '        strFiltroSinWhere = DeterminaFiltros()
                    '        strSqlComp = IIf(Trim(strFiltroSinWhere) <> "", " where " & strFiltroSinWhere, "") & IIf(Trim(strOrdenaCampos) <> "", " order by " & strOrdenaCampos, "")
                    '        ImpRep.StrSql = strSqlGlobal & strSqlComp
                    '    End If
                    '    NumReg = ImpRep.LlenaDataSet
                End If
            Else
                strOrdenaCampos = DeterminaOrden()
                If ImpRep.BandRepEsp Then
                    'strFiltroSinWhere = DeterminaFiltros()
                    ImpRep.StrSql = CreaSQLConsulta(ImpRep.NomReporte, strFiltroSinWhere, strOrdenaCampos)
                Else
                    strFiltroSinWhere = DeterminaFiltros()
                    strSqlComp = IIf(Trim(strFiltroSinWhere) <> "", " where " & strFiltroSinWhere, "") & IIf(Trim(strOrdenaCampos) <> "", " order by " & strOrdenaCampos, "")
                    ImpRep.StrSql = strSqlGlobal & strSqlComp
                End If
                NumReg = ImpRep.LlenaDataSet()
            End If



            Return NumReg
        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, Me.Text)
        End Try

    End Function

    Private Sub PreImprime(ByVal PantoImpresora As Boolean)

        'If vNomTabla = "REPPUNTOS" Then
        '    If CaleFecha1(0).Checked Then
        '        Dim RpPnts As New RepPuntos("RELACION DE PUNTOS", "(SELECT P.idContrato Contrato,(Cli.ApePaterno + ' ' + Cli.ApeMaterno + ' ' + Cli.Nombres) NOMBRES, " &
        '                "(SELECT MAX(CAST(pg.FechaPunto AS DATE)) FROM Puntos pg WHERE pg.Puntos > 0 AND pg.idContrato = p.idContrato AND CAST(pg.FechaPunto as DATE) >= '" & FormatFecHora(CaleFecha1(0).Value, False) & "' AND CAST(pg.FechaPunto as DATE) <= '" & FormatFecHora(CaleFecha2(0).Value, False) & "') 'U. FEC. PUN. GEN.', " &
        '                "(SELECT MAX(CAST(pg.FechaPunto AS DATE)) FROM Puntos pg WHERE pg.Puntos < 0 AND pg.idContrato = p.idContrato AND CAST(pg.FechaPunto as DATE) >= '" & FormatFecHora(CaleFecha1(0).Value, False) & "' AND CAST(pg.FechaPunto as DATE) <= '" & FormatFecHora(CaleFecha2(0).Value, False) & "') 'U. FEC. PUN. CANJEA.', " &
        '                "(SELECT ISNULL(SUM(pg.Puntos),0) FROM Puntos pg WHERE pg.Puntos > 0 AND pg.idContrato = p.idContrato) 'PUNTOS ACUMULADOS', " &
        '                "(SELECT ISNULL(SUM(pg.Puntos),0) FROM Puntos pg WHERE pg.Puntos > 0 AND pg.idContrato = p.idContrato AND CAST(pg.FechaPunto as DATE) >= '" & FormatFecHora(CaleFecha1(0).Value, False) & "' AND CAST(pg.FechaPunto as DATE) <= '" & FormatFecHora(CaleFecha2(0).Value, False) & "') 'PUNTOS GENERADOS', " &
        '                "(SELECT ABS(ISNULL(SUM(pg.Puntos),0)) FROM Puntos pg WHERE pg.Puntos < 0 AND pg.idContrato = p.idContrato AND CAST(pg.FechaPunto as DATE) >= '" & FormatFecHora(CaleFecha1(0).Value, False) & "' AND CAST(pg.FechaPunto as DATE) <= '" & FormatFecHora(CaleFecha2(0).Value, False) & "') 'PUNTOS CANJEADOS', " &
        '                "(SELECT ISNULL(SUM(pg.Puntos),0) FROM Puntos pg WHERE  pg.idContrato = p.idContrato) 'SALDO EN PUNTOS' " &
        '                "FROM puntos p LEFT JOIN c_Clientes Cli ON P.idCliente = Cli.idCliente " &
        '                "WHERE CAST(P.FechaPunto as DATE) >= '" & FormatFecHora(CaleFecha1(0).Value, False) & "' AND CAST(P.FechaPunto as DATE) <= '" & FormatFecHora(CaleFecha2(0).Value, False) & "' " &
        '                "Group By p.idContrato,Cli.ApePaterno,Cli.ApeMaterno,Cli.Nombres) UNION ALL(SELECT '','TOTAL de Clientes: %c',NULL,NULL,-999,-999,-999,-999)", "Relacion del: " & CaleFecha1(0).Value.ToString("dd/MMMM/yyyy") & " al " & CaleFecha2(0).Value.ToString("dd/MMMM/yyyy"), "", Now,
        '                {100, 250, 100, 100, 100, 100, 100, 100},
        '                  {StringAlignment.Near, StringAlignment.Near, StringAlignment.Center, StringAlignment.Center, StringAlignment.Center,
        '                   StringAlignment.Center, StringAlignment.Center, StringAlignment.Center})
        '        RpPnts.Imprimir(PantoImpresora)

        '    Else
        '        MessageBox.Show("No se ha habilitado un intervalo de Fecha Valido!!!", "Error en el intervalo de fecha!!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    End If
        'Else


        '    NumRegistros = LlenaNumRegistros()

        '    If NumRegistros > 0 Then
        '        ' Vista preliminar
        '        NumPaginas = CalculaNumPaginas()
        '        Imprimir(PantoImpresora)
        '    Else
        '        MsgBox("No se encuentran Datos para este Reporte", MsgBoxStyle.Information, Me.Text)
        '    End If
        'End If
        NumRegistros = 0

        NumRegistros = LlenaNumRegistros()

        If NumRegistros > 0 Then
            If strSqlGlobalDet <> "" Then
                'Dim NumRegDET As Integer
                ImpRep.StrSql = strSqlGlobalDet
                NumRegistros_DET = ImpRep.LlenaDataSetDET()
                If NumRegistros_DET > 0 Then

                End If
            Else
                NumRegistros_DET = 0
                If ImpRep.DsReporte_DET Is Nothing Then ImpRep.DsReporte_DET = New DataSet
                ImpRep.DsReporte_DET.Clear()
            End If
            ' Vista preliminar
            NumPaginas = CalculaNumPaginas()
            Imprimir(PantoImpresora)
        Else
            MsgBox("No se encuentran Datos para este Reporte", MsgBoxStyle.Information, Me.Text)
        End If

    End Sub

    Private Sub cmdOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOk.Click
        PreImprime(True)
    End Sub

    Private Sub AcrobatpdfToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AcrobatpdfToolStripMenuItem.Click
        Try
            DsRep = ImpRep.LlenaDsImprime(ImpRep.NomTabla)
            If DsRep.Tables(ImpRep.NomTabla).DefaultView.Count > 0 Then
                GridDatos.DataSource = DsRep.Tables(ImpRep.NomTabla)
                'NumPaginas = CalculaNumPaginas()
                'Imprimir(True)

                'Intentar generar el documento.
                Dim doc As New Document(PageSize.A4.Rotate(), 10, 10, 10, 10)
                'Path que guarda el reporte en el escritorio de windows (Desktop).
                Dim filename As String = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + "\" & ImpRep.NomReporte & ".pdf"
                Dim file As New FileStream(filename, FileMode.Create, FileAccess.Write, FileShare.ReadWrite)
                PdfWriter.GetInstance(doc, file)
                doc.Open()
                ExportarDatosPDF(doc)
                doc.Close()
                Process.Start(filename)

            Else
                MsgBox("No se encuentran Datos para este Reporte", MsgBoxStyle.Information, Me.Text)
            End If

        Catch ex As Exception
            'Si el intento es fallido, mostrar MsgBox.
            MessageBox.Show("No se puede generar el documento PDF.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Public Sub ExportarDatosPDF(ByVal document As Document)
        Try

            'Se crea un objeto PDFTable con el numero de columnas del DataGridView.  
            Dim datatable As New PdfPTable(GridDatos.ColumnCount)
            'Se asignan algunas propiedades para el dise�o del PDF.
            datatable.DefaultCell.Padding = 3
            Dim headerwidths As Single() = GetColumnasSize(GridDatos)
            datatable.SetWidths(headerwidths)
            datatable.WidthPercentage = 100
            datatable.DefaultCell.BorderWidth = 2
            datatable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER
            'Se crea el encabezado en el PDF.  
            Dim encabezado As New Paragraph(ImpRep.Encabezado1, New Font(Font.Name = "Tahoma", 20, Font.Bold))
            Dim encabezado2 As New Paragraph(ImpRep.Encabezado2, New Font(Font.Name = "Tahoma", 20, Font.Bold))
            'Se crea el texto abajo del encabezado.
            Dim texto As New Phrase(ImpRep.TituloReporte, New Font(Font.Name = "Tahoma", 25, Font.Bold))
            'Dim texto As New Phrase("Reporte de Corrida Financiera: " + Now.Date(), New Font(Font.Name = "Tahoma", 14, Font.Bold))

            'Se capturan los nombres de las columnas del DataGridView.
            For i As Integer = 0 To GridDatos.ColumnCount - 1
                datatable.AddCell(GridDatos.Columns(i).HeaderText)
            Next
            datatable.HeaderRows = 1
            datatable.DefaultCell.BorderWidth = 1
            'Se generan las columnas del DataGridView.  
            'GridCorridaFin.RowCount - 2 se quita 2 en vez de 1 por el que esta vacio
            For i As Integer = 0 To GridDatos.RowCount - 2
                For j As Integer = 0 To GridDatos.ColumnCount - 1
                    datatable.AddCell(GridDatos(j, i).Value.ToString())
                Next
                datatable.CompleteRow()
            Next
            'Se agrega el PDFTable al documento. 
            document.Add(encabezado)
            document.Add(encabezado2)
            document.Add(texto)
            document.Add(datatable)
        Catch ex As Exception
            MsgBox(ex)
        End Try

    End Sub
    'Funcion que obtiene el tama�o de las columnas del DataGridView.
    'La funcion se usara en el metodo para crear el reporte en PDF.
    Public Function GetColumnasSize(ByVal dg As DataGridView) As Single()
        Dim values As Single() = New Single(dg.ColumnCount - 1) {}
        For i As Integer = 0 To dg.ColumnCount - 1
            values(i) = CSng(dg.Columns(i).Width)
        Next
        Return values
    End Function
    'Funcion que obtiene el tama�o de las columnas del DataGridView.
    'La funcion se usara en el metodo para crear el reporte en PDF.
    'Public Function GetColumnasSize(ByVal dg As CamposReporte) As Single()
    '    'leftMargin = leftMargin + (.AnchoCampo * Factor)
    '    Dim values As Single() = New Single(dg.Length - 1) {}
    '    For i As Integer = 0 To dg.Length - 1
    '        With dg(i)
    '            values(i) = CSng(.)
    '        End With

    '    Next
    '    Return values
    'End Function

    'Metodo para crear el reporte en PDF.
    'Public Sub ExportarDatosPDF(ByVal document As Document)
    '    Try

    '        'Se crea un objeto PDFTable con el numero de columnas del DataGridView.  

    '        'Dim datatable As New PdfPTable(GridCorridaFin.ColumnCount)
    '        Dim datatable As New PdfPTable(ImpRep.vNumColsRep)
    '        'Se asignan algunas propiedades para el dise�o del PDF.
    '        datatable.DefaultCell.Padding = 3
    '        'Dim headerwidths As Single() = GetColumnasSize(GridCorridaFin) ojo
    '        'datatable.SetWidths(headerwidths) ojo
    '        datatable.WidthPercentage = 100
    '        datatable.DefaultCell.BorderWidth = 2
    '        datatable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER
    '        'Se crea el encabezado en el PDF.  
    '        Dim encabezado1 As New Paragraph(ImpRep.Encabezado1, New Font(Font.Name = "Tahoma", 20, Font.Bold))
    '        Dim encabezado2 As New Paragraph(ImpRep.Encabezado2, New Font(Font.Name = "Tahoma", 20, Font.Bold))
    '        'Se crea el texto abajo del encabezado.
    '        'Dim texto As New Phrase("Reporte de Corrida Financiera: " + Now.Date(), New Font(Font.Name = "Tahoma", 14, Font.Bold))
    '        Dim texto As New Phrase(ImpRep.TituloReporte, New Font(Font.Name = "Tahoma", 25, Font.Bold))
    '        'Se capturan los nombres de las columnas del DataGridView.
    '        For i As Integer = 0 To _ArrColClass.Length - 1
    '            With _ArrColClass(i)

    '                datatable.AddCell(.DesripcionCampo)
    '                'E.Graphics.DrawString(.DesripcionCampo, vFuenteEncabCampos, Brushes.Black, leftMargin, yPos)
    '                'leftMargin = leftMargin + (.AnchoCampo * Factor)
    '            End With
    '        Next
    '        datatable.HeaderRows = 1
    '        datatable.DefaultCell.BorderWidth = 1

    '        For i = 0 To ImpRep.DsReporte.Tables(vNomTabla).DefaultView.Count - 1
    '            With ImpRep.DsReporte.Tables(vNomTabla).DefaultView
    '                For j As Integer = 0 To _ArrColClass.Length - 1
    '                    datatable.AddCell(.Item(i).Item(j).ToString)
    '                Next

    '            End With


    '        Next
    '        'Se generan las columnas del DataGridView.  
    '        'GridCorridaFin.RowCount - 2 se quita 2 en vez de 1 por el que esta vacio
    '        'For i As Integer = 0 To GridCorridaFin.RowCount - 2
    '        '    For j As Integer = 0 To GridCorridaFin.ColumnCount - 1
    '        '        datatable.AddCell(GridCorridaFin(j, i).Value.ToString())
    '        '    Next
    '        '    datatable.CompleteRow()
    '        'Next
    '        'Se agrega el PDFTable al documento. 
    '        document.Add(encabezado1)
    '        document.Add(encabezado2)
    '        document.Add(texto)
    '        document.Add(datatable)
    '    Catch ex As Exception
    '        MsgBox(ex)
    '    End Try

    'End Sub

    Private Sub cmdBorrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBorrar.Click
        PreImprime(False)
    End Sub

    'Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    MessageBox.Show(GetContenidoControl(Val(TextBox1.Text), Val(TextBox2.Text)))
    'End Sub

#Region "ExportaExcel"
    Private Sub ExcelxlsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExcelxlsToolStripMenuItem.Click
        Try
            'Intentar generar el documento.
            'Se adjunta un texto debajo del encabezado con la fecha actual del sistema.
            NumRegistros = LlenaNumRegistros()
            If NumRegistros > 0 Then
                DsRep = ImpRep.LlenaDsImprime(ImpRep.NomTabla)
                If DsRep.Tables(ImpRep.NomTabla).DefaultView.Count > 0 Then
                    GridDatos.DataSource = DsRep.Tables(ImpRep.NomTabla)
                    'GridDatos.DataSource = DsRep
                    ExportarDatosExcel(GridDatos, ImpRep.TituloReporte & " al: " + Now.Date())
                Else
                    MsgBox("No se encuentran Datos para este Reporte", MsgBoxStyle.Information, Me.Text)
                End If
            End If
        Catch ex As Exception
            'Si el intento es fallido, mostrar MsgBox.
            MessageBox.Show("No se puede generar el documento Excel.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    'Metodo para crear el reporte en Microsoft Excel.
    'Public Sub ExportarDatosExcel(ByVal DataGridView1 As DataGridView, ByVal titulo As String)
    '    Dim m_Excel As New Excel.Application
    '    m_Excel.Cursor = Excel.XlMousePointer.xlWait
    '    m_Excel.Visible = True
    '    Dim objLibroExcel As Excel.Workbook = m_Excel.Workbooks.Add
    '    Dim objHojaExcel As Excel.Worksheet = objLibroExcel.Worksheets(1)
    '    With objHojaExcel
    '        .Visible = Excel.XlSheetVisibility.xlSheetVisible
    '        .Activate()
    '        'Encabezado.   
    '        .Range("A1:L1").Merge()
    '        .Range("A1:L1").Value = "INSTITUTO DE VIVIENDA DEL ESTADO DE YUCATAN"
    '        .Range("A1:L1").Font.Bold = True
    '        .Range("A1:L1").Font.Size = 16
    '        'Texto despues del encabezado.   
    '        .Range("A2:L2").Merge()
    '        .Range("A2:L2").Value = titulo
    '        .Range("A2:L2").Font.Bold = True
    '        .Range("A2:L2").Font.Size = 10
    '        'Espacio.
    '        .Range("A3:L3").Merge()
    '        .Range("A3:L3").Value = ""
    '        .Range("A3:L3").Font.Bold = True
    '        .Range("A3:L3").Font.Size = 10
    '        'Estilo a titulos de la tabla.
    '        .Range("A4:P4").Font.Bold = True
    '        'Establecer tipo de letra al rango determinado.
    '        .Range("A1:P100").Font.Name = "Tahoma"
    '        'Los datos se registran a partir de la columna A, fila 4.
    '        Const primeraLetra As Char = "A"
    '        Const primerNumero As Short = 4
    '        Dim Letra As Char, UltimaLetra As Char
    '        Dim Numero As Integer, UltimoNumero As Integer
    '        Dim cod_letra As Byte = Asc(primeraLetra) - 1
    '        Dim sepDec As String = Application.CurrentCulture.NumberFormat.NumberDecimalSeparator
    '        Dim sepMil As String = Application.CurrentCulture.NumberFormat.NumberGroupSeparator
    '        Dim strColumna As String = ""
    '        Dim LetraIzq As String = ""
    '        Dim cod_LetraIzq As Byte = Asc(primeraLetra) - 1
    '        Letra = primeraLetra
    '        Numero = primerNumero
    '        Dim objCelda As Excel.Range
    '        For Each c As DataGridViewColumn In DataGridView1.Columns
    '            If c.Visible Then
    '                : If Letra = "Z" Then
    '                    Letra = primeraLetra
    '                    cod_letra = Asc(primeraLetra)
    '                    cod_LetraIzq += 1
    '                    LetraIzq = Chr(cod_LetraIzq)
    '                Else
    '                    cod_letra += 1
    '                    Letra = Chr(cod_letra)
    '                End If
    '                strColumna = LetraIzq + Letra + Numero.ToString
    '                objCelda = .Range(strColumna, Type.Missing)
    '                objCelda.Value = c.HeaderText
    '                objCelda.EntireColumn.Font.Size = 10
    '                'Establece un formato a los numeros por Default.
    '                'objCelda.EntireColumn.NumberFormat = c.DefaultCellStyle.Format
    '                If c.ValueType Is GetType(Decimal) OrElse c.ValueType Is GetType(Double) Then
    '                    objCelda.EntireColumn.NumberFormat = "#" + sepMil + "0" + sepDec + "00"
    '                End If
    '            End If
    '        Next
    '        Dim objRangoEncab As Excel.Range = .Range(primeraLetra + Numero.ToString, LetraIzq + Letra + Numero.ToString)
    '        objRangoEncab.BorderAround(1, Excel.XlBorderWeight.xlMedium)
    '        UltimaLetra = Letra
    '        Dim UltimaLetraIzq As String = LetraIzq
    '        'Cargar Datos del DataGridView.   
    '        Dim i As Integer = Numero + 1
    '        For Each reg As DataGridViewRow In DataGridView1.Rows
    '            LetraIzq = ""
    '            cod_LetraIzq = Asc(primeraLetra) - 1
    '            Letra = primeraLetra
    '            cod_letra = Asc(primeraLetra) - 1
    '            For Each c As DataGridViewColumn In DataGridView1.Columns
    '                If c.Visible Then
    '                    If Letra = "Z" Then
    '                        Letra = primeraLetra
    '                        cod_letra = Asc(primeraLetra)
    '                        cod_LetraIzq += 1
    '                        LetraIzq = Chr(cod_LetraIzq)
    '                    Else
    '                        cod_letra += 1
    '                        Letra = Chr(cod_letra)
    '                    End If
    '                    strColumna = LetraIzq + Letra
    '                    'Aqui se realiza la carga de datos.  
    '                    .Cells(i, strColumna) = IIf(IsDBNull(reg.ToString), "", reg.Cells(c.Index).Value)
    '                    'Establece las propiedades de los datos del DataGridView por Default.
    '                    '.Cells(i, strColumna) = IIf(IsDBNull(reg.(c.DataPropertyName)), c.DefaultCellStyle.NullValue, reg(c.DataPropertyName))   
    '                    '.Range(strColumna + i, strColumna + i).In()   
    '                End If
    '            Next
    '            Dim objRangoReg As Excel.Range = .Range(primeraLetra + i.ToString, strColumna + i.ToString)
    '            objRangoReg.Rows.BorderAround()
    '            objRangoReg.Select()
    '            i += 1
    '        Next
    '        UltimoNumero = i
    '        'Dibujar las l�neas de las columnas.  
    '        LetraIzq = ""
    '        cod_LetraIzq = Asc("A")
    '        cod_letra = Asc(primeraLetra)
    '        Letra = primeraLetra
    '        For Each c As DataGridViewColumn In DataGridView1.Columns
    '            If c.Visible Then
    '                objCelda = .Range(LetraIzq + Letra + primerNumero.ToString, LetraIzq + Letra + (UltimoNumero - 1).ToString)
    '                objCelda.BorderAround()
    '                If Letra = "Z" Then
    '                    Letra = primeraLetra
    '                    cod_letra = Asc(primeraLetra)
    '                    LetraIzq = Chr(cod_LetraIzq)
    '                    cod_LetraIzq += 1
    '                Else
    '                    cod_letra += 1
    '                    Letra = Chr(cod_letra)
    '                End If
    '            End If
    '        Next
    '        'Dibujar el border exterior grueso de la tabla.   
    '        Dim objRango As Excel.Range = .Range(primeraLetra + primerNumero.ToString, UltimaLetraIzq + UltimaLetra + (UltimoNumero - 1).ToString)
    '        objRango.Select()
    '        objRango.Columns.AutoFit()
    '        objRango.Columns.BorderAround(1, Excel.XlBorderWeight.xlMedium)
    '    End With
    '    m_Excel.Cursor = Excel.XlMousePointer.xlDefault
    'End Sub
    Public Sub ExportarDatosExcel(ByVal vGrid As DataGridView, ByVal titulo As String)
        Dim m_Excel As Excel.Application
        Dim objLibroExcel As Excel.Workbook
        Dim objHojaExcel As Excel.Worksheet
        m_Excel = CreateObject("Excel.Application")
        objLibroExcel = m_Excel.Workbooks.Add()
        objHojaExcel = objLibroExcel.Worksheets(1)
        objHojaExcel.Name = "ExportExcel"
        objHojaExcel.Visible = Excel.XlSheetVisibility.xlSheetVisible
        objHojaExcel.Activate()

        '/////////////////////////////////////////////////////////
        '// Definimos dos variables para controlar fila y columna
        '/////////////////////////////////////////////////////////
        Dim fila As Integer = 1
        Dim columna As Integer = 1
        Try
            '/////////////////////////////////////////////////
            '// Armamos la linea con los t�tulos de columnas
            '/////////////////////////////////////////////////
            With objHojaExcel.PageSetup
                .Orientation = Excel.XlPageOrientation.xlLandscape
                .LeftMargin = m_Excel.CentimetersToPoints(0.7)
                .RightMargin = m_Excel.CentimetersToPoints(0.4)
                .TopMargin = m_Excel.CentimetersToPoints(3)
                .BottomMargin = m_Excel.CentimetersToPoints(1)
                '.HeaderMargin = m_Excel.CentimetersToPoints(0) 'Encabezado
                '.FooterMargin = m_Excel.CentimetersToPoints(0.6) 'Pie de pagina
                .PaperSize = 1 '5 para Oficio 1 para carta
                .Zoom = 100
                'Pie de pagina
                'Encabezado Izquierdo
                'My.Resources.LogGobEdo.Save(My.Application.Info.DirectoryPath & "\LogGobEdo.jpg")
                .LeftHeader = "&G"
                '.LeftHeaderPicture.Filename = My.Application.Info.DirectoryPath & "\LogGobEdo.jpg"
                'Encabezado Derecho
                'My.Resources.ivey.Save(My.Application.Info.DirectoryPath & "\LogIvey.jpg")
                .RightHeader = "&G"
                '.RightHeaderPicture.Filename = My.Application.Info.DirectoryPath & "\LogIvey.jpg"
                'Encabezado Central
                .CenterHeader = titulo


                .CenterFooter = "P�gina &P de &N"

            End With


            objHojaExcel.Range("A1").Select()
            For Ci As Integer = 0 To vGrid.Columns.Count - 1
                objHojaExcel.Range(LetraColumna(columna) & 1).Value = vGrid.Columns(Ci).HeaderText
                columna += 1
            Next
            fila += 1
            '/////////////////////////////////////////////
            '// Le damos formato a la fila de los t�tulos
            '/////////////////////////////////////////////
            Dim objRango As Excel.Range = objHojaExcel.Range("A1:" & LetraColumna(vGrid.Columns.Count) & "1")
            objRango.Font.Bold = True
            objRango.Cells.Interior.ColorIndex = 35

            objRango.Cells.Borders(Excel.XlBordersIndex.xlDiagonalDown).LineStyle = Excel.XlLineStyle.xlLineStyleNone
            objRango.Cells.Borders(Excel.XlBordersIndex.xlDiagonalUp).LineStyle = Excel.XlLineStyle.xlLineStyleNone
            objRango.Cells.Borders(Excel.XlBordersIndex.xlEdgeLeft).LineStyle = Excel.XlLineStyle.xlLineStyleNone
            objRango.Cells.Borders(Excel.XlBordersIndex.xlEdgeRight).LineStyle = Excel.XlLineStyle.xlContinuous
            objRango.Cells.Borders(Excel.XlBordersIndex.xlEdgeTop).LineStyle = Excel.XlLineStyle.xlContinuous
            objRango.Cells.Borders(Excel.XlBordersIndex.xlEdgeBottom).LineStyle = Excel.XlLineStyle.xlContinuous
            '//////////////////////////////////////////
            '// Cargamos todas las filas del datatable
            '//////////////////////////////////////////
            'Ftn.Status("Inicio la exportacion a EXCEL . .", vForm, 0, vGrid.RowCount)
            columna = 1
            For j As Integer = 0 To vGrid.Rows.Count - 1
                columna = 1
                For k As Integer = 0 To vGrid.Columns.Count - 1
                    If IsDate(vGrid.Item(k, j).Value & "") Then
                        objHojaExcel.Range(LetraColumna(columna) & fila).Value = CDate(vGrid.Item(k, j).Value).ToString("yyyy/MM/dd")
                    Else
                        objHojaExcel.Range(LetraColumna(columna) & fila).Value = vGrid.Item(k, j).Value & ""
                    End If

                    columna += 1
                Next
                fila += 1
                ' Status("exportacion a EXCEL Fila: " & fila, vForm, fila)
            Next
            '//////////////////////////////////////
            '// Ajustamos automaticamente el ancho
            '// de todas las columnas utilizada
            '//////////////////////////////////////
            objRango = objHojaExcel.Range("A1:" & LetraColumna(vGrid.Columns.Count) & vGrid.Rows.Count.ToString)
            objRango.Select()
            objRango.Columns.AutoFit()

            '/////////////////////////////////////
            '// Le decimos a Excel que se muestre
            '/////////////////////////////////////
            m_Excel.Visible = True

            ' Status("Exportacion a EXCEL Finalizo Con EXITO!!! ", vForm)
        Catch ex As Exception
            m_Excel.Application.Quit()
            m_Excel = Nothing
            objLibroExcel = Nothing
            objHojaExcel = Nothing
            ' Ftn.Status("Error: " & ex.Message, vForm, Err:=True)
            MsgBox("Error al Exportar Excel: " & ex.Message, MsgBoxStyle.Critical, "Error: " & Err.Number)
        End Try
    End Sub
    Private Function LetraColumna(ByVal vNumeroCol As Integer) As String
        'Creado Juan alias erkHca CHOSS
        If vNumeroCol <= 0 Then Throw New Exception("El numero de columna tiene que ser ma�or a CERO!!!!")
        Dim LetraAntes As String = "", bndMod As Boolean = False
        If vNumeroCol / 26 > 1 Then
            If vNumeroCol Mod 26 = 0 Then
                LetraAntes = LetraColumna(Math.Truncate((vNumeroCol - 1) / 26))
            Else
                LetraAntes = LetraColumna(Math.Truncate((vNumeroCol) / 26))
            End If
            vNumeroCol = vNumeroCol - (Math.Truncate(vNumeroCol / 26)) * 26
        End If
        If vNumeroCol = 0 Then vNumeroCol = 26
        Return LetraAntes & Chr(64 + vNumeroCol)
    End Function

    Private Sub btnImportar_Click(sender As Object, e As EventArgs) Handles btnImportar.Click
        ExcelxlsToolStripMenuItem_Click(sender, e)
    End Sub
#End Region
    Private Sub btnImportar_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnImportar.MouseDown
        If e.Button = Windows.Forms.MouseButtons.Left Then
            MenuImporta.Show(sender, e.X, e.Y)
        End If
    End Sub

    Private Sub WebhtmlToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles WebhtmlToolStripMenuItem.Click

    End Sub

    Private Sub cmdReporte_Click(sender As Object, e As EventArgs) Handles cmdReporte.Click
        'Dim PrintDialog1 As PrintDialog
        'prtSettings = New PrinterSettings
        'PrintDialog1 = New PrintDialog

        'prtDoc = New PrintDocument
        'If vNomTabla = UCase("PEDIDOSPEN") Then
        '    If Registro_ColaPedidoLocal <> "" Then
        '        prtSettings.PrinterName = Registro_ColaPedidoLocal
        '    End If
        'ElseIf vNomTabla = UCase("REMISION") Then
        '    If Registro_ColaRemisionLocal <> "" Then
        '        prtSettings.PrinterName = Registro_ColaRemisionLocal
        '    End If
        '    prtSettings.Copies = GetContenidoControl(0, 0)
        'End If
        'prtDoc.PrinterSettings = prtSettings

        'PrintDialog1.Document = prtDoc
        'PrintDialog1.PrinterSettings = prtDoc.PrinterSettings
        'PrintDialog1.AllowSomePages = True
        'PrintDialog1.AllowPrintToFile = False
        'If PrintDialog1.ShowDialog = DialogResult.OK Then
        '    prtDoc.PrinterSettings = PrintDialog1.PrinterSettings
        '    'ObjCnt(0).value = PrintDialog1.PrinterSettings.Copies
        '    'ObjCnt(0)
        '    'prtDoc.Print()
        'End If


    End Sub
End Class
