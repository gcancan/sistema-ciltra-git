'Realizo: Roger Gala
'Fecha: 4 / Junio / 2009
'Fecha Mod: 22 / Abril / 2013
Imports System.IO
Public Class frmLogin
    Inherits System.Windows.Forms.Form

    Protected DS As New DataSet
    Dim DsCombo As New DataSet
    Dim Opcion As String
    Dim ExisteUsr As String
    Dim Existe As String
    Dim Salida As Boolean
    Dim indice As Integer = 0
    Dim ArraySql() As String
    Dim sPosibleError As String
    Dim PassWord As String
    Dim GpoUser, NomUser, UserId, vCCODIGOC01_CLI As String
    Dim vEsExterno As Boolean = False
    Dim UserActivo As Boolean = False
    Dim NomTablaEmp As String = "CatEmpresas"
    Public WithEvents MeStatus1 As ControlStatus.MeStatus
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents chkAutWindows As System.Windows.Forms.CheckBox
    Friend WithEvents txtsUserId As System.Windows.Forms.ComboBox
    Friend WithEvents TLCopilacion As System.Windows.Forms.ToolStripLabel
    Friend WithEvents TSConfig As System.Windows.Forms.ToolStripButton
    Public WithEvents fraSeleccionEmpresa As System.Windows.Forms.GroupBox
    Public WithEvents cbEmpresa As System.Windows.Forms.ComboBox
    Public WithEvents imgLogoCompac As System.Windows.Forms.PictureBox
    Dim NomTabla As String = "Usuarios"
    'Dim NomServidor, NomBd As String
    Dim myDataColumn As DataColumn
    Public MyTablaEmpresas As DataTable = New DataTable(TablaBd)
    Dim TablaBd As String = "EmpresasAdminPaq"
    Dim myDataRow As DataRow
    Friend WithEvents cmdTerminar As System.Windows.Forms.Button
    Friend WithEvents cmdOk As System.Windows.Forms.Button
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents chkAutWindowsCOM As System.Windows.Forms.CheckBox

    Dim ExisteEmpresa As String
    Dim bAutWinCOM As Boolean = True

    Dim StrSql As String = ""
    Dim DsConsulta As New DataTable

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents lblnNumNotario As System.Windows.Forms.Label
    Friend WithEvents lblsNombre As System.Windows.Forms.Label
    Friend WithEvents txtsPasword As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cmbEmpresa As System.Windows.Forms.ComboBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLogin))
        Me.lblnNumNotario = New System.Windows.Forms.Label()
        Me.txtsPasword = New System.Windows.Forms.TextBox()
        Me.lblsNombre = New System.Windows.Forms.Label()
        Me.cmbEmpresa = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.MeStatus1 = New ControlStatus.MeStatus()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.TSConfig = New System.Windows.Forms.ToolStripButton()
        Me.TLCopilacion = New System.Windows.Forms.ToolStripLabel()
        Me.chkAutWindows = New System.Windows.Forms.CheckBox()
        Me.txtsUserId = New System.Windows.Forms.ComboBox()
        Me.fraSeleccionEmpresa = New System.Windows.Forms.GroupBox()
        Me.chkAutWindowsCOM = New System.Windows.Forms.CheckBox()
        Me.cbEmpresa = New System.Windows.Forms.ComboBox()
        Me.imgLogoCompac = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.cmdTerminar = New System.Windows.Forms.Button()
        Me.cmdOk = New System.Windows.Forms.Button()
        Me.ToolStrip1.SuspendLayout()
        Me.fraSeleccionEmpresa.SuspendLayout()
        CType(Me.imgLogoCompac, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblnNumNotario
        '
        Me.lblnNumNotario.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lblnNumNotario.Location = New System.Drawing.Point(86, 43)
        Me.lblnNumNotario.Name = "lblnNumNotario"
        Me.lblnNumNotario.Size = New System.Drawing.Size(80, 16)
        Me.lblnNumNotario.TabIndex = 14
        Me.lblnNumNotario.Text = "Usuario:"
        '
        'txtsPasword
        '
        Me.txtsPasword.Location = New System.Drawing.Point(238, 75)
        Me.txtsPasword.MaxLength = 15
        Me.txtsPasword.Name = "txtsPasword"
        Me.txtsPasword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtsPasword.Size = New System.Drawing.Size(184, 20)
        Me.txtsPasword.TabIndex = 2
        '
        'lblsNombre
        '
        Me.lblsNombre.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lblsNombre.Location = New System.Drawing.Point(86, 75)
        Me.lblsNombre.Name = "lblsNombre"
        Me.lblsNombre.Size = New System.Drawing.Size(88, 16)
        Me.lblsNombre.TabIndex = 16
        Me.lblsNombre.Text = "Contrase�a:"
        '
        'cmbEmpresa
        '
        Me.cmbEmpresa.Location = New System.Drawing.Point(235, 18)
        Me.cmbEmpresa.Name = "cmbEmpresa"
        Me.cmbEmpresa.Size = New System.Drawing.Size(184, 21)
        Me.cmbEmpresa.TabIndex = 78
        Me.cmbEmpresa.Visible = False
        '
        'Label1
        '
        Me.Label1.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label1.Location = New System.Drawing.Point(91, 18)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(80, 16)
        Me.Label1.TabIndex = 19
        Me.Label1.Text = "Empresa:"
        Me.Label1.Visible = False
        '
        'MeStatus1
        '
        Me.MeStatus1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.MeStatus1.Location = New System.Drawing.Point(0, 276)
        Me.MeStatus1.Margin = New System.Windows.Forms.Padding(4)
        Me.MeStatus1.Name = "MeStatus1"
        Me.MeStatus1.Size = New System.Drawing.Size(443, 27)
        Me.MeStatus1.TabIndex = 21
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TSConfig, Me.TLCopilacion})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(443, 25)
        Me.ToolStrip1.TabIndex = 22
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'TSConfig
        '
        Me.TSConfig.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.TSConfig.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.TSConfig.Image = Global.Fletera.My.Resources.Resources.settings2
        Me.TSConfig.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TSConfig.Name = "TSConfig"
        Me.TSConfig.Size = New System.Drawing.Size(23, 22)
        Me.TSConfig.Text = "ToolStripDropDownButton1"
        Me.TSConfig.ToolTipText = "Configurar Conexion al Servidor"
        '
        'TLCopilacion
        '
        Me.TLCopilacion.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.TLCopilacion.Font = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TLCopilacion.Name = "TLCopilacion"
        Me.TLCopilacion.Size = New System.Drawing.Size(112, 22)
        Me.TLCopilacion.Text = "No. Compilacion:"
        '
        'chkAutWindows
        '
        Me.chkAutWindows.AutoSize = True
        Me.chkAutWindows.Checked = True
        Me.chkAutWindows.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkAutWindows.Location = New System.Drawing.Point(18, 251)
        Me.chkAutWindows.Name = "chkAutWindows"
        Me.chkAutWindows.Size = New System.Drawing.Size(158, 17)
        Me.chkAutWindows.TabIndex = 23
        Me.chkAutWindows.Text = "Autentificaci�n de Windows"
        Me.chkAutWindows.UseVisualStyleBackColor = True
        '
        'txtsUserId
        '
        Me.txtsUserId.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.txtsUserId.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.txtsUserId.FormattingEnabled = True
        Me.txtsUserId.Location = New System.Drawing.Point(238, 45)
        Me.txtsUserId.Name = "txtsUserId"
        Me.txtsUserId.Size = New System.Drawing.Size(184, 21)
        Me.txtsUserId.TabIndex = 1
        '
        'fraSeleccionEmpresa
        '
        Me.fraSeleccionEmpresa.BackColor = System.Drawing.SystemColors.Control
        Me.fraSeleccionEmpresa.Controls.Add(Me.chkAutWindowsCOM)
        Me.fraSeleccionEmpresa.Controls.Add(Me.cbEmpresa)
        Me.fraSeleccionEmpresa.Controls.Add(Me.imgLogoCompac)
        Me.fraSeleccionEmpresa.Font = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.fraSeleccionEmpresa.ForeColor = System.Drawing.SystemColors.Highlight
        Me.fraSeleccionEmpresa.Location = New System.Drawing.Point(9, 99)
        Me.fraSeleccionEmpresa.Name = "fraSeleccionEmpresa"
        Me.fraSeleccionEmpresa.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraSeleccionEmpresa.Size = New System.Drawing.Size(425, 105)
        Me.fraSeleccionEmpresa.TabIndex = 25
        Me.fraSeleccionEmpresa.TabStop = False
        Me.fraSeleccionEmpresa.Text = "Selecci�n de empresa "
        '
        'chkAutWindowsCOM
        '
        Me.chkAutWindowsCOM.AutoSize = True
        Me.chkAutWindowsCOM.Location = New System.Drawing.Point(218, 78)
        Me.chkAutWindowsCOM.Name = "chkAutWindowsCOM"
        Me.chkAutWindowsCOM.Size = New System.Drawing.Size(199, 21)
        Me.chkAutWindowsCOM.TabIndex = 25
        Me.chkAutWindowsCOM.Text = "Autentificaci�n de Windows "
        Me.chkAutWindowsCOM.UseVisualStyleBackColor = True
        '
        'cbEmpresa
        '
        Me.cbEmpresa.BackColor = System.Drawing.SystemColors.Window
        Me.cbEmpresa.Cursor = System.Windows.Forms.Cursors.Default
        Me.cbEmpresa.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cbEmpresa.Location = New System.Drawing.Point(120, 24)
        Me.cbEmpresa.Name = "cbEmpresa"
        Me.cbEmpresa.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cbEmpresa.Size = New System.Drawing.Size(297, 25)
        Me.cbEmpresa.TabIndex = 3
        '
        'imgLogoCompac
        '
        Me.imgLogoCompac.Cursor = System.Windows.Forms.Cursors.Default
        Me.imgLogoCompac.Image = Global.Fletera.My.Resources.Resources.logo_ciltra_transparente1
        Me.imgLogoCompac.Location = New System.Drawing.Point(16, 16)
        Me.imgLogoCompac.Name = "imgLogoCompac"
        Me.imgLogoCompac.Size = New System.Drawing.Size(98, 83)
        Me.imgLogoCompac.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.imgLogoCompac.TabIndex = 5
        Me.imgLogoCompac.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(9, 18)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(72, 56)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 81
        Me.PictureBox1.TabStop = False
        '
        'cmdTerminar
        '
        Me.cmdTerminar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdTerminar.Image = CType(resources.GetObject("cmdTerminar.Image"), System.Drawing.Image)
        Me.cmdTerminar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdTerminar.Location = New System.Drawing.Point(348, 213)
        Me.cmdTerminar.Name = "cmdTerminar"
        Me.cmdTerminar.Size = New System.Drawing.Size(88, 56)
        Me.cmdTerminar.TabIndex = 80
        Me.cmdTerminar.Text = "&Terminar"
        Me.cmdTerminar.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cmdOk
        '
        Me.cmdOk.Image = CType(resources.GetObject("cmdOk.Image"), System.Drawing.Image)
        Me.cmdOk.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdOk.Location = New System.Drawing.Point(252, 213)
        Me.cmdOk.Name = "cmdOk"
        Me.cmdOk.Size = New System.Drawing.Size(88, 56)
        Me.cmdOk.TabIndex = 79
        Me.cmdOk.Text = "&Ok"
        Me.cmdOk.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'frmLogin
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(443, 303)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.cmdTerminar)
        Me.Controls.Add(Me.cmdOk)
        Me.Controls.Add(Me.fraSeleccionEmpresa)
        Me.Controls.Add(Me.txtsUserId)
        Me.Controls.Add(Me.chkAutWindows)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.MeStatus1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cmbEmpresa)
        Me.Controls.Add(Me.txtsPasword)
        Me.Controls.Add(Me.lblsNombre)
        Me.Controls.Add(Me.lblnNumNotario)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmLogin"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Acceso al Sistema"
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.fraSeleccionEmpresa.ResumeLayout(False)
        Me.fraSeleccionEmpresa.PerformLayout()
        CType(Me.imgLogoCompac, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    'FUNCIONES Y PROCEDIMIENTOS
    Private Sub LimpiaCampos()
        txtsUserId.Text = ""
        txtsPasword.Text = ""
        SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")
    End Sub
    Private Sub ActivaCampos(ByVal valor As Boolean)
        txtsUserId.Enabled = Not valor
        txtsPasword.Enabled = Not valor
    End Sub
    Private Sub ActivaBotones(ByVal valor As Boolean)
        cmdOk.Enabled = valor
        cmdTerminar.Enabled = valor
    End Sub
    Private Function LlenaDatos(ByVal cCve As String, ByVal dbtabla As String, ByVal Campo As String, ByVal TipoCampo As TipoDato) As String
        Try
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'SE CREA LA CADENA DE CONEXION VALIDA PARA LA EMPRESA QUE SE ESCOJA
            Dim util As New CapaNegocio.Parametros

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'PARA MULTIEMPRESA
            'With DsCombo.Tables(NomTablaEmp).DefaultView
            '    Dim nEmp As Integer = cmbEmpresa.SelectedValue
            '    If .Count > 0 Then
            '        .RowFilter = "sCveEmp = " & cmbEmpresa.SelectedValue
            '        If .Count > 0 Then
            '            NomServidor = .Item(0).Item("sNomServidor")
            '            NomBd = .ITEM(0).ITEM("SNOMBD")
            '        End If
            '        .RowFilter = Nothing
            '        cmbEmpresa.SelectedValue = nEmp
            '    End If
            'End With
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Process.GetCurrentProcess.ProcessName = SOPORTETEC
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Salida = True

            'ChecaValoresRegistro(chkAutWindows.Checked)
            ChecaValoresRegistro(chkAutWindows.Checked, chkAutWindowsCOM.Checked, "")
            Salida = False

            DS = Inicio.ChecaClave(txtsUserId.Text, Inicio.CONSTR, TipoConexion.TcSQL, dbtabla, Campo, TipoCampo, Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS)
            sPosibleError = GetSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")
            If sPosibleError <> "ERROR" Or sPosibleError = Nothing Then
                SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")
                If Not DS Is Nothing Then
                    If DS.Tables(dbtabla).DefaultView.Count > 0 Then
                        PassWord = DS.Tables(dbtabla).DefaultView.Item(0).Item("sPassword")
                        UserActivo = DS.Tables(dbtabla).DefaultView.Item(0).Item("bActivo")

                        If generarClaveSHA1(Trim(txtsPasword.Text)) = PassWord Then
                            'If Trim(txtsPasword.Text) = PassWord Then


                            If Not DS.Tables(dbtabla).DefaultView.Item(0).Item("sCveGrupo") Is DBNull.Value Then
                                GpoUser = DS.Tables(dbtabla).DefaultView.Item(0).Item("sCveGrupo")
                            Else
                                GpoUser = ""
                            End If

                            'If Not DS.Tables(dbtabla).DefaultView.Item(0).Item("CCODIGOC01_CLI") Is DBNull.Value Then
                            '    vCCODIGOC01_CLI = DS.Tables(dbtabla).DefaultView.Item(0).Item("CCODIGOC01_CLI")
                            'Else
                            '    vCCODIGOC01_CLI = ""
                            'End If

                            'If Not DS.Tables(dbtabla).DefaultView.Item(0).Item("bExterno") Is DBNull.Value Then
                            '    vEsExterno = DS.Tables(dbtabla).DefaultView.Item(0).Item("bExterno")
                            'Else
                            '    vEsExterno = False
                            'End If

                            NomUser = DS.Tables(dbtabla).DefaultView.Item(0).Item("sNomUser")
                            If UserActivo Then
                                LlenaDatos = KEY_RCORRECTO
                            Else
                                LlenaDatos = "USUARIOINACTIVO"
                            End If
                        Else
                            LlenaDatos = "PASSINCORRECTO"
                        End If
                    Else
                        LlenaDatos = "NOEXISTEUSR"
                    End If
                Else
                    LlenaDatos = "NOEXISTEUSR"
                End If
            Else
                SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")
                LlenaDatos = KEY_RERROR
            End If

        Catch ex As Exception
            SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")
            LlenaDatos = KEY_RERROR
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message & vbCrLf & ex.ToString, MsgBoxStyle.Information, Me.Text)
        End Try
    End Function

    Private Function LlenaDatosEsp(ByVal cCve As String, ByVal dbtabla As String, ByVal Campo As String, ByVal TipoCampo As TipoDato, _
                         ByVal Constr As String, Optional ByVal FiltroEsp As String = "", Optional ByVal Filtro2 As String = "", _
                         Optional ByVal cCve2 As String = "", Optional ByVal cCve3 As String = "") As String
        Dim strsql As String = ""
        Try
            Select Case UCase(dbtabla)
                Case UCase("EMPRESAADM")
                    strsql = "SELECT ISNULL(CIDEMPRESA,0) AS CIDEMPRESA, ISNULL(CNOMBREE01,'') AS CNOMBREE01, ISNULL(CRUTADATOS,' ') AS CRUTADATOS FROM PARAMETROS WHERE IDPARAMETRO = 1"

                    DS = Inicio.ChecaClavexSentencia(cCve, Inicio.CONSTR, TipoConexion.TcSQL, dbtabla, "IdParametro", TipoCampo, Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, strsql, FiltroEsp, Filtro2, , True)

            End Select
            sPosibleError = GetSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")
            If sPosibleError <> "ERROR" Or sPosibleError = Nothing Then
                SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")
                If Not DS Is Nothing Then
                    If DS.Tables(dbtabla).DefaultView.Count > 0 Then
                        Select Case UCase(dbtabla)
                            Case UCase("EMPRESAADM")
                                If Not DS.Tables(dbtabla).DefaultView.Item(0).Item("CIDEMPRESA") Is DBNull.Value Then
                                    sIdEmpresa = DS.Tables(dbtabla).DefaultView.Item(0).Item("CIDEMPRESA")
                                Else
                                    sIdEmpresa = ""
                                End If

                                If Not DS.Tables(dbtabla).DefaultView.Item(0).Item("CNOMBREE01") Is DBNull.Value Then
                                    sNombreEmpresaAdmPAQ = DS.Tables(dbtabla).DefaultView.Item(0).Item("CNOMBREE01")
                                Else
                                    sNombreEmpresaAdmPAQ = ""
                                End If

                                If Not DS.Tables(dbtabla).DefaultView.Item(0).Item("CRUTADATOS") Is DBNull.Value Then

                                    'sRutaEmpresaAdmPAQ = Mid(DS.Tables(dbtabla).DefaultView.Item(0).Item("CRUTADATOS"), 1, DS.Tables(dbtabla).DefaultView.Item(0).Item("CRUTADATOS").Length - 1)
                                    sRutaEmpresaAdmPAQ = DS.Tables(dbtabla).DefaultView.Item(0).Item("CRUTADATOS")

                                Else
                                    sRutaEmpresaAdmPAQ = ""
                                End If
                        End Select
                        LlenaDatosEsp = KEY_RCORRECTO
                    Else
                        LlenaDatosEsp = KEY_RINCORRECTO
                    End If
                Else
                    LlenaDatosEsp = KEY_RINCORRECTO
                End If
            Else
                SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")
                LlenaDatosEsp = KEY_RERROR
            End If

        Catch ex As Exception
            SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")
            LlenaDatosEsp = KEY_RERROR
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, Me.Text)
        End Try

    End Function
    Private Sub Cancelar()
        ActivaCampos(False)
        ActivaBotones(True)
        LimpiaCampos()
        indice = 0
    End Sub

    Private Function Validar() As Boolean
        Validar = True
        If Not Inicio.ValidandoCampo(txtsUserId.Text, TipoDato.TdCadena) Then
            MsgBox("La clave del Motivo no puede ser Nulo !!!", MsgBoxStyle.Information, Me.Text)
            txtsUserId.Focus()
            txtsUserId.SelectAll()
            Validar = False
        ElseIf Not Inicio.ValidandoCampo(txtsPasword.Text, TipoDato.TdCadena) Then
            MsgBox("La descripci�n del motivo no puede ser vacio !!!", MsgBoxStyle.Information, Me.Text)
            txtsPasword.Focus()
            txtsPasword.SelectAll()
            Validar = False
        End If
    End Function
    'Private Sub LlenaCombos(ByVal CampoClave As String, ByVal CampoDescripcion As String, ByVal dbTabla As String)
    '    Dim StrSql As String

    '    Try
    '        DsCombo.Clear()
    '        'StrSql = "select sCveEmp,sNomEmp,sNomBd,sNomServidor from CatEmpresas"
    '        'If UCase(dbTabla) = UCase("Persona") Then
    '        '    StrSql = " SELECT 1 AS PERSONA, 'PERSONA FISICA' AS NOMPERSONA " & _
    '        '            " UNION " & _
    '        '            " SELECT 0 AS PERSONA, 'PERSONA MORAL' AS NOMPERSONA ORDER BY PERSONA DESC"
    '        'Else
    '        '    StrSql = "select " & CampoClave & "," & CampoDescripcion & ", " & CampoClave & " + ' - ' + " & CampoDescripcion & " as Descripcion2 from " & dbTabla & " order by " & CampoDescripcion
    '        'End If

    '        Dim util As New CapaNegocio.Parametros
    '        Dim ConStrInicio As String = util.CreaConStrSQL("(local)", "BDCONFIG", True, "", "")

    '        'DsCombo = Inicio.ChecaClavexSentencia(ConStrInicio, TipoConexion.TcSQL, StrSql, dbTabla)
    '        StrSql = "select sCveEmp,sNomEmp,sNomBd,sNomServidor from CatEmpresas"
    '        DsCombo = Inicio.LlenaCombos("sCveEmp", "NOMSOLICITANTE", dbTabla, _
    '        TipoConexion.TcSQL, Inicio.CONSTR, Process.GetCurrentProcess.ProcessName, _
    '        KEY_CHECAERROR, KEY_ESTATUS, "", StrSql)

    '        If Not DsCombo Is Nothing Then
    '            If DsCombo.Tables(dbTabla).DefaultView.Count > 0 Then
    '                cmbEmpresa.DataSource = DsCombo.Tables(dbTabla)
    '                cmbEmpresa.ValueMember = CampoClave
    '                cmbEmpresa.DisplayMember = CampoDescripcion
    '            End If
    '        End If
    '    Catch ex As Exception

    '    End Try
    'End Sub
    ''C O N T R O L E S
    Private Sub frmLogin_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If Trim(txtsUserId.Text) <> "" Then
            txtsPasword.Focus()
        Else
            txtsUserId.Focus()
            txtsUserId.SelectAll()
        End If

    End Sub
    Private Sub BorraValoresRegistro(ByVal Opcion As String)
        If Opcion = "TODOS" Then
            DeleteSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL")
        Else
            DeleteSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", Opcion)
        End If

    End Sub

    Private Sub frmLogin_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.GotFocus

    End Sub



    Private Sub frmLogin_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        If e.KeyValue = Keys.F12 Then

            'txtsPasword.Text = "softng2013"
            txtsPasword.Text = "softng2013"
        ElseIf e.KeyValue = Keys.F1 Then
            FConfig.Show(Me)

        ElseIf e.KeyValue = Keys.F11 Then
            If MsgBox("!! Esta seguro que desea borrar los ultimos valores de configuraci�n para el acceso al sistema ???", MsgBoxStyle.YesNo, "Restablecer valores de Configuracion") = MsgBoxResult.Yes Then
                BorraValoresRegistro("TODOS")
                Salida = True
                'ChecaValoresRegistro(chkAutWindows.Checked)
                ChecaValoresRegistro(chkAutWindows.Checked, chkAutWindowsCOM.Checked, "")
                Salida = False
            End If
        ElseIf e.KeyValue = Keys.F10 Then
            If MsgBox("!! Esta seguro que desea borrar los valores para Usuario y Password SQL ???", MsgBoxStyle.YesNo, "Restablecer valores de Configuracion") = MsgBoxResult.Yes Then
                'Nombre del Usuario SQL
                'Password del Usuario SQL
                BorraValoresRegistro("Nombre del Usuario SQL")
                BorraValoresRegistro("Password del Usuario SQL")
                Salida = True
                'ChecaValoresRegistro(chkAutWindows.Checked)
                ChecaValoresRegistro(chkAutWindows.Checked, chkAutWindowsCOM.Checked, "")
                Salida = False
            End If

        End If
    End Sub

    'Private Function ChecaValorRutaSDK() As String
    '    Dim ObjReg As New ToolRegistro
    '    Dim Resp As String
    '    ChecaValorRutaSDK = ""
    '    If ObjReg.LeeKey(KeyPathAdminPAQ) Then
    '        Resp = ObjReg.LeeLlaveRegistro(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Autentificacion Windows", KeyPathLM)
    '        If Resp = "" Then
    '            Return Resp
    '        End If
    '    End If
    'End Function

    Private Function ChecaValorAutWindows() As Boolean
        'leerlo de local_machine
        Dim ObjReg As New ToolRegistro
        'Dim Resp As String

        Try
            '3/FEB/2016
            'If ObjReg.LeeKey(KeyPathLM) Then
            '    Resp = ObjReg.LeeLlaveRegistro(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Autentificacion Windows", KeyPathLM)
            '    If Resp = "" Then
            '        sAutentificacionWindows = chkAutWindows.Checked
            '        'Return chkAutWindows.Checked

            '    Else
            '        sAutentificacionWindows = CBool(Resp)
            '        'Return CBool(Resp)
            '    End If
            'Else
            '    sAutentificacionWindows = chkAutWindows.Checked
            'End If

            'Return sAutentificacionWindows

            'AUTENTIFICACION windows
            If GetSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Autentificacion Windows", "") = "" Then
                sAutentificacionWindows = chkAutWindows.Checked
                SaveSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Autentificacion Windows", sAutentificacionWindows)
            Else
                sAutentificacionWindows = GetSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Autentificacion Windows", False)
                'chkAutWindows.Checked = sAutentificacionWindows
                'If sAutentificacionWindows <> chkAutWindows.Checked Then
                '    sAutentificacionWindows = chkAutWindows.Checked
                '    SaveSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Autentificacion Windows", sAutentificacionWindows)
                'Else
                '    sAutentificacionWindows = GetSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Autentificacion Windows", False)
                'End If
            End If
            'ChecaValoresRegistro(chkAutWindows.Checked)
            Return sAutentificacionWindows

        Catch ex As Exception
            Return False
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try

    End Function

    Private Sub CreaTablaEmpresas()


        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.String")
        myDataColumn.ColumnName = "EMPRESA"
        myDataColumn.ReadOnly = False
        myDataColumn.Unique = False
        ' Add the Column to the DataColumnCollection.
        MyTablaEmpresas.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.String")
        myDataColumn.ColumnName = "RUTA"
        myDataColumn.ReadOnly = False
        myDataColumn.Unique = False
        ' Add the Column to the DataColumnCollection.
        MyTablaEmpresas.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Int64")
        myDataColumn.ColumnName = "IDEMPRESA"
        myDataColumn.ReadOnly = False
        myDataColumn.Unique = False
        ' Add the Column to the DataColumnCollection.
        MyTablaEmpresas.Columns.Add(myDataColumn)

    End Sub

    'Private Sub llenaEmpresas()
    '    Dim ObjReg As New ToolRegistro
    '    Dim lError As Integer = 0
    '    Dim lidEmpresa As Integer = 0
    '    Dim lRutaEmpresa As String = ""
    '    Dim lNombreEmpresa As String = ""
    '    Try
    '        CreaTablaEmpresas()
    '        '----------------- ADMINPAQ ----------------- 
    '        lRutaEmpresa = Space(61)
    '        lNombreEmpresa = Space(61)



    '        'directoriobase()

    '        lError = fSetNombrePAQ("AdminPAQ")
    '        If lError <> 0 Then
    '            MensajeError(lError)
    '            End
    '        End If

    '        Parametro_DIRBASE = ObjReg.LeeLlaveRegistro("Computaci�n en Acci�n, SA CV", "AdminPAQ", "DIRECTORIOBASE", KeyPathCA)
    '        Directory.SetCurrentDirectory(Parametro_DIRBASE)
    '        'Resultado = ObjReg.LeeLlaveRegistro(Me.ProductName, "ServerSQL", "Nombre del servidor")
    '        ' Inicializar el SDK
    '        lError = fInicializaSDK()
    '        If lError <> 0 Then
    '            MensajeError(lError)
    '            End
    '        End If

    '        '            lLicencia = 0
    '        '           lError = fInicializaLicenseInfo(lLicencia) ' 0=AdminPAQ, 1=FACTURA ELECTR�NICA
    '        '          If lError <> 0 Then
    '        'MensajeError(lError)
    '        'End
    '        'End If


    '        cbEmpresa.Items.Clear()
    '        'cbRuta.Items.Clear()

    '        ' Ir a la primera empresa
    '        lError = fPosPrimerEmpresa(lidEmpresa, lNombreEmpresa, lRutaEmpresa)
    '        If lError <> 0 Then
    '            MensajeError(lError)
    '            End
    '        End If

    '        While lError = 0
    '            'cbRuta.Items.Add(lRutaEmpresa)
    '            'cbEmpresa.Items.Add(lNombreEmpresa)
    '            myDataRow = MyTablaEmpresas.NewRow()
    '            myDataRow("EMPRESA") = lNombreEmpresa
    '            myDataRow("RUTA") = lRutaEmpresa
    '            myDataRow("IDEMPRESA") = lidEmpresa
    '            '
    '            MyTablaEmpresas.Rows.Add(myDataRow)
    '            lRutaEmpresa = Space(61)
    '            lNombreEmpresa = Space(61)
    '            lError = fPosSiguienteEmpresa(lidEmpresa, lNombreEmpresa, lRutaEmpresa)
    '        End While


    '        '----------------- ADMINPAQ ----------------- 
    '        If Not MyTablaEmpresas Is Nothing Then
    '            If MyTablaEmpresas.DefaultView.Count > 0 Then
    '                cbEmpresa.DataSource = MyTablaEmpresas
    '                cbEmpresa.ValueMember = "RUTA"
    '                cbEmpresa.DisplayMember = "EMPRESA"
    '            End If
    '        End If
    '        cbEmpresa.SelectedIndex = 0
    '        'cbRuta.SelectedIndex = 0
    '    Catch ex As Exception
    '        MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "LlenaEmpresas")
    '    End Try
    'End Sub

    Private Sub frmLogin_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try

            Me.Text = "Acceso al Sistema " & Application.ProductName & " " & FormatNumber(Version, 2)
            Status("Iniciar Sesion al Sistema " & Application.ProductName & " ", Me)
            TLCopilacion.Text = TLCopilacion.Text & " " & idActualizacion
            ActivaCampos(False)
            LimpiaCampos()
            ActivaBotones(True)
            'LlenaCombos("sCveEmp", "sNomEmp", NomTablaEmp)
            Dim UsrLog() As String = My.Settings.UsersLogeados.Split({"|"}, StringSplitOptions.RemoveEmptyEntries)
            'Dim UsrLog() As String = UsersLogeados.Split({"|"}, StringSplitOptions.RemoveEmptyEntries)
            For i As Integer = 0 To UsrLog.Length - 1
                txtsUserId.Items.Add(UsrLog(i))
            Next
            If GetSetting(Process.GetCurrentProcess.ProcessName, KEY_CONFIGURACION, KEY_CONFUSUARIO, "") <> "" Then
                txtsUserId.Text = GetSetting(Process.GetCurrentProcess.ProcessName, KEY_CONFIGURACION, KEY_CONFUSUARIO, "")
            End If



            chkAutWindows.Checked = ChecaValorAutWindows()

            Salida = True
            'ChecaValoresRegistro(chkAutWindows.Checked)
            ChecaValoresRegistro(chkAutWindows.Checked, chkAutWindowsCOM.Checked, "")
            Salida = False


            'OJJOOOOOOOO
            '---------------------------------------------------------------------------------------------------------------------
            'UbicacionSDK = ChecaValorRutaSDK()
            'If UbicacionSDK <> "" Then
            '    AgregaPath(UbicacionSDK)
            '    'MsgBox("La Ubicaci�n " & UbicacionSDK & " No se encuentra en el PATH", MsgBoxStyle.Information, Me.Text)
            'End If

            'llenaEmpresas()
            CreaTablaEmpresas()
            llenaEmpresasFin()


            ''Checar los valores de empresa en Parametros
            'ExisteEmpresa = LlenaDatosEsp("1", "EMPRESAADM", "", TipoDato.TdCadena, Inicio.CONSTR)
            'If ExisteEmpresa = KEY_RCORRECTO Then
            '    If sIdEmpresa > 0 Then
            '        cbEmpresa.SelectedValue = sRutaEmpresaAdmPAQ
            '        cbEmpresa.Enabled = False
            '    End If
            'Else
            '    MsgBox("No Leyo la Empresa ")
            'End If
            '---------------------------------------------------------------------------------------------------------------------

            'YA no se utiliza por que el dato se encuentra en parametros
            'If GetSetting(Process.GetCurrentProcess.ProcessName, KEY_CONFIG_SECCION, KEY_CONFIF_KEY, "") <> "" Then
            '    cbEmpresa.SelectedValue = GetSetting(Process.GetCurrentProcess.ProcessName, KEY_CONFIG_SECCION, KEY_CONFIF_KEY, "")
            'End If



            ''AUTENTIFICACION windows
            'If GetSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Autentificacion Windows", "") = "" Then
            '    sAutentificacionWindows = chkAutWindows.Checked
            '    SaveSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Autentificacion Windows", sAutentificacionWindows)
            'Else
            '    sAutentificacionWindows = GetSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Autentificacion Windows", False)
            '    chkAutWindows.Checked = sAutentificacionWindows
            'End If


            'checar que parametro es el de Autentificacion de windows
            'Existe = LlenaDatosEsp("1", "parametros", "IdParametro", TipoDato.TdNumerico)

            'txtsPasword.Text = "galita"


        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, Me.Text)
        End Try

    End Sub

    Private Sub AgregaPath(ByVal vPath As String)
        Dim envName As String = "Path"
        Dim envValue As String = vPath & ";"

        'Dim ValorPath As String = ChecaValorRutaPATH()
        Dim ValorPath As String = ""
        Dim ExistePath As Boolean = False
        Try


            ValorPath = Environment.GetEnvironmentVariable(envName)

            'ExistePath = ValorPath.con(vPath)

            ExistePath = ValorPath.Contains(vPath)

            If Not ExistePath Then
                MsgBox("La Ubicaci�n " & UbicacionSDK & " No se encuentra en el PATH", MsgBoxStyle.Information, Me.Text)


                'NO funciona
                'Environment.SetEnvironmentVariable(envName, ValorPath & ";" & envValue)

                '' Determine whether the environment variable exists.
                'If Environment.GetEnvironmentVariable(envName) Is Nothing Then
                '    ' If it doesn't exist, create it.
                '    Environment.SetEnvironmentVariable(envName, envValue)
                'End If
            End If
        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, Me.Text)
        End Try

    End Sub

    Private Function ChecaValorRutaPATH() As String
        Dim ObjReg As New ToolRegistro
        Dim Resp As String
        ChecaValorRutaPATH = ""
        Try

            If ObjReg.LeeKey(KeyPathAdminPAQ) Then
                'Public KeyPathAdminPAQ As String = "HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Computaci�n en Acci�n, SA CV\AdminPAQ"
                'Resp = ObjReg.LeeLlaveRegistro(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Autentificacion Windows", KeyPathLM)
                Resp = ObjReg.LeeLlaveRegistro("", "AdminPAQ", "DIRECTORIOBASE", KeyPathAdminPAQ)
                If Resp <> "" Then
                    Return Resp
                Else
                    Return ""
                End If
            End If
        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, Me.Text)
        End Try

    End Function
    Private Function ChecaValorRutaSDK() As String
        Dim ObjReg As New ToolRegistro
        Dim Resp As String
        ChecaValorRutaSDK = ""
        Try

            If ObjReg.LeeKey(KeyPathAdminPAQ) Then
                'Public KeyPathAdminPAQ As String = "HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Computaci�n en Acci�n, SA CV\AdminPAQ"
                'Resp = ObjReg.LeeLlaveRegistro(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Autentificacion Windows", KeyPathLM)
                Resp = ObjReg.LeeLlaveRegistro("", "AdminPAQ", "DIRECTORIOBASE", KeyPathAdminPAQ)
                If Resp <> "" Then
                    Return Resp
                Else
                    Return ""
                End If
            End If
        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, Me.Text)
        End Try

    End Function

    Private Sub txtsUserId_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtsUserId.KeyDown
        If e.KeyCode = Keys.Enter Then
            txtsPasword.Focus()
            e.Handled = True
        End If
    End Sub
    Private Sub txtsPasword_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtsPasword.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then

            If cbEmpresa.Enabled Then
                cbEmpresa.Focus()
            Else
                cmdOk.Focus()
            End If
            '   cmdOk.Focus()

            e.Handled = True
        End If
    End Sub

    'BOTONES
    Private Sub cmdCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Cancelar()
    End Sub

    Private Sub cmdTerminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        fCierraEmpresa()
        fTerminaSDK()
        Me.Close()
    End Sub
    Private Function BuscaIdEmpresa(ByVal NomEmpresa As String) As Integer
        BuscaIdEmpresa = 0
        If MyTablaEmpresas.DefaultView.Count > 0 Then
            MyTablaEmpresas.DefaultView.RowFilter = Nothing
            MyTablaEmpresas.DefaultView.RowFilter = "EMPRESA = '" & NomEmpresa & "'"
            If MyTablaEmpresas.DefaultView.Count > 0 Then
                Return MyTablaEmpresas.DefaultView.Item(0).Item("IDEMPRESA").ToString()
            End If
            MyTablaEmpresas.DefaultView.RowFilter = Nothing

        End If
    End Function

    Private Sub cmdOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOk.Click
        'Checar si el Usuario Existe para darle acceso al sistema
        If Salida Then Exit Sub
        Try

            ExisteUsr = LlenaDatos(txtsUserId.Text, "Usuarios", "sUserId", TipoDato.TdCadena)

            If ExisteUsr = KEY_RCORRECTO Then
                'MsgBox(KEY_RCORRECTO)
                'El Usuario Existe - Cargar Permisos
                'Me.DialogResult = DialogResult.OK
                BD = New CapaDatos.UtilSQL(CONSTR, NomUser) 'Se inicializa la Clase BD

                BDCOM = New CapaDatos.UtilSQL(CONSTR_COM, NomUser) 'Se inicializa la Clase BD



                'Para cambiar la cadena de conexion solo tiene que hacer esto
                'BD = New CapaDatos.UtilSQL("Nueva Conexion", NomUser) ' y todo lo que despues de esto use bd.executeretur se conectara a la nueva cadena de conexion
                'MsgBox("UserConectado = New UsuarioClass(txtsUserId.Text)")
                UserConectado = New UsuarioClass(txtsUserId.Text)
                If Not txtsUserId.Items.Contains(txtsUserId.Text) Then
                    txtsUserId.Items.Add(txtsUserId.Text)
                End If
                Dim CadUsLog As String = ""
                For i As Integer = 0 To txtsUserId.Items.Count - 1
                    CadUsLog += txtsUserId.Items(i) & "|"
                Next
                My.Settings.UsersLogeados = CadUsLog
                My.Settings.Save()
                'If BuscaActualizacion(Me) Then 'Si se hicieron los cambios correctamente CIERRA PROGRAMA
                '    End
                '    ' Exit Sub
                'End If
                'MsgBox("Grabar en el Registro")
                'Grabar en el Registro
                SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CONFIGURACION, KEY_CONFUSUARIO, txtsUserId.Text)

                'MsgBox("AbrirEmpresa = " & cbEmpresa.SelectedValue)
                'ojoooo
                'AbrirEmpresa(cbEmpresa.SelectedValue)

                'MsgBox("Dim frm As New FMenu")
                Dim frm As New FMenu

                'frm.RecibeDatosUsuario(txtsUserId.Text, GpoUser, NomUser, sNomBd, sNomServidor, chkAutWindows.Checked, cbEmpresa.Text, _
                'Trim(cbEmpresa.SelectedValue), BuscaIdEmpresa(cbEmpresa.Text), vEsExterno, vCCODIGOC01_CLI)

                'frm.RecibeDatosUsuario(txtsUserId.Text, GpoUser, NomUser, sNomBd, sNomServidor, chkAutWindows.Checked, "", _
                '"", 0, vEsExterno, vCCODIGOC01_CLI)

                frm.RecibeDatosUsuario(txtsUserId.Text, GpoUser, NomUser, sNomBd, sNomServidor, chkAutWindows.Checked, cbEmpresa.Text, _
                Trim(cbEmpresa.SelectedValue), BuscaIdEmpresa(cbEmpresa.Text), vEsExterno, vCCODIGOC01_CLI)



                Me.DialogResult = Windows.Forms.DialogResult.OK

            ElseIf ExisteUsr = "PASSINCORRECTO" Then
                'El Usuario no Existe 
                MsgBox("La Contrase�a del usuario es Incorrecta", MsgBoxStyle.Exclamation, Me.Text)
                txtsPasword.Focus()
                txtsPasword.SelectAll()
            ElseIf ExisteUsr = "NOEXISTEUSR" Then
                MsgBox("El Usuario no Existe", MsgBoxStyle.Exclamation, Me.Text)
                txtsPasword.Text = ""
                txtsUserId.Focus()
                txtsUserId.SelectAll()
            ElseIf ExisteUsr = "USUARIOINACTIVO" Then
                MsgBox("El Usuario se encuentra INACTIVO", MsgBoxStyle.Exclamation, Me.Text)
                txtsPasword.Text = ""
                txtsUserId.Focus()
                txtsUserId.SelectAll()

            End If

        Catch ex As Exception
            indice = 0
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try
    End Sub

    Private Sub txtsUserId_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtsUserId.KeyPress
        e.KeyChar = UCase(e.KeyChar)
        e.Handled = False
    End Sub

    'Private Sub cmdBorrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Dim strSql As String
    '    Try
    '        If ExisteUsr = KEY_RCORRECTO Then
    '            If MsgBox("!! Esta seguro que desea Eliminar el Motivo : " & txtsUserId.Text & " " & txtsPasword.Text & "? !!", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
    '                strSql = "DELETE FROM PUB.JurCatMotivosDon WHERE sCveMotDon = '" & txtsUserId.Text & "'"
    '                ReDim Preserve ArraySql(indice)
    '                ArraySql(indice) = strSql
    '                indice += 1

    '                'Dim obj As New NegocioDB.Tablas
    '                Dim obj As New bdNegocio.Tablas
    '                Dim Respuesta As String

    '                Respuesta = obj.EjecutarSql(Inicio.ConStrProg, "PROGRESS", ArraySql, Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, indice)
    '                If Respuesta = "EFECTUADO" Then
    '                    MsgBox("Motivo Eliminado satisfactoriamente", MsgBoxStyle.Exclamation, Me.Text)
    '                ElseIf Respuesta = "NOEFECTUADO" Then

    '                ElseIf Respuesta = "ERROR" Then
    '                    indice = 0
    '                    SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")
    '                End If
    '            Else
    '                Exit Sub
    '            End If
    '        Else
    '            MsgBox("No se puede Eliminar el Motivo : " & txtsUserId.Text & ", ya que no Existe ", MsgBoxStyle.Information, Me.Text)
    '            txtsPasword.Focus()
    '            txtsPasword.SelectAll()
    '            Exit Sub
    '        End If
    '    Catch ex As Exception
    '    End Try
    '    Cancelar()
    'End Sub

    Private Sub txtsUserId_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles txtsUserId.MouseDown
        txtsUserId.SelectAll()
    End Sub

    Private Sub txtsPasword_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtsPasword.Leave

    End Sub

    Private Sub txtsPasword_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles txtsPasword.MouseDown
        txtsPasword.SelectAll()
    End Sub
    Private Sub cmbEmpresa_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cmbEmpresa.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            e.Handled = True
            txtsUserId.Focus()
            txtsUserId.SelectAll()
        End If
    End Sub
    Private Sub TSConfig_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSConfig.Click
        FConfig.Show(Me)
    End Sub

    'Private Function LlenaDatosEsp(ByVal cCve As String, ByVal dbtabla As String, ByVal Campo As String, ByVal TipoCampo As TipoDato, Optional ByVal FiltroEsp As String = "", Optional ByVal Filtro2 As String = "") As String
    '    Dim strSql As String = ""
    '    Try
    '        'Select Case UCase(dbtabla)
    '        '    Case UCase(TablaBd)
    '        '        DS = Inicio.ChecaClave(cCve, Inicio.CONSTR, TipoConexion.TcSQL, dbtabla, Campo, TipoCampo, Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS)
    '        'End Select
    '        Salida = True
    '        ChecaValoresRegistro(chkAutWindows.Checked)
    '        Salida = False
    '        'Inicio.CONSTR = util.CreaConStrSQL(UCase(sNomServidor), UCase(sNomBd), False, "SA", "sofng2012")

    '        DS = Inicio.ChecaClave(cCve, Inicio.CONSTR, TipoConexion.TcSQL, dbtabla, Campo, TipoCampo, Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS)
    '        sPosibleError = GetSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")
    '        If sPosibleError <> "ERROR" Or sPosibleError = Nothing Then
    '            SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")
    '            If Not DS Is Nothing Then
    '                If DS.Tables(dbtabla).DefaultView.Count > 0 Then
    '                    'txtPrecioMetCuaTit.Text = DS.Tables(dbtabla).DefaultView.Item(0).Item("ImpxMetCuaTit")
    '                    'txtMetCuadTit.Text = DS.Tables(dbtabla).DefaultView.Item(0).Item("CantMetCuaTit")
    '                    'txtDiasSaldoVenc.Text = DS.Tables(dbtabla).DefaultView.Item(0).Item("DiasJuridico")
    '                    If DS.Tables(dbtabla).DefaultView.Item(0).Item("AutentificaWin") = True Then
    '                        chkAutWindows.Checked = True
    '                    Else
    '                        chkAutWindows.Checked = False
    '                    End If

    '                    LlenaDatosEsp = KEY_RCORRECTO
    '                Else
    '                    LlenaDatosEsp = KEY_RINCORRECTO
    '                End If
    '            Else
    '                LlenaDatosEsp = KEY_RINCORRECTO
    '            End If
    '        Else
    '            SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")
    '            LlenaDatosEsp = KEY_RERROR
    '        End If

    '    Catch ex As Exception
    '        SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")
    '        LlenaDatosEsp = KEY_RERROR
    '        MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, Me.Text)
    '    End Try
    'End Function

    Private Sub txtsUserId_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtsUserId.SelectedIndexChanged

    End Sub


    'Private Sub chkAutWindows_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkAutWindows.CheckedChanged
    '    'AUTENTIFICACION windows
    '    If GetSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Autentificacion Windows", "") = "" Then
    '        sAutentificacionWindows = chkAutWindows.Checked
    '        SaveSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Autentificacion Windows", sAutentificacionWindows)
    '    Else
    '        sAutentificacionWindows = GetSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Autentificacion Windows", False)
    '        chkAutWindows.Checked = sAutentificacionWindows
    '        'If sAutentificacionWindows <> chkAutWindows.Checked Then
    '        '    sAutentificacionWindows = chkAutWindows.Checked
    '        '    SaveSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Autentificacion Windows", sAutentificacionWindows)
    '        'Else
    '        '    sAutentificacionWindows = GetSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Autentificacion Windows", False)
    '        'End If
    '    End If
    '    ChecaValoresRegistro(chkAutWindows.Checked)
    'End Sub


    Private Sub txtsPasword_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtsPasword.TextChanged

    End Sub

    Private Sub cbEmpresa_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cbEmpresa.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            cmdOk.Focus()
            'cmdOk_Click(sender, e)
            e.Handled = True
        End If
    End Sub

    Private Sub cbEmpresa_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbEmpresa.SelectedIndexChanged

    End Sub

    Private Sub cmdOk_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs)
        'cmdOk_Click(sender, e)
    End Sub

    Private Sub fraSeleccionEmpresa_Enter(sender As Object, e As EventArgs) Handles fraSeleccionEmpresa.Enter

    End Sub
    Private Sub chkAutWindowsCOM_CheckedChanged(sender As Object, e As EventArgs) Handles chkAutWindowsCOM.CheckedChanged
        'If SalidaCOM Then Exit Sub
        'Salida = True
        If bAutWinCOM <> chkAutWindowsCOM.Checked Then
            'SalidaCOM = False
            ChecaValoresRegistro(chkAutWindows.Checked, chkAutWindowsCOM.Checked, "")
            llenaEmpresasFin()
            'SalidaCOM = True
        End If
        'Salida = False

    End Sub

    Private Sub llenaEmpresasFin()

        cbEmpresa.Items.Clear()
        Dim util As New CapaNegocio.Parametros


        'Dim CnStrEsp As String = CadenaConexion2(UCase(sNomServidor & "\compac"), "CompacWAdmin").ToString
        'Inicio.CnStrEsp = util.CreaConStrSQL(UCase(sNomServidor & "\compac"), "CompacWAdmin", True, "", "", True)
        'Inicio.CnStrEsp = util.CreaConStrSQL(UCase(sNomServidorCOM), "CompacWAdmin", True, sNomUsuarioSQLCOM, sPassUsuarioSQLCOM, True)

        BDCOMConf = New CapaDatos.UtilSQL(CnStrEsp, NomUser) 'Se inicializa la Clase BD

        StrSql = "SELECT CIDEMPRESA,CNOMBREEMPRESA,CRUTADATOS,CRUTARESPALDOS FROM Empresas where CIDEMPRESA > 1"

        DsConsulta = BDCOMConf.ExecuteReturn(StrSql)
        If DsConsulta.Rows.Count > 0 Then
            For i = 0 To DsConsulta.Rows.Count - 1
                myDataRow = MyTablaEmpresas.NewRow()
                myDataRow("EMPRESA") = Trim(DsConsulta.Rows(i).Item("CNOMBREEMPRESA"))
                myDataRow("RUTA") = Trim(DsConsulta.Rows(i).Item("CRUTADATOS"))
                myDataRow("IDEMPRESA") = DsConsulta.Rows(i).Item("CIDEMPRESA")
                MyTablaEmpresas.Rows.Add(myDataRow)

            Next
        End If

        cbEmpresa.DataSource = MyTablaEmpresas
        cbEmpresa.ValueMember = "RUTA"
        cbEmpresa.DisplayMember = "EMPRESA"

        If sRutaBdCOM <> "" Then
            cbEmpresa.SelectedValue = sRutaBdCOM
        End If

    End Sub
End Class
