﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FEspera
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Bar1 = New System.Windows.Forms.ProgressBar()
        Me.LStatus = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Bar1
        '
        Me.Bar1.Location = New System.Drawing.Point(10, 32)
        Me.Bar1.MarqueeAnimationSpeed = 10
        Me.Bar1.Name = "Bar1"
        Me.Bar1.Size = New System.Drawing.Size(450, 23)
        Me.Bar1.Style = System.Windows.Forms.ProgressBarStyle.Marquee
        Me.Bar1.TabIndex = 1
        '
        'LStatus
        '
        Me.LStatus.AutoSize = True
        Me.LStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LStatus.ForeColor = System.Drawing.Color.Black
        Me.LStatus.Location = New System.Drawing.Point(12, 9)
        Me.LStatus.Name = "LStatus"
        Me.LStatus.Size = New System.Drawing.Size(29, 20)
        Me.LStatus.TabIndex = 2
        Me.LStatus.Text = ". . ."
        '
        'FEspera
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(472, 67)
        Me.ControlBox = False
        Me.Controls.Add(Me.LStatus)
        Me.Controls.Add(Me.Bar1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "FEspera"
        Me.Text = "Espere un momento. . . ."
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Bar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents LStatus As System.Windows.Forms.Label
End Class
