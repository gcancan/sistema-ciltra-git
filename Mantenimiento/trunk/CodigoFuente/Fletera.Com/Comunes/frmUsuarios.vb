﻿Public Class frmUsuarios
    Dim DtMnuPermisos As New DataTable 'Para Saber si se pone el Alta,Baja,Modif,Reporte
    Dim OpcionForma As TipoOpcionForma
    Private TablaBd As String = "Usuarios", CampoLLave = "sUserId"
    Private UserGrupo As UsuarioClass
    Private _Tag As String
    Dim Existe As String
    Dim Salida As Boolean = False
    Dim DsComboFox As New DataSet
    Dim CIDCLIEN01 As Integer
    Dim sPosibleError As String
    Protected DS As New DataSet
    Private _NomTab As String

    Sub New(ByVal vTag As String)
        ' Llamada necesaria para el diseñador.
        InitializeComponent()
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        _Tag = vTag
    End Sub

    Private Sub ArbolPerm_AfterCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles ArbolPerm.AfterCheck
        If e.Node.Checked = False Then
            e.Node.ForeColor = Color.Red
            e.Node.BackColor = Color.White
            For i As Integer = 0 To e.Node.GetNodeCount(False) - 1
                If e.Node.Nodes(i).Checked Then e.Node.Nodes(i).Checked = False
            Next
        Else
            e.Node.ForeColor = Color.Black
            e.Node.BackColor = Color.GreenYellow
            If Not e.Node.Parent Is Nothing Then
                If e.Node.Parent.Checked = False Then e.Node.Parent.Checked = True
            End If
        End If
    End Sub
#Region "Guardar MenuPermisos"
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGrabaMnu.Click
        If MessageBox.Show("Esta completamente segurio que desea guardar el Menu Principal????",
                          "Actualizar Menu Principal???", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then Exit Sub
        ArbolPerm.Nodes.Clear()
        For j = 0 To FMenu.Menu.MenuItems.Count - 1
            With FMenu.Menu.MenuItems(j)
                If .Tag = "" Then Continue For
                With ArbolPerm.Nodes.Add(FMenu.Menu.MenuItems(j).Text) 'NIVELES PRINCIPALES!!!
                    .Tag = FMenu.Menu.MenuItems(j).Tag
                    .Checked = False
                    DibGuardPerm(FMenu.Menu.MenuItems(j), ArbolPerm.Nodes(j))
                End With
            End With
        Next 'PRIMERO DIBUJAA EL MENUUUU
        BD.Execute("DELETE FROM MnuPermisos")
        For i As Integer = 0 To ArbolPerm.Nodes.Count - 1
            If ArbolPerm.Nodes(i).GetNodeCount(False) > 0 Then
                Try
                    GuardaMenuPermisos(ArbolPerm.Nodes(i))
                Catch ex As Exception
                    MessageBox.Show("Error al tratar de guardar el Nodo: " & ArbolPerm.Nodes(i).Text & vbNewLine &
                                    ex.Message, "Error: " & Err.Number, MessageBoxButtons.OK, MessageBoxIcon.Error)
                End Try

            End If
        Next
        MessageBox.Show("Listo. . . .", "Accion terminada con exito!!!", MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub
    Private Sub GuardaMenuPermisos(ByVal vNodo As TreeNode)
        BD.Execute("INSERT INTO MnuPermisos(TAG,idTipoPer,NomForma) VALUES('" & _
                    vNodo.Tag & "','tSin','" & vNodo.Text & "')")
        'TextBox2.Text += vNodo.Text & vbNewLine
        For i As Integer = 0 To vNodo.GetNodeCount(False) - 1
            If vNodo.Nodes(i).GetNodeCount(False) > 0 Then
                GuardaMenuPermisos(vNodo.Nodes(i))
            Else
                'BD.Execute("INSERT INTO MnuPermisos(TAG,idTipoPer,NomForma) VALUES('" & _
                '    vNodo.Nodes(i).Tag & "','tPrinc','" & vNodo.Nodes(i).Text & "')")

                BD.Execute("INSERT INTO MnuPermisos(TAG,idTipoPer,NomForma) VALUES('" & _
                    vNodo.Nodes(i).Tag & "','tSin','" & vNodo.Nodes(i).Text & "')")

                'TextBox2.Text += vNodo.Nodes(i).Text & "*ABCR*" & vbNewLine
            End If
        Next
        'If vNodo.GetNodeCount(False) > 0 Then
        '    ListaElementosTre(vNodo.Nodes(0))
        'End If
    End Sub
    Private Sub DibGuardPerm(ByVal vMenu As MenuItem, ByVal vNodo As TreeNode)
        Dim ResMen As String = "", FilItm As Integer = 0
        For Each mycontrol As MenuItem In vMenu.MenuItems
            If mycontrol.IsParent Then
                If mycontrol.Tag = "" Then Continue For
                With vNodo.Nodes.Add(mycontrol.Text)
                    .Tag = mycontrol.Tag
                    .Checked = False
                    DibGuardPerm(mycontrol, vNodo.Nodes(FilItm))
                End With
            Else
                If mycontrol.Tag = "" Then Continue For
                With vNodo.Nodes.Add(mycontrol.Text)
                    .Tag = mycontrol.Tag
                    .Checked = False
                    'vNodo.Nodes(FilItm).Nodes.Add("Alta")
                    'vNodo.Nodes(FilItm).Nodes.Add("Baja")
                    'vNodo.Nodes(FilItm).Nodes.Add("Modif")
                    'vNodo.Nodes(FilItm).Nodes.Add("Reporte")
                End With
                FilItm += 1
            End If
        Next
    End Sub
#End Region
#Region "MenusToolStrip"
    Private Sub OpcionMnuTools(ByVal vOpcion As TipoOpcionForma)
        'LlenaCombos()
        If vOpcion <> TipoOpcionForma.tOpcImprimir And vOpcion <> TipoOpcionForma.tOpcEliminar Then
            OpcionForma = vOpcion
        End If
        txtMenuBusca.Enabled = True
        GridTodos.Enabled = True
        If vOpcion = TipoOpcionForma.tOpcInsertar Then
            LlenaCombos()
            'ALTAS
            If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Alta) = False Then

                If Not (UserId = KEY_CONFIG_USUARIO) Then

                    MessageBox.Show("No tiene los permisos Necesarios para poder dar de Alta en el Catalogo de Usuarios!!!",
                                    "Acceso Denegado a Alta de Usuarios", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Exit Sub
                End If
            End If
            comboMnu.SelectedIndex = 0
            For Each ct As Control In GrupoCaptura.Controls
                If Not ct Is txtidUsuario Then
                    ct.Enabled = False
                Else
                    ct.Enabled = True
                End If
                If TypeOf (ct) Is TextBox Then
                    TryCast(ct, TextBox).Text = ""
                End If
            Next
            btnMnuAltas.Enabled = False
            btnMnuModificar.Enabled = False
            btnMnuEliminar.Enabled = False
            btnMnuConsulta.Enabled = False

            'cmdOk.Enabled = False
            'cmdCancelar.Enabled = True

            btnMnuOk.Enabled = False
            btnMnuCancelar.Enabled = True

            ComboUserGrupo.Enabled = True
            ComboUserGrupo.SelectedIndex = 0
            txtMenuBusca.Enabled = False
            GridTodos.Enabled = False
            For i As Integer = 0 To ArbolPerm.Nodes.Count - 1
                ArbolPerm.Nodes(i).Checked = False 'Limpia el arbol de permisos
            Next
            txtidUsuario.Focus()
            Status("Ingrese nuevo Usuario para guardar", Me)
        ElseIf vOpcion = TipoOpcionForma.tOpcModificar Then
            'MODIFICAR
            'ToolStripMenu.Enabled = False
            If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Modificar) = False Then
                If Not (UserId = KEY_CONFIG_USUARIO) Then
                    MessageBox.Show("No tiene los permisos Necesarios para poder Modificar la Informacion del Usuario!!!",
                                    "Acceso Denegado a Modificacion de la Informacion del Usuario", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Exit Sub

                End If
            End If
            comboMnu.SelectedIndex = 1
            For Each ct As Control In GrupoCaptura.Controls
                If Not ct Is txtidUsuario Then
                    ct.Enabled = True
                Else
                    ct.Enabled = False
                End If
            Next
            If ComboUserGrupo.SelectedIndex = 1 Then 'Es grupo
                txtPass.Text = ""
                txtPass2.Text = ""
                txtPass.Enabled = False
                txtPass2.Enabled = False
                cmbGrupo.Text = ""
                cmbGrupo.Enabled = False
            End If
            btnMnuAltas.Enabled = False
            btnMnuModificar.Enabled = False
            btnMnuEliminar.Enabled = True
            btnMnuConsulta.Enabled = False
            ComboUserGrupo.Enabled = False

            'cmdCancelar.Enabled = True
            'cmdOk.Enabled = True
            btnMnuOk.Enabled = True
            btnMnuCancelar.Enabled = True


            If Trim(txtidUsuario.Text) = "" Then
                txtidUsuario.Text = DespliegaGrid(TablaBd, Inicio.CONSTR, CampoLLave)
                '''''''''''              CargaForm(txtidUsuario.Text)
            End If
            'cmbGrupo.Text = UserGrupo.Grupo.NombreGrupo
            txtNombreUsuario.Focus()
            Status("Formulario Preparado para modificar Usuarios", Me)
        ElseIf vOpcion = TipoOpcionForma.tOpcEliminar Then
            If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Baja) = False Then
                If Not (UserId = KEY_CONFIG_USUARIO) Then
                    MessageBox.Show("No tiene los permisos Necesarios para poder Eliminar Usuario!!!",
                                    "Acceso Denegado a Eliminacion de Usuario", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Exit Sub

                End If
            End If
            If txtidUsuario.Text = UserGrupo.idUsuario Then
                With UserGrupo
                    If MessageBox.Show("Esta completamente seguro que desea Eliminar el " & ComboUserGrupo.Text & " " &
                                               .NombreUsuario & " Con Id: " & .idUsuario & " ????",
                                               "Confirma que desea eliminar : el " & ComboUserGrupo.Text & " " & .NombreUsuario & "???",
                                             MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then Exit Sub
                    Try
                        .Eliminar() 'Elimina al Usuario o Grupo
                        'MessageBox.Show(ComboUserGrupo.Text & " " & .NombreUsuario & " Eliminado con exito!!!",
                        '              ComboUserGrupo.Text & " Eliminado con exito", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        Status(ComboUserGrupo.Text & " " & .NombreUsuario & " Eliminado con exito!!!", Me)
                        ActualizaGrid(txtMenuBusca.Text)
                        OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
                    Catch ex As Exception
                        Status("Error al tratar de eliminar el " & ComboUserGrupo.Text & ex.Message, Me, Err:=True)
                        MessageBox.Show("Error al Tratar de eliminar el " & ComboUserGrupo.Text & " " & .NombreUsuario & vbNewLine &
                                       "Error. " & ex.Message, "Error: " & Err.Number, MessageBoxButtons.OK, MessageBoxIcon.Error)

                    End Try
                End With
            End If
        ElseIf vOpcion = TipoOpcionForma.tOpcConsultar Then
            'CONSULTA
            comboMnu.SelectedIndex = 3
            For Each ct As Control In GrupoCaptura.Controls
                If Not ct Is cmdBuscaClave Then
                    ct.Enabled = False
                Else
                    ct.Enabled = True
                End If
            Next
            txtMenuBusca.Enabled = True
            btnMnuAltas.Enabled = True
            btnMnuModificar.Enabled = True
            btnMnuEliminar.Enabled = False
            btnMnuConsulta.Enabled = False
            btnMnuReporte.Enabled = True
            ComboUserGrupo.Enabled = False
            'cmdOk.Enabled = False
            'cmdCancelar.Enabled = False
            btnMnuOk.Enabled = False
            btnMnuCancelar.Enabled = False
            'ActivaCampos(False, TipoOpcActivaCampos.tOpcINICIALIZA)
            'ActivaBotones(True, TipoOpcActivaBoton.tOpcConsulta)
            'If Trim(txtsIdColonia.Text) = "" Then
            'txtsIdColonia.Text = DespliegaGrid(TablaBd, Inicio.CONSTR, CampoLLave)
            'End If
            Status("Formulario preparado para Consultar los Usuarios y/o Grupos Existentes!!!", Me)
        ElseIf vOpcion = TipoOpcionForma.tOpcImprimir Then
            'REPORTE
            If UserConectado.TieneAcceso(_Tag, UsuarioClass.enOpcionTag.Reporte) = False Then
                MessageBox.Show("No tiene los permisos Necesarios para poder realizar Reporte de Usuarios!!!",
                                "Acceso Denegado a Reportes de Usuarios", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Exit Sub
            End If
            ToolStripMenu.Enabled = False
            Me.Cursor = Cursors.WaitCursor
            'comboMnu.SelectedIndex = 4
            'DespliegaReporte(TablaBd, CONSTR)
            Me.Cursor = Cursors.Default
            ToolStripMenu.Enabled = True
        End If
    End Sub
    Private Sub btnMnuAltas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuAltas.Click
        'ALTAS
        OpcionMnuTools(TipoOpcionForma.tOpcInsertar)
    End Sub
    Private Sub btnMnuModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuModificar.Click
        'MODIFICAR
        OpcionMnuTools(TipoOpcionForma.tOpcModificar)
        'txtsIdColonia_Leave(sender, e)
        'txtsDescripcion.Enabled = False
    End Sub
    Private Sub btnMnuEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuEliminar.Click
        'ELIMINAR
        OpcionMnuTools(TipoOpcionForma.tOpcEliminar)
        'txtsIdColonia_Leave(sender, e)
        'txtsDescripcion.Enabled = False
    End Sub
    Private Sub btnMnuConsulta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuConsulta.Click
        'CONSULTA
        OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
        'txtsIdColonia_Leave(sender, e)
        'txtsNomColonia.Enabled = False
    End Sub
    Private Sub btnMnuReporte_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuReporte.Click
        'REPORTE
        OpcionMnuTools(TipoOpcionForma.tOpcImprimir)
    End Sub
#End Region
#Region "Dibuja Arbol Permisos Inicial"
    Private Sub DibujaArbolPermisos() 'Se copio en Clase Usuarios
        ArbolPerm.Nodes.Clear()
        For j = 0 To FMenu.Menu.MenuItems.Count - 1
            With FMenu.Menu.MenuItems(j)
                If .Tag = "" Then Continue For
                With ArbolPerm.Nodes.Add(FMenu.Menu.MenuItems(j).Text) 'NIVELES PRINCIPALES!!!
                    .Tag = FMenu.Menu.MenuItems(j).Tag
                    .Checked = False
                    GetNodos(FMenu.Menu.MenuItems(j), ArbolPerm.Nodes(j))
                End With
            End With
        Next
    End Sub
    Private Sub GetNodos(ByVal vMenu As MenuItem, ByVal vNodo As TreeNode) 'Se copio Clase Usuarios
        Dim ResMen As String = "", FilItm As Integer = 0
        For Each mycontrol As MenuItem In vMenu.MenuItems
            If mycontrol.IsParent Then
                If mycontrol.Tag = "" Then Continue For
                With vNodo.Nodes.Add(mycontrol.Text)
                    .Tag = mycontrol.Tag
                    .Checked = False
                    GetNodos(mycontrol, vNodo.Nodes(FilItm))
                End With
            Else
                If mycontrol.Tag = "" Then Continue For
                With vNodo.Nodes.Add(mycontrol.Text)
                    .Tag = mycontrol.Tag
                    .Checked = False
                    DtMnuPermisos.DefaultView.RowFilter = "TAG = '" & .Tag & "'"
                    DtMnuPermisos.DefaultView.Sort = "Opcion"
                    For i As Integer = 0 To DtMnuPermisos.DefaultView.Count - 1
                        With vNodo.Nodes(FilItm).Nodes.Add(DtMnuPermisos.DefaultView.Item(i).Item("NomOpcion"))
                            .tag = .parent.tag & "$" & DtMnuPermisos.DefaultView.Item(i).Item("Opcion")
                            .checked = False
                        End With
                    Next


                    'vNodo.Nodes(FilItm).Nodes.Add("Alta")
                    'vNodo.Nodes(FilItm).Nodes.Add("Baja")
                    'vNodo.Nodes(FilItm).Nodes.Add("Modif")
                    'vNodo.Nodes(FilItm).Nodes.Add("Reporte")
                End With
                FilItm += 1
            End If
        Next
    End Sub
#End Region
    
    Private Sub ActualizaGrid(ByVal vNombreUserGrupo As String)
        If vNombreUserGrupo = "" Then
            GridTodos.DataSource = BD.ExecuteReturn("SELECT (Case WHEN EsGrupo = 1 THEN sNomUser + ' (G)' ELSE sNomuser + ' (U)' END) Nombre,sUserId FROM " & TablaBd)
            Status(GridTodos.RowCount & " Resultados de Usuarios y/o Grupos", Me)
        Else
            GridTodos.DataSource = BD.ExecuteReturn("SELECT (Case WHEN EsGrupo = 1 THEN sNomUser + ' (G)' ELSE sNomuser + ' (U)' END) Nombre,sUserId FROM " & TablaBd &
                                                " WHERE sNomUser Like '%" & vNombreUserGrupo & "%'")
            Status(GridTodos.RowCount & " Resultados obtenidos con el filtro %" & vNombreUserGrupo & "%", Me)
        End If

    End Sub

    Private Sub LlenaCombos()
        Try
            'COMBO UBICACION
            'DsCombo.Clear()
            'DsCombo = Inicio.LlenaCombos("idDescripcion", "NomDescripcion", "Descripciones", _
            '                   TipoConexion.TcSQL, Inicio.CONSTR, Process.GetCurrentProcess.ProcessName, _
            '                   KEY_CHECAERROR, KEY_ESTATUS, " TIPO = 'TIPPRO'")
            'If Not DsCombo Is Nothing Then
            '    If DsCombo.Tables("Descripciones").DefaultView.Count > 0 Then
            '        cmbidTipo.DataSource = DsCombo.Tables("Descripciones")
            '        cmbidTipo.ValueMember = "idDescripcion"
            '        cmbidTipo.DisplayMember = "NomDescripcion"
            '    End If
            'End If

            cmbGrupo.DataSource = BD.ExecuteReturn("SELECT sUserId,sNomUser FROM " & TablaBd & " WHERE EsGrupo = '1'")
            cmbGrupo.DisplayMember = "sNomUser"
            cmbGrupo.ValueMember = "sUserId"
            'DsCombo = Inicio.LlenaCombos("idcobratario", "nombre", "c_Cobratarios", _
            '                   TipoConexion.TcSQL, Inicio.CONSTR, Process.GetCurrentProcess.ProcessName, _
            '                   KEY_CHECAERROR, KEY_ESTATUS, " idTipo = 'CAJ'")
            'If Not DsCombo Is Nothing Then
            '    If DsCombo.Tables("c_Cobratarios").DefaultView.Count > 0 Then
            '        cmbCajero.DataSource = DsCombo.Tables("c_Cobratarios")
            '        cmbCajero.ValueMember = "idcobratario"
            '        cmbCajero.DisplayMember = "nombre"
            '    End If
            'End If

            'cmbTodos
        Catch ex As Exception

        End Try
    End Sub

    Public Sub RecibeDatos(ByVal vNomTab As String)
        _NomTab = vNomTab
    End Sub

    Private Sub frmUsuarios_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LlenaCombos()

        If UserId = "SUPERVISOR" Then
            cmdGrabaMnu.Visible = True
        Else
            cmdGrabaMnu.Visible = False
        End If

        DtMnuPermisos = BD.ExecuteReturn("SELECT Mp.TAG, Mp.idTipoPer, Mp.NomForma, Tp.Opcion, Tp.NomOpcion " &
                               "FROM MnuPermisos Mp LEFT JOIN " &
                               "MnuTiposPermisos Tp ON Mp.idTipoPer = Tp.idTipoPer " &
                               "WHERE Tp.Opcion <> 0") 'Jala Todos los Permisos. . . al Cargar
        DibujaArbolPermisos()
        OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
        ActualizaGrid("")
        GridTodos.Columns(0).Width = GridTodos.Width - 20
        GridTodos.Columns(1).Visible = False
        txtMenuBusca.Focus()
    End Sub

    Private Sub txtMenuBusca_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles _
                    txtMenuBusca.KeyPress
        If Asc(e.KeyChar) = 39 Then e.Handled = True : Exit Sub 'Caracter ' Evita errores
        If Asc(e.KeyChar) = 13 Then
            GridTodos.Focus()
            e.Handled = True
        End If
    End Sub
    Private Sub txtMenuBusca_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles _
                    txtMenuBusca.KeyUp
        If txtMenuBusca.Text.Trim = "" Then Exit Sub
        If e.KeyCode = Keys.Down Or e.KeyCode = Keys.Up Then
            GridTodos.Focus()
            e.Handled = True
            Exit Sub
        End If
        If e.KeyCode = Keys.Left Or e.KeyCode = Keys.Right Then Exit Sub
        ActualizaGrid(txtMenuBusca.Text)
    End Sub
    Private Sub GridTodos_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles GridTodos.CellEnter
        With GridTodos
            If Not UserGrupo Is Nothing Then
                If UserGrupo.idUsuario = .Rows(.CurrentRow.Index).Cells(1).Value & "" Then
                    Exit Sub 'Para que no entre muchas veces!!!
                End If
            End If
            CargaForm(.Rows(.CurrentRow.Index).Cells(1).Value & "")
        End With
    End Sub
    Private Sub GridTodos_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles GridTodos.KeyPress
        If Asc(e.KeyChar) = 8 Then
            txtMenuBusca.Focus()
            e.Handled = True
        End If
    End Sub 'CONTROLES
    Private Sub txtsIdColonia_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles _
               txtidUsuario.KeyDown
        If e.KeyCode = Keys.F3 Then
            cmdBuscaClave_Click(cmdBuscaClave, e)
        End If
    End Sub
    Private Sub txtsIdColonia_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles _
                    txtidUsuario.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If txtidUsuario.Text.Trim <> "" Then
                CargaForm(txtidUsuario.Text)
            End If
            e.Handled = True
        End If
    End Sub
    Private Sub cmdBuscaClave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBuscaClave.Click
        Dim idUser As String = DespliegaGrid(TablaBd, Inicio.CONSTR, CampoLLave)
        CargaForm(idUser)
    End Sub
    Private Sub CargaForm(ByVal vidUsuario As String)
        UserGrupo = New UsuarioClass(vidUsuario)
        With UserGrupo 'Puede ser usuario o GRUPO!!! dependiendo del Bit EsGrupo
            txtidUsuario.Text = .idUsuario
            ComboUserGrupo.Enabled = False
            If .Existe Then
                If Not .EsGrupo Then 'NO ES GRUPO es un USUARIO NORMAL
                    ComboUserGrupo.SelectedIndex = 0
                    If OpcionForma = TipoOpcionForma.tOpcModificar Then
                        txtPass.Enabled = True
                        txtPass2.Enabled = True
                        cmbGrupo.Enabled = True
                    End If
                    LtitNomUser.Text = "Nombre Usuario:"
                    txtNombreUsuario.Text = .NombreUsuario
                    txtPass.Text = .Pass
                    txtPass2.Text = .Pass
                    If .Activo Then
                        rdbActivo.Checked = True
                    Else
                        rdbINActivo.Checked = True
                    End If
                    'If .AutorizaDescuento Then
                    '    chkAutorizaDescuento.Checked = True
                    'Else
                    '    chkAutorizaDescuento.Checked = False
                    'End If
                    If .Grupo.Existe Then
                        cmbGrupo.Text = .Grupo.NombreGrupo
                    Else
                        cmbGrupo.SelectedIndex = -1
                        cmbGrupo.Text = ""
                    End If
                    Status("Se Cargo el Usuario: " & .NombreUsuario, Me)
                Else 'ESGRUPO
                    ComboUserGrupo.SelectedIndex = 1
                    txtPass.Enabled = False
                    txtPass2.Enabled = False
                    cmbGrupo.Enabled = False
                    LtitNomUser.Text = "Nombre Grupo:"
                    cmbGrupo.Text = ""
                    txtPass.Text = ""
                    txtPass2.Text = ""
                    txtNombreUsuario.Text = .NombreUsuario
                    If .Activo Then
                        rdbActivo.Checked = True
                    Else
                        rdbINActivo.Checked = True
                    End If
                    'If .AutorizaDescuento Then
                    '    chkAutorizaDescuento.Checked = True
                    'Else
                    '    chkAutorizaDescuento.Checked = False
                    'End If

                    Status("Se Cargo el Grupo: " & .NombreUsuario, Me)
                End If
                For i As Integer = 0 To ArbolPerm.Nodes.Count - 1 'SE USA EN UsuarioClass
                    ArbolPerm.Nodes(i).Checked = False
                    CargaNodos(ArbolPerm.Nodes(i))
                Next

                If OpcionForma = TipoOpcionForma.tOpcInsertar Then
                    Status("El " & ComboUserGrupo.Text & " Con id: " & .idUsuario & " Ya existe!!!, Favor de modificarla para poder dar de alta a otro " & ComboUserGrupo.Text, Me, Err:=True)
                    MessageBox.Show("El " & ComboUserGrupo.Text & " con id: " & .idUsuario & " YA EXISTE!!!, Favor de Cambiarlo y volver a intentarlo",
                                    "El " & ComboUserGrupo.Text & " con id: " & .idUsuario & " YA EXISTE!!!", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End If
            Else 'No existe el usuario
                If OpcionForma = TipoOpcionForma.tOpcInsertar Then
                    For Each Ct As Control In GrupoCaptura.Controls
                        If Not Ct Is txtidUsuario And Not Ct Is ComboUserGrupo And Not Ct Is cmdBuscaClave Then
                            Ct.Enabled = True
                        Else
                            Ct.Enabled = False
                        End If
                        If TypeOf (Ct) Is TextBox Then
                            If Not Ct Is txtidUsuario Then
                                TryCast(Ct, TextBox).Text = ""
                            End If

                        End If
                    Next
                    'cmdOk.Enabled = True
                    btnMnuOk.Enabled = True
                    txtPass.Focus()
                    If ComboUserGrupo.SelectedIndex = 0 Then
                        txtPass.Enabled = True
                        txtPass2.Enabled = True
                        LtitNomUser.Text = "Nombre Usuario:"
                        cmbGrupo.Enabled = True
                    ElseIf ComboUserGrupo.SelectedIndex = 1 Then
                        txtPass.Enabled = False
                        txtPass2.Enabled = False
                        LtitNomUser.Text = "Nombre Grupo:"
                        cmbGrupo.Text = ""
                        cmbGrupo.Enabled = False
                    End If
                    Status("Listo para Guardar el nuevo " & ComboUserGrupo.Text & " con id: " & .idUsuario, Me)
                Else
                    Status("", Me)
                End If
            End If
        End With
    End Sub
    Private Sub CargaNodos(ByVal vNodo As TreeNode)
        If UserGrupo.TieneAccesoUltNodos(vNodo.Tag) Then
            vNodo.Checked = True
        End If
        For i As Integer = 0 To vNodo.GetNodeCount(False) - 1
            If vNodo.Nodes(i).GetNodeCount(False) > 0 Then
                CargaNodos(vNodo.Nodes(i))
            Else
                If UserGrupo.TieneAccesoUltNodos(vNodo.Nodes(i).Tag) Then
                    vNodo.Nodes(i).Checked = True
                End If
            End If
        Next
    End Sub
    Private Function BuscaUltimoCheked(ByVal vNodo As TreeNode) As List(Of String)
        Dim ResList As New List(Of String)
        'MessageBox.Show(vNodo.Text & vbNewLine)
        If vNodo.Checked Then
            For i As Integer = 0 To vNodo.GetNodeCount(False) - 1
                If vNodo.Nodes(i).GetNodeCount(False) > 0 Then
                    If vNodo.Nodes(i).Checked Then
                        ResList.AddRange(BuscaUltimoCheked(vNodo.Nodes(i)))
                    Else
                        If i = vNodo.GetNodeCount(False) - 1 Then 'Es el Ultimo Elemento
                            Dim BndAgre As Boolean = True
                            For j As Integer = 0 To vNodo.GetNodeCount(False) - 1
                                If vNodo.Nodes(j).Checked = True Then
                                    BndAgre = False
                                    Exit For
                                End If
                            Next
                            If BndAgre Then ResList.Add(vNodo.Nodes(i).Parent.Tag)
                            'If BndAgre Then ResList.Add("('" & Usuario.idUsuario & "','" & vNodo.Nodes(i).Parent.Tag & "','1')")
                        End If
                    End If
                Else
                    If vNodo.Nodes(i).Checked Then
                        ResList.Add(vNodo.Nodes(i).Tag)
                        'ResList.Add("('" & Usuario.idUsuario & "','" & vNodo.Nodes(i).Tag & "','1')")
                    Else
                        If i = vNodo.GetNodeCount(False) - 1 Then 'Es el Ultimo Elemento
                            Dim BndAgre As Boolean = True
                            For j As Integer = 0 To vNodo.GetNodeCount(False) - 1
                                If vNodo.Nodes(j).Checked = True Then
                                    BndAgre = False
                                    Exit For
                                End If
                            Next
                            If BndAgre Then ResList.Add(vNodo.Nodes(i).Parent.Tag)
                            'If BndAgre Then ResList.Add("('" & Usuario.idUsuario & "','" & vNodo.Nodes(i).Parent.Tag & "','1')")
                        End If
                    End If
                    'MessageBox.Show(vNodo.Nodes(i).Text & "*ABCR*")
                End If
            Next
        End If
        Return ResList
    End Function
    Private Sub cmdCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
    End Sub
    Private Function Validar() As Boolean
        If txtidUsuario.Text.Trim = "" Then
            MessageBox.Show("No se puede Guardar un " & ComboUserGrupo.Text & " que no Tenga id!!!, Favor de verificarlo",
                            "Error de campo vacio id" & ComboUserGrupo.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return False
        End If
        If txtPass.Text.Trim = "" And ComboUserGrupo.SelectedIndex = 0 Then
            MessageBox.Show("No se puede quedar Vacio el Campo de la Contraseña, Favor de Verificarlo!!!",
                            "Error de campo Vacio: Contraseña", MessageBoxButtons.OK, MessageBoxIcon.Error)
            txtPass.Focus()
            Return False
        End If
        If txtPass.Text <> txtPass2.Text And ComboUserGrupo.SelectedIndex = 0 Then
            MessageBox.Show("La confirmacion de la Contraseña y la Contraseña NO son iguales, Favor de verificarlo!!!",
                            "La verificacion de la contraseña y la Contraseña NO sin iguales", MessageBoxButtons.OK, MessageBoxIcon.Error)
            txtPass.Focus()
            Return False
        End If
        If txtNombreUsuario.Text.Trim = "" Then
            MessageBox.Show("No se puede quedar Vacio el campo de Nombre de " & ComboUserGrupo.Text & ", Favor de verificarlo",
                            "Error de campo Vacio: Nombre de " & ComboUserGrupo.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
            txtNombreUsuario.Focus()
            Return False
        End If
        If cmbGrupo.SelectedIndex < 0 And ComboUserGrupo.SelectedIndex = 0 Then
            If MessageBox.Show("El usuario no Pertenece a ningun grupo, Esta completamente seguro que desea Guardar este Usuario Solo Con Permisos personalizados???",
                               "Confirma que desea guardar Usuario sin un Grupo???", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then

                cmbGrupo.Focus()
                cmbGrupo.DroppedDown = True
                Return False
            End If
        End If
        Return True
    End Function
    Private Sub cmdOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim CadCam As String = ""
        If Validar() = False Then Exit Sub
        Try
            With UserGrupo
                If OpcionForma = TipoOpcionForma.tOpcModificar Then
                    If ComboUserGrupo.SelectedIndex = 1 Then 'Guarda Modificaciones GRUPO
                        CadCam = .GetCambiosGrupo(txtNombreUsuario.Text, rdbActivo.Checked, ArbolPerm)
                        If CadCam <> "" Then
                            'If MessageBox.Show("Esta completamente Seguro que Desea guardar los Cambios Realizados al Grupo Con id: " & .idUsuario & vbNewLine &
                            '           CadCam, "Confirma que desea guardar los Cambios Realizados???", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then Exit Sub

                            Status("Guardando modificaciones del Grupo Con Id: " & .idUsuario, Me, 1, 2)
                            .GuardaGrupo(txtNombreUsuario.Text, rdbActivo.Checked, ArbolPerm)
                            Status("Grupo con Id: " & .idUsuario & " Modificada con exito!!!", Me, 2, 2)
                            MessageBox.Show("Grupo con Id: " & .idUsuario & " Modificada con exito!!!",
                                                "Accion terminada con exito!!!", MessageBoxButtons.OK, MessageBoxIcon.Information)
                            ActualizaGrid(txtMenuBusca.Text)
                            OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
                        Else
                            Status("No se detectaron cambios para realizar al Grupo: " & .NombreUsuario & "!!!", Me, Err:=True)
                        End If
                    Else
                        CadCam = .GetCambiosUsuario(txtNombreUsuario.Text, txtPass.Text, cmbGrupo.SelectedValue, rdbActivo.Checked, ArbolPerm, chkAutorizaDescuento.Checked)
                        If CadCam <> "" Then
                            'If MessageBox.Show("Esta completamente Seguro que Desea guardar los Cambios Realizados al Usuario Con id: " & .idUsuario & vbNewLine &
                            '          CadCam, "Confirma que desea guardar los Cambios Realizados???", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then Exit Sub

                            Status("Guardando modificaciones del Usuario Con Id: " & .idUsuario, Me, 1, 2)
                            .GuardaUsuario(txtNombreUsuario.Text, txtPass.Text, cmbGrupo.SelectedValue, rdbActivo.Checked, ArbolPerm, chkAutorizaDescuento.Checked)
                            Status("Usuario con Id: " & .idUsuario & " Modificada con exito!!!", Me, 2, 2)
                            'MessageBox.Show("Usuario con Id: " & .idUsuario & " Modificada con exito!!!",
                            '                    "Accion terminada con exito!!!", MessageBoxButtons.OK, MessageBoxIcon.Information)
                            ActualizaGrid(txtMenuBusca.Text)
                            OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
                        Else
                            Status("No se detectaron cambios para realizarle al Usuario: " & .NombreUsuario, Me, Err:=True)
                        End If
                    End If
                ElseIf OpcionForma = TipoOpcionForma.tOpcInsertar Then
                    Status("Guardando Nuevo " & ComboUserGrupo.Text & ": " & txtNombreUsuario.Text, Me, 1, 2)
                    '.Guardar(, New PoblacionClass(txtsIdPoblacion.Text))
                    If ComboUserGrupo.SelectedIndex = 1 Then
                        .GuardaGrupo(txtNombreUsuario.Text, rdbActivo.Checked, ArbolPerm)
                    Else
                        .GuardaUsuario(txtNombreUsuario.Text, txtPass.Text, cmbGrupo.SelectedValue, rdbActivo.Checked, ArbolPerm, chkAutorizaDescuento.Checked)

                    End If
                    Status(ComboUserGrupo.Text & " " & .NombreUsuario & " Guardada con exito!!!", Me, 2, 2)
                    'MessageBox.Show("Nuevo " & ComboUserGrupo.Text & ": " & .NombreUsuario & " Guardada con exito!!!",
                    '                "Accion terminada con exito!!!", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
                    ActualizaGrid(txtMenuBusca.Text)
                End If
            End With
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error: " & Err.Number, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub cmbGrupo_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cmbGrupo.KeyDown
        If e.KeyCode = 13 Then
            If UserGrupo.Existe AndAlso UserGrupo.Grupo.Existe Then
                If UserGrupo.Grupo.idGrupo = cmbGrupo.SelectedValue Then
                    CargaTreeGrupo(UserGrupo.Grupo)
                Else
                    For i As Integer = 0 To ArbolPerm.Nodes.Count - 1
                        ArbolPerm.Nodes(i).Checked = False
                    Next
                End If
            Else
                CargaTreeGrupo(New GrupoClass(cmbGrupo.SelectedValue))
            End If
        End If
    End Sub
    Private Sub CargaTreeGrupo(ByVal vGrupo As GrupoClass)
        If vGrupo.Existe Then
            For i As Integer = 0 To ArbolPerm.Nodes.Count - 1 'SE USA EN UsuarioClass
                ArbolPerm.Nodes(i).Checked = False
                CargaNodosGrupo(ArbolPerm.Nodes(i), vGrupo)
            Next
        Else
            For i As Integer = 0 To ArbolPerm.Nodes.Count - 1 'SE USA EN UsuarioClass
                ArbolPerm.Nodes(i).Checked = False
            Next
        End If
    End Sub
    Private Sub CargaNodosGrupo(ByVal vNodo As TreeNode, ByVal vGrupo As GrupoClass)
        For i As Integer = 0 To vNodo.GetNodeCount(False) - 1
            If vNodo.Nodes(i).GetNodeCount(False) > 0 Then
                CargaNodosGrupo(vNodo.Nodes(i), vGrupo)
            Else
                If vGrupo.TieneAccesoUltNodos(vNodo.Nodes(i).Tag) Then
                    vNodo.Nodes(i).Checked = True
                End If
            End If
        Next
    End Sub
    Private Sub cmbGrupo_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cmbGrupo.Validating
        If cmbGrupo.SelectedIndex < 0 Then
            cmbGrupo.Text = "<SIN GRUPO>"
        End If
    End Sub

    Private Sub ArbolPerm_DrawNode(ByVal sender As Object, ByVal e As System.Windows.Forms.DrawTreeNodeEventArgs) Handles ArbolPerm.DrawNode
        If e.State = TreeNodeStates.Checked Then
            'e.Graphics.DrawImage(My.Resources.ImportarDisk, New Point(10, 10))
            'e.DrawDefault = False
            e.Graphics.DrawString(e.Node.Tag.ToString(), New Font("Arial", 12), _
              Brushes.Black, e.Bounds.Right + 2, e.Bounds.Top)
            'e.DrawDefault = True
        Else
            e.DrawDefault = True
        End If
    End Sub
    Private Sub txtPass_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles _
                    txtPass.KeyPress, txtPass2.KeyPress, txtNombreUsuario.KeyPress
        If Asc(e.KeyChar) = 13 Then
            SendKeys.Send("{TAB}")
            e.Handled = True
        End If
    End Sub
    Private Sub btngetPermG_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btngetPermG.Click
        If UserGrupo.Existe AndAlso UserGrupo.Grupo.Existe Then
            If UserGrupo.Grupo.idGrupo = cmbGrupo.SelectedValue Then
                CargaTreeGrupo(UserGrupo.Grupo)
              

            Else
                For i As Integer = 0 To ArbolPerm.Nodes.Count - 1
                    ArbolPerm.Nodes(i).Checked = False
                Next
                'CargaTreeGrupo(cmbGrupo.SelectedValue)
                CargaTreeGrupo(New GrupoClass(cmbGrupo.SelectedValue))
            End If
        Else
            CargaTreeGrupo(New GrupoClass(cmbGrupo.SelectedValue))
           
        End If
    End Sub

    Private Sub cmdTerminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Close()

    End Sub

    Private Sub GridTodos_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles GridTodos.CellContentClick

    End Sub

    Private Sub btnMnuOk_Click(sender As Object, e As EventArgs) Handles btnMnuOk.Click
        Dim CadCam As String = ""
        If Validar() = False Then Exit Sub
        Try
            With UserGrupo
                If OpcionForma = TipoOpcionForma.tOpcModificar Then
                    If ComboUserGrupo.SelectedIndex = 1 Then 'Guarda Modificaciones GRUPO
                        CadCam = .GetCambiosGrupo(txtNombreUsuario.Text, rdbActivo.Checked, ArbolPerm)
                        If CadCam <> "" Then
                            'If MessageBox.Show("Esta completamente Seguro que Desea guardar los Cambios Realizados al Grupo Con id: " & .idUsuario & vbNewLine &
                            '           CadCam, "Confirma que desea guardar los Cambios Realizados???", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then Exit Sub

                            Status("Guardando modificaciones del Grupo Con Id: " & .idUsuario, Me, 1, 2)
                            .GuardaGrupo(txtNombreUsuario.Text, rdbActivo.Checked, ArbolPerm)
                            Status("Grupo con Id: " & .idUsuario & " Modificada con exito!!!", Me, 2, 2)
                            MessageBox.Show("Grupo con Id: " & .idUsuario & " Modificada con exito!!!",
                                                "Accion terminada con exito!!!", MessageBoxButtons.OK, MessageBoxIcon.Information)
                            ActualizaGrid(txtMenuBusca.Text)
                            OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
                        Else
                            Status("No se detectaron cambios para realizar al Grupo: " & .NombreUsuario & "!!!", Me, Err:=True)
                        End If
                    Else
                        CadCam = .GetCambiosUsuario(txtNombreUsuario.Text, txtPass.Text, cmbGrupo.SelectedValue, rdbActivo.Checked, ArbolPerm, chkAutorizaDescuento.Checked)
                        If CadCam <> "" Then
                            'If MessageBox.Show("Esta completamente Seguro que Desea guardar los Cambios Realizados al Usuario Con id: " & .idUsuario & vbNewLine &
                            '          CadCam, "Confirma que desea guardar los Cambios Realizados???", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then Exit Sub

                            Status("Guardando modificaciones del Usuario Con Id: " & .idUsuario, Me, 1, 2)
                            .GuardaUsuario(txtNombreUsuario.Text, txtPass.Text, cmbGrupo.SelectedValue, rdbActivo.Checked, ArbolPerm, chkAutorizaDescuento.Checked)
                            Status("Usuario con Id: " & .idUsuario & " Modificada con exito!!!", Me, 2, 2)
                            'MessageBox.Show("Usuario con Id: " & .idUsuario & " Modificada con exito!!!",
                            '                    "Accion terminada con exito!!!", MessageBoxButtons.OK, MessageBoxIcon.Information)
                            ActualizaGrid(txtMenuBusca.Text)
                            OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
                        Else
                            Status("No se detectaron cambios para realizarle al Usuario: " & .NombreUsuario, Me, Err:=True)
                        End If
                    End If
                ElseIf OpcionForma = TipoOpcionForma.tOpcInsertar Then
                    Status("Guardando Nuevo " & ComboUserGrupo.Text & ": " & txtNombreUsuario.Text, Me, 1, 2)
                    '.Guardar(, New PoblacionClass(txtsIdPoblacion.Text))
                    If ComboUserGrupo.SelectedIndex = 1 Then
                        .GuardaGrupo(txtNombreUsuario.Text, rdbActivo.Checked, ArbolPerm)
                    Else
                        .GuardaUsuario(txtNombreUsuario.Text, txtPass.Text, cmbGrupo.SelectedValue, rdbActivo.Checked, ArbolPerm, chkAutorizaDescuento.Checked)

                    End If
                    Status(ComboUserGrupo.Text & " " & .NombreUsuario & " Guardada con exito!!!", Me, 2, 2)
                    'MessageBox.Show("Nuevo " & ComboUserGrupo.Text & ": " & .NombreUsuario & " Guardada con exito!!!",
                    '                "Accion terminada con exito!!!", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
                    ActualizaGrid(txtMenuBusca.Text)
                End If
            End With
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error: " & Err.Number, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub btnMnuCancelar_Click(sender As Object, e As EventArgs) Handles btnMnuCancelar.Click
        OpcionMnuTools(TipoOpcionForma.tOpcConsultar)
    End Sub

    Private Sub btnMnuSalir_Click(sender As Object, e As EventArgs) Handles btnMnuSalir.Click
        'If _NomTab <> "" Then
        '    FMenu.TabPrincipal.TabPages.RemoveByKey(_NomTab)
        'End If
        Me.Close()
    End Sub

   

    

    


    Private Function LlenaDatosFox(ByVal cCve As String, ByVal dbtabla As String, ByVal Campo As String, ByVal TipoCampo As TipoDato, _
                                  Optional ByVal cCve2 As String = "", Optional ByVal cCve3 As String = "") As String
        Dim Consulta As String = ""
        Dim Conexion As New OleDb.OleDbConnection(ConStrFox)
        Try
            Select Case UCase(dbtabla)
                Case UCase("MGW10002")
                    'Consulta = "select * from MGW10002 where CCODIGOC01 = '" & cCve & "'"
                    Consulta = "SELECT CLI.CIDCLIEN01, CLI.CCODIGOC01, CLI.CRAZONSO01,CLI.CDESCUEN02,CLI.CDIASCRE01, " & _
                    "(ALLTRIM(DIR.CNOMBREC01) + ' ' + ALLTRIM(DIR.CCOLONIA) + ' ' +  " & _
                    "ALLTRIM(DIR.CCIUDAD) + ' ' + ALLTRIM(DIR.CESTADO)  ) AS DIRECCION, DIR.CTELEFONO1, " & _
                    " DIR.CEMAIL, CLI.CRFC, CLI.CBANVENT01,CLI.CEMAIL1,CLI.CEMAIL2,CLI.CEMAIL3,cli.CMETODOPAG, cli.CNUMCTAPAG, " & _
                    "CLIMITEC01, CLIMDOCTOS, CBANEXCE01 " & _
                    "FROM  MGW10002 CLI " & _
                    "LEFT JOIN MGW10011 DIR ON DIR.CIDCATAL01 = CLI.CIDCLIEN01 AND DIR.CTIPOCAT01 = 1 WHERE " & Campo & " == '" & cCve & "'"
            End Select
            Dim Adt As New OleDb.OleDbDataAdapter(Consulta, Conexion)
            DsComboFox.Clear()
            Adt.Fill(DsComboFox, dbtabla)
            If Not DsComboFox Is Nothing Then
                If DsComboFox.Tables(dbtabla).DefaultView.Count > 0 Then
                    Select Case UCase(dbtabla)
                        Case UCase("MGW10002")
                            'txtCRAZONSO01.Text = Trim(DsComboFox.Tables(dbtabla).DefaultView.Item(0).Item("CRAZONSO01"))
                            CIDCLIEN01 = Trim(DsComboFox.Tables(dbtabla).DefaultView.Item(0).Item("CIDCLIEN01"))
                    End Select
                    LlenaDatosFox = KEY_RCORRECTO
                Else
                    LlenaDatosFox = KEY_RINCORRECTO
                End If
            Else
                LlenaDatosFox = KEY_RINCORRECTO
            End If
        Catch ex As Exception
            SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")
            LlenaDatosFox = KEY_RERROR
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, Me.Text)

        End Try
    End Function

    Private Function LlenaDatos(ByVal cCve As String, ByVal dbtabla As String, ByVal Campo As String, ByVal TipoCampo As TipoDato, _
                        ByVal Constr As String, Optional ByVal FiltroEsp As String = "", Optional ByVal Filtro2 As String = "", _
                        Optional ByVal cCve2 As String = "", Optional ByVal cCve3 As String = "") As String

        Dim strsql As String = ""
        Try
            Select Case UCase(dbtabla)
                Case UCase("CatClientes")
                    strsql = "select CIDCLIEN01,CCODIGOC01,CRAZONSO01,CRFC,DOMICILIO from CatClientes where CCODIGOC01 = '" & cCve & "'"
                    DS = Inicio.ChecaClavexSentencia(cCve, Constr, TipoConexion.TcSQL, dbtabla, "CCODIGOC01", TipoCampo, Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, strsql, FiltroEsp, , , True)

            End Select
            sPosibleError = GetSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")
            If sPosibleError <> "ERROR" Or sPosibleError = Nothing Then
                SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")
                If Not DS Is Nothing Then
                    If DS.Tables(dbtabla).DefaultView.Count > 0 Then
                        Select Case UCase(dbtabla)
                            Case UCase("CatClientes")
                                'txtCRAZONSO01.Text = DS.Tables(dbtabla).DefaultView.Item(0).Item("CRAZONSO01")
                                CIDCLIEN01 = DS.Tables(dbtabla).DefaultView.Item(0).Item("CIDCLIEN01")
                        End Select
                        LlenaDatos = KEY_RCORRECTO
                    Else
                        LlenaDatos = KEY_RINCORRECTO
                    End If
                Else
                    LlenaDatos = KEY_RINCORRECTO
                End If
            Else
                SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")
                LlenaDatos = KEY_RERROR
            End If
        Catch ex As Exception
            SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")
            LlenaDatos = KEY_RERROR
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, Me.Text)
        End Try
    End Function

    


   
    Private Sub txtCCODIGOC01_CLI_TextChanged(sender As Object, e As EventArgs)

    End Sub
End Class