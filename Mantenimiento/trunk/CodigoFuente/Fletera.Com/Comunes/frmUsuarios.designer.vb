﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmUsuarios
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmUsuarios))
        Me.ToolStripMenu = New System.Windows.Forms.ToolStrip()
        Me.txtMenuBusca = New System.Windows.Forms.ToolStripTextBox()
        Me.comboMnu = New System.Windows.Forms.ToolStripComboBox()
        Me.btnMnuCancelar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuSalir = New System.Windows.Forms.ToolStripButton()
        Me.GridTodos = New System.Windows.Forms.DataGridView()
        Me.GrupoCaptura = New System.Windows.Forms.GroupBox()
        Me.cmdGrabaMnu = New System.Windows.Forms.Button()
        Me.chkAutorizaDescuento = New System.Windows.Forms.CheckBox()
        Me.ArbolPerm = New System.Windows.Forms.TreeView()
        Me.btngetPermG = New System.Windows.Forms.Button()
        Me.ComboUserGrupo = New System.Windows.Forms.ComboBox()
        Me.txtPass2 = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.rdbINActivo = New System.Windows.Forms.RadioButton()
        Me.rdbActivo = New System.Windows.Forms.RadioButton()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtPass = New System.Windows.Forms.TextBox()
        Me.LtitNomUser = New System.Windows.Forms.Label()
        Me.cmbGrupo = New System.Windows.Forms.ComboBox()
        Me.txtidUsuario = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtNombreUsuario = New System.Windows.Forms.TextBox()
        Me.MeStatus1 = New ControlStatus.MeStatus()
        Me.cmdBuscaClave = New System.Windows.Forms.Button()
        Me.btnMnuAltas = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuModificar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuEliminar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuConsulta = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuReporte = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuOk = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripMenu.SuspendLayout()
        CType(Me.GridTodos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GrupoCaptura.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'ToolStripMenu
        '
        Me.ToolStripMenu.AutoSize = False
        Me.ToolStripMenu.BackColor = System.Drawing.Color.Silver
        Me.ToolStripMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.txtMenuBusca, Me.comboMnu, Me.btnMnuAltas, Me.btnMnuModificar, Me.btnMnuEliminar, Me.btnMnuConsulta, Me.btnMnuReporte, Me.btnMnuOk, Me.btnMnuCancelar, Me.btnMnuSalir})
        Me.ToolStripMenu.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow
        Me.ToolStripMenu.Location = New System.Drawing.Point(0, 0)
        Me.ToolStripMenu.Name = "ToolStripMenu"
        Me.ToolStripMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.ToolStripMenu.Size = New System.Drawing.Size(785, 42)
        Me.ToolStripMenu.TabIndex = 1
        Me.ToolStripMenu.Text = "ToolStrip1"
        '
        'txtMenuBusca
        '
        Me.txtMenuBusca.Name = "txtMenuBusca"
        Me.txtMenuBusca.Size = New System.Drawing.Size(150, 42)
        Me.txtMenuBusca.ToolTipText = "Filtra Listado"
        '
        'comboMnu
        '
        Me.comboMnu.AutoSize = False
        Me.comboMnu.BackColor = System.Drawing.Color.Silver
        Me.comboMnu.Enabled = False
        Me.comboMnu.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.comboMnu.ForeColor = System.Drawing.Color.Yellow
        Me.comboMnu.Items.AddRange(New Object() {"ALTAS", "MODIFICAR", "ELIMINAR", "CONSULTA", "REPORTE"})
        Me.comboMnu.MergeIndex = 0
        Me.comboMnu.Name = "comboMnu"
        Me.comboMnu.Size = New System.Drawing.Size(110, 25)
        Me.comboMnu.ToolTipText = "Dale Click a los Botones para Escoger una Opcion"
        '
        'btnMnuCancelar
        '
        Me.btnMnuCancelar.ForeColor = System.Drawing.Color.Red
        'Me.btnMnuCancelar.Image = Global.TraspasosAdmPAQ.My.Resources.Resources.signos_f_059
        Me.btnMnuCancelar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuCancelar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuCancelar.Name = "btnMnuCancelar"
        Me.btnMnuCancelar.Size = New System.Drawing.Size(71, 39)
        Me.btnMnuCancelar.Text = "&CANCELAR"
        Me.btnMnuCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuSalir
        '
        Me.btnMnuSalir.ForeColor = System.Drawing.Color.Red
        'Me.btnMnuSalir.Image = Global.TraspasosAdmPAQ.My.Resources.Resources.Salida2
        Me.btnMnuSalir.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuSalir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuSalir.Name = "btnMnuSalir"
        Me.btnMnuSalir.Size = New System.Drawing.Size(41, 39)
        Me.btnMnuSalir.Text = "&SALIR"
        Me.btnMnuSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'GridTodos
        '
        Me.GridTodos.AllowUserToAddRows = False
        Me.GridTodos.AllowUserToDeleteRows = False
        Me.GridTodos.AllowUserToResizeRows = False
        Me.GridTodos.BackgroundColor = System.Drawing.Color.White
        Me.GridTodos.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.GridTodos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.GridTodos.Location = New System.Drawing.Point(0, 45)
        Me.GridTodos.MultiSelect = False
        Me.GridTodos.Name = "GridTodos"
        Me.GridTodos.ReadOnly = True
        Me.GridTodos.RowHeadersVisible = False
        Me.GridTodos.Size = New System.Drawing.Size(162, 498)
        Me.GridTodos.TabIndex = 70
        '
        'GrupoCaptura
        '
        Me.GrupoCaptura.Controls.Add(Me.cmdGrabaMnu)
        Me.GrupoCaptura.Controls.Add(Me.chkAutorizaDescuento)
        Me.GrupoCaptura.Controls.Add(Me.ArbolPerm)
        Me.GrupoCaptura.Controls.Add(Me.btngetPermG)
        Me.GrupoCaptura.Controls.Add(Me.ComboUserGrupo)
        Me.GrupoCaptura.Controls.Add(Me.txtPass2)
        Me.GrupoCaptura.Controls.Add(Me.Label5)
        Me.GrupoCaptura.Controls.Add(Me.Panel1)
        Me.GrupoCaptura.Controls.Add(Me.Label19)
        Me.GrupoCaptura.Controls.Add(Me.Label3)
        Me.GrupoCaptura.Controls.Add(Me.txtPass)
        Me.GrupoCaptura.Controls.Add(Me.LtitNomUser)
        Me.GrupoCaptura.Controls.Add(Me.cmbGrupo)
        Me.GrupoCaptura.Controls.Add(Me.txtidUsuario)
        Me.GrupoCaptura.Controls.Add(Me.Label6)
        Me.GrupoCaptura.Controls.Add(Me.txtNombreUsuario)
        Me.GrupoCaptura.Controls.Add(Me.cmdBuscaClave)
        Me.GrupoCaptura.Location = New System.Drawing.Point(168, 45)
        Me.GrupoCaptura.Name = "GrupoCaptura"
        Me.GrupoCaptura.Size = New System.Drawing.Size(521, 498)
        Me.GrupoCaptura.TabIndex = 73
        Me.GrupoCaptura.TabStop = False
        '
        'cmdGrabaMnu
        '
        Me.cmdGrabaMnu.ForeColor = System.Drawing.Color.Black
        Me.cmdGrabaMnu.Location = New System.Drawing.Point(459, 133)
        Me.cmdGrabaMnu.Name = "cmdGrabaMnu"
        Me.cmdGrabaMnu.Size = New System.Drawing.Size(56, 56)
        Me.cmdGrabaMnu.TabIndex = 75
        Me.cmdGrabaMnu.Text = "Graba MenuPermisos"
        Me.cmdGrabaMnu.UseVisualStyleBackColor = True
        Me.cmdGrabaMnu.Visible = False
        '
        'chkAutorizaDescuento
        '
        Me.chkAutorizaDescuento.AutoSize = True
        Me.chkAutorizaDescuento.Location = New System.Drawing.Point(6, 178)
        Me.chkAutorizaDescuento.Name = "chkAutorizaDescuento"
        Me.chkAutorizaDescuento.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.chkAutorizaDescuento.Size = New System.Drawing.Size(119, 17)
        Me.chkAutorizaDescuento.TabIndex = 73
        Me.chkAutorizaDescuento.Text = "Autoriza Descuento"
        Me.chkAutorizaDescuento.UseVisualStyleBackColor = True
        Me.chkAutorizaDescuento.Visible = False
        '
        'ArbolPerm
        '
        Me.ArbolPerm.CheckBoxes = True
        Me.ArbolPerm.DrawMode = System.Windows.Forms.TreeViewDrawMode.OwnerDrawAll
        Me.ArbolPerm.Location = New System.Drawing.Point(13, 201)
        Me.ArbolPerm.Name = "ArbolPerm"
        Me.ArbolPerm.Size = New System.Drawing.Size(502, 291)
        Me.ArbolPerm.TabIndex = 72
        '
        'btngetPermG
        '
        Me.btngetPermG.FlatAppearance.BorderSize = 0
        Me.btngetPermG.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.btngetPermG.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.btngetPermG.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btngetPermG.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btngetPermG.Location = New System.Drawing.Point(283, 151)
        Me.btngetPermG.Name = "btngetPermG"
        Me.btngetPermG.Size = New System.Drawing.Size(30, 21)
        Me.btngetPermG.TabIndex = 71
        Me.btngetPermG.Text = "->"
        Me.btngetPermG.UseVisualStyleBackColor = True
        '
        'ComboUserGrupo
        '
        Me.ComboUserGrupo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboUserGrupo.FormattingEnabled = True
        Me.ComboUserGrupo.Items.AddRange(New Object() {"Usuario:", "Grupo:"})
        Me.ComboUserGrupo.Location = New System.Drawing.Point(12, 24)
        Me.ComboUserGrupo.Name = "ComboUserGrupo"
        Me.ComboUserGrupo.Size = New System.Drawing.Size(95, 21)
        Me.ComboUserGrupo.TabIndex = 0
        '
        'txtPass2
        '
        Me.txtPass2.Enabled = False
        Me.txtPass2.Location = New System.Drawing.Point(113, 74)
        Me.txtPass2.MaxLength = 40
        Me.txtPass2.Name = "txtPass2"
        Me.txtPass2.Size = New System.Drawing.Size(176, 20)
        Me.txtPass2.TabIndex = 3
        Me.txtPass2.UseSystemPasswordChar = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.ForeColor = System.Drawing.Color.Black
        Me.Label5.Location = New System.Drawing.Point(9, 77)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(98, 13)
        Me.Label5.TabIndex = 70
        Me.Label5.Text = "Repite Contraseña:"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.rdbINActivo)
        Me.Panel1.Controls.Add(Me.rdbActivo)
        Me.Panel1.Location = New System.Drawing.Point(113, 124)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(152, 22)
        Me.Panel1.TabIndex = 5
        '
        'rdbINActivo
        '
        Me.rdbINActivo.AutoSize = True
        Me.rdbINActivo.Location = New System.Drawing.Point(72, 4)
        Me.rdbINActivo.Name = "rdbINActivo"
        Me.rdbINActivo.Size = New System.Drawing.Size(75, 17)
        Me.rdbINActivo.TabIndex = 7
        Me.rdbINActivo.TabStop = True
        Me.rdbINActivo.Text = "INACTIVO"
        Me.rdbINActivo.UseVisualStyleBackColor = True
        '
        'rdbActivo
        '
        Me.rdbActivo.AutoSize = True
        Me.rdbActivo.Location = New System.Drawing.Point(6, 4)
        Me.rdbActivo.Name = "rdbActivo"
        Me.rdbActivo.Size = New System.Drawing.Size(64, 17)
        Me.rdbActivo.TabIndex = 6
        Me.rdbActivo.TabStop = True
        Me.rdbActivo.Text = "ACTIVO"
        Me.rdbActivo.UseVisualStyleBackColor = True
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.ForeColor = System.Drawing.Color.Black
        Me.Label19.Location = New System.Drawing.Point(10, 124)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(45, 13)
        Me.Label19.TabIndex = 67
        Me.Label19.Text = "Estatus:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ForeColor = System.Drawing.Color.Black
        Me.Label3.Location = New System.Drawing.Point(9, 52)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(64, 13)
        Me.Label3.TabIndex = 65
        Me.Label3.Text = "Contraseña:"
        '
        'txtPass
        '
        Me.txtPass.Enabled = False
        Me.txtPass.Location = New System.Drawing.Point(113, 49)
        Me.txtPass.MaxLength = 40
        Me.txtPass.Name = "txtPass"
        Me.txtPass.Size = New System.Drawing.Size(176, 20)
        Me.txtPass.TabIndex = 2
        Me.txtPass.UseSystemPasswordChar = True
        '
        'LtitNomUser
        '
        Me.LtitNomUser.AutoSize = True
        Me.LtitNomUser.ForeColor = System.Drawing.Color.Black
        Me.LtitNomUser.Location = New System.Drawing.Point(9, 98)
        Me.LtitNomUser.Name = "LtitNomUser"
        Me.LtitNomUser.Size = New System.Drawing.Size(86, 13)
        Me.LtitNomUser.TabIndex = 16
        Me.LtitNomUser.Text = "Nombre Usuario:"
        '
        'cmbGrupo
        '
        Me.cmbGrupo.AutoCompleteCustomSource.AddRange(New String() {"Regularización", "Vivienda"})
        Me.cmbGrupo.Enabled = False
        Me.cmbGrupo.FormattingEnabled = True
        Me.cmbGrupo.Location = New System.Drawing.Point(113, 151)
        Me.cmbGrupo.Name = "cmbGrupo"
        Me.cmbGrupo.Size = New System.Drawing.Size(168, 21)
        Me.cmbGrupo.TabIndex = 8
        '
        'txtidUsuario
        '
        Me.txtidUsuario.BackColor = System.Drawing.SystemColors.Info
        Me.txtidUsuario.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtidUsuario.Location = New System.Drawing.Point(113, 25)
        Me.txtidUsuario.MaxLength = 15
        Me.txtidUsuario.Name = "txtidUsuario"
        Me.txtidUsuario.Size = New System.Drawing.Size(176, 20)
        Me.txtidUsuario.TabIndex = 1
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.ForeColor = System.Drawing.Color.Black
        Me.Label6.Location = New System.Drawing.Point(8, 154)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(39, 13)
        Me.Label6.TabIndex = 63
        Me.Label6.Text = "Grupo:"
        '
        'txtNombreUsuario
        '
        Me.txtNombreUsuario.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNombreUsuario.Location = New System.Drawing.Point(113, 98)
        Me.txtNombreUsuario.MaxLength = 50
        Me.txtNombreUsuario.Name = "txtNombreUsuario"
        Me.txtNombreUsuario.Size = New System.Drawing.Size(214, 20)
        Me.txtNombreUsuario.TabIndex = 4
        '
        'MeStatus1
        '
        Me.MeStatus1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.MeStatus1.Location = New System.Drawing.Point(0, 550)
        Me.MeStatus1.Margin = New System.Windows.Forms.Padding(4)
        Me.MeStatus1.Name = "MeStatus1"
        Me.MeStatus1.Size = New System.Drawing.Size(785, 26)
        Me.MeStatus1.TabIndex = 71
        '
        'cmdBuscaClave
        '
        Me.cmdBuscaClave.Image = CType(resources.GetObject("cmdBuscaClave.Image"), System.Drawing.Image)
        Me.cmdBuscaClave.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdBuscaClave.Location = New System.Drawing.Point(295, 19)
        Me.cmdBuscaClave.Name = "cmdBuscaClave"
        Me.cmdBuscaClave.Size = New System.Drawing.Size(32, 32)
        Me.cmdBuscaClave.TabIndex = 5
        Me.cmdBuscaClave.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'btnMnuAltas
        '
        Me.btnMnuAltas.ForeColor = System.Drawing.Color.Red
        Me.btnMnuAltas.Image = CType(resources.GetObject("btnMnuAltas.Image"), System.Drawing.Image)
        Me.btnMnuAltas.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuAltas.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuAltas.Name = "btnMnuAltas"
        Me.btnMnuAltas.Size = New System.Drawing.Size(46, 39)
        Me.btnMnuAltas.Text = "&ALTAS"
        Me.btnMnuAltas.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnMnuAltas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuModificar
        '
        Me.btnMnuModificar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuModificar.Image = CType(resources.GetObject("btnMnuModificar.Image"), System.Drawing.Image)
        Me.btnMnuModificar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuModificar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuModificar.Name = "btnMnuModificar"
        Me.btnMnuModificar.Size = New System.Drawing.Size(74, 39)
        Me.btnMnuModificar.Text = "&MODIFICAR"
        Me.btnMnuModificar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnMnuModificar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuEliminar
        '
        Me.btnMnuEliminar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuEliminar.Image = CType(resources.GetObject("btnMnuEliminar.Image"), System.Drawing.Image)
        Me.btnMnuEliminar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuEliminar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuEliminar.Name = "btnMnuEliminar"
        Me.btnMnuEliminar.Size = New System.Drawing.Size(64, 39)
        Me.btnMnuEliminar.Text = "&ELIMINAR"
        Me.btnMnuEliminar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuConsulta
        '
        Me.btnMnuConsulta.ForeColor = System.Drawing.Color.Red
        Me.btnMnuConsulta.Image = CType(resources.GetObject("btnMnuConsulta.Image"), System.Drawing.Image)
        Me.btnMnuConsulta.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuConsulta.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuConsulta.Name = "btnMnuConsulta"
        Me.btnMnuConsulta.Size = New System.Drawing.Size(72, 39)
        Me.btnMnuConsulta.Text = "C&ONSULTA"
        Me.btnMnuConsulta.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuReporte
        '
        Me.btnMnuReporte.ForeColor = System.Drawing.Color.Red
        Me.btnMnuReporte.Image = CType(resources.GetObject("btnMnuReporte.Image"), System.Drawing.Image)
        Me.btnMnuReporte.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuReporte.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuReporte.Name = "btnMnuReporte"
        Me.btnMnuReporte.Size = New System.Drawing.Size(60, 39)
        Me.btnMnuReporte.Text = "&REPORTE"
        Me.btnMnuReporte.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuOk
        '
        Me.btnMnuOk.ForeColor = System.Drawing.Color.Red
        'Me.btnMnuOk.Image = Global.TraspasosAdmPAQ.My.Resources.Resources.Save1
        Me.btnMnuOk.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuOk.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuOk.Name = "btnMnuOk"
        Me.btnMnuOk.Size = New System.Drawing.Size(56, 39)
        Me.btnMnuOk.Text = "&GRABAR"
        Me.btnMnuOk.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'frmUsuarios
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(785, 576)
        Me.Controls.Add(Me.GrupoCaptura)
        Me.Controls.Add(Me.MeStatus1)
        Me.Controls.Add(Me.GridTodos)
        Me.Controls.Add(Me.ToolStripMenu)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmUsuarios"
        Me.Text = "Catalogo de Usuarios"
        Me.ToolStripMenu.ResumeLayout(False)
        Me.ToolStripMenu.PerformLayout()
        CType(Me.GridTodos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GrupoCaptura.ResumeLayout(False)
        Me.GrupoCaptura.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ToolStripMenu As System.Windows.Forms.ToolStrip
    Friend WithEvents txtMenuBusca As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents comboMnu As System.Windows.Forms.ToolStripComboBox
    Friend WithEvents btnMnuAltas As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuModificar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuEliminar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuConsulta As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuReporte As System.Windows.Forms.ToolStripButton
    Friend WithEvents GridTodos As System.Windows.Forms.DataGridView
    Public WithEvents MeStatus1 As ControlStatus.MeStatus
    Friend WithEvents GrupoCaptura As System.Windows.Forms.GroupBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents rdbINActivo As System.Windows.Forms.RadioButton
    Friend WithEvents rdbActivo As System.Windows.Forms.RadioButton
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtPass As System.Windows.Forms.TextBox
    Friend WithEvents LtitNomUser As System.Windows.Forms.Label
    Friend WithEvents cmbGrupo As System.Windows.Forms.ComboBox
    Friend WithEvents txtidUsuario As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtNombreUsuario As System.Windows.Forms.TextBox
    Friend WithEvents cmdBuscaClave As System.Windows.Forms.Button
    Friend WithEvents cmdGrabaMnu As System.Windows.Forms.Button
    Friend WithEvents txtPass2 As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents ComboUserGrupo As System.Windows.Forms.ComboBox
    Friend WithEvents btngetPermG As System.Windows.Forms.Button
    Friend WithEvents ArbolPerm As System.Windows.Forms.TreeView
    Friend WithEvents chkAutorizaDescuento As System.Windows.Forms.CheckBox
    Friend WithEvents btnMnuOk As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuCancelar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuSalir As System.Windows.Forms.ToolStripButton
End Class
