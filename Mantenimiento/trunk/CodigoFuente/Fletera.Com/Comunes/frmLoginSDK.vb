'Realizo: Roger Gala
'Fecha: 4 / Junio / 2009
'Fecha Mod: 22 / Abril / 2013
Imports System.IO
Imports System.Text
Imports Microsoft.Win32
Imports SDK_VC2010
Imports System.Data.SqlClient
Imports CapaDatos

Public Class frmLoginSDK
    Inherits System.Windows.Forms.Form

    Dim myDataColumn As DataColumn
    Public MyTablaEmpresas As DataTable = New DataTable(TablaBd)
    Dim TablaBd As String = "EmpresasAdminPaq"
    Friend WithEvents cmdTerminar As System.Windows.Forms.Button
    Friend WithEvents cmdOk As System.Windows.Forms.Button
    Dim myDataRow As DataRow
    Dim BandExito As Boolean = False


    Dim DsConsulta As New DataTable
    Dim StrSql As String = ""

    Public vRuta As String = ""
    Public vCatalog As String = ""
    'Dim _initialCatalog As String

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    Public Sub New(bDCOM As UtilSQL, BDCOMConfig As UtilSQL)
        ', vInitialCatalog As String
        Me.bDCOM = bDCOM
        Me.BDCOMConfig = BDCOMConfig
        '_initialCatalog = vInitialCatalog
        InitializeComponent()
    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Public WithEvents MeStatus1 As ControlStatus.MeStatus
    Public WithEvents fraSeleccionEmpresa As System.Windows.Forms.GroupBox
    Public WithEvents cbEmpresa As System.Windows.Forms.ComboBox
    Public WithEvents imgLogoCompac As System.Windows.Forms.PictureBox
    Private bDCOM As UtilSQL
    Private BDCOMConfig As UtilSQL

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLoginSDK))
        Me.MeStatus1 = New ControlStatus.MeStatus()
        Me.fraSeleccionEmpresa = New System.Windows.Forms.GroupBox()
        Me.cmdTerminar = New System.Windows.Forms.Button()
        Me.cmdOk = New System.Windows.Forms.Button()
        Me.imgLogoCompac = New System.Windows.Forms.PictureBox()
        Me.cbEmpresa = New System.Windows.Forms.ComboBox()
        Me.fraSeleccionEmpresa.SuspendLayout()
        CType(Me.imgLogoCompac, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MeStatus1
        '
        Me.MeStatus1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.MeStatus1.Location = New System.Drawing.Point(0, 136)
        Me.MeStatus1.Margin = New System.Windows.Forms.Padding(4)
        Me.MeStatus1.Name = "MeStatus1"
        Me.MeStatus1.Size = New System.Drawing.Size(568, 27)
        Me.MeStatus1.TabIndex = 21
        '
        'fraSeleccionEmpresa
        '
        Me.fraSeleccionEmpresa.BackColor = System.Drawing.SystemColors.Control
        Me.fraSeleccionEmpresa.Controls.Add(Me.cmdTerminar)
        Me.fraSeleccionEmpresa.Controls.Add(Me.cmdOk)
        Me.fraSeleccionEmpresa.Controls.Add(Me.imgLogoCompac)
        Me.fraSeleccionEmpresa.Controls.Add(Me.cbEmpresa)
        Me.fraSeleccionEmpresa.Font = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.fraSeleccionEmpresa.ForeColor = System.Drawing.SystemColors.Highlight
        Me.fraSeleccionEmpresa.Location = New System.Drawing.Point(9, 12)
        Me.fraSeleccionEmpresa.Name = "fraSeleccionEmpresa"
        Me.fraSeleccionEmpresa.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraSeleccionEmpresa.Size = New System.Drawing.Size(550, 125)
        Me.fraSeleccionEmpresa.TabIndex = 25
        Me.fraSeleccionEmpresa.TabStop = False
        '
        'cmdTerminar
        '
        Me.cmdTerminar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdTerminar.Image = CType(resources.GetObject("cmdTerminar.Image"), System.Drawing.Image)
        Me.cmdTerminar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdTerminar.Location = New System.Drawing.Point(456, 61)
        Me.cmdTerminar.Name = "cmdTerminar"
        Me.cmdTerminar.Size = New System.Drawing.Size(88, 56)
        Me.cmdTerminar.TabIndex = 86
        Me.cmdTerminar.Text = "&Terminar"
        Me.cmdTerminar.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cmdOk
        '
        Me.cmdOk.Image = CType(resources.GetObject("cmdOk.Image"), System.Drawing.Image)
        Me.cmdOk.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdOk.Location = New System.Drawing.Point(351, 61)
        Me.cmdOk.Name = "cmdOk"
        Me.cmdOk.Size = New System.Drawing.Size(88, 56)
        Me.cmdOk.TabIndex = 85
        Me.cmdOk.Text = "&Ok"
        Me.cmdOk.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'imgLogoCompac
        '
        Me.imgLogoCompac.Cursor = System.Windows.Forms.Cursors.Default
        Me.imgLogoCompac.Image = Global.Fletera.My.Resources.Resources.logo_ciltra_transparente1
        Me.imgLogoCompac.Location = New System.Drawing.Point(6, 15)
        Me.imgLogoCompac.Name = "imgLogoCompac"
        Me.imgLogoCompac.Size = New System.Drawing.Size(120, 102)
        Me.imgLogoCompac.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.imgLogoCompac.TabIndex = 83
        Me.imgLogoCompac.TabStop = False
        '
        'cbEmpresa
        '
        Me.cbEmpresa.BackColor = System.Drawing.SystemColors.Window
        Me.cbEmpresa.Cursor = System.Windows.Forms.Cursors.Default
        Me.cbEmpresa.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cbEmpresa.Location = New System.Drawing.Point(158, 15)
        Me.cbEmpresa.Name = "cbEmpresa"
        Me.cbEmpresa.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cbEmpresa.Size = New System.Drawing.Size(386, 25)
        Me.cbEmpresa.TabIndex = 3
        '
        'frmLoginSDK
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(568, 163)
        Me.Controls.Add(Me.fraSeleccionEmpresa)
        Me.Controls.Add(Me.MeStatus1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmLoginSDK"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Acceso a CONTPAQ i� COMERCIAL"
        Me.fraSeleccionEmpresa.ResumeLayout(False)
        CType(Me.imgLogoCompac, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region


    Private Sub frmLoginSDK_FormClosed(sender As Object, e As FormClosedEventArgs) Handles Me.FormClosed
        If Not BandExito Then
            If bEmpresaSDKAbierta Then
                fCierraEmpresa()
                bEmpresaSDKAbierta = False
            End If
            If bActivoSDK Then
                fTerminaSDK()
                bActivoSDK = False
            End If
        End If
    End Sub

    Private Sub cmdOk_Click(sender As Object, e As EventArgs) Handles cmdOk.Click
        'Abrir Empresa

        'Dim Cadena As String = Trim("C:\Compac\Empresas\")
        Dim Cadena As String = Trim(cbEmpresa.SelectedValue)
        Dim dir As New DirectoryInfo(Cadena)
        Dim CadanaFin = dir.Name

        'Dim NewCadena() As String = Split(Cadena, "\")
        'Dim CadanaFin As String = NewCadena(3).ToString

        'Inicio.CONSTR_COM = CadenaConexion2(UCase(sNomServidor), Trim(NewCadena(3))).ToString
        'Inicio.CONSTR_COM = CadenaConexion2(UCase(sNomServidor), CadanaFin).ToString
        lError = fAbreEmpresa(cbEmpresa.SelectedValue)
        If lError <> 0 Then
            bEmpresaSDKAbierta = False
            BandExito = False
            DespliegaError(lError)
        Else
            'lLicencia = 205
            ''MsgBox("fInicializaLicenseInfo(lLicencia) " & lLicencia)
            'lError = fInicializaLicenseInfo(lLicencia)
            vRuta = Trim(cbEmpresa.SelectedValue)
            vCatalog = Trim(CadanaFin)


            bEmpresaSDKAbierta = True
            Status("Empresa:" & cbEmpresa.Text & " Abierta Exitosamente", Me)
            BandExito = True
            Me.DialogResult = Windows.Forms.DialogResult.OK
            'Me.Close()
        End If
    End Sub

    Private Sub InicializaSDK()

        Dim KeySistema As RegistryKey = Registry.LocalMachine.OpenSubKey(szRegKeySistema)

        Dim lEntrada = KeySistema.GetValue("DirectorioBase")


        Directory.SetCurrentDirectory(lEntrada)

        'lError = fSetNombrePAQ(sNombrePAQ)
        lError = fSetNombrePAQ("CONTPAQ I COMERCIAL")
        If lError <> 0 Then
            If lError = 999999 Then
                bActivoSDK = True
            Else
                rError(lError)
                bActivoSDK = False
            End If

        Else
            Status("Inicio de Sesi�n Exitoso", Me)
            bActivoSDK = True
            cbEmpresa.Enabled = True
            cbEmpresa.Focus()
        End If
    End Sub

    Private Sub frmLoginSDK_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try

            Me.Text = "Acceso al Sistema " & Application.ProductName & " " & FormatNumber(Version, 2)


            Status("Iniciar Sesion al Sistema " & Application.ProductName & " ", Me)

            'If MsgBox("!! Desea Iniciar : " & NoSerie_SAL & " " & Folio_SAL & " ? !!", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
            'End If

            InicializaSDK()
            If bActivoSDK Then
                'llenaEmpresas()
                llenaEmpresasFin()
            End If

            'If _initialCatalog <> "" Then
            '    cbEmpresa.SelectedValue
            'End If
        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, Me.Text)
        End Try

    End Sub

    'Private Sub AgregaPath(ByVal vPath As String)
    '    Dim envName As String = "Path"
    '    Dim envValue As String = vPath & ";"

    '    'Dim ValorPath As String = ChecaValorRutaPATH()
    '    Dim ValorPath As String = ""
    '    Dim ExistePath As Boolean = False
    '    Try


    '        ValorPath = Environment.GetEnvironmentVariable(envName)

    '        'ExistePath = ValorPath.con(vPath)

    '        ExistePath = ValorPath.Contains(vPath)

    '        If Not ExistePath Then
    '            MsgBox("La Ubicaci�n " & UbicacionSDK & " No se encuentra en el PATH", MsgBoxStyle.Information, Me.Text)


    '            'NO funciona
    '            'Environment.SetEnvironmentVariable(envName, ValorPath & ";" & envValue)

    '            '' Determine whether the environment variable exists.
    '            'If Environment.GetEnvironmentVariable(envName) Is Nothing Then
    '            '    ' If it doesn't exist, create it.
    '            '    Environment.SetEnvironmentVariable(envName, envValue)
    '            'End If
    '        End If
    '    Catch ex As Exception
    '        MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, Me.Text)
    '    End Try

    'End Sub

    'Private Function ChecaValorRutaPATH() As String
    '    Dim ObjReg As New ToolRegistro
    '    Dim Resp As String
    '    ChecaValorRutaPATH = ""
    '    Try

    '        If ObjReg.LeeKey(KeyPathAdminPAQ) Then
    '            'Public KeyPathAdminPAQ As String = "HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Computaci�n en Acci�n, SA CV\AdminPAQ"
    '            'Resp = ObjReg.LeeLlaveRegistro(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Autentificacion Windows", KeyPathLM)
    '            Resp = ObjReg.LeeLlaveRegistro("", "AdminPAQ", "DIRECTORIOBASE", KeyPathAdminPAQ)
    '            If Resp <> "" Then
    '                Return Resp
    '            Else
    '                Return ""
    '            End If
    '        End If
    '    Catch ex As Exception
    '        MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, Me.Text)
    '    End Try

    'End Function
    'Private Function ChecaValorRutaSDK() As String
    '    Dim ObjReg As New ToolRegistro
    '    Dim Resp As String
    '    ChecaValorRutaSDK = ""
    '    Try

    '        If ObjReg.LeeKey(KeyPathAdminPAQ) Then
    '            'Public KeyPathAdminPAQ As String = "HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Computaci�n en Acci�n, SA CV\AdminPAQ"
    '            'Resp = ObjReg.LeeLlaveRegistro(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Autentificacion Windows", KeyPathLM)
    '            Resp = ObjReg.LeeLlaveRegistro("", "AdminPAQ", "DIRECTORIOBASE", KeyPathAdminPAQ)
    '            If Resp <> "" Then
    '                Return Resp
    '            Else
    '                Return ""
    '            End If
    '        End If
    '    Catch ex As Exception
    '        MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, Me.Text)
    '    End Try

    'End Function



    'BOTONES


    Private Sub cmdTerminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdTerminar.Click
        If bEmpresaSDKAbierta Then
            fCierraEmpresa()
            bEmpresaSDKAbierta = False
        End If
        If bActivoSDK Then
            fTerminaSDK()
            bActivoSDK = False
        End If
        DialogResult = DialogResult.Cancel
    End Sub

    Private Function BuscaIdEmpresa(ByVal NomEmpresa As String) As Integer
        BuscaIdEmpresa = 0
        If MyTablaEmpresas.DefaultView.Count > 0 Then
            MyTablaEmpresas.DefaultView.RowFilter = Nothing
            MyTablaEmpresas.DefaultView.RowFilter = "EMPRESA = '" & NomEmpresa & "'"
            If MyTablaEmpresas.DefaultView.Count > 0 Then
                Return MyTablaEmpresas.DefaultView.Item(0).Item("IDEMPRESA").ToString()
            End If
            MyTablaEmpresas.DefaultView.RowFilter = Nothing

        End If
    End Function




    Private Sub cbEmpresa_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cbEmpresa.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            ' cmdOk.Focus()
            'cmdOk_Click(sender, e)
            e.Handled = True
        End If
    End Sub


    Private Sub llenaEmpresasFin()
        CreaTablaEmpresas()
        cbEmpresa.Items.Clear()
        Dim util As New CapaNegocio.Parametros


        'Dim CnStrEsp As String = CadenaConexion2(UCase(sNomServidor & "\compac"), "CompacWAdmin").ToString
        'Inicio.CnStrEsp = util.CreaConStrSQL(UCase(sNomServidor & "\compac"), "CompacWAdmin", True, "", "", True)

        'BDCOMConf = New CapaDatos.UtilSQL(CnStrEsp, NomUser) 'Se inicializa la Clase BD
        If BDCOMConf Is Nothing Then
            BDCOMConf = Me.BDCOMConfig
        End If

        StrSql = "SELECT CIDEMPRESA,CNOMBREEMPRESA,CRUTADATOS,CRUTARESPALDOS FROM Empresas"

        DsConsulta = BDCOMConf.ExecuteReturn(StrSql)
        If DsConsulta.Rows.Count > 0 Then
            For i = 0 To DsConsulta.Rows.Count - 1
                myDataRow = MyTablaEmpresas.NewRow()
                myDataRow("EMPRESA") = DsConsulta.Rows(i).Item("CNOMBREEMPRESA")
                myDataRow("RUTA") = DsConsulta.Rows(i).Item("CRUTADATOS")
                myDataRow("IDEMPRESA") = DsConsulta.Rows(i).Item("CIDEMPRESA")
                MyTablaEmpresas.Rows.Add(myDataRow)

            Next
        End If

        cbEmpresa.DataSource = MyTablaEmpresas
        cbEmpresa.ValueMember = "RUTA"
        cbEmpresa.DisplayMember = "EMPRESA"

    End Sub



    Private Sub llenaEmpresas()
        Dim ObjReg As New ToolRegistro
        Dim lError As Integer = 0
        Dim lidEmpresa As Integer = 0
        Dim lRutaEmpresa As String = ""
        Dim lNombreEmpresa As String = ""
        Try
            CreaTablaEmpresas()
            '----------------- ADMINPAQ ----------------- 
            lRutaEmpresa = Space(61)
            lNombreEmpresa = Space(61)






            'lError = fSetNombrePAQ("AdminPAQ")
            'If lError <> 0 Then
            '    MensajeError(lError)
            '    End
            'End If

            'Parametro_DIRBASE = ObjReg.LeeLlaveRegistro("Computaci�n en Acci�n, SA CV", "AdminPAQ", "DIRECTORIOBASE", KeyPathCA)
            'Directory.SetCurrentDirectory(Parametro_DIRBASE)
            ''Resultado = ObjReg.LeeLlaveRegistro(Me.ProductName, "ServerSQL", "Nombre del servidor")
            '' Inicializar el SDK
            'lError = fInicializaSDK()
            'If lError <> 0 Then
            '    MensajeError(lError)
            '    End
            'End If

            '            lLicencia = 0
            '           lError = fInicializaLicenseInfo(lLicencia) ' 0=AdminPAQ, 1=FACTURA ELECTR�NICA
            '          If lError <> 0 Then
            'MensajeError(lError)
            'End
            'End If


            cbEmpresa.Items.Clear()
            'cbRuta.Items.Clear()

            ' Ir a la primera empresa
            lError = fPosPrimerEmpresa(lidEmpresa, lNombreEmpresa, lRutaEmpresa)
            If lError <> 0 Then
                MensajeError(lError)
                End
            End If

            While lError = 0
                'cbRuta.Items.Add(lRutaEmpresa)
                'cbEmpresa.Items.Add(lNombreEmpresa)
                myDataRow = MyTablaEmpresas.NewRow()
                myDataRow("EMPRESA") = lNombreEmpresa
                myDataRow("RUTA") = lRutaEmpresa
                myDataRow("IDEMPRESA") = lidEmpresa
                '
                MyTablaEmpresas.Rows.Add(myDataRow)
                lRutaEmpresa = Space(61)
                lNombreEmpresa = Space(61)
                lError = fPosSiguienteEmpresa(lidEmpresa, lNombreEmpresa, lRutaEmpresa)
            End While


            '----------------- ADMINPAQ ----------------- 
            If Not MyTablaEmpresas Is Nothing Then
                If MyTablaEmpresas.DefaultView.Count > 0 Then
                    cbEmpresa.DataSource = MyTablaEmpresas
                    cbEmpresa.ValueMember = "RUTA"
                    cbEmpresa.DisplayMember = "EMPRESA"
                End If
            End If
            cbEmpresa.SelectedIndex = 0
            'cbRuta.SelectedIndex = 0
        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "LlenaEmpresas")
        End Try
    End Sub

    Private Sub CreaTablaEmpresas()
        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.String")
        myDataColumn.ColumnName = "EMPRESA"
        myDataColumn.ReadOnly = False
        myDataColumn.Unique = False
        ' Add the Column to the DataColumnCollection.
        MyTablaEmpresas.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.String")
        myDataColumn.ColumnName = "RUTA"
        myDataColumn.ReadOnly = False
        myDataColumn.Unique = False
        ' Add the Column to the DataColumnCollection.
        MyTablaEmpresas.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Int64")
        myDataColumn.ColumnName = "IDEMPRESA"
        myDataColumn.ReadOnly = False
        myDataColumn.Unique = False
        ' Add the Column to the DataColumnCollection.
        MyTablaEmpresas.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.String")
        myDataColumn.ColumnName = "NOMBASE"
        myDataColumn.ReadOnly = False
        myDataColumn.Unique = False
        ' Add the Column to the DataColumnCollection.
        MyTablaEmpresas.Columns.Add(myDataColumn)

    End Sub






End Class
