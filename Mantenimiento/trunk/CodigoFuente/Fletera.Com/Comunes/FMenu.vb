Imports System.Data
Imports System.Data.SqlClient
Imports System.Math
Imports System.Drawing.Text
Imports System.Security.Cryptography
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.Win32
Imports System.IO
Imports System.Windows.Forms
Imports System.Security.Principal
Imports System.Reflection
Imports WpfClient
Imports Core
Imports Core.Models
Imports Core.Interfaces

Public Class FMenu
    Inherits System.Windows.Forms.Form
    Protected DsAccesos As New DataSet
    Dim DS As New DataSet
    Dim sPosibleError As String = ""
    Friend WithEvents mnuReportes As System.Windows.Forms.MenuItem
    Friend WithEvents TSSesion As System.Windows.Forms.ToolStripDropDownButton
    Friend WithEvents TSServer As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TSBD As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TSPerfil As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TSPCUsuario As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TSHora As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents TSCerrarSesion As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TSFecha As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripInfoUser As System.Windows.Forms.ToolStrip
    Friend WithEvents TSEmpresa As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuSalir As System.Windows.Forms.MenuItem

    Protected Const TABLAACCES As String = "ACCESOS"
    Dim DsComboFox As New DataSet
    Friend WithEvents mnuCatalogos As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCatDivision As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCatFam As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCatMarcas As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCatPais As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCatPuesto As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCatTipHerr As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCatTipOrd As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCatTipServ As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCatCiu As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCatEdo As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCatEmpresa As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCatSuc As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCatAlm As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCatColonias As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCatActividad As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCatTipoUniTras As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCatProv As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCatHerr As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCatSer As System.Windows.Forms.MenuItem
    Friend WithEvents mnuMovDiag As System.Windows.Forms.MenuItem
    Friend WithEvents mnuMovRecep As System.Windows.Forms.MenuItem
    Friend WithEvents mnuMovAsigOT As System.Windows.Forms.MenuItem
    Friend WithEvents mnuConfMig As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCatPer As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Dim Existe As String

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents MenuPrin As System.Windows.Forms.MainMenu
    Friend WithEvents mnuMovimientos As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuParametro As System.Windows.Forms.MenuItem
    Friend WithEvents mnuUbicaCompaq As System.Windows.Forms.MenuItem
    Friend WithEvents mnuUsuarios As System.Windows.Forms.MenuItem
    Friend WithEvents Status As System.Windows.Forms.StatusBar
    Friend WithEvents StatusUser As System.Windows.Forms.StatusBarPanel
    Friend WithEvents StatusServer As System.Windows.Forms.StatusBarPanel
    Friend WithEvents StatusDepto As System.Windows.Forms.StatusBarPanel
    Friend WithEvents StatusFecha As System.Windows.Forms.StatusBarPanel
    Friend WithEvents StatusHora As System.Windows.Forms.StatusBarPanel
    Friend WithEvents StatusMaquina As System.Windows.Forms.StatusBarPanel
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FMenu))
        Me.Status = New System.Windows.Forms.StatusBar()
        Me.StatusUser = New System.Windows.Forms.StatusBarPanel()
        Me.StatusServer = New System.Windows.Forms.StatusBarPanel()
        Me.StatusDepto = New System.Windows.Forms.StatusBarPanel()
        Me.StatusFecha = New System.Windows.Forms.StatusBarPanel()
        Me.StatusHora = New System.Windows.Forms.StatusBarPanel()
        Me.StatusMaquina = New System.Windows.Forms.StatusBarPanel()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.MenuPrin = New System.Windows.Forms.MainMenu(Me.components)
        Me.mnuCatalogos = New System.Windows.Forms.MenuItem()
        Me.mnuCatDivision = New System.Windows.Forms.MenuItem()
        Me.mnuCatFam = New System.Windows.Forms.MenuItem()
        Me.mnuCatMarcas = New System.Windows.Forms.MenuItem()
        Me.mnuCatPais = New System.Windows.Forms.MenuItem()
        Me.mnuCatPuesto = New System.Windows.Forms.MenuItem()
        Me.mnuCatTipHerr = New System.Windows.Forms.MenuItem()
        Me.mnuCatTipOrd = New System.Windows.Forms.MenuItem()
        Me.mnuCatTipServ = New System.Windows.Forms.MenuItem()
        Me.mnuCatCiu = New System.Windows.Forms.MenuItem()
        Me.mnuCatEdo = New System.Windows.Forms.MenuItem()
        Me.mnuCatEmpresa = New System.Windows.Forms.MenuItem()
        Me.mnuCatSuc = New System.Windows.Forms.MenuItem()
        Me.mnuCatAlm = New System.Windows.Forms.MenuItem()
        Me.mnuCatColonias = New System.Windows.Forms.MenuItem()
        Me.mnuCatActividad = New System.Windows.Forms.MenuItem()
        Me.mnuCatTipoUniTras = New System.Windows.Forms.MenuItem()
        Me.mnuCatProv = New System.Windows.Forms.MenuItem()
        Me.mnuCatHerr = New System.Windows.Forms.MenuItem()
        Me.mnuCatSer = New System.Windows.Forms.MenuItem()
        Me.mnuCatPer = New System.Windows.Forms.MenuItem()
        Me.mnuMovimientos = New System.Windows.Forms.MenuItem()
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.mnuMovRecep = New System.Windows.Forms.MenuItem()
        Me.mnuMovAsigOT = New System.Windows.Forms.MenuItem()
        Me.mnuMovDiag = New System.Windows.Forms.MenuItem()
        Me.MenuItem2 = New System.Windows.Forms.MenuItem()
        Me.mnuReportes = New System.Windows.Forms.MenuItem()
        Me.MenuItem4 = New System.Windows.Forms.MenuItem()
        Me.mnuParametro = New System.Windows.Forms.MenuItem()
        Me.mnuUbicaCompaq = New System.Windows.Forms.MenuItem()
        Me.mnuUsuarios = New System.Windows.Forms.MenuItem()
        Me.mnuConfMig = New System.Windows.Forms.MenuItem()
        Me.MenuSalir = New System.Windows.Forms.MenuItem()
        Me.ToolStripInfoUser = New System.Windows.Forms.ToolStrip()
        Me.TSSesion = New System.Windows.Forms.ToolStripDropDownButton()
        Me.TSServer = New System.Windows.Forms.ToolStripMenuItem()
        Me.TSBD = New System.Windows.Forms.ToolStripMenuItem()
        Me.TSPerfil = New System.Windows.Forms.ToolStripMenuItem()
        Me.TSPCUsuario = New System.Windows.Forms.ToolStripMenuItem()
        Me.TSHora = New System.Windows.Forms.ToolStripMenuItem()
        Me.TSEmpresa = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.TSCerrarSesion = New System.Windows.Forms.ToolStripMenuItem()
        Me.TSFecha = New System.Windows.Forms.ToolStripButton()
        Me.MenuItem3 = New System.Windows.Forms.MenuItem()
        CType(Me.StatusUser, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StatusServer, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StatusDepto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StatusFecha, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StatusHora, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StatusMaquina, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ToolStripInfoUser.SuspendLayout()
        Me.SuspendLayout()
        '
        'Status
        '
        Me.Status.Location = New System.Drawing.Point(0, 289)
        Me.Status.Name = "Status"
        Me.Status.Panels.AddRange(New System.Windows.Forms.StatusBarPanel() {Me.StatusUser, Me.StatusServer, Me.StatusDepto, Me.StatusFecha, Me.StatusHora, Me.StatusMaquina})
        Me.Status.ShowPanels = True
        Me.Status.Size = New System.Drawing.Size(736, 30)
        Me.Status.TabIndex = 2
        Me.Status.Text = "StatusBar1"
        '
        'StatusUser
        '
        Me.StatusUser.Name = "StatusUser"
        Me.StatusUser.ToolTipText = "Usuario Del Sistema"
        Me.StatusUser.Width = 160
        '
        'StatusServer
        '
        Me.StatusServer.Name = "StatusServer"
        Me.StatusServer.ToolTipText = "Servidor Conectado"
        Me.StatusServer.Width = 130
        '
        'StatusDepto
        '
        Me.StatusDepto.Name = "StatusDepto"
        Me.StatusDepto.ToolTipText = "Perfil Del Usuario"
        Me.StatusDepto.Width = 190
        '
        'StatusFecha
        '
        Me.StatusFecha.Name = "StatusFecha"
        Me.StatusFecha.ToolTipText = "Fecha Del Sistema"
        Me.StatusFecha.Width = 150
        '
        'StatusHora
        '
        Me.StatusHora.Name = "StatusHora"
        Me.StatusHora.ToolTipText = "Hora Del Sistema"
        Me.StatusHora.Width = 130
        '
        'StatusMaquina
        '
        Me.StatusMaquina.Name = "StatusMaquina"
        Me.StatusMaquina.ToolTipText = "Computadora del usuario"
        Me.StatusMaquina.Width = 280
        '
        'Timer1
        '
        '
        'MenuPrin
        '
        Me.MenuPrin.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuCatalogos, Me.mnuMovimientos, Me.mnuReportes, Me.MenuItem4})
        '
        'mnuCatalogos
        '
        Me.mnuCatalogos.Index = 0
        Me.mnuCatalogos.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuCatDivision, Me.mnuCatFam, Me.mnuCatMarcas, Me.mnuCatPais, Me.mnuCatPuesto, Me.mnuCatTipHerr, Me.mnuCatTipOrd, Me.mnuCatTipServ, Me.mnuCatCiu, Me.mnuCatEdo, Me.mnuCatEmpresa, Me.mnuCatSuc, Me.mnuCatAlm, Me.mnuCatColonias, Me.mnuCatActividad, Me.mnuCatTipoUniTras, Me.mnuCatProv, Me.mnuCatHerr, Me.mnuCatSer, Me.mnuCatPer})
        Me.mnuCatalogos.Tag = "010000"
        Me.mnuCatalogos.Text = "Catalogos"
        '
        'mnuCatDivision
        '
        Me.mnuCatDivision.Index = 0
        Me.mnuCatDivision.Tag = "010100"
        Me.mnuCatDivision.Text = "Divisiones"
        '
        'mnuCatFam
        '
        Me.mnuCatFam.Index = 1
        Me.mnuCatFam.Tag = "010200"
        Me.mnuCatFam.Text = "Familias"
        '
        'mnuCatMarcas
        '
        Me.mnuCatMarcas.Index = 2
        Me.mnuCatMarcas.Tag = "010300"
        Me.mnuCatMarcas.Text = "Marcas"
        '
        'mnuCatPais
        '
        Me.mnuCatPais.Index = 3
        Me.mnuCatPais.Tag = "010400"
        Me.mnuCatPais.Text = "Pa�s"
        '
        'mnuCatPuesto
        '
        Me.mnuCatPuesto.Index = 4
        Me.mnuCatPuesto.Tag = "010500"
        Me.mnuCatPuesto.Text = "Puestos"
        '
        'mnuCatTipHerr
        '
        Me.mnuCatTipHerr.Index = 5
        Me.mnuCatTipHerr.Tag = "010600"
        Me.mnuCatTipHerr.Text = "Tipo de Herramientas"
        '
        'mnuCatTipOrd
        '
        Me.mnuCatTipOrd.Index = 6
        Me.mnuCatTipOrd.Tag = "010700"
        Me.mnuCatTipOrd.Text = "Tipo de Orden"
        '
        'mnuCatTipServ
        '
        Me.mnuCatTipServ.Index = 7
        Me.mnuCatTipServ.Tag = "010800"
        Me.mnuCatTipServ.Text = "Tipo de Servicios"
        '
        'mnuCatCiu
        '
        Me.mnuCatCiu.Index = 8
        Me.mnuCatCiu.Tag = "010900"
        Me.mnuCatCiu.Text = "Ciudades"
        '
        'mnuCatEdo
        '
        Me.mnuCatEdo.Index = 9
        Me.mnuCatEdo.Tag = "011000"
        Me.mnuCatEdo.Text = "Estados"
        '
        'mnuCatEmpresa
        '
        Me.mnuCatEmpresa.Index = 10
        Me.mnuCatEmpresa.Tag = "011100"
        Me.mnuCatEmpresa.Text = "Empresas"
        '
        'mnuCatSuc
        '
        Me.mnuCatSuc.Index = 11
        Me.mnuCatSuc.Tag = "011200"
        Me.mnuCatSuc.Text = "Sucursales"
        '
        'mnuCatAlm
        '
        Me.mnuCatAlm.Index = 12
        Me.mnuCatAlm.Tag = "011300"
        Me.mnuCatAlm.Text = "Almacen"
        '
        'mnuCatColonias
        '
        Me.mnuCatColonias.Index = 13
        Me.mnuCatColonias.Tag = "011400"
        Me.mnuCatColonias.Text = "Colonias"
        '
        'mnuCatActividad
        '
        Me.mnuCatActividad.Index = 14
        Me.mnuCatActividad.Tag = "011500"
        Me.mnuCatActividad.Text = "Actividades"
        '
        'mnuCatTipoUniTras
        '
        Me.mnuCatTipoUniTras.Index = 15
        Me.mnuCatTipoUniTras.Tag = "011600"
        Me.mnuCatTipoUniTras.Text = "Tipo Unidades Trans."
        '
        'mnuCatProv
        '
        Me.mnuCatProv.Index = 16
        Me.mnuCatProv.Tag = "011700"
        Me.mnuCatProv.Text = "Proveedores"
        '
        'mnuCatHerr
        '
        Me.mnuCatHerr.Index = 17
        Me.mnuCatHerr.Tag = "011800"
        Me.mnuCatHerr.Text = "Herramientas"
        '
        'mnuCatSer
        '
        Me.mnuCatSer.Index = 18
        Me.mnuCatSer.Tag = "011900"
        Me.mnuCatSer.Text = "Servicios"
        '
        'mnuCatPer
        '
        Me.mnuCatPer.Index = 19
        Me.mnuCatPer.Tag = "012000"
        Me.mnuCatPer.Text = "Personal"
        '
        'mnuMovimientos
        '
        Me.mnuMovimientos.Index = 1
        Me.mnuMovimientos.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1, Me.mnuMovRecep, Me.mnuMovAsigOT, Me.mnuMovDiag, Me.MenuItem2, Me.MenuItem3})
        Me.mnuMovimientos.Tag = "020000"
        Me.mnuMovimientos.Text = "Movimientos"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.Text = "Solicitud de Orden de Trabajo"
        '
        'mnuMovRecep
        '
        Me.mnuMovRecep.Index = 1
        Me.mnuMovRecep.Tag = "020100"
        Me.mnuMovRecep.Text = "Recepcion de Orden de Trabajo"
        '
        'mnuMovAsigOT
        '
        Me.mnuMovAsigOT.Index = 2
        Me.mnuMovAsigOT.Tag = "020200"
        Me.mnuMovAsigOT.Text = "Asignacion O.T."
        '
        'mnuMovDiag
        '
        Me.mnuMovDiag.Index = 3
        Me.mnuMovDiag.Tag = "020300"
        Me.mnuMovDiag.Text = "Diagnostico"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 4
        Me.MenuItem2.Text = "Monitor Taller"
        '
        'mnuReportes
        '
        Me.mnuReportes.Index = 2
        Me.mnuReportes.Tag = "030000"
        Me.mnuReportes.Text = "Reportes"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 3
        Me.MenuItem4.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuParametro, Me.mnuUbicaCompaq, Me.mnuUsuarios, Me.mnuConfMig, Me.MenuSalir})
        Me.MenuItem4.Tag = "040000"
        Me.MenuItem4.Text = "Configuraci�n"
        '
        'mnuParametro
        '
        Me.mnuParametro.Index = 0
        Me.mnuParametro.Tag = "030100"
        Me.mnuParametro.Text = "Parametros"
        '
        'mnuUbicaCompaq
        '
        Me.mnuUbicaCompaq.Index = 1
        Me.mnuUbicaCompaq.Tag = "030200"
        Me.mnuUbicaCompaq.Text = "Ubicaci�n ADMINPAQ"
        '
        'mnuUsuarios
        '
        Me.mnuUsuarios.Index = 2
        Me.mnuUsuarios.Tag = "030300"
        Me.mnuUsuarios.Text = "Usuarios"
        '
        'mnuConfMig
        '
        Me.mnuConfMig.Index = 3
        Me.mnuConfMig.Text = "Migracion COMERCIAL"
        '
        'MenuSalir
        '
        Me.MenuSalir.Index = 4
        Me.MenuSalir.Tag = "030400"
        Me.MenuSalir.Text = "Salir"
        '
        'ToolStripInfoUser
        '
        Me.ToolStripInfoUser.Dock = System.Windows.Forms.DockStyle.None
        Me.ToolStripInfoUser.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.ToolStripInfoUser.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TSSesion, Me.TSFecha})
        Me.ToolStripInfoUser.Location = New System.Drawing.Point(597, 261)
        Me.ToolStripInfoUser.Name = "ToolStripInfoUser"
        Me.ToolStripInfoUser.Size = New System.Drawing.Size(99, 25)
        Me.ToolStripInfoUser.TabIndex = 6
        Me.ToolStripInfoUser.Text = "ToolStrip1"
        Me.ToolStripInfoUser.Visible = False
        '
        'TSSesion
        '
        Me.TSSesion.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.TSSesion.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TSServer, Me.TSBD, Me.TSPerfil, Me.TSPCUsuario, Me.TSHora, Me.TSEmpresa, Me.ToolStripSeparator1, Me.TSCerrarSesion})
        Me.TSSesion.ForeColor = System.Drawing.Color.Black
        Me.TSSesion.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TSSesion.Name = "TSSesion"
        Me.TSSesion.Size = New System.Drawing.Size(54, 22)
        Me.TSSesion.Text = "Sesion"
        '
        'TSServer
        '
        Me.TSServer.Enabled = False
        Me.TSServer.Name = "TSServer"
        Me.TSServer.Size = New System.Drawing.Size(182, 22)
        Me.TSServer.Text = "Server"
        '
        'TSBD
        '
        Me.TSBD.Enabled = False
        Me.TSBD.Name = "TSBD"
        Me.TSBD.Size = New System.Drawing.Size(182, 22)
        Me.TSBD.Text = "Base"
        '
        'TSPerfil
        '
        Me.TSPerfil.Enabled = False
        Me.TSPerfil.Name = "TSPerfil"
        Me.TSPerfil.Size = New System.Drawing.Size(182, 22)
        Me.TSPerfil.Text = "Perfil"
        '
        'TSPCUsuario
        '
        Me.TSPCUsuario.Enabled = False
        Me.TSPCUsuario.Name = "TSPCUsuario"
        Me.TSPCUsuario.Size = New System.Drawing.Size(182, 22)
        Me.TSPCUsuario.Text = "PcUsuario"
        '
        'TSHora
        '
        Me.TSHora.Enabled = False
        Me.TSHora.Name = "TSHora"
        Me.TSHora.Size = New System.Drawing.Size(182, 22)
        Me.TSHora.Text = "Hora"
        '
        'TSEmpresa
        '
        Me.TSEmpresa.Enabled = False
        Me.TSEmpresa.Name = "TSEmpresa"
        Me.TSEmpresa.Size = New System.Drawing.Size(182, 22)
        Me.TSEmpresa.Text = "Empresa AdminPAQ"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(179, 6)
        '
        'TSCerrarSesion
        '
        Me.TSCerrarSesion.Name = "TSCerrarSesion"
        Me.TSCerrarSesion.Size = New System.Drawing.Size(182, 22)
        Me.TSCerrarSesion.Text = "Cerrar Sesion"
        '
        'TSFecha
        '
        Me.TSFecha.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.TSFecha.ForeColor = System.Drawing.Color.Black
        Me.TSFecha.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TSFecha.Name = "TSFecha"
        Me.TSFecha.Size = New System.Drawing.Size(42, 22)
        Me.TSFecha.Text = "Fecha"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 5
        Me.MenuItem3.Text = "Factura Viajes"
        '
        'FMenu
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(736, 319)
        Me.Controls.Add(Me.ToolStripInfoUser)
        Me.Controls.Add(Me.Status)
        Me.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.IsMdiContainer = True
        Me.KeyPreview = True
        Me.Menu = Me.MenuPrin
        Me.Name = "FMenu"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Sistema Fletera - Comercial"
        CType(Me.StatusUser, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StatusServer, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StatusDepto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StatusFecha, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StatusHora, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StatusMaquina, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ToolStripInfoUser.ResumeLayout(False)
        Me.ToolStripInfoUser.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Function InformacionVersion() As String
        'MsgBox("hOLA")
        Return "HOLA"
        'Try
        '    If FechaActual = "" Then
        '        FechaActual = BUSCAFECHA_ESCRITURA()
        '        If Not IsDate(FechaActual) Then
        '            FechaActual = ""
        '        End If
        '    End If
        '    If ContaSegundos = 0 Or Not ContaSegundos = 20 Then
        '        ContaSegundos += 1
        '    Else
        '        NuevaFecha = BUSCAFECHA_ESCRITURA()
        '        If Not IsDate(NuevaFecha) Then
        '            NuevaFecha = ""
        '        ElseIf Format(CDate(FechaActual), "dd/MMM/yyyy hh:mm:ss tt") <> Format(CDate(NuevaFecha), "dd/MMM/yyyy hh:mm:ss tt") Then
        '            MsgBox("Se ha realizado una actualizacion a tu programa INTEGRAL" & vbCrLf & _
        '            "Para que la actualizacion tenga efecto debes de cerrar y abrir de nuevo tu INTEGRAL" & vbCrLf & _
        '            "Cuando termines de realizar tus actividades puedes cerrar y abrir tu INTEGRAL" & vbCrLf & _
        '            "Este mensaje seguira apareciendo hasta que cierres y abras tu INTEGRAL" & vbCrLf & _
        '            "Disculpa las molestias que esto te ocasiona" & vbCrLf & _
        '             vbCrLf & _
        '             vbCrLf & _
        '            "                       Atentamente " & vbCrLf & _
        '            "                El Departamento de Sistemas", MsgBoxStyle.Information, Me.Text)
        '        End If
        '        ContaSegundos = 0
        '    End If
        'Catch ex As Exception

        'End Try
    End Function

    Sub InformacionUser()
        Try
            Timer1.Start()
            'Status.Panels.Item(0).Text = "Usuario: " & UCase(System.Environment.UserName)
            Status.Panels.Item(0).Text = "Usuario: " & UserConectado.idUsuario
            Status.Panels.Item(0).ToolTipText = "Nombre: " & UserConectado.NombreUsuario
            'Status.Panels.Item(1).Text = "Server: " & UCase(GetSetting("BASE", "ServerSQL", "Nombre", ""))
            Status.Panels.Item(1).Text = "Server: " & sNomServidor
            Status.Panels.Item(2).Text = "Perfil: " & UserConectado.Grupo.NombreGrupo
            Status.Panels.Item(3).Text = "Fecha: " & Format(DateTime.Now, "dd/MMMM/yyyy")
            Status.Panels.Item(4).Text = "Hora: " & Format(DateTime.Now, "hh:mm:ss tt")
            Status.Panels.Item(5).Text = "PC usuario: " & UCase(Environment.MachineName)
            TSServer.Text = "Server: " & sNomServidor
            TSPerfil.Text = "Perfil: " & UserConectado.Grupo.NombreGrupo
            TSPCUsuario.Text = "PC Usuario: " & UCase(Environment.MachineName)
            TSBD.Text = "Base de Datos: " & sNomBd
            TSEmpresa.Text = "Empresa AdminPAQ :" & sNombreEmpresaAdmPAQ
            TSEmpresa.ToolTipText = "Ruta Empresa : " & sRutaEmpresaAdmPAQ
            'sNombreEmpresa
            InformacionVersion()
        Catch Ex As Exception
        End Try
    End Sub

    Private Sub FMenu_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Try
            TSSesion.Text = UserConectado.idUsuario
            TSSesion.ToolTipText = "Nombre: " & UserConectado.NombreUsuario & IIf(UserConectado.Grupo.Existe,
                                                        vbNewLine & "Nombre Grupo: " & UserConectado.Grupo.NombreGrupo, "")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Menu_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        GrabaEmpresa()
        If bEmpresaSDKAbierta Then
            fCierraEmpresa()
        End If
        If bActivoSDK Then
            fTerminaSDK()
        End If



        'Me.Close()
    End Sub

    Private Sub Menu_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyValue = Keys.F5 And UserId = KEY_CONFIG_USUARIO Then
            Dim frm As New frmUsuarios("030300")
            frm.MdiParent = Me
            frm.Show()

            'Dim frm As New frmUsuarios(TryCast(sender, MenuItem).Tag)
            'frm.MdiParent = Me
            'frm.StartPosition = FormStartPosition.CenterScreen
            'frm.Show()
        ElseIf e.KeyValue = Keys.F4 Then
            'Dim frm As New frmDiagnosticoTaller("020300")
            'frm.MdiParent = Me
            'frm.Show()

        End If
    End Sub

    Private Sub AplicaPermisos()
        For j = 0 To Me.Menu.MenuItems.Count - 1
            RecAplicaPermItem(Me.Menu.MenuItems(j))
        Next
    End Sub

    Private Sub RecAplicaPermItem(ByVal vMenu As MenuItem)
        If vMenu.Tag <> "" Then
            vMenu.Enabled = UserConectado.TieneAcceso(vMenu.Tag)
        End If
        For j = 0 To vMenu.MenuItems.Count - 1
            With vMenu.MenuItems(j)
                If .Tag = "" Then Continue For
                .Enabled = UserConectado.TieneAcceso(.Tag)
                If vMenu.MenuItems(j).MenuItems.Count > 0 Then
                    RecAplicaPermItem(vMenu.MenuItems(j))
                End If
                'DibGuardPerm(FMenu.Menu.MenuItems(j), ArbolPerm.Nodes(j)) 

            End With
        Next 'PRIMERO DIBUJAA EL MENUUUU
    End Sub

    'Private Sub CreaEnLocalMachine()
    '    Dim ObjReg As New ToolRegistro
    '    Dim Resultado As String
    '    'ObjReg.CreaSubKey(Me.ProductName, LugarRegistry.regLocalMachine, "SOFTWARE")
    '    ObjReg.CreaSubKey(Me.ProductName)
    '    ObjReg.CreaSubSubKey(Me.ProductName, "ServerSQL")
    '    ObjReg.CreaValoresSubKey(Me.ProductName, "ServerSQL", "Nombre del servidor", sNomServidor, KeyPathLM)
    '    ObjReg.CreaValoresSubKey(Me.ProductName, "ServerSQL", "Nombre de la Base de Datos", sNomBd, KeyPathLM)
    '    ObjReg.CreaValoresSubKey(Me.ProductName, "ServerSQL", "Autentificacion Windows", sAutentificacionWindows, KeyPathLM)
    '    If Not sNomUsuarioSQL Is Nothing Then
    '        ObjReg.CreaValoresSubKey(Me.ProductName, "ServerSQL", "Nombre del Usuario SQL", sNomUsuarioSQL, KeyPathLM)
    '    End If
    '    If Not sPassUsuarioSQL Is Nothing Then
    '        ObjReg.CreaValoresSubKey(Me.ProductName, "ServerSQL", "Password del Usuario SQL", sPassUsuarioSQL, KeyPathLM)
    '    End If

    '    Resultado = ObjReg.LeeLlaveRegistro(Me.ProductName, "ServerSQL", "Nombre del servidor", KeyPathLM)
    '    'MsgBox(Resultado)
    '    'SaveSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Nombre del servidor", sNomServidor)
    '    'SaveSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Nombre de la Base de Datos", sNomBd)
    '    'SaveSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Autentificacion Windows", sAutentificacionWindows)
    '    'SaveSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Nombre del Usuario SQL", sNomUsuarioSQL)
    '    'SaveSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Password del Usuario SQL", sPassUsuarioSQL)
    'End Sub
    Private Function IsRunAsAdministrator() As Boolean
        Dim wi = WindowsIdentity.GetCurrent()
        Dim wp As New WindowsPrincipal(wi)
        Return wp.IsInRole(WindowsBuiltInRole.Administrator)
    End Function
    Private Function ValidaInicioAdministrador() As Boolean
        If (Not IsRunAsAdministrator()) Then
            'Cuando no se inicia en modo administrador
            Dim processInfo = New ProcessStartInfo(Assembly.GetExecutingAssembly().CodeBase)

            'se le aprega a sus propiedades que inicie como administrador

            processInfo.UseShellExecute = True
            processInfo.Verb = "runas"

            'Inicia el proceso
            Try
                Process.Start(processInfo)
            Catch ex As Exception
                MessageBox.Show("Sorry, this application must be run as Administrator.")
            End Try
            'Cierrra la aplicacion que no se inicio en modo administrador

            Application.Exit()
            Return False
        Else
            Return True
        End If
    End Function

    Private Sub Menu_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If (Not ValidaInicioAdministrador()) Then Exit Sub

            'TODO falta hacer pruebas. . . etc. . .'asi luego facilmente puedes buscar esos comentarios luego te explico como

            'OJOOO
            'esPrueba = ComprobarLicencia()



            If IniciarSesion() = False Then End
            TSFecha.Text = FormatDateTime(Now.Date, DateFormat.LongDate)
            Me.WindowState = FormWindowState.Maximized
            'UserId = Inicio.UserId
            'GrupoUser = Inicio.GrupoUser
            'NomUser = Inicio.NomUser
            If esPrueba Then
                Me.Text = Me.Text & " ** VERSION DEMO !!!"
            End If
            'Dim util As New CapaNegocio.Parametros
            'Inicio.CONSTR = util.CreaConStrSQL(UCase(sNomServidor), UCase(sNomBd), True, "", "")
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            'If GetSetting(Process.GetCurrentProcess.ProcessName, KEY_CONFIG_SECCION, KEY_CONFIF_KEY, "") = "" Then
            '    MsgBox("No se ha declarado la ubicacion de la base de datos", MsgBoxStyle.Information)
            '    'Ubicacion = AbreArchivos()
            '    UbicacionAdminPaq = SeleccionaCarpeta()
            '    SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CONFIG_SECCION, KEY_CONFIF_KEY, UbicacionAdminPaq)
            'Else
            '    UbicacionAdminPaq = GetSetting(Process.GetCurrentProcess.ProcessName, KEY_CONFIG_SECCION, KEY_CONFIF_KEY, "")
            'End If

            'ya no se usa por que se lee de la base parametros
            'SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CONFIG_SECCION, KEY_CONFIF_KEY, sRutaEmpresaAdmPAQ)


            Inicio.ConStrFox = CreaCadenaFox(sRutaEmpresaAdmPAQ)


            LlenaParametros()

            InformacionUser()

            'ChecaValoresRegistro(sAutentificacionWindows)
            'CreaEnLocalMachine(Me.ProductName)
            'Pendiente
            AplicaPermisos()

            'If usuario.esTaller Then
            '    mnuMovRecep.Text = "Recepci�n de Orden de Servicio."
            'Else
            '    mnuMovRecep.Text = "Solicitud de Orden de Servicio."
            'End If

        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, Me.Text)
        End Try

    End Sub

    Private Function LlenaDatosFox(ByVal cCve As String, ByVal dbtabla As String, ByVal Campo As String, ByVal TipoCampo As TipoDato,
                               Optional ByVal cCve2 As String = "") As String
        Dim Consulta As String = ""
        Dim Conexion As New OleDb.OleDbConnection(ConStrFox)
        Try
            Select Case UCase(dbtabla)
                Case UCase("MGW10000")
                    Consulta = "select CIDEMPRESA, CNOMBREE01, CRFCEMPR01 from MGW10000 where CIDEMPRESA = " & sIdEmpresa

                Case UCase("MGW10011")
                    Consulta = "SELECT (ALLTRIM(DIR.CNOMBREC01) + ' ' + ALLTRIM(DIR.CCOLONIA) + ' ' +  " &
                    "ALLTRIM(DIR.CCIUDAD) + ' ' + ALLTRIM(DIR.CESTADO)  ) AS DIRECCION, DIR.CTELEFONO1,DIR.CEMAIL " &
                    "FROM MGW10011 DIR WHERE DIR.CTIPOCAT01 = 4 AND DIR.CTIPODIR01 = 0 "
            End Select
            Dim Adt As New OleDb.OleDbDataAdapter(Consulta, Conexion)
            DsComboFox.Clear()
            Adt.Fill(DsComboFox, dbtabla)
            If Not DsComboFox Is Nothing Then
                If DsComboFox.Tables(dbtabla).DefaultView.Count > 0 Then
                    Select Case UCase(dbtabla)
                        Case UCase("MGW10000")
                            Parametro_Empresa = Trim(DsComboFox.Tables("MGW10000").DefaultView.Item(0).Item("CNOMBREE01"))
                            Parametro_RFC = Trim(DsComboFox.Tables("MGW10000").DefaultView.Item(0).Item("CRFCEMPR01"))

                        Case UCase("MGW10011")
                            Parametro_Direccion = Trim(DsComboFox.Tables("MGW10011").DefaultView.Item(0).Item("DIRECCION"))
                            Parametro_Telefono = Trim(DsComboFox.Tables("MGW10011").DefaultView.Item(0).Item("CTELEFONO1"))
                    End Select
                    LlenaDatosFox = KEY_RCORRECTO
                Else
                    LlenaDatosFox = KEY_RINCORRECTO
                End If
            Else
                LlenaDatosFox = KEY_RINCORRECTO
            End If


        Catch ex As Exception
            SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")
            LlenaDatosFox = KEY_RERROR
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, Me.Text)
        End Try
    End Function



    Private Sub LlenaParametros()
        Dim util As New CapaNegocio.Parametros
        DS = Inicio.ChecaClave("1", Inicio.CONSTR, TipoConexion.TcSQL, "PARAMETROS", "IdParametro", TipoDato.TdNumerico, Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS)
        sPosibleError = GetSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")
        If sPosibleError <> "ERROR" Or sPosibleError = Nothing Then
            SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")
            If Not DS Is Nothing Then
                If DS.Tables("PARAMETROS").DefaultView.Count > 0 Then
                    'SE INSERTAN LOS PARAMETROS CORRESPONDIENTES
                    'Parametro_ServidorxAfectar = DS.Tables("PARAMETROS").DefaultView.Item(0).Item("ServidorxAfectar")
                    'Parametro_BaseDatosxAfectar = DS.Tables("PARAMETROS").Defaul_loadtView.Item(0).Item("BaseDatosxAfectar")

                    'OJOOOOOOO
                    'Parametro_UsuarioSQL = DS.Tables("PARAMETROS").DefaultView.Item(0).Item("UsuarioSQL")
                    Parametro_UsuarioSQL = sNomUsuarioSQL
                    Parametro_PasswordSQL = sPassUsuarioSQL
                    'Criptografia.Decrypt(Trim())
                    'hAY QUE ENCRIPTAR
                    ''Parametro_PasswordSQL = DS.Tables("PARAMETROS").DefaultView.Item(0).Item("PasswordSQL")
                    'Parametro_PasswordSQL = Criptografia.Decrypt(Trim(DS.Tables("PARAMETROS").DefaultView.Item(0).Item("PasswordSQL")), KEY_CONFIG_ENCRYPT)




                    'Parametro_PassEmailEmpresa = Criptografia.Decrypt(Trim(DS.Tables("PARAMETROS").DefaultView.Item(0).Item("EmailPassEmpresa")), KEY_CONFIG_ENCRYPT)
                    'Parametro_PassEmailEmpresa = DS.Tables("PARAMETROS").DefaultView.Item(0).Item("EmailPassEmpresa")

                    '15/ABRIL/2014
                    'ya no se toma de la base de datos, si no del registro
                    'Registro_ColaFacturaLocal = GetSetting(Process.GetCurrentProcess.ProcessName, "Configuracion", "ColaFacturaLocal", "") & ""
                    'Registro_ColaRemisionLocal = GetSetting(Process.GetCurrentProcess.ProcessName, "Configuracion", "ColaRemisionLocal", "") & ""
                    'Registro_ColaPedidoLocal = GetSetting(Process.GetCurrentProcess.ProcessName, "Configuracion", "ColaPedidoLocal", "") & ""


                    'If Not DS.Tables("PARAMETROS").DefaultView.Item(0).Item("ColaImpresionFact") Is DBNull.Value Then
                    '    Parametro_ColaImpresionFact = DS.Tables("PARAMETROS").DefaultView.Item(0).Item("ColaImpresionFact")
                    'Else
                    '    Parametro_ColaImpresionFact = ""
                    'End If
                    'If Not DS.Tables("PARAMETROS").DefaultView.Item(0).Item("ColaImpresionPed") Is DBNull.Value Then
                    '    Parametro_ColaImpresionPed = DS.Tables("PARAMETROS").DefaultView.Item(0).Item("ColaImpresionPed")
                    'Else
                    '    Parametro_ColaImpresionPed = ""
                    'End If

                    'If Not DS.Tables("PARAMETROS").DefaultView.Item(0).Item("ColaImpresionRem") Is DBNull.Value Then
                    '    Parametro_ColaImpresionRem = DS.Tables("PARAMETROS").DefaultView.Item(0).Item("ColaImpresionRem")
                    'Else
                    '    Parametro_ColaImpresionRem = ""
                    'End If


                    'If Not DS.Tables("PARAMETROS").DefaultView.Item(0).Item("ImprimeRegistro") Is DBNull.Value Then
                    '    Parametro_ImprimeReg = DS.Tables("PARAMETROS").DefaultView.Item(0).Item("ImprimeRegistro")
                    'Else
                    '    Parametro_ImprimeReg = False
                    'End If



                    'If Not DS.Tables("PARAMETROS").DefaultView.Item(0).Item("ServidorPOP3") Is DBNull.Value Then
                    '    Parametro_ServidorPOP3 = DS.Tables("PARAMETROS").DefaultView.Item(0).Item("ServidorPOP3")
                    'Else
                    '    Parametro_ServidorPOP3 = ""
                    'End If

                    'If Not DS.Tables("PARAMETROS").DefaultView.Item(0).Item("PuertoPOP3") Is DBNull.Value Then
                    '    Parametro_PuertoPOP3 = DS.Tables("PARAMETROS").DefaultView.Item(0).Item("PuertoPOP3")
                    'Else
                    '    Parametro_PuertoPOP3 = ""
                    'End If


                    'If Not DS.Tables("PARAMETROS").DefaultView.Item(0).Item("ServidorSMTP") Is DBNull.Value Then
                    '    Parametro_ServidorSMTP = DS.Tables("PARAMETROS").DefaultView.Item(0).Item("ServidorSMTP")
                    'Else
                    '    Parametro_ServidorSMTP = ""
                    'End If


                    'If Not DS.Tables("PARAMETROS").DefaultView.Item(0).Item("PuertoSMTP") Is DBNull.Value Then
                    '    Parametro_PuertoSMTP = DS.Tables("PARAMETROS").DefaultView.Item(0).Item("PuertoSMTP")
                    'Else
                    '    Parametro_PuertoSMTP = 587
                    'End If

                    'If Not DS.Tables("PARAMETROS").DefaultView.Item(0).Item("ConexionSSL") Is DBNull.Value Then
                    '    Parametro_ConexionSSL = DS.Tables("PARAMETROS").DefaultView.Item(0).Item("ConexionSSL")
                    'Else
                    '    Parametro_ConexionSSL = False
                    'End If

                    'If Not DS.Tables("PARAMETROS").DefaultView.Item(0).Item("MensajeCorreo") Is DBNull.Value Then
                    '    Parametro_MensajeCorreo = DS.Tables("PARAMETROS").DefaultView.Item(0).Item("MensajeCorreo")
                    'Else
                    '    Parametro_MensajeCorreo = ""
                    'End If

                    'If Not DS.Tables("PARAMETROS").DefaultView.Item(0).Item("PasswordFacturacion") Is DBNull.Value Then
                    '    Parametro_PassFacturacion = Criptografia.Decrypt(Trim(DS.Tables("PARAMETROS").DefaultView.Item(0).Item("PasswordFacturacion")), KEY_CONFIG_ENCRYPT)
                    '    'Parametro_PassFacturacion = DS.Tables("PARAMETROS").DefaultView.Item(0).Item("vPassFacturacion")
                    'Else
                    '    Parametro_PassFacturacion = ""
                    'End If


                    'CARGARLOS DE ADMINPAQ
                    'Parametro_Empresa = DS.Tables("PARAMETROS").DefaultView.Item(0).Item("Empresa")
                    'Parametro_Direccion = DS.Tables("PARAMETROS").DefaultView.Item(0).Item("Direccion")
                    'Parametro_RFC = DS.Tables("PARAMETROS").DefaultView.Item(0).Item("RFC")
                    'Parametro_Telefono = DS.Tables("PARAMETROS").DefaultView.Item(0).Item("Telefono")

                    'Parametro_EmailEmpresa = DS.Tables("PARAMETROS").DefaultView.Item(0).Item("EmailEmpresa")

                    ''3/ENERO/2015
                    'If Not DS.Tables("PARAMETROS").DefaultView.Item(0).Item("CPLAMIGCFD") Is DBNull.Value Then
                    '    Parametro_CPLAMIGCFD = DS.Tables("PARAMETROS").DefaultView.Item(0).Item("CPLAMIGCFD")
                    'Else
                    '    Parametro_CPLAMIGCFD = ""
                    'End If


                    'Existe = LlenaDatosFox(sIdEmpresa, "MGW10000", "CCODIGOC01", TipoDato.TdCadena)
                    'If Existe = KEY_RCORRECTO Then
                    '    Existe = LlenaDatosFox(sIdEmpresa, "MGW10011", "CCODIGOC01", TipoDato.TdCadena)
                    '    If Existe = KEY_RINCORRECTO Then
                    '        'no se encontraron: Parametro_Direccion,Parametro_Telefono,Parametro_EmailEmpresa  de adminpaq
                    '        Parametro_Direccion = DS.Tables("PARAMETROS").DefaultView.Item(0).Item("Direccion")
                    '        Parametro_Telefono = DS.Tables("PARAMETROS").DefaultView.Item(0).Item("Telefono")
                    '    End If
                    'Else
                    '    'no se encontraron: Parametro_Empresa,Parametro_RFC de adminpaq
                    '    Parametro_Empresa = DS.Tables("PARAMETROS").DefaultView.Item(0).Item("Empresa")
                    '    Parametro_RFC = DS.Tables("PARAMETROS").DefaultView.Item(0).Item("RFC")
                    'End If

                    'If Not DS.Tables("PARAMETROS").DefaultView.Item(0).Item("ListaGrupo") Is DBNull.Value Then
                    '    Parametro_GrupoDefault = DS.Tables("PARAMETROS").DefaultView.Item(0).Item("ListaGrupo")
                    'Else
                    '    Parametro_GrupoDefault = ""
                    'End If




                    'TMSetup = Criptografia.Decrypt(GetSetting(Process.GetCurrentProcess.ProcessName, "RegSoft", "TMSetup", "") & "", "RgDsgSoft")
                    'Criptografia.Encrypt(HideInfo, "RgDsgSoft")

                    'Esto es para la pantalla de Parametros
                    'Parametro_PassEmailEmpresa = Criptografia.Encrypt(Trim(DS.Tables("PARAMETROS").DefaultView.Item(0).Item("EmailPassEmpresa")), KEY_CONFIG_ENCRYPT)


                    'Parametro_PassEmailEmpresa = DS.Tables("PARAMETROS").DefaultView.Item(0).Item("EmailPassEmpresa")



                    'sAutentificacionWindows = DS.Tables("PARAMETROS").DefaultView.Item(0).Item("AutentificaWin")
                Else
                    'SE ABRE LA PANTALLA DE PARAMETROS 
                    Dim Frm As New frmParametros()
                    Frm.MdiParent = Me
                    Frm.Show()
                End If

            Else
                'SE ABRE LA PANTALLA DE PARAMETROS 
                Dim Frm As New frmParametros()
                Frm.MdiParent = Me
                Frm.Show()
            End If
            'se crea la cadena de conexion para el servidor a Afectar
            'If sAutentificacionWindows Then
            '    Inicio.CONSTR_COMPAQ = util.CreaConStrSQL(UCase(Parametro_ServidorxAfectar), UCase(Parametro_BaseDatosxAfectar), True, "", "")
            'Else
            '    Inicio.CONSTR_COMPAQ = util.CreaConStrSQL(UCase(Parametro_ServidorxAfectar), UCase(Parametro_BaseDatosxAfectar), False, sNomUsuarioSQL, sPassUsuarioSQL)
            'End If

            'ubicacion Empresa AdminPAQ
            'SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CONFIG_SECCION, KEY_CONFIF_KEY, sRutaEmpresaAdmPAQ)
            ConStrFox = CreaCadenaFox(sRutaEmpresaAdmPAQ)
            'ConStrFox = CreaCadenaFox(Mid(sRutaEmpresaAdmPAQ, 1, sRutaEmpresaAdmPAQ.Length - 1))

        End If

    End Sub

    Private Sub Timer1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        'Status.Panels.Item(1).Text = "Server: " & UCase(GetSetting("BASE", "ServerSQL", "Nombre", ""))
        Status.Panels.Item(3).Text = "Fecha: " & Format(DateTime.Now, "dd/MMMM/yyyy")
        Status.Panels.Item(4).Text = "Hora: " & Format(DateTime.Now, "hh:mm:ss tt")
        TSHora.Text = "Hora: " & Format(DateTime.Now, "hh:mm:ss tt")
    End Sub
    Private Sub MenuItem14_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        For i As Integer = 0 To Me.MdiChildren.Length - 1
            Me.MdiChildren(i).WindowState = FormWindowState.Minimized
        Next

    End Sub

    Private Sub MenuItem17_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        For i As Integer = 0 To Me.MdiChildren.Length - 1
            Me.MdiChildren(i).WindowState = FormWindowState.Normal
        Next
        Me.LayoutMdi(MdiLayout.Cascade)
    End Sub

    Private Sub MenuItem15_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        For i As Integer = 0 To Me.MdiChildren.Length - 1
            Me.MdiChildren(i).WindowState = FormWindowState.Normal
        Next
        Me.LayoutMdi(MdiLayout.Cascade)

    End Sub

    'Private Sub MenuItem16_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem16.Click
    '    Me.LayoutMdi(MdiLayout.TileHorizontal)
    'End Sub

    'Private Sub MenuItem11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem11.Click
    '    Dim frm As New frmCatUsuarios
    '    frm.MdiParent = Me
    '    frm.Show()
    'End Sub

    'Public Sub RecibeDatosUsuario(ByVal vUserId As String, ByVal vgrupo As String, ByVal vsNomUser As String, ByVal vsNomBd As String, ByVal vsNomServidor As String)
    '    UserId = vUserId
    '    GrupoUser = vgrupo
    '    NomUser = vsNomUser
    '    sNomBd = vsNomBd
    '    sNomServidor = vsNomServidor
    'End Sub
    Public Sub RecibeDatosUsuario(ByVal vUserId As String, ByVal vgrupo As String, ByVal vsNomUser As String, ByVal vsNomBd As String,
                                  ByVal vsNomServidor As String, ByVal AutentifWindows As Boolean, ByVal vsNombreEmpresa As String,
                                  ByVal vsRutaEmpresa As String, ByVal vsIdEmpresa As Integer, ByVal EsExterno As Boolean, ByVal CCODIGOC01_CLI As String)

        UserId = vUserId
        GrupoUser = vgrupo
        NomUser = vsNomUser
        sNomBd = vsNomBd
        sNomServidor = vsNomServidor
        sAutentificacionWindows = AutentifWindows
        'sNombreEmpresaAdmPAQ = Trim(vsNombreEmpresa)
        'sRutaEmpresaAdmPAQ = Mid(vsRutaEmpresa, 1, vsRutaEmpresa.Length - 1)
        'sIdEmpresa = vsIdEmpresa

        sNombreEmpresaAdmPAQ = Trim(vsNombreEmpresa)
        sRutaEmpresaAdmPAQ = vsRutaEmpresa
        sIdEmpresa = vsIdEmpresa


        'sEsExterno = EsExterno
        'sCCODIGOC01_CLI = CCODIGOC01_CLI

    End Sub


    'Public Sub CargaTreeUsuario(ByVal Usuario As String, ByVal ConStr As String, ByVal Opcion As String, ByVal Tipo As String, ByVal dbTabla As String)

    '    Dim strSQL As String
    '    Dim Resultado As String = ""
    '    Dim Tag As String = ""
    '    Try

    '        strSQL = "DECLARE @GRUPO AS VARCHAR(15) " & _
    '        " SET @GRUPO = (SELECT CVEGRUPO FROM USUARIOS WHERE USERID = '" & Usuario & "')" & _
    '        " SELECT USERID,ESTADO,MODULO, " & _
    '        " CAST(SUBSTRING(MODULO,1,2) AS INTEGER) AS NIVEL_0, " & _
    '        " CAST(SUBSTRING(MODULO,3,2) AS INTEGER) AS NIVEL_1,  " & _
    '        " CAST(SUBSTRING(MODULO,5,2) AS INTEGER) AS NIVEL_2, 1 AS ORDEN  " & _
    '        " FROM ACCESOS WHERE tipo = '" & Tipo & "' and USERID = (SELECT USERID FROM USUARIOS WHERE CVEGRUPO = 'GPO' AND USERID = @GRUPO) " & _
    '        " UNION " & _
    '        " SELECT USERID,ESTADO,MODULO, " & _
    '        " CAST(SUBSTRING(MODULO,1,2) AS INTEGER) AS NIVEL_0, " & _
    '        " CAST(SUBSTRING(MODULO,3,2) AS INTEGER) AS NIVEL_1, " & _
    '        " CAST(SUBSTRING(MODULO,5,2) AS INTEGER) AS NIVEL_2, 2 AS ORDEN " & _
    '        " FROM ACCESOS WHERE tipo = '" & Tipo & "' and USERID = (SELECT USERID FROM USUARIOS WHERE USERID = '" & Usuario & "') " & _
    '        " ORDER BY ORDEN "



    '        DsAccesos = Inicio.ChecaClavexSentencia(Inicio.ConStrProg, TipoConexion.TcSQL, strSQL, dbTabla)


    '        Dim ObjConection As New SqlConnection(ConStr)
    '        Dim ObjAdapter As New SqlDataAdapter(strSQL, ObjConection)
    '        DsAccesos.Clear()
    '        Dim Cuantos As Integer
    '        Cuantos = ObjAdapter.Fill(DsAccesos, TABLAACCES)
    '        If Cuantos > 0 Then
    '            Resultado = "CORRECTO"
    '        Else
    '            Resultado = "INCORRECTO"
    '        End If

    '        If Resultado = "CORRECTO" Then
    '            If Opcion = "HABILITA" Then
    '                AccesoxNivel(1, DsAccesos, Opcion)
    '                AccesoxNivel(2, DsAccesos, Opcion)
    '                AccesoxNivel(3, DsAccesos, Opcion)
    '            ElseIf Opcion = "TEXT" Then
    '                AccesoxNivel(3, DsAccesos, Opcion)
    '                AccesoxNivel(2, DsAccesos, Opcion)
    '                AccesoxNivel(1, DsAccesos, Opcion)
    '            ElseIf Opcion = "ESPECIAL" Then
    '                DsAccesos.Tables(TABLAACCES).DefaultView.RowFilter = Nothing
    '                'AccesosEsp()
    '            End If



    '        End If


    '        If Not ObjConection Is Nothing Then ObjConection.Close() : ObjConection.Dispose()
    '        If Not ObjAdapter Is Nothing Then ObjAdapter.Dispose()
    '    Catch e1 As SqlException
    '        MsgBox(e1.Message, MsgBoxStyle.Information, "SQL Error")
    '    Catch e1 As Exception
    '        MsgBox(e1.Message, MsgBoxStyle.Information, Me.Text)
    '    Finally

    '    End Try

    'End Sub

    'Private Function AccesoxNivel(ByVal nivel As Int16, ByVal DsAccesos As DataSet, ByVal opcion As String) As Boolean
    '    Dim Niv0, Niv1, Niv2 As Integer
    '    Dim I, MnuCol, MnuNiv As Integer
    '    Dim Valor As Boolean
    '    Dim mycontrol, mycontrol2 As System.Windows.Forms.MenuItem
    '    Dim Name, Nombre As String

    '    Try

    '        Select Case nivel
    '            Case 1
    '                'NIVEL 1
    '                DsAccesos.Tables(TABLAACCES).DefaultView.RowFilter = Nothing
    '                DsAccesos.Tables(TABLAACCES).DefaultView.RowFilter = "nivel_0 <> 0 and nivel_1 = 0 and nivel_2 = 0"
    '                If DsAccesos.Tables(TABLAACCES).DefaultView.Count > 0 Then
    '                    For I = 0 To DsAccesos.Tables(TABLAACCES).DefaultView.Count - 1
    '                        ' Name = DsAccesos.Tables(TABLAACCES).DefaultView.Item(I).Item("Nombre").ToString
    '                        Niv0 = DsAccesos.Tables(TABLAACCES).DefaultView.Item(I).Item("nivel_0") - 1
    '                        Tag = DsAccesos.Tables(TABLAACCES).DefaultView.Item(I).Item("modulo").ToString
    '                        Valor = DesEncriptaPer(DsAccesos.Tables(TABLAACCES).DefaultView.Item(I).Item("Estado").ToString)

    '                        For Each mycontrol In Me.Menu.MenuItems
    '                            If mycontrol.Text <> "-" Then
    '                                Name = Mid(mycontrol.Text, 1, 6)
    '                                Nombre = Mid(mycontrol.Text, 8)
    '                                If Tag = Name Then
    '                                    If opcion = "HABILITA" Then
    '                                        mycontrol.Enabled = Valor
    '                                    ElseIf opcion = "TEXT" Then
    '                                        mycontrol.Text = Nombre
    '                                    End If
    '                                    Exit For
    '                                End If
    '                            End If
    '                        Next

    '                        'Arbol.Nodes(Niv0).Checked = Valor
    '                    Next
    '                End If
    '            Case 2
    '                'NIVEL 2
    '                DsAccesos.Tables(TABLAACCES).DefaultView.RowFilter = Nothing
    '                DsAccesos.Tables(TABLAACCES).DefaultView.RowFilter = "nivel_0 <> 0 and nivel_1 <> 0 and nivel_2 = 0"
    '                If DsAccesos.Tables(TABLAACCES).DefaultView.Count > 0 Then
    '                    For I = 0 To DsAccesos.Tables(TABLAACCES).DefaultView.Count - 1
    '                        Niv0 = DsAccesos.Tables(TABLAACCES).DefaultView.Item(I).Item("nivel_0") - 1
    '                        Niv1 = DsAccesos.Tables(TABLAACCES).DefaultView.Item(I).Item("nivel_1") - 1
    '                        Tag = DsAccesos.Tables(TABLAACCES).DefaultView.Item(I).Item("modulo").ToString
    '                        Valor = DesEncriptaPer(DsAccesos.Tables(TABLAACCES).DefaultView.Item(I).Item("Estado").ToString)
    '                        For MnuCol = 0 To Me.Menu.MenuItems.Count - 1
    '                            For Each mycontrol In Me.Menu.MenuItems(MnuCol).MenuItems
    '                                If mycontrol.Text <> "-" Then
    '                                    Name = Mid(mycontrol.Text, 1, 6)
    '                                    Nombre = Mid(mycontrol.Text, 8)
    '                                    If Tag = Name Then
    '                                        If opcion = "HABILITA" Then
    '                                            mycontrol.Enabled = Valor
    '                                        ElseIf opcion = "TEXT" Then
    '                                            mycontrol.Text = Nombre
    '                                        End If
    '                                        Exit For
    '                                    End If
    '                                End If
    '                            Next

    '                        Next


    '                    Next
    '                End If
    '            Case 3
    '                'Nivel 3
    '                DsAccesos.Tables(TABLAACCES).DefaultView.RowFilter = Nothing
    '                DsAccesos.Tables(TABLAACCES).DefaultView.RowFilter = "nivel_0 <> 0 and nivel_1 <> 0 and nivel_2 <> 0"
    '                If DsAccesos.Tables(TABLAACCES).DefaultView.Count > 0 Then
    '                    MnuCol = 0
    '                    For I = 0 To DsAccesos.Tables(TABLAACCES).DefaultView.Count - 1
    '                        Niv0 = DsAccesos.Tables(TABLAACCES).DefaultView.Item(I).Item("nivel_0") - 1
    '                        Niv1 = DsAccesos.Tables(TABLAACCES).DefaultView.Item(I).Item("nivel_1") - 1
    '                        Niv2 = DsAccesos.Tables(TABLAACCES).DefaultView.Item(I).Item("nivel_2") - 1
    '                        Tag = DsAccesos.Tables(TABLAACCES).DefaultView.Item(I).Item("modulo").ToString
    '                        Valor = DesEncriptaPer(DsAccesos.Tables(TABLAACCES).DefaultView.Item(I).Item("Estado").ToString)
    '                        For MnuCol = 0 To Me.Menu.MenuItems.Count - 1
    '                            For Each mycontrol In Me.Menu.MenuItems(MnuCol).MenuItems
    '                                If mycontrol.Text <> "-" Then
    '                                    Name = Mid(mycontrol.Text, 1, 6)
    '                                    Nombre = Mid(mycontrol.Text, 8)
    '                                    MnuNiv = Val(Mid(Name, 3, 2)) - 1
    '                                    For Each mycontrol2 In Me.Menu.MenuItems(MnuCol).MenuItems(MnuNiv).MenuItems
    '                                        If mycontrol2.Text <> "-" Then
    '                                            Name = Mid(mycontrol2.Text, 1, 6)
    '                                            Nombre = Mid(mycontrol2.Text, 8)
    '                                            If Tag = Name Then
    '                                                If opcion = "HABILITA" Then
    '                                                    mycontrol2.Enabled = Valor
    '                                                ElseIf opcion = "TEXT" Then
    '                                                    mycontrol2.Text = Nombre
    '                                                End If
    '                                                Exit For
    '                                            End If
    '                                        End If
    '                                    Next
    '                                End If
    '                            Next

    '                        Next
    '                    Next
    '                End If
    '        End Select
    '    Catch e1 As SqlException
    '        MsgBox(e1.Message, MsgBoxStyle.Information, "SQL Error")
    '    Catch e1 As Exception
    '        MsgBox(e1.Message, MsgBoxStyle.Information, Me.Text)
    '    Finally

    '    End Try
    'End Function

    'Private Sub MenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem1.Click
    '    Dim frm As New frmC_Area
    '    frm.MdiParent = Me
    '    frm.Show()
    'End Sub





    'CONFIGURACION TABLAS!!!


    'Private Sub MenuItem6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    sNomBd = InputBox("Deme el nombre de la Base de Datos SQL:" & sNomBd, "Declarando SQL Server")
    'End Sub

    'Private Sub MnuMovCorFin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MnuMovCorFin.Click
    '    Dim frm As New frmM_CorridaFinanciera
    '    frm.MdiParent = Me
    '    frm.Show()
    'End Sub





    'Private Sub FMenu_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
    '    '27/ABR/2014
    '    'ToolStripInfoUser.Location = New Point(Me.Width - ToolStripInfoUser.Width - 40, 0)
    'End Sub

    Private Sub FMenu_ResizeEnd(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ResizeEnd
        If Me.WindowState = FormWindowState.Normal Then
            Me.WindowState = FormWindowState.Maximized
        End If
    End Sub
    Private Sub TSSesion_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'ToolStripInfoUser.Location = New Point(Me.Width - ToolStripInfoUser.Width - 40, 0)
    End Sub
    Private Function IniciarSesion() As Boolean
        frmLogin.Close()
        If frmLogin.ShowDialog = Windows.Forms.DialogResult.OK Then
            Return True
        Else
            Return False
        End If
    End Function


    Private Sub TSActuPerm_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSCerrarSesion.Click

        '{INI} comentado intencionalmente no requiere este codigo
        'If sender Is TSActuPerm Then
        '    UserConectado.ActualizaPermisos()
        '    AplicaPermisos()
        'Else
        If sender Is TSCerrarSesion Then
            If MessageBox.Show("Esta completamente Seguro que Desea Cerrar Sesion del Usuario: " & UserConectado.NombreUsuario & "???",
                               "Confirma que desea Cerrar Sesion???", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then Exit Sub
            TSSesion.Text = "<NO CONECTADO>"
            If IniciarSesion() = False Then End
            InformacionUser()
            AplicaPermisos()
            '{FIN} comentado intencionalmente no requiere este codigo
        End If
    End Sub
    'Private Sub TSActuPerm_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _
    '                  TSActuPerm.Click, TSCerrarSesion.Click

    '    If sender Is TSActuPerm Then
    '        UserConectado.ActualizaPermisos()
    '        AplicaPermisos()
    '    ElseIf sender Is TSCerrarSesion Then
    '        If MessageBox.Show("Esta completamente Seguro que Desea Cerrar Sesion del Usuario: " & UserConectado.NombreUsuario & "???",
    '                           "Confirma que desea Cerrar Sesion???", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then Exit Sub
    '        TSSesion.Text = "<NO CONECTADO>"
    '        If IniciarSesion() = False Then End
    '        InformacionUser()
    '        AplicaPermisos()
    '    End If
    'End Sub



    'Private Sub mnuConfParam_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Dim frm As New frmParametros(TryCast(sender, MenuItem).Tag)
    '    frm.MdiParent = Me
    '    frm.Show()
    'End Sub


    'Private Sub mnuRepEdoCta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    DespliegaReporte("EdoCuenta", Inicio.CONSTR)
    'End Sub

    'Private Sub mnuRepResPag_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    'DespliegaReporte("ResPagos", Inicio.CONSTR)
    '    DespliegaReporte("EdoCuentaDet", Inicio.CONSTR)
    'End Sub



    'Private Sub mnuRepAntSaldos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    DespliegaReporte("AntSaldos", Inicio.CONSTR)
    'End Sub

    'Private Sub MenuItem1_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    DespliegaReporte("AntSaldosDet", Inicio.CONSTR)
    'End Sub

    'Private Sub mnuRepSaldos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    DespliegaReporte("SaldosCapital", Inicio.CONSTR)
    'End Sub

    'Private Sub MnuSubirActu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    'FActuSis.ShowDialog()
    'End Sub


    'Private Sub MnuRepPuntos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    'Dim RpPnts As New RepPuntos("RELACION DE PUNTOS", "(SELECT P.idContrato Contrato,(Cli.ApePaterno + ' ' + Cli.ApeMaterno + ' ' + Cli.Nombres) NOMBRES, " &
    '    '        "(SELECT MAX(CAST(pg.FechaPunto AS DATE)) FROM Puntos pg WHERE pg.Puntos > 0 AND pg.idContrato = p.idContrato AND CAST(pg.FechaPunto as DATE) >= '2012/07/20' AND CAST(pg.FechaPunto as DATE) <= '2012/08/01') 'U. FEC. PUN. GEN.', " &
    '    '        "(SELECT MAX(CAST(pg.FechaPunto AS DATE)) FROM Puntos pg WHERE pg.Puntos < 0 AND pg.idContrato = p.idContrato AND CAST(pg.FechaPunto as DATE) >= '2012/07/20' AND CAST(pg.FechaPunto as DATE) <= '2012/08/01') 'U. FEC. PUN. CANJEA.', " &
    '    '        "(SELECT ISNULL(SUM(pg.Puntos),0) FROM Puntos pg WHERE pg.Puntos > 0 AND pg.idContrato = p.idContrato) 'PUNTOS ACUMULADOS', " &
    '    '        "(SELECT ISNULL(SUM(pg.Puntos),0) FROM Puntos pg WHERE pg.Puntos > 0 AND pg.idContrato = p.idContrato AND CAST(pg.FechaPunto as DATE) >= '2012/07/20' AND CAST(pg.FechaPunto as DATE) <= '2012/08/01') 'PUNTOS GENERADOS', " &
    '    '        "(SELECT ABS(ISNULL(SUM(pg.Puntos),0)) FROM Puntos pg WHERE pg.Puntos < 0 AND pg.idContrato = p.idContrato AND CAST(pg.FechaPunto as DATE) >= '2012/07/20' AND CAST(pg.FechaPunto as DATE) <= '2012/08/01') 'PUNTOS CANJEADOS', " &
    '    '        "(SELECT ISNULL(SUM(pg.Puntos),0) FROM Puntos pg WHERE  pg.idContrato = p.idContrato) 'SALDO EN PUNTOS' " &
    '    '        "FROM puntos p LEFT JOIN c_Clientes Cli ON P.idCliente = Cli.idCliente " &
    '    '        "WHERE CAST(P.FechaPunto as DATE) >= '2012/07/20' AND CAST(P.FechaPunto as DATE) <= '2012/08/01' " &
    '    '        "Group By p.idContrato,Cli.ApePaterno,Cli.ApeMaterno,Cli.Nombres) UNION ALL(SELECT '','TOTAL de Clientes: %c',NULL,NULL,-999,-999,-999,-999)", "Titulo Tabla1", "Titulo Tabla2", Now,
    '    '        {100, 250, 100, 100, 100, 100, 100, 100},
    '    '          {StringAlignment.Near, StringAlignment.Near, StringAlignment.Center, StringAlignment.Center, StringAlignment.Center,
    '    '           StringAlignment.Center, StringAlignment.Center, StringAlignment.Center})
    '    'RpPnts.Imprimir(True)
    '    DespliegaReporte("REPPUNTOS", CONSTR)
    'End Sub




    'Private Sub MenuSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    fCierraEmpresa()
    '    fTerminaSDK()
    '    Me.Close()
    'End Sub

    'Private Sub mnuCatRegalos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCatRegalos.Click
    '    Dim frm As New frmCatPremios(TryCast(sender, MenuItem).Tag)
    '    frm.MdiParent = Me
    '    frm.Show()

    'End Sub


    'Private Sub mnuCatPromociones_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCatPromociones.Click
    '    Dim frm As New frmCatPromociones(TryCast(sender, MenuItem).Tag)
    '    frm.MdiParent = Me
    '    frm.Show()
    'End Sub

    'Private Sub mnuUbicaCompaq_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUbicaCompaq.Click
    '    If MsgBox("!! Esta seguro que desea cambiar la ubicaci�n de la fuente de datos : " & UbicacionAdminPaq & "? !!", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
    '        UbicacionAdminPaq = SeleccionaCarpeta()
    '        SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CONFIG_SECCION, KEY_CONFIF_KEY, UbicacionAdminPaq)
    '        ConStrFox = CreaCadenaFox(UbicacionAdminPaq)
    '        Status.Panels.Item(1).Text = "Ubicacion Archivos: " & UbicacionAdminPaq
    '        MsgBox("Se Cambio la ubicaci�n de la fuente de datos a: " & UbicacionAdminPaq)
    '    End If

    'End Sub



    'Private Sub mnuCatTarjetas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCatTarjetas.Click
    '    Dim frm As New frmCatTarjetas(TryCast(sender, MenuItem).Tag)
    '    frm.MdiParent = Me
    '    frm.Show()
    'End Sub


    'Private Sub mnuMovRegistro_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuMovRegistro.Click
    '    Dim frm As New frmRegistroPuntos(TryCast(sender, MenuItem).Tag)
    '    frm.MdiParent = Me
    '    frm.Show()
    'End Sub

    'Private Sub mnuMovCanje_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuMovCanje.Click
    '    Dim frm As New frmCanjePuntos(TryCast(sender, MenuItem).Tag)
    '    frm.MdiParent = Me
    '    frm.Show()
    'End Sub

    Private Sub mnuParametro_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuParametro.Click
        Dim frm As New frmParametros(TryCast(sender, MenuItem).Tag)
        frm.MdiParent = Me
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.Show()
    End Sub

    'Private Sub mnuReimprime_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuReimprime.Click
    '    Dim frm As New frmReimpresion(TryCast(sender, MenuItem).Tag)
    '    frm.MdiParent = Me
    '    frm.Show()
    'End Sub

    'Private Sub mnuRepCatCli_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuRepCatCli.Click
    '    DespliegaReporte("CLIENTES", Inicio.CONSTR)
    'End Sub

    'Private Sub mnuRepEdoCtaCli_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuRepEdoCtaCli.Click
    '    DespliegaReporte("EDOCTACLI", Inicio.CONSTR)
    'End Sub

    'Private Sub mnuRepSalxCli_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuRepSalxCli.Click
    '    DespliegaReporte("SALDOSXCLI", Inicio.CONSTR)
    'End Sub

    'Private Sub mnuRepRecVen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuRepRecVen.Click
    '    DespliegaReporte("RECVEN", Inicio.CONSTR)
    'End Sub

    'Private Sub mnuRepOpexDia_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuRepOpexDia.Click
    '    DespliegaReporte("OPEXDIA", Inicio.CONSTR)
    'End Sub

    'Private Sub mnuRepOpexCon_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuRepOpexCon.Click
    '    DespliegaReporte("OPEXCON", Inicio.CONSTR)
    'End Sub

    'Private Sub mnuUsuarios_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUsuarios.Click
    '    Dim frm As New frmUsuarios(TryCast(sender, MenuItem).Tag)
    '    frm.MdiParent = Me
    '    frm.Show()
    'End Sub

    Private Sub mnuUsuarios_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUsuarios.Click
        Dim frm As New frmUsuarios(TryCast(sender, MenuItem).Tag)
        frm.MdiParent = Me
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.Show()
    End Sub

    'Public Sub mnuCatListaPrecios_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Dim frm As New frmListaPrecios(TryCast(sender, MenuItem).Tag)
    '    frm.MdiParent = Me
    '    frm.StartPosition = FormStartPosition.CenterScreen
    '    frm.Show()
    'End Sub

    Private Sub mnuUbicaCompaq_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUbicaCompaq.Click

        MsgBox("La Ruta de La empresa es : " & sRutaEmpresaAdmPAQ, vbInformation, "Aviso " & Me.Text)

        'MsgBox("La Ruta de La empresa es : " & sRutaEmpresaAdmPAQ & vbCrLf & "Cadena Conexion Fox: " & Inicio.ConStrFox, vbInformation, "Aviso " & Me.Text)
        'If MsgBox("!! Esta seguro que desea cambiar la ubicaci�n de la fuente de datos : " & UbicacionAdminPaq & "? !!", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
        '    UbicacionAdminPaq = SeleccionaCarpeta()
        '    SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CONFIG_SECCION, KEY_CONFIF_KEY, UbicacionAdminPaq)
        '    ConStrFox = CreaCadenaFox(UbicacionAdminPaq)
        '    Status.Panels.Item(1).Text = "Ubicacion Archivos: " & UbicacionAdminPaq
        '    MsgBox("Se Cambio la ubicaci�n de la fuente de datos a: " & UbicacionAdminPaq)
        'End If
    End Sub

    'Private Sub mnuMovRegistro_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Dim frm As New frmReimpresion(TryCast(sender, MenuItem).Tag)
    '    frm.MdiParent = Me
    '    frm.StartPosition = FormStartPosition.CenterScreen
    '    frm.Show()
    'End Sub

    'Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Dim frm As New frmVentasFinal(TryCast(sender, MenuItem).Tag)
    '    frm.MdiParent = Me
    '    'frm.Location = New Point(frm.Location.X, frm.Location.Y)

    '    frm.StartPosition = FormStartPosition.Manual
    '    frm.Location = New Point(10, 35)
    '    frm.Show()

    'End Sub

    'Private Sub MenuItem3_Click(sender As Object, e As EventArgs)
    '    DespliegaReporte("PEDIDOSPEN", Inicio.CONSTR)
    'End Sub


    'Private Sub mnuCatGrupos_Click(sender As Object, e As EventArgs)
    '    Dim frm As New frmCatGrupos(TryCast(sender, MenuItem).Tag)
    '    frm.MdiParent = Me
    '    frm.StartPosition = FormStartPosition.CenterScreen
    '    frm.Show()
    'End Sub

    Private Sub MenuSalir_Click(sender As Object, e As EventArgs) Handles MenuSalir.Click
        GrabaEmpresa()
        If bEmpresaSDKAbierta Then
            fCierraEmpresa()
        End If
        If bActivoSDK Then
            fTerminaSDK()
        End If

        Me.Close()
    End Sub

    Public Sub llamarform(ByVal formulario As Form, ByVal mdi As Form)

        Dim frmllamado As Form

        Dim escargado As Boolean = False

        Try

            'Chequea si el formulario ha sido cargado

            For Each frmllamado In mdi.MdiChildren

                'Compara si es igual y retorna verdadero si lo es.

                If frmllamado.Text = formulario.Text Then

                    escargado = True   'Marca la bandera

                    Exit For      'Sale del loop si es verdadero

                End If

            Next

            If Not escargado Then


                'Carga el formulario si no esta llamado
                formulario.MdiParent = mdi

                formulario.Show()

            ElseIf escargado Then

                frmllamado.Focus()   'Enfoca el objeto

            End If

        Catch ex As Exception

            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")

        End Try

        formulario = Nothing  'Limpiar todo

        frmllamado = Nothing

    End Sub

    Private Sub mnuReimpFact_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub mnuReimpRem_Click(sender As Object, e As EventArgs)
        'DespliegaReporte("REMISION", Inicio.CONSTR)
    End Sub

    'Private Sub mnuVentasExt_Click(sender As Object, e As EventArgs)
    '    If sEsExterno Then
    '        Dim frm As New frmVentasExterno(TryCast(sender, MenuItem).Tag)
    '        frm.MdiParent = Me
    '        'frm.Location = New Point(frm.Location.X, frm.Location.Y)

    '        frm.StartPosition = FormStartPosition.Manual
    '        frm.Location = New Point(10, 35)
    '        frm.Show()
    '    Else
    '        MsgBox("El Usuario " & UserId & " No tiene Acceso a Esta Opci�n" & vbCrLf & "Consultelo con el administrador del Sistema, Gracias")

    '    End If

    'End Sub

    Private Sub GrabaEmpresa()
        'BD = New CapaDatos.UtilSQL(CONSTR, NomUser) 'Se inicializa la Clase BD
        Try
            Dim STRSQL As String = "UPDATE PARAMETROS SET CIDEMPRESA=" & sIdEmpresa & ", CNOMBREE01 = '" & Trim(sNombreEmpresaAdmPAQ) & "', CRUTADATOS = '" & sRutaEmpresaAdmPAQ & "' WHERE IDPARAMETRO = 1"

            BD.Execute(STRSQL, False)
        Catch ex As Exception

        End Try

        'BD.Transaccion()
    End Sub


    Private Sub MenuItem2_Click(sender As Object, e As EventArgs)
        'Dim frm As New frmCargaExcel(TryCast(sender, MenuItem).Tag)
        'frm.MdiParent = Me
        'frm.StartPosition = FormStartPosition.CenterScreen
        'frm.Show()
    End Sub

    Private Sub mnuMovTras_Click(sender As Object, e As EventArgs)
        'Dim frm As New frmTraspasos(TryCast(sender, MenuItem).Tag)
        'frm.MdiParent = Me
        'frm.StartPosition = FormStartPosition.CenterScreen
        'frm.Show()
    End Sub

    Private Sub mnuMovReTras_Click(sender As Object, e As EventArgs)
        'Dim frm As New frmTraspasosAutom(TryCast(sender, MenuItem).Tag)
        'frm.MdiParent = Me
        'frm.StartPosition = FormStartPosition.CenterScreen
        'frm.Show()
    End Sub



    Private Sub mnuRepRei_Click(sender As Object, e As EventArgs)
        Inicio.ImprimeRemision(False, 0)
    End Sub

    Private Sub mnuRepTraFal_Click(sender As Object, e As EventArgs)
        DespliegaReporte("TRASPFALTANTES", Inicio.CONSTR)
    End Sub

    Private Sub mnuRepTraFalIni_Click(sender As Object, e As EventArgs)
        DespliegaReporte("TRASPFALTANTESINI", Inicio.CONSTR)
    End Sub


    Private Sub mnuCatMarcas_Click(sender As Object, e As EventArgs) Handles mnuCatMarcas.Click
        Dim frm As New frmCatLlantasProd(TryCast(sender, MenuItem).Tag)
        frm.MdiParent = Me
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.Show()
    End Sub

    Private Sub mnuCatDivision_Click(sender As Object, e As EventArgs) Handles mnuCatDivision.Click
        Dim frm As New frmCatDivision(TryCast(sender, MenuItem).Tag)
        frm.MdiParent = Me
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.Show()
    End Sub

    Private Sub mnuCatPais_Click(sender As Object, e As EventArgs) Handles mnuCatPais.Click
        Dim frm As New frmCatPais(TryCast(sender, MenuItem).Tag)
        frm.MdiParent = Me
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.Show()
    End Sub

    Private Sub mnuCatPuesto_Click(sender As Object, e As EventArgs) Handles mnuCatPuesto.Click
        Dim frm As New frmCatPuestos(TryCast(sender, MenuItem).Tag)
        frm.MdiParent = Me
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.Show()
    End Sub

    Private Sub mnuCatTipHerr_Click(sender As Object, e As EventArgs) Handles mnuCatTipHerr.Click
        Dim frm As New frmCatDestinos(TryCast(sender, MenuItem).Tag)
        frm.MdiParent = Me
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.Show()
    End Sub

    Private Sub mnuCatTipOrd_Click(sender As Object, e As EventArgs) Handles mnuCatTipOrd.Click
        Dim frm As New frmCatTipoOrden(TryCast(sender, MenuItem).Tag)
        frm.MdiParent = Me
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.Show()
    End Sub

    Private Sub mnuCatTipServ_Click(sender As Object, e As EventArgs) Handles mnuCatTipServ.Click
        Dim frm As New frmCatTipoServ(TryCast(sender, MenuItem).Tag)
        frm.MdiParent = Me
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.Show()
    End Sub


    Private Sub mnuCatFam_Click(sender As Object, e As EventArgs) Handles mnuCatFam.Click
        Dim frm As New frmCatFamilias(TryCast(sender, MenuItem).Tag)
        frm.MdiParent = Me
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.Show()
    End Sub

    Private Sub mnuCatTipCiu_Click(sender As Object, e As EventArgs) Handles mnuCatCiu.Click
        Dim frm As New frmCatCiudad(TryCast(sender, MenuItem).Tag)
        frm.MdiParent = Me
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.Show()
    End Sub

    Private Sub mnuCatEdo_Click(sender As Object, e As EventArgs) Handles mnuCatEdo.Click
        Dim frm As New frmCatEstados(TryCast(sender, MenuItem).Tag)
        frm.MdiParent = Me
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.Show()
    End Sub

    Private Sub mnuCatEmpresa_Click(sender As Object, e As EventArgs) Handles mnuCatEmpresa.Click
        'Dim frm As New frmCatEstado(TryCast(sender, MenuItem).Tag)
        'frm.MdiParent = Me
        'frm.StartPosition = FormStartPosition.CenterScreen
        'frm.Show()
    End Sub


    Private Sub mnuCatSuc_Click(sender As Object, e As EventArgs) Handles mnuCatSuc.Click
        Dim frm As New frmCatSucursal(TryCast(sender, MenuItem).Tag)
        frm.MdiParent = Me
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.Show()
    End Sub

    Private Sub mnuCatAlm_Click(sender As Object, e As EventArgs) Handles mnuCatAlm.Click
        Dim frm As New frmCatAlmacen(TryCast(sender, MenuItem).Tag)
        frm.MdiParent = Me
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.Show()
    End Sub

    Private Sub mnuCatColonias_Click(sender As Object, e As EventArgs) Handles mnuCatColonias.Click
        Dim frm As New frmCatColonias(TryCast(sender, MenuItem).Tag)
        frm.MdiParent = Me
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.Show()
    End Sub


    Private Sub mnuCatActividad_Click(sender As Object, e As EventArgs) Handles mnuCatActividad.Click
        Dim frm As New fOrdenTrab(TryCast(sender, MenuItem).Tag)
        frm.MdiParent = Me
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.Show()
    End Sub

    Private Sub mnuCatTipoUniTras_Click(sender As Object, e As EventArgs) Handles mnuCatTipoUniTras.Click
        Dim frm As New frmCatClasLlan(TryCast(sender, MenuItem).Tag)
        frm.MdiParent = Me
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.Show()
    End Sub

    Private Sub mnuCatProv_Click(sender As Object, e As EventArgs) Handles mnuCatProv.Click
        Dim frm As New frmCatClientes(TryCast(sender, MenuItem).Tag)
        frm.MdiParent = Me
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.Show()
    End Sub

    Private Sub mnuCatHerr_Click(sender As Object, e As EventArgs) Handles mnuCatHerr.Click
        Dim frm As New frmCatHerramienta(TryCast(sender, MenuItem).Tag)
        frm.MdiParent = Me
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.Show()
    End Sub

    Private Sub mnuCatSer_Click(sender As Object, e As EventArgs) Handles mnuCatSer.Click
        Dim frm As New frmCatUniTransp(TryCast(sender, MenuItem).Tag)
        frm.MdiParent = Me
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.Show()
    End Sub

    Private Sub MenuItem1_Click(sender As Object, e As EventArgs) Handles mnuMovDiag.Click
        'Dim frm As New frmDiagnosticoTaller(TryCast(sender, MenuItem).Tag)
        'frm.MdiParent = Me
        'frm.StartPosition = FormStartPosition.CenterScreen
        'frm.Show()
    End Sub

    Private Sub mnuMovRecep_Click(sender As Object, e As EventArgs) Handles mnuMovRecep.Click
        OpenMainWindow("MENU_MOVIMIENTOS_RECEPCION", True)
    End Sub

    'Dim usuario As New Core.Models.Usuario With
    '  {.sNomUser = NomUser,
    '  .sUserId = UserId,
    '  .AsignaActividad = False,
    '  .EsGrupo = True,
    '  .AutorizaCompra = False,
    '  .bActivo = True,
    '  .capturaRecepcion = False,
    '  .esTaller = True
    '  }

    'Private Sub mapUsuario()
    '    Usuario = New Core.Models.Usuario With
    '  {.sNomUser = NomUser,
    '  .sUserId = UserId,
    '  .AsignaActividad = False,
    '  .EsGrupo = True,
    '  .AutorizaCompra = False,
    '  .bActivo = True,
    '  .capturaRecepcion = False,
    '  .esTaller = True
    '  }
    'End Sub

    Private Sub OpenMainWindow(ByVal ventana As String, ByVal esTaller As Boolean)
        'mapUsuario()
        'Try
        '    MessageBox.Show(Usuario.sUserId)
        '    Usuario.personal = mapPersonal(Usuario.sUserId)
        'Catch ex As Exception

        'End Try
        'Try
        '    If Usuario.personal.empresa IsNot Nothing Then
        '        Dim maninWindow As New MainWindow(Usuario, esTaller)
        '        maninWindow.createView(ventana)
        '        maninWindow.ShowDialog()
        '    End If

        'Catch ex As Exception
        '    MsgBox(ex.Message)
        'End Try
    End Sub

    Public Sub OpenMainWindow(ByVal ventana As String, ByVal esTaller As Boolean, ByVal clave As Integer)
        'mapUsuario()
        Try
            'MessageBox.Show(usuario.sUserId)
            'usuario.personal = mapPersonal(usuario.sUserId)
        Catch ex As Exception

        End Try
        Try
            'If usuario.personal.empresa IsNot Nothing Then
            '    Dim maninWindow As New MainWindow(usuario, esTaller, clave)
            '    maninWindow.createView(ventana)
            '    maninWindow.ShowDialog()
            'End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    'Private Function mapPersonal(sUserId As String) As Personal
    '    Try
    '        Dim service As OperationResult = New PersonalSvc().getPersonalByUsuario(sUserId)
    '        If service.typeResult = ResultTypes.error Then
    '            MessageBox.Show(service.mensaje, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '            Return New Personal
    '        End If
    '        If service.typeResult = ResultTypes.warning Then
    '            MessageBox.Show(service.mensaje, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning)
    '            Return New Personal
    '        End If
    '        If service.typeResult = ResultTypes.recordNotFound Then
    '            MessageBox.Show(service.mensaje + " El usuario no tiene asignado una empresa", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '            Return New Personal
    '        End If
    '        If service.typeResult = ResultTypes.success Then
    '            Return TryCast(service.result, Personal)
    '        End If
    '        Return New Personal
    '    Catch ex As Exception
    '        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '        Return New Personal
    '    End Try
    'End Function


    'Public usuario As New Core.Models.Usuario With
    '   {.sNomUser = NomUser,
    '   .sUserId = UserId,
    '   .AsignaActividad = False,
    '   .EsGrupo = True,
    '   .AutorizaCompra = False,
    '   .bActivo = True,
    '   .capturaRecepcion = False,
    '   .esTaller = True
    '   }

    Private Sub mnuMovAsigOT_Click(sender As Object, e As EventArgs) Handles mnuMovAsigOT.Click
        OpenMainWindow("MENU_MOVIMIENTOS_DIAGNOSTICO", False)
    End Sub


    Private Sub mnuConfMig_Click(sender As Object, e As EventArgs) Handles mnuConfMig.Click
        Dim frm As New FMigraComercial(TryCast(sender, MenuItem).Tag)
        frm.MdiParent = Me
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.Show()
    End Sub

    Private Sub mnuCatPer_Click(sender As Object, e As EventArgs) Handles mnuCatPer.Click
        Dim frm As New frmCatPersonal(TryCast(sender, MenuItem).Tag)
        frm.MdiParent = Me
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.Show()
    End Sub

    Private Sub MenuItem1_Click_1(sender As Object, e As EventArgs) Handles MenuItem1.Click
        OpenMainWindow("MENU_MOVIMIENTOS_RECEPCION", False)
    End Sub

    Private Sub MenuItem2_Click_1(sender As Object, e As EventArgs) Handles MenuItem2.Click
        Dim frm As New FMonitorGuardias(TryCast(sender, MenuItem).Tag)
        frm.MdiParent = Me
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.Show()
    End Sub

    Private Sub MenuItem3_Click(sender As Object, e As EventArgs) Handles MenuItem3.Click
        Dim frm As New fFacturaViajesAtl(TryCast(sender, MenuItem).Tag)
        frm.MdiParent = Me
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.Show()
    End Sub
End Class
