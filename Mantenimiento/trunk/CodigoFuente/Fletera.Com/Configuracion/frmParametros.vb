'Fecha Creacion: 
'Fecha Modificacion : 
Imports System.IO
Public Class frmParametros
    Inherits System.Windows.Forms.Form

    Dim OpcionForma As TipoOpcionForma
    Private _tag As String

    Dim TablaBd As String = "parametros"
    Dim CampoLLave As String = "IdParametro"
    Dim sPosibleError As String

    Dim Existe As String

    Protected DS_Param As New DataSet
    'Protected DS As New DataSet

    Dim indice As Integer = 0
    Dim ArraySql() As String
    Dim NuevoParametros As Boolean = False
    Dim vPassEmail As String = ""
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents txtPuertoSMTP As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txtPuertoPOP3 As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txtServidorSMTP As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txtServidorPOP3 As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents btnVerPassEmail As System.Windows.Forms.Button
    Friend WithEvents txtEmailPassEmpresa As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtEmailEmpresa As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents chkConexionSSL As System.Windows.Forms.CheckBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents txtMensajeCorreo As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txtPasswordFacturacion As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Dim vPassUserSQl As String = ""
    Friend WithEvents btnVerPassFacturacion As System.Windows.Forms.Button
    Dim vPassFacturacion As String = ""
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents txtCNOMBREC01 As System.Windows.Forms.TextBox
    Friend WithEvents cmdBuscaConcepto As System.Windows.Forms.Button
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents txtidConcepto As System.Windows.Forms.TextBox
    Friend WithEvents grdConceptos As System.Windows.Forms.DataGridView
    Dim DsComboFox As New DataSet
    Dim Salida As Boolean
    Dim vIndiceConcepto As Integer = 0
    Dim vIndiceConceptoExt As Integer = 0

    'Dim vCCODIGOC01 As String
    Dim vCIDCONCE01 As String
    Dim vCIDDOCUM01 As Integer
    Dim vCPLAMIGCFD As String
    Dim vCDOCTOAC01 As Boolean

    Dim vCIDCONCE01Ext As String
    Dim vCIDDOCUM01Ext As Integer
    Dim vCPLAMIGCFDExt As String
    Dim vCDOCTOAC01Ext As Boolean
    'CDOCTOAC01


    Dim myDataColumn As DataColumn
    Dim myDataRow As DataRow
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents lblNomBodegaSalDefault As System.Windows.Forms.TextBox
    Friend WithEvents cmdBuscaGrupo As System.Windows.Forms.Button
    Friend WithEvents txtBodegaSalDefault As System.Windows.Forms.TextBox
    Friend WithEvents TabPage5 As System.Windows.Forms.TabPage
    Friend WithEvents txtColaImpresionPed As System.Windows.Forms.TextBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents txtColaImpresionRem As System.Windows.Forms.TextBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents txtColaImpresionFact As System.Windows.Forms.TextBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents gConsecutivo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gCIDCONCE01 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gCCODIGOC01 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gCNOMBREC01 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gCIDDOCUM01 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gCPLAMIGCFD As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gCDOCTOAC01 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnMnuOk As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuCancelar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnMnuSalir As System.Windows.Forms.ToolStripButton
    Friend WithEvents chkImprimeRegistro As System.Windows.Forms.CheckBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Dim MyTablaConceptos As DataTable = New DataTable(TablaBd)
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Public WithEvents cbEmpresa As System.Windows.Forms.ComboBox
    Dim MyTablaConceptosExt As DataTable = New DataTable(TablaBd)

    Public MyTablaEmpresas As DataTable = New DataTable(TablaBd)
    Friend WithEvents btnFormatoImp As System.Windows.Forms.Button
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents txtFormatoFac As System.Windows.Forms.TextBox
    Dim vIdEmpresa As Integer
    Dim vCIDALMACEN As Integer = 0


    Sub New(ByVal vTag As String)
        InitializeComponent()
        _tag = vTag
    End Sub

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents chkAutWindows As System.Windows.Forms.CheckBox
    Friend WithEvents txtPasswordSQL As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtDiasSaldoVenc As System.Windows.Forms.TextBox
    Friend WithEvents btnVerPassSQL As System.Windows.Forms.Button
    Friend WithEvents txtUsuarioSQL As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents txtRFC As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtDireccion As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtEmpresa As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtTelefono As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents ToolStripMenu As System.Windows.Forms.ToolStrip
    Friend WithEvents comboMnu As System.Windows.Forms.ToolStripComboBox
    Friend WithEvents btnMnuModificar As System.Windows.Forms.ToolStripButton
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmParametros))
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.txtFormatoFac = New System.Windows.Forms.TextBox()
        Me.btnFormatoImp = New System.Windows.Forms.Button()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.cbEmpresa = New System.Windows.Forms.ComboBox()
        Me.chkImprimeRegistro = New System.Windows.Forms.CheckBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtUsuarioSQL = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.btnVerPassSQL = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.chkAutWindows = New System.Windows.Forms.CheckBox()
        Me.txtPasswordSQL = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ToolStripMenu = New System.Windows.Forms.ToolStrip()
        Me.comboMnu = New System.Windows.Forms.ToolStripComboBox()
        Me.btnMnuModificar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuOk = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuCancelar = New System.Windows.Forms.ToolStripButton()
        Me.btnMnuSalir = New System.Windows.Forms.ToolStripButton()
        Me.txtDiasSaldoVenc = New System.Windows.Forms.TextBox()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.txtBodegaSalDefault = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.lblNomBodegaSalDefault = New System.Windows.Forms.TextBox()
        Me.cmdBuscaGrupo = New System.Windows.Forms.Button()
        Me.btnVerPassFacturacion = New System.Windows.Forms.Button()
        Me.txtPasswordFacturacion = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.txtTelefono = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtRFC = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtDireccion = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtEmpresa = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.txtMensajeCorreo = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.chkConexionSSL = New System.Windows.Forms.CheckBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txtPuertoSMTP = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txtPuertoPOP3 = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtServidorSMTP = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtServidorPOP3 = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.btnVerPassEmail = New System.Windows.Forms.Button()
        Me.txtEmailPassEmpresa = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtEmailEmpresa = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.txtCNOMBREC01 = New System.Windows.Forms.TextBox()
        Me.cmdBuscaConcepto = New System.Windows.Forms.Button()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.txtidConcepto = New System.Windows.Forms.TextBox()
        Me.grdConceptos = New System.Windows.Forms.DataGridView()
        Me.gConsecutivo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gCIDCONCE01 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gCCODIGOC01 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gCNOMBREC01 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gCIDDOCUM01 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gCPLAMIGCFD = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gCDOCTOAC01 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.TabPage5 = New System.Windows.Forms.TabPage()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtColaImpresionPed = New System.Windows.Forms.TextBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.txtColaImpresionRem = New System.Windows.Forms.TextBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.txtColaImpresionFact = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.GroupBox2.SuspendLayout()
        Me.ToolStripMenu.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.TabPage4.SuspendLayout()
        CType(Me.grdConceptos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage5.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.txtFormatoFac)
        Me.GroupBox2.Controls.Add(Me.btnFormatoImp)
        Me.GroupBox2.Controls.Add(Me.Label26)
        Me.GroupBox2.Controls.Add(Me.Label25)
        Me.GroupBox2.Controls.Add(Me.cbEmpresa)
        Me.GroupBox2.Controls.Add(Me.chkImprimeRegistro)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.txtUsuarioSQL)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.btnVerPassSQL)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.chkAutWindows)
        Me.GroupBox2.Controls.Add(Me.txtPasswordSQL)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.ForeColor = System.Drawing.Color.Black
        Me.GroupBox2.Location = New System.Drawing.Point(6, 6)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(533, 255)
        Me.GroupBox2.TabIndex = 18
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Conexi�n SQL"
        '
        'txtFormatoFac
        '
        Me.txtFormatoFac.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtFormatoFac.Location = New System.Drawing.Point(134, 115)
        Me.txtFormatoFac.MaxLength = 250
        Me.txtFormatoFac.Name = "txtFormatoFac"
        Me.txtFormatoFac.Size = New System.Drawing.Size(367, 20)
        Me.txtFormatoFac.TabIndex = 69
        Me.txtFormatoFac.Visible = False
        '
        'btnFormatoImp
        '
        Me.btnFormatoImp.Location = New System.Drawing.Point(505, 113)
        Me.btnFormatoImp.Name = "btnFormatoImp"
        Me.btnFormatoImp.Size = New System.Drawing.Size(22, 23)
        Me.btnFormatoImp.TabIndex = 68
        Me.btnFormatoImp.Text = "..."
        Me.btnFormatoImp.UseVisualStyleBackColor = True
        Me.btnFormatoImp.Visible = False
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.ForeColor = System.Drawing.Color.Black
        Me.Label26.Location = New System.Drawing.Point(6, 118)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(126, 13)
        Me.Label26.TabIndex = 67
        Me.Label26.Text = "4.- Formato para Facturar"
        Me.Label26.Visible = False
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.ForeColor = System.Drawing.Color.Black
        Me.Label25.Location = New System.Drawing.Point(6, 88)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(123, 13)
        Me.Label25.TabIndex = 65
        Me.Label25.Text = "3.- Empresa ADMINPAQ"
        '
        'cbEmpresa
        '
        Me.cbEmpresa.BackColor = System.Drawing.SystemColors.Window
        Me.cbEmpresa.Cursor = System.Windows.Forms.Cursors.Default
        Me.cbEmpresa.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cbEmpresa.Location = New System.Drawing.Point(134, 84)
        Me.cbEmpresa.Name = "cbEmpresa"
        Me.cbEmpresa.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cbEmpresa.Size = New System.Drawing.Size(297, 21)
        Me.cbEmpresa.TabIndex = 64
        '
        'chkImprimeRegistro
        '
        Me.chkImprimeRegistro.AutoSize = True
        Me.chkImprimeRegistro.Checked = True
        Me.chkImprimeRegistro.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkImprimeRegistro.Location = New System.Drawing.Point(192, 194)
        Me.chkImprimeRegistro.Name = "chkImprimeRegistro"
        Me.chkImprimeRegistro.Size = New System.Drawing.Size(15, 14)
        Me.chkImprimeRegistro.TabIndex = 63
        Me.chkImprimeRegistro.UseVisualStyleBackColor = True
        Me.chkImprimeRegistro.Visible = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ForeColor = System.Drawing.Color.Black
        Me.Label3.Location = New System.Drawing.Point(6, 191)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(146, 13)
        Me.Label3.TabIndex = 62
        Me.Label3.Text = "4.- Imprime Hoja de Registro :"
        Me.Label3.Visible = False
        '
        'txtUsuarioSQL
        '
        Me.txtUsuarioSQL.Location = New System.Drawing.Point(192, 12)
        Me.txtUsuarioSQL.MaxLength = 50
        Me.txtUsuarioSQL.Name = "txtUsuarioSQL"
        Me.txtUsuarioSQL.Size = New System.Drawing.Size(239, 20)
        Me.txtUsuarioSQL.TabIndex = 1
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.ForeColor = System.Drawing.Color.Black
        Me.Label5.Location = New System.Drawing.Point(6, 16)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(99, 13)
        Me.Label5.TabIndex = 58
        Me.Label5.Text = "1.- Usuario del SQL"
        '
        'btnVerPassSQL
        '
        Me.btnVerPassSQL.Location = New System.Drawing.Point(443, 40)
        Me.btnVerPassSQL.Name = "btnVerPassSQL"
        Me.btnVerPassSQL.Size = New System.Drawing.Size(31, 33)
        Me.btnVerPassSQL.TabIndex = 56
        Me.btnVerPassSQL.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.ForeColor = System.Drawing.Color.Black
        Me.Label4.Location = New System.Drawing.Point(6, 150)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(395, 13)
        Me.Label4.TabIndex = 53
        Me.Label4.Text = "3.- Determina si requiere Autentificaci�n de Windows por default al inicio de ses" & _
    "i�n"
        Me.Label4.Visible = False
        '
        'chkAutWindows
        '
        Me.chkAutWindows.AutoSize = True
        Me.chkAutWindows.Checked = True
        Me.chkAutWindows.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkAutWindows.Location = New System.Drawing.Point(416, 150)
        Me.chkAutWindows.Name = "chkAutWindows"
        Me.chkAutWindows.Size = New System.Drawing.Size(15, 14)
        Me.chkAutWindows.TabIndex = 3
        Me.chkAutWindows.UseVisualStyleBackColor = True
        Me.chkAutWindows.Visible = False
        '
        'txtPasswordSQL
        '
        Me.txtPasswordSQL.Location = New System.Drawing.Point(192, 47)
        Me.txtPasswordSQL.MaxLength = 50
        Me.txtPasswordSQL.Name = "txtPasswordSQL"
        Me.txtPasswordSQL.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtPasswordSQL.Size = New System.Drawing.Size(239, 20)
        Me.txtPasswordSQL.TabIndex = 2
        Me.txtPasswordSQL.UseSystemPasswordChar = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.ForeColor = System.Drawing.Color.Black
        Me.Label2.Location = New System.Drawing.Point(6, 47)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(165, 13)
        Me.Label2.TabIndex = 55
        Me.Label2.Text = "2.- Password del Usuario del SQL"
        '
        'ToolStripMenu
        '
        Me.ToolStripMenu.AutoSize = False
        Me.ToolStripMenu.BackColor = System.Drawing.Color.Silver
        Me.ToolStripMenu.Dock = System.Windows.Forms.DockStyle.None
        Me.ToolStripMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.comboMnu, Me.btnMnuModificar, Me.btnMnuOk, Me.btnMnuCancelar, Me.btnMnuSalir})
        Me.ToolStripMenu.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow
        Me.ToolStripMenu.Location = New System.Drawing.Point(9, 9)
        Me.ToolStripMenu.Name = "ToolStripMenu"
        Me.ToolStripMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.ToolStripMenu.Size = New System.Drawing.Size(595, 42)
        Me.ToolStripMenu.TabIndex = 19
        Me.ToolStripMenu.Text = "ToolStrip1"
        '
        'comboMnu
        '
        Me.comboMnu.AutoSize = False
        Me.comboMnu.BackColor = System.Drawing.Color.Silver
        Me.comboMnu.Enabled = False
        Me.comboMnu.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.comboMnu.ForeColor = System.Drawing.Color.Yellow
        Me.comboMnu.Items.AddRange(New Object() {"ALTAS", "MODIFICAR", "ELIMINAR", "CONSULTA", "REPORTE"})
        Me.comboMnu.MergeIndex = 0
        Me.comboMnu.Name = "comboMnu"
        Me.comboMnu.Size = New System.Drawing.Size(110, 25)
        Me.comboMnu.ToolTipText = "Dale Click a los Botones para Escoger una Opcion"
        '
        'btnMnuModificar
        '
        Me.btnMnuModificar.ForeColor = System.Drawing.Color.Red
        Me.btnMnuModificar.Image = CType(resources.GetObject("btnMnuModificar.Image"), System.Drawing.Image)
        Me.btnMnuModificar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuModificar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuModificar.Name = "btnMnuModificar"
        Me.btnMnuModificar.Size = New System.Drawing.Size(74, 39)
        Me.btnMnuModificar.Text = "&MODIFICAR"
        Me.btnMnuModificar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnMnuModificar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuOk
        '
        Me.btnMnuOk.ForeColor = System.Drawing.Color.Red
        'Me.btnMnuOk.Image = Global.TraspasosAdmPAQ.My.Resources.Resources.Save1
        Me.btnMnuOk.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuOk.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuOk.Name = "btnMnuOk"
        Me.btnMnuOk.Size = New System.Drawing.Size(56, 39)
        Me.btnMnuOk.Text = "&GRABAR"
        Me.btnMnuOk.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuCancelar
        '
        Me.btnMnuCancelar.ForeColor = System.Drawing.Color.Red
        'Me.btnMnuCancelar.Image = Global.TraspasosAdmPAQ.My.Resources.Resources.signos_f_059
        Me.btnMnuCancelar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuCancelar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuCancelar.Name = "btnMnuCancelar"
        Me.btnMnuCancelar.Size = New System.Drawing.Size(71, 39)
        Me.btnMnuCancelar.Text = "&CANCELAR"
        Me.btnMnuCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnMnuSalir
        '
        Me.btnMnuSalir.ForeColor = System.Drawing.Color.Red
        'Me.btnMnuSalir.Image = Global.TraspasosAdmPAQ.My.Resources.Resources.Salida2
        Me.btnMnuSalir.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMnuSalir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnMnuSalir.Name = "btnMnuSalir"
        Me.btnMnuSalir.Size = New System.Drawing.Size(41, 39)
        Me.btnMnuSalir.Text = "&SALIR"
        Me.btnMnuSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'txtDiasSaldoVenc
        '
        Me.txtDiasSaldoVenc.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDiasSaldoVenc.Location = New System.Drawing.Point(192, 19)
        Me.txtDiasSaldoVenc.MaxLength = 3
        Me.txtDiasSaldoVenc.Name = "txtDiasSaldoVenc"
        Me.txtDiasSaldoVenc.Size = New System.Drawing.Size(282, 20)
        Me.txtDiasSaldoVenc.TabIndex = 1
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Controls.Add(Me.TabPage4)
        Me.TabControl1.Controls.Add(Me.TabPage5)
        Me.TabControl1.Location = New System.Drawing.Point(9, 68)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(549, 310)
        Me.TabControl1.TabIndex = 20
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.GroupBox2)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(541, 284)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Servidor"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.GroupBox3)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(541, 284)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Empresa"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.txtBodegaSalDefault)
        Me.GroupBox3.Controls.Add(Me.Label20)
        Me.GroupBox3.Controls.Add(Me.lblNomBodegaSalDefault)
        Me.GroupBox3.Controls.Add(Me.cmdBuscaGrupo)
        Me.GroupBox3.Controls.Add(Me.btnVerPassFacturacion)
        Me.GroupBox3.Controls.Add(Me.txtPasswordFacturacion)
        Me.GroupBox3.Controls.Add(Me.Label18)
        Me.GroupBox3.Controls.Add(Me.txtTelefono)
        Me.GroupBox3.Controls.Add(Me.Label11)
        Me.GroupBox3.Controls.Add(Me.txtRFC)
        Me.GroupBox3.Controls.Add(Me.Label6)
        Me.GroupBox3.Controls.Add(Me.txtDireccion)
        Me.GroupBox3.Controls.Add(Me.Label9)
        Me.GroupBox3.Controls.Add(Me.txtEmpresa)
        Me.GroupBox3.Controls.Add(Me.Label10)
        Me.GroupBox3.Location = New System.Drawing.Point(6, 6)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(533, 272)
        Me.GroupBox3.TabIndex = 19
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Conexi�n SQL"
        '
        'txtBodegaSalDefault
        '
        Me.txtBodegaSalDefault.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtBodegaSalDefault.Enabled = False
        Me.txtBodegaSalDefault.Location = New System.Drawing.Point(81, 99)
        Me.txtBodegaSalDefault.MaxLength = 30
        Me.txtBodegaSalDefault.Name = "txtBodegaSalDefault"
        Me.txtBodegaSalDefault.Size = New System.Drawing.Size(96, 20)
        Me.txtBodegaSalDefault.TabIndex = 107
        '
        'Label20
        '
        Me.Label20.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label20.Location = New System.Drawing.Point(2, 92)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(81, 52)
        Me.Label20.TabIndex = 110
        Me.Label20.Text = "6.- Almacen Salida Default:"
        '
        'lblNomBodegaSalDefault
        '
        Me.lblNomBodegaSalDefault.CausesValidation = False
        Me.lblNomBodegaSalDefault.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.lblNomBodegaSalDefault.Location = New System.Drawing.Point(224, 99)
        Me.lblNomBodegaSalDefault.MaxLength = 50
        Me.lblNomBodegaSalDefault.Multiline = True
        Me.lblNomBodegaSalDefault.Name = "lblNomBodegaSalDefault"
        Me.lblNomBodegaSalDefault.ReadOnly = True
        Me.lblNomBodegaSalDefault.Size = New System.Drawing.Size(303, 20)
        Me.lblNomBodegaSalDefault.TabIndex = 109
        '
        'cmdBuscaGrupo
        '
        Me.cmdBuscaGrupo.Image = CType(resources.GetObject("cmdBuscaGrupo.Image"), System.Drawing.Image)
        Me.cmdBuscaGrupo.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdBuscaGrupo.Location = New System.Drawing.Point(183, 90)
        Me.cmdBuscaGrupo.Name = "cmdBuscaGrupo"
        Me.cmdBuscaGrupo.Size = New System.Drawing.Size(32, 32)
        Me.cmdBuscaGrupo.TabIndex = 108
        Me.cmdBuscaGrupo.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'btnVerPassFacturacion
        '
        Me.btnVerPassFacturacion.Location = New System.Drawing.Point(434, 169)
        Me.btnVerPassFacturacion.Name = "btnVerPassFacturacion"
        Me.btnVerPassFacturacion.Size = New System.Drawing.Size(31, 33)
        Me.btnVerPassFacturacion.TabIndex = 63
        Me.btnVerPassFacturacion.UseVisualStyleBackColor = True
        Me.btnVerPassFacturacion.Visible = False
        '
        'txtPasswordFacturacion
        '
        Me.txtPasswordFacturacion.Location = New System.Drawing.Point(183, 182)
        Me.txtPasswordFacturacion.MaxLength = 50
        Me.txtPasswordFacturacion.Name = "txtPasswordFacturacion"
        Me.txtPasswordFacturacion.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtPasswordFacturacion.Size = New System.Drawing.Size(239, 20)
        Me.txtPasswordFacturacion.TabIndex = 9
        Me.txtPasswordFacturacion.UseSystemPasswordChar = True
        Me.txtPasswordFacturacion.Visible = False
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label18.Location = New System.Drawing.Point(-3, 182)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(151, 13)
        Me.Label18.TabIndex = 62
        Me.Label18.Text = "5.- Password para Facturaci�n"
        Me.Label18.Visible = False
        '
        'txtTelefono
        '
        Me.txtTelefono.Location = New System.Drawing.Point(294, 64)
        Me.txtTelefono.MaxLength = 25
        Me.txtTelefono.Name = "txtTelefono"
        Me.txtTelefono.Size = New System.Drawing.Size(170, 20)
        Me.txtTelefono.TabIndex = 8
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label11.Location = New System.Drawing.Point(221, 67)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(67, 13)
        Me.Label11.TabIndex = 60
        Me.Label11.Text = "4.- T�lefono:"
        '
        'txtRFC
        '
        Me.txtRFC.Location = New System.Drawing.Point(81, 67)
        Me.txtRFC.MaxLength = 20
        Me.txtRFC.Name = "txtRFC"
        Me.txtRFC.Size = New System.Drawing.Size(96, 20)
        Me.txtRFC.TabIndex = 7
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label6.Location = New System.Drawing.Point(6, 71)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(46, 13)
        Me.Label6.TabIndex = 58
        Me.Label6.Text = "3.- RFC:"
        '
        'txtDireccion
        '
        Me.txtDireccion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDireccion.Location = New System.Drawing.Point(81, 42)
        Me.txtDireccion.MaxLength = 300
        Me.txtDireccion.Name = "txtDireccion"
        Me.txtDireccion.Size = New System.Drawing.Size(446, 20)
        Me.txtDireccion.TabIndex = 6
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label9.Location = New System.Drawing.Point(6, 45)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(70, 13)
        Me.Label9.TabIndex = 50
        Me.Label9.Text = "2.- Direcci�n:"
        '
        'txtEmpresa
        '
        Me.txtEmpresa.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtEmpresa.Location = New System.Drawing.Point(128, 16)
        Me.txtEmpresa.MaxLength = 250
        Me.txtEmpresa.Name = "txtEmpresa"
        Me.txtEmpresa.Size = New System.Drawing.Size(399, 20)
        Me.txtEmpresa.TabIndex = 5
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label10.Location = New System.Drawing.Point(6, 19)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(121, 13)
        Me.Label10.TabIndex = 49
        Me.Label10.Text = "1.- Nombre de Empresa:"
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.txtMensajeCorreo)
        Me.TabPage3.Controls.Add(Me.Label17)
        Me.TabPage3.Controls.Add(Me.chkConexionSSL)
        Me.TabPage3.Controls.Add(Me.Label16)
        Me.TabPage3.Controls.Add(Me.txtPuertoSMTP)
        Me.TabPage3.Controls.Add(Me.Label15)
        Me.TabPage3.Controls.Add(Me.txtPuertoPOP3)
        Me.TabPage3.Controls.Add(Me.Label14)
        Me.TabPage3.Controls.Add(Me.txtServidorSMTP)
        Me.TabPage3.Controls.Add(Me.Label13)
        Me.TabPage3.Controls.Add(Me.txtServidorPOP3)
        Me.TabPage3.Controls.Add(Me.Label12)
        Me.TabPage3.Controls.Add(Me.btnVerPassEmail)
        Me.TabPage3.Controls.Add(Me.txtEmailPassEmpresa)
        Me.TabPage3.Controls.Add(Me.Label7)
        Me.TabPage3.Controls.Add(Me.txtEmailEmpresa)
        Me.TabPage3.Controls.Add(Me.Label8)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(541, 284)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Correo Electr�nico"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'txtMensajeCorreo
        '
        Me.txtMensajeCorreo.Location = New System.Drawing.Point(136, 136)
        Me.txtMensajeCorreo.MaxLength = 300
        Me.txtMensajeCorreo.Multiline = True
        Me.txtMensajeCorreo.Name = "txtMensajeCorreo"
        Me.txtMensajeCorreo.Size = New System.Drawing.Size(399, 46)
        Me.txtMensajeCorreo.TabIndex = 79
        Me.txtMensajeCorreo.Visible = False
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label17.Location = New System.Drawing.Point(4, 143)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(99, 13)
        Me.Label17.TabIndex = 80
        Me.Label17.Text = "Mensaje de Correo:"
        Me.Label17.Visible = False
        '
        'chkConexionSSL
        '
        Me.chkConexionSSL.AutoSize = True
        Me.chkConexionSSL.Checked = True
        Me.chkConexionSSL.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkConexionSSL.Location = New System.Drawing.Point(136, 65)
        Me.chkConexionSSL.Name = "chkConexionSSL"
        Me.chkConexionSSL.Size = New System.Drawing.Size(15, 14)
        Me.chkConexionSSL.TabIndex = 78
        Me.chkConexionSSL.UseVisualStyleBackColor = True
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label16.Location = New System.Drawing.Point(3, 65)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(114, 13)
        Me.Label16.TabIndex = 77
        Me.Label16.Text = "Conexi�n Segura SSL:"
        '
        'txtPuertoSMTP
        '
        Me.txtPuertoSMTP.Location = New System.Drawing.Point(469, 39)
        Me.txtPuertoSMTP.MaxLength = 150
        Me.txtPuertoSMTP.Name = "txtPuertoSMTP"
        Me.txtPuertoSMTP.Size = New System.Drawing.Size(66, 20)
        Me.txtPuertoSMTP.TabIndex = 75
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label15.Location = New System.Drawing.Point(394, 43)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(71, 13)
        Me.Label15.TabIndex = 76
        Me.Label15.Text = "Puerto SMTP"
        '
        'txtPuertoPOP3
        '
        Me.txtPuertoPOP3.Location = New System.Drawing.Point(469, 9)
        Me.txtPuertoPOP3.MaxLength = 150
        Me.txtPuertoPOP3.Name = "txtPuertoPOP3"
        Me.txtPuertoPOP3.Size = New System.Drawing.Size(66, 20)
        Me.txtPuertoPOP3.TabIndex = 73
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label14.Location = New System.Drawing.Point(394, 13)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(69, 13)
        Me.Label14.TabIndex = 74
        Me.Label14.Text = "Puerto POP3"
        '
        'txtServidorSMTP
        '
        Me.txtServidorSMTP.Location = New System.Drawing.Point(136, 39)
        Me.txtServidorSMTP.MaxLength = 150
        Me.txtServidorSMTP.Name = "txtServidorSMTP"
        Me.txtServidorSMTP.Size = New System.Drawing.Size(252, 20)
        Me.txtServidorSMTP.TabIndex = 71
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label13.Location = New System.Drawing.Point(4, 46)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(131, 13)
        Me.Label13.TabIndex = 72
        Me.Label13.Text = "Servidor de Correo SMTP:"
        '
        'txtServidorPOP3
        '
        Me.txtServidorPOP3.Location = New System.Drawing.Point(136, 13)
        Me.txtServidorPOP3.MaxLength = 150
        Me.txtServidorPOP3.Name = "txtServidorPOP3"
        Me.txtServidorPOP3.Size = New System.Drawing.Size(252, 20)
        Me.txtServidorPOP3.TabIndex = 69
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label12.Location = New System.Drawing.Point(4, 20)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(129, 13)
        Me.Label12.TabIndex = 70
        Me.Label12.Text = "Servidor de Correo POP3:"
        '
        'btnVerPassEmail
        '
        Me.btnVerPassEmail.Location = New System.Drawing.Point(469, 97)
        Me.btnVerPassEmail.Name = "btnVerPassEmail"
        Me.btnVerPassEmail.Size = New System.Drawing.Size(31, 33)
        Me.btnVerPassEmail.TabIndex = 68
        Me.btnVerPassEmail.UseVisualStyleBackColor = True
        '
        'txtEmailPassEmpresa
        '
        Me.txtEmailPassEmpresa.Location = New System.Drawing.Point(136, 110)
        Me.txtEmailPassEmpresa.MaxLength = 150
        Me.txtEmailPassEmpresa.Name = "txtEmailPassEmpresa"
        Me.txtEmailPassEmpresa.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtEmailPassEmpresa.Size = New System.Drawing.Size(252, 20)
        Me.txtEmailPassEmpresa.TabIndex = 65
        Me.txtEmailPassEmpresa.UseSystemPasswordChar = True
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label7.Location = New System.Drawing.Point(4, 113)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(64, 13)
        Me.Label7.TabIndex = 67
        Me.Label7.Text = "Contrase�a:"
        '
        'txtEmailEmpresa
        '
        Me.txtEmailEmpresa.Location = New System.Drawing.Point(135, 84)
        Me.txtEmailEmpresa.MaxLength = 150
        Me.txtEmailEmpresa.Name = "txtEmailEmpresa"
        Me.txtEmailEmpresa.Size = New System.Drawing.Size(327, 20)
        Me.txtEmailEmpresa.TabIndex = 64
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label8.Location = New System.Drawing.Point(3, 87)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(97, 13)
        Me.Label8.TabIndex = 66
        Me.Label8.Text = "Correo El�ctronico:"
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.btnEliminar)
        Me.TabPage4.Controls.Add(Me.btnAgregar)
        Me.TabPage4.Controls.Add(Me.txtCNOMBREC01)
        Me.TabPage4.Controls.Add(Me.cmdBuscaConcepto)
        Me.TabPage4.Controls.Add(Me.Label19)
        Me.TabPage4.Controls.Add(Me.txtidConcepto)
        Me.TabPage4.Controls.Add(Me.grdConceptos)
        Me.TabPage4.Location = New System.Drawing.Point(4, 22)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Size = New System.Drawing.Size(541, 284)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "Conceptos"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.Image = CType(resources.GetObject("btnEliminar.Image"), System.Drawing.Image)
        Me.btnEliminar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnEliminar.Location = New System.Drawing.Point(458, 122)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(80, 56)
        Me.btnEliminar.TabIndex = 103
        Me.btnEliminar.Text = "DesAgregar"
        Me.btnEliminar.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'btnAgregar
        '
        Me.btnAgregar.Image = CType(resources.GetObject("btnAgregar.Image"), System.Drawing.Image)
        Me.btnAgregar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnAgregar.Location = New System.Drawing.Point(458, 42)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(80, 56)
        Me.btnAgregar.TabIndex = 102
        Me.btnAgregar.Text = "Agregar"
        Me.btnAgregar.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'txtCNOMBREC01
        '
        Me.txtCNOMBREC01.CausesValidation = False
        Me.txtCNOMBREC01.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCNOMBREC01.Location = New System.Drawing.Point(173, 42)
        Me.txtCNOMBREC01.MaxLength = 150
        Me.txtCNOMBREC01.Multiline = True
        Me.txtCNOMBREC01.Name = "txtCNOMBREC01"
        Me.txtCNOMBREC01.ReadOnly = True
        Me.txtCNOMBREC01.Size = New System.Drawing.Size(279, 20)
        Me.txtCNOMBREC01.TabIndex = 105
        '
        'cmdBuscaConcepto
        '
        Me.cmdBuscaConcepto.Image = CType(resources.GetObject("cmdBuscaConcepto.Image"), System.Drawing.Image)
        Me.cmdBuscaConcepto.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdBuscaConcepto.Location = New System.Drawing.Point(135, 30)
        Me.cmdBuscaConcepto.Name = "cmdBuscaConcepto"
        Me.cmdBuscaConcepto.Size = New System.Drawing.Size(32, 32)
        Me.cmdBuscaConcepto.TabIndex = 101
        Me.cmdBuscaConcepto.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.ForeColor = System.Drawing.Color.Black
        Me.Label19.Location = New System.Drawing.Point(13, 45)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(56, 13)
        Me.Label19.TabIndex = 106
        Me.Label19.Text = "Concepto:"
        '
        'txtidConcepto
        '
        Me.txtidConcepto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtidConcepto.Enabled = False
        Me.txtidConcepto.Location = New System.Drawing.Point(75, 42)
        Me.txtidConcepto.MaxLength = 30
        Me.txtidConcepto.Name = "txtidConcepto"
        Me.txtidConcepto.Size = New System.Drawing.Size(54, 20)
        Me.txtidConcepto.TabIndex = 100
        '
        'grdConceptos
        '
        Me.grdConceptos.AllowUserToAddRows = False
        Me.grdConceptos.AllowUserToDeleteRows = False
        DataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        DataGridViewCellStyle7.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.Yellow
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black
        Me.grdConceptos.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle7
        Me.grdConceptos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdConceptos.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.gConsecutivo, Me.gCIDCONCE01, Me.gCCODIGOC01, Me.gCNOMBREC01, Me.gCIDDOCUM01, Me.gCPLAMIGCFD, Me.gCDOCTOAC01})
        Me.grdConceptos.Location = New System.Drawing.Point(9, 68)
        Me.grdConceptos.Name = "grdConceptos"
        Me.grdConceptos.ReadOnly = True
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.grdConceptos.RowHeadersDefaultCellStyle = DataGridViewCellStyle8
        Me.grdConceptos.RowHeadersWidth = 26
        DataGridViewCellStyle9.ForeColor = System.Drawing.Color.Black
        Me.grdConceptos.RowsDefaultCellStyle = DataGridViewCellStyle9
        Me.grdConceptos.Size = New System.Drawing.Size(445, 203)
        Me.grdConceptos.TabIndex = 104
        '
        'gConsecutivo
        '
        Me.gConsecutivo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader
        Me.gConsecutivo.HeaderText = "#"
        Me.gConsecutivo.Name = "gConsecutivo"
        Me.gConsecutivo.ReadOnly = True
        Me.gConsecutivo.Width = 39
        '
        'gCIDCONCE01
        '
        Me.gCIDCONCE01.HeaderText = "ConceptocODIGO"
        Me.gCIDCONCE01.Name = "gCIDCONCE01"
        Me.gCIDCONCE01.ReadOnly = True
        Me.gCIDCONCE01.Visible = False
        Me.gCIDCONCE01.Width = 80
        '
        'gCCODIGOC01
        '
        Me.gCCODIGOC01.HeaderText = "Concepto"
        Me.gCCODIGOC01.Name = "gCCODIGOC01"
        Me.gCCODIGOC01.ReadOnly = True
        Me.gCCODIGOC01.Width = 80
        '
        'gCNOMBREC01
        '
        Me.gCNOMBREC01.HeaderText = "Nombre"
        Me.gCNOMBREC01.Name = "gCNOMBREC01"
        Me.gCNOMBREC01.ReadOnly = True
        Me.gCNOMBREC01.Width = 200
        '
        'gCIDDOCUM01
        '
        Me.gCIDDOCUM01.HeaderText = "Tipo Doc."
        Me.gCIDDOCUM01.Name = "gCIDDOCUM01"
        Me.gCIDDOCUM01.ReadOnly = True
        Me.gCIDDOCUM01.Width = 80
        '
        'gCPLAMIGCFD
        '
        Me.gCPLAMIGCFD.HeaderText = "CPLAMIGCFD"
        Me.gCPLAMIGCFD.Name = "gCPLAMIGCFD"
        Me.gCPLAMIGCFD.ReadOnly = True
        Me.gCPLAMIGCFD.Visible = False
        '
        'gCDOCTOAC01
        '
        Me.gCDOCTOAC01.HeaderText = "CDOCTOAC01"
        Me.gCDOCTOAC01.Name = "gCDOCTOAC01"
        Me.gCDOCTOAC01.ReadOnly = True
        Me.gCDOCTOAC01.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.gCDOCTOAC01.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.gCDOCTOAC01.Visible = False
        '
        'TabPage5
        '
        Me.TabPage5.Controls.Add(Me.Label1)
        Me.TabPage5.Controls.Add(Me.txtColaImpresionPed)
        Me.TabPage5.Controls.Add(Me.Label23)
        Me.TabPage5.Controls.Add(Me.txtColaImpresionRem)
        Me.TabPage5.Controls.Add(Me.Label22)
        Me.TabPage5.Controls.Add(Me.txtColaImpresionFact)
        Me.TabPage5.Controls.Add(Me.Label21)
        Me.TabPage5.Location = New System.Drawing.Point(4, 22)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage5.Size = New System.Drawing.Size(541, 284)
        Me.TabPage5.TabIndex = 4
        Me.TabPage5.Text = "Cola Impresi�n"
        Me.TabPage5.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(11, 92)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(473, 13)
        Me.Label1.TabIndex = 67
        Me.Label1.Text = "Nota: Las colas de impresi�n se guardaran en la configuraci�n de cada usuario par" & _
    "a cada terminal."
        Me.Label1.Visible = False
        '
        'txtColaImpresionPed
        '
        Me.txtColaImpresionPed.Location = New System.Drawing.Point(144, 58)
        Me.txtColaImpresionPed.MaxLength = 250
        Me.txtColaImpresionPed.Name = "txtColaImpresionPed"
        Me.txtColaImpresionPed.Size = New System.Drawing.Size(391, 20)
        Me.txtColaImpresionPed.TabIndex = 65
        Me.txtColaImpresionPed.Visible = False
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label23.Location = New System.Drawing.Point(8, 67)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(130, 13)
        Me.Label23.TabIndex = 66
        Me.Label23.Text = "Cola de Impresi�n Pedido:"
        Me.Label23.Visible = False
        '
        'txtColaImpresionRem
        '
        Me.txtColaImpresionRem.Location = New System.Drawing.Point(144, 32)
        Me.txtColaImpresionRem.MaxLength = 250
        Me.txtColaImpresionRem.Name = "txtColaImpresionRem"
        Me.txtColaImpresionRem.Size = New System.Drawing.Size(391, 20)
        Me.txtColaImpresionRem.TabIndex = 63
        Me.txtColaImpresionRem.Visible = False
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label22.Location = New System.Drawing.Point(8, 41)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(140, 13)
        Me.Label22.TabIndex = 64
        Me.Label22.Text = "Cola de Impresi�n Remisi�n:"
        Me.Label22.Visible = False
        '
        'txtColaImpresionFact
        '
        Me.txtColaImpresionFact.Location = New System.Drawing.Point(144, 6)
        Me.txtColaImpresionFact.MaxLength = 250
        Me.txtColaImpresionFact.Name = "txtColaImpresionFact"
        Me.txtColaImpresionFact.Size = New System.Drawing.Size(392, 20)
        Me.txtColaImpresionFact.TabIndex = 61
        Me.txtColaImpresionFact.Visible = False
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label21.Location = New System.Drawing.Point(8, 15)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(133, 13)
        Me.Label21.TabIndex = 62
        Me.Label21.Text = "Cola de Impresi�n Factura:"
        Me.Label21.Visible = False
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'frmParametros
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(565, 390)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.ToolStripMenu)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmParametros"
        Me.Text = "Parametros"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ToolStripMenu.ResumeLayout(False)
        Me.ToolStripMenu.PerformLayout()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        Me.TabPage4.ResumeLayout(False)
        Me.TabPage4.PerformLayout()
        CType(Me.grdConceptos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage5.ResumeLayout(False)
        Me.TabPage5.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "Funciones Estandares"
    Private Sub LimpiaCampos()
        txtUsuarioSQL.Text = ""
        txtPasswordSQL.Text = ""
        chkAutWindows.Checked = False
        txtColaImpresionFact.Text = ""
        txtColaImpresionRem.Text = ""
        txtColaImpresionPed.Text = ""
        txtEmpresa.Text = ""
        txtDireccion.Text = ""
        txtRFC.Text = ""
        txtTelefono.Text = ""
        txtEmailEmpresa.Text = ""
        txtEmailPassEmpresa.Text = ""
        chkImprimeRegistro.Checked = False
        txtPasswordFacturacion.Text = ""

        txtBodegaSalDefault.Text = ""
        lblNomBodegaSalDefault.Text = ""

        txtFormatoFac.Text = ""
    End Sub

    Private Sub ActivaCampos(ByVal valor As Boolean, ByVal vOpcion As TipoOpcActivaCampos)
        If vOpcion = TipoOpcActivaCampos.tOpcDESHABTODOS Then
            txtUsuarioSQL.Enabled = False
            txtPasswordSQL.Enabled = False
            chkAutWindows.Enabled = False
            txtColaImpresionFact.Enabled = False
            txtColaImpresionRem.Enabled = False
            txtColaImpresionPed.Enabled = False
            chkImprimeRegistro.Enabled = False

            txtEmpresa.Enabled = False
            txtDireccion.Enabled = False
            txtRFC.Enabled = False
            txtTelefono.Enabled = False
            txtEmailEmpresa.Enabled = False
            txtEmailPassEmpresa.Enabled = False
            txtPasswordFacturacion.Enabled = False

            txtServidorPOP3.Enabled = False
            txtPuertoPOP3.Enabled = False
            txtServidorSMTP.Enabled = False
            txtPuertoSMTP.Enabled = False
            chkConexionSSL.Enabled = False
            txtMensajeCorreo.Enabled = False

            txtidConcepto.Enabled = False
            txtCNOMBREC01.Enabled = False
            grdConceptos.Enabled = False

            'txtidConceptoExt.Enabled = False
            'txtCNOMBREC01Ext.Enabled = False
            'grdConceptosExt.Enabled = False

            txtBodegaSalDefault.Enabled = False
            lblNomBodegaSalDefault.Enabled = False

            cbEmpresa.Enabled = False
            txtFormatoFac.Enabled = False

        ElseIf vOpcion = TipoOpcActivaCampos.tOpcINICIALIZA Then
            txtUsuarioSQL.Enabled = valor
            txtPasswordSQL.Enabled = valor
            chkAutWindows.Enabled = valor
            txtColaImpresionFact.Enabled = valor
            txtColaImpresionRem.Enabled = valor
            txtColaImpresionPed.Enabled = valor
            chkImprimeRegistro.Enabled = valor

            txtEmpresa.Enabled = valor
            txtDireccion.Enabled = valor
            txtRFC.Enabled = valor
            txtTelefono.Enabled = valor
            txtEmailEmpresa.Enabled = valor
            txtEmailPassEmpresa.Enabled = valor
            txtPasswordFacturacion.Enabled = valor

            txtServidorPOP3.Enabled = valor
            txtPuertoPOP3.Enabled = valor
            txtServidorSMTP.Enabled = valor
            txtPuertoSMTP.Enabled = valor
            chkConexionSSL.Enabled = valor
            txtMensajeCorreo.Enabled = valor

            txtidConcepto.Enabled = valor
            txtCNOMBREC01.Enabled = valor
            grdConceptos.Enabled = valor

            'txtidConceptoExt.Enabled = valor
            'txtCNOMBREC01Ext.Enabled = valor
            'grdConceptosExt.Enabled = valor

            txtBodegaSalDefault.Enabled = valor
            lblNomBodegaSalDefault.Enabled = valor

            cbEmpresa.Enabled = valor

            txtFormatoFac.Enabled = valor
        End If
    End Sub

    Private Sub ActivaBotones(ByVal valor As Boolean, Optional ByVal vOpcion As TipoOpcActivaBoton = TipoOpcActivaBoton.tOpcInicializa)
        If vOpcion = TipoOpcActivaBoton.tOpcConsulta Then
            'cmdOk.Enabled = False
            'cmdCancelar.Enabled = valor
            'cmdTerminar.Enabled = Not valor

            btnVerPassSQL.Enabled = valor
            btnVerPassFacturacion.Enabled = valor
            btnVerPassEmail.Enabled = valor
            btnFormatoImp.Enabled = valor

            btnAgregar.Enabled = valor
            btnEliminar.Enabled = valor

            'btnAgregarExt.Enabled = valor
            'btnEliminarExt.Enabled = valor


            btnMnuOk.Enabled = False
            btnMnuCancelar.Enabled = valor
            btnMnuSalir.Enabled = Not valor

        ElseIf vOpcion = TipoOpcActivaBoton.tOpcInicializa Then
            'cmdOk.Enabled = valor
            'cmdCancelar.Enabled = valor
            'cmdTerminar.Enabled = Not valor

            btnVerPassSQL.Enabled = valor
            btnVerPassFacturacion.Enabled = valor
            btnVerPassEmail.Enabled = valor
            btnFormatoImp.Enabled = valor


            btnAgregar.Enabled = valor
            btnEliminar.Enabled = valor

            'btnAgregarExt.Enabled = valor
            'btnEliminarExt.Enabled = valor

            btnMnuOk.Enabled = valor
            btnMnuCancelar.Enabled = valor
            btnMnuSalir.Enabled = Not valor
        ElseIf vOpcion = TipoOpcActivaBoton.tOpcInsertar Then
            'cmdOk.Enabled = valor
            'cmdCancelar.Enabled = valor
            'cmdTerminar.Enabled = Not valor

            btnVerPassSQL.Enabled = valor
            btnVerPassEmail.Enabled = valor
            btnVerPassFacturacion.Enabled = valor
            btnFormatoImp.Enabled = valor

            btnAgregar.Enabled = valor
            btnEliminar.Enabled = valor

            'btnAgregarExt.Enabled = valor
            'btnEliminarExt.Enabled = valor

            btnMnuOk.Enabled = valor
            btnMnuCancelar.Enabled = valor
            btnMnuSalir.Enabled = Not valor
        End If
    End Sub

    Private Sub Cancelar()
        ActivaCampos(False, TipoOpcActivaCampos.tOpcDESHABTODOS)
        ActivaBotones(False)
        'LimpiaCampos()
        LimpiaCamposConcepto(True)
        LimpiaCamposConcepto(False)
        ToolStripMenu.Enabled = True
    End Sub

    Private Function LlenaDatosFox(ByVal cCve As String, ByVal dbtabla As String, ByVal Campo As String, ByVal TipoCampo As TipoDato, _
                               Optional ByVal cCve2 As String = "", Optional ByVal EsExterno As Boolean = False) As String
        Dim Consulta As String = ""
        Dim Conexion As New OleDb.OleDbConnection(ConStrFox)
        Try
            Select Case UCase(dbtabla)
                Case UCase("MGW10000")
                    Consulta = "select CIDEMPRESA, CNOMBREE01, CRFCEMPR01 from MGW10000 where CIDEMPRESA = " & sIdEmpresa

                Case UCase("MGW10011")
                    Consulta = "SELECT (ALLTRIM(DIR.CNOMBREC01) + ' ' + ALLTRIM(DIR.CCOLONIA) + ' ' +  " & _
                    "ALLTRIM(DIR.CCIUDAD) + ' ' + ALLTRIM(DIR.CESTADO)  ) AS DIRECCION, DIR.CTELEFONO1,DIR.CEMAIL " & _
                    "FROM MGW10011 DIR WHERE DIR.CTIPOCAT01 = 4 AND DIR.CTIPODIR01 = 0 "
                Case UCase("MGW10006")
                    'Consulta = "SELECT CONCEP.CIDCONCE01,CONCEP.CCODIGOC01,CONCEP.CNOMBREC01,CONCEP.CIDDOCUM01  FROM MGW10006 CONCEP WHERE CIDCONCE01 = 0 and CIDDOCUM01 = 4"
                    Consulta = "SELECT CONCEP.CIDCONCE01,CONCEP.CCODIGOC01,CONCEP.CNOMBREC01,CONCEP.CIDDOCUM01, " & _
                    "CONCEP.CPLAMIGCFD, CONCEP.CDOCTOAC01  FROM MGW10006 CONCEP WHERE CCODIGOC01 = '" & cCve & "'"
                Case UCase("BODEGASAL")
                    Consulta = "SELECT CIDALMACEN, CCODIGOA01, ALLTRIM(CNOMBREA01) AS CNOMBREA01 FROM MGW10003 where CIDALMACEN > 0 and CCODIGOA01 == '" & cCve & "'"

            End Select
            Dim Adt As New OleDb.OleDbDataAdapter(Consulta, Conexion)
            DsComboFox.Clear()
            Adt.Fill(DsComboFox, dbtabla)
            If Not DsComboFox Is Nothing Then
                If DsComboFox.Tables(dbtabla).DefaultView.Count > 0 Then
                    Select Case UCase(dbtabla)
                        Case UCase("MGW10000")
                            txtEmpresa.Text = Trim(DsComboFox.Tables("MGW10000").DefaultView.Item(0).Item("CNOMBREE01"))
                            txtRFC.Text = Trim(DsComboFox.Tables("MGW10000").DefaultView.Item(0).Item("CRFCEMPR01"))
                        Case UCase("MGW10011")
                            txtDireccion.Text = Trim(DsComboFox.Tables("MGW10011").DefaultView.Item(0).Item("DIRECCION"))
                            txtTelefono.Text = Trim(DsComboFox.Tables("MGW10011").DefaultView.Item(0).Item("CTELEFONO1"))
                        Case UCase("MGW10006")
                            If EsExterno Then
                                'txtCNOMBREC01Ext.Text = Trim(DsComboFox.Tables(dbtabla).DefaultView.Item(0).Item("CNOMBREC01"))
                                'vCCODIGOC01 = Trim(DsComboFox.Tables(dbtabla).DefaultView.Item(0).Item("CCODIGOC01"))
                                vCIDCONCE01Ext = Trim(DsComboFox.Tables(dbtabla).DefaultView.Item(0).Item("CIDCONCE01"))
                                vCIDDOCUM01Ext = DsComboFox.Tables(dbtabla).DefaultView.Item(0).Item("CIDDOCUM01")
                                vCPLAMIGCFDExt = Trim(DsComboFox.Tables(dbtabla).DefaultView.Item(0).Item("CPLAMIGCFD"))
                                vCDOCTOAC01Ext = DsComboFox.Tables(dbtabla).DefaultView.Item(0).Item("CDOCTOAC01")
                            Else
                                txtCNOMBREC01.Text = Trim(DsComboFox.Tables(dbtabla).DefaultView.Item(0).Item("CNOMBREC01"))
                                'vCCODIGOC01 = Trim(DsComboFox.Tables(dbtabla).DefaultView.Item(0).Item("CCODIGOC01"))
                                vCIDCONCE01 = Trim(DsComboFox.Tables(dbtabla).DefaultView.Item(0).Item("CIDCONCE01"))
                                vCIDDOCUM01 = DsComboFox.Tables(dbtabla).DefaultView.Item(0).Item("CIDDOCUM01")
                                vCPLAMIGCFD = Trim(DsComboFox.Tables(dbtabla).DefaultView.Item(0).Item("CPLAMIGCFD"))
                                vCDOCTOAC01 = DsComboFox.Tables(dbtabla).DefaultView.Item(0).Item("CDOCTOAC01")
                            End If
                        Case UCase("BODEGASAL")
                            vCIDALMACEN = DsComboFox.Tables(dbtabla).DefaultView.Item(0).Item("CIDALMACEN")

                            lblNomBodegaSalDefault.Text = Trim(DsComboFox.Tables(dbtabla).DefaultView.Item(0).Item("CNOMBREA01"))
                            'IdAlmacen2 = Trim(DsComboFox.Tables(dbtabla).DefaultView.Item(0).Item("CCODIGOA01"))

                            'IdAlmacenSal = IdAlmacen
                            'CodAlmacenSal = IdAlmacen2


                    End Select
                    LlenaDatosFox = KEY_RCORRECTO
                Else
                    LlenaDatosFox = KEY_RINCORRECTO
                End If
            Else
                LlenaDatosFox = KEY_RINCORRECTO
            End If


        Catch ex As Exception
            SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")
            LlenaDatosFox = KEY_RERROR
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, Me.Text)
        End Try
    End Function

    Private Function LlenaDatos(ByVal cCve As String, ByVal dbtabla As String, ByVal Campo As String, ByVal TipoCampo As TipoDato, Optional ByVal FiltroEsp As String = "", Optional ByVal Filtro2 As String = "") As String
        Dim strSql As String = ""
        Try
            Select Case UCase(dbtabla)
                Case UCase("CatGrupos")
                    strSql = "select CveGrupo, NombreGrupo from catgrupos where CveGrupo = '" & cCve & "'"
                    'DS_Param = Inicio.ChecaClave(cCve, Inicio.CONSTR, TipoConexion.TcSQL, dbtabla, Campo, TipoCampo, Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS)
                    DS_Param = Inicio.ChecaClavexSentencia(cCve, CONSTR, TipoConexion.TcSQL, dbtabla, "", TipoCampo, Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, strSql, FiltroEsp, , , True)
                Case UCase(TablaBd)
                    DS_Param = Inicio.ChecaClave(cCve, Inicio.CONSTR, TipoConexion.TcSQL, dbtabla, Campo, TipoCampo, Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS)
            End Select
            'DS.Clear()

            sPosibleError = GetSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")
            If sPosibleError <> "ERROR" Or sPosibleError = Nothing Then
                SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")
                If Not DS_Param Is Nothing Then
                    If DS_Param.Tables(dbtabla).DefaultView.Count > 0 Then
                        Select Case UCase(dbtabla)
                            Case UCase(TablaBd)
                                If sNomUsuarioSQL = "" Then
                                    txtUsuarioSQL.Text = Trim(DS_Param.Tables(dbtabla).DefaultView.Item(0).Item("UsuarioSQL"))
                                Else
                                    txtUsuarioSQL.Text = sNomUsuarioSQL
                                End If

                                txtPasswordSQL.Text = Criptografia.Encrypt(Trim(sPassUsuarioSQL), KEY_CONFIG_ENCRYPT)


                                'txtUsuarioSQL.Text = DS_Param.Tables(dbtabla).DefaultView.Item(0).Item("UsuarioSQL")
                                'txtPasswordSQL.Text = DS_Param.Tables(dbtabla).DefaultView.Item(0).Item("PasswordSQL")
                                'txtPasswordSQL.Text = Criptografia.Decrypt(Trim(DS.Tables(dbtabla).DefaultView.Item(0).Item("PasswordSQL")), KEY_CONFIG_ENCRYPT)

                                If DS_Param.Tables(dbtabla).DefaultView.Item(0).Item("AutentificaWin") = True Then
                                    chkAutWindows.Checked = True
                                Else
                                    chkAutWindows.Checked = False
                                End If

                                'chkImprimeRegistro.Enabled = False
                                If Not DS_Param.Tables(dbtabla).DefaultView.Item(0).Item("ImprimeRegistro") Is DBNull.Value Then
                                    If DS_Param.Tables(dbtabla).DefaultView.Item(0).Item("ImprimeRegistro") = True Then
                                        chkImprimeRegistro.Checked = True
                                    Else
                                        chkImprimeRegistro.Checked = False
                                    End If
                                Else
                                    chkImprimeRegistro.Checked = False

                                End If



                                'txtEmpresa.Text = DS.Tables(dbtabla).DefaultView.Item(0).Item("Empresa")
                                'txtRFC.Text = DS.Tables(dbtabla).DefaultView.Item(0).Item("RFC")
                                'txtDireccion.Text = DS.Tables(dbtabla).DefaultView.Item(0).Item("Direccion")
                                'txtTelefono.Text = DS.Tables(dbtabla).DefaultView.Item(0).Item("Telefono")

                                'CARGARLOS DE ADMINPAQ
                                'txtEmpresa.Text = DS.Tables(dbtabla).DefaultView.Item(0).Item("Empresa")
                                'txtRFC.Text = DS.Tables(dbtabla).DefaultView.Item(0).Item("RFC")

                                'txtDireccion.Text = DS.Tables(dbtabla).DefaultView.Item(0).Item("Direccion")
                                'txtTelefono.Text = DS.Tables(dbtabla).DefaultView.Item(0).Item("Telefono")

                                txtEmailEmpresa.Text = DS_Param.Tables("PARAMETROS").DefaultView.Item(0).Item("EmailEmpresa")

                                Existe = LlenaDatosFox(sIdEmpresa, "MGW10000", "CCODIGOC01", TipoDato.TdCadena)
                                If Existe = KEY_RCORRECTO Then
                                    Existe = LlenaDatosFox(sIdEmpresa, "MGW10011", "CCODIGOC01", TipoDato.TdCadena)
                                    If Existe = KEY_RINCORRECTO Then
                                        'no se encontraron: Parametro_Direccion,Parametro_Telefono  de adminpaq
                                        txtDireccion.Text = DS_Param.Tables(dbtabla).DefaultView.Item(0).Item("Direccion")
                                        txtTelefono.Text = DS_Param.Tables(dbtabla).DefaultView.Item(0).Item("Telefono")
                                    End If
                                Else
                                    'no se encontraron: Parametro_Empresa,Parametro_RFC de adminpaq
                                    txtEmpresa.Text = DS_Param.Tables(dbtabla).DefaultView.Item(0).Item("Empresa")
                                    txtRFC.Text = DS_Param.Tables(dbtabla).DefaultView.Item(0).Item("RFC")

                                End If


                                txtEmailEmpresa.Text = DS_Param.Tables(dbtabla).DefaultView.Item(0).Item("EmailEmpresa")
                                txtEmailPassEmpresa.Text = DS_Param.Tables(dbtabla).DefaultView.Item(0).Item("EmailPassEmpresa")
                                'txtEmailPassEmpresa.Text = Criptografia.Decrypt(Trim(DS.Tables(dbtabla).DefaultView.Item(0).Item("EmailPassEmpresa")), KEY_CONFIG_ENCRYPT)
                                If Not DS_Param.Tables(dbtabla).DefaultView.Item(0).Item("PasswordFacturacion") Is DBNull.Value Then
                                    txtPasswordFacturacion.Text = DS_Param.Tables(dbtabla).DefaultView.Item(0).Item("PasswordFacturacion")
                                End If

                                vPassUserSQl = txtPasswordSQL.Text
                                vPassEmail = txtEmailPassEmpresa.Text
                                vPassFacturacion = txtPasswordFacturacion.Text

                                'ActualizaParametros(txtUsuarioSQL.Text, txtPasswordSQL.Text, txtEmpresa.Text, txtDireccion.Text, txtRFC.Text, txtTelefono.Text, txtEmailEmpresa.Text, txtEmailPassEmpresa.Text)
                                'Parametro_Servidor = DS.Tables(dbtabla).DefaultView.Item(0).Item("ServidorxAfectar")
                                'Parametro_BaseDatos = DS.Tables(dbtabla).DefaultView.Item(0).Item("BaseDatosxAfectar")
                                'Parametro_UsuarioSQLxAfectar = DS.Tables(dbtabla).DefaultView.Item(0).Item("UsuarioSQLxAfectar")
                                'Parametro_PasswordSQLxAfectar = DS.Tables(dbtabla).DefaultView.Item(0).Item("PasswordSQLxAfectar")


                                'If Not DS.Tables(dbtabla).DefaultView.Item(0).Item("ColaImpresionRecibos") Is DBNull.Value Then
                                '    'txtColaImpRecibCajas.Text = DS.Tables(dbtabla).DefaultView.Item(0).Item("ColaImpresionRecibos")
                                'End If

                                'txtPorcIVA.Value = DS.Tables(dbtabla).DefaultView.Item(0).Item("PorcIVA")

                                If Not DS_Param.Tables(dbtabla).DefaultView.Item(0).Item("ServidorPOP3") Is DBNull.Value Then
                                    txtServidorPOP3.Text = DS_Param.Tables(dbtabla).DefaultView.Item(0).Item("ServidorPOP3")
                                Else
                                    txtServidorPOP3.Text = ""
                                End If

                                If Not DS_Param.Tables(dbtabla).DefaultView.Item(0).Item("PuertoPOP3") Is DBNull.Value Then
                                    txtPuertoPOP3.Text = DS_Param.Tables(dbtabla).DefaultView.Item(0).Item("PuertoPOP3")
                                Else
                                    txtPuertoPOP3.Text = ""
                                End If


                                If Not DS_Param.Tables(dbtabla).DefaultView.Item(0).Item("ServidorSMTP") Is DBNull.Value Then
                                    txtServidorSMTP.Text = DS_Param.Tables(dbtabla).DefaultView.Item(0).Item("ServidorSMTP")
                                Else
                                    txtServidorSMTP.Text = ""
                                End If


                                If Not DS_Param.Tables(dbtabla).DefaultView.Item(0).Item("PuertoSMTP") Is DBNull.Value Then
                                    txtPuertoSMTP.Text = DS_Param.Tables(dbtabla).DefaultView.Item(0).Item("PuertoSMTP")
                                Else
                                    txtPuertoSMTP.Text = ""
                                End If

                                If Not DS_Param.Tables(dbtabla).DefaultView.Item(0).Item("ConexionSSL") Is DBNull.Value Then
                                    If DS_Param.Tables(dbtabla).DefaultView.Item(0).Item("ConexionSSL") = True Then
                                        chkConexionSSL.Checked = True
                                    Else
                                        chkConexionSSL.Checked = False
                                    End If
                                Else
                                    chkConexionSSL.Checked = False
                                End If

                                If Not DS_Param.Tables(dbtabla).DefaultView.Item(0).Item("MensajeCorreo") Is DBNull.Value Then
                                    txtMensajeCorreo.Text = DS_Param.Tables(dbtabla).DefaultView.Item(0).Item("MensajeCorreo")
                                Else
                                    txtMensajeCorreo.Text = ""
                                End If

                                '15/ABRIL/2014
                                'ya no se toma de la base de datos, si no del registro
                                'Registro_ColaFacturaLocal = GetSetting(Process.GetCurrentProcess.ProcessName, "Configuracion", "ColaFacturaLocal", "") & ""
                                'txtColaImpresionFact.Text = Registro_ColaFacturaLocal
                                'Registro_ColaRemisionLocal = GetSetting(Process.GetCurrentProcess.ProcessName, "Configuracion", "ColaRemisionLocal", "") & ""
                                'txtColaImpresionRem.Text = Registro_ColaRemisionLocal
                                'Registro_ColaPedidoLocal = GetSetting(Process.GetCurrentProcess.ProcessName, "Configuracion", "ColaPedidoLocal", "") & ""
                                'txtColaImpresionPed.Text = Registro_ColaPedidoLocal


                                ''aqui se actualiza parametros de colas
                                'If Not DS_Param.Tables(dbtabla).DefaultView.Item(0).Item("ColaImpresionFact") Is DBNull.Value Then
                                '    Parametro_ColaImpresionFact = DS_Param.Tables(dbtabla).DefaultView.Item(0).Item("ColaImpresionFact")
                                'Else
                                '    Parametro_ColaImpresionFact = ""
                                'End If

                                'If Not DS_Param.Tables(dbtabla).DefaultView.Item(0).Item("ColaImpresionPed") Is DBNull.Value Then
                                '    Parametro_ColaImpresionPed = DS_Param.Tables(dbtabla).DefaultView.Item(0).Item("ColaImpresionPed")
                                'Else
                                '    Parametro_ColaImpresionPed = ""
                                'End If

                                'If Not DS_Param.Tables(dbtabla).DefaultView.Item(0).Item("ColaImpresionRem") Is DBNull.Value Then
                                '    Parametro_ColaImpresionRem = DS_Param.Tables(dbtabla).DefaultView.Item(0).Item("ColaImpresionRem")
                                'Else
                                '    Parametro_ColaImpresionRem = ""
                                'End If
                                'If Not DS_Param.Tables(dbtabla).DefaultView.Item(0).Item("ColaImpresionFact") Is DBNull.Value Then
                                '    txtColaImpresionFact.Text = DS_Param.Tables(dbtabla).DefaultView.Item(0).Item("ColaImpresionFact")
                                'End If
                                'If Not DS_Param.Tables(dbtabla).DefaultView.Item(0).Item("ColaImpresionPed") Is DBNull.Value Then
                                '    txtColaImpresionPed.Text = DS_Param.Tables(dbtabla).DefaultView.Item(0).Item("ColaImpresionPed")
                                'End If
                                'If Not DS_Param.Tables(dbtabla).DefaultView.Item(0).Item("ColaImpresionRem") Is DBNull.Value Then
                                '    txtColaImpresionRem.Text = DS_Param.Tables(dbtabla).DefaultView.Item(0).Item("ColaImpresionRem")
                                'End If

                                If Not DS_Param.Tables(dbtabla).DefaultView.Item(0).Item("CIDEMPRESA") Is DBNull.Value Then
                                    If DS_Param.Tables(dbtabla).DefaultView.Item(0).Item("CIDEMPRESA") > 0 Then
                                        sIdEmpresa = DS_Param.Tables(dbtabla).DefaultView.Item(0).Item("CIDEMPRESA")
                                    End If

                                Else
                                    sIdEmpresa = 0
                                End If

                                If Not DS_Param.Tables(dbtabla).DefaultView.Item(0).Item("CNOMBREE01") Is DBNull.Value Then
                                    If DS_Param.Tables(dbtabla).DefaultView.Item(0).Item("CIDEMPRESA") > 0 Then
                                        sNombreEmpresaAdmPAQ = DS_Param.Tables(dbtabla).DefaultView.Item(0).Item("CNOMBREE01")
                                    End If
                                Else
                                    sNombreEmpresaAdmPAQ = ""
                                End If

                                If Not DS_Param.Tables(dbtabla).DefaultView.Item(0).Item("CRUTADATOS") Is DBNull.Value Then
                                    If DS_Param.Tables(dbtabla).DefaultView.Item(0).Item("CIDEMPRESA") > 0 Then
                                        sRutaEmpresaAdmPAQ = DS_Param.Tables(dbtabla).DefaultView.Item(0).Item("CRUTADATOS")
                                    End If
                                    cbEmpresa.SelectedValue = sRutaEmpresaAdmPAQ
                                    'aqui se vuelve a generar la cadena de conexion fox
                                    ConStrFox = CreaCadenaFox(sRutaEmpresaAdmPAQ)
                                Else
                                    sRutaEmpresaAdmPAQ = ""
                                End If

                                If Not DS_Param.Tables(dbtabla).DefaultView.Item(0).Item("CPLAMIGCFD") Is DBNull.Value Then
                                    txtFormatoFac.Text = DS_Param.Tables(dbtabla).DefaultView.Item(0).Item("CPLAMIGCFD")
                                Else
                                    txtFormatoFac.Text = ""
                                End If

                                If Not DS_Param.Tables(dbtabla).DefaultView.Item(0).Item("CCODIGOA01_SALdefault") Is DBNull.Value Then
                                    txtBodegaSalDefault.Text = DS_Param.Tables(dbtabla).DefaultView.Item(0).Item("CCODIGOA01_SALdefault")
                                    'Existe = LlenaDatos(txtBodegaSalDefault.Text, "CatGrupos", "CveGrupo", TipoDato.TdCadena)
                                    Existe = LlenaDatosFox(txtBodegaSalDefault.Text, "BODEGASAL", "CCODIGOC01", TipoDato.TdCadena)
                                Else
                                    txtBodegaSalDefault.Text = ""
                                End If




                                'AQui llenamos El Grid
                                strSql = "SELECT CONCEP.CIDCONCE01 as tdCIDCONCE01,CONCEP.CCODIGOC01 as tdCCODIGOC01, " & _
                                "CONCEP.CNOMBREC01 as tdCNOMBREC01,CONCEP.CIDDOCUM01 as tdCIDDOCUM01,CONCEP.CPLAMIGCFD as tdCPLAMIGCFD, CONCEP.CDOCTOAC01 as tdCDOCTOAC01  " & _
                                "FROM CATCONCEPTOS CONCEP"

                                MyTablaConceptos = BD.ExecuteReturn(strSql)
                                InsertaRegGrid(False)

                                'strSql = "SELECT CONCEP.CIDCONCE01 as tdCIDCONCE01,CONCEP.CCODIGOC01 as tdCCODIGOC01, " & _
                                '"CONCEP.CNOMBREC01 as tdCNOMBREC01,CONCEP.CIDDOCUM01 as tdCIDDOCUM01,CONCEP.CPLAMIGCFD as tdCPLAMIGCFD, CONCEP.CDOCTOAC01 as tdCDOCTOAC01  " & _
                                '"FROM CATCONCEPTOSEXT CONCEP"

                                'MyTablaConceptosExt = BD.ExecuteReturn(strSql)
                                'InsertaRegGrid(True)

                            Case UCase("CatGrupos")
                                lblNomBodegaSalDefault.Text = DS_Param.Tables(dbtabla).DefaultView.Item(0).Item("NombreGrupo")
                        End Select



                        LlenaDatos = KEY_RCORRECTO
                    Else
                        LlenaDatos = KEY_RINCORRECTO
                    End If
                Else
                    LlenaDatos = KEY_RINCORRECTO
                End If
            Else
                SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")
                LlenaDatos = KEY_RERROR
            End If

        Catch ex As Exception
            SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")
            LlenaDatos = KEY_RERROR
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, Me.Text)
        End Try
    End Function

    Private Sub LimpiaVariables()
        'ConsecutivoAudita = 0
        indice = 0
        Existe = False
        'RegAModif = ""
        'RegModif = ""
        SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")

    End Sub
#End Region

#Region "Funciones para la Barra"
    Private Sub OpcionMnuTools(ByVal vOpcion As TipoOpcionForma)
        'ToolStripMenu.Enabled = False
        If vOpcion = TipoOpcionForma.tOpcInsertar Then
            'ALTAS
            comboMnu.SelectedIndex = 0
            OpcionForma = TipoOpcionForma.tOpcInsertar
            ActivaCampos(False, TipoOpcActivaCampos.tOpcINICIALIZA)
            ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)
            LimpiaCampos()
            txtUsuarioSQL.Focus()
        ElseIf vOpcion = TipoOpcionForma.tOpcModificar Then
            'MODIFICAR
            'ToolStripMenu.Enabled = False
            comboMnu.SelectedIndex = 1
            OpcionForma = TipoOpcionForma.tOpcModificar
            ActivaCampos(True, TipoOpcActivaCampos.tOpcINICIALIZA)
            ActivaBotones(True, TipoOpcActivaBoton.tOpcInsertar)
            txtUsuarioSQL.Focus()

        ElseIf vOpcion = TipoOpcionForma.tOpcEliminar Then
            'ELIMINAR
            comboMnu.SelectedIndex = 2
            OpcionForma = TipoOpcionForma.tOpcEliminar
            ActivaCampos(False, TipoOpcActivaCampos.tOpcINICIALIZA)
            ActivaBotones(True, TipoOpcActivaBoton.tOpcConsulta)

            txtUsuarioSQL.Focus()

        ElseIf vOpcion = TipoOpcionForma.tOpcConsultar Then
            'CONSULTA
            comboMnu.SelectedIndex = 3
            OpcionForma = TipoOpcionForma.tOpcConsultar
            ActivaCampos(False, TipoOpcActivaCampos.tOpcINICIALIZA)
            ActivaBotones(True, TipoOpcActivaBoton.tOpcConsulta)
            txtUsuarioSQL.Focus()
        End If
    End Sub

    Private Sub btnMnuModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMnuModificar.Click
        'MODIFICAR
        OpcionMnuTools(TipoOpcionForma.tOpcModificar)
    End Sub

#End Region

    Private Sub InicializaParametros()

        Existe = LlenaDatos("1", TablaBd, CampoLLave, TipoDato.TdNumerico)
        If Existe = KEY_RINCORRECTO Then
            NuevoParametros = True
            ActivaCampos(True, TipoOpcActivaCampos.tOpcINICIALIZA)
            ActivaBotones(True, TipoOpcActivaBoton.tOpcInicializa)
            'txtServidorxAfectar.Text = Inicio.sNomServidor
            'txtServidorxAfectar.Focus()
            'txtServidorxAfectar.SelectAll()
            txtUsuarioSQL.Focus()
            txtUsuarioSQL.SelectAll()
        Else
            NuevoParametros = False
        End If

    End Sub

    Private Sub frmParametros_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ActivaCampos(False, TipoOpcActivaCampos.tOpcDESHABTODOS)
        LimpiaCampos()

        'Pendientes
        'llenaEmpresas()

        ActivaBotones(False)
        CreaTablaConceptos(False)
        CreaTablaConceptos(True)
        InicializaParametros()
    End Sub

    Private Sub cmdTerminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Close()
    End Sub

    Private Sub cmdCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Cancelar()
    End Sub

    'Private Sub cmdOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Dim objAudita As New CapaNegocio.Tablas
    '    Dim strSQL As String
    '    Dim util As New CapaNegocio.Parametros
    '    Dim ObjSql As New ToolSQLs

    '    Try
    '        'If Existe = KEY_RCORRECTO Then
    '        If OpcionForma = TipoOpcionForma.tOpcModificar Then

    '            'strSQL = "UPDATE " & TablaBd & " SET UsuarioSQL = '" & txtUsuarioSQL.Text & _
    '            '"', PasswordSQL = '" & Criptografia.Encrypt(Trim(sPassUsuarioSQL), KEY_CONFIG_ENCRYPT) & _
    '            '"', AutentificaWin = " & IIf(chkAutWindows.Checked, 1, 0) & _
    '            '", Empresa = '" & txtEmpresa.Text & _
    '            '"',Direccion = '" & txtDireccion.Text & _
    '            '"',RFC = '" & txtRFC.Text & _
    '            '"',Telefono = '" & txtTelefono.Text & _
    '            '"',ColaImpresionFact = '" & txtColaImpresionFact.Text & _
    '            '"',ColaImpresionPed = '" & txtColaImpresionPed.Text & _
    '            '"',ColaImpresionRem = '" & txtColaImpresionRem.Text & _
    '            '"',ImprimeRegistro = " & IIf(chkImprimeRegistro.Checked, 1, 0) & _
    '            '",EmailEmpresa = '" & txtEmailEmpresa.Text & _
    '            '"',EmailPassEmpresa = '" & IIf(vPassEmail = txtEmailPassEmpresa.Text, txtEmailPassEmpresa.Text, Criptografia.Encrypt(Trim(txtEmailPassEmpresa.Text), KEY_CONFIG_ENCRYPT)) & _
    '            '"',ServidorPOP3 = '" & txtServidorPOP3.Text & _
    '            '"',PuertoPOP3 = '" & txtPuertoPOP3.Text & _
    '            '"',ServidorSMTP = '" & txtServidorSMTP.Text & _
    '            '"',PuertoSMTP = '" & txtPuertoSMTP.Text & _
    '            '"',ConexionSSL = " & IIf(chkConexionSSL.Checked, 1, 0) & _
    '            '" ,MensajeCorreo = '" & txtMensajeCorreo.Text & _
    '            '"' ,ListaGrupo = '" & txtListaGrupo.Text & _
    '            '"' ,PasswordFacturacion = '" & IIf(vPassFacturacion = txtPasswordFacturacion.Text, txtPasswordFacturacion.Text, Criptografia.Encrypt(txtPasswordFacturacion.Text, KEY_CONFIG_ENCRYPT)) & _
    '            '"' WHERE " & CampoLLave & " = '1'"

    '            strSQL = "UPDATE " & TablaBd & " SET UsuarioSQL = '" & txtUsuarioSQL.Text & _
    '            "', PasswordSQL = '" & Criptografia.Encrypt(Trim(sPassUsuarioSQL), KEY_CONFIG_ENCRYPT) & _
    '            "', AutentificaWin = " & IIf(chkAutWindows.Checked, 1, 0) & _
    '            ", Empresa = '" & txtEmpresa.Text & _
    '            "',Direccion = '" & txtDireccion.Text & _
    '            "',RFC = '" & txtRFC.Text & _
    '            "',Telefono = '" & txtTelefono.Text & _
    '            "',ImprimeRegistro = " & IIf(chkImprimeRegistro.Checked, 1, 0) & _
    '            ",EmailEmpresa = '" & txtEmailEmpresa.Text & _
    '            "',EmailPassEmpresa = '" & IIf(vPassEmail = txtEmailPassEmpresa.Text, txtEmailPassEmpresa.Text, Criptografia.Encrypt(Trim(txtEmailPassEmpresa.Text), KEY_CONFIG_ENCRYPT)) & _
    '            "',ServidorPOP3 = '" & txtServidorPOP3.Text & _
    '            "',PuertoPOP3 = '" & txtPuertoPOP3.Text & _
    '            "',ServidorSMTP = '" & txtServidorSMTP.Text & _
    '            "',PuertoSMTP = '" & txtPuertoSMTP.Text & _
    '            "',ConexionSSL = " & IIf(chkConexionSSL.Checked, 1, 0) & _
    '            " ,MensajeCorreo = '" & txtMensajeCorreo.Text & _
    '            "' ,ListaGrupo = '" & txtListaGrupo.Text & _
    '            "' ,PasswordFacturacion = '" & IIf(vPassFacturacion = txtPasswordFacturacion.Text, txtPasswordFacturacion.Text, Criptografia.Encrypt(txtPasswordFacturacion.Text, KEY_CONFIG_ENCRYPT)) & _
    '            "' WHERE " & CampoLLave & " = '1'"


    '            '"',EmailPassEmpresa = '" & Criptografia.Encrypt(Trim(txtEmailPassEmpresa.Text), KEY_CONFIG_ENCRYPT) & _

    '            ReDim Preserve ArraySql(indice)
    '            ArraySql(indice) = strSQL
    '            indice += 1

    '            strSQL = ObjSql.BorraTodosConceptos(False)
    '            ReDim Preserve ArraySql(indice)
    '            ArraySql(indice) = strSQL
    '            indice += 1

    '            If grdConceptos.Rows.Count > 0 Then
    '                For i = 0 To grdConceptos.Rows.Count - 1
    '                    strSQL = ObjSql.InsertaConcepto(grdConceptos.Item("gCIDCONCE01", i).Value, grdConceptos.Item("gCCODIGOC01", i).Value, _
    '                    grdConceptos.Item("gCNOMBREC01", i).Value, grdConceptos.Item("gCIDDOCUM01", i).Value, grdConceptos.Item("gCPLAMIGCFD", i).Value, grdConceptos.Item("gCDOCTOAC01", i).Value, False)

    '                    ReDim Preserve ArraySql(indice)
    '                    ArraySql(indice) = strSQL
    '                    indice += 1
    '                Next
    '            End If

    '            strSQL = ObjSql.BorraTodosConceptos(True)
    '            ReDim Preserve ArraySql(indice)
    '            ArraySql(indice) = strSQL
    '            indice += 1

    '            If grdConceptos.Rows.Count > 0 Then
    '                For i = 0 To grdConceptos.Rows.Count - 1
    '                    strSQL = ObjSql.InsertaConcepto(grdConceptos.Item("gCIDCONCE01", i).Value, grdConceptos.Item("gCCODIGOC01", i).Value, _
    '                    grdConceptos.Item("gCNOMBREC01", i).Value, grdConceptos.Item("gCIDDOCUM01", i).Value, grdConceptos.Item("gCPLAMIGCFD", i).Value, grdConceptos.Item("gCDOCTOAC01", i).Value, True)

    '                    ReDim Preserve ArraySql(indice)
    '                    ArraySql(indice) = strSQL
    '                    indice += 1
    '                Next
    '            End If


    '        ElseIf NuevoParametros Then

    '            'strSQL = "INSERT INTO parametros (IdParametro, UsuarioSQL, PasswordSQL, AutentificaWin, Empresa, Direccion, RFC, " & _
    '            '" Telefono,ColaImpresionFact,EmailEmpresa,EmailPassEmpresa,ImprimeRegistro,ServidorPOP3,PuertoPOP3,ServidorSMTP, " & _
    '            '"PuertoSMTP,ConexionSSL,MensajeCorreo,PasswordFacturacion,ListaGrupo,ColaImpresionPed,ColaImpresionRem) " & _
    '            '" VALUES (1,'" & txtUsuarioSQL.Text & "','" & Criptografia.Encrypt(Trim(sPassUsuarioSQL), KEY_CONFIG_ENCRYPT) & "'," & IIf(chkAutWindows.Checked, 1, 0) & _
    '            '",'" & txtEmpresa.Text & "','" & txtDireccion.Text & "','" & txtRFC.Text & "','" & txtTelefono.Text & "','" & _
    '            'txtColaImpresionFact.Text & "','" & txtEmailEmpresa.Text & "','" & _
    '            'IIf(vPassEmail = txtEmailPassEmpresa.Text, txtEmailPassEmpresa.Text, Criptografia.Encrypt(Trim(txtEmailPassEmpresa.Text), KEY_CONFIG_ENCRYPT)) & _
    '            '"'," & IIf(chkImprimeRegistro.Checked, 1, 0) & ",'" & txtServidorPOP3.Text & _
    '            '"','" & txtPuertoPOP3.Text & "','" & txtServidorSMTP.Text & "','" & txtPuertoSMTP.Text & _
    '            '"'," & IIf(chkConexionSSL.Checked, 1, 0) & ",'" & txtMensajeCorreo.Text & _
    '            '"','" & IIf(vPassFacturacion = txtPasswordFacturacion.Text, txtPasswordFacturacion.Text, Criptografia.Encrypt(txtPasswordFacturacion.Text, KEY_CONFIG_ENCRYPT)) & _
    '            '"','" & txtListaGrupo.Text & "','" & txtColaImpresionPed.Text & "','" & txtColaImpresionRem.Text & "') "

    '            strSQL = "INSERT INTO parametros (IdParametro, UsuarioSQL, PasswordSQL, AutentificaWin, Empresa, Direccion, RFC, " & _
    '            " Telefono, EmailEmpresa,EmailPassEmpresa,ImprimeRegistro,ServidorPOP3,PuertoPOP3,ServidorSMTP, " & _
    '            "PuertoSMTP,ConexionSSL,MensajeCorreo,PasswordFacturacion,ListaGrupo) " & _
    '            " VALUES (1,'" & txtUsuarioSQL.Text & "','" & Criptografia.Encrypt(Trim(sPassUsuarioSQL), KEY_CONFIG_ENCRYPT) & "'," & IIf(chkAutWindows.Checked, 1, 0) & _
    '            ",'" & txtEmpresa.Text & "','" & txtDireccion.Text & "','" & txtRFC.Text & "','" & txtTelefono.Text & "','" & txtEmailEmpresa.Text & "','" & _
    '            IIf(vPassEmail = txtEmailPassEmpresa.Text, txtEmailPassEmpresa.Text, Criptografia.Encrypt(Trim(txtEmailPassEmpresa.Text), KEY_CONFIG_ENCRYPT)) & _
    '            "'," & IIf(chkImprimeRegistro.Checked, 1, 0) & ",'" & txtServidorPOP3.Text & _
    '            "','" & txtPuertoPOP3.Text & "','" & txtServidorSMTP.Text & "','" & txtPuertoSMTP.Text & _
    '            "'," & IIf(chkConexionSSL.Checked, 1, 0) & ",'" & txtMensajeCorreo.Text & _
    '            "','" & IIf(vPassFacturacion = txtPasswordFacturacion.Text, txtPasswordFacturacion.Text, Criptografia.Encrypt(txtPasswordFacturacion.Text, KEY_CONFIG_ENCRYPT)) & _
    '            "','" & txtListaGrupo.Text & "') "




    '            ReDim Preserve ArraySql(indice)
    '            ArraySql(indice) = strSQL
    '            indice += 1

    '            strSQL = ObjSql.BorraTodosConceptos(False)
    '            ReDim Preserve ArraySql(indice)
    '            ArraySql(indice) = strSQL
    '            indice += 1
    '            If grdConceptos.Rows.Count > 0 Then
    '                For i = 0 To grdConceptos.Rows.Count - 1
    '                    strSQL = ObjSql.InsertaConcepto(grdConceptos.Item("gCIDCONCE01", i).Value, grdConceptos.Item("gCCODIGOC01", i).Value, _
    '                    grdConceptos.Item("gCNOMBREC01", i).Value, grdConceptos.Item("gCIDDOCUM01", i).Value, grdConceptos.Item("gCPLAMIGCFD", i).Value, grdConceptos.Item("gCDOCTOAC01", i).Value, False)

    '                    ReDim Preserve ArraySql(indice)
    '                    ArraySql(indice) = strSQL
    '                    indice += 1
    '                Next
    '            End If

    '            strSQL = ObjSql.BorraTodosConceptos(True)
    '            ReDim Preserve ArraySql(indice)
    '            ArraySql(indice) = strSQL
    '            indice += 1
    '            If grdConceptos.Rows.Count > 0 Then
    '                For i = 0 To grdConceptos.Rows.Count - 1
    '                    strSQL = ObjSql.InsertaConcepto(grdConceptos.Item("gCIDCONCE01", i).Value, grdConceptos.Item("gCCODIGOC01", i).Value, _
    '                    grdConceptos.Item("gCNOMBREC01", i).Value, grdConceptos.Item("gCIDDOCUM01", i).Value, grdConceptos.Item("gCPLAMIGCFD", i).Value, grdConceptos.Item("gCDOCTOAC01", i).Value, True)

    '                    ReDim Preserve ArraySql(indice)
    '                    ArraySql(indice) = strSQL
    '                    indice += 1
    '                Next
    '            End If


    '        End If
    '        'End If
    '        sNomUsuarioSQL = txtUsuarioSQL.Text
    '        'sPassUsuarioSQL = txtPasswordSQL.Text

    '        Dim obj As New CapaNegocio.Tablas
    '        Dim Respuesta As String

    '        Respuesta = obj.EjecutarSql(Inicio.CONSTR, "SQL", ArraySql, Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, indice)

    '        If Respuesta = "EFECTUADO" Then
    '            If OpcionForma = TipoOpcionForma.tOpcModificar Then
    '                'MsgBox("Parametros actualizados satisfactoriamente", MsgBoxStyle.Exclamation, Me.Text)

    '            ElseIf NuevoParametros Then
    '                'MsgBox("Parametros actualizados satisfactoriamente", MsgBoxStyle.Exclamation, Me.Text)
    '                Me.Close()
    '            End If

    '            SaveSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Nombre del Usuario SQL", sNomUsuarioSQL)

    '            'SaveSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Password del Usuario SQL", sPassUsuarioSQL)
    '            SaveSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Password del Usuario SQL", Criptografia.Encrypt(Trim(sPassUsuarioSQL), KEY_CONFIG_ENCRYPT))

    '            '15/ABRIL/2014
    '            'COLAS LOCALES
    '            SaveSetting(Process.GetCurrentProcess.ProcessName, "Configuracion", "ColaFacturaLocal", txtColaImpresionFact.Text)
    '            SaveSetting(Process.GetCurrentProcess.ProcessName, "Configuracion", "ColaRemisionLocal", txtColaImpresionRem.Text)
    '            SaveSetting(Process.GetCurrentProcess.ProcessName, "Configuracion", "ColaPedidoLocal", txtColaImpresionPed.Text)

    '            ActualizaParametros(txtUsuarioSQL.Text, txtPasswordSQL.Text, txtEmpresa.Text, txtDireccion.Text, txtRFC.Text, _
    '            txtTelefono.Text, txtEmailEmpresa.Text, txtEmailPassEmpresa.Text, chkImprimeRegistro.Checked, txtListaGrupo.Text)

    '            'If sAutentificacionWindows Then
    '            '    Inicio.CONSTR_COMPAQ = util.CreaConStrSQL(UCase(Parametro_ServidorxAfectar), UCase(Parametro_BaseDatosxAfectar), True, "", "")
    '            'Else
    '            '    Inicio.CONSTR_COMPAQ = util.CreaConStrSQL(UCase(Parametro_ServidorxAfectar), UCase(Parametro_BaseDatosxAfectar), False, sNomUsuarioSQL, sPassUsuarioSQL)
    '            'End If
    '        ElseIf Respuesta = "NOEFECTUADO" Then

    '        ElseIf Respuesta = "ERROR" Then
    '            indice = 0
    '            SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")
    '        End If

    '    Catch ex As Exception
    '        indice = 0
    '        'ConsecutivoAudita = 0
    '        LimpiaVariables()
    '        MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
    '    End Try
    '    Cancelar()
    '    'Existe = LlenaDatos("1", TablaBd, CampoLLave, TipoDato.TdNumerico)
    '    InicializaParametros()
    'End Sub


    Private Sub txtUsuarioSQLxAfectar_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtUsuarioSQL.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            e.Handled = True
            txtPasswordSQL.Focus()
            txtPasswordSQL.SelectAll()
        End If
    End Sub

    Private Sub txtUsuarioSQL_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles txtUsuarioSQL.MouseDown
        txtUsuarioSQL.Focus()
        txtUsuarioSQL.SelectAll()
    End Sub

    'Private Sub txtPasswordSQL_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPasswordSQL.KeyPress
    '    If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
    '        txtColaImpresion.Focus()
    '        txtColaImpresion.SelectAll()
    '    End If
    'End Sub

    Private Sub txtPasswordSQL_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles txtPasswordSQL.MouseDown
        txtPasswordSQL.Focus()
        txtPasswordSQL.SelectAll()
    End Sub

    'Private Sub txtColaImpresion_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
    '    If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
    '        txtColaImpresion.Focus()
    '        txtColaImpresion.SelectAll()
    '    End If
    'End Sub


    'Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
    '    txtPasswordSQL.PasswordChar = ""
    '    txtPasswordSQL.UseSystemPasswordChar = False
    'End Sub


    Private Sub btnVerPassSQL_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVerPassSQL.MouseDown
        txtPasswordSQL.PasswordChar = ""
        txtPasswordSQL.UseSystemPasswordChar = False
        txtPasswordSQL.Text = Criptografia.Decrypt(Trim(txtPasswordSQL.Text), KEY_CONFIG_ENCRYPT)
    End Sub

    Private Sub btnVerPassSQL_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVerPassSQL.MouseUp
        txtPasswordSQL.UseSystemPasswordChar = True
        txtPasswordSQL.PasswordChar = "*"c
        txtPasswordSQL.Text = Criptografia.Encrypt(Trim(txtPasswordSQL.Text), KEY_CONFIG_ENCRYPT)
    End Sub

    Private Sub btnVerPassEmail_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVerPassEmail.MouseDown
        txtEmailPassEmpresa.PasswordChar = ""
        txtEmailPassEmpresa.UseSystemPasswordChar = False
        txtEmailPassEmpresa.Text = Criptografia.Decrypt(Trim(txtEmailPassEmpresa.Text), KEY_CONFIG_ENCRYPT)
    End Sub

    'Private Sub btnVerPassEmail_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVerPassEmail.MouseUp
    '    txtEmailPassEmpresa.UseSystemPasswordChar = True
    '    txtEmailPassEmpresa.PasswordChar = "*"c
    '    txtEmailPassEmpresa.Text = Criptografia.Encrypt(Trim(txtEmailPassEmpresa.Text), KEY_CONFIG_ENCRYPT)
    'End Sub




    Private Sub btnVerPassEmail_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnVerPassEmail.MouseUp
        txtEmailPassEmpresa.UseSystemPasswordChar = True
        txtEmailPassEmpresa.PasswordChar = "*"c
        txtEmailPassEmpresa.Text = Criptografia.Encrypt(Trim(txtEmailPassEmpresa.Text), KEY_CONFIG_ENCRYPT)
    End Sub

    Private Sub txtPasswordSQL_TextChanged(sender As Object, e As EventArgs) Handles txtPasswordSQL.TextChanged

    End Sub

    Private Sub btnVerPassSQL_Click(sender As Object, e As EventArgs) Handles btnVerPassSQL.Click

    End Sub

    Private Sub btnVerPassEmail_Click(sender As Object, e As EventArgs) Handles btnVerPassEmail.Click

    End Sub

    Private Sub btnVerPassFacturacion_Click(sender As Object, e As EventArgs) Handles btnVerPassFacturacion.Click

    End Sub

    Private Sub btnVerPassFacturacion_MouseDown(sender As Object, e As MouseEventArgs) Handles btnVerPassFacturacion.MouseDown
        txtPasswordFacturacion.PasswordChar = ""
        txtPasswordFacturacion.UseSystemPasswordChar = False
        txtPasswordFacturacion.Text = Criptografia.Decrypt(Trim(txtPasswordFacturacion.Text), KEY_CONFIG_ENCRYPT)
    End Sub

    Private Sub btnVerPassFacturacion_MouseUp(sender As Object, e As MouseEventArgs) Handles btnVerPassFacturacion.MouseUp
        txtPasswordFacturacion.UseSystemPasswordChar = True
        txtPasswordFacturacion.PasswordChar = "*"c
        txtPasswordFacturacion.Text = Criptografia.Encrypt(Trim(txtPasswordFacturacion.Text), KEY_CONFIG_ENCRYPT)
    End Sub

    Private Sub txtidConcepto_EnabledChanged(sender As Object, e As EventArgs) Handles txtidConcepto.EnabledChanged
        cmdBuscaConcepto.Enabled = txtidConcepto.Enabled
    End Sub

    Private Sub txtidConcepto_KeyDown(sender As Object, e As KeyEventArgs) Handles txtidConcepto.KeyDown
        If e.KeyCode = Keys.F3 Then
            cmdBuscaConcepto_Click(sender, e)
        End If
    End Sub

    Private Sub txtidConcepto_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtidConcepto.KeyPress
        If txtidConcepto.Text.Length = 0 Then
            txtidConcepto.Focus()
            txtidConcepto.SelectAll()
        Else
            If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
                e.Handled = True
                Salida = True
                '1.- Checar si Existe en el Grid, si ya es parte de la relacion
                Existe = ChecaConceptoGrid(txtidConcepto.Text, False)
                If Existe = KEY_RINCORRECTO Then
                    '2.- Checar en la Base de AdminPaq si existe el cliente

                    'Existe = LlenaDatosFox(txtidConcepto.Text, "mgw10006", "CIDCONCE01", TipoDato.TdCadena)
                    Existe = LlenaDatosFox(txtidConcepto.Text, "mgw10006", "CCODIGOC01", TipoDato.TdCadena)
                    If Existe = KEY_RINCORRECTO Then
                        MsgBox("!! El Concepto : " & txtidConcepto.Text & " no existe en el Adminpaq", vbInformation, "Aviso" & Me.Text)
                        Cancelar()
                        Exit Sub
                    End If
                End If
                btnAgregar.Focus()

                Salida = False
            End If
        End If
    End Sub

    Private Function ChecaConceptoGrid(ByVal IdConcepto As String, ByVal EsExterno As Boolean) As String
        ChecaConceptoGrid = KEY_RINCORRECTO
        If EsExterno Then
            'If grdConceptosExt.Rows.Count > 0 Then
            '    For i = 0 To grdConceptosExt.Rows.Count - 1
            '        If IdConcepto = grdConceptosExt.Item("gCCODIGOC01", i).Value Then
            '            txtCNOMBREC01Ext.Text = grdConceptosExt.Item("gCNOMBREC01", i).Value
            '            vIndiceConceptoExt = i
            '            ChecaConceptoGrid = KEY_RCORRECTO
            '            Exit For
            '        End If
            '    Next
            'End If

        Else
            If grdConceptos.Rows.Count > 0 Then
                For i = 0 To grdConceptos.Rows.Count - 1
                    If IdConcepto = grdConceptos.Item("gCCODIGOC01", i).Value Then
                        txtCNOMBREC01.Text = grdConceptos.Item("gCNOMBREC01", i).Value
                        vIndiceConcepto = i
                        ChecaConceptoGrid = KEY_RCORRECTO
                        Exit For
                    End If
                Next
            End If

        End If
    End Function

    Private Sub txtidConcepto_Leave(sender As Object, e As EventArgs) Handles txtidConcepto.Leave
        'If Salida Or Me.ActiveControl.Name = txtNombreGrupo.Name Then Exit Sub
        If Salida Then Exit Sub
        If txtidConcepto.Text.Length = 0 Then
            txtidConcepto.Focus()
            txtidConcepto.SelectAll()
        Else
            If txtidConcepto.Enabled Then
                Salida = True
                'AQUI VA EL CODIGO
                '1.- Checar si Existe en el Grid, si ya es parte de la relacion
                Existe = ChecaConceptoGrid(txtidConcepto.Text, False)
                If Existe = KEY_RINCORRECTO Then
                    '2.- Checar en la Base de AdminPaq si existe el cliente
                    'ExisteCliADMPAQ = LlenaDatosFox(txtIdCliente.Text, "mgw10002", "CCODIGOC01", TipoDato.TdCadena)
                    Existe = LlenaDatosFox(txtidConcepto.Text, "mgw10006", "CCODIGOC01", TipoDato.TdCadena)
                    If Existe = KEY_RINCORRECTO Then
                        MsgBox("!! El Concepto : " & txtidConcepto.Text & " no existe en el Adminpaq", vbInformation, "Aviso" & Me.Text)
                        Cancelar()
                        Exit Sub
                    End If
                End If
                btnAgregar.Focus()
                Salida = False
            End If
        End If
    End Sub

    Private Sub txtidConcepto_TextChanged(sender As Object, e As EventArgs) Handles txtidConcepto.TextChanged

    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        If txtidConcepto.Text <> "" Then
            '0.- VErificar si existe el cliente en el GRid
            Existe = ChecaConceptoGrid(txtidConcepto.Text, False)
            If Existe = KEY_RINCORRECTO Then
                'AGREGARLO AL GRID
                'VERIFICAR SI TIENE LISTA DE PRECIOS E INDICAR SI LO VAS A SOBRESCRIBIR - OJOOOOO
                'Existe = LlenaDatos(txtIdCliente.Text, "LISTAPRECIOS", "CCODIGOC01_CLI", TipoDato.TdCadena, Inicio.CONSTR, txtCveGrupo.Text)
                If InsertaConcepto(False) Then
                    LimpiaCamposConcepto(False)
                Else
                    Exit Sub
                End If
            End If
        End If
    End Sub

    Private Function InsertaConcepto(EsExterno As Boolean) As Boolean
        Try
            If EsExterno Then
                myDataRow = MyTablaConceptosExt.NewRow()
                myDataRow("tdCIDCONCE01") = vCIDCONCE01Ext
                'myDataRow("tdCCODIGOC01") = txtidConceptoExt.Text
                'myDataRow("tdCNOMBREC01") = txtCNOMBREC01Ext.Text
                myDataRow("tdCIDDOCUM01") = vCIDDOCUM01Ext
                myDataRow("tdCPLAMIGCFD") = vCPLAMIGCFDExt
                myDataRow("tdCDOCTOAC01") = vCDOCTOAC01Ext
                MyTablaConceptosExt.Rows.Add(myDataRow)
            Else
                myDataRow = MyTablaConceptos.NewRow()
                myDataRow("tdCIDCONCE01") = vCIDCONCE01
                myDataRow("tdCCODIGOC01") = txtidConcepto.Text
                myDataRow("tdCNOMBREC01") = txtCNOMBREC01.Text
                myDataRow("tdCIDDOCUM01") = vCIDDOCUM01
                myDataRow("tdCPLAMIGCFD") = vCPLAMIGCFD
                myDataRow("tdCDOCTOAC01") = vCDOCTOAC01
                MyTablaConceptos.Rows.Add(myDataRow)
            End If
            'vCIDCONCE01


            If InsertaRegGrid(EsExterno) Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Return False
        End Try

    End Function

    Private Sub LimpiaCamposConcepto(ByVal EsExterno As Boolean)
        If EsExterno Then
            'txtidConceptoExt.Text = ""
            'txtCNOMBREC01Ext.Text = ""
            vCIDCONCE01Ext = ""
            vCIDDOCUM01Ext = 0
            vCPLAMIGCFDExt = ""
            'txtidConceptoExt.Focus()

        Else
            txtidConcepto.Text = ""
            txtCNOMBREC01.Text = ""
            vCIDCONCE01 = ""
            vCIDDOCUM01 = 0
            vCPLAMIGCFD = ""
            txtidConcepto.Focus()

        End If
    End Sub

    Private Sub CreaTablaConceptos(ByVal EsExterno As Boolean)
        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Int64")
        myDataColumn.ColumnName = "tdCIDCONCE01"
        myDataColumn.ReadOnly = False
        myDataColumn.Unique = False
        ' Add the Column to the DataColumnCollection.
        If EsExterno Then
            MyTablaConceptosExt.Columns.Add(myDataColumn)
        Else
            MyTablaConceptos.Columns.Add(myDataColumn)
        End If


        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.String")
        myDataColumn.ColumnName = "tdCCODIGOC01"
        myDataColumn.ReadOnly = False
        myDataColumn.Unique = False
        If EsExterno Then
            MyTablaConceptosExt.Columns.Add(myDataColumn)
        Else
            MyTablaConceptos.Columns.Add(myDataColumn)
        End If

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.String")
        myDataColumn.ColumnName = "tdCNOMBREC01"
        myDataColumn.ReadOnly = False
        myDataColumn.Unique = False
        If EsExterno Then
            MyTablaConceptosExt.Columns.Add(myDataColumn)
        Else
            MyTablaConceptos.Columns.Add(myDataColumn)
        End If

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Int64")
        myDataColumn.ColumnName = "tdCIDDOCUM01"
        myDataColumn.ReadOnly = False
        myDataColumn.Unique = False
        If EsExterno Then
            MyTablaConceptosExt.Columns.Add(myDataColumn)
        Else
            MyTablaConceptos.Columns.Add(myDataColumn)
        End If

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.String")
        myDataColumn.ColumnName = "tdCPLAMIGCFD"
        myDataColumn.ReadOnly = False
        myDataColumn.Unique = False
        If EsExterno Then
            MyTablaConceptosExt.Columns.Add(myDataColumn)
        Else
            MyTablaConceptos.Columns.Add(myDataColumn)
        End If

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Boolean")
        myDataColumn.ColumnName = "tdCDOCTOAC01"
        myDataColumn.ReadOnly = False
        myDataColumn.Unique = False
        If EsExterno Then
            MyTablaConceptosExt.Columns.Add(myDataColumn)
        Else
            MyTablaConceptos.Columns.Add(myDataColumn)
        End If

    End Sub

    Private Function InsertaRegGrid(ByVal EsExterno As Boolean) As Boolean
        Try
            If EsExterno Then
                'grdConceptosExt.Rows.Clear()

                'For i = 0 To MyTablaConceptosExt.DefaultView.Count - 1
                '    grdConceptosExt.Rows.Add()
                '    grdConceptosExt.Item("gExtConsecutivo", i).Value = i + 1
                '    grdConceptosExt.Item("gExtCIDCONCE01", i).Value = MyTablaConceptosExt.DefaultView.Item(i).Item("tdCIDCONCE01").ToString
                '    grdConceptosExt.Item("gExtCCODIGOC01", i).Value = MyTablaConceptosExt.DefaultView.Item(i).Item("tdCCODIGOC01").ToString
                '    grdConceptosExt.Item("gExtCNOMBREC01", i).Value = MyTablaConceptosExt.DefaultView.Item(i).Item("tdCNOMBREC01").ToString
                '    grdConceptosExt.Item("gExtCIDDOCUM01", i).Value = MyTablaConceptosExt.DefaultView.Item(i).Item("tdCIDDOCUM01").ToString
                '    grdConceptosExt.Item("gExtCPLAMIGCFD", i).Value = MyTablaConceptosExt.DefaultView.Item(i).Item("tdCPLAMIGCFD").ToString
                '    grdConceptosExt.Item("gExtCDOCTOAC01", i).Value = MyTablaConceptosExt.DefaultView.Item(i).Item("tdCDOCTOAC01")
                'Next
                'grdConceptosExt.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
            Else
                grdConceptos.Rows.Clear()

                For i = 0 To MyTablaConceptos.DefaultView.Count - 1
                    grdConceptos.Rows.Add()
                    grdConceptos.Item("gConsecutivo", i).Value = i + 1
                    grdConceptos.Item("gCIDCONCE01", i).Value = MyTablaConceptos.DefaultView.Item(i).Item("tdCIDCONCE01").ToString
                    grdConceptos.Item("gCCODIGOC01", i).Value = MyTablaConceptos.DefaultView.Item(i).Item("tdCCODIGOC01").ToString
                    grdConceptos.Item("gCNOMBREC01", i).Value = MyTablaConceptos.DefaultView.Item(i).Item("tdCNOMBREC01").ToString
                    grdConceptos.Item("gCIDDOCUM01", i).Value = MyTablaConceptos.DefaultView.Item(i).Item("tdCIDDOCUM01").ToString
                    grdConceptos.Item("gCPLAMIGCFD", i).Value = MyTablaConceptos.DefaultView.Item(i).Item("tdCPLAMIGCFD").ToString
                    grdConceptos.Item("gCDOCTOAC01", i).Value = MyTablaConceptos.DefaultView.Item(i).Item("tdCDOCTOAC01")
                Next
                grdConceptos.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
            End If


            Return True
        Catch ex As Exception
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, Me.Text)
            Return False
        End Try

    End Function

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        If txtidConcepto.Text <> "" Then
            '0.- VErificar si existe el cliente en el GRid
            Existe = ChecaConceptoGrid(txtidConcepto.Text, False)
            If Existe = KEY_RINCORRECTO Then
                'MsgBox("!! El Concepto : " & txtidConcepto.Text & " no se encuentra en la Relacion Grupo: " & txtCveGrupo.Text & " Cliente:" & txtIdCliente.Text, vbInformation, "Aviso" & Me.Text)
                'LimpiaCamposCliente()

            Else
                'se borra

                If BorraConcepto(vIndiceConcepto, False) Then
                    LimpiaCamposConcepto(False)
                Else
                    LimpiaCamposConcepto(False)
                    Exit Sub
                End If

            End If

        End If
    End Sub

    Private Function BorraConcepto(ByVal indice As Integer, ByVal EsExterno As Boolean) As Boolean
        Try
            If EsExterno Then
                MyTablaConceptosExt.DefaultView.Delete(indice)
            Else
                MyTablaConceptos.DefaultView.Delete(indice)
            End If

            If InsertaRegGrid(EsExterno) Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Sub grdConceptos_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles grdConceptos.CellContentClick

    End Sub

    Private Sub grdConceptos_CellMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles grdConceptos.CellMouseClick

    End Sub

    Private Sub grdConceptos_CellMouseDoubleClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles grdConceptos.CellMouseDoubleClick
        If Salida Then Exit Sub
        txtidConcepto.Text = Trim(grdConceptos.Rows(grdConceptos.CurrentRow.Index).Cells("gCCODIGOC01").Value & "")
        txtCNOMBREC01.Text = grdConceptos.Rows(grdConceptos.CurrentRow.Index).Cells("gCNOMBREC01").Value
        txtidConcepto_Leave(sender, e)

    End Sub

    Private Sub cmdBuscaConcepto_Click(sender As Object, e As EventArgs) Handles cmdBuscaConcepto.Click
        'txtidConcepto.Text = DespliegaGrid("Conceptos", Inicio.ConStrFox, CampoLLave, "CIDDOCUM01 = 3 or CIDDOCUM01 = 4 or CIDDOCUM01 = 2")
        txtidConcepto.Text = DespliegaGrid("Conceptos", Inicio.ConStrFox, CampoLLave, "CIDDOCUM01 = 34 or CIDDOCUM01 = 33 or CIDDOCUM01 = 32")
        txtidConcepto_Leave(sender, e)
    End Sub

    Private Sub txtListaGrupo_EnabledChanged(sender As Object, e As EventArgs) Handles txtBodegaSalDefault.EnabledChanged
        cmdBuscaGrupo.Enabled = txtBodegaSalDefault.Enabled
    End Sub

    Private Sub txtListaGrupo_KeyDown(sender As Object, e As KeyEventArgs) Handles txtBodegaSalDefault.KeyDown
        If e.KeyCode = Keys.F3 Then
            cmdBuscaGrupo_Click(sender, e)
        End If
    End Sub

    Private Sub txtListaGrupo_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtBodegaSalDefault.KeyPress
        'KeyPress()
        If txtBodegaSalDefault.Text.Length = 0 Then
            txtBodegaSalDefault.Focus()
            txtBodegaSalDefault.SelectAll()
        Else
            If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
                e.Handled = True
                Salida = True
                'Existe = LlenaDatos(txtBodegaSalDefault.Text, "CatGrupos", "CveGrupo", TipoDato.TdCadena)
                '12/FEB/2016
                Existe = LlenaDatosFox(txtBodegaSalDefault.Text, "BODEGASAL", "CCODIGOC01", TipoDato.TdCadena)
                If Existe = KEY_RINCORRECTO Then
                    MsgBox("!! El Almacen de Salida: " & txtBodegaSalDefault.Text & " No Existe en la Base de Datos de AdminPaq", vbInformation, "Aviso" & Me.Text)
                    'MsgBox("!! El Grupo : " & txtBodegaSalDefault.Text & " no existe", vbInformation, "Aviso" & Me.Text)
                    Cancelar()
                    Exit Sub

                End If
                'btnAgregar.Focus()

                Salida = False
            End If
        End If
    End Sub

    Private Sub txtBodegaSalDefault_Leave(sender As Object, e As EventArgs) Handles txtBodegaSalDefault.Leave
        'Leave()
        If Salida Then Exit Sub
        If txtBodegaSalDefault.Text.Length = 0 Then
            txtBodegaSalDefault.Focus()
            txtBodegaSalDefault.SelectAll()
        Else
            If txtBodegaSalDefault.Enabled Then
                Salida = True
                'Existe = LlenaDatos(txtBodegaSalDefault.Text, "CatGrupos", "CveGrupo", TipoDato.TdCadena)
                'Existe = LlenaDatosFox(txtBodegaSalDefault.Text, "BODEGASAL", "CCODIGOC01", TipoDato.TdCadena, txtCABREVIA01.Text)
                '12/FEB/2016
                Existe = LlenaDatosFox(txtBodegaSalDefault.Text, "BODEGASAL", "CCODIGOC01", TipoDato.TdCadena)
                If Existe = KEY_RINCORRECTO Then
                    'MsgBox("!! El Grupo : " & txtBodegaSalDefault.Text & " no existe", vbInformation, "Aviso" & Me.Text)
                    MsgBox("!! El Almacen de Salida: " & txtBodegaSalDefault.Text & " No Existe en la Base de Datos de AdminPaq", vbInformation, "Aviso" & Me.Text)
                    Cancelar()
                    Exit Sub

                End If
                'btnAgregar.Focus()
                Salida = False
            End If
        End If
    End Sub

    Private Sub txtBodegaSalDefault_MouseDown(sender As Object, e As MouseEventArgs) Handles txtBodegaSalDefault.MouseDown
        txtBodegaSalDefault.Focus()
        txtBodegaSalDefault.SelectAll()
    End Sub


    Private Sub cmdBuscaGrupo_Click(sender As Object, e As EventArgs) Handles cmdBuscaGrupo.Click
        txtBodegaSalDefault.Text = DespliegaGrid("Almacenes", Inicio.ConStrFox, CampoLLave, " CIDALMACEN > 0 ")
        'txtBodegaSalDefault.Text = DespliegaGrid("Grupos", Inicio.CONSTR, CampoLLave)
        txtBodegaSalDefault_Leave(sender, e)
    End Sub


    Private Sub btnMnuOk_Click(sender As Object, e As EventArgs) Handles btnMnuOk.Click
        Dim objAudita As New CapaNegocio.Tablas
        Dim strSQL As String
        Dim util As New CapaNegocio.Parametros
        Dim ObjSql As New ToolSQLs

        Try
            'If Existe = KEY_RCORRECTO Then
            BuscaEmpresaAdminPAQ(Trim(cbEmpresa.Text))
            If OpcionForma = TipoOpcionForma.tOpcModificar Then

                'strSQL = "UPDATE " & TablaBd & " SET UsuarioSQL = '" & txtUsuarioSQL.Text & _
                '"', PasswordSQL = '" & Criptografia.Encrypt(Trim(sPassUsuarioSQL), KEY_CONFIG_ENCRYPT) & _
                '"', AutentificaWin = " & IIf(chkAutWindows.Checked, 1, 0) & _
                '", Empresa = '" & txtEmpresa.Text & _
                '"',Direccion = '" & txtDireccion.Text & _
                '"',RFC = '" & txtRFC.Text & _
                '"',Telefono = '" & txtTelefono.Text & _
                '"',ColaImpresionFact = '" & txtColaImpresionFact.Text & _
                '"',ColaImpresionPed = '" & txtColaImpresionPed.Text & _
                '"',ColaImpresionRem = '" & txtColaImpresionRem.Text & _
                '"',ImprimeRegistro = " & IIf(chkImprimeRegistro.Checked, 1, 0) & _
                '",EmailEmpresa = '" & txtEmailEmpresa.Text & _
                '"',EmailPassEmpresa = '" & IIf(vPassEmail = txtEmailPassEmpresa.Text, txtEmailPassEmpresa.Text, Criptografia.Encrypt(Trim(txtEmailPassEmpresa.Text), KEY_CONFIG_ENCRYPT)) & _
                '"',ServidorPOP3 = '" & txtServidorPOP3.Text & _
                '"',PuertoPOP3 = '" & txtPuertoPOP3.Text & _
                '"',ServidorSMTP = '" & txtServidorSMTP.Text & _
                '"',PuertoSMTP = '" & txtPuertoSMTP.Text & _
                '"',ConexionSSL = " & IIf(chkConexionSSL.Checked, 1, 0) & _
                '" ,MensajeCorreo = '" & txtMensajeCorreo.Text & _
                '"' ,ListaGrupo = '" & txtListaGrupo.Text & _
                '"' ,PasswordFacturacion = '" & IIf(vPassFacturacion = txtPasswordFacturacion.Text, txtPasswordFacturacion.Text, Criptografia.Encrypt(txtPasswordFacturacion.Text, KEY_CONFIG_ENCRYPT)) & _
                '"' WHERE " & CampoLLave & " = '1'"

                strSQL = "UPDATE " & TablaBd & " SET UsuarioSQL = '" & txtUsuarioSQL.Text & _
                "', PasswordSQL = '" & Criptografia.Encrypt(Trim(sPassUsuarioSQL), KEY_CONFIG_ENCRYPT) & _
                "', AutentificaWin = " & IIf(chkAutWindows.Checked, 1, 0) & _
                ", Empresa = '" & txtEmpresa.Text & _
                "',Direccion = '" & txtDireccion.Text & _
                "',RFC = '" & txtRFC.Text & _
                "',Telefono = '" & txtTelefono.Text & _
                "',ImprimeRegistro = " & IIf(chkImprimeRegistro.Checked, 1, 0) & _
                ",EmailEmpresa = '" & txtEmailEmpresa.Text & _
                "',EmailPassEmpresa = '" & IIf(vPassEmail = txtEmailPassEmpresa.Text, txtEmailPassEmpresa.Text, Criptografia.Encrypt(Trim(txtEmailPassEmpresa.Text), KEY_CONFIG_ENCRYPT)) & _
                "',ServidorPOP3 = '" & txtServidorPOP3.Text & _
                "',PuertoPOP3 = '" & txtPuertoPOP3.Text & _
                "',ServidorSMTP = '" & txtServidorSMTP.Text & _
                "',PuertoSMTP = '" & txtPuertoSMTP.Text & _
                "',ConexionSSL = " & IIf(chkConexionSSL.Checked, 1, 0) & _
                " ,MensajeCorreo = '" & txtMensajeCorreo.Text & _
                "' ,ListaGrupo = '" & txtBodegaSalDefault.Text & _
                "' ,PasswordFacturacion = '" & IIf(vPassFacturacion = txtPasswordFacturacion.Text, txtPasswordFacturacion.Text, Criptografia.Encrypt(txtPasswordFacturacion.Text, KEY_CONFIG_ENCRYPT)) & _
                "', CIDEMPRESA = " & vIdEmpresa & _
                ", CRUTADATOS = '" & Mid(Trim(cbEmpresa.SelectedValue), 1, Trim(cbEmpresa.SelectedValue).Length - 1) & _
                "', CNOMBREE01 = '" & Trim(cbEmpresa.Text) & _
                "', CCODIGOA01_SALdefault = '" & txtBodegaSalDefault.Text & _
                "' WHERE " & CampoLLave & " = '1'"

                '                "', CIDALMACEN_SALdefault = " & vCIDALMACEN & _
                '"', CPLAMIGCFD = '" & Trim(txtFormatoFac.Text) & _
                '"',EmailPassEmpresa = '" & Criptografia.Encrypt(Trim(txtEmailPassEmpresa.Text), KEY_CONFIG_ENCRYPT) & _

                ReDim Preserve ArraySql(indice)
                ArraySql(indice) = strSQL
                indice += 1

                strSQL = ObjSql.BorraTodosConceptos(False)
                ReDim Preserve ArraySql(indice)
                ArraySql(indice) = strSQL
                indice += 1

                If grdConceptos.Rows.Count > 0 Then
                    For i = 0 To grdConceptos.Rows.Count - 1
                        strSQL = ObjSql.InsertaConcepto(grdConceptos.Item("gCIDCONCE01", i).Value, grdConceptos.Item("gCCODIGOC01", i).Value, _
                        grdConceptos.Item("gCNOMBREC01", i).Value, grdConceptos.Item("gCIDDOCUM01", i).Value, grdConceptos.Item("gCPLAMIGCFD", i).Value, grdConceptos.Item("gCDOCTOAC01", i).Value, False)

                        ReDim Preserve ArraySql(indice)
                        ArraySql(indice) = strSQL
                        indice += 1
                    Next
                End If

                If vCIDALMACEN > 0 Then
                    'CATALOGO DE ALMACENES
                    strSQL = ObjSql.InsertaAlmacen(vCIDALMACEN, txtBodegaSalDefault.Text, lblNomBodegaSalDefault.Text)
                    ReDim Preserve ArraySql(indice)
                    ArraySql(indice) = strSQL
                    indice += 1
                End If

                ''Insertamos los conceptos Externos
                'strSQL = ObjSql.BorraTodosConceptos(True)
                'ReDim Preserve ArraySql(indice)
                'ArraySql(indice) = strSQL
                'indice += 1

                'If grdConceptosExt.Rows.Count > 0 Then
                '    For i = 0 To grdConceptosExt.Rows.Count - 1
                '        strSQL = ObjSql.InsertaConcepto(grdConceptosExt.Item("gExtCIDCONCE01", i).Value, _
                '                                        grdConceptosExt.Item("gExtCCODIGOC01", i).Value, _
                '                                        grdConceptosExt.Item("gExtCNOMBREC01", i).Value, _
                '                                        grdConceptosExt.Item("gExtCIDDOCUM01", i).Value, _
                '                                        grdConceptosExt.Item("gExtCPLAMIGCFD", i).Value, _
                '                                        grdConceptosExt.Item("gExtCDOCTOAC01", i).Value, True)

                '        ReDim Preserve ArraySql(indice)
                '        ArraySql(indice) = strSQL
                '        indice += 1
                '    Next
                'End If

            ElseIf NuevoParametros Then

                'strSQL = "INSERT INTO parametros (IdParametro, UsuarioSQL, PasswordSQL, AutentificaWin, Empresa, Direccion, RFC, " & _
                '" Telefono,ColaImpresionFact,EmailEmpresa,EmailPassEmpresa,ImprimeRegistro,ServidorPOP3,PuertoPOP3,ServidorSMTP, " & _
                '"PuertoSMTP,ConexionSSL,MensajeCorreo,PasswordFacturacion,ListaGrupo,ColaImpresionPed,ColaImpresionRem) " & _
                '" VALUES (1,'" & txtUsuarioSQL.Text & "','" & Criptografia.Encrypt(Trim(sPassUsuarioSQL), KEY_CONFIG_ENCRYPT) & "'," & IIf(chkAutWindows.Checked, 1, 0) & _
                '",'" & txtEmpresa.Text & "','" & txtDireccion.Text & "','" & txtRFC.Text & "','" & txtTelefono.Text & "','" & _
                'txtColaImpresionFact.Text & "','" & txtEmailEmpresa.Text & "','" & _
                'IIf(vPassEmail = txtEmailPassEmpresa.Text, txtEmailPassEmpresa.Text, Criptografia.Encrypt(Trim(txtEmailPassEmpresa.Text), KEY_CONFIG_ENCRYPT)) & _
                '"'," & IIf(chkImprimeRegistro.Checked, 1, 0) & ",'" & txtServidorPOP3.Text & _
                '"','" & txtPuertoPOP3.Text & "','" & txtServidorSMTP.Text & "','" & txtPuertoSMTP.Text & _
                '"'," & IIf(chkConexionSSL.Checked, 1, 0) & ",'" & txtMensajeCorreo.Text & _
                '"','" & IIf(vPassFacturacion = txtPasswordFacturacion.Text, txtPasswordFacturacion.Text, Criptografia.Encrypt(txtPasswordFacturacion.Text, KEY_CONFIG_ENCRYPT)) & _
                '"','" & txtListaGrupo.Text & "','" & txtColaImpresionPed.Text & "','" & txtColaImpresionRem.Text & "') "

                strSQL = "INSERT INTO parametros (IdParametro, UsuarioSQL, PasswordSQL, AutentificaWin, Empresa, Direccion, RFC, " & _
                " Telefono, EmailEmpresa,EmailPassEmpresa,ImprimeRegistro,ServidorPOP3,PuertoPOP3,ServidorSMTP, " & _
                "PuertoSMTP,ConexionSSL,MensajeCorreo,PasswordFacturacion,ListaGrupo,CIDEMPRESA,CRUTADATOS,CNOMBREE01, " & _
                "CPLAMIGCFD,CCODIGOA01_SALdefault) " & _
                " VALUES (1,'" & txtUsuarioSQL.Text & "','" & Criptografia.Encrypt(Trim(sPassUsuarioSQL), KEY_CONFIG_ENCRYPT) & "'," & IIf(chkAutWindows.Checked, 1, 0) & _
                ",'" & txtEmpresa.Text & "','" & txtDireccion.Text & "','" & txtRFC.Text & "','" & txtTelefono.Text & "','" & txtEmailEmpresa.Text & "','" & _
                IIf(vPassEmail = txtEmailPassEmpresa.Text, txtEmailPassEmpresa.Text, Criptografia.Encrypt(Trim(txtEmailPassEmpresa.Text), KEY_CONFIG_ENCRYPT)) & _
                "'," & IIf(chkImprimeRegistro.Checked, 1, 0) & ",'" & txtServidorPOP3.Text & _
                "','" & txtPuertoPOP3.Text & "','" & txtServidorSMTP.Text & "','" & txtPuertoSMTP.Text & _
                "'," & IIf(chkConexionSSL.Checked, 1, 0) & ",'" & txtMensajeCorreo.Text & _
                "','" & IIf(vPassFacturacion = txtPasswordFacturacion.Text, txtPasswordFacturacion.Text, Criptografia.Encrypt(txtPasswordFacturacion.Text, KEY_CONFIG_ENCRYPT)) & _
                "','" & txtBodegaSalDefault.Text & "'," & vIdEmpresa & _
                ",'" & Mid(Trim(cbEmpresa.SelectedValue), 1, Trim(cbEmpresa.SelectedValue).Length - 1) & _
                "','" & Trim(cbEmpresa.Text) & "','" & Trim(txtFormatoFac.Text) & "','" & txtBodegaSalDefault.Text & "')"

                ReDim Preserve ArraySql(indice)
                ArraySql(indice) = strSQL
                indice += 1

                strSQL = ObjSql.BorraTodosConceptos(False)
                ReDim Preserve ArraySql(indice)
                ArraySql(indice) = strSQL
                indice += 1
                If grdConceptos.Rows.Count > 0 Then
                    For i = 0 To grdConceptos.Rows.Count - 1
                        strSQL = ObjSql.InsertaConcepto(grdConceptos.Item("gCIDCONCE01", i).Value, grdConceptos.Item("gCCODIGOC01", i).Value, _
                        grdConceptos.Item("gCNOMBREC01", i).Value, grdConceptos.Item("gCIDDOCUM01", i).Value, grdConceptos.Item("gCPLAMIGCFD", i).Value, grdConceptos.Item("gCDOCTOAC01", i).Value, False)

                        ReDim Preserve ArraySql(indice)
                        ArraySql(indice) = strSQL
                        indice += 1
                    Next
                End If

                If vCIDALMACEN > 0 Then
                    'CATALOGO DE ALMACENES
                    strSQL = ObjSql.InsertaAlmacen(vCIDALMACEN, txtBodegaSalDefault.Text, lblNomBodegaSalDefault.Text)
                    ReDim Preserve ArraySql(indice)
                    ArraySql(indice) = strSQL
                    indice += 1
                End If

                'strSQL = ObjSql.BorraTodosConceptos(True)
                'ReDim Preserve ArraySql(indice)
                'ArraySql(indice) = strSQL
                'indice += 1
                'If grdConceptosExt.Rows.Count > 0 Then
                '    For i = 0 To grdConceptosExt.Rows.Count - 1
                '        strSQL = ObjSql.InsertaConcepto(grdConceptosExt.Item("gExtCIDCONCE01", i).Value, grdConceptosExt.Item("gExtCCODIGOC01", i).Value, _
                '        grdConceptosExt.Item("gExtCNOMBREC01", i).Value, grdConceptosExt.Item("gExtCIDDOCUM01", i).Value, grdConceptosExt.Item("gExtCPLAMIGCFD", i).Value, grdConceptosExt.Item("gExtCDOCTOAC01", i).Value, True)

                '        ReDim Preserve ArraySql(indice)
                '        ArraySql(indice) = strSQL
                '        indice += 1
                '    Next
                'End If


            End If
            'End If
            sNomUsuarioSQL = txtUsuarioSQL.Text
            'sPassUsuarioSQL = txtPasswordSQL.Text

            Dim obj As New CapaNegocio.Tablas
            Dim Respuesta As String

            Respuesta = obj.EjecutarSql(Inicio.CONSTR, "SQL", ArraySql, Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, indice)

            If Respuesta = "EFECTUADO" Then
                If OpcionForma = TipoOpcionForma.tOpcModificar Then
                    'MsgBox("Parametros actualizados satisfactoriamente", MsgBoxStyle.Exclamation, Me.Text)

                ElseIf NuevoParametros Then
                    'MsgBox("Parametros actualizados satisfactoriamente", MsgBoxStyle.Exclamation, Me.Text)
                    Me.Close()
                End If

                SaveSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Nombre del Usuario SQL", sNomUsuarioSQL)

                'SaveSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Password del Usuario SQL", sPassUsuarioSQL)
                SaveSetting(Process.GetCurrentProcess.ProcessName, "ServerSQL", "Password del Usuario SQL", Criptografia.Encrypt(Trim(sPassUsuarioSQL), KEY_CONFIG_ENCRYPT))

                '15/ABRIL/2014
                'COLAS LOCALES
                SaveSetting(Process.GetCurrentProcess.ProcessName, "Configuracion", "ColaFacturaLocal", txtColaImpresionFact.Text)
                SaveSetting(Process.GetCurrentProcess.ProcessName, "Configuracion", "ColaRemisionLocal", txtColaImpresionRem.Text)
                SaveSetting(Process.GetCurrentProcess.ProcessName, "Configuracion", "ColaPedidoLocal", txtColaImpresionPed.Text)

                ActualizaParametros(txtUsuarioSQL.Text, txtPasswordSQL.Text, txtEmpresa.Text, txtDireccion.Text, txtRFC.Text, _
                txtTelefono.Text, txtEmailEmpresa.Text, txtEmailPassEmpresa.Text, chkImprimeRegistro.Checked, _
                txtBodegaSalDefault.Text, vIdEmpresa, Mid(Trim(cbEmpresa.SelectedValue), 1, Trim(cbEmpresa.SelectedValue).Length - 1), _
                Trim(cbEmpresa.Text), Trim(txtFormatoFac.Text), vCIDALMACEN)

                'If sAutentificacionWindows Then
                '    Inicio.CONSTR_COMPAQ = util.CreaConStrSQL(UCase(Parametro_ServidorxAfectar), UCase(Parametro_BaseDatosxAfectar), True, "", "")
                'Else
                '    Inicio.CONSTR_COMPAQ = util.CreaConStrSQL(UCase(Parametro_ServidorxAfectar), UCase(Parametro_BaseDatosxAfectar), False, sNomUsuarioSQL, sPassUsuarioSQL)
                'End If
            ElseIf Respuesta = "NOEFECTUADO" Then

            ElseIf Respuesta = "ERROR" Then
                indice = 0
                SaveSetting(Process.GetCurrentProcess.ProcessName, KEY_CHECAERROR, KEY_ESTATUS, "")
            End If

        Catch ex As Exception
            indice = 0
            'ConsecutivoAudita = 0
            LimpiaVariables()
            MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "GENERAL Error")
        End Try
        Cancelar()
        'Existe = LlenaDatos("1", TablaBd, CampoLLave, TipoDato.TdNumerico)
        InicializaParametros()
    End Sub

    Private Sub btnMnuCancelar_Click(sender As Object, e As EventArgs) Handles btnMnuCancelar.Click
        Cancelar()
    End Sub

    Private Sub btnMnuSalir_Click(sender As Object, e As EventArgs) Handles btnMnuSalir.Click
        Me.Close()
    End Sub

    'Private Sub cmdBuscaConceptoExt_Click(sender As Object, e As EventArgs)
    '    txtidConceptoExt.Text = DespliegaGrid("Conceptos", Inicio.ConStrFox, CampoLLave, "CIDDOCUM01 = 3 or CIDDOCUM01 = 4 or CIDDOCUM01 = 2")
    '    txtidConceptoExt_Leave(sender, e)

    'End Sub

    'Private Sub txtidConceptoExt_EnabledChanged(sender As Object, e As EventArgs)
    '    cmdBuscaConceptoExt.Enabled = txtidConceptoExt.Enabled
    'End Sub

    'Private Sub txtidConceptoExt_KeyDown(sender As Object, e As KeyEventArgs)
    '    If e.KeyCode = Keys.F3 Then
    '        cmdBuscaConceptoExt_Click(sender, e)
    '    End If
    'End Sub

    'Private Sub txtidConceptoExt_KeyPress(sender As Object, e As KeyPressEventArgs)
    '    If txtidConceptoExt.Text.Length = 0 Then
    '        txtidConceptoExt.Focus()
    '        txtidConceptoExt.SelectAll()
    '    Else
    '        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
    '            e.Handled = True
    '            Salida = True
    '            '1.- Checar si Existe en el Grid, si ya es parte de la relacion
    '            Existe = ChecaConceptoGrid(txtidConceptoExt.Text, True)
    '            If Existe = KEY_RINCORRECTO Then
    '                '2.- Checar en la Base de AdminPaq si existe el cliente

    '                'Existe = LlenaDatosFox(txtidConcepto.Text, "mgw10006", "CIDCONCE01", TipoDato.TdCadena)
    '                Existe = LlenaDatosFox(txtidConceptoExt.Text, "mgw10006", "CCODIGOC01", TipoDato.TdCadena, , True)
    '                If Existe = KEY_RINCORRECTO Then
    '                    MsgBox("!! El Concepto : " & txtidConceptoExt.Text & " no existe en el Adminpaq", vbInformation, "Aviso" & Me.Text)
    '                    Cancelar()
    '                    Exit Sub
    '                End If
    '            End If
    '            btnAgregarExt.Focus()

    '            Salida = False
    '        End If
    '    End If
    'End Sub
    'Private Sub txtidConceptoExt_Leave(sender As Object, e As EventArgs)
    '    'If Salida Or Me.ActiveControl.Name = txtNombreGrupo.Name Then Exit Sub
    '    If Salida Then Exit Sub
    '    If txtidConceptoExt.Text.Length = 0 Then
    '        txtidConceptoExt.Focus()
    '        txtidConceptoExt.SelectAll()
    '    Else
    '        If txtidConceptoExt.Enabled Then
    '            Salida = True
    '            'AQUI VA EL CODIGO
    '            '1.- Checar si Existe en el Grid, si ya es parte de la relacion
    '            Existe = ChecaConceptoGrid(txtidConceptoExt.Text, True)
    '            If Existe = KEY_RINCORRECTO Then
    '                '2.- Checar en la Base de AdminPaq si existe el cliente
    '                'ExisteCliADMPAQ = LlenaDatosFox(txtIdCliente.Text, "mgw10002", "CCODIGOC01", TipoDato.TdCadena)
    '                Existe = LlenaDatosFox(txtidConceptoExt.Text, "mgw10006", "CCODIGOC01", TipoDato.TdCadena, , True)
    '                If Existe = KEY_RINCORRECTO Then
    '                    MsgBox("!! El Concepto : " & txtidConceptoExt.Text & " no existe en el Adminpaq", vbInformation, "Aviso" & Me.Text)
    '                    Cancelar()
    '                    Exit Sub
    '                End If
    '            End If
    '            btnAgregarExt.Focus()
    '            Salida = False
    '        End If
    '    End If
    'End Sub
    'Private Sub btnAgregarExt_Click(sender As Object, e As EventArgs)
    '    If txtidConceptoExt.Text <> "" Then
    '        '0.- VErificar si existe el cliente en el GRid
    '        Existe = ChecaConceptoGrid(txtidConceptoExt.Text, True)
    '        If Existe = KEY_RINCORRECTO Then
    '            'AGREGARLO AL GRID
    '            'VERIFICAR SI TIENE LISTA DE PRECIOS E INDICAR SI LO VAS A SOBRESCRIBIR - OJOOOOO
    '            'Existe = LlenaDatos(txtIdCliente.Text, "LISTAPRECIOS", "CCODIGOC01_CLI", TipoDato.TdCadena, Inicio.CONSTR, txtCveGrupo.Text)
    '            If InsertaConcepto(True) Then
    '                LimpiaCamposConcepto(True)
    '            Else
    '                Exit Sub
    '            End If
    '        End If
    '    End If
    'End Sub
    'Private Sub btnEliminarExt_Click(sender As Object, e As EventArgs)
    '    If txtidConcepto.Text <> "" Then
    '        '0.- VErificar si existe el cliente en el GRid
    '        Existe = ChecaConceptoGrid(txtidConceptoExt.Text, True)
    '        If Existe = KEY_RINCORRECTO Then
    '            'MsgBox("!! El Concepto : " & txtidConcepto.Text & " no se encuentra en la Relacion Grupo: " & txtCveGrupo.Text & " Cliente:" & txtIdCliente.Text, vbInformation, "Aviso" & Me.Text)
    '            'LimpiaCamposCliente()

    '        Else
    '            'se borra

    '            If BorraConcepto(vIndiceConcepto, True) Then
    '                LimpiaCamposConcepto(True)
    '            Else
    '                LimpiaCamposConcepto(True)
    '                Exit Sub
    '            End If

    '        End If

    '    End If
    'End Sub

    Private Sub CreaTablaEmpresas()


        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.String")
        myDataColumn.ColumnName = "EMPRESA"
        myDataColumn.ReadOnly = False
        myDataColumn.Unique = False
        ' Add the Column to the DataColumnCollection.
        MyTablaEmpresas.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.String")
        myDataColumn.ColumnName = "RUTA"
        myDataColumn.ReadOnly = False
        myDataColumn.Unique = False
        ' Add the Column to the DataColumnCollection.
        MyTablaEmpresas.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Int64")
        myDataColumn.ColumnName = "IDEMPRESA"
        myDataColumn.ReadOnly = False
        myDataColumn.Unique = False
        ' Add the Column to the DataColumnCollection.
        MyTablaEmpresas.Columns.Add(myDataColumn)

    End Sub

    'Private Sub llenaEmpresas()
    '    Dim ObjReg As New ToolRegistro
    '    Dim lError As Integer = 0
    '    Dim lidEmpresa As Integer = 0
    '    Dim lRutaEmpresa As String = ""
    '    Dim lNombreEmpresa As String = ""
    '    Try
    '        CreaTablaEmpresas()
    '        '----------------- ADMINPAQ ----------------- 
    '        lRutaEmpresa = Space(61)
    '        lNombreEmpresa = Space(61)



    '        'directoriobase()

    '        lError = fSetNombrePAQ("AdminPAQ")
    '        If lError <> 0 Then
    '            MensajeError(lError)
    '            End
    '        End If

    '        Parametro_DIRBASE = ObjReg.LeeLlaveRegistro("Computaci�n en Acci�n, SA CV", "AdminPAQ", "DIRECTORIOBASE", KeyPathCA)
    '        Directory.SetCurrentDirectory(Parametro_DIRBASE)
    '        'Resultado = ObjReg.LeeLlaveRegistro(Me.ProductName, "ServerSQL", "Nombre del servidor")
    '        ' Inicializar el SDK
    '        lError = fInicializaSDK()
    '        If lError <> 0 Then
    '            MensajeError(lError)
    '            End
    '        End If

    '        '            lLicencia = 0
    '        '           lError = fInicializaLicenseInfo(lLicencia) ' 0=AdminPAQ, 1=FACTURA ELECTR�NICA
    '        '          If lError <> 0 Then
    '        'MensajeError(lError)
    '        'End
    '        'End If


    '        cbEmpresa.Items.Clear()
    '        'cbRuta.Items.Clear()

    '        ' Ir a la primera empresa
    '        lError = fPosPrimerEmpresa(lidEmpresa, lNombreEmpresa, lRutaEmpresa)
    '        If lError <> 0 Then
    '            MensajeError(lError)
    '            End
    '        End If

    '        While lError = 0
    '            'cbRuta.Items.Add(lRutaEmpresa)
    '            'cbEmpresa.Items.Add(lNombreEmpresa)
    '            myDataRow = MyTablaEmpresas.NewRow()
    '            myDataRow("EMPRESA") = lNombreEmpresa
    '            myDataRow("RUTA") = lRutaEmpresa
    '            myDataRow("IDEMPRESA") = lidEmpresa
    '            '
    '            MyTablaEmpresas.Rows.Add(myDataRow)
    '            lRutaEmpresa = Space(61)
    '            lNombreEmpresa = Space(61)
    '            lError = fPosSiguienteEmpresa(lidEmpresa, lNombreEmpresa, lRutaEmpresa)
    '        End While


    '        '----------------- ADMINPAQ ----------------- 
    '        If Not MyTablaEmpresas Is Nothing Then
    '            If MyTablaEmpresas.DefaultView.Count > 0 Then
    '                cbEmpresa.DataSource = MyTablaEmpresas
    '                cbEmpresa.ValueMember = "RUTA"
    '                cbEmpresa.DisplayMember = "EMPRESA"
    '            End If
    '        End If
    '        cbEmpresa.SelectedIndex = 0
    '        'cbRuta.SelectedIndex = 0
    '    Catch ex As Exception
    '        MsgBox("Ocurrio un error!!!" & vbCrLf & "Error: " & ex.Message, MsgBoxStyle.Information, "LlenaEmpresas")
    '    End Try
    'End Sub

    Private Sub BuscaEmpresaAdminPAQ(ByVal NombreEmpresa As String)
        If Not MyTablaEmpresas Is Nothing Then
            If MyTablaEmpresas.DefaultView.Count > 0 Then
                For i = 0 To MyTablaEmpresas.DefaultView.Count - 1
                    If NombreEmpresa = Trim(MyTablaEmpresas.DefaultView.Item(i).Item("EMPRESA").ToString()) Then
                        vIdEmpresa = MyTablaEmpresas.DefaultView.Item(i).Item("IDEMPRESA")
                        Exit For
                    End If
                Next
            End If
        End If
    End Sub

    Private Sub TabPage1_Click(sender As Object, e As EventArgs) Handles TabPage1.Click

    End Sub

    Private Sub btnFormatoImp_Click(sender As Object, e As EventArgs) Handles btnFormatoImp.Click
        With OpenFileDialog1
            .Title = "Seleccione el Formato para Facturar"
            .FileName = "FormatoImp"
            '.Filter = "Archivos de Imagen (*.jpg,*.bmp,*.jpeg,*.png,*.ico,*.gif,*.tif)|*.jpg;*.bmp;*.jpeg;*.png;*.ico;*.gif;*.tif|Todos los archivos|*.*"
            If .ShowDialog() = Windows.Forms.DialogResult.OK Then
                'ImagenFoto = New System.Drawing.Bitmap(.FileName)
                txtFormatoFac.Text = .FileName
            End If
        End With
    End Sub

    Private Sub txtFormatoFac_EnabledChanged(sender As Object, e As EventArgs) Handles txtFormatoFac.EnabledChanged
        btnFormatoImp.Enabled = txtFormatoFac.Enabled
    End Sub

    Private Sub txtFormatoFac_TextChanged(sender As Object, e As EventArgs) Handles txtFormatoFac.TextChanged

    End Sub
    Private Sub Label26_Click(sender As Object, e As EventArgs) Handles Label26.Click

    End Sub

    Private Sub txtBodegaSalDefault_TextChanged(sender As Object, e As EventArgs) Handles txtBodegaSalDefault.TextChanged

    End Sub
End Class
