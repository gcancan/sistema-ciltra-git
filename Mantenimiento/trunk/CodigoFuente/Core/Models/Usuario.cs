﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class Usuario : Persona
    {
        public string sUserId { get; set; }
        public string sPassword { get; set; }
        public bool bActivo { get; set; }
        public string sCveGrupo { get; set; }
        public string sNomUser { get; set; }
        public bool ? EsGrupo { get; set; }
        public bool ? AutorizaCompra { get; set; }
        public bool ? AsignaActividad { get; set; }
        public bool  esTaller { get; set; }
        public bool  capturaRecepcion { get; set; }
        public Personal personal { get; set; }
    }
}
