﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public enum TipoDocumento
    {
        OREDEN_SERVICIO_RECEPCION = 0,
        ASIGNACION_DE_TRABAJO = 1
    }
}
