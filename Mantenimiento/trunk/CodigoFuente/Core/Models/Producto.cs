﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class Producto
    {
        public int idProducto { get; set; }
        public int idActividad { get; set; }
        public string clave { get; set; }
        public string nombre { get; set; }
        public decimal cantidad { get; set; }
        public decimal existencia { get; set; }
        public decimal costo { get; set; }
        public decimal cantidadOrdenAlmacen
        {
            get
            {
                if (existencia < cantidad)
                {
                    return existencia;
                }
                else
                {
                    return cantidad;
                }
            }
        }

        public decimal cantidadOrdenCompra
        {
            get
            {
                if (existencia < cantidad)
                {
                    return (decimal)(cantidad - existencia);
                }
                else
                {
                    return 0.00m;
                }
            }
        }
    }
}
