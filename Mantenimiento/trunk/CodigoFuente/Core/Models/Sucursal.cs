﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class Sucursal
    {
        public int id_sucursal { get; set; }
        public string nombreSucursal { get; set; }
        public int idEmpresa { get; set; }
    }
}
