﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class Personal : Persona
    {
        public int idPersonal { get; set; }
        public Empresa empresa { get; set; }
        public int idEmpresa
        {
            get
            {
                if (empresa.idEmpresa !=0)
                {
                    return empresa.idEmpresa;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string nombreCompleto { get; set; }
        public string estatus { get; set; }
    }
}
