﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class Diagnostico
    {


        public int idOrdenActividad { get; set; }
        public int idOrdenTrabajo { get; set; }

        public Recepcion recepcion { get; set; }
        public int idOrdenServ
        {
            get
            {
                if (recepcion.idOrdenSer == 0)
                {
                    return 0;
                }
                return recepcion.idOrdenSer;
            }
        }
        public int ? idServicio { get; set; }
        public Actividad actividad { get; set; }
        public int idActividad
        {
            get
            {
                if (actividad.idActividad == 0)
                {
                    return 0;
                }
                return actividad.idActividad;
            }
        }

        public string DuracionActHr
        {
            get
            {
                if (actividad.duracionHoras == "")
                {
                    return "";
                }
                return actividad.duracionHoras;
            }
        }
        public int NoPersonal { get; set; }
        public int id_proveedor { get; set; }
        public string Estatus { get; set; }
        public decimal CostoManoObra
        {
            get
            {
                if (actividad.costoManoObra == 0)
                {
                    return 0;
                }
                return actividad.costoManoObra;
            }
        }
        public decimal CostoProductos { get; set; }
        public DateTime FechaDiag { get; set; }
        public string UsuarioDiag { get; set; }
        public DateTime FechaAsig { get; set; }
        public string UsuarioAsig { get; set; }
        public DateTime FechaPausado { get; set; }
        public string NotaPausado { get; set; }
        public DateTime FechaTerminado { get; set; }
        public string UsuarioTerminado { get; set; }
    }
}
