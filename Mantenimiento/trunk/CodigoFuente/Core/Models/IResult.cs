﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    interface IResult
    {
        //public Result(IResult result);
        //public Result(ResultTypes resultType, string descripcion);
        //public Result(ResultTypes resultType, string descripcion, string detalle);

        string descripcion { get; set; }
        string detalle { get; set; }
        bool isError { get; }
        bool isRecordNotFound { get; }
        bool isSuccess { get; }
        ResultTypes resultType { get; set; }
    }
}
