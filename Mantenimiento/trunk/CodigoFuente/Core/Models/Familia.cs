﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class Familia
    {
        public int idFamilia { get; set; }
        public string nomFamilia { get; set; }
        public Divicion divicion { get; set; }

        public string claDes
        {
            get
            {
                return idFamilia.ToString() + " - " + nomFamilia;
            }
        }
    }
}
