﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class Actividad
    {
        public int idActividad { get; set; }
        public string nombreAct { get; set; }
        public decimal costoManoObra { get; set; }
        public Familia familia { get; set; }
        public int idFamilia
        {
            get
            {
                if (familia.idFamilia == null)
                {
                    return 0;
                }
                return familia.idFamilia;
            }
        }
        public string duracionHoras { get; set; }
        public List<Producto> listProductos { get; set; }

        public Personal personalAsignado { get; set; }

        //public List<Personal> listPersonal { get; set; }

        public int idServicio { get; set; }
        public string nombreServicio { get; set; }
    }
}
