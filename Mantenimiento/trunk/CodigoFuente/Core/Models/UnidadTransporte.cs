﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class UnidadTransporte
    {
        public string id_UnidadTrans { get; set; }

        public string  descripcionUni { get; set; }

        public TipoUnidad tipoUnidad { get; set; }

        public int idTipoUnidad
        {
            get
            {
                if (tipoUnidad.idTipoUniTrans != 0)
                {
                    return tipoUnidad.idTipoUniTrans;
                }
                else
                {
                    return 0;
                }
            }
        }
        
        public Empresa empresa { get; set; }

        public int idEmpresa
        {
            get
            {
                if (empresa.idEmpresa != 0)
                {
                    return empresa.idEmpresa;
                }
                else
                {
                    return 0;
                }
            }
        }

        public Marca marca { get; set; }

        public int idMarca
        {
            get
            {
                if (marca.idMarca != 0)
                {
                    return marca.idMarca;
                }
                else
                {
                    return 0;
                }
            }
        }

        public Sucursal sucursal { get; set; }

        public int idSucursal
        {
            get
            {
                if (sucursal.id_sucursal != 0)
                {
                    return sucursal.id_sucursal;
                }
                else
                {
                    return 0;
                }
            }
        }

        public string estatus { get; set; }
    }
}
