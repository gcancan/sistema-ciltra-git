﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class MenusAplicacion
    {
        public int idMenuAplicacion { get; set; }
       
        public string clave { get; set; }
        public string nombre { get; set; }
       
        public MenusAplicacion padre { get; set; }

        public int? idPadre
        {
            get
            {
                return padre == null ? new int?() : padre.idMenuAplicacion;
            }
            set
            {
                if (value.HasValue)
                {
                    padre = new MenusAplicacion();
                    padre.idMenuAplicacion = value.Value;
                }
                else
                    padre = null;
            }
        }
    }
}

