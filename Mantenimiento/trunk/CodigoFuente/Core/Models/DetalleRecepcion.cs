﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Interfaces;
namespace Core.Models
{
    public class DetalleRecepcion
    {
        public int idDetalleRecepcion { get; set; }
        public int idRecepcion { get; set; }
        public string descripcion { get; set; }
        public Familia familia { get; set; }
        public int idFamilia
        {
            get
            {
                if (familia.idFamilia != 0)
                {
                    return familia.idFamilia;
                }
                return 0;
            }
        }
        public Familia _familia { get; set; }

        public Divicion divicion { get; set; }
        public int ? idDivicion
        {
            get
            {
                if (divicion.nomDivicion != "")
                {
                    return divicion.idDivicion;
                }
                return null;
            }
        }
        public Divicion _divicion { get; set; }


        public Personal _personal { get; set; }

        public bool isCancelado { get; set; }
        public string motivoCancelacion { get; set; }
    }
}
