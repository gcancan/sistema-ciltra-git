﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class TipoOrden
    {
        public int idTipoOrden { get; set; }
        public string nomTipoOrden { get; set; }
    }
}
