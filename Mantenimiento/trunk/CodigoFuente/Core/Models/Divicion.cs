﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Interfaces;
namespace Core.Models
{
    public class Divicion
    {
        public int ? idDivicion { get; set; }
        public string nomDivicion { get; set; }

        public List<Familia> listFamilia
        {
            get
            {
                if (idDivicion != null)
                {
                    OperationResult resp = new FamiliaSvc().selectAllFamiliasByIdDivicion((int)idDivicion);
                    return (List<Familia>)resp.result;
                }
                return null;
            }
        }

        public List<Personal> listPersonal
        {
            get
            {
                if (idDivicion != null)
                {
                    OperationResult resp = new PersonalSvc().getAllPersonalByIdDivicion((int)idDivicion);
                    return (List<Personal>)resp.result;
                }
                return null;
            }
        }

    }
}
