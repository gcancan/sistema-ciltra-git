﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class Recepcion
    {
        public int idOrdenSer { get; set; }

        public Empresa empresa { get; set; }
        public int IdEmpresa
        {
            get
            {
                if (empresa.idEmpresa != 0)
                {
                    return empresa.idEmpresa;
                }
                else
                {
                    return 0;
                }
            }
        }

        public UnidadTransporte unidadTransporte { get; set; }
        public string id_UnidadTrans
        {
            get
            {
                if (!string.IsNullOrEmpty(unidadTransporte.id_UnidadTrans))
                {
                    return unidadTransporte.id_UnidadTrans;
                }
                else
                {
                    return "";
                }
            }
        }

        public TipoOrden tipoOrden { get; set; }
        public int idTipoOrden
        {
            get
            {
                if (tipoOrden.idTipoOrden != 0)
                {
                    return tipoOrden.idTipoOrden;
                }
                else
                {
                    return 0;
                }
            }
        }

        public DateTime ? FechaRecepcion  { get; set; }
        public DateTime fechaCaptura { get; set; }

        public Personal personal { get; set; }
        public int idEmpleadoEntrega
        {
            get
            {
                if (personal.idPersonal != 0)
                {
                    return personal.idPersonal;
                }
                else
                {
                    return 0;
                }
            }
        }

        public Personal personalAutoriza { get; set; }
        public int idEmpleadoEntregaAutoriza
        {
            get
            {
                if (personalAutoriza.idPersonal != 0)
                {
                    return personalAutoriza.idPersonal;
                }
                else
                {
                    return 0;
                }
            }
        }

        public Usuario usuario { get; set; }
        public string UsuarioRecepcion
        {
            get
            {
                if (usuario.sUserId != null)
                {
                    return usuario.sUserId;
                }
                else
                {
                    return "";
                }
            }
        }

        public decimal Kilometraje { get; set; }
        public string Estatus { get; set; }

        public List<DetalleRecepcion> detalles { get; set; }

        public string motivoBaja { get; set; }

       
    }
}
