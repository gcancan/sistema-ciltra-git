﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class Servicio
    {
        public int? idServicio { get; set; }
        public TipoServicio tipoServicio { get; set; }
        public int idTipoServicio
        {
            get
            {
                if (tipoServicio.idTipoServicio == null)
                {
                    return 0;
                }
                return tipoServicio.idTipoServicio;
            }
        }
        public string NomServicio { get; set; }
        public string descripcion { get; set; }
        public decimal CadaKms { get; set; }
        public decimal CadaTiempoDias { get; set; }
        public string Estatus { get; set; }
        public decimal costo { get; set; }
        public List<Actividad> listActividades { get; set; }
    }
}
