﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class Empresa
    {
        public int idEmpresa { get; set; }
        public string razonSocial { get; set; }
        public string representante { get; set; }
        public int CIDEMPRESA_COM { get; set; }
        public string direccion { get; set; }
        public int idColonia { get; set; }
        public string telefono { get; set; }
        public string fax { get; set; }
        public string movilRepresentante { get; set; }
    }
}
