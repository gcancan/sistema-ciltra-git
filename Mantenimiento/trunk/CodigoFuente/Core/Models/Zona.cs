﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class Zona
    {
        public int clave_empresa { get; set; }
        public int clave { get; set; }       
        public string descripcion { get; set; }       
        public bool activo { get; set; } 
        public decimal costoSencillo { get; set; }
        public decimal costoFull { get; set; }
        public decimal km { get; set; }

    }
}
