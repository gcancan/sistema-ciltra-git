﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class HeaderDiagnostico
    {
        public Recepcion recepcion { get; set; }
        public int idOrdenServ { get; set; }
        public List<Diagnostico> listDiagnostico { get; set; }
        public List<DetalleRecepcion> listDetallesRecepcion { get; set; }
        public DateTime fecha { get; set; }
        public string persona { get; set; }
    }
}
