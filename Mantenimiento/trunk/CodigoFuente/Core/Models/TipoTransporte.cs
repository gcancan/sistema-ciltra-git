﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class TipoTransporte
    {
        public int ? idTipoTransporte { get; set; }
        public string nombre { get; set; }
    }
}
