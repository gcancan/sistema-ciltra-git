﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class TipoUnidad
    {
        public int idTipoUniTrans { get; set; }
        public string nomTipoUniTrans { get; set; }
        public string noLlantas { get; set; }
        public string noEjes { get; set; }
        public string tonelajeMax { get; set; }
    }
}
