﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class OperationResult
    {
        public int ? valor { get; set; }
        public string mensaje { get; set; }
        public ResultTypes typeResult { get {
                if (valor == null)
                {
                    return ResultTypes.warning;
                }
                return (ResultTypes)valor;
            } }
        public int ? identity { get; set; }

        public object result { get; set; }

        public static explicit operator OperationResult(Task<OperationResult> v)
        {
            throw new NotImplementedException();
        }
    }
}
