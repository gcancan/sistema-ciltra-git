﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using Core.BusinessLogic;
namespace Core.Interfaces
{
    public class DiagnosticoSvc : IDiagnostico
    {
        public OperationResult deleteAsignacion(HeaderDiagnostico header)
        {
            return new DiagnosticoServices().deleteAsignacion(header);
        }

        public OperationResult getAllAsignaciones()
        {
            return new DiagnosticoServices().getAllAsignaciones();
        }

        public OperationResult saveAsignacion(HeaderDiagnostico header)
        {
            return new DiagnosticoServices().saveAsignacion(header);
        }

        public OperationResult saveRecepcion(List<Diagnostico> listDiagnostico)
        {
            return new DiagnosticoServices().saveRecepcion(listDiagnostico);
        }
    }
}
