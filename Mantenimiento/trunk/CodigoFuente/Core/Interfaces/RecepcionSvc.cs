﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using Core.BusinessLogic;
namespace Core.Interfaces
{
    public class RecepcionSvc : IRecepcion
    {
        public OperationResult deleteRecepcion(Recepcion recepcion)
        {
            return new RecepcionService().deleteRecepcion(recepcion);
        }

        public OperationResult getAllrecepciones(int parametro)
        {
            return new RecepcionService().getAllrecepciones(parametro);
        }

        public OperationResult getrecepcionComplete(int idOrdenServ)
        {
            return new RecepcionService().getrecepcionComplete(idOrdenServ);
        }

        public OperationResult getRecepcionesById(int parametro, int id)
        {
            return new RecepcionService().getRecepcionesById(parametro, id);
        }

        public OperationResult insertDetallesRecepcion(List<DetalleRecepcion> detalle)
        {
            return new RecepcionService().insertDetallesRecepcion(detalle);
        }

        public OperationResult saveRecepcion(Recepcion recepcion)
        {
            return new RecepcionService().saveRecepcion(recepcion);
        }
    }
}
