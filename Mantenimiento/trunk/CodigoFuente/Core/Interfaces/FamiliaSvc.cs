﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using Core.BusinessLogic;

namespace Core.Interfaces
{
    public class FamiliaSvc : IFamilia
    {
        public OperationResult selectAllFamilias()
        {
            return new FamiliaService().selectAllFamilias();
        }

        public OperationResult selectAllFamiliasByIdDivicion(int idDivicion)
        {
            return new FamiliaService().selectAllFamiliasByIdDivicion(idDivicion);
        }
    }
}
