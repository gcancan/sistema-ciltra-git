﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using Core.BusinessLogic;

namespace Core.Interfaces
{
    public class TiposTransporteSvc : ITiposTransporte
    {
        public OperationResult deleteTipoTransporte(TipoTransporte tipoTransporte)
        {
            return new TiposTransporteServices().deleteTipoTransporte(tipoTransporte);
        }

        public OperationResult findTiposTransportes(string parametro)
        {
            return new TiposTransporteServices().findTiposTransportes(parametro);
        }

        public OperationResult saveTipoTransporte(TipoTransporte tipoTransporte)
        {
            return new TiposTransporteServices().saveTipoTransporte(tipoTransporte);
        }
    }
}
