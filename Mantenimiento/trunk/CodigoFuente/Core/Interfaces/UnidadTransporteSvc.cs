﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using Core.BusinessLogic;
namespace Core.Interfaces
{
    public class UnidadTransporteSvc : IUnidadeTransporte
    {
        public OperationResult findAllUnidadesTransporte(string idEmpresa)
        {
            return new UnidadTransporteService().findAllUnidadesTransporte(idEmpresa);
        }

        public OperationResult findAllUnidadesTransporteByIdAndIdEmpresa(string idUnidad, string idEmpresa)
        {
            return new UnidadTransporteService().findAllUnidadesTransporteByIdAndIdEmpresa(idUnidad, idEmpresa);
        }

        public OperationResult findAUnidadesTransporteById(string idParam)
        {
            return new UnidadTransporteSvc().findAUnidadesTransporteById(idParam);
        }
    }
}
