﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using Core.BusinessLogic;
namespace Core.Interfaces
{
    public class ServicioSvc : IServisio
    {
        public OperationResult deleteServicio(Servicio servicio)
        {
            //ServicioSvc serv = new ServicioSvc();
            //OperationResult res = new OperationResult();
            //res = serv.deleteServicio(servicio);
            //return res;

            return new ServicioService().deleteServicio(servicio);
        }

        public OperationResult findDetallesServicio(int parametro)
        {
            return new ServicioService().findDetallesServicio(parametro);
        }

        public OperationResult findServicio()
        {
            return new ServicioService().findServicio();
        }

       

        //public OperationResult saveServicio(Servicio servicio)
        //{
        //    return new ServicioService().saveServicio(servicio);
        //}
    }
}
