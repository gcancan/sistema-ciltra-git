﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
namespace Core.Interfaces
{
    public interface IUnidadTransporte
    {
        OperationResult saveUnidadTransporte(UnidadTransporte unidadTransporte);

        //UnidadTransporte getUnidadTransporteByRFID(UnidadTransporte unidadTransporte);

        OperationResult deleteUnidadTransporte(UnidadTransporte unidadTransporte);

        OperationResult findUnidadTransporteByStatusID(string parametro, string status);
    }
}
