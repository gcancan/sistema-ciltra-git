﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using Core.BusinessLogic;
namespace Core.Interfaces
{
    public class PersonalSvc : IPersonal
    {
        public OperationResult findAllPersonal()
        {
            return new PersonalService().findAllPersonal();
        }

        public OperationResult getAllPersonalByIdDivicion(int idDivicion)
        {
            return new PersonalService().getAllPersonalByIdDivicion(idDivicion);
        }

        public OperationResult getPersonalById(int parametro)
        {
            return new PersonalService().getPersonalById(parametro);
        }

        public OperationResult getPersonalByUsuario(string sUserId)
        {
            return new PersonalService().getPersonalByUsuario(sUserId);
        }
    }
}
