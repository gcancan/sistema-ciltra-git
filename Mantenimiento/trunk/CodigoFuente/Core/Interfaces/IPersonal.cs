﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Interfaces
{
    public interface IPersonal
    {
        OperationResult findAllPersonal();
        OperationResult getPersonalById(int parametro);
        OperationResult getPersonalByUsuario(string sUserId);
        OperationResult getAllPersonalByIdDivicion(int idDivicion);
    }
}
