﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using Core.BusinessLogic;
namespace Core.Interfaces
{
    public class MenusAplicacionSvc : IMenusAplicacion
    {
        public OperationResult getAllMenus()
        {
            return new MenusAplicacionService().getAllMenus();
        }
    }
}
