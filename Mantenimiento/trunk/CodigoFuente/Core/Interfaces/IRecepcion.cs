﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Interfaces
{
    public interface IRecepcion
    {
        OperationResult saveRecepcion(Recepcion recepcion);
        OperationResult deleteRecepcion(Recepcion recepcion);
        OperationResult getAllrecepciones(int parametro);
        OperationResult insertDetallesRecepcion(List<DetalleRecepcion> detalle);
        OperationResult getrecepcionComplete(int idOrdenServ);
        OperationResult getRecepcionesById(int parametro, int id);
    }
}
