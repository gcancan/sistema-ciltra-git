﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using Core.BusinessLogic;
namespace Core.Interfaces
{
    public class TipoOrdenSvc : ITipoOrden
    {
        public OperationResult getAllTiposOrden()
        {
            return new TipoOrdenServices().getAllTiposOrden();
        }

        public OperationResult getTiposOrdenByID(int parametro)
        {
            return new TipoOrdenServices().getTiposOrdenByID(parametro);
        }
    }
}
