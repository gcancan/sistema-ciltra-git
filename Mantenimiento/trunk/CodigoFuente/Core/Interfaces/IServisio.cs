﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
namespace Core.Interfaces
{
    public interface IServisio
    {
        //OperationResult saveServicio(Servicio servicio);
        OperationResult deleteServicio(Servicio servicio);
        OperationResult findServicio();
        OperationResult findDetallesServicio(int parametro);
    }
}
