﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Interfaces
{
    public interface IUnidadeTransporte
    {
        OperationResult findAllUnidadesTransporte(string idEmpresa);
        OperationResult findAUnidadesTransporteById(string idParam);
        OperationResult findAllUnidadesTransporteByIdAndIdEmpresa(string idUnidad, string idEmpresa);
    }
}
