﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using Core.Utils;
using System.Data;
using System.Data.SqlClient;
using Core.Interfaces;
namespace Core.BusinessLogic
{
    public class DiagnosticoServices
    {
        public OperationResult saveRecepcion(List<Diagnostico> listDiagnostico)
        {
            try
            {
                var xmlResult = GenerateXML.generatelistDiagnostico(listDiagnostico);
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_saveDiagnostico";


                        command.Parameters.Add(new SqlParameter("@XML", xmlResult));

                        //recepcion.personal.nombreCompleto
                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        //var id = command.Parameters.Add(new SqlParameter("@identificador", SqlDbType.Int) { Direction = ParameterDirection.Output });

                        connection.Open();
                        var i = command.ExecuteReader();


                        return new OperationResult { mensaje = (string)codigo.Value, valor = (int)spValor.Value };

                    }
                }
            }
            catch (Exception e)
            {
                return new OperationResult { mensaje = e.Message, valor = 1, result = null };
            }
        }

        public OperationResult saveAsignacion(HeaderDiagnostico header)
        {
            try
            {
                var xmlResult = GenerateXML.generatelistDiagnostico(header.listDiagnostico);
                var xmlResulDet = GenerateXML.generateListDetallesRecepcion(header.listDetallesRecepcion);
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_insertDiagnosticos";


                        command.Parameters.Add(new SqlParameter("@idOrdenServ", header.idOrdenServ));
                        command.Parameters.Add(new SqlParameter("@XMLdiagnostico", xmlResult));
                        command.Parameters.Add(new SqlParameter("@XMLdetalles", xmlResulDet));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });


                        connection.Open();
                        var i = command.ExecuteReader();

                        List<DetalleRecepcion> listDetalle = new List<DetalleRecepcion>();
                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                listDetalle.Add(new DetalleRecepcion
                                {
                                    idDetalleRecepcion = (int)sqlReader["idOrdenTrabajo"],
                                    idRecepcion = (int)sqlReader["idOrdenSer"],
                                    descripcion = (string)sqlReader["NotaRecepcion"],
                                    familia = new Familia
                                    {
                                        idFamilia = (int)sqlReader["idFamilia"],
                                        nomFamilia = (string)sqlReader["NomFamilia"]
                                    },
                                    divicion = new Divicion
                                    {
                                        idDivicion = (int)sqlReader["idDivision"],
                                        nomDivicion = (string)sqlReader["NomDivision"]
                                    },
                                    isCancelado = (bool)sqlReader["isCancelado"],
                                    motivoCancelacion = (string)sqlReader["motivoCancelacion"],
                                    _personal = mapPersonal(((int)sqlReader["idPersonal"]))
                                });

                                header.fecha = (DateTime)sqlReader["fechaAsig"];
                                header.persona = (string)sqlReader["persona"];
                            }
                        }
                        header.listDetallesRecepcion = listDetalle;
                        return new OperationResult { mensaje = (string)codigo.Value, valor = (int)spValor.Value, result = header };
                    }
                }
            }
            catch (Exception e)
            {
                return new OperationResult { mensaje = e.Message, valor = 1, result = null };
            }
        }

        private Personal mapPersonal(int v)
        {
            var resp = new PersonalService().getPersonalById(v);
            if (resp.typeResult == ResultTypes.success)
            {
                return (Personal)resp.result;
            }
            return null;
        }

        public OperationResult deleteAsignacion(HeaderDiagnostico header)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_deleteAsignaciones";

                        command.Parameters.Add(new SqlParameter("@idOrdenServ", header.idOrdenServ));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                        connection.Open();
                        var i = command.ExecuteReader();

                        return new OperationResult { mensaje = (string)codigo.Value, valor = (int)spValor.Value};
                    }
                }
            }
            catch (Exception e)
            {
                return new OperationResult { mensaje = e.Message, valor = 1, result = null };
            }
        }

        public OperationResult getAllAsignaciones()
        {
            try
            {
               
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getAllAsignaciones";                        

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                        connection.Open();
                        var i = command.ExecuteReader();

                        List<HeaderDiagnostico> listAsignacion = new List<HeaderDiagnostico>();
                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                HeaderDiagnostico header = new HeaderDiagnostico
                                {
                                    idOrdenServ = (int)sqlReader["idOrdenSer"],
                                    fecha = (DateTime)sqlReader["FechaDiag"],
                                    persona = (string)sqlReader["persona"]
                                };
                                OperationResult resp = new RecepcionSvc().getrecepcionComplete((int)sqlReader["idOrdenSer"]);
                                if (resp.typeResult == ResultTypes.success)
                                {
                                    header.recepcion = (Recepcion)resp.result;
                                }
                                else
                                {
                                    return new OperationResult { mensaje = resp.mensaje, valor = resp.valor, result = null };
                                }
                                listAsignacion.Add(header);
                            }
                        }
                       
                        return new OperationResult { mensaje = (string)codigo.Value, valor = (int)spValor.Value, result = listAsignacion };
                    }
                }
            }
            catch (Exception e)
            {
                return new OperationResult { mensaje = e.Message, valor = 1, result = null };
            }
        }
    }

}
