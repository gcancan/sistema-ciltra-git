﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using Core.Utils;
using System.Data;
using System.Data.SqlClient;

namespace Core.BusinessLogic
{
    public class TipoOrdenServices
    {
        public OperationResult getAllTiposOrden()
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        int parametro = 0;
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getAllTiposOrden";
                        command.Parameters.Add(new SqlParameter("@parametro", parametro));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        connection.Open();
                        var i = command.ExecuteReader();
                        List<TipoOrden> listTipoOrden = new List<TipoOrden>();

                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                listTipoOrden.Add(new TipoOrden
                                {
                                    idTipoOrden = (int)sqlReader["idTipoOrden"],
                                    nomTipoOrden = (string)sqlReader["NomTipoOrden"],

                                });
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)codigo.Value, result = listTipoOrden };
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }

        public OperationResult getTiposOrdenByID(int parametro)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {

                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getAllTiposOrden";
                        command.Parameters.Add(new SqlParameter("@parametro", parametro));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        connection.Open();
                        var i = command.ExecuteReader();
                        TipoOrden tipoOrden = new TipoOrden();

                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                tipoOrden = (new TipoOrden
                                {
                                    idTipoOrden = (int)sqlReader["idTipoOrden"],
                                    nomTipoOrden = (string)sqlReader["NomTipoOrden"],

                                });
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)codigo.Value, result = tipoOrden };
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }
    }
}
