﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using Core.Utils;
using System.Data;
using System.Data.SqlClient;

namespace Core.BusinessLogic
{
    public class RecepcionService
    {
        public OperationResult saveRecepcion(Recepcion recepcion)
        {
            try
            {
                var xmlResult = GenerateXML.generateListDetallesRecepcion(recepcion.detalles);
                //string mensaje = deta(xmlResult);
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_saveRecepcion";

                        command.Parameters.Add(new SqlParameter("@idOrdenSer", recepcion.idOrdenSer));
                        command.Parameters.Add(new SqlParameter("@IdEmpresa", recepcion.IdEmpresa));
                        command.Parameters.Add(new SqlParameter("@id_UnidadTrans", recepcion.id_UnidadTrans));
                        command.Parameters.Add(new SqlParameter("@idTipoOrden", recepcion.idTipoOrden));
                        command.Parameters.Add(new SqlParameter("@idEmpleadoEntrega", recepcion.idEmpleadoEntrega));
                        command.Parameters.Add(new SqlParameter("@UsuarioAutoriza", recepcion.idEmpleadoEntregaAutoriza));
                        command.Parameters.Add(new SqlParameter("@FechaRecepcion", recepcion.FechaRecepcion));
                        command.Parameters.Add(new SqlParameter("@UsuarioRecepcion", recepcion.UsuarioRecepcion));
                        command.Parameters.Add(new SqlParameter("@Kilometraje", recepcion.Kilometraje));
                        command.Parameters.Add(new SqlParameter("@Estatus", recepcion.Estatus));
                        command.Parameters.Add(new SqlParameter("@XML", xmlResult));

                        //recepcion.personal.nombreCompleto
                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        var id = command.Parameters.Add(new SqlParameter("@identificador", SqlDbType.Int) { Direction = ParameterDirection.Output });

                        connection.Open();
                        var i = command.ExecuteReader();
                        recepcion.detalles = new List<DetalleRecepcion>();
                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                recepcion.detalles.Add(new DetalleRecepcion
                                {
                                    idDetalleRecepcion = (int)sqlReader["idOrdenTrabajo"],
                                    idRecepcion = (int)sqlReader["idOrdenSer"],
                                    descripcion = (string)sqlReader["NotaRecepcion"],
                                    //familia = new Familia
                                    //{
                                    //    idFamilia = (int)sqlReader["idFamilia"],
                                    //    nomFamilia = (string)sqlReader["NomFamilia"]
                                    //},
                                    divicion = new Divicion
                                    {
                                        idDivicion = (int)sqlReader["idDivision"],
                                        nomDivicion = (string)sqlReader["NomDivision"]
                                    },
                                    isCancelado = (bool)sqlReader["isCancelado"],
                                    motivoCancelacion = (string)sqlReader["motivoCancelacion"]
                                });
                            }
                        }
                        recepcion.idOrdenSer = (int)id.Value;
                        return new OperationResult { mensaje = (string)codigo.Value, valor = (int)spValor.Value, identity = (int)id.Value, result = recepcion };
                    }
                }

            }
            catch (Exception e)
            {
                return new OperationResult { mensaje = e.Message, valor = 1, result = null };
            }
        }

        public OperationResult insertDetallesRecepcion(List<DetalleRecepcion> detalle)
        {
            try
            {
                var xmlResult = GenerateXML.generateListDetallesRecepcion(detalle);
                //string mensaje = deta(xmlResult);
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_insertDetallesSerOS_2";

                        command.Parameters.Add(new SqlParameter("@identificador", detalle[0].idRecepcion));                        
                        command.Parameters.Add(new SqlParameter("@XML", xmlResult));

                        //recepcion.personal.nombreCompleto
                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        var id = command.Parameters.Add(new SqlParameter("@id", SqlDbType.Int) { Direction = ParameterDirection.Output });

                        connection.Open();
                        var i = command.ExecuteReader();
                        
                        detalle[0].idDetalleRecepcion = (int)id.Value;
                        return new OperationResult { mensaje = (string)codigo.Value, valor = (int)spValor.Value, identity = (int)id.Value, result = detalle[0] };
                    }
                }

            }
            catch (Exception e)
            {
                return new OperationResult { mensaje = e.Message, valor = 1, result = null };
            }
        }


        public OperationResult deleteRecepcion(Recepcion recepcion)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_deleteRecepcion";

                        command.Parameters.Add(new SqlParameter("@idOrdenSer", recepcion.idOrdenSer));
                        command.Parameters.Add(new SqlParameter("@motivoBaja", recepcion.motivoBaja));
                        command.Parameters.Add(new SqlParameter("@idUsuario", recepcion.UsuarioRecepcion));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                        connection.Open();
                        var i = command.ExecuteNonQuery();

                        return new OperationResult { mensaje = (string)codigo.Value, valor = (int)spValor.Value };
                    }
                }
            }
            catch (Exception e)
            {
                return new OperationResult { mensaje = e.Message, valor = 1, result = null };
            }
        }

        public OperationResult getAllrecepciones(int parametro)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getAllrecepciones";
                        command.Parameters.Add(new SqlParameter("@parametro", parametro));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        connection.Open();
                        var i = command.ExecuteReader();
                        List<Recepcion> ListRecepcion = new List<Recepcion>();

                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                Recepcion recepcion = new Recepcion
                                {
                                    idOrdenSer = (int)sqlReader["recepcion_idOrdenSer"],
                                    unidadTransporte = getUnidadTransporte((string)sqlReader["recepcion_id_UnidadTrans"]),
                                    personal = (int)sqlReader["recepcion_idEmpleadoEntrega"] == 0 ? null: getPersonal((int)sqlReader["recepcion_idEmpleadoEntrega"]),
                                    personalAutoriza = getPersonal((int)sqlReader["recepcion_idEmpleadoAutoriza"]),
                                    FechaRecepcion = sqlReader["recepcion_FechaRecepcion"] == DBNull.Value ? DateTime.Now : (DateTime)sqlReader["recepcion_FechaRecepcion"],
                                    fechaCaptura = sqlReader["recepcion_fechaCaptura"] == DBNull.Value ?  DateTime.Now : (DateTime)sqlReader["recepcion_fechaCaptura"],
                                    Kilometraje = (decimal)sqlReader["recepcion_Kilometraje"],
                                    Estatus = (string)sqlReader["recepcion_Estatus"]

                                };
                                OperationResult svc = new OperationResult();

                                svc = getTipoOrden((int)sqlReader["recepcion_idTipoOrden"]);
                                if (svc.typeResult != ResultTypes.success)
                                {
                                    return new OperationResult { valor = svc.valor, mensaje = svc.mensaje, result = null };
                                }
                                else
                                {
                                    recepcion.tipoOrden = (TipoOrden)svc.result;
                                }

                                svc =parametro == 2 ? getDetallesRecepcionCompleto((int)sqlReader["recepcion_idOrdenSer"]) : getDetallesRecepcion((int)sqlReader["recepcion_idOrdenSer"]);
                                if (svc.typeResult != ResultTypes.success && svc.typeResult != ResultTypes.recordNotFound)
                                {
                                    return new OperationResult { valor = svc.valor, mensaje = svc.mensaje, result = null };
                                }
                                else
                                {
                                    recepcion.detalles = (List<DetalleRecepcion>)svc.result;
                                }
                                ListRecepcion.Add(recepcion);
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)codigo.Value, result = ListRecepcion };
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }

        public OperationResult getRecepcionesById(int parametro, int id)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getAllrecepcionesById";
                        command.Parameters.Add(new SqlParameter("@parametro", parametro));
                        command.Parameters.Add(new SqlParameter("@id", id));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        connection.Open();
                        var i = command.ExecuteReader();
                        List<Recepcion> ListRecepcion = new List<Recepcion>();

                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                Recepcion recepcion = new Recepcion
                                {
                                    idOrdenSer = (int)sqlReader["recepcion_idOrdenSer"],
                                    unidadTransporte = getUnidadTransporte((string)sqlReader["recepcion_id_UnidadTrans"]),
                                    personal = (int)sqlReader["recepcion_idEmpleadoEntrega"] == 0 ? null : getPersonal((int)sqlReader["recepcion_idEmpleadoEntrega"]),
                                    personalAutoriza = getPersonal((int)sqlReader["recepcion_idEmpleadoAutoriza"]),
                                    FechaRecepcion = sqlReader["recepcion_FechaRecepcion"] == DBNull.Value ? DateTime.Now : (DateTime)sqlReader["recepcion_FechaRecepcion"],
                                    fechaCaptura = sqlReader["recepcion_FechaRecepcion"] == DBNull.Value ? new DateTime() : (DateTime)sqlReader["recepcion_FechaRecepcion"],
                                    Kilometraje = (decimal)sqlReader["recepcion_Kilometraje"],
                                    Estatus = (string)sqlReader["recepcion_Estatus"]

                                };
                                OperationResult svc = new OperationResult();

                                svc = getTipoOrden((int)sqlReader["recepcion_idTipoOrden"]);
                                if (svc.typeResult != ResultTypes.success)
                                {
                                    return new OperationResult { valor = svc.valor, mensaje = svc.mensaje, result = null };
                                }
                                else
                                {
                                    recepcion.tipoOrden = (TipoOrden)svc.result;
                                }

                                svc = parametro == 2 ? getDetallesRecepcionCompleto((int)sqlReader["recepcion_idOrdenSer"]) : getDetallesRecepcion((int)sqlReader["recepcion_idOrdenSer"]);
                                if (svc.typeResult != ResultTypes.success && svc.typeResult != ResultTypes.recordNotFound)
                                {
                                    return new OperationResult { valor = svc.valor, mensaje = svc.mensaje, result = null };
                                }
                                else
                                {
                                    recepcion.detalles = (List<DetalleRecepcion>)svc.result;
                                }
                                ListRecepcion.Add(recepcion);
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)codigo.Value, result = ListRecepcion };
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }

        public OperationResult getrecepcionComplete(int idOrdenServ)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getRecepcionByID";
                        command.Parameters.Add(new SqlParameter("@idOrdenServ", idOrdenServ));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        connection.Open();
                        var i = command.ExecuteReader();
                        Recepcion recepcion = new Recepcion();

                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                recepcion = new Recepcion
                                {
                                    idOrdenSer = (int)sqlReader["recepcion_idOrdenSer"],
                                    unidadTransporte = getUnidadTransporte((string)sqlReader["recepcion_id_UnidadTrans"]),
                                    personal = getPersonal((int)sqlReader["recepcion_idEmpleadoEntrega"]),
                                    personalAutoriza = getPersonal((int)sqlReader["recepcion_idEmpleadoAutoriza"]),
                                    FechaRecepcion = sqlReader["recepcion_FechaRecepcion"] == DBNull.Value ? DateTime.Now : (DateTime)sqlReader["recepcion_FechaRecepcion"],
                                    fechaCaptura = sqlReader["recepcion_fechaCaptura"] == DBNull.Value ? new DateTime() : (DateTime)sqlReader["recepcion_fechaCaptura"],
                                    Kilometraje = (decimal)sqlReader["recepcion_Kilometraje"],
                                    Estatus = (string)sqlReader["recepcion_Estatus"]

                                };
                                OperationResult svc = new OperationResult();

                                svc = getTipoOrden((int)sqlReader["recepcion_idTipoOrden"]);
                                if (svc.typeResult != ResultTypes.success)
                                {
                                    return new OperationResult { valor = svc.valor, mensaje = svc.mensaje, result = null };
                                }
                                else
                                {
                                    recepcion.tipoOrden = (TipoOrden)svc.result;
                                }

                                svc =getDetallesRecepcionCompleto((int)sqlReader["recepcion_idOrdenSer"]);
                                if (svc.typeResult != ResultTypes.success && svc.typeResult != ResultTypes.recordNotFound)
                                {
                                    return new OperationResult { valor = svc.valor, mensaje = svc.mensaje, result = null };
                                }
                                else
                                {
                                    recepcion.detalles = (List<DetalleRecepcion>)svc.result;
                                }                                
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)codigo.Value, result = recepcion };
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }

        public OperationResult getDetallesRecepcion(int parametro)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getDetallesRecepcion";

                        command.Parameters.Add(new SqlParameter("@parametro", parametro));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                        connection.Open();
                        var i = command.ExecuteReader();
                        List<DetalleRecepcion> detalles = new List<DetalleRecepcion>();
                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                detalles.Add(new DetalleRecepcion
                                {
                                    idDetalleRecepcion = (int)sqlReader["idOrdenTrabajo"],
                                    idRecepcion = (int)sqlReader["idOrdenSer"],
                                    descripcion = (string)sqlReader["NotaRecepcion"],
                                    //familia = new Familia
                                    //{
                                    //    idFamilia = (int)sqlReader["idFamilia"],
                                    //    nomFamilia = (string)sqlReader["NomFamilia"]
                                    //},
                                    divicion = new Divicion
                                    {
                                        idDivicion = (int)sqlReader["idDivision"],
                                        nomDivicion = (string)sqlReader["NomDivision"]
                                    },
                                    isCancelado = (bool)sqlReader["isCancelado"],
                                    motivoCancelacion = (string)sqlReader["motivoCancelacion"]
                                });
                            }
                        }
                        if (detalles.Count > 0)
                        {
                            var s = detalles;
                        }
                        return new OperationResult { mensaje = (string)codigo.Value, valor = (int)spValor.Value, result = detalles };
                    }
                }

            }
            catch (Exception e)
            {
                return new OperationResult { mensaje = e.Message, valor = 1, result = null };
            }
        }

        public OperationResult getDetallesRecepcionCompleto(int parametro)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getDetallesRecepcionCompleto";

                        command.Parameters.Add(new SqlParameter("@parametro", parametro));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                        connection.Open();
                        var i = command.ExecuteReader();
                        List<DetalleRecepcion> detalles = new List<DetalleRecepcion>();
                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                detalles.Add(new DetalleRecepcion
                                {
                                    idDetalleRecepcion = (int)sqlReader["idOrdenTrabajo"],
                                    idRecepcion = (int)sqlReader["idOrdenSer"],
                                    descripcion = (string)sqlReader["NotaRecepcion"],
                                    familia = new Familia
                                    {
                                        idFamilia = (int)sqlReader["idFamilia"],
                                        nomFamilia = (string)sqlReader["NomFamilia"]
                                    },
                                    divicion = new Divicion
                                    {
                                        idDivicion = (int)sqlReader["idDivision"],
                                        nomDivicion = (string)sqlReader["NomDivision"]
                                    },
                                    isCancelado = (bool)sqlReader["isCancelado"],
                                    motivoCancelacion = (string)sqlReader["motivoCancelacion"],
                                    _personal = mapPersonal(((int)sqlReader["idPersonal"]))
                                });
                            }
                        }
                        if (detalles.Count > 0)
                        {
                            var s = detalles;
                        }
                        return new OperationResult { mensaje = (string)codigo.Value, valor = (int)spValor.Value, result = detalles };
                    }
                }

            }
            catch (Exception e)
            {
                return new OperationResult { mensaje = e.Message, valor = 1, result = null };
            }
        }
        private Personal mapPersonal(int v)
        {
            var resp = new PersonalService().getPersonalById(v);
            if (resp.typeResult == ResultTypes.success)
            {
                return (Personal)resp.result;
            }
            return null;
        }

        private UnidadTransporte getUnidadTransporte(string idParam)
        {
            UnidadTransporteService servicio = new UnidadTransporteService();
            return (UnidadTransporte)servicio.findAUnidadesTransporteById(idParam).result;
        }
        private Personal getPersonal(int parametro)
        {
            OperationResult servicio = new PersonalService().getPersonalById(parametro);
            return (Personal)servicio.result;
        }
        private OperationResult getTipoOrden(int parametro)
        {
            OperationResult servicio = new TipoOrdenServices().getTiposOrdenByID(parametro);
            return servicio;
        }

    }
}
