﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using Core.Utils;
using System.Data;
using System.Data.SqlClient;

namespace Core.BusinessLogic
{
    public class PersonalService
    {
        public  OperationResult findAllPersonal()
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {

                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_findAllPersonal";
                        int parametro = 0;
                        command.Parameters.Add(new SqlParameter("@parametro", parametro));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        connection.Open();
                        var i = command.ExecuteReader();
                        List<Personal> listPersonal = new List<Personal>();

                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                listPersonal.Add(new Personal
                                {
                                    idPersonal = (int)sqlReader["Personal_idPersonal"],
                                    apellido_Paterno = (string)sqlReader["Personal_ApellidoPat"],
                                    apellido_Materno = (string)sqlReader["Personal_ApellidoMat"],
                                    nombreCompleto = (string)sqlReader["Personal_NombreCompleto"],
                                    estatus = (string)sqlReader["Personal_Estatus"],
                                    empresa = new Empresa
                                    {
                                        idEmpresa = (int)sqlReader["empresa_idEmpresa"],
                                        razonSocial = (string)sqlReader["empresa_RazonSocial"],
                                        representante = (string)sqlReader["empresa_Representante"],
                                        CIDEMPRESA_COM = (int)sqlReader["empresa_CIDEMPRESA_COM"],
                                        direccion = (string)sqlReader["empresa_Direccion"],
                                        idColonia = (int)sqlReader["empresa_idColonia"],
                                        telefono = (string)sqlReader["empresa_Telefono"],
                                        fax = (string)sqlReader["empresa_Fax"],
                                        movilRepresentante = (string)sqlReader["empresa_MovilRepresentante"]
                                    }
                                    
                                });
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)codigo.Value, result = listPersonal };
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }
        public OperationResult getPersonalById(int parametro)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {

                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_findAllPersonal";
                        command.Parameters.Add(new SqlParameter("@parametro", parametro));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        connection.Open();
                        var i = command.ExecuteReader();
                        Personal personal = new Personal();

                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                personal = (new Personal
                                {
                                    idPersonal = (int)sqlReader["Personal_idPersonal"],
                                    apellido_Paterno = (string)sqlReader["Personal_ApellidoPat"],
                                    apellido_Materno = (string)sqlReader["Personal_ApellidoMat"],
                                    nombreCompleto = (string)sqlReader["Personal_NombreCompleto"],
                                    estatus = (string)sqlReader["Personal_Estatus"],
                                    empresa = new Empresa
                                    {
                                        idEmpresa = (int)sqlReader["empresa_idEmpresa"],
                                        razonSocial = (string)sqlReader["empresa_RazonSocial"],
                                        representante = (string)sqlReader["empresa_Representante"],
                                        CIDEMPRESA_COM = (int)sqlReader["empresa_CIDEMPRESA_COM"],
                                        direccion = (string)sqlReader["empresa_Direccion"],
                                        idColonia = (int)sqlReader["empresa_idColonia"],
                                        telefono = (string)sqlReader["empresa_Telefono"],
                                        fax = (string)sqlReader["empresa_Fax"],
                                        movilRepresentante = (string)sqlReader["empresa_MovilRepresentante"]
                                    }

                                });
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)codigo.Value, result = personal };
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }

        public OperationResult getPersonalByUsuario(string sUserId)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {

                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_findAllPersonalByUser";
                        command.Parameters.Add(new SqlParameter("@parametro", sUserId));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        connection.Open();
                        var i = command.ExecuteReader();
                        Personal personal = new Personal();

                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                personal = (new Personal
                                {
                                    idPersonal = (int)sqlReader["Personal_idPersonal"],
                                    apellido_Paterno = (string)sqlReader["Personal_ApellidoPat"],
                                    apellido_Materno = (string)sqlReader["Personal_ApellidoMat"],
                                    nombreCompleto = (string)sqlReader["Personal_NombreCompleto"],
                                    estatus = (string)sqlReader["Personal_Estatus"],
                                    empresa = new Empresa
                                    {
                                        idEmpresa = (int)sqlReader["empresa_idEmpresa"],
                                        razonSocial = (string)sqlReader["empresa_RazonSocial"],
                                        representante = (string)sqlReader["empresa_Representante"],
                                        CIDEMPRESA_COM = (int)sqlReader["empresa_CIDEMPRESA_COM"],
                                        direccion = (string)sqlReader["empresa_Direccion"],
                                        idColonia = (int)sqlReader["empresa_idColonia"],
                                        telefono = (string)sqlReader["empresa_Telefono"],
                                        fax = (string)sqlReader["empresa_Fax"],
                                        movilRepresentante = (string)sqlReader["empresa_MovilRepresentante"]
                                    }

                                });
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)codigo.Value, result = personal };
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }

        public OperationResult getAllPersonalByIdDivicion(int idDivicion)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {

                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getAllPersonalByIdDivicion";
                        
                        command.Parameters.Add(new SqlParameter("@parametro", idDivicion));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        connection.Open();
                        var i = command.ExecuteReader();
                        List<Personal> listPersonal = new List<Personal>();

                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                listPersonal.Add(new Personal
                                {
                                    idPersonal = (int)sqlReader["Personal_idPersonal"],
                                    apellido_Paterno = (string)sqlReader["Personal_ApellidoPat"],
                                    apellido_Materno = (string)sqlReader["Personal_ApellidoMat"],
                                    nombreCompleto = (string)sqlReader["Personal_NombreCompleto"],
                                    estatus = (string)sqlReader["Personal_Estatus"],
                                    empresa = new Empresa
                                    {
                                        idEmpresa = (int)sqlReader["empresa_idEmpresa"],
                                        razonSocial = (string)sqlReader["empresa_RazonSocial"],
                                        representante = (string)sqlReader["empresa_Representante"],
                                        CIDEMPRESA_COM = (int)sqlReader["empresa_CIDEMPRESA_COM"],
                                        direccion = (string)sqlReader["empresa_Direccion"],
                                        idColonia = (int)sqlReader["empresa_idColonia"],
                                        telefono = (string)sqlReader["empresa_Telefono"],
                                        fax = (string)sqlReader["empresa_Fax"],
                                        movilRepresentante = (string)sqlReader["empresa_MovilRepresentante"]
                                    }

                                });
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)codigo.Value, result = listPersonal };
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }
    }
}
