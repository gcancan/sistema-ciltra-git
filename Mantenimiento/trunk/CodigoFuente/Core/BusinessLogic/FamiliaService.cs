﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using Core.Utils;
using System.Data;
using System.Data.SqlClient;

namespace Core.BusinessLogic
{
    public class FamiliaService
    {
        public OperationResult selectAllFamilias()
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {

                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_selectFamilia";
                        //command.Parameters.Add(new SqlParameter("@parametro", parametro));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        connection.Open();
                        var i = command.ExecuteReader();
                        List<Familia> listFamilia = new List<Familia>();

                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                listFamilia.Add(new Familia
                                {
                                    idFamilia = (int)sqlReader["familia_idFamilia"],
                                    nomFamilia = (string)sqlReader["familia_NomFamilia"],
                                    divicion = new Divicion
                                    {
                                        idDivicion = (int)sqlReader["divicion_idDivision"],
                                        nomDivicion = (string)sqlReader["divicion_NomDivision"]
                                    }
                                });
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)codigo.Value, result = listFamilia };
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }

        public OperationResult selectAllFamiliasByIdDivicion(int idDivicion)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {

                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "getFamliaByIdDivicion";
                        command.Parameters.Add(new SqlParameter("@parametro", idDivicion));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        connection.Open();
                        var i = command.ExecuteReader();
                        List<Familia> listFamilia = new List<Familia>();

                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                listFamilia.Add(new Familia
                                {
                                    idFamilia = (int)sqlReader["familia_idFamilia"],
                                    nomFamilia = (string)sqlReader["familia_NomFamilia"],
                                    divicion = new Divicion
                                    {
                                        idDivicion = (int)sqlReader["divicion_idDivision"],
                                        nomDivicion = (string)sqlReader["divicion_NomDivision"]
                                    }
                                });
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)codigo.Value, result = listFamilia };
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }
    }
}
