﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using Core.Utils;
using System.Data;
using System.Data.SqlClient;

namespace Core.BusinessLogic
{
    public class DivicionServices
    {
        public OperationResult getAllDiviciones()
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getAllDiviciones";

                        //command.Parameters.Add(new SqlParameter("@idOrdenSer", parametro);
                      

                        //recepcion.personal.nombreCompleto
                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                       

                        connection.Open();
                        var i = command.ExecuteReader();
                        List<Divicion> listDivicion = new List<Divicion>();
                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                listDivicion.Add(new Divicion
                                {
                                    idDivicion = (int)sqlReader["idDivision"],
                                    nomDivicion = (string)sqlReader["NomDivision"]
                                });
                            }
                        }
                        return new OperationResult { mensaje = (string)codigo.Value, valor = (int)spValor.Value, result = listDivicion };

                    }
                }

            }
            catch (Exception e)
            {
                return new OperationResult { mensaje = e.Message, valor = 1, result = null };
            }
        }
    }
}
