﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using Core.Utils;
using System.Data;
using System.Data.SqlClient;

namespace Core.BusinessLogic
{
    public class ServicioService
    {
        //public OperationResult saveServicio(Servicio servicio)
        //{
        //    try
        //    {
        //        using (var connection = BoundedContextFactory.ConnectionFactory())
        //        {
        //            using (var command = connection.CreateCommand())
        //            {
        //                command.CommandType = CommandType.StoredProcedure;
        //                command.CommandText = "sp_saveCatServicio";

        //                command.Parameters.Add(new SqlParameter("@idServicio", servicio.idServicio));
        //                command.Parameters.Add(new SqlParameter("@clave", servicio.clave));
        //                command.Parameters.Add(new SqlParameter("@nombre", servicio.nombre));
        //                command.Parameters.Add(new SqlParameter("@costo", servicio.costo));
        //                command.Parameters.Add(new SqlParameter("@km_Hr", servicio.km_Hr));
        //                command.Parameters.Add(new SqlParameter("@descripcion", servicio.descripcion));

        //                var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
        //                var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
        //                var id = command.Parameters.Add(new SqlParameter("@identity", SqlDbType.Int) { Direction = ParameterDirection.Output });

        //                connection.Open();
        //                var i = command.ExecuteNonQuery();
        //                servicio.idServicio = (int)id.Value;
        //                return new OperationResult { mensaje = (string)codigo.Value, valor = (int)spValor.Value, identity = (int)id.Value, result = servicio };

        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        return new OperationResult { result = null, mensaje = e.Message, valor = 1 };
        //    }
        //}

        internal OperationResult deleteServicio(Servicio servicio)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_deleteCatServicio";

                        command.Parameters.Add(new SqlParameter("@idServicio", servicio.idServicio));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                        connection.Open();
                        var i = command.ExecuteNonQuery();

                        return new OperationResult { mensaje = (string)codigo.Value, valor = (int)spValor.Value };

                    }
                }
            }
            catch (Exception e)
            {
                return new OperationResult { mensaje = e.Message, valor = 1 };
            }
        }

        internal OperationResult findServicio()
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {

                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_findAllServicios";
                        //command.Parameters.Add(new SqlParameter("@parametro", parametro));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        connection.Open();
                        var i = command.ExecuteReader();
                        List<Servicio> listServicio = new List<Servicio>();

                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                Servicio servicio = (new Servicio
                                {
                                    idServicio = (int)sqlReader["idServicio"],
                                    NomServicio = (string)sqlReader["NomServicio"],
                                    descripcion = (string)sqlReader["Descripcion"],
                                    CadaKms = (decimal)sqlReader["CadaKms"],
                                    CadaTiempoDias = (decimal)sqlReader["CadaTiempoDias"],
                                    Estatus = (string)sqlReader["Estatus"],
                                    costo = (decimal)sqlReader["Costo"],
                                    tipoServicio = new TipoServicio
                                    {
                                        idTipoServicio = (int)sqlReader["idTipoServicio"],
                                        nombreServicio = (string)sqlReader["NomTipoServicio"]
                                    }
                                    //,listActividades = mapList((int)sqlReader["idServicio"])
                                });
                                OperationResult find = findDetallesServicio((int)servicio.idServicio);
                                if (find.typeResult == ResultTypes.success)
                                {
                                    servicio.listActividades = (List<Actividad>)find.result;
                                }
                                else
                                {
                                    return find;
                                }
                                listServicio.Add(servicio);
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)codigo.Value, result = listServicio };
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }

        private List<Actividad> mapList(int v)
        {
            OperationResult resp = findDetallesServicio(v);
            return (List<Actividad>)resp.result;
        }

        public OperationResult findDetallesServicio(int parametro)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {

                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_findDetallesServicio";
                        command.Parameters.Add(new SqlParameter("@parametro", parametro));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        connection.Open();
                        var i = command.ExecuteReader();
                        List<Actividad> listActividad = new List<Actividad>();

                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                Actividad actividad = new Actividad
                                {
                                    idActividad = (int)sqlReader["actividad_idActividad"],
                                    nombreAct = (string)sqlReader["actividad_NombreAct"],
                                    costoManoObra = (decimal)sqlReader["actividad_CostoManoObra"],
                                    duracionHoras = (string)sqlReader["actividad_DuracionHoras"],
                                    familia = new Familia
                                    {
                                        idFamilia = (int)sqlReader["Familia_idFamilia"],
                                        nomFamilia = (string)sqlReader["Familia_NomFamilia"]
                                    }
                                };
                                OperationResult servicio = mapListProductos(actividad.idActividad);
                                if (servicio.typeResult == ResultTypes.success)
                                {
                                    actividad.listProductos = (List<Producto>)servicio.result;
                                }
                                else
                                {
                                    return servicio;
                                }
                                listActividad.Add(actividad);
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)codigo.Value, result = listActividad };
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }

        private OperationResult mapListProductos(int idActividad)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {

                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_getProductosByidActividad";
                        command.Parameters.Add(new SqlParameter("@idActividad", idActividad));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        connection.Open();
                        var i = command.ExecuteReader();
                        List<Producto> listProducto = new List<Producto>();

                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                listProducto.Add(new Producto
                                {
                                    idProducto = (int)sqlReader["CIDPRODUCTO"],
                                    idActividad = idActividad,
                                    clave = (string)sqlReader["CCODIGOPRODUCTO"],
                                    nombre = (string)sqlReader["CNOMBREPRODUCTO"],
                                    cantidad = (decimal)sqlReader["CANTIDAD"],
                                    existencia = (decimal)sqlReader["EXISTENCIA"],
                                    costo = (decimal)sqlReader["COSTO"]
                                });
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)codigo.Value, result = listProducto };
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }
    }
}
