﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using Core.Utils;
using System.Data;
using System.Data.SqlClient;
using System.Windows;
namespace Core.BusinessLogic
{
    public class UnidadTransporteService
    {
        public OperationResult findAllUnidadesTransporte(string idEmpresa)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {

                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_findAllUnidadesTransportes";
                        command.Parameters.Add(new SqlParameter("@idEmpresa", idEmpresa));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        connection.Open();
                        var i = command.ExecuteReader();
                        List<UnidadTransporte> listUnidadesTransporte = new List<UnidadTransporte>();

                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                listUnidadesTransporte.Add(new UnidadTransporte
                                {
                                    id_UnidadTrans = (string)sqlReader["UnidadTransporte_id_UnidadTrans"],
                                    descripcionUni = (string)sqlReader["UnidadTransporte_descripcionUni"],
                                    estatus = (string)sqlReader["UnidadTransporte_Estatus"],
                                    tipoUnidad = new TipoUnidad
                                    {
                                        idTipoUniTrans = (int)sqlReader["tipoUnidad_idTipoUniTras"],
                                        nomTipoUniTrans = (string)sqlReader["tipoUnidad_nomTipoUniTras"],
                                        noLlantas = (string)sqlReader["tipoUnidad_NoLlantas"],
                                        noEjes = (string)sqlReader["tipoUnidad_NoEjes"],
                                        tonelajeMax = (string)sqlReader["tipoUnidad_TonelajeMax"]
                                    },
                                    empresa = new Empresa
                                    {
                                        idEmpresa = (int)sqlReader["empresa_idEmpresa"],
                                        razonSocial = (string)sqlReader["empresa_RazonSocial"],
                                        representante = (string)sqlReader["empresa_Representante"],
                                        CIDEMPRESA_COM = (int)sqlReader["empresa_CIDEMPRESA_COM"],
                                        direccion = (string)sqlReader["empresa_Direccion"],
                                        idColonia = (int)sqlReader["empresa_idColonia"],
                                        telefono = (string)sqlReader["empresa_Telefono"],
                                        fax = (string)sqlReader["empresa_Fax"],
                                        movilRepresentante = (string)sqlReader["empresa_MovilRepresentante"]
                                    },
                                    marca = new Marca
                                    {
                                        idMarca = (int)sqlReader["marcas_idMarca"],
                                        nombreMarca = (string)sqlReader["marcas_NombreMarca"]
                                    },
                                    sucursal = new Sucursal
                                    {
                                        id_sucursal = (int)sqlReader["sucursal_id_sucursal"],
                                        nombreSucursal = (string)sqlReader["sucursal_NomSucursal"],
                                        idEmpresa = (int)sqlReader["sucursal_id_empresa"]
                                    }
                                });
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)codigo.Value, result = listUnidadesTransporte };
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }

        public OperationResult findAllUnidadesTransporteByIdAndIdEmpresa(string idUnidad, string idEmpresa)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {

                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_findAllUnidadesTransportesById";
                        command.Parameters.Add(new SqlParameter("@idEmpresa", idEmpresa));
                        command.Parameters.Add(new SqlParameter("@id", idUnidad));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        connection.Open();
                        var i = command.ExecuteReader();
                        List<UnidadTransporte> listUnidadesTransporte = new List<UnidadTransporte>();

                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                listUnidadesTransporte.Add(new UnidadTransporte
                                {
                                    id_UnidadTrans = (string)sqlReader["UnidadTransporte_id_UnidadTrans"],
                                    descripcionUni = (string)sqlReader["UnidadTransporte_descripcionUni"],
                                    estatus = (string)sqlReader["UnidadTransporte_Estatus"],
                                    tipoUnidad = new TipoUnidad
                                    {
                                        idTipoUniTrans = (int)sqlReader["tipoUnidad_idTipoUniTras"],
                                        nomTipoUniTrans = (string)sqlReader["tipoUnidad_nomTipoUniTras"],
                                        noLlantas = (string)sqlReader["tipoUnidad_NoLlantas"],
                                        noEjes = (string)sqlReader["tipoUnidad_NoEjes"],
                                        tonelajeMax = (string)sqlReader["tipoUnidad_TonelajeMax"]
                                    },
                                    empresa = new Empresa
                                    {
                                        idEmpresa = (int)sqlReader["empresa_idEmpresa"],
                                        razonSocial = (string)sqlReader["empresa_RazonSocial"],
                                        representante = (string)sqlReader["empresa_Representante"],
                                        CIDEMPRESA_COM = (int)sqlReader["empresa_CIDEMPRESA_COM"],
                                        direccion = (string)sqlReader["empresa_Direccion"],
                                        idColonia = (int)sqlReader["empresa_idColonia"],
                                        telefono = (string)sqlReader["empresa_Telefono"],
                                        fax = (string)sqlReader["empresa_Fax"],
                                        movilRepresentante = (string)sqlReader["empresa_MovilRepresentante"]
                                    },
                                    marca = new Marca
                                    {
                                        idMarca = (int)sqlReader["marcas_idMarca"],
                                        nombreMarca = (string)sqlReader["marcas_NombreMarca"]
                                    },
                                    sucursal = new Sucursal
                                    {
                                        id_sucursal = (int)sqlReader["sucursal_id_sucursal"],
                                        nombreSucursal = (string)sqlReader["sucursal_NomSucursal"],
                                        idEmpresa = (int)sqlReader["sucursal_id_empresa"]
                                    }
                                });
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)codigo.Value, result = listUnidadesTransporte };
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }

        public OperationResult findAUnidadesTransporteById(string idParam)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        //return new OperationResult { mensaje = connection.ToString(), valor = 1 };
                        command.CommandType = CommandType.StoredProcedure;
                        //command.CommandText = "sp_findUnidadesTransporteById"; 
                        command.CommandText = "sp_findUnidadesTransporteById";
                        command.Parameters.Add(new SqlParameter("@parametro", idParam));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        connection.Open();
                        var i = command.ExecuteReader();
                        UnidadTransporte  UnidadesTransporte = new UnidadTransporte();

                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                UnidadesTransporte = (new UnidadTransporte
                                {
                                    id_UnidadTrans = (string)sqlReader["UnidadTransporte_id_UnidadTrans"],
                                    descripcionUni = (string)sqlReader["UnidadTransporte_descripcionUni"],
                                    estatus = (string)sqlReader["UnidadTransporte_Estatus"],
                                    tipoUnidad = new TipoUnidad
                                    {
                                        idTipoUniTrans = (int)sqlReader["tipoUnidad_idTipoUniTras"],
                                        nomTipoUniTrans = (string)sqlReader["tipoUnidad_nomTipoUniTras"],
                                        noLlantas = (string)sqlReader["tipoUnidad_NoLlantas"],
                                        noEjes = (string)sqlReader["tipoUnidad_NoEjes"],
                                        tonelajeMax = (string)sqlReader["tipoUnidad_TonelajeMax"]
                                    },
                                    empresa = new Empresa
                                    {
                                        idEmpresa = (int)sqlReader["empresa_idEmpresa"],
                                        razonSocial = (string)sqlReader["empresa_RazonSocial"],
                                        representante = (string)sqlReader["empresa_Representante"],
                                        CIDEMPRESA_COM = (int)sqlReader["empresa_CIDEMPRESA_COM"],
                                        direccion = (string)sqlReader["empresa_Direccion"],
                                        idColonia = (int)sqlReader["empresa_idColonia"],
                                        telefono = (string)sqlReader["empresa_Telefono"],
                                        fax = (string)sqlReader["empresa_Fax"],
                                        movilRepresentante = (string)sqlReader["empresa_MovilRepresentante"]
                                    },
                                    marca = new Marca
                                    {
                                        idMarca = (int)sqlReader["marcas_idMarca"],
                                        nombreMarca = (string)sqlReader["marcas_NombreMarca"]
                                    },
                                    sucursal = new Sucursal
                                    {
                                        id_sucursal = (int)sqlReader["sucursal_id_sucursal"],
                                        nombreSucursal = (string)sqlReader["sucursal_NomSucursal"],
                                        idEmpresa = (int)sqlReader["sucursal_id_empresa"]
                                    }
                                });
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)codigo.Value, result = UnidadesTransporte };
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }
    }
}
