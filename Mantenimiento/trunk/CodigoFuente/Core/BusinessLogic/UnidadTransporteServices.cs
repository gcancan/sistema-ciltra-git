﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using Core.Utils;
using System.Data;
using System.Data.SqlClient;

namespace Core.BusinessLogic
{
    public class UnidadTransporteServices
    {
        public OperationResult saveUnidadTransporte(UnidadTransporte unidadTransporte)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_SaveUnidadSalida";

                        command.Parameters.Add(new SqlParameter("@idUnidadTransporte", unidadTransporte.idUnidadTransporte));
                        command.Parameters.Add(new SqlParameter("@placas", unidadTransporte.placas));
                        command.Parameters.Add(new SqlParameter("@modelo", unidadTransporte.modelo));
                        command.Parameters.Add(new SqlParameter("@numSerie", unidadTransporte.numSerie));
                        command.Parameters.Add(new SqlParameter("@marca", unidadTransporte.marca));
                        command.Parameters.Add(new SqlParameter("@tipoMotor", unidadTransporte.tipoMotor));
                        command.Parameters.Add(new SqlParameter("@tipoCaja", unidadTransporte.tipoCaja));
                        command.Parameters.Add(new SqlParameter("@diferencialInter", unidadTransporte.diferencialInter));
                        command.Parameters.Add(new SqlParameter("@pasoDiferencialInter", unidadTransporte.pasoDiferencialInter));
                        command.Parameters.Add(new SqlParameter("@diferencialMotriz", unidadTransporte.diferencialMotriz));
                        command.Parameters.Add(new SqlParameter("@pasoDiferencialMotriz", unidadTransporte.pasoDiferencialMotriz));
                        command.Parameters.Add(new SqlParameter("@noMotor", unidadTransporte.noMotor));
                        command.Parameters.Add(new SqlParameter("@vin", unidadTransporte.vin));
                        command.Parameters.Add(new SqlParameter("@serieECM", unidadTransporte.serieECM));
                        command.Parameters.Add(new SqlParameter("@RFID", unidadTransporte.RFID));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        var id = command.Parameters.Add(new SqlParameter("@IDENTITY", SqlDbType.Int) { Direction = ParameterDirection.Output });



                        connection.Open();
                        var i = command.ExecuteNonQuery();
                        unidadTransporte.idUnidadTransporte = (int)id.Value;
                        return new OperationResult { mensaje = (string)codigo.Value, valor = (int)spValor.Value, identity = (int)id.Value, result = unidadTransporte };

                    }
                }
            }
            catch (Exception e)
            {
                return new OperationResult { mensaje = e.Message, valor = 1 };
            }
        }

        internal OperationResult findUnidadTransporteByStatusID(string parametro, string status)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                       
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_findUnidadTransporte";
                        command.Parameters.Add(new SqlParameter("@parametro", parametro));
                        command.Parameters.Add(new SqlParameter("@status", status));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        connection.Open();
                        var i = command.ExecuteReader();
                        List<UnidadTransporte> listUnidadTransporte = new List<UnidadTransporte>();

                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                listUnidadTransporte.Add(new UnidadTransporte
                                {
                                    idUnidadTransporte = (int)sqlReader["idUnidadTransporte"],
                                    placas = (string)sqlReader["placas"],
                                    modelo = (int)sqlReader["modelo"],
                                    numSerie = (string)sqlReader["numSerie"],
                                    marca = (string)sqlReader["marca"],
                                    tipoMotor = (string)sqlReader["tipoMotor"],
                                    tipoCaja = (string)sqlReader["tipoCaja"],
                                    diferencialInter = (string)sqlReader["diferencialInter"],
                                    pasoDiferencialInter = (string)sqlReader["pasoDiferencialInter"],
                                    diferencialMotriz = (string)sqlReader["diferencialMotriz"],
                                    pasoDiferencialMotriz = (string)sqlReader["pasoDiferencialMotriz"],
                                    noMotor = (string)sqlReader["noMotor"],
                                    vin = (string)sqlReader["vin"],
                                    serieECM = (string)sqlReader["serieECM"],
                                    RFID = (string)sqlReader["RFID"],
                                    status = Convert.ToInt32((bool)sqlReader["status"])
                                });
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)codigo.Value, result = listUnidadTransporte };
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }

        public OperationResult deleteUnidadTransporte(UnidadTransporte unidadTransporte)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_deleteUnidadTransporte";

                        command.Parameters.Add(new SqlParameter("@idUnidadTransporte", (int)unidadTransporte.idUnidadTransporte));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                        connection.Open();
                        var i = command.ExecuteNonQuery();

                        return new OperationResult { mensaje = (string)codigo.Value, valor = (int)spValor.Value };
                    }
                }
            }
            catch (Exception e)
            {
                return new OperationResult { mensaje = e.Message, valor = 1 };
            }
        }
    }
}
