﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using Core.Utils;
using System.Data;
using System.Data.SqlClient;

namespace Core.BusinessLogic
{
    public class TiposTransporteServices
    {
        public OperationResult saveTipoTransporte(TipoTransporte tipoTransporte)
        {
            try
            {

                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_saveCatTipoTransporte";

                        command.Parameters.Add(new SqlParameter("@id", tipoTransporte.idTipoTransporte));
                        command.Parameters.Add(new SqlParameter("@nombre", tipoTransporte.nombre));
                        

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        var id = command.Parameters.Add(new SqlParameter("@identificador", SqlDbType.Int) { Direction = ParameterDirection.Output });

                        connection.Open();
                        var i = command.ExecuteNonQuery();
                        tipoTransporte.idTipoTransporte = (int)id.Value;
                        return new OperationResult { mensaje = (string)codigo.Value, valor = (int)spValor.Value, identity = (int)id.Value, result = tipoTransporte };

                    }
                }

            }
            catch (Exception e)
            {
                return new OperationResult { mensaje = e.Message, valor = 1, result = null };
            }
        }

        internal OperationResult findTiposTransportes(string parametro)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {

                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_findTiposTransporte";
                        command.Parameters.Add(new SqlParameter("@parametro", parametro));

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        connection.Open();
                        var i = command.ExecuteReader();
                        List<TipoTransporte> listTipoTransporte = new List<TipoTransporte>();

                        using (SqlDataReader sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                listTipoTransporte.Add(new TipoTransporte
                                {
                                    idTipoTransporte = (int)sqlReader["idTipoTransp"],
                                    nombre = (string)sqlReader["nombre"],                                    
                                });
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)codigo.Value, result = listTipoTransporte };
                    }
                }
            }
            catch (Exception x)
            {
                return new OperationResult { valor = 1, mensaje = x.Message, result = null };
            }
        }

        public OperationResult deleteTipoTransporte(TipoTransporte tipoTransporte)
        {
            try
            {

                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "sp_deleteCatTipoTransporte";

                        command.Parameters.Add(new SqlParameter("@id", tipoTransporte.idTipoTransporte));  

                        var spValor = command.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        var codigo = command.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });                       

                        connection.Open();
                        var i = command.ExecuteNonQuery();                     
                        return new OperationResult { mensaje = (string)codigo.Value, valor = (int)spValor.Value};

                    }
                }
            }
            catch (Exception e)
            {
                return new OperationResult { mensaje = e.Message, valor = 1};
            }
        }
    }
}
