﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Threading.Tasks;
using Microsoft.Win32;

namespace Core.Utils
{
    public static class BoundedContextFactory
    {
        
        public static SqlConnection ConnectionFactory()
        {
            SqlConnection sqlCon = new SqlConnection(getConnectionString());
            return sqlCon;
        }

        public static string getConnectionString()
        {
            bool d = false;
#if DEBUG
            d = true;
#endif
            RegistryKey KeySistema = Registry.CurrentUser.OpenSubKey(string.Format(@"SOFTWARE\VB and VBA Program Settings\{0}\ServerSQL", d ? "Fletera.Com.vsHost" : "Fletera.Com"));



            return new SqlConnectionStringBuilder
            {
                InitialCatalog = KeySistema.GetValue("Nombre de la Base de Datos").ToString(),
                DataSource = KeySistema.GetValue("Nombre del servidor").ToString(),
                UserID = KeySistema.GetValue("Nombre del Usuario SQL").ToString(),
                Password = new decodificar().buscarPass(KeySistema.GetValue("Password del Usuario SQL").ToString())
            }.ToString();
            //var connectionString = ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["DBConnectionName"]].ConnectionString;
            //return connectionString;
        }
    }
}
