﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using System.Xml.Linq;

namespace Core.Utils
{
    public class GenerateXML
    {
        public static String generateListDetallesRecepcion(List<DetalleRecepcion> listaDetalles)
        {
            try
            {
                XElement itemsXML = new XElement("DetallesRecepcion");
                itemsXML.Add(
                    from item in listaDetalles
                    where item != null && item.divicion != null
                    select new XElement("Detalles",
                            new XElement("idOrdenTrabajo", item.idDetalleRecepcion),
                            new XElement("idFamilia", item.familia == null ? 0 : item.idFamilia),
                            new XElement("NotaRecepcion", item.descripcion),
                            new XElement("isCancelado", item.isCancelado),
                            new XElement("motivoCancelacion", item.motivoCancelacion),
                            new XElement("idDivicion", item.idDivicion),
                            new XElement("idPersonal" ,item._personal == null ? 0 : item._personal.idPersonal)
                            )
                    );

                return itemsXML.ToString();
            }
            catch (Exception e)
            {
                var s = e;
                return null;
            }
        }

        internal static object generatelistDiagnostico(List<Diagnostico> listDiagnostico)
        {
            try
            {
                XElement itemsXML = new XElement("listDiagnostico");
                itemsXML.Add(
                    from item in listDiagnostico
                    where item != null && item.recepcion != null
                    select new XElement("Diagnostico",
                            new XElement("idOrdenTrabajo", item.idOrdenTrabajo),
                            new XElement("idOrdenSer", item.idOrdenServ),
                            new XElement("idServicio", item.idServicio),
                            new XElement("idActividad", item.actividad == null ? 0 : item.idActividad),
                            new XElement("DuracionActHr", item.actividad == null ? "0" :  item.DuracionActHr),
                            new XElement("NoPersonal", item.NoPersonal),
                            new XElement("id_proveedor", item.id_proveedor),
                            new XElement("Estatus", item.Estatus),
                            new XElement("CostoManoObra", item.actividad == null ? 0.00m : item.CostoManoObra),
                            new XElement("CostoProductos", item.actividad == null ? 0.00m : item.CostoProductos),
                            new XElement("FechaDiag", DateTime.Now),
                            new XElement("UsuarioDiag", item.UsuarioDiag),
                            new XElement("FechaAsig", item.FechaAsig),
                            new XElement("UsuarioAsig", item.UsuarioAsig),
                            new XElement("FechaPausado", null),
                            new XElement("NotaPausado", item.NotaPausado),
                            new XElement("FechaTerminado", null),
                            new XElement("UsuarioTerminado", item.UsuarioTerminado)
                            )
                    );

                return itemsXML.ToString();
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
