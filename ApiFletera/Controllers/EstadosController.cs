﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Core.Models;
using Core.Interfaces;
namespace ApiFletera.Controllers
{
    [RoutePrefix("api/Estados")]
    public class EstadosController : ApiController
    {
        [AllowAnonymous]
        [Route("GetAllEstadosApi")]
        public List<Estado> GetAllBaseAnswerApi()
        {
            var serv = new EstadoSvc().getEstadosAllorById();
            if (serv.typeResult == ResultTypes.success)
            {
                return (List<Estado>)serv.result;
            }
            return null;
        }
    }
}
