﻿using ApiFletera.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace ApiFletera.Providers
{
    public class SimpleAuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        private readonly Func<UserManager<ApplicationUser>> _userManagerFactory;

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            // Resource owner password credentials does not provide a client ID.
            if (context.ClientId == null)
            {
                context.Validated();
            }

            return Task.FromResult<object>(null);
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            try
            {
                //context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });
                //ClaimsIdentity identity;
                //using (UserManager<ApplicationUser> userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new AuthContext())))
                //{
                //    ApplicationUser user = await userManager.FindAsync(context.UserName, context.Password);

                //    if (user == null)
                //    {
                //        context.SetError("invalid_grant", "The user name or password is incorrect.");
                //        return;
                //    }

                //    //      identity = new ClaimsIdentity(context.Options.AuthenticationType);
                //    //  identity.AddClaim(new Claim("sub", context.UserName));
                //    //identity.Name = user.Id;
                //    //ClaimsIdentity cookiesIdentity = await userManager.CreateIdentityAsync(user,
                //    //    CookieAuthenticationDefaults.AuthenticationType);
                //    //  identity.AddClaim(new Claim("role", "Administrator"));
                //    //  IList<string> roles = await _repo.UserRoles(user.Id);
                //    //  identity.AddClaim(new Claim(ClaimTypes.Role, roles.First()));
                //    //   identity.AddClaim(
                //    ClaimsIdentity oAuthIdentity = await userManager.CreateIdentityAsync(user,
                //     context.Options.AuthenticationType);
                //    ClaimsIdentity cookiesIdentity = await userManager.CreateIdentityAsync(user,
                //        CookieAuthenticationDefaults.AuthenticationType);
                //    AuthenticationProperties properties = CreateProperties(user.UserName, user.Id, user.ExportData.ToString(), user.IdProduct, oAuthIdentity.Claims.ElementAt(3).Value);
                //    AuthenticationTicket ticket = new AuthenticationTicket(oAuthIdentity, properties);
                //    context.Validated(ticket);
                //    context.Request.Context.Authentication.SignIn(cookiesIdentity);
                //    //, 
                //}

            }
            catch (Exception ex)
            {
                context.SetError("invalid_grant", ex.Message + ": " + ex.InnerException.Message);
                return;
            }
            //            context.Validated(identity);

        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            // var n = context.Request.User;
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            //  context.AdditionalResponseParameters.Add("userID", context.Ide);
            return Task.FromResult<object>(null);
        }

        public static AuthenticationProperties CreateProperties(string userName, string userId, string exportExcel, string idproduct, string role)
        {
            IDictionary<string, string> data = new Dictionary<string, string>
            {
                { "userName", userName },{ "userId", userId },{ "exportExcel", exportExcel }, {"IdProduct",idproduct}, {"Role",role}
            };
            return new AuthenticationProperties(data);
        }
    }
}