--SELECT  *
--FROM    dbo.menusAplicacion;
INSERT  INTO dbo.menusAplicacion
        ( idPadre ,
          clave ,
          nombre ,
          descripcion
        )
VALUES  ( ( SELECT  idMenuAplicacion
            FROM    dbo.menusAplicacion
            WHERE   clave = 'MENU_SALIDAS'
          ) , -- idPadre - int
          N'MENU_SALIDAS_VIAJE_EN_FALSO' , -- clave - nvarchar(40)
          N'VIAJES EN FALSO' , -- nombre - nvarchar(max)
          N'menu para acceder a la pantalla de viajes en falso'  -- descripcion - nvarchar(max)
        );
INSERT  INTO dbo.privilegios
        ( clave ,
          descripcion
        )
VALUES  ( N'MENU_SALIDAS_VIAJE_EN_FALSO' , -- clave - nvarchar(max)
          N'privilegio para acceder al modulo de viajes en falso'  -- descripcion - nvarchar(max)
        );
        
INSERT  INTO dbo.privilegiosUsuario
        ( idPrivilegio ,
          idUsuario
        )
VALUES  ( ( SELECT  idPrivilegio
            FROM    dbo.privilegios
            WHERE   clave = 'MENU_SALIDAS_VIAJE_EN_FALSO'
          ) , -- idPrivilegio - int
          ( SELECT  idUsuario
            FROM    dbo.usuariosSys
            WHERE   nombreUsuario = 'admin'
          )  -- idUsuario - int
        );