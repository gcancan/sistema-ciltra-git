
INSERT  INTO dbo.menusAplicacion
        ( idPadre ,
          clave ,
          nombre ,
          descripcion
        )
VALUES  ( ( SELECT  idMenuAplicacion
            FROM    dbo.menusAplicacion
            WHERE   clave = 'MENU_CATALOGOS'
          ) , -- idPadre - int
          N'MENU_CATALOGOS_ASIGNACION_RFID' , -- clave - nvarchar(40)
          N'ASIGNACION DE RFID' , -- nombre - nvarchar(max)
          N'menu para la asignacion de rfid'  -- descripcion - nvarchar(max)
        );
INSERT  INTO dbo.privilegios
        ( clave ,
          descripcion
        )
VALUES  ( N'MENU_CATALOGOS_ASIGNACION_RFID' , -- clave - nvarchar(max)
          N'privilegio para acceder al modulo de asignacion de rfid'  -- descripcion - nvarchar(max)
        );
        
INSERT  INTO dbo.privilegiosUsuario
        ( idPrivilegio ,
          idUsuario
        )
VALUES  ( ( SELECT  idPrivilegio
            FROM    dbo.privilegios
            WHERE   clave = 'MENU_CATALOGOS_ASIGNACION_RFID'
          ) , -- idPrivilegio - int
          ( SELECT  idUsuario
            FROM    dbo.usuariosSys
            WHERE   nombreUsuario = 'admin'
          )  -- idUsuario - int
        );