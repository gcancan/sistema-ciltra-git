
GO
/****** Object:  StoredProcedure [dbo].[sp_saveSalidasEntradas]    Script Date: 09/26/2016 12:55:39 ******/
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
-- =============================================
-- Author:		<Author,GRCC,Name>
-- Create date: <Create Date,20/06/2016,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[sp_saveSalidasEntradas]
    @valor INT OUT ,
    @mensaje NVARCHAR(MAX) OUT ,
    @id INT ,
    @identificador INT OUT ,
    
    --@idTractor NVARCHAR(25) ,
    --@idTolva1 NVARCHAR(25) = NULL ,
    --@idDolly NVARCHAR(25) = NULL ,
    --@idTolva2 NVARCHAR(25) = NULL ,
    --@idChofer INT ,
    @idPadreOrdSer INT ,
    @tipoMovimiento INT ,
    @km DECIMAL(18, 4) ,
    @kmFinal DECIMAL(18, 4) = 0 ,
    @kmRecorridos DECIMAL(18, 4) = 0 ,
    @ltCombustible DECIMAL(18, 4) = 0 ,
    @ltCombustibleUtilizado DECIMAL(18, 4) = 0 ,
    @ltCombustiblePTO DECIMAL(18, 4) = 0 ,
    @ltCombustibleEST DECIMAL(18, 4) = 0 ,
    @fecha DATETIME = NULL ,
    @fechaFin DATETIME = NULL ,
    @fechaCombustible DATETIME = NULL ,
    @tiempoTotal NVARCHAR(20) = NULL ,
    @tiempoPTO NVARCHAR(20) = NULL ,
    @tiempoEST NVARCHAR(20) = NULL ,
    @xml XML = NULL ,
    @idDespachador INT = NULL ,
    @sello1 INT = NULL ,
    @sello2 INT = NULL ,
    @selloRes INT = NULL
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
        BEGIN TRAN guardar;
        BEGIN TRY
           
            IF @id = 0
                BEGIN
                    INSERT  INTO dbo.salidasEntradas
                            ( idPadreOrdSer ,
                              fecha ,
                              fechaCombustible ,
                              fechaFin ,
                              tipoOperacion ,
                              KM ,
                              KMFinal ,
                              kmRecorridos ,
                              ltConbustible ,
                              ltCombustibleUtilizado ,
                              ltCombustiblePTO ,
                              ltCombustibleEST ,
                              tiempoTotal ,
                              tiempoPTO ,
                              tiempoEST ,
                              idDespachador ,
                              sello1 ,
                              sello2 ,
                              selloRes
                            )
                    VALUES  ( @idPadreOrdSer , -- idPadreOrdSer - int
                              GETDATE() , -- fecha - datetime
                              @fechaCombustible , -- fechaCombustible - datetime
                              @fechaFin , -- fechaFin - datetime
                              @tipoMovimiento , -- tipoOperacion - int
                              @km , -- KM - decimal
                              @kmFinal , -- KMFinal - decimal
                              @kmRecorridos , -- kmRecorridos - decimal
                              @ltCombustible , -- ltConbustible - decimal
                              @ltCombustibleUtilizado , -- ltCombustibleUtilizado - decimal
                              @ltCombustiblePTO , -- ltCombustiblePTO - decimal
                              @ltCombustibleEST , -- ltCombustibleEST - decimal
                              @tiempoTotal , -- tiempoTotal - nvarchar(15)
                              @tiempoPTO , -- tiempoPTO - nvarchar(15)
                              @tiempoEST , -- tiempoEST - nvarchar(15)
                              @idDespachador ,
                              @sello1 ,
                              @sello2 ,
                              @selloRes
                            );
                    SET @identificador = @@IDENTITY;
                   
                    
					--//////////////////////////////////////////
                    DECLARE @DocHandle INT;
                    DECLARE @tableDetalles TABLE
                        (
                          idMotivo INT ,
                          descripcion NVARCHAR(100)
                        );
            
                    EXEC sp_xml_preparedocument @DocHandle OUTPUT, @xml;
                    INSERT  INTO @tableDetalles
                            ( idMotivo ,
                              descripcion
                            )
                            SELECT  idMotivo ,
                                    descripcion
                            FROM    OPENXML (@DocHandle, '/listaMotivos/motivo', 2)
			WITH (idMotivo INT, descripcion NVARCHAR(100)); 
                    EXEC sp_xml_removedocument @DocHandle;
            
                    INSERT  INTO dbo.detalleMotivosEntrada
                            ( idSalidaEntrada ,
                              idMotivo ,
                              descripcion
                            )
                            SELECT  @identificador ,
                                    idMotivo ,
                                    descripcion
                            FROM    @tableDetalles;
					--/////////////////////////////////////////
                  
                END;
            ELSE
                BEGIN
                    UPDATE  dbo.salidasEntradas
                    SET     kmRecorridos = @kmRecorridos ,
                            KMFinal = @kmFinal ,
                            ltConbustible = @ltCombustible ,
                            ltCombustibleUtilizado = @ltCombustibleUtilizado ,
                            ltCombustiblePTO = @ltCombustiblePTO ,
                            ltCombustibleEST = @ltCombustibleEST ,
                            fechaCombustible = @fechaCombustible ,
                            fechaFin = @fechaFin ,
                            tiempoTotal = @tiempoTotal ,
                            tiempoPTO = @tiempoPTO ,
                            tiempoEST = @tiempoEST ,
                            tipoOperacion = @tipoMovimiento ,
                            idDespachador = @idDespachador ,
                            sello1 = @sello1 ,
                            sello2 = @sello2 ,
                            selloRes = @selloRes
                    WHERE   idSalidaEntrada = @id;
                    SET @identificador = @id;
                END;
                
                
            COMMIT TRAN guardar;
            SET @valor = 0;
            SET @mensaje = 'Operación realizada con exito';
            
        END TRY
        BEGIN CATCH
            ROLLBACK TRAN guardar;
            SET @identificador = 0;
            SET @valor = 1;
            SET @mensaje = ERROR_MESSAGE();
        END CATCH;
    END;
