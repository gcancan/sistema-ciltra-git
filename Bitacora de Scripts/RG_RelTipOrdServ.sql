USE [DBFleteraPrueba]
GO
/****** Object:  Table [dbo].[RelTipOrdServ]    Script Date: 09/23/2016 12:27:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RelTipOrdServ](
	[idTipOrdServ] [int] NOT NULL,
	[NomCatalogo] [nvarchar](50) NOT NULL,
	[IdRelacion] [int] NOT NULL,
 CONSTRAINT [PK_RelTipOrdServ] PRIMARY KEY CLUSTERED 
(
	[idTipOrdServ] ASC,
	[NomCatalogo] ASC,
	[IdRelacion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[RelTipOrdServ] ([idTipOrdServ], [NomCatalogo], [IdRelacion]) VALUES (1, N'CatServicios', 3)
