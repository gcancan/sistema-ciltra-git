
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
-- =============================================
-- Author:		GRCC
-- Create date: 12/12/16
-- Description:	
-- =============================================
CREATE PROCEDURE getOrigenesAll 
	-- Add the parameters for the stored procedure here
    @idCliente INT = 0 ,
    @idOrigen INT = 0 ,
    @valor INT OUT ,
    @mensaje NVARCHAR(MAX) OUT
AS
    BEGIN	
        SET NOCOUNT ON;
        BEGIN TRY
            SELECT  O.idOrigen ,
                    O.nombreOrigen ,
                    O.estado ,
                    O.clasificacion ,
                    CO.idCliente
            FROM    dbo.ClienteOrigen AS CO
                    JOIN dbo.origen AS O ON O.idOrigen = CO.idOrigen
            WHERE   CO.idCliente LIKE CASE @idCliente
                                        WHEN 0 THEN '%'
                                        ELSE CAST(@idCliente AS NVARCHAR(MAX))
                                      END
                    AND O.idOrigen LIKE CASE @idOrigen
                                          WHEN 0 THEN '%'
                                          ELSE CAST(@idOrigen AS NVARCHAR(MAX))
                                        END;
            IF @@ROWCOUNT > 0
                BEGIN
                    SET @valor = 0;
                    SET @mensaje = 'SE ENCONTRARON REGISTROS';
                END;
            ELSE
                BEGIN 
                    SET @valor = 3;
                    SET @mensaje = 'NO SE ENCONTRARON REGISTROS';
                END;
        END TRY
        BEGIN CATCH
            SET @valor = 1;
            SET @mensaje = ERROR_MESSAGE();
        END CATCH;
    END;
GO
