
GO

/****** Object:  View [dbo].[destinosAtlante_vw]    Script Date: 12/13/2016 11:33:29 ******/
SET ANSI_NULLS ON;
GO

SET QUOTED_IDENTIFIER ON;
GO

ALTER VIEW [dbo].[destinosAtlante_vw]
AS
    SELECT DISTINCT
			o.idOrigen,
            p.Clave AS destino_clave ,
            p.Clave_empresa AS destino_empresa_clave ,
            p.Descripcion AS destino_descripcion ,
            p.Activo AS destino_activo ,
            OD.precioSencillo AS destino_costoSencillo ,
            destino_costo_sencillo35 = 0.00,
            OD.precioFull AS destino_costoFull ,
            OD.km AS destino_km ,
            '' AS destino_claveZona ,
            co.idCliente AS destino_cliente_clave ,
            p.claveExtra AS destino_claveExtra ,
            p.correo AS destino_correo ,
            estado.estado_idEstado ,
            estado.estado_NombreEstado ,
            estado.estado_idPais ,
            estado.estado_pais ,
            cli.cliente_idCliente ,
            cli.cliente_nombre ,
            cli.cliente_Domicilio ,
            cli.cliente_rfc ,
            cli.cliente_activo ,
            cli.cliente_claveFraccion ,
            cli.cliente_estado ,
            cli.cliente_pais ,
            cli.cliente_municipio ,
            cli.cliente_colonia ,
            cli.cliente_cp ,
            cli.cliente_telefono ,
            cli.impuesto_idImpuesto ,
            cli.impuesto_idEmpresa ,
            cli.impuesto_descripcion ,
            cli.impuesto_valor ,
            cli.impuesto_activo ,
            cli.impuestoFlete_idImpuesto ,
            cli.impuestoFlete_idEmpresa ,
            cli.impuestoFlete_Descripcion ,
            cli.impuestoFlete_valor ,
            cli.impuestoFlete_activo ,
            cli.empresa_idEmpresa ,
            cli.empresa_nombre ,
            cli.empresa_direccion ,
            cli.empresa_telefono ,
            cli.empresa_fax ,
            cli.empresa_estado ,
            cli.empresa_pais ,
            cli.empresa_municipio ,
            cli.empresa_colonia ,
            cli.empresa_cp ,
            cli.empresa_rfc
    FROM    dbo.origenDestinos AS OD
            INNER JOIN dbo.trafico_plazas_cat AS p ON OD.idDestino = p.Clave
            INNER JOIN dbo.origen AS o ON o.idOrigen = OD.idOrigen
            INNER JOIN dbo.ClienteOrigen AS co ON co.idOrigen = OD.idOrigen
            INNER JOIN dbo.cliente_vw AS cli ON cli.cliente_idCliente = co.idCliente
            INNER JOIN dbo.estado_vw AS estado ON estado.estado_idEstado = p.idEstado;

GO


