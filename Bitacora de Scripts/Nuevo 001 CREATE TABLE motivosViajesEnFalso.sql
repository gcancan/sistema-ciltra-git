CREATE TABLE motivosViajesEnFalso
    (
      idMotivo INT PRIMARY KEY
                   IDENTITY(1, 1) ,
      motivo NVARCHAR(100) NOT NULL
                           DEFAULT '' ,
      imputable INT NOT NULL
                    DEFAULT 2
    );
    
INSERT  INTO dbo.motivosViajesEnFalso
        ( motivo, imputable )
VALUES  ( N'Error en la programaci�n', -- motivo - nvarchar(100)
          0  -- imputable - int
          ),
        ( N'Falta de capacidad en granja', -- motivo - nvarchar(100)
          0  -- imputable - int
          ),
        ( N'Error de b�scula en granja', -- motivo - nvarchar(100)
          0  -- imputable - int
          ),
        ( N'Falsa alarma de robo', -- motivo - nvarchar(100)
          0  -- imputable - int
          ),
        ( N'Fuera de horario (por cargar tarde)', -- motivo - nvarchar(100)
          0  -- imputable - int
          );
INSERT  INTO dbo.motivosViajesEnFalso
        ( motivo, imputable )
VALUES  ( N'Falla mec�nica', -- motivo - nvarchar(100)
          1  -- imputable - int
          ),
        ( N'Confusi�n de granja ', -- motivo - nvarchar(100)
          1  -- imputable - int
          ),
        ( N'Fuera de horario', -- motivo - nvarchar(100)
          1  -- imputable - int
          );
INSERT  INTO dbo.motivosViajesEnFalso
        ( motivo, imputable )
VALUES  ( N'Robo de alimento del fletero', -- motivo - nvarchar(100)
          2  -- imputable - int
          ),
        ( N'Siniestro de la unidad', -- motivo - nvarchar(100)
          2  -- imputable - int
          ),
        ( N'Mezcla de alimento', -- motivo - nvarchar(100)
          2  -- imputable - int
          ),
        ( N'Descarga en  granja incorrecta', -- motivo - nvarchar(100)
          2  -- imputable - int
          ),
        ( N'Env�o de tractor para rescate', -- motivo - nvarchar(100)
          2  -- imputable - int
          );