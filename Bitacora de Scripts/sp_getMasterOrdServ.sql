
GO
/****** Object:  StoredProcedure [dbo].[sp_getMasterOrdServ]    Script Date: 10/10/2016 17:17:16 ******/
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
-- =============================================
-- Author:		<Author,GRCC,Name>
-- Create date: <Create Date,2016/09/21,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_getMasterOrdServ]
    @folio INT ,
    @valor INT OUT ,
    @mensaje NVARCHAR(MAX) OUT
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
        BEGIN TRY
        
            --IF EXISTS ( SELECT  *
            --            FROM    dbo.salidasEntradas
            --            WHERE   idPadreOrdSer = @folio )
            --    BEGIN
            --        SET @valor = 2;
            --        SET @mensaje = 'Ya se registro la entrada para este folio';
            --        RETURN -1
            --    END;
        
            SELECT  * ,
                    p.*
            FROM    dbo.MasterOrdServ AS m
                    JOIN dbo.personal_vw AS p ON m.idChofer = p.persona_idPersonal
            WHERE   idPadreOrdSer = @folio;
            
            IF @@ROWCOUNT > 0
                BEGIN
                    SET @valor = 0;
                    SET @mensaje = 'Se encontro registros';
                END;
            ELSE
                BEGIN
                    SET @valor = 3;
                    SET @mensaje = 'La búsqueda no tuvo resultados';
                END;
        END TRY
        BEGIN CATCH
            SET @valor = 1;
            SET @mensaje = ERROR_MESSAGE();
        END CATCH;
    END;
