CREATE TABLE origen
    (
      idOrigen INT PRIMARY KEY
                   IDENTITY(1, 1) ,
      idCliente INT NOT NULL ,
      nombreOrigen NVARCHAR(100) NOT NULL ,
      estado NVARCHAR(30) NOT NULL ,
      clasificacion NVARCHAR(100) NOT NULL
    );