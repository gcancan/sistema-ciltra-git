
GO
/****** Object:  StoredProcedure [dbo].[sp_saveRutasDestino]    Script Date: 11/14/2016 15:56:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,GRCC,Name>
-- Create date: <Create Date, 20/04/2016,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[sp_saveRutasDestino]
    @claveEmpresa NVARCHAR(10) ,
    @descripcion NVARCHAR(80) ,
    @clave INT ,
    @claveCliente INT ,
    @precioSencillo DECIMAL(18, 4) ,
    @costoSencillo35 DECIMAL(18, 4) ,
    @precioFull DECIMAL(18, 4) ,
    @km DECIMAL(18, 4) ,
    @valor INT OUT ,
    @claveExtra INT ,
    @correo NVARCHAR(60) ,
    @idEstado INT ,
    @mensaje NVARCHAR(MAX) OUT ,
    @identity INT OUT
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	
        SET NOCOUNT ON;
        BEGIN TRAN guardar;
        BEGIN TRY
            
            IF EXISTS ( SELECT  *
                        FROM    dbo.trafico_plazas_cat
                        WHERE   Descripcion = @descripcion
                                AND @clave = 0 )
                BEGIN                
                    SET @valor = 2;
                    SET @mensaje = 'El destino que desea ingresar ya existe';
                    SET @identity = 0;
                END;
            ELSE
                BEGIN                    
                    IF @clave = 0
                        BEGIN
		/*----------------------------------------------------*/
                            INSERT  INTO dbo.trafico_plazas_cat
                                    ( Clave_empresa ,
                                      Descripcion ,
                                      Activo ,
                                      --km ,
                                      claveExtra ,
                                      idEstado ,
                                      correo
                                    )
                            VALUES  ( @claveEmpresa , -- Clave_empresa - int
                                      @descripcion , -- Descripcion - varchar(100)
                                      1 ,  -- Activo - bit,
                                      --@km ,
                                      @claveExtra ,
                                      @idEstado ,
                                      @correo
                                    );
                            SET @identity = @@IDENTITY;
                            INSERT  INTO dbo.trafico_clientes_plazas
                                    ( Clave_empresa ,
                                      Clave_cliente ,
                                      Clave_plaza ,
                                      Direccion_plaza ,
                                      Precio_sencillo ,
                                      Precio_sencillo35 ,
                                      Precio_full ,
                                      km
                                    )
                            VALUES  ( @claveEmpresa , -- Clave_empresa - int
                                      @claveCliente , -- Clave_cliente - int
                                      @identity , -- Clave_plaza - int
                                      '' , -- Direccion_plaza - text
                                      @precioSencillo , -- Precio_sencillo - decimal
                                      @costoSencillo35 ,
                                      @precioFull, -- Precio_full - decimal
                                      @km
                                    );
		/*----------------------------------------------------*/
                        END;
                    ELSE
                        BEGIN
                            UPDATE  dbo.trafico_plazas_cat
                            SET     Clave_empresa = @claveEmpresa ,
                                    Descripcion = @descripcion ,
                                    --km = @km ,
                                    correo = @correo ,
                                    claveExtra = @claveExtra ,
                                    idEstado = @idEstado
                            WHERE   Clave = @clave;
                    
                            UPDATE  dbo.trafico_clientes_plazas
                            SET     Precio_sencillo = @precioSencillo ,
                                    Precio_sencillo35 = @costoSencillo35 ,
                                    Precio_full = @precioFull,
                                    km = @km
                            WHERE   Clave_plaza = @clave AND Clave_cliente = @claveCliente;
                            SET @identity = @clave;
                        END;
            
                    
                    SET @valor = 0;
                    SET @mensaje = 'Los Cambios se guardaron correctamente';
                END;
            COMMIT TRAN guardar; 
        END TRY
        BEGIN CATCH
            ROLLBACK TRAN guardar;
            SET @valor = 1;
            SET @mensaje = ERROR_MESSAGE();
        END CATCH;

    END;
