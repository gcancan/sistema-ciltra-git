
GO
/****** Object:  StoredProcedure [dbo].[sp_getCartaPorteByFecha]    Script Date: 10/20/2016 12:54:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,GRCC,Name>
-- Create date: <Create Date,06/04/2016,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[sp_getCartaPorteByFecha]
    @valor INT OUT ,
    @mensaje NVARCHAR(MAX) OUT ,
    @fechaInicio DATETIME ,
    @fechaFin DATETIME
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
        BEGIN TRY
            SELECT  NumGuiaId ,
                    SerieGuia ,
                    NumViaje ,
                    --idCliente ,
                    IdEmpresa ,
                    FechaHoraViaje ,
                    idOperador ,
                    idTractor ,
                    idRemolque1 ,
                    ISNULL(idDolly, '') AS idDolly ,
                    ISNULL(idRemolque2, '') AS idRemolque2 ,
                    FechaHoraFin ,
                    EstatusGuia ,
                    tara ,
                    TipoViaje ,
                    c.*,
                    per.*,
                    km
            FROM    dbo.CabGuia
                    JOIN dbo.cliente_vw AS c ON c.cliente_idCliente = idCliente
                    JOIN dbo.personal_vw AS per ON dbo.CabGuia.idOperador = per.persona_idPersonal
            WHERE   FechaHoraViaje BETWEEN @fechaInicio AND @fechaFin
                    AND EstatusGuia = 'ACTIVO'--  <> 'INICIADO'
            ORDER BY NumGuiaId DESC;
            
            IF @@ROWCOUNT <> 0
                BEGIN
                    SET @valor = 0;
                    SET @mensaje = 'Se encontro registros';
                END;
            ELSE
                BEGIN
                    SET @valor = 3;
                    SET @mensaje = 'No se encontraron registros';
                END; 
        END TRY
        BEGIN CATCH
            SET @valor = 1;
            SET @mensaje = ERROR_MESSAGE();
        END CATCH;
    END;
