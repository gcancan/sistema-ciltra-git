
GO
/****** Object:  StoredProcedure [dbo].[sp_getZonasALLorById]    Script Date: 11/16/2016 10:18:33 ******/
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
-- =============================================
-- Author:		<Author,GRCC,Name>
-- Create date: <Create Date,11/03/2016,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[sp_getZonasALLorById]
    @valor INT OUT ,
    @mensaje NVARCHAR(MAX) OUT ,
    @id INT ,
    @idCliente NVARCHAR(10) = '%'
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
        BEGIN TRY
            IF @id = 0
                BEGIN 
                    SELECT  *
                    FROM    dbo.destinos_vw AS d
                            --JOIN dbo.cliente_vw AS c ON d.destino_cliente_clave = c.cliente_idCliente
                    WHERE   destino_cliente_clave LIKE @idCliente
                            AND destino_activo = 1
                    ORDER BY destino_descripcion ASC;
                END;
            ELSE
                BEGIN  
                    SELECT  *
                    FROM    dbo.destinos_vw AS d
                            --JOIN dbo.cliente_vw AS c ON d.destino_cliente_clave = c.cliente_idCliente
                    WHERE   destino_cliente_clave LIKE @idCliente
                            AND destino_clave = @id
                    ORDER BY destino_descripcion ASC;
                END;
            IF @@ROWCOUNT <> 0
                BEGIN
                    SET @valor = 0;
                    SET @mensaje = 'Se encontro registros';
                END;
            ELSE
                BEGIN
                    SET @valor = 3;
                    SET @mensaje = 'No se encontraron registros';
                END; 
        END TRY
        BEGIN CATCH
            SET @valor = 1;
            SET @mensaje = ERROR_MESSAGE();
        END CATCH;
    END;
