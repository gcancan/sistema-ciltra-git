
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
-- =============================================
-- Author:		GRCC
-- Create date: 29/12/2016
-- Description:	
-- =============================================
CREATE PROCEDURE sp_checarViaje
    @valor INT OUT ,
    @mensaje NVARCHAR(MAX) OUT ,
    @idUsuario INT ,
    @fecha DATETIME ,
    @id INT
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
        BEGIN TRAN act; 
        BEGIN TRY
            UPDATE  dbo.CabGuia
            SET     EstatusGuia = 'CHECADO' ,
                    idUsuarioCheco = @idUsuario ,
                    fechaChecado = @fecha
            WHERE   NumGuiaId = @id;
            SET @valor = 0;
            SET @mensaje = 'Viaje checado con exito';
            COMMIT TRAN act;
        END TRY
        BEGIN CATCH
            SET @valor = 1;
            SET @mensaje = ERROR_MESSAGE();
            ROLLBACK TRAN act;
        END CATCH;
    END;
GO
