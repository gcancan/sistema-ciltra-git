USE [DBFleteraPrueba];
GO

/****** Object:  View [dbo].[AllUnidadesTransportes_vw]    Script Date: 10/04/2016 11:07:07 ******/
SET ANSI_NULLS ON;
GO

SET QUOTED_IDENTIFIER ON;
GO



ALTER VIEW [dbo].[AllUnidadesTransportes_vw]
AS
    SELECT  UnidadTransporte.idUnidadTrans AS UnidadTransporte_id_UnidadTrans ,
            UnidadTransporte.descripcionUni AS UnidadTransporte_descripcionUni ,
            UnidadTransporte.Estatus AS UnidadTransporte_Estatus ,
            tipoUnidad.idTipoUniTras AS tipoUnidad_idTipoUniTras ,
            tipoUnidad.nomTipoUniTras AS tipoUnidad_nomTipoUniTras ,
            tipoUnidad.NoLlantas AS tipoUnidad_NoLlantas ,
            tipoUnidad.NoEjes AS tipoUnidad_NoEjes ,
            ISNULL(tipoUnidad.TonelajeMax, 0) AS tipoUnidad_TonelajeMax ,
            tipoUnidad.bCombustible AS tipoUnidad_bCombustible,
            empresa.idEmpresa AS empresa_idEmpresa ,
            empresa.RazonSocial AS empresa_RazonSocial ,
            empresa.Representante AS empresa_Representante ,
            empresa.CIDEMPRESA_COM AS empresa_CIDEMPRESA_COM ,
            empresa.Direccion AS empresa_Direccion ,
            empresa.idColonia AS empresa_idColonia ,
            empresa.Telefono AS empresa_Telefono ,
            empresa.Fax AS empresa_Fax ,
            empresa.MovilRepresentante AS empresa_MovilRepresentante ,
            marcas.idMarca AS marcas_idMarca ,
            marcas.NombreMarca AS marcas_NombreMarca ,
            sucursal.id_sucursal AS sucursal_id_sucursal ,
            sucursal.NomSucursal AS sucursal_NomSucursal ,
            sucursal.id_empresa AS sucursal_id_empresa
    FROM    dbo.CatUnidadTrans AS UnidadTransporte
            INNER JOIN dbo.CatTipoUniTrans AS tipoUnidad ON tipoUnidad.idTipoUniTras = UnidadTransporte.idTipoUnidad
            INNER JOIN dbo.CatEmpresas AS empresa ON empresa.idEmpresa = UnidadTransporte.idEmpresa
            INNER JOIN dbo.CatMarcas AS marcas ON UnidadTransporte.idMarca = marcas.idMarca
            INNER JOIN dbo.CatSucursal AS sucursal ON sucursal.id_sucursal = UnidadTransporte.idSucursal;



GO


