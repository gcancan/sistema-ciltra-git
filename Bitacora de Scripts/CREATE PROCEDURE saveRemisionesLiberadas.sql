--DROP PROCEDURE saveRemisionesLiberadas
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
-- =============================================
-- Author:		GRCC
-- Create date: 28/11/2016
-- Description:	SP para guardar las remisiones restauradas
-- =============================================
CREATE PROCEDURE sp_saveRemisionesLiberadas
	-- Add the parameters for the stored procedure here
    @valor INT OUT ,
    @mensaje NVARCHAR(MAX) OUT ,
    @xml XML
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
        BEGIN TRAN guardar;
        BEGIN TRY
        
            DECLARE @DocHandle INT;
            DECLARE @tableRemisiones TABLE
                (
                  remision INT ,
                  idCliente INT
                );
            EXEC sp_xml_preparedocument @DocHandle OUTPUT, @xml;
            INSERT  INTO @tableRemisiones
                    ( remision ,
                      idCliente
                    )
                    SELECT  remision ,
                            idCliente
                    FROM    OPENXML (@DocHandle, '/RemisionesLiberadas/Detalles', 2)
			WITH (
                  remision INT, idCliente INT ); 
            EXEC sp_xml_removedocument @DocHandle;
            
            INSERT  INTO dbo.remisionesLiberadas
                    ( idCliente ,
                      remision ,
                      fecha ,
                      activo
                    )
                    SELECT  idCliente ,
                            remision ,
                            GETDATE() ,
                            1
                    FROM    @tableRemisiones;
            SET @valor = 0;
            SET @mensaje = 'Remisiones liberadas con exito';
            COMMIT TRAN guardar;
        END TRY
        BEGIN CATCH
            SET @valor = 1;
            SET @mensaje = ERROR_MESSAGE();
            ROLLBACK TRAN guardar;
        END CATCH;
    END;
GO
