
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
-- =============================================
-- Author:		GRCC
-- Create date: 12/10/2016
-- Description:	sp para traer las cp por id carga gasolina
-- =============================================
CREATE PROCEDURE sp_getCartaPortesByIdCargaGas 
	-- Add the parameters for the stored procedure here
    @valor INT OUT ,
    @mensaje NVARCHAR(MAX) OUT ,
    @idCargaGas INT
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
        BEGIN TRY
            SELECT  Consecutivo ,
                    NumGuiaId ,
                    SerieGuia ,
                    NoRemision ,
                    IdProdTraf ,
                    IdPlazaTraf ,
                    VolDescarga ,
                    precio ,
                    PorcIVA ,
                    ImpIVA ,
                    PorcRetencion ,
                    ImpRetencion ,
                    idTarea ,
                    subTotal ,
                    tara ,
                    horaLlegada ,
                    horaCaptura ,
                    horaImpresion ,
                    pesoBruto
            FROM    dbo.DetGuia
            WHERE   idCargaGasolina = @idCargaGas
            IF @@ROWCOUNT <> 0
                BEGIN
                    SET @valor = 0;
                    SET @mensaje = 'Se encontro registros';
                END;
            ELSE
                BEGIN
                    SET @valor = 3;
                    SET @mensaje = 'No se encontraron registros';
                END; 
        END TRY
        BEGIN CATCH
            SET @valor = 1;
            SET @mensaje = ERROR_MESSAGE();
        END CATCH;

    END;
GO
