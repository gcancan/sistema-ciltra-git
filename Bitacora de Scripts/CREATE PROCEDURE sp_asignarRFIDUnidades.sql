
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
-- =============================================
-- Author:		GRCC
-- Create date: 24/11/2016
-- Description:	asignar rfid a unidades
-- =============================================
CREATE PROCEDURE sp_asignarRFIDUnidades 
	-- Add the parameters for the stored procedure here
    @valor INT OUT ,
    @mensaje NVARCHAR(MAX) OUT ,
    @rfid NVARCHAR(MAX) ,
    @clave NVARCHAR(20)
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN TRAN actualizar;
        BEGIN TRY	
            IF EXISTS ( SELECT  *
                        FROM    dbo.CatUnidadTrans
                        WHERE   rfid = @rfid )
                BEGIN
                    SET @valor = 2;
                    SET @mensaje = 'Este RFID ya esta asignado a una unidad';
                    ROLLBACK TRAN actualizar;
                    RETURN;
                END;
            IF EXISTS ( SELECT  *
                        FROM    dbo.CatPersonal
                        WHERE   rfid = @rfid )
                BEGIN
                    SET @valor = 2;
                    SET @mensaje = 'Este RFID ya esta asignado a un Operador o Personal';
                    ROLLBACK TRAN actualizar;
                    RETURN;
                END;
                
            UPDATE  dbo.CatUnidadTrans
            SET     rfid = @rfid
            WHERE   idUnidadTrans = @clave;
            
            SET @valor = 0;
            SET @mensaje = 'RFID asignado con Exito';
            COMMIT TRAN actualizar;
        END TRY
        BEGIN CATCH
            SET @valor = 1;
            SET @mensaje = ERROR_MESSAGE();
            ROLLBACK TRAN actualizar;
        END CATCH;
    END;
GO
