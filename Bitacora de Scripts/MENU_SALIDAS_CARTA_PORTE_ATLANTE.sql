INSERT  INTO dbo.menusAplicacion
        ( idPadre ,
          clave ,
          nombre ,
          descripcion
        )
VALUES  ( ( SELECT  idMenuAplicacion
            FROM    dbo.menusAplicacion
            WHERE   clave = 'MENU_SALIDAS'
          ) , -- idPadre - int
          N'MENU_SALIDAS_CARTA_PORTE_ATLANTE' , -- clave - nvarchar(40)
          N'VIAJES ATLANTE' , -- nombre - nvarchar(max)
          N'menu para la pantalla de carta portes de atlante'  -- descripcion - nvarchar(max)
        );
INSERT  INTO dbo.privilegios
        ( clave ,
          descripcion
        )
VALUES  ( N'MENU_SALIDAS_CARTA_PORTE_ATLANTE' , -- clave - nvarchar(max)
          N'privilegio para entrar al menu de CP de atlante'  -- descripcion - nvarchar(max)
        );
INSERT  INTO dbo.privilegiosUsuario
        ( idPrivilegio ,
          idUsuario
        )
VALUES  ( ( SELECT  idPrivilegio
            FROM    dbo.privilegios
            WHERE   clave = 'MENU_SALIDAS_CARTA_PORTE_ATLANTE'
          ) , -- idPrivilegio - int
          ( SELECT  idUsuario
            FROM    dbo.usuariosSys
            WHERE   nombreUsuario = 'admin'
          )  -- idUsuario - int
        );