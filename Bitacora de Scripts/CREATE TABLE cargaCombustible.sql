
CREATE TABLE cargaCombustible
    (
      idCargaCombustible INT PRIMARY KEY
                             IDENTITY(1, 1) ,
      idSalidaEntrada INT ,
      idOrdenServ INT NOT NULL ,
      fechaCombustible DATETIME NULL ,
      kmFinal DECIMAL(16, 4) NULL ,
      kmRecorridos DECIMAL(16, 4) NULL ,
      kmDespachador DECIMAL(16, 4) NULL ,
      ltCombustible DECIMAL(16, 4) NULL ,
      ltCombustibleUtilizado DECIMAL(16, 4) NULL ,
      ltCombustiblePTO DECIMAL(16, 4) NULL ,
      ltCombustibleEST DECIMAL(16, 4) NULL ,
      tiempoTotal NVARCHAR(15) ,
      tiempoPTO NVARCHAR(15) ,
      tiempoEST NVARCHAR(15) ,
      sello1 INT ,
      sello2 INT ,
      selloRes INT,
      idDespachador INT NULL
    );