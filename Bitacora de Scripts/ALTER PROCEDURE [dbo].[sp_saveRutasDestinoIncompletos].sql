
GO
/****** Object:  StoredProcedure [dbo].[sp_saveRutasDestinoIncompletos]    Script Date: 11/14/2016 15:52:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,GRCC,Name>
-- Create date: <Create Date, 20/04/2016,>
-- Description:	<Description,,>

-- =============================================
ALTER PROCEDURE [dbo].[sp_saveRutasDestinoIncompletos]
    @clave INT ,
    @precioSencillo DECIMAL(18, 4) ,
    @costoSencillo35 DECIMAL(18, 4) ,
    @precioFull DECIMAL(18, 4) ,
    @km DECIMAL(18, 4) ,
    @valor INT OUT ,
    @mensaje NVARCHAR(MAX) OUT
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
        BEGIN TRAN guardar;
        BEGIN TRY
            
            
            --UPDATE  dbo.trafico_plazas_cat
            --SET     km = @km
            --WHERE   Clave = @clave;
                    
            UPDATE  dbo.trafico_clientes_plazas
            SET     Precio_sencillo = @precioSencillo ,
                    Precio_sencillo35 = @costoSencillo35 ,
                    Precio_full = @precioFull,
                    km = @km
            WHERE   Clave_plaza = @clave;
            
            DECLARE @valor2 INT;
            DECLARE @mensaje2 NVARCHAR(MAX);
            EXEC dbo.sp_actualizarPreciosCartaPorte @id = @clave, -- int
                @valor = @valor2 OUTPUT, -- int
                @mensaje = @mensaje2 OUTPUT; -- nvarchar(max)
                
            IF @valor2 = 0
                BEGIN   
                    COMMIT TRAN guardar;
                    SET @valor = 0;
                    SET @mensaje = 'Los Cambios se guardaron correctamente';
                END;
            ELSE
                BEGIN
                    ROLLBACK;
                    SET @valor = @valor2;
                    SET @mensaje = @mensaje2;
                END;
            
            SELECT  p.Clave_empresa ,
                    Clave ,
                    Descripcion ,
                    Activo ,
                    costoSencillo = c.Precio_sencillo ,
                    Precio_sencillo35 = c.Precio_sencillo35 ,
                    costoFull = c.Precio_full ,
                    c.km ,
                    claveZona = ISNULL(( SELECT clave
                                         FROM   dbo.zonas
                                         WHERE  idCliente = c.Clave_cliente
                                                AND p.km BETWEEN kmMenor AND kmMayor
                                       ), '') ,
                    c.Clave_cliente
            FROM    trafico_plazas_cat AS p
                    JOIN dbo.trafico_clientes_plazas AS c ON p.Clave = c.Clave_plaza
            WHERE   Clave = @clave;
                
                
                
                
        END TRY
        BEGIN CATCH
            ROLLBACK;
            SET @valor = 1;
            SET @mensaje = ERROR_MESSAGE();
        END CATCH;

    END;
