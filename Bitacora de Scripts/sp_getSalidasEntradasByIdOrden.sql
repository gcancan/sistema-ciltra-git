
GO
/****** Object:  StoredProcedure [dbo].[sp_getSalidasEntradasByIdOrden]    Script Date: 10/13/2016 12:44:42 ******/
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
-- =============================================
-- Author:		<Author,GRCC,Name>
-- Create date: <Create Date,2016/09/22,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[sp_getSalidasEntradasByIdOrden]
    @valor INT OUT ,
    @mensaje NVARCHAR(MAX) OUT ,
    @folio INT
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
        BEGIN TRY	
            SELECT  idSalidaEntrada ,
                    idPadreOrdSer ,
                    idChofer ,
                    idUnidadTrans ,
                    fecha ,
                    fechaFin ,
                    tipoOperacion ,
                    --idUsuario = (SELECT * FROM dbo.cargaCombustible )
                    KM ,
                    idUsuario ,
                    p.persona_idPersonal ,
                    p.personal_nombre ,
                    p.personal_rfid ,
                    p.personal_ipPuesto ,
                    p.personal_estatus ,
                    p.empresa_idEmpresa ,
                    p.empresa_nombre ,
                    p.empresa_direccion ,
                    p.empresa_telefono ,
                    p.empresa_fax ,
                    p.empresa_estado ,
                    p.empresa_pais ,
                    p.empresa_municipio ,
                    p.empresa_colonia ,
                    p.empresa_cp ,
                    p.empresa_rfc
            FROM    dbo.salidasEntradas
                    LEFT JOIN dbo.personal_vw AS p ON p.persona_idPersonal = idChofer
            WHERE   idPadreOrdSer = @folio
                    --AND tipoOperacion = 1
            ORDER BY fecha DESC;
            
            IF @@ROWCOUNT > 0
                BEGIN
                    SET @valor = 0;
                    SET @mensaje = 'Se encontraron registros';
                END;
            ELSE
                BEGIN
                    SET @valor = 3;
                    SET @mensaje = 'No se encontro entrada registrada para ese folio ffff';
                END;
        END TRY
        BEGIN CATCH
            SET @valor = 1;
            SET @mensaje = ERROR_MESSAGE();
        END CATCH;
    END;
