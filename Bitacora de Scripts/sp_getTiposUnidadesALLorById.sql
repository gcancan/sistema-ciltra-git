
GO
/****** Object:  StoredProcedure [dbo].[sp_getTiposUnidadesALLorById]    Script Date: 10/04/2016 11:01:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,GRCC,Name>
-- Create date: <Create Date,11/03/2016,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[sp_getTiposUnidadesALLorById]
    @valor INT OUT ,
    @mensaje NVARCHAR(MAX) OUT ,
    @id INT
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
        BEGIN TRY
            IF @id = 0
                BEGIN 
                    SELECT  tipo.idTipoUniTras AS Clave ,
                            tipo.nomTipoUniTras AS Descripcion ,
                            tipo.NoEjes ,
                            tipo.NoLlantas ,
                            ISNULL(tipo.TonelajeMax, 0) AS TonelajeMax,
                            ISNULL(tipo.clasificacion, '') AS clasificacion,
                            tipo.bCombustible
                    FROM    dbo.CatTipoUniTrans AS tipo;
                END;
            ELSE
                BEGIN  
                    SELECT  tipo.idTipoUniTras AS Clave ,
                            tipo.nomTipoUniTras AS Descripcion ,
                            tipo.NoEjes ,
                            tipo.NoLlantas ,
                            ISNULL(tipo.TonelajeMax, 0) AS TonelajeMax,
                            ISNULL(tipo.clasificacion, '') AS clasificacion,
                            tipo.bCombustible
                    FROM    dbo.CatTipoUniTrans AS tipo
                    WHERE   tipo.idTipoUniTras = @id;
                END;
            IF @@ROWCOUNT <> 0
                BEGIN
                    SET @valor = 0;
                    SET @mensaje = 'Se encontro registros';
                END;
            ELSE
                BEGIN
                    SET @valor = 3;
                    SET @mensaje = 'No se encontraron registros';
                END; 
        END TRY
        BEGIN CATCH
            SET @valor = 1;
            SET @mensaje = ERROR_MESSAGE();
        END CATCH;
    END;
