
GO
/****** Object:  StoredProcedure [dbo].[sp_saveDetallesCarta]    Script Date: 11/28/2016 16:34:50 ******/
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
-- =============================================
-- Author:		<Author,GRCC,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[sp_saveDetallesCarta]
    @XML XML ,
    @id INT ,
    @valor INT OUT ,
    @mensaje NVARCHAR(MAX) OUT
AS
    BEGIN
	
        SET NOCOUNT ON;
        BEGIN TRAN detalle;
	
        BEGIN TRY
	--///////////////////////////////////DETALLES//////////////////////////////////////////////

            DECLARE @DocHandle INT;
            DECLARE @tableDetalles TABLE
                (
                  idTarea INT ,
                  Consecutivo INT ,
                  NumGuiaId INT ,
                  SerieGuia NVARCHAR(5) ,
                  NoRemision INT ,
                  IdProdTraf INT ,
                  idUnidadProd INT ,
                  IdPlazaTraf INT ,
                  VolDescarga DECIMAL(18, 4) ,
                  precio DECIMAL(18, 4) ,
                  PorcIVA DECIMAL(18, 4) ,
                  ImpIVA DECIMAL(18, 4) ,
                  PorcRetencion DECIMAL(18, 4) ,
                  ImpRetencion DECIMAL(18, 4) ,
                  subTotal DECIMAL(18, 4) ,
                  fCartaPorte INT ,
                  tara DECIMAL(18, 4) ,
                  horaLlegada DATETIME NULL ,
                  horaCaptura DATETIME NULL ,
                  horaImpresion DATETIME NULL ,
                  observaciones NVARCHAR(MAX) ,
                  pesoBruto DECIMAL(18, 4)
                );
                
/***********************************Metodos para leer el XML e insertar los detalles**************************/

            EXEC sp_xml_preparedocument @DocHandle OUTPUT, @XML;
            INSERT  INTO @tableDetalles
                    ( idTarea ,
                      Consecutivo ,
                      NumGuiaId ,
                      SerieGuia ,
                      NoRemision ,
                      IdProdTraf ,
                      idUnidadProd ,
                      IdPlazaTraf ,
                      VolDescarga ,
                      precio ,
                      PorcIVA ,
                      ImpIVA ,
                      PorcRetencion ,
                      ImpRetencion ,
                      subTotal ,
                      fCartaPorte ,
                      tara ,
                      horaLlegada ,
                      horaCaptura ,
                      horaImpresion ,
                      observaciones ,
                      pesoBruto 
                    )
                    SELECT  idTarea ,
                            Consecutivo ,
                            @id ,
                            SerieGuia ,
                            NoRemision ,
                            IdProdTraf ,
                            idUnidadProd ,
                            IdPlazaTraf ,
                            VolDescarga ,
                            precio ,
                            PorcIVA ,
                            ImpIVA ,
                            PorcRetencion ,
                            ImpRetencion ,
                            subTotal ,
                            fCartaPorte ,
                            tara ,
                            horaLlegada ,
                            horaCaptura ,
                            horaImpresion ,
                            observaciones ,
                            pesoBruto
                    FROM    OPENXML (@DocHandle, '/DetallesCartaPorte/Detalles', 2)
			WITH (idTarea INT, Consecutivo INT, NumGuiaId INT ,
                  SerieGuia NVARCHAR(5) ,
                  NoRemision INT ,
                  IdProdTraf INT ,
                  idUnidadProd INT ,
                  IdPlazaTraf INT ,
                  VolDescarga DECIMAL(18,4) ,
                  precio DECIMAL(18,4),
                  PorcIVA DECIMAL(18,4) ,
                  ImpIVA DECIMAL(18,4) ,
                  PorcRetencion DECIMAL(18,4) ,
                  ImpRetencion DECIMAL(18,4),
                  subTotal DECIMAL(18,4),
                  fCartaPorte INT,
                  tara  DECIMAL(18, 4),
                  horaLlegada DATETIME,
                  horaCaptura  DATETIME,
                  horaImpresion DATETIME,
                  observaciones NVARCHAR(MAX),
                  pesoBruto DECIMAL(18,4)); 
            EXEC sp_xml_removedocument @DocHandle;  
/*************************************************************************************************************/

----------------------------------Cursor---------------------------------------------------------------------
            DECLARE cursor_ CURSOR
            FOR
                SELECT  idTarea ,
                        Consecutivo ,
                        NumGuiaId ,
                        SerieGuia ,
                        NoRemision ,
                        IdProdTraf ,
                        idUnidadProd ,
                        IdPlazaTraf ,
                        VolDescarga ,
                        precio ,
                        PorcIVA ,
                        ImpIVA ,
                        PorcRetencion ,
                        ImpRetencion ,
                        subTotal ,
                        fCartaPorte ,
                        tara ,
                        horaLlegada ,
                        horaCaptura ,
                        horaImpresion ,
                        observaciones ,
                        pesoBruto
                FROM    @tableDetalles;
            DECLARE @idTarea INT;    
            DECLARE @Consecutivo INT;    
            DECLARE @NumGuiaId_ INT;
            DECLARE @SerieGuia_ NVARCHAR(5);
            DECLARE @NoRemision INT;
            DECLARE @IdProdTraf INT;
            DECLARE @idUnidadProd INT;
            DECLARE @IdPlazaTraf INT;
            DECLARE @VolDescarga DECIMAL(18, 4);
            DECLARE @precio DECIMAL(18, 4);
            DECLARE @PorcIVA DECIMAL(18, 4);
            DECLARE @ImpIVA DECIMAL(18, 4);
            DECLARE @PorcRetencion DECIMAL(18, 4);
            DECLARE @ImpRetencion DECIMAL(18, 4);
            DECLARE @subTotal DECIMAL(18, 4);
            DECLARE @fCartaPorte INT;
            DECLARE @tara DECIMAL(18, 4);
            DECLARE @horaLlegada DATETIME = NULL;
            DECLARE @horaCaptura DATETIME = NULL;
            DECLARE @horaImpresion DATETIME = NULL;
            DECLARE @observaciones NVARCHAR(MAX);
            DECLARE @pesoBruto DECIMAL(18, 4);
            OPEN cursor_;
            
            --DEALLOCATE cursor_;
            FETCH NEXT FROM cursor_ INTO @idTarea, @Consecutivo, @NumGuiaId_,
                @SerieGuia_, @NoRemision, @IdProdTraf, @idUnidadProd,
                @IdPlazaTraf, @VolDescarga, @precio, @PorcIVA, @ImpIVA,
                @PorcRetencion, @ImpRetencion, @subTotal, @fCartaPorte, @tara,
                @horaLlegada, @horaCaptura, @horaImpresion, @observaciones,
                @pesoBruto; 
                
            WHILE @@FETCH_STATUS = 0
                BEGIN
                    IF @Consecutivo = 0
                        BEGIN
                            INSERT  INTO dbo.DetGuia
                                    ( idTarea ,
                                      NumGuiaId ,
                                      SerieGuia ,
                                      NoRemision ,
                                      IdProdTraf ,
                                      --idUnidadProd ,
                                      IdPlazaTraf ,
                                      VolDescarga ,
                                      precio ,
                                      PorcIVA ,
                                      ImpIVA ,
                                      PorcRetencion ,
                                      ImpRetencion ,
                                      subTotal ,
                                      fCartaPorte ,
                                      tara ,
                                      horaLlegada ,
                                      horaCaptura ,
                                      horaImpresion ,
                                      observaciones ,
                                      pesoBruto
                                    )
                            VALUES  ( @idTarea ,
                                      @NumGuiaId_ ,
                                      @SerieGuia_ ,
                                      @NoRemision ,
                                      @IdProdTraf ,
                                      --@idUnidadProd ,
                                      @IdPlazaTraf ,
                                      @VolDescarga ,
                                      @precio ,
                                      @PorcIVA ,
                                      @ImpIVA ,
                                      @PorcRetencion ,
                                      @ImpRetencion ,
                                      @subTotal ,
                                      @fCartaPorte ,
                                      @tara ,
                                      CASE @horaLlegada
                                        WHEN '19000101 00:00:00.000' THEN NULL
                                        ELSE @horaLlegada
                                      END ,
                                      CASE @horaCaptura
                                        WHEN '19000101 00:00:00.000' THEN NULL
                                        ELSE @horaCaptura
                                      END ,
                                      @horaImpresion ,
                                      @observaciones ,
                                      @pesoBruto
                                    );
                        END;
                        
                    UPDATE  dbo.remisionesLiberadas
                    SET     activo = 0
                    WHERE   remision = @NoRemision
                            AND idCliente = ( SELECT    idCliente
                                              FROM      dbo.CabGuia
                                              WHERE     NumGuiaId = @id
                                            );
                    IF @idTarea <> 0
                        BEGIN
                            UPDATE  dbo.tareasProgramadas
                            SET     estatus = 0
                            WHERE   idTareaProgramada = @idTarea;
                        END;
                    FETCH NEXT FROM cursor_ INTO @idTarea, @Consecutivo,
                        @NumGuiaId_, @SerieGuia_, @NoRemision, @IdProdTraf,
                        @idUnidadProd, @IdPlazaTraf, @VolDescarga, @precio,
                        @PorcIVA, @ImpIVA, @PorcRetencion, @ImpRetencion,
                        @subTotal, @fCartaPorte, @tara, @horaLlegada,
                        @horaCaptura, @horaImpresion, @observaciones,
                        @pesoBruto;
                END;
            CLOSE cursor_;    
            DEALLOCATE cursor_;  
            
            COMMIT TRAN detalle;
            SET @valor = 0;
            SET @mensaje = 'Cambios guardados correctamente'; 
-------------------------------------------------------------------------------------------------------------

--//////////////////////////////////////////////////////////////////////////////////////////////////////
        END TRY
        BEGIN CATCH
            ROLLBACK TRAN detalle;
            DEALLOCATE cursor_; 
            SET @valor = 1;
            SET @mensaje = ERROR_MESSAGE();        
        END CATCH;
	
    END;
