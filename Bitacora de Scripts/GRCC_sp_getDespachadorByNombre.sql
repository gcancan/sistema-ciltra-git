
GO
/****** Object:  StoredProcedure [dbo].[sp_getChoferByNombre]    Script Date: 09/26/2016 15:34:07 ******/
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,GRCC,>
-- Description:	<Description,26/09/2016,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_getDespachadorByNombre]
    @nombre NVARCHAR(60) = '%',
    @valor INT OUT ,
    @mensaje NVARCHAR(MAX) OUT
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
        BEGIN TRY
            SELECT  *
            FROM    dbo.personal_vw
            WHERE   personal_ipPuesto IN (
                    SELECT  idPuesto
                    FROM    dbo.CatPuestos
                    WHERE   NomPuesto LIKE '%DESPACHADOR%' )
                    AND personal_estatus = 'ACT'
                    AND personal_nombre LIKE '%' + @nombre + '%'
            ORDER BY personal_nombre ASC;
        
            IF @@ROWCOUNT <> 0
                BEGIN
                    SET @valor = 0;
                    SET @mensaje = 'Se encontro registros';
                END;
            ELSE
                BEGIN
                    SET @valor = 3;
                    SET @mensaje = 'No se encontraron registros';
                END; 
        END TRY
        BEGIN CATCH
            SET @valor = 1;
            SET @mensaje = ERROR_MESSAGE();
        END CATCH;
    END;
