CREATE TABLE registrosKilometros
    (
      idRegistrosKilometraje INT PRIMARY KEY
                                 IDENTITY(1, 1) ,
      idUnidadTrans VARCHAR(10)
        FOREIGN KEY REFERENCES dbo.CatUnidadTrans ( idUnidadTrans )
        NOT NULL ,
      kmInicial DECIMAL(16, 4) NOT NULL,
      kmFinal DECIMAL(16, 4) NOT NULL
    );