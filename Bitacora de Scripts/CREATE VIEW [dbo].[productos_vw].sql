
GO

/****** Object:  View [dbo].[productos_vw]    Script Date: 11/17/2016 13:47:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[productos_vw]
AS
    SELECT  IdEmpresa AS Clave_empresa ,
            IdFraccion AS Clave_fraccion ,
            IdProducto AS Clave ,
            Descripcion ,
            Embalaje ,
            Clave_unidad_de_medida ,
            ISNULL(Clave_externo, '') AS Clave_externo,
            estatus
    FROM    dbo.CatProductos;


GO


