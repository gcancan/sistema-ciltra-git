
GO
/****** Object:  StoredProcedure [dbo].[sp_getDestinosIncompletosAllById]    Script Date: 11/14/2016 15:47:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,GRCC,Name>
-- Create date: <Create Date,11/03/2016,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[sp_getDestinosIncompletosAllById]
    @valor INT OUT ,
    @mensaje NVARCHAR(MAX) OUT ,
    @id INT ,
    @idCliente NVARCHAR(10) = '%'
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
        BEGIN TRY
            IF @id = 0
                BEGIN 
                    
                    SELECT  *
                    FROM    ( SELECT    p.Clave_empresa ,
                                        Clave ,
                                        Descripcion ,
                                        Activo ,
                                        costoSencillo = c.Precio_sencillo ,
                                        Precio_sencillo35 = c.Precio_sencillo35 ,
                                        costoFull = c.Precio_full ,
                                        c.km ,
                                        claveZona = ISNULL(( SELECT
                                                              clave
                                                             FROM
                                                              dbo.zonas
                                                             WHERE
                                                              idCliente = c.Clave_cliente
                                                              AND p.km BETWEEN kmMenor AND kmMayor
                                                           ), '') ,
                                        c.Clave_cliente
                              FROM      dbo.trafico_plazas_cat AS p
                                        JOIN dbo.trafico_clientes_plazas AS c ON c.Clave_plaza = p.Clave
                                                              AND c.Clave_cliente = @idCliente
                              WHERE     p.Clave <> 0
                                        AND c.Precio_sencillo < = 0
                              UNION ALL
                              SELECT    p.Clave_empresa ,
                                        Clave ,
                                        Descripcion ,
                                        Activo ,
                                        costoSencillo = c.Precio_sencillo ,
                                        Precio_sencillo35 = c.Precio_sencillo35 ,
                                        costoFull = c.Precio_full ,
                                        c.km ,
                                        claveZona = ISNULL(( SELECT
                                                              clave
                                                             FROM
                                                              dbo.zonas
                                                             WHERE
                                                              idCliente = c.Clave_cliente
                                                              AND p.km BETWEEN kmMenor AND kmMayor
                                                           ), '') ,
                                        c.Clave_cliente
                              FROM      dbo.trafico_plazas_cat AS p
                                        JOIN dbo.trafico_clientes_plazas AS c ON c.Clave_plaza = p.Clave
                                                              AND c.Clave_cliente = @idCliente
                              WHERE     p.Clave <> 0
                                        AND c.Precio_full < = 0
                              UNION ALL
                              SELECT    p.Clave_empresa ,
                                        Clave ,
                                        Descripcion ,
                                        Activo ,
                                        costoSencillo = c.Precio_sencillo ,
                                        Precio_sencillo35 = c.Precio_sencillo35 ,
                                        costoFull = c.Precio_full ,
                                        c.km ,
                                        claveZona = ISNULL(( SELECT
                                                              clave
                                                             FROM
                                                              dbo.zonas
                                                             WHERE
                                                              idCliente = c.Clave_cliente
                                                              AND p.km BETWEEN kmMenor AND kmMayor
                                                           ), '') ,
                                        c.Clave_cliente
                              FROM      dbo.trafico_plazas_cat AS p
                                        JOIN dbo.trafico_clientes_plazas AS c ON c.Clave_plaza = p.Clave
                                                              AND c.Clave_cliente = @idCliente
                              WHERE     p.Clave <> 0
                                        AND c.Precio_sencillo35 < = 0
                              UNION ALL
                              SELECT    p.Clave_empresa ,
                                        Clave ,
                                        Descripcion ,
                                        Activo ,
                                        costoSencillo = c.Precio_sencillo ,
                                        Precio_sencillo35 = c.Precio_sencillo35 ,
                                        costoFull = c.Precio_full ,
                                        c.km ,
                                        claveZona = ISNULL(( SELECT
                                                              clave
                                                             FROM
                                                              dbo.zonas
                                                             WHERE
                                                              idCliente = c.Clave_cliente
                                                              AND p.km BETWEEN kmMenor AND kmMayor
                                                           ), '') ,
                                        c.Clave_cliente
                              FROM      dbo.trafico_plazas_cat AS p
                                        JOIN dbo.trafico_clientes_plazas AS c ON c.Clave_plaza = p.Clave
                                                              AND c.Clave_cliente = @idCliente
                              WHERE     p.Clave <> 0
                                        AND p.km < = 0
                            ) AS todo
                    WHERE   todo.Activo = 1
                    GROUP BY todo.Clave_empresa ,
                            todo.Clave ,
                            todo.Descripcion ,
                            todo.km ,
                            todo.Activo ,
                            todo.costoSencillo ,
                            todo.Precio_sencillo35 ,
                            todo.costoFull ,
                            todo.claveZona,
                            todo.Clave_cliente
                    ORDER BY todo.Clave;
                END;
            ELSE
                BEGIN  
                    
                    SELECT  *
                    FROM    ( SELECT    p.Clave_empresa ,
                                        Clave ,
                                        Descripcion ,
                                        Activo ,
                                        costoSencillo = c.Precio_sencillo ,
                                        Precio_sencillo35 = c.Precio_sencillo35 ,
                                        costoFull = c.Precio_full ,
                                        c.km ,
                                        claveZona = ISNULL(( SELECT
                                                              clave
                                                             FROM
                                                              dbo.zonas
                                                             WHERE
                                                              idCliente = c.Clave_cliente
                                                              AND p.km BETWEEN kmMenor AND kmMayor
                                                           ), '') ,
                                        c.Clave_cliente
                              FROM      dbo.trafico_plazas_cat AS p
                                        JOIN dbo.trafico_clientes_plazas AS c ON c.Clave_plaza = p.Clave
                                                              AND c.Clave_cliente = @idCliente
                              WHERE     p.Clave <> 0
                                        AND c.Precio_sencillo < = 0
                              UNION ALL
                              SELECT    p.Clave_empresa ,
                                        Clave ,
                                        Descripcion ,
                                        Activo ,
                                        costoSencillo = c.Precio_sencillo ,
                                        Precio_sencillo35 = c.Precio_sencillo35 ,
                                        costoFull = c.Precio_full ,
                                        c.km ,
                                        claveZona = ISNULL(( SELECT
                                                              clave
                                                             FROM
                                                              dbo.zonas
                                                             WHERE
                                                              idCliente = c.Clave_cliente
                                                              AND p.km BETWEEN kmMenor AND kmMayor
                                                           ), '') ,
                                        c.Clave_cliente
                              FROM      dbo.trafico_plazas_cat AS p
                                        JOIN dbo.trafico_clientes_plazas AS c ON c.Clave_plaza = p.Clave
                                                              AND c.Clave_cliente = @idCliente
                              WHERE     p.Clave <> 0
                                        AND c.Precio_full < = 0
                              UNION ALL
                              SELECT    p.Clave_empresa ,
                                        Clave ,
                                        Descripcion ,
                                        Activo ,
                                        costoSencillo = c.Precio_sencillo ,
                                        Precio_sencillo35 = c.Precio_sencillo35 ,
                                        costoFull = c.Precio_full ,
                                        c.km ,
                                        claveZona = ISNULL(( SELECT
                                                              clave
                                                             FROM
                                                              dbo.zonas
                                                             WHERE
                                                              idCliente = c.Clave_cliente
                                                              AND p.km BETWEEN kmMenor AND kmMayor
                                                           ), '') ,
                                        c.Clave_cliente
                              FROM      dbo.trafico_plazas_cat AS p
                                        JOIN dbo.trafico_clientes_plazas AS c ON c.Clave_plaza = p.Clave
                                                              AND c.Clave_cliente = @idCliente
                              WHERE     p.Clave <> 0
                                        AND c.Precio_sencillo35 < = 0
                              UNION ALL
                              SELECT    p.Clave_empresa ,
                                        Clave ,
                                        Descripcion ,
                                        Activo ,
                                        costoSencillo = c.Precio_sencillo ,
                                        Precio_sencillo35 = c.Precio_sencillo35 ,
                                        costoFull = c.Precio_full ,
                                        c.km ,
                                        claveZona = ISNULL(( SELECT
                                                              clave
                                                             FROM
                                                              dbo.zonas
                                                             WHERE
                                                              idCliente = c.Clave_cliente
                                                              AND p.km BETWEEN kmMenor AND kmMayor
                                                           ), '') ,
                                        c.Clave_cliente
                              FROM      dbo.trafico_plazas_cat AS p
                                        JOIN dbo.trafico_clientes_plazas AS c ON c.Clave_plaza = p.Clave
                                                              AND c.Clave_cliente = @idCliente
                              WHERE     p.Clave <> 0
                                        AND p.km < = 0
                            ) AS todo
                    WHERE   todo.Activo = 1
                            AND todo.Clave = @id
                    GROUP BY todo.Clave_empresa ,
                            todo.Clave ,
                            todo.Descripcion ,
                            todo.km ,
                            todo.Activo ,
                            todo.costoSencillo ,
                            todo.Precio_sencillo35 ,
                            todo.costoFull ,
                            todo.claveZona ,
                            todo.Clave_cliente
                    ORDER BY todo.Clave;
                END;
            IF @@ROWCOUNT <> 0
                BEGIN
                    SET @valor = 0;
                    SET @mensaje = 'Se encontro registros';
                END;
            ELSE
                BEGIN
                    SET @valor = 3;
                    SET @mensaje = 'No se encontraron registros';
                END; 
        END TRY
        BEGIN CATCH
            SET @valor = 1;
            SET @mensaje = ERROR_MESSAGE();
        END CATCH;
    END;
