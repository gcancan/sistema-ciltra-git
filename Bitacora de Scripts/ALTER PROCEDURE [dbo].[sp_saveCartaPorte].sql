
GO
/****** Object:  StoredProcedure [dbo].[sp_saveCartaPorte]    Script Date: 12/29/2016 13:59:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author, GRCC ,Name>
-- Create date: <Create Date,15/03/2016,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[sp_saveCartaPorte]
    @NumGuiaId INT ,
    @SerieGuia NVARCHAR(5) ,
    @NumViaje INT = 2 ,
    @idCliente INT ,
    @IdEmpresa INT ,
    @idOperador INT ,
    @idTractor NVARCHAR(20) ,
    @idRemolque1 NVARCHAR(20) ,
    @idDolly NVARCHAR(20) = NULL ,
    @idRemolque2 NVARCHAR(20) = NULL ,
    @EstatusGuia NVARCHAR(20) ,
    @UserViaje INT ,
    @tipoViaje CHAR(1) ,
    @tara DECIMAL(18, 4) ,
    @km DECIMAL(18, 4) =0,
    @XML XML ,
    @FechaHoraViaje DATETIME ,
    @omitirRegraRemisiones BIT = 0 ,
    ----------------
    @valor INT OUT ,
    @mensaje NVARCHAR(MAX) OUT ,
    @identificador INT OUT
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
        
        IF @UserViaje = 10
            BEGIN
                SET @identificador = 0;
                SET @valor = 2;
                SET @mensaje = 'Este usuario se ha bloqueado para realizar esta accion.';
                RETURN -1;
            END;
        
        DECLARE @fechaG DATETIME; 
        DECLARE @v INT;
        DECLARE @men NVARCHAR(MAX);
        
        EXEC dbo.sp_consultarPrivilegio @idUsuario = @UserViaje, -- int
            @privilegio = N'APLICAR_FECHA_VIAJE', -- nvarchar(50)
            @valor = @v OUTPUT, -- int
            @mensaje = @men OUTPUT; -- nvarchar(max)
            
        IF @v = 0
            BEGIN
                SET @fechaG = @FechaHoraViaje;
            END;
        ELSE
            BEGIN
                SET @fechaG = GETDATE();
            END;
        
        IF @omitirRegraRemisiones = 0
            BEGIN
                DECLARE @e NVARCHAR(MAX); 
                EXEC @e= dbo.existRemision @xml = @XML, -- xml
                    @idCliente = @idCliente; -- int
         
                IF @e <> ''
                    BEGIN
                        SET @identificador = 0;
                        SET @valor = 2;
                        SET @mensaje = @e;
                        RETURN -1;
                    END;
            END;
        BEGIN TRAN guardar;
        BEGIN TRY            
            IF @NumGuiaId = 0
                BEGIN
                    INSERT  INTO dbo.CabGuia
                            ( SerieGuia ,
                              NumViaje ,
                              idCliente ,
                              IdEmpresa ,
                              FechaHoraViaje ,
                              idOperador ,
                              idTractor ,
                              idRemolque1 ,
                              idDolly ,
                              idRemolque2 ,
                              FechaHoraFin ,
                              EstatusGuia ,
                              UserViaje ,
                              TipoViaje ,
                              tara ,
                              km
			                )
                    VALUES  ( @SerieGuia , -- SerieGuia - varchar(3)
                              ( SELECT  ISNULL(MAX(NumViaje) + 1, 1)
                                FROM    dbo.CabGuia
                              ) , -- NumViaje - int
                              @idCliente , -- idCliente - int
                              @IdEmpresa , -- IdEmpresa - int
                              @fechaG , --GETDATE() , -- FechaHoraViaje - datetime
                              @idOperador , -- idOperador - int
                              @idTractor , -- idTractor - varchar(10)
                              @idRemolque1 , -- idRemolque1 - varchar(10)
                              @idDolly , -- idDolly - varchar(10)
                              @idRemolque2 , -- idRemolque2 - varchar(10)
                              NULL , -- FechaHoraFin - datetime
                              @EstatusGuia ,  -- EstatusGuia - varchar(6)
                              @UserViaje ,
                              @tipoViaje ,
                              @tara ,
                              @km                              
			                );
			              
                    SET @identificador = @@IDENTITY;
                    SET @valor = 0;
                    SET @mensaje = 'Se guardaron correctamente los cambios';
                END;
            ELSE
                BEGIN
                    --UPDATE  dbo.CabGuia
                    --SET     idCliente = @idCliente
                    --WHERE   NumGuiaId = @NumGuiaId;
                    
                    SET @identificador = @NumGuiaId;
                    SET @valor = 0;
                    SET @mensaje = 'Se actualizaron correctamente los cambios';
                END;
                
            DECLARE @valor_ INT;
            DECLARE @mensaje_ NVARCHAR(MAX);
            EXEC dbo.sp_saveDetallesCarta @XML = @XML, -- xml
                @id = @identificador, -- int
                @valor = @valor_ OUTPUT, -- int
                @mensaje = @mensaje_ OUTPUT; -- nvarchar(max)
            IF @valor_ = 0
                BEGIN
                    SET @mensaje = 'Se guardaron correctamente los cambios';
                    SET @valor = 0;
                END;
            ELSE
                BEGIN
                    SET @mensaje = @mensaje_;
                    SET @valor = 1;
                    ROLLBACK TRAN guardar;
                END;
                
            SELECT  NumGuiaId ,
                    SerieGuia ,
                    NumViaje ,
                    idCliente ,
                    IdEmpresa ,
                    FechaHoraViaje ,
                    idOperador ,
                    idTractor ,
                    idRemolque1 ,
                    ISNULL(idDolly, '') AS idDolly ,
                    ISNULL(idRemolque2, '') AS idRemolque2 ,
                    FechaHoraFin ,
                    EstatusGuia ,
                    UserViaje ,
                    TipoViaje ,
                    tara ,
                    km
            FROM    dbo.CabGuia
            WHERE   NumGuiaId = @identificador;
               
                
            --DECLARE @valor_2 INT;
            --DECLARE @mensaje_2 NVARCHAR(MAX);
            --IF ( SELECT penalizacion
            --     FROM   dbo.CatClientes
            --     WHERE  IdCliente = @idCliente
            --   ) > 0
            --    BEGIN
            --        EXEC dbo.sp_savePenalizacion @valor = @valor_2 OUTPUT, -- int
            --            @mensaje = @mensaje_2 OUTPUT, -- nvarchar(max)
            --            @idViaje = @identificador; -- int
            --        IF @valor_2 = 0
            --            BEGIN
            --                SET @mensaje = 'Se guardaron correctamente los cambios';
            --                SET @valor = 0;
                            
            --            END;
            --        ELSE
            --            BEGIN
            --                SET @mensaje = @mensaje_2;
            --                SET @valor = 1;
            --                ROLLBACK TRAN guardar;
            --            END;
                
            --    END;
            
			
            
			
            COMMIT TRAN guardar; 
        END TRY
        BEGIN CATCH
            SET @mensaje = ERROR_MESSAGE();
            SET @valor = 1;
            ROLLBACK TRAN guardar;
            --DEALLOCATE cursor_;
        END CATCH;

    END;
