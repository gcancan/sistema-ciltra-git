
GO
/****** Object:  StoredProcedure [dbo].[sp_getALLProductosByIdORidFraccion]    Script Date: 11/16/2016 12:46:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,GRCC,Name>
-- Create date: <Create Date,14/03/2016,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[sp_getALLProductosByIdORidFraccion]
    @valor INT OUT ,
    @mensaje NVARCHAR(MAX) OUT ,
    @id INT ,
    @idFraccion INT
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
        BEGIN TRY
        
            IF @idFraccion = 0
                BEGIN 
                    IF @id = 0
                        BEGIN 
                            SELECT  *
                            FROM    dbo.productos_vw AS p
                            WHERE   p.Clave <> 0
                                    AND p.estatus = 1
                            ORDER BY Descripcion ASC;
                        END;
                    ELSE
                        BEGIN  
                            SELECT  *
                            FROM    dbo.productos_vw AS p
                            WHERE   p.Clave = @id
                                    AND p.estatus = 1
                            ORDER BY Descripcion ASC;;
                        END;
                END;
            ELSE
                BEGIN               
                
                    IF @id = 0
                        BEGIN 
                            SELECT  *
                            FROM    dbo.productos_vw AS p
                            WHERE   p.Clave <> 0
                                    AND p.Clave_fraccion = @idFraccion
                                    AND p.estatus = 1
                                    ORDER BY Descripcion ASC;;
                        END;
                    ELSE
                        BEGIN  
                            SELECT  *
                            FROM    dbo.productos_vw AS p
                            WHERE   p.Clave_fraccion = @idFraccion
                                    AND p.Clave = @id
                                    ORDER BY Descripcion ASC;;
                        END;
                    --SELECT  Clave_empresa = IdEmpresa ,
                    --        Clave_fraccion = IdFraccion ,
                    --        Clave = IdProducto ,
                    --        Descripcion ,
                    --        Embalaje ,
                    --        Clave_unidad_de_medida ,
                    --        ISNULL(Clave_externo, '') AS Clave_externo
                    --FROM    dbo.CatProductos
                    --WHERE   IdFraccion = @idFraccion;
                END;               
        
            
            IF @@ROWCOUNT <> 0
                BEGIN
                    SET @valor = 0;
                    SET @mensaje = 'Se encontro registros';
                END;
            ELSE
                BEGIN
                    SET @valor = 3;
                    SET @mensaje = 'No se encontraron registros';
                END; 
        END TRY
        BEGIN CATCH
            SET @valor = 1;
            SET @mensaje = ERROR_MESSAGE();
        END CATCH;
    END;
