CREATE TABLE tipoMores
    (
      idTipoMotor INT PRIMARY KEY IDENTITY(1,1),
      tipoMotor NVARCHAR(50) NOT NULL ,
      porcTolerancia DECIMAL(16, 4) NOT NULL
                                    DEFAULT 0
    );