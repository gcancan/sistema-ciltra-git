
GO
/****** Object:  StoredProcedure [dbo].[sp_getCartaPorteByFiltros]    Script Date: 10/20/2016 12:52:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,GRCC,Name>
-- Create date: <Create Date,05/05/2016,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[sp_getCartaPorteByFiltros]
    @valor INT OUT ,
    @mensaje NVARCHAR(MAX) OUT ,
    @fechaInicio DATETIME ,
    @fechaFin DATETIME ,
    @idDestino NVARCHAR(10) ,
    @idOperador NVARCHAR(10) ,
    @idTractor NVARCHAR(10) ,
    @estatus NVARCHAR(20) ,
    @idcliente INT
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
        BEGIN TRY

            SELECT  DISTINCT
                    ( c.NumGuiaId ) ,
                    c.SerieGuia AS SerieGuia ,
                    NumViaje AS NumViaje ,
                    c.km,
                    c.idCliente AS idCliente ,
        --cli.Nombre AS cliente_nombre ,
        --cli.RFC AS cliente_RFC ,
        --cli.Domicilio AS cliente_domicilio ,
                    c.IdEmpresa AS IdEmpresa ,
                    FechaHoraViaje AS FechaHoraViaje ,
                    idTractor AS idTractor ,
                    idRemolque1 AS idRemolque1 ,
                    ISNULL(idDolly, '') AS idDolly ,
                    ISNULL(idRemolque2, '') AS idRemolque2 ,
                    FechaHoraFin AS FechaHoraFin ,
                    EstatusGuia AS EstatusGuia ,
                    TipoViaje AS tipoViaje ,
                    c.tara ,
                    c.TipoViaje ,
        /*---------------------------*/
                    idOperador AS idOperador ,
                    per.NombreCompleto AS operador_Nombre ,
                    per.rfid AS operador_rfid ,
                    perEmp.idEmpresa AS operador_Clave_empresa ,
                    perEmp.RazonSocial AS operador_empresa_nombre ,
                    perEmp.Telefono AS operador_empresa_telefono ,
                    perEmp.Direccion AS operador_empresa_direccion ,
                    perEmp.Fax AS operador_empresa_fax
        /*---------------------------*/
            FROM    dbo.CabGuia AS c
                    JOIN dbo.DetGuia AS d ON d.NumGuiaId = c.NumGuiaId
                    JOIN dbo.CatClientes AS cli ON cli.IdCliente = c.idCliente
                    JOIN dbo.CatPersonal AS per ON c.idOperador = per.idPersonal
                    JOIN dbo.CatEmpresas AS perEmp ON perEmp.idEmpresa = per.idEmpresa
            WHERE   FechaHoraViaje BETWEEN @fechaInicio AND @fechaFin
                    AND idOperador LIKE @idOperador
                    AND idTractor LIKE @idTractor
                    AND c.EstatusGuia LIKE @estatus
                    AND c.idCliente = @idcliente
                    AND d.IdPlazaTraf LIKE @idDestino
            ORDER BY c.NumGuiaId ASC;
            
            IF @@ROWCOUNT <> 0
                BEGIN
                    SET @valor = 0;
                    SET @mensaje = 'Se encontro registros';
                END;
            ELSE
                BEGIN
                    SET @valor = 3;
                    SET @mensaje = '*No se encontraron registros';
                END; 
        END TRY
        BEGIN CATCH
            SET @valor = 1;
            SET @mensaje = ERROR_MESSAGE();
        END CATCH;
    END;
