CREATE TABLE ClienteOrigen
    (
      idClienteDestino INT PRIMARY KEY
                           IDENTITY(1, 1) ,
      idCliente INT NOT NULL ,
      idOrigen INT FOREIGN KEY REFERENCES dbo.origen ( idOrigen )
    );