USE [DBFleteraPrueba]
GO
/****** Object:  Table [dbo].[MasterOrdServ]    Script Date: 09/23/2016 12:24:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MasterOrdServ](
	[idPadreOrdSer] [int] IDENTITY(2,1) NOT NULL,
	[idTractor] [nvarchar](50) NULL,
	[idTolva1] [nvarchar](50) NULL,
	[idDolly] [nvarchar](50) NULL,
	[idTolva2] [nvarchar](50) NULL,
	[idChofer] [int] NULL,
	[FechaCreacion] [smalldatetime] NULL,
	[UserCrea] [varchar](15) NULL,
 CONSTRAINT [PK_MasterOrdServ] PRIMARY KEY CLUSTERED 
(
	[idPadreOrdSer] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
