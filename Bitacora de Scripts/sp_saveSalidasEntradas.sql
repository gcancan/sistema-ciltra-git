
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE sp_saveSalidasEntradas
    @valor INT = 1 OUT ,
    @mensaje NVARCHAR(MAX) = '' OUT ,
    @xml XML
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
        BEGIN TRAN guardar;
        BEGIN TRY
        
            DECLARE @DocHandle INT;
            DECLARE @tablaSalidas TABLE
                (
                  idOperacion INT ,
                  idPadreOrdSer INT ,
                  idChofer INT ,
                  idUnidadTrans NVARCHAR(20) ,
                  fecha DATETIME ,
                  fechaFin DATETIME ,
                  tipoOperacion INT ,
                  KM DECIMAL(16, 4) ,
                  idUsuario INT
                );
            EXEC sp_xml_preparedocument @DocHandle OUTPUT, @xml;
            INSERT  INTO @tablaSalidas
                    ( idOperacion ,
                      idPadreOrdSer ,
                      idChofer ,
                      idUnidadTrans ,
                      fecha ,
                      fechaFin ,
                      tipoOperacion ,
                      KM ,
                      idUsuario
                    )
                    SELECT  idOperacion ,
                            idPadreOrdSer ,
                            idChofer ,
                            idUnidadTrans ,
                            fecha ,
                            fechaFin ,
                            tipoOperacion ,
                            KM ,
                            idUsuario
                    FROM    OPENXML (@DocHandle, '/listSalidasEntradas/salidasEntradas', 2)
			WITH (idOperacion INT, 
			idPadreOrdSer INT, 
			idChofer INT, 
			idUnidadTrans NVARCHAR(20), 
			fecha DATETIME, 
			fechaFin DATETIME, 
			tipoOperacion INT, 
			KM DECIMAL(16,4),
			idUsuario INT); 
            EXEC sp_xml_removedocument @DocHandle;
            
            DECLARE cursor_ CURSOR
            FOR
                SELECT  idOperacion ,
                        idPadreOrdSer ,
                        idChofer ,
                        idUnidadTrans ,
                        fecha ,
                        fechaFin ,
                        tipoOperacion ,
                        KM ,
                        idUsuario
                FROM    @tablaSalidas;         
            DECLARE @idOperacion INT;
            DECLARE @idPadreOrdSer INT;
            DECLARE @idChofer INT;
            DECLARE @idUnidadTrans NVARCHAR(20);
            DECLARE @fecha DATETIME;
            DECLARE @fechaFin DATETIME;
            DECLARE @tipoOperacion INT;
            DECLARE @KM DECIMAL(16, 4);
            DECLARE @idUsuario INT;
            
            OPEN cursor_;
            FETCH NEXT FROM cursor_ INTO @idOperacion, @idPadreOrdSer,
                @idChofer, @idUnidadTrans, @fecha, @fechaFin, @tipoOperacion,
                @KM, @idUsuario;
            WHILE @@FETCH_STATUS = 0
                BEGIN
                    
                    IF @idOperacion = 0
                        BEGIN
                            INSERT  INTO dbo.salidasEntradas
                                    ( idPadreOrdSer ,
                                      idChofer ,
                                      idUnidadTrans ,
                                      fecha ,
                                      fechaFin ,
                                      tipoOperacion ,
                                      KM ,
                                      idUsuario
                                    )
                            VALUES  ( @idPadreOrdSer , -- idPadreOrdSer - int
                                      @idChofer , -- idChofer - int
                                      @idUnidadTrans , -- idUnidadTrans - nvarchar(20)
                                      GETDATE() , -- fecha - datetime
                                      CASE @fechaFin
                                        WHEN '1900-01-01 00:00:00.000'
                                        THEN NULL
                                        ELSE @fechaFin
                                      END , -- fechaFin - datetime
                                      @tipoOperacion , -- tipoOperacion - int
                                      @KM , -- KM - decimal
                                      @idUsuario
                                    );
                                    
                            IF EXISTS ( SELECT  *
                                        FROM    dbo.CabOrdenServicio
                                        WHERE   idPadreOrdSer = @idPadreOrdSer
                                                AND idUnidadTrans = @idUnidadTrans
                                                AND idTipoOrden = 1 )
                                BEGIN 
                                    INSERT  INTO dbo.cargaCombustible
                                            ( idSalidaEntrada ,
                                              idOrdenServ ,
                                              fechaCombustible ,
                                              kmFinal ,
                                              kmRecorridos ,
                                              kmDespachador ,
                                              ltCombustible ,
                                              ltCombustibleUtilizado ,
                                              ltCombustiblePTO ,
                                              ltCombustibleEST ,
                                              tiempoTotal ,
                                              tiempoPTO ,
                                              tiempoEST ,
                                              sello1 ,
                                              sello2 ,
                                              selloRes
                                            )
                                    VALUES  ( @@IDENTITY , -- idSalidaEntrada - int
                                              ( SELECT TOP 1
                                                        idOrdenSer
                                                FROM    dbo.CabOrdenServicio
                                                WHERE   idPadreOrdSer = @idPadreOrdSer
                                                        AND idUnidadTrans = @idUnidadTrans
                                                        AND idTipoOrden = 1
                                              ) , -- idOrdenServ - int
                                              NULL , -- fechaCombustible - datetime
                                              0 , -- kmFinal - decimal
                                              0 , -- kmRecorridos - decimal
                                              0 , -- kmDespachador - decimal
                                              0 , -- ltCombustible - decimal
                                              0 , -- ltCombustibleUtilizado - decimal
                                              0 , -- ltCombustiblePTO - decimal
                                              0 , -- ltCombustibleEST - decimal
                                              N'' , -- tiempoTotal - nvarchar(15)
                                              N'' , -- tiempoPTO - nvarchar(15)
                                              N'' , -- tiempoEST - nvarchar(15)
                                              0 , -- sello1 - int
                                              0 , -- sello2 - int
                                              0  -- selloRes - int
                                            );
                                END;
                        END;
                    ELSE
                        BEGIN 
                            UPDATE  dbo.salidasEntradas
                            SET     fechaFin = @fechaFin ,
                                    tipoOperacion = @tipoOperacion
                            WHERE   idSalidaEntrada = @idOperacion;
                        END;
                    FETCH NEXT FROM cursor_ INTO @idOperacion, @idPadreOrdSer,
                        @idChofer, @idUnidadTrans, @fecha, @fechaFin,
                        @tipoOperacion, @KM, @idUsuario;
                        
                END;
            CLOSE cursor_;    
            DEALLOCATE cursor_;
            SET @valor = 0;
            SET @mensaje = 'Operación Guardada Correctamente';
            COMMIT TRAN guardar;
        END TRY
        BEGIN CATCH
            DEALLOCATE cursor_;
            SET @valor = 1;
            SET @mensaje = ERROR_MESSAGE();
            ROLLBACK TRAN guardar;
        END CATCH;
    END;
GO
