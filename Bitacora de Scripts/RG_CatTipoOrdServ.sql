USE [DBFleteraPrueba]
GO
/****** Object:  Table [dbo].[CatTipoOrdServ]    Script Date: 09/25/2016 17:42:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CatTipoOrdServ](
	[idTipOrdServ] [int] IDENTITY(1,1) NOT NULL,
	[NomTipOrdServ] [varchar](50) NULL,
	[NumOrden] [int] NULL,
 CONSTRAINT [PK_CatTipoOrdServ] PRIMARY KEY CLUSTERED 
(
	[idTipOrdServ] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[CatTipoOrdServ] ON
INSERT [dbo].[CatTipoOrdServ] ([idTipOrdServ], [NomTipOrdServ], [NumOrden]) VALUES (1, N'COMBUSTIBLE', 1)
INSERT [dbo].[CatTipoOrdServ] ([idTipOrdServ], [NomTipOrdServ], [NumOrden]) VALUES (2, N'TALLER', 2)
INSERT [dbo].[CatTipoOrdServ] ([idTipOrdServ], [NomTipOrdServ], [NumOrden]) VALUES (3, N'LLANTAS', 3)
INSERT [dbo].[CatTipoOrdServ] ([idTipOrdServ], [NomTipOrdServ], [NumOrden]) VALUES (4, N'LAVADERO', 4)
SET IDENTITY_INSERT [dbo].[CatTipoOrdServ] OFF
