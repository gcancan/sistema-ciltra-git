
GO
/****** Object:  StoredProcedure [dbo].[sp_getUnidadesALLorById]    Script Date: 09/26/2016 12:18:07 ******/
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
-- =============================================
-- Author:		<Author,GRCC,Name>
-- Create date: <Create Date,11/03/2016,>
-- Description:	<Description, se migran las tablas,>
-- =============================================
ALTER PROCEDURE [dbo].[sp_getUnidadesALLorById]
    @valor INT OUT ,
    @mensaje NVARCHAR(MAX) OUT ,
    @id NVARCHAR(MAX) ,
    @idEmpresa INT
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
        BEGIN TRY
            IF @id = ''
                BEGIN 
                    SELECT  unidad.idEmpresa AS Clave_empresa ,
                            unidad.idUnidadTrans AS Clave ,
                            unidad.idMarca AS Clave_marca_de_unidad ,
                            unidad.descripcionUni AS Clasificacion ,
                            unidad.Estatus ,
                            ISNULL(unidad.rfid, '') AS rfid ,        
        ---------------------------------------------
                            unidad.idTipoUnidad AS Clave_tipo_de_unidad ,
                            tipo.clasificacion AS tipoUnidad_clasificacion ,
                            tipo.NoEjes AS tipoUnidad_noEjes ,
                            tipo.NoLlantas AS tipoUnidad_NoLlantas ,
                            tipo.TonelajeMax AS tipoUnidad_TonelajeMax ,
                            tipo.nomTipoUniTras AS tipoUnidad_descripcion ,
        ----------------------------------------------
                            unidad.idEmpresa AS Clave_empresa ,
                            emp.RazonSocial AS empresa_nombre ,
                            emp.Telefono AS empresa_telefono ,
                            emp.Direccion AS empresa_direccion ,
                            emp.Fax AS empresa_fax ,
        ----------------------------------------------
                            mot.idTipoMotor AS motor_idTipoMotor ,
                            mot.tipoMotor AS motor_tipoMotor ,
                            mot.porcTolerancia AS motor_tolerancia
                    FROM    dbo.CatUnidadTrans AS unidad
                            JOIN dbo.CatTipoUniTrans AS tipo ON tipo.idTipoUniTras = unidad.idTipoUnidad
                            JOIN dbo.CatEmpresas AS emp ON emp.idEmpresa = unidad.idEmpresa
                            LEFT JOIN dbo.tipoMores AS mot ON mot.idTipoMotor = unidad.idTipoMotor
                    WHERE   tipo.clasificacion IN ( 'tractor', 'dolly',
                                                    'tolva' )
                            AND unidad.idEmpresa = @idEmpresa
                            AND unidad.Estatus = 'ACT';
                END;
            ELSE
                BEGIN  
                    SELECT  unidad.idEmpresa AS Clave_empresa ,
                            unidad.idUnidadTrans AS Clave ,
                            unidad.idMarca AS Clave_marca_de_unidad ,
                            unidad.descripcionUni AS Clasificacion ,
                            unidad.Estatus ,
                            ISNULL(unidad.rfid, '') AS rfid ,        
        ---------------------------------------------
                            unidad.idTipoUnidad AS Clave_tipo_de_unidad ,
                            tipo.clasificacion AS tipoUnidad_clasificacion ,
                            tipo.NoEjes AS tipoUnidad_noEjes ,
                            tipo.NoLlantas AS tipoUnidad_NoLlantas ,
                            tipo.TonelajeMax AS tipoUnidad_TonelajeMax ,
                            tipo.nomTipoUniTras AS tipoUnidad_descripcion ,
        ----------------------------------------------
                            unidad.idEmpresa AS Clave_empresa ,
                            emp.RazonSocial AS empresa_nombre ,
                            emp.Telefono AS empresa_telefono ,
                            emp.Direccion AS empresa_direccion ,
                            emp.Fax AS empresa_fax ,
        ----------------------------------------------
                            mot.idTipoMotor AS motor_idTipoMotor ,
                            mot.tipoMotor AS motor_tipoMotor ,
                            mot.porcTolerancia AS motor_tolerancia
                    FROM    dbo.CatUnidadTrans AS unidad
                            JOIN dbo.CatTipoUniTrans AS tipo ON tipo.idTipoUniTras = unidad.idTipoUnidad
                            JOIN dbo.CatEmpresas AS emp ON emp.idEmpresa = unidad.idEmpresa
                            LEFT JOIN dbo.tipoMores AS mot ON mot.idTipoMotor = unidad.idTipoMotor
                    WHERE   tipo.clasificacion IN ( 'tractor', 'dolly',
                                                    'tolva' )
                            AND unidad.Estatus = 'ACT'
                            AND unidad.idUnidadTrans LIKE @id;
                            
                END;
            IF @@ROWCOUNT <> 0
                BEGIN
                    SET @valor = 0;
                    SET @mensaje = 'Se encontro registros';
                END;
            ELSE
                BEGIN
                    SET @valor = 3;
                    SET @mensaje = 'No se encontraron registros';
                END; 
        END TRY
        BEGIN CATCH
            SET @valor = 1;
            SET @mensaje = ERROR_MESSAGE();
        END CATCH;
    END;
