
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
-- =============================================
-- Author:		GRCC
-- Create date: 13/12/2016
-- Description:	sp para buscar los destinos por origen
-- =============================================
CREATE PROCEDURE sp_getDestinosByOrigenAndIdCliente 
	-- Add the parameters for the stored procedure here
    @valor INT OUT ,
    @mensaje NVARCHAR(MAX) OUT ,
    @idOrigen INT ,
    @idCliente INT
AS
    BEGIN
        SET NOCOUNT ON;	
        BEGIN TRY
            SELECT  *
            FROM    dbo.destinosAtlante_vw
            WHERE   idOrigen = @idOrigen
                    AND cliente_idCliente = @idCliente;
            IF @@ROWCOUNT > 0
                BEGIN 
                    SET @valor = 0;
                    SET @mensaje = 'SE ENCONTRARON REGISTROS';
                END;
            ELSE
                BEGIN
                    SET @valor = 3;
                    SET @mensaje = 'NO SE ENCONTRARON DESTINOS PARA ESTE CLIENTE Y ORIGEN';
                END;
        END TRY
        BEGIN CATCH
            SET @valor = 1;
            SET @mensaje = ERROR_MESSAGE();
        END CATCH;
	
    END;
GO
