CREATE TABLE origenDestinos
    (
      idOrigenDestino INT PRIMARY KEY
                          IDENTITY(1, 1) ,
      idOrigen INT FOREIGN KEY REFERENCES dbo.origen ( idOrigen )
                   NOT NULL ,
      idDestino INT NOT NULL ,
      precioSencillo DECIMAL(16, 4) NOT NULL ,
      precioFull DECIMAL(16, 4) NOT NULL ,
      km DECIMAL(16, 4),
    );