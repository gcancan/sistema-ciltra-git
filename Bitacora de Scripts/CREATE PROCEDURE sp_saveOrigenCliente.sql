SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
-- =============================================
-- Author:		GRCC
-- Create date: 15/12/2015
-- Description:	sp para guardar origenes del cliente
-- =============================================
CREATE PROCEDURE sp_saveOrigenCliente
    @valor INT OUT ,
    @mensaje NVARCHAR(MAX) OUT ,
    @identity INT OUT ,
    @nombre NVARCHAR(100) ,
    @estado NVARCHAR(30) ,
    @clasificacion NVARCHAR(50) ,
    @idCliente INT
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
        BEGIN TRAN [save];
        BEGIN TRY	
        
            DECLARE @id INT; 
            SET @id = ISNULL(( SELECT   idOrigen
                               FROM     dbo.origen
                               WHERE    nombreOrigen = @nombre
                                        AND estado = @estado
                             ), 0);
            IF @id = 0
                BEGIN
                    INSERT  INTO dbo.origen
                            ( nombreOrigen ,
                              estado ,
                              clasificacion
                            )
                    VALUES  ( @nombre , -- nombreOrigen - nvarchar(100)
                              @estado , -- estado - nvarchar(30)
                              @clasificacion  -- clasificacion - nvarchar(100)
                            );
                    SET @id = @@IDENTITY;
                END;
            IF NOT EXISTS ( SELECT  *
                            FROM    dbo.ClienteOrigen
                            WHERE   idCliente = @idCliente
                                    AND idOrigen = @id )
                BEGIN
                    INSERT  INTO dbo.ClienteOrigen
                            ( idCliente, idOrigen )
                    VALUES  ( @idCliente, -- idCliente - int
                              @id  -- idOrigen - int
                              );
                END;
			
            SET @identity = @id;
            SET @valor = 0;
            SET @mensaje = 'REGISTROS GUARDADOS CON EXITO';
            COMMIT TRAN [save];
        END TRY
        BEGIN CATCH
            SET @valor = 1;
            SET @mensaje = ERROR_MESSAGE();
            ROLLBACK TRAN [save];
        END CATCH;
    END;
GO
