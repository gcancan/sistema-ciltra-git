
GO
/****** Object:  StoredProcedure [dbo].[sp_getTotalViajesActivos]    Script Date: 12/29/2016 14:21:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,GRCC,Name>
-- Create date: <Create Date,29/12/2016,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_getTotalViajesFinalizados]
    @valor INT OUT ,
    @mensaje NVARCHAR(MAX) OUT ,
    @idCliente INT = 0
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        BEGIN TRY
            SET NOCOUNT ON;
            SELECT  MIN(FechaHoraViaje) AS fecha ,
                    COUNT(NumGuiaId) AS countViajes
            FROM    dbo.CabGuia
            WHERE   EstatusGuia = 'FINALIZADO'
                    AND idCliente  = 1 CASE @idCliente
                                         WHEN 0 THEN '%'
                                         ELSE CAST(@idCliente AS NVARCHAR(10))
                                       END;
            SET @valor = 0;
            SET @mensaje = 'Se econtraron resultados';
        END TRY 
        BEGIN CATCH
        END CATCH;
    END;
