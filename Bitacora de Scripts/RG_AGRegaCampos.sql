USE DBFleteraPrueba

/*23-SEP-2016*/
ALTER TABLE dbo.CabOrdenServicio ADD idTipOrdServ VARCHAR(6) NULL
ALTER TABLE dbo.CabOrdenServicio ADD idTipoServicio INT NULL
ALTER TABLE dbo.CabOrdenServicio ADD idPadreOrdSer INT NULL

/*27-SEP-2016*/
ALTER TABLE dbo.CatServicios ADD idDivision INT NULL

/*28-SEP-2016*/
ALTER TABLE dbo.Parametros ADD idPadreOrdSer INT NULL
ALTER TABLE dbo.Parametros ADD idOrdenSer INT NULL

/*29-SEP-2016*/
ALTER TABLE dbo.Parametros ADD idOrdenTrabajo INT NULL
UPDATE dbo.Parametros SET idPadreOrdSer = 0, idOrdenSer = 0, idOrdenTrabajo = 0

/*3-OCT-2016*/
ALTER TABLE dbo.CatTipoUniTrans ADD bCombustible bit NULL

/*11-OCT-2016*/
ALTER TABLE dbo.DetOrdenServicio ADD idDivision INT NULL
