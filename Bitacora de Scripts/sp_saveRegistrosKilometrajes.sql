
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
-- =============================================
-- Author:		<Author,GRCC,Name>
-- Create date: <Create Date,07/10/2016,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE sp_saveRegistrosKilometrajes
    @idUnidadTrans NVARCHAR(50) ,
    @km DECIMAL(16, 4) ,
    @usuario INT ,
    @nomTabla NVARCHAR(30) ,
    @idTabla INT ,
    @validado INT = 0 ,
    @valor INT = 0 OUT ,
    @mensaje NVARCHAR(MAX) = '' OUT ,
    @identity INT = 0 OUT ,
    @kmInicial DECIMAL(16, 4) = 0 OUT ,
    @kmFinal DECIMAL(16, 4) = 0 OUT
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
        BEGIN TRAN guardar;
        BEGIN TRY
            
            SET @kmInicial = ( ISNULL(( SELECT TOP 1
                                                kmInicial
                                        FROM    dbo.registrosKilometros
                                        WHERE   validado = 1
                                                AND idUnidadTrans = @idUnidadTrans
                                        ORDER BY fecha DESC
                                      ), 0) );
                                      
            SET @kmFinal = @km;
            
            INSERT  INTO dbo.registrosKilometros
                    ( idUnidadTrans ,
                      kmInicial ,
                      kmFinal ,
                      fecha ,
                      usuario ,
                      nomTabla ,
                      idTabla ,
                      activo ,
                      validado
                    )
            VALUES  ( @idUnidadTrans , -- idUnidadTrans - varchar(10)
                      @kmInicial , -- kmInicial - decimal
                      @kmFinal , -- kmFinal - decimal
                      GETDATE() , -- fecha - datetime
                      @usuario , -- usuario - int
                      @nomTabla , -- nomTabla - nvarchar(25)
                      @idTabla , -- idTabla - int
                      1 , -- activo - bit
                      @validado  -- validado - bit
                    );
            
                    
            SET @identity = @@IDENTITY;
            SET @valor = 0;
            SET @mensaje = 'Operación Guardada Correctamente';
                    
            COMMIT TRAN guardar;
        END TRY
        BEGIN CATCH
            SET @identity = 0;
            SET @valor = 1;
            SET @mensaje = ERROR_MESSAGE();
            ROLLBACK TRAN guardar;
        END CATCH;        
    END;
GO
