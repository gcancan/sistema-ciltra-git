--DROP TABLE dbo.remisionesLiberadas
CREATE TABLE remisionesLiberadas
    (
      idRemisionesLiberadas INT PRIMARY KEY
                                IDENTITY(1, 1) ,
      idCliente INT NOT NULL ,
      remision INT NOT NULL ,
      fecha DATETIME NOT NULL
                     DEFAULT GETDATE() ,
      activo BIT DEFAULT 1
    );
    