
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE sp_saveCargaGasolina
    @idCargaCombustible INT ,
    @idOrdenServ INT ,
    @kmDespachador DECIMAL(16, 4) = 0 ,
    @kmRecorridos DECIMAL(18, 4) = 0 ,
    @ltCombustible DECIMAL(18, 4) = 0 ,
    @ltCombustibleUtilizado DECIMAL(18, 4) = 0 ,
    @ltCombustiblePTO DECIMAL(18, 4) = 0 ,
    @ltCombustibleEST DECIMAL(18, 4) = 0 ,
    @tiempoTotal NVARCHAR(20) = NULL ,
    @tiempoPTO NVARCHAR(20) = NULL ,
    @tiempoEST NVARCHAR(20) = NULL ,
    @idDespachador INT = NULL ,
    @sello1 INT = NULL ,
    @sello2 INT = NULL ,
    @selloRes INT = NULL ,
    @idUsuario INT = 1 ,
    @valor INT OUT ,
    @mensaje NVARCHAR(MAX) OUT ,
    @inicial DECIMAL(16, 4) = 0 OUT ,
    @final DECIMAL(16, 4) = 0 OUT ,
    @xml XML = NULL
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN TRAN guardar;
        BEGIN TRY
           
            DECLARE @idUnidad VARCHAR(10) = ( SELECT    idUnidadTrans
                                              FROM      dbo.CabOrdenServicio
                                              WHERE     idOrdenSer = @idOrdenServ
                                            ); 
                                            
            IF @xml IS NULL
                BEGIN
                    UPDATE  dbo.DetGuia
                    SET     idCargaGasolina = @idCargaCombustible
                    WHERE   NumGuiaId IN (
                            SELECT  NumGuiaId
                            FROM    dbo.CabGuia
                            WHERE   FechaHoraViaje BETWEEN ( SELECT
                                                              MAX(fechaCombustible)
                                                             FROM
                                                              dbo.cargaCombustible
                                                             WHERE
                                                              idOrdenServ IN (
                                                              SELECT
                                                              idOrdenSer
                                                              FROM
                                                              dbo.CabOrdenServicio
                                                              WHERE
                                                              idUnidadTrans = @idUnidad )
                                                           )
                                                   AND     GETDATE()
                                    AND idTractor = @idUnidad );
                END;
            ELSE
                BEGIN
                    DECLARE @DocHandle INT;
                    DECLARE @tablaConsecutivos TABLE ( consecutivo INT );
                    EXEC sp_xml_preparedocument @DocHandle OUTPUT, @xml;
                    INSERT  INTO @tablaConsecutivos
                            SELECT  ( consecutivo )
                            FROM    OPENXML (@DocHandle, '/listaConsecutivos/consecutivos', 2)
							WITH (consecutivo INT);
                    UPDATE  dbo.DetGuia
                    SET     idCargaGasolina = @idCargaCombustible
                    WHERE   Consecutivo IN ( SELECT consecutivo
                                             FROM   @tablaConsecutivos );
                END;
            IF NOT EXISTS ( SELECT  *
                            FROM    dbo.registrosKilometros
                            WHERE   idUnidadTrans = @idUnidad
                                    AND validado = 1 )
                BEGIN
                    DECLARE @km DECIMAL(16, 4)= ( SELECT    KM
                                                  FROM      dbo.salidasEntradas
                                                  WHERE     idSalidaEntrada = ( SELECT
                                                              s.idSalidaEntrada
                                                              FROM
                                                              dbo.cargaCombustible
                                                              AS s
                                                              WHERE
                                                              s.idOrdenServ = @idOrdenServ
                                                              )
                                                );
                    SET @inicial = @km - @kmRecorridos;
                    SET @final = @km;    
                END;
            ELSE
                BEGIN
                        
                        
                    SET @inicial = ( SELECT TOP 1
                                            kmFinal
                                     FROM   dbo.registrosKilometros
                                     WHERE  idUnidadTrans = @idUnidad
                                            AND validado = 1
                                     ORDER BY fecha DESC
                                   );
                                           
                    SET @final = ( ( SELECT TOP 1
                                            kmFinal
                                     FROM   dbo.registrosKilometros
                                     WHERE  idUnidadTrans = @idUnidad
                                            AND validado = 1
                                     ORDER BY fecha DESC
                                   ) + @kmRecorridos );                                    
                    --SET @FinalKm = @final;
                END;
            INSERT  INTO dbo.registrosKilometros
                    ( idUnidadTrans ,
                      kmInicial ,
                      kmFinal ,
                      fecha ,
                      usuario ,
                      nomTabla ,
                      idTabla ,
                      activo ,
                      validado
                    )
            VALUES  ( @idUnidad , -- idUnidadTrans - varchar(10)
                      @inicial , -- kmInicial - decimal
                      @final , -- kmFinal - decimal
                      GETDATE() , -- fecha - datetime
                      @idUsuario , -- usuario - int
                      N'CargaCombustible' , -- nomTabla - nvarchar(25)
                      @idCargaCombustible , -- idTabla - int
                      1 , -- activo - bit
                      1  -- validado - bit
                    );           
                       
            UPDATE  dbo.cargaCombustible
            SET     kmRecorridos = @kmRecorridos ,
                    kmFinal = @final ,
                    ltCombustible = @ltCombustible ,
                    ltCombustibleUtilizado = @ltCombustibleUtilizado ,
                    ltCombustiblePTO = @ltCombustiblePTO ,
                    ltCombustibleEST = @ltCombustibleEST ,
                    fechaCombustible = GETDATE() ,
                    tiempoTotal = @tiempoTotal ,
                    tiempoPTO = @tiempoPTO ,
                    tiempoEST = @tiempoEST ,
                    idDespachador = @idDespachador ,
                    sello1 = @sello1 ,
                    sello2 = @sello2 ,
                    selloRes = @selloRes ,
                    kmDespachador = @kmDespachador
            WHERE   idCargaCombustible = @idCargaCombustible;
            
            
            
            SET @valor = 0;
            SET @mensaje = 'Operación realizada con exito';
            COMMIT TRAN guardar;
        END TRY
        BEGIN CATCH
            SET @valor = 1;
            SET @mensaje = ERROR_MESSAGE();
            ROLLBACK TRAN guardar;
        END CATCH;

    END;
GO
