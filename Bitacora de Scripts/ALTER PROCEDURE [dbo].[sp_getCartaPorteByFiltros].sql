
GO
/****** Object:  StoredProcedure [dbo].[sp_getCartaPorteByFiltros]    Script Date: 12/29/2016 13:47:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,GRCC,Name>
-- Create date: <Create Date,05/05/2016,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[sp_getCartaPorteByFiltros]
    @valor INT OUT ,
    @mensaje NVARCHAR(MAX) OUT ,
    @fechaInicio DATETIME ,
    @fechaFin DATETIME ,
    @idDestino NVARCHAR(10) ,
    @idOperador NVARCHAR(10) ,
    @idTractor NVARCHAR(10) ,
    @estatus NVARCHAR(20) ,
    @idcliente INT
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
        BEGIN TRY

            SELECT  g.NumGuiaId ,
                    g.SerieGuia ,
                    NumViaje ,
                    --idCliente ,
                    IdEmpresa ,
                    FechaHoraViaje ,
                    idOperador ,
                    idTractor ,
                    idRemolque1 ,
                    ISNULL(idDolly, '') AS idDolly ,
                    ISNULL(idRemolque2, '') AS idRemolque2 ,
                    FechaHoraFin ,
                    EstatusGuia ,
                    g.tara ,
                    TipoViaje ,
                    c.* ,
                    per.* ,
                    km
            FROM    dbo.CabGuia AS g
                    JOIN dbo.cliente_vw AS c ON c.cliente_idCliente = g.idCliente
                    JOIN dbo.personal_vw AS per ON g.idOperador = per.persona_idPersonal
                    JOIN dbo.DetGuia AS d ON d.NumGuiaId = g.NumGuiaId
            WHERE   FechaHoraViaje BETWEEN @fechaInicio AND @fechaFin
                    AND idOperador LIKE @idOperador
                    AND idTractor LIKE @idTractor
                    AND g.EstatusGuia LIKE @estatus
                    AND g.idCliente = @idcliente
                    AND d.IdPlazaTraf LIKE @idDestino
            ORDER BY g.NumGuiaId ASC;
            
            IF @@ROWCOUNT <> 0
                BEGIN
                    SET @valor = 0;
                    SET @mensaje = 'Se encontro registros';
                END;
            ELSE
                BEGIN
                    SET @valor = 3;
                    SET @mensaje = '*No se encontraron registros';
                END; 
        END TRY
        BEGIN CATCH
            SET @valor = 1;
            SET @mensaje = ERROR_MESSAGE();
        END CATCH;
    END;
