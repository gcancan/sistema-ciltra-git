
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
-- =============================================
-- Author:		GRCC
-- Create date: 12/10/2016
-- Description:	sp para buscar si ya tiene carga de conbustible
-- =============================================
CREATE PROCEDURE sp_getCargasConbustibleByIdUnidad 
	-- Add the parameters for the stored procedure here
    @valor INT OUT ,
    @mensaje NVARCHAR(MAX) OUT ,
    @idUnidad NVARCHAR(20)
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN TRY
            SELECT  idCargaCombustible ,
                    idSalidaEntrada ,
                    idOrdenServ ,
                    fechaCombustible ,
                    kmFinal ,
                    kmRecorridos ,
                    kmDespachador ,
                    ltCombustible ,
                    ltCombustibleUtilizado ,
                    ltCombustiblePTO ,
                    ltCombustibleEST ,
                    tiempoTotal ,
                    tiempoPTO ,
                    tiempoEST ,
                    sello1 ,
                    sello2 ,
                    selloRes ,
                    idDespachador
            FROM    dbo.cargaCombustible
            WHERE   idOrdenServ IN ( SELECT idOrdenSer
                                     FROM   dbo.CabOrdenServicio
                                     WHERE  idUnidadTrans = @idUnidad )
            ORDER BY fechaCombustible DESC;
            IF @@ROWCOUNT > 0
                BEGIN
                    SET @valor = 0;
                    SET @mensaje = 'Se encontraron resultados';
                END;
            ELSE
                BEGIN
                    SET @valor = 3;
                    SET @mensaje = 'No se encontraron registros';
                END;
        END TRY
        BEGIN CATCH
            SET @valor = 1;
            SET @mensaje = ERROR_MESSAGE();
        END CATCH;
    END;
GO
