USE [DBFleteraPrueba];
GO
/****** Object:  StoredProcedure [dbo].[sp_getDetallesCartaPorteById]    Script Date: 12/14/2016 13:44:03 ******/
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
-- =============================================
-- Author:		<Author,GRCC,Name>
-- Create date: <Create Date,07/04/2016,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_getDetallesCartaPorteByIdAtlante]
    @valor INT OUT ,
    @mensaje NVARCHAR(MAX) OUT ,
    @id INT
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
        BEGIN TRY
            SELECT  Consecutivo ,
                    NumGuiaId ,
                    SerieGuia ,
                    NoRemision ,
                    IdProdTraf ,
                    IdPlazaTraf ,
                    VolDescarga ,
                    precio ,
                    PorcIVA ,
                    ImpIVA ,
                    PorcRetencion ,
                    ImpRetencion ,
                    idTarea ,
                    subTotal ,
                    tara ,
                    horaLlegada ,
                    horaCaptura ,
                    horaImpresion ,
                    pesoBruto ,
                    observaciones ,
                    v.* ,
                    pro.*
            FROM    dbo.DetGuia AS d
                    JOIN dbo.destinosAtlante_vw AS v ON v.idOrigen = d.idOrigen
                                                        AND d.IdPlazaTraf = v.destino_clave
                                                        AND v.cliente_idCliente = ( SELECT
                                                              idCliente
                                                              FROM
                                                              dbo.CabGuia
                                                              WHERE
                                                              NumGuiaId = @id
                                                              )
                    JOIN dbo.productos_vw AS pro ON pro.Clave = d.IdProdTraf
            WHERE   NumGuiaId = @id
                    AND baja = 0;
            IF @@ROWCOUNT <> 0
                BEGIN
                    SET @valor = 0;
                    SET @mensaje = 'Se encontro registros';
                END;
            ELSE
                BEGIN
                    SET @valor = 3;
                    SET @mensaje = 'No se encontraron registros';
                END; 
        END TRY
        BEGIN CATCH
            SET @valor = 1;
            SET @mensaje = ERROR_MESSAGE();
        END CATCH;
    END;
