
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
-- =============================================
-- Author:		<Author,GRCC,Name>
-- Create date: <Create Date,07/10/2016,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE sp_getRegistroKmByIdUnidad
    @valor INT OUT ,
    @mensaje NVARCHAR(MAX) OUT ,
    @idUnidad NVARCHAR(20)
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
        BEGIN TRY
            SELECT TOP 1
                    idRegistrosKilometraje ,
                    idUnidadTrans ,
                    kmInicial ,
                    kmFinal ,
                    fecha ,
                    usuario ,
                    nomTabla ,
                    idTabla ,
                    activo ,
                    validado
            FROM    dbo.registrosKilometros
            WHERE   validado = 1
                    AND idUnidadTrans = @idUnidad
                    AND activo = 1
            ORDER BY fecha DESC;
            IF @@ROWCOUNT <> 0
                BEGIN
                    SET @valor = 0;
                    SET @mensaje = 'Se encontro registros';
                END;
            ELSE
                BEGIN
                    SET @valor = 3;
                    SET @mensaje = 'No se encontro registro';
                END;
        END TRY
        BEGIN CATCH
        SET @valor = 1;
                    SET @mensaje = ERROR_MESSAGE();
        END CATCH;
    END;
GO
