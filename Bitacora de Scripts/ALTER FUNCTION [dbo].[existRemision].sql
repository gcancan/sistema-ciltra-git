
GO
/****** Object:  UserDefinedFunction [dbo].[existRemision]    Script Date: 11/28/2016 15:41:22 ******/
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
-- =============================================
-- Author:		<Author,GRCC,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, GRCC,>
-- =============================================
ALTER FUNCTION [dbo].[existRemision]
    (
      @xml XML ,
      @idCliente INT
    )
RETURNS NVARCHAR(MAX)
AS
    BEGIN
        DECLARE @DocHandle INT;
        DECLARE @tableDetalles TABLE ( NoRemision INT );
        EXEC sp_xml_preparedocument @DocHandle OUTPUT, @xml;
        INSERT  @tableDetalles
                ( NoRemision
                )
                SELECT  NoRemision
                FROM    OPENXML (@DocHandle, '/DetallesCartaPorte/Detalles', 2)
			WITH (
                  NoRemision INT ); 
        EXEC sp_xml_removedocument @DocHandle;  
            
        DECLARE cursor_ CURSOR
        FOR
            SELECT  NoRemision
            FROM    @tableDetalles;
           
        DECLARE @NoRemision INT;
            
        OPEN cursor_;
            
            --DEALLOCATE cursor_;
        FETCH NEXT FROM cursor_ INTO @NoRemision;
                
        WHILE @@FETCH_STATUS = 0
            BEGIN
                IF EXISTS ( SELECT  *
                            FROM    dbo.DetGuia AS d
                                    JOIN dbo.CabGuia AS c ON c.NumGuiaId = d.NumGuiaId
                            WHERE   d.NoRemision = @NoRemision
                                    AND c.EstatusGuia <> 'Cancelado'
                                    AND c.idCliente = @idCliente )
                    BEGIN
                        IF NOT EXISTS ( SELECT  *
                                        FROM    dbo.remisionesLiberadas
                                        WHERE   remision = @NoRemision
                                                AND idCliente = @idCliente AND activo = 1)
                            BEGIN
                                RETURN 'La remisión ' + CONVERT(NVARCHAR(50), @NoRemision) + ' ya se encuentra registrado en el sistema';
                            END;
                    END;
                FETCH NEXT FROM cursor_ INTO @NoRemision;
            END;
        CLOSE cursor_;    
        DEALLOCATE cursor_;
        RETURN '';
    END;
